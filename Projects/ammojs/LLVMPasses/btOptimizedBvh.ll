; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btOptimizedBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btOptimizedBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%struct.QuantizedNodeTriangleCallback = type { %class.btInternalTriangleIndexCallback, %class.btAlignedObjectArray.0*, %class.btQuantizedBvh* }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%struct.NodeTriangleCallback = type { %class.btInternalTriangleIndexCallback, %class.btAlignedObjectArray* }
%class.btSerializer = type opaque
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }

$_ZN14btOptimizedBvhdlEPv = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN18btOptimizedBvhNodeC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_ = comdat any

$_ZN16btBvhSubtreeInfoC2Ev = comdat any

$_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK18btQuantizedBvhNode10isLeafNodeEv = comdat any

$_ZNK18btQuantizedBvhNode14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i = comdat any

$_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZNK18btQuantizedBvhNode9getPartIdEv = comdat any

$_ZNK18btQuantizedBvhNode16getTriangleIndexEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv = comdat any

$_ZNK14btOptimizedBvh16serializeInPlaceEPvjb = comdat any

$_ZN31btInternalTriangleIndexCallbackC2Ev = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi = comdat any

$_ZN18btQuantizedBvhNodenwEmPv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi = comdat any

$_ZN18btOptimizedBvhNodenwEmPv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z8btSelectjii = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi = comdat any

$_ZN16btBvhSubtreeInfonwEmPv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv = comdat any

@_ZTV14btOptimizedBvh = hidden unnamed_addr constant { [10 x i8*] } { [10 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btOptimizedBvh to i8*), i8* bitcast (%class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD1Ev to i8*), i8* bitcast (void (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD0Ev to i8*), i8* bitcast (i1 (%class.btQuantizedBvh*, i8*, i32, i1)* @_ZNK14btQuantizedBvh9serializeEPvjb to i8*), i8* bitcast (i32 (%class.btQuantizedBvh*)* @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv to i8*), i8* bitcast (i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData*)* @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData*)* @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData to i8*), i8* bitcast (i1 (%class.btOptimizedBvh*, i8*, i32, i1)* @_ZNK14btOptimizedBvh16serializeInPlaceEPvjb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS14btOptimizedBvh = hidden constant [17 x i8] c"14btOptimizedBvh\00", align 1
@_ZTI14btQuantizedBvh = external constant i8*
@_ZTI14btOptimizedBvh = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btOptimizedBvh, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btQuantizedBvh to i8*) }, align 4
@_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback to i8*), i8* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to i8*), i8* bitcast (void (%struct.QuantizedNodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev to i8*), i8* bitcast (void (%struct.QuantizedNodeTriangleCallback*, %class.btVector3*, i32, i32)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal constant [100 x i8] c"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback\00", align 1
@_ZTI31btInternalTriangleIndexCallback = external constant i8*
@_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([100 x i8], [100 x i8]* @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@_ZTV31btInternalTriangleIndexCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback to i8*), i8* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to i8*), i8* bitcast (void (%struct.NodeTriangleCallback*)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev to i8*), i8* bitcast (void (%struct.NodeTriangleCallback*, %class.btVector3*, i32, i32)* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii to i8*)] }, align 4
@_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal constant [91 x i8] c"ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback\00", align 1
@_ZTIZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([91 x i8], [91 x i8]* @_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4

@_ZN14btOptimizedBvhC1Ev = hidden unnamed_addr alias %class.btOptimizedBvh* (%class.btOptimizedBvh*), %class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhC2Ev
@_ZN14btOptimizedBvhD1Ev = hidden unnamed_addr alias %class.btOptimizedBvh* (%class.btOptimizedBvh*), %class.btOptimizedBvh* (%class.btOptimizedBvh*)* @_ZN14btOptimizedBvhD2Ev

define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvhC2Ev(%class.btOptimizedBvh* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* %0)
  %1 = bitcast %class.btOptimizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [10 x i8*] }, { [10 x i8*] }* @_ZTV14btOptimizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btOptimizedBvh* %this1
}

declare %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* returned) unnamed_addr #1

; Function Attrs: nounwind
declare %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* returned) unnamed_addr #2

; Function Attrs: nounwind
define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvhD2Ev(%class.btOptimizedBvh* returned %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* %0) #9
  ret %class.btOptimizedBvh* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN14btOptimizedBvhD0Ev(%class.btOptimizedBvh* %this) unnamed_addr #3 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %call = call %class.btOptimizedBvh* @_ZN14btOptimizedBvhD1Ev(%class.btOptimizedBvh* %this1) #9
  %0 = bitcast %class.btOptimizedBvh* %this1 to i8*
  call void @_ZN14btOptimizedBvhdlEPv(i8* %0) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN14btOptimizedBvhdlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %triangles, i1 zeroext %useQuantizedAabbCompression, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax) #0 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %triangles.addr = alloca %class.btStridingMeshInterface*, align 4
  %useQuantizedAabbCompression.addr = alloca i8, align 1
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %numLeafNodes = alloca i32, align 4
  %callback = alloca %struct.QuantizedNodeTriangleCallback, align 4
  %ref.tmp = alloca %struct.btQuantizedBvhNode, align 4
  %callback8 = alloca %struct.NodeTriangleCallback, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp23 = alloca %struct.btOptimizedBvhNode, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp32 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %triangles, %class.btStridingMeshInterface** %triangles.addr, align 4, !tbaa !2
  %frombool = zext i1 %useQuantizedAabbCompression to i8
  store i8 %frombool, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !8
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = load i8, i8* %useQuantizedAabbCompression.addr, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %0 to i1
  %1 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %1, i32 0, i32 6
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_useQuantization, align 4, !tbaa !11
  %2 = bitcast i32* %numLeafNodes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 0, i32* %numLeafNodes, align 4, !tbaa !22
  %3 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %3, i32 0, i32 6
  %4 = load i8, i8* %m_useQuantization3, align 4, !tbaa !11, !range !10
  %tobool4 = trunc i8 %4 to i1
  br i1 %tobool4, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %5 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %6 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  %7 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, float 1.000000e+00)
  %8 = bitcast %struct.QuantizedNodeTriangleCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %8) #9
  %9 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 10
  %10 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %call = call %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackC2ER20btAlignedObjectArrayI18btQuantizedBvhNodeEPK14btQuantizedBvh(%struct.QuantizedNodeTriangleCallback* %callback, %class.btAlignedObjectArray.0* nonnull align 4 dereferenceable(17) %m_quantizedLeafNodes, %class.btQuantizedBvh* %10)
  %11 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %triangles.addr, align 4, !tbaa !2
  %12 = bitcast %struct.QuantizedNodeTriangleCallback* %callback to %class.btInternalTriangleIndexCallback*
  %13 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %13, i32 0, i32 1
  %14 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %14, i32 0, i32 2
  %15 = bitcast %class.btStridingMeshInterface* %11 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %15, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %16 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %16(%class.btStridingMeshInterface* %11, %class.btInternalTriangleIndexCallback* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %17 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %17, i32 0, i32 10
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes5)
  store i32 %call6, i32* %numLeafNodes, align 4, !tbaa !22
  %18 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %18, i32 0, i32 11
  %19 = load i32, i32* %numLeafNodes, align 4, !tbaa !22
  %mul = mul nsw i32 2, %19
  %20 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #9
  %21 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %21, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %mul, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp)
  %22 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #9
  %call7 = call %struct.QuantizedNodeTriangleCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.QuantizedNodeTriangleCallback* (%struct.QuantizedNodeTriangleCallback*)*)(%struct.QuantizedNodeTriangleCallback* %callback) #9
  %23 = bitcast %struct.QuantizedNodeTriangleCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %23) #9
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = bitcast %struct.NodeTriangleCallback* %callback8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %24) #9
  %25 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %25, i32 0, i32 8
  %call9 = call %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackC2ER20btAlignedObjectArrayI18btOptimizedBvhNodeE(%struct.NodeTriangleCallback* %callback8, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_leafNodes)
  %26 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  %27 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp10, align 4, !tbaa !23
  %28 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp11, align 4, !tbaa !23
  %29 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp12, align 4, !tbaa !23
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %30 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #9
  %34 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  store float 0x43ABC16D60000000, float* %ref.tmp14, align 4, !tbaa !23
  %35 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  store float 0x43ABC16D60000000, float* %ref.tmp15, align 4, !tbaa !23
  %36 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #9
  store float 0x43ABC16D60000000, float* %ref.tmp16, align 4, !tbaa !23
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  %37 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #9
  %39 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #9
  %40 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %triangles.addr, align 4, !tbaa !2
  %41 = bitcast %struct.NodeTriangleCallback* %callback8 to %class.btInternalTriangleIndexCallback*
  %42 = bitcast %class.btStridingMeshInterface* %40 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable18 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %42, align 4, !tbaa !6
  %vfn19 = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable18, i64 2
  %43 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn19, align 4
  call void %43(%class.btStridingMeshInterface* %40, %class.btInternalTriangleIndexCallback* %41, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %44 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %44, i32 0, i32 8
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_leafNodes20)
  store i32 %call21, i32* %numLeafNodes, align 4, !tbaa !22
  %45 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %45, i32 0, i32 9
  %46 = load i32, i32* %numLeafNodes, align 4, !tbaa !22
  %mul22 = mul nsw i32 2, %46
  %47 = bitcast %struct.btOptimizedBvhNode* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %47) #9
  %48 = bitcast %struct.btOptimizedBvhNode* %ref.tmp23 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %48, i8 0, i32 64, i1 false)
  %call24 = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp23)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %mul22, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp23)
  %49 = bitcast %struct.btOptimizedBvhNode* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %49) #9
  %50 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #9
  %51 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #9
  %call25 = call %struct.NodeTriangleCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.NodeTriangleCallback* (%struct.NodeTriangleCallback*)*)(%struct.NodeTriangleCallback* %callback8) #9
  %52 = bitcast %struct.NodeTriangleCallback* %callback8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %52) #9
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %53 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %53, i32 0, i32 5
  store i32 0, i32* %m_curNodeIndex, align 4, !tbaa !25
  %54 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %55 = load i32, i32* %numLeafNodes, align 4, !tbaa !22
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %54, i32 0, i32 %55)
  %56 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization26 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %56, i32 0, i32 6
  %57 = load i8, i8* %m_useQuantization26, align 4, !tbaa !11, !range !10
  %tobool27 = trunc i8 %57 to i1
  br i1 %tobool27, label %land.lhs.true, label %if.end43

land.lhs.true:                                    ; preds = %if.end
  %58 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %58, i32 0, i32 13
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %tobool29 = icmp ne i32 %call28, 0
  br i1 %tobool29, label %if.end43, label %if.then30

if.then30:                                        ; preds = %land.lhs.true
  %59 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %60 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders31 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %60, i32 0, i32 13
  %61 = bitcast %class.btBvhSubtreeInfo* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %61) #9
  %call33 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp32)
  %call34 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders31, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp32)
  %62 = bitcast %class.btBvhSubtreeInfo* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %62) #9
  store %class.btBvhSubtreeInfo* %call34, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %63 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %64 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %64, i32 0, i32 11
  %call36 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes35, i32 0)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %63, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call36)
  %65 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %65, i32 0, i32 2
  store i32 0, i32* %m_rootNodeIndex, align 4, !tbaa !26
  %66 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes37 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %66, i32 0, i32 11
  %call38 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes37, i32 0)
  %call39 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %call38)
  br i1 %call39, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then30
  br label %cond.end

cond.false:                                       ; preds = %if.then30
  %67 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes40 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %67, i32 0, i32 11
  %call41 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes40, i32 0)
  %call42 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %call41)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call42, %cond.false ]
  %68 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %68, i32 0, i32 3
  store i32 %cond, i32* %m_subtreeSize, align 4, !tbaa !28
  %69 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  br label %if.end43

if.end43:                                         ; preds = %cond.end, %land.lhs.true, %if.end
  %70 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders44 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %70, i32 0, i32 13
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders44)
  %71 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %71, i32 0, i32 14
  store i32 %call45, i32* %m_subtreeHeaderCount, align 4, !tbaa !29
  %72 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedLeafNodes46 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %72, i32 0, i32 10
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes46)
  %73 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_leafNodes47 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %73, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %m_leafNodes47)
  %74 = bitcast i32* %numLeafNodes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

declare void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float) #1

; Function Attrs: nounwind
define internal %struct.QuantizedNodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackC2ER20btAlignedObjectArrayI18btQuantizedBvhNodeEPK14btQuantizedBvh(%struct.QuantizedNodeTriangleCallback* returned %this, %class.btAlignedObjectArray.0* nonnull align 4 dereferenceable(17) %triangleNodes, %class.btQuantizedBvh* %tree) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  %triangleNodes.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %tree.addr = alloca %class.btQuantizedBvh*, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.0* %triangleNodes, %class.btAlignedObjectArray.0** %triangleNodes.addr, align 4, !tbaa !2
  store %class.btQuantizedBvh* %tree, %class.btQuantizedBvh** %tree.addr, align 4, !tbaa !2
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #9
  %1 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_triangleNodes = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %triangleNodes.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.0* %2, %class.btAlignedObjectArray.0** %m_triangleNodes, align 4, !tbaa !2
  %m_optimizedTree = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %3 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %tree.addr, align 4, !tbaa !2
  store %class.btQuantizedBvh* %3, %class.btQuantizedBvh** %m_optimizedTree, align 4, !tbaa !30
  ret %struct.QuantizedNodeTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !32
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !22
  store %struct.btQuantizedBvhNode* %fillData, %struct.btQuantizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !22
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %2 = load i32, i32* %curSize, align 4, !tbaa !22
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  store i32 %4, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %6 = load i32, i32* %curSize, align 4, !tbaa !22
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !22
  store i32 %14, i32* %i6, align 4, !tbaa !22
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !22
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data11, align 4, !tbaa !33
  %19 = load i32, i32* %i6, align 4, !tbaa !22
  %arrayidx12 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %18, i32 %19
  %20 = bitcast %struct.btQuantizedBvhNode* %arrayidx12 to i8*
  %call13 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btQuantizedBvhNode*
  %22 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btQuantizedBvhNode* %21 to i8*
  %24 = bitcast %struct.btQuantizedBvhNode* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !34
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !22
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !22
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !32
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
declare %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned) unnamed_addr #2

; Function Attrs: nounwind
define internal %struct.NodeTriangleCallback* @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackC2ER20btAlignedObjectArrayI18btOptimizedBvhNodeE(%struct.NodeTriangleCallback* returned %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %triangleNodes) unnamed_addr #3 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  %triangleNodes.addr = alloca %class.btAlignedObjectArray*, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %triangleNodes, %class.btAlignedObjectArray** %triangleNodes.addr, align 4, !tbaa !2
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.NodeTriangleCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #9
  %1 = bitcast %struct.NodeTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_triangleNodes = getelementptr inbounds %struct.NodeTriangleCallback, %struct.NodeTriangleCallback* %this1, i32 0, i32 1
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %triangleNodes.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %2, %class.btAlignedObjectArray** %m_triangleNodes, align 4, !tbaa !2
  ret %struct.NodeTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !36
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !22
  store %struct.btOptimizedBvhNode* %fillData, %struct.btOptimizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !22
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %2 = load i32, i32* %curSize, align 4, !tbaa !22
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  store i32 %4, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %6 = load i32, i32* %curSize, align 4, !tbaa !22
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !22
  store i32 %14, i32* %i6, align 4, !tbaa !22
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !22
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data11, align 4, !tbaa !37
  %19 = load i32, i32* %i6, align 4, !tbaa !22
  %arrayidx12 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %18, i32 %19
  %20 = bitcast %struct.btOptimizedBvhNode* %arrayidx12 to i8*
  %call13 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btOptimizedBvhNode*
  %22 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btOptimizedBvhNode* %21 to i8*
  %24 = bitcast %struct.btOptimizedBvhNode* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 64, i1 false), !tbaa.struct !38
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !22
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !22
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !22
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !36
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %struct.btOptimizedBvhNode* %this, %struct.btOptimizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %this.addr, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMinOrg)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMaxOrg)
  ret %struct.btOptimizedBvhNode* %this1
}

declare void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh*, i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %this, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillValue) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %fillValue.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store %class.btBvhSubtreeInfo* %fillValue, %class.btBvhSubtreeInfo** %fillValue.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !22
  %1 = load i32, i32* %sz, align 4, !tbaa !22
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !39
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !39
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %4 = load i32, i32* %sz, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  %5 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call5 to %class.btBvhSubtreeInfo*
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillValue.addr, align 4, !tbaa !2
  %8 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  %9 = bitcast %class.btBvhSubtreeInfo* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 32, i1 false), !tbaa.struct !41
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %10 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data6, align 4, !tbaa !40
  %11 = load i32, i32* %sz, align 4, !tbaa !22
  %arrayidx7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %10, i32 %11
  %12 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret %class.btBvhSubtreeInfo* %arrayidx7
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  ret %class.btBvhSubtreeInfo* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %quantizedNode) #3 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %quantizedNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %quantizedNode, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %1 = load i16, i16* %arrayidx, align 4, !tbaa !42
  %m_quantizedAabbMin2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin2, i32 0, i32 0
  store i16 %1, i16* %arrayidx3, align 4, !tbaa !42
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin4 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin4, i32 0, i32 1
  %3 = load i16, i16* %arrayidx5, align 2, !tbaa !42
  %m_quantizedAabbMin6 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin6, i32 0, i32 1
  store i16 %3, i16* %arrayidx7, align 2, !tbaa !42
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin8, i32 0, i32 2
  %5 = load i16, i16* %arrayidx9, align 4, !tbaa !42
  %m_quantizedAabbMin10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 2
  store i16 %5, i16* %arrayidx11, align 4, !tbaa !42
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %6, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %7 = load i16, i16* %arrayidx12, align 2, !tbaa !42
  %m_quantizedAabbMax13 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax13, i32 0, i32 0
  store i16 %7, i16* %arrayidx14, align 2, !tbaa !42
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax15 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax15, i32 0, i32 1
  %9 = load i16, i16* %arrayidx16, align 2, !tbaa !42
  %m_quantizedAabbMax17 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax17, i32 0, i32 1
  store i16 %9, i16* %arrayidx18, align 2, !tbaa !42
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax19 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %10, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax19, i32 0, i32 2
  %11 = load i16, i16* %arrayidx20, align 2, !tbaa !42
  %m_quantizedAabbMax21 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax21, i32 0, i32 2
  store i16 %11, i16* %arrayidx22, align 2, !tbaa !42
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %1 = load i32, i32* %n.addr, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !44
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !44
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

define hidden void @_ZN14btOptimizedBvh5refitEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %0, i32 0, i32 6
  %1 = load i8, i8* %m_useQuantization, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, float 1.000000e+00)
  %5 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %6 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %6, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !25
  call void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this1, %class.btStridingMeshInterface* %5, i32 0, i32 %7, i32 0)
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store i32 0, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %10 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %9, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %12, i32 0, i32 13
  %13 = load i32, i32* %i, align 4, !tbaa !22
  %call3 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders2, i32 %13)
  store %class.btBvhSubtreeInfo* %call3, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %15 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %15, i32 0, i32 11
  %16 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %16, i32 0, i32 2
  %17 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !26
  %call4 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %17)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %14, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call4)
  %18 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  br label %if.end

if.else:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  ret void
}

define hidden void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, i32 %firstNode, i32 %endNode, i32 %index) #0 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %firstNode.addr = alloca i32, align 4
  %endNode.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  %curNodeSubPart = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %triangleVerts = alloca [3 x %class.btVector3], align 16
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %curNode = alloca %struct.btQuantizedBvhNode*, align 4
  %nodeSubPart = alloca i32, align 4
  %nodeTriangleIndex = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %graphicsbase39 = alloca double*, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %i94 = alloca i32, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store i32 %firstNode, i32* %firstNode.addr, align 4, !tbaa !22
  store i32 %endNode, i32* %endNode.addr, align 4, !tbaa !22
  store i32 %index, i32* %index.addr, align 4, !tbaa !22
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast i32* %curNodeSubPart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 -1, i32* %curNodeSubPart, align 4, !tbaa !22
  %1 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i8* null, i8** %vertexbase, align 4, !tbaa !2
  %2 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 0, i32* %numverts, align 4, !tbaa !22
  %3 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 2, i32* %type, align 4, !tbaa !46
  %4 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store i32 0, i32* %stride, align 4, !tbaa !22
  %5 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store i8* null, i8** %indexbase, align 4, !tbaa !2
  %6 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store i32 0, i32* %indexstride, align 4, !tbaa !22
  %7 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store i32 0, i32* %numfaces, align 4, !tbaa !22
  %8 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store i32 2, i32* %indicestype, align 4, !tbaa !46
  %9 = bitcast [3 x %class.btVector3]* %triangleVerts to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %9) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %10 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %11 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %12 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %13)
  store %class.btVector3* %call4, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load i32, i32* %endNode.addr, align 4, !tbaa !22
  %sub = sub nsw i32 %15, 1
  store i32 %sub, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc136, %arrayctor.cont
  %16 = load i32, i32* %i, align 4, !tbaa !22
  %17 = load i32, i32* %firstNode.addr, align 4, !tbaa !22
  %cmp = icmp sge i32 %16, %17
  br i1 %cmp, label %for.body, label %for.end138

for.body:                                         ; preds = %for.cond
  %18 = bitcast %struct.btQuantizedBvhNode** %curNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %19, i32 0, i32 11
  %20 = load i32, i32* %i, align 4, !tbaa !22
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %20)
  store %struct.btQuantizedBvhNode* %call5, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %21 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %call6 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %21)
  br i1 %call6, label %if.then, label %if.else78

if.then:                                          ; preds = %for.body
  %22 = bitcast i32* %nodeSubPart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %call7 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %23)
  store i32 %call7, i32* %nodeSubPart, align 4, !tbaa !22
  %24 = bitcast i32* %nodeTriangleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %call8 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %25)
  store i32 %call8, i32* %nodeTriangleIndex, align 4, !tbaa !22
  %26 = load i32, i32* %nodeSubPart, align 4, !tbaa !22
  %27 = load i32, i32* %curNodeSubPart, align 4, !tbaa !22
  %cmp9 = icmp ne i32 %26, %27
  br i1 %cmp9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.then
  %28 = load i32, i32* %curNodeSubPart, align 4, !tbaa !22
  %cmp11 = icmp sge i32 %28, 0
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.then10
  %29 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %30 = load i32, i32* %curNodeSubPart, align 4, !tbaa !22
  %31 = bitcast %class.btStridingMeshInterface* %29 to void (%class.btStridingMeshInterface*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %31, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable, i64 6
  %32 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn, align 4
  call void %32(%class.btStridingMeshInterface* %29, i32 %30)
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.then10
  %33 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %34 = load i32, i32* %nodeSubPart, align 4, !tbaa !22
  %35 = bitcast %class.btStridingMeshInterface* %33 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable13 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %35, align 4, !tbaa !6
  %vfn14 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable13, i64 4
  %36 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn14, align 4
  call void %36(%class.btStridingMeshInterface* %33, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %34)
  %37 = load i32, i32* %nodeSubPart, align 4, !tbaa !22
  store i32 %37, i32* %curNodeSubPart, align 4, !tbaa !22
  br label %if.end15

if.end15:                                         ; preds = %if.end, %if.then
  %38 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  %39 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %40 = load i32, i32* %nodeTriangleIndex, align 4, !tbaa !22
  %41 = load i32, i32* %indexstride, align 4, !tbaa !22
  %mul = mul nsw i32 %40, %41
  %add.ptr = getelementptr inbounds i8, i8* %39, i32 %mul
  %42 = bitcast i8* %add.ptr to i32*
  store i32* %42, i32** %gfxbase, align 4, !tbaa !2
  %43 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #9
  store i32 2, i32* %j, align 4, !tbaa !22
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc, %if.end15
  %44 = load i32, i32* %j, align 4, !tbaa !22
  %cmp17 = icmp sge i32 %44, 0
  br i1 %cmp17, label %for.body18, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond16
  %45 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  br label %for.end

for.body18:                                       ; preds = %for.cond16
  %46 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %47 = load i32, i32* %indicestype, align 4, !tbaa !46
  %cmp19 = icmp eq i32 %47, 3
  br i1 %cmp19, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body18
  %48 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %49 = bitcast i32* %48 to i16*
  %50 = load i32, i32* %j, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds i16, i16* %49, i32 %50
  %51 = load i16, i16* %arrayidx, align 2, !tbaa !42
  %conv = zext i16 %51 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body18
  %52 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %53 = load i32, i32* %j, align 4, !tbaa !22
  %arrayidx20 = getelementptr inbounds i32, i32* %52, i32 %53
  %54 = load i32, i32* %arrayidx20, align 4, !tbaa !22
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %54, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4, !tbaa !22
  %55 = load i32, i32* %type, align 4, !tbaa !46
  %cmp21 = icmp eq i32 %55, 0
  br i1 %cmp21, label %if.then22, label %if.else

if.then22:                                        ; preds = %cond.end
  %56 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  %57 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %58 = load i32, i32* %graphicsindex, align 4, !tbaa !22
  %59 = load i32, i32* %stride, align 4, !tbaa !22
  %mul23 = mul nsw i32 %58, %59
  %add.ptr24 = getelementptr inbounds i8, i8* %57, i32 %mul23
  %60 = bitcast i8* %add.ptr24 to float*
  store float* %60, float** %graphicsbase, align 4, !tbaa !2
  %61 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #9
  %62 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  %63 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds float, float* %63, i32 0
  %64 = load float, float* %arrayidx26, align 4, !tbaa !23
  %65 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %65)
  %66 = load float, float* %call27, align 4, !tbaa !23
  %mul28 = fmul float %64, %66
  store float %mul28, float* %ref.tmp25, align 4, !tbaa !23
  %67 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds float, float* %68, i32 1
  %69 = load float, float* %arrayidx30, align 4, !tbaa !23
  %70 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %70)
  %71 = load float, float* %call31, align 4, !tbaa !23
  %mul32 = fmul float %69, %71
  store float %mul32, float* %ref.tmp29, align 4, !tbaa !23
  %72 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #9
  %73 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds float, float* %73, i32 2
  %74 = load float, float* %arrayidx34, align 4, !tbaa !23
  %75 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %75)
  %76 = load float, float* %call35, align 4, !tbaa !23
  %mul36 = fmul float %74, %76
  store float %mul36, float* %ref.tmp33, align 4, !tbaa !23
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %77 = load i32, i32* %j, align 4, !tbaa !22
  %arrayidx38 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %77
  %78 = bitcast %class.btVector3* %arrayidx38 to i8*
  %79 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %78, i8* align 4 %79, i32 16, i1 false), !tbaa.struct !48
  %80 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #9
  %81 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #9
  %82 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #9
  %83 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #9
  %84 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #9
  br label %if.end63

if.else:                                          ; preds = %cond.end
  %85 = bitcast double** %graphicsbase39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #9
  %86 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %87 = load i32, i32* %graphicsindex, align 4, !tbaa !22
  %88 = load i32, i32* %stride, align 4, !tbaa !22
  %mul40 = mul nsw i32 %87, %88
  %add.ptr41 = getelementptr inbounds i8, i8* %86, i32 %mul40
  %89 = bitcast i8* %add.ptr41 to double*
  store double* %89, double** %graphicsbase39, align 4, !tbaa !2
  %90 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #9
  %91 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #9
  %92 = load double*, double** %graphicsbase39, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds double, double* %92, i32 0
  %93 = load double, double* %arrayidx44, align 8, !tbaa !49
  %94 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %94)
  %95 = load float, float* %call45, align 4, !tbaa !23
  %conv46 = fpext float %95 to double
  %mul47 = fmul double %93, %conv46
  %conv48 = fptrunc double %mul47 to float
  store float %conv48, float* %ref.tmp43, align 4, !tbaa !23
  %96 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #9
  %97 = load double*, double** %graphicsbase39, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds double, double* %97, i32 1
  %98 = load double, double* %arrayidx50, align 8, !tbaa !49
  %99 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call51 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %99)
  %100 = load float, float* %call51, align 4, !tbaa !23
  %conv52 = fpext float %100 to double
  %mul53 = fmul double %98, %conv52
  %conv54 = fptrunc double %mul53 to float
  store float %conv54, float* %ref.tmp49, align 4, !tbaa !23
  %101 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #9
  %102 = load double*, double** %graphicsbase39, align 4, !tbaa !2
  %arrayidx56 = getelementptr inbounds double, double* %102, i32 2
  %103 = load double, double* %arrayidx56, align 8, !tbaa !49
  %104 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %104)
  %105 = load float, float* %call57, align 4, !tbaa !23
  %conv58 = fpext float %105 to double
  %mul59 = fmul double %103, %conv58
  %conv60 = fptrunc double %mul59 to float
  store float %conv60, float* %ref.tmp55, align 4, !tbaa !23
  %call61 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp55)
  %106 = load i32, i32* %j, align 4, !tbaa !22
  %arrayidx62 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %106
  %107 = bitcast %class.btVector3* %arrayidx62 to i8*
  %108 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %107, i8* align 4 %108, i32 16, i1 false), !tbaa.struct !48
  %109 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %112) #9
  %113 = bitcast double** %graphicsbase39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  br label %if.end63

if.end63:                                         ; preds = %if.else, %if.then22
  %114 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end63
  %115 = load i32, i32* %j, align 4, !tbaa !22
  %dec = add nsw i32 %115, -1
  store i32 %dec, i32* %j, align 4, !tbaa !22
  br label %for.cond16

for.end:                                          ; preds = %for.cond.cleanup
  %116 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #9
  store float 0x43ABC16D60000000, float* %ref.tmp64, align 4, !tbaa !23
  %117 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #9
  store float 0x43ABC16D60000000, float* %ref.tmp65, align 4, !tbaa !23
  %118 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #9
  store float 0x43ABC16D60000000, float* %ref.tmp66, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66)
  %119 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #9
  %120 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #9
  %121 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #9
  %122 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp67, align 4, !tbaa !23
  %123 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp68, align 4, !tbaa !23
  %124 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp69, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %125 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #9
  %126 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #9
  %127 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #9
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx70)
  %arrayidx71 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71)
  %arrayidx72 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72)
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73)
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx74)
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx75)
  %128 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %129 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %129, i32 0, i32 0
  %arrayidx76 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %128, i16* %arrayidx76, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, i32 0)
  %130 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %131 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %131, i32 0, i32 1
  %arrayidx77 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %130, i16* %arrayidx77, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 1)
  %132 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #9
  %133 = bitcast i32* %nodeTriangleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #9
  %134 = bitcast i32* %nodeSubPart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #9
  br label %if.end135

if.else78:                                        ; preds = %for.body
  %135 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #9
  %136 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes79 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %136, i32 0, i32 11
  %137 = load i32, i32* %i, align 4, !tbaa !22
  %add = add nsw i32 %137, 1
  %call80 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes79, i32 %add)
  store %struct.btQuantizedBvhNode* %call80, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %138 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #9
  %139 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call81 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %139)
  br i1 %call81, label %cond.true82, label %cond.false86

cond.true82:                                      ; preds = %if.else78
  %140 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes83 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %140, i32 0, i32 11
  %141 = load i32, i32* %i, align 4, !tbaa !22
  %add84 = add nsw i32 %141, 2
  %call85 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes83, i32 %add84)
  br label %cond.end92

cond.false86:                                     ; preds = %if.else78
  %142 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes87 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %142, i32 0, i32 11
  %143 = load i32, i32* %i, align 4, !tbaa !22
  %add88 = add nsw i32 %143, 1
  %144 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call89 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %144)
  %add90 = add nsw i32 %add88, %call89
  %call91 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes87, i32 %add90)
  br label %cond.end92

cond.end92:                                       ; preds = %cond.false86, %cond.true82
  %cond93 = phi %struct.btQuantizedBvhNode* [ %call85, %cond.true82 ], [ %call91, %cond.false86 ]
  store %struct.btQuantizedBvhNode* %cond93, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %145 = bitcast i32* %i94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #9
  store i32 0, i32* %i94, align 4, !tbaa !22
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc133, %cond.end92
  %146 = load i32, i32* %i94, align 4, !tbaa !22
  %cmp96 = icmp slt i32 %146, 3
  br i1 %cmp96, label %for.body98, label %for.cond.cleanup97

for.cond.cleanup97:                               ; preds = %for.cond95
  %147 = bitcast i32* %i94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #9
  br label %for.end134

for.body98:                                       ; preds = %for.cond95
  %148 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %m_quantizedAabbMin99 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %148, i32 0, i32 0
  %149 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx100 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin99, i32 0, i32 %149
  %150 = load i16, i16* %arrayidx100, align 2, !tbaa !42
  %151 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMin101 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %151, i32 0, i32 0
  %152 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx102 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin101, i32 0, i32 %152
  store i16 %150, i16* %arrayidx102, align 2, !tbaa !42
  %153 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMin103 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %153, i32 0, i32 0
  %154 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx104 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin103, i32 0, i32 %154
  %155 = load i16, i16* %arrayidx104, align 2, !tbaa !42
  %conv105 = zext i16 %155 to i32
  %156 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %m_quantizedAabbMin106 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %156, i32 0, i32 0
  %157 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx107 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin106, i32 0, i32 %157
  %158 = load i16, i16* %arrayidx107, align 2, !tbaa !42
  %conv108 = zext i16 %158 to i32
  %cmp109 = icmp sgt i32 %conv105, %conv108
  br i1 %cmp109, label %if.then110, label %if.end115

if.then110:                                       ; preds = %for.body98
  %159 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %m_quantizedAabbMin111 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %159, i32 0, i32 0
  %160 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx112 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin111, i32 0, i32 %160
  %161 = load i16, i16* %arrayidx112, align 2, !tbaa !42
  %162 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMin113 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %162, i32 0, i32 0
  %163 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx114 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin113, i32 0, i32 %163
  store i16 %161, i16* %arrayidx114, align 2, !tbaa !42
  br label %if.end115

if.end115:                                        ; preds = %if.then110, %for.body98
  %164 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %m_quantizedAabbMax116 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %164, i32 0, i32 1
  %165 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx117 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax116, i32 0, i32 %165
  %166 = load i16, i16* %arrayidx117, align 2, !tbaa !42
  %167 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMax118 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %167, i32 0, i32 1
  %168 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx119 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax118, i32 0, i32 %168
  store i16 %166, i16* %arrayidx119, align 2, !tbaa !42
  %169 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMax120 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %169, i32 0, i32 1
  %170 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx121 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax120, i32 0, i32 %170
  %171 = load i16, i16* %arrayidx121, align 2, !tbaa !42
  %conv122 = zext i16 %171 to i32
  %172 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %m_quantizedAabbMax123 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %172, i32 0, i32 1
  %173 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx124 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax123, i32 0, i32 %173
  %174 = load i16, i16* %arrayidx124, align 2, !tbaa !42
  %conv125 = zext i16 %174 to i32
  %cmp126 = icmp slt i32 %conv122, %conv125
  br i1 %cmp126, label %if.then127, label %if.end132

if.then127:                                       ; preds = %if.end115
  %175 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %m_quantizedAabbMax128 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %175, i32 0, i32 1
  %176 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx129 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax128, i32 0, i32 %176
  %177 = load i16, i16* %arrayidx129, align 2, !tbaa !42
  %178 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %curNode, align 4, !tbaa !2
  %m_quantizedAabbMax130 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %178, i32 0, i32 1
  %179 = load i32, i32* %i94, align 4, !tbaa !22
  %arrayidx131 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax130, i32 0, i32 %179
  store i16 %177, i16* %arrayidx131, align 2, !tbaa !42
  br label %if.end132

if.end132:                                        ; preds = %if.then127, %if.end115
  br label %for.inc133

for.inc133:                                       ; preds = %if.end132
  %180 = load i32, i32* %i94, align 4, !tbaa !22
  %inc = add nsw i32 %180, 1
  store i32 %inc, i32* %i94, align 4, !tbaa !22
  br label %for.cond95

for.end134:                                       ; preds = %for.cond.cleanup97
  %181 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #9
  %182 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #9
  br label %if.end135

if.end135:                                        ; preds = %for.end134, %for.end
  %183 = bitcast %struct.btQuantizedBvhNode** %curNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #9
  br label %for.inc136

for.inc136:                                       ; preds = %if.end135
  %184 = load i32, i32* %i, align 4, !tbaa !22
  %dec137 = add nsw i32 %184, -1
  store i32 %dec137, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end138:                                       ; preds = %for.cond
  %185 = load i32, i32* %curNodeSubPart, align 4, !tbaa !22
  %cmp139 = icmp sge i32 %185, 0
  br i1 %cmp139, label %if.then140, label %if.end143

if.then140:                                       ; preds = %for.end138
  %186 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %187 = load i32, i32* %curNodeSubPart, align 4, !tbaa !22
  %188 = bitcast %class.btStridingMeshInterface* %186 to void (%class.btStridingMeshInterface*, i32)***
  %vtable141 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %188, align 4, !tbaa !6
  %vfn142 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable141, i64 6
  %189 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn142, align 4
  call void %189(%class.btStridingMeshInterface* %186, i32 %187)
  br label %if.end143

if.end143:                                        ; preds = %if.then140, %for.end138
  %190 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #9
  %191 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #9
  %192 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %192) #9
  %193 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %193) #9
  %194 = bitcast [3 x %class.btVector3]* %triangleVerts to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %194) #9
  %195 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #9
  %196 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #9
  %197 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #9
  %198 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #9
  %199 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #9
  %200 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #9
  %201 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #9
  %202 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #9
  %203 = bitcast i32* %curNodeSubPart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %1 = load i32, i32* %n.addr, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

define hidden void @_ZN14btOptimizedBvh12refitPartialEP23btStridingMeshInterfaceRK9btVector3S4_(%class.btOptimizedBvh* %this, %class.btStridingMeshInterface* %meshInterface, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %overlap = alloca i32, align 4
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %0) #9
  %1 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %1) #9
  %2 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %2, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 0)
  %4 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %4, i16* %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, i32 1)
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store i32 0, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4, !tbaa !22
  %8 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %7, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_SubtreeHeaders3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 13
  %11 = load i32, i32* %i, align 4, !tbaa !22
  %call4 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders3, i32 %11)
  store %class.btBvhSubtreeInfo* %call4, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %12 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %13 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %13, i32 0, i32 0
  %arraydecay6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 0, i32 1
  %arraydecay7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call8 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %arraydecay, i16* %arraydecay5, i16* %arraydecay6, i16* %arraydecay7)
  store i32 %call8, i32* %overlap, align 4, !tbaa !22
  %15 = load i32, i32* %overlap, align 4, !tbaa !22
  %cmp9 = icmp ne i32 %15, 0
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %16 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4, !tbaa !2
  %17 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %17, i32 0, i32 2
  %18 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !26
  %19 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %19, i32 0, i32 2
  %20 = load i32, i32* %m_rootNodeIndex10, align 4, !tbaa !26
  %21 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %21, i32 0, i32 3
  %22 = load i32, i32* %m_subtreeSize, align 4, !tbaa !28
  %add = add nsw i32 %20, %22
  %23 = load i32, i32* %i, align 4, !tbaa !22
  call void @_ZN14btOptimizedBvh14updateBvhNodesEP23btStridingMeshInterfaceiii(%class.btOptimizedBvh* %this1, %class.btStridingMeshInterface* %16, i32 %18, i32 %add, i32 %23)
  %24 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %25 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %25, i32 0, i32 11
  %26 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex11 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %26, i32 0, i32 2
  %27 = load i32, i32* %m_rootNodeIndex11, align 4, !tbaa !26
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %27)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %24, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %28 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %30 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %32) #9
  %33 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %33) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #6 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i16* %out, i16** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !22
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #9
  %4 = load i32, i32* %isMax.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %5 = load float, float* %call, align 4, !tbaa !23
  %add = fadd float %5, 1.000000e+00
  %conv = fptoui float %add to i16
  %conv2 = zext i16 %conv to i32
  %or = or i32 %conv2, 1
  %conv3 = trunc i32 %or to i16
  %6 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 0
  store i16 %conv3, i16* %arrayidx, align 2, !tbaa !42
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %7 = load float, float* %call4, align 4, !tbaa !23
  %add5 = fadd float %7, 1.000000e+00
  %conv6 = fptoui float %add5 to i16
  %conv7 = zext i16 %conv6 to i32
  %or8 = or i32 %conv7, 1
  %conv9 = trunc i32 %or8 to i16
  %8 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 1
  store i16 %conv9, i16* %arrayidx10, align 2, !tbaa !42
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %9 = load float, float* %call11, align 4, !tbaa !23
  %add12 = fadd float %9, 1.000000e+00
  %conv13 = fptoui float %add12 to i16
  %conv14 = zext i16 %conv13 to i32
  %or15 = or i32 %conv14, 1
  %conv16 = trunc i32 %or15 to i16
  %10 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %10, i32 2
  store i16 %conv16, i16* %arrayidx17, align 2, !tbaa !42
  br label %if.end

if.else:                                          ; preds = %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %11 = load float, float* %call18, align 4, !tbaa !23
  %conv19 = fptoui float %11 to i16
  %conv20 = zext i16 %conv19 to i32
  %and = and i32 %conv20, 65534
  %conv21 = trunc i32 %and to i16
  %12 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i16, i16* %12, i32 0
  store i16 %conv21, i16* %arrayidx22, align 2, !tbaa !42
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %13 = load float, float* %call23, align 4, !tbaa !23
  %conv24 = fptoui float %13 to i16
  %conv25 = zext i16 %conv24 to i32
  %and26 = and i32 %conv25, 65534
  %conv27 = trunc i32 %and26 to i16
  %14 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %14, i32 1
  store i16 %conv27, i16* %arrayidx28, align 2, !tbaa !42
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %15 = load float, float* %call29, align 4, !tbaa !23
  %conv30 = fptoui float %15 to i16
  %conv31 = zext i16 %conv30 to i32
  %and32 = and i32 %conv31, 65534
  %conv33 = trunc i32 %and32 to i16
  %16 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i16, i16* %16, i32 2
  store i16 %conv33, i16* %arrayidx34, align 2, !tbaa !42
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %aabbMin1, i16* %aabbMax1, i16* %aabbMin2, i16* %aabbMax2) #6 comdat {
entry:
  %aabbMin1.addr = alloca i16*, align 4
  %aabbMax1.addr = alloca i16*, align 4
  %aabbMin2.addr = alloca i16*, align 4
  %aabbMax2.addr = alloca i16*, align 4
  store i16* %aabbMin1, i16** %aabbMin1.addr, align 4, !tbaa !2
  store i16* %aabbMax1, i16** %aabbMax1.addr, align 4, !tbaa !2
  store i16* %aabbMin2, i16** %aabbMin2.addr, align 4, !tbaa !2
  store i16* %aabbMax2, i16** %aabbMax2.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2, !tbaa !42
  %conv = zext i16 %1 to i32
  %2 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %2, i32 0
  %3 = load i16, i16* %arrayidx1, align 2, !tbaa !42
  %conv2 = zext i16 %3 to i32
  %cmp = icmp sle i32 %conv, %conv2
  %conv3 = zext i1 %cmp to i32
  %4 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx4, align 2, !tbaa !42
  %conv5 = zext i16 %5 to i32
  %6 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %6, i32 0
  %7 = load i16, i16* %arrayidx6, align 2, !tbaa !42
  %conv7 = zext i16 %7 to i32
  %cmp8 = icmp sge i32 %conv5, %conv7
  %conv9 = zext i1 %cmp8 to i32
  %and = and i32 %conv3, %conv9
  %8 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 2
  %9 = load i16, i16* %arrayidx10, align 2, !tbaa !42
  %conv11 = zext i16 %9 to i32
  %10 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %10, i32 2
  %11 = load i16, i16* %arrayidx12, align 2, !tbaa !42
  %conv13 = zext i16 %11 to i32
  %cmp14 = icmp sle i32 %conv11, %conv13
  %conv15 = zext i1 %cmp14 to i32
  %and16 = and i32 %and, %conv15
  %12 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %12, i32 2
  %13 = load i16, i16* %arrayidx17, align 2, !tbaa !42
  %conv18 = zext i16 %13 to i32
  %14 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i16, i16* %14, i32 2
  %15 = load i16, i16* %arrayidx19, align 2, !tbaa !42
  %conv20 = zext i16 %15 to i32
  %cmp21 = icmp sge i32 %conv18, %conv20
  %conv22 = zext i1 %cmp21 to i32
  %and23 = and i32 %and16, %conv22
  %16 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i16, i16* %16, i32 1
  %17 = load i16, i16* %arrayidx24, align 2, !tbaa !42
  %conv25 = zext i16 %17 to i32
  %18 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i16, i16* %18, i32 1
  %19 = load i16, i16* %arrayidx26, align 2, !tbaa !42
  %conv27 = zext i16 %19 to i32
  %cmp28 = icmp sle i32 %conv25, %conv27
  %conv29 = zext i1 %cmp28 to i32
  %and30 = and i32 %and23, %conv29
  %20 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i16, i16* %20, i32 1
  %21 = load i16, i16* %arrayidx31, align 2, !tbaa !42
  %conv32 = zext i16 %21 to i32
  %22 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %22, i32 1
  %23 = load i16, i16* %arrayidx33, align 2, !tbaa !42
  %conv34 = zext i16 %23 to i32
  %cmp35 = icmp sge i32 %conv32, %conv34
  %conv36 = zext i1 %cmp35 to i32
  %and37 = and i32 %and30, %conv36
  %call = call i32 @_Z8btSelectjii(i32 %and37, i32 1, i32 0)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !44
  %shr = ashr i32 %0, 21
  ret i32 %shr
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %x, align 4, !tbaa !22
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %x, align 4, !tbaa !22
  %and = and i32 %2, 0
  %neg = xor i32 %and, -1
  %shl = shl i32 %neg, 21
  store i32 %shl, i32* %y, align 4, !tbaa !22
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !44
  %4 = load i32, i32* %y, align 4, !tbaa !22
  %neg2 = xor i32 %4, -1
  %and3 = and i32 %3, %neg2
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  ret i32 %and3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

define hidden %class.btOptimizedBvh* @_ZN14btOptimizedBvh18deSerializeInPlaceEPvjb(i8* %i_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) #0 {
entry:
  %i_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %bvh = alloca %class.btQuantizedBvh*, align 4
  store i8* %i_alignedDataBuffer, i8** %i_alignedDataBuffer.addr, align 4, !tbaa !2
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4, !tbaa !22
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1, !tbaa !8
  %0 = bitcast %class.btQuantizedBvh** %bvh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %i_alignedDataBuffer.addr, align 4, !tbaa !2
  %2 = load i32, i32* %i_dataBufferSize.addr, align 4, !tbaa !22
  %3 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %3 to i1
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8* %1, i32 %2, i1 zeroext %tobool)
  store %class.btQuantizedBvh* %call, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %4 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %5 = bitcast %class.btQuantizedBvh* %4 to %class.btOptimizedBvh*
  %6 = bitcast %class.btQuantizedBvh** %bvh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  ret %class.btOptimizedBvh* %5
}

declare %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8*, i32, i1 zeroext) #1

declare zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh*, i8*, i32, i1 zeroext) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv(%class.btQuantizedBvh* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  ret i32 84
}

declare i8* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer(%class.btQuantizedBvh*, i8*, %class.btSerializer*) unnamed_addr #1

declare void @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData(%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData* nonnull align 4 dereferenceable(84)) unnamed_addr #1

declare void @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData(%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData* nonnull align 8 dereferenceable(136)) unnamed_addr #1

define linkonce_odr hidden zeroext i1 @_ZNK14btOptimizedBvh16serializeInPlaceEPvjb(%class.btOptimizedBvh* %this, i8* %o_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btOptimizedBvh*, align 4
  %o_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  store %class.btOptimizedBvh* %this, %class.btOptimizedBvh** %this.addr, align 4, !tbaa !2
  store i8* %o_alignedDataBuffer, i8** %o_alignedDataBuffer.addr, align 4, !tbaa !2
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4, !tbaa !22
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1, !tbaa !8
  %this1 = load %class.btOptimizedBvh*, %class.btOptimizedBvh** %this.addr, align 4
  %0 = bitcast %class.btOptimizedBvh* %this1 to %class.btQuantizedBvh*
  %1 = load i8*, i8** %o_alignedDataBuffer.addr, align 4, !tbaa !2
  %2 = load i32, i32* %i_dataBufferSize.addr, align 4, !tbaa !22
  %3 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %3 to i1
  %call = call zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh* %0, i8* %1, i32 %2, i1 zeroext %tobool)
  ret i1 %call
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %0 = bitcast %class.btInternalTriangleIndexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV31btInternalTriangleIndexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallbackD0Ev(%struct.QuantizedNodeTriangleCallback* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %call = call %struct.QuantizedNodeTriangleCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.QuantizedNodeTriangleCallback* (%struct.QuantizedNodeTriangleCallback*)*)(%struct.QuantizedNodeTriangleCallback* %this1) #9
  %0 = bitcast %struct.QuantizedNodeTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN29QuantizedNodeTriangleCallback28internalProcessTriangleIndexEPS2_ii(%struct.QuantizedNodeTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.QuantizedNodeTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %node = alloca %struct.btQuantizedBvhNode, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %MIN_AABB_DIMENSION = alloca float, align 4
  %MIN_AABB_HALF_DIMENSION = alloca float, align 4
  store %struct.QuantizedNodeTriangleCallback* %this, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !22
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !22
  %this1 = load %struct.QuantizedNodeTriangleCallback*, %struct.QuantizedNodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.btQuantizedBvhNode* %node to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %2 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !23
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %16 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %17 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %18 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  %19 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %20 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %20, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  %21 = bitcast float* %MIN_AABB_DIMENSION to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 0x3F60624DE0000000, float* %MIN_AABB_DIMENSION, align 4, !tbaa !23
  %22 = bitcast float* %MIN_AABB_HALF_DIMENSION to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0x3F50624DE0000000, float* %MIN_AABB_HALF_DIMENSION, align 4, !tbaa !23
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMax)
  %23 = load float, float* %call13, align 4, !tbaa !23
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMin)
  %24 = load float, float* %call14, align 4, !tbaa !23
  %sub = fsub float %23, %24
  %cmp = fcmp olt float %sub, 0x3F60624DE0000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMax)
  %25 = load float, float* %call15, align 4, !tbaa !23
  %add = fadd float %25, 0x3F50624DE0000000
  call void @_ZN9btVector34setXEf(%class.btVector3* %aabbMax, float %add)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %aabbMin)
  %26 = load float, float* %call16, align 4, !tbaa !23
  %sub17 = fsub float %26, 0x3F50624DE0000000
  call void @_ZN9btVector34setXEf(%class.btVector3* %aabbMin, float %sub17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMax)
  %27 = load float, float* %call18, align 4, !tbaa !23
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMin)
  %28 = load float, float* %call19, align 4, !tbaa !23
  %sub20 = fsub float %27, %28
  %cmp21 = fcmp olt float %sub20, 0x3F60624DE0000000
  br i1 %cmp21, label %if.then22, label %if.end27

if.then22:                                        ; preds = %if.end
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMax)
  %29 = load float, float* %call23, align 4, !tbaa !23
  %add24 = fadd float %29, 0x3F50624DE0000000
  call void @_ZN9btVector34setYEf(%class.btVector3* %aabbMax, float %add24)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %aabbMin)
  %30 = load float, float* %call25, align 4, !tbaa !23
  %sub26 = fsub float %30, 0x3F50624DE0000000
  call void @_ZN9btVector34setYEf(%class.btVector3* %aabbMin, float %sub26)
  br label %if.end27

if.end27:                                         ; preds = %if.then22, %if.end
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMax)
  %31 = load float, float* %call28, align 4, !tbaa !23
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMin)
  %32 = load float, float* %call29, align 4, !tbaa !23
  %sub30 = fsub float %31, %32
  %cmp31 = fcmp olt float %sub30, 0x3F60624DE0000000
  br i1 %cmp31, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end27
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMax)
  %33 = load float, float* %call33, align 4, !tbaa !23
  %add34 = fadd float %33, 0x3F50624DE0000000
  call void @_ZN9btVector34setZEf(%class.btVector3* %aabbMax, float %add34)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %aabbMin)
  %34 = load float, float* %call35, align 4, !tbaa !23
  %sub36 = fsub float %34, 0x3F50624DE0000000
  call void @_ZN9btVector34setZEf(%class.btVector3* %aabbMin, float %sub36)
  br label %if.end37

if.end37:                                         ; preds = %if.then32, %if.end27
  %m_optimizedTree = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %35 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedTree, align 4, !tbaa !30
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 0
  %arrayidx38 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %35, i16* %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, i32 0)
  %m_optimizedTree39 = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 2
  %36 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %m_optimizedTree39, align 4, !tbaa !30
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %36, i16* %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 1)
  %37 = load i32, i32* %partId.addr, align 4, !tbaa !22
  %shl = shl i32 %37, 21
  %38 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !22
  %or = or i32 %shl, %38
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %node, i32 0, i32 2
  store i32 %or, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !44
  %m_triangleNodes = getelementptr inbounds %struct.QuantizedNodeTriangleCallback, %struct.QuantizedNodeTriangleCallback* %this1, i32 0, i32 1
  %39 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %m_triangleNodes, align 4, !tbaa !51
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.0* %39, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %node)
  %40 = bitcast float* %MIN_AABB_HALF_DIMENSION to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #9
  %41 = bitcast float* %MIN_AABB_DIMENSION to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #9
  %43 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  %44 = bitcast %struct.btQuantizedBvhNode* %node to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_x, float* %_x.addr, align 4, !tbaa !23
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !23
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_y, float* %_y.addr, align 4, !tbaa !23
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4, !tbaa !23
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_z, float* %_z.addr, align 4, !tbaa !23
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4, !tbaa !23
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %_Val, %struct.btQuantizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !22
  %1 = load i32, i32* %sz, align 4, !tbaa !22
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 %3
  %4 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call5 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btQuantizedBvhNode*
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btQuantizedBvhNode* %5 to i8*
  %8 = bitcast %struct.btQuantizedBvhNode* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !34
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !32
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !32
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !52
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btQuantizedBvhNode*
  store %struct.btQuantizedBvhNode* %3, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btQuantizedBvhNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !53
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %5, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !52
  %7 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !54
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %call = call %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btQuantizedBvhNode** null)
  %2 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btQuantizedBvhNode* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !22
  store i32 %end, i32* %end.addr, align 4, !tbaa !22
  store %struct.btQuantizedBvhNode* %dest, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %end.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  %6 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx2 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 %9
  %10 = bitcast %struct.btQuantizedBvhNode* %7 to i8*
  %11 = bitcast %struct.btQuantizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !34
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !22
  store i32 %last, i32* %last.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %last.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %tobool = icmp ne %struct.btQuantizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !53, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data4, align 4, !tbaa !33
  call void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btQuantizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data5, align 4, !tbaa !33
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btQuantizedBvhNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btQuantizedBvhNode**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !22
  store %struct.btQuantizedBvhNode** %hint, %struct.btQuantizedBvhNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !22
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  ret %struct.btQuantizedBvhNode* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btQuantizedBvhNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %ptr, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btQuantizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallbackD0Ev(%struct.NodeTriangleCallback* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %call = call %struct.NodeTriangleCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.NodeTriangleCallback* (%struct.NodeTriangleCallback*)*)(%struct.NodeTriangleCallback* %this1) #9
  %0 = bitcast %struct.NodeTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_EN20NodeTriangleCallback28internalProcessTriangleIndexEPS2_ii(%struct.NodeTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.NodeTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %node = alloca %struct.btOptimizedBvhNode, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %struct.NodeTriangleCallback* %this, %struct.NodeTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !22
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !22
  %this1 = load %struct.NodeTriangleCallback*, %struct.NodeTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.btOptimizedBvhNode* %node to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %node)
  %1 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %2 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4, !tbaa !23
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %12 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %16 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %17 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  %18 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx11 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %19 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  %20 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %20, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 0
  %21 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  %22 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !48
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 1
  %23 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  %24 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !48
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 2
  store i32 -1, i32* %m_escapeIndex, align 4, !tbaa !56
  %25 = load i32, i32* %partId.addr, align 4, !tbaa !22
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 3
  store i32 %25, i32* %m_subPart, align 4, !tbaa !58
  %26 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !22
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %node, i32 0, i32 4
  store i32 %26, i32* %m_triangleIndex, align 4, !tbaa !59
  %m_triangleNodes = getelementptr inbounds %struct.NodeTriangleCallback, %struct.NodeTriangleCallback* %this1, i32 0, i32 1
  %27 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %m_triangleNodes, align 4, !tbaa !60
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray* %27, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %node)
  %28 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  %29 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  %30 = bitcast %struct.btOptimizedBvhNode* %node to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %30) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btOptimizedBvhNode* %_Val, %struct.btOptimizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !22
  %1 = load i32, i32* %sz, align 4, !tbaa !22
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !36
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %2, i32 %3
  %4 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call5 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %4)
  %5 = bitcast i8* %call5 to %struct.btOptimizedBvhNode*
  %6 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btOptimizedBvhNode* %5 to i8*
  %8 = bitcast %struct.btOptimizedBvhNode* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 64, i1 false), !tbaa.struct !38
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size6, align 4, !tbaa !36
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !36
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !62
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btOptimizedBvhNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btOptimizedBvhNode*
  store %struct.btOptimizedBvhNode* %3, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btOptimizedBvhNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !63
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %5, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !62
  %7 = bitcast %struct.btOptimizedBvhNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !54
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %call = call %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btOptimizedBvhNode** null)
  %2 = bitcast %struct.btOptimizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btOptimizedBvhNode* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !22
  store i32 %end, i32* %end.addr, align 4, !tbaa !22
  store %struct.btOptimizedBvhNode* %dest, %struct.btOptimizedBvhNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %end.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %4, i32 %5
  %6 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %6)
  %7 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx2 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %8, i32 %9
  %10 = bitcast %struct.btOptimizedBvhNode* %7 to i8*
  %11 = bitcast %struct.btOptimizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 64, i1 false), !tbaa.struct !38
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !22
  store i32 %last, i32* %last.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %last.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %tobool = icmp ne %struct.btOptimizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !63, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data4, align 4, !tbaa !37
  call void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btOptimizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data5, align 4, !tbaa !37
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btOptimizedBvhNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btOptimizedBvhNode**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !22
  store %struct.btOptimizedBvhNode** %hint, %struct.btOptimizedBvhNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !22
  %mul = mul i32 64, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  ret %struct.btOptimizedBvhNode* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btOptimizedBvhNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btOptimizedBvhNode* %ptr, %struct.btOptimizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btOptimizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z8btSelectjii(i32 %condition, i32 %valueIfConditionNonZero, i32 %valueIfConditionZero) #4 comdat {
entry:
  %condition.addr = alloca i32, align 4
  %valueIfConditionNonZero.addr = alloca i32, align 4
  %valueIfConditionZero.addr = alloca i32, align 4
  %testNz = alloca i32, align 4
  %testEqz = alloca i32, align 4
  store i32 %condition, i32* %condition.addr, align 4, !tbaa !22
  store i32 %valueIfConditionNonZero, i32* %valueIfConditionNonZero.addr, align 4, !tbaa !22
  store i32 %valueIfConditionZero, i32* %valueIfConditionZero.addr, align 4, !tbaa !22
  %0 = bitcast i32* %testNz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %condition.addr, align 4, !tbaa !22
  %2 = load i32, i32* %condition.addr, align 4, !tbaa !22
  %sub = sub nsw i32 0, %2
  %or = or i32 %1, %sub
  %shr = ashr i32 %or, 31
  store i32 %shr, i32* %testNz, align 4, !tbaa !22
  %3 = bitcast i32* %testEqz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %testNz, align 4, !tbaa !22
  %neg = xor i32 %4, -1
  store i32 %neg, i32* %testEqz, align 4, !tbaa !22
  %5 = load i32, i32* %valueIfConditionNonZero.addr, align 4, !tbaa !22
  %6 = load i32, i32* %testNz, align 4, !tbaa !22
  %and = and i32 %5, %6
  %7 = load i32, i32* %valueIfConditionZero.addr, align 4, !tbaa !22
  %8 = load i32, i32* %testEqz, align 4, !tbaa !22
  %and1 = and i32 %7, %8
  %or2 = or i32 %and, %and1
  %9 = bitcast i32* %testEqz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast i32* %testNz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret i32 %or2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !64
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btBvhSubtreeInfo** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btBvhSubtreeInfo*
  store %class.btBvhSubtreeInfo* %3, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btBvhSubtreeInfo* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !65
  %5 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %5, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !22
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !64
  %7 = bitcast %class.btBvhSubtreeInfo** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !54
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !22
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !22
  %call = call %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btBvhSubtreeInfo** null)
  %2 = bitcast %class.btBvhSubtreeInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btBvhSubtreeInfo* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !22
  store i32 %end, i32* %end.addr, align 4, !tbaa !22
  store %class.btBvhSubtreeInfo* %dest, %class.btBvhSubtreeInfo** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %end.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %4, i32 %5
  %6 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %6)
  %7 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %9 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %8, i32 %9
  %10 = bitcast %class.btBvhSubtreeInfo* %7 to i8*
  %11 = bitcast %class.btBvhSubtreeInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 32, i1 false), !tbaa.struct !41
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !22
  store i32 %last, i32* %last.addr, align 4, !tbaa !22
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !22
  store i32 %1, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !22
  %3 = load i32, i32* %last.addr, align 4, !tbaa !22
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %5 = load i32, i32* %i, align 4, !tbaa !22
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !22
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !22
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !40
  %tobool = icmp ne %class.btBvhSubtreeInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !65, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data4, align 4, !tbaa !40
  call void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %class.btBvhSubtreeInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data5, align 4, !tbaa !40
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btBvhSubtreeInfo** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btBvhSubtreeInfo**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !22
  store %class.btBvhSubtreeInfo** %hint, %class.btBvhSubtreeInfo*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !22
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  ret %class.btBvhSubtreeInfo* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %class.btBvhSubtreeInfo* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store %class.btBvhSubtreeInfo* %ptr, %class.btBvhSubtreeInfo** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btBvhSubtreeInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !53
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !33
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !32
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !52
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !63
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !37
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !36
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !62
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"bool", !4, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !9, i64 60}
!12 = !{!"_ZTS14btQuantizedBvh", !13, i64 4, !13, i64 20, !13, i64 36, !14, i64 52, !14, i64 56, !9, i64 60, !15, i64 64, !15, i64 84, !17, i64 104, !17, i64 124, !19, i64 144, !20, i64 148, !14, i64 168}
!13 = !{!"_ZTS9btVector3", !4, i64 0}
!14 = !{!"int", !4, i64 0}
!15 = !{!"_ZTS20btAlignedObjectArrayI18btOptimizedBvhNodeE", !16, i64 0, !14, i64 4, !14, i64 8, !3, i64 12, !9, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE"}
!17 = !{!"_ZTS20btAlignedObjectArrayI18btQuantizedBvhNodeE", !18, i64 0, !14, i64 4, !14, i64 8, !3, i64 12, !9, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE"}
!19 = !{!"_ZTSN14btQuantizedBvh15btTraversalModeE", !4, i64 0}
!20 = !{!"_ZTS20btAlignedObjectArrayI16btBvhSubtreeInfoE", !21, i64 0, !14, i64 4, !14, i64 8, !3, i64 12, !9, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE"}
!22 = !{!14, !14, i64 0}
!23 = !{!24, !24, i64 0}
!24 = !{!"float", !4, i64 0}
!25 = !{!12, !14, i64 56}
!26 = !{!27, !14, i64 12}
!27 = !{!"_ZTS16btBvhSubtreeInfo", !4, i64 0, !4, i64 6, !14, i64 12, !14, i64 16, !4, i64 20}
!28 = !{!27, !14, i64 16}
!29 = !{!12, !14, i64 168}
!30 = !{!31, !3, i64 8}
!31 = !{!"_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E29QuantizedNodeTriangleCallback", !3, i64 4, !3, i64 8}
!32 = !{!17, !14, i64 4}
!33 = !{!17, !3, i64 12}
!34 = !{i64 0, i64 6, !35, i64 6, i64 6, !35, i64 12, i64 4, !22}
!35 = !{!4, !4, i64 0}
!36 = !{!15, !14, i64 4}
!37 = !{!15, !3, i64 12}
!38 = !{i64 0, i64 16, !35, i64 16, i64 16, !35, i64 32, i64 4, !22, i64 36, i64 4, !22, i64 40, i64 4, !22, i64 44, i64 20, !35}
!39 = !{!20, !14, i64 4}
!40 = !{!20, !3, i64 12}
!41 = !{i64 0, i64 6, !35, i64 6, i64 6, !35, i64 12, i64 4, !22, i64 16, i64 4, !22, i64 20, i64 12, !35}
!42 = !{!43, !43, i64 0}
!43 = !{!"short", !4, i64 0}
!44 = !{!45, !14, i64 12}
!45 = !{!"_ZTS18btQuantizedBvhNode", !4, i64 0, !4, i64 6, !14, i64 12}
!46 = !{!47, !47, i64 0}
!47 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!48 = !{i64 0, i64 16, !35}
!49 = !{!50, !50, i64 0}
!50 = !{!"double", !4, i64 0}
!51 = !{!31, !3, i64 4}
!52 = !{!17, !14, i64 8}
!53 = !{!17, !9, i64 16}
!54 = !{!55, !55, i64 0}
!55 = !{!"long", !4, i64 0}
!56 = !{!57, !14, i64 32}
!57 = !{!"_ZTS18btOptimizedBvhNode", !13, i64 0, !13, i64 16, !14, i64 32, !14, i64 36, !14, i64 40, !4, i64 44}
!58 = !{!57, !14, i64 36}
!59 = !{!57, !14, i64 40}
!60 = !{!61, !3, i64 4}
!61 = !{!"_ZTSZN14btOptimizedBvh5buildEP23btStridingMeshInterfacebRK9btVector3S4_E20NodeTriangleCallback", !3, i64 4}
!62 = !{!15, !14, i64 8}
!63 = !{!15, !9, i64 16}
!64 = !{!20, !14, i64 8}
!65 = !{!20, !9, i64 16}
