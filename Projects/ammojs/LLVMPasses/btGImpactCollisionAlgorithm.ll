; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/btGImpactCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%"struct.btGImpactCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%class.btGImpactCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btCollisionAlgorithm*, %class.btPersistentManifold*, %class.btManifoldResult*, %struct.btDispatcherInfo*, i32, i32, i32, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btGImpactShapeInterface = type { %class.btConcaveShape, %class.btAABB, i8, %class.btVector3, %class.btGImpactQuantizedBvh }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btAABB = type { %class.btVector3, %class.btVector3 }
%class.btGImpactQuantizedBvh = type { %class.btQuantizedBvhTree, %class.btPrimitiveManagerBase* }
%class.btQuantizedBvhTree = type { i32, %class.GIM_QUANTIZED_BVH_NODE_ARRAY, %class.btAABB, %class.btVector3 }
%class.GIM_QUANTIZED_BVH_NODE_ARRAY = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.BT_QUANTIZED_BVH_NODE*, i8 }>
%class.btAlignedAllocator = type { i8 }
%struct.BT_QUANTIZED_BVH_NODE = type { [3 x i16], [3 x i16], i32 }
%class.btPrimitiveManagerBase = type { i32 (...)** }
%class.btPairSet = type { %class.btAlignedObjectArray.base.3, [3 x i8] }
%class.btAlignedObjectArray.base.3 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8 }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.GIM_PAIR = type { i32, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.GIM_PAIR*, i8, [3 x i8] }>
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btGImpactMeshShapePart = type { %class.btGImpactShapeInterface, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager" }
%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager" = type { %class.btPrimitiveManagerBase, float, %class.btStridingMeshInterface*, %class.btVector3, i32, i32, i8*, i32, i32, i32, i8*, i32, i32, i32 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btTriangleShapeEx = type { %class.btTriangleShape }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%class.btPrimitiveTriangle = type { [3 x %class.btVector3], %class.btVector4, float, float }
%class.btVector4 = type { %class.btVector3 }
%struct.GIM_TRIANGLE_CONTACT = type { float, i32, %class.btVector4, [16 x %class.btVector3] }
%class.btGImpactMeshShape = type { %class.btGImpactShapeInterface, %class.btStridingMeshInterface*, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btGImpactMeshShapePart**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.GIM_ShapeRetriever = type { %class.btGImpactShapeInterface*, %class.btTriangleShapeEx, %class.btTetrahedronShapeEx, %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::TriangleShapeRetriever", %"class.GIM_ShapeRetriever::TetraShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* }
%class.btTetrahedronShapeEx = type { %class.btBU_Simplex1to4 }
%class.btBU_Simplex1to4 = type { %class.btPolyhedralConvexAabbCachingShape.base, i32, [4 x %class.btVector3] }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%"class.GIM_ShapeRetriever::ChildShapeRetriever" = type { i32 (...)**, %class.GIM_ShapeRetriever* }
%"class.GIM_ShapeRetriever::TriangleShapeRetriever" = type { %"class.GIM_ShapeRetriever::ChildShapeRetriever" }
%"class.GIM_ShapeRetriever::TetraShapeRetriever" = type { %"class.GIM_ShapeRetriever::ChildShapeRetriever" }
%class.btStaticPlaneShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btVector3 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.12, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%struct.btDbvtNode = type opaque
%struct.btDbvt = type opaque
%class.btPlaneShape = type { %class.btStaticPlaneShape }
%class.btGImpactTriangleCallback = type { %class.btTriangleCallback, %class.btGImpactCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btGImpactShapeInterface*, i8, float }
%class.btTriangleCallback = type { i32 (...)** }
%class.btCollisionDispatcher = type { %class.btDispatcher, i32, %class.btAlignedObjectArray.16, %class.btManifoldResult, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, %class.btPoolAllocator*, %class.btPoolAllocator*, [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], %class.btCollisionConfiguration* }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.20 }
%union.anon.20 = type { i8* }
%class.btPoolAllocator = type opaque
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%class.btCollisionConfiguration = type opaque
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>

$_ZN27btGImpactCollisionAlgorithm10clearCacheEv = comdat any

$_ZN27btGImpactCollisionAlgorithm13checkManifoldEPK24btCollisionObjectWrapperS2_ = comdat any

$_ZN27btGImpactCollisionAlgorithm12newAlgorithmEPK24btCollisionObjectWrapperS2_ = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEPK24btCollisionObjectWrapperS2_ = comdat any

$_ZNK23btGImpactShapeInterface9hasBoxSetEv = comdat any

$_ZNK23btGImpactShapeInterface9getBoxSetEv = comdat any

$_ZN6btAABBC2Ev = comdat any

$_ZNK6btAABB13has_collisionERKS_ = comdat any

$_ZN9btPairSet9push_pairEii = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN11btTransformmLERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZN17btTriangleShapeExC2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN19btPrimitiveTriangleC2Ev = comdat any

$_ZN20GIM_TRIANGLE_CONTACTC2Ev = comdat any

$_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle = comdat any

$_ZN19btPrimitiveTriangle14applyTransformERK11btTransform = comdat any

$_ZN19btPrimitiveTriangle13buildTriPlaneEv = comdat any

$_ZNK18btGImpactMeshShape16getMeshPartCountEv = comdat any

$_ZNK18btGImpactMeshShape11getMeshPartEi = comdat any

$_ZN9btPairSetC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIREixEi = comdat any

$_ZN18GIM_ShapeRetrieverC2EPK23btGImpactShapeInterface = comdat any

$_ZN18GIM_ShapeRetriever13getChildShapeEi = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN18GIM_ShapeRetrieverD2Ev = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRED2Ev = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK16btCollisionShape10isCompoundEv = comdat any

$_ZNK16btCollisionShape9isConcaveEv = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK16btManifoldResult12getBody0WrapEv = comdat any

$_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZNK15btCompoundShape17getNumChildShapesEv = comdat any

$_ZNK15btCompoundShape13getChildShapeEi = comdat any

$_ZNK15btCompoundShape17getChildTransformEi = comdat any

$_ZNK16btManifoldResult12getBody1WrapEv = comdat any

$_ZN9btVector4C2Ev = comdat any

$_ZNK12btPlaneShape30get_plane_equation_transformedERK11btTransformR9btVector4 = comdat any

$_ZN6btAABB16increment_marginEf = comdat any

$_ZNK6btAABB14plane_classifyERK9btVector4 = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK22btGImpactMeshShapePart14getVertexCountEv = comdat any

$_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3 = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN25btGImpactTriangleCallbackC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZN27btGImpactCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv = comdat any

$_ZN27btGImpactCollisionAlgorithm22destroyConvexAlgorithmEv = comdat any

$_ZN27btGImpactCollisionAlgorithm15getLastManifoldEv = comdat any

$_ZN27btGImpactCollisionAlgorithm18newContactManifoldEPK17btCollisionObjectS2_ = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZNK21btGImpactQuantizedBvh12getNodeCountEv = comdat any

$_ZNK18btQuantizedBvhTree12getNodeCountEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_ = comdat any

$_ZN8GIM_PAIRC2Eii = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi = comdat any

$_ZN8GIM_PAIRC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN11btMatrix3x3mLERKS_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN15btTriangleShapeC2ERK9btVector3S2_S2_ = comdat any

$_ZN17btTriangleShapeExD0Ev = comdat any

$_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK15btTriangleShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i = comdat any

$_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btTriangleShape14getNumVerticesEv = comdat any

$_ZNK15btTriangleShape11getNumEdgesEv = comdat any

$_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ = comdat any

$_ZNK15btTriangleShape9getVertexEiR9btVector3 = comdat any

$_ZNK15btTriangleShape12getNumPlanesEv = comdat any

$_ZNK15btTriangleShape8getPlaneER9btVector3S1_i = comdat any

$_ZNK15btTriangleShape8isInsideERK9btVector3f = comdat any

$_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ = comdat any

$_ZN15btTriangleShapeD0Ev = comdat any

$_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZN15btTriangleShapedlEPv = comdat any

$_ZN6btAABBC2ERK9btVector3S2_S2_f = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK15btTriangleShape10calcNormalER9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector48setValueERKfS1_S1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIREC2Ev = comdat any

$_ZN18btAlignedAllocatorI8GIM_PAIRLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE4initEv = comdat any

$_ZN20btTetrahedronShapeExC2Ev = comdat any

$_ZN18GIM_ShapeRetriever19ChildShapeRetrieverC2Ev = comdat any

$_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverC2Ev = comdat any

$_ZN18GIM_ShapeRetriever19TetraShapeRetrieverC2Ev = comdat any

$_ZN20btTetrahedronShapeExD0Ev = comdat any

$_ZNK16btBU_Simplex1to47getNameEv = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN16btBU_Simplex1to4dlEPv = comdat any

$_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi = comdat any

$_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev = comdat any

$_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev = comdat any

$_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi = comdat any

$_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev = comdat any

$_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi = comdat any

$_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN20btAlignedObjectArrayI8GIM_PAIRE5clearEv = comdat any

$_ZN17btBroadphaseProxy10isCompoundEi = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x36getRowEi = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK6btAABB19projection_intervalERK9btVector3RfS3_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector38absoluteEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3 = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN25btGImpactTriangleCallbackD0Ev = comdat any

$_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii = comdat any

$_ZN17btTriangleShapeExC2ERK9btVector3S2_S2_ = comdat any

$_ZN27btGImpactCollisionAlgorithm8setPart0Ei = comdat any

$_ZN27btGImpactCollisionAlgorithm8setFace0Ei = comdat any

$_ZN27btGImpactCollisionAlgorithm8setPart1Ei = comdat any

$_ZN27btGImpactCollisionAlgorithm8setFace1Ei = comdat any

$_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZTV17btTriangleShapeEx = comdat any

$_ZTS17btTriangleShapeEx = comdat any

$_ZTS15btTriangleShape = comdat any

$_ZTI15btTriangleShape = comdat any

$_ZTI17btTriangleShapeEx = comdat any

$_ZTV15btTriangleShape = comdat any

$_ZTV20btTetrahedronShapeEx = comdat any

$_ZTS20btTetrahedronShapeEx = comdat any

$_ZTI20btTetrahedronShapeEx = comdat any

$_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE = comdat any

$_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE = comdat any

$_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE = comdat any

$_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE = comdat any

$_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE = comdat any

$_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE = comdat any

$_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE = comdat any

$_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE = comdat any

$_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE = comdat any

$_ZTV25btGImpactTriangleCallback = comdat any

$_ZTS25btGImpactTriangleCallback = comdat any

$_ZTI25btGImpactTriangleCallback = comdat any

$_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

@_ZTV27btGImpactCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27btGImpactCollisionAlgorithm to i8*), i8* bitcast (%class.btGImpactCollisionAlgorithm* (%class.btGImpactCollisionAlgorithm*)* @_ZN27btGImpactCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btGImpactCollisionAlgorithm*)* @_ZN27btGImpactCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btGImpactCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN27btGImpactCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btGImpactCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btGImpactCollisionAlgorithm*, %class.btAlignedObjectArray.16*)* @_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf = internal global %"struct.btGImpactCollisionAlgorithm::CreateFunc" zeroinitializer, align 4
@_ZGVZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS27btGImpactCollisionAlgorithm = hidden constant [30 x i8] c"27btGImpactCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI27btGImpactCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27btGImpactCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@_ZTV17btTriangleShapeEx = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btTriangleShapeEx to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShapeEx*)* @_ZN17btTriangleShapeExD0Ev to i8*), i8* bitcast (void (%class.btTriangleShapeEx*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@_ZTS17btTriangleShapeEx = linkonce_odr hidden constant [20 x i8] c"17btTriangleShapeEx\00", comdat, align 1
@_ZTS15btTriangleShape = linkonce_odr hidden constant [18 x i8] c"15btTriangleShape\00", comdat, align 1
@_ZTI23btPolyhedralConvexShape = external constant i8*
@_ZTI15btTriangleShape = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btTriangleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btPolyhedralConvexShape to i8*) }, comdat, align 4
@_ZTI17btTriangleShapeEx = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btTriangleShapeEx, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*) }, comdat, align 4
@_ZTV15btTriangleShape = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShape*)* @_ZN15btTriangleShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@.str = private unnamed_addr constant [9 x i8] c"Triangle\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@_ZTV20btTetrahedronShapeEx = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btTetrahedronShapeEx to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTetrahedronShapeEx*)* @_ZN20btTetrahedronShapeExD0Ev to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3*)* @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to47getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to414getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to411getNumEdgesEv to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, i32, %class.btVector3*)* @_ZNK16btBU_Simplex1to49getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*)* @_ZNK16btBU_Simplex1to412getNumPlanesEv to i8*), i8* bitcast (void (%class.btBU_Simplex1to4*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btBU_Simplex1to4*, %class.btVector3*, float)* @_ZNK16btBU_Simplex1to48isInsideERK9btVector3f to i8*), i8* bitcast (i32 (%class.btBU_Simplex1to4*, i32)* @_ZNK16btBU_Simplex1to48getIndexEi to i8*)] }, comdat, align 4
@_ZTS20btTetrahedronShapeEx = linkonce_odr hidden constant [23 x i8] c"20btTetrahedronShapeEx\00", comdat, align 1
@_ZTI16btBU_Simplex1to4 = external constant i8*
@_ZTI20btTetrahedronShapeEx = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btTetrahedronShapeEx, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btBU_Simplex1to4 to i8*) }, comdat, align 4
@.str.2 = private unnamed_addr constant [17 x i8] c"btBU_Simplex1to4\00", align 1
@_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE to i8*), i8* bitcast (%class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)* @_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi to i8*), i8* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to i8*), i8* bitcast (void (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE = linkonce_odr hidden constant [44 x i8] c"N18GIM_ShapeRetriever19ChildShapeRetrieverE\00", comdat, align 1
@_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([44 x i8], [44 x i8]* @_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE, i32 0, i32 0) }, comdat, align 4
@_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE to i8*), i8* bitcast (%class.btCollisionShape* (%"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, i32)* @_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi to i8*), i8* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to i8*), i8* bitcast (void (%"class.GIM_ShapeRetriever::TriangleShapeRetriever"*)* @_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev to i8*)] }, comdat, align 4
@_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE = linkonce_odr hidden constant [47 x i8] c"N18GIM_ShapeRetriever22TriangleShapeRetrieverE\00", comdat, align 1
@_ZTIN18GIM_ShapeRetriever22TriangleShapeRetrieverE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([47 x i8], [47 x i8]* @_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE to i8*) }, comdat, align 4
@_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE to i8*), i8* bitcast (%class.btCollisionShape* (%"class.GIM_ShapeRetriever::TetraShapeRetriever"*, i32)* @_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi to i8*), i8* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to i8*), i8* bitcast (void (%"class.GIM_ShapeRetriever::TetraShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev to i8*)] }, comdat, align 4
@_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE = linkonce_odr hidden constant [44 x i8] c"N18GIM_ShapeRetriever19TetraShapeRetrieverE\00", comdat, align 1
@_ZTIN18GIM_ShapeRetriever19TetraShapeRetrieverE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([44 x i8], [44 x i8]* @_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN18GIM_ShapeRetriever19ChildShapeRetrieverE to i8*) }, comdat, align 4
@_ZTV25btGImpactTriangleCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btGImpactTriangleCallback to i8*), i8* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to i8*), i8* bitcast (void (%class.btGImpactTriangleCallback*)* @_ZN25btGImpactTriangleCallbackD0Ev to i8*), i8* bitcast (void (%class.btGImpactTriangleCallback*, %class.btVector3*, i32, i32)* @_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTS25btGImpactTriangleCallback = linkonce_odr hidden constant [28 x i8] c"25btGImpactTriangleCallback\00", comdat, align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI25btGImpactTriangleCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btGImpactTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, comdat, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btGImpactCollisionAlgorithm::CreateFunc"*)* @_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btGImpactCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [44 x i8] c"N27btGImpactCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN27btGImpactCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([44 x i8], [44 x i8]* @_ZTSN27btGImpactCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4

@_ZN27btGImpactCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = hidden unnamed_addr alias %class.btGImpactCollisionAlgorithm* (%class.btGImpactCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*), %class.btGImpactCollisionAlgorithm* (%class.btGImpactCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_
@_ZN27btGImpactCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btGImpactCollisionAlgorithm* (%class.btGImpactCollisionAlgorithm*), %class.btGImpactCollisionAlgorithm* (%class.btGImpactCollisionAlgorithm*)* @_ZN27btGImpactCollisionAlgorithmD2Ev

define hidden %class.btGImpactCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btGImpactCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV27btGImpactCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  %m_manifoldPtr = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !8
  %m_convex_algorithm = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_convex_algorithm, align 4, !tbaa !11
  ret %class.btGImpactCollisionAlgorithm* %this1
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #1

; Function Attrs: nounwind
define hidden %class.btGImpactCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithmD2Ev(%class.btGImpactCollisionAlgorithm* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV27btGImpactCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  call void @_ZN27btGImpactCollisionAlgorithm10clearCacheEv(%class.btGImpactCollisionAlgorithm* %this1)
  %1 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %1) #8
  ret %class.btGImpactCollisionAlgorithm* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm10clearCacheEv(%class.btGImpactCollisionAlgorithm* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  call void @_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv(%class.btGImpactCollisionAlgorithm* %this1)
  call void @_ZN27btGImpactCollisionAlgorithm22destroyConvexAlgorithmEv(%class.btGImpactCollisionAlgorithm* %this1)
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 -1, i32* %m_triface0, align 4, !tbaa !12
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  store i32 -1, i32* %m_part0, align 4, !tbaa !13
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 -1, i32* %m_triface1, align 4, !tbaa !14
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  store i32 -1, i32* %m_part1, align 4, !tbaa !15
  ret void
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: nounwind
define hidden void @_ZN27btGImpactCollisionAlgorithmD0Ev(%class.btGImpactCollisionAlgorithm* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btGImpactCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithmD1Ev(%class.btGImpactCollisionAlgorithm* %this1) #8
  %0 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden void @_ZN27btGImpactCollisionAlgorithm15addContactPointEPK24btCollisionObjectWrapperS2_RK9btVector3S5_f(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btVector3* nonnull align 4 dereferenceable(16) %point, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %distance) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %distance.addr = alloca float, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  store float %distance, float* %distance.addr, align 4, !tbaa !16
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %0 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_part0, align 4, !tbaa !13
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_triface0, align 4, !tbaa !12
  %3 = bitcast %class.btManifoldResult* %0 to void (%class.btManifoldResult*, i32, i32)***
  %vtable = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable, i64 2
  %4 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn, align 4
  call void %4(%class.btManifoldResult* %0, i32 %1, i32 %2)
  %m_resultOut2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %5 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut2, align 4, !tbaa !18
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %6 = load i32, i32* %m_part1, align 4, !tbaa !15
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %7 = load i32, i32* %m_triface1, align 4, !tbaa !14
  %8 = bitcast %class.btManifoldResult* %5 to void (%class.btManifoldResult*, i32, i32)***
  %vtable3 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %8, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable3, i64 3
  %9 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn4, align 4
  call void %9(%class.btManifoldResult* %5, i32 %6, i32 %7)
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm13checkManifoldEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11)
  %m_resultOut5 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %12 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut5, align 4, !tbaa !18
  %13 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %15 = load float, float* %distance.addr, align 4, !tbaa !16
  %16 = bitcast %class.btManifoldResult* %12 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable6 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %16, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable6, i64 4
  %17 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn7, align 4
  call void %17(%class.btManifoldResult* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, float %15)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm13checkManifoldEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm15getLastManifoldEv(%class.btGImpactCollisionAlgorithm* %this1)
  %cmp = icmp eq %class.btPersistentManifold* %call, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %0)
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %1)
  %call4 = call %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm18newContactManifoldEPK17btCollisionObjectS2_(%class.btGImpactCollisionAlgorithm* %this1, %class.btCollisionObject* %call2, %class.btCollisionObject* %call3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %2 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %call5 = call %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm15getLastManifoldEv(%class.btGImpactCollisionAlgorithm* %this1)
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %2, %class.btPersistentManifold* %call5)
  ret void
}

define hidden void @_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btCollisionShape* %shape0, %class.btCollisionShape* %shape1) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btCollisionShape*, align 4
  %shape1.addr = alloca %class.btCollisionShape*, align 4
  %algor = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape0, %class.btCollisionShape** %shape0.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape1, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCollisionAlgorithm** %algor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithm12newAlgorithmEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper* %2)
  store %class.btCollisionAlgorithm* %call, %class.btCollisionAlgorithm** %algor, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %3 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %4 = load i32, i32* %m_part0, align 4, !tbaa !13
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %5 = load i32, i32* %m_triface0, align 4, !tbaa !12
  %6 = bitcast %class.btManifoldResult* %3 to void (%class.btManifoldResult*, i32, i32)***
  %vtable = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable, i64 2
  %7 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn, align 4
  call void %7(%class.btManifoldResult* %3, i32 %4, i32 %5)
  %m_resultOut2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %8 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut2, align 4, !tbaa !18
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %9 = load i32, i32* %m_part1, align 4, !tbaa !15
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %10 = load i32, i32* %m_triface1, align 4, !tbaa !14
  %11 = bitcast %class.btManifoldResult* %8 to void (%class.btManifoldResult*, i32, i32)***
  %vtable3 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %11, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable3, i64 3
  %12 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn4, align 4
  call void %12(%class.btManifoldResult* %8, i32 %9, i32 %10)
  %13 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algor, align 4, !tbaa !2
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %m_dispatchInfo = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 4
  %16 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !19
  %m_resultOut5 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %17 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut5, align 4, !tbaa !18
  %18 = bitcast %class.btCollisionAlgorithm* %13 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable6 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %18, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable6, i64 2
  %19 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn7, align 4
  call void %19(%class.btCollisionAlgorithm* %13, %struct.btCollisionObjectWrapper* %14, %struct.btCollisionObjectWrapper* %15, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %16, %class.btManifoldResult* %17)
  %20 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algor, align 4, !tbaa !2
  %21 = bitcast %class.btCollisionAlgorithm* %20 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable8 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %21, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable8, i64 0
  %22 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn9, align 4
  %call10 = call %class.btCollisionAlgorithm* %22(%class.btCollisionAlgorithm* %20) #8
  %23 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %23, i32 0, i32 1
  %24 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !20
  %25 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algor, align 4, !tbaa !2
  %26 = bitcast %class.btCollisionAlgorithm* %25 to i8*
  %27 = bitcast %class.btDispatcher* %24 to void (%class.btDispatcher*, i8*)***
  %vtable11 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %27, align 4, !tbaa !6
  %vfn12 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable11, i64 15
  %28 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn12, align 4
  call void %28(%class.btDispatcher* %24, i8* %26)
  %29 = bitcast %class.btCollisionAlgorithm** %algor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #6

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithm12newAlgorithmEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %convex_algorithm = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm13checkManifoldEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper* %1)
  %2 = bitcast %class.btCollisionAlgorithm** %convex_algorithm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %3, i32 0, i32 1
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !20
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm15getLastManifoldEv(%class.btGImpactCollisionAlgorithm* %this1)
  %7 = bitcast %class.btDispatcher* %4 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %7, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable, i64 2
  %8 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn, align 4
  %call2 = call %class.btCollisionAlgorithm* %8(%class.btDispatcher* %4, %struct.btCollisionObjectWrapper* %5, %struct.btCollisionObjectWrapper* %6, %class.btPersistentManifold* %call)
  store %class.btCollisionAlgorithm* %call2, %class.btCollisionAlgorithm** %convex_algorithm, align 4, !tbaa !2
  %9 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %convex_algorithm, align 4, !tbaa !2
  %10 = bitcast %class.btCollisionAlgorithm** %convex_algorithm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret %class.btCollisionAlgorithm* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #6

define hidden void @_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btCollisionShape* %shape0, %class.btCollisionShape* %shape1) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btCollisionShape*, align 4
  %shape1.addr = alloca %class.btCollisionShape*, align 4
  %ob0 = alloca %struct.btCollisionObjectWrapper, align 4
  %ob1 = alloca %struct.btCollisionObjectWrapper, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape0, %class.btCollisionShape** %shape0.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape1, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %0 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_part0, align 4, !tbaa !13
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_triface0, align 4, !tbaa !12
  %3 = bitcast %class.btManifoldResult* %0 to void (%class.btManifoldResult*, i32, i32)***
  %vtable = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable, i64 2
  %4 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn, align 4
  call void %4(%class.btManifoldResult* %0, i32 %1, i32 %2)
  %m_resultOut2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %5 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut2, align 4, !tbaa !18
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %6 = load i32, i32* %m_part1, align 4, !tbaa !15
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %7 = load i32, i32* %m_triface1, align 4, !tbaa !14
  %8 = bitcast %class.btManifoldResult* %5 to void (%class.btManifoldResult*, i32, i32)***
  %vtable3 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %8, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable3, i64 3
  %9 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn4, align 4
  call void %9(%class.btManifoldResult* %5, i32 %6, i32 %7)
  %10 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %10) #8
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %12 = load %class.btCollisionShape*, %class.btCollisionShape** %shape0.addr, align 4, !tbaa !2
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %13)
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %14)
  %m_part06 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %15 = load i32, i32* %m_part06, align 4, !tbaa !13
  %m_triface07 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %16 = load i32, i32* %m_triface07, align 4, !tbaa !12
  %call8 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %11, %class.btCollisionShape* %12, %class.btCollisionObject* %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call5, i32 %15, i32 %16)
  %17 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %17) #8
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %19 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %20 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call9 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %20)
  %21 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %21)
  %m_part111 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %22 = load i32, i32* %m_part111, align 4, !tbaa !15
  %m_triface112 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %23 = load i32, i32* %m_triface112, align 4, !tbaa !14
  %call13 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob1, %struct.btCollisionObjectWrapper* %18, %class.btCollisionShape* %19, %class.btCollisionObject* %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call10, i32 %22, i32 %23)
  call void @_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %ob1)
  %m_convex_algorithm = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  %24 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_convex_algorithm, align 4, !tbaa !11
  %m_dispatchInfo = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 4
  %25 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !19
  %m_resultOut14 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %26 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut14, align 4, !tbaa !18
  %27 = bitcast %class.btCollisionAlgorithm* %24 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable15 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %27, align 4, !tbaa !6
  %vfn16 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable15, i64 2
  %28 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn16, align 4
  call void %28(%class.btCollisionAlgorithm* %24, %struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %ob1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %25, %class.btManifoldResult* %26)
  %29 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %29) #8
  %30 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %30) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #7 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !22
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #7 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !24
  ret %class.btTransform* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !25
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4, !tbaa !26
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4, !tbaa !27
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !22
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4, !tbaa !2
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4, !tbaa !25
  store i32 %4, i32* %m_partId, align 4, !tbaa !28
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4, !tbaa !25
  store i32 %5, i32* %m_index, align 4, !tbaa !29
  ret %struct.btCollisionObjectWrapper* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm20checkConvexAlgorithmEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_convex_algorithm = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  %0 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_convex_algorithm, align 4, !tbaa !11
  %tobool = icmp ne %class.btCollisionAlgorithm* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithm12newAlgorithmEPK24btCollisionObjectWrapperS2_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper* %2)
  %m_convex_algorithm2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btCollisionAlgorithm* %call, %class.btCollisionAlgorithm** %m_convex_algorithm2, align 4, !tbaa !11
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

define hidden void @_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_PK23btGImpactShapeInterfaceS5_R9btPairSet(%class.btGImpactCollisionAlgorithm* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface* %shape1, %class.btPairSet* nonnull align 4 dereferenceable(17) %pairset) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btGImpactShapeInterface*, align 4
  %pairset.addr = alloca %class.btPairSet*, align 4
  %boxshape0 = alloca %class.btAABB, align 4
  %boxshape1 = alloca %class.btAABB, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape1, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  store %class.btPairSet* %pairset, %class.btPairSet** %pairset.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK23btGImpactShapeInterface9hasBoxSetEv(%class.btGImpactShapeInterface* %0)
  br i1 %call, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %call2 = call zeroext i1 @_ZNK23btGImpactShapeInterface9hasBoxSetEv(%class.btGImpactShapeInterface* %1)
  br i1 %call2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call3 = call %class.btGImpactQuantizedBvh* @_ZNK23btGImpactShapeInterface9getBoxSetEv(%class.btGImpactShapeInterface* %2)
  %3 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  %4 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %call4 = call %class.btGImpactQuantizedBvh* @_ZNK23btGImpactShapeInterface9getBoxSetEv(%class.btGImpactShapeInterface* %4)
  %5 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  %6 = load %class.btPairSet*, %class.btPairSet** %pairset.addr, align 4, !tbaa !2
  call void @_ZN21btGImpactQuantizedBvh14find_collisionEPKS_RK11btTransformS1_S4_R9btPairSet(%class.btGImpactQuantizedBvh* %call3, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btGImpactQuantizedBvh* %call4, %class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btPairSet* nonnull align 4 dereferenceable(17) %6)
  br label %if.end24

if.else:                                          ; preds = %land.lhs.true, %entry
  %7 = bitcast %class.btAABB* %boxshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %7) #8
  %call5 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %boxshape0)
  %8 = bitcast %class.btAABB* %boxshape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %8) #8
  %call6 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %boxshape1)
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %11 = bitcast %class.btGImpactShapeInterface* %10 to i32 (%class.btGImpactShapeInterface*)***
  %vtable = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable, i64 22
  %12 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call7 = call i32 %12(%class.btGImpactShapeInterface* %10)
  store i32 %call7, i32* %i, align 4, !tbaa !25
  br label %while.cond

while.cond:                                       ; preds = %while.end, %if.else
  %13 = load i32, i32* %i, align 4, !tbaa !25
  %dec = add nsw i32 %13, -1
  store i32 %dec, i32* %i, align 4, !tbaa !25
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %while.body, label %while.end23

while.body:                                       ; preds = %while.cond
  %14 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %15 = load i32, i32* %i, align 4, !tbaa !25
  %16 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape0, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape0, i32 0, i32 1
  %17 = bitcast %class.btGImpactShapeInterface* %14 to void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable8 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %17, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable8, i64 30
  %18 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn9, align 4
  call void %18(%class.btGImpactShapeInterface* %14, i32 %15, %class.btTransform* nonnull align 4 dereferenceable(64) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max)
  %19 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %21 = bitcast %class.btGImpactShapeInterface* %20 to i32 (%class.btGImpactShapeInterface*)***
  %vtable10 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %21, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable10, i64 22
  %22 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn11, align 4
  %call12 = call i32 %22(%class.btGImpactShapeInterface* %20)
  store i32 %call12, i32* %j, align 4, !tbaa !25
  br label %while.cond13

while.cond13:                                     ; preds = %if.end, %while.body
  %23 = load i32, i32* %j, align 4, !tbaa !25
  %dec14 = add nsw i32 %23, -1
  store i32 %dec14, i32* %j, align 4, !tbaa !25
  %tobool15 = icmp ne i32 %23, 0
  br i1 %tobool15, label %while.body16, label %while.end

while.body16:                                     ; preds = %while.cond13
  %24 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %25 = load i32, i32* %i, align 4, !tbaa !25
  %26 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  %m_min17 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape1, i32 0, i32 0
  %m_max18 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape1, i32 0, i32 1
  %27 = bitcast %class.btGImpactShapeInterface* %24 to void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable19 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %27, align 4, !tbaa !6
  %vfn20 = getelementptr inbounds void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable19, i64 30
  %28 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn20, align 4
  call void %28(%class.btGImpactShapeInterface* %24, i32 %25, %class.btTransform* nonnull align 4 dereferenceable(64) %26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max18)
  %call21 = call zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %boxshape1, %class.btAABB* nonnull align 4 dereferenceable(32) %boxshape0)
  br i1 %call21, label %if.then22, label %if.end

if.then22:                                        ; preds = %while.body16
  %29 = load %class.btPairSet*, %class.btPairSet** %pairset.addr, align 4, !tbaa !2
  %30 = load i32, i32* %i, align 4, !tbaa !25
  %31 = load i32, i32* %j, align 4, !tbaa !25
  call void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %29, i32 %30, i32 %31)
  br label %if.end

if.end:                                           ; preds = %if.then22, %while.body16
  br label %while.cond13

while.end:                                        ; preds = %while.cond13
  %32 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  br label %while.cond

while.end23:                                      ; preds = %while.cond
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast %class.btAABB* %boxshape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %34) #8
  %35 = bitcast %class.btAABB* %boxshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %35) #8
  br label %if.end24

if.end24:                                         ; preds = %while.end23, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK23btGImpactShapeInterface9hasBoxSetEv(%class.btGImpactShapeInterface* %this) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  %call = call i32 @_ZNK21btGImpactQuantizedBvh12getNodeCountEv(%class.btGImpactQuantizedBvh* %m_box_set)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %0 = load i1, i1* %retval, align 1
  ret i1 %0
}

declare void @_ZN21btGImpactQuantizedBvh14find_collisionEPKS_RK11btTransformS1_S4_R9btPairSet(%class.btGImpactQuantizedBvh*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btGImpactQuantizedBvh*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btPairSet* nonnull align 4 dereferenceable(17)) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btGImpactQuantizedBvh* @_ZNK23btGImpactShapeInterface9getBoxSetEv(%class.btGImpactShapeInterface* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  ret %class.btGImpactQuantizedBvh* %m_box_set
}

define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  ret %class.btAABB* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) #3 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !16
  %1 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 1
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !16
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_max4 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %3 = load float, float* %arrayidx6, align 4, !tbaa !16
  %4 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min7 = getelementptr inbounds %class.btAABB, %class.btAABB* %4, i32 0, i32 0
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %5 = load float, float* %arrayidx9, align 4, !tbaa !16
  %cmp10 = fcmp olt float %3, %5
  br i1 %cmp10, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %m_min12 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %6 = load float, float* %arrayidx14, align 4, !tbaa !16
  %7 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max15 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 1
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max15)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %8 = load float, float* %arrayidx17, align 4, !tbaa !16
  %cmp18 = fcmp ogt float %6, %8
  br i1 %cmp18, label %if.then, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false11
  %m_max20 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %9 = load float, float* %arrayidx22, align 4, !tbaa !16
  %10 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min23 = getelementptr inbounds %class.btAABB, %class.btAABB* %10, i32 0, i32 0
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min23)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  %11 = load float, float* %arrayidx25, align 4, !tbaa !16
  %cmp26 = fcmp olt float %9, %11
  br i1 %cmp26, label %if.then, label %lor.lhs.false27

lor.lhs.false27:                                  ; preds = %lor.lhs.false19
  %m_min28 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min28)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %12 = load float, float* %arrayidx30, align 4, !tbaa !16
  %13 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_max31 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 1
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 2
  %14 = load float, float* %arrayidx33, align 4, !tbaa !16
  %cmp34 = fcmp ogt float %12, %14
  br i1 %cmp34, label %if.then, label %lor.lhs.false35

lor.lhs.false35:                                  ; preds = %lor.lhs.false27
  %m_max36 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call37 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max36)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 2
  %15 = load float, float* %arrayidx38, align 4, !tbaa !16
  %16 = load %class.btAABB*, %class.btAABB** %other.addr, align 4, !tbaa !2
  %m_min39 = getelementptr inbounds %class.btAABB, %class.btAABB* %16, i32 0, i32 0
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min39)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  %17 = load float, float* %arrayidx41, align 4, !tbaa !16
  %cmp42 = fcmp olt float %15, %17
  br i1 %cmp42, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false35, %lor.lhs.false27, %lor.lhs.false19, %lor.lhs.false11, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false35
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %18 = load i1, i1* %retval, align 1
  ret i1 %18
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btPairSet9push_pairEii(%class.btPairSet* %this, i32 %index1, i32 %index2) #3 comdat {
entry:
  %this.addr = alloca %class.btPairSet*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.GIM_PAIR, align 4
  store %class.btPairSet* %this, %class.btPairSet** %this.addr, align 4, !tbaa !2
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !25
  store i32 %index2, i32* %index2.addr, align 4, !tbaa !25
  %this1 = load %class.btPairSet*, %class.btPairSet** %this.addr, align 4
  %0 = bitcast %class.btPairSet* %this1 to %class.btAlignedObjectArray.0*
  %1 = bitcast %struct.GIM_PAIR* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #8
  %2 = load i32, i32* %index1.addr, align 4, !tbaa !25
  %3 = load i32, i32* %index2.addr, align 4, !tbaa !25
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* %ref.tmp, i32 %2, i32 %3)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.0* %0, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %ref.tmp)
  %4 = bitcast %struct.GIM_PAIR* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %4) #8
  ret void
}

define hidden void @_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_PK23btGImpactShapeInterfacePK16btCollisionShapeR20btAlignedObjectArrayIiE(%class.btGImpactCollisionAlgorithm* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btGImpactShapeInterface* %shape0, %class.btCollisionShape* %shape1, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_primitives) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btCollisionShape*, align 4
  %collided_primitives.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %boxshape = alloca %class.btAABB, align 4
  %trans1to0 = alloca %class.btTransform, align 4
  %boxshape0 = alloca %class.btAABB, align 4
  %i = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape1, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.4* %collided_primitives, %class.btAlignedObjectArray.4** %collided_primitives.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btAABB* %boxshape to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %0) #8
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %boxshape)
  %1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call2 = call zeroext i1 @_ZNK23btGImpactShapeInterface9hasBoxSetEv(%class.btGImpactShapeInterface* %1)
  br i1 %call2, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btTransform* %trans1to0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #8
  %3 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %trans1to0, %class.btTransform* %3)
  %4 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformmLERKS_(%class.btTransform* %trans1to0, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %5 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape, i32 0, i32 1
  %6 = bitcast %class.btCollisionShape* %5 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %7 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btCollisionShape* %5, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1to0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max)
  %8 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call4 = call %class.btGImpactQuantizedBvh* @_ZNK23btGImpactShapeInterface9getBoxSetEv(%class.btGImpactShapeInterface* %8)
  %9 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_primitives.addr, align 4, !tbaa !2
  %call5 = call zeroext i1 @_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh* %call4, %class.btAABB* nonnull align 4 dereferenceable(32) %boxshape, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %9)
  %10 = bitcast %class.btTransform* %trans1to0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %10) #8
  br label %if.end20

if.else:                                          ; preds = %entry
  %11 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %12 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4, !tbaa !2
  %m_min6 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape, i32 0, i32 0
  %m_max7 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape, i32 0, i32 1
  %13 = bitcast %class.btCollisionShape* %11 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable8 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %13, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable8, i64 2
  %14 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn9, align 4
  call void %14(%class.btCollisionShape* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max7)
  %15 = bitcast %class.btAABB* %boxshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %15) #8
  %call10 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %boxshape0)
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %18 = bitcast %class.btGImpactShapeInterface* %17 to i32 (%class.btGImpactShapeInterface*)***
  %vtable11 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %18, align 4, !tbaa !6
  %vfn12 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable11, i64 22
  %19 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn12, align 4
  %call13 = call i32 %19(%class.btGImpactShapeInterface* %17)
  store i32 %call13, i32* %i, align 4, !tbaa !25
  br label %while.cond

while.cond:                                       ; preds = %if.end, %if.else
  %20 = load i32, i32* %i, align 4, !tbaa !25
  %dec = add nsw i32 %20, -1
  store i32 %dec, i32* %i, align 4, !tbaa !25
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %21 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %22 = load i32, i32* %i, align 4, !tbaa !25
  %23 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4, !tbaa !2
  %m_min14 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape0, i32 0, i32 0
  %m_max15 = getelementptr inbounds %class.btAABB, %class.btAABB* %boxshape0, i32 0, i32 1
  %24 = bitcast %class.btGImpactShapeInterface* %21 to void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable16 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %24, align 4, !tbaa !6
  %vfn17 = getelementptr inbounds void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable16, i64 30
  %25 = load void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn17, align 4
  call void %25(%class.btGImpactShapeInterface* %21, i32 %22, %class.btTransform* nonnull align 4 dereferenceable(64) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max15)
  %call18 = call zeroext i1 @_ZNK6btAABB13has_collisionERKS_(%class.btAABB* %boxshape, %class.btAABB* nonnull align 4 dereferenceable(32) %boxshape0)
  br i1 %call18, label %if.then19, label %if.end

if.then19:                                        ; preds = %while.body
  %26 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %collided_primitives.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %26, i32* nonnull align 4 dereferenceable(4) %i)
  br label %if.end

if.end:                                           ; preds = %if.then19, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %27 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast %class.btAABB* %boxshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %28) #8
  br label %if.end20

if.end20:                                         ; preds = %while.end, %if.then
  %29 = bitcast %class.btAABB* %boxshape to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %29) #8
  ret void
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformmLERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %m_origin2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_origin2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis3 = getelementptr inbounds %class.btTransform, %class.btTransform* %3, i32 0, i32 0
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3mLERKS_(%class.btMatrix3x3* %m_basis4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis3)
  ret %class.btTransform* %this1
}

declare zeroext i1 @_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh*, %class.btAABB* nonnull align 4 dereferenceable(32), %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17)) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.4* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32* %_Val, i32** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !25
  %1 = load i32, i32* %sz, align 4, !tbaa !25
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data, align 4, !tbaa !30
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = bitcast i32* %arrayidx to i8*
  %5 = bitcast i8* %4 to i32*
  %6 = load i32*, i32** %_Val.addr, align 4, !tbaa !2
  %7 = load i32, i32* %6, align 4, !tbaa !25
  store i32 %7, i32* %5, align 4, !tbaa !25
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !34
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !34
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

define hidden void @_ZN27btGImpactCollisionAlgorithm21collide_gjk_trianglesEPK24btCollisionObjectWrapperS2_PK22btGImpactMeshShapePartS5_PKii(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactMeshShapePart* %shape0, %class.btGImpactMeshShapePart* %shape1, i32* %pairs, i32 %pair_count) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %shape1.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %pairs.addr = alloca i32*, align 4
  %pair_count.addr = alloca i32, align 4
  %tri0 = alloca %class.btTriangleShapeEx, align 4
  %tri1 = alloca %class.btTriangleShapeEx, align 4
  %pair_pointer = alloca i32*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactMeshShapePart* %shape0, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  store %class.btGImpactMeshShapePart* %shape1, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  store i32* %pairs, i32** %pairs.addr, align 4, !tbaa !2
  store i32 %pair_count, i32* %pair_count.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %tri0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %0) #8
  %call = call %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2Ev(%class.btTriangleShapeEx* %tri0)
  %1 = bitcast %class.btTriangleShapeEx* %tri1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %1) #8
  %call2 = call %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2Ev(%class.btTriangleShapeEx* %tri1)
  %2 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %3 = bitcast %class.btGImpactMeshShapePart* %2 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 28
  %4 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %4(%class.btGImpactMeshShapePart* %2)
  %5 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %6 = bitcast %class.btGImpactMeshShapePart* %5 to void (%class.btGImpactMeshShapePart*)***
  %vtable3 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %6, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable3, i64 28
  %7 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn4, align 4
  call void %7(%class.btGImpactMeshShapePart* %5)
  %8 = bitcast i32** %pair_pointer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load i32*, i32** %pairs.addr, align 4, !tbaa !2
  store i32* %9, i32** %pair_pointer, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %10 = load i32, i32* %pair_count.addr, align 4, !tbaa !25
  %dec = add nsw i32 %10, -1
  store i32 %dec, i32* %pair_count.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %12 = load i32, i32* %11, align 4, !tbaa !25
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %12, i32* %m_triface0, align 4, !tbaa !12
  %13 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %13, i32 1
  %14 = load i32, i32* %add.ptr, align 4, !tbaa !25
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %14, i32* %m_triface1, align 4, !tbaa !14
  %15 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %add.ptr5 = getelementptr inbounds i32, i32* %15, i32 2
  store i32* %add.ptr5, i32** %pair_pointer, align 4, !tbaa !2
  %16 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %m_triface06 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %17 = load i32, i32* %m_triface06, align 4, !tbaa !12
  %18 = bitcast %class.btGImpactMeshShapePart* %16 to void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)***
  %vtable7 = load void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)**, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*** %18, align 4, !tbaa !6
  %vfn8 = getelementptr inbounds void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)** %vtable7, i64 26
  %19 = load void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)** %vfn8, align 4
  call void %19(%class.btGImpactMeshShapePart* %16, i32 %17, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(104) %tri0)
  %20 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %m_triface19 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %21 = load i32, i32* %m_triface19, align 4, !tbaa !14
  %22 = bitcast %class.btGImpactMeshShapePart* %20 to void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)***
  %vtable10 = load void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)**, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*** %22, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)** %vtable10, i64 26
  %23 = load void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)** %vfn11, align 4
  call void %23(%class.btGImpactMeshShapePart* %20, i32 %21, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(104) %tri1)
  %call12 = call zeroext i1 @_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_(%class.btTriangleShapeEx* %tri0, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(104) %tri1)
  br i1 %call12, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %24 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %25 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %26 = bitcast %class.btTriangleShapeEx* %tri0 to %class.btCollisionShape*
  %27 = bitcast %class.btTriangleShapeEx* %tri1 to %class.btCollisionShape*
  call void @_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %24, %struct.btCollisionObjectWrapper* %25, %class.btCollisionShape* %26, %class.btCollisionShape* %27)
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %29 = bitcast %class.btGImpactMeshShapePart* %28 to void (%class.btGImpactMeshShapePart*)***
  %vtable13 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %29, align 4, !tbaa !6
  %vfn14 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable13, i64 29
  %30 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn14, align 4
  call void %30(%class.btGImpactMeshShapePart* %28)
  %31 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %32 = bitcast %class.btGImpactMeshShapePart* %31 to void (%class.btGImpactMeshShapePart*)***
  %vtable15 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %32, align 4, !tbaa !6
  %vfn16 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable15, i64 29
  %33 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn16, align 4
  call void %33(%class.btGImpactMeshShapePart* %31)
  %34 = bitcast i32** %pair_pointer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %call17 = call %class.btTriangleShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShapeEx* (%class.btTriangleShapeEx*)*)(%class.btTriangleShapeEx* %tri1) #8
  %35 = bitcast %class.btTriangleShapeEx* %tri1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %35) #8
  %call18 = call %class.btTriangleShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShapeEx* (%class.btTriangleShapeEx*)*)(%class.btTriangleShapeEx* %tri0) #8
  %36 = bitcast %class.btTriangleShapeEx* %tri0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %36) #8
  ret void
}

define linkonce_odr hidden %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2Ev(%class.btTriangleShapeEx* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !16
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %5 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !16
  %7 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !16
  %8 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !16
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %9 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp11, align 4, !tbaa !16
  %11 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !16
  %12 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !16
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %call15 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %13 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btTriangleShapeEx* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV17btTriangleShapeEx, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %25, align 4, !tbaa !6
  ret %class.btTriangleShapeEx* %this1
}

declare zeroext i1 @_ZN17btTriangleShapeEx25overlap_test_conservativeERKS_(%class.btTriangleShapeEx*, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(104)) #1

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #4

define hidden void @_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEPK24btCollisionObjectWrapperS2_PK22btGImpactMeshShapePartS5_PKii(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactMeshShapePart* %shape0, %class.btGImpactMeshShapePart* %shape1, i32* %pairs, i32 %pair_count) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %shape1.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %pairs.addr = alloca i32*, align 4
  %pair_count.addr = alloca i32, align 4
  %orgtrans0 = alloca %class.btTransform, align 4
  %orgtrans1 = alloca %class.btTransform, align 4
  %ptri0 = alloca %class.btPrimitiveTriangle, align 4
  %ptri1 = alloca %class.btPrimitiveTriangle, align 4
  %contact_data = alloca %struct.GIM_TRIANGLE_CONTACT, align 4
  %pair_pointer = alloca i32*, align 4
  %j = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactMeshShapePart* %shape0, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  store %class.btGImpactMeshShapePart* %shape1, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  store i32* %pairs, i32** %pairs.addr, align 4, !tbaa !2
  store i32 %pair_count, i32* %pair_count.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %2 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #8
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %3)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %4 = bitcast %class.btPrimitiveTriangle* %ptri0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %4) #8
  %call5 = call %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* %ptri0)
  %5 = bitcast %class.btPrimitiveTriangle* %ptri1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 72, i8* %5) #8
  %call6 = call %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* %ptri1)
  %6 = bitcast %struct.GIM_TRIANGLE_CONTACT* %contact_data to i8*
  call void @llvm.lifetime.start.p0i8(i64 280, i8* %6) #8
  %call7 = call %struct.GIM_TRIANGLE_CONTACT* @_ZN20GIM_TRIANGLE_CONTACTC2Ev(%struct.GIM_TRIANGLE_CONTACT* %contact_data)
  %7 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %8 = bitcast %class.btGImpactMeshShapePart* %7 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %8, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 28
  %9 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %9(%class.btGImpactMeshShapePart* %7)
  %10 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %11 = bitcast %class.btGImpactMeshShapePart* %10 to void (%class.btGImpactMeshShapePart*)***
  %vtable8 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %11, align 4, !tbaa !6
  %vfn9 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable8, i64 28
  %12 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn9, align 4
  call void %12(%class.btGImpactMeshShapePart* %10)
  %13 = bitcast i32** %pair_pointer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32*, i32** %pairs.addr, align 4, !tbaa !2
  store i32* %14, i32** %pair_pointer, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end20, %entry
  %15 = load i32, i32* %pair_count.addr, align 4, !tbaa !25
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %pair_count.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %while.body, label %while.end21

while.body:                                       ; preds = %while.cond
  %16 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %17 = load i32, i32* %16, align 4, !tbaa !25
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %17, i32* %m_triface0, align 4, !tbaa !12
  %18 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i32, i32* %18, i32 1
  %19 = load i32, i32* %add.ptr, align 4, !tbaa !25
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %19, i32* %m_triface1, align 4, !tbaa !14
  %20 = load i32*, i32** %pair_pointer, align 4, !tbaa !2
  %add.ptr10 = getelementptr inbounds i32, i32* %20, i32 2
  store i32* %add.ptr10, i32** %pair_pointer, align 4, !tbaa !2
  %21 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %22 = bitcast %class.btGImpactMeshShapePart* %21 to %class.btGImpactShapeInterface*
  %m_triface011 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %23 = load i32, i32* %m_triface011, align 4, !tbaa !12
  call void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %22, i32 %23, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %ptri0)
  %24 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %25 = bitcast %class.btGImpactMeshShapePart* %24 to %class.btGImpactShapeInterface*
  %m_triface112 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %26 = load i32, i32* %m_triface112, align 4, !tbaa !14
  call void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %25, i32 %26, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %ptri1)
  call void @_ZN19btPrimitiveTriangle14applyTransformERK11btTransform(%class.btPrimitiveTriangle* %ptri0, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans0)
  call void @_ZN19btPrimitiveTriangle14applyTransformERK11btTransform(%class.btPrimitiveTriangle* %ptri1, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans1)
  call void @_ZN19btPrimitiveTriangle13buildTriPlaneEv(%class.btPrimitiveTriangle* %ptri0)
  call void @_ZN19btPrimitiveTriangle13buildTriPlaneEv(%class.btPrimitiveTriangle* %ptri1)
  %call13 = call zeroext i1 @_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_(%class.btPrimitiveTriangle* %ptri0, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %ptri1)
  br i1 %call13, label %if.then, label %if.end20

if.then:                                          ; preds = %while.body
  %call14 = call zeroext i1 @_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT(%class.btPrimitiveTriangle* %ptri0, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %ptri1, %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280) %contact_data)
  br i1 %call14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  %27 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %m_point_count = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contact_data, i32 0, i32 1
  %28 = load i32, i32* %m_point_count, align 4, !tbaa !35
  store i32 %28, i32* %j, align 4, !tbaa !25
  br label %while.cond16

while.cond16:                                     ; preds = %while.body19, %if.then15
  %29 = load i32, i32* %j, align 4, !tbaa !25
  %dec17 = add nsw i32 %29, -1
  store i32 %dec17, i32* %j, align 4, !tbaa !25
  %tobool18 = icmp ne i32 %29, 0
  br i1 %tobool18, label %while.body19, label %while.end

while.body19:                                     ; preds = %while.cond16
  %30 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %31 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contact_data, i32 0, i32 3
  %32 = load i32, i32* %j, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 %32
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contact_data, i32 0, i32 2
  %33 = bitcast %class.btVector4* %m_separating_normal to %class.btVector3*
  %m_penetration_depth = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %contact_data, i32 0, i32 0
  %34 = load float, float* %m_penetration_depth, align 4, !tbaa !38
  %fneg = fneg float %34
  call void @_ZN27btGImpactCollisionAlgorithm15addContactPointEPK24btCollisionObjectWrapperS2_RK9btVector3S5_f(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %30, %struct.btCollisionObjectWrapper* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %33, float %fneg)
  br label %while.cond16

while.end:                                        ; preds = %while.cond16
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %if.end

if.end:                                           ; preds = %while.end, %if.then
  br label %if.end20

if.end20:                                         ; preds = %if.end, %while.body
  br label %while.cond

while.end21:                                      ; preds = %while.cond
  %36 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %37 = bitcast %class.btGImpactMeshShapePart* %36 to void (%class.btGImpactMeshShapePart*)***
  %vtable22 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %37, align 4, !tbaa !6
  %vfn23 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable22, i64 29
  %38 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn23, align 4
  call void %38(%class.btGImpactMeshShapePart* %36)
  %39 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape1.addr, align 4, !tbaa !2
  %40 = bitcast %class.btGImpactMeshShapePart* %39 to void (%class.btGImpactMeshShapePart*)***
  %vtable24 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %40, align 4, !tbaa !6
  %vfn25 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable24, i64 29
  %41 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn25, align 4
  call void %41(%class.btGImpactMeshShapePart* %39)
  %42 = bitcast i32** %pair_pointer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast %struct.GIM_TRIANGLE_CONTACT* %contact_data to i8*
  call void @llvm.lifetime.end.p0i8(i64 280, i8* %43) #8
  %44 = bitcast %class.btPrimitiveTriangle* %ptri1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %44) #8
  %45 = bitcast %class.btPrimitiveTriangle* %ptri0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 72, i8* %45) #8
  %46 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %46) #8
  %47 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %47) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btPrimitiveTriangle*, align 4
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  store %class.btPrimitiveTriangle* %this1, %class.btPrimitiveTriangle** %retval, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %call2 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %m_plane)
  %m_margin = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 2
  store float 0x3F847AE140000000, float* %m_margin, align 4, !tbaa !41
  %0 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %retval, align 4
  ret %class.btPrimitiveTriangle* %0
}

define linkonce_odr hidden %struct.GIM_TRIANGLE_CONTACT* @_ZN20GIM_TRIANGLE_CONTACTC2Ev(%struct.GIM_TRIANGLE_CONTACT* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  %this.addr = alloca %struct.GIM_TRIANGLE_CONTACT*, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %this.addr, align 4
  store %struct.GIM_TRIANGLE_CONTACT* %this1, %struct.GIM_TRIANGLE_CONTACT** %retval, align 4
  %m_separating_normal = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 2
  %call = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %m_separating_normal)
  %m_points = getelementptr inbounds %struct.GIM_TRIANGLE_CONTACT, %struct.GIM_TRIANGLE_CONTACT* %this1, i32 0, i32 3
  %array.begin = getelementptr inbounds [16 x %class.btVector3], [16 x %class.btVector3]* %m_points, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 16
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %struct.GIM_TRIANGLE_CONTACT*, %struct.GIM_TRIANGLE_CONTACT** %retval, align 4
  ret %struct.GIM_TRIANGLE_CONTACT* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %this, i32 %index, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btPrimitiveTriangle*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  store %class.btPrimitiveTriangle* %triangle, %class.btPrimitiveTriangle** %triangle.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)***
  %vtable = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)**, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*** %0, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vtable, i64 21
  %1 = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call = call %class.btPrimitiveManagerBase* %1(%class.btGImpactShapeInterface* %this1)
  %2 = load i32, i32* %index.addr, align 4, !tbaa !25
  %3 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4, !tbaa !2
  %4 = bitcast %class.btPrimitiveManagerBase* %call to void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)***
  %vtable2 = load void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*** %4, align 4, !tbaa !6
  %vfn3 = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)** %vtable2, i64 5
  %5 = load void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)** %vfn3, align 4
  call void %5(%class.btPrimitiveManagerBase* %call, i32 %2, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %3)
  ret void
}

define linkonce_odr hidden void @_ZN19btPrimitiveTriangle14applyTransformERK11btTransform(%class.btPrimitiveTriangle* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #0 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_vertices2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 0
  %2 = bitcast %class.btVector3* %arrayidx3 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_vertices5 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices5, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btTransform* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices7 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices7, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx8 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !39
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #8
  %10 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  %11 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_vertices10 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices10, i32 0, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btTransform* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %m_vertices12 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 2
  %12 = bitcast %class.btVector3* %arrayidx13 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !39
  %14 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN19btPrimitiveTriangle13buildTriPlaneEv(%class.btPrimitiveTriangle* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 1
  %m_vertices2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3)
  %2 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %m_vertices5 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices5, i32 0, i32 2
  %m_vertices7 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices7, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %3 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normal)
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %normal)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 2
  %5 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_vertices16 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 0
  %call18 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx17, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call18, float* %ref.tmp15, align 4, !tbaa !16
  call void @_ZN9btVector48setValueERKfS1_S1_S1_(%class.btVector4* %m_plane, float* nonnull align 4 dereferenceable(4) %arrayidx10, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %6 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  ret void
}

declare zeroext i1 @_ZN19btPrimitiveTriangle25overlap_test_conservativeERKS_(%class.btPrimitiveTriangle*, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72)) #1

declare zeroext i1 @_ZN19btPrimitiveTriangle35find_triangle_collision_clip_methodERS_R20GIM_TRIANGLE_CONTACT(%class.btPrimitiveTriangle*, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72), %struct.GIM_TRIANGLE_CONTACT* nonnull align 4 dereferenceable(280)) #1

define hidden void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfaceS5_(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface* %shape1) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btGImpactShapeInterface*, align 4
  %meshshape0 = alloca %class.btGImpactMeshShape*, align 4
  %meshshape1 = alloca %class.btGImpactMeshShape*, align 4
  %orgtrans0 = alloca %class.btTransform, align 4
  %orgtrans1 = alloca %class.btTransform, align 4
  %pairset = alloca %class.btPairSet, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %shapepart0 = alloca %class.btGImpactMeshShapePart*, align 4
  %shapepart1 = alloca %class.btGImpactMeshShapePart*, align 4
  %retriever0 = alloca %class.GIM_ShapeRetriever, align 4
  %retriever1 = alloca %class.GIM_ShapeRetriever, align 4
  %child_has_transform0 = alloca i8, align 1
  %child_has_transform1 = alloca i8, align 1
  %i = alloca i32, align 4
  %pair = alloca %struct.GIM_PAIR*, align 4
  %colshape0 = alloca %class.btCollisionShape*, align 4
  %colshape1 = alloca %class.btCollisionShape*, align 4
  %tr0 = alloca %class.btTransform, align 4
  %tr1 = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp72 = alloca %class.btTransform, align 4
  %ref.tmp80 = alloca %class.btTransform, align 4
  %ref.tmp81 = alloca %class.btTransform, align 4
  %ob0 = alloca %struct.btCollisionObjectWrapper, align 4
  %ob1 = alloca %struct.btCollisionObjectWrapper, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape1, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %1 = bitcast %class.btGImpactShapeInterface* %0 to i32 (%class.btGImpactShapeInterface*)***
  %vtable = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable, i64 20
  %2 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call = call i32 %2(%class.btGImpactShapeInterface* %0)
  %cmp = icmp eq i32 %call, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btGImpactMeshShape** %meshshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %5 = bitcast %class.btGImpactShapeInterface* %4 to %class.btGImpactMeshShape*
  store %class.btGImpactMeshShape* %5, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %6 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %call2 = call i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %6)
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  store i32 %call2, i32* %m_part0, align 4, !tbaa !13
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %m_part03 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %7 = load i32, i32* %m_part03, align 4, !tbaa !13
  %dec = add nsw i32 %7, -1
  store i32 %dec, i32* %m_part03, align 4, !tbaa !13
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %10 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %m_part04 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %11 = load i32, i32* %m_part04, align 4, !tbaa !13
  %call5 = call %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %10, i32 %11)
  %12 = bitcast %class.btGImpactMeshShapePart* %call5 to %class.btGImpactShapeInterface*
  %13 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfaceS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9, %class.btGImpactShapeInterface* %12, %class.btGImpactShapeInterface* %13)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %14 = bitcast %class.btGImpactMeshShape** %meshshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  br label %cleanup.cont

if.end:                                           ; preds = %entry
  %15 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %16 = bitcast %class.btGImpactShapeInterface* %15 to i32 (%class.btGImpactShapeInterface*)***
  %vtable6 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %16, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable6, i64 20
  %17 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn7, align 4
  %call8 = call i32 %17(%class.btGImpactShapeInterface* %15)
  %cmp9 = icmp eq i32 %call8, 2
  br i1 %cmp9, label %if.then10, label %if.end20

if.then10:                                        ; preds = %if.end
  %18 = bitcast %class.btGImpactMeshShape** %meshshape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  %19 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %20 = bitcast %class.btGImpactShapeInterface* %19 to %class.btGImpactMeshShape*
  store %class.btGImpactMeshShape* %20, %class.btGImpactMeshShape** %meshshape1, align 4, !tbaa !2
  %21 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape1, align 4, !tbaa !2
  %call11 = call i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %21)
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  store i32 %call11, i32* %m_part1, align 4, !tbaa !15
  br label %while.cond12

while.cond12:                                     ; preds = %while.body16, %if.then10
  %m_part113 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %22 = load i32, i32* %m_part113, align 4, !tbaa !15
  %dec14 = add nsw i32 %22, -1
  store i32 %dec14, i32* %m_part113, align 4, !tbaa !15
  %tobool15 = icmp ne i32 %22, 0
  br i1 %tobool15, label %while.body16, label %while.end19

while.body16:                                     ; preds = %while.cond12
  %23 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %24 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %25 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %26 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape1, align 4, !tbaa !2
  %m_part117 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %27 = load i32, i32* %m_part117, align 4, !tbaa !15
  %call18 = call %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %26, i32 %27)
  %28 = bitcast %class.btGImpactMeshShapePart* %call18 to %class.btGImpactShapeInterface*
  call void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfaceS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %23, %struct.btCollisionObjectWrapper* %24, %class.btGImpactShapeInterface* %25, %class.btGImpactShapeInterface* %28)
  br label %while.cond12

while.end19:                                      ; preds = %while.cond12
  %29 = bitcast %class.btGImpactMeshShape** %meshshape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  br label %cleanup.cont

if.end20:                                         ; preds = %if.end
  %30 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %30) #8
  %31 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %31)
  %call22 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call21)
  %32 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %32) #8
  %33 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %33)
  %call24 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call23)
  %34 = bitcast %class.btPairSet* %pairset to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %34) #8
  %call25 = call %class.btPairSet* @_ZN9btPairSetC2Ev(%class.btPairSet* %pairset)
  %35 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %36 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm29gimpact_vs_gimpact_find_pairsERK11btTransformS2_PK23btGImpactShapeInterfaceS5_R9btPairSet(%class.btGImpactCollisionAlgorithm* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans1, %class.btGImpactShapeInterface* %35, %class.btGImpactShapeInterface* %36, %class.btPairSet* nonnull align 4 dereferenceable(17) %pairset)
  %37 = bitcast %class.btPairSet* %pairset to %class.btAlignedObjectArray.0*
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %37)
  %cmp27 = icmp eq i32 %call26, 0
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.end20
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %if.end20
  %38 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %39 = bitcast %class.btGImpactShapeInterface* %38 to i32 (%class.btGImpactShapeInterface*)***
  %vtable30 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %39, align 4, !tbaa !6
  %vfn31 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable30, i64 20
  %40 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn31, align 4
  %call32 = call i32 %40(%class.btGImpactShapeInterface* %38)
  %cmp33 = icmp eq i32 %call32, 1
  br i1 %cmp33, label %land.lhs.true, label %if.end41

land.lhs.true:                                    ; preds = %if.end29
  %41 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %42 = bitcast %class.btGImpactShapeInterface* %41 to i32 (%class.btGImpactShapeInterface*)***
  %vtable34 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %42, align 4, !tbaa !6
  %vfn35 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable34, i64 20
  %43 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn35, align 4
  %call36 = call i32 %43(%class.btGImpactShapeInterface* %41)
  %cmp37 = icmp eq i32 %call36, 1
  br i1 %cmp37, label %if.then38, label %if.end41

if.then38:                                        ; preds = %land.lhs.true
  %44 = bitcast %class.btGImpactMeshShapePart** %shapepart0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #8
  %45 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %46 = bitcast %class.btGImpactShapeInterface* %45 to %class.btGImpactMeshShapePart*
  store %class.btGImpactMeshShapePart* %46, %class.btGImpactMeshShapePart** %shapepart0, align 4, !tbaa !2
  %47 = bitcast %class.btGImpactMeshShapePart** %shapepart1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  %48 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %49 = bitcast %class.btGImpactShapeInterface* %48 to %class.btGImpactMeshShapePart*
  store %class.btGImpactMeshShapePart* %49, %class.btGImpactMeshShapePart** %shapepart1, align 4, !tbaa !2
  %50 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %51 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %52 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shapepart0, align 4, !tbaa !2
  %53 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shapepart1, align 4, !tbaa !2
  %54 = bitcast %class.btPairSet* %pairset to %class.btAlignedObjectArray.0*
  %call39 = call nonnull align 4 dereferenceable(8) %struct.GIM_PAIR* @_ZN20btAlignedObjectArrayI8GIM_PAIREixEi(%class.btAlignedObjectArray.0* %54, i32 0)
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %call39, i32 0, i32 0
  %55 = bitcast %class.btPairSet* %pairset to %class.btAlignedObjectArray.0*
  %call40 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %55)
  call void @_ZN27btGImpactCollisionAlgorithm21collide_sat_trianglesEPK24btCollisionObjectWrapperS2_PK22btGImpactMeshShapePartS5_PKii(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %50, %struct.btCollisionObjectWrapper* %51, %class.btGImpactMeshShapePart* %52, %class.btGImpactMeshShapePart* %53, i32* %m_index1, i32 %call40)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %56 = bitcast %class.btGImpactMeshShapePart** %shapepart1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  %57 = bitcast %class.btGImpactMeshShapePart** %shapepart0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  br label %cleanup

if.end41:                                         ; preds = %land.lhs.true, %if.end29
  %58 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %59 = bitcast %class.btGImpactShapeInterface* %58 to void (%class.btGImpactShapeInterface*)***
  %vtable42 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %59, align 4, !tbaa !6
  %vfn43 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable42, i64 28
  %60 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn43, align 4
  call void %60(%class.btGImpactShapeInterface* %58)
  %61 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %62 = bitcast %class.btGImpactShapeInterface* %61 to void (%class.btGImpactShapeInterface*)***
  %vtable44 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %62, align 4, !tbaa !6
  %vfn45 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable44, i64 28
  %63 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn45, align 4
  call void %63(%class.btGImpactShapeInterface* %61)
  %64 = bitcast %class.GIM_ShapeRetriever* %retriever0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 296, i8* %64) #8
  %65 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call46 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverC2EPK23btGImpactShapeInterface(%class.GIM_ShapeRetriever* %retriever0, %class.btGImpactShapeInterface* %65)
  %66 = bitcast %class.GIM_ShapeRetriever* %retriever1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 296, i8* %66) #8
  %67 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %call47 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverC2EPK23btGImpactShapeInterface(%class.GIM_ShapeRetriever* %retriever1, %class.btGImpactShapeInterface* %67)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %child_has_transform0) #8
  %68 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %69 = bitcast %class.btGImpactShapeInterface* %68 to i1 (%class.btGImpactShapeInterface*)***
  %vtable48 = load i1 (%class.btGImpactShapeInterface*)**, i1 (%class.btGImpactShapeInterface*)*** %69, align 4, !tbaa !6
  %vfn49 = getelementptr inbounds i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vtable48, i64 23
  %70 = load i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vfn49, align 4
  %call50 = call zeroext i1 %70(%class.btGImpactShapeInterface* %68)
  %frombool = zext i1 %call50 to i8
  store i8 %frombool, i8* %child_has_transform0, align 1, !tbaa !43
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %child_has_transform1) #8
  %71 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %72 = bitcast %class.btGImpactShapeInterface* %71 to i1 (%class.btGImpactShapeInterface*)***
  %vtable51 = load i1 (%class.btGImpactShapeInterface*)**, i1 (%class.btGImpactShapeInterface*)*** %72, align 4, !tbaa !6
  %vfn52 = getelementptr inbounds i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vtable51, i64 23
  %73 = load i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vfn52, align 4
  %call53 = call zeroext i1 %73(%class.btGImpactShapeInterface* %71)
  %frombool54 = zext i1 %call53 to i8
  store i8 %frombool54, i8* %child_has_transform1, align 1, !tbaa !43
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #8
  %75 = bitcast %class.btPairSet* %pairset to %class.btAlignedObjectArray.0*
  %call55 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %75)
  store i32 %call55, i32* %i, align 4, !tbaa !25
  br label %while.cond56

while.cond56:                                     ; preds = %if.end86, %if.end41
  %76 = load i32, i32* %i, align 4, !tbaa !25
  %dec57 = add nsw i32 %76, -1
  store i32 %dec57, i32* %i, align 4, !tbaa !25
  %tobool58 = icmp ne i32 %76, 0
  br i1 %tobool58, label %while.body59, label %while.end95

while.body59:                                     ; preds = %while.cond56
  %77 = bitcast %struct.GIM_PAIR** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #8
  %78 = bitcast %class.btPairSet* %pairset to %class.btAlignedObjectArray.0*
  %79 = load i32, i32* %i, align 4, !tbaa !25
  %call60 = call nonnull align 4 dereferenceable(8) %struct.GIM_PAIR* @_ZN20btAlignedObjectArrayI8GIM_PAIREixEi(%class.btAlignedObjectArray.0* %78, i32 %79)
  store %struct.GIM_PAIR* %call60, %struct.GIM_PAIR** %pair, align 4, !tbaa !2
  %80 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %pair, align 4, !tbaa !2
  %m_index161 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %80, i32 0, i32 0
  %81 = load i32, i32* %m_index161, align 4, !tbaa !44
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %81, i32* %m_triface0, align 4, !tbaa !12
  %82 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %pair, align 4, !tbaa !2
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %82, i32 0, i32 1
  %83 = load i32, i32* %m_index2, align 4, !tbaa !46
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %83, i32* %m_triface1, align 4, !tbaa !14
  %84 = bitcast %class.btCollisionShape** %colshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #8
  %m_triface062 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %85 = load i32, i32* %m_triface062, align 4, !tbaa !12
  %call63 = call %class.btCollisionShape* @_ZN18GIM_ShapeRetriever13getChildShapeEi(%class.GIM_ShapeRetriever* %retriever0, i32 %85)
  store %class.btCollisionShape* %call63, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %86 = bitcast %class.btCollisionShape** %colshape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #8
  %m_triface164 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %87 = load i32, i32* %m_triface164, align 4, !tbaa !14
  %call65 = call %class.btCollisionShape* @_ZN18GIM_ShapeRetriever13getChildShapeEi(%class.GIM_ShapeRetriever* %retriever1, i32 %87)
  store %class.btCollisionShape* %call65, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  %88 = bitcast %class.btTransform* %tr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %88) #8
  %89 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call66 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %89)
  %call67 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr0, %class.btTransform* nonnull align 4 dereferenceable(64) %call66)
  %90 = bitcast %class.btTransform* %tr1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %90) #8
  %91 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call68 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %91)
  %call69 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr1, %class.btTransform* nonnull align 4 dereferenceable(64) %call68)
  %92 = load i8, i8* %child_has_transform0, align 1, !tbaa !43, !range !47
  %tobool70 = trunc i8 %92 to i1
  br i1 %tobool70, label %if.then71, label %if.end77

if.then71:                                        ; preds = %while.body59
  %93 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %93) #8
  %94 = bitcast %class.btTransform* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %94) #8
  %95 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %m_triface073 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %96 = load i32, i32* %m_triface073, align 4, !tbaa !12
  %97 = bitcast %class.btGImpactShapeInterface* %95 to void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)***
  %vtable74 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)**, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*** %97, align 4, !tbaa !6
  %vfn75 = getelementptr inbounds void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vtable74, i64 33
  %98 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vfn75, align 4
  call void %98(%class.btTransform* sret align 4 %ref.tmp72, %class.btGImpactShapeInterface* %95, i32 %96)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp72)
  %call76 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %99 = bitcast %class.btTransform* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %99) #8
  %100 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %100) #8
  br label %if.end77

if.end77:                                         ; preds = %if.then71, %while.body59
  %101 = load i8, i8* %child_has_transform1, align 1, !tbaa !43, !range !47
  %tobool78 = trunc i8 %101 to i1
  br i1 %tobool78, label %if.then79, label %if.end86

if.then79:                                        ; preds = %if.end77
  %102 = bitcast %class.btTransform* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %102) #8
  %103 = bitcast %class.btTransform* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %103) #8
  %104 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %m_triface182 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %105 = load i32, i32* %m_triface182, align 4, !tbaa !14
  %106 = bitcast %class.btGImpactShapeInterface* %104 to void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)***
  %vtable83 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)**, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*** %106, align 4, !tbaa !6
  %vfn84 = getelementptr inbounds void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vtable83, i64 33
  %107 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vfn84, align 4
  call void %107(%class.btTransform* sret align 4 %ref.tmp81, %class.btGImpactShapeInterface* %104, i32 %105)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp80, %class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp81)
  %call85 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr1, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp80)
  %108 = bitcast %class.btTransform* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %108) #8
  %109 = bitcast %class.btTransform* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %109) #8
  br label %if.end86

if.end86:                                         ; preds = %if.then79, %if.end77
  %110 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %110) #8
  %111 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %112 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %113 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call87 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %113)
  %m_part088 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %114 = load i32, i32* %m_part088, align 4, !tbaa !13
  %m_triface089 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %115 = load i32, i32* %m_triface089, align 4, !tbaa !12
  %call90 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %111, %class.btCollisionShape* %112, %class.btCollisionObject* %call87, %class.btTransform* nonnull align 4 dereferenceable(64) %tr0, i32 %114, i32 %115)
  %116 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %116) #8
  %117 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %118 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  %119 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call91 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %119)
  %m_part192 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  %120 = load i32, i32* %m_part192, align 4, !tbaa !15
  %m_triface193 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  %121 = load i32, i32* %m_triface193, align 4, !tbaa !14
  %call94 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob1, %struct.btCollisionObjectWrapper* %117, %class.btCollisionShape* %118, %class.btCollisionObject* %call91, %class.btTransform* nonnull align 4 dereferenceable(64) %tr1, i32 %120, i32 %121)
  %122 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %123 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm26convex_vs_convex_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %ob1, %class.btCollisionShape* %122, %class.btCollisionShape* %123)
  %124 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %124) #8
  %125 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %125) #8
  %126 = bitcast %class.btTransform* %tr1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %126) #8
  %127 = bitcast %class.btTransform* %tr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %127) #8
  %128 = bitcast %class.btCollisionShape** %colshape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  %129 = bitcast %class.btCollisionShape** %colshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  %130 = bitcast %struct.GIM_PAIR** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #8
  br label %while.cond56

while.end95:                                      ; preds = %while.cond56
  %131 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %132 = bitcast %class.btGImpactShapeInterface* %131 to void (%class.btGImpactShapeInterface*)***
  %vtable96 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %132, align 4, !tbaa !6
  %vfn97 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable96, i64 29
  %133 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn97, align 4
  call void %133(%class.btGImpactShapeInterface* %131)
  %134 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape1.addr, align 4, !tbaa !2
  %135 = bitcast %class.btGImpactShapeInterface* %134 to void (%class.btGImpactShapeInterface*)***
  %vtable98 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %135, align 4, !tbaa !6
  %vfn99 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable98, i64 29
  %136 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn99, align 4
  call void %136(%class.btGImpactShapeInterface* %134)
  %137 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %child_has_transform1) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %child_has_transform0) #8
  %call100 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverD2Ev(%class.GIM_ShapeRetriever* %retriever1) #8
  %138 = bitcast %class.GIM_ShapeRetriever* %retriever1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 296, i8* %138) #8
  %call101 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverD2Ev(%class.GIM_ShapeRetriever* %retriever0) #8
  %139 = bitcast %class.GIM_ShapeRetriever* %retriever0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 296, i8* %139) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end95, %if.then38, %if.then28
  %call102 = call %class.btPairSet* bitcast (%class.btAlignedObjectArray.0* (%class.btAlignedObjectArray.0*)* @_ZN20btAlignedObjectArrayI8GIM_PAIRED2Ev to %class.btPairSet* (%class.btPairSet*)*)(%class.btPairSet* %pairset) #8
  %140 = bitcast %class.btPairSet* %pairset to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %140) #8
  %141 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %141) #8
  %142 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %142) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %while.end, %while.end19, %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

define linkonce_odr hidden i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  ret i32 %call
}

define linkonce_odr hidden %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %index.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts, i32 %0)
  %1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call, align 4, !tbaa !2
  ret %class.btGImpactMeshShapePart* %1
}

define linkonce_odr hidden %class.btPairSet* @_ZN9btPairSetC2Ev(%class.btPairSet* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btPairSet*, align 4
  store %class.btPairSet* %this, %class.btPairSet** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPairSet*, %class.btPairSet** %this.addr, align 4
  %0 = bitcast %class.btPairSet* %this1 to %class.btAlignedObjectArray.0*
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI8GIM_PAIREC2Ev(%class.btAlignedObjectArray.0* %0)
  %1 = bitcast %class.btPairSet* %this1 to %class.btAlignedObjectArray.0*
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.0* %1, i32 32)
  ret %class.btPairSet* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !48
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.GIM_PAIR* @_ZN20btAlignedObjectArrayI8GIM_PAIREixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %0, i32 %1
  ret %struct.GIM_PAIR* %arrayidx
}

define linkonce_odr hidden %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverC2EPK23btGImpactShapeInterface(%class.GIM_ShapeRetriever* returned %this, %class.btGImpactShapeInterface* %gim_shape) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.GIM_ShapeRetriever*, align 4
  %this.addr = alloca %class.GIM_ShapeRetriever*, align 4
  %gim_shape.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.GIM_ShapeRetriever* %this, %class.GIM_ShapeRetriever** %this.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %gim_shape, %class.btGImpactShapeInterface** %gim_shape.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %this.addr, align 4
  store %class.GIM_ShapeRetriever* %this1, %class.GIM_ShapeRetriever** %retval, align 4
  %m_trishape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 1
  %call = call %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2Ev(%class.btTriangleShapeEx* %m_trishape)
  %m_tetrashape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 2
  %call2 = call %class.btTetrahedronShapeEx* @_ZN20btTetrahedronShapeExC2Ev(%class.btTetrahedronShapeEx* %m_tetrashape)
  %m_child_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 3
  %call3 = call %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %m_child_retriever) #8
  %m_tri_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 4
  %call4 = call %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* @_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %m_tri_retriever) #8
  %m_tetra_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 5
  %call5 = call %"class.GIM_ShapeRetriever::TetraShapeRetriever"* @_ZN18GIM_ShapeRetriever19TetraShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* %m_tetra_retriever) #8
  %0 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gim_shape.addr, align 4, !tbaa !2
  %m_gim_shape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 0
  store %class.btGImpactShapeInterface* %0, %class.btGImpactShapeInterface** %m_gim_shape, align 4, !tbaa !52
  %m_gim_shape6 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 0
  %1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %m_gim_shape6, align 4, !tbaa !52
  %2 = bitcast %class.btGImpactShapeInterface* %1 to i1 (%class.btGImpactShapeInterface*)***
  %vtable = load i1 (%class.btGImpactShapeInterface*)**, i1 (%class.btGImpactShapeInterface*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vtable, i64 24
  %3 = load i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call7 = call zeroext i1 %3(%class.btGImpactShapeInterface* %1)
  br i1 %call7, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_tri_retriever8 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 4
  %4 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %m_tri_retriever8 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_current_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 6
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %4, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %m_current_retriever, align 4, !tbaa !59
  br label %if.end19

if.else:                                          ; preds = %entry
  %m_gim_shape9 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 0
  %5 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %m_gim_shape9, align 4, !tbaa !52
  %6 = bitcast %class.btGImpactShapeInterface* %5 to i1 (%class.btGImpactShapeInterface*)***
  %vtable10 = load i1 (%class.btGImpactShapeInterface*)**, i1 (%class.btGImpactShapeInterface*)*** %6, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vtable10, i64 25
  %7 = load i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vfn11, align 4
  %call12 = call zeroext i1 %7(%class.btGImpactShapeInterface* %5)
  br i1 %call12, label %if.then13, label %if.else16

if.then13:                                        ; preds = %if.else
  %m_tetra_retriever14 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 5
  %8 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %m_tetra_retriever14 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_current_retriever15 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 6
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %8, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %m_current_retriever15, align 4, !tbaa !59
  br label %if.end

if.else16:                                        ; preds = %if.else
  %m_child_retriever17 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 3
  %m_current_retriever18 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 6
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %m_child_retriever17, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %m_current_retriever18, align 4, !tbaa !59
  br label %if.end

if.end:                                           ; preds = %if.else16, %if.then13
  br label %if.end19

if.end19:                                         ; preds = %if.end, %if.then
  %m_current_retriever20 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 6
  %9 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %m_current_retriever20, align 4, !tbaa !59
  %m_parent = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %9, i32 0, i32 1
  store %class.GIM_ShapeRetriever* %this1, %class.GIM_ShapeRetriever** %m_parent, align 4, !tbaa !60
  %10 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %retval, align 4
  ret %class.GIM_ShapeRetriever* %10
}

define linkonce_odr hidden %class.btCollisionShape* @_ZN18GIM_ShapeRetriever13getChildShapeEi(%class.GIM_ShapeRetriever* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.GIM_ShapeRetriever*, align 4
  %index.addr = alloca i32, align 4
  store %class.GIM_ShapeRetriever* %this, %class.GIM_ShapeRetriever** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %this.addr, align 4
  %m_current_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 6
  %0 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %m_current_retriever, align 4, !tbaa !59
  %1 = load i32, i32* %index.addr, align 4, !tbaa !25
  %2 = bitcast %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0 to %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)***
  %vtable = load %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)**, %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)*, %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)** %vtable, i64 0
  %3 = load %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)*, %class.btCollisionShape* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*, i32)** %vfn, align 4
  %call = call %class.btCollisionShape* %3(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0, i32 %1)
  ret %class.btCollisionShape* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #8
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverD2Ev(%class.GIM_ShapeRetriever* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.GIM_ShapeRetriever*, align 4
  store %class.GIM_ShapeRetriever* %this, %class.GIM_ShapeRetriever** %this.addr, align 4, !tbaa !2
  %this1 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %this.addr, align 4
  %m_tetra_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 5
  %call = call %"class.GIM_ShapeRetriever::TetraShapeRetriever"* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to %"class.GIM_ShapeRetriever::TetraShapeRetriever"* (%"class.GIM_ShapeRetriever::TetraShapeRetriever"*)*)(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* %m_tetra_retriever) #8
  %m_tri_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 4
  %call2 = call %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* (%"class.GIM_ShapeRetriever::TriangleShapeRetriever"*)*)(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %m_tri_retriever) #8
  %m_child_retriever = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 3
  %call3 = call %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %m_child_retriever) #8
  %m_tetrashape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 2
  %call4 = call %class.btTetrahedronShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTetrahedronShapeEx* (%class.btTetrahedronShapeEx*)*)(%class.btTetrahedronShapeEx* %m_tetrashape) #8
  %m_trishape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %this1, i32 0, i32 1
  %call5 = call %class.btTriangleShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShapeEx* (%class.btTriangleShapeEx*)*)(%class.btTriangleShapeEx* %m_trishape) #8
  ret %class.GIM_ShapeRetriever* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI8GIM_PAIRED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define hidden void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactShapeInterface* %shape0, %class.btCollisionShape* %shape1, i1 zeroext %swapped) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btCollisionShape*, align 4
  %swapped.addr = alloca i8, align 1
  %meshshape0 = alloca %class.btGImpactMeshShape*, align 4
  %part = alloca i32*, align 4
  %shapepart = alloca %class.btGImpactMeshShapePart*, align 4
  %planeshape = alloca %class.btStaticPlaneShape*, align 4
  %compoundshape = alloca %class.btCompoundShape*, align 4
  %concaveshape = alloca %class.btConcaveShape*, align 4
  %orgtrans0 = alloca %class.btTransform, align 4
  %orgtrans1 = alloca %class.btTransform, align 4
  %collided_results = alloca %class.btAlignedObjectArray.4, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %retriever0 = alloca %class.GIM_ShapeRetriever, align 4
  %child_has_transform0 = alloca i8, align 1
  %i = alloca i32, align 4
  %child_index = alloca i32, align 4
  %colshape0 = alloca %class.btCollisionShape*, align 4
  %tr0 = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp54 = alloca %class.btTransform, align 4
  %ob0 = alloca %struct.btCollisionObjectWrapper, align 4
  %prevObj0 = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape1, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %frombool = zext i1 %swapped to i8
  store i8 %frombool, i8* %swapped.addr, align 1, !tbaa !43
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %1 = bitcast %class.btGImpactShapeInterface* %0 to i32 (%class.btGImpactShapeInterface*)***
  %vtable = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable, i64 20
  %2 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call = call i32 %2(%class.btGImpactShapeInterface* %0)
  %cmp = icmp eq i32 %call, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btGImpactMeshShape** %meshshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %5 = bitcast %class.btGImpactShapeInterface* %4 to %class.btGImpactMeshShape*
  store %class.btGImpactMeshShape* %5, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %6 = bitcast i32** %part to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %m_part1, %cond.true ], [ %m_part0, %cond.false ]
  store i32* %cond-lvalue, i32** %part, align 4, !tbaa !2
  %8 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %call2 = call i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %8)
  %9 = load i32*, i32** %part, align 4, !tbaa !2
  store i32 %call2, i32* %9, align 4, !tbaa !25
  br label %while.cond

while.cond:                                       ; preds = %while.body, %cond.end
  %10 = load i32*, i32** %part, align 4, !tbaa !2
  %11 = load i32, i32* %10, align 4, !tbaa !25
  %dec = add nsw i32 %11, -1
  store i32 %dec, i32* %10, align 4, !tbaa !25
  %tobool3 = icmp ne i32 %11, 0
  br i1 %tobool3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %14 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %meshshape0, align 4, !tbaa !2
  %15 = load i32*, i32** %part, align 4, !tbaa !2
  %16 = load i32, i32* %15, align 4, !tbaa !25
  %call4 = call %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %14, i32 %16)
  %17 = bitcast %class.btGImpactMeshShapePart* %call4 to %class.btGImpactShapeInterface*
  %18 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %19 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool5 = trunc i8 %19 to i1
  call void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %12, %struct.btCollisionObjectWrapper* %13, %class.btGImpactShapeInterface* %17, %class.btCollisionShape* %18, i1 zeroext %tobool5)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %20 = bitcast i32** %part to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast %class.btGImpactMeshShape** %meshshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %cleanup.cont

if.end:                                           ; preds = %entry
  %22 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %23 = bitcast %class.btGImpactShapeInterface* %22 to i32 (%class.btGImpactShapeInterface*)***
  %vtable6 = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %23, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable6, i64 20
  %24 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn7, align 4
  %call8 = call i32 %24(%class.btGImpactShapeInterface* %22)
  %cmp9 = icmp eq i32 %call8, 1
  br i1 %cmp9, label %land.lhs.true, label %if.end14

land.lhs.true:                                    ; preds = %if.end
  %25 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %call10 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %25)
  %cmp11 = icmp eq i32 %call10, 28
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %land.lhs.true
  %26 = bitcast %class.btGImpactMeshShapePart** %shapepart to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %28 = bitcast %class.btGImpactShapeInterface* %27 to %class.btGImpactMeshShapePart*
  store %class.btGImpactMeshShapePart* %28, %class.btGImpactMeshShapePart** %shapepart, align 4, !tbaa !2
  %29 = bitcast %class.btStaticPlaneShape** %planeshape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %31 = bitcast %class.btCollisionShape* %30 to %class.btStaticPlaneShape*
  store %class.btStaticPlaneShape* %31, %class.btStaticPlaneShape** %planeshape, align 4, !tbaa !2
  %32 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %33 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %34 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shapepart, align 4, !tbaa !2
  %35 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %planeshape, align 4, !tbaa !2
  %36 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool13 = trunc i8 %36 to i1
  call void @_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEPK24btCollisionObjectWrapperS2_PK22btGImpactMeshShapePartPK18btStaticPlaneShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %32, %struct.btCollisionObjectWrapper* %33, %class.btGImpactMeshShapePart* %34, %class.btStaticPlaneShape* %35, i1 zeroext %tobool13)
  %37 = bitcast %class.btStaticPlaneShape** %planeshape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast %class.btGImpactMeshShapePart** %shapepart to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %cleanup.cont

if.end14:                                         ; preds = %land.lhs.true, %if.end
  %39 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %call15 = call zeroext i1 @_ZNK16btCollisionShape10isCompoundEv(%class.btCollisionShape* %39)
  br i1 %call15, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.end14
  %40 = bitcast %class.btCompoundShape** %compoundshape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  %41 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %42 = bitcast %class.btCollisionShape* %41 to %class.btCompoundShape*
  store %class.btCompoundShape* %42, %class.btCompoundShape** %compoundshape, align 4, !tbaa !2
  %43 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %44 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %45 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %46 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundshape, align 4, !tbaa !2
  %47 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool17 = trunc i8 %47 to i1
  call void @_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK15btCompoundShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %43, %struct.btCollisionObjectWrapper* %44, %class.btGImpactShapeInterface* %45, %class.btCompoundShape* %46, i1 zeroext %tobool17)
  %48 = bitcast %class.btCompoundShape** %compoundshape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  br label %cleanup.cont

if.else:                                          ; preds = %if.end14
  %49 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %call18 = call zeroext i1 @_ZNK16btCollisionShape9isConcaveEv(%class.btCollisionShape* %49)
  br i1 %call18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.else
  %50 = bitcast %class.btConcaveShape** %concaveshape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  %51 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %52 = bitcast %class.btCollisionShape* %51 to %class.btConcaveShape*
  store %class.btConcaveShape* %52, %class.btConcaveShape** %concaveshape, align 4, !tbaa !2
  %53 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %54 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %55 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %56 = load %class.btConcaveShape*, %class.btConcaveShape** %concaveshape, align 4, !tbaa !2
  %57 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool20 = trunc i8 %57 to i1
  call void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK14btConcaveShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %53, %struct.btCollisionObjectWrapper* %54, %class.btGImpactShapeInterface* %55, %class.btConcaveShape* %56, i1 zeroext %tobool20)
  %58 = bitcast %class.btConcaveShape** %concaveshape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  br label %cleanup.cont

if.end21:                                         ; preds = %if.else
  br label %if.end22

if.end22:                                         ; preds = %if.end21
  %59 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %59) #8
  %60 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %60)
  %call24 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call23)
  %61 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %61) #8
  %62 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %62)
  %call26 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call25)
  %63 = bitcast %class.btAlignedObjectArray.4* %collided_results to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %63) #8
  %call27 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.4* %collided_results)
  %64 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %65 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm27gimpact_vs_shape_find_pairsERK11btTransformS2_PK23btGImpactShapeInterfacePK16btCollisionShapeR20btAlignedObjectArrayIiE(%class.btGImpactCollisionAlgorithm* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans1, %class.btGImpactShapeInterface* %64, %class.btCollisionShape* %65, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %collided_results)
  %call28 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %collided_results)
  %cmp29 = icmp eq i32 %call28, 0
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end22
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end31:                                         ; preds = %if.end22
  %66 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %67 = bitcast %class.btGImpactShapeInterface* %66 to void (%class.btGImpactShapeInterface*)***
  %vtable32 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %67, align 4, !tbaa !6
  %vfn33 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable32, i64 28
  %68 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn33, align 4
  call void %68(%class.btGImpactShapeInterface* %66)
  %69 = bitcast %class.GIM_ShapeRetriever* %retriever0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 296, i8* %69) #8
  %70 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %call34 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverC2EPK23btGImpactShapeInterface(%class.GIM_ShapeRetriever* %retriever0, %class.btGImpactShapeInterface* %70)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %child_has_transform0) #8
  %71 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %72 = bitcast %class.btGImpactShapeInterface* %71 to i1 (%class.btGImpactShapeInterface*)***
  %vtable35 = load i1 (%class.btGImpactShapeInterface*)**, i1 (%class.btGImpactShapeInterface*)*** %72, align 4, !tbaa !6
  %vfn36 = getelementptr inbounds i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vtable35, i64 23
  %73 = load i1 (%class.btGImpactShapeInterface*)*, i1 (%class.btGImpactShapeInterface*)** %vfn36, align 4
  %call37 = call zeroext i1 %73(%class.btGImpactShapeInterface* %71)
  %frombool38 = zext i1 %call37 to i8
  store i8 %frombool38, i8* %child_has_transform0, align 1, !tbaa !43
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #8
  %call39 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %collided_results)
  store i32 %call39, i32* %i, align 4, !tbaa !25
  br label %while.cond40

while.cond40:                                     ; preds = %if.end78, %if.end31
  %75 = load i32, i32* %i, align 4, !tbaa !25
  %dec41 = add nsw i32 %75, -1
  store i32 %dec41, i32* %i, align 4, !tbaa !25
  %tobool42 = icmp ne i32 %75, 0
  br i1 %tobool42, label %while.body43, label %while.end80

while.body43:                                     ; preds = %while.cond40
  %76 = bitcast i32* %child_index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  %77 = load i32, i32* %i, align 4, !tbaa !25
  %call44 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %collided_results, i32 %77)
  %78 = load i32, i32* %call44, align 4, !tbaa !25
  store i32 %78, i32* %child_index, align 4, !tbaa !25
  %79 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool45 = trunc i8 %79 to i1
  br i1 %tobool45, label %if.then46, label %if.else47

if.then46:                                        ; preds = %while.body43
  %80 = load i32, i32* %child_index, align 4, !tbaa !25
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %80, i32* %m_triface1, align 4, !tbaa !14
  br label %if.end48

if.else47:                                        ; preds = %while.body43
  %81 = load i32, i32* %child_index, align 4, !tbaa !25
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %81, i32* %m_triface0, align 4, !tbaa !12
  br label %if.end48

if.end48:                                         ; preds = %if.else47, %if.then46
  %82 = bitcast %class.btCollisionShape** %colshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #8
  %83 = load i32, i32* %child_index, align 4, !tbaa !25
  %call49 = call %class.btCollisionShape* @_ZN18GIM_ShapeRetriever13getChildShapeEi(%class.GIM_ShapeRetriever* %retriever0, i32 %83)
  store %class.btCollisionShape* %call49, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %84 = bitcast %class.btTransform* %tr0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %84) #8
  %85 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call50 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %85)
  %call51 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %tr0, %class.btTransform* nonnull align 4 dereferenceable(64) %call50)
  %86 = load i8, i8* %child_has_transform0, align 1, !tbaa !43, !range !47
  %tobool52 = trunc i8 %86 to i1
  br i1 %tobool52, label %if.then53, label %if.end58

if.then53:                                        ; preds = %if.end48
  %87 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %87) #8
  %88 = bitcast %class.btTransform* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %88) #8
  %89 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %90 = load i32, i32* %child_index, align 4, !tbaa !25
  %91 = bitcast %class.btGImpactShapeInterface* %89 to void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)***
  %vtable55 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)**, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*** %91, align 4, !tbaa !6
  %vfn56 = getelementptr inbounds void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vtable55, i64 33
  %92 = load void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)*, void (%class.btTransform*, %class.btGImpactShapeInterface*, i32)** %vfn56, align 4
  call void %92(%class.btTransform* sret align 4 %ref.tmp54, %class.btGImpactShapeInterface* %89, i32 %90)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp54)
  %call57 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %tr0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %93 = bitcast %class.btTransform* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %93) #8
  %94 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %94) #8
  br label %if.end58

if.end58:                                         ; preds = %if.then53, %if.end48
  %95 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %95) #8
  %96 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %97 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %98 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call59 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %98)
  %99 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call60 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %99)
  %m_part061 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  %100 = load i32, i32* %m_part061, align 4, !tbaa !13
  %m_triface062 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  %101 = load i32, i32* %m_triface062, align 4, !tbaa !12
  %call63 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %96, %class.btCollisionShape* %97, %class.btCollisionObject* %call59, %class.btTransform* nonnull align 4 dereferenceable(64) %call60, i32 %100, i32 %101)
  %102 = bitcast %struct.btCollisionObjectWrapper** %prevObj0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #8
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %103 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %call64 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %103)
  store %struct.btCollisionObjectWrapper* %call64, %struct.btCollisionObjectWrapper** %prevObj0, align 4, !tbaa !2
  %m_resultOut65 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %104 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut65, align 4, !tbaa !18
  %call66 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %104)
  %call67 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %call66)
  %call68 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %ob0)
  %cmp69 = icmp eq %class.btCollisionObject* %call67, %call68
  br i1 %cmp69, label %if.then70, label %if.else72

if.then70:                                        ; preds = %if.end58
  %m_resultOut71 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %105 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut71, align 4, !tbaa !18
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %105, %struct.btCollisionObjectWrapper* %ob0)
  br label %if.end74

if.else72:                                        ; preds = %if.end58
  %m_resultOut73 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %106 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut73, align 4, !tbaa !18
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %106, %struct.btCollisionObjectWrapper* %ob0)
  br label %if.end74

if.end74:                                         ; preds = %if.else72, %if.then70
  %107 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool75 = trunc i8 %107 to i1
  br i1 %tobool75, label %if.then76, label %if.else77

if.then76:                                        ; preds = %if.end74
  %108 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %109 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  %110 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %108, %struct.btCollisionObjectWrapper* %ob0, %class.btCollisionShape* %109, %class.btCollisionShape* %110)
  br label %if.end78

if.else77:                                        ; preds = %if.end74
  %111 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %112 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape0, align 4, !tbaa !2
  %113 = load %class.btCollisionShape*, %class.btCollisionShape** %shape1.addr, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm24shape_vs_shape_collisionEPK24btCollisionObjectWrapperS2_PK16btCollisionShapeS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %ob0, %struct.btCollisionObjectWrapper* %111, %class.btCollisionShape* %112, %class.btCollisionShape* %113)
  br label %if.end78

if.end78:                                         ; preds = %if.else77, %if.then76
  %m_resultOut79 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %114 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut79, align 4, !tbaa !18
  %115 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %prevObj0, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %114, %struct.btCollisionObjectWrapper* %115)
  %116 = bitcast %struct.btCollisionObjectWrapper** %prevObj0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #8
  %117 = bitcast %struct.btCollisionObjectWrapper* %ob0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %117) #8
  %118 = bitcast %class.btTransform* %tr0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %118) #8
  %119 = bitcast %class.btCollisionShape** %colshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #8
  %120 = bitcast i32* %child_index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #8
  br label %while.cond40

while.end80:                                      ; preds = %while.cond40
  %121 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %122 = bitcast %class.btGImpactShapeInterface* %121 to void (%class.btGImpactShapeInterface*)***
  %vtable81 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %122, align 4, !tbaa !6
  %vfn82 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable81, i64 29
  %123 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn82, align 4
  call void %123(%class.btGImpactShapeInterface* %121)
  %124 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %child_has_transform0) #8
  %call83 = call %class.GIM_ShapeRetriever* @_ZN18GIM_ShapeRetrieverD2Ev(%class.GIM_ShapeRetriever* %retriever0) #8
  %125 = bitcast %class.GIM_ShapeRetriever* %retriever0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 296, i8* %125) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end80, %if.then30
  %call84 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.4* %collided_results) #8
  %126 = bitcast %class.btAlignedObjectArray.4* %collided_results to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %126) #8
  %127 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %127) #8
  %128 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %128) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %while.end, %if.then12, %if.then16, %if.then19, %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !61
  ret i32 %0
}

define hidden void @_ZN27btGImpactCollisionAlgorithm37gimpacttrimeshpart_vs_plane_collisionEPK24btCollisionObjectWrapperS2_PK22btGImpactMeshShapePartPK18btStaticPlaneShapeb(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactMeshShapePart* %shape0, %class.btStaticPlaneShape* %shape1, i1 zeroext %swapped) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %shape1.addr = alloca %class.btStaticPlaneShape*, align 4
  %swapped.addr = alloca i8, align 1
  %orgtrans0 = alloca %class.btTransform, align 4
  %orgtrans1 = alloca %class.btTransform, align 4
  %planeshape = alloca %class.btPlaneShape*, align 4
  %plane = alloca %class.btVector4, align 4
  %tribox = alloca %class.btAABB, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %margin = alloca float, align 4
  %vertex = alloca %class.btVector3, align 4
  %vi = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %distance = alloca float, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactMeshShapePart* %shape0, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  store %class.btStaticPlaneShape* %shape1, %class.btStaticPlaneShape** %shape1.addr, align 4, !tbaa !2
  %frombool = zext i1 %swapped to i8
  store i8 %frombool, i8* %swapped.addr, align 1, !tbaa !43
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %2 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %2) #8
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %3)
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %4 = bitcast %class.btPlaneShape** %planeshape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btStaticPlaneShape*, %class.btStaticPlaneShape** %shape1.addr, align 4, !tbaa !2
  %6 = bitcast %class.btStaticPlaneShape* %5 to %class.btPlaneShape*
  store %class.btPlaneShape* %6, %class.btPlaneShape** %planeshape, align 4, !tbaa !2
  %7 = bitcast %class.btVector4* %plane to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %call5 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %plane)
  %8 = load %class.btPlaneShape*, %class.btPlaneShape** %planeshape, align 4, !tbaa !2
  call void @_ZNK12btPlaneShape30get_plane_equation_transformedERK11btTransformR9btVector4(%class.btPlaneShape* %8, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans1, %class.btVector4* nonnull align 4 dereferenceable(16) %plane)
  %9 = bitcast %class.btAABB* %tribox to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %9) #8
  %call6 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %tribox)
  %10 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %11 = bitcast %class.btGImpactMeshShapePart* %10 to %class.btGImpactShapeInterface*
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %tribox, i32 0, i32 0
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %tribox, i32 0, i32 1
  %12 = bitcast %class.btGImpactShapeInterface* %11 to void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %12, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %13 = load void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %13(%class.btGImpactShapeInterface* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %orgtrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max)
  %14 = load %class.btPlaneShape*, %class.btPlaneShape** %planeshape, align 4, !tbaa !2
  %15 = bitcast %class.btPlaneShape* %14 to %class.btConcaveShape*
  %16 = bitcast %class.btConcaveShape* %15 to float (%class.btConcaveShape*)***
  %vtable7 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %16, align 4, !tbaa !6
  %vfn8 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable7, i64 12
  %17 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn8, align 4
  %call9 = call float %17(%class.btConcaveShape* %15)
  call void @_ZN6btAABB16increment_marginEf(%class.btAABB* %tribox, float %call9)
  %call10 = call i32 @_ZNK6btAABB14plane_classifyERK9btVector4(%class.btAABB* %tribox, %class.btVector4* nonnull align 4 dereferenceable(16) %plane)
  %cmp = icmp ne i32 %call10, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %18 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %19 = bitcast %class.btGImpactMeshShapePart* %18 to void (%class.btGImpactMeshShapePart*)***
  %vtable11 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %19, align 4, !tbaa !6
  %vfn12 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable11, i64 28
  %20 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn12, align 4
  call void %20(%class.btGImpactMeshShapePart* %18)
  %21 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %23 = bitcast %class.btGImpactMeshShapePart* %22 to float (%class.btGImpactMeshShapePart*)***
  %vtable13 = load float (%class.btGImpactMeshShapePart*)**, float (%class.btGImpactMeshShapePart*)*** %23, align 4, !tbaa !6
  %vfn14 = getelementptr inbounds float (%class.btGImpactMeshShapePart*)*, float (%class.btGImpactMeshShapePart*)** %vtable13, i64 12
  %24 = load float (%class.btGImpactMeshShapePart*)*, float (%class.btGImpactMeshShapePart*)** %vfn14, align 4
  %call15 = call float %24(%class.btGImpactMeshShapePart* %22)
  %25 = load %class.btPlaneShape*, %class.btPlaneShape** %planeshape, align 4, !tbaa !2
  %26 = bitcast %class.btPlaneShape* %25 to %class.btConcaveShape*
  %27 = bitcast %class.btConcaveShape* %26 to float (%class.btConcaveShape*)***
  %vtable16 = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %27, align 4, !tbaa !6
  %vfn17 = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable16, i64 12
  %28 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn17, align 4
  %call18 = call float %28(%class.btConcaveShape* %26)
  %add = fadd float %call15, %call18
  store float %add, float* %margin, align 4, !tbaa !16
  %29 = bitcast %class.btVector3* %vertex to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #8
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vertex)
  %30 = bitcast i32* %vi to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %call20 = call i32 @_ZNK22btGImpactMeshShapePart14getVertexCountEv(%class.btGImpactMeshShapePart* %31)
  store i32 %call20, i32* %vi, align 4, !tbaa !25
  br label %while.cond

while.cond:                                       ; preds = %if.end30, %if.end
  %32 = load i32, i32* %vi, align 4, !tbaa !25
  %dec = add nsw i32 %32, -1
  store i32 %dec, i32* %vi, align 4, !tbaa !25
  %tobool = icmp ne i32 %32, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %33 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %34 = load i32, i32* %vi, align 4, !tbaa !25
  call void @_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3(%class.btGImpactMeshShapePart* %33, i32 %34, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex)
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #8
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %orgtrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex)
  %36 = bitcast %class.btVector3* %vertex to i8*
  %37 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false), !tbaa.struct !39
  %38 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #8
  %39 = bitcast float* %distance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %class.btVector4* %plane to %class.btVector3*
  %call21 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vertex, %class.btVector3* nonnull align 4 dereferenceable(16) %40)
  %41 = bitcast %class.btVector4* %plane to %class.btVector3*
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %41)
  %arrayidx = getelementptr inbounds float, float* %call22, i32 3
  %42 = load float, float* %arrayidx, align 4, !tbaa !16
  %sub = fsub float %call21, %42
  %43 = load float, float* %margin, align 4, !tbaa !16
  %sub23 = fsub float %sub, %43
  store float %sub23, float* %distance, align 4, !tbaa !16
  %44 = load float, float* %distance, align 4, !tbaa !16
  %conv = fpext float %44 to double
  %cmp24 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp24, label %if.then25, label %if.end30

if.then25:                                        ; preds = %while.body
  %45 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool26 = trunc i8 %45 to i1
  br i1 %tobool26, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.then25
  %46 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %47 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %48 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btVector4* %plane to %class.btVector3*
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  %50 = load float, float* %distance, align 4, !tbaa !16
  call void @_ZN27btGImpactCollisionAlgorithm15addContactPointEPK24btCollisionObjectWrapperS2_RK9btVector3S5_f(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %46, %struct.btCollisionObjectWrapper* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, float %50)
  %51 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #8
  br label %if.end29

if.else:                                          ; preds = %if.then25
  %52 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %53 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %54 = bitcast %class.btVector4* %plane to %class.btVector3*
  %55 = load float, float* %distance, align 4, !tbaa !16
  call void @_ZN27btGImpactCollisionAlgorithm15addContactPointEPK24btCollisionObjectWrapperS2_RK9btVector3S5_f(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %52, %struct.btCollisionObjectWrapper* %53, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex, %class.btVector3* nonnull align 4 dereferenceable(16) %54, float %55)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.then27
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %while.body
  %56 = bitcast float* %distance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %57 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %shape0.addr, align 4, !tbaa !2
  %58 = bitcast %class.btGImpactMeshShapePart* %57 to void (%class.btGImpactMeshShapePart*)***
  %vtable31 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %58, align 4, !tbaa !6
  %vfn32 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable31, i64 29
  %59 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn32, align 4
  call void %59(%class.btGImpactMeshShapePart* %57)
  %60 = bitcast i32* %vi to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #8
  %61 = bitcast %class.btVector3* %vertex to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #8
  %62 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %63 = bitcast %class.btAABB* %tribox to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %63) #8
  %64 = bitcast %class.btVector4* %plane to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast %class.btPlaneShape** %planeshape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %66 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %66) #8
  %67 = bitcast %class.btTransform* %orgtrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %67) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isCompoundEv(%class.btCollisionShape* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %call)
  ret i1 %call2
}

define hidden void @_ZN27btGImpactCollisionAlgorithm24gimpact_vs_compoundshapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK15btCompoundShapeb(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactShapeInterface* %shape0, %class.btCompoundShape* %shape1, i1 zeroext %swapped) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btCompoundShape*, align 4
  %swapped.addr = alloca i8, align 1
  %orgtrans1 = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %colshape1 = alloca %class.btCollisionShape*, align 4
  %childtrans1 = alloca %class.btTransform, align 4
  %ob1 = alloca %struct.btCollisionObjectWrapper, align 4
  %tmp = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btCompoundShape* %shape1, %class.btCompoundShape** %shape1.addr, align 4, !tbaa !2
  %frombool = zext i1 %swapped to i8
  store i8 %frombool, i8* %swapped.addr, align 1, !tbaa !43
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btCompoundShape*, %class.btCompoundShape** %shape1.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %3)
  store i32 %call3, i32* %i, align 4, !tbaa !25
  br label %while.cond

while.cond:                                       ; preds = %if.end27, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !25
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %i, align 4, !tbaa !25
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = bitcast %class.btCollisionShape** %colshape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btCompoundShape*, %class.btCompoundShape** %shape1.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %call4 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %6, i32 %7)
  store %class.btCollisionShape* %call4, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  %8 = bitcast %class.btTransform* %childtrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %8) #8
  %9 = load %class.btCompoundShape*, %class.btCompoundShape** %shape1.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %9, i32 %10)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %childtrans1, %class.btTransform* %orgtrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call5)
  %11 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %11) #8
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %13 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call6 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %14)
  %15 = load i32, i32* %i, align 4, !tbaa !25
  %call7 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob1, %struct.btCollisionObjectWrapper* %12, %class.btCollisionShape* %13, %class.btCollisionObject* %call6, %class.btTransform* nonnull align 4 dereferenceable(64) %childtrans1, i32 -1, i32 %15)
  %16 = bitcast %struct.btCollisionObjectWrapper** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %17 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %call8 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %17)
  %call9 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %call8)
  %call10 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %ob1)
  %cmp = icmp eq %class.btCollisionObject* %call9, %call10
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %m_resultOut11 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %18 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut11, align 4, !tbaa !18
  %call12 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %18)
  store %struct.btCollisionObjectWrapper* %call12, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %m_resultOut13 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %19 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut13, align 4, !tbaa !18
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %19, %struct.btCollisionObjectWrapper* %ob1)
  br label %if.end

if.else:                                          ; preds = %while.body
  %m_resultOut14 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %20 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut14, align 4, !tbaa !18
  %call15 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %20)
  store %struct.btCollisionObjectWrapper* %call15, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %m_resultOut16 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %21 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut16, align 4, !tbaa !18
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %21, %struct.btCollisionObjectWrapper* %ob1)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %22 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %23 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %24 = load %class.btCollisionShape*, %class.btCollisionShape** %colshape1, align 4, !tbaa !2
  %25 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool17 = trunc i8 %25 to i1
  call void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %22, %struct.btCollisionObjectWrapper* %ob1, %class.btGImpactShapeInterface* %23, %class.btCollisionShape* %24, i1 zeroext %tobool17)
  %m_resultOut18 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %26 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut18, align 4, !tbaa !18
  %call19 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %26)
  %call20 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %call19)
  %call21 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %ob1)
  %cmp22 = icmp eq %class.btCollisionObject* %call20, %call21
  br i1 %cmp22, label %if.then23, label %if.else25

if.then23:                                        ; preds = %if.end
  %m_resultOut24 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %27 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut24, align 4, !tbaa !18
  %28 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %27, %struct.btCollisionObjectWrapper* %28)
  br label %if.end27

if.else25:                                        ; preds = %if.end
  %m_resultOut26 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %29 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut26, align 4, !tbaa !18
  %30 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %29, %struct.btCollisionObjectWrapper* %30)
  br label %if.end27

if.end27:                                         ; preds = %if.else25, %if.then23
  %31 = bitcast %struct.btCollisionObjectWrapper** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast %struct.btCollisionObjectWrapper* %ob1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %32) #8
  %33 = bitcast %class.btTransform* %childtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %33) #8
  %34 = bitcast %class.btCollisionShape** %colshape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %35 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btTransform* %orgtrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape9isConcaveEv(%class.btCollisionShape* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %call)
  ret i1 %call2
}

define hidden void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_concaveEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK14btConcaveShapeb(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btGImpactShapeInterface* %shape0, %class.btConcaveShape* %shape1, i1 zeroext %swapped) #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape0.addr = alloca %class.btGImpactShapeInterface*, align 4
  %shape1.addr = alloca %class.btConcaveShape*, align 4
  %swapped.addr = alloca i8, align 1
  %tricallback = alloca %class.btGImpactTriangleCallback, align 4
  %gimpactInConcaveSpace = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp8 = alloca %class.btTransform, align 4
  %minAABB = alloca %class.btVector3, align 4
  %maxAABB = alloca %class.btVector3, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %class.btGImpactShapeInterface* %shape0, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  store %class.btConcaveShape* %shape1, %class.btConcaveShape** %shape1.addr, align 4, !tbaa !2
  %frombool = zext i1 %swapped to i8
  store i8 %frombool, i8* %swapped.addr, align 1, !tbaa !43
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btGImpactTriangleCallback* %tricallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %0) #8
  %call = call %class.btGImpactTriangleCallback* @_ZN25btGImpactTriangleCallbackC2Ev(%class.btGImpactTriangleCallback* %tricallback) #8
  %algorithm = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 1
  store %class.btGImpactCollisionAlgorithm* %this1, %class.btGImpactCollisionAlgorithm** %algorithm, align 4, !tbaa !63
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %body0Wrap2 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %body0Wrap2, align 4, !tbaa !65
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %body1Wrap3 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %body1Wrap3, align 4, !tbaa !66
  %3 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %gimpactshape0 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 4
  store %class.btGImpactShapeInterface* %3, %class.btGImpactShapeInterface** %gimpactshape0, align 4, !tbaa !67
  %4 = load i8, i8* %swapped.addr, align 1, !tbaa !43, !range !47
  %tobool = trunc i8 %4 to i1
  %swapped4 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 5
  %frombool5 = zext i1 %tobool to i8
  store i8 %frombool5, i8* %swapped4, align 4, !tbaa !68
  %5 = load %class.btConcaveShape*, %class.btConcaveShape** %shape1.addr, align 4, !tbaa !2
  %6 = bitcast %class.btConcaveShape* %5 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %7 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call6 = call float %7(%class.btConcaveShape* %5)
  %margin = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %tricallback, i32 0, i32 6
  store float %call6, float* %margin, align 4, !tbaa !69
  %8 = bitcast %class.btTransform* %gimpactInConcaveSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %8) #8
  %call7 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %gimpactInConcaveSpace)
  %9 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %9) #8
  %10 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %10) #8
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %11)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp8, %class.btTransform* %call9)
  %12 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %12)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %ref.tmp8, %class.btTransform* nonnull align 4 dereferenceable(64) %call10)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %gimpactInConcaveSpace, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %13 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %13) #8
  %14 = bitcast %class.btTransform* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %14) #8
  %15 = bitcast %class.btVector3* %minAABB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minAABB)
  %16 = bitcast %class.btVector3* %maxAABB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %maxAABB)
  %17 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %shape0.addr, align 4, !tbaa !2
  %18 = bitcast %class.btGImpactShapeInterface* %17 to void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable14 = load void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %18, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable14, i64 2
  %19 = load void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn15, align 4
  call void %19(%class.btGImpactShapeInterface* %17, %class.btTransform* nonnull align 4 dereferenceable(64) %gimpactInConcaveSpace, %class.btVector3* nonnull align 4 dereferenceable(16) %minAABB, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAABB)
  %20 = load %class.btConcaveShape*, %class.btConcaveShape** %shape1.addr, align 4, !tbaa !2
  %21 = bitcast %class.btGImpactTriangleCallback* %tricallback to %class.btTriangleCallback*
  %22 = bitcast %class.btConcaveShape* %20 to void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable16 = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %22, align 4, !tbaa !6
  %vfn17 = getelementptr inbounds void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable16, i64 16
  %23 = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn17, align 4
  call void %23(%class.btConcaveShape* %20, %class.btTriangleCallback* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %minAABB, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAABB)
  %24 = bitcast %class.btVector3* %maxAABB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btVector3* %minAABB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btTransform* %gimpactInConcaveSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %26) #8
  %call18 = call %class.btGImpactTriangleCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %class.btGImpactTriangleCallback* (%class.btGImpactTriangleCallback*)*)(%class.btGImpactTriangleCallback* %tricallback) #8
  %27 = bitcast %class.btGImpactTriangleCallback* %tricallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %27) #8
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !34
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !30
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !70
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj0Wrap) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !70
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj1Wrap) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !72
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getNumChildShapesEv(%class.btCompoundShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.12* %m_children)
  ret i32 %call
}

define linkonce_odr hidden %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.12* %m_children, i32 %0)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !73
  ret %class.btCollisionShape* %1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.12* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !72
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %0)
  ret %class.btVector4* %this1
}

define linkonce_odr hidden void @_ZNK12btPlaneShape30get_plane_equation_transformedERK11btTransformR9btVector4(%class.btPlaneShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector4* nonnull align 4 dereferenceable(16) %equation) #0 comdat {
entry:
  %this.addr = alloca %class.btPlaneShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %equation.addr = alloca %class.btVector4*, align 4
  store %class.btPlaneShape* %this, %class.btPlaneShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector4* %equation, %class.btVector4** %equation.addr, align 4, !tbaa !2
  %this1 = load %class.btPlaneShape*, %class.btPlaneShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call, i32 0)
  %1 = bitcast %class.btPlaneShape* %this1 to %class.btStaticPlaneShape*
  %m_planeNormal = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %1, i32 0, i32 3
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_planeNormal)
  %2 = load %class.btVector4*, %class.btVector4** %equation.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector4* %2 to %class.btVector3*
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %3)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  store float %call3, float* %arrayidx, align 4, !tbaa !16
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call5, i32 1)
  %5 = bitcast %class.btPlaneShape* %this1 to %class.btStaticPlaneShape*
  %m_planeNormal7 = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %5, i32 0, i32 3
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_planeNormal7)
  %6 = load %class.btVector4*, %class.btVector4** %equation.addr, align 4, !tbaa !2
  %7 = bitcast %class.btVector4* %6 to %class.btVector3*
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %7)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  store float %call8, float* %arrayidx10, align 4, !tbaa !16
  %8 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %8)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call11, i32 2)
  %9 = bitcast %class.btPlaneShape* %this1 to %class.btStaticPlaneShape*
  %m_planeNormal13 = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %9, i32 0, i32 3
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_planeNormal13)
  %10 = load %class.btVector4*, %class.btVector4** %equation.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector4* %10 to %class.btVector3*
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %call14, float* %arrayidx16, align 4, !tbaa !16
  %12 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %12)
  %13 = bitcast %class.btPlaneShape* %this1 to %class.btStaticPlaneShape*
  %m_planeNormal18 = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %13, i32 0, i32 3
  %call19 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_planeNormal18)
  %14 = bitcast %class.btPlaneShape* %this1 to %class.btStaticPlaneShape*
  %m_planeConstant = getelementptr inbounds %class.btStaticPlaneShape, %class.btStaticPlaneShape* %14, i32 0, i32 4
  %15 = load float, float* %m_planeConstant, align 4, !tbaa !78
  %add = fadd float %call19, %15
  %16 = load %class.btVector4*, %class.btVector4** %equation.addr, align 4, !tbaa !2
  %17 = bitcast %class.btVector4* %16 to %class.btVector3*
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 3
  store float %add, float* %arrayidx21, align 4, !tbaa !16
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN6btAABB16increment_marginEf(%class.btAABB* %this, float %margin) #3 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %margin.addr = alloca float, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !16
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %sub = fsub float %1, %0
  store float %sub, float* %arrayidx, align 4, !tbaa !16
  %2 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  %3 = load float, float* %arrayidx4, align 4, !tbaa !16
  %sub5 = fsub float %3, %2
  store float %sub5, float* %arrayidx4, align 4, !tbaa !16
  %4 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min6 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 2
  %5 = load float, float* %arrayidx8, align 4, !tbaa !16
  %sub9 = fsub float %5, %4
  store float %sub9, float* %arrayidx8, align 4, !tbaa !16
  %6 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  %7 = load float, float* %arrayidx11, align 4, !tbaa !16
  %add = fadd float %7, %6
  store float %add, float* %arrayidx11, align 4, !tbaa !16
  %8 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max12 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max12)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %9 = load float, float* %arrayidx14, align 4, !tbaa !16
  %add15 = fadd float %9, %8
  store float %add15, float* %arrayidx14, align 4, !tbaa !16
  %10 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max16 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max16)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 2
  %11 = load float, float* %arrayidx18, align 4, !tbaa !16
  %add19 = fadd float %11, %10
  store float %add19, float* %arrayidx18, align 4, !tbaa !16
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK6btAABB14plane_classifyERK9btVector4(%class.btAABB* %this, %class.btVector4* nonnull align 4 dereferenceable(16) %plane) #3 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btAABB*, align 4
  %plane.addr = alloca %class.btVector4*, align 4
  %_fmin = alloca float, align 4
  %_fmax = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btVector4* %plane, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = bitcast float* %_fmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast float* %_fmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector4* %2 to %class.btVector3*
  call void @_ZNK6btAABB19projection_intervalERK9btVector3RfS3_(%class.btAABB* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float* nonnull align 4 dereferenceable(4) %_fmin, float* nonnull align 4 dereferenceable(4) %_fmax)
  %4 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector4* %4 to %class.btVector3*
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %5)
  %arrayidx = getelementptr inbounds float, float* %call, i32 3
  %6 = load float, float* %arrayidx, align 4, !tbaa !16
  %7 = load float, float* %_fmax, align 4, !tbaa !16
  %add = fadd float %7, 0x3EB0C6F7A0000000
  %cmp = fcmp ogt float %6, %add
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %8 = load %class.btVector4*, %class.btVector4** %plane.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector4* %8 to %class.btVector3*
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %9)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 3
  %10 = load float, float* %arrayidx3, align 4, !tbaa !16
  %add4 = fadd float %10, 0x3EB0C6F7A0000000
  %11 = load float, float* %_fmin, align 4, !tbaa !16
  %cmp5 = fcmp oge float %add4, %11
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end7:                                          ; preds = %if.end
  store i32 2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end7, %if.then6, %if.then
  %12 = bitcast float* %_fmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %_fmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart14getVertexCountEv(%class.btGImpactMeshShapePart* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %call = call i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3(%class.btGImpactMeshShapePart* %this, i32 %vertex_index, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %vertex_index.addr = alloca i32, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4, !tbaa !2
  store i32 %vertex_index, i32* %vertex_index.addr, align 4, !tbaa !25
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %0 = load i32, i32* %vertex_index.addr, align 4, !tbaa !25
  %1 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !16
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !16
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !16
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !16
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !16
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !16
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !16
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !16
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !16
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btGImpactTriangleCallback* @_ZN25btGImpactTriangleCallbackC2Ev(%class.btGImpactTriangleCallback* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btGImpactTriangleCallback*, align 4
  store %class.btGImpactTriangleCallback* %this, %class.btGImpactTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactTriangleCallback*, %class.btGImpactTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btGImpactTriangleCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #8
  %1 = bitcast %class.btGImpactTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV25btGImpactTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btGImpactTriangleCallback* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #4

define hidden void @_ZN27btGImpactCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btGImpactCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %gimpactshape0 = alloca %class.btGImpactShapeInterface*, align 4
  %gimpactshape1 = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  call void @_ZN27btGImpactCollisionAlgorithm10clearCacheEv(%class.btGImpactCollisionAlgorithm* %this1)
  %0 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  store %class.btManifoldResult* %0, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  %1 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_dispatchInfo = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 4
  store %struct.btDispatcherInfo* %1, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !19
  %2 = bitcast %class.btGImpactShapeInterface** %gimpactshape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast %class.btGImpactShapeInterface** %gimpactshape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %4)
  %call2 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call)
  %cmp = icmp eq i32 %call2, 25
  br i1 %cmp, label %if.then, label %if.else10

if.then:                                          ; preds = %entry
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call3 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %5)
  %6 = bitcast %class.btCollisionShape* %call3 to %class.btGImpactShapeInterface*
  store %class.btGImpactShapeInterface* %6, %class.btGImpactShapeInterface** %gimpactshape0, align 4, !tbaa !2
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call4 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %7)
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call4)
  %cmp6 = icmp eq i32 %call5, 25
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call8 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %8)
  %9 = bitcast %class.btCollisionShape* %call8 to %class.btGImpactShapeInterface*
  store %class.btGImpactShapeInterface* %9, %class.btGImpactShapeInterface** %gimpactshape1, align 4, !tbaa !2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %12 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gimpactshape0, align 4, !tbaa !2
  %13 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gimpactshape1, align 4, !tbaa !2
  call void @_ZN27btGImpactCollisionAlgorithm18gimpact_vs_gimpactEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfaceS5_(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %10, %struct.btCollisionObjectWrapper* %11, %class.btGImpactShapeInterface* %12, %class.btGImpactShapeInterface* %13)
  br label %if.end

if.else:                                          ; preds = %if.then
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %16 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gimpactshape0, align 4, !tbaa !2
  %17 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call9 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %17)
  call void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %14, %struct.btCollisionObjectWrapper* %15, %class.btGImpactShapeInterface* %16, %class.btCollisionShape* %call9, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then7
  br label %if.end18

if.else10:                                        ; preds = %entry
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call11 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %18)
  %call12 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call11)
  %cmp13 = icmp eq i32 %call12, 25
  br i1 %cmp13, label %if.then14, label %if.end17

if.then14:                                        ; preds = %if.else10
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call15 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %19)
  %20 = bitcast %class.btCollisionShape* %call15 to %class.btGImpactShapeInterface*
  store %class.btGImpactShapeInterface* %20, %class.btGImpactShapeInterface** %gimpactshape1, align 4, !tbaa !2
  %21 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %22 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %23 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gimpactshape1, align 4, !tbaa !2
  %24 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call16 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %24)
  call void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %this1, %struct.btCollisionObjectWrapper* %21, %struct.btCollisionObjectWrapper* %22, %class.btGImpactShapeInterface* %23, %class.btCollisionShape* %call16, i1 zeroext true)
  br label %if.end17

if.end17:                                         ; preds = %if.then14, %if.else10
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end
  %25 = bitcast %class.btGImpactShapeInterface** %gimpactshape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btGImpactShapeInterface** %gimpactshape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #7 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !27
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define hidden float @_ZN27btGImpactCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btGImpactCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  ret float 1.000000e+00
}

define hidden void @_ZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcher(%class.btCollisionDispatcher* %dispatcher) #0 {
entry:
  %dispatcher.addr = alloca %class.btCollisionDispatcher*, align 4
  %i = alloca i32, align 4
  store %class.btCollisionDispatcher* %dispatcher, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !80

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf) #8
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call %"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZN27btGImpactCollisionAlgorithm10CreateFuncC2Ev(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf)
  %3 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #8
  call void @__cxa_guard_release(i32* @_ZGVZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf) #8
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %init.end
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %5, 36
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher* %6, i32 25, i32 %7, %struct.btCollisionAlgorithmCreateFunc* bitcast (%"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf to %struct.btCollisionAlgorithmCreateFunc*))
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc4, %for.end
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %cmp2 = icmp slt i32 %9, 36
  br i1 %cmp2, label %for.body3, label %for.end6

for.body3:                                        ; preds = %for.cond1
  %10 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %11 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher* %10, i32 %11, i32 25, %struct.btCollisionAlgorithmCreateFunc* bitcast (%"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf to %struct.btCollisionAlgorithmCreateFunc*))
  br label %for.inc4

for.inc4:                                         ; preds = %for.body3
  %12 = load i32, i32* %i, align 4, !tbaa !25
  %inc5 = add nsw i32 %12, 1
  store i32 %inc5, i32* %i, align 4, !tbaa !25
  br label %for.cond1

for.end6:                                         ; preds = %for.cond1
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #8

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZN27btGImpactCollisionAlgorithm10CreateFuncC2Ev(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN27btGImpactCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this1
}

define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !2
  %call = call %"struct.btGImpactCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btGImpactCollisionAlgorithm::CreateFunc"* (%"struct.btGImpactCollisionAlgorithm::CreateFunc"*)*)(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* @_ZZN27btGImpactCollisionAlgorithm17registerAlgorithmEP21btCollisionDispatcherE12s_gimpact_cf) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #8

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #8

declare void @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher*, i32, i32, %struct.btCollisionAlgorithmCreateFunc*) #1

define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btGImpactCollisionAlgorithm* %this, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.16* %manifoldArray, %class.btAlignedObjectArray.16** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !8
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %manifoldArray.addr, align 4, !tbaa !2
  %m_manifoldPtr2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.16* %1, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm23destroyContactManifoldsEv(%class.btGImpactCollisionAlgorithm* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !8
  %cmp = icmp eq %class.btPersistentManifold* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %1, i32 0, i32 1
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !20
  %m_manifoldPtr2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  %3 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !8
  %4 = bitcast %class.btDispatcher* %2 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %5 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %5(%class.btDispatcher* %2, %class.btPersistentManifold* %3)
  %m_manifoldPtr3 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifoldPtr3, align 4, !tbaa !8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm22destroyConvexAlgorithmEv(%class.btGImpactCollisionAlgorithm* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_convex_algorithm = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  %0 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_convex_algorithm, align 4, !tbaa !11
  %tobool = icmp ne %class.btCollisionAlgorithm* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_convex_algorithm2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_convex_algorithm2, align 4, !tbaa !11
  %2 = bitcast %class.btCollisionAlgorithm* %1 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %3 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call = call %class.btCollisionAlgorithm* %3(%class.btCollisionAlgorithm* %1) #8
  %4 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %4, i32 0, i32 1
  %5 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !20
  %m_convex_algorithm3 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  %6 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_convex_algorithm3, align 4, !tbaa !11
  %7 = bitcast %class.btCollisionAlgorithm* %6 to i8*
  %8 = bitcast %class.btDispatcher* %5 to void (%class.btDispatcher*, i8*)***
  %vtable4 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %8, align 4, !tbaa !6
  %vfn5 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable4, i64 15
  %9 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn5, align 4
  call void %9(%class.btDispatcher* %5, i8* %7)
  %m_convex_algorithm6 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_convex_algorithm6, align 4, !tbaa !11
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm15getLastManifoldEv(%class.btGImpactCollisionAlgorithm* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !8
  ret %class.btPersistentManifold* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btPersistentManifold* @_ZN27btGImpactCollisionAlgorithm18newContactManifoldEPK17btCollisionObjectS2_(%class.btGImpactCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btGImpactCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %0, i32 0, i32 1
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !20
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  %4 = bitcast %class.btDispatcher* %1 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 3
  %5 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call = call %class.btPersistentManifold* %5(%class.btDispatcher* %1, %class.btCollisionObject* %2, %class.btCollisionObject* %3)
  %m_manifoldPtr = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* %call, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !8
  %m_manifoldPtr2 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 2
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !8
  ret %class.btPersistentManifold* %6
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !81
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK21btGImpactQuantizedBvh12getNodeCountEv(%class.btGImpactQuantizedBvh* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 0
  %call = call i32 @_ZNK18btQuantizedBvhTree12getNodeCountEv(%class.btQuantizedBvhTree* %m_box_tree)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhTree12getNodeCountEv(%class.btQuantizedBvhTree* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvhTree*, align 4
  store %class.btQuantizedBvhTree* %this, %class.btQuantizedBvhTree** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvhTree*, %class.btQuantizedBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4, !tbaa !82
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.GIM_PAIR*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %_Val, %struct.GIM_PAIR** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !25
  %1 = load i32, i32* %sz, align 4, !tbaa !25
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !48
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %2, i32 %3
  %4 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.GIM_PAIR*
  %6 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %_Val.addr, align 4, !tbaa !2
  %call5 = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %5, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !48
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !48
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2Eii(%struct.GIM_PAIR* returned %this, i32 %index1, i32 %index2) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4, !tbaa !2
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !25
  store i32 %index2, i32* %index2.addr, align 4, !tbaa !25
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load i32, i32* %index1.addr, align 4, !tbaa !25
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %0, i32* %m_index1, align 4, !tbaa !44
  %1 = load i32, i32* %index2.addr, align 4, !tbaa !25
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %1, i32* %m_index2, align 4, !tbaa !46
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !86
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.GIM_PAIR** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.GIM_PAIR*
  store %struct.GIM_PAIR* %3, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.GIM_PAIR* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %5 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* %5, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !86
  %7 = bitcast %struct.GIM_PAIR** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI8GIM_PAIRE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* returned %this, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %p) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_PAIR*, align 4
  %p.addr = alloca %struct.GIM_PAIR*, align 4
  store %struct.GIM_PAIR* %this, %struct.GIM_PAIR** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %p, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %this1 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %m_index1 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %0, i32 0, i32 0
  %1 = load i32, i32* %m_index1, align 4, !tbaa !44
  %m_index12 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 0
  store i32 %1, i32* %m_index12, align 4, !tbaa !44
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %p.addr, align 4, !tbaa !2
  %m_index2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %2, i32 0, i32 1
  %3 = load i32, i32* %m_index2, align 4, !tbaa !46
  %m_index23 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %this1, i32 0, i32 1
  store i32 %3, i32* %m_index23, align 4, !tbaa !46
  ret %struct.GIM_PAIR* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI8GIM_PAIRE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.GIM_PAIR** null)
  %2 = bitcast %struct.GIM_PAIR* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.GIM_PAIR* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GIM_PAIR*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store %struct.GIM_PAIR* %dest, %struct.GIM_PAIR** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %4, i32 %5
  %6 = bitcast %struct.GIM_PAIR* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.GIM_PAIR*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %8, i32 %9
  %call = call %struct.GIM_PAIR* @_ZN8GIM_PAIRC2ERKS_(%struct.GIM_PAIR* %7, %struct.GIM_PAIR* nonnull align 4 dereferenceable(8) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.GIM_PAIR, %struct.GIM_PAIR* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %tobool = icmp ne %struct.GIM_PAIR* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !87, !range !47
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %m_data4, align 4, !tbaa !51
  call void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.GIM_PAIR* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* null, %struct.GIM_PAIR** %m_data5, align 4, !tbaa !51
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.GIM_PAIR* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.GIM_PAIR** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GIM_PAIR**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store %struct.GIM_PAIR** %hint, %struct.GIM_PAIR*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GIM_PAIR*
  ret %struct.GIM_PAIR* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.GIM_PAIR* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.GIM_PAIR*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.GIM_PAIR* %ptr, %struct.GIM_PAIR** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.GIM_PAIR*, %struct.GIM_PAIR** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.GIM_PAIR* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !16
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !16
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !16
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !16
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !16
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !16
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !16
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !16
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #3 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !39
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !39
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !39
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !16
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !16
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !16
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !16
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !16
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3mLERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  store float %call, float* %ref.tmp, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %call5 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  store float %call5, float* %ref.tmp2, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 0
  %call9 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  store float %call9, float* %ref.tmp6, align 4, !tbaa !16
  %6 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  store float %call13, float* %ref.tmp10, align 4, !tbaa !16
  %8 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16)
  store float %call17, float* %ref.tmp14, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  store float %call21, float* %ref.tmp18, align 4, !tbaa !16
  %12 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  store float %call25, float* %ref.tmp22, align 4, !tbaa !16
  %14 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el27 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el27, i32 0, i32 2
  %call29 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx28)
  store float %call29, float* %ref.tmp26, align 4, !tbaa !16
  %16 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %m_el31 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el31, i32 0, i32 2
  %call33 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx32)
  store float %call33, float* %ref.tmp30, align 4, !tbaa !16
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  %18 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !16
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !16
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !16
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !16
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !16
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !16
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !16
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !16
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !16
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !16
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !16
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !16
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !16
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !16
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !16
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btTriangleShape*, align 4
  %this.addr = alloca %class.btTriangleShape*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store %class.btTriangleShape* %this1, %class.btTriangleShape** %retval, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btTriangleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV15btTriangleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btTriangleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 1, i32* %m_shapeType, align 4, !tbaa !61
  %3 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4, !tbaa !2
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !39
  %6 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !39
  %9 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !39
  %12 = load %class.btTriangleShape*, %class.btTriangleShape** %retval, align 4
  ret %class.btTriangleShape* %12
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17btTriangleShapeExD0Ev(%class.btTriangleShapeEx* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %call = call %class.btTriangleShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShapeEx* (%class.btTriangleShapeEx*)*)(%class.btTriangleShapeEx* %this1) #8
  %0 = bitcast %class.btTriangleShapeEx* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #8
  ret void
}

define linkonce_odr hidden void @_ZNK17btTriangleShapeEx7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShapeEx* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %tv0 = alloca %class.btVector3, align 4
  %tv1 = alloca %class.btVector3, align 4
  %tv2 = alloca %class.btVector3, align 4
  %trianglebox = alloca %class.btAABB, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %0 = bitcast %class.btVector3* %tv0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %tv0, %class.btTransform* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %3 = bitcast %class.btVector3* %tv1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %5 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %5, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %tv1, %class.btTransform* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3)
  %6 = bitcast %class.btVector3* %tv2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %8 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %8, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %tv2, %class.btTransform* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %9 = bitcast %class.btAABB* %trianglebox to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %9) #8
  %10 = bitcast %class.btTriangleShapeEx* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %10, i32 0, i32 3
  %11 = load float, float* %m_collisionMargin, align 4, !tbaa !88
  %call = call %class.btAABB* @_ZN6btAABBC2ERK9btVector3S2_S2_f(%class.btAABB* %trianglebox, %class.btVector3* nonnull align 4 dereferenceable(16) %tv0, %class.btVector3* nonnull align 4 dereferenceable(16) %tv1, %class.btVector3* nonnull align 4 dereferenceable(16) %tv2, float %11)
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %trianglebox, i32 0, i32 0
  %12 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %m_min to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !39
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %trianglebox, i32 0, i32 1
  %15 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %m_max to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !39
  %18 = bitcast %class.btAABB* %trianglebox to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %18) #8
  %19 = bitcast %class.btVector3* %tv2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %tv1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %tv0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #8
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !16
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !16
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !16
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btTriangleShape7getNameEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !16
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !16
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !88
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !88
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !88
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !90
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

define linkonce_odr hidden void @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %dir) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 %call
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  %4 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

define linkonce_odr hidden void @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btTriangleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dir = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !25
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  store %class.btVector3* %arrayidx, %class.btVector3** %dir, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = load %class.btVector3*, %class.btVector3** %dir, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 1
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 %call
  %9 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx9 to i8*
  %12 = bitcast %class.btVector3* %arrayidx8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !39
  %13 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #8
  %14 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 2
}

define linkonce_odr hidden void @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i32, i32* %index.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %3, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape14getNumVerticesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape11getNumEdgesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !25
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %2 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %3 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = load i32, i32* %i.addr, align 4, !tbaa !25
  %add = add nsw i32 %4, 1
  %rem = srem i32 %add, 3
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %6 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable2 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %6, align 4, !tbaa !6
  %vfn3 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable2, i64 27
  %7 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn3, align 4
  call void %7(%class.btTriangleShape* %this1, i32 %rem, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape9getVertexEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %vert) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %vert.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  store %class.btVector3* %vert, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape12getNumPlanesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 1
}

define linkonce_odr hidden void @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport, i32 %i) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !25
  %1 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %3 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 31
  %4 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK15btTriangleShape8isInsideERK9btVector3f(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, float %tolerance) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShape*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  %tolerance.addr = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %planeconst = alloca float, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %edgeNormal = alloca %class.btVector3, align 4
  %dist9 = alloca float, align 4
  %edgeConst = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4, !tbaa !2
  store float %tolerance, float* %tolerance.addr, align 4, !tbaa !16
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %1 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call2, float* %dist, align 4, !tbaa !16
  %3 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call3, float* %planeconst, align 4, !tbaa !16
  %4 = load float, float* %planeconst, align 4, !tbaa !16
  %5 = load float, float* %dist, align 4, !tbaa !16
  %sub = fsub float %5, %4
  store float %sub, float* %dist, align 4, !tbaa !16
  %6 = load float, float* %dist, align 4, !tbaa !16
  %7 = load float, float* %tolerance.addr, align 4, !tbaa !16
  %fneg = fneg float %7
  %cmp = fcmp oge float %6, %fneg
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %dist, align 4, !tbaa !16
  %9 = load float, float* %tolerance.addr, align 4, !tbaa !16
  %cmp4 = fcmp ole float %8, %9
  br i1 %cmp4, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !25
  %cmp5 = icmp slt i32 %11, 3
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %13 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %14 = load i32, i32* %i, align 4, !tbaa !25
  %15 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %15, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 26
  %16 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %16(%class.btTriangleShape* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  %17 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %pa)
  %18 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #8
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edgeNormal, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edgeNormal)
  %19 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call10, float* %dist9, align 4, !tbaa !16
  %21 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call11, float* %edgeConst, align 4, !tbaa !16
  %22 = load float, float* %edgeConst, align 4, !tbaa !16
  %23 = load float, float* %dist9, align 4, !tbaa !16
  %sub12 = fsub float %23, %22
  store float %sub12, float* %dist9, align 4, !tbaa !16
  %24 = load float, float* %dist9, align 4, !tbaa !16
  %25 = load float, float* %tolerance.addr, align 4, !tbaa !16
  %fneg13 = fneg float %25
  %cmp14 = fcmp olt float %24, %fneg13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then15
  %26 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %28 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %32 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup21

cleanup21:                                        ; preds = %for.end, %cleanup
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %cleanup23

if.end22:                                         ; preds = %land.lhs.true, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup21
  %34 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  %37 = load i1, i1* %retval, align 1
  ret i1 %37
}

define linkonce_odr hidden void @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !39
  ret void
}

declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapeD0Ev(%class.btTriangleShape* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %this1) #8
  %0 = bitcast %class.btTriangleShape* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #8
  ret void
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexInternalShape* %0 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapedlEPv(i8* %ptr) #7 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2ERK9btVector3S2_S2_f(%class.btAABB* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %V1, %class.btVector3* nonnull align 4 dereferenceable(16) %V2, %class.btVector3* nonnull align 4 dereferenceable(16) %V3, float %margin) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btAABB*, align 4
  %this.addr = alloca %class.btAABB*, align 4
  %V1.addr = alloca %class.btVector3*, align 4
  %V2.addr = alloca %class.btVector3*, align 4
  %V3.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %V1, %class.btVector3** %V1.addr, align 4, !tbaa !2
  store %class.btVector3* %V2, %class.btVector3** %V2.addr, align 4, !tbaa !2
  store %class.btVector3* %V3, %class.btVector3** %V3.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !16
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %this1, %class.btAABB** %retval, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  %0 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %2 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %3 = load float, float* %arrayidx5, align 4, !tbaa !16
  %4 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %5 = load float, float* %arrayidx7, align 4, !tbaa !16
  %cmp = fcmp ogt float %3, %5
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %7 = load float, float* %arrayidx9, align 4, !tbaa !16
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  %9 = load float, float* %arrayidx11, align 4, !tbaa !16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %7, %cond.true ], [ %9, %cond.false ]
  %cmp12 = fcmp ogt float %1, %cond
  br i1 %cmp12, label %cond.true13, label %cond.false27

cond.true13:                                      ; preds = %cond.end
  %10 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %11 = load float, float* %arrayidx15, align 4, !tbaa !16
  %12 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 0
  %13 = load float, float* %arrayidx17, align 4, !tbaa !16
  %cmp18 = fcmp ogt float %11, %13
  br i1 %cmp18, label %cond.true19, label %cond.false22

cond.true19:                                      ; preds = %cond.true13
  %14 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %15 = load float, float* %arrayidx21, align 4, !tbaa !16
  br label %cond.end25

cond.false22:                                     ; preds = %cond.true13
  %16 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %17 = load float, float* %arrayidx24, align 4, !tbaa !16
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false22, %cond.true19
  %cond26 = phi float [ %15, %cond.true19 ], [ %17, %cond.false22 ]
  br label %cond.end30

cond.false27:                                     ; preds = %cond.end
  %18 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 0
  %19 = load float, float* %arrayidx29, align 4, !tbaa !16
  br label %cond.end30

cond.end30:                                       ; preds = %cond.false27, %cond.end25
  %cond31 = phi float [ %cond26, %cond.end25 ], [ %19, %cond.false27 ]
  %m_min32 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min32)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 0
  store float %cond31, float* %arrayidx34, align 4, !tbaa !16
  %20 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %20)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 1
  %21 = load float, float* %arrayidx36, align 4, !tbaa !16
  %22 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call37 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %23 = load float, float* %arrayidx38, align 4, !tbaa !16
  %24 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %24)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 1
  %25 = load float, float* %arrayidx40, align 4, !tbaa !16
  %cmp41 = fcmp ogt float %23, %25
  br i1 %cmp41, label %cond.true42, label %cond.false45

cond.true42:                                      ; preds = %cond.end30
  %26 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 1
  %27 = load float, float* %arrayidx44, align 4, !tbaa !16
  br label %cond.end48

cond.false45:                                     ; preds = %cond.end30
  %28 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call46 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 1
  %29 = load float, float* %arrayidx47, align 4, !tbaa !16
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false45, %cond.true42
  %cond49 = phi float [ %27, %cond.true42 ], [ %29, %cond.false45 ]
  %cmp50 = fcmp ogt float %21, %cond49
  br i1 %cmp50, label %cond.true51, label %cond.false65

cond.true51:                                      ; preds = %cond.end48
  %30 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call52 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 1
  %31 = load float, float* %arrayidx53, align 4, !tbaa !16
  %32 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 1
  %33 = load float, float* %arrayidx55, align 4, !tbaa !16
  %cmp56 = fcmp ogt float %31, %33
  br i1 %cmp56, label %cond.true57, label %cond.false60

cond.true57:                                      ; preds = %cond.true51
  %34 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %34)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %35 = load float, float* %arrayidx59, align 4, !tbaa !16
  br label %cond.end63

cond.false60:                                     ; preds = %cond.true51
  %36 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call61 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %36)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 1
  %37 = load float, float* %arrayidx62, align 4, !tbaa !16
  br label %cond.end63

cond.end63:                                       ; preds = %cond.false60, %cond.true57
  %cond64 = phi float [ %35, %cond.true57 ], [ %37, %cond.false60 ]
  br label %cond.end68

cond.false65:                                     ; preds = %cond.end48
  %38 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  %39 = load float, float* %arrayidx67, align 4, !tbaa !16
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false65, %cond.end63
  %cond69 = phi float [ %cond64, %cond.end63 ], [ %39, %cond.false65 ]
  %m_min70 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min70)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 1
  store float %cond69, float* %arrayidx72, align 4, !tbaa !16
  %40 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call73 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 2
  %41 = load float, float* %arrayidx74, align 4, !tbaa !16
  %42 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call75 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 2
  %43 = load float, float* %arrayidx76, align 4, !tbaa !16
  %44 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 2
  %45 = load float, float* %arrayidx78, align 4, !tbaa !16
  %cmp79 = fcmp ogt float %43, %45
  br i1 %cmp79, label %cond.true80, label %cond.false83

cond.true80:                                      ; preds = %cond.end68
  %46 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %47 = load float, float* %arrayidx82, align 4, !tbaa !16
  br label %cond.end86

cond.false83:                                     ; preds = %cond.end68
  %48 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call84 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 2
  %49 = load float, float* %arrayidx85, align 4, !tbaa !16
  br label %cond.end86

cond.end86:                                       ; preds = %cond.false83, %cond.true80
  %cond87 = phi float [ %47, %cond.true80 ], [ %49, %cond.false83 ]
  %cmp88 = fcmp ogt float %41, %cond87
  br i1 %cmp88, label %cond.true89, label %cond.false103

cond.true89:                                      ; preds = %cond.end86
  %50 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %50)
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 2
  %51 = load float, float* %arrayidx91, align 4, !tbaa !16
  %52 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call92 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 2
  %53 = load float, float* %arrayidx93, align 4, !tbaa !16
  %cmp94 = fcmp ogt float %51, %53
  br i1 %cmp94, label %cond.true95, label %cond.false98

cond.true95:                                      ; preds = %cond.true89
  %54 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call96 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %54)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 2
  %55 = load float, float* %arrayidx97, align 4, !tbaa !16
  br label %cond.end101

cond.false98:                                     ; preds = %cond.true89
  %56 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call99 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx100 = getelementptr inbounds float, float* %call99, i32 2
  %57 = load float, float* %arrayidx100, align 4, !tbaa !16
  br label %cond.end101

cond.end101:                                      ; preds = %cond.false98, %cond.true95
  %cond102 = phi float [ %55, %cond.true95 ], [ %57, %cond.false98 ]
  br label %cond.end106

cond.false103:                                    ; preds = %cond.end86
  %58 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call104 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %58)
  %arrayidx105 = getelementptr inbounds float, float* %call104, i32 2
  %59 = load float, float* %arrayidx105, align 4, !tbaa !16
  br label %cond.end106

cond.end106:                                      ; preds = %cond.false103, %cond.end101
  %cond107 = phi float [ %cond102, %cond.end101 ], [ %59, %cond.false103 ]
  %m_min108 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call109 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min108)
  %arrayidx110 = getelementptr inbounds float, float* %call109, i32 2
  store float %cond107, float* %arrayidx110, align 4, !tbaa !16
  %60 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call111 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %60)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 0
  %61 = load float, float* %arrayidx112, align 4, !tbaa !16
  %62 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call113 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx114 = getelementptr inbounds float, float* %call113, i32 0
  %63 = load float, float* %arrayidx114, align 4, !tbaa !16
  %64 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call115 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %64)
  %arrayidx116 = getelementptr inbounds float, float* %call115, i32 0
  %65 = load float, float* %arrayidx116, align 4, !tbaa !16
  %cmp117 = fcmp olt float %63, %65
  br i1 %cmp117, label %cond.true118, label %cond.false121

cond.true118:                                     ; preds = %cond.end106
  %66 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call119 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %66)
  %arrayidx120 = getelementptr inbounds float, float* %call119, i32 0
  %67 = load float, float* %arrayidx120, align 4, !tbaa !16
  br label %cond.end124

cond.false121:                                    ; preds = %cond.end106
  %68 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call122 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %68)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 0
  %69 = load float, float* %arrayidx123, align 4, !tbaa !16
  br label %cond.end124

cond.end124:                                      ; preds = %cond.false121, %cond.true118
  %cond125 = phi float [ %67, %cond.true118 ], [ %69, %cond.false121 ]
  %cmp126 = fcmp olt float %61, %cond125
  br i1 %cmp126, label %cond.true127, label %cond.false141

cond.true127:                                     ; preds = %cond.end124
  %70 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call128 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %70)
  %arrayidx129 = getelementptr inbounds float, float* %call128, i32 0
  %71 = load float, float* %arrayidx129, align 4, !tbaa !16
  %72 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call130 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %72)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 0
  %73 = load float, float* %arrayidx131, align 4, !tbaa !16
  %cmp132 = fcmp olt float %71, %73
  br i1 %cmp132, label %cond.true133, label %cond.false136

cond.true133:                                     ; preds = %cond.true127
  %74 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call134 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %74)
  %arrayidx135 = getelementptr inbounds float, float* %call134, i32 0
  %75 = load float, float* %arrayidx135, align 4, !tbaa !16
  br label %cond.end139

cond.false136:                                    ; preds = %cond.true127
  %76 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call137 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %76)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 0
  %77 = load float, float* %arrayidx138, align 4, !tbaa !16
  br label %cond.end139

cond.end139:                                      ; preds = %cond.false136, %cond.true133
  %cond140 = phi float [ %75, %cond.true133 ], [ %77, %cond.false136 ]
  br label %cond.end144

cond.false141:                                    ; preds = %cond.end124
  %78 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call142 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %78)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 0
  %79 = load float, float* %arrayidx143, align 4, !tbaa !16
  br label %cond.end144

cond.end144:                                      ; preds = %cond.false141, %cond.end139
  %cond145 = phi float [ %cond140, %cond.end139 ], [ %79, %cond.false141 ]
  %m_max146 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max146)
  %arrayidx148 = getelementptr inbounds float, float* %call147, i32 0
  store float %cond145, float* %arrayidx148, align 4, !tbaa !16
  %80 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call149 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %80)
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 1
  %81 = load float, float* %arrayidx150, align 4, !tbaa !16
  %82 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call151 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %82)
  %arrayidx152 = getelementptr inbounds float, float* %call151, i32 1
  %83 = load float, float* %arrayidx152, align 4, !tbaa !16
  %84 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call153 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %84)
  %arrayidx154 = getelementptr inbounds float, float* %call153, i32 1
  %85 = load float, float* %arrayidx154, align 4, !tbaa !16
  %cmp155 = fcmp olt float %83, %85
  br i1 %cmp155, label %cond.true156, label %cond.false159

cond.true156:                                     ; preds = %cond.end144
  %86 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call157 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %86)
  %arrayidx158 = getelementptr inbounds float, float* %call157, i32 1
  %87 = load float, float* %arrayidx158, align 4, !tbaa !16
  br label %cond.end162

cond.false159:                                    ; preds = %cond.end144
  %88 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call160 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %88)
  %arrayidx161 = getelementptr inbounds float, float* %call160, i32 1
  %89 = load float, float* %arrayidx161, align 4, !tbaa !16
  br label %cond.end162

cond.end162:                                      ; preds = %cond.false159, %cond.true156
  %cond163 = phi float [ %87, %cond.true156 ], [ %89, %cond.false159 ]
  %cmp164 = fcmp olt float %81, %cond163
  br i1 %cmp164, label %cond.true165, label %cond.false179

cond.true165:                                     ; preds = %cond.end162
  %90 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call166 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %90)
  %arrayidx167 = getelementptr inbounds float, float* %call166, i32 1
  %91 = load float, float* %arrayidx167, align 4, !tbaa !16
  %92 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call168 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %92)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 1
  %93 = load float, float* %arrayidx169, align 4, !tbaa !16
  %cmp170 = fcmp olt float %91, %93
  br i1 %cmp170, label %cond.true171, label %cond.false174

cond.true171:                                     ; preds = %cond.true165
  %94 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call172 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %94)
  %arrayidx173 = getelementptr inbounds float, float* %call172, i32 1
  %95 = load float, float* %arrayidx173, align 4, !tbaa !16
  br label %cond.end177

cond.false174:                                    ; preds = %cond.true165
  %96 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call175 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %96)
  %arrayidx176 = getelementptr inbounds float, float* %call175, i32 1
  %97 = load float, float* %arrayidx176, align 4, !tbaa !16
  br label %cond.end177

cond.end177:                                      ; preds = %cond.false174, %cond.true171
  %cond178 = phi float [ %95, %cond.true171 ], [ %97, %cond.false174 ]
  br label %cond.end182

cond.false179:                                    ; preds = %cond.end162
  %98 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call180 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %98)
  %arrayidx181 = getelementptr inbounds float, float* %call180, i32 1
  %99 = load float, float* %arrayidx181, align 4, !tbaa !16
  br label %cond.end182

cond.end182:                                      ; preds = %cond.false179, %cond.end177
  %cond183 = phi float [ %cond178, %cond.end177 ], [ %99, %cond.false179 ]
  %m_max184 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call185 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max184)
  %arrayidx186 = getelementptr inbounds float, float* %call185, i32 1
  store float %cond183, float* %arrayidx186, align 4, !tbaa !16
  %100 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call187 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %100)
  %arrayidx188 = getelementptr inbounds float, float* %call187, i32 2
  %101 = load float, float* %arrayidx188, align 4, !tbaa !16
  %102 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call189 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %102)
  %arrayidx190 = getelementptr inbounds float, float* %call189, i32 2
  %103 = load float, float* %arrayidx190, align 4, !tbaa !16
  %104 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call191 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %104)
  %arrayidx192 = getelementptr inbounds float, float* %call191, i32 2
  %105 = load float, float* %arrayidx192, align 4, !tbaa !16
  %cmp193 = fcmp olt float %103, %105
  br i1 %cmp193, label %cond.true194, label %cond.false197

cond.true194:                                     ; preds = %cond.end182
  %106 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call195 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %106)
  %arrayidx196 = getelementptr inbounds float, float* %call195, i32 2
  %107 = load float, float* %arrayidx196, align 4, !tbaa !16
  br label %cond.end200

cond.false197:                                    ; preds = %cond.end182
  %108 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call198 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %108)
  %arrayidx199 = getelementptr inbounds float, float* %call198, i32 2
  %109 = load float, float* %arrayidx199, align 4, !tbaa !16
  br label %cond.end200

cond.end200:                                      ; preds = %cond.false197, %cond.true194
  %cond201 = phi float [ %107, %cond.true194 ], [ %109, %cond.false197 ]
  %cmp202 = fcmp olt float %101, %cond201
  br i1 %cmp202, label %cond.true203, label %cond.false217

cond.true203:                                     ; preds = %cond.end200
  %110 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call204 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %110)
  %arrayidx205 = getelementptr inbounds float, float* %call204, i32 2
  %111 = load float, float* %arrayidx205, align 4, !tbaa !16
  %112 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call206 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %112)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 2
  %113 = load float, float* %arrayidx207, align 4, !tbaa !16
  %cmp208 = fcmp olt float %111, %113
  br i1 %cmp208, label %cond.true209, label %cond.false212

cond.true209:                                     ; preds = %cond.true203
  %114 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4, !tbaa !2
  %call210 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %114)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 2
  %115 = load float, float* %arrayidx211, align 4, !tbaa !16
  br label %cond.end215

cond.false212:                                    ; preds = %cond.true203
  %116 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4, !tbaa !2
  %call213 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %116)
  %arrayidx214 = getelementptr inbounds float, float* %call213, i32 2
  %117 = load float, float* %arrayidx214, align 4, !tbaa !16
  br label %cond.end215

cond.end215:                                      ; preds = %cond.false212, %cond.true209
  %cond216 = phi float [ %115, %cond.true209 ], [ %117, %cond.false212 ]
  br label %cond.end220

cond.false217:                                    ; preds = %cond.end200
  %118 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4, !tbaa !2
  %call218 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %118)
  %arrayidx219 = getelementptr inbounds float, float* %call218, i32 2
  %119 = load float, float* %arrayidx219, align 4, !tbaa !16
  br label %cond.end220

cond.end220:                                      ; preds = %cond.false217, %cond.end215
  %cond221 = phi float [ %cond216, %cond.end215 ], [ %119, %cond.false217 ]
  %m_max222 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call223 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max222)
  %arrayidx224 = getelementptr inbounds float, float* %call223, i32 2
  store float %cond221, float* %arrayidx224, align 4, !tbaa !16
  %120 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min225 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call226 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min225)
  %arrayidx227 = getelementptr inbounds float, float* %call226, i32 0
  %121 = load float, float* %arrayidx227, align 4, !tbaa !16
  %sub = fsub float %121, %120
  store float %sub, float* %arrayidx227, align 4, !tbaa !16
  %122 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min228 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call229 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min228)
  %arrayidx230 = getelementptr inbounds float, float* %call229, i32 1
  %123 = load float, float* %arrayidx230, align 4, !tbaa !16
  %sub231 = fsub float %123, %122
  store float %sub231, float* %arrayidx230, align 4, !tbaa !16
  %124 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_min232 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call233 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min232)
  %arrayidx234 = getelementptr inbounds float, float* %call233, i32 2
  %125 = load float, float* %arrayidx234, align 4, !tbaa !16
  %sub235 = fsub float %125, %124
  store float %sub235, float* %arrayidx234, align 4, !tbaa !16
  %126 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max236 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call237 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max236)
  %arrayidx238 = getelementptr inbounds float, float* %call237, i32 0
  %127 = load float, float* %arrayidx238, align 4, !tbaa !16
  %add = fadd float %127, %126
  store float %add, float* %arrayidx238, align 4, !tbaa !16
  %128 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max239 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max239)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 1
  %129 = load float, float* %arrayidx241, align 4, !tbaa !16
  %add242 = fadd float %129, %128
  store float %add242, float* %arrayidx241, align 4, !tbaa !16
  %130 = load float, float* %margin.addr, align 4, !tbaa !16
  %m_max243 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call244 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max243)
  %arrayidx245 = getelementptr inbounds float, float* %call244, i32 2
  %131 = load float, float* %arrayidx245, align 4, !tbaa !16
  %add246 = fadd float %131, %130
  store float %add246, float* %arrayidx245, align 4, !tbaa !16
  %132 = load %class.btAABB*, %class.btAABB** %retval, align 4
  ret %class.btAABB* %132
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !16
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !16
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !16
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !16
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !16
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !16
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !16
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

define linkonce_odr hidden void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %2 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !39
  %6 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #8
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %9)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !16
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !16
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !16
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !16
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !16
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !16
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !16
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !16
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !16
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !16
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !16
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !16
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !16
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !16
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !16
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !16
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !16
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !16
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !16
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !16
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !16
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !16
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !16
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !16
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !16
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !16
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !16
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !16
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #7 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !16
  %0 = load float, float* %y.addr, align 4, !tbaa !16
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector48setValueERKfS1_S1_S1_(%class.btVector4* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #7 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !16
  %2 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !16
  %3 = load float*, float** %_y.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !16
  %5 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %4, float* %arrayidx3, align 4, !tbaa !16
  %6 = load float*, float** %_z.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !16
  %8 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %7, float* %arrayidx5, align 4, !tbaa !16
  %9 = load float*, float** %_w.addr, align 4, !tbaa !2
  %10 = load float, float* %9, align 4, !tbaa !16
  %11 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %10, float* %arrayidx7, align 4, !tbaa !16
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !94
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data, align 4, !tbaa !97
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %0, i32 %1
  ret %class.btGImpactMeshShapePart** %arrayidx
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI8GIM_PAIREC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI8GIM_PAIRLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE4initEv(%class.btAlignedObjectArray.0* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.GIM_PAIR* null, %struct.GIM_PAIR** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !48
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !86
  ret void
}

define linkonce_odr hidden %class.btTetrahedronShapeEx* @_ZN20btTetrahedronShapeExC2Ev(%class.btTetrahedronShapeEx* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTetrahedronShapeEx*, align 4
  store %class.btTetrahedronShapeEx* %this, %class.btTetrahedronShapeEx** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTetrahedronShapeEx*, %class.btTetrahedronShapeEx** %this.addr, align 4
  %0 = bitcast %class.btTetrahedronShapeEx* %this1 to %class.btBU_Simplex1to4*
  %call = call %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2Ev(%class.btBU_Simplex1to4* %0)
  %1 = bitcast %class.btTetrahedronShapeEx* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV20btTetrahedronShapeEx, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %2 = bitcast %class.btTetrahedronShapeEx* %this1 to %class.btBU_Simplex1to4*
  %m_numVertices = getelementptr inbounds %class.btBU_Simplex1to4, %class.btBU_Simplex1to4* %2, i32 0, i32 1
  store i32 4, i32* %m_numVertices, align 4, !tbaa !98
  ret %class.btTetrahedronShapeEx* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4
  %0 = bitcast %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN18GIM_ShapeRetriever19ChildShapeRetrieverE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* @_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4
  %0 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %call = call %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0) #8
  %1 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN18GIM_ShapeRetriever22TriangleShapeRetrieverE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"class.GIM_ShapeRetriever::TetraShapeRetriever"* @_ZN18GIM_ShapeRetriever19TetraShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4
  %0 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %call = call %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverC2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0) #8
  %1 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN18GIM_ShapeRetriever19TetraShapeRetrieverE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1
}

declare %class.btBU_Simplex1to4* @_ZN16btBU_Simplex1to4C2Ev(%class.btBU_Simplex1to4* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btTetrahedronShapeExD0Ev(%class.btTetrahedronShapeEx* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTetrahedronShapeEx*, align 4
  store %class.btTetrahedronShapeEx* %this, %class.btTetrahedronShapeEx** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTetrahedronShapeEx*, %class.btTetrahedronShapeEx** %this.addr, align 4
  %call = call %class.btTetrahedronShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTetrahedronShapeEx* (%class.btTetrahedronShapeEx*)*)(%class.btTetrahedronShapeEx* %this1) #8
  %0 = bitcast %class.btTetrahedronShapeEx* %this1 to i8*
  call void @_ZN16btBU_Simplex1to4dlEPv(i8* %0) #8
  ret void
}

declare void @_ZNK16btBU_Simplex1to47getAabbERK11btTransformR9btVector3S4_(%class.btBU_Simplex1to4*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3(%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK16btBU_Simplex1to47getNameEv(%class.btBU_Simplex1to4* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btBU_Simplex1to4*, align 4
  store %class.btBU_Simplex1to4* %this, %class.btBU_Simplex1to4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBU_Simplex1to4*, %class.btBU_Simplex1to4** %this.addr, align 4
  ret i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.2, i32 0, i32 0)
}

declare void @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* sret align 4, %class.btPolyhedralConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

declare i32 @_ZNK16btBU_Simplex1to414getNumVerticesEv(%class.btBU_Simplex1to4*) unnamed_addr #1

declare i32 @_ZNK16btBU_Simplex1to411getNumEdgesEv(%class.btBU_Simplex1to4*) unnamed_addr #1

declare void @_ZNK16btBU_Simplex1to47getEdgeEiR9btVector3S1_(%class.btBU_Simplex1to4*, i32, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK16btBU_Simplex1to49getVertexEiR9btVector3(%class.btBU_Simplex1to4*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare i32 @_ZNK16btBU_Simplex1to412getNumPlanesEv(%class.btBU_Simplex1to4*) unnamed_addr #1

declare void @_ZNK16btBU_Simplex1to48getPlaneER9btVector3S1_i(%class.btBU_Simplex1to4*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), i32) unnamed_addr #1

declare zeroext i1 @_ZNK16btBU_Simplex1to48isInsideERK9btVector3f(%class.btBU_Simplex1to4*, %class.btVector3* nonnull align 4 dereferenceable(16), float) unnamed_addr #1

declare i32 @_ZNK16btBU_Simplex1to48getIndexEi(%class.btBU_Simplex1to4*, i32) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btBU_Simplex1to4dlEPv(i8* %ptr) #7 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define linkonce_odr hidden %class.btCollisionShape* @_ZN18GIM_ShapeRetriever19ChildShapeRetriever13getChildShapeEi(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, align 4
  %index.addr = alloca i32, align 4
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4
  %m_parent = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1, i32 0, i32 1
  %0 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent, align 4, !tbaa !60
  %m_gim_shape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %0, i32 0, i32 0
  %1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %m_gim_shape, align 4, !tbaa !52
  %2 = load i32, i32* %index.addr, align 4, !tbaa !25
  %3 = bitcast %class.btGImpactShapeInterface* %1 to %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)***
  %vtable = load %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)**, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)** %vtable, i64 32
  %4 = load %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)** %vfn, align 4
  %call = call %class.btCollisionShape* %4(%class.btGImpactShapeInterface* %1, i32 %2)
  ret %class.btCollisionShape* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4
  ret %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD0Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::ChildShapeRetriever"*, %"class.GIM_ShapeRetriever::ChildShapeRetriever"** %this.addr, align 4
  %call = call %"class.GIM_ShapeRetriever::ChildShapeRetriever"* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev(%"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1) #8
  %0 = bitcast %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden %class.btCollisionShape* @_ZN18GIM_ShapeRetriever22TriangleShapeRetriever13getChildShapeEi(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, align 4
  %index.addr = alloca i32, align 4
  store %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4
  %0 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0, i32 0, i32 1
  %1 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent, align 4, !tbaa !60
  %m_gim_shape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %1, i32 0, i32 0
  %2 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %m_gim_shape, align 4, !tbaa !52
  %3 = load i32, i32* %index.addr, align 4, !tbaa !25
  %4 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent2 = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %4, i32 0, i32 1
  %5 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent2, align 4, !tbaa !60
  %m_trishape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %5, i32 0, i32 1
  %6 = bitcast %class.btGImpactShapeInterface* %2 to void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)***
  %vtable = load void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)**, void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)** %vtable, i64 26
  %7 = load void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTriangleShapeEx*)** %vfn, align 4
  call void %7(%class.btGImpactShapeInterface* %2, i32 %3, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(104) %m_trishape)
  %8 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent3 = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %8, i32 0, i32 1
  %9 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent3, align 4, !tbaa !60
  %m_trishape4 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %9, i32 0, i32 1
  %10 = bitcast %class.btTriangleShapeEx* %m_trishape4 to %class.btCollisionShape*
  ret %class.btCollisionShape* %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN18GIM_ShapeRetriever22TriangleShapeRetrieverD0Ev(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::TriangleShapeRetriever"*, %"class.GIM_ShapeRetriever::TriangleShapeRetriever"** %this.addr, align 4
  %call = call %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* (%"class.GIM_ShapeRetriever::TriangleShapeRetriever"*)*)(%"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1) #8
  %0 = bitcast %"class.GIM_ShapeRetriever::TriangleShapeRetriever"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden %class.btCollisionShape* @_ZN18GIM_ShapeRetriever19TetraShapeRetriever13getChildShapeEi(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, align 4
  %index.addr = alloca i32, align 4
  store %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !25
  %this1 = load %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4
  %0 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %0, i32 0, i32 1
  %1 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent, align 4, !tbaa !60
  %m_gim_shape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %1, i32 0, i32 0
  %2 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %m_gim_shape, align 4, !tbaa !52
  %3 = load i32, i32* %index.addr, align 4, !tbaa !25
  %4 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent2 = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %4, i32 0, i32 1
  %5 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent2, align 4, !tbaa !60
  %m_tetrashape = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %5, i32 0, i32 2
  %6 = bitcast %class.btGImpactShapeInterface* %2 to void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)***
  %vtable = load void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)**, void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)** %vtable, i64 27
  %7 = load void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)*, void (%class.btGImpactShapeInterface*, i32, %class.btTetrahedronShapeEx*)** %vfn, align 4
  call void %7(%class.btGImpactShapeInterface* %2, i32 %3, %class.btTetrahedronShapeEx* nonnull align 4 dereferenceable(160) %m_tetrashape)
  %8 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to %"class.GIM_ShapeRetriever::ChildShapeRetriever"*
  %m_parent3 = getelementptr inbounds %"class.GIM_ShapeRetriever::ChildShapeRetriever", %"class.GIM_ShapeRetriever::ChildShapeRetriever"* %8, i32 0, i32 1
  %9 = load %class.GIM_ShapeRetriever*, %class.GIM_ShapeRetriever** %m_parent3, align 4, !tbaa !60
  %m_tetrashape4 = getelementptr inbounds %class.GIM_ShapeRetriever, %class.GIM_ShapeRetriever* %9, i32 0, i32 2
  %10 = bitcast %class.btTetrahedronShapeEx* %m_tetrashape4 to %class.btCollisionShape*
  ret %class.btCollisionShape* %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN18GIM_ShapeRetriever19TetraShapeRetrieverD0Ev(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, align 4
  store %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.GIM_ShapeRetriever::TetraShapeRetriever"*, %"class.GIM_ShapeRetriever::TetraShapeRetriever"** %this.addr, align 4
  %call = call %"class.GIM_ShapeRetriever::TetraShapeRetriever"* bitcast (%"class.GIM_ShapeRetriever::ChildShapeRetriever"* (%"class.GIM_ShapeRetriever::ChildShapeRetriever"*)* @_ZN18GIM_ShapeRetriever19ChildShapeRetrieverD2Ev to %"class.GIM_ShapeRetriever::TetraShapeRetriever"* (%"class.GIM_ShapeRetriever::TetraShapeRetriever"*)*)(%"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1) #8
  %0 = bitcast %"class.GIM_ShapeRetriever::TetraShapeRetriever"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #3 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !16
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !16
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !16
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !16
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !16
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !16
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !16
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !39
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !39
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !39
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI8GIM_PAIRE5clearEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI8GIM_PAIRE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI8GIM_PAIRE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %proxyType) #7 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !25
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !25
  %cmp = icmp eq i32 %0, 31
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #7 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !25
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !25
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !25
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray.12* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !100
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !103
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %this, i32 %i) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK6btAABB19projection_intervalERK9btVector3RfS3_(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %direction, float* nonnull align 4 dereferenceable(4) %vmin, float* nonnull align 4 dereferenceable(4) %vmax) #3 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %direction.addr = alloca %class.btVector3*, align 4
  %vmin.addr = alloca float*, align 4
  %vmax.addr = alloca float*, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %extend = alloca %class.btVector3, align 4
  %_fOrigin = alloca float, align 4
  %_fMaximumExtent = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %direction, %class.btVector3** %direction.addr, align 4, !tbaa !2
  store float* %vmin, float** %vmin.addr, align 4, !tbaa !2
  store float* %vmax, float** %vmax.addr, align 4, !tbaa !2
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 5.000000e-01, float* %ref.tmp2, align 4, !tbaa !16
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #8
  %5 = bitcast %class.btVector3* %extend to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %m_max3 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %extend, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max3, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %6 = bitcast float* %_fOrigin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  store float %call, float* %_fOrigin, align 4, !tbaa !16
  %8 = bitcast float* %_fMaximumExtent to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = load %class.btVector3*, %class.btVector3** %direction.addr, align 4, !tbaa !2
  call void @_ZNK9btVector38absoluteEv(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* %10)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %extend, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %11 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  store float %call5, float* %_fMaximumExtent, align 4, !tbaa !16
  %12 = load float, float* %_fOrigin, align 4, !tbaa !16
  %13 = load float, float* %_fMaximumExtent, align 4, !tbaa !16
  %sub = fsub float %12, %13
  %14 = load float*, float** %vmin.addr, align 4, !tbaa !2
  store float %sub, float* %14, align 4, !tbaa !16
  %15 = load float, float* %_fOrigin, align 4, !tbaa !16
  %16 = load float, float* %_fMaximumExtent, align 4, !tbaa !16
  %add = fadd float %15, %16
  %17 = load float*, float** %vmax.addr, align 4, !tbaa !2
  store float %add, float* %17, align 4, !tbaa !16
  %18 = bitcast float* %_fMaximumExtent to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %_fOrigin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast %class.btVector3* %extend to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !16
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !16
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !16
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !16
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !16
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !16
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !16
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !16
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !16
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !16
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !16
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !16
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !16
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !16
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector38absoluteEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !16
  %call = call float @_Z6btFabsf(float %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !16
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_floats3 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4, !tbaa !16
  %call5 = call float @_Z6btFabsf(float %3)
  store float %call5, float* %ref.tmp2, align 4, !tbaa !16
  %4 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4, !tbaa !16
  %call9 = call float @_Z6btFabsf(float %5)
  store float %call9, float* %ref.tmp6, align 4, !tbaa !16
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #7 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !16
  %0 = load float, float* %x.addr, align 4, !tbaa !16
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) #7 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %numverts = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 7
  %0 = load i32, i32* %numverts, align 4, !tbaa !104
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %vertex_index, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex) #3 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %vertex_index.addr = alloca i32, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  %dvertices = alloca double*, align 4
  %svertices = alloca float*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4, !tbaa !2
  store i32 %vertex_index, i32* %vertex_index.addr, align 4, !tbaa !25
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %type = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 8
  %0 = load i32, i32* %type, align 4, !tbaa !107
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast double** %dvertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %vertexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  %2 = load i8*, i8** %vertexbase, align 4, !tbaa !108
  %3 = load i32, i32* %vertex_index.addr, align 4, !tbaa !25
  %stride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  %4 = load i32, i32* %stride, align 4, !tbaa !109
  %mul = mul i32 %3, %4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %mul
  %5 = bitcast i8* %add.ptr to double*
  store double* %5, double** %dvertices, align 4, !tbaa !2
  %6 = load double*, double** %dvertices, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds double, double* %6, i32 0
  %7 = load double, double* %arrayidx, align 8, !tbaa !110
  %m_scale = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 0
  %8 = load float, float* %arrayidx2, align 4, !tbaa !16
  %conv = fpext float %8 to double
  %mul3 = fmul double %7, %conv
  %conv4 = fptrunc double %mul3 to float
  %9 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %9)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  store float %conv4, float* %arrayidx6, align 4, !tbaa !16
  %10 = load double*, double** %dvertices, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds double, double* %10, i32 1
  %11 = load double, double* %arrayidx7, align 8, !tbaa !110
  %m_scale8 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %12 = load float, float* %arrayidx10, align 4, !tbaa !16
  %conv11 = fpext float %12 to double
  %mul12 = fmul double %11, %conv11
  %conv13 = fptrunc double %mul12 to float
  %13 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  store float %conv13, float* %arrayidx15, align 4, !tbaa !16
  %14 = load double*, double** %dvertices, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds double, double* %14, i32 2
  %15 = load double, double* %arrayidx16, align 8, !tbaa !110
  %m_scale17 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %16 = load float, float* %arrayidx19, align 4, !tbaa !16
  %conv20 = fpext float %16 to double
  %mul21 = fmul double %15, %conv20
  %conv22 = fptrunc double %mul21 to float
  %17 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  store float %conv22, float* %arrayidx24, align 4, !tbaa !16
  %18 = bitcast double** %dvertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %19 = bitcast float** %svertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %vertexbase25 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  %20 = load i8*, i8** %vertexbase25, align 4, !tbaa !108
  %21 = load i32, i32* %vertex_index.addr, align 4, !tbaa !25
  %stride26 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  %22 = load i32, i32* %stride26, align 4, !tbaa !109
  %mul27 = mul i32 %21, %22
  %add.ptr28 = getelementptr inbounds i8, i8* %20, i32 %mul27
  %23 = bitcast i8* %add.ptr28 to float*
  store float* %23, float** %svertices, align 4, !tbaa !2
  %24 = load float*, float** %svertices, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds float, float* %24, i32 0
  %25 = load float, float* %arrayidx29, align 4, !tbaa !16
  %m_scale30 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 0
  %26 = load float, float* %arrayidx32, align 4, !tbaa !16
  %mul33 = fmul float %25, %26
  %27 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  store float %mul33, float* %arrayidx35, align 4, !tbaa !16
  %28 = load float*, float** %svertices, align 4, !tbaa !2
  %arrayidx36 = getelementptr inbounds float, float* %28, i32 1
  %29 = load float, float* %arrayidx36, align 4, !tbaa !16
  %m_scale37 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %30 = load float, float* %arrayidx39, align 4, !tbaa !16
  %mul40 = fmul float %29, %30
  %31 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %31)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 1
  store float %mul40, float* %arrayidx42, align 4, !tbaa !16
  %32 = load float*, float** %svertices, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds float, float* %32, i32 2
  %33 = load float, float* %arrayidx43, align 4, !tbaa !16
  %m_scale44 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale44)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 2
  %34 = load float, float* %arrayidx46, align 4, !tbaa !16
  %mul47 = fmul float %33, %34
  %35 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4, !tbaa !2
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %35)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 2
  store float %mul47, float* %arrayidx49, align 4, !tbaa !16
  %36 = bitcast float** %svertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN25btGImpactTriangleCallbackD0Ev(%class.btGImpactTriangleCallback* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btGImpactTriangleCallback*, align 4
  store %class.btGImpactTriangleCallback* %this, %class.btGImpactTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactTriangleCallback*, %class.btGImpactTriangleCallback** %this.addr, align 4
  %call = call %class.btGImpactTriangleCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %class.btGImpactTriangleCallback* (%class.btGImpactTriangleCallback*)*)(%class.btGImpactTriangleCallback* %this1) #8
  %0 = bitcast %class.btGImpactTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden void @_ZN25btGImpactTriangleCallback15processTriangleEP9btVector3ii(%class.btGImpactTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btGImpactTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %tri1 = alloca %class.btTriangleShapeEx, align 4
  %ob1Wrap = alloca %struct.btCollisionObjectWrapper, align 4
  %tmp = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btGImpactTriangleCallback* %this, %class.btGImpactTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !25
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactTriangleCallback*, %class.btGImpactTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %tri1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 2
  %call = call %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2ERK9btVector3S2_S2_(%class.btTriangleShapeEx* %tri1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3)
  %4 = bitcast %class.btTriangleShapeEx* %tri1 to %class.btConvexInternalShape*
  %margin = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 6
  %5 = load float, float* %margin, align 4, !tbaa !69
  call void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %4, float %5)
  %swapped = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 5
  %6 = load i8, i8* %swapped, align 4, !tbaa !68, !range !47
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %algorithm = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %7 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm, align 4, !tbaa !63
  %8 = load i32, i32* %partId.addr, align 4, !tbaa !25
  call void @_ZN27btGImpactCollisionAlgorithm8setPart0Ei(%class.btGImpactCollisionAlgorithm* %7, i32 %8)
  %algorithm4 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %9 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm4, align 4, !tbaa !63
  %10 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !25
  call void @_ZN27btGImpactCollisionAlgorithm8setFace0Ei(%class.btGImpactCollisionAlgorithm* %9, i32 %10)
  br label %if.end

if.else:                                          ; preds = %entry
  %algorithm5 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %11 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm5, align 4, !tbaa !63
  %12 = load i32, i32* %partId.addr, align 4, !tbaa !25
  call void @_ZN27btGImpactCollisionAlgorithm8setPart1Ei(%class.btGImpactCollisionAlgorithm* %11, i32 %12)
  %algorithm6 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %13 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm6, align 4, !tbaa !63
  %14 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !25
  call void @_ZN27btGImpactCollisionAlgorithm8setFace1Ei(%class.btGImpactCollisionAlgorithm* %13, i32 %14)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %15 = bitcast %struct.btCollisionObjectWrapper* %ob1Wrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %15) #8
  %body1Wrap = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 3
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap, align 4, !tbaa !66
  %17 = bitcast %class.btTriangleShapeEx* %tri1 to %class.btCollisionShape*
  %body1Wrap7 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 3
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap7, align 4, !tbaa !66
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %18)
  %body1Wrap9 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 3
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap9, align 4, !tbaa !66
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %19)
  %20 = load i32, i32* %partId.addr, align 4, !tbaa !25
  %21 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !25
  %call11 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %ob1Wrap, %struct.btCollisionObjectWrapper* %16, %class.btCollisionShape* %17, %class.btCollisionObject* %call8, %class.btTransform* nonnull align 4 dereferenceable(64) %call10, i32 %20, i32 %21)
  %22 = bitcast %struct.btCollisionObjectWrapper** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %algorithm12 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %23 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm12, align 4, !tbaa !63
  %call13 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %23)
  %call14 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %call13)
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %call14)
  %call16 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %ob1Wrap)
  %cmp = icmp eq %class.btCollisionObject* %call15, %call16
  br i1 %cmp, label %if.then17, label %if.else23

if.then17:                                        ; preds = %if.end
  %algorithm18 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %24 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm18, align 4, !tbaa !63
  %call19 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %24)
  %call20 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %call19)
  store %struct.btCollisionObjectWrapper* %call20, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %algorithm21 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %25 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm21, align 4, !tbaa !63
  %call22 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %25)
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %call22, %struct.btCollisionObjectWrapper* %ob1Wrap)
  br label %if.end29

if.else23:                                        ; preds = %if.end
  %algorithm24 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %26 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm24, align 4, !tbaa !63
  %call25 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %26)
  %call26 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %call25)
  store %struct.btCollisionObjectWrapper* %call26, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  %algorithm27 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %27 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm27, align 4, !tbaa !63
  %call28 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %27)
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %call28, %struct.btCollisionObjectWrapper* %ob1Wrap)
  br label %if.end29

if.end29:                                         ; preds = %if.else23, %if.then17
  %algorithm30 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %28 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm30, align 4, !tbaa !63
  %body0Wrap = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 2
  %29 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap, align 4, !tbaa !65
  %gimpactshape0 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 4
  %30 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %gimpactshape0, align 4, !tbaa !67
  %31 = bitcast %class.btTriangleShapeEx* %tri1 to %class.btCollisionShape*
  %swapped31 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 5
  %32 = load i8, i8* %swapped31, align 4, !tbaa !68, !range !47
  %tobool32 = trunc i8 %32 to i1
  call void @_ZN27btGImpactCollisionAlgorithm16gimpact_vs_shapeEPK24btCollisionObjectWrapperS2_PK23btGImpactShapeInterfacePK16btCollisionShapeb(%class.btGImpactCollisionAlgorithm* %28, %struct.btCollisionObjectWrapper* %29, %struct.btCollisionObjectWrapper* %ob1Wrap, %class.btGImpactShapeInterface* %30, %class.btCollisionShape* %31, i1 zeroext %tobool32)
  %algorithm33 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %33 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm33, align 4, !tbaa !63
  %call34 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %33)
  %call35 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %call34)
  %call36 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %call35)
  %call37 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %ob1Wrap)
  %cmp38 = icmp eq %class.btCollisionObject* %call36, %call37
  br i1 %cmp38, label %if.then39, label %if.else42

if.then39:                                        ; preds = %if.end29
  %algorithm40 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %34 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm40, align 4, !tbaa !63
  %call41 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %34)
  %35 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %call41, %struct.btCollisionObjectWrapper* %35)
  br label %if.end45

if.else42:                                        ; preds = %if.end29
  %algorithm43 = getelementptr inbounds %class.btGImpactTriangleCallback, %class.btGImpactTriangleCallback* %this1, i32 0, i32 1
  %36 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %algorithm43, align 4, !tbaa !63
  %call44 = call %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %36)
  %37 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmp, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %call44, %struct.btCollisionObjectWrapper* %37)
  br label %if.end45

if.end45:                                         ; preds = %if.else42, %if.then39
  %38 = bitcast %struct.btCollisionObjectWrapper** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %struct.btCollisionObjectWrapper* %ob1Wrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %39) #8
  %call46 = call %class.btTriangleShapeEx* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShapeEx* (%class.btTriangleShapeEx*)*)(%class.btTriangleShapeEx* %tri1) #8
  %40 = bitcast %class.btTriangleShapeEx* %tri1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %40) #8
  ret void
}

define linkonce_odr hidden %class.btTriangleShapeEx* @_ZN17btTriangleShapeExC2ERK9btVector3S2_S2_(%class.btTriangleShapeEx* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShapeEx*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShapeEx* %this, %class.btTriangleShapeEx** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %this.addr, align 4
  %0 = bitcast %class.btTriangleShapeEx* %this1 to %class.btTriangleShape*
  %1 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %call = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast %class.btTriangleShapeEx* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV17btTriangleShapeEx, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  ret %class.btTriangleShapeEx* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm8setPart0Ei(%class.btGImpactCollisionAlgorithm* %this, i32 %value) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %value.addr = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load i32, i32* %value.addr, align 4, !tbaa !25
  %m_part0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 6
  store i32 %0, i32* %m_part0, align 4, !tbaa !13
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm8setFace0Ei(%class.btGImpactCollisionAlgorithm* %this, i32 %value) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %value.addr = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load i32, i32* %value.addr, align 4, !tbaa !25
  %m_triface0 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %0, i32* %m_triface0, align 4, !tbaa !12
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm8setPart1Ei(%class.btGImpactCollisionAlgorithm* %this, i32 %value) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %value.addr = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load i32, i32* %value.addr, align 4, !tbaa !25
  %m_part1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 8
  store i32 %0, i32* %m_part1, align 4, !tbaa !15
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm8setFace1Ei(%class.btGImpactCollisionAlgorithm* %this, i32 %value) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  %value.addr = alloca i32, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store i32 %value, i32* %value.addr, align 4, !tbaa !25
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %0 = load i32, i32* %value.addr, align 4, !tbaa !25
  %m_triface1 = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 7
  store i32 %0, i32* %m_triface1, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btManifoldResult* @_ZN27btGImpactCollisionAlgorithm20internalGetResultOutEv(%class.btGImpactCollisionAlgorithm* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCollisionAlgorithm*, align 4
  store %class.btGImpactCollisionAlgorithm* %this, %class.btGImpactCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btGImpactCollisionAlgorithm*, %class.btGImpactCollisionAlgorithm** %this.addr, align 4
  %m_resultOut = getelementptr inbounds %class.btGImpactCollisionAlgorithm, %class.btGImpactCollisionAlgorithm* %this1, i32 0, i32 3
  %0 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !18
  ret %class.btManifoldResult* %0
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4, !tbaa !112
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN27btGImpactCollisionAlgorithm10CreateFuncD0Ev(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btGImpactCollisionAlgorithm::CreateFunc"* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to %"struct.btGImpactCollisionAlgorithm::CreateFunc"* (%"struct.btGImpactCollisionAlgorithm::CreateFunc"*)*)(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this1) #8
  %0 = bitcast %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btGImpactCollisionAlgorithm::CreateFunc"* %this, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %"struct.btGImpactCollisionAlgorithm::CreateFunc"*, %"struct.btGImpactCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %1, i32 0, i32 0
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !114
  %3 = bitcast %class.btDispatcher* %2 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %2, i32 40)
  store i8* %call, i8** %mem, align 4, !tbaa !2
  %5 = load i8*, i8** %mem, align 4, !tbaa !2
  %6 = bitcast i8* %5 to %class.btGImpactCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btGImpactCollisionAlgorithm* @_ZN27btGImpactCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btGImpactCollisionAlgorithm* %6, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btGImpactCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  %11 = bitcast i8** %mem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret %class.btCollisionAlgorithm* %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #8
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.16* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !25
  %1 = load i32, i32* %sz, align 4, !tbaa !25
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !116
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !119
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %3
  %4 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btPersistentManifold**
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %6, align 4, !tbaa !2
  store %class.btPersistentManifold* %7, %class.btPersistentManifold** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !119
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !119
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !119
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !120
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !121
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !116
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !120
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !116
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !116
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !116
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !121, !range !47
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !116
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !116
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !122
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !123
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !30
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !122
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, i32* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !30
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !25
  store i32 %10, i32* %7, align 4, !tbaa !25
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !30
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !30
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !123, !range !47
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !30
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !30
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.4* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !123
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !30
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !34
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !122
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 12}
!9 = !{!"_ZTS27btGImpactCollisionAlgorithm", !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !10, i64 24, !10, i64 28, !10, i64 32, !10, i64 36}
!10 = !{!"int", !4, i64 0}
!11 = !{!9, !3, i64 8}
!12 = !{!9, !10, i64 24}
!13 = !{!9, !10, i64 28}
!14 = !{!9, !10, i64 32}
!15 = !{!9, !10, i64 36}
!16 = !{!17, !17, i64 0}
!17 = !{!"float", !4, i64 0}
!18 = !{!9, !3, i64 16}
!19 = !{!9, !3, i64 20}
!20 = !{!21, !3, i64 4}
!21 = !{!"_ZTS20btCollisionAlgorithm", !3, i64 4}
!22 = !{!23, !3, i64 8}
!23 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20}
!24 = !{!23, !3, i64 12}
!25 = !{!10, !10, i64 0}
!26 = !{!23, !3, i64 0}
!27 = !{!23, !3, i64 4}
!28 = !{!23, !10, i64 16}
!29 = !{!23, !10, i64 20}
!30 = !{!31, !3, i64 12}
!31 = !{!"_ZTS20btAlignedObjectArrayIiE", !32, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !33, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!33 = !{!"bool", !4, i64 0}
!34 = !{!31, !10, i64 4}
!35 = !{!36, !10, i64 4}
!36 = !{!"_ZTS20GIM_TRIANGLE_CONTACT", !17, i64 0, !10, i64 4, !37, i64 8, !4, i64 24}
!37 = !{!"_ZTS9btVector4"}
!38 = !{!36, !17, i64 0}
!39 = !{i64 0, i64 16, !40}
!40 = !{!4, !4, i64 0}
!41 = !{!42, !17, i64 64}
!42 = !{!"_ZTS19btPrimitiveTriangle", !4, i64 0, !37, i64 48, !17, i64 64, !17, i64 68}
!43 = !{!33, !33, i64 0}
!44 = !{!45, !10, i64 0}
!45 = !{!"_ZTS8GIM_PAIR", !10, i64 0, !10, i64 4}
!46 = !{!45, !10, i64 4}
!47 = !{i8 0, i8 2}
!48 = !{!49, !10, i64 4}
!49 = !{!"_ZTS20btAlignedObjectArrayI8GIM_PAIRE", !50, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !33, i64 16}
!50 = !{!"_ZTS18btAlignedAllocatorI8GIM_PAIRLj16EE"}
!51 = !{!49, !3, i64 12}
!52 = !{!53, !3, i64 0}
!53 = !{!"_ZTS18GIM_ShapeRetriever", !3, i64 0, !54, i64 4, !55, i64 108, !56, i64 268, !57, i64 276, !58, i64 284, !3, i64 292}
!54 = !{!"_ZTS17btTriangleShapeEx"}
!55 = !{!"_ZTS20btTetrahedronShapeEx"}
!56 = !{!"_ZTSN18GIM_ShapeRetriever19ChildShapeRetrieverE", !3, i64 4}
!57 = !{!"_ZTSN18GIM_ShapeRetriever22TriangleShapeRetrieverE"}
!58 = !{!"_ZTSN18GIM_ShapeRetriever19TetraShapeRetrieverE"}
!59 = !{!53, !3, i64 292}
!60 = !{!56, !3, i64 4}
!61 = !{!62, !10, i64 4}
!62 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!63 = !{!64, !3, i64 4}
!64 = !{!"_ZTS25btGImpactTriangleCallback", !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !33, i64 20, !17, i64 24}
!65 = !{!64, !3, i64 8}
!66 = !{!64, !3, i64 12}
!67 = !{!64, !3, i64 16}
!68 = !{!64, !33, i64 20}
!69 = !{!64, !17, i64 24}
!70 = !{!71, !3, i64 8}
!71 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !10, i64 16, !10, i64 20, !10, i64 24, !10, i64 28}
!72 = !{!71, !3, i64 12}
!73 = !{!74, !3, i64 64}
!74 = !{!"_ZTS20btCompoundShapeChild", !75, i64 0, !3, i64 64, !10, i64 68, !17, i64 72, !3, i64 76}
!75 = !{!"_ZTS11btTransform", !76, i64 0, !77, i64 48}
!76 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!77 = !{!"_ZTS9btVector3", !4, i64 0}
!78 = !{!79, !17, i64 64}
!79 = !{!"_ZTS18btStaticPlaneShape", !77, i64 16, !77, i64 32, !77, i64 48, !17, i64 64, !77, i64 68}
!80 = !{!"branch_weights", i32 1, i32 1048575}
!81 = !{!71, !3, i64 4}
!82 = !{!83, !10, i64 0}
!83 = !{!"_ZTS18btQuantizedBvhTree", !10, i64 0, !84, i64 4, !85, i64 24, !77, i64 56}
!84 = !{!"_ZTS28GIM_QUANTIZED_BVH_NODE_ARRAY"}
!85 = !{!"_ZTS6btAABB", !77, i64 0, !77, i64 16}
!86 = !{!49, !10, i64 8}
!87 = !{!49, !33, i64 16}
!88 = !{!89, !17, i64 44}
!89 = !{!"_ZTS21btConvexInternalShape", !77, i64 12, !77, i64 28, !17, i64 44, !17, i64 48}
!90 = !{!91, !17, i64 44}
!91 = !{!"_ZTS25btConvexInternalShapeData", !92, i64 0, !93, i64 12, !93, i64 28, !17, i64 44, !10, i64 48}
!92 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !10, i64 4, !4, i64 8}
!93 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!94 = !{!95, !10, i64 4}
!95 = !{!"_ZTS20btAlignedObjectArrayIP22btGImpactMeshShapePartE", !96, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !33, i64 16}
!96 = !{!"_ZTS18btAlignedAllocatorIP22btGImpactMeshShapePartLj16EE"}
!97 = !{!95, !3, i64 12}
!98 = !{!99, !10, i64 92}
!99 = !{!"_ZTS16btBU_Simplex1to4", !10, i64 92, !4, i64 96}
!100 = !{!101, !10, i64 4}
!101 = !{!"_ZTS20btAlignedObjectArrayI20btCompoundShapeChildE", !102, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !33, i64 16}
!102 = !{!"_ZTS18btAlignedAllocatorI20btCompoundShapeChildLj16EE"}
!103 = !{!101, !3, i64 12}
!104 = !{!105, !10, i64 40}
!105 = !{!"_ZTSN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE", !17, i64 4, !3, i64 8, !77, i64 12, !10, i64 28, !10, i64 32, !3, i64 36, !10, i64 40, !106, i64 44, !10, i64 48, !3, i64 52, !10, i64 56, !10, i64 60, !106, i64 64}
!106 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!107 = !{!105, !106, i64 44}
!108 = !{!105, !3, i64 36}
!109 = !{!105, !10, i64 48}
!110 = !{!111, !111, i64 0}
!111 = !{!"double", !4, i64 0}
!112 = !{!113, !33, i64 4}
!113 = !{!"_ZTS30btCollisionAlgorithmCreateFunc", !33, i64 4}
!114 = !{!115, !3, i64 0}
!115 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!116 = !{!117, !3, i64 12}
!117 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !118, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !33, i64 16}
!118 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!119 = !{!117, !10, i64 4}
!120 = !{!117, !10, i64 8}
!121 = !{!117, !33, i64 16}
!122 = !{!31, !10, i64 8}
!123 = !{!31, !33, i64 16}
