; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftRigidCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftRigidCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSoftRigidCollisionAlgorithm = type <{ %class.btCollisionAlgorithm, %class.btSoftBody*, %class.btCollisionObject*, i8, [3 x i8] }>
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.65, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.81, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.85 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type opaque
%struct.btSparseSdf = type { %class.btAlignedObjectArray.16, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.23 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.23 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.44, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.57 = type <{ %class.btAlignedAllocator.58, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.58 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.8, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%class.btAlignedObjectArray.65 = type <{ %class.btAlignedAllocator.66, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.66 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.69, %class.btAlignedObjectArray.73 }
%class.btAlignedObjectArray.69 = type <{ %class.btAlignedAllocator.70, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.70 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.73 = type <{ %class.btAlignedAllocator.74, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.74 = type { i8 }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.85 = type <{ %class.btAlignedAllocator.86, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.86 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btAlignedObjectArray.89 = type <{ %class.btAlignedAllocator.90, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.90 = type { i8 }

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv = comdat any

$_ZN10btSoftBody17getSoftBodySolverEv = comdat any

$_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

@_ZTV29btSoftRigidCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI29btSoftRigidCollisionAlgorithm to i8*), i8* bitcast (%class.btSoftRigidCollisionAlgorithm* (%class.btSoftRigidCollisionAlgorithm*)* @_ZN29btSoftRigidCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btSoftRigidCollisionAlgorithm*)* @_ZN29btSoftRigidCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btSoftRigidCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN29btSoftRigidCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btSoftRigidCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btSoftRigidCollisionAlgorithm*, %class.btAlignedObjectArray.89*)* @_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS29btSoftRigidCollisionAlgorithm = hidden constant [32 x i8] c"29btSoftRigidCollisionAlgorithm\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4
@_ZTI29btSoftRigidCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @_ZTS29btSoftRigidCollisionAlgorithm, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*) }, align 4

@_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b = hidden unnamed_addr alias %class.btSoftRigidCollisionAlgorithm* (%class.btSoftRigidCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btSoftRigidCollisionAlgorithm* (%class.btSoftRigidCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b
@_ZN29btSoftRigidCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btSoftRigidCollisionAlgorithm* (%class.btSoftRigidCollisionAlgorithm*), %class.btSoftRigidCollisionAlgorithm* (%class.btSoftRigidCollisionAlgorithm*)* @_ZN29btSoftRigidCollisionAlgorithmD2Ev

define hidden %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSoftRigidCollisionAlgorithm* returned %this, %class.btPersistentManifold* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper* %2, i1 zeroext %isSwapped) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  %.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %.addr1 = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr2 = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %.addr1, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %.addr2, align 4, !tbaa !2
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1, !tbaa !6
  %this3 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  %3 = bitcast %class.btSoftRigidCollisionAlgorithm* %this3 to %class.btCollisionAlgorithm*
  %4 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %3, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %4)
  %5 = bitcast %class.btSoftRigidCollisionAlgorithm* %this3 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV29btSoftRigidCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !8
  %m_isSwapped = getelementptr inbounds %class.btSoftRigidCollisionAlgorithm, %class.btSoftRigidCollisionAlgorithm* %this3, i32 0, i32 3
  %6 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %6 to i1
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %m_isSwapped, align 4, !tbaa !11
  ret %class.btSoftRigidCollisionAlgorithm* %this3
}

declare %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmD2Ev(%class.btSoftRigidCollisionAlgorithm* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btSoftRigidCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* %0) #6
  ret %class.btSoftRigidCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN29btSoftRigidCollisionAlgorithmD0Ev(%class.btSoftRigidCollisionAlgorithm* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btSoftRigidCollisionAlgorithm* @_ZN29btSoftRigidCollisionAlgorithmD1Ev(%class.btSoftRigidCollisionAlgorithm* %this1) #6
  %0 = bitcast %class.btSoftRigidCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

define hidden void @_ZN29btSoftRigidCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftRigidCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %softBody = alloca %class.btSoftBody*, align 4
  %rigidCollisionObjectWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %ref.tmp = alloca %class.btCollisionObject*, align 4
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %1 = bitcast %class.btSoftBody** %softBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %m_isSwapped = getelementptr inbounds %class.btSoftRigidCollisionAlgorithm, %class.btSoftRigidCollisionAlgorithm* %this1, i32 0, i32 3
  %2 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCollisionObject* %call to %class.btSoftBody*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %6 = bitcast %class.btCollisionObject* %call2 to %class.btSoftBody*
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btSoftBody* [ %4, %cond.true ], [ %6, %cond.false ]
  store %class.btSoftBody* %cond, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %7 = bitcast %struct.btCollisionObjectWrapper** %rigidCollisionObjectWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  %m_isSwapped3 = getelementptr inbounds %class.btSoftRigidCollisionAlgorithm, %class.btSoftRigidCollisionAlgorithm* %this1, i32 0, i32 3
  %8 = load i8, i8* %m_isSwapped3, align 4, !tbaa !11, !range !10
  %tobool4 = trunc i8 %8 to i1
  br i1 %tobool4, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %cond.end
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true5
  %cond8 = phi %struct.btCollisionObjectWrapper* [ %9, %cond.true5 ], [ %10, %cond.false6 ]
  store %struct.btCollisionObjectWrapper* %cond8, %struct.btCollisionObjectWrapper** %rigidCollisionObjectWrap, align 4, !tbaa !2
  %11 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %m_collisionDisabledObjects = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %11, i32 0, i32 1
  %12 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %rigidCollisionObjectWrap, align 4, !tbaa !2
  %call9 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %13)
  store %class.btCollisionObject* %call9, %class.btCollisionObject** %ref.tmp, align 4, !tbaa !2
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %m_collisionDisabledObjects, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp)
  %14 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %m_collisionDisabledObjects11 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %14, i32 0, i32 1
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionDisabledObjects11)
  %cmp = icmp eq i32 %call10, %call12
  %15 = bitcast %class.btCollisionObject** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #6
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end7
  %16 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %call13 = call %class.btSoftBodySolver* @_ZN10btSoftBody17getSoftBodySolverEv(%class.btSoftBody* %16)
  %17 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %rigidCollisionObjectWrap, align 4, !tbaa !2
  %19 = bitcast %class.btSoftBodySolver* %call13 to void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)***
  %vtable = load void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)**, void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)*** %19, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)*, void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)** %vtable, i64 9
  %20 = load void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)*, void (%class.btSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)** %vfn, align 4
  call void %20(%class.btSoftBodySolver* %call13, %class.btSoftBody* %17, %struct.btCollisionObjectWrapper* %18)
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end7
  %21 = bitcast %struct.btCollisionObjectWrapper** %rigidCollisionObjectWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #6
  %22 = bitcast %class.btSoftBody** %softBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !13
  ret %class.btCollisionObject* %0
}

define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btCollisionObject**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject** %key, %class.btCollisionObject*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !16
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !16
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !17
  %4 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btCollisionObject* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !16
  store i32 %8, i32* %index, align 4, !tbaa !16
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !16
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #6
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret i32 %10
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !20
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: nounwind
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN10btSoftBody17getSoftBodySolverEv(%class.btSoftBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSoftBody*, align 4
  store %class.btSoftBody* %this, %class.btSoftBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBody*, %class.btSoftBody** %this.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %this1, i32 0, i32 2
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !21
  ret %class.btSoftBodySolver* %0
}

; Function Attrs: nounwind
define hidden float @_ZN29btSoftRigidCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftRigidCollisionAlgorithm* %this, %class.btCollisionObject* %col0, %class.btCollisionObject* %col1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  %col0.addr = alloca %class.btCollisionObject*, align 4
  %col1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %col0, %class.btCollisionObject** %col0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %col1, %class.btCollisionObject** %col1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  ret float 1.000000e+00
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN29btSoftRigidCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btSoftRigidCollisionAlgorithm* %this, %class.btAlignedObjectArray.89* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSoftRigidCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btSoftRigidCollisionAlgorithm* %this, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.89* %manifoldArray, %class.btAlignedObjectArray.89** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidCollisionAlgorithm*, %class.btSoftRigidCollisionAlgorithm** %this.addr, align 4
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !7, i64 16}
!12 = !{!"_ZTS29btSoftRigidCollisionAlgorithm", !3, i64 8, !3, i64 12, !7, i64 16}
!13 = !{!14, !3, i64 8}
!14 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !15, i64 16, !15, i64 20}
!15 = !{!"int", !4, i64 0}
!16 = !{!15, !15, i64 0}
!17 = !{!18, !3, i64 12}
!18 = !{!"_ZTS20btAlignedObjectArrayIPK17btCollisionObjectE", !19, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorIPK17btCollisionObjectLj16EE"}
!20 = !{!18, !15, i64 4}
!21 = !{!22, !3, i64 284}
!22 = !{!"_ZTS10btSoftBody", !18, i64 264, !3, i64 284, !23, i64 288, !30, i64 452, !31, i64 472, !3, i64 680, !3, i64 684, !38, i64 688, !40, i64 708, !42, i64 728, !44, i64 748, !46, i64 768, !48, i64 788, !50, i64 808, !52, i64 828, !54, i64 848, !56, i64 868, !25, i64 888, !4, i64 892, !7, i64 924, !58, i64 928, !58, i64 988, !58, i64 1048, !63, i64 1108, !65, i64 1128, !67, i64 1148, !36, i64 1212, !25, i64 1228, !68, i64 1232}
!23 = !{!"_ZTSN10btSoftBody6ConfigE", !24, i64 0, !25, i64 4, !25, i64 8, !25, i64 12, !25, i64 16, !25, i64 20, !25, i64 24, !25, i64 28, !25, i64 32, !25, i64 36, !25, i64 40, !25, i64 44, !25, i64 48, !25, i64 52, !25, i64 56, !25, i64 60, !25, i64 64, !25, i64 68, !25, i64 72, !25, i64 76, !25, i64 80, !15, i64 84, !15, i64 88, !15, i64 92, !15, i64 96, !15, i64 100, !26, i64 104, !28, i64 124, !28, i64 144}
!24 = !{!"_ZTSN10btSoftBody10eAeroModel1_E", !4, i64 0}
!25 = !{!"float", !4, i64 0}
!26 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8eVSolver1_EE", !27, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!27 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8eVSolver1_ELj16EE"}
!28 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8ePSolver1_EE", !29, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!29 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8ePSolver1_ELj16EE"}
!30 = !{!"_ZTSN10btSoftBody11SolverStateE", !25, i64 0, !25, i64 4, !25, i64 8, !25, i64 12, !25, i64 16}
!31 = !{!"_ZTSN10btSoftBody4PoseE", !7, i64 0, !7, i64 1, !25, i64 4, !32, i64 8, !34, i64 28, !36, i64 48, !37, i64 64, !37, i64 112, !37, i64 160}
!32 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !33, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!33 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!34 = !{!"_ZTS20btAlignedObjectArrayIfE", !35, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!35 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!36 = !{!"_ZTS9btVector3", !4, i64 0}
!37 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!38 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NoteEE", !39, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!39 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NoteELj16EE"}
!40 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !41, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!41 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!42 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4LinkEE", !43, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!43 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4LinkELj16EE"}
!44 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4FaceEE", !45, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!45 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4FaceELj16EE"}
!46 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody5TetraEE", !47, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!47 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody5TetraELj16EE"}
!48 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody6AnchorEE", !49, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!49 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody6AnchorELj16EE"}
!50 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8RContactEE", !51, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!51 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8RContactELj16EE"}
!52 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8SContactEE", !53, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8SContactELj16EE"}
!54 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody5JointEE", !55, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!55 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody5JointELj16EE"}
!56 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody8MaterialEE", !57, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!57 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody8MaterialELj16EE"}
!58 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !59, i64 20, !61, i64 40}
!59 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !60, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!60 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!61 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !62, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!62 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!63 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody7ClusterEE", !64, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!64 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody7ClusterELj16EE"}
!65 = !{!"_ZTS20btAlignedObjectArrayIbE", !66, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!66 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!67 = !{!"_ZTS11btTransform", !37, i64 0, !36, i64 48}
!68 = !{!"_ZTS20btAlignedObjectArrayIiE", !69, i64 0, !15, i64 4, !15, i64 8, !3, i64 12, !7, i64 16}
!69 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
