; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btUnionFind.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btUnionFind.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btUnionFind = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btUnionFindElementSortPredicate = type { i8 }

$_ZN20btAlignedObjectArrayI9btElementED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4sizeEv = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_ = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btElementE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btElementE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii = comdat any

$_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementE4swapEii = comdat any

@_ZN11btUnionFindD1Ev = hidden unnamed_addr alias %class.btUnionFind* (%class.btUnionFind*), %class.btUnionFind* (%class.btUnionFind*)* @_ZN11btUnionFindD2Ev
@_ZN11btUnionFindC1Ev = hidden unnamed_addr alias %class.btUnionFind* (%class.btUnionFind*), %class.btUnionFind* (%class.btUnionFind*)* @_ZN11btUnionFindC2Ev

; Function Attrs: nounwind
define hidden %class.btUnionFind* @_ZN11btUnionFindD2Ev(%class.btUnionFind* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  call void @_ZN11btUnionFind4FreeEv(%class.btUnionFind* %this1)
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementED2Ev(%class.btAlignedObjectArray* %m_elements) #7
  ret %class.btUnionFind* %this1
}

define hidden void @_ZN11btUnionFind4FreeEv(%class.btUnionFind* %this) #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %m_elements)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden %class.btUnionFind* @_ZN11btUnionFindC2Ev(%class.btUnionFind* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementEC2Ev(%class.btAlignedObjectArray* %m_elements)
  ret %class.btUnionFind* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btElementEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden void @_ZN11btUnionFind8allocateEi(%class.btUnionFind* %this, i32 %N) #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %N.addr = alloca i32, align 4
  %ref.tmp = alloca %struct.btElement, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %N, i32* %N.addr, align 4, !tbaa !6
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %0 = load i32, i32* %N.addr, align 4, !tbaa !6
  %1 = bitcast %struct.btElement* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %1) #7
  %2 = bitcast %struct.btElement* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %2, i8 0, i32 8, i1 false)
  call void @_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_elements, i32 %0, %struct.btElement* nonnull align 4 dereferenceable(8) %ref.tmp)
  %3 = bitcast %struct.btElement* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %3) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btElement* nonnull align 4 dereferenceable(8) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btElement*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store %struct.btElement* %fillData, %struct.btElement** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btElementE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #7
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #7
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %struct.btElement*, %struct.btElement** %m_data11, align 4, !tbaa !8
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds %struct.btElement, %struct.btElement* %18, i32 %19
  %20 = bitcast %struct.btElement* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %struct.btElement*
  %22 = load %struct.btElement*, %struct.btElement** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btElement* %21 to i8*
  %24 = bitcast %struct.btElement* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 8, i1 false), !tbaa.struct !12
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %25, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !13
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #7
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

define hidden void @_ZN11btUnionFind5resetEi(%class.btUnionFind* %this, i32 %N) #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %N.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %N, i32* %N.addr, align 4, !tbaa !6
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = load i32, i32* %N.addr, align 4, !tbaa !6
  call void @_ZN11btUnionFind8allocateEi(%class.btUnionFind* %this1, i32 %0)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %N.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %6)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  store i32 %5, i32* %m_id, align 4, !tbaa !14
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %7)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 1
  store i32 1, i32* %m_sz, align 4, !tbaa !16
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

define hidden void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind* %this) #1 {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %numElements = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btUnionFindElementSortPredicate, align 1
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = bitcast i32* %numElements to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %m_elements)
  store i32 %call, i32* %numElements, align 4, !tbaa !6
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %numElements, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #7
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %5)
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %6)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  store i32 %call2, i32* %m_id, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_elements5 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = bitcast %class.btUnionFindElementSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %8) #7
  call void @_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_(%class.btAlignedObjectArray* %m_elements5, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %9 = bitcast %class.btUnionFindElementSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %9) #7
  %10 = bitcast i32* %numElements to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !13
  ret i32 %0
}

define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #1 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4, !tbaa !2
  store i32 %x, i32* %x.addr, align 4, !tbaa !6
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4, !tbaa !6
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4, !tbaa !14
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #7
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %4 = load i32, i32* %x.addr, align 4, !tbaa !6
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %4)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %5 = load i32, i32* %m_id5, align 4, !tbaa !14
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %5)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %6 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %6, i32 0, i32 0
  %7 = load i32, i32* %m_id7, align 4, !tbaa !14
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %x.addr, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements8, i32 %8)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %7, i32* %m_id10, align 4, !tbaa !14
  %9 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4, !tbaa !2
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %9, i32 0, i32 0
  %10 = load i32, i32* %m_id11, align 4, !tbaa !14
  store i32 %10, i32* %x.addr, align 4, !tbaa !6
  %11 = bitcast %struct.btElement** %elementPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = load i32, i32* %x.addr, align 4, !tbaa !6
  ret i32 %12
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE9quickSortI31btUnionFindElementSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btUnionFindElementSortPredicate* %CompareFunc, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btElementLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE4initEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !17
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* null, %struct.btElement** %m_data, align 4, !tbaa !8
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !13
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !18
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btElement*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btElement** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btElementE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btElement*
  store %struct.btElement* %3, %struct.btElement** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btElement*, %struct.btElement** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btElement* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !17
  %5 = load %struct.btElement*, %struct.btElement** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* %5, %struct.btElement** %m_data, align 4, !tbaa !8
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !18
  %7 = bitcast %struct.btElement** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE8capacityEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !18
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btElementE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btElement* @_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btElement** null)
  %2 = bitcast %struct.btElement* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btElementE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btElement* %dest) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btElement*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btElement* %dest, %struct.btElement** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btElement*, %struct.btElement** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %4, i32 %5
  %6 = bitcast %struct.btElement* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btElement*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 %9
  %10 = bitcast %struct.btElement* %7 to i8*
  %11 = bitcast %struct.btElement* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 8, i1 false), !tbaa.struct !12
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %tobool = icmp ne %struct.btElement* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !17, !range !19
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btElement*, %struct.btElement** %m_data4, align 4, !tbaa !8
  call void @_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btElement* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btElement* null, %struct.btElement** %m_data5, align 4, !tbaa !8
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btElement* @_ZN18btAlignedAllocatorI9btElementLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btElement** %hint) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btElement**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btElement** %hint, %struct.btElement*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btElement*
  ret %struct.btElement* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btElementLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btElement* %ptr) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btElement*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btElement* %ptr, %struct.btElement** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btElement*, %struct.btElement** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btElement* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btElement, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btUnionFindElementSortPredicate* %CompareFunc, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !6
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #7
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !6
  store i32 %3, i32* %j, align 4, !tbaa !6
  %4 = bitcast %struct.btElement* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %4) #7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 %div
  %8 = bitcast %struct.btElement* %x to i8*
  %9 = bitcast %struct.btElement* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 8, i1 false), !tbaa.struct !12
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %10 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %11 = load %struct.btElement*, %struct.btElement** %m_data2, align 4, !tbaa !8
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btElement, %struct.btElement* %11, i32 %12
  %call = call zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %10, %struct.btElement* nonnull align 4 dereferenceable(8) %arrayidx3, %struct.btElement* nonnull align 4 dereferenceable(8) %x)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %14 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %15 = load %struct.btElement*, %struct.btElement** %m_data5, align 4, !tbaa !8
  %16 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx6 = getelementptr inbounds %struct.btElement, %struct.btElement* %15, i32 %16
  %call7 = call zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %14, %struct.btElement* nonnull align 4 dereferenceable(8) %x, %struct.btElement* nonnull align 4 dereferenceable(8) %arrayidx6)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %19 = load i32, i32* %j, align 4, !tbaa !6
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4, !tbaa !6
  %21 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btElementE4swapEii(%class.btAlignedObjectArray* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4, !tbaa !6
  %23 = load i32, i32* %j, align 4, !tbaa !6
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4, !tbaa !6
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4, !tbaa !6
  %25 = load i32, i32* %j, align 4, !tbaa !6
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %27 = load i32, i32* %j, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %29 = load i32, i32* %lo.addr, align 4, !tbaa !6
  %30 = load i32, i32* %j, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4, !tbaa !6
  %32 = load i32, i32* %hi.addr, align 4, !tbaa !6
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %34 = load i32, i32* %i, align 4, !tbaa !6
  %35 = load i32, i32* %hi.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayI9btElementE17quickSortInternalI31btUnionFindElementSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btUnionFindElementSortPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  %36 = bitcast %struct.btElement* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #7
  %37 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #7
  %38 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK31btUnionFindElementSortPredicateclERK9btElementS2_(%class.btUnionFindElementSortPredicate* %this, %struct.btElement* nonnull align 4 dereferenceable(8) %lhs, %struct.btElement* nonnull align 4 dereferenceable(8) %rhs) #0 comdat {
entry:
  %this.addr = alloca %class.btUnionFindElementSortPredicate*, align 4
  %lhs.addr = alloca %struct.btElement*, align 4
  %rhs.addr = alloca %struct.btElement*, align 4
  store %class.btUnionFindElementSortPredicate* %this, %class.btUnionFindElementSortPredicate** %this.addr, align 4, !tbaa !2
  store %struct.btElement* %lhs, %struct.btElement** %lhs.addr, align 4, !tbaa !2
  store %struct.btElement* %rhs, %struct.btElement** %rhs.addr, align 4, !tbaa !2
  %this1 = load %class.btUnionFindElementSortPredicate*, %class.btUnionFindElementSortPredicate** %this.addr, align 4
  %0 = load %struct.btElement*, %struct.btElement** %lhs.addr, align 4, !tbaa !2
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 0, i32 0
  %1 = load i32, i32* %m_id, align 4, !tbaa !14
  %2 = load %struct.btElement*, %struct.btElement** %rhs.addr, align 4, !tbaa !2
  %m_id2 = getelementptr inbounds %struct.btElement, %struct.btElement* %2, i32 0, i32 0
  %3 = load i32, i32* %m_id2, align 4, !tbaa !14
  %cmp = icmp slt i32 %1, %3
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btElementE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btElement, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast %struct.btElement* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btElement*, %struct.btElement** %m_data, align 4, !tbaa !8
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %1, i32 %2
  %3 = bitcast %struct.btElement* %temp to i8*
  %4 = bitcast %struct.btElement* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 8, i1 false), !tbaa.struct !12
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btElement*, %struct.btElement** %m_data2, align 4, !tbaa !8
  %6 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 %6
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btElement*, %struct.btElement** %m_data4, align 4, !tbaa !8
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %arrayidx5 = getelementptr inbounds %struct.btElement, %struct.btElement* %7, i32 %8
  %9 = bitcast %struct.btElement* %arrayidx5 to i8*
  %10 = bitcast %struct.btElement* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false), !tbaa.struct !12
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %11 = load %struct.btElement*, %struct.btElement** %m_data6, align 4, !tbaa !8
  %12 = load i32, i32* %index1.addr, align 4, !tbaa !6
  %arrayidx7 = getelementptr inbounds %struct.btElement, %struct.btElement* %11, i32 %12
  %13 = bitcast %struct.btElement* %arrayidx7 to i8*
  %14 = bitcast %struct.btElement* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 8, i1 false), !tbaa.struct !12
  %15 = bitcast %struct.btElement* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %15) #7
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !3, i64 12}
!9 = !{!"_ZTS20btAlignedObjectArrayI9btElementE", !10, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !11, i64 16}
!10 = !{!"_ZTS18btAlignedAllocatorI9btElementLj16EE"}
!11 = !{!"bool", !4, i64 0}
!12 = !{i64 0, i64 4, !6, i64 4, i64 4, !6}
!13 = !{!9, !7, i64 4}
!14 = !{!15, !7, i64 0}
!15 = !{!"_ZTS9btElement", !7, i64 0, !7, i64 4}
!16 = !{!15, !7, i64 4}
!17 = !{!9, !11, i64 16}
!18 = !{!9, !7, i64 8}
!19 = !{i8 0, i8 2}
