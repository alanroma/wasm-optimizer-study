; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/btSimpleDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Dynamics/btSimpleDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSimpleDynamicsWorld = type { %class.btDynamicsWorld, %class.btConstraintSolver*, i8, %class.btVector3 }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%class.btVector3 = type { [4 x float] }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type { i32 (...)** }
%class.btCollisionConfiguration = type opaque
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionDispatcher = type { %class.btDispatcher, i32, %class.btAlignedObjectArray.0, %class.btManifoldResult, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, %class.btPoolAllocator*, %class.btPoolAllocator*, [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], %class.btCollisionConfiguration* }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.4 }
%class.btCollisionAlgorithm = type opaque
%union.anon.4 = type { i8* }
%class.btPoolAllocator = type opaque
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.5, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%union.anon.5 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.6, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btActionInterface = type opaque
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i16, i16, i32 }
%class.btSerializer = type opaque

$_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN16btCollisionWorld15getDispatchInfoEv = comdat any

$_ZN19btContactSolverInfoC2Ev = comdat any

$_ZN16btCollisionWorld23getCollisionObjectArrayEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK16btCollisionWorld22getNumCollisionObjectsEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN11btRigidBody6upcastEP17btCollisionObject = comdat any

$_ZN11btRigidBody11clearForcesEv = comdat any

$_ZN11btRigidBody17getCollisionShapeEv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK17btCollisionObject14isStaticObjectEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN16btCollisionWorld13getBroadphaseEv = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN17btCollisionObject30getInterpolationWorldTransformEv = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb = comdat any

$_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint = comdat any

$_ZNK15btDynamicsWorld17getNumConstraintsEv = comdat any

$_ZN15btDynamicsWorld13getConstraintEi = comdat any

$_ZNK15btDynamicsWorld13getConstraintEi = comdat any

$_ZNK21btSimpleDynamicsWorld12getWorldTypeEv = comdat any

$_ZN15btDynamicsWorld10addVehicleEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld12addCharacterEP17btActionInterface = comdat any

$_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface = comdat any

$_ZN15btDynamicsWorldD0Ev = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZTS15btDynamicsWorld = comdat any

$_ZTI15btDynamicsWorld = comdat any

$_ZTV15btDynamicsWorld = comdat any

@_ZTV21btSimpleDynamicsWorld = hidden unnamed_addr constant { [37 x i8*] } { [37 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btSimpleDynamicsWorld to i8*), i8* bitcast (%class.btSimpleDynamicsWorld* (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)* @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btCollisionObject*)* @_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btSerializer*)* @_ZN16btCollisionWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btSimpleDynamicsWorld*, float, i32, float)* @_ZN21btSimpleDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*)* @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btActionInterface*)* @_ZN21btSimpleDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btActionInterface*)* @_ZN21btSimpleDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btVector3*)* @_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btSimpleDynamicsWorld*)* @_ZNK21btSimpleDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)* @_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*, i16, i16)* @_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBodyss to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)* @_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*, %class.btConstraintSolver*)* @_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDynamicsWorld*)* @_ZNK15btDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZN15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZNK15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btSimpleDynamicsWorld*)* @_ZNK21btSimpleDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btSimpleDynamicsWorld = hidden constant [24 x i8] c"21btSimpleDynamicsWorld\00", align 1
@_ZTS15btDynamicsWorld = linkonce_odr hidden constant [18 x i8] c"15btDynamicsWorld\00", comdat, align 1
@_ZTI16btCollisionWorld = external constant i8*
@_ZTI15btDynamicsWorld = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionWorld to i8*) }, comdat, align 4
@_ZTI21btSimpleDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btSimpleDynamicsWorld, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btDynamicsWorld to i8*) }, align 4
@_ZTV15btDynamicsWorld = linkonce_odr hidden unnamed_addr constant { [37 x i8*] } { [37 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btDynamicsWorld to i8*), i8* bitcast (%class.btCollisionWorld* (%class.btCollisionWorld*)* @_ZN16btCollisionWorldD2Ev to i8*), i8* bitcast (void (%class.btDynamicsWorld*)* @_ZN15btDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)* @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btCollisionObject*)* @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btSerializer*)* @_ZN16btCollisionWorld9serializeEP12btSerializer to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btTypedConstraint*)* @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i32 (%class.btDynamicsWorld*)* @_ZNK15btDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZN15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDynamicsWorld*, i32)* @_ZNK15btDynamicsWorld13getConstraintEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDynamicsWorld*, %class.btActionInterface*)* @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface to i8*)] }, comdat, align 4

@_ZN21btSimpleDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = hidden unnamed_addr alias %class.btSimpleDynamicsWorld* (%class.btSimpleDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*), %class.btSimpleDynamicsWorld* (%class.btSimpleDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*)* @_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
@_ZN21btSimpleDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btSimpleDynamicsWorld* (%class.btSimpleDynamicsWorld*), %class.btSimpleDynamicsWorld* (%class.btSimpleDynamicsWorld*)* @_ZN21btSimpleDynamicsWorldD2Ev

; Function Attrs: nounwind
define hidden void @btBulletDynamicsProbe() #0 {
entry:
  ret void
}

define hidden %class.btSimpleDynamicsWorld* @_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btSimpleDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  store %class.btConstraintSolver* %constraintSolver, %class.btConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  %3 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %call = call %class.btDynamicsWorld* @_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btCollisionConfiguration* %3)
  %4 = bitcast %class.btSimpleDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [37 x i8*] }, { [37 x i8*] }* @_ZTV21btSimpleDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  %m_constraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %5 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  store %class.btConstraintSolver* %5, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !8
  %m_ownsConstraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 2
  store i8 0, i8* %m_ownsConstraintSolver, align 4, !tbaa !12
  %m_gravity = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 3
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !13
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !13
  %8 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float -1.000000e+01, float* %ref.tmp3, align 4, !tbaa !13
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret %class.btSimpleDynamicsWorld* %this1
}

define linkonce_odr hidden %class.btDynamicsWorld* @_ZN15btDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %broadphase, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %broadphase.addr = alloca %class.btBroadphaseInterface*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %broadphase, %class.btBroadphaseInterface** %broadphase.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %broadphase.addr, align 4, !tbaa !2
  %3 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %call = call %class.btCollisionWorld* @_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btCollisionWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btCollisionConfiguration* %3)
  %4 = bitcast %class.btDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [37 x i8*] }, { [37 x i8*] }* @_ZTV15btDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !6
  %m_internalTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 1
  store void (%class.btDynamicsWorld*, float)* null, void (%class.btDynamicsWorld*, float)** %m_internalTickCallback, align 4, !tbaa !15
  %m_internalPreTickCallback = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 2
  store void (%class.btDynamicsWorld*, float)* null, void (%class.btDynamicsWorld*, float)** %m_internalPreTickCallback, align 4, !tbaa !18
  %m_worldUserInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 3
  store i8* null, i8** %m_worldUserInfo, align 4, !tbaa !19
  %m_solverInfo = getelementptr inbounds %class.btDynamicsWorld, %class.btDynamicsWorld* %this1, i32 0, i32 4
  %call2 = call %struct.btContactSolverInfo* @_ZN19btContactSolverInfoC2Ev(%struct.btContactSolverInfo* %m_solverInfo)
  ret %class.btDynamicsWorld* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !13
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !13
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !13
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !13
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !13
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !13
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !13
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btSimpleDynamicsWorld* @_ZN21btSimpleDynamicsWorldD2Ev(%class.btSimpleDynamicsWorld* returned %this) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSimpleDynamicsWorld*, align 4
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  store %class.btSimpleDynamicsWorld* %this1, %class.btSimpleDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btSimpleDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [37 x i8*] }, { [37 x i8*] }* @_ZTV21btSimpleDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_ownsConstraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 2
  %1 = load i8, i8* %m_ownsConstraintSolver, align 4, !tbaa !12, !range !20
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_constraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %2 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !8
  %3 = bitcast %class.btConstraintSolver* %2 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btDynamicsWorld*
  %call = call %class.btDynamicsWorld* bitcast (%class.btCollisionWorld* (%class.btCollisionWorld*)* @_ZN16btCollisionWorldD2Ev to %class.btDynamicsWorld* (%class.btDynamicsWorld*)*)(%class.btDynamicsWorld* %4) #9
  %5 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %retval, align 4
  ret %class.btSimpleDynamicsWorld* %5
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: nounwind
define hidden void @_ZN21btSimpleDynamicsWorldD0Ev(%class.btSimpleDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %call = call %class.btSimpleDynamicsWorld* @_ZN21btSimpleDynamicsWorldD1Ev(%class.btSimpleDynamicsWorld* %this1) #9
  %0 = bitcast %class.btSimpleDynamicsWorld* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

define hidden i32 @_ZN21btSimpleDynamicsWorld14stepSimulationEfif(%class.btSimpleDynamicsWorld* %this, float %timeStep, i32 %maxSubSteps, float %fixedTimeStep) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %maxSubSteps.addr = alloca i32, align 4
  %fixedTimeStep.addr = alloca float, align 4
  %dispatchInfo = alloca %struct.btDispatcherInfo*, align 4
  %numManifolds = alloca i32, align 4
  %manifoldPtr = alloca %class.btPersistentManifold**, align 4
  %infoGlobal = alloca %struct.btContactSolverInfo, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !13
  store i32 %maxSubSteps, i32* %maxSubSteps.addr, align 4, !tbaa !21
  store float %fixedTimeStep, float* %fixedTimeStep.addr, align 4, !tbaa !13
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4, !tbaa !13
  call void @_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf(%class.btSimpleDynamicsWorld* %this1, float %0)
  %1 = bitcast %struct.btDispatcherInfo** %dispatchInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call = call nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %2)
  store %struct.btDispatcherInfo* %call, %struct.btDispatcherInfo** %dispatchInfo, align 4, !tbaa !2
  %3 = load float, float* %timeStep.addr, align 4, !tbaa !13
  %4 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4, !tbaa !2
  %m_timeStep = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %4, i32 0, i32 0
  store float %3, float* %m_timeStep, align 4, !tbaa !23
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4, !tbaa !2
  %m_stepCount = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %5, i32 0, i32 1
  store i32 0, i32* %m_stepCount, align 4, !tbaa !25
  %6 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %7 = bitcast %class.btCollisionWorld* %6 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %7, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %8 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call2 = call %class.btIDebugDraw* %8(%class.btCollisionWorld* %6)
  %9 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo, align 4, !tbaa !2
  %m_debugDraw = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %9, i32 0, i32 5
  store %class.btIDebugDraw* %call2, %class.btIDebugDraw** %m_debugDraw, align 4, !tbaa !26
  %10 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %11 = bitcast %class.btCollisionWorld* %10 to void (%class.btCollisionWorld*)***
  %vtable3 = load void (%class.btCollisionWorld*)**, void (%class.btCollisionWorld*)*** %11, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds void (%class.btCollisionWorld*)*, void (%class.btCollisionWorld*)** %vtable3, i64 11
  %12 = load void (%class.btCollisionWorld*)*, void (%class.btCollisionWorld*)** %vfn4, align 4
  call void %12(%class.btCollisionWorld* %10)
  %13 = bitcast i32* %numManifolds to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %14, i32 0, i32 2
  %15 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !27
  %16 = bitcast %class.btDispatcher* %15 to i32 (%class.btDispatcher*)***
  %vtable5 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %16, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable5, i64 9
  %17 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn6, align 4
  %call7 = call i32 %17(%class.btDispatcher* %15)
  store i32 %call7, i32* %numManifolds, align 4, !tbaa !21
  %18 = load i32, i32* %numManifolds, align 4, !tbaa !21
  %tobool = icmp ne i32 %18, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %19 = bitcast %class.btPersistentManifold*** %manifoldPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher18 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %20, i32 0, i32 2
  %21 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher18, align 4, !tbaa !27
  %22 = bitcast %class.btDispatcher* %21 to %class.btCollisionDispatcher*
  %23 = bitcast %class.btCollisionDispatcher* %22 to %class.btPersistentManifold** (%class.btCollisionDispatcher*)***
  %vtable9 = load %class.btPersistentManifold** (%class.btCollisionDispatcher*)**, %class.btPersistentManifold** (%class.btCollisionDispatcher*)*** %23, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds %class.btPersistentManifold** (%class.btCollisionDispatcher*)*, %class.btPersistentManifold** (%class.btCollisionDispatcher*)** %vtable9, i64 11
  %24 = load %class.btPersistentManifold** (%class.btCollisionDispatcher*)*, %class.btPersistentManifold** (%class.btCollisionDispatcher*)** %vfn10, align 4
  %call11 = call %class.btPersistentManifold** %24(%class.btCollisionDispatcher* %22)
  store %class.btPersistentManifold** %call11, %class.btPersistentManifold*** %manifoldPtr, align 4, !tbaa !2
  %25 = bitcast %struct.btContactSolverInfo* %infoGlobal to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %25) #9
  %call12 = call %struct.btContactSolverInfo* @_ZN19btContactSolverInfoC2Ev(%struct.btContactSolverInfo* %infoGlobal)
  %26 = load float, float* %timeStep.addr, align 4, !tbaa !13
  %27 = bitcast %struct.btContactSolverInfo* %infoGlobal to %struct.btContactSolverInfoData*
  %m_timeStep13 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %27, i32 0, i32 3
  store float %26, float* %m_timeStep13, align 4, !tbaa !31
  %m_constraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %28 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !8
  %29 = load i32, i32* %numManifolds, align 4, !tbaa !21
  %30 = bitcast %class.btConstraintSolver* %28 to void (%class.btConstraintSolver*, i32, i32)***
  %vtable14 = load void (%class.btConstraintSolver*, i32, i32)**, void (%class.btConstraintSolver*, i32, i32)*** %30, align 4, !tbaa !6
  %vfn15 = getelementptr inbounds void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vtable14, i64 2
  %31 = load void (%class.btConstraintSolver*, i32, i32)*, void (%class.btConstraintSolver*, i32, i32)** %vfn15, align 4
  call void %31(%class.btConstraintSolver* %28, i32 0, i32 %29)
  %m_constraintSolver16 = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %32 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver16, align 4, !tbaa !8
  %33 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call17 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %33)
  %call18 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %call17, i32 0)
  %34 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call19 = call i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %34)
  %35 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifoldPtr, align 4, !tbaa !2
  %36 = load i32, i32* %numManifolds, align 4, !tbaa !21
  %37 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %37, i32 0, i32 5
  %38 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !33
  %39 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher120 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %39, i32 0, i32 2
  %40 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher120, align 4, !tbaa !27
  %41 = bitcast %class.btConstraintSolver* %32 to float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)***
  %vtable21 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)**, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*** %41, align 4, !tbaa !6
  %vfn22 = getelementptr inbounds float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vtable21, i64 3
  %42 = load float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)*, float (%class.btConstraintSolver*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, %struct.btContactSolverInfo*, %class.btIDebugDraw*, %class.btDispatcher*)** %vfn22, align 4
  %call23 = call float %42(%class.btConstraintSolver* %32, %class.btCollisionObject** %call18, i32 %call19, %class.btPersistentManifold** %35, i32 %36, %class.btTypedConstraint** null, i32 0, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %38, %class.btDispatcher* %40)
  %m_constraintSolver24 = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %43 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver24, align 4, !tbaa !8
  %44 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer25 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %44, i32 0, i32 5
  %45 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer25, align 4, !tbaa !33
  %46 = bitcast %class.btConstraintSolver* %43 to void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)***
  %vtable26 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)**, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*** %46, align 4, !tbaa !6
  %vfn27 = getelementptr inbounds void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vtable26, i64 4
  %47 = load void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)*, void (%class.btConstraintSolver*, %struct.btContactSolverInfo*, %class.btIDebugDraw*)** %vfn27, align 4
  call void %47(%class.btConstraintSolver* %43, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal, %class.btIDebugDraw* %45)
  %48 = bitcast %struct.btContactSolverInfo* %infoGlobal to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %48) #9
  %49 = bitcast %class.btPersistentManifold*** %manifoldPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %50 = load float, float* %timeStep.addr, align 4, !tbaa !13
  call void @_ZN21btSimpleDynamicsWorld19integrateTransformsEf(%class.btSimpleDynamicsWorld* %this1, float %50)
  %51 = bitcast %class.btSimpleDynamicsWorld* %this1 to void (%class.btSimpleDynamicsWorld*)***
  %vtable28 = load void (%class.btSimpleDynamicsWorld*)**, void (%class.btSimpleDynamicsWorld*)*** %51, align 4, !tbaa !6
  %vfn29 = getelementptr inbounds void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vtable28, i64 2
  %52 = load void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vfn29, align 4
  call void %52(%class.btSimpleDynamicsWorld* %this1)
  %53 = bitcast %class.btSimpleDynamicsWorld* %this1 to void (%class.btSimpleDynamicsWorld*)***
  %vtable30 = load void (%class.btSimpleDynamicsWorld*)**, void (%class.btSimpleDynamicsWorld*)*** %53, align 4, !tbaa !6
  %vfn31 = getelementptr inbounds void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vtable30, i64 20
  %54 = load void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vfn31, align 4
  call void %54(%class.btSimpleDynamicsWorld* %this1)
  %55 = bitcast %class.btSimpleDynamicsWorld* %this1 to void (%class.btSimpleDynamicsWorld*)***
  %vtable32 = load void (%class.btSimpleDynamicsWorld*)**, void (%class.btSimpleDynamicsWorld*)*** %55, align 4, !tbaa !6
  %vfn33 = getelementptr inbounds void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vtable32, i64 30
  %56 = load void (%class.btSimpleDynamicsWorld*)*, void (%class.btSimpleDynamicsWorld*)** %vfn33, align 4
  call void %56(%class.btSimpleDynamicsWorld* %this1)
  %57 = bitcast i32* %numManifolds to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #9
  %58 = bitcast %struct.btDispatcherInfo** %dispatchInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  ret i32 1
}

define hidden void @_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf(%class.btSimpleDynamicsWorld* %this, float %timeStep) #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !13
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !21
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %5, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !21
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %8 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call4 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %9)
  store %class.btRigidBody* %call4, %class.btRigidBody** %body, align 4, !tbaa !2
  %10 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %10, null
  br i1 %tobool, label %if.then, label %if.end11

if.then:                                          ; preds = %for.body
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %12 = bitcast %class.btRigidBody* %11 to %class.btCollisionObject*
  %call5 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %12)
  br i1 %call5, label %if.end10, label %if.then6

if.then6:                                         ; preds = %if.then
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %14 = bitcast %class.btRigidBody* %13 to %class.btCollisionObject*
  %call7 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %14)
  br i1 %call7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.then6
  %15 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  call void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody* %15)
  %16 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %17 = load float, float* %timeStep.addr, align 4, !tbaa !13
  call void @_ZN11btRigidBody19integrateVelocitiesEf(%class.btRigidBody* %16, float %17)
  %18 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %19 = load float, float* %timeStep.addr, align 4, !tbaa !13
  call void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody* %18, float %19)
  %20 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %21 = load float, float* %timeStep.addr, align 4, !tbaa !13
  %22 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %23 = bitcast %class.btRigidBody* %22 to %class.btCollisionObject*
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %23)
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %20, float %21, %class.btTransform* nonnull align 4 dereferenceable(64) %call9)
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then6
  br label %if.end10

if.end10:                                         ; preds = %if.end, %if.then
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %for.body
  %24 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %26 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(40) %struct.btDispatcherInfo* @_ZN16btCollisionWorld15getDispatchInfoEv(%class.btCollisionWorld* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 3
  ret %struct.btDispatcherInfo* %m_dispatchInfo
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btContactSolverInfo* @_ZN19btContactSolverInfoC2Ev(%struct.btContactSolverInfo* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btContactSolverInfo*, align 4
  store %struct.btContactSolverInfo* %this, %struct.btContactSolverInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %this.addr, align 4
  %0 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %1 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_tau = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %1, i32 0, i32 0
  store float 0x3FE3333340000000, float* %m_tau, align 4, !tbaa !34
  %2 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_damping = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %2, i32 0, i32 1
  store float 1.000000e+00, float* %m_damping, align 4, !tbaa !35
  %3 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_friction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %3, i32 0, i32 2
  store float 0x3FD3333340000000, float* %m_friction, align 4, !tbaa !36
  %4 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %4, i32 0, i32 3
  store float 0x3F91111120000000, float* %m_timeStep, align 4, !tbaa !31
  %5 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_restitution = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %5, i32 0, i32 4
  store float 0.000000e+00, float* %m_restitution, align 4, !tbaa !37
  %6 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_maxErrorReduction = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %6, i32 0, i32 6
  store float 2.000000e+01, float* %m_maxErrorReduction, align 4, !tbaa !38
  %7 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_numIterations = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %7, i32 0, i32 5
  store i32 10, i32* %m_numIterations, align 4, !tbaa !39
  %8 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %8, i32 0, i32 8
  store float 0x3FC99999A0000000, float* %m_erp, align 4, !tbaa !40
  %9 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %9, i32 0, i32 9
  store float 0x3FE99999A0000000, float* %m_erp2, align 4, !tbaa !41
  %10 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_globalCfm = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %10, i32 0, i32 10
  store float 0.000000e+00, float* %m_globalCfm, align 4, !tbaa !42
  %11 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_sor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %11, i32 0, i32 7
  store float 1.000000e+00, float* %m_sor, align 4, !tbaa !43
  %12 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %12, i32 0, i32 11
  store i32 1, i32* %m_splitImpulse, align 4, !tbaa !44
  %13 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %13, i32 0, i32 12
  store float 0xBFA47AE140000000, float* %m_splitImpulsePenetrationThreshold, align 4, !tbaa !45
  %14 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_splitImpulseTurnErp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %14, i32 0, i32 13
  store float 0x3FB99999A0000000, float* %m_splitImpulseTurnErp, align 4, !tbaa !46
  %15 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_linearSlop = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %15, i32 0, i32 14
  store float 0.000000e+00, float* %m_linearSlop, align 4, !tbaa !47
  %16 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_warmstartingFactor = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %16, i32 0, i32 15
  store float 0x3FEB333340000000, float* %m_warmstartingFactor, align 4, !tbaa !48
  %17 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_solverMode = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %17, i32 0, i32 16
  store i32 260, i32* %m_solverMode, align 4, !tbaa !49
  %18 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_restingContactRestitutionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %18, i32 0, i32 17
  store i32 2, i32* %m_restingContactRestitutionThreshold, align 4, !tbaa !50
  %19 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_minimumSolverBatchSize = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %19, i32 0, i32 18
  store i32 128, i32* %m_minimumSolverBatchSize, align 4, !tbaa !51
  %20 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_maxGyroscopicForce = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %20, i32 0, i32 19
  store float 1.000000e+02, float* %m_maxGyroscopicForce, align 4, !tbaa !52
  %21 = bitcast %struct.btContactSolverInfo* %this1 to %struct.btContactSolverInfoData*
  %m_singleAxisRollingFrictionThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %21, i32 0, i32 20
  store float 0x46293E5940000000, float* %m_singleAxisRollingFrictionThreshold, align 4, !tbaa !53
  ret %struct.btContactSolverInfo* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_collisionObjects
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !21
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !54
  %1 = load i32, i32* %n.addr, align 4, !tbaa !21
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

define linkonce_odr hidden i32 @_ZNK16btCollisionWorld22getNumCollisionObjectsEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  ret i32 %call
}

define hidden void @_ZN21btSimpleDynamicsWorld19integrateTransformsEf(%class.btSimpleDynamicsWorld* %this, float %timeStep) #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTrans = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !13
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btTransform* %predictedTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %predictedTrans)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !21
  %3 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %3, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects3 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %6, i32 0, i32 1
  %7 = load i32, i32* %i, align 4, !tbaa !21
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects3, i32 %7)
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4, !tbaa !2
  store %class.btCollisionObject* %8, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %9 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call5 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %10)
  store %class.btRigidBody* %call5, %class.btRigidBody** %body, align 4, !tbaa !2
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %11, null
  br i1 %tobool, label %if.then, label %if.end9

if.then:                                          ; preds = %for.body
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %13 = bitcast %class.btRigidBody* %12 to %class.btCollisionObject*
  %call6 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %13)
  br i1 %call6, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %15 = bitcast %class.btRigidBody* %14 to %class.btCollisionObject*
  %call7 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %15)
  br i1 %call7, label %if.end, label %if.then8

if.then8:                                         ; preds = %land.lhs.true
  %16 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %17 = load float, float* %timeStep.addr, align 4, !tbaa !13
  call void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %16, float %17, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  %18 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  call void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody* %18, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTrans)
  br label %if.end

if.end:                                           ; preds = %if.then8, %land.lhs.true, %if.then
  br label %if.end9

if.end9:                                          ; preds = %if.end, %for.body
  %19 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %21 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %22 = bitcast %class.btTransform* %predictedTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %22) #9
  ret void
}

define hidden void @_ZN21btSimpleDynamicsWorld11clearForcesEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !21
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %5, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !21
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %8 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call4 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %9)
  store %class.btRigidBody* %call4, %class.btRigidBody** %body, align 4, !tbaa !2
  %10 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  call void @_ZN11btRigidBody11clearForcesEv(%class.btRigidBody* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %12 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !55
  ret i32 %0
}

define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #1 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

define linkonce_odr hidden void @_ZN11btRigidBody11clearForcesEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !13
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !13
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !13
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !13
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !13
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !13
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalTorque, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

define hidden void @_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3(%class.btSimpleDynamicsWorld* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %gravity) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %gravity.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %gravity, %class.btVector3** %gravity.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4, !tbaa !2
  %m_gravity = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 3
  %1 = bitcast %class.btVector3* %m_gravity to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !56
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4, !tbaa !21
  %5 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %5, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %4, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %7 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %8, i32 0, i32 1
  %9 = load i32, i32* %i, align 4, !tbaa !21
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %9)
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4, !tbaa !2
  store %class.btCollisionObject* %10, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %11 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call4 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %12)
  store %class.btRigidBody* %call4, %class.btRigidBody** %body, align 4, !tbaa !2
  %13 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %13, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %15 = load %class.btVector3*, %class.btVector3** %gravity.addr, align 4, !tbaa !2
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %16 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

declare void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody*, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

; Function Attrs: nounwind
define hidden void @_ZNK21btSimpleDynamicsWorld10getGravityEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btSimpleDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %m_gravity = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 3
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %m_gravity to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false), !tbaa.struct !56
  ret void
}

define hidden void @_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btSimpleDynamicsWorld* %this, %class.btRigidBody* %body) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %0, %class.btCollisionObject* %2)
  ret void
}

declare void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld*, %class.btCollisionObject*) unnamed_addr #4

define hidden void @_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btSimpleDynamicsWorld* %this, %class.btCollisionObject* %collisionObject) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %call = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %1)
  store %class.btRigidBody* %call, %class.btRigidBody** %body, align 4, !tbaa !2
  %2 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %2, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %4 = bitcast %class.btSimpleDynamicsWorld* %this1 to void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)***
  %vtable = load void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)**, void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)*, void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)** %vtable, i64 23
  %5 = load void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)*, void (%class.btSimpleDynamicsWorld*, %class.btRigidBody*)** %vfn, align 4
  call void %5(%class.btSimpleDynamicsWorld* %this1, %class.btRigidBody* %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %6, %class.btCollisionObject* %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

define hidden void @_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btSimpleDynamicsWorld* %this, %class.btRigidBody* %body) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %m_gravity = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 3
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %1)
  %tobool = icmp ne %class.btCollisionShape* %call, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %5 = bitcast %class.btCollisionWorld* %2 to void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)***
  %vtable = load void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)**, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)** %vtable, i64 9
  %6 = load void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)** %vfn, align 4
  call void %6(%class.btCollisionWorld* %2, %class.btCollisionObject* %4, i16 signext 1, i16 signext -1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 9
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !58
  ret %class.btCollisionShape* %1
}

define hidden void @_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBodyss(%class.btSimpleDynamicsWorld* %this, %class.btRigidBody* %body, i16 signext %group, i16 signext %mask) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %body.addr = alloca %class.btRigidBody*, align 4
  %group.addr = alloca i16, align 2
  %mask.addr = alloca i16, align 2
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btRigidBody* %body, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  store i16 %group, i16* %group.addr, align 2, !tbaa !62
  store i16 %mask, i16* %mask.addr, align 2, !tbaa !62
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %m_gravity = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 3
  call void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZN11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %1)
  %tobool = icmp ne %class.btCollisionShape* %call, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %3 = load %class.btRigidBody*, %class.btRigidBody** %body.addr, align 4, !tbaa !2
  %4 = bitcast %class.btRigidBody* %3 to %class.btCollisionObject*
  %5 = load i16, i16* %group.addr, align 2, !tbaa !62
  %6 = load i16, i16* %mask.addr, align 2, !tbaa !62
  %7 = bitcast %class.btCollisionWorld* %2 to void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)***
  %vtable = load void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)**, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*** %7, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)** %vtable, i64 9
  %8 = load void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)*, void (%class.btCollisionWorld*, %class.btCollisionObject*, i16, i16)** %vfn, align 4
  call void %8(%class.btCollisionWorld* %2, %class.btCollisionObject* %4, i16 signext %5, i16 signext %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN21btSimpleDynamicsWorld14debugDrawWorldEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN21btSimpleDynamicsWorld9addActionEP17btActionInterface(%class.btSimpleDynamicsWorld* %this, %class.btActionInterface* %action) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %action.addr = alloca %class.btActionInterface*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %action, %class.btActionInterface** %action.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define hidden void @_ZN21btSimpleDynamicsWorld12removeActionEP17btActionInterface(%class.btSimpleDynamicsWorld* %this, %class.btActionInterface* %action) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %action.addr = alloca %class.btActionInterface*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %action, %class.btActionInterface** %action.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  ret void
}

define hidden void @_ZN21btSimpleDynamicsWorld11updateAabbsEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %predictedTrans = alloca %class.btTransform, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  %minAabb = alloca %class.btVector3, align 4
  %maxAabb = alloca %class.btVector3, align 4
  %bp = alloca %class.btBroadphaseInterface*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btTransform* %predictedTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %predictedTrans)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !21
  %3 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %3, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects3 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %6, i32 0, i32 1
  %7 = load i32, i32* %i, align 4, !tbaa !21
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects3, i32 %7)
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4, !tbaa !2
  store %class.btCollisionObject* %8, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %9 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call5 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %10)
  store %class.btRigidBody* %call5, %class.btRigidBody** %body, align 4, !tbaa !2
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %11, null
  br i1 %tobool, label %if.then, label %if.end17

if.then:                                          ; preds = %for.body
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %13 = bitcast %class.btRigidBody* %12 to %class.btCollisionObject*
  %call6 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %13)
  br i1 %call6, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %15 = bitcast %class.btRigidBody* %14 to %class.btCollisionObject*
  %call7 = call zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %15)
  br i1 %call7, label %if.end, label %if.then8

if.then8:                                         ; preds = %land.lhs.true
  %16 = bitcast %class.btVector3* %minAabb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %minAabb)
  %17 = bitcast %class.btVector3* %maxAabb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %maxAabb)
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call11 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %18)
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %19)
  %20 = bitcast %class.btCollisionShape* %call11 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %20, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %21 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %21(%class.btCollisionShape* %call11, %class.btTransform* nonnull align 4 dereferenceable(64) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb)
  %22 = bitcast %class.btBroadphaseInterface** %bp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %call13 = call %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %23)
  store %class.btBroadphaseInterface* %call13, %class.btBroadphaseInterface** %bp, align 4, !tbaa !2
  %24 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %bp, align 4, !tbaa !2
  %25 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %26 = bitcast %class.btRigidBody* %25 to %class.btCollisionObject*
  %call14 = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %26)
  %27 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_dispatcher1 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %27, i32 0, i32 2
  %28 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !27
  %29 = bitcast %class.btBroadphaseInterface* %24 to void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable15 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %29, align 4, !tbaa !6
  %vfn16 = getelementptr inbounds void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable15, i64 4
  %30 = load void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%class.btBroadphaseInterface*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn16, align 4
  call void %30(%class.btBroadphaseInterface* %24, %struct.btBroadphaseProxy* %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %minAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %maxAabb, %class.btDispatcher* %28)
  %31 = bitcast %class.btBroadphaseInterface** %bp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast %class.btVector3* %maxAabb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #9
  %33 = bitcast %class.btVector3* %minAabb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #9
  br label %if.end

if.end:                                           ; preds = %if.then8, %land.lhs.true, %if.then
  br label %if.end17

if.end17:                                         ; preds = %if.end, %for.body
  %34 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end17
  %36 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %37 = bitcast %class.btTransform* %predictedTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %37) #9
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject14isStaticObjectEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !64
  %and = and i32 %0, 1
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !58
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN16btCollisionWorld13getBroadphaseEv(%class.btCollisionWorld* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4, !tbaa !65
  ret %class.btBroadphaseInterface* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4, !tbaa !66
  ret %struct.btBroadphaseProxy* %0
}

declare void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody*, float, %class.btTransform* nonnull align 4 dereferenceable(64)) #4

declare void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody*, %class.btTransform* nonnull align 4 dereferenceable(64)) #4

declare void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody*) #4

declare void @_ZN11btRigidBody19integrateVelocitiesEf(%class.btRigidBody*, float) #4

declare void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody*, float) #4

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  ret %class.btTransform* %m_interpolationWorldTransform
}

define hidden void @_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btRigidBody*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !21
  %2 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast %class.btSimpleDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %5, i32 0, i32 1
  %6 = load i32, i32* %i, align 4, !tbaa !21
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %6)
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4, !tbaa !2
  store %class.btCollisionObject* %7, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %8 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call4 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEP17btCollisionObject(%class.btCollisionObject* %9)
  store %class.btRigidBody* %call4, %class.btRigidBody** %body, align 4, !tbaa !2
  %10 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btRigidBody* %10, null
  br i1 %tobool, label %land.lhs.true, label %if.end12

land.lhs.true:                                    ; preds = %for.body
  %11 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %call5 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %11)
  %tobool6 = icmp ne %class.btMotionState* %call5, null
  br i1 %tobool6, label %if.then, label %if.end12

if.then:                                          ; preds = %land.lhs.true
  %12 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %13 = bitcast %class.btRigidBody* %12 to %class.btCollisionObject*
  %call7 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %13)
  %cmp8 = icmp ne i32 %call7, 2
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.then
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %call10 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %14)
  %15 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !2
  %16 = bitcast %class.btRigidBody* %15 to %class.btCollisionObject*
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %16)
  %17 = bitcast %class.btMotionState* %call10 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %17, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 3
  %18 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %18(%class.btMotionState* %call10, %class.btTransform* nonnull align 4 dereferenceable(64) %call11)
  br label %if.end

if.end:                                           ; preds = %if.then9, %if.then
  br label %if.end12

if.end12:                                         ; preds = %if.end, %land.lhs.true, %for.body
  %19 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %21 = load i32, i32* %i, align 4, !tbaa !21
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !21
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4, !tbaa !67
  ret %class.btMotionState* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !71
  ret i32 %0
}

define hidden void @_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btSimpleDynamicsWorld* %this, %class.btConstraintSolver* %solver) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  %solver.addr = alloca %class.btConstraintSolver*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btConstraintSolver* %solver, %class.btConstraintSolver** %solver.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %m_ownsConstraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 2
  %0 = load i8, i8* %m_ownsConstraintSolver, align 4, !tbaa !12, !range !20
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_constraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %1 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !8
  %2 = bitcast %class.btConstraintSolver* %1 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_ownsConstraintSolver2 = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 2
  store i8 0, i8* %m_ownsConstraintSolver2, align 4, !tbaa !12
  %3 = load %class.btConstraintSolver*, %class.btConstraintSolver** %solver.addr, align 4, !tbaa !2
  %m_constraintSolver3 = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  store %class.btConstraintSolver* %3, %class.btConstraintSolver** %m_constraintSolver3, align 4, !tbaa !8
  ret void
}

; Function Attrs: nounwind
define hidden %class.btConstraintSolver* @_ZN21btSimpleDynamicsWorld19getConstraintSolverEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  %m_constraintSolver = getelementptr inbounds %class.btSimpleDynamicsWorld, %class.btSimpleDynamicsWorld* %this1, i32 0, i32 1
  %0 = load %class.btConstraintSolver*, %class.btConstraintSolver** %m_constraintSolver, align 4, !tbaa !8
  ret %class.btConstraintSolver* %0
}

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #4

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !33
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !33
  ret %class.btIDebugDraw* %0
}

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #4

declare void @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE(%class.btCollisionWorld*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20)) unnamed_addr #4

declare void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss(%class.btCollisionWorld*, %class.btCollisionObject*, i16 signext, i16 signext) unnamed_addr #4

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #4

declare void @_ZN16btCollisionWorld9serializeEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) unnamed_addr #4

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDynamicsWorld* %this, %class.btTypedConstraint* %constraint, i1 zeroext %disableCollisionsBetweenLinkedBodies) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  %disableCollisionsBetweenLinkedBodies.addr = alloca i8, align 1
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableCollisionsBetweenLinkedBodies to i8
  store i8 %frombool, i8* %disableCollisionsBetweenLinkedBodies.addr, align 1, !tbaa !72
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDynamicsWorld* %this, %class.btTypedConstraint* %constraint) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %constraint.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btTypedConstraint* %constraint, %class.btTypedConstraint** %constraint.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btDynamicsWorld17getNumConstraintsEv(%class.btDynamicsWorld* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZN15btDynamicsWorld13getConstraintEi(%class.btDynamicsWorld* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !21
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret %class.btTypedConstraint* null
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTypedConstraint* @_ZNK15btDynamicsWorld13getConstraintEi(%class.btDynamicsWorld* %this, i32 %index) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %index.addr = alloca i32, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !21
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret %class.btTypedConstraint* null
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btSimpleDynamicsWorld12getWorldTypeEv(%class.btSimpleDynamicsWorld* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSimpleDynamicsWorld*, align 4
  store %class.btSimpleDynamicsWorld* %this, %class.btSimpleDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSimpleDynamicsWorld*, %class.btSimpleDynamicsWorld** %this.addr, align 4
  ret i32 1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %vehicle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %vehicle.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %vehicle, %class.btActionInterface** %vehicle.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDynamicsWorld* %this, %class.btActionInterface* %character) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  %character.addr = alloca %class.btActionInterface*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btActionInterface* %character, %class.btActionInterface** %character.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  ret void
}

declare %class.btCollisionWorld* @_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration(%class.btCollisionWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btCollisionConfiguration*) unnamed_addr #4

; Function Attrs: nounwind
declare %class.btCollisionWorld* @_ZN16btCollisionWorldD2Ev(%class.btCollisionWorld* returned) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btDynamicsWorldD0Ev(%class.btDynamicsWorld* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDynamicsWorld*, align 4
  store %class.btDynamicsWorld* %this, %class.btDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #4

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %0 = load i32, i32* %m_internalType, align 4, !tbaa !73
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !13
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !13
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !13
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !13
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !13
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !13
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !13
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 176}
!9 = !{!"_ZTS21btSimpleDynamicsWorld", !3, i64 176, !10, i64 180, !11, i64 184}
!10 = !{!"bool", !4, i64 0}
!11 = !{!"_ZTS9btVector3", !4, i64 0}
!12 = !{!9, !10, i64 180}
!13 = !{!14, !14, i64 0}
!14 = !{!"float", !4, i64 0}
!15 = !{!16, !3, i64 80}
!16 = !{!"_ZTS15btDynamicsWorld", !3, i64 80, !3, i64 84, !3, i64 88, !17, i64 92}
!17 = !{!"_ZTS19btContactSolverInfo"}
!18 = !{!16, !3, i64 84}
!19 = !{!16, !3, i64 88}
!20 = !{i8 0, i8 2}
!21 = !{!22, !22, i64 0}
!22 = !{!"int", !4, i64 0}
!23 = !{!24, !14, i64 0}
!24 = !{!"_ZTS16btDispatcherInfo", !14, i64 0, !22, i64 4, !22, i64 8, !14, i64 12, !10, i64 16, !3, i64 20, !10, i64 24, !10, i64 25, !10, i64 26, !14, i64 28, !10, i64 32, !14, i64 36}
!25 = !{!24, !22, i64 4}
!26 = !{!24, !3, i64 20}
!27 = !{!28, !3, i64 24}
!28 = !{!"_ZTS16btCollisionWorld", !29, i64 4, !3, i64 24, !24, i64 28, !3, i64 68, !3, i64 72, !10, i64 76}
!29 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !30, i64 0, !22, i64 4, !22, i64 8, !3, i64 12, !10, i64 16}
!30 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!31 = !{!32, !14, i64 12}
!32 = !{!"_ZTS23btContactSolverInfoData", !14, i64 0, !14, i64 4, !14, i64 8, !14, i64 12, !14, i64 16, !22, i64 20, !14, i64 24, !14, i64 28, !14, i64 32, !14, i64 36, !14, i64 40, !22, i64 44, !14, i64 48, !14, i64 52, !14, i64 56, !14, i64 60, !22, i64 64, !22, i64 68, !22, i64 72, !14, i64 76, !14, i64 80}
!33 = !{!28, !3, i64 72}
!34 = !{!32, !14, i64 0}
!35 = !{!32, !14, i64 4}
!36 = !{!32, !14, i64 8}
!37 = !{!32, !14, i64 16}
!38 = !{!32, !14, i64 24}
!39 = !{!32, !22, i64 20}
!40 = !{!32, !14, i64 32}
!41 = !{!32, !14, i64 36}
!42 = !{!32, !14, i64 40}
!43 = !{!32, !14, i64 28}
!44 = !{!32, !22, i64 44}
!45 = !{!32, !14, i64 48}
!46 = !{!32, !14, i64 52}
!47 = !{!32, !14, i64 56}
!48 = !{!32, !14, i64 60}
!49 = !{!32, !22, i64 64}
!50 = !{!32, !22, i64 68}
!51 = !{!32, !22, i64 72}
!52 = !{!32, !14, i64 76}
!53 = !{!32, !14, i64 80}
!54 = !{!29, !3, i64 12}
!55 = !{!29, !22, i64 4}
!56 = !{i64 0, i64 16, !57}
!57 = !{!4, !4, i64 0}
!58 = !{!59, !3, i64 192}
!59 = !{!"_ZTS17btCollisionObject", !60, i64 4, !60, i64 68, !11, i64 132, !11, i64 148, !11, i64 164, !22, i64 180, !14, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !22, i64 204, !22, i64 208, !22, i64 212, !22, i64 216, !14, i64 220, !14, i64 224, !14, i64 228, !14, i64 232, !22, i64 236, !4, i64 240, !14, i64 244, !14, i64 248, !14, i64 252, !22, i64 256, !22, i64 260}
!60 = !{!"_ZTS11btTransform", !61, i64 0, !11, i64 48}
!61 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!62 = !{!63, !63, i64 0}
!63 = !{!"short", !4, i64 0}
!64 = !{!59, !22, i64 204}
!65 = !{!28, !3, i64 68}
!66 = !{!59, !3, i64 188}
!67 = !{!68, !3, i64 480}
!68 = !{!"_ZTS11btRigidBody", !61, i64 264, !11, i64 312, !11, i64 328, !14, i64 344, !11, i64 348, !11, i64 364, !11, i64 380, !11, i64 396, !11, i64 412, !11, i64 428, !14, i64 444, !14, i64 448, !10, i64 452, !14, i64 456, !14, i64 460, !14, i64 464, !14, i64 468, !14, i64 472, !14, i64 476, !3, i64 480, !69, i64 484, !22, i64 504, !22, i64 508, !11, i64 512, !11, i64 528, !11, i64 544, !11, i64 560, !11, i64 576, !11, i64 592, !22, i64 608, !22, i64 612}
!69 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !70, i64 0, !22, i64 4, !22, i64 8, !3, i64 12, !10, i64 16}
!70 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!71 = !{!59, !22, i64 216}
!72 = !{!10, !10, i64 0}
!73 = !{!59, !22, i64 236}
