; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btStridingMeshInterface.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btStridingMeshInterface.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%struct.AabbCalculationCallback = type { %class.btInternalTriangleIndexCallback, %class.btVector3, %class.btVector3 }
%class.btSerializer = type { i32 (...)** }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK23btStridingMeshInterface14hasPremadeAabbEv = comdat any

$_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_ = comdat any

$_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_ = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN31btInternalTriangleIndexCallbackC2Ev = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

@.str = private unnamed_addr constant [15 x i8] c"btIntIndexData\00", align 1
@.str.1 = private unnamed_addr constant [27 x i8] c"btShortIntIndexTripletData\00", align 1
@.str.2 = private unnamed_addr constant [23 x i8] c"btCharIndexTripletData\00", align 1
@.str.3 = private unnamed_addr constant [19 x i8] c"btVector3FloatData\00", align 1
@.str.4 = private unnamed_addr constant [20 x i8] c"btVector3DoubleData\00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"btMeshPartData\00", align 1
@.str.6 = private unnamed_addr constant [28 x i8] c"btStridingMeshInterfaceData\00", align 1
@_ZTV23btStridingMeshInterface = hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI23btStridingMeshInterface to i8*), i8* bitcast (%class.btStridingMeshInterface* (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD1Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i1 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS23btStridingMeshInterface = hidden constant [26 x i8] c"23btStridingMeshInterface\00", align 1
@_ZTI23btStridingMeshInterface = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btStridingMeshInterface, i32 0, i32 0) }, align 4
@_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback to i8*), i8* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to i8*), i8* bitcast (void (%struct.AabbCalculationCallback*)* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev to i8*), i8* bitcast (void (%struct.AabbCalculationCallback*, %class.btVector3*, i32, i32)* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal constant [94 x i8] c"ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback\00", align 1
@_ZTI31btInternalTriangleIndexCallback = external constant i8*
@_ZTIZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([94 x i8], [94 x i8]* @_ZTSZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI31btInternalTriangleIndexCallback to i8*) }, align 4
@_ZTV31btInternalTriangleIndexCallback = external unnamed_addr constant { [5 x i8*] }, align 4

@_ZN23btStridingMeshInterfaceD1Ev = hidden unnamed_addr alias %class.btStridingMeshInterface* (%class.btStridingMeshInterface*), %class.btStridingMeshInterface* (%class.btStridingMeshInterface*)* @_ZN23btStridingMeshInterfaceD2Ev

; Function Attrs: nounwind
define hidden %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceD2Ev(%class.btStridingMeshInterface* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret %class.btStridingMeshInterface* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN23btStridingMeshInterfaceD0Ev(%class.btStridingMeshInterface* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  call void @llvm.trap() #8
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #1

define hidden void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface* %this, %class.btInternalTriangleIndexCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %callback.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %numtotalphysicsverts = alloca i32, align 4
  %part = alloca i32, align 4
  %graphicssubparts = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %type = alloca i32, align 4
  %gfxindextype = alloca i32, align 4
  %stride = alloca i32, align 4
  %numverts = alloca i32, align 4
  %numtriangles = alloca i32, align 4
  %gfxindex = alloca i32, align 4
  %triangle = alloca [3 x %class.btVector3], align 16
  %meshScaling = alloca %class.btVector3, align 4
  %graphicsbase = alloca float*, align 4
  %tri_indices = alloca i32*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %tri_indices63 = alloca i16*, align 4
  %ref.tmp70 = alloca float, align 4
  %ref.tmp74 = alloca float, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %tri_indices126 = alloca i8*, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp138 = alloca float, align 4
  %ref.tmp142 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp168 = alloca float, align 4
  %ref.tmp172 = alloca float, align 4
  %ref.tmp176 = alloca float, align 4
  %graphicsbase187 = alloca double*, align 4
  %tri_indices192 = alloca i32*, align 4
  %ref.tmp199 = alloca float, align 4
  %ref.tmp204 = alloca float, align 4
  %ref.tmp209 = alloca float, align 4
  %ref.tmp218 = alloca float, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp228 = alloca float, align 4
  %ref.tmp237 = alloca float, align 4
  %ref.tmp242 = alloca float, align 4
  %ref.tmp247 = alloca float, align 4
  %tri_indices262 = alloca i16*, align 4
  %ref.tmp270 = alloca float, align 4
  %ref.tmp275 = alloca float, align 4
  %ref.tmp280 = alloca float, align 4
  %ref.tmp290 = alloca float, align 4
  %ref.tmp295 = alloca float, align 4
  %ref.tmp300 = alloca float, align 4
  %ref.tmp310 = alloca float, align 4
  %ref.tmp315 = alloca float, align 4
  %ref.tmp320 = alloca float, align 4
  %tri_indices335 = alloca i8*, align 4
  %ref.tmp343 = alloca float, align 4
  %ref.tmp348 = alloca float, align 4
  %ref.tmp353 = alloca float, align 4
  %ref.tmp363 = alloca float, align 4
  %ref.tmp368 = alloca float, align 4
  %ref.tmp373 = alloca float, align 4
  %ref.tmp383 = alloca float, align 4
  %ref.tmp388 = alloca float, align 4
  %ref.tmp393 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  store %class.btInternalTriangleIndexCallback* %callback, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %2 = bitcast i32* %numtotalphysicsverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 0, i32* %numtotalphysicsverts, align 4, !tbaa !6
  %3 = bitcast i32* %part to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = bitcast i32* %graphicssubparts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %5, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %6 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call = call i32 %6(%class.btStridingMeshInterface* %this1)
  store i32 %call, i32* %graphicssubparts, align 4, !tbaa !6
  %7 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = bitcast i32* %gfxindextype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = bitcast i32* %numtriangles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = bitcast i32* %gfxindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = bitcast [3 x %class.btVector3]* %triangle to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %16) #9
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %17 = bitcast %class.btVector3* %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this1)
  %18 = bitcast %class.btVector3* %meshScaling to i8*
  %19 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !10
  store i32 0, i32* %part, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc410, %arrayctor.cont
  %20 = load i32, i32* %part, align 4, !tbaa !6
  %21 = load i32, i32* %graphicssubparts, align 4, !tbaa !6
  %cmp = icmp slt i32 %20, %21
  br i1 %cmp, label %for.body, label %for.end412

for.body:                                         ; preds = %for.cond
  %22 = load i32, i32* %part, align 4, !tbaa !6
  %23 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable4 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %23, align 4, !tbaa !8
  %vfn5 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable4, i64 4
  %24 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn5, align 4
  call void %24(%class.btStridingMeshInterface* %this1, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numtriangles, i32* nonnull align 4 dereferenceable(4) %gfxindextype, i32 %22)
  %25 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %mul = mul nsw i32 %25, 3
  %26 = load i32, i32* %numtotalphysicsverts, align 4, !tbaa !6
  %add = add nsw i32 %26, %mul
  store i32 %add, i32* %numtotalphysicsverts, align 4, !tbaa !6
  %27 = load i32, i32* %type, align 4, !tbaa !12
  switch i32 %27, label %sw.default406 [
    i32 0, label %sw.bb
    i32 1, label %sw.bb186
  ]

sw.bb:                                            ; preds = %for.body
  %28 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  %29 = load i32, i32* %gfxindextype, align 4, !tbaa !12
  switch i32 %29, label %sw.default [
    i32 2, label %sw.bb6
    i32 3, label %sw.bb59
    i32 5, label %sw.bb122
  ]

sw.bb6:                                           ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %sw.bb6
  %30 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %31 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %30, %31
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %32 = bitcast i32** %tri_indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  %33 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %34 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %35 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul10 = mul nsw i32 %34, %35
  %add.ptr = getelementptr inbounds i8, i8* %33, i32 %mul10
  %36 = bitcast i8* %add.ptr to i32*
  store i32* %36, i32** %tri_indices, align 4, !tbaa !2
  %37 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %38 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %38, i32 0
  %39 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %40 = load i32, i32* %stride, align 4, !tbaa !6
  %mul11 = mul i32 %39, %40
  %add.ptr12 = getelementptr inbounds i8, i8* %37, i32 %mul11
  %41 = bitcast i8* %add.ptr12 to float*
  store float* %41, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %42 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #9
  %43 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %43, i32 0
  %44 = load float, float* %arrayidx14, align 4, !tbaa !14
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %45 = load float, float* %call15, align 4, !tbaa !14
  %mul16 = fmul float %44, %45
  store float %mul16, float* %ref.tmp, align 4, !tbaa !14
  %46 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #9
  %47 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx18 = getelementptr inbounds float, float* %47, i32 1
  %48 = load float, float* %arrayidx18, align 4, !tbaa !14
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %49 = load float, float* %call19, align 4, !tbaa !14
  %mul20 = fmul float %48, %49
  store float %mul20, float* %ref.tmp17, align 4, !tbaa !14
  %50 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  %51 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds float, float* %51, i32 2
  %52 = load float, float* %arrayidx22, align 4, !tbaa !14
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %53 = load float, float* %call23, align 4, !tbaa !14
  %mul24 = fmul float %52, %53
  store float %mul24, float* %ref.tmp21, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx13, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %54 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  %56 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #9
  %57 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %58 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds i32, i32* %58, i32 1
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !6
  %60 = load i32, i32* %stride, align 4, !tbaa !6
  %mul26 = mul i32 %59, %60
  %add.ptr27 = getelementptr inbounds i8, i8* %57, i32 %mul26
  %61 = bitcast i8* %add.ptr27 to float*
  store float* %61, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %62 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  %63 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds float, float* %63, i32 0
  %64 = load float, float* %arrayidx30, align 4, !tbaa !14
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %65 = load float, float* %call31, align 4, !tbaa !14
  %mul32 = fmul float %64, %65
  store float %mul32, float* %ref.tmp29, align 4, !tbaa !14
  %66 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #9
  %67 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds float, float* %67, i32 1
  %68 = load float, float* %arrayidx34, align 4, !tbaa !14
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %69 = load float, float* %call35, align 4, !tbaa !14
  %mul36 = fmul float %68, %69
  store float %mul36, float* %ref.tmp33, align 4, !tbaa !14
  %70 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx38 = getelementptr inbounds float, float* %71, i32 2
  %72 = load float, float* %arrayidx38, align 4, !tbaa !14
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %73 = load float, float* %call39, align 4, !tbaa !14
  %mul40 = fmul float %72, %73
  store float %mul40, float* %ref.tmp37, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %74 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  %75 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #9
  %76 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  %77 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %78 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %78, i32 2
  %79 = load i32, i32* %arrayidx41, align 4, !tbaa !6
  %80 = load i32, i32* %stride, align 4, !tbaa !6
  %mul42 = mul i32 %79, %80
  %add.ptr43 = getelementptr inbounds i8, i8* %77, i32 %mul42
  %81 = bitcast i8* %add.ptr43 to float*
  store float* %81, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %82 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  %83 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx46 = getelementptr inbounds float, float* %83, i32 0
  %84 = load float, float* %arrayidx46, align 4, !tbaa !14
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %85 = load float, float* %call47, align 4, !tbaa !14
  %mul48 = fmul float %84, %85
  store float %mul48, float* %ref.tmp45, align 4, !tbaa !14
  %86 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #9
  %87 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds float, float* %87, i32 1
  %88 = load float, float* %arrayidx50, align 4, !tbaa !14
  %call51 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %89 = load float, float* %call51, align 4, !tbaa !14
  %mul52 = fmul float %88, %89
  store float %mul52, float* %ref.tmp49, align 4, !tbaa !14
  %90 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #9
  %91 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds float, float* %91, i32 2
  %92 = load float, float* %arrayidx54, align 4, !tbaa !14
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %93 = load float, float* %call55, align 4, !tbaa !14
  %mul56 = fmul float %92, %93
  store float %mul56, float* %ref.tmp53, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx44, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %94 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  %95 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  %96 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %98 = load i32, i32* %part, align 4, !tbaa !6
  %99 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %100 = bitcast %class.btInternalTriangleIndexCallback* %97 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable57 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %100, align 4, !tbaa !8
  %vfn58 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable57, i64 2
  %101 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn58, align 4
  call void %101(%class.btInternalTriangleIndexCallback* %97, %class.btVector3* %arraydecay, i32 %98, i32 %99)
  %102 = bitcast i32** %tri_indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %103 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc = add nsw i32 %103, 1
  store i32 %inc, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %sw.epilog

sw.bb59:                                          ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc119, %sw.bb59
  %104 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %105 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp61 = icmp slt i32 %104, %105
  br i1 %cmp61, label %for.body62, label %for.end121

for.body62:                                       ; preds = %for.cond60
  %106 = bitcast i16** %tri_indices63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #9
  %107 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %108 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %109 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul64 = mul nsw i32 %108, %109
  %add.ptr65 = getelementptr inbounds i8, i8* %107, i32 %mul64
  %110 = bitcast i8* %add.ptr65 to i16*
  store i16* %110, i16** %tri_indices63, align 4, !tbaa !2
  %111 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %112 = load i16*, i16** %tri_indices63, align 4, !tbaa !2
  %arrayidx66 = getelementptr inbounds i16, i16* %112, i32 0
  %113 = load i16, i16* %arrayidx66, align 2, !tbaa !16
  %conv = zext i16 %113 to i32
  %114 = load i32, i32* %stride, align 4, !tbaa !6
  %mul67 = mul nsw i32 %conv, %114
  %add.ptr68 = getelementptr inbounds i8, i8* %111, i32 %mul67
  %115 = bitcast i8* %add.ptr68 to float*
  store float* %115, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %116 = bitcast float* %ref.tmp70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #9
  %117 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds float, float* %117, i32 0
  %118 = load float, float* %arrayidx71, align 4, !tbaa !14
  %call72 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %119 = load float, float* %call72, align 4, !tbaa !14
  %mul73 = fmul float %118, %119
  store float %mul73, float* %ref.tmp70, align 4, !tbaa !14
  %120 = bitcast float* %ref.tmp74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #9
  %121 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds float, float* %121, i32 1
  %122 = load float, float* %arrayidx75, align 4, !tbaa !14
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %123 = load float, float* %call76, align 4, !tbaa !14
  %mul77 = fmul float %122, %123
  store float %mul77, float* %ref.tmp74, align 4, !tbaa !14
  %124 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #9
  %125 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds float, float* %125, i32 2
  %126 = load float, float* %arrayidx79, align 4, !tbaa !14
  %call80 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %127 = load float, float* %call80, align 4, !tbaa !14
  %mul81 = fmul float %126, %127
  store float %mul81, float* %ref.tmp78, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx69, float* nonnull align 4 dereferenceable(4) %ref.tmp70, float* nonnull align 4 dereferenceable(4) %ref.tmp74, float* nonnull align 4 dereferenceable(4) %ref.tmp78)
  %128 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #9
  %129 = bitcast float* %ref.tmp74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #9
  %130 = bitcast float* %ref.tmp70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #9
  %131 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %132 = load i16*, i16** %tri_indices63, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds i16, i16* %132, i32 1
  %133 = load i16, i16* %arrayidx82, align 2, !tbaa !16
  %conv83 = zext i16 %133 to i32
  %134 = load i32, i32* %stride, align 4, !tbaa !6
  %mul84 = mul nsw i32 %conv83, %134
  %add.ptr85 = getelementptr inbounds i8, i8* %131, i32 %mul84
  %135 = bitcast i8* %add.ptr85 to float*
  store float* %135, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %136 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #9
  %137 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx88 = getelementptr inbounds float, float* %137, i32 0
  %138 = load float, float* %arrayidx88, align 4, !tbaa !14
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %139 = load float, float* %call89, align 4, !tbaa !14
  %mul90 = fmul float %138, %139
  store float %mul90, float* %ref.tmp87, align 4, !tbaa !14
  %140 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #9
  %141 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx92 = getelementptr inbounds float, float* %141, i32 1
  %142 = load float, float* %arrayidx92, align 4, !tbaa !14
  %call93 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %143 = load float, float* %call93, align 4, !tbaa !14
  %mul94 = fmul float %142, %143
  store float %mul94, float* %ref.tmp91, align 4, !tbaa !14
  %144 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #9
  %145 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx96 = getelementptr inbounds float, float* %145, i32 2
  %146 = load float, float* %arrayidx96, align 4, !tbaa !14
  %call97 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %147 = load float, float* %call97, align 4, !tbaa !14
  %mul98 = fmul float %146, %147
  store float %mul98, float* %ref.tmp95, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx86, float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp95)
  %148 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #9
  %149 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #9
  %150 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #9
  %151 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %152 = load i16*, i16** %tri_indices63, align 4, !tbaa !2
  %arrayidx99 = getelementptr inbounds i16, i16* %152, i32 2
  %153 = load i16, i16* %arrayidx99, align 2, !tbaa !16
  %conv100 = zext i16 %153 to i32
  %154 = load i32, i32* %stride, align 4, !tbaa !6
  %mul101 = mul nsw i32 %conv100, %154
  %add.ptr102 = getelementptr inbounds i8, i8* %151, i32 %mul101
  %155 = bitcast i8* %add.ptr102 to float*
  store float* %155, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %156 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #9
  %157 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds float, float* %157, i32 0
  %158 = load float, float* %arrayidx105, align 4, !tbaa !14
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %159 = load float, float* %call106, align 4, !tbaa !14
  %mul107 = fmul float %158, %159
  store float %mul107, float* %ref.tmp104, align 4, !tbaa !14
  %160 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %160) #9
  %161 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds float, float* %161, i32 1
  %162 = load float, float* %arrayidx109, align 4, !tbaa !14
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %163 = load float, float* %call110, align 4, !tbaa !14
  %mul111 = fmul float %162, %163
  store float %mul111, float* %ref.tmp108, align 4, !tbaa !14
  %164 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #9
  %165 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx113 = getelementptr inbounds float, float* %165, i32 2
  %166 = load float, float* %arrayidx113, align 4, !tbaa !14
  %call114 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %167 = load float, float* %call114, align 4, !tbaa !14
  %mul115 = fmul float %166, %167
  store float %mul115, float* %ref.tmp112, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp112)
  %168 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #9
  %169 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #9
  %170 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #9
  %171 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %172 = load i32, i32* %part, align 4, !tbaa !6
  %173 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %174 = bitcast %class.btInternalTriangleIndexCallback* %171 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable117 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %174, align 4, !tbaa !8
  %vfn118 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable117, i64 2
  %175 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn118, align 4
  call void %175(%class.btInternalTriangleIndexCallback* %171, %class.btVector3* %arraydecay116, i32 %172, i32 %173)
  %176 = bitcast i16** %tri_indices63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #9
  br label %for.inc119

for.inc119:                                       ; preds = %for.body62
  %177 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc120 = add nsw i32 %177, 1
  store i32 %inc120, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond60

for.end121:                                       ; preds = %for.cond60
  br label %sw.epilog

sw.bb122:                                         ; preds = %sw.bb
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond123

for.cond123:                                      ; preds = %for.inc183, %sw.bb122
  %178 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %179 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp124 = icmp slt i32 %178, %179
  br i1 %cmp124, label %for.body125, label %for.end185

for.body125:                                      ; preds = %for.cond123
  %180 = bitcast i8** %tri_indices126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %180) #9
  %181 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %182 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %183 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul127 = mul nsw i32 %182, %183
  %add.ptr128 = getelementptr inbounds i8, i8* %181, i32 %mul127
  store i8* %add.ptr128, i8** %tri_indices126, align 4, !tbaa !2
  %184 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %185 = load i8*, i8** %tri_indices126, align 4, !tbaa !2
  %arrayidx129 = getelementptr inbounds i8, i8* %185, i32 0
  %186 = load i8, i8* %arrayidx129, align 1, !tbaa !11
  %conv130 = zext i8 %186 to i32
  %187 = load i32, i32* %stride, align 4, !tbaa !6
  %mul131 = mul nsw i32 %conv130, %187
  %add.ptr132 = getelementptr inbounds i8, i8* %184, i32 %mul131
  %188 = bitcast i8* %add.ptr132 to float*
  store float* %188, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx133 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %189 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %189) #9
  %190 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx135 = getelementptr inbounds float, float* %190, i32 0
  %191 = load float, float* %arrayidx135, align 4, !tbaa !14
  %call136 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %192 = load float, float* %call136, align 4, !tbaa !14
  %mul137 = fmul float %191, %192
  store float %mul137, float* %ref.tmp134, align 4, !tbaa !14
  %193 = bitcast float* %ref.tmp138 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %193) #9
  %194 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx139 = getelementptr inbounds float, float* %194, i32 1
  %195 = load float, float* %arrayidx139, align 4, !tbaa !14
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %196 = load float, float* %call140, align 4, !tbaa !14
  %mul141 = fmul float %195, %196
  store float %mul141, float* %ref.tmp138, align 4, !tbaa !14
  %197 = bitcast float* %ref.tmp142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #9
  %198 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx143 = getelementptr inbounds float, float* %198, i32 2
  %199 = load float, float* %arrayidx143, align 4, !tbaa !14
  %call144 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %200 = load float, float* %call144, align 4, !tbaa !14
  %mul145 = fmul float %199, %200
  store float %mul145, float* %ref.tmp142, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp142)
  %201 = bitcast float* %ref.tmp142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #9
  %202 = bitcast float* %ref.tmp138 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #9
  %203 = bitcast float* %ref.tmp134 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #9
  %204 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %205 = load i8*, i8** %tri_indices126, align 4, !tbaa !2
  %arrayidx146 = getelementptr inbounds i8, i8* %205, i32 1
  %206 = load i8, i8* %arrayidx146, align 1, !tbaa !11
  %conv147 = zext i8 %206 to i32
  %207 = load i32, i32* %stride, align 4, !tbaa !6
  %mul148 = mul nsw i32 %conv147, %207
  %add.ptr149 = getelementptr inbounds i8, i8* %204, i32 %mul148
  %208 = bitcast i8* %add.ptr149 to float*
  store float* %208, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx150 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %209 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %209) #9
  %210 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx152 = getelementptr inbounds float, float* %210, i32 0
  %211 = load float, float* %arrayidx152, align 4, !tbaa !14
  %call153 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %212 = load float, float* %call153, align 4, !tbaa !14
  %mul154 = fmul float %211, %212
  store float %mul154, float* %ref.tmp151, align 4, !tbaa !14
  %213 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %213) #9
  %214 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx156 = getelementptr inbounds float, float* %214, i32 1
  %215 = load float, float* %arrayidx156, align 4, !tbaa !14
  %call157 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %216 = load float, float* %call157, align 4, !tbaa !14
  %mul158 = fmul float %215, %216
  store float %mul158, float* %ref.tmp155, align 4, !tbaa !14
  %217 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %217) #9
  %218 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx160 = getelementptr inbounds float, float* %218, i32 2
  %219 = load float, float* %arrayidx160, align 4, !tbaa !14
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %220 = load float, float* %call161, align 4, !tbaa !14
  %mul162 = fmul float %219, %220
  store float %mul162, float* %ref.tmp159, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx150, float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp159)
  %221 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #9
  %222 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #9
  %223 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #9
  %224 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %225 = load i8*, i8** %tri_indices126, align 4, !tbaa !2
  %arrayidx163 = getelementptr inbounds i8, i8* %225, i32 2
  %226 = load i8, i8* %arrayidx163, align 1, !tbaa !11
  %conv164 = zext i8 %226 to i32
  %227 = load i32, i32* %stride, align 4, !tbaa !6
  %mul165 = mul nsw i32 %conv164, %227
  %add.ptr166 = getelementptr inbounds i8, i8* %224, i32 %mul165
  %228 = bitcast i8* %add.ptr166 to float*
  store float* %228, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx167 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %229 = bitcast float* %ref.tmp168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %229) #9
  %230 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx169 = getelementptr inbounds float, float* %230, i32 0
  %231 = load float, float* %arrayidx169, align 4, !tbaa !14
  %call170 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %232 = load float, float* %call170, align 4, !tbaa !14
  %mul171 = fmul float %231, %232
  store float %mul171, float* %ref.tmp168, align 4, !tbaa !14
  %233 = bitcast float* %ref.tmp172 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %233) #9
  %234 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx173 = getelementptr inbounds float, float* %234, i32 1
  %235 = load float, float* %arrayidx173, align 4, !tbaa !14
  %call174 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %236 = load float, float* %call174, align 4, !tbaa !14
  %mul175 = fmul float %235, %236
  store float %mul175, float* %ref.tmp172, align 4, !tbaa !14
  %237 = bitcast float* %ref.tmp176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %237) #9
  %238 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx177 = getelementptr inbounds float, float* %238, i32 2
  %239 = load float, float* %arrayidx177, align 4, !tbaa !14
  %call178 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %240 = load float, float* %call178, align 4, !tbaa !14
  %mul179 = fmul float %239, %240
  store float %mul179, float* %ref.tmp176, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx167, float* nonnull align 4 dereferenceable(4) %ref.tmp168, float* nonnull align 4 dereferenceable(4) %ref.tmp172, float* nonnull align 4 dereferenceable(4) %ref.tmp176)
  %241 = bitcast float* %ref.tmp176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #9
  %242 = bitcast float* %ref.tmp172 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #9
  %243 = bitcast float* %ref.tmp168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #9
  %244 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay180 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %245 = load i32, i32* %part, align 4, !tbaa !6
  %246 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %247 = bitcast %class.btInternalTriangleIndexCallback* %244 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable181 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %247, align 4, !tbaa !8
  %vfn182 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable181, i64 2
  %248 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn182, align 4
  call void %248(%class.btInternalTriangleIndexCallback* %244, %class.btVector3* %arraydecay180, i32 %245, i32 %246)
  %249 = bitcast i8** %tri_indices126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #9
  br label %for.inc183

for.inc183:                                       ; preds = %for.body125
  %250 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc184 = add nsw i32 %250, 1
  store i32 %inc184, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond123

for.end185:                                       ; preds = %for.cond123
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %for.end185, %for.end121, %for.end
  %251 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #9
  br label %sw.epilog407

sw.bb186:                                         ; preds = %for.body
  %252 = bitcast double** %graphicsbase187 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %252) #9
  %253 = load i32, i32* %gfxindextype, align 4, !tbaa !12
  switch i32 %253, label %sw.default404 [
    i32 2, label %sw.bb188
    i32 3, label %sw.bb258
    i32 5, label %sw.bb331
  ]

sw.bb188:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond189

for.cond189:                                      ; preds = %for.inc255, %sw.bb188
  %254 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %255 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp190 = icmp slt i32 %254, %255
  br i1 %cmp190, label %for.body191, label %for.end257

for.body191:                                      ; preds = %for.cond189
  %256 = bitcast i32** %tri_indices192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %256) #9
  %257 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %258 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %259 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul193 = mul nsw i32 %258, %259
  %add.ptr194 = getelementptr inbounds i8, i8* %257, i32 %mul193
  %260 = bitcast i8* %add.ptr194 to i32*
  store i32* %260, i32** %tri_indices192, align 4, !tbaa !2
  %261 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %262 = load i32*, i32** %tri_indices192, align 4, !tbaa !2
  %arrayidx195 = getelementptr inbounds i32, i32* %262, i32 0
  %263 = load i32, i32* %arrayidx195, align 4, !tbaa !6
  %264 = load i32, i32* %stride, align 4, !tbaa !6
  %mul196 = mul i32 %263, %264
  %add.ptr197 = getelementptr inbounds i8, i8* %261, i32 %mul196
  %265 = bitcast i8* %add.ptr197 to double*
  store double* %265, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx198 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %266 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %266) #9
  %267 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx200 = getelementptr inbounds double, double* %267, i32 0
  %268 = load double, double* %arrayidx200, align 8, !tbaa !18
  %conv201 = fptrunc double %268 to float
  %call202 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %269 = load float, float* %call202, align 4, !tbaa !14
  %mul203 = fmul float %conv201, %269
  store float %mul203, float* %ref.tmp199, align 4, !tbaa !14
  %270 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %270) #9
  %271 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx205 = getelementptr inbounds double, double* %271, i32 1
  %272 = load double, double* %arrayidx205, align 8, !tbaa !18
  %conv206 = fptrunc double %272 to float
  %call207 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %273 = load float, float* %call207, align 4, !tbaa !14
  %mul208 = fmul float %conv206, %273
  store float %mul208, float* %ref.tmp204, align 4, !tbaa !14
  %274 = bitcast float* %ref.tmp209 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %274) #9
  %275 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx210 = getelementptr inbounds double, double* %275, i32 2
  %276 = load double, double* %arrayidx210, align 8, !tbaa !18
  %conv211 = fptrunc double %276 to float
  %call212 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %277 = load float, float* %call212, align 4, !tbaa !14
  %mul213 = fmul float %conv211, %277
  store float %mul213, float* %ref.tmp209, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx198, float* nonnull align 4 dereferenceable(4) %ref.tmp199, float* nonnull align 4 dereferenceable(4) %ref.tmp204, float* nonnull align 4 dereferenceable(4) %ref.tmp209)
  %278 = bitcast float* %ref.tmp209 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %278) #9
  %279 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %279) #9
  %280 = bitcast float* %ref.tmp199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %280) #9
  %281 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %282 = load i32*, i32** %tri_indices192, align 4, !tbaa !2
  %arrayidx214 = getelementptr inbounds i32, i32* %282, i32 1
  %283 = load i32, i32* %arrayidx214, align 4, !tbaa !6
  %284 = load i32, i32* %stride, align 4, !tbaa !6
  %mul215 = mul i32 %283, %284
  %add.ptr216 = getelementptr inbounds i8, i8* %281, i32 %mul215
  %285 = bitcast i8* %add.ptr216 to double*
  store double* %285, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx217 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %286 = bitcast float* %ref.tmp218 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %286) #9
  %287 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx219 = getelementptr inbounds double, double* %287, i32 0
  %288 = load double, double* %arrayidx219, align 8, !tbaa !18
  %conv220 = fptrunc double %288 to float
  %call221 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %289 = load float, float* %call221, align 4, !tbaa !14
  %mul222 = fmul float %conv220, %289
  store float %mul222, float* %ref.tmp218, align 4, !tbaa !14
  %290 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %290) #9
  %291 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx224 = getelementptr inbounds double, double* %291, i32 1
  %292 = load double, double* %arrayidx224, align 8, !tbaa !18
  %conv225 = fptrunc double %292 to float
  %call226 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %293 = load float, float* %call226, align 4, !tbaa !14
  %mul227 = fmul float %conv225, %293
  store float %mul227, float* %ref.tmp223, align 4, !tbaa !14
  %294 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %294) #9
  %295 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx229 = getelementptr inbounds double, double* %295, i32 2
  %296 = load double, double* %arrayidx229, align 8, !tbaa !18
  %conv230 = fptrunc double %296 to float
  %call231 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %297 = load float, float* %call231, align 4, !tbaa !14
  %mul232 = fmul float %conv230, %297
  store float %mul232, float* %ref.tmp228, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx217, float* nonnull align 4 dereferenceable(4) %ref.tmp218, float* nonnull align 4 dereferenceable(4) %ref.tmp223, float* nonnull align 4 dereferenceable(4) %ref.tmp228)
  %298 = bitcast float* %ref.tmp228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #9
  %299 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #9
  %300 = bitcast float* %ref.tmp218 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %300) #9
  %301 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %302 = load i32*, i32** %tri_indices192, align 4, !tbaa !2
  %arrayidx233 = getelementptr inbounds i32, i32* %302, i32 2
  %303 = load i32, i32* %arrayidx233, align 4, !tbaa !6
  %304 = load i32, i32* %stride, align 4, !tbaa !6
  %mul234 = mul i32 %303, %304
  %add.ptr235 = getelementptr inbounds i8, i8* %301, i32 %mul234
  %305 = bitcast i8* %add.ptr235 to double*
  store double* %305, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx236 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %306 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %306) #9
  %307 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx238 = getelementptr inbounds double, double* %307, i32 0
  %308 = load double, double* %arrayidx238, align 8, !tbaa !18
  %conv239 = fptrunc double %308 to float
  %call240 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %309 = load float, float* %call240, align 4, !tbaa !14
  %mul241 = fmul float %conv239, %309
  store float %mul241, float* %ref.tmp237, align 4, !tbaa !14
  %310 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %310) #9
  %311 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx243 = getelementptr inbounds double, double* %311, i32 1
  %312 = load double, double* %arrayidx243, align 8, !tbaa !18
  %conv244 = fptrunc double %312 to float
  %call245 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %313 = load float, float* %call245, align 4, !tbaa !14
  %mul246 = fmul float %conv244, %313
  store float %mul246, float* %ref.tmp242, align 4, !tbaa !14
  %314 = bitcast float* %ref.tmp247 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %314) #9
  %315 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx248 = getelementptr inbounds double, double* %315, i32 2
  %316 = load double, double* %arrayidx248, align 8, !tbaa !18
  %conv249 = fptrunc double %316 to float
  %call250 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %317 = load float, float* %call250, align 4, !tbaa !14
  %mul251 = fmul float %conv249, %317
  store float %mul251, float* %ref.tmp247, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx236, float* nonnull align 4 dereferenceable(4) %ref.tmp237, float* nonnull align 4 dereferenceable(4) %ref.tmp242, float* nonnull align 4 dereferenceable(4) %ref.tmp247)
  %318 = bitcast float* %ref.tmp247 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %318) #9
  %319 = bitcast float* %ref.tmp242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %319) #9
  %320 = bitcast float* %ref.tmp237 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %320) #9
  %321 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay252 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %322 = load i32, i32* %part, align 4, !tbaa !6
  %323 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %324 = bitcast %class.btInternalTriangleIndexCallback* %321 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable253 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %324, align 4, !tbaa !8
  %vfn254 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable253, i64 2
  %325 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn254, align 4
  call void %325(%class.btInternalTriangleIndexCallback* %321, %class.btVector3* %arraydecay252, i32 %322, i32 %323)
  %326 = bitcast i32** %tri_indices192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %326) #9
  br label %for.inc255

for.inc255:                                       ; preds = %for.body191
  %327 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc256 = add nsw i32 %327, 1
  store i32 %inc256, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond189

for.end257:                                       ; preds = %for.cond189
  br label %sw.epilog405

sw.bb258:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond259

for.cond259:                                      ; preds = %for.inc328, %sw.bb258
  %328 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %329 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp260 = icmp slt i32 %328, %329
  br i1 %cmp260, label %for.body261, label %for.end330

for.body261:                                      ; preds = %for.cond259
  %330 = bitcast i16** %tri_indices262 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %330) #9
  %331 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %332 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %333 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul263 = mul nsw i32 %332, %333
  %add.ptr264 = getelementptr inbounds i8, i8* %331, i32 %mul263
  %334 = bitcast i8* %add.ptr264 to i16*
  store i16* %334, i16** %tri_indices262, align 4, !tbaa !2
  %335 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %336 = load i16*, i16** %tri_indices262, align 4, !tbaa !2
  %arrayidx265 = getelementptr inbounds i16, i16* %336, i32 0
  %337 = load i16, i16* %arrayidx265, align 2, !tbaa !16
  %conv266 = zext i16 %337 to i32
  %338 = load i32, i32* %stride, align 4, !tbaa !6
  %mul267 = mul nsw i32 %conv266, %338
  %add.ptr268 = getelementptr inbounds i8, i8* %335, i32 %mul267
  %339 = bitcast i8* %add.ptr268 to double*
  store double* %339, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx269 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %340 = bitcast float* %ref.tmp270 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %340) #9
  %341 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx271 = getelementptr inbounds double, double* %341, i32 0
  %342 = load double, double* %arrayidx271, align 8, !tbaa !18
  %conv272 = fptrunc double %342 to float
  %call273 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %343 = load float, float* %call273, align 4, !tbaa !14
  %mul274 = fmul float %conv272, %343
  store float %mul274, float* %ref.tmp270, align 4, !tbaa !14
  %344 = bitcast float* %ref.tmp275 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %344) #9
  %345 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx276 = getelementptr inbounds double, double* %345, i32 1
  %346 = load double, double* %arrayidx276, align 8, !tbaa !18
  %conv277 = fptrunc double %346 to float
  %call278 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %347 = load float, float* %call278, align 4, !tbaa !14
  %mul279 = fmul float %conv277, %347
  store float %mul279, float* %ref.tmp275, align 4, !tbaa !14
  %348 = bitcast float* %ref.tmp280 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %348) #9
  %349 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx281 = getelementptr inbounds double, double* %349, i32 2
  %350 = load double, double* %arrayidx281, align 8, !tbaa !18
  %conv282 = fptrunc double %350 to float
  %call283 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %351 = load float, float* %call283, align 4, !tbaa !14
  %mul284 = fmul float %conv282, %351
  store float %mul284, float* %ref.tmp280, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx269, float* nonnull align 4 dereferenceable(4) %ref.tmp270, float* nonnull align 4 dereferenceable(4) %ref.tmp275, float* nonnull align 4 dereferenceable(4) %ref.tmp280)
  %352 = bitcast float* %ref.tmp280 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %352) #9
  %353 = bitcast float* %ref.tmp275 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %353) #9
  %354 = bitcast float* %ref.tmp270 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %354) #9
  %355 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %356 = load i16*, i16** %tri_indices262, align 4, !tbaa !2
  %arrayidx285 = getelementptr inbounds i16, i16* %356, i32 1
  %357 = load i16, i16* %arrayidx285, align 2, !tbaa !16
  %conv286 = zext i16 %357 to i32
  %358 = load i32, i32* %stride, align 4, !tbaa !6
  %mul287 = mul nsw i32 %conv286, %358
  %add.ptr288 = getelementptr inbounds i8, i8* %355, i32 %mul287
  %359 = bitcast i8* %add.ptr288 to double*
  store double* %359, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx289 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %360 = bitcast float* %ref.tmp290 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %360) #9
  %361 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx291 = getelementptr inbounds double, double* %361, i32 0
  %362 = load double, double* %arrayidx291, align 8, !tbaa !18
  %conv292 = fptrunc double %362 to float
  %call293 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %363 = load float, float* %call293, align 4, !tbaa !14
  %mul294 = fmul float %conv292, %363
  store float %mul294, float* %ref.tmp290, align 4, !tbaa !14
  %364 = bitcast float* %ref.tmp295 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %364) #9
  %365 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx296 = getelementptr inbounds double, double* %365, i32 1
  %366 = load double, double* %arrayidx296, align 8, !tbaa !18
  %conv297 = fptrunc double %366 to float
  %call298 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %367 = load float, float* %call298, align 4, !tbaa !14
  %mul299 = fmul float %conv297, %367
  store float %mul299, float* %ref.tmp295, align 4, !tbaa !14
  %368 = bitcast float* %ref.tmp300 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %368) #9
  %369 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx301 = getelementptr inbounds double, double* %369, i32 2
  %370 = load double, double* %arrayidx301, align 8, !tbaa !18
  %conv302 = fptrunc double %370 to float
  %call303 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %371 = load float, float* %call303, align 4, !tbaa !14
  %mul304 = fmul float %conv302, %371
  store float %mul304, float* %ref.tmp300, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx289, float* nonnull align 4 dereferenceable(4) %ref.tmp290, float* nonnull align 4 dereferenceable(4) %ref.tmp295, float* nonnull align 4 dereferenceable(4) %ref.tmp300)
  %372 = bitcast float* %ref.tmp300 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %372) #9
  %373 = bitcast float* %ref.tmp295 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %373) #9
  %374 = bitcast float* %ref.tmp290 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %374) #9
  %375 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %376 = load i16*, i16** %tri_indices262, align 4, !tbaa !2
  %arrayidx305 = getelementptr inbounds i16, i16* %376, i32 2
  %377 = load i16, i16* %arrayidx305, align 2, !tbaa !16
  %conv306 = zext i16 %377 to i32
  %378 = load i32, i32* %stride, align 4, !tbaa !6
  %mul307 = mul nsw i32 %conv306, %378
  %add.ptr308 = getelementptr inbounds i8, i8* %375, i32 %mul307
  %379 = bitcast i8* %add.ptr308 to double*
  store double* %379, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx309 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %380 = bitcast float* %ref.tmp310 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %380) #9
  %381 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx311 = getelementptr inbounds double, double* %381, i32 0
  %382 = load double, double* %arrayidx311, align 8, !tbaa !18
  %conv312 = fptrunc double %382 to float
  %call313 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %383 = load float, float* %call313, align 4, !tbaa !14
  %mul314 = fmul float %conv312, %383
  store float %mul314, float* %ref.tmp310, align 4, !tbaa !14
  %384 = bitcast float* %ref.tmp315 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %384) #9
  %385 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx316 = getelementptr inbounds double, double* %385, i32 1
  %386 = load double, double* %arrayidx316, align 8, !tbaa !18
  %conv317 = fptrunc double %386 to float
  %call318 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %387 = load float, float* %call318, align 4, !tbaa !14
  %mul319 = fmul float %conv317, %387
  store float %mul319, float* %ref.tmp315, align 4, !tbaa !14
  %388 = bitcast float* %ref.tmp320 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %388) #9
  %389 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx321 = getelementptr inbounds double, double* %389, i32 2
  %390 = load double, double* %arrayidx321, align 8, !tbaa !18
  %conv322 = fptrunc double %390 to float
  %call323 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %391 = load float, float* %call323, align 4, !tbaa !14
  %mul324 = fmul float %conv322, %391
  store float %mul324, float* %ref.tmp320, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx309, float* nonnull align 4 dereferenceable(4) %ref.tmp310, float* nonnull align 4 dereferenceable(4) %ref.tmp315, float* nonnull align 4 dereferenceable(4) %ref.tmp320)
  %392 = bitcast float* %ref.tmp320 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %392) #9
  %393 = bitcast float* %ref.tmp315 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %393) #9
  %394 = bitcast float* %ref.tmp310 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #9
  %395 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay325 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %396 = load i32, i32* %part, align 4, !tbaa !6
  %397 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %398 = bitcast %class.btInternalTriangleIndexCallback* %395 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable326 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %398, align 4, !tbaa !8
  %vfn327 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable326, i64 2
  %399 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn327, align 4
  call void %399(%class.btInternalTriangleIndexCallback* %395, %class.btVector3* %arraydecay325, i32 %396, i32 %397)
  %400 = bitcast i16** %tri_indices262 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %400) #9
  br label %for.inc328

for.inc328:                                       ; preds = %for.body261
  %401 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc329 = add nsw i32 %401, 1
  store i32 %inc329, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond259

for.end330:                                       ; preds = %for.cond259
  br label %sw.epilog405

sw.bb331:                                         ; preds = %sw.bb186
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond332

for.cond332:                                      ; preds = %for.inc401, %sw.bb331
  %402 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %403 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp333 = icmp slt i32 %402, %403
  br i1 %cmp333, label %for.body334, label %for.end403

for.body334:                                      ; preds = %for.cond332
  %404 = bitcast i8** %tri_indices335 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %404) #9
  %405 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %406 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %407 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul336 = mul nsw i32 %406, %407
  %add.ptr337 = getelementptr inbounds i8, i8* %405, i32 %mul336
  store i8* %add.ptr337, i8** %tri_indices335, align 4, !tbaa !2
  %408 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %409 = load i8*, i8** %tri_indices335, align 4, !tbaa !2
  %arrayidx338 = getelementptr inbounds i8, i8* %409, i32 0
  %410 = load i8, i8* %arrayidx338, align 1, !tbaa !11
  %conv339 = zext i8 %410 to i32
  %411 = load i32, i32* %stride, align 4, !tbaa !6
  %mul340 = mul nsw i32 %conv339, %411
  %add.ptr341 = getelementptr inbounds i8, i8* %408, i32 %mul340
  %412 = bitcast i8* %add.ptr341 to double*
  store double* %412, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx342 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %413 = bitcast float* %ref.tmp343 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %413) #9
  %414 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx344 = getelementptr inbounds double, double* %414, i32 0
  %415 = load double, double* %arrayidx344, align 8, !tbaa !18
  %conv345 = fptrunc double %415 to float
  %call346 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %416 = load float, float* %call346, align 4, !tbaa !14
  %mul347 = fmul float %conv345, %416
  store float %mul347, float* %ref.tmp343, align 4, !tbaa !14
  %417 = bitcast float* %ref.tmp348 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %417) #9
  %418 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx349 = getelementptr inbounds double, double* %418, i32 1
  %419 = load double, double* %arrayidx349, align 8, !tbaa !18
  %conv350 = fptrunc double %419 to float
  %call351 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %420 = load float, float* %call351, align 4, !tbaa !14
  %mul352 = fmul float %conv350, %420
  store float %mul352, float* %ref.tmp348, align 4, !tbaa !14
  %421 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %421) #9
  %422 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx354 = getelementptr inbounds double, double* %422, i32 2
  %423 = load double, double* %arrayidx354, align 8, !tbaa !18
  %conv355 = fptrunc double %423 to float
  %call356 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %424 = load float, float* %call356, align 4, !tbaa !14
  %mul357 = fmul float %conv355, %424
  store float %mul357, float* %ref.tmp353, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx342, float* nonnull align 4 dereferenceable(4) %ref.tmp343, float* nonnull align 4 dereferenceable(4) %ref.tmp348, float* nonnull align 4 dereferenceable(4) %ref.tmp353)
  %425 = bitcast float* %ref.tmp353 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %425) #9
  %426 = bitcast float* %ref.tmp348 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %426) #9
  %427 = bitcast float* %ref.tmp343 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %427) #9
  %428 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %429 = load i8*, i8** %tri_indices335, align 4, !tbaa !2
  %arrayidx358 = getelementptr inbounds i8, i8* %429, i32 1
  %430 = load i8, i8* %arrayidx358, align 1, !tbaa !11
  %conv359 = zext i8 %430 to i32
  %431 = load i32, i32* %stride, align 4, !tbaa !6
  %mul360 = mul nsw i32 %conv359, %431
  %add.ptr361 = getelementptr inbounds i8, i8* %428, i32 %mul360
  %432 = bitcast i8* %add.ptr361 to double*
  store double* %432, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx362 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 1
  %433 = bitcast float* %ref.tmp363 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %433) #9
  %434 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx364 = getelementptr inbounds double, double* %434, i32 0
  %435 = load double, double* %arrayidx364, align 8, !tbaa !18
  %conv365 = fptrunc double %435 to float
  %call366 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %436 = load float, float* %call366, align 4, !tbaa !14
  %mul367 = fmul float %conv365, %436
  store float %mul367, float* %ref.tmp363, align 4, !tbaa !14
  %437 = bitcast float* %ref.tmp368 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %437) #9
  %438 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx369 = getelementptr inbounds double, double* %438, i32 1
  %439 = load double, double* %arrayidx369, align 8, !tbaa !18
  %conv370 = fptrunc double %439 to float
  %call371 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %440 = load float, float* %call371, align 4, !tbaa !14
  %mul372 = fmul float %conv370, %440
  store float %mul372, float* %ref.tmp368, align 4, !tbaa !14
  %441 = bitcast float* %ref.tmp373 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %441) #9
  %442 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx374 = getelementptr inbounds double, double* %442, i32 2
  %443 = load double, double* %arrayidx374, align 8, !tbaa !18
  %conv375 = fptrunc double %443 to float
  %call376 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %444 = load float, float* %call376, align 4, !tbaa !14
  %mul377 = fmul float %conv375, %444
  store float %mul377, float* %ref.tmp373, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx362, float* nonnull align 4 dereferenceable(4) %ref.tmp363, float* nonnull align 4 dereferenceable(4) %ref.tmp368, float* nonnull align 4 dereferenceable(4) %ref.tmp373)
  %445 = bitcast float* %ref.tmp373 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %445) #9
  %446 = bitcast float* %ref.tmp368 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %446) #9
  %447 = bitcast float* %ref.tmp363 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %447) #9
  %448 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %449 = load i8*, i8** %tri_indices335, align 4, !tbaa !2
  %arrayidx378 = getelementptr inbounds i8, i8* %449, i32 2
  %450 = load i8, i8* %arrayidx378, align 1, !tbaa !11
  %conv379 = zext i8 %450 to i32
  %451 = load i32, i32* %stride, align 4, !tbaa !6
  %mul380 = mul nsw i32 %conv379, %451
  %add.ptr381 = getelementptr inbounds i8, i8* %448, i32 %mul380
  %452 = bitcast i8* %add.ptr381 to double*
  store double* %452, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx382 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 2
  %453 = bitcast float* %ref.tmp383 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %453) #9
  %454 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx384 = getelementptr inbounds double, double* %454, i32 0
  %455 = load double, double* %arrayidx384, align 8, !tbaa !18
  %conv385 = fptrunc double %455 to float
  %call386 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %meshScaling)
  %456 = load float, float* %call386, align 4, !tbaa !14
  %mul387 = fmul float %conv385, %456
  store float %mul387, float* %ref.tmp383, align 4, !tbaa !14
  %457 = bitcast float* %ref.tmp388 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %457) #9
  %458 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx389 = getelementptr inbounds double, double* %458, i32 1
  %459 = load double, double* %arrayidx389, align 8, !tbaa !18
  %conv390 = fptrunc double %459 to float
  %call391 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %meshScaling)
  %460 = load float, float* %call391, align 4, !tbaa !14
  %mul392 = fmul float %conv390, %460
  store float %mul392, float* %ref.tmp388, align 4, !tbaa !14
  %461 = bitcast float* %ref.tmp393 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %461) #9
  %462 = load double*, double** %graphicsbase187, align 4, !tbaa !2
  %arrayidx394 = getelementptr inbounds double, double* %462, i32 2
  %463 = load double, double* %arrayidx394, align 8, !tbaa !18
  %conv395 = fptrunc double %463 to float
  %call396 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %meshScaling)
  %464 = load float, float* %call396, align 4, !tbaa !14
  %mul397 = fmul float %conv395, %464
  store float %mul397, float* %ref.tmp393, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx382, float* nonnull align 4 dereferenceable(4) %ref.tmp383, float* nonnull align 4 dereferenceable(4) %ref.tmp388, float* nonnull align 4 dereferenceable(4) %ref.tmp393)
  %465 = bitcast float* %ref.tmp393 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %465) #9
  %466 = bitcast float* %ref.tmp388 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %466) #9
  %467 = bitcast float* %ref.tmp383 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %467) #9
  %468 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %callback.addr, align 4, !tbaa !2
  %arraydecay398 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangle, i32 0, i32 0
  %469 = load i32, i32* %part, align 4, !tbaa !6
  %470 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %471 = bitcast %class.btInternalTriangleIndexCallback* %468 to void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)***
  %vtable399 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)**, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*** %471, align 4, !tbaa !8
  %vfn400 = getelementptr inbounds void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vtable399, i64 2
  %472 = load void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)*, void (%class.btInternalTriangleIndexCallback*, %class.btVector3*, i32, i32)** %vfn400, align 4
  call void %472(%class.btInternalTriangleIndexCallback* %468, %class.btVector3* %arraydecay398, i32 %469, i32 %470)
  %473 = bitcast i8** %tri_indices335 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %473) #9
  br label %for.inc401

for.inc401:                                       ; preds = %for.body334
  %474 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc402 = add nsw i32 %474, 1
  store i32 %inc402, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond332

for.end403:                                       ; preds = %for.cond332
  br label %sw.epilog405

sw.default404:                                    ; preds = %sw.bb186
  br label %sw.epilog405

sw.epilog405:                                     ; preds = %sw.default404, %for.end403, %for.end330, %for.end257
  %475 = bitcast double** %graphicsbase187 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %475) #9
  br label %sw.epilog407

sw.default406:                                    ; preds = %for.body
  br label %sw.epilog407

sw.epilog407:                                     ; preds = %sw.default406, %sw.epilog405, %sw.epilog
  %476 = load i32, i32* %part, align 4, !tbaa !6
  %477 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i32)***
  %vtable408 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %477, align 4, !tbaa !8
  %vfn409 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable408, i64 6
  %478 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn409, align 4
  call void %478(%class.btStridingMeshInterface* %this1, i32 %476)
  br label %for.inc410

for.inc410:                                       ; preds = %sw.epilog407
  %479 = load i32, i32* %part, align 4, !tbaa !6
  %inc411 = add nsw i32 %479, 1
  store i32 %inc411, i32* %part, align 4, !tbaa !6
  br label %for.cond

for.end412:                                       ; preds = %for.cond
  %480 = bitcast %class.btVector3* %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %480) #9
  %481 = bitcast [3 x %class.btVector3]* %triangle to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %481) #9
  %482 = bitcast i32* %gfxindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %482) #9
  %483 = bitcast i32* %numtriangles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %483) #9
  %484 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %484) #9
  %485 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %485) #9
  %486 = bitcast i32* %gfxindextype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %486) #9
  %487 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %487) #9
  %488 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %488) #9
  %489 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %489) #9
  %490 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %490) #9
  %491 = bitcast i32* %graphicssubparts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %491) #9
  %492 = bitcast i32* %part to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %492) #9
  %493 = bitcast i32* %numtotalphysicsverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %493) #9
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

define hidden void @_ZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_(%class.btStridingMeshInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %aabbCallback = alloca %struct.AabbCalculationCallback, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = bitcast %struct.AabbCalculationCallback* %aabbCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %0) #9
  %call = call %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackC2Ev(%struct.AabbCalculationCallback* %aabbCallback)
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp2, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp3, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !14
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %8, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast %struct.AabbCalculationCallback* %aabbCallback to %class.btInternalTriangleIndexCallback*
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %18 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*** %18, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %19 = load void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %19(%class.btStridingMeshInterface* %this1, %class.btInternalTriangleIndexCallback* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %aabbCallback, i32 0, i32 1
  %20 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %21 = bitcast %class.btVector3* %20 to i8*
  %22 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !10
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %aabbCallback, i32 0, i32 2
  %23 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !10
  %call7 = call %struct.AabbCalculationCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.AabbCalculationCallback* (%struct.AabbCalculationCallback*)*)(%struct.AabbCalculationCallback* %aabbCallback) #9
  %26 = bitcast %struct.AabbCalculationCallback* %aabbCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %26) #9
  ret void
}

define internal %struct.AabbCalculationCallback* @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackC2Ev(%struct.AabbCalculationCallback* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %0 = bitcast %struct.AabbCalculationCallback* %this1 to %class.btInternalTriangleIndexCallback*
  %call = call %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* %0) #9
  %1 = bitcast %struct.AabbCalculationCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_E23AabbCalculationCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  %m_aabbMin4 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_aabbMin4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %m_aabbMax7 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %8 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4, !tbaa !14
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp9, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 0xC3ABC16D60000000, float* %ref.tmp10, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_aabbMax7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %11 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret %struct.AabbCalculationCallback* %this1
}

; Function Attrs: nounwind
declare %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackD2Ev(%class.btInternalTriangleIndexCallback* returned) unnamed_addr #5

define hidden i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %trimeshData = alloca %struct.btStridingMeshInterfaceData*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btMeshPartData*, align 4
  %part = alloca i32, align 4
  %graphicssubparts = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %type = alloca i32, align 4
  %gfxindextype = alloca i32, align 4
  %stride = alloca i32, align 4
  %numverts = alloca i32, align 4
  %numtriangles = alloca i32, align 4
  %gfxindex = alloca i32, align 4
  %numindices = alloca i32, align 4
  %chunk18 = alloca %class.btChunk*, align 4
  %tmpIndices = alloca %struct.btIntIndexData*, align 4
  %tri_indices = alloca i32*, align 4
  %chunk48 = alloca %class.btChunk*, align 4
  %tmpIndices52 = alloca %struct.btShortIntIndexTripletData*, align 4
  %tri_indices61 = alloca i16*, align 4
  %chunk85 = alloca %class.btChunk*, align 4
  %tmpIndices89 = alloca %struct.btCharIndexTripletData*, align 4
  %tri_indices98 = alloca i8*, align 4
  %graphicsbase = alloca float*, align 4
  %chunk123 = alloca %class.btChunk*, align 4
  %tmpVertices = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  %chunk158 = alloca %class.btChunk*, align 4
  %tmpVertices162 = alloca %struct.btVector3DoubleData*, align 4
  %i168 = alloca i32, align 4
  %graphicsbase173 = alloca double*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = bitcast %struct.btStridingMeshInterfaceData** %trimeshData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btStridingMeshInterfaceData*
  store %struct.btStridingMeshInterfaceData* %2, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %3 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %4 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call = call i32 %4(%class.btStridingMeshInterface* %this1)
  %5 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_numMeshParts = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %5, i32 0, i32 2
  store i32 %call, i32* %m_numMeshParts, align 4, !tbaa !20
  %6 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_meshPartsPtr = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %6, i32 0, i32 0
  store %struct.btMeshPartData* null, %struct.btMeshPartData** %m_meshPartsPtr, align 4, !tbaa !23
  %7 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_numMeshParts2 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %7, i32 0, i32 2
  %8 = load i32, i32* %m_numMeshParts2, align 4, !tbaa !20
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %if.then, label %if.end205

if.then:                                          ; preds = %entry
  %9 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %11 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_numMeshParts3 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %11, i32 0, i32 2
  %12 = load i32, i32* %m_numMeshParts3, align 4, !tbaa !20
  %13 = bitcast %class.btSerializer* %10 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable4 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %13, align 4, !tbaa !8
  %vfn5 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable4, i64 4
  %14 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn5, align 4
  %call6 = call %class.btChunk* %14(%class.btSerializer* %10, i32 32, i32 %12)
  store %class.btChunk* %call6, %class.btChunk** %chunk, align 4, !tbaa !2
  %15 = bitcast %struct.btMeshPartData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %16, i32 0, i32 2
  %17 = load i8*, i8** %m_oldPtr, align 4, !tbaa !24
  %18 = bitcast i8* %17 to %struct.btMeshPartData*
  store %struct.btMeshPartData* %18, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %19 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %20 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %21 = bitcast %struct.btMeshPartData* %20 to i8*
  %22 = bitcast %class.btSerializer* %19 to i8* (%class.btSerializer*, i8*)***
  %vtable7 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %22, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable7, i64 7
  %23 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn8, align 4
  %call9 = call i8* %23(%class.btSerializer* %19, i8* %21)
  %24 = bitcast i8* %call9 to %struct.btMeshPartData*
  %25 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_meshPartsPtr10 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %25, i32 0, i32 0
  store %struct.btMeshPartData* %24, %struct.btMeshPartData** %m_meshPartsPtr10, align 4, !tbaa !23
  %26 = bitcast i32* %part to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  %27 = bitcast i32* %graphicssubparts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  %28 = bitcast %class.btStridingMeshInterface* %this1 to i32 (%class.btStridingMeshInterface*)***
  %vtable11 = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %28, align 4, !tbaa !8
  %vfn12 = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable11, i64 7
  %29 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn12, align 4
  %call13 = call i32 %29(%class.btStridingMeshInterface* %this1)
  store i32 %call13, i32* %graphicssubparts, align 4, !tbaa !6
  %30 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %32 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  %33 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %34 = bitcast i32* %gfxindextype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  %36 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #9
  %37 = bitcast i32* %numtriangles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = bitcast i32* %gfxindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store i32 0, i32* %part, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc199, %if.then
  %39 = load i32, i32* %part, align 4, !tbaa !6
  %40 = load i32, i32* %graphicssubparts, align 4, !tbaa !6
  %cmp = icmp slt i32 %39, %40
  br i1 %cmp, label %for.body, label %for.end201

for.body:                                         ; preds = %for.cond
  %41 = load i32, i32* %part, align 4, !tbaa !6
  %42 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable14 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %42, align 4, !tbaa !8
  %vfn15 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable14, i64 4
  %43 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn15, align 4
  call void %43(%class.btStridingMeshInterface* %this1, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numtriangles, i32* nonnull align 4 dereferenceable(4) %gfxindextype, i32 %41)
  %44 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %45 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_numTriangles = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %45, i32 0, i32 6
  store i32 %44, i32* %m_numTriangles, align 4, !tbaa !26
  %46 = load i32, i32* %numverts, align 4, !tbaa !6
  %47 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_numVertices = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %47, i32 0, i32 7
  store i32 %46, i32* %m_numVertices, align 4, !tbaa !28
  %48 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_indices16 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %48, i32 0, i32 5
  store %struct.btShortIntIndexData* null, %struct.btShortIntIndexData** %m_indices16, align 4, !tbaa !29
  %49 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_indices32 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %49, i32 0, i32 2
  store %struct.btIntIndexData* null, %struct.btIntIndexData** %m_indices32, align 4, !tbaa !30
  %50 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_3indices16 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %50, i32 0, i32 3
  store %struct.btShortIntIndexTripletData* null, %struct.btShortIntIndexTripletData** %m_3indices16, align 4, !tbaa !31
  %51 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_3indices8 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %51, i32 0, i32 4
  store %struct.btCharIndexTripletData* null, %struct.btCharIndexTripletData** %m_3indices8, align 4, !tbaa !32
  %52 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_vertices3f = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %52, i32 0, i32 0
  store %struct.btVector3FloatData* null, %struct.btVector3FloatData** %m_vertices3f, align 4, !tbaa !33
  %53 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_vertices3d = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %53, i32 0, i32 1
  store %struct.btVector3DoubleData* null, %struct.btVector3DoubleData** %m_vertices3d, align 4, !tbaa !34
  %54 = load i32, i32* %gfxindextype, align 4, !tbaa !12
  switch i32 %54, label %sw.default [
    i32 2, label %sw.bb
    i32 3, label %sw.bb45
    i32 5, label %sw.bb82
  ]

sw.bb:                                            ; preds = %for.body
  %55 = bitcast i32* %numindices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #9
  %56 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %mul = mul nsw i32 %56, 3
  store i32 %mul, i32* %numindices, align 4, !tbaa !6
  %57 = load i32, i32* %numindices, align 4, !tbaa !6
  %tobool16 = icmp ne i32 %57, 0
  br i1 %tobool16, label %if.then17, label %if.end

if.then17:                                        ; preds = %sw.bb
  %58 = bitcast %class.btChunk** %chunk18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #9
  %59 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %60 = load i32, i32* %numindices, align 4, !tbaa !6
  %61 = bitcast %class.btSerializer* %59 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable19 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %61, align 4, !tbaa !8
  %vfn20 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable19, i64 4
  %62 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn20, align 4
  %call21 = call %class.btChunk* %62(%class.btSerializer* %59, i32 4, i32 %60)
  store %class.btChunk* %call21, %class.btChunk** %chunk18, align 4, !tbaa !2
  %63 = bitcast %struct.btIntIndexData** %tmpIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #9
  %64 = load %class.btChunk*, %class.btChunk** %chunk18, align 4, !tbaa !2
  %m_oldPtr22 = getelementptr inbounds %class.btChunk, %class.btChunk* %64, i32 0, i32 2
  %65 = load i8*, i8** %m_oldPtr22, align 4, !tbaa !24
  %66 = bitcast i8* %65 to %struct.btIntIndexData*
  store %struct.btIntIndexData* %66, %struct.btIntIndexData** %tmpIndices, align 4, !tbaa !2
  %67 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %68 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4, !tbaa !2
  %69 = bitcast %struct.btIntIndexData* %68 to i8*
  %70 = bitcast %class.btSerializer* %67 to i8* (%class.btSerializer*, i8*)***
  %vtable23 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %70, align 4, !tbaa !8
  %vfn24 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable23, i64 7
  %71 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn24, align 4
  %call25 = call i8* %71(%class.btSerializer* %67, i8* %69)
  %72 = bitcast i8* %call25 to %struct.btIntIndexData*
  %73 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_indices3226 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %73, i32 0, i32 2
  store %struct.btIntIndexData* %72, %struct.btIntIndexData** %m_indices3226, align 4, !tbaa !30
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc, %if.then17
  %74 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %75 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp28 = icmp slt i32 %74, %75
  br i1 %cmp28, label %for.body29, label %for.end

for.body29:                                       ; preds = %for.cond27
  %76 = bitcast i32** %tri_indices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #9
  %77 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %78 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %79 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul30 = mul nsw i32 %78, %79
  %add.ptr = getelementptr inbounds i8, i8* %77, i32 %mul30
  %80 = bitcast i8* %add.ptr to i32*
  store i32* %80, i32** %tri_indices, align 4, !tbaa !2
  %81 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %81, i32 0
  %82 = load i32, i32* %arrayidx, align 4, !tbaa !6
  %83 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4, !tbaa !2
  %84 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %mul31 = mul nsw i32 %84, 3
  %arrayidx32 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %83, i32 %mul31
  %m_value = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx32, i32 0, i32 0
  store i32 %82, i32* %m_value, align 4, !tbaa !35
  %85 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i32, i32* %85, i32 1
  %86 = load i32, i32* %arrayidx33, align 4, !tbaa !6
  %87 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4, !tbaa !2
  %88 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %mul34 = mul nsw i32 %88, 3
  %add = add nsw i32 %mul34, 1
  %arrayidx35 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %87, i32 %add
  %m_value36 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx35, i32 0, i32 0
  store i32 %86, i32* %m_value36, align 4, !tbaa !35
  %89 = load i32*, i32** %tri_indices, align 4, !tbaa !2
  %arrayidx37 = getelementptr inbounds i32, i32* %89, i32 2
  %90 = load i32, i32* %arrayidx37, align 4, !tbaa !6
  %91 = load %struct.btIntIndexData*, %struct.btIntIndexData** %tmpIndices, align 4, !tbaa !2
  %92 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %mul38 = mul nsw i32 %92, 3
  %add39 = add nsw i32 %mul38, 2
  %arrayidx40 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %91, i32 %add39
  %m_value41 = getelementptr inbounds %struct.btIntIndexData, %struct.btIntIndexData* %arrayidx40, i32 0, i32 0
  store i32 %90, i32* %m_value41, align 4, !tbaa !35
  %93 = bitcast i32** %tri_indices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body29
  %94 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc = add nsw i32 %94, 1
  store i32 %inc, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond27

for.end:                                          ; preds = %for.cond27
  %95 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %96 = load %class.btChunk*, %class.btChunk** %chunk18, align 4, !tbaa !2
  %97 = load %class.btChunk*, %class.btChunk** %chunk18, align 4, !tbaa !2
  %m_oldPtr42 = getelementptr inbounds %class.btChunk, %class.btChunk* %97, i32 0, i32 2
  %98 = load i8*, i8** %m_oldPtr42, align 4, !tbaa !24
  %99 = bitcast %class.btSerializer* %95 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable43 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %99, align 4, !tbaa !8
  %vfn44 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable43, i64 5
  %100 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn44, align 4
  call void %100(%class.btSerializer* %95, %class.btChunk* %96, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %98)
  %101 = bitcast %struct.btIntIndexData** %tmpIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast %class.btChunk** %chunk18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  br label %if.end

if.end:                                           ; preds = %for.end, %sw.bb
  %103 = bitcast i32* %numindices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  br label %sw.epilog

sw.bb45:                                          ; preds = %for.body
  %104 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %tobool46 = icmp ne i32 %104, 0
  br i1 %tobool46, label %if.then47, label %if.end81

if.then47:                                        ; preds = %sw.bb45
  %105 = bitcast %class.btChunk** %chunk48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #9
  %106 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %107 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %108 = bitcast %class.btSerializer* %106 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable49 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %108, align 4, !tbaa !8
  %vfn50 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable49, i64 4
  %109 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn50, align 4
  %call51 = call %class.btChunk* %109(%class.btSerializer* %106, i32 8, i32 %107)
  store %class.btChunk* %call51, %class.btChunk** %chunk48, align 4, !tbaa !2
  %110 = bitcast %struct.btShortIntIndexTripletData** %tmpIndices52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #9
  %111 = load %class.btChunk*, %class.btChunk** %chunk48, align 4, !tbaa !2
  %m_oldPtr53 = getelementptr inbounds %class.btChunk, %class.btChunk* %111, i32 0, i32 2
  %112 = load i8*, i8** %m_oldPtr53, align 4, !tbaa !24
  %113 = bitcast i8* %112 to %struct.btShortIntIndexTripletData*
  store %struct.btShortIntIndexTripletData* %113, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4, !tbaa !2
  %114 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %115 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4, !tbaa !2
  %116 = bitcast %struct.btShortIntIndexTripletData* %115 to i8*
  %117 = bitcast %class.btSerializer* %114 to i8* (%class.btSerializer*, i8*)***
  %vtable54 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %117, align 4, !tbaa !8
  %vfn55 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable54, i64 7
  %118 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn55, align 4
  %call56 = call i8* %118(%class.btSerializer* %114, i8* %116)
  %119 = bitcast i8* %call56 to %struct.btShortIntIndexTripletData*
  %120 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_3indices1657 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %120, i32 0, i32 3
  store %struct.btShortIntIndexTripletData* %119, %struct.btShortIntIndexTripletData** %m_3indices1657, align 4, !tbaa !31
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc75, %if.then47
  %121 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %122 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp59 = icmp slt i32 %121, %122
  br i1 %cmp59, label %for.body60, label %for.end77

for.body60:                                       ; preds = %for.cond58
  %123 = bitcast i16** %tri_indices61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #9
  %124 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %125 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %126 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul62 = mul nsw i32 %125, %126
  %add.ptr63 = getelementptr inbounds i8, i8* %124, i32 %mul62
  %127 = bitcast i8* %add.ptr63 to i16*
  store i16* %127, i16** %tri_indices61, align 4, !tbaa !2
  %128 = load i16*, i16** %tri_indices61, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds i16, i16* %128, i32 0
  %129 = load i16, i16* %arrayidx64, align 2, !tbaa !16
  %130 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4, !tbaa !2
  %131 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %130, i32 %131
  %m_values = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx65, i32 0, i32 0
  %arrayidx66 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values, i32 0, i32 0
  store i16 %129, i16* %arrayidx66, align 2, !tbaa !16
  %132 = load i16*, i16** %tri_indices61, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds i16, i16* %132, i32 1
  %133 = load i16, i16* %arrayidx67, align 2, !tbaa !16
  %134 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4, !tbaa !2
  %135 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx68 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %134, i32 %135
  %m_values69 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx68, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values69, i32 0, i32 1
  store i16 %133, i16* %arrayidx70, align 2, !tbaa !16
  %136 = load i16*, i16** %tri_indices61, align 4, !tbaa !2
  %arrayidx71 = getelementptr inbounds i16, i16* %136, i32 2
  %137 = load i16, i16* %arrayidx71, align 2, !tbaa !16
  %138 = load %struct.btShortIntIndexTripletData*, %struct.btShortIntIndexTripletData** %tmpIndices52, align 4, !tbaa !2
  %139 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx72 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %138, i32 %139
  %m_values73 = getelementptr inbounds %struct.btShortIntIndexTripletData, %struct.btShortIntIndexTripletData* %arrayidx72, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x i16], [3 x i16]* %m_values73, i32 0, i32 2
  store i16 %137, i16* %arrayidx74, align 2, !tbaa !16
  %140 = bitcast i16** %tri_indices61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #9
  br label %for.inc75

for.inc75:                                        ; preds = %for.body60
  %141 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc76 = add nsw i32 %141, 1
  store i32 %inc76, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond58

for.end77:                                        ; preds = %for.cond58
  %142 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %143 = load %class.btChunk*, %class.btChunk** %chunk48, align 4, !tbaa !2
  %144 = load %class.btChunk*, %class.btChunk** %chunk48, align 4, !tbaa !2
  %m_oldPtr78 = getelementptr inbounds %class.btChunk, %class.btChunk* %144, i32 0, i32 2
  %145 = load i8*, i8** %m_oldPtr78, align 4, !tbaa !24
  %146 = bitcast %class.btSerializer* %142 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable79 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %146, align 4, !tbaa !8
  %vfn80 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable79, i64 5
  %147 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn80, align 4
  call void %147(%class.btSerializer* %142, %class.btChunk* %143, i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str.1, i32 0, i32 0), i32 1497453121, i8* %145)
  %148 = bitcast %struct.btShortIntIndexTripletData** %tmpIndices52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #9
  %149 = bitcast %class.btChunk** %chunk48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #9
  br label %if.end81

if.end81:                                         ; preds = %for.end77, %sw.bb45
  br label %sw.epilog

sw.bb82:                                          ; preds = %for.body
  %150 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %tobool83 = icmp ne i32 %150, 0
  br i1 %tobool83, label %if.then84, label %if.end119

if.then84:                                        ; preds = %sw.bb82
  %151 = bitcast %class.btChunk** %chunk85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %151) #9
  %152 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %153 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %154 = bitcast %class.btSerializer* %152 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable86 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %154, align 4, !tbaa !8
  %vfn87 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable86, i64 4
  %155 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn87, align 4
  %call88 = call %class.btChunk* %155(%class.btSerializer* %152, i32 4, i32 %153)
  store %class.btChunk* %call88, %class.btChunk** %chunk85, align 4, !tbaa !2
  %156 = bitcast %struct.btCharIndexTripletData** %tmpIndices89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #9
  %157 = load %class.btChunk*, %class.btChunk** %chunk85, align 4, !tbaa !2
  %m_oldPtr90 = getelementptr inbounds %class.btChunk, %class.btChunk* %157, i32 0, i32 2
  %158 = load i8*, i8** %m_oldPtr90, align 4, !tbaa !24
  %159 = bitcast i8* %158 to %struct.btCharIndexTripletData*
  store %struct.btCharIndexTripletData* %159, %struct.btCharIndexTripletData** %tmpIndices89, align 4, !tbaa !2
  %160 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %161 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4, !tbaa !2
  %162 = bitcast %struct.btCharIndexTripletData* %161 to i8*
  %163 = bitcast %class.btSerializer* %160 to i8* (%class.btSerializer*, i8*)***
  %vtable91 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %163, align 4, !tbaa !8
  %vfn92 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable91, i64 7
  %164 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn92, align 4
  %call93 = call i8* %164(%class.btSerializer* %160, i8* %162)
  %165 = bitcast i8* %call93 to %struct.btCharIndexTripletData*
  %166 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_3indices894 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %166, i32 0, i32 4
  store %struct.btCharIndexTripletData* %165, %struct.btCharIndexTripletData** %m_3indices894, align 4, !tbaa !32
  store i32 0, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc113, %if.then84
  %167 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %168 = load i32, i32* %numtriangles, align 4, !tbaa !6
  %cmp96 = icmp slt i32 %167, %168
  br i1 %cmp96, label %for.body97, label %for.end115

for.body97:                                       ; preds = %for.cond95
  %169 = bitcast i8** %tri_indices98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #9
  %170 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %171 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %172 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul99 = mul nsw i32 %171, %172
  %add.ptr100 = getelementptr inbounds i8, i8* %170, i32 %mul99
  store i8* %add.ptr100, i8** %tri_indices98, align 4, !tbaa !2
  %173 = load i8*, i8** %tri_indices98, align 4, !tbaa !2
  %arrayidx101 = getelementptr inbounds i8, i8* %173, i32 0
  %174 = load i8, i8* %arrayidx101, align 1, !tbaa !11
  %175 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4, !tbaa !2
  %176 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx102 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %175, i32 %176
  %m_values103 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx102, i32 0, i32 0
  %arrayidx104 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values103, i32 0, i32 0
  store i8 %174, i8* %arrayidx104, align 1, !tbaa !11
  %177 = load i8*, i8** %tri_indices98, align 4, !tbaa !2
  %arrayidx105 = getelementptr inbounds i8, i8* %177, i32 1
  %178 = load i8, i8* %arrayidx105, align 1, !tbaa !11
  %179 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4, !tbaa !2
  %180 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx106 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %179, i32 %180
  %m_values107 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx106, i32 0, i32 0
  %arrayidx108 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values107, i32 0, i32 1
  store i8 %178, i8* %arrayidx108, align 1, !tbaa !11
  %181 = load i8*, i8** %tri_indices98, align 4, !tbaa !2
  %arrayidx109 = getelementptr inbounds i8, i8* %181, i32 2
  %182 = load i8, i8* %arrayidx109, align 1, !tbaa !11
  %183 = load %struct.btCharIndexTripletData*, %struct.btCharIndexTripletData** %tmpIndices89, align 4, !tbaa !2
  %184 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %arrayidx110 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %183, i32 %184
  %m_values111 = getelementptr inbounds %struct.btCharIndexTripletData, %struct.btCharIndexTripletData* %arrayidx110, i32 0, i32 0
  %arrayidx112 = getelementptr inbounds [3 x i8], [3 x i8]* %m_values111, i32 0, i32 2
  store i8 %182, i8* %arrayidx112, align 1, !tbaa !11
  %185 = bitcast i8** %tri_indices98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #9
  br label %for.inc113

for.inc113:                                       ; preds = %for.body97
  %186 = load i32, i32* %gfxindex, align 4, !tbaa !6
  %inc114 = add nsw i32 %186, 1
  store i32 %inc114, i32* %gfxindex, align 4, !tbaa !6
  br label %for.cond95

for.end115:                                       ; preds = %for.cond95
  %187 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %188 = load %class.btChunk*, %class.btChunk** %chunk85, align 4, !tbaa !2
  %189 = load %class.btChunk*, %class.btChunk** %chunk85, align 4, !tbaa !2
  %m_oldPtr116 = getelementptr inbounds %class.btChunk, %class.btChunk* %189, i32 0, i32 2
  %190 = load i8*, i8** %m_oldPtr116, align 4, !tbaa !24
  %191 = bitcast %class.btSerializer* %187 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable117 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %191, align 4, !tbaa !8
  %vfn118 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable117, i64 5
  %192 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn118, align 4
  call void %192(%class.btSerializer* %187, %class.btChunk* %188, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.2, i32 0, i32 0), i32 1497453121, i8* %190)
  %193 = bitcast %struct.btCharIndexTripletData** %tmpIndices89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #9
  %194 = bitcast %class.btChunk** %chunk85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #9
  br label %if.end119

if.end119:                                        ; preds = %for.end115, %sw.bb82
  br label %sw.epilog

sw.default:                                       ; preds = %for.body
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %if.end119, %if.end81, %if.end
  %195 = load i32, i32* %type, align 4, !tbaa !12
  switch i32 %195, label %sw.default195 [
    i32 0, label %sw.bb120
    i32 1, label %sw.bb155
  ]

sw.bb120:                                         ; preds = %sw.epilog
  %196 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #9
  %197 = load i32, i32* %numverts, align 4, !tbaa !6
  %tobool121 = icmp ne i32 %197, 0
  br i1 %tobool121, label %if.then122, label %if.end154

if.then122:                                       ; preds = %sw.bb120
  %198 = bitcast %class.btChunk** %chunk123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #9
  %199 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %200 = load i32, i32* %numverts, align 4, !tbaa !6
  %201 = bitcast %class.btSerializer* %199 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable124 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %201, align 4, !tbaa !8
  %vfn125 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable124, i64 4
  %202 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn125, align 4
  %call126 = call %class.btChunk* %202(%class.btSerializer* %199, i32 16, i32 %200)
  store %class.btChunk* %call126, %class.btChunk** %chunk123, align 4, !tbaa !2
  %203 = bitcast %struct.btVector3FloatData** %tmpVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %203) #9
  %204 = load %class.btChunk*, %class.btChunk** %chunk123, align 4, !tbaa !2
  %m_oldPtr127 = getelementptr inbounds %class.btChunk, %class.btChunk* %204, i32 0, i32 2
  %205 = load i8*, i8** %m_oldPtr127, align 4, !tbaa !24
  %206 = bitcast i8* %205 to %struct.btVector3FloatData*
  store %struct.btVector3FloatData* %206, %struct.btVector3FloatData** %tmpVertices, align 4, !tbaa !2
  %207 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %208 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4, !tbaa !2
  %209 = bitcast %struct.btVector3FloatData* %208 to i8*
  %210 = bitcast %class.btSerializer* %207 to i8* (%class.btSerializer*, i8*)***
  %vtable128 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %210, align 4, !tbaa !8
  %vfn129 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable128, i64 7
  %211 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn129, align 4
  %call130 = call i8* %211(%class.btSerializer* %207, i8* %209)
  %212 = bitcast i8* %call130 to %struct.btVector3FloatData*
  %213 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_vertices3f131 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %213, i32 0, i32 0
  store %struct.btVector3FloatData* %212, %struct.btVector3FloatData** %m_vertices3f131, align 4, !tbaa !33
  %214 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %214) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond132

for.cond132:                                      ; preds = %for.inc148, %if.then122
  %215 = load i32, i32* %i, align 4, !tbaa !6
  %216 = load i32, i32* %numverts, align 4, !tbaa !6
  %cmp133 = icmp slt i32 %215, %216
  br i1 %cmp133, label %for.body134, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond132
  %217 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #9
  br label %for.end150

for.body134:                                      ; preds = %for.cond132
  %218 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %219 = load i32, i32* %i, align 4, !tbaa !6
  %220 = load i32, i32* %stride, align 4, !tbaa !6
  %mul135 = mul nsw i32 %219, %220
  %add.ptr136 = getelementptr inbounds i8, i8* %218, i32 %mul135
  %221 = bitcast i8* %add.ptr136 to float*
  store float* %221, float** %graphicsbase, align 4, !tbaa !2
  %222 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds float, float* %222, i32 0
  %223 = load float, float* %arrayidx137, align 4, !tbaa !14
  %224 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4, !tbaa !2
  %225 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx138 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %224, i32 %225
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx138, i32 0, i32 0
  %arrayidx139 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %223, float* %arrayidx139, align 4, !tbaa !14
  %226 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx140 = getelementptr inbounds float, float* %226, i32 1
  %227 = load float, float* %arrayidx140, align 4, !tbaa !14
  %228 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4, !tbaa !2
  %229 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx141 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %228, i32 %229
  %m_floats142 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx141, i32 0, i32 0
  %arrayidx143 = getelementptr inbounds [4 x float], [4 x float]* %m_floats142, i32 0, i32 1
  store float %227, float* %arrayidx143, align 4, !tbaa !14
  %230 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx144 = getelementptr inbounds float, float* %230, i32 2
  %231 = load float, float* %arrayidx144, align 4, !tbaa !14
  %232 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %tmpVertices, align 4, !tbaa !2
  %233 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx145 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %232, i32 %233
  %m_floats146 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %arrayidx145, i32 0, i32 0
  %arrayidx147 = getelementptr inbounds [4 x float], [4 x float]* %m_floats146, i32 0, i32 2
  store float %231, float* %arrayidx147, align 4, !tbaa !14
  br label %for.inc148

for.inc148:                                       ; preds = %for.body134
  %234 = load i32, i32* %i, align 4, !tbaa !6
  %inc149 = add nsw i32 %234, 1
  store i32 %inc149, i32* %i, align 4, !tbaa !6
  br label %for.cond132

for.end150:                                       ; preds = %for.cond.cleanup
  %235 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %236 = load %class.btChunk*, %class.btChunk** %chunk123, align 4, !tbaa !2
  %237 = load %class.btChunk*, %class.btChunk** %chunk123, align 4, !tbaa !2
  %m_oldPtr151 = getelementptr inbounds %class.btChunk, %class.btChunk* %237, i32 0, i32 2
  %238 = load i8*, i8** %m_oldPtr151, align 4, !tbaa !24
  %239 = bitcast %class.btSerializer* %235 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable152 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %239, align 4, !tbaa !8
  %vfn153 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable152, i64 5
  %240 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn153, align 4
  call void %240(%class.btSerializer* %235, %class.btChunk* %236, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.3, i32 0, i32 0), i32 1497453121, i8* %238)
  %241 = bitcast %struct.btVector3FloatData** %tmpVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #9
  %242 = bitcast %class.btChunk** %chunk123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #9
  br label %if.end154

if.end154:                                        ; preds = %for.end150, %sw.bb120
  %243 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #9
  br label %sw.epilog196

sw.bb155:                                         ; preds = %sw.epilog
  %244 = load i32, i32* %numverts, align 4, !tbaa !6
  %tobool156 = icmp ne i32 %244, 0
  br i1 %tobool156, label %if.then157, label %if.end194

if.then157:                                       ; preds = %sw.bb155
  %245 = bitcast %class.btChunk** %chunk158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %245) #9
  %246 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %247 = load i32, i32* %numverts, align 4, !tbaa !6
  %248 = bitcast %class.btSerializer* %246 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable159 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %248, align 4, !tbaa !8
  %vfn160 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable159, i64 4
  %249 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn160, align 4
  %call161 = call %class.btChunk* %249(%class.btSerializer* %246, i32 32, i32 %247)
  store %class.btChunk* %call161, %class.btChunk** %chunk158, align 4, !tbaa !2
  %250 = bitcast %struct.btVector3DoubleData** %tmpVertices162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %250) #9
  %251 = load %class.btChunk*, %class.btChunk** %chunk158, align 4, !tbaa !2
  %m_oldPtr163 = getelementptr inbounds %class.btChunk, %class.btChunk* %251, i32 0, i32 2
  %252 = load i8*, i8** %m_oldPtr163, align 4, !tbaa !24
  %253 = bitcast i8* %252 to %struct.btVector3DoubleData*
  store %struct.btVector3DoubleData* %253, %struct.btVector3DoubleData** %tmpVertices162, align 4, !tbaa !2
  %254 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %255 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4, !tbaa !2
  %256 = bitcast %struct.btVector3DoubleData* %255 to i8*
  %257 = bitcast %class.btSerializer* %254 to i8* (%class.btSerializer*, i8*)***
  %vtable164 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %257, align 4, !tbaa !8
  %vfn165 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable164, i64 7
  %258 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn165, align 4
  %call166 = call i8* %258(%class.btSerializer* %254, i8* %256)
  %259 = bitcast i8* %call166 to %struct.btVector3DoubleData*
  %260 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %m_vertices3d167 = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %260, i32 0, i32 1
  store %struct.btVector3DoubleData* %259, %struct.btVector3DoubleData** %m_vertices3d167, align 4, !tbaa !34
  %261 = bitcast i32* %i168 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %261) #9
  store i32 0, i32* %i168, align 4, !tbaa !6
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc188, %if.then157
  %262 = load i32, i32* %i168, align 4, !tbaa !6
  %263 = load i32, i32* %numverts, align 4, !tbaa !6
  %cmp170 = icmp slt i32 %262, %263
  br i1 %cmp170, label %for.body172, label %for.cond.cleanup171

for.cond.cleanup171:                              ; preds = %for.cond169
  %264 = bitcast i32* %i168 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %264) #9
  br label %for.end190

for.body172:                                      ; preds = %for.cond169
  %265 = bitcast double** %graphicsbase173 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %265) #9
  %266 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %267 = load i32, i32* %i168, align 4, !tbaa !6
  %268 = load i32, i32* %stride, align 4, !tbaa !6
  %mul174 = mul nsw i32 %267, %268
  %add.ptr175 = getelementptr inbounds i8, i8* %266, i32 %mul174
  %269 = bitcast i8* %add.ptr175 to double*
  store double* %269, double** %graphicsbase173, align 4, !tbaa !2
  %270 = load double*, double** %graphicsbase173, align 4, !tbaa !2
  %arrayidx176 = getelementptr inbounds double, double* %270, i32 0
  %271 = load double, double* %arrayidx176, align 8, !tbaa !18
  %272 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4, !tbaa !2
  %273 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx177 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %272, i32 %273
  %m_floats178 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx177, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [4 x double], [4 x double]* %m_floats178, i32 0, i32 0
  store double %271, double* %arrayidx179, align 8, !tbaa !18
  %274 = load double*, double** %graphicsbase173, align 4, !tbaa !2
  %arrayidx180 = getelementptr inbounds double, double* %274, i32 1
  %275 = load double, double* %arrayidx180, align 8, !tbaa !18
  %276 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4, !tbaa !2
  %277 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx181 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %276, i32 %277
  %m_floats182 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx181, i32 0, i32 0
  %arrayidx183 = getelementptr inbounds [4 x double], [4 x double]* %m_floats182, i32 0, i32 1
  store double %275, double* %arrayidx183, align 8, !tbaa !18
  %278 = load double*, double** %graphicsbase173, align 4, !tbaa !2
  %arrayidx184 = getelementptr inbounds double, double* %278, i32 2
  %279 = load double, double* %arrayidx184, align 8, !tbaa !18
  %280 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %tmpVertices162, align 4, !tbaa !2
  %281 = load i32, i32* %i168, align 4, !tbaa !6
  %arrayidx185 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %280, i32 %281
  %m_floats186 = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %arrayidx185, i32 0, i32 0
  %arrayidx187 = getelementptr inbounds [4 x double], [4 x double]* %m_floats186, i32 0, i32 2
  store double %279, double* %arrayidx187, align 8, !tbaa !18
  %282 = bitcast double** %graphicsbase173 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %282) #9
  br label %for.inc188

for.inc188:                                       ; preds = %for.body172
  %283 = load i32, i32* %i168, align 4, !tbaa !6
  %inc189 = add nsw i32 %283, 1
  store i32 %inc189, i32* %i168, align 4, !tbaa !6
  br label %for.cond169

for.end190:                                       ; preds = %for.cond.cleanup171
  %284 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %285 = load %class.btChunk*, %class.btChunk** %chunk158, align 4, !tbaa !2
  %286 = load %class.btChunk*, %class.btChunk** %chunk158, align 4, !tbaa !2
  %m_oldPtr191 = getelementptr inbounds %class.btChunk, %class.btChunk* %286, i32 0, i32 2
  %287 = load i8*, i8** %m_oldPtr191, align 4, !tbaa !24
  %288 = bitcast %class.btSerializer* %284 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable192 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %288, align 4, !tbaa !8
  %vfn193 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable192, i64 5
  %289 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn193, align 4
  call void %289(%class.btSerializer* %284, %class.btChunk* %285, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.4, i32 0, i32 0), i32 1497453121, i8* %287)
  %290 = bitcast %struct.btVector3DoubleData** %tmpVertices162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %290) #9
  %291 = bitcast %class.btChunk** %chunk158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %291) #9
  br label %if.end194

if.end194:                                        ; preds = %for.end190, %sw.bb155
  br label %sw.epilog196

sw.default195:                                    ; preds = %sw.epilog
  br label %sw.epilog196

sw.epilog196:                                     ; preds = %sw.default195, %if.end194, %if.end154
  %292 = load i32, i32* %part, align 4, !tbaa !6
  %293 = bitcast %class.btStridingMeshInterface* %this1 to void (%class.btStridingMeshInterface*, i32)***
  %vtable197 = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %293, align 4, !tbaa !8
  %vfn198 = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable197, i64 6
  %294 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn198, align 4
  call void %294(%class.btStridingMeshInterface* %this1, i32 %292)
  br label %for.inc199

for.inc199:                                       ; preds = %sw.epilog196
  %295 = load i32, i32* %part, align 4, !tbaa !6
  %inc200 = add nsw i32 %295, 1
  store i32 %inc200, i32* %part, align 4, !tbaa !6
  %296 = load %struct.btMeshPartData*, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btMeshPartData, %struct.btMeshPartData* %296, i32 1
  store %struct.btMeshPartData* %incdec.ptr, %struct.btMeshPartData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end201:                                       ; preds = %for.cond
  %297 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %298 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %299 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr202 = getelementptr inbounds %class.btChunk, %class.btChunk* %299, i32 0, i32 2
  %300 = load i8*, i8** %m_oldPtr202, align 4, !tbaa !24
  %301 = bitcast %class.btSerializer* %297 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable203 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %301, align 4, !tbaa !8
  %vfn204 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable203, i64 5
  %302 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn204, align 4
  call void %302(%class.btSerializer* %297, %class.btChunk* %298, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0), i32 1497453121, i8* %300)
  %303 = bitcast i32* %gfxindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %303) #9
  %304 = bitcast i32* %numtriangles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #9
  %305 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %305) #9
  %306 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #9
  %307 = bitcast i32* %gfxindextype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %307) #9
  %308 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %308) #9
  %309 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %309) #9
  %310 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %310) #9
  %311 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %311) #9
  %312 = bitcast i32* %graphicssubparts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %312) #9
  %313 = bitcast i32* %part to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %313) #9
  %314 = bitcast %struct.btMeshPartData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %314) #9
  %315 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %315) #9
  br label %if.end205

if.end205:                                        ; preds = %for.end201, %entry
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  %316 = load %struct.btStridingMeshInterfaceData*, %struct.btStridingMeshInterfaceData** %trimeshData, align 4, !tbaa !2
  %m_scaling206 = getelementptr inbounds %struct.btStridingMeshInterfaceData, %struct.btStridingMeshInterfaceData* %316, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_scaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_scaling206)
  %317 = bitcast %struct.btStridingMeshInterfaceData** %trimeshData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %317) #9
  ret i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.6, i32 0, i32 0)
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !14
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK23btStridingMeshInterface14hasPremadeAabbEv(%class.btStridingMeshInterface* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i1 false
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK23btStridingMeshInterface14setPremadeAabbERK9btVector3S2_(%class.btStridingMeshInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK23btStridingMeshInterface14getPremadeAabbEP9btVector3S1_(%class.btStridingMeshInterface* %this, %class.btVector3* %aabbMin, %class.btVector3* %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btInternalTriangleIndexCallback* @_ZN31btInternalTriangleIndexCallbackC2Ev(%class.btInternalTriangleIndexCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btInternalTriangleIndexCallback*, align 4
  store %class.btInternalTriangleIndexCallback* %this, %class.btInternalTriangleIndexCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btInternalTriangleIndexCallback*, %class.btInternalTriangleIndexCallback** %this.addr, align 4
  %0 = bitcast %class.btInternalTriangleIndexCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV31btInternalTriangleIndexCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btInternalTriangleIndexCallback* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallbackD0Ev(%struct.AabbCalculationCallback* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %call = call %struct.AabbCalculationCallback* bitcast (%class.btInternalTriangleIndexCallback* (%class.btInternalTriangleIndexCallback*)* @_ZN31btInternalTriangleIndexCallbackD2Ev to %struct.AabbCalculationCallback* (%struct.AabbCalculationCallback*)*)(%struct.AabbCalculationCallback* %this1) #9
  %0 = bitcast %struct.AabbCalculationCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN23btStridingMeshInterface23calculateAabbBruteForceER9btVector3S1_EN23AabbCalculationCallback28internalProcessTriangleIndexEPS0_ii(%struct.AabbCalculationCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.AabbCalculationCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  store %struct.AabbCalculationCallback* %this, %struct.AabbCalculationCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !6
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !6
  %this1 = load %struct.AabbCalculationCallback*, %struct.AabbCalculationCallback** %this.addr, align 4
  %m_aabbMin = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_aabbMax = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %1 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2)
  %m_aabbMin3 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %m_aabbMax5 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %3 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_aabbMin7 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMin7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %m_aabbMax9 = getelementptr inbounds %struct.AabbCalculationCallback, %struct.AabbCalculationCallback* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMax9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx10)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { cold noreturn nounwind }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { noreturn nounwind }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i64 0, i64 16, !11}
!11 = !{!4, !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"short", !4, i64 0}
!18 = !{!19, !19, i64 0}
!19 = !{!"double", !4, i64 0}
!20 = !{!21, !7, i64 20}
!21 = !{!"_ZTS27btStridingMeshInterfaceData", !3, i64 0, !22, i64 4, !7, i64 20, !4, i64 24}
!22 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!23 = !{!21, !3, i64 0}
!24 = !{!25, !3, i64 8}
!25 = !{!"_ZTS7btChunk", !7, i64 0, !7, i64 4, !3, i64 8, !7, i64 12, !7, i64 16}
!26 = !{!27, !7, i64 24}
!27 = !{!"_ZTS14btMeshPartData", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !7, i64 24, !7, i64 28}
!28 = !{!27, !7, i64 28}
!29 = !{!27, !3, i64 20}
!30 = !{!27, !3, i64 8}
!31 = !{!27, !3, i64 12}
!32 = !{!27, !3, i64 16}
!33 = !{!27, !3, i64 0}
!34 = !{!27, !3, i64 4}
!35 = !{!36, !7, i64 0}
!36 = !{!"_ZTS14btIntIndexData", !7, i64 0}
