; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btCapsuleShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btCapsuleShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCapsuleShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btCapsuleShapeX = type { %class.btCapsuleShape }
%class.btCapsuleShapeZ = type { %class.btCapsuleShape }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%struct.btCapsuleShapeData = type { %struct.btConvexInternalShapeData, i32, [4 x i8] }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK14btCapsuleShape9getRadiusEv = comdat any

$_ZNK14btCapsuleShape13getHalfHeightEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK14btCapsuleShape9getUpAxisEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN14btCapsuleShapeC2Ev = comdat any

$_ZN14btCapsuleShapeD0Ev = comdat any

$_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZN14btCapsuleShape15setLocalScalingERK9btVector3 = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK14btCapsuleShape7getNameEv = comdat any

$_ZNK14btCapsuleShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN14btCapsuleShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK14btCapsuleShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK14btCapsuleShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN15btCapsuleShapeXD0Ev = comdat any

$_ZNK15btCapsuleShapeX7getNameEv = comdat any

$_ZN15btCapsuleShapeZD0Ev = comdat any

$_ZNK15btCapsuleShapeZ7getNameEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN14btCapsuleShapedlEPv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZTV15btCapsuleShapeX = comdat any

$_ZTV15btCapsuleShapeZ = comdat any

$_ZTS15btCapsuleShapeX = comdat any

$_ZTI15btCapsuleShapeX = comdat any

$_ZTS15btCapsuleShapeZ = comdat any

$_ZTI15btCapsuleShapeZ = comdat any

@_ZTV14btCapsuleShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btCapsuleShape to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btCapsuleShape*)* @_ZN14btCapsuleShapeD0Ev to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*)* @_ZN14btCapsuleShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float, %class.btVector3*)* @_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCapsuleShape*)* @_ZNK14btCapsuleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*)* @_ZNK14btCapsuleShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float)* @_ZN14btCapsuleShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCapsuleShape*)* @_ZNK14btCapsuleShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCapsuleShape*, i8*, %class.btSerializer*)* @_ZNK14btCapsuleShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*, %class.btVector3*)* @_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, align 4
@_ZTV15btCapsuleShapeX = linkonce_odr hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCapsuleShapeX to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btCapsuleShapeX*)* @_ZN15btCapsuleShapeXD0Ev to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*)* @_ZN14btCapsuleShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float, %class.btVector3*)* @_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCapsuleShapeX*)* @_ZNK15btCapsuleShapeX7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*)* @_ZNK14btCapsuleShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float)* @_ZN14btCapsuleShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCapsuleShape*)* @_ZNK14btCapsuleShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCapsuleShape*, i8*, %class.btSerializer*)* @_ZNK14btCapsuleShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*, %class.btVector3*)* @_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, comdat, align 4
@_ZTV15btCapsuleShapeZ = linkonce_odr hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCapsuleShapeZ to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btCapsuleShapeZ*)* @_ZN15btCapsuleShapeZD0Ev to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*)* @_ZN14btCapsuleShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float, %class.btVector3*)* @_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCapsuleShapeZ*)* @_ZNK15btCapsuleShapeZ7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*)* @_ZNK14btCapsuleShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCapsuleShape*, float)* @_ZN14btCapsuleShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCapsuleShape*)* @_ZNK14btCapsuleShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCapsuleShape*, i8*, %class.btSerializer*)* @_ZNK14btCapsuleShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCapsuleShape*, %class.btVector3*)* @_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btCapsuleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS14btCapsuleShape = hidden constant [17 x i8] c"14btCapsuleShape\00", align 1
@_ZTI21btConvexInternalShape = external constant i8*
@_ZTI14btCapsuleShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btCapsuleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btConvexInternalShape to i8*) }, align 4
@_ZTS15btCapsuleShapeX = linkonce_odr hidden constant [18 x i8] c"15btCapsuleShapeX\00", comdat, align 1
@_ZTI15btCapsuleShapeX = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btCapsuleShapeX, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btCapsuleShape to i8*) }, comdat, align 4
@_ZTS15btCapsuleShapeZ = linkonce_odr hidden constant [18 x i8] c"15btCapsuleShapeZ\00", comdat, align 1
@_ZTI15btCapsuleShapeZ = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btCapsuleShapeZ, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btCapsuleShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [13 x i8] c"CapsuleShape\00", align 1
@.str.1 = private unnamed_addr constant [19 x i8] c"btCapsuleShapeData\00", align 1
@.str.2 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@.str.3 = private unnamed_addr constant [9 x i8] c"CapsuleX\00", align 1
@.str.4 = private unnamed_addr constant [9 x i8] c"CapsuleZ\00", align 1

@_ZN14btCapsuleShapeC1Eff = hidden unnamed_addr alias %class.btCapsuleShape* (%class.btCapsuleShape*, float, float), %class.btCapsuleShape* (%class.btCapsuleShape*, float, float)* @_ZN14btCapsuleShapeC2Eff
@_ZN15btCapsuleShapeXC1Eff = hidden unnamed_addr alias %class.btCapsuleShapeX* (%class.btCapsuleShapeX*, float, float), %class.btCapsuleShapeX* (%class.btCapsuleShapeX*, float, float)* @_ZN15btCapsuleShapeXC2Eff
@_ZN15btCapsuleShapeZC1Eff = hidden unnamed_addr alias %class.btCapsuleShapeZ* (%class.btCapsuleShapeZ*, float, float), %class.btCapsuleShapeZ* (%class.btCapsuleShapeZ*, float, float)* @_ZN15btCapsuleShapeZC2Eff

define hidden %class.btCapsuleShape* @_ZN14btCapsuleShapeC2Eff(%class.btCapsuleShape* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btCapsuleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV14btCapsuleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 10, i32* %m_shapeType, align 4, !tbaa !10
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  store i32 1, i32* %m_upAxis, align 4, !tbaa !13
  %3 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load float, float* %height.addr, align 4, !tbaa !6
  %mul = fmul float 5.000000e-01, %5
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_implicitShapeDimensions, float* nonnull align 4 dereferenceable(4) %radius.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %radius.addr)
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret %class.btCapsuleShape* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

define hidden void @_ZNK14btCapsuleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btCapsuleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec0) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %vec0.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %rlen = alloca float, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %radius = alloca float, align 4
  %pos = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %pos29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca float, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vec0, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !6
  %7 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = load %class.btVector3*, %class.btVector3** %vec0.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %vec to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !15
  %11 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call4, float* %lenSqr, align 4, !tbaa !6
  %12 = load float, float* %lenSqr, align 4, !tbaa !6
  %cmp = fcmp olt float %12, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %15 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %19 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load float, float* %lenSqr, align 4, !tbaa !6
  %call8 = call float @_Z6btSqrtf(float %20)
  %div = fdiv float 1.000000e+00, %call8
  store float %div, float* %rlen, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  %21 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %22 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  %23 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %radius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %call11 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call11, float* %radius, align 4, !tbaa !6
  %25 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %26 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  store float 0.000000e+00, float* %ref.tmp12, align 4, !tbaa !6
  %27 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  store float 0.000000e+00, float* %ref.tmp13, align 4, !tbaa !6
  %28 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  store float 0.000000e+00, float* %ref.tmp14, align 4, !tbaa !6
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %29 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %call16 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos)
  %call18 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx = getelementptr inbounds float, float* %call17, i32 %call18
  store float %call16, float* %arrayidx, align 4, !tbaa !6
  %32 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %33 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #8
  %34 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %radius)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  %35 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #8
  %36 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #8
  %37 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %38 = bitcast %class.btConvexInternalShape* %37 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %38, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %39 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call24 = call float %39(%class.btConvexInternalShape* %37)
  store float %call24, float* %ref.tmp23, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %40 = bitcast %class.btVector3* %vtx to i8*
  %41 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false), !tbaa.struct !15
  %42 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #8
  %45 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #8
  %call25 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call25, float* %newDot, align 4, !tbaa !6
  %47 = load float, float* %newDot, align 4, !tbaa !6
  %48 = load float, float* %maxDot, align 4, !tbaa !6
  %cmp26 = fcmp ogt float %47, %48
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.end
  %49 = load float, float* %newDot, align 4, !tbaa !6
  store float %49, float* %maxDot, align 4, !tbaa !6
  %50 = bitcast %class.btVector3* %agg.result to i8*
  %51 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false), !tbaa.struct !15
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.end
  %52 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btVector3* %pos29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  %54 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #8
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !6
  %55 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !6
  %56 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #8
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !6
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %57 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  %59 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #8
  %call34 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %fneg = fneg float %call34
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos29)
  %call36 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx37 = getelementptr inbounds float, float* %call35, i32 %call36
  store float %fneg, float* %arrayidx37, align 4, !tbaa !6
  %60 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #8
  %61 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #8
  %62 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %radius)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %pos29, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp40)
  %63 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #8
  %64 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #8
  %65 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %66 = bitcast %class.btConvexInternalShape* %65 to float (%class.btConvexInternalShape*)***
  %vtable43 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %66, align 4, !tbaa !8
  %vfn44 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable43, i64 12
  %67 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn44, align 4
  %call45 = call float %67(%class.btConvexInternalShape* %65)
  store float %call45, float* %ref.tmp42, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp42)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp38, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %68 = bitcast %class.btVector3* %vtx to i8*
  %69 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %68, i8* align 4 %69, i32 16, i1 false), !tbaa.struct !15
  %70 = bitcast float* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #8
  %71 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #8
  %72 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %72) #8
  %73 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %73) #8
  %74 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #8
  %call46 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call46, float* %newDot, align 4, !tbaa !6
  %75 = load float, float* %newDot, align 4, !tbaa !6
  %76 = load float, float* %maxDot, align 4, !tbaa !6
  %cmp47 = fcmp ogt float %75, %76
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %if.end28
  %77 = load float, float* %newDot, align 4, !tbaa !6
  store float %77, float* %maxDot, align 4, !tbaa !6
  %78 = bitcast %class.btVector3* %agg.result to i8*
  %79 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false), !tbaa.struct !15
  br label %if.end49

if.end49:                                         ; preds = %if.then48, %if.end28
  %80 = bitcast %class.btVector3* %pos29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #8
  %81 = bitcast float* %radius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #8
  %82 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #8
  %83 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #8
  %84 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #8
  %85 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  %86 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !6
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !6
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !6
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #2 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !6
  %0 = load float, float* %y.addr, align 4, !tbaa !6
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !6
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !6
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !6
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !6
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %radiusAxis = alloca i32, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast i32* %radiusAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4, !tbaa !13
  %add = add nsw i32 %1, 2
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %radiusAxis, align 4, !tbaa !17
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %3 = load i32, i32* %radiusAxis, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %5 = bitcast i32* %radiusAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret float %4
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  ret float %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4, !tbaa !13
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !6
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !6
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !6
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !6
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !6
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !6
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define hidden void @_ZNK14btCapsuleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btCapsuleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %radius = alloca float, align 4
  %j = alloca i32, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3*, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %pos = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %pos19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !17
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast float* %radius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call, float* %radius, align 4, !tbaa !6
  %1 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %j, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %j, align 4, !tbaa !17
  %3 = load i32, i32* %numVectors.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !6
  %6 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %8 = load i32, i32* %j, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  store %class.btVector3* %arrayidx, %class.btVector3** %vec, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  %10 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %13 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %call6 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos)
  %call8 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx9 = getelementptr inbounds float, float* %call7, i32 %call8
  store float %call6, float* %arrayidx9, align 4, !tbaa !6
  %18 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %21, float* nonnull align 4 dereferenceable(4) %radius)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %22 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %23 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %24 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %26 = bitcast %class.btConvexInternalShape* %25 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %26, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %27 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call15 = call float %27(%class.btConvexInternalShape* %25)
  store float %call15, float* %ref.tmp14, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %23, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %28 = bitcast %class.btVector3* %vtx to i8*
  %29 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !15
  %30 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  %33 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #8
  %34 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #8
  %35 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %call16 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call16, float* %newDot, align 4, !tbaa !6
  %36 = load float, float* %newDot, align 4, !tbaa !6
  %37 = load float, float* %maxDot, align 4, !tbaa !6
  %cmp17 = fcmp ogt float %36, %37
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %38 = load float, float* %newDot, align 4, !tbaa !6
  store float %38, float* %maxDot, align 4, !tbaa !6
  %39 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %40 = load i32, i32* %j, align 4, !tbaa !17
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %39, i32 %40
  %41 = bitcast %class.btVector3* %arrayidx18 to i8*
  %42 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false), !tbaa.struct !15
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %43 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btVector3* %pos19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #8
  %45 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !6
  %46 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !6
  %47 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  store float 0.000000e+00, float* %ref.tmp22, align 4, !tbaa !6
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  %48 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  %49 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %call24 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %fneg = fneg float %call24
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos19)
  %call26 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx27 = getelementptr inbounds float, float* %call25, i32 %call26
  store float %fneg, float* %arrayidx27, align 4, !tbaa !6
  %51 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  %52 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #8
  %54 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %54, float* nonnull align 4 dereferenceable(4) %radius)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %pos19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  %55 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #8
  %56 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %57 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #8
  %58 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %59 = bitcast %class.btConvexInternalShape* %58 to float (%class.btConvexInternalShape*)***
  %vtable33 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %59, align 4, !tbaa !8
  %vfn34 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable33, i64 12
  %60 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn34, align 4
  %call35 = call float %60(%class.btConvexInternalShape* %58)
  store float %call35, float* %ref.tmp32, align 4, !tbaa !6
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %56, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %61 = bitcast %class.btVector3* %vtx to i8*
  %62 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 16, i1 false), !tbaa.struct !15
  %63 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #8
  %64 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #8
  %66 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #8
  %67 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #8
  %68 = load %class.btVector3*, %class.btVector3** %vec, align 4, !tbaa !2
  %call36 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %68, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call36, float* %newDot, align 4, !tbaa !6
  %69 = load float, float* %newDot, align 4, !tbaa !6
  %70 = load float, float* %maxDot, align 4, !tbaa !6
  %cmp37 = fcmp ogt float %69, %70
  br i1 %cmp37, label %if.then38, label %if.end40

if.then38:                                        ; preds = %if.end
  %71 = load float, float* %newDot, align 4, !tbaa !6
  store float %71, float* %maxDot, align 4, !tbaa !6
  %72 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %73 = load i32, i32* %j, align 4, !tbaa !17
  %arrayidx39 = getelementptr inbounds %class.btVector3, %class.btVector3* %72, i32 %73
  %74 = bitcast %class.btVector3* %arrayidx39 to i8*
  %75 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false), !tbaa.struct !15
  br label %if.end40

if.end40:                                         ; preds = %if.then38, %if.end
  %76 = bitcast %class.btVector3* %pos19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #8
  %77 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #8
  %79 = bitcast %class.btVector3** %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  %80 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end40
  %81 = load i32, i32* %j, align 4, !tbaa !17
  %inc = add nsw i32 %81, 1
  store i32 %inc, i32* %j, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %82 = bitcast float* %radius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #8
  ret void
}

define hidden void @_ZNK14btCapsuleShape21calculateLocalInertiaEfR9btVector3(%class.btCapsuleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ident = alloca %class.btTransform, align 4
  %radius = alloca float, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %margin = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %scaledmass = alloca float, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !6
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #8
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %1 = bitcast float* %radius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %call2 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call2, float* %radius, align 4, !tbaa !6
  %2 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %halfExtents, float* nonnull align 4 dereferenceable(4) %radius, float* nonnull align 4 dereferenceable(4) %radius, float* nonnull align 4 dereferenceable(4) %radius)
  %call4 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %call6 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx = getelementptr inbounds float, float* %call5, i32 %call6
  %3 = load float, float* %arrayidx, align 4, !tbaa !6
  %add = fadd float %3, %call4
  store float %add, float* %arrayidx, align 4, !tbaa !6
  %4 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0x3FA47AE140000000, float* %margin, align 4, !tbaa !6
  %5 = bitcast float* %lx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %6 = load float, float* %arrayidx8, align 4, !tbaa !6
  %7 = load float, float* %margin, align 4, !tbaa !6
  %add9 = fadd float %6, %7
  %mul = fmul float 2.000000e+00, %add9
  store float %mul, float* %lx, align 4, !tbaa !6
  %8 = bitcast float* %ly to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %9 = load float, float* %arrayidx11, align 4, !tbaa !6
  %10 = load float, float* %margin, align 4, !tbaa !6
  %add12 = fadd float %9, %10
  %mul13 = fmul float 2.000000e+00, %add12
  store float %mul13, float* %ly, align 4, !tbaa !6
  %11 = bitcast float* %lz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %12 = load float, float* %arrayidx15, align 4, !tbaa !6
  %13 = load float, float* %margin, align 4, !tbaa !6
  %add16 = fadd float %12, %13
  %mul17 = fmul float 2.000000e+00, %add16
  store float %mul17, float* %lz, align 4, !tbaa !6
  %14 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  %15 = load float, float* %lx, align 4, !tbaa !6
  %16 = load float, float* %lx, align 4, !tbaa !6
  %mul18 = fmul float %15, %16
  store float %mul18, float* %x2, align 4, !tbaa !6
  %17 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = load float, float* %ly, align 4, !tbaa !6
  %19 = load float, float* %ly, align 4, !tbaa !6
  %mul19 = fmul float %18, %19
  store float %mul19, float* %y2, align 4, !tbaa !6
  %20 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load float, float* %lz, align 4, !tbaa !6
  %22 = load float, float* %lz, align 4, !tbaa !6
  %mul20 = fmul float %21, %22
  store float %mul20, float* %z2, align 4, !tbaa !6
  %23 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #8
  %24 = load float, float* %mass.addr, align 4, !tbaa !6
  %mul21 = fmul float %24, 0x3FB5555540000000
  store float %mul21, float* %scaledmass, align 4, !tbaa !6
  %25 = load float, float* %scaledmass, align 4, !tbaa !6
  %26 = load float, float* %y2, align 4, !tbaa !6
  %27 = load float, float* %z2, align 4, !tbaa !6
  %add22 = fadd float %26, %27
  %mul23 = fmul float %25, %add22
  %28 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %28)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 0
  store float %mul23, float* %arrayidx25, align 4, !tbaa !6
  %29 = load float, float* %scaledmass, align 4, !tbaa !6
  %30 = load float, float* %x2, align 4, !tbaa !6
  %31 = load float, float* %z2, align 4, !tbaa !6
  %add26 = fadd float %30, %31
  %mul27 = fmul float %29, %add26
  %32 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  store float %mul27, float* %arrayidx29, align 4, !tbaa !6
  %33 = load float, float* %scaledmass, align 4, !tbaa !6
  %34 = load float, float* %x2, align 4, !tbaa !6
  %35 = load float, float* %y2, align 4, !tbaa !6
  %add30 = fadd float %34, %35
  %mul31 = fmul float %33, %add30
  %36 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %36)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 2
  store float %mul31, float* %arrayidx33, align 4, !tbaa !6
  %37 = bitcast float* %scaledmass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast float* %z2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast float* %y2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast float* %x2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast float* %lz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast float* %ly to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  %43 = bitcast float* %lx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #8
  %44 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #8
  %45 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = bitcast float* %radius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #8
  %47 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %47) #8
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

define hidden %class.btCapsuleShapeX* @_ZN15btCapsuleShapeXC2Eff(%class.btCapsuleShapeX* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShapeX*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  store %class.btCapsuleShapeX* %this, %class.btCapsuleShapeX** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btCapsuleShapeX*, %class.btCapsuleShapeX** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShapeX* %this1 to %class.btCapsuleShape*
  %call = call %class.btCapsuleShape* @_ZN14btCapsuleShapeC2Ev(%class.btCapsuleShape* %0)
  %1 = bitcast %class.btCapsuleShapeX* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV15btCapsuleShapeX, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %2 = bitcast %class.btCapsuleShapeX* %this1 to %class.btCapsuleShape*
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %2, i32 0, i32 1
  store i32 0, i32* %m_upAxis, align 4, !tbaa !13
  %3 = bitcast %class.btCapsuleShapeX* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load float, float* %height.addr, align 4, !tbaa !6
  %mul = fmul float 5.000000e-01, %5
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_implicitShapeDimensions, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %radius.addr, float* nonnull align 4 dereferenceable(4) %radius.addr)
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret %class.btCapsuleShapeX* %this1
}

define linkonce_odr hidden %class.btCapsuleShape* @_ZN14btCapsuleShapeC2Ev(%class.btCapsuleShape* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btCapsuleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV14btCapsuleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 10, i32* %m_shapeType, align 4, !tbaa !10
  ret %class.btCapsuleShape* %this1
}

define hidden %class.btCapsuleShapeZ* @_ZN15btCapsuleShapeZC2Eff(%class.btCapsuleShapeZ* returned %this, float %radius, float %height) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCapsuleShapeZ*, align 4
  %radius.addr = alloca float, align 4
  %height.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  store %class.btCapsuleShapeZ* %this, %class.btCapsuleShapeZ** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !6
  store float %height, float* %height.addr, align 4, !tbaa !6
  %this1 = load %class.btCapsuleShapeZ*, %class.btCapsuleShapeZ** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShapeZ* %this1 to %class.btCapsuleShape*
  %call = call %class.btCapsuleShape* @_ZN14btCapsuleShapeC2Ev(%class.btCapsuleShape* %0)
  %1 = bitcast %class.btCapsuleShapeZ* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV15btCapsuleShapeZ, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %2 = bitcast %class.btCapsuleShapeZ* %this1 to %class.btCapsuleShape*
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %2, i32 0, i32 1
  store i32 2, i32* %m_upAxis, align 4, !tbaa !13
  %3 = bitcast %class.btCapsuleShapeZ* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load float, float* %height.addr, align 4, !tbaa !6
  %mul = fmul float 5.000000e-01, %5
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_implicitShapeDimensions, float* nonnull align 4 dereferenceable(4) %radius.addr, float* nonnull align 4 dereferenceable(4) %radius.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret %class.btCapsuleShapeZ* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN14btCapsuleShapeD0Ev(%class.btCapsuleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %call = call %class.btCapsuleShape* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btCapsuleShape* (%class.btCapsuleShape*)*)(%class.btCapsuleShape* %this1) #8
  %0 = bitcast %class.btCapsuleShape* %this1 to i8*
  call void @_ZN14btCapsuleShapedlEPv(i8* %0) #8
  ret void
}

define linkonce_odr hidden void @_ZNK14btCapsuleShape7getAabbERK11btTransformR9btVector3S4_(%class.btCapsuleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %call = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %call3 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %call5 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %halfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %call7 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this1)
  %call8 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this1)
  %add = fadd float %call7, %call8
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_upAxis, align 4, !tbaa !13
  %arrayidx = getelementptr inbounds float, float* %call9, i32 %7
  store float %add, float* %arrayidx, align 4, !tbaa !6
  %8 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %11 = bitcast %class.btConvexInternalShape* %10 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %11, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %12 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call12 = call float %12(%class.btConvexInternalShape* %10)
  store float %call12, float* %ref.tmp11, align 4, !tbaa !6
  %13 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %15 = bitcast %class.btConvexInternalShape* %14 to float (%class.btConvexInternalShape*)***
  %vtable14 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %15, align 4, !tbaa !8
  %vfn15 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable14, i64 12
  %16 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn15, align 4
  %call16 = call float %16(%class.btConvexInternalShape* %14)
  store float %call16, float* %ref.tmp13, align 4, !tbaa !6
  %17 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %19 = bitcast %class.btConvexInternalShape* %18 to float (%class.btConvexInternalShape*)***
  %vtable18 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %19, align 4, !tbaa !8
  %vfn19 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable18, i64 12
  %20 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn19, align 4
  %call20 = call float %20(%class.btConvexInternalShape* %18)
  store float %call20, float* %ref.tmp17, align 4, !tbaa !6
  %call21 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %21 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %25) #8
  %26 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %26)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call23)
  %27 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %28 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %28)
  %29 = bitcast %class.btVector3* %center to i8*
  %30 = bitcast %class.btVector3* %call24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !15
  %31 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #8
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call25, %class.btVector3* nonnull align 4 dereferenceable(16) %call26, %class.btVector3* nonnull align 4 dereferenceable(16) %call27)
  %32 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %33 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %34 = bitcast %class.btVector3* %33 to i8*
  %35 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false), !tbaa.struct !15
  %36 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  %37 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %38 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %39 = bitcast %class.btVector3* %38 to i8*
  %40 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false), !tbaa.struct !15
  %41 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #8
  %43 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %44) #8
  %45 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

define linkonce_odr hidden void @_ZN14btCapsuleShape15setLocalScalingERK9btVector3(%class.btCapsuleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %oldMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %implicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %unScaledImplicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %oldMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %3 = bitcast %class.btConvexInternalShape* %2 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %4 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %4(%class.btConvexInternalShape* %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %7 = bitcast %class.btConvexInternalShape* %6 to float (%class.btConvexInternalShape*)***
  %vtable3 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %7, align 4, !tbaa !8
  %vfn4 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable3, i64 12
  %8 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn4, align 4
  %call5 = call float %8(%class.btConvexInternalShape* %6)
  store float %call5, float* %ref.tmp2, align 4, !tbaa !6
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %11 = bitcast %class.btConvexInternalShape* %10 to float (%class.btConvexInternalShape*)***
  %vtable7 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %11, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable7, i64 12
  %12 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn8, align 4
  %call9 = call float %12(%class.btConvexInternalShape* %10)
  store float %call9, float* %ref.tmp6, align 4, !tbaa !6
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %oldMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btVector3* %implicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %17, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %18 = bitcast %class.btVector3* %unScaledImplicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #8
  %19 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %19, i32 0, i32 1
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %unScaledImplicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %20 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %21 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4, !tbaa !2
  call void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21)
  %22 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %23 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %24 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling13 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %24, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %unScaledImplicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling13)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %25 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions14 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %25, i32 0, i32 2
  %26 = bitcast %class.btVector3* %m_implicitShapeDimensions14 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !15
  %28 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btVector3* %unScaledImplicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast %class.btVector3* %implicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %oldMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK14btCapsuleShape7getNameEv(%class.btCapsuleShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK14btCapsuleShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCapsuleShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %call5 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this1)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 %call5
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !6
  ret void
}

define linkonce_odr hidden void @_ZN14btCapsuleShape9setMarginEf(%class.btCapsuleShape* %this, float %collisionMargin) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  %oldMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %implicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %newMargin = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store float %collisionMargin, float* %collisionMargin.addr, align 4, !tbaa !6
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %oldMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %3 = bitcast %class.btConvexInternalShape* %2 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %4 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %4(%class.btConvexInternalShape* %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %7 = bitcast %class.btConvexInternalShape* %6 to float (%class.btConvexInternalShape*)***
  %vtable3 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %7, align 4, !tbaa !8
  %vfn4 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable3, i64 12
  %8 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn4, align 4
  %call5 = call float %8(%class.btConvexInternalShape* %6)
  store float %call5, float* %ref.tmp2, align 4, !tbaa !6
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %11 = bitcast %class.btConvexInternalShape* %10 to float (%class.btConvexInternalShape*)***
  %vtable7 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %11, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable7, i64 12
  %12 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn8, align 4
  %call9 = call float %12(%class.btConvexInternalShape* %10)
  store float %call9, float* %ref.tmp6, align 4, !tbaa !6
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %oldMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btVector3* %implicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #8
  %17 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %17, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %18 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %19 = load float, float* %collisionMargin.addr, align 4, !tbaa !6
  call void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %18, float %19)
  %20 = bitcast %class.btVector3* %newMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %23 = bitcast %class.btConvexInternalShape* %22 to float (%class.btConvexInternalShape*)***
  %vtable12 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %23, align 4, !tbaa !8
  %vfn13 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable12, i64 12
  %24 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn13, align 4
  %call14 = call float %24(%class.btConvexInternalShape* %22)
  store float %call14, float* %ref.tmp11, align 4, !tbaa !6
  %25 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %27 = bitcast %class.btConvexInternalShape* %26 to float (%class.btConvexInternalShape*)***
  %vtable16 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %27, align 4, !tbaa !8
  %vfn17 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable16, i64 12
  %28 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn17, align 4
  %call18 = call float %28(%class.btConvexInternalShape* %26)
  store float %call18, float* %ref.tmp15, align 4, !tbaa !6
  %29 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #8
  %30 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %31 = bitcast %class.btConvexInternalShape* %30 to float (%class.btConvexInternalShape*)***
  %vtable20 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %31, align 4, !tbaa !8
  %vfn21 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable20, i64 12
  %32 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn21, align 4
  %call22 = call float %32(%class.btConvexInternalShape* %30)
  store float %call22, float* %ref.tmp19, align 4, !tbaa !6
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %newMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %33 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  %34 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %newMargin)
  %37 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions25 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %37, i32 0, i32 2
  %38 = bitcast %class.btVector3* %m_implicitShapeDimensions25 to i8*
  %39 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false), !tbaa.struct !15
  %40 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #8
  %41 = bitcast %class.btVector3* %newMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %class.btVector3* %implicitShapeDimensionsWithMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #8
  %43 = bitcast %class.btVector3* %oldMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !18
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK14btCapsuleShape28calculateSerializeBufferSizeEv(%class.btCapsuleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  ret i32 60
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK14btCapsuleShape9serializeEPvP12btSerializer(%class.btCapsuleShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btCapsuleShapeData*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %struct.btCapsuleShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btCapsuleShapeData*
  store %struct.btCapsuleShapeData* %2, %struct.btCapsuleShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %4 = load %struct.btCapsuleShapeData*, %struct.btCapsuleShapeData** %shapeData, align 4, !tbaa !2
  %m_convexInternalShapeData = getelementptr inbounds %struct.btCapsuleShapeData, %struct.btCapsuleShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btConvexInternalShapeData* %m_convexInternalShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %3, i8* %5, %class.btSerializer* %6)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_upAxis, align 4, !tbaa !13
  %8 = load %struct.btCapsuleShapeData*, %struct.btCapsuleShapeData** %shapeData, align 4, !tbaa !2
  %m_upAxis2 = getelementptr inbounds %struct.btCapsuleShapeData, %struct.btCapsuleShapeData* %8, i32 0, i32 1
  store i32 %7, i32* %m_upAxis2, align 4, !tbaa !21
  %9 = bitcast %struct.btCapsuleShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  ret i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !17
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btCapsuleShapeXD0Ev(%class.btCapsuleShapeX* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShapeX*, align 4
  store %class.btCapsuleShapeX* %this, %class.btCapsuleShapeX** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShapeX*, %class.btCapsuleShapeX** %this.addr, align 4
  %call = call %class.btCapsuleShapeX* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btCapsuleShapeX* (%class.btCapsuleShapeX*)*)(%class.btCapsuleShapeX* %this1) #8
  %0 = bitcast %class.btCapsuleShapeX* %this1 to i8*
  call void @_ZN14btCapsuleShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btCapsuleShapeX7getNameEv(%class.btCapsuleShapeX* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShapeX*, align 4
  store %class.btCapsuleShapeX* %this, %class.btCapsuleShapeX** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShapeX*, %class.btCapsuleShapeX** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btCapsuleShapeZD0Ev(%class.btCapsuleShapeZ* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShapeZ*, align 4
  store %class.btCapsuleShapeZ* %this, %class.btCapsuleShapeZ** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShapeZ*, %class.btCapsuleShapeZ** %this.addr, align 4
  %call = call %class.btCapsuleShapeZ* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btCapsuleShapeZ* (%class.btCapsuleShapeZ*)*)(%class.btCapsuleShapeZ* %this1) #8
  %0 = bitcast %class.btCapsuleShapeZ* %this1 to i8*
  call void @_ZN14btCapsuleShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btCapsuleShapeZ7getNameEv(%class.btCapsuleShapeZ* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShapeZ*, align 4
  store %class.btCapsuleShapeZ* %this, %class.btCapsuleShapeZ** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShapeZ*, %class.btCapsuleShapeZ** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i32 0, i32 0)
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !6
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !6
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !6
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !6
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN14btCapsuleShapedlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !6
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !6
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !6
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !6
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !6
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !6
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !6
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !6
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !6
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !6
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !6
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !6
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !6
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !6
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !6
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !6
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !6
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !6
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !6
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !6
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !6
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !6
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !6
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !6
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !6
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !6
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !6
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !6
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !17
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #2 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !6
  %0 = load float, float* %x.addr, align 4, !tbaa !6
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %div = fdiv float %2, %4
  store float %div, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %div8 = fdiv float %7, %9
  store float %div8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %div14 = fdiv float %12, %14
  store float %div14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !6
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !6
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !6
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !6
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !6
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !6
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !6
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !6
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !6
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !6
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !6
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !18
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !18
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !26
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !17
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !6
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"float", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !12, i64 4}
!11 = !{!"_ZTS16btCollisionShape", !12, i64 4, !3, i64 8}
!12 = !{!"int", !4, i64 0}
!13 = !{!14, !12, i64 52}
!14 = !{!"_ZTS14btCapsuleShape", !12, i64 52}
!15 = !{i64 0, i64 16, !16}
!16 = !{!4, !4, i64 0}
!17 = !{!12, !12, i64 0}
!18 = !{!19, !7, i64 44}
!19 = !{!"_ZTS21btConvexInternalShape", !20, i64 12, !20, i64 28, !7, i64 44, !7, i64 48}
!20 = !{!"_ZTS9btVector3", !4, i64 0}
!21 = !{!22, !12, i64 52}
!22 = !{!"_ZTS18btCapsuleShapeData", !23, i64 0, !12, i64 52, !4, i64 56}
!23 = !{!"_ZTS25btConvexInternalShapeData", !24, i64 0, !25, i64 12, !25, i64 28, !7, i64 44, !12, i64 48}
!24 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !12, i64 4, !4, i64 8}
!25 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!26 = !{!23, !7, i64 44}
