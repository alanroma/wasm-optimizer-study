; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyConcaveCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftBodyConcaveCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSoftBodyConcaveCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btSoftBodyTriangleCallback }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btSoftBodyTriangleCallback = type { %class.btTriangleCallback, %class.btSoftBody*, %class.btCollisionObject*, %class.btVector3, %class.btVector3, %class.btManifoldResult*, %class.btDispatcher*, %struct.btDispatcherInfo*, float, %class.btHashMap, i32 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.65, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.81, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.85 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBodySolver = type opaque
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btSparseSdf = type { %class.btAlignedObjectArray.16, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type { [4 x [4 x [4 x float]]], [3 x i32], i32, i32, %class.btCollisionShape*, %"struct.btSparseSdf<3>::Cell"* }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.23 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.23 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.44, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.57 = type <{ %class.btAlignedAllocator.58, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.58 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.8, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%class.btAlignedObjectArray.65 = type <{ %class.btAlignedAllocator.66, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.66 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.69, %class.btAlignedObjectArray.73 }
%class.btAlignedObjectArray.69 = type <{ %class.btAlignedAllocator.70, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.70 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.73 = type <{ %class.btAlignedAllocator.74, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.74 = type { i8 }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btAlignedObjectArray.85 = type <{ %class.btAlignedAllocator.86, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.86 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btHashMap = type { %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.93 }
%class.btAlignedObjectArray.89 = type <{ %class.btAlignedAllocator.90, [3 x i8], i32, i32, %struct.btTriIndex*, i8, [3 x i8] }>
%class.btAlignedAllocator.90 = type { i8 }
%struct.btTriIndex = type { i32, %class.btCollisionShape* }
%class.btAlignedObjectArray.93 = type <{ %class.btAlignedAllocator.94, [3 x i8], i32, i32, %class.btHashKey*, i8, [3 x i8] }>
%class.btAlignedAllocator.94 = type { i8 }
%class.btHashKey = type { i32 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btConvexHullShape = type { %class.btPolyhedralConvexAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray.8 }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%class.btConcaveShape = type { %class.btCollisionShape, float }
%struct.LocalTriangleSphereCastCallback = type { %class.btTriangleCallback, %class.btTransform, %class.btTransform, %class.btTransform, float, float }
%class.btAlignedObjectArray.97 = type opaque
%"struct.btConvexCast::CastResult" = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, float, %class.btIDebugDraw*, float }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btSubsimplexConvexCast = type { %class.btConvexCast, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape* }
%class.btConvexCast = type { i32 (...)** }
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev = comdat any

$_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E4sizeEv = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10getAtIndexEi = comdat any

$_ZN10btSoftBody12getWorldInfoEv = comdat any

$_ZN11btSparseSdfILi3EE16RemoveReferencesEP16btCollisionShape = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv = comdat any

$_ZN36btCollisionAlgorithmConstructionInfoC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN10btTriIndexC2EiiP16btCollisionShape = comdat any

$_ZNK10btTriIndex6getUidEv = comdat any

$_ZN9btHashKeyI10btTriIndexEC2Ei = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_ = comdat any

$_ZN16btCollisionShape14setUserPointerEPv = comdat any

$_ZNK17btCollisionObject17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape14getUserPointerEv = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN17btConvexHullShapenwEm = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_ = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape9isConcaveEv = comdat any

$_ZNK26btSoftBodyTriangleCallback10getAabbMinEv = comdat any

$_ZNK26btSoftBodyTriangleCallback10getAabbMaxEv = comdat any

$_ZN17btCollisionObject30getInterpolationWorldTransformEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK17btCollisionObject14getHitFractionEv = comdat any

$_ZN17btCollisionObject14setHitFractionEf = comdat any

$_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorI10btTriIndexLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE4initEv = comdat any

$_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEED2Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI10btTriIndexLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_Z6btFabsf = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN12btConvexCast10CastResultC2Ev = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN15btTriangleShapeC2ERK9btVector3S2_S2_ = comdat any

$_ZN22btVoronoiSimplexSolverC2Ev = comdat any

$_ZN12btConvexCast10CastResultD2Ev = comdat any

$_ZN12btConvexCast10CastResult9DebugDrawEf = comdat any

$_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform = comdat any

$_ZN12btConvexCast10CastResult13reportFailureEii = comdat any

$_ZN12btConvexCast10CastResultD0Ev = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN15btTriangleShapeD0Ev = comdat any

$_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK15btTriangleShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i = comdat any

$_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btTriangleShape14getNumVerticesEv = comdat any

$_ZNK15btTriangleShape11getNumEdgesEv = comdat any

$_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ = comdat any

$_ZNK15btTriangleShape9getVertexEiR9btVector3 = comdat any

$_ZNK15btTriangleShape12getNumPlanesEv = comdat any

$_ZNK15btTriangleShape8getPlaneER9btVector3S1_i = comdat any

$_ZNK15btTriangleShape8isInsideERK9btVector3f = comdat any

$_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ = comdat any

$_ZN15btTriangleShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK15btTriangleShape10calcNormalER9btVector3 = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E4findERKS2_ = comdat any

$_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_ = comdat any

$_ZNK9btHashKeyI10btTriIndexE7getHashEv = comdat any

$_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK9btHashKeyI10btTriIndexE6equalsERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi = comdat any

$_ZNK9btHashKeyI10btTriIndexE7getUid1Ev = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9push_backERKS2_ = comdat any

$_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI10btTriIndexE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI10btTriIndexE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI10btTriIndexLj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4copyEiiPS2_ = comdat any

$_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE8allocateEiPPKS2_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

$_ZTVN12btConvexCast10CastResultE = comdat any

$_ZTSN12btConvexCast10CastResultE = comdat any

$_ZTIN12btConvexCast10CastResultE = comdat any

$_ZTV15btTriangleShape = comdat any

$_ZTS15btTriangleShape = comdat any

$_ZTI15btTriangleShape = comdat any

@_ZTV35btSoftBodyConcaveCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI35btSoftBodyConcaveCollisionAlgorithm to i8*), i8* bitcast (%class.btSoftBodyConcaveCollisionAlgorithm* (%class.btSoftBodyConcaveCollisionAlgorithm*)* @_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btSoftBodyConcaveCollisionAlgorithm*)* @_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btSoftBodyConcaveCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btSoftBodyConcaveCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btSoftBodyConcaveCollisionAlgorithm*, %class.btAlignedObjectArray.97*)* @_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTV26btSoftBodyTriangleCallback = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI26btSoftBodyTriangleCallback to i8*), i8* bitcast (%class.btSoftBodyTriangleCallback* (%class.btSoftBodyTriangleCallback*)* @_ZN26btSoftBodyTriangleCallbackD1Ev to i8*), i8* bitcast (void (%class.btSoftBodyTriangleCallback*)* @_ZN26btSoftBodyTriangleCallbackD0Ev to i8*), i8* bitcast (void (%class.btSoftBodyTriangleCallback*, %class.btVector3*, i32, i32)* @_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS35btSoftBodyConcaveCollisionAlgorithm = hidden constant [38 x i8] c"35btSoftBodyConcaveCollisionAlgorithm\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4
@_ZTI35btSoftBodyConcaveCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([38 x i8], [38 x i8]* @_ZTS35btSoftBodyConcaveCollisionAlgorithm, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*) }, align 4
@_ZTS26btSoftBodyTriangleCallback = hidden constant [29 x i8] c"26btSoftBodyTriangleCallback\00", align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI26btSoftBodyTriangleCallback = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTS26btSoftBodyTriangleCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback = internal unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback to i8*), i8* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to i8*), i8* bitcast (void (%struct.LocalTriangleSphereCastCallback*)* @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev to i8*), i8* bitcast (void (%struct.LocalTriangleSphereCastCallback*, %class.btVector3*, i32, i32)* @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii to i8*)] }, align 4
@_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback = internal constant [160 x i8] c"ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback\00", align 1
@_ZTIZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([160 x i8], [160 x i8]* @_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, align 4
@_ZTVN12btConvexCast10CastResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN12btConvexCast10CastResultE to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, float)* @_ZN12btConvexCast10CastResult9DebugDrawEf to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, %class.btTransform*)* @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, i32, i32)* @_ZN12btConvexCast10CastResult13reportFailureEii to i8*), i8* bitcast (%"struct.btConvexCast::CastResult"* (%"struct.btConvexCast::CastResult"*)* @_ZN12btConvexCast10CastResultD2Ev to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*)* @_ZN12btConvexCast10CastResultD0Ev to i8*)] }, comdat, align 4
@_ZTSN12btConvexCast10CastResultE = linkonce_odr hidden constant [29 x i8] c"N12btConvexCast10CastResultE\00", comdat, align 1
@_ZTIN12btConvexCast10CastResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTSN12btConvexCast10CastResultE, i32 0, i32 0) }, comdat, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4
@_ZTV15btTriangleShape = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShape*)* @_ZN15btTriangleShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@_ZTS15btTriangleShape = linkonce_odr hidden constant [18 x i8] c"15btTriangleShape\00", comdat, align 1
@_ZTI23btPolyhedralConvexShape = external constant i8*
@_ZTI15btTriangleShape = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btTriangleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btPolyhedralConvexShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [9 x i8] c"Triangle\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1

@_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b = hidden unnamed_addr alias %class.btSoftBodyConcaveCollisionAlgorithm* (%class.btSoftBodyConcaveCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btSoftBodyConcaveCollisionAlgorithm* (%class.btSoftBodyConcaveCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b
@_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btSoftBodyConcaveCollisionAlgorithm* (%class.btSoftBodyConcaveCollisionAlgorithm*), %class.btSoftBodyConcaveCollisionAlgorithm* (%class.btSoftBodyConcaveCollisionAlgorithm*)* @_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev
@_ZN26btSoftBodyTriangleCallbackC1EP12btDispatcherPK24btCollisionObjectWrapperS4_b = hidden unnamed_addr alias %class.btSoftBodyTriangleCallback* (%class.btSoftBodyTriangleCallback*, %class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btSoftBodyTriangleCallback* (%class.btSoftBodyTriangleCallback*, %class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherPK24btCollisionObjectWrapperS4_b
@_ZN26btSoftBodyTriangleCallbackD1Ev = hidden unnamed_addr alias %class.btSoftBodyTriangleCallback* (%class.btSoftBodyTriangleCallback*), %class.btSoftBodyTriangleCallback* (%class.btSoftBodyTriangleCallback*)* @_ZN26btSoftBodyTriangleCallbackD2Ev

define hidden %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btSoftBodyConcaveCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1, !tbaa !6
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1)
  %2 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV35btSoftBodyConcaveCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4, !tbaa !8
  %m_isSwapped = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 1
  %3 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %3 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_isSwapped, align 4, !tbaa !11
  %m_btSoftBodyTriangleCallback = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %4 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %4, i32 0, i32 0
  %5 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !24
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %8 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool3 = trunc i8 %8 to i1
  %call4 = call %class.btSoftBodyTriangleCallback* @_ZN26btSoftBodyTriangleCallbackC1EP12btDispatcherPK24btCollisionObjectWrapperS4_b(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback, %class.btDispatcher* %5, %struct.btCollisionObjectWrapper* %6, %struct.btCollisionObjectWrapper* %7, i1 zeroext %tobool3)
  ret %class.btSoftBodyConcaveCollisionAlgorithm* %this1
}

declare %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #1

; Function Attrs: nounwind
define hidden %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmD2Ev(%class.btSoftBodyConcaveCollisionAlgorithm* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV35btSoftBodyConcaveCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_btSoftBodyTriangleCallback = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %call = call %class.btSoftBodyTriangleCallback* @_ZN26btSoftBodyTriangleCallbackD1Ev(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback) #9
  %1 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %call2 = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* %1) #9
  ret %class.btSoftBodyConcaveCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithmD0Ev(%class.btSoftBodyConcaveCollisionAlgorithm* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btSoftBodyConcaveCollisionAlgorithm* @_ZN35btSoftBodyConcaveCollisionAlgorithmD1Ev(%class.btSoftBodyConcaveCollisionAlgorithm* %this1) #9
  %0 = bitcast %class.btSoftBodyConcaveCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

define hidden %class.btSoftBodyTriangleCallback* @_ZN26btSoftBodyTriangleCallbackC2EP12btDispatcherPK24btCollisionObjectWrapperS4_b(%class.btSoftBodyTriangleCallback* returned %this, %class.btDispatcher* %dispatcher, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSoftBodyTriangleCallback*, align 4
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1, !tbaa !6
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  store %class.btSoftBodyTriangleCallback* %this1, %class.btSoftBodyTriangleCallback** %retval, align 4
  %0 = bitcast %class.btSoftBodyTriangleCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #9
  %1 = bitcast %class.btSoftBodyTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV26btSoftBodyTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_aabbMin = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  %m_dispatcher = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 6
  %2 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %2, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !26
  %m_dispatchInfoPtr = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  store %struct.btDispatcherInfo* null, %struct.btDispatcherInfo** %m_dispatchInfoPtr, align 4, !tbaa !27
  %m_shapeCache = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  %call4 = call %class.btHashMap* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EC2Ev(%class.btHashMap* %m_shapeCache)
  %3 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call5 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %4)
  %5 = bitcast %class.btCollisionObject* %call5 to %class.btSoftBody*
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call6 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %7 = bitcast %class.btCollisionObject* %call6 to %class.btSoftBody*
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btSoftBody* [ %5, %cond.true ], [ %7, %cond.false ]
  %m_softBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  store %class.btSoftBody* %cond, %class.btSoftBody** %m_softBody, align 4, !tbaa !28
  %8 = load i8, i8* %isSwapped.addr, align 1, !tbaa !6, !range !10
  %tobool7 = trunc i8 %8 to i1
  br i1 %tobool7, label %cond.true8, label %cond.false10

cond.true8:                                       ; preds = %cond.end
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %call9 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  br label %cond.end12

cond.false10:                                     ; preds = %cond.end
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %10)
  br label %cond.end12

cond.end12:                                       ; preds = %cond.false10, %cond.true8
  %cond13 = phi %class.btCollisionObject* [ %call9, %cond.true8 ], [ %call11, %cond.false10 ]
  %m_triBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  store %class.btCollisionObject* %cond13, %class.btCollisionObject** %m_triBody, align 4, !tbaa !29
  call void @_ZN26btSoftBodyTriangleCallback10clearCacheEv(%class.btSoftBodyTriangleCallback* %this1)
  %11 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %retval, align 4
  ret %class.btSoftBodyTriangleCallback* %11
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EC2Ev(%class.btHashMap* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* %m_next)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayI10btTriIndexEC2Ev(%class.btAlignedObjectArray.89* %m_valueArray)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEC2Ev(%class.btAlignedObjectArray.93* %m_keyArray)
  ret %class.btHashMap* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !30
  ret %class.btCollisionObject* %0
}

define hidden void @_ZN26btSoftBodyTriangleCallback10clearCacheEv(%class.btSoftBodyTriangleCallback* %this) #0 {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  %i = alloca i32, align 4
  %tmp = alloca %struct.btTriIndex*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !32
  %m_shapeCache = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  %call = call i32 @_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E4sizeEv(%class.btHashMap* %m_shapeCache)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %struct.btTriIndex** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_shapeCache2 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  %4 = load i32, i32* %i, align 4, !tbaa !32
  %call3 = call %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10getAtIndexEi(%class.btHashMap* %m_shapeCache2, i32 %4)
  store %struct.btTriIndex* %call3, %struct.btTriIndex** %tmp, align 4, !tbaa !2
  %m_softBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %5 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody, align 4, !tbaa !28
  %call4 = call %struct.btSoftBodyWorldInfo* @_ZN10btSoftBody12getWorldInfoEv(%class.btSoftBody* %5)
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %call4, i32 0, i32 8
  %6 = load %struct.btTriIndex*, %struct.btTriIndex** %tmp, align 4, !tbaa !2
  %m_childShape = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %6, i32 0, i32 1
  %7 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !33
  %call5 = call i32 @_ZN11btSparseSdfILi3EE16RemoveReferencesEP16btCollisionShape(%struct.btSparseSdf* %m_sparsesdf, %class.btCollisionShape* %7)
  %8 = load %struct.btTriIndex*, %struct.btTriIndex** %tmp, align 4, !tbaa !2
  %m_childShape6 = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %8, i32 0, i32 1
  %9 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape6, align 4, !tbaa !33
  %isnull = icmp eq %class.btCollisionShape* %9, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.body
  %10 = bitcast %class.btCollisionShape* %9 to void (%class.btCollisionShape*)***
  %vtable = load void (%class.btCollisionShape*)**, void (%class.btCollisionShape*)*** %10, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*)*, void (%class.btCollisionShape*)** %vtable, i64 1
  %11 = load void (%class.btCollisionShape*)*, void (%class.btCollisionShape*)** %vfn, align 4
  call void %11(%class.btCollisionShape* %9) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.body
  %12 = bitcast %struct.btTriIndex** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  br label %for.inc

for.inc:                                          ; preds = %delete.end
  %13 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_shapeCache7 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  call void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv(%class.btHashMap* %m_shapeCache7)
  ret void
}

; Function Attrs: nounwind
define hidden %class.btSoftBodyTriangleCallback* @_ZN26btSoftBodyTriangleCallbackD2Ev(%class.btSoftBodyTriangleCallback* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btSoftBodyTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV26btSoftBodyTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  call void @_ZN26btSoftBodyTriangleCallback10clearCacheEv(%class.btSoftBodyTriangleCallback* %this1)
  %m_shapeCache = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  %call = call %class.btHashMap* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev(%class.btHashMap* %m_shapeCache) #9
  %1 = bitcast %class.btSoftBodyTriangleCallback* %this1 to %class.btTriangleCallback*
  %call2 = call %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* %1) #9
  ret %class.btSoftBodyTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_ED2Ev(%class.btHashMap* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEED2Ev(%class.btAlignedObjectArray.93* %m_keyArray) #9
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayI10btTriIndexED2Ev(%class.btAlignedObjectArray.89* %m_valueArray) #9
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* %m_next) #9
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* %m_hashTable) #9
  ret %class.btHashMap* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN26btSoftBodyTriangleCallbackD0Ev(%class.btSoftBodyTriangleCallback* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %call = call %class.btSoftBodyTriangleCallback* @_ZN26btSoftBodyTriangleCallbackD1Ev(%class.btSoftBodyTriangleCallback* %this1) #9
  %0 = bitcast %class.btSoftBodyTriangleCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #6

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E4sizeEv(%class.btHashMap* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %m_valueArray)
  ret i32 %call
}

define linkonce_odr hidden %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10getAtIndexEi(%class.btHashMap* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %index.addr = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !32
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %0 = load i32, i32* %index.addr, align 4, !tbaa !32
  %call = call nonnull align 4 dereferenceable(8) %struct.btTriIndex* @_ZN20btAlignedObjectArrayI10btTriIndexEixEi(%class.btAlignedObjectArray.89* %m_valueArray, i32 %0)
  ret %struct.btTriIndex* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btSoftBodyWorldInfo* @_ZN10btSoftBody12getWorldInfoEv(%class.btSoftBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSoftBody*, align 4
  store %class.btSoftBody* %this, %class.btSoftBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBody*, %class.btSoftBody** %this.addr, align 4
  %m_worldInfo = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %this1, i32 0, i32 7
  %0 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %m_worldInfo, align 4, !tbaa !35
  ret %struct.btSoftBodyWorldInfo* %0
}

define linkonce_odr hidden i32 @_ZN11btSparseSdfILi3EE16RemoveReferencesEP16btCollisionShape(%struct.btSparseSdf* %this, %class.btCollisionShape* %pcs) #0 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  %pcs.addr = alloca %class.btCollisionShape*, align 4
  %refcount = alloca i32, align 4
  %i = alloca i32, align 4
  %root = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  %pp = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  %pc = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  %pn = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %pcs, %class.btCollisionShape** %pcs.addr, align 4, !tbaa !2
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %0 = bitcast i32* %refcount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %refcount, align 4, !tbaa !32
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.16* %cells)
  %cmp = icmp slt i32 %2, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %"struct.btSparseSdf<3>::Cell"*** %root to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %cells2 = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.16* %cells2, i32 %5)
  store %"struct.btSparseSdf<3>::Cell"** %call3, %"struct.btSparseSdf<3>::Cell"*** %root, align 4, !tbaa !2
  %6 = bitcast %"struct.btSparseSdf<3>::Cell"** %pp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store %"struct.btSparseSdf<3>::Cell"* null, %"struct.btSparseSdf<3>::Cell"** %pp, align 4, !tbaa !2
  %7 = bitcast %"struct.btSparseSdf<3>::Cell"** %pc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %root, align 4, !tbaa !2
  %9 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %8, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %9, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end8, %for.body
  %10 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %tobool = icmp ne %"struct.btSparseSdf<3>::Cell"* %10, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = bitcast %"struct.btSparseSdf<3>::Cell"** %pn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %next = getelementptr inbounds %"struct.btSparseSdf<3>::Cell", %"struct.btSparseSdf<3>::Cell"* %12, i32 0, i32 5
  %13 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %next, align 4, !tbaa !82
  store %"struct.btSparseSdf<3>::Cell"* %13, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  %14 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %pclient = getelementptr inbounds %"struct.btSparseSdf<3>::Cell", %"struct.btSparseSdf<3>::Cell"* %14, i32 0, i32 4
  %15 = load %class.btCollisionShape*, %class.btCollisionShape** %pclient, align 4, !tbaa !84
  %16 = load %class.btCollisionShape*, %class.btCollisionShape** %pcs.addr, align 4, !tbaa !2
  %cmp4 = icmp eq %class.btCollisionShape* %15, %16
  br i1 %cmp4, label %if.then, label %if.end8

if.then:                                          ; preds = %while.body
  %17 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pp, align 4, !tbaa !2
  %tobool5 = icmp ne %"struct.btSparseSdf<3>::Cell"* %17, null
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then
  %18 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  %19 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pp, align 4, !tbaa !2
  %next7 = getelementptr inbounds %"struct.btSparseSdf<3>::Cell", %"struct.btSparseSdf<3>::Cell"* %19, i32 0, i32 5
  store %"struct.btSparseSdf<3>::Cell"* %18, %"struct.btSparseSdf<3>::Cell"** %next7, align 4, !tbaa !82
  br label %if.end

if.else:                                          ; preds = %if.then
  %20 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  %21 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %root, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %20, %"struct.btSparseSdf<3>::Cell"** %21, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  %22 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %isnull = icmp eq %"struct.btSparseSdf<3>::Cell"* %22, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end
  %23 = bitcast %"struct.btSparseSdf<3>::Cell"* %22 to i8*
  call void @_ZdlPv(i8* %23) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end
  %24 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pp, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %24, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %25 = load i32, i32* %refcount, align 4, !tbaa !32
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %refcount, align 4, !tbaa !32
  br label %if.end8

if.end8:                                          ; preds = %delete.end, %while.body
  %26 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %26, %"struct.btSparseSdf<3>::Cell"** %pp, align 4, !tbaa !2
  %27 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %27, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %28 = bitcast %"struct.btSparseSdf<3>::Cell"** %pn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %29 = bitcast %"struct.btSparseSdf<3>::Cell"** %pc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast %"struct.btSparseSdf<3>::Cell"** %pp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast %"struct.btSparseSdf<3>::Cell"*** %root to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %32 = load i32, i32* %i, align 4, !tbaa !32
  %inc9 = add nsw i32 %32, 1
  store i32 %inc9, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = load i32, i32* %refcount, align 4, !tbaa !32
  %34 = bitcast i32* %refcount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  ret i32 %33
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #6

define linkonce_odr hidden void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E5clearEv(%class.btHashMap* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %m_next)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE5clearEv(%class.btAlignedObjectArray.89* %m_valueArray)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE5clearEv(%class.btAlignedObjectArray.93* %m_keyArray)
  ret void
}

define hidden void @_ZN26btSoftBodyTriangleCallback15processTriangleEP9btVector3ii(%class.btSoftBodyTriangleCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %ci = alloca %struct.btCollisionAlgorithmConstructionInfo, align 4
  %color = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %tr = alloca %class.btTransform*, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %triIndex = alloca %struct.btTriIndex, align 4
  %triKey = alloca %class.btHashKey, align 4
  %shapeIndex = alloca %struct.btTriIndex*, align 4
  %tm = alloca %class.btCollisionShape*, align 4
  %softBody = alloca %struct.btCollisionObjectWrapper, align 4
  %triBody = alloca %struct.btCollisionObjectWrapper, align 4
  %colAlgo = alloca %class.btCollisionAlgorithm*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca float, align 4
  %pts = alloca [6 x %class.btVector3], align 16
  %tm87 = alloca %class.btConvexHullShape*, align 4
  %softBody95 = alloca %struct.btCollisionObjectWrapper, align 4
  %triBody102 = alloca %struct.btCollisionObjectWrapper, align 4
  %colAlgo107 = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !32
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !32
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmConstructionInfo* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %0) #9
  %call = call %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* %ci)
  %m_dispatcher = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 6
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !26
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  store %class.btDispatcher* %1, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !24
  %m_dispatchInfoPtr = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %2 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr, align 4, !tbaa !27
  %tobool = icmp ne %struct.btDispatcherInfo* %2, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_dispatchInfoPtr2 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %3 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr2, align 4, !tbaa !27
  %m_debugDraw = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %3, i32 0, i32 5
  %4 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw, align 4, !tbaa !85
  %tobool3 = icmp ne %class.btIDebugDraw* %4, null
  br i1 %tobool3, label %land.lhs.true4, label %if.end

land.lhs.true4:                                   ; preds = %land.lhs.true
  %m_dispatchInfoPtr5 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr5, align 4, !tbaa !27
  %m_debugDraw6 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %5, i32 0, i32 5
  %6 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw6, align 4, !tbaa !85
  %7 = bitcast %class.btIDebugDraw* %6 to i32 (%class.btIDebugDraw*)***
  %vtable = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %7, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable, i64 12
  %8 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn, align 4
  %call7 = call i32 %8(%class.btIDebugDraw* %6)
  %and = and i32 %call7, 1
  %tobool8 = icmp ne i32 %and, 0
  br i1 %tobool8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true4
  %9 = bitcast %class.btVector3* %color to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !87
  %12 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !87
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %color, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %13 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast %class.btTransform** %tr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %m_triBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody, align 4, !tbaa !29
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %17)
  store %class.btTransform* %call12, %class.btTransform** %tr, align 4, !tbaa !2
  %m_dispatchInfoPtr13 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %18 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr13, align 4, !tbaa !27
  %m_debugDraw14 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %18, i32 0, i32 5
  %19 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw14, align 4, !tbaa !85
  %20 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #9
  %21 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %22 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 0
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp15, %class.btTransform* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %23 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %24 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %25 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp16, %class.btTransform* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx17)
  %26 = bitcast %class.btIDebugDraw* %19 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable18 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %26, align 4, !tbaa !8
  %vfn19 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable18, i64 2
  %27 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn19, align 4
  call void %27(%class.btIDebugDraw* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %color)
  %28 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  %29 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  %m_dispatchInfoPtr20 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %30 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr20, align 4, !tbaa !27
  %m_debugDraw21 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %30, i32 0, i32 5
  %31 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw21, align 4, !tbaa !85
  %32 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #9
  %33 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %34 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx23 = getelementptr inbounds %class.btVector3, %class.btVector3* %34, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp22, %class.btTransform* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx23)
  %35 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #9
  %36 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %37 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx25 = getelementptr inbounds %class.btVector3, %class.btVector3* %37, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp24, %class.btTransform* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx25)
  %38 = bitcast %class.btIDebugDraw* %31 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable26 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %38, align 4, !tbaa !8
  %vfn27 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable26, i64 2
  %39 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn27, align 4
  call void %39(%class.btIDebugDraw* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %color)
  %40 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #9
  %41 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %m_dispatchInfoPtr28 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %42 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr28, align 4, !tbaa !27
  %m_debugDraw29 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %42, i32 0, i32 5
  %43 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw29, align 4, !tbaa !85
  %44 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #9
  %45 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %46 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds %class.btVector3, %class.btVector3* %46, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp30, %class.btTransform* %45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx31)
  %47 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #9
  %48 = load %class.btTransform*, %class.btTransform** %tr, align 4, !tbaa !2
  %49 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds %class.btVector3, %class.btVector3* %49, i32 0
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btTransform* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx33)
  %50 = bitcast %class.btIDebugDraw* %43 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable34 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %50, align 4, !tbaa !8
  %vfn35 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable34, i64 2
  %51 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn35, align 4
  call void %51(%class.btIDebugDraw* %43, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %color)
  %52 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #9
  %53 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #9
  %54 = bitcast %class.btTransform** %tr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast %class.btVector3* %color to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true4, %land.lhs.true, %entry
  %56 = bitcast %struct.btTriIndex* %triIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %56) #9
  %57 = load i32, i32* %partId.addr, align 4, !tbaa !32
  %58 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !32
  %call36 = call %struct.btTriIndex* @_ZN10btTriIndexC2EiiP16btCollisionShape(%struct.btTriIndex* %triIndex, i32 %57, i32 %58, %class.btCollisionShape* null)
  %59 = bitcast %class.btHashKey* %triKey to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %call37 = call i32 @_ZNK10btTriIndex6getUidEv(%struct.btTriIndex* %triIndex)
  %call38 = call %class.btHashKey* @_ZN9btHashKeyI10btTriIndexEC2Ei(%class.btHashKey* %triKey, i32 %call37)
  %60 = bitcast %struct.btTriIndex** %shapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #9
  %m_shapeCache = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  %call39 = call %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_(%class.btHashMap* %m_shapeCache, %class.btHashKey* nonnull align 4 dereferenceable(4) %triKey)
  store %struct.btTriIndex* %call39, %struct.btTriIndex** %shapeIndex, align 4, !tbaa !2
  %61 = load %struct.btTriIndex*, %struct.btTriIndex** %shapeIndex, align 4, !tbaa !2
  %tobool40 = icmp ne %struct.btTriIndex* %61, null
  br i1 %tobool40, label %if.then41, label %if.end67

if.then41:                                        ; preds = %if.end
  %62 = bitcast %class.btCollisionShape** %tm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #9
  %63 = load %struct.btTriIndex*, %struct.btTriIndex** %shapeIndex, align 4, !tbaa !2
  %m_childShape = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %63, i32 0, i32 1
  %64 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !33
  store %class.btCollisionShape* %64, %class.btCollisionShape** %tm, align 4, !tbaa !2
  %65 = load %class.btCollisionShape*, %class.btCollisionShape** %tm, align 4, !tbaa !2
  %m_triBody42 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %66 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody42, align 4, !tbaa !29
  %call43 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %66)
  %call44 = call i8* @_ZNK16btCollisionShape14getUserPointerEv(%class.btCollisionShape* %call43)
  call void @_ZN16btCollisionShape14setUserPointerEPv(%class.btCollisionShape* %65, i8* %call44)
  %67 = bitcast %struct.btCollisionObjectWrapper* %softBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %67) #9
  %m_softBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %68 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody, align 4, !tbaa !28
  %69 = bitcast %class.btSoftBody* %68 to %class.btCollisionObject*
  %call45 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %69)
  %m_softBody46 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %70 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody46, align 4, !tbaa !28
  %71 = bitcast %class.btSoftBody* %70 to %class.btCollisionObject*
  %m_softBody47 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %72 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody47, align 4, !tbaa !28
  %73 = bitcast %class.btSoftBody* %72 to %class.btCollisionObject*
  %call48 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %73)
  %call49 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %softBody, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call45, %class.btCollisionObject* %71, %class.btTransform* nonnull align 4 dereferenceable(64) %call48, i32 -1, i32 -1)
  %74 = bitcast %struct.btCollisionObjectWrapper* %triBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %74) #9
  %75 = load %class.btCollisionShape*, %class.btCollisionShape** %tm, align 4, !tbaa !2
  %m_triBody50 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %76 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody50, align 4, !tbaa !29
  %m_triBody51 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %77 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody51, align 4, !tbaa !29
  %call52 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %77)
  %78 = load i32, i32* %partId.addr, align 4, !tbaa !32
  %79 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !32
  %call53 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %triBody, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %75, %class.btCollisionObject* %76, %class.btTransform* nonnull align 4 dereferenceable(64) %call52, i32 %78, i32 %79)
  %80 = bitcast %class.btCollisionAlgorithm** %colAlgo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #9
  %m_dispatcher154 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  %81 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher154, align 4, !tbaa !24
  %82 = bitcast %class.btDispatcher* %81 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable55 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %82, align 4, !tbaa !8
  %vfn56 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable55, i64 2
  %83 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn56, align 4
  %call57 = call %class.btCollisionAlgorithm* %83(%class.btDispatcher* %81, %struct.btCollisionObjectWrapper* %softBody, %struct.btCollisionObjectWrapper* %triBody, %class.btPersistentManifold* null)
  store %class.btCollisionAlgorithm* %call57, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %84 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %m_dispatchInfoPtr58 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %85 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr58, align 4, !tbaa !27
  %m_resultOut = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 5
  %86 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !88
  %87 = bitcast %class.btCollisionAlgorithm* %84 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable59 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %87, align 4, !tbaa !8
  %vfn60 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable59, i64 2
  %88 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn60, align 4
  call void %88(%class.btCollisionAlgorithm* %84, %struct.btCollisionObjectWrapper* %softBody, %struct.btCollisionObjectWrapper* %triBody, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %85, %class.btManifoldResult* %86)
  %89 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %90 = bitcast %class.btCollisionAlgorithm* %89 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable61 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %90, align 4, !tbaa !8
  %vfn62 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable61, i64 0
  %91 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn62, align 4
  %call63 = call %class.btCollisionAlgorithm* %91(%class.btCollisionAlgorithm* %89) #9
  %m_dispatcher164 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  %92 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher164, align 4, !tbaa !24
  %93 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %94 = bitcast %class.btCollisionAlgorithm* %93 to i8*
  %95 = bitcast %class.btDispatcher* %92 to void (%class.btDispatcher*, i8*)***
  %vtable65 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %95, align 4, !tbaa !8
  %vfn66 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable65, i64 15
  %96 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn66, align 4
  call void %96(%class.btDispatcher* %92, i8* %94)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %97 = bitcast %class.btCollisionAlgorithm** %colAlgo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %98 = bitcast %struct.btCollisionObjectWrapper* %triBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %98) #9
  %99 = bitcast %struct.btCollisionObjectWrapper* %softBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %99) #9
  %100 = bitcast %class.btCollisionShape** %tm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  br label %cleanup

if.end67:                                         ; preds = %if.end
  %101 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %101) #9
  %102 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %102) #9
  %103 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds %class.btVector3, %class.btVector3* %103, i32 1
  %104 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds %class.btVector3, %class.btVector3* %104, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx69, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx70)
  %105 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %105) #9
  %106 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds %class.btVector3, %class.btVector3* %106, i32 2
  %107 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds %class.btVector3, %class.btVector3* %107, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp71)
  %108 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %108) #9
  %109 = bitcast %class.btVector3* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %109) #9
  %call74 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normal)
  %110 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #9
  store float 0x3FAEB851E0000000, float* %ref.tmp75, align 4, !tbaa !87
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normal, float* nonnull align 4 dereferenceable(4) %ref.tmp75)
  %111 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast [6 x %class.btVector3]* %pts to i8*
  call void @llvm.lifetime.start.p0i8(i64 96, i8* %112) #9
  %arrayinit.begin = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %pts, i32 0, i32 0
  %113 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx77 = getelementptr inbounds %class.btVector3, %class.btVector3* %113, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.begin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx77, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %114 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx78 = getelementptr inbounds %class.btVector3, %class.btVector3* %114, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx78, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %arrayinit.element79 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %115 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds %class.btVector3, %class.btVector3* %115, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element79, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx80, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %arrayinit.element81 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element79, i32 1
  %116 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds %class.btVector3, %class.btVector3* %116, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element81, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx82, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %arrayinit.element83 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element81, i32 1
  %117 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx84 = getelementptr inbounds %class.btVector3, %class.btVector3* %117, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element83, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx84, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %arrayinit.element85 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element83, i32 1
  %118 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds %class.btVector3, %class.btVector3* %118, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element85, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx86, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %119 = bitcast %class.btConvexHullShape** %tm87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #9
  %call88 = call i8* @_ZN17btConvexHullShapenwEm(i32 112)
  %120 = bitcast i8* %call88 to %class.btConvexHullShape*
  %arrayidx89 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %pts, i32 0, i32 0
  %call90 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx89)
  %call91 = call %class.btConvexHullShape* @_ZN17btConvexHullShapeC1EPKfii(%class.btConvexHullShape* %120, float* %call90, i32 6, i32 16)
  store %class.btConvexHullShape* %120, %class.btConvexHullShape** %tm87, align 4, !tbaa !2
  %121 = load %class.btConvexHullShape*, %class.btConvexHullShape** %tm87, align 4, !tbaa !2
  %122 = bitcast %class.btConvexHullShape* %121 to %class.btCollisionShape*
  %m_triBody92 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %123 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody92, align 4, !tbaa !29
  %call93 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %123)
  %call94 = call i8* @_ZNK16btCollisionShape14getUserPointerEv(%class.btCollisionShape* %call93)
  call void @_ZN16btCollisionShape14setUserPointerEPv(%class.btCollisionShape* %122, i8* %call94)
  %124 = bitcast %struct.btCollisionObjectWrapper* %softBody95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %124) #9
  %m_softBody96 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %125 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody96, align 4, !tbaa !28
  %126 = bitcast %class.btSoftBody* %125 to %class.btCollisionObject*
  %call97 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %126)
  %m_softBody98 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %127 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody98, align 4, !tbaa !28
  %128 = bitcast %class.btSoftBody* %127 to %class.btCollisionObject*
  %m_softBody99 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %129 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody99, align 4, !tbaa !28
  %130 = bitcast %class.btSoftBody* %129 to %class.btCollisionObject*
  %call100 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %130)
  %call101 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %softBody95, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call97, %class.btCollisionObject* %128, %class.btTransform* nonnull align 4 dereferenceable(64) %call100, i32 -1, i32 -1)
  %131 = bitcast %struct.btCollisionObjectWrapper* %triBody102 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %131) #9
  %132 = load %class.btConvexHullShape*, %class.btConvexHullShape** %tm87, align 4, !tbaa !2
  %133 = bitcast %class.btConvexHullShape* %132 to %class.btCollisionShape*
  %m_triBody103 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %134 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody103, align 4, !tbaa !29
  %m_triBody104 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 2
  %135 = load %class.btCollisionObject*, %class.btCollisionObject** %m_triBody104, align 4, !tbaa !29
  %call105 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %135)
  %136 = load i32, i32* %partId.addr, align 4, !tbaa !32
  %137 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !32
  %call106 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %triBody102, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %133, %class.btCollisionObject* %134, %class.btTransform* nonnull align 4 dereferenceable(64) %call105, i32 %136, i32 %137)
  %138 = bitcast %class.btCollisionAlgorithm** %colAlgo107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #9
  %m_dispatcher1108 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  %139 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1108, align 4, !tbaa !24
  %140 = bitcast %class.btDispatcher* %139 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable109 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %140, align 4, !tbaa !8
  %vfn110 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable109, i64 2
  %141 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn110, align 4
  %call111 = call %class.btCollisionAlgorithm* %141(%class.btDispatcher* %139, %struct.btCollisionObjectWrapper* %softBody95, %struct.btCollisionObjectWrapper* %triBody102, %class.btPersistentManifold* null)
  store %class.btCollisionAlgorithm* %call111, %class.btCollisionAlgorithm** %colAlgo107, align 4, !tbaa !2
  %142 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo107, align 4, !tbaa !2
  %m_dispatchInfoPtr112 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  %143 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfoPtr112, align 4, !tbaa !27
  %m_resultOut113 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 5
  %144 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut113, align 4, !tbaa !88
  %145 = bitcast %class.btCollisionAlgorithm* %142 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable114 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %145, align 4, !tbaa !8
  %vfn115 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable114, i64 2
  %146 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn115, align 4
  call void %146(%class.btCollisionAlgorithm* %142, %struct.btCollisionObjectWrapper* %softBody95, %struct.btCollisionObjectWrapper* %triBody102, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %143, %class.btManifoldResult* %144)
  %147 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo107, align 4, !tbaa !2
  %148 = bitcast %class.btCollisionAlgorithm* %147 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable116 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %148, align 4, !tbaa !8
  %vfn117 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable116, i64 0
  %149 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn117, align 4
  %call118 = call %class.btCollisionAlgorithm* %149(%class.btCollisionAlgorithm* %147) #9
  %m_dispatcher1119 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  %150 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1119, align 4, !tbaa !24
  %151 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo107, align 4, !tbaa !2
  %152 = bitcast %class.btCollisionAlgorithm* %151 to i8*
  %153 = bitcast %class.btDispatcher* %150 to void (%class.btDispatcher*, i8*)***
  %vtable120 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %153, align 4, !tbaa !8
  %vfn121 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable120, i64 15
  %154 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn121, align 4
  call void %154(%class.btDispatcher* %150, i8* %152)
  %155 = load %class.btConvexHullShape*, %class.btConvexHullShape** %tm87, align 4, !tbaa !2
  %156 = bitcast %class.btConvexHullShape* %155 to %class.btCollisionShape*
  %m_childShape122 = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %triIndex, i32 0, i32 1
  store %class.btCollisionShape* %156, %class.btCollisionShape** %m_childShape122, align 4, !tbaa !33
  %m_shapeCache123 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 9
  call void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_(%class.btHashMap* %m_shapeCache123, %class.btHashKey* nonnull align 4 dereferenceable(4) %triKey, %struct.btTriIndex* nonnull align 4 dereferenceable(8) %triIndex)
  %157 = bitcast %class.btCollisionAlgorithm** %colAlgo107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #9
  %158 = bitcast %struct.btCollisionObjectWrapper* %triBody102 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %158) #9
  %159 = bitcast %struct.btCollisionObjectWrapper* %softBody95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %159) #9
  %160 = bitcast %class.btConvexHullShape** %tm87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #9
  %161 = bitcast [6 x %class.btVector3]* %pts to i8*
  call void @llvm.lifetime.end.p0i8(i64 96, i8* %161) #9
  %162 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %162) #9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end67, %if.then41
  %163 = bitcast %struct.btTriIndex** %shapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #9
  %164 = bitcast %class.btHashKey* %triKey to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #9
  %165 = bitcast %struct.btTriIndex* %triIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %165) #9
  %166 = bitcast %struct.btCollisionAlgorithmConstructionInfo* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %166) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %this, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 0
  store %class.btDispatcher* null, %class.btDispatcher** %m_dispatcher1, align 4, !tbaa !24
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 1
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !89
  ret %struct.btCollisionAlgorithmConstructionInfo* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !87
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !87
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !87
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !87
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !87
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !87
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btTriIndex* @_ZN10btTriIndexC2EiiP16btCollisionShape(%struct.btTriIndex* returned %this, i32 %partId, i32 %triangleIndex, %class.btCollisionShape* %shape) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btTriIndex*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  store %struct.btTriIndex* %this, %struct.btTriIndex** %this.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !32
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !32
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %this1 = load %struct.btTriIndex*, %struct.btTriIndex** %this.addr, align 4
  %0 = load i32, i32* %partId.addr, align 4, !tbaa !32
  %shl = shl i32 %0, 21
  %1 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !32
  %or = or i32 %shl, %1
  %m_PartIdTriangleIndex = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %this1, i32 0, i32 0
  store i32 %or, i32* %m_PartIdTriangleIndex, align 4, !tbaa !90
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  %m_childShape = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %this1, i32 0, i32 1
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_childShape, align 4, !tbaa !33
  ret %struct.btTriIndex* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK10btTriIndex6getUidEv(%struct.btTriIndex* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btTriIndex*, align 4
  store %struct.btTriIndex* %this, %struct.btTriIndex** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btTriIndex*, %struct.btTriIndex** %this.addr, align 4
  %m_PartIdTriangleIndex = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_PartIdTriangleIndex, align 4, !tbaa !90
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btHashKey* @_ZN9btHashKeyI10btTriIndexEC2Ei(%class.btHashKey* returned %this, i32 %uid) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHashKey*, align 4
  %uid.addr = alloca i32, align 4
  store %class.btHashKey* %this, %class.btHashKey** %this.addr, align 4, !tbaa !2
  store i32 %uid, i32* %uid.addr, align 4, !tbaa !32
  %this1 = load %class.btHashKey*, %class.btHashKey** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashKey, %class.btHashKey* %this1, i32 0, i32 0
  %0 = load i32, i32* %uid.addr, align 4, !tbaa !32
  store i32 %0, i32* %m_uid, align 4, !tbaa !91
  ret %class.btHashKey* %this1
}

define linkonce_odr hidden %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_EixERKS2_(%class.btHashMap* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashKey*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %key, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call = call %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E4findERKS2_(%class.btHashMap* %this1, %class.btHashKey* nonnull align 4 dereferenceable(4) %0)
  ret %struct.btTriIndex* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionShape14setUserPointerEPv(%class.btCollisionShape* %this, i8* %userPtr) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %userPtr.addr = alloca i8*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* %0, i8** %m_userPointer, align 4, !tbaa !93
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !95
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK16btCollisionShape14getUserPointerEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  %0 = load i8*, i8** %m_userPointer, align 4, !tbaa !93
  ret i8* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !95
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !32
  store i32 %index, i32* %index.addr, align 4, !tbaa !32
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4, !tbaa !97
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4, !tbaa !98
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !30
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4, !tbaa !2
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4, !tbaa !32
  store i32 %4, i32* %m_partId, align 4, !tbaa !99
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4, !tbaa !32
  store i32 %5, i32* %m_index, align 4, !tbaa !100
  ret %struct.btCollisionObjectWrapper* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !87
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !87
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !87
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !87
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !87
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !87
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !87
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !87
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !87
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !87
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !87
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !87
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !87
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !87
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !87
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !87
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !87
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !87
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !87
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !87
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !87
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !87
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !87
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !87
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !87
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !87
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !87
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !87
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !87
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !87
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !87
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !87
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !87
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !87
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !87
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !87
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !87
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !87
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !87
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN17btConvexHullShapenwEm(i32 %sizeInBytes) #5 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !101
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !101
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

declare %class.btConvexHullShape* @_ZN17btConvexHullShapeC1EPKfii(%class.btConvexHullShape* returned, float*, i32, i32) unnamed_addr #1

define linkonce_odr hidden void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E6insertERKS2_RKS1_(%class.btHashMap* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %key, %struct.btTriIndex* nonnull align 4 dereferenceable(8) %value) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashKey*, align 4
  %value.addr = alloca %struct.btTriIndex*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %key, %class.btHashKey** %key.addr, align 4, !tbaa !2
  store %struct.btTriIndex* %value, %struct.btTriIndex** %value.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashKeyI10btTriIndexE7getHashEv(%class.btHashKey* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !32
  %2 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_(%class.btHashMap* %this1, %class.btHashKey* nonnull align 4 dereferenceable(4) %3)
  store i32 %call3, i32* %index, align 4, !tbaa !32
  %4 = load i32, i32* %index, align 4, !tbaa !32
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.btTriIndex*, %struct.btTriIndex** %value.addr, align 4, !tbaa !2
  %m_valueArray4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %6 = load i32, i32* %index, align 4, !tbaa !32
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btTriIndex* @_ZN20btAlignedObjectArrayI10btTriIndexEixEi(%class.btAlignedObjectArray.89* %m_valueArray4, i32 %6)
  %7 = bitcast %struct.btTriIndex* %call5 to i8*
  %8 = bitcast %struct.btTriIndex* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false), !tbaa.struct !103
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_valueArray6 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %m_valueArray6)
  store i32 %call7, i32* %count, align 4, !tbaa !32
  %10 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %m_valueArray8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray8)
  store i32 %call9, i32* %oldCapacity, align 4, !tbaa !32
  %m_valueArray10 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %11 = load %struct.btTriIndex*, %struct.btTriIndex** %value.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE9push_backERKS0_(%class.btAlignedObjectArray.89* %m_valueArray10, %struct.btTriIndex* nonnull align 4 dereferenceable(8) %11)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %12 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9push_backERKS2_(%class.btAlignedObjectArray.93* %m_keyArray, %class.btHashKey* nonnull align 4 dereferenceable(4) %12)
  %13 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_valueArray11 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray11)
  store i32 %call12, i32* %newCapacity, align 4, !tbaa !32
  %14 = load i32, i32* %oldCapacity, align 4, !tbaa !32
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %cmp13 = icmp slt i32 %14, %15
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end
  %16 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  call void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_(%class.btHashMap* %this1, %class.btHashKey* nonnull align 4 dereferenceable(4) %16)
  %17 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call15 = call i32 @_ZNK9btHashKeyI10btTriIndexE7getHashEv(%class.btHashKey* %17)
  %m_valueArray16 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray16)
  %sub18 = sub nsw i32 %call17, 1
  %and19 = and i32 %call15, %sub18
  store i32 %and19, i32* %hash, align 4, !tbaa !32
  br label %if.end20

if.end20:                                         ; preds = %if.then14, %if.end
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %18 = load i32, i32* %hash, align 4, !tbaa !32
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable, i32 %18)
  %19 = load i32, i32* %call21, align 4, !tbaa !32
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %20 = load i32, i32* %count, align 4, !tbaa !32
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_next, i32 %20)
  store i32 %19, i32* %call22, align 4, !tbaa !32
  %21 = load i32, i32* %count, align 4, !tbaa !32
  %m_hashTable23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %22 = load i32, i32* %hash, align 4, !tbaa !32
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable23, i32 %22)
  store i32 %21, i32* %call24, align 4, !tbaa !32
  %23 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then
  %26 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

define hidden void @_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfPK24btCollisionObjectWrapperRK16btDispatcherInfoP16btManifoldResult(%class.btSoftBodyTriangleCallback* %this, float %collisionMarginTriangle, %struct.btCollisionObjectWrapper* %triBodyWrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) #0 {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  %collisionMarginTriangle.addr = alloca float, align 4
  %triBodyWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %aabbWorldSpaceMin = alloca %class.btVector3, align 4
  %aabbWorldSpaceMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %softBodyCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %softTransform = alloca %class.btTransform, align 4
  %convexInTriangleSpace = alloca %class.btTransform, align 4
  %ref.tmp8 = alloca %class.btTransform, align 4
  %ref.tmp9 = alloca %class.btTransform, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  store float %collisionMarginTriangle, float* %collisionMarginTriangle.addr, align 4, !tbaa !87
  store %struct.btCollisionObjectWrapper* %triBodyWrap, %struct.btCollisionObjectWrapper** %triBodyWrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %m_dispatchInfoPtr = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 7
  store %struct.btDispatcherInfo* %0, %struct.btDispatcherInfo** %m_dispatchInfoPtr, align 4, !tbaa !27
  %1 = load float, float* %collisionMarginTriangle.addr, align 4, !tbaa !87
  %add = fadd float %1, 0x3FAEB851E0000000
  %m_collisionMarginTriangle = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 8
  store float %add, float* %m_collisionMarginTriangle, align 4, !tbaa !104
  %2 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 5
  store %class.btManifoldResult* %2, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !88
  %3 = bitcast %class.btVector3* %aabbWorldSpaceMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbWorldSpaceMin)
  %4 = bitcast %class.btVector3* %aabbWorldSpaceMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbWorldSpaceMax)
  %m_softBody = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 1
  %5 = load %class.btSoftBody*, %class.btSoftBody** %m_softBody, align 4, !tbaa !28
  %6 = bitcast %class.btSoftBody* %5 to void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)**, void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)*** %6, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)*, void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)** %vtable, i64 7
  %7 = load void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)*, void (%class.btSoftBody*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btSoftBody* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMax)
  %8 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMin)
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 5.000000e-01, float* %ref.tmp3, align 4, !tbaa !87
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %11 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #9
  %13 = bitcast %class.btVector3* %softBodyCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %14 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbWorldSpaceMin)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store float 5.000000e-01, float* %ref.tmp5, align 4, !tbaa !87
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %softBodyCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %16 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  %18 = bitcast %class.btTransform* %softTransform to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %18) #9
  %call6 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %softTransform)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %softTransform)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %softTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %softBodyCenter)
  %19 = bitcast %class.btTransform* %convexInTriangleSpace to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %19) #9
  %call7 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %convexInTriangleSpace)
  %20 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %20) #9
  %21 = bitcast %class.btTransform* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %21) #9
  %22 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triBodyWrap.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %22)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp9, %class.btTransform* %call10)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp8, %class.btTransform* %ref.tmp9, %class.btTransform* nonnull align 4 dereferenceable(64) %softTransform)
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %convexInTriangleSpace, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp8)
  %23 = bitcast %class.btTransform* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %23) #9
  %24 = bitcast %class.btTransform* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %24) #9
  %m_collisionMarginTriangle12 = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 8
  %25 = load float, float* %m_collisionMarginTriangle12, align 4, !tbaa !104
  %m_aabbMin = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 3
  %m_aabbMax = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 4
  call void @_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, float %25, %class.btTransform* nonnull align 4 dereferenceable(64) %convexInTriangleSpace, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  %26 = bitcast %class.btTransform* %convexInTriangleSpace to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %26) #9
  %27 = bitcast %class.btTransform* %softTransform to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %27) #9
  %28 = bitcast %class.btVector3* %softBodyCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  %29 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  %30 = bitcast %class.btVector3* %aabbWorldSpaceMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  %31 = bitcast %class.btVector3* %aabbWorldSpaceMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !87
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !87
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !87
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !87
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !87
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !87
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !87
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !87
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !87
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !87
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !105
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !107
  ret %class.btTransform* %0
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #9
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #5 comdat {
entry:
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %halfExtentsWithMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !87
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %halfExtentsWithMargin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %halfExtentsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #9
  %4 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %4) #9
  %5 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call1)
  %6 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #9
  %7 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %7)
  %8 = bitcast %class.btVector3* %center to i8*
  %9 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !105
  %10 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %halfExtentsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %11 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %12 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !105
  %15 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #9
  %16 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %17 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !105
  %20 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #9
  %21 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #9
  %22 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #9
  %23 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %23) #9
  %24 = bitcast %class.btVector3* %halfExtentsWithMargin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #9
  ret void
}

define hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm10clearCacheEv(%class.btSoftBodyConcaveCollisionAlgorithm* %this) #0 {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %m_btSoftBodyTriangleCallback = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN26btSoftBodyTriangleCallback10clearCacheEv(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback)
  ret void
}

define hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftBodyConcaveCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %triBody = alloca %struct.btCollisionObjectWrapper*, align 4
  %triOb = alloca %class.btCollisionObject*, align 4
  %concaveShape = alloca %class.btConcaveShape*, align 4
  %collisionMarginTriangle = alloca float, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %struct.btCollisionObjectWrapper** %triBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_isSwapped = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %2, %cond.true ], [ %3, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %triBody, align 4, !tbaa !2
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triBody, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %4)
  %call2 = call zeroext i1 @_ZNK16btCollisionShape9isConcaveEv(%class.btCollisionShape* %call)
  br i1 %call2, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %5 = bitcast %class.btCollisionObject** %triOb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triBody, align 4, !tbaa !2
  %call3 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  store %class.btCollisionObject* %call3, %class.btCollisionObject** %triOb, align 4, !tbaa !2
  %7 = bitcast %class.btConcaveShape** %concaveShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %triOb, align 4, !tbaa !2
  %call4 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %8)
  %9 = bitcast %class.btCollisionShape* %call4 to %class.btConcaveShape*
  store %class.btConcaveShape* %9, %class.btConcaveShape** %concaveShape, align 4, !tbaa !2
  %10 = bitcast float* %collisionMarginTriangle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btConcaveShape*, %class.btConcaveShape** %concaveShape, align 4, !tbaa !2
  %12 = bitcast %class.btConcaveShape* %11 to float (%class.btConcaveShape*)***
  %vtable = load float (%class.btConcaveShape*)**, float (%class.btConcaveShape*)*** %12, align 4, !tbaa !8
  %vfn = getelementptr inbounds float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vtable, i64 12
  %13 = load float (%class.btConcaveShape*)*, float (%class.btConcaveShape*)** %vfn, align 4
  %call5 = call float %13(%class.btConcaveShape* %11)
  store float %call5, float* %collisionMarginTriangle, align 4, !tbaa !87
  %m_btSoftBodyTriangleCallback = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %14 = load float, float* %collisionMarginTriangle, align 4, !tbaa !87
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triBody, align 4, !tbaa !2
  %16 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %17 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN26btSoftBodyTriangleCallback22setTimeStepAndCountersEfPK24btCollisionObjectWrapperRK16btDispatcherInfoP16btManifoldResult(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback, float %14, %struct.btCollisionObjectWrapper* %15, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %16, %class.btManifoldResult* %17)
  %18 = load %class.btConcaveShape*, %class.btConcaveShape** %concaveShape, align 4, !tbaa !2
  %m_btSoftBodyTriangleCallback6 = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %19 = bitcast %class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback6 to %class.btTriangleCallback*
  %m_btSoftBodyTriangleCallback7 = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK26btSoftBodyTriangleCallback10getAabbMinEv(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback7)
  %m_btSoftBodyTriangleCallback9 = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK26btSoftBodyTriangleCallback10getAabbMaxEv(%class.btSoftBodyTriangleCallback* %m_btSoftBodyTriangleCallback9)
  %20 = bitcast %class.btConcaveShape* %18 to void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable11 = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %20, align 4, !tbaa !8
  %vfn12 = getelementptr inbounds void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable11, i64 16
  %21 = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn12, align 4
  call void %21(%class.btConcaveShape* %18, %class.btTriangleCallback* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %22 = bitcast float* %collisionMarginTriangle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast %class.btConcaveShape** %concaveShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast %class.btCollisionObject** %triOb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %25 = bitcast %struct.btCollisionObjectWrapper** %triBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !98
  ret %class.btCollisionShape* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape9isConcaveEv(%class.btCollisionShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK26btSoftBodyTriangleCallback10getAabbMinEv(%class.btSoftBodyTriangleCallback* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %m_aabbMin = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 3
  ret %class.btVector3* %m_aabbMin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK26btSoftBodyTriangleCallback10getAabbMaxEv(%class.btSoftBodyTriangleCallback* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btSoftBodyTriangleCallback*, align 4
  store %class.btSoftBodyTriangleCallback* %this, %class.btSoftBodyTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyTriangleCallback*, %class.btSoftBodyTriangleCallback** %this.addr, align 4
  %m_aabbMax = getelementptr inbounds %class.btSoftBodyTriangleCallback, %class.btSoftBodyTriangleCallback* %this1, i32 0, i32 4
  ret %class.btVector3* %m_aabbMax
}

define hidden float @_ZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %convexbody = alloca %class.btCollisionObject*, align 4
  %triBody = alloca %class.btCollisionObject*, align 4
  %squareMot0 = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %triInv = alloca %class.btTransform, align 4
  %convexFromLocal = alloca %class.btTransform, align 4
  %convexToLocal = alloca %class.btTransform, align 4
  %rayAabbMin = alloca %class.btVector3, align 4
  %rayAabbMax = alloca %class.btVector3, align 4
  %ccdRadius0 = alloca float, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %curHitFraction = alloca float, align 4
  %raycastCallback = alloca %struct.LocalTriangleSphereCastCallback, align 4
  %concavebody = alloca %class.btCollisionObject*, align 4
  %triangleMesh = alloca %class.btConcaveShape*, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %1 = bitcast %class.btCollisionObject** %convexbody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_isSwapped = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 1
  %2 = load i8, i8* %m_isSwapped, align 4, !tbaa !11, !range !10
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btCollisionObject* [ %3, %cond.true ], [ %4, %cond.false ]
  store %class.btCollisionObject* %cond, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %5 = bitcast %class.btCollisionObject** %triBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_isSwapped2 = getelementptr inbounds %class.btSoftBodyConcaveCollisionAlgorithm, %class.btSoftBodyConcaveCollisionAlgorithm* %this1, i32 0, i32 1
  %6 = load i8, i8* %m_isSwapped2, align 4, !tbaa !11, !range !10
  %tobool3 = trunc i8 %6 to i1
  br i1 %tobool3, label %cond.true4, label %cond.false5

cond.true4:                                       ; preds = %cond.end
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  br label %cond.end6

cond.false5:                                      ; preds = %cond.end
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  br label %cond.end6

cond.end6:                                        ; preds = %cond.false5, %cond.true4
  %cond7 = phi %class.btCollisionObject* [ %7, %cond.true4 ], [ %8, %cond.false5 ]
  store %class.btCollisionObject* %cond7, %class.btCollisionObject** %triBody, align 4, !tbaa !2
  %9 = bitcast float* %squareMot0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %11)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call)
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %12)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  store float %call11, float* %squareMot0, align 4, !tbaa !87
  %14 = load float, float* %squareMot0, align 4, !tbaa !87
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call12 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %15)
  %cmp = fcmp olt float %14, %call12
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end6
  store float 1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

if.end:                                           ; preds = %cond.end6
  %16 = bitcast %class.btTransform* %triInv to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %16) #9
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %triBody, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %17)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %triInv, %class.btTransform* %call13)
  %18 = bitcast %class.btTransform* %convexFromLocal to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %18) #9
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %19)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %convexFromLocal, %class.btTransform* %triInv, %class.btTransform* nonnull align 4 dereferenceable(64) %call14)
  %20 = bitcast %class.btTransform* %convexToLocal to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %20) #9
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %21)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %convexToLocal, %class.btTransform* %triInv, %class.btTransform* nonnull align 4 dereferenceable(64) %call15)
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %triBody, align 4, !tbaa !2
  %call16 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %22)
  %call17 = call zeroext i1 @_ZNK16btCollisionShape9isConcaveEv(%class.btCollisionShape* %call16)
  br i1 %call17, label %if.then18, label %if.end52

if.then18:                                        ; preds = %if.end
  %23 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %convexFromLocal)
  %24 = bitcast %class.btVector3* %rayAabbMin to i8*
  %25 = bitcast %class.btVector3* %call19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !105
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %convexToLocal)
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %call20)
  %26 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %convexFromLocal)
  %27 = bitcast %class.btVector3* %rayAabbMax to i8*
  %28 = bitcast %class.btVector3* %call21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !105
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %convexToLocal)
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %call22)
  %29 = bitcast float* %ccdRadius0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call23 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %30)
  store float %call23, float* %ccdRadius0, align 4, !tbaa !87
  %31 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #9
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ccdRadius0, float* nonnull align 4 dereferenceable(4) %ccdRadius0, float* nonnull align 4 dereferenceable(4) %ccdRadius0)
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %32 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #9
  %33 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #9
  %call28 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ccdRadius0, float* nonnull align 4 dereferenceable(4) %ccdRadius0, float* nonnull align 4 dereferenceable(4) %ccdRadius0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %34 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #9
  %35 = bitcast float* %curHitFraction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  store float 1.000000e+00, float* %curHitFraction, align 4, !tbaa !87
  %36 = bitcast %struct.LocalTriangleSphereCastCallback* %raycastCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 204, i8* %36) #9
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call30 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %37)
  %38 = load float, float* %curHitFraction, align 4, !tbaa !87
  %call31 = call %struct.LocalTriangleSphereCastCallback* @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackC2ERK11btTransformSA_ff(%struct.LocalTriangleSphereCastCallback* %raycastCallback, %class.btTransform* nonnull align 4 dereferenceable(64) %convexFromLocal, %class.btTransform* nonnull align 4 dereferenceable(64) %convexToLocal, float %call30, float %38)
  %39 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call32 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %39)
  %m_hitFraction = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %raycastCallback, i32 0, i32 5
  store float %call32, float* %m_hitFraction, align 4, !tbaa !108
  %40 = bitcast %class.btCollisionObject** %concavebody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  %41 = load %class.btCollisionObject*, %class.btCollisionObject** %triBody, align 4, !tbaa !2
  store %class.btCollisionObject* %41, %class.btCollisionObject** %concavebody, align 4, !tbaa !2
  %42 = bitcast %class.btConcaveShape** %triangleMesh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #9
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %concavebody, align 4, !tbaa !2
  %call33 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %43)
  %44 = bitcast %class.btCollisionShape* %call33 to %class.btConcaveShape*
  store %class.btConcaveShape* %44, %class.btConcaveShape** %triangleMesh, align 4, !tbaa !2
  %45 = load %class.btConcaveShape*, %class.btConcaveShape** %triangleMesh, align 4, !tbaa !2
  %tobool34 = icmp ne %class.btConcaveShape* %45, null
  br i1 %tobool34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.then18
  %46 = load %class.btConcaveShape*, %class.btConcaveShape** %triangleMesh, align 4, !tbaa !2
  %47 = bitcast %struct.LocalTriangleSphereCastCallback* %raycastCallback to %class.btTriangleCallback*
  %48 = bitcast %class.btConcaveShape* %46 to void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %48, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 16
  %49 = load void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btConcaveShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %49(%class.btConcaveShape* %46, %class.btTriangleCallback* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMax)
  br label %if.end36

if.end36:                                         ; preds = %if.then35, %if.then18
  %m_hitFraction37 = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %raycastCallback, i32 0, i32 5
  %50 = load float, float* %m_hitFraction37, align 4, !tbaa !108
  %51 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %call38 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %51)
  %cmp39 = fcmp olt float %50, %call38
  br i1 %cmp39, label %if.then40, label %if.end43

if.then40:                                        ; preds = %if.end36
  %52 = load %class.btCollisionObject*, %class.btCollisionObject** %convexbody, align 4, !tbaa !2
  %m_hitFraction41 = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %raycastCallback, i32 0, i32 5
  %53 = load float, float* %m_hitFraction41, align 4, !tbaa !108
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %52, float %53)
  %m_hitFraction42 = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %raycastCallback, i32 0, i32 5
  %54 = load float, float* %m_hitFraction42, align 4, !tbaa !108
  store float %54, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end43:                                         ; preds = %if.end36
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end43, %if.then40
  %55 = bitcast %class.btConcaveShape** %triangleMesh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  %56 = bitcast %class.btCollisionObject** %concavebody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #9
  %call46 = call %struct.LocalTriangleSphereCastCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %struct.LocalTriangleSphereCastCallback* (%struct.LocalTriangleSphereCastCallback*)*)(%struct.LocalTriangleSphereCastCallback* %raycastCallback) #9
  %57 = bitcast %struct.LocalTriangleSphereCastCallback* %raycastCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 204, i8* %57) #9
  %58 = bitcast float* %curHitFraction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #9
  %59 = bitcast float* %ccdRadius0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %60 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #9
  %61 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup53 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end52

if.end52:                                         ; preds = %cleanup.cont, %if.end
  store float 1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

cleanup53:                                        ; preds = %if.end52, %cleanup
  %62 = bitcast %class.btTransform* %convexToLocal to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %62) #9
  %63 = bitcast %class.btTransform* %convexFromLocal to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %63) #9
  %64 = bitcast %class.btTransform* %triInv to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %64) #9
  br label %cleanup56

cleanup56:                                        ; preds = %cleanup53, %if.then
  %65 = bitcast float* %squareMot0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #9
  %66 = bitcast %class.btCollisionObject** %triBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #9
  %67 = bitcast %class.btCollisionObject** %convexbody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #9
  %68 = load float, float* %retval, align 4
  ret float %68
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  ret %class.btTransform* %m_interpolationWorldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load float, float* %m_ccdMotionThreshold, align 4, !tbaa !110
  %m_ccdMotionThreshold2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %1 = load float, float* %m_ccdMotionThreshold2, align 4, !tbaa !110
  %mul = fmul float %0, %1
  ret float %mul
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 23
  %0 = load float, float* %m_ccdSweptSphereRadius, align 4, !tbaa !111
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !87
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !87
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !87
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !87
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !87
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !87
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !87
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !87
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !87
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !87
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !87
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !87
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !87
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !87
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !87
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !87
  ret %class.btVector3* %this1
}

define internal %struct.LocalTriangleSphereCastCallback* @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackC2ERK11btTransformSA_ff(%struct.LocalTriangleSphereCastCallback* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %from, %class.btTransform* nonnull align 4 dereferenceable(64) %to, float %ccdSphereRadius, float %hitFraction) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.LocalTriangleSphereCastCallback*, align 4
  %from.addr = alloca %class.btTransform*, align 4
  %to.addr = alloca %class.btTransform*, align 4
  %ccdSphereRadius.addr = alloca float, align 4
  %hitFraction.addr = alloca float, align 4
  store %struct.LocalTriangleSphereCastCallback* %this, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %from, %class.btTransform** %from.addr, align 4, !tbaa !2
  store %class.btTransform* %to, %class.btTransform** %to.addr, align 4, !tbaa !2
  store float %ccdSphereRadius, float* %ccdSphereRadius.addr, align 4, !tbaa !87
  store float %hitFraction, float* %hitFraction.addr, align 4, !tbaa !87
  %this1 = load %struct.LocalTriangleSphereCastCallback*, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4
  %0 = bitcast %struct.LocalTriangleSphereCastCallback* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #9
  %1 = bitcast %struct.LocalTriangleSphereCastCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_ccdSphereFromTrans = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 1
  %2 = load %class.btTransform*, %class.btTransform** %from.addr, align 4, !tbaa !2
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_ccdSphereFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %2)
  %m_ccdSphereToTrans = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %to.addr, align 4, !tbaa !2
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_ccdSphereToTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_meshTransform = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 3
  %call4 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_meshTransform)
  %m_ccdSphereRadius = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 4
  %4 = load float, float* %ccdSphereRadius.addr, align 4, !tbaa !87
  store float %4, float* %m_ccdSphereRadius, align 4, !tbaa !112
  %m_hitFraction = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 5
  %5 = load float, float* %hitFraction.addr, align 4, !tbaa !87
  store float %5, float* %m_hitFraction, align 4, !tbaa !108
  ret %struct.LocalTriangleSphereCastCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  %0 = load float, float* %m_hitFraction, align 4, !tbaa !113
  ret float %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %this, float %hitFraction) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %hitFraction.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  store float %hitFraction, float* %hitFraction.addr, align 4, !tbaa !87
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %hitFraction.addr, align 4, !tbaa !87
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  store float %0, float* %m_hitFraction, align 4, !tbaa !113
  ret void
}

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN35btSoftBodyConcaveCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btAlignedObjectArray.97* nonnull align 1 %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSoftBodyConcaveCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.97*, align 4
  store %class.btSoftBodyConcaveCollisionAlgorithm* %this, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.97* %manifoldArray, %class.btAlignedObjectArray.97** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodyConcaveCollisionAlgorithm*, %class.btSoftBodyConcaveCollisionAlgorithm** %this.addr, align 4
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.86* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayI10btTriIndexEC2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorI10btTriIndexLj16EEC2Ev(%class.btAlignedAllocator.90* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEC2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EEC2Ev(%class.btAlignedAllocator.94* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.86* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  ret %class.btAlignedAllocator.86* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !114
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !115
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !116
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !117
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorI10btTriIndexLj16EEC2Ev(%class.btAlignedAllocator.90* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  ret %class.btAlignedAllocator.90* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE4initEv(%class.btAlignedObjectArray.89* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !118
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %struct.btTriIndex* null, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !120
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !121
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EEC2Ev(%class.btAlignedAllocator.94* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  ret %class.btAlignedAllocator.94* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4initEv(%class.btAlignedObjectArray.93* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !122
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store %class.btHashKey* null, %class.btHashKey** %m_data, align 4, !tbaa !123
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !124
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !125
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEED2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE5clearEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayI10btTriIndexED2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE5clearEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE5clearEv(%class.btAlignedObjectArray.93* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7destroyEii(%class.btAlignedObjectArray.93* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE10deallocateEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7destroyEii(%class.btAlignedObjectArray.93* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !32
  store i32 %last, i32* %last.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %last.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %4 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btHashKey, %class.btHashKey* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !124
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE10deallocateEv(%class.btAlignedObjectArray.93* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %tobool = icmp ne %class.btHashKey* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !122, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %2 = load %class.btHashKey*, %class.btHashKey** %m_data4, align 4, !tbaa !123
  call void @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE10deallocateEPS2_(%class.btAlignedAllocator.94* %m_allocator, %class.btHashKey* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store %class.btHashKey* null, %class.btHashKey** %m_data5, align 4, !tbaa !123
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE10deallocateEPS2_(%class.btAlignedAllocator.94* %this, %class.btHashKey* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  %ptr.addr = alloca %class.btHashKey*, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %ptr, %class.btHashKey** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  %0 = load %class.btHashKey*, %class.btHashKey** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btHashKey* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE5clearEv(%class.btAlignedObjectArray.89* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE7destroyEii(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE10deallocateEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE7destroyEii(%class.btAlignedObjectArray.89* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !32
  store i32 %last, i32* %last.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %last.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %4 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !120
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE10deallocateEv(%class.btAlignedObjectArray.89* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %tobool = icmp ne %struct.btTriIndex* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !118, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data4, align 4, !tbaa !119
  call void @_ZN18btAlignedAllocatorI10btTriIndexLj16EE10deallocateEPS0_(%class.btAlignedAllocator.90* %m_allocator, %struct.btTriIndex* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %struct.btTriIndex* null, %struct.btTriIndex** %m_data5, align 4, !tbaa !119
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI10btTriIndexLj16EE10deallocateEPS0_(%class.btAlignedAllocator.90* %this, %struct.btTriIndex* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %ptr.addr = alloca %struct.btTriIndex*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  store %struct.btTriIndex* %ptr, %struct.btTriIndex** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load %struct.btTriIndex*, %struct.btTriIndex** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btTriIndex* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.85* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.85* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.85* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !32
  store i32 %last, i32* %last.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %last.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !115
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !116
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.85* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !115
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !114, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !115
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.86* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !115
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.86* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !87
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !87
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !87
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !32
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !87
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !87
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !87
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !87
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !87
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !87
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !87
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !87
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #4 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !87
  %0 = load float, float* %y.addr, align 4, !tbaa !87
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !87
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !87
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !87
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !87
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !87
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !87
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !87
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !87
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !87
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !87
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !87
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !87
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !87
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !87
  ret void
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !87
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !87
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !87
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !87
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !87
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !87
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !87
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !87
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #5 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !105
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !105
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !105
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !87
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !87
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !87
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !87
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !87
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !87
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !87
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !87
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !87
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !87
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !87
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !87
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !87
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !87
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !87
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !87
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !87
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !87
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !87
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !87
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !87
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !87
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !87
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !87
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !87
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !87
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !87
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !105
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !105
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !105
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #5 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !87
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !87
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !87
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !87
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !87
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !87
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !87
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !87
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !87
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !87
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !87
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !87
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !87
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !87
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !87
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !87
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !87
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !87
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !32
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #4 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !87
  %0 = load float, float* %x.addr, align 4, !tbaa !87
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #4 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !32
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !32
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4, !tbaa !32
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !126
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !87
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !87
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !87
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !87
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #4 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !87
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !87
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !87
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !87
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define internal void @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallbackD0Ev(%struct.LocalTriangleSphereCastCallback* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %struct.LocalTriangleSphereCastCallback*, align 4
  store %struct.LocalTriangleSphereCastCallback* %this, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.LocalTriangleSphereCastCallback*, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4
  %call = call %struct.LocalTriangleSphereCastCallback* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %struct.LocalTriangleSphereCastCallback* (%struct.LocalTriangleSphereCastCallback*)*)(%struct.LocalTriangleSphereCastCallback* %this1) #9
  %0 = bitcast %struct.LocalTriangleSphereCastCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define internal void @_ZZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultEN31LocalTriangleSphereCastCallback15processTriangleEP9btVector3ii(%struct.LocalTriangleSphereCastCallback* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 {
entry:
  %this.addr = alloca %struct.LocalTriangleSphereCastCallback*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %ident = alloca %class.btTransform, align 4
  %castResult = alloca %"struct.btConvexCast::CastResult", align 4
  %pointShape = alloca %class.btSphereShape, align 4
  %triShape = alloca %class.btTriangleShape, align 4
  %simplexSolver = alloca %class.btVoronoiSimplexSolver, align 4
  %convexCaster = alloca %class.btSubsimplexConvexCast, align 4
  store %struct.LocalTriangleSphereCastCallback* %this, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !32
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !32
  %this1 = load %struct.LocalTriangleSphereCastCallback*, %struct.LocalTriangleSphereCastCallback** %this.addr, align 4
  %0 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %0) #9
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %1 = bitcast %"struct.btConvexCast::CastResult"* %castResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 176, i8* %1) #9
  %call2 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultC2Ev(%"struct.btConvexCast::CastResult"* %castResult)
  %m_hitFraction = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 5
  %2 = load float, float* %m_hitFraction, align 4, !tbaa !108
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %castResult, i32 0, i32 5
  store float %2, float* %m_fraction, align 4, !tbaa !127
  %3 = bitcast %class.btSphereShape* %pointShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 52, i8* %3) #9
  %m_ccdSphereRadius = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 4
  %4 = load float, float* %m_ccdSphereRadius, align 4, !tbaa !112
  %call3 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %pointShape, float %4)
  %5 = bitcast %class.btTriangleShape* %triShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0
  %7 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 1
  %8 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 2
  %call6 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %triShape, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %9 = bitcast %class.btVoronoiSimplexSolver* %simplexSolver to i8*
  call void @llvm.lifetime.start.p0i8(i64 360, i8* %9) #9
  %call7 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* %simplexSolver)
  %10 = bitcast %class.btSubsimplexConvexCast* %convexCaster to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = bitcast %class.btSphereShape* %pointShape to %class.btConvexShape*
  %12 = bitcast %class.btTriangleShape* %triShape to %class.btConvexShape*
  %call8 = call %class.btSubsimplexConvexCast* @_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btSubsimplexConvexCast* %convexCaster, %class.btConvexShape* %11, %class.btConvexShape* %12, %class.btVoronoiSimplexSolver* %simplexSolver)
  %m_ccdSphereFromTrans = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 1
  %m_ccdSphereToTrans = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 2
  %call9 = call zeroext i1 @_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btSubsimplexConvexCast* %convexCaster, %class.btTransform* nonnull align 4 dereferenceable(64) %m_ccdSphereFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %m_ccdSphereToTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %castResult)
  br i1 %call9, label %if.then, label %if.end15

if.then:                                          ; preds = %entry
  %m_hitFraction10 = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 5
  %13 = load float, float* %m_hitFraction10, align 4, !tbaa !108
  %m_fraction11 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %castResult, i32 0, i32 5
  %14 = load float, float* %m_fraction11, align 4, !tbaa !127
  %cmp = fcmp ogt float %13, %14
  br i1 %cmp, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.then
  %m_fraction13 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %castResult, i32 0, i32 5
  %15 = load float, float* %m_fraction13, align 4, !tbaa !127
  %m_hitFraction14 = getelementptr inbounds %struct.LocalTriangleSphereCastCallback, %struct.LocalTriangleSphereCastCallback* %this1, i32 0, i32 5
  store float %15, float* %m_hitFraction14, align 4, !tbaa !108
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.then
  br label %if.end15

if.end15:                                         ; preds = %if.end, %entry
  %call16 = call %class.btSubsimplexConvexCast* bitcast (%class.btConvexCast* (%class.btConvexCast*)* @_ZN12btConvexCastD2Ev to %class.btSubsimplexConvexCast* (%class.btSubsimplexConvexCast*)*)(%class.btSubsimplexConvexCast* %convexCaster) #9
  %16 = bitcast %class.btSubsimplexConvexCast* %convexCaster to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #9
  %17 = bitcast %class.btVoronoiSimplexSolver* %simplexSolver to i8*
  call void @llvm.lifetime.end.p0i8(i64 360, i8* %17) #9
  %call17 = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %triShape) #9
  %18 = bitcast %class.btTriangleShape* %triShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %18) #9
  %call18 = call %class.btSphereShape* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btSphereShape* (%class.btSphereShape*)*)(%class.btSphereShape* %pointShape) #9
  %19 = bitcast %class.btSphereShape* %pointShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 52, i8* %19) #9
  %call19 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* %castResult) #9
  %20 = bitcast %"struct.btConvexCast::CastResult"* %castResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 176, i8* %20) #9
  %21 = bitcast %class.btTransform* %ident to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %21) #9
  ret void
}

define linkonce_odr hidden %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultC2Ev(%"struct.btConvexCast::CastResult"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexCast::CastResult"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN12btConvexCast10CastResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_hitTransformA = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_hitTransformA)
  %m_hitTransformB = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_hitTransformB)
  %m_normal = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normal)
  %m_hitPoint = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPoint)
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 5
  store float 0x43ABC16D60000000, float* %m_fraction, align 4, !tbaa !127
  %m_debugDrawer = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 6
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !129
  %m_allowedPenetration = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %m_allowedPenetration, align 4, !tbaa !130
  ret %"struct.btConvexCast::CastResult"* %this1
}

define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !87
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4, !tbaa !126
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4, !tbaa !87
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4, !tbaa !87
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4, !tbaa !131
  ret %class.btSphereShape* %this1
}

define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btTriangleShape*, align 4
  %this.addr = alloca %class.btTriangleShape*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store %class.btTriangleShape* %this1, %class.btTriangleShape** %retval, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btTriangleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV15btTriangleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btTriangleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 1, i32* %m_shapeType, align 4, !tbaa !126
  %3 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4, !tbaa !2
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !105
  %6 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !105
  %9 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !105
  %12 = load %class.btTriangleShape*, %class.btTriangleShape** %retval, align 4
  ret %class.btTriangleShape* %12
}

define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btVoronoiSimplexSolver*, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %this1, %class.btVoronoiSimplexSolver** %retval, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 5
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 5
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %array.begin10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %arrayctor.end11 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin10, i32 5
  br label %arrayctor.loop12

arrayctor.loop12:                                 ; preds = %arrayctor.loop12, %arrayctor.cont9
  %arrayctor.cur13 = phi %class.btVector3* [ %array.begin10, %arrayctor.cont9 ], [ %arrayctor.next15, %arrayctor.loop12 ]
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur13)
  %arrayctor.next15 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur13, i32 1
  %arrayctor.done16 = icmp eq %class.btVector3* %arrayctor.next15, %arrayctor.end11
  br i1 %arrayctor.done16, label %arrayctor.cont17, label %arrayctor.loop12

arrayctor.cont17:                                 ; preds = %arrayctor.loop12
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP1)
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP2)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedV)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lastW)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  store float 0x3F1A36E2E0000000, float* %m_equalVertexThreshold, align 4, !tbaa !133
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call22 = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %retval, align 4
  ret %class.btVoronoiSimplexSolver* %0
}

declare %class.btSubsimplexConvexCast* @_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btSubsimplexConvexCast* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*) unnamed_addr #1

declare zeroext i1 @_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btSubsimplexConvexCast*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176)) unnamed_addr #1

; Function Attrs: nounwind
declare %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned) unnamed_addr #7

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #7

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret %"struct.btConvexCast::CastResult"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult9DebugDrawEf(%"struct.btConvexCast::CastResult"* %this, float %fraction) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %fraction.addr = alloca float, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  store float %fraction, float* %fraction.addr, align 4, !tbaa !87
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform(%"struct.btConvexCast::CastResult"* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult13reportFailureEii(%"struct.btConvexCast::CastResult"* %this, i32 %errNo, i32 %numIterations) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %errNo.addr = alloca i32, align 4
  %numIterations.addr = alloca i32, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  store i32 %errNo, i32* %errNo.addr, align 4, !tbaa !32
  store i32 %numIterations, i32* %numIterations.addr, align 4, !tbaa !32
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12btConvexCast10CastResultD0Ev(%"struct.btConvexCast::CastResult"* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %call = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* %this1) #9
  %0 = bitcast %"struct.btConvexCast::CastResult"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_x, float* %_x.addr, align 4, !tbaa !87
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4, !tbaa !87
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !87
  ret void
}

declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapeD0Ev(%class.btTriangleShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %this1) #9
  %0 = bitcast %class.btTriangleShape* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #9
  ret void
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexInternalShape* %0 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #1

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #1

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !87
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !87
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !87
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btTriangleShape7getNameEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !87
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !87
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !87
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !87
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !131
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !131
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !131
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !138
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #1

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

define linkonce_odr hidden void @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %dir) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 %call
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  %4 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #9
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #1

define linkonce_odr hidden void @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btTriangleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dir = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !32
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !32
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  store %class.btVector3* %arrayidx, %class.btVector3** %dir, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %8 = load %class.btVector3*, %class.btVector3** %dir, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 1
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 %call
  %9 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx9 to i8*
  %12 = bitcast %class.btVector3* %arrayidx8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !105
  %13 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  %14 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 2
}

define linkonce_odr hidden void @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !32
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i32, i32* %index.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !87
  %3 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %3, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape14getNumVerticesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape11getNumEdgesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !32
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !32
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %2 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %3 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = load i32, i32* %i.addr, align 4, !tbaa !32
  %add = add nsw i32 %4, 1
  %rem = srem i32 %add, 3
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %6 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable2 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %6, align 4, !tbaa !8
  %vfn3 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable2, i64 27
  %7 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn3, align 4
  call void %7(%class.btTriangleShape* %this1, i32 %rem, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape9getVertexEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %vert) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %vert.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !32
  store %class.btVector3* %vert, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape12getNumPlanesEv(%class.btTriangleShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 1
}

define linkonce_odr hidden void @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport, i32 %i) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !32
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !32
  %1 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %3 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 31
  %4 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK15btTriangleShape8isInsideERK9btVector3f(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, float %tolerance) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShape*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  %tolerance.addr = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %planeconst = alloca float, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %edgeNormal = alloca %class.btVector3, align 4
  %dist9 = alloca float, align 4
  %edgeConst = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4, !tbaa !2
  store float %tolerance, float* %tolerance.addr, align 4, !tbaa !87
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %1 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call2, float* %dist, align 4, !tbaa !87
  %3 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call3, float* %planeconst, align 4, !tbaa !87
  %4 = load float, float* %planeconst, align 4, !tbaa !87
  %5 = load float, float* %dist, align 4, !tbaa !87
  %sub = fsub float %5, %4
  store float %sub, float* %dist, align 4, !tbaa !87
  %6 = load float, float* %dist, align 4, !tbaa !87
  %7 = load float, float* %tolerance.addr, align 4, !tbaa !87
  %fneg = fneg float %7
  %cmp = fcmp oge float %6, %fneg
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %dist, align 4, !tbaa !87
  %9 = load float, float* %tolerance.addr, align 4, !tbaa !87
  %cmp4 = fcmp ole float %8, %9
  br i1 %cmp4, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !32
  %cmp5 = icmp slt i32 %11, 3
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #9
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %13 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %14 = load i32, i32* %i, align 4, !tbaa !32
  %15 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %15, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 26
  %16 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %16(%class.btTriangleShape* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  %17 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %pa)
  %18 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #9
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edgeNormal, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edgeNormal)
  %19 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call10, float* %dist9, align 4, !tbaa !87
  %21 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call11, float* %edgeConst, align 4, !tbaa !87
  %22 = load float, float* %edgeConst, align 4, !tbaa !87
  %23 = load float, float* %dist9, align 4, !tbaa !87
  %sub12 = fsub float %23, %22
  store float %sub12, float* %dist9, align 4, !tbaa !87
  %24 = load float, float* %dist9, align 4, !tbaa !87
  %25 = load float, float* %tolerance.addr, align 4, !tbaa !87
  %fneg13 = fneg float %25
  %cmp14 = fcmp olt float %24, %fneg13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then15
  %26 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  %29 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  %30 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  %31 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %32 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup21

cleanup21:                                        ; preds = %for.end, %cleanup
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %cleanup23

if.end22:                                         ; preds = %land.lhs.true, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup21
  %34 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #9
  %37 = load i1, i1* %retval, align 1
  ret i1 %37
}

define linkonce_odr hidden void @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !32
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !105
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapedlEPv(i8* %ptr) #4 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !32
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !87
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !87
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !87
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !87
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !87
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !87
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !87
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !87
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

define linkonce_odr hidden void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %2 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !105
  %6 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %9)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btTriIndex* @_ZN20btAlignedObjectArrayI10btTriIndexEixEi(%class.btAlignedObjectArray.89* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %0, i32 %1
  ret %struct.btTriIndex* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.16* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !142
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !145
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %0, i32 %1
  ret %"struct.btSparseSdf<3>::Cell"** %arrayidx
}

define linkonce_odr hidden %struct.btTriIndex* @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E4findERKS2_(%class.btHashMap* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca %struct.btTriIndex*, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashKey*, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %key, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_(%class.btHashMap* %this1, %class.btHashKey* nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %index, align 4, !tbaa !32
  %2 = load i32, i32* %index, align 4, !tbaa !32
  %cmp = icmp eq i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btTriIndex* null, %struct.btTriIndex** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %3 = load i32, i32* %index, align 4, !tbaa !32
  %call2 = call nonnull align 4 dereferenceable(8) %struct.btTriIndex* @_ZN20btAlignedObjectArrayI10btTriIndexEixEi(%class.btAlignedObjectArray.89* %m_valueArray, i32 %3)
  store %struct.btTriIndex* %call2, %struct.btTriIndex** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btTriIndex*, %struct.btTriIndex** %retval, align 4
  ret %struct.btTriIndex* %5
}

define linkonce_odr hidden i32 @_ZNK9btHashMapI9btHashKeyI10btTriIndexES1_E9findIndexERKS2_(%class.btHashMap* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashKey*, align 4
  %hash = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %key, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashKeyI10btTriIndexE7getHashEv(%class.btHashKey* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !32
  %2 = load i32, i32* %hash, align 4, !tbaa !32
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %m_hashTable)
  %cmp = icmp uge i32 %2, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_hashTable4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %hash, align 4, !tbaa !32
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable4, i32 %4)
  %5 = load i32, i32* %call5, align 4, !tbaa !32
  store i32 %5, i32* %index, align 4, !tbaa !32
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %6 = load i32, i32* %index, align 4, !tbaa !32
  %cmp6 = icmp ne i32 %6, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %7 = load %class.btHashKey*, %class.btHashKey** %key.addr, align 4, !tbaa !2
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %8 = load i32, i32* %index, align 4, !tbaa !32
  %call7 = call nonnull align 4 dereferenceable(4) %class.btHashKey* @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi(%class.btAlignedObjectArray.93* %m_keyArray, i32 %8)
  %call8 = call zeroext i1 @_ZNK9btHashKeyI10btTriIndexE6equalsERKS1_(%class.btHashKey* %7, %class.btHashKey* nonnull align 4 dereferenceable(4) %call7)
  %conv = zext i1 %call8 to i32
  %cmp9 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %9 = phi i1 [ false, %while.cond ], [ %cmp9, %land.rhs ]
  br i1 %9, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %10 = load i32, i32* %index, align 4, !tbaa !32
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_next, i32 %10)
  %11 = load i32, i32* %call10, align 4, !tbaa !32
  store i32 %11, i32* %index, align 4, !tbaa !32
  br label %while.cond

while.end:                                        ; preds = %land.end
  %12 = load i32, i32* %index, align 4, !tbaa !32
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %14 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btHashKeyI10btTriIndexE7getHashEv(%class.btHashKey* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btHashKey*, align 4
  %key = alloca i32, align 4
  store %class.btHashKey* %this, %class.btHashKey** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashKey*, %class.btHashKey** %this.addr, align 4
  %0 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_uid = getelementptr inbounds %class.btHashKey, %class.btHashKey* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_uid, align 4, !tbaa !91
  store i32 %1, i32* %key, align 4, !tbaa !32
  %2 = load i32, i32* %key, align 4, !tbaa !32
  %shl = shl i32 %2, 15
  %neg = xor i32 %shl, -1
  %3 = load i32, i32* %key, align 4, !tbaa !32
  %add = add nsw i32 %3, %neg
  store i32 %add, i32* %key, align 4, !tbaa !32
  %4 = load i32, i32* %key, align 4, !tbaa !32
  %shr = ashr i32 %4, 10
  %5 = load i32, i32* %key, align 4, !tbaa !32
  %xor = xor i32 %5, %shr
  store i32 %xor, i32* %key, align 4, !tbaa !32
  %6 = load i32, i32* %key, align 4, !tbaa !32
  %shl2 = shl i32 %6, 3
  %7 = load i32, i32* %key, align 4, !tbaa !32
  %add3 = add nsw i32 %7, %shl2
  store i32 %add3, i32* %key, align 4, !tbaa !32
  %8 = load i32, i32* %key, align 4, !tbaa !32
  %shr4 = ashr i32 %8, 6
  %9 = load i32, i32* %key, align 4, !tbaa !32
  %xor5 = xor i32 %9, %shr4
  store i32 %xor5, i32* %key, align 4, !tbaa !32
  %10 = load i32, i32* %key, align 4, !tbaa !32
  %shl6 = shl i32 %10, 11
  %neg7 = xor i32 %shl6, -1
  %11 = load i32, i32* %key, align 4, !tbaa !32
  %add8 = add nsw i32 %11, %neg7
  store i32 %add8, i32* %key, align 4, !tbaa !32
  %12 = load i32, i32* %key, align 4, !tbaa !32
  %shr9 = ashr i32 %12, 16
  %13 = load i32, i32* %key, align 4, !tbaa !32
  %xor10 = xor i32 %13, %shr9
  store i32 %xor10, i32* %key, align 4, !tbaa !32
  %14 = load i32, i32* %key, align 4, !tbaa !32
  %15 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  ret i32 %14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !121
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !115
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

define linkonce_odr hidden zeroext i1 @_ZNK9btHashKeyI10btTriIndexE6equalsERKS1_(%class.btHashKey* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %other) #0 comdat {
entry:
  %this.addr = alloca %class.btHashKey*, align 4
  %other.addr = alloca %class.btHashKey*, align 4
  store %class.btHashKey* %this, %class.btHashKey** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %other, %class.btHashKey** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btHashKey*, %class.btHashKey** %this.addr, align 4
  %call = call i32 @_ZNK9btHashKeyI10btTriIndexE7getUid1Ev(%class.btHashKey* %this1)
  %0 = load %class.btHashKey*, %class.btHashKey** %other.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK9btHashKeyI10btTriIndexE7getUid1Ev(%class.btHashKey* %0)
  %cmp = icmp eq i32 %call, %call2
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashKey* @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi(%class.btAlignedObjectArray.93* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btHashKey, %class.btHashKey* %0, i32 %1
  ret %class.btHashKey* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK9btHashKeyI10btTriIndexE7getUid1Ev(%class.btHashKey* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btHashKey*, align 4
  store %class.btHashKey* %this, %class.btHashKey** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashKey*, %class.btHashKey** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashKey, %class.btHashKey* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_uid, align 4, !tbaa !91
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE9push_backERKS0_(%class.btAlignedObjectArray.89* %this, %struct.btTriIndex* nonnull align 4 dereferenceable(8) %_Val) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %_Val.addr = alloca %struct.btTriIndex*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store %struct.btTriIndex* %_Val, %struct.btTriIndex** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !32
  %1 = load i32, i32* %sz, align 4, !tbaa !32
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI10btTriIndexE9allocSizeEi(%class.btAlignedObjectArray.89* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE7reserveEi(%class.btAlignedObjectArray.89* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !120
  %arrayidx = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %2, i32 %3
  %4 = bitcast %struct.btTriIndex* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btTriIndex*
  %6 = load %struct.btTriIndex*, %struct.btTriIndex** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btTriIndex* %5 to i8*
  %8 = bitcast %struct.btTriIndex* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false), !tbaa.struct !103
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !120
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !120
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9push_backERKS2_(%class.btAlignedObjectArray.93* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %_Val) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %_Val.addr = alloca %class.btHashKey*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %_Val, %class.btHashKey** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !32
  %1 = load i32, i32* %sz, align 4, !tbaa !32
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8capacityEv(%class.btAlignedObjectArray.93* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9allocSizeEi(%class.btAlignedObjectArray.93* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7reserveEi(%class.btAlignedObjectArray.93* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %2 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !124
  %arrayidx = getelementptr inbounds %class.btHashKey, %class.btHashKey* %2, i32 %3
  %4 = bitcast %class.btHashKey* %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btHashKey*
  %6 = load %class.btHashKey*, %class.btHashKey** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btHashKey* %5 to i8*
  %8 = bitcast %class.btHashKey* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false), !tbaa.struct !146
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !124
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !124
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

define linkonce_odr hidden void @_ZN9btHashMapI9btHashKeyI10btTriIndexES1_E10growTablesERKS2_(%class.btHashMap* %this, %class.btHashKey* nonnull align 4 dereferenceable(4) %0) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %.addr = alloca %class.btHashKey*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashKey* %0, %class.btHashKey** %.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %1 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray)
  store i32 %call, i32* %newCapacity, align 4, !tbaa !32
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %m_hashTable)
  %2 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %cmp = icmp slt i32 %call2, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_hashTable3 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4, !tbaa !32
  %m_hashTable5 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %5 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store i32 0, i32* %ref.tmp, align 4, !tbaa !32
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.85* %m_hashTable5, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %6 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %7 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %8 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store i32 0, i32* %ref.tmp6, align 4, !tbaa !32
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.85* %m_next, i32 %7, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !32
  %12 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %cmp7 = icmp slt i32 %11, %12
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %13 = load i32, i32* %i, align 4, !tbaa !32
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable8, i32 %13)
  store i32 -1, i32* %call9, align 4, !tbaa !32
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %15 = load i32, i32* %i, align 4, !tbaa !32
  %16 = load i32, i32* %newCapacity, align 4, !tbaa !32
  %cmp11 = icmp slt i32 %15, %16
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4, !tbaa !32
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_next13, i32 %17)
  store i32 -1, i32* %call14, align 4, !tbaa !32
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %18 = load i32, i32* %i, align 4, !tbaa !32
  %inc16 = add nsw i32 %18, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !32
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !32
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc31, %for.end17
  %19 = load i32, i32* %i, align 4, !tbaa !32
  %20 = load i32, i32* %curHashtableSize, align 4, !tbaa !32
  %cmp19 = icmp slt i32 %19, %20
  br i1 %cmp19, label %for.body20, label %for.end33

for.body20:                                       ; preds = %for.cond18
  %21 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %22 = load i32, i32* %i, align 4, !tbaa !32
  %call21 = call nonnull align 4 dereferenceable(4) %class.btHashKey* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi(%class.btAlignedObjectArray.93* %m_keyArray, i32 %22)
  %call22 = call i32 @_ZNK9btHashKeyI10btTriIndexE7getHashEv(%class.btHashKey* %call21)
  %m_valueArray23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %m_valueArray23)
  %sub = sub nsw i32 %call24, 1
  %and = and i32 %call22, %sub
  store i32 %and, i32* %hashValue, align 4, !tbaa !32
  %m_hashTable25 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %23 = load i32, i32* %hashValue, align 4, !tbaa !32
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable25, i32 %23)
  %24 = load i32, i32* %call26, align 4, !tbaa !32
  %m_next27 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %25 = load i32, i32* %i, align 4, !tbaa !32
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_next27, i32 %25)
  store i32 %24, i32* %call28, align 4, !tbaa !32
  %26 = load i32, i32* %i, align 4, !tbaa !32
  %m_hashTable29 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %27 = load i32, i32* %hashValue, align 4, !tbaa !32
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %m_hashTable29, i32 %27)
  store i32 %26, i32* %call30, align 4, !tbaa !32
  %28 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  br label %for.inc31

for.inc31:                                        ; preds = %for.body20
  %29 = load i32, i32* %i, align 4, !tbaa !32
  %inc32 = add nsw i32 %29, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !32
  br label %for.cond18

for.end33:                                        ; preds = %for.cond18
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  br label %if.end

if.end:                                           ; preds = %for.end33, %entry
  %32 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.85* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !115
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI10btTriIndexE7reserveEi(%class.btAlignedObjectArray.89* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btTriIndex*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE8capacityEv(%class.btAlignedObjectArray.89* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btTriIndex** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %call2 = call i8* @_ZN20btAlignedObjectArrayI10btTriIndexE8allocateEi(%class.btAlignedObjectArray.89* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btTriIndex*
  store %struct.btTriIndex* %3, %struct.btTriIndex** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  %4 = load %struct.btTriIndex*, %struct.btTriIndex** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI10btTriIndexE4copyEiiPS0_(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call3, %struct.btTriIndex* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI10btTriIndexE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE7destroyEii(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI10btTriIndexE10deallocateEv(%class.btAlignedObjectArray.89* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !118
  %5 = load %struct.btTriIndex*, %struct.btTriIndex** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store %struct.btTriIndex* %5, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !121
  %7 = bitcast %struct.btTriIndex** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI10btTriIndexE9allocSizeEi(%class.btAlignedObjectArray.89* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI10btTriIndexE8allocateEi(%class.btAlignedObjectArray.89* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %call = call %struct.btTriIndex* @_ZN18btAlignedAllocatorI10btTriIndexLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.90* %m_allocator, i32 %1, %struct.btTriIndex** null)
  %2 = bitcast %struct.btTriIndex* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI10btTriIndexE4copyEiiPS0_(%class.btAlignedObjectArray.89* %this, i32 %start, i32 %end, %struct.btTriIndex* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btTriIndex*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !32
  store i32 %end, i32* %end.addr, align 4, !tbaa !32
  store %struct.btTriIndex* %dest, %struct.btTriIndex** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %end.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btTriIndex*, %struct.btTriIndex** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %4, i32 %5
  %6 = bitcast %struct.btTriIndex* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btTriIndex*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %8 = load %struct.btTriIndex*, %struct.btTriIndex** %m_data, align 4, !tbaa !119
  %9 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds %struct.btTriIndex, %struct.btTriIndex* %8, i32 %9
  %10 = bitcast %struct.btTriIndex* %7 to i8*
  %11 = bitcast %struct.btTriIndex* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 8, i1 false), !tbaa.struct !103
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

define linkonce_odr hidden %struct.btTriIndex* @_ZN18btAlignedAllocatorI10btTriIndexLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.90* %this, i32 %n, %struct.btTriIndex** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btTriIndex**, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  store %struct.btTriIndex** %hint, %struct.btTriIndex*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !32
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btTriIndex*
  ret %struct.btTriIndex* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8capacityEv(%class.btAlignedObjectArray.93* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !125
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7reserveEi(%class.btAlignedObjectArray.93* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btHashKey*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8capacityEv(%class.btAlignedObjectArray.93* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btHashKey** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8allocateEi(%class.btAlignedObjectArray.93* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btHashKey*
  store %class.btHashKey* %3, %class.btHashKey** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  %4 = load %class.btHashKey*, %class.btHashKey** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4copyEiiPS2_(%class.btAlignedObjectArray.93* %this1, i32 0, i32 %call3, %class.btHashKey* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE7destroyEii(%class.btAlignedObjectArray.93* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE10deallocateEv(%class.btAlignedObjectArray.93* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !122
  %5 = load %class.btHashKey*, %class.btHashKey** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store %class.btHashKey* %5, %class.btHashKey** %m_data, align 4, !tbaa !123
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !125
  %7 = bitcast %class.btHashKey** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE9allocSizeEi(%class.btAlignedObjectArray.93* %this, i32 %size) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE8allocateEi(%class.btAlignedObjectArray.93* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %call = call %class.btHashKey* @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.94* %m_allocator, i32 %1, %class.btHashKey** null)
  %2 = bitcast %class.btHashKey* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE4copyEiiPS2_(%class.btAlignedObjectArray.93* %this, i32 %start, i32 %end, %class.btHashKey* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btHashKey*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !32
  store i32 %end, i32* %end.addr, align 4, !tbaa !32
  store %class.btHashKey* %dest, %class.btHashKey** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %end.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btHashKey*, %class.btHashKey** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btHashKey, %class.btHashKey* %4, i32 %5
  %6 = bitcast %class.btHashKey* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btHashKey*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %8 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %9 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds %class.btHashKey, %class.btHashKey* %8, i32 %9
  %10 = bitcast %class.btHashKey* %7 to i8*
  %11 = bitcast %class.btHashKey* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false), !tbaa.struct !146
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

define linkonce_odr hidden %class.btHashKey* @_ZN18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.94* %this, i32 %n, %class.btHashKey** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btHashKey**, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  store %class.btHashKey** %hint, %class.btHashKey*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !32
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btHashKey*
  ret %class.btHashKey* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.85* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !32
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !32
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  %2 = load i32, i32* %curSize, align 4, !tbaa !32
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  store i32 %4, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %6 = load i32, i32* %curSize, align 4, !tbaa !32
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !115
  %9 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.85* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !32
  store i32 %14, i32* %i6, align 4, !tbaa !32
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !32
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !115
  %19 = load i32, i32* %i6, align 4, !tbaa !32
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !32
  store i32 %23, i32* %21, align 4, !tbaa !32
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !32
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !32
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !32
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !116
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashKey* @_ZN20btAlignedObjectArrayI9btHashKeyI10btTriIndexEEixEi(%class.btAlignedObjectArray.93* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load %class.btHashKey*, %class.btHashKey** %m_data, align 4, !tbaa !123
  %1 = load i32, i32* %n.addr, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds %class.btHashKey, %class.btHashKey* %0, i32 %1
  ret %class.btHashKey* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.85* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.85* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.85* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.85* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.85* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.85* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !114
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !115
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !32
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !117
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.85* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !117
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.85* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !32
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !32
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !32
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.86* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.85* %this, i32 %start, i32 %end, i32* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !32
  store i32 %end, i32* %end.addr, align 4, !tbaa !32
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !32
  store i32 %1, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !32
  %3 = load i32, i32* %end.addr, align 4, !tbaa !32
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !115
  %9 = load i32, i32* %i, align 4, !tbaa !32
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !32
  store i32 %10, i32* %7, align 4, !tbaa !32
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !32
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !32
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.86* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !32
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !32
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !7, i64 8}
!12 = !{!"_ZTS35btSoftBodyConcaveCollisionAlgorithm", !7, i64 8, !13, i64 12}
!13 = !{!"_ZTS26btSoftBodyTriangleCallback", !3, i64 4, !3, i64 8, !14, i64 12, !14, i64 28, !3, i64 44, !3, i64 48, !3, i64 52, !15, i64 56, !16, i64 60, !19, i64 140}
!14 = !{!"_ZTS9btVector3", !4, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{!"_ZTS9btHashMapI9btHashKeyI10btTriIndexES1_E", !17, i64 0, !17, i64 20, !20, i64 40, !22, i64 60}
!17 = !{!"_ZTS20btAlignedObjectArrayIiE", !18, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!19 = !{!"int", !4, i64 0}
!20 = !{!"_ZTS20btAlignedObjectArrayI10btTriIndexE", !21, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!21 = !{!"_ZTS18btAlignedAllocatorI10btTriIndexLj16EE"}
!22 = !{!"_ZTS20btAlignedObjectArrayI9btHashKeyI10btTriIndexEE", !23, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!23 = !{!"_ZTS18btAlignedAllocatorI9btHashKeyI10btTriIndexELj16EE"}
!24 = !{!25, !3, i64 0}
!25 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!26 = !{!13, !3, i64 48}
!27 = !{!13, !3, i64 52}
!28 = !{!13, !3, i64 4}
!29 = !{!13, !3, i64 8}
!30 = !{!31, !3, i64 8}
!31 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !19, i64 16, !19, i64 20}
!32 = !{!19, !19, i64 0}
!33 = !{!34, !3, i64 4}
!34 = !{!"_ZTS10btTriIndex", !19, i64 0, !3, i64 4}
!35 = !{!36, !3, i64 684}
!36 = !{!"_ZTS10btSoftBody", !37, i64 264, !3, i64 284, !39, i64 288, !45, i64 452, !46, i64 472, !3, i64 680, !3, i64 684, !52, i64 688, !54, i64 708, !56, i64 728, !58, i64 748, !60, i64 768, !62, i64 788, !64, i64 808, !66, i64 828, !68, i64 848, !70, i64 868, !15, i64 888, !4, i64 892, !7, i64 924, !72, i64 928, !72, i64 988, !72, i64 1048, !77, i64 1108, !79, i64 1128, !81, i64 1148, !14, i64 1212, !15, i64 1228, !17, i64 1232}
!37 = !{!"_ZTS20btAlignedObjectArrayIPK17btCollisionObjectE", !38, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorIPK17btCollisionObjectLj16EE"}
!39 = !{!"_ZTSN10btSoftBody6ConfigE", !40, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28, !15, i64 32, !15, i64 36, !15, i64 40, !15, i64 44, !15, i64 48, !15, i64 52, !15, i64 56, !15, i64 60, !15, i64 64, !15, i64 68, !15, i64 72, !15, i64 76, !15, i64 80, !19, i64 84, !19, i64 88, !19, i64 92, !19, i64 96, !19, i64 100, !41, i64 104, !43, i64 124, !43, i64 144}
!40 = !{!"_ZTSN10btSoftBody10eAeroModel1_E", !4, i64 0}
!41 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8eVSolver1_EE", !42, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!42 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8eVSolver1_ELj16EE"}
!43 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8ePSolver1_EE", !44, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!44 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8ePSolver1_ELj16EE"}
!45 = !{!"_ZTSN10btSoftBody11SolverStateE", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16}
!46 = !{!"_ZTSN10btSoftBody4PoseE", !7, i64 0, !7, i64 1, !15, i64 4, !47, i64 8, !49, i64 28, !14, i64 48, !51, i64 64, !51, i64 112, !51, i64 160}
!47 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !48, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!48 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!49 = !{!"_ZTS20btAlignedObjectArrayIfE", !50, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!50 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!51 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!52 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NoteEE", !53, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!53 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NoteELj16EE"}
!54 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !55, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!55 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!56 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4LinkEE", !57, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!57 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4LinkELj16EE"}
!58 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4FaceEE", !59, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!59 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4FaceELj16EE"}
!60 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody5TetraEE", !61, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!61 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody5TetraELj16EE"}
!62 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody6AnchorEE", !63, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!63 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody6AnchorELj16EE"}
!64 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8RContactEE", !65, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!65 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8RContactELj16EE"}
!66 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8SContactEE", !67, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!67 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8SContactELj16EE"}
!68 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody5JointEE", !69, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!69 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody5JointELj16EE"}
!70 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody8MaterialEE", !71, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!71 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody8MaterialELj16EE"}
!72 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !19, i64 8, !19, i64 12, !19, i64 16, !73, i64 20, !75, i64 40}
!73 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !74, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!74 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!75 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !76, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!76 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!77 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody7ClusterEE", !78, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!78 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody7ClusterELj16EE"}
!79 = !{!"_ZTS20btAlignedObjectArrayIbE", !80, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!80 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!81 = !{!"_ZTS11btTransform", !51, i64 0, !14, i64 48}
!82 = !{!83, !3, i64 280}
!83 = !{!"_ZTSN11btSparseSdfILi3EE4CellE", !4, i64 0, !4, i64 256, !19, i64 268, !19, i64 272, !3, i64 276, !3, i64 280}
!84 = !{!83, !3, i64 276}
!85 = !{!86, !3, i64 20}
!86 = !{!"_ZTS16btDispatcherInfo", !15, i64 0, !19, i64 4, !19, i64 8, !15, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !7, i64 25, !7, i64 26, !15, i64 28, !7, i64 32, !15, i64 36}
!87 = !{!15, !15, i64 0}
!88 = !{!13, !3, i64 44}
!89 = !{!25, !3, i64 4}
!90 = !{!34, !19, i64 0}
!91 = !{!92, !19, i64 0}
!92 = !{!"_ZTS9btHashKeyI10btTriIndexE", !19, i64 0}
!93 = !{!94, !3, i64 8}
!94 = !{!"_ZTS16btCollisionShape", !19, i64 4, !3, i64 8}
!95 = !{!96, !3, i64 192}
!96 = !{!"_ZTS17btCollisionObject", !81, i64 4, !81, i64 68, !14, i64 132, !14, i64 148, !14, i64 164, !19, i64 180, !15, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !19, i64 204, !19, i64 208, !19, i64 212, !19, i64 216, !15, i64 220, !15, i64 224, !15, i64 228, !15, i64 232, !19, i64 236, !4, i64 240, !15, i64 244, !15, i64 248, !15, i64 252, !19, i64 256, !19, i64 260}
!97 = !{!31, !3, i64 0}
!98 = !{!31, !3, i64 4}
!99 = !{!31, !19, i64 16}
!100 = !{!31, !19, i64 20}
!101 = !{!102, !102, i64 0}
!102 = !{!"long", !4, i64 0}
!103 = !{i64 0, i64 4, !32, i64 4, i64 4, !2}
!104 = !{!13, !15, i64 56}
!105 = !{i64 0, i64 16, !106}
!106 = !{!4, !4, i64 0}
!107 = !{!31, !3, i64 12}
!108 = !{!109, !15, i64 200}
!109 = !{!"_ZTSZN35btSoftBodyConcaveCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResultE31LocalTriangleSphereCastCallback", !81, i64 4, !81, i64 68, !81, i64 132, !15, i64 196, !15, i64 200}
!110 = !{!96, !15, i64 252}
!111 = !{!96, !15, i64 248}
!112 = !{!109, !15, i64 196}
!113 = !{!96, !15, i64 244}
!114 = !{!17, !7, i64 16}
!115 = !{!17, !3, i64 12}
!116 = !{!17, !19, i64 4}
!117 = !{!17, !19, i64 8}
!118 = !{!20, !7, i64 16}
!119 = !{!20, !3, i64 12}
!120 = !{!20, !19, i64 4}
!121 = !{!20, !19, i64 8}
!122 = !{!22, !7, i64 16}
!123 = !{!22, !3, i64 12}
!124 = !{!22, !19, i64 4}
!125 = !{!22, !19, i64 8}
!126 = !{!94, !19, i64 4}
!127 = !{!128, !15, i64 164}
!128 = !{!"_ZTSN12btConvexCast10CastResultE", !81, i64 4, !81, i64 68, !14, i64 132, !14, i64 148, !15, i64 164, !3, i64 168, !15, i64 172}
!129 = !{!128, !3, i64 168}
!130 = !{!128, !15, i64 172}
!131 = !{!132, !15, i64 44}
!132 = !{!"_ZTS21btConvexInternalShape", !14, i64 12, !14, i64 28, !15, i64 44, !15, i64 48}
!133 = !{!134, !15, i64 308}
!134 = !{!"_ZTS22btVoronoiSimplexSolver", !19, i64 0, !4, i64 4, !4, i64 84, !4, i64 164, !14, i64 244, !14, i64 260, !14, i64 276, !14, i64 292, !15, i64 308, !7, i64 312, !135, i64 316, !7, i64 356}
!135 = !{!"_ZTS25btSubSimplexClosestResult", !14, i64 0, !136, i64 16, !4, i64 20, !7, i64 36}
!136 = !{!"_ZTS15btUsageBitfield", !137, i64 0, !137, i64 0, !137, i64 0, !137, i64 0, !137, i64 0, !137, i64 0, !137, i64 0, !137, i64 0}
!137 = !{!"short", !4, i64 0}
!138 = !{!139, !15, i64 44}
!139 = !{!"_ZTS25btConvexInternalShapeData", !140, i64 0, !141, i64 12, !141, i64 28, !15, i64 44, !19, i64 48}
!140 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !19, i64 4, !4, i64 8}
!141 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!142 = !{!143, !19, i64 4}
!143 = !{!"_ZTS20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE", !144, i64 0, !19, i64 4, !19, i64 8, !3, i64 12, !7, i64 16}
!144 = !{!"_ZTS18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE"}
!145 = !{!143, !3, i64 12}
!146 = !{i64 0, i64 4, !32}
