; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexMaterialArray.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexMaterialArray.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btTriangleIndexVertexMaterialArray = type { %class.btTriangleIndexVertexArray, %class.btAlignedObjectArray.0 }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btMaterialProperties*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btMaterialProperties = type { i32, i8*, i32, i32, i32, i8*, i32, i32 }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%class.btSerializer = type opaque

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev = comdat any

$_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi = comdat any

$_ZN34btTriangleIndexVertexMaterialArrayD2Ev = comdat any

$_ZN34btTriangleIndexVertexMaterialArrayD0Ev = comdat any

$_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv = comdat any

$_ZN26btTriangleIndexVertexArray19preallocateVerticesEi = comdat any

$_ZN26btTriangleIndexVertexArray18preallocateIndicesEi = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv = comdat any

$_ZN34btTriangleIndexVertexMaterialArraydlEPv = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev = comdat any

@_ZTV34btTriangleIndexVertexMaterialArray = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btTriangleIndexVertexMaterialArray to i8*), i8* bitcast (%class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*)* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*)* @_ZN34btTriangleIndexVertexMaterialArrayD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi to i8*), i8* bitcast (i32 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi to i8*), i8* bitcast (i1 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS34btTriangleIndexVertexMaterialArray = hidden constant [37 x i8] c"34btTriangleIndexVertexMaterialArray\00", align 1
@_ZTI26btTriangleIndexVertexArray = external constant i8*
@_ZTI34btTriangleIndexVertexMaterialArray = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btTriangleIndexVertexMaterialArray, i32 0, i32 0), i8* bitcast (i8** @_ZTI26btTriangleIndexVertexArray to i8*) }, align 4

@_ZN34btTriangleIndexVertexMaterialArrayC1EiPiiiPfiiPhiS0_i = hidden unnamed_addr alias %class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*, i32, i32*, i32, i32, float*, i32, i32, i8*, i32, i32*, i32), %class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*, i32, i32*, i32, i32, float*, i32, i32, i8*, i32, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i

define hidden %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i(%class.btTriangleIndexVertexMaterialArray* returned %this, i32 %numTriangles, i32* %triangleIndexBase, i32 %triangleIndexStride, i32 %numVertices, float* %vertexBase, i32 %vertexStride, i32 %numMaterials, i8* %materialBase, i32 %materialStride, i32* %triangleMaterialsBase, i32 %materialIndexStride) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %numTriangles.addr = alloca i32, align 4
  %triangleIndexBase.addr = alloca i32*, align 4
  %triangleIndexStride.addr = alloca i32, align 4
  %numVertices.addr = alloca i32, align 4
  %vertexBase.addr = alloca float*, align 4
  %vertexStride.addr = alloca i32, align 4
  %numMaterials.addr = alloca i32, align 4
  %materialBase.addr = alloca i8*, align 4
  %materialStride.addr = alloca i32, align 4
  %triangleMaterialsBase.addr = alloca i32*, align 4
  %materialIndexStride.addr = alloca i32, align 4
  %mat = alloca %struct.btMaterialProperties, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  store i32 %numTriangles, i32* %numTriangles.addr, align 4, !tbaa !6
  store i32* %triangleIndexBase, i32** %triangleIndexBase.addr, align 4, !tbaa !2
  store i32 %triangleIndexStride, i32* %triangleIndexStride.addr, align 4, !tbaa !6
  store i32 %numVertices, i32* %numVertices.addr, align 4, !tbaa !6
  store float* %vertexBase, float** %vertexBase.addr, align 4, !tbaa !2
  store i32 %vertexStride, i32* %vertexStride.addr, align 4, !tbaa !6
  store i32 %numMaterials, i32* %numMaterials.addr, align 4, !tbaa !6
  store i8* %materialBase, i8** %materialBase.addr, align 4, !tbaa !2
  store i32 %materialStride, i32* %materialStride.addr, align 4, !tbaa !6
  store i32* %triangleMaterialsBase, i32** %triangleMaterialsBase.addr, align 4, !tbaa !2
  store i32 %materialIndexStride, i32* %materialIndexStride.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to %class.btTriangleIndexVertexArray*
  %1 = load i32, i32* %numTriangles.addr, align 4, !tbaa !6
  %2 = load i32*, i32** %triangleIndexBase.addr, align 4, !tbaa !2
  %3 = load i32, i32* %triangleIndexStride.addr, align 4, !tbaa !6
  %4 = load i32, i32* %numVertices.addr, align 4, !tbaa !6
  %5 = load float*, float** %vertexBase.addr, align 4, !tbaa !2
  %6 = load i32, i32* %vertexStride.addr, align 4, !tbaa !6
  %call = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi(%class.btTriangleIndexVertexArray* %0, i32 %1, i32* %2, i32 %3, i32 %4, float* %5, i32 %6)
  %7 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV34btTriangleIndexVertexMaterialArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %7, align 4, !tbaa !8
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev(%class.btAlignedObjectArray.0* %m_materials)
  %8 = bitcast %struct.btMaterialProperties* %mat to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %8) #7
  %9 = load i32, i32* %numMaterials.addr, align 4, !tbaa !6
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 0
  store i32 %9, i32* %m_numMaterials, align 4, !tbaa !10
  %10 = load i8*, i8** %materialBase.addr, align 4, !tbaa !2
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 1
  store i8* %10, i8** %m_materialBase, align 4, !tbaa !13
  %11 = load i32, i32* %materialStride.addr, align 4, !tbaa !6
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 2
  store i32 %11, i32* %m_materialStride, align 4, !tbaa !14
  %m_materialType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 3
  store i32 0, i32* %m_materialType, align 4, !tbaa !15
  %12 = load i32, i32* %numTriangles.addr, align 4, !tbaa !6
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 4
  store i32 %12, i32* %m_numTriangles, align 4, !tbaa !16
  %13 = load i32*, i32** %triangleMaterialsBase.addr, align 4, !tbaa !2
  %14 = bitcast i32* %13 to i8*
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 5
  store i8* %14, i8** %m_triangleMaterialsBase, align 4, !tbaa !17
  %15 = load i32, i32* %materialIndexStride.addr, align 4, !tbaa !6
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 6
  store i32 %15, i32* %m_triangleMaterialStride, align 4, !tbaa !18
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 7
  store i32 2, i32* %m_triangleType, align 4, !tbaa !19
  call void @_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType(%class.btTriangleIndexVertexMaterialArray* %this1, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %mat, i32 2)
  %16 = bitcast %struct.btMaterialProperties* %mat to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %16) #7
  ret %class.btTriangleIndexVertexMaterialArray* %this1
}

declare %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi(%class.btTriangleIndexVertexArray* returned, i32, i32*, i32, i32, float*, i32) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType(%class.btTriangleIndexVertexMaterialArray* %this, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %mat, i32 %triangleType) #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %mat.addr = alloca %struct.btMaterialProperties*, align 4
  %triangleType.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  store %struct.btMaterialProperties* %mat, %struct.btMaterialProperties** %mat.addr, align 4, !tbaa !2
  store i32 %triangleType, i32* %triangleType.addr, align 4, !tbaa !20
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mat.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_materials, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %0)
  %1 = load i32, i32* %triangleType.addr, align 4, !tbaa !20
  %m_materials2 = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %m_materials3 = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %m_materials3)
  %sub = sub nsw i32 %call, 1
  %call4 = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials2, i32 %sub)
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %call4, i32 0, i32 7
  store i32 %1, i32* %m_triangleType, align 4, !tbaa !19
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden void @_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexMaterialArray* %this, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %subpart) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %materialBase.addr = alloca i8**, align 4
  %numMaterials.addr = alloca i32*, align 4
  %materialType.addr = alloca i32*, align 4
  %materialStride.addr = alloca i32*, align 4
  %triangleMaterialBase.addr = alloca i8**, align 4
  %numTriangles.addr = alloca i32*, align 4
  %triangleMaterialStride.addr = alloca i32*, align 4
  %triangleType.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mats = alloca %struct.btMaterialProperties*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  store i8** %materialBase, i8*** %materialBase.addr, align 4, !tbaa !2
  store i32* %numMaterials, i32** %numMaterials.addr, align 4, !tbaa !2
  store i32* %materialType, i32** %materialType.addr, align 4, !tbaa !2
  store i32* %materialStride, i32** %materialStride.addr, align 4, !tbaa !2
  store i8** %triangleMaterialBase, i8*** %triangleMaterialBase.addr, align 4, !tbaa !2
  store i32* %numTriangles, i32** %numTriangles.addr, align 4, !tbaa !2
  store i32* %triangleMaterialStride, i32** %triangleMaterialStride.addr, align 4, !tbaa !2
  store i32* %triangleType, i32** %triangleType.addr, align 4, !tbaa !2
  store i32 %subpart, i32* %subpart.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %struct.btMaterialProperties** %mats to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %1 = load i32, i32* %subpart.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials, i32 %1)
  store %struct.btMaterialProperties* %call, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %2 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %2, i32 0, i32 0
  %3 = load i32, i32* %m_numMaterials, align 4, !tbaa !10
  %4 = load i32*, i32** %numMaterials.addr, align 4, !tbaa !2
  store i32 %3, i32* %4, align 4, !tbaa !6
  %5 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %5, i32 0, i32 1
  %6 = load i8*, i8** %m_materialBase, align 4, !tbaa !13
  %7 = load i8**, i8*** %materialBase.addr, align 4, !tbaa !2
  store i8* %6, i8** %7, align 4, !tbaa !2
  %8 = load i32*, i32** %materialType.addr, align 4, !tbaa !2
  store i32 0, i32* %8, align 4, !tbaa !20
  %9 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %9, i32 0, i32 2
  %10 = load i32, i32* %m_materialStride, align 4, !tbaa !14
  %11 = load i32*, i32** %materialStride.addr, align 4, !tbaa !2
  store i32 %10, i32* %11, align 4, !tbaa !6
  %12 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %12, i32 0, i32 4
  %13 = load i32, i32* %m_numTriangles, align 4, !tbaa !16
  %14 = load i32*, i32** %numTriangles.addr, align 4, !tbaa !2
  store i32 %13, i32* %14, align 4, !tbaa !6
  %15 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %15, i32 0, i32 5
  %16 = load i8*, i8** %m_triangleMaterialsBase, align 4, !tbaa !17
  %17 = load i8**, i8*** %triangleMaterialBase.addr, align 4, !tbaa !2
  store i8* %16, i8** %17, align 4, !tbaa !2
  %18 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %18, i32 0, i32 6
  %19 = load i32, i32* %m_triangleMaterialStride, align 4, !tbaa !18
  %20 = load i32*, i32** %triangleMaterialStride.addr, align 4, !tbaa !2
  store i32 %19, i32* %20, align 4, !tbaa !6
  %21 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %21, i32 0, i32 7
  %22 = load i32, i32* %m_triangleType, align 4, !tbaa !19
  %23 = load i32*, i32** %triangleType.addr, align 4, !tbaa !2
  store i32 %22, i32* %23, align 4, !tbaa !20
  %24 = bitcast %struct.btMaterialProperties** %mats to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %0, i32 %1
  ret %struct.btMaterialProperties* %arrayidx
}

define hidden void @_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexMaterialArray* %this, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %subpart) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %materialBase.addr = alloca i8**, align 4
  %numMaterials.addr = alloca i32*, align 4
  %materialType.addr = alloca i32*, align 4
  %materialStride.addr = alloca i32*, align 4
  %triangleMaterialBase.addr = alloca i8**, align 4
  %numTriangles.addr = alloca i32*, align 4
  %triangleMaterialStride.addr = alloca i32*, align 4
  %triangleType.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mats = alloca %struct.btMaterialProperties*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  store i8** %materialBase, i8*** %materialBase.addr, align 4, !tbaa !2
  store i32* %numMaterials, i32** %numMaterials.addr, align 4, !tbaa !2
  store i32* %materialType, i32** %materialType.addr, align 4, !tbaa !2
  store i32* %materialStride, i32** %materialStride.addr, align 4, !tbaa !2
  store i8** %triangleMaterialBase, i8*** %triangleMaterialBase.addr, align 4, !tbaa !2
  store i32* %numTriangles, i32** %numTriangles.addr, align 4, !tbaa !2
  store i32* %triangleMaterialStride, i32** %triangleMaterialStride.addr, align 4, !tbaa !2
  store i32* %triangleType, i32** %triangleType.addr, align 4, !tbaa !2
  store i32 %subpart, i32* %subpart.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %struct.btMaterialProperties** %mats to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %1 = load i32, i32* %subpart.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials, i32 %1)
  store %struct.btMaterialProperties* %call, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %2 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %2, i32 0, i32 0
  %3 = load i32, i32* %m_numMaterials, align 4, !tbaa !10
  %4 = load i32*, i32** %numMaterials.addr, align 4, !tbaa !2
  store i32 %3, i32* %4, align 4, !tbaa !6
  %5 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %5, i32 0, i32 1
  %6 = load i8*, i8** %m_materialBase, align 4, !tbaa !13
  %7 = load i8**, i8*** %materialBase.addr, align 4, !tbaa !2
  store i8* %6, i8** %7, align 4, !tbaa !2
  %8 = load i32*, i32** %materialType.addr, align 4, !tbaa !2
  store i32 0, i32* %8, align 4, !tbaa !20
  %9 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %9, i32 0, i32 2
  %10 = load i32, i32* %m_materialStride, align 4, !tbaa !14
  %11 = load i32*, i32** %materialStride.addr, align 4, !tbaa !2
  store i32 %10, i32* %11, align 4, !tbaa !6
  %12 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %12, i32 0, i32 4
  %13 = load i32, i32* %m_numTriangles, align 4, !tbaa !16
  %14 = load i32*, i32** %numTriangles.addr, align 4, !tbaa !2
  store i32 %13, i32* %14, align 4, !tbaa !6
  %15 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %15, i32 0, i32 5
  %16 = load i8*, i8** %m_triangleMaterialsBase, align 4, !tbaa !17
  %17 = load i8**, i8*** %triangleMaterialBase.addr, align 4, !tbaa !2
  store i8* %16, i8** %17, align 4, !tbaa !2
  %18 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %18, i32 0, i32 6
  %19 = load i32, i32* %m_triangleMaterialStride, align 4, !tbaa !18
  %20 = load i32*, i32** %triangleMaterialStride.addr, align 4, !tbaa !2
  store i32 %19, i32* %20, align 4, !tbaa !6
  %21 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4, !tbaa !2
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %21, i32 0, i32 7
  %22 = load i32, i32* %m_triangleType, align 4, !tbaa !19
  %23 = load i32*, i32** %triangleType.addr, align 4, !tbaa !2
  store i32 %22, i32* %23, align 4, !tbaa !20
  %24 = bitcast %struct.btMaterialProperties** %mats to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #7
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev(%class.btTriangleIndexVertexMaterialArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV34btTriangleIndexVertexMaterialArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev(%class.btAlignedObjectArray.0* %m_materials) #7
  %1 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to %class.btTriangleIndexVertexArray*
  %call2 = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* %1) #7
  ret %class.btTriangleIndexVertexMaterialArray* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArrayD0Ev(%class.btTriangleIndexVertexMaterialArray* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %call = call %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev(%class.btTriangleIndexVertexMaterialArray* %this1) #7
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i8*
  call void @_ZN34btTriangleIndexVertexMaterialArraydlEPv(i8* %0) #7
  ret void
}

declare void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #1

declare void @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4, !tbaa !2
  store i32 %subpart, i32* %subpart.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4, !tbaa !2
  store i32 %subpart, i32* %subpart.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

define linkonce_odr hidden i32 @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv(%class.btTriangleIndexVertexArray* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %m_indexedMeshes)
  ret i32 %call
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi(%class.btTriangleIndexVertexArray* %this, i32 %numverts) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numverts.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4, !tbaa !2
  store i32 %numverts, i32* %numverts.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi(%class.btTriangleIndexVertexArray* %this, i32 %numindices) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numindices.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4, !tbaa !2
  store i32 %numindices, i32* %numindices.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

declare zeroext i1 @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv(%class.btTriangleIndexVertexArray*) unnamed_addr #1

declare void @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_(%class.btTriangleIndexVertexArray*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_(%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

declare i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface*, i8*, %class.btSerializer*) unnamed_addr #1

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %_Val) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btMaterialProperties*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store %struct.btMaterialProperties* %_Val, %struct.btMaterialProperties** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %2, i32 %3
  %4 = bitcast %struct.btMaterialProperties* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btMaterialProperties*
  %6 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btMaterialProperties* %5 to i8*
  %8 = bitcast %struct.btMaterialProperties* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 32, i1 false), !tbaa.struct !26
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !25
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !25
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !25
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !27
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMaterialProperties*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMaterialProperties** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #7
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMaterialProperties*
  store %struct.btMaterialProperties* %3, %struct.btMaterialProperties** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btMaterialProperties* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %5 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* %5, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !27
  %7 = bitcast %struct.btMaterialProperties** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMaterialProperties* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btMaterialProperties** null)
  %2 = bitcast %struct.btMaterialProperties* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btMaterialProperties* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMaterialProperties*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMaterialProperties* %dest, %struct.btMaterialProperties** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %4, i32 %5
  %6 = bitcast %struct.btMaterialProperties* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btMaterialProperties*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %8, i32 %9
  %10 = bitcast %struct.btMaterialProperties* %7 to i8*
  %11 = bitcast %struct.btMaterialProperties* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 32, i1 false), !tbaa.struct !26
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #7
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #7
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #7
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %tobool = icmp ne %struct.btMaterialProperties* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !28, !range !29
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data4, align 4, !tbaa !21
  call void @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btMaterialProperties* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* null, %struct.btMaterialProperties** %m_data5, align 4, !tbaa !21
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMaterialProperties* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btMaterialProperties** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMaterialProperties**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMaterialProperties** %hint, %struct.btMaterialProperties*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMaterialProperties*
  ret %struct.btMaterialProperties* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btMaterialProperties* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btMaterialProperties*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.btMaterialProperties* %ptr, %struct.btMaterialProperties** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMaterialProperties* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* returned) unnamed_addr #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv(%class.btAlignedObjectArray.0* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !28
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* null, %struct.btMaterialProperties** %m_data, align 4, !tbaa !21
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !27
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArraydlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !30
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !7, i64 0}
!11 = !{!"_ZTS20btMaterialProperties", !7, i64 0, !3, i64 4, !7, i64 8, !12, i64 12, !7, i64 16, !3, i64 20, !7, i64 24, !12, i64 28}
!12 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!13 = !{!11, !3, i64 4}
!14 = !{!11, !7, i64 8}
!15 = !{!11, !12, i64 12}
!16 = !{!11, !7, i64 16}
!17 = !{!11, !3, i64 20}
!18 = !{!11, !7, i64 24}
!19 = !{!11, !12, i64 28}
!20 = !{!12, !12, i64 0}
!21 = !{!22, !3, i64 12}
!22 = !{!"_ZTS20btAlignedObjectArrayI20btMaterialPropertiesE", !23, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !24, i64 16}
!23 = !{!"_ZTS18btAlignedAllocatorI20btMaterialPropertiesLj16EE"}
!24 = !{!"bool", !4, i64 0}
!25 = !{!22, !7, i64 4}
!26 = !{i64 0, i64 4, !6, i64 4, i64 4, !2, i64 8, i64 4, !6, i64 12, i64 4, !20, i64 16, i64 4, !6, i64 20, i64 4, !2, i64 24, i64 4, !6, i64 28, i64 4, !20}
!27 = !{!22, !7, i64 8}
!28 = !{!22, !24, i64 16}
!29 = !{i8 0, i8 2}
!30 = !{!31, !7, i64 4}
!31 = !{!"_ZTS20btAlignedObjectArrayI13btIndexedMeshE", !32, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !24, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorI13btIndexedMeshLj16EE"}
