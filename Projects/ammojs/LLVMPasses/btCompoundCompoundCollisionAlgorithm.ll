; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCompoundCompoundCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btCompoundCompoundCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btCompoundCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btHashedSimplePairCache*, %class.btAlignedObjectArray, %class.btPersistentManifold*, i8, i32, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btHashedSimplePairCache = type { i32 (...)**, %class.btAlignedObjectArray, i8, [3 x i8], %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btSimplePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btSimplePair = type { i32, i32, %union.anon }
%union.anon = type { i8* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon.4, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%union.anon.4 = type { i8* }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray.5, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.8 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.8 = type { [2 x %struct.btDbvtNode*] }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.14 }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%class.btAlignedObjectArray.18 = type <{ %class.btAlignedAllocator.19, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.19 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCompoundCompoundLeafCallback = type { %"struct.btDbvt::ICollide", i32, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btDispatcher*, %struct.btDispatcherInfo*, %class.btManifoldResult*, %class.btHashedSimplePairCache*, %class.btPersistentManifold* }
%"struct.btDbvt::ICollide" = type { i32 (...)** }

$_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK15btCompoundShape17getUpdateRevisionEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairED2Ev = comdat any

$_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairEixEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZNK15btCompoundShape18getDynamicAabbTreeEv = comdat any

$_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZNK15btCompoundShape13getChildShapeEi = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK15btCompoundShape17getChildTransformEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_ = comdat any

$_ZN12btSimplePairC2Eii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN30btCompoundCompoundLeafCallbackD0Ev = comdat any

$_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_ = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZNK16btManifoldResult12getBody0WrapEv = comdat any

$_ZNK16btManifoldResult12getBody1WrapEv = comdat any

$_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_ = comdat any

$_ZN6btDbvt6sStkNNC2Ev = comdat any

$_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

$_ZNK12btDbvtAabbMm4MinsEv = comdat any

$_ZNK12btDbvtAabbMm4MaxsEv = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_Z9IntersectRK12btDbvtAabbMmS1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_ = comdat any

$_ZTV30btCompoundCompoundLeafCallback = comdat any

$_ZTS30btCompoundCompoundLeafCallback = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI30btCompoundCompoundLeafCallback = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

@gCompoundCompoundChildShapePairCallback = hidden global i1 (%class.btCollisionShape*, %class.btCollisionShape*)* null, align 4
@_ZTV36btCompoundCompoundCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI36btCompoundCompoundCollisionAlgorithm to i8*), i8* bitcast (%class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN36btCompoundCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btCompoundCompoundCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN36btCompoundCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btCompoundCompoundCollisionAlgorithm*, %class.btAlignedObjectArray.18*)* @_ZN36btCompoundCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS36btCompoundCompoundCollisionAlgorithm = hidden constant [39 x i8] c"36btCompoundCompoundCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI36btCompoundCompoundCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btCompoundCompoundCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@_ZTV30btCompoundCompoundLeafCallback = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btCompoundCompoundLeafCallback to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%struct.btCompoundCompoundLeafCallback*)* @_ZN30btCompoundCompoundLeafCallbackD0Ev to i8*), i8* bitcast (void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS30btCompoundCompoundLeafCallback = linkonce_odr hidden constant [33 x i8] c"30btCompoundCompoundLeafCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI30btCompoundCompoundLeafCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCompoundCompoundLeafCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4

@_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b = hidden unnamed_addr alias %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN36btCompoundCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b
@_ZN36btCompoundCompoundCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*), %class.btCompoundCompoundCollisionAlgorithm* (%class.btCompoundCompoundCollisionAlgorithm*)* @_ZN36btCompoundCompoundCollisionAlgorithmD2Ev

define hidden %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %isSwapped) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %isSwapped.addr = alloca i8, align 1
  %ptr = alloca i8*, align 4
  %col0ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %frombool = zext i1 %isSwapped to i8
  store i8 %frombool, i8* %isSwapped.addr, align 1, !tbaa !6
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV36btCompoundCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4, !tbaa !8
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray* %m_removePairs)
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4, !tbaa !2
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %5, i32 0, i32 1
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4, !tbaa !10
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !12
  %m_ownsManifold = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 4
  store i8 0, i8* %m_ownsManifold, align 4, !tbaa !17
  %7 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %call3 = call i8* @_Z22btAlignedAllocInternalmi(i32 68, i32 16)
  store i8* %call3, i8** %ptr, align 4, !tbaa !2
  %8 = load i8*, i8** %ptr, align 4, !tbaa !2
  %9 = bitcast i8* %8 to %class.btHashedSimplePairCache*
  %call4 = call %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheC1Ev(%class.btHashedSimplePairCache* %9)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  store %class.btHashedSimplePairCache* %9, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !18
  %10 = bitcast %struct.btCollisionObjectWrapper** %col0ObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %11, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %12 = bitcast %struct.btCollisionObjectWrapper** %col1ObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %13, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %14 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %call5 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %15)
  %16 = bitcast %class.btCollisionShape* %call5 to %class.btCompoundShape*
  store %class.btCompoundShape* %16, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %17 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %call6 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %17)
  %m_compoundShapeRevision0 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  store i32 %call6, i32* %m_compoundShapeRevision0, align 4, !tbaa !19
  %18 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %call7 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %19)
  %20 = bitcast %class.btCollisionShape* %call7 to %class.btCompoundShape*
  store %class.btCompoundShape* %20, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %21 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %call8 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %21)
  %m_compoundShapeRevision1 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 6
  store i32 %call8, i32* %m_compoundShapeRevision1, align 4, !tbaa !20
  %22 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast %struct.btCollisionObjectWrapper** %col1ObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast %struct.btCollisionObjectWrapper** %col0ObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret %class.btCompoundCompoundCollisionAlgorithm* %this1
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

declare %class.btHashedSimplePairCache* @_ZN23btHashedSimplePairCacheC1Ev(%class.btHashedSimplePairCache* returned) unnamed_addr #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !21
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4, !tbaa !23
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define hidden %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmD2Ev(%class.btCompoundCompoundCollisionAlgorithm* returned %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV36btCompoundCompoundCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  call void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this1)
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !18
  %2 = bitcast %class.btHashedSimplePairCache* %1 to %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)***
  %vtable = load %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)**, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)** %vtable, i64 0
  %3 = load %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)*, %class.btHashedSimplePairCache* (%class.btHashedSimplePairCache*)** %vfn, align 4
  %call = call %class.btHashedSimplePairCache* %3(%class.btHashedSimplePairCache* %1) #9
  %m_childCollisionAlgorithmCache2 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %4 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache2, align 4, !tbaa !18
  %5 = bitcast %class.btHashedSimplePairCache* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray* %m_removePairs) #9
  %6 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call4 = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %6) #9
  ret %class.btCompoundCompoundCollisionAlgorithm* %this1
}

define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this) #0 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %pairs = alloca %class.btAlignedObjectArray*, align 4
  %numChildren = alloca i32, align 4
  %i = alloca i32, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !18
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %1)
  store %class.btAlignedObjectArray* %call, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %2 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %3)
  store i32 %call2, i32* %numChildren, align 4, !tbaa !29
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %numChildren, align 4, !tbaa !29
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %8 = load i32, i32* %i, align 4, !tbaa !29
  %call3 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %7, i32 %8)
  %9 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call3, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %9 to i8**
  %10 = load i8*, i8** %m_userPointer, align 4, !tbaa !30
  %tobool = icmp ne i8* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %13 = load i32, i32* %i, align 4, !tbaa !29
  %call4 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %12, i32 %13)
  %14 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call4, i32 0, i32 2
  %m_userPointer5 = bitcast %union.anon* %14 to i8**
  %15 = load i8*, i8** %m_userPointer5, align 4, !tbaa !30
  %16 = bitcast i8* %15 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %16, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %17 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %18 = bitcast %class.btCollisionAlgorithm* %17 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %18, align 4, !tbaa !8
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable, i64 0
  %19 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn, align 4
  %call6 = call %class.btCollisionAlgorithm* %19(%class.btCollisionAlgorithm* %17) #9
  %20 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %20, i32 0, i32 1
  %21 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !31
  %22 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %23 = bitcast %class.btCollisionAlgorithm* %22 to i8*
  %24 = bitcast %class.btDispatcher* %21 to void (%class.btDispatcher*, i8*)***
  %vtable7 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %24, align 4, !tbaa !8
  %vfn8 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable7, i64 15
  %25 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn8, align 4
  call void %25(%class.btDispatcher* %21, i8* %23)
  %26 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_childCollisionAlgorithmCache9 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %28 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache9, align 4, !tbaa !18
  call void @_ZN23btHashedSimplePairCache14removeAllPairsEv(%class.btHashedSimplePairCache* %28)
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast i32* %numChildren to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI12btSimplePairED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #5

; Function Attrs: nounwind
define hidden void @_ZN36btCompoundCompoundCollisionAlgorithmD0Ev(%class.btCompoundCompoundCollisionAlgorithm* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmD1Ev(%class.btCompoundCompoundCollisionAlgorithm* %this1) #9
  %0 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btCompoundCompoundCollisionAlgorithm* %this, %class.btAlignedObjectArray.18* nonnull align 1 %manifoldArray) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %i = alloca i32, align 4
  %pairs = alloca %class.btAlignedObjectArray*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.18* %manifoldArray, %class.btAlignedObjectArray.18** %manifoldArray.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %2 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !18
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %2)
  store %class.btAlignedObjectArray* %call, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !29
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %4)
  %cmp = icmp slt i32 %3, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %call3 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %5, i32 %6)
  %7 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call3, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %7 to i8**
  %8 = load i8*, i8** %m_userPointer, align 4, !tbaa !30
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %9 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %call4 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %9, i32 %10)
  %11 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call4, i32 0, i32 2
  %m_userPointer5 = bitcast %union.anon* %11 to i8**
  %12 = load i8*, i8** %m_userPointer5, align 4, !tbaa !30
  %13 = bitcast i8* %12 to %class.btCollisionAlgorithm*
  %14 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %manifoldArray.addr, align 4, !tbaa !2
  %15 = bitcast %class.btCollisionAlgorithm* %13 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*** %15, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)** %vtable, i64 4
  %16 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)** %vfn, align 4
  call void %16(%class.btCollisionAlgorithm* %13, %class.btAlignedObjectArray.18* nonnull align 1 %14)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %17 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #9
  %19 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btHashedSimplePairCache*, align 4
  store %class.btHashedSimplePairCache* %this, %class.btHashedSimplePairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btHashedSimplePairCache, %class.btHashedSimplePairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray* %m_overlappingPairArray
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %0, i32 %1
  ret %struct.btSimplePair* %arrayidx
}

declare void @_ZN23btHashedSimplePairCache14removeAllPairsEv(%class.btHashedSimplePairCache*) #1

define hidden void @_ZN36btCompoundCompoundCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCompoundCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %col0ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1ObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  %i = alloca i32, align 4
  %manifoldArray = alloca %class.btAlignedObjectArray.18, align 4
  %pairs = alloca %class.btAlignedObjectArray*, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  %m = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %tree0 = alloca %struct.btDbvt*, align 4
  %tree1 = alloca %struct.btDbvt*, align 4
  %callback = alloca %struct.btCompoundCompoundLeafCallback, align 4
  %xform = alloca %class.btTransform, align 4
  %ref.tmp33 = alloca %class.btTransform, align 4
  %pairs37 = alloca %class.btAlignedObjectArray*, align 4
  %i40 = alloca i32, align 4
  %manifoldArray41 = alloca %class.btAlignedObjectArray.18, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %algo55 = alloca %class.btCollisionAlgorithm*, align 4
  %orgTrans0 = alloca %class.btTransform, align 4
  %childShape0 = alloca %class.btCollisionShape*, align 4
  %newChildWorldTrans0 = alloca %class.btTransform, align 4
  %orgInterpolationTrans0 = alloca %class.btTransform, align 4
  %childTrans0 = alloca %class.btTransform*, align 4
  %ref.tmp70 = alloca %class.btTransform, align 4
  %orgInterpolationTrans1 = alloca %class.btTransform, align 4
  %childShape1 = alloca %class.btCollisionShape*, align 4
  %orgTrans1 = alloca %class.btTransform, align 4
  %newChildWorldTrans1 = alloca %class.btTransform, align 4
  %childTrans1 = alloca %class.btTransform*, align 4
  %ref.tmp86 = alloca %class.btTransform, align 4
  %ref.tmp98 = alloca %struct.btSimplePair, align 4
  %i109 = alloca i32, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  %0 = bitcast %struct.btCollisionObjectWrapper** %col0ObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %2 = bitcast %struct.btCollisionObjectWrapper** %col1ObjWrap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %4 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %5)
  %6 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %6, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %7 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %call2 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %8)
  %9 = bitcast %class.btCollisionShape* %call2 to %class.btCompoundShape*
  store %class.btCompoundShape* %9, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %10 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %call3 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %10)
  %m_compoundShapeRevision0 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 5
  %11 = load i32, i32* %m_compoundShapeRevision0, align 4, !tbaa !19
  %cmp = icmp ne i32 %call3, %11
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %12 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %call4 = call i32 @_ZNK15btCompoundShape17getUpdateRevisionEv(%class.btCompoundShape* %12)
  %m_compoundShapeRevision1 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 6
  %13 = load i32, i32* %m_compoundShapeRevision1, align 4, !tbaa !20
  %cmp5 = icmp ne i32 %call4, %13
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @_ZN36btCompoundCompoundCollisionAlgorithm21removeChildAlgorithmsEv(%class.btCompoundCompoundCollisionAlgorithm* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = bitcast %class.btAlignedObjectArray.18* %manifoldArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %15) #9
  %call6 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.18* %manifoldArray)
  %16 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %m_childCollisionAlgorithmCache = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %17 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !18
  %call7 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %17)
  store %class.btAlignedObjectArray* %call7, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  store i32 0, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.end
  %18 = load i32, i32* %i, align 4, !tbaa !29
  %19 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %19)
  %cmp9 = icmp slt i32 %18, %call8
  br i1 %cmp9, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %20 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !29
  %call10 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %20, i32 %21)
  %22 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call10, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %22 to i8**
  %23 = load i8*, i8** %m_userPointer, align 4, !tbaa !30
  %tobool = icmp ne i8* %23, null
  br i1 %tobool, label %if.then11, label %if.end24

if.then11:                                        ; preds = %for.body
  %24 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs, align 4, !tbaa !2
  %26 = load i32, i32* %i, align 4, !tbaa !29
  %call12 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %25, i32 %26)
  %27 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call12, i32 0, i32 2
  %m_userPointer13 = bitcast %union.anon* %27 to i8**
  %28 = load i8*, i8** %m_userPointer13, align 4, !tbaa !30
  %29 = bitcast i8* %28 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %29, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %30 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4, !tbaa !2
  %31 = bitcast %class.btCollisionAlgorithm* %30 to void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)***
  %vtable = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)**, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*** %31, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)** %vtable, i64 4
  %32 = load void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)*, void (%class.btCollisionAlgorithm*, %class.btAlignedObjectArray.18*)** %vfn, align 4
  call void %32(%class.btCollisionAlgorithm* %30, %class.btAlignedObjectArray.18* nonnull align 4 dereferenceable(17) %manifoldArray)
  %33 = bitcast i32* %m to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  store i32 0, i32* %m, align 4, !tbaa !29
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %if.then11
  %34 = load i32, i32* %m, align 4, !tbaa !29
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %manifoldArray)
  %cmp16 = icmp slt i32 %34, %call15
  br i1 %cmp16, label %for.body17, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond14
  %35 = bitcast i32* %m to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  br label %for.end

for.body17:                                       ; preds = %for.cond14
  %36 = load i32, i32* %m, align 4, !tbaa !29
  %call18 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.18* %manifoldArray, i32 %36)
  %37 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call18, align 4, !tbaa !2
  %call19 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %37)
  %tobool20 = icmp ne i32 %call19, 0
  br i1 %tobool20, label %if.then21, label %if.end23

if.then21:                                        ; preds = %for.body17
  %38 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %39 = load i32, i32* %m, align 4, !tbaa !29
  %call22 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.18* %manifoldArray, i32 %39)
  %40 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call22, align 4, !tbaa !2
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %38, %class.btPersistentManifold* %40)
  %41 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %41)
  %42 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %42, %class.btPersistentManifold* null)
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %for.body17
  br label %for.inc

for.inc:                                          ; preds = %if.end23
  %43 = load i32, i32* %m, align 4, !tbaa !29
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %m, align 4, !tbaa !29
  br label %for.cond14

for.end:                                          ; preds = %for.cond.cleanup
  %44 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.18* %manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %45 = bitcast %class.btPersistentManifold** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #9
  %46 = bitcast %class.btCollisionAlgorithm** %algo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  br label %if.end24

if.end24:                                         ; preds = %for.end, %for.body
  br label %for.inc25

for.inc25:                                        ; preds = %if.end24
  %47 = load i32, i32* %i, align 4, !tbaa !29
  %inc26 = add nsw i32 %47, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  %48 = bitcast %class.btAlignedObjectArray** %pairs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #9
  %call28 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.18* %manifoldArray) #9
  %49 = bitcast %class.btAlignedObjectArray.18* %manifoldArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %49) #9
  %50 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #9
  %51 = bitcast %struct.btDbvt** %tree0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #9
  %52 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %call29 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %52)
  store %struct.btDbvt* %call29, %struct.btDbvt** %tree0, align 4, !tbaa !2
  %53 = bitcast %struct.btDbvt** %tree1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #9
  %54 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %call30 = call %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %54)
  store %struct.btDbvt* %call30, %struct.btDbvt** %tree1, align 4, !tbaa !2
  %55 = bitcast %struct.btCompoundCompoundLeafCallback* %callback to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %55) #9
  %56 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %57 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %58 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %58, i32 0, i32 1
  %59 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !31
  %60 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  %61 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %m_childCollisionAlgorithmCache31 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %62 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache31, align 4, !tbaa !18
  %m_sharedManifold = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 3
  %63 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !12
  %call32 = call %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold(%struct.btCompoundCompoundLeafCallback* %callback, %struct.btCollisionObjectWrapper* %56, %struct.btCollisionObjectWrapper* %57, %class.btDispatcher* %59, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %60, %class.btManifoldResult* %61, %class.btHashedSimplePairCache* %62, %class.btPersistentManifold* %63)
  %64 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %64) #9
  %65 = bitcast %class.btTransform* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %65) #9
  %66 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %66)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp33, %class.btTransform* %call34)
  %67 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %call35 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %67)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %xform, %class.btTransform* %ref.tmp33, %class.btTransform* nonnull align 4 dereferenceable(64) %call35)
  %68 = bitcast %class.btTransform* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %68) #9
  %69 = load %struct.btDbvt*, %struct.btDbvt** %tree0, align 4, !tbaa !2
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %69, i32 0, i32 0
  %70 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4, !tbaa !35
  %71 = load %struct.btDbvt*, %struct.btDbvt** %tree1, align 4, !tbaa !2
  %m_root36 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %71, i32 0, i32 0
  %72 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root36, align 4, !tbaa !35
  call void @_ZL11MycollideTTPK10btDbvtNodeS1_RK11btTransformP30btCompoundCompoundLeafCallback(%struct.btDbvtNode* %70, %struct.btDbvtNode* %72, %class.btTransform* nonnull align 4 dereferenceable(64) %xform, %struct.btCompoundCompoundLeafCallback* %callback)
  %73 = bitcast %class.btAlignedObjectArray** %pairs37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %m_childCollisionAlgorithmCache38 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %74 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache38, align 4, !tbaa !18
  %call39 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* @_ZN23btHashedSimplePairCache23getOverlappingPairArrayEv(%class.btHashedSimplePairCache* %74)
  store %class.btAlignedObjectArray* %call39, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %75 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #9
  %76 = bitcast %class.btAlignedObjectArray.18* %manifoldArray41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %76) #9
  %call42 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.18* %manifoldArray41)
  %77 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %77) #9
  %call43 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %78 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %78) #9
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %79 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %79) #9
  %call45 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %80 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %80) #9
  %call46 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  store i32 0, i32* %i40, align 4, !tbaa !29
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc106, %for.end27
  %81 = load i32, i32* %i40, align 4, !tbaa !29
  %82 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %call48 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %82)
  %cmp49 = icmp slt i32 %81, %call48
  br i1 %cmp49, label %for.body50, label %for.end108

for.body50:                                       ; preds = %for.cond47
  %83 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %84 = load i32, i32* %i40, align 4, !tbaa !29
  %call51 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %83, i32 %84)
  %85 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call51, i32 0, i32 2
  %m_userPointer52 = bitcast %union.anon* %85 to i8**
  %86 = load i8*, i8** %m_userPointer52, align 4, !tbaa !30
  %tobool53 = icmp ne i8* %86, null
  br i1 %tobool53, label %if.then54, label %if.end105

if.then54:                                        ; preds = %for.body50
  %87 = bitcast %class.btCollisionAlgorithm** %algo55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #9
  %88 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %89 = load i32, i32* %i40, align 4, !tbaa !29
  %call56 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %88, i32 %89)
  %90 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call56, i32 0, i32 2
  %m_userPointer57 = bitcast %union.anon* %90 to i8**
  %91 = load i8*, i8** %m_userPointer57, align 4, !tbaa !30
  %92 = bitcast i8* %91 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %92, %class.btCollisionAlgorithm** %algo55, align 4, !tbaa !2
  %93 = bitcast %class.btTransform* %orgTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %93) #9
  %call58 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans0)
  %94 = bitcast %class.btCollisionShape** %childShape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #9
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %95 = bitcast %class.btTransform* %newChildWorldTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %95) #9
  %call59 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans0)
  %96 = bitcast %class.btTransform* %orgInterpolationTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %96) #9
  %call60 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgInterpolationTrans0)
  %97 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %98 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %99 = load i32, i32* %i40, align 4, !tbaa !29
  %call61 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %98, i32 %99)
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call61, i32 0, i32 0
  %100 = load i32, i32* %m_indexA, align 4, !tbaa !41
  %call62 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %97, i32 %100)
  store %class.btCollisionShape* %call62, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %101 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %call63 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %101)
  %call64 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call63)
  %102 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0ObjWrap, align 4, !tbaa !2
  %call65 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %102)
  %call66 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgInterpolationTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call65)
  %103 = bitcast %class.btTransform** %childTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #9
  %104 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %105 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %106 = load i32, i32* %i40, align 4, !tbaa !29
  %call67 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %105, i32 %106)
  %m_indexA68 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call67, i32 0, i32 0
  %107 = load i32, i32* %m_indexA68, align 4, !tbaa !41
  %call69 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %104, i32 %107)
  store %class.btTransform* %call69, %class.btTransform** %childTrans0, align 4, !tbaa !2
  %108 = bitcast %class.btTransform* %ref.tmp70 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %108) #9
  %109 = load %class.btTransform*, %class.btTransform** %childTrans0, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp70, %class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %109)
  %call71 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp70)
  %110 = bitcast %class.btTransform* %ref.tmp70 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %110) #9
  %111 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %112 = bitcast %class.btCollisionShape* %111 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable72 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %112, align 4, !tbaa !8
  %vfn73 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable72, i64 2
  %113 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn73, align 4
  call void %113(%class.btCollisionShape* %111, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %114 = bitcast %class.btTransform** %childTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #9
  %115 = bitcast %class.btTransform* %orgInterpolationTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %115) #9
  %116 = bitcast %class.btTransform* %newChildWorldTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %116) #9
  %117 = bitcast %class.btCollisionShape** %childShape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #9
  %118 = bitcast %class.btTransform* %orgTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %118) #9
  %119 = bitcast %class.btTransform* %orgInterpolationTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %119) #9
  %call74 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgInterpolationTrans1)
  %120 = bitcast %class.btCollisionShape** %childShape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #9
  store %class.btCollisionShape* null, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %121 = bitcast %class.btTransform* %orgTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %121) #9
  %call75 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %orgTrans1)
  %122 = bitcast %class.btTransform* %newChildWorldTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %122) #9
  %call76 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %newChildWorldTrans1)
  %123 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %124 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %125 = load i32, i32* %i40, align 4, !tbaa !29
  %call77 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %124, i32 %125)
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call77, i32 0, i32 1
  %126 = load i32, i32* %m_indexB, align 4, !tbaa !43
  %call78 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %123, i32 %126)
  store %class.btCollisionShape* %call78, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %127 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %call79 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %127)
  %call80 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call79)
  %128 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1ObjWrap, align 4, !tbaa !2
  %call81 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %128)
  %call82 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %orgInterpolationTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call81)
  %129 = bitcast %class.btTransform** %childTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #9
  %130 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %131 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %132 = load i32, i32* %i40, align 4, !tbaa !29
  %call83 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %131, i32 %132)
  %m_indexB84 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call83, i32 0, i32 1
  %133 = load i32, i32* %m_indexB84, align 4, !tbaa !43
  %call85 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %130, i32 %133)
  store %class.btTransform* %call85, %class.btTransform** %childTrans1, align 4, !tbaa !2
  %134 = bitcast %class.btTransform* %ref.tmp86 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %134) #9
  %135 = load %class.btTransform*, %class.btTransform** %childTrans1, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp86, %class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %135)
  %call87 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %newChildWorldTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp86)
  %136 = bitcast %class.btTransform* %ref.tmp86 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %136) #9
  %137 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %138 = bitcast %class.btCollisionShape* %137 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable88 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %138, align 4, !tbaa !8
  %vfn89 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable88, i64 2
  %139 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn89, align 4
  call void %139(%class.btCollisionShape* %137, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %140 = bitcast %class.btTransform** %childTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #9
  %141 = bitcast %class.btTransform* %newChildWorldTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %141) #9
  %142 = bitcast %class.btTransform* %orgTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %142) #9
  %143 = bitcast %class.btCollisionShape** %childShape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #9
  %144 = bitcast %class.btTransform* %orgInterpolationTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %144) #9
  %call90 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call90, label %if.end104, label %if.then91

if.then91:                                        ; preds = %if.then54
  %145 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo55, align 4, !tbaa !2
  %146 = bitcast %class.btCollisionAlgorithm* %145 to %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)***
  %vtable92 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)**, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*** %146, align 4, !tbaa !8
  %vfn93 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vtable92, i64 0
  %147 = load %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)*, %class.btCollisionAlgorithm* (%class.btCollisionAlgorithm*)** %vfn93, align 4
  %call94 = call %class.btCollisionAlgorithm* %147(%class.btCollisionAlgorithm* %145) #9
  %148 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher95 = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %148, i32 0, i32 1
  %149 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher95, align 4, !tbaa !31
  %150 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo55, align 4, !tbaa !2
  %151 = bitcast %class.btCollisionAlgorithm* %150 to i8*
  %152 = bitcast %class.btDispatcher* %149 to void (%class.btDispatcher*, i8*)***
  %vtable96 = load void (%class.btDispatcher*, i8*)**, void (%class.btDispatcher*, i8*)*** %152, align 4, !tbaa !8
  %vfn97 = getelementptr inbounds void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vtable96, i64 15
  %153 = load void (%class.btDispatcher*, i8*)*, void (%class.btDispatcher*, i8*)** %vfn97, align 4
  call void %153(%class.btDispatcher* %149, i8* %151)
  %m_removePairs = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %154 = bitcast %struct.btSimplePair* %ref.tmp98 to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %154) #9
  %155 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %156 = load i32, i32* %i40, align 4, !tbaa !29
  %call99 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %155, i32 %156)
  %m_indexA100 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call99, i32 0, i32 0
  %157 = load i32, i32* %m_indexA100, align 4, !tbaa !41
  %158 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pairs37, align 4, !tbaa !2
  %159 = load i32, i32* %i40, align 4, !tbaa !29
  %call101 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %158, i32 %159)
  %m_indexB102 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call101, i32 0, i32 1
  %160 = load i32, i32* %m_indexB102, align 4, !tbaa !43
  %call103 = call %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* %ref.tmp98, i32 %157, i32 %160)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_(%class.btAlignedObjectArray* %m_removePairs, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %ref.tmp98)
  %161 = bitcast %struct.btSimplePair* %ref.tmp98 to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %161) #9
  br label %if.end104

if.end104:                                        ; preds = %if.then91, %if.then54
  %162 = bitcast %class.btCollisionAlgorithm** %algo55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #9
  br label %if.end105

if.end105:                                        ; preds = %if.end104, %for.body50
  br label %for.inc106

for.inc106:                                       ; preds = %if.end105
  %163 = load i32, i32* %i40, align 4, !tbaa !29
  %inc107 = add nsw i32 %163, 1
  store i32 %inc107, i32* %i40, align 4, !tbaa !29
  br label %for.cond47

for.end108:                                       ; preds = %for.cond47
  %164 = bitcast i32* %i109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #9
  store i32 0, i32* %i109, align 4, !tbaa !29
  br label %for.cond110

for.cond110:                                      ; preds = %for.inc126, %for.end108
  %165 = load i32, i32* %i109, align 4, !tbaa !29
  %m_removePairs111 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %call112 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %m_removePairs111)
  %cmp113 = icmp slt i32 %165, %call112
  br i1 %cmp113, label %for.body115, label %for.cond.cleanup114

for.cond.cleanup114:                              ; preds = %for.cond110
  %166 = bitcast i32* %i109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #9
  br label %for.end128

for.body115:                                      ; preds = %for.cond110
  %m_childCollisionAlgorithmCache116 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 1
  %167 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache116, align 4, !tbaa !18
  %m_removePairs117 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %168 = load i32, i32* %i109, align 4, !tbaa !29
  %call118 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_removePairs117, i32 %168)
  %m_indexA119 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call118, i32 0, i32 0
  %169 = load i32, i32* %m_indexA119, align 4, !tbaa !41
  %m_removePairs120 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  %170 = load i32, i32* %i109, align 4, !tbaa !29
  %call121 = call nonnull align 4 dereferenceable(12) %struct.btSimplePair* @_ZN20btAlignedObjectArrayI12btSimplePairEixEi(%class.btAlignedObjectArray* %m_removePairs120, i32 %170)
  %m_indexB122 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %call121, i32 0, i32 1
  %171 = load i32, i32* %m_indexB122, align 4, !tbaa !43
  %172 = bitcast %class.btHashedSimplePairCache* %167 to i8* (%class.btHashedSimplePairCache*, i32, i32)***
  %vtable123 = load i8* (%class.btHashedSimplePairCache*, i32, i32)**, i8* (%class.btHashedSimplePairCache*, i32, i32)*** %172, align 4, !tbaa !8
  %vfn124 = getelementptr inbounds i8* (%class.btHashedSimplePairCache*, i32, i32)*, i8* (%class.btHashedSimplePairCache*, i32, i32)** %vtable123, i64 2
  %173 = load i8* (%class.btHashedSimplePairCache*, i32, i32)*, i8* (%class.btHashedSimplePairCache*, i32, i32)** %vfn124, align 4
  %call125 = call i8* %173(%class.btHashedSimplePairCache* %167, i32 %169, i32 %171)
  br label %for.inc126

for.inc126:                                       ; preds = %for.body115
  %174 = load i32, i32* %i109, align 4, !tbaa !29
  %inc127 = add nsw i32 %174, 1
  store i32 %inc127, i32* %i109, align 4, !tbaa !29
  br label %for.cond110

for.end128:                                       ; preds = %for.cond.cleanup114
  %m_removePairs129 = getelementptr inbounds %class.btCompoundCompoundCollisionAlgorithm, %class.btCompoundCompoundCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %m_removePairs129)
  %175 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %175) #9
  %176 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %176) #9
  %177 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %177) #9
  %178 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %178) #9
  %call130 = call %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.18* %manifoldArray41) #9
  %179 = bitcast %class.btAlignedObjectArray.18* %manifoldArray41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %179) #9
  %180 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #9
  %181 = bitcast %class.btAlignedObjectArray** %pairs37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #9
  %182 = bitcast %class.btTransform* %xform to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %182) #9
  %call131 = call %struct.btCompoundCompoundLeafCallback* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to %struct.btCompoundCompoundLeafCallback* (%struct.btCompoundCompoundLeafCallback*)*)(%struct.btCompoundCompoundLeafCallback* %callback) #9
  %183 = bitcast %struct.btCompoundCompoundLeafCallback* %callback to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %183) #9
  %184 = bitcast %struct.btDbvt** %tree1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #9
  %185 = bitcast %struct.btDbvt** %tree0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #9
  %186 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #9
  %187 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #9
  %188 = bitcast %struct.btCollisionObjectWrapper** %col1ObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #9
  %189 = bitcast %struct.btCollisionObjectWrapper** %col0ObjWrap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #9
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.19* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !44
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.18* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4, !tbaa !48
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4, !tbaa !2
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !50
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4, !tbaa !50
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isSwapped) #9
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4, !tbaa !50
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !52
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1, !tbaa !6
  %3 = load i8, i8* %isSwapped, align 1, !tbaa !6, !range !53
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4, !tbaa !50
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !54
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4, !tbaa !52
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4, !tbaa !50
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4, !tbaa !52
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4, !tbaa !54
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then6
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isSwapped) #9
  br label %return

return:                                           ; preds = %if.end20, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.18* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !29
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !29
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %2 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  store i32 %4, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.18* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !29
  store i32 %14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !29
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data11, align 4, !tbaa !47
  %19 = load i32, i32* %i6, align 4, !tbaa !29
  %arrayidx12 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %18, i32 %19
  %20 = bitcast %class.btPersistentManifold** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btPersistentManifold**
  %22 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btPersistentManifold*, %class.btPersistentManifold** %22, align 4, !tbaa !2
  store %class.btPersistentManifold* %23, %class.btPersistentManifold** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !29
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !44
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.18* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.18* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.18* %this1)
  ret %class.btAlignedObjectArray.18* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btDbvt* @_ZNK15btCompoundShape18getDynamicAabbTreeEv(%class.btCompoundShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4, !tbaa !55
  ret %struct.btDbvt* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCompoundCompoundLeafCallback* @_ZN30btCompoundCompoundLeafCallbackC2EPK24btCollisionObjectWrapperS2_P12btDispatcherRK16btDispatcherInfoP16btManifoldResultP23btHashedSimplePairCacheP20btPersistentManifold(%struct.btCompoundCompoundLeafCallback* returned %this, %struct.btCollisionObjectWrapper* %compound1ObjWrap, %struct.btCollisionObjectWrapper* %compound0ObjWrap, %class.btDispatcher* %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut, %class.btHashedSimplePairCache* %childAlgorithmsCache, %class.btPersistentManifold* %sharedManifold) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %compound1ObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %compound0ObjWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %childAlgorithmsCache.addr = alloca %class.btHashedSimplePairCache*, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %compound1ObjWrap, %struct.btCollisionObjectWrapper** %compound1ObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %compound0ObjWrap, %struct.btCollisionObjectWrapper** %compound0ObjWrap.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  store %class.btHashedSimplePairCache* %childAlgorithmsCache, %class.btHashedSimplePairCache** %childAlgorithmsCache.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %0 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #9
  %1 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV30btCompoundCompoundLeafCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_numOverlapPairs = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 1
  store i32 0, i32* %m_numOverlapPairs, align 4, !tbaa !56
  %m_compound0ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compound1ObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap, align 4, !tbaa !58
  %m_compound1ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %compound0ObjWrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %3, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap, align 4, !tbaa !59
  %m_dispatcher = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 4
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btDispatcher* %4, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !60
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 5
  %5 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %5, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %6 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %6, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !61
  %m_childCollisionAlgorithmCache = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %7 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %childAlgorithmsCache.addr, align 4, !tbaa !2
  store %class.btHashedSimplePairCache* %7, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !62
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 8
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4, !tbaa !2
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !63
  ret %struct.btCompoundCompoundLeafCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !64
  ret %class.btTransform* %0
}

define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #0 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #9
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btMatrix3x3* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %1) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %6) #9
  ret void
}

; Function Attrs: inlinehint
define internal void @_ZL11MycollideTTPK10btDbvtNodeS1_RK11btTransformP30btCompoundCompoundLeafCallback(%struct.btDbvtNode* %root0, %struct.btDbvtNode* %root1, %class.btTransform* nonnull align 4 dereferenceable(64) %xform, %struct.btCompoundCompoundLeafCallback* %callback) #7 {
entry:
  %root0.addr = alloca %struct.btDbvtNode*, align 4
  %root1.addr = alloca %struct.btDbvtNode*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  %callback.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %depth = alloca i32, align 4
  %treshold = alloca i32, align 4
  %stkStack = alloca %class.btAlignedObjectArray.10, align 4
  %ref.tmp = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp3 = alloca %"struct.btDbvt::sStkNN", align 4
  %p = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp12 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp21 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp28 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp38 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp48 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp58 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp66 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp79 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp87 = alloca %"struct.btDbvt::sStkNN", align 4
  store %struct.btDbvtNode* %root0, %struct.btDbvtNode** %root0.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %root1, %struct.btDbvtNode** %root1.addr, align 4, !tbaa !2
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4, !tbaa !2
  store %struct.btCompoundCompoundLeafCallback* %callback, %struct.btCompoundCompoundLeafCallback** %callback.addr, align 4, !tbaa !2
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end103

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4, !tbaa !2
  %tobool1 = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool1, label %if.then, label %if.end103

if.then:                                          ; preds = %land.lhs.true
  %2 = bitcast i32* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 1, i32* %depth, align 4, !tbaa !29
  %3 = bitcast i32* %treshold to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store i32 124, i32* %treshold, align 4, !tbaa !29
  %4 = bitcast %class.btAlignedObjectArray.10* %stkStack to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %4) #9
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray.10* %stkStack)
  %5 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %5) #9
  %call2 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %stkStack, i32 128, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %6 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %6) #9
  %7 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %7) #9
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4, !tbaa !2
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4, !tbaa !2
  %call4 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp3, %struct.btDbvtNode* %8, %struct.btDbvtNode* %9)
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 0)
  %10 = bitcast %"struct.btDbvt::sStkNN"* %call5 to i8*
  %11 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 8, i1 false), !tbaa.struct !65
  %12 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %12) #9
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %13 = bitcast %"struct.btDbvt::sStkNN"* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %13) #9
  %14 = load i32, i32* %depth, align 4, !tbaa !29
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %depth, align 4, !tbaa !29
  %call6 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %dec)
  %15 = bitcast %"struct.btDbvt::sStkNN"* %p to i8*
  %16 = bitcast %"struct.btDbvt::sStkNN"* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 8, i1 false), !tbaa.struct !65
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a, align 4, !tbaa !66
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %17, i32 0, i32 0
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %18 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b, align 4, !tbaa !68
  %volume7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %18, i32 0, i32 0
  %19 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4, !tbaa !2
  %call8 = call zeroext i1 @_ZL11MyIntersectRK12btDbvtAabbMmS1_RK11btTransform(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume7, %class.btTransform* nonnull align 4 dereferenceable(64) %19)
  br i1 %call8, label %if.then9, label %if.end100

if.then9:                                         ; preds = %do.body
  %20 = load i32, i32* %depth, align 4, !tbaa !29
  %21 = load i32, i32* %treshold, align 4, !tbaa !29
  %cmp = icmp sgt i32 %20, %21
  br i1 %cmp, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then9
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %stkStack)
  %mul = mul nsw i32 %call11, 2
  %22 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %22) #9
  %call13 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %ref.tmp12)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %stkStack, i32 %mul, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %ref.tmp12)
  %23 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %23) #9
  %call14 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %stkStack)
  %sub = sub nsw i32 %call14, 4
  store i32 %sub, i32* %treshold, align 4, !tbaa !29
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then9
  %a15 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %24 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a15, align 4, !tbaa !66
  %call16 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %24)
  br i1 %call16, label %if.then17, label %if.else75

if.then17:                                        ; preds = %if.end
  %b18 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b18, align 4, !tbaa !68
  %call19 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %25)
  br i1 %call19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.then17
  %26 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %26) #9
  %a22 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a22, align 4, !tbaa !66
  %28 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %27, i32 0, i32 2
  %childs = bitcast %union.anon.8* %28 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4, !tbaa !30
  %b23 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b23, align 4, !tbaa !68
  %31 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 2
  %childs24 = bitcast %union.anon.8* %31 to [2 x %struct.btDbvtNode*]*
  %arrayidx25 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs24, i32 0, i32 0
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx25, align 4, !tbaa !30
  %call26 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp21, %struct.btDbvtNode* %29, %struct.btDbvtNode* %32)
  %33 = load i32, i32* %depth, align 4, !tbaa !29
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %depth, align 4, !tbaa !29
  %call27 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %33)
  %34 = bitcast %"struct.btDbvt::sStkNN"* %call27 to i8*
  %35 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 8, i1 false), !tbaa.struct !65
  %36 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #9
  %37 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %37) #9
  %a29 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a29, align 4, !tbaa !66
  %39 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %38, i32 0, i32 2
  %childs30 = bitcast %union.anon.8* %39 to [2 x %struct.btDbvtNode*]*
  %arrayidx31 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs30, i32 0, i32 1
  %40 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx31, align 4, !tbaa !30
  %b32 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %41 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b32, align 4, !tbaa !68
  %42 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %41, i32 0, i32 2
  %childs33 = bitcast %union.anon.8* %42 to [2 x %struct.btDbvtNode*]*
  %arrayidx34 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs33, i32 0, i32 0
  %43 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx34, align 4, !tbaa !30
  %call35 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp28, %struct.btDbvtNode* %40, %struct.btDbvtNode* %43)
  %44 = load i32, i32* %depth, align 4, !tbaa !29
  %inc36 = add nsw i32 %44, 1
  store i32 %inc36, i32* %depth, align 4, !tbaa !29
  %call37 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %44)
  %45 = bitcast %"struct.btDbvt::sStkNN"* %call37 to i8*
  %46 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 8, i1 false), !tbaa.struct !65
  %47 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %47) #9
  %48 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %48) #9
  %a39 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %49 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a39, align 4, !tbaa !66
  %50 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %49, i32 0, i32 2
  %childs40 = bitcast %union.anon.8* %50 to [2 x %struct.btDbvtNode*]*
  %arrayidx41 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs40, i32 0, i32 0
  %51 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx41, align 4, !tbaa !30
  %b42 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %52 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b42, align 4, !tbaa !68
  %53 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %52, i32 0, i32 2
  %childs43 = bitcast %union.anon.8* %53 to [2 x %struct.btDbvtNode*]*
  %arrayidx44 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs43, i32 0, i32 1
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx44, align 4, !tbaa !30
  %call45 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp38, %struct.btDbvtNode* %51, %struct.btDbvtNode* %54)
  %55 = load i32, i32* %depth, align 4, !tbaa !29
  %inc46 = add nsw i32 %55, 1
  store i32 %inc46, i32* %depth, align 4, !tbaa !29
  %call47 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %55)
  %56 = bitcast %"struct.btDbvt::sStkNN"* %call47 to i8*
  %57 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp38 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 8, i1 false), !tbaa.struct !65
  %58 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %58) #9
  %59 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %59) #9
  %a49 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %60 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a49, align 4, !tbaa !66
  %61 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %60, i32 0, i32 2
  %childs50 = bitcast %union.anon.8* %61 to [2 x %struct.btDbvtNode*]*
  %arrayidx51 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs50, i32 0, i32 1
  %62 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx51, align 4, !tbaa !30
  %b52 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %63 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b52, align 4, !tbaa !68
  %64 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %63, i32 0, i32 2
  %childs53 = bitcast %union.anon.8* %64 to [2 x %struct.btDbvtNode*]*
  %arrayidx54 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs53, i32 0, i32 1
  %65 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx54, align 4, !tbaa !30
  %call55 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp48, %struct.btDbvtNode* %62, %struct.btDbvtNode* %65)
  %66 = load i32, i32* %depth, align 4, !tbaa !29
  %inc56 = add nsw i32 %66, 1
  store i32 %inc56, i32* %depth, align 4, !tbaa !29
  %call57 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %66)
  %67 = bitcast %"struct.btDbvt::sStkNN"* %call57 to i8*
  %68 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp48 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 8, i1 false), !tbaa.struct !65
  %69 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %69) #9
  br label %if.end74

if.else:                                          ; preds = %if.then17
  %70 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %70) #9
  %a59 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %71 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a59, align 4, !tbaa !66
  %72 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %71, i32 0, i32 2
  %childs60 = bitcast %union.anon.8* %72 to [2 x %struct.btDbvtNode*]*
  %arrayidx61 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs60, i32 0, i32 0
  %73 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx61, align 4, !tbaa !30
  %b62 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %74 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b62, align 4, !tbaa !68
  %call63 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp58, %struct.btDbvtNode* %73, %struct.btDbvtNode* %74)
  %75 = load i32, i32* %depth, align 4, !tbaa !29
  %inc64 = add nsw i32 %75, 1
  store i32 %inc64, i32* %depth, align 4, !tbaa !29
  %call65 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %75)
  %76 = bitcast %"struct.btDbvt::sStkNN"* %call65 to i8*
  %77 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 8, i1 false), !tbaa.struct !65
  %78 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %78) #9
  %79 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %79) #9
  %a67 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %80 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a67, align 4, !tbaa !66
  %81 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %80, i32 0, i32 2
  %childs68 = bitcast %union.anon.8* %81 to [2 x %struct.btDbvtNode*]*
  %arrayidx69 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs68, i32 0, i32 1
  %82 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx69, align 4, !tbaa !30
  %b70 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %83 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b70, align 4, !tbaa !68
  %call71 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp66, %struct.btDbvtNode* %82, %struct.btDbvtNode* %83)
  %84 = load i32, i32* %depth, align 4, !tbaa !29
  %inc72 = add nsw i32 %84, 1
  store i32 %inc72, i32* %depth, align 4, !tbaa !29
  %call73 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %84)
  %85 = bitcast %"struct.btDbvt::sStkNN"* %call73 to i8*
  %86 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %85, i8* align 4 %86, i32 8, i1 false), !tbaa.struct !65
  %87 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %87) #9
  br label %if.end74

if.end74:                                         ; preds = %if.else, %if.then20
  br label %if.end99

if.else75:                                        ; preds = %if.end
  %b76 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %88 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b76, align 4, !tbaa !68
  %call77 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %88)
  br i1 %call77, label %if.then78, label %if.else95

if.then78:                                        ; preds = %if.else75
  %89 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %89) #9
  %a80 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %90 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a80, align 4, !tbaa !66
  %b81 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %91 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b81, align 4, !tbaa !68
  %92 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %91, i32 0, i32 2
  %childs82 = bitcast %union.anon.8* %92 to [2 x %struct.btDbvtNode*]*
  %arrayidx83 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs82, i32 0, i32 0
  %93 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx83, align 4, !tbaa !30
  %call84 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp79, %struct.btDbvtNode* %90, %struct.btDbvtNode* %93)
  %94 = load i32, i32* %depth, align 4, !tbaa !29
  %inc85 = add nsw i32 %94, 1
  store i32 %inc85, i32* %depth, align 4, !tbaa !29
  %call86 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %94)
  %95 = bitcast %"struct.btDbvt::sStkNN"* %call86 to i8*
  %96 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp79 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %96, i32 8, i1 false), !tbaa.struct !65
  %97 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %97) #9
  %98 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %98) #9
  %a88 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %99 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a88, align 4, !tbaa !66
  %b89 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %100 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b89, align 4, !tbaa !68
  %101 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %100, i32 0, i32 2
  %childs90 = bitcast %union.anon.8* %101 to [2 x %struct.btDbvtNode*]*
  %arrayidx91 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs90, i32 0, i32 1
  %102 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx91, align 4, !tbaa !30
  %call92 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp87, %struct.btDbvtNode* %99, %struct.btDbvtNode* %102)
  %103 = load i32, i32* %depth, align 4, !tbaa !29
  %inc93 = add nsw i32 %103, 1
  store i32 %inc93, i32* %depth, align 4, !tbaa !29
  %call94 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %stkStack, i32 %103)
  %104 = bitcast %"struct.btDbvt::sStkNN"* %call94 to i8*
  %105 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %104, i8* align 4 %105, i32 8, i1 false), !tbaa.struct !65
  %106 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %106) #9
  br label %if.end98

if.else95:                                        ; preds = %if.else75
  %107 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %callback.addr, align 4, !tbaa !2
  %a96 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %108 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a96, align 4, !tbaa !66
  %b97 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %109 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b97, align 4, !tbaa !68
  %110 = bitcast %struct.btCompoundCompoundLeafCallback* %107 to void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)***
  %vtable = load void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)**, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*** %110, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vtable, i64 2
  %111 = load void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btCompoundCompoundLeafCallback*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vfn, align 4
  call void %111(%struct.btCompoundCompoundLeafCallback* %107, %struct.btDbvtNode* %108, %struct.btDbvtNode* %109)
  br label %if.end98

if.end98:                                         ; preds = %if.else95, %if.then78
  br label %if.end99

if.end99:                                         ; preds = %if.end98, %if.end74
  br label %if.end100

if.end100:                                        ; preds = %if.end99, %do.body
  %112 = bitcast %"struct.btDbvt::sStkNN"* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %112) #9
  br label %do.cond

do.cond:                                          ; preds = %if.end100
  %113 = load i32, i32* %depth, align 4, !tbaa !29
  %tobool101 = icmp ne i32 %113, 0
  br i1 %tobool101, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %call102 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray.10* %stkStack) #9
  %114 = bitcast %class.btAlignedObjectArray.10* %stkStack to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %114) #9
  %115 = bitcast i32* %treshold to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #9
  %116 = bitcast i32* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #9
  br label %if.end103

if.end103:                                        ; preds = %do.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.5* %m_children, i32 %0)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4, !tbaa !69
  ret %class.btCollisionShape* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !73
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #4 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !29
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.5* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #7 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #9
  store i8 1, i8* %overlap, align 1, !tbaa !6
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !74
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !74
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !74
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !74
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !6, !range !53
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !6
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !74
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !74
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !74
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !74
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !6, !range !53
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !6
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !74
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !74
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !74
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !74
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !6, !range !53
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !6
  %27 = load i8, i8* %overlap, align 1, !tbaa !6, !range !53
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #9
  ret i1 %tobool31
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btSimplePair* nonnull align 4 dereferenceable(12) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btSimplePair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %struct.btSimplePair* %_Val, %struct.btSimplePair** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !29
  %1 = load i32, i32* %sz, align 4, !tbaa !29
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !33
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %2, i32 %3
  %4 = bitcast %struct.btSimplePair* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btSimplePair*
  %6 = load %struct.btSimplePair*, %struct.btSimplePair** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btSimplePair* %5 to i8*
  %8 = bitcast %struct.btSimplePair* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 12, i1 false), !tbaa.struct !75
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !33
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !33
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btSimplePair* @_ZN12btSimplePairC2Eii(%struct.btSimplePair* returned %this, i32 %indexA, i32 %indexB) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btSimplePair*, align 4
  %indexA.addr = alloca i32, align 4
  %indexB.addr = alloca i32, align 4
  store %struct.btSimplePair* %this, %struct.btSimplePair** %this.addr, align 4, !tbaa !2
  store i32 %indexA, i32* %indexA.addr, align 4, !tbaa !29
  store i32 %indexB, i32* %indexB.addr, align 4, !tbaa !29
  %this1 = load %struct.btSimplePair*, %struct.btSimplePair** %this.addr, align 4
  %m_indexA = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 0
  %0 = load i32, i32* %indexA.addr, align 4, !tbaa !29
  store i32 %0, i32* %m_indexA, align 4, !tbaa !41
  %m_indexB = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 1
  %1 = load i32, i32* %indexB.addr, align 4, !tbaa !29
  store i32 %1, i32* %m_indexB, align 4, !tbaa !43
  %2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %this1, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %2 to i8**
  store i8* null, i8** %m_userPointer, align 4, !tbaa !30
  ret %struct.btSimplePair* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE5clearEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: nounwind
define hidden float @_ZN36btCompoundCompoundCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btCompoundCompoundCollisionAlgorithm*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btCompoundCompoundCollisionAlgorithm* %this, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4, !tbaa !2
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4, !tbaa !2
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4, !tbaa !2
  %this1 = load %class.btCompoundCompoundCollisionAlgorithm*, %class.btCompoundCompoundCollisionAlgorithm** %this.addr, align 4
  ret float 0.000000e+00
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4, !tbaa !76
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !77
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #1

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN30btCompoundCompoundLeafCallbackD0Ev(%struct.btCompoundCompoundLeafCallback* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %call = call %struct.btCompoundCompoundLeafCallback* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to %struct.btCompoundCompoundLeafCallback* (%struct.btCompoundCompoundLeafCallback*)*)(%struct.btCompoundCompoundLeafCallback* %this1) #9
  %0 = bitcast %struct.btCompoundCompoundLeafCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden void @_ZN30btCompoundCompoundLeafCallback7ProcessEPK10btDbvtNodeS2_(%struct.btCompoundCompoundLeafCallback* %this, %struct.btDbvtNode* %leaf0, %struct.btDbvtNode* %leaf1) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btCompoundCompoundLeafCallback*, align 4
  %leaf0.addr = alloca %struct.btDbvtNode*, align 4
  %leaf1.addr = alloca %struct.btDbvtNode*, align 4
  %childIndex0 = alloca i32, align 4
  %childIndex1 = alloca i32, align 4
  %compoundShape0 = alloca %class.btCompoundShape*, align 4
  %compoundShape1 = alloca %class.btCompoundShape*, align 4
  %childShape0 = alloca %class.btCollisionShape*, align 4
  %childShape1 = alloca %class.btCollisionShape*, align 4
  %orgTrans0 = alloca %class.btTransform, align 4
  %childTrans0 = alloca %class.btTransform*, align 4
  %newChildWorldTrans0 = alloca %class.btTransform, align 4
  %orgTrans1 = alloca %class.btTransform, align 4
  %childTrans1 = alloca %class.btTransform*, align 4
  %newChildWorldTrans1 = alloca %class.btTransform, align 4
  %aabbMin0 = alloca %class.btVector3, align 4
  %aabbMax0 = alloca %class.btVector3, align 4
  %aabbMin1 = alloca %class.btVector3, align 4
  %aabbMax1 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %compoundWrap0 = alloca %struct.btCollisionObjectWrapper, align 4
  %compoundWrap1 = alloca %struct.btCollisionObjectWrapper, align 4
  %pair = alloca %struct.btSimplePair*, align 4
  %colAlgo = alloca %class.btCollisionAlgorithm*, align 4
  %tmpWrap0 = alloca %struct.btCollisionObjectWrapper*, align 4
  %tmpWrap1 = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCompoundCompoundLeafCallback* %this, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %leaf0, %struct.btDbvtNode** %leaf0.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %leaf1, %struct.btDbvtNode** %leaf1.addr, align 4, !tbaa !2
  %this1 = load %struct.btCompoundCompoundLeafCallback*, %struct.btCompoundCompoundLeafCallback** %this.addr, align 4
  %m_numOverlapPairs = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numOverlapPairs, align 4, !tbaa !56
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_numOverlapPairs, align 4, !tbaa !56
  %1 = bitcast i32* %childIndex0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf0.addr, align 4, !tbaa !2
  %3 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %2, i32 0, i32 2
  %dataAsInt = bitcast %union.anon.8* %3 to i32*
  %4 = load i32, i32* %dataAsInt, align 4, !tbaa !30
  store i32 %4, i32* %childIndex0, align 4, !tbaa !29
  %5 = bitcast i32* %childIndex1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf1.addr, align 4, !tbaa !2
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %dataAsInt2 = bitcast %union.anon.8* %7 to i32*
  %8 = load i32, i32* %dataAsInt2, align 4, !tbaa !30
  store i32 %8, i32* %childIndex1, align 4, !tbaa !29
  %9 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_compound0ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap, align 4, !tbaa !58
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %10)
  %11 = bitcast %class.btCollisionShape* %call to %class.btCompoundShape*
  store %class.btCompoundShape* %11, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %12 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %m_compound1ColObjWrap = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap, align 4, !tbaa !59
  %call3 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %13)
  %14 = bitcast %class.btCollisionShape* %call3 to %class.btCompoundShape*
  store %class.btCompoundShape* %14, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %15 = bitcast %class.btCollisionShape** %childShape0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %16 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %17 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %call4 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %16, i32 %17)
  store %class.btCollisionShape* %call4, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %18 = bitcast %class.btCollisionShape** %childShape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  %19 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %20 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %call5 = call %class.btCollisionShape* @_ZNK15btCompoundShape13getChildShapeEi(%class.btCompoundShape* %19, i32 %20)
  store %class.btCollisionShape* %call5, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %21 = bitcast %class.btTransform* %orgTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %21) #9
  %m_compound0ColObjWrap6 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %22 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap6, align 4, !tbaa !58
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %22)
  %call8 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %call7)
  %23 = bitcast %class.btTransform** %childTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  %24 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape0, align 4, !tbaa !2
  %25 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %24, i32 %25)
  store %class.btTransform* %call9, %class.btTransform** %childTrans0, align 4, !tbaa !2
  %26 = bitcast %class.btTransform* %newChildWorldTrans0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %26) #9
  %27 = load %class.btTransform*, %class.btTransform** %childTrans0, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans0, %class.btTransform* %orgTrans0, %class.btTransform* nonnull align 4 dereferenceable(64) %27)
  %28 = bitcast %class.btTransform* %orgTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %28) #9
  %m_compound1ColObjWrap10 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %29 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap10, align 4, !tbaa !59
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %29)
  %call12 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %call11)
  %30 = bitcast %class.btTransform** %childTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  %31 = load %class.btCompoundShape*, %class.btCompoundShape** %compoundShape1, align 4, !tbaa !2
  %32 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %call13 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %31, i32 %32)
  store %class.btTransform* %call13, %class.btTransform** %childTrans1, align 4, !tbaa !2
  %33 = bitcast %class.btTransform* %newChildWorldTrans1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %33) #9
  %34 = load %class.btTransform*, %class.btTransform** %childTrans1, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %newChildWorldTrans1, %class.btTransform* %orgTrans1, %class.btTransform* nonnull align 4 dereferenceable(64) %34)
  %35 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #9
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin0)
  %36 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #9
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax0)
  %37 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #9
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin1)
  %38 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax1)
  %39 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %40 = bitcast %class.btCollisionShape* %39 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %40, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %41 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %41(%class.btCollisionShape* %39, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0)
  %42 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %43 = bitcast %class.btCollisionShape* %42 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable18 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %43, align 4, !tbaa !8
  %vfn19 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable18, i64 2
  %44 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn19, align 4
  call void %44(%class.btCollisionShape* %42, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  %45 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundCompoundChildShapePairCallback, align 4, !tbaa !2
  %tobool = icmp ne i1 (%class.btCollisionShape*, %class.btCollisionShape*)* %45, null
  br i1 %tobool, label %if.then, label %if.end22

if.then:                                          ; preds = %entry
  %46 = load i1 (%class.btCollisionShape*, %class.btCollisionShape*)*, i1 (%class.btCollisionShape*, %class.btCollisionShape*)** @gCompoundCompoundChildShapePairCallback, align 4, !tbaa !2
  %47 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %48 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %call20 = call zeroext i1 %46(%class.btCollisionShape* %47, %class.btCollisionShape* %48)
  br i1 %call20, label %if.end, label %if.then21

if.then21:                                        ; preds = %if.then
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %if.then
  br label %if.end22

if.end22:                                         ; preds = %if.end, %entry
  %call23 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax0, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1)
  br i1 %call23, label %if.then24, label %if.end61

if.then24:                                        ; preds = %if.end22
  %49 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %49) #9
  %m_compound0ColObjWrap25 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %50 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap25, align 4, !tbaa !58
  %51 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape0, align 4, !tbaa !2
  %m_compound0ColObjWrap26 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 2
  %52 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound0ColObjWrap26, align 4, !tbaa !58
  %call27 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %52)
  %53 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %call28 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %50, %class.btCollisionShape* %51, %class.btCollisionObject* %call27, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans0, i32 -1, i32 %53)
  %54 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 24, i8* %54) #9
  %m_compound1ColObjWrap29 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %55 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap29, align 4, !tbaa !59
  %56 = load %class.btCollisionShape*, %class.btCollisionShape** %childShape1, align 4, !tbaa !2
  %m_compound1ColObjWrap30 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 3
  %57 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_compound1ColObjWrap30, align 4, !tbaa !59
  %call31 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %57)
  %58 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %call32 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %compoundWrap1, %struct.btCollisionObjectWrapper* %55, %class.btCollisionShape* %56, %class.btCollisionObject* %call31, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildWorldTrans1, i32 -1, i32 %58)
  %59 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %m_childCollisionAlgorithmCache = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %60 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache, align 4, !tbaa !62
  %61 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %62 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %call33 = call %struct.btSimplePair* @_ZN23btHashedSimplePairCache8findPairEii(%class.btHashedSimplePairCache* %60, i32 %61, i32 %62)
  store %struct.btSimplePair* %call33, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %63 = bitcast %class.btCollisionAlgorithm** %colAlgo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #9
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %64 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %tobool34 = icmp ne %struct.btSimplePair* %64, null
  br i1 %tobool34, label %if.then35, label %if.else

if.then35:                                        ; preds = %if.then24
  %65 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %66 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %65, i32 0, i32 2
  %m_userPointer = bitcast %union.anon* %66 to i8**
  %67 = load i8*, i8** %m_userPointer, align 4, !tbaa !30
  %68 = bitcast i8* %67 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %68, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  br label %if.end44

if.else:                                          ; preds = %if.then24
  %m_dispatcher = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 4
  %69 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !60
  %m_sharedManifold = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 8
  %70 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_sharedManifold, align 4, !tbaa !63
  %71 = bitcast %class.btDispatcher* %69 to %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)***
  %vtable36 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)**, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*** %71, align 4, !tbaa !8
  %vfn37 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vtable36, i64 2
  %72 = load %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)*, %class.btCollisionAlgorithm* (%class.btDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*)** %vfn37, align 4
  %call38 = call %class.btCollisionAlgorithm* %72(%class.btDispatcher* %69, %struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %compoundWrap1, %class.btPersistentManifold* %70)
  store %class.btCollisionAlgorithm* %call38, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %m_childCollisionAlgorithmCache39 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 7
  %73 = load %class.btHashedSimplePairCache*, %class.btHashedSimplePairCache** %m_childCollisionAlgorithmCache39, align 4, !tbaa !62
  %74 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %75 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %76 = bitcast %class.btHashedSimplePairCache* %73 to %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)***
  %vtable40 = load %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)**, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*** %76, align 4, !tbaa !8
  %vfn41 = getelementptr inbounds %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)** %vtable40, i64 3
  %77 = load %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)*, %struct.btSimplePair* (%class.btHashedSimplePairCache*, i32, i32)** %vfn41, align 4
  %call42 = call %struct.btSimplePair* %77(%class.btHashedSimplePairCache* %73, i32 %74, i32 %75)
  store %struct.btSimplePair* %call42, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %78 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %79 = bitcast %class.btCollisionAlgorithm* %78 to i8*
  %80 = load %struct.btSimplePair*, %struct.btSimplePair** %pair, align 4, !tbaa !2
  %81 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %80, i32 0, i32 2
  %m_userPointer43 = bitcast %union.anon* %81 to i8**
  store i8* %79, i8** %m_userPointer43, align 4, !tbaa !30
  br label %if.end44

if.end44:                                         ; preds = %if.else, %if.then35
  %82 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4, !tbaa !2
  %83 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #9
  store %struct.btCollisionObjectWrapper* null, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4, !tbaa !2
  %m_resultOut = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %84 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut, align 4, !tbaa !61
  %call45 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %84)
  store %struct.btCollisionObjectWrapper* %call45, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4, !tbaa !2
  %m_resultOut46 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %85 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut46, align 4, !tbaa !61
  %call47 = call %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %85)
  store %struct.btCollisionObjectWrapper* %call47, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4, !tbaa !2
  %m_resultOut48 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %86 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut48, align 4, !tbaa !61
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %86, %struct.btCollisionObjectWrapper* %compoundWrap0)
  %m_resultOut49 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %87 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut49, align 4, !tbaa !61
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %87, %struct.btCollisionObjectWrapper* %compoundWrap1)
  %m_resultOut50 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %88 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut50, align 4, !tbaa !61
  %89 = load i32, i32* %childIndex0, align 4, !tbaa !29
  %90 = bitcast %class.btManifoldResult* %88 to void (%class.btManifoldResult*, i32, i32)***
  %vtable51 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %90, align 4, !tbaa !8
  %vfn52 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable51, i64 2
  %91 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn52, align 4
  call void %91(%class.btManifoldResult* %88, i32 -1, i32 %89)
  %m_resultOut53 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %92 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut53, align 4, !tbaa !61
  %93 = load i32, i32* %childIndex1, align 4, !tbaa !29
  %94 = bitcast %class.btManifoldResult* %92 to void (%class.btManifoldResult*, i32, i32)***
  %vtable54 = load void (%class.btManifoldResult*, i32, i32)**, void (%class.btManifoldResult*, i32, i32)*** %94, align 4, !tbaa !8
  %vfn55 = getelementptr inbounds void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vtable54, i64 3
  %95 = load void (%class.btManifoldResult*, i32, i32)*, void (%class.btManifoldResult*, i32, i32)** %vfn55, align 4
  call void %95(%class.btManifoldResult* %92, i32 -1, i32 %93)
  %96 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %colAlgo, align 4, !tbaa !2
  %m_dispatchInfo = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 5
  %97 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4, !tbaa !78
  %m_resultOut56 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %98 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut56, align 4, !tbaa !61
  %99 = bitcast %class.btCollisionAlgorithm* %96 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable57 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %99, align 4, !tbaa !8
  %vfn58 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable57, i64 2
  %100 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn58, align 4
  call void %100(%class.btCollisionAlgorithm* %96, %struct.btCollisionObjectWrapper* %compoundWrap0, %struct.btCollisionObjectWrapper* %compoundWrap1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %97, %class.btManifoldResult* %98)
  %m_resultOut59 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %101 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut59, align 4, !tbaa !61
  %102 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap0, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %101, %struct.btCollisionObjectWrapper* %102)
  %m_resultOut60 = getelementptr inbounds %struct.btCompoundCompoundLeafCallback, %struct.btCompoundCompoundLeafCallback* %this1, i32 0, i32 6
  %103 = load %class.btManifoldResult*, %class.btManifoldResult** %m_resultOut60, align 4, !tbaa !61
  %104 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %tmpWrap1, align 4, !tbaa !2
  call void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %103, %struct.btCollisionObjectWrapper* %104)
  %105 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast %struct.btCollisionObjectWrapper** %tmpWrap0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast %class.btCollisionAlgorithm** %colAlgo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast %struct.btSimplePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %109) #9
  %110 = bitcast %struct.btCollisionObjectWrapper* %compoundWrap0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 24, i8* %110) #9
  br label %if.end61

if.end61:                                         ; preds = %if.end44, %if.end22
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end61, %if.then21
  %111 = bitcast %class.btVector3* %aabbMax1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #9
  %112 = bitcast %class.btVector3* %aabbMin1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %112) #9
  %113 = bitcast %class.btVector3* %aabbMax0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %113) #9
  %114 = bitcast %class.btVector3* %aabbMin0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %114) #9
  %115 = bitcast %class.btTransform* %newChildWorldTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %115) #9
  %116 = bitcast %class.btTransform** %childTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #9
  %117 = bitcast %class.btTransform* %orgTrans1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %117) #9
  %118 = bitcast %class.btTransform* %newChildWorldTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %118) #9
  %119 = bitcast %class.btTransform** %childTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #9
  %120 = bitcast %class.btTransform* %orgTrans0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %120) #9
  %121 = bitcast %class.btCollisionShape** %childShape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #9
  %122 = bitcast %class.btCollisionShape** %childShape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #9
  %123 = bitcast %class.btCompoundShape** %compoundShape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #9
  %124 = bitcast %class.btCompoundShape** %compoundShape0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #9
  %125 = bitcast i32* %childIndex1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #9
  %126 = bitcast i32* %childIndex0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4, !tbaa !2
  store float %0, float* %.addr, align 4, !tbaa !74
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4, !tbaa !2
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #9
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4, !tbaa !2
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !73
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !29
  store i32 %index, i32* %index.addr, align 4, !tbaa !29
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4, !tbaa !79
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4, !tbaa !21
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !77
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4, !tbaa !2
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4, !tbaa !2
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4, !tbaa !29
  store i32 %4, i32* %m_partId, align 4, !tbaa !80
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4, !tbaa !29
  store i32 %5, i32* %m_index, align 4, !tbaa !81
  ret %struct.btCollisionObjectWrapper* %this1
}

declare %struct.btSimplePair* @_ZN23btHashedSimplePairCache8findPairEii(%class.btHashedSimplePairCache*, i32, i32) #1

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody0WrapEv(%class.btManifoldResult* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !52
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZNK16btManifoldResult12getBody1WrapEv(%class.btManifoldResult* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !54
  ret %struct.btCollisionObjectWrapper* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody0WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj0Wrap) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4, !tbaa !2
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4, !tbaa !52
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btManifoldResult12setBody1WrapEPK24btCollisionObjectWrapper(%class.btManifoldResult* %this, %struct.btCollisionObjectWrapper* %obj1Wrap) #4 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4, !tbaa !2
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4, !tbaa !54
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #7 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !73
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !73
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !73
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !74
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !74
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !74
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !74
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !74
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !74
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !74
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !74
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !74
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !73
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !74
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !74
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !74
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !74
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !74
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !74
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !74
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !29
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !74
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !74
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !74
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !74
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !74
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !74
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !74
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !74
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !74
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !74
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !74
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !74
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !74
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !74
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !74
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !74
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !74
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !74
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #9
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !74
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !74
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !74
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !2
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !2
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !74
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #9
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !74
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !74
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !74
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !74
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !74
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !74
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !74
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !74
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !74
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !74
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !74
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !74
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !74
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !74
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !74
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !74
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !74
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !74
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !74
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !74
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !74
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !74
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !74
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !74
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !74
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !74
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !74
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !74
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !74
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !74
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %this, i32 %newsize, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !29
  store %"struct.btDbvt::sStkNN"* %fillData, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !29
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %2 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  store i32 %4, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %6 = load i32, i32* %curSize, align 4, !tbaa !29
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %8 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = load i32, i32* %curSize, align 4, !tbaa !29
  store i32 %14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !29
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %18 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data11, align 4, !tbaa !82
  %19 = load i32, i32* %i6, align 4, !tbaa !29
  %arrayidx12 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %18, i32 %19
  %20 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %"struct.btDbvt::sStkNN"*
  %22 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %"struct.btDbvt::sStkNN"* %21 to i8*
  %24 = bitcast %"struct.btDbvt::sStkNN"* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 8, i1 false), !tbaa.struct !65
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !29
  %inc14 = add nsw i32 %25, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !29
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !29
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !83
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* returned %this, %struct.btDbvtNode* %na, %struct.btDbvtNode* %nb) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %na.addr = alloca %struct.btDbvtNode*, align 4
  %nb.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %na, %struct.btDbvtNode** %na.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %nb, %struct.btDbvtNode** %nb.addr, align 4, !tbaa !2
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %na.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %a, align 4, !tbaa !66
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %nb.addr, align 4, !tbaa !2
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %b, align 4, !tbaa !68
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %0, i32 %1
  ret %"struct.btDbvt::sStkNN"* %arrayidx
}

; Function Attrs: inlinehint
define internal zeroext i1 @_ZL11MyIntersectRK12btDbvtAabbMmS1_RK11btTransform(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b, %class.btTransform* nonnull align 4 dereferenceable(64) %xform) #7 {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  %newmin = alloca %class.btVector3, align 4
  %newmax = alloca %class.btVector3, align 4
  %newb = alloca %struct.btDbvtAabbMm, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %newmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newmin)
  %1 = bitcast %class.btVector3* %newmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newmax)
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %2)
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %3)
  %4 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4, !tbaa !2
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, float 0.000000e+00, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %newmin, %class.btVector3* nonnull align 4 dereferenceable(16) %newmax)
  %5 = bitcast %struct.btDbvtAabbMm* %newb to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %5) #9
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %newb, %class.btVector3* nonnull align 4 dereferenceable(16) %newmin, %class.btVector3* nonnull align 4 dereferenceable(16) %newmax)
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %call4 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %6, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %newb)
  %7 = bitcast %struct.btDbvtAabbMm* %newb to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %7) #9
  %8 = bitcast %class.btVector3* %newmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %newmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  ret i1 %call4
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !83
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #7 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator.11* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  ret %class.btAlignedAllocator.11* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !84
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !83
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !85
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray.10* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"struct.btDbvt::sStkNN"** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray.10* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %"struct.btDbvt::sStkNN"*
  store %"struct.btDbvt::sStkNN"* %3, %"struct.btDbvt::sStkNN"** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %4 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call3, %"struct.btDbvt::sStkNN"* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !84
  %5 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* %5, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !85
  %7 = bitcast %"struct.btDbvt::sStkNN"** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray.10* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !85
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray.10* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %m_allocator, i32 %1, %"struct.btDbvt::sStkNN"** null)
  %2 = bitcast %"struct.btDbvt::sStkNN"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this, i32 %start, i32 %end, %"struct.btDbvt::sStkNN"* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %"struct.btDbvt::sStkNN"* %dest, %"struct.btDbvt::sStkNN"** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %4, i32 %5
  %6 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx to i8*
  %7 = bitcast i8* %6 to %"struct.btDbvt::sStkNN"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %8 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %8, i32 %9
  %10 = bitcast %"struct.btDbvt::sStkNN"* %7 to i8*
  %11 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 8, i1 false), !tbaa.struct !65
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.10* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %4 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.10* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4, !tbaa !82
  %tobool = icmp ne %"struct.btDbvt::sStkNN"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !84, !range !53
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data4, align 4, !tbaa !82
  call void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %m_allocator, %"struct.btDbvt::sStkNN"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data5, align 4, !tbaa !82
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %this, i32 %n, %"struct.btDbvt::sStkNN"** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btDbvt::sStkNN"**, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %"struct.btDbvt::sStkNN"** %hint, %"struct.btDbvt::sStkNN"*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btDbvt::sStkNN"*
  ret %"struct.btDbvt::sStkNN"* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %this, %"struct.btDbvt::sStkNN"* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %ptr.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4, !tbaa !2
  store %"struct.btDbvt::sStkNN"* %ptr, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"struct.btDbvt::sStkNN"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #7 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !74
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 5.000000e-01, float* %ref.tmp, align 4, !tbaa !74
  %2 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %5 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #9
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !74
  %11 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %12 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %16) #9
  %17 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %18 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #9
  %19 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %20 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #9
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  %21 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %22 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %22 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !73
  %25 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #9
  %26 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %27 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !73
  %30 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  %31 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  %32 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #9
  %33 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %33) #9
  %34 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #9
  %35 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  ret %class.btVector3* %mi
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  ret %class.btVector3* %mx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #7 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4, !tbaa !2
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4, !tbaa !2
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !73
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4, !tbaa !2
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !73
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #3 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4, !tbaa !74
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 1
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %3 = load float, float* %call1, align 4, !tbaa !74
  %cmp = fcmp ole float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %5 = load float, float* %call3, align 4, !tbaa !74
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi4)
  %7 = load float, float* %call5, align 4, !tbaa !74
  %cmp6 = fcmp oge float %5, %7
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi8)
  %9 = load float, float* %call9, align 4, !tbaa !74
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx10)
  %11 = load float, float* %call11, align 4, !tbaa !74
  %cmp12 = fcmp ole float %9, %11
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true7
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %13 = load float, float* %call15, align 4, !tbaa !74
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 0
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi16)
  %15 = load float, float* %call17, align 4, !tbaa !74
  %cmp18 = fcmp oge float %13, %15
  br i1 %cmp18, label %land.lhs.true19, label %land.end

land.lhs.true19:                                  ; preds = %land.lhs.true13
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi20)
  %17 = load float, float* %call21, align 4, !tbaa !74
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4, !tbaa !74
  %cmp24 = fcmp ole float %17, %19
  br i1 %cmp24, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4, !tbaa !2
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4, !tbaa !74
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4, !tbaa !2
  %mi27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 0
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi27)
  %23 = load float, float* %call28, align 4, !tbaa !74
  %cmp29 = fcmp oge float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true19, %land.lhs.true13, %land.lhs.true7, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true19 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp29, %land.rhs ]
  ret i1 %24
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !74
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !74
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !74
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !74
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !74
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !74
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !74
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !74
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !74
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !74
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !74
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !74
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !74
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !74
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !74
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !74
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !74
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !74
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !74
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !74
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !74
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !74
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !74
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !74
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !74
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !74
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !74
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !74
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !74
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !74
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !74
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !74
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !74
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !74
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !74
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !74
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #9
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !29
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !74
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !74
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !74
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !74
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !74
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !74
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !74
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !74
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !74
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !74
  %0 = load float, float* %x.addr, align 4, !tbaa !74
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.8* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4, !tbaa !30
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray.10* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4, !tbaa !86
  %1 = load i32, i32* %n.addr, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !73
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !73
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !73
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI12btSimplePairLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !33
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !88
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.19* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.19* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  ret %class.btAlignedAllocator.19* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !89
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !44
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !90
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.18* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.18* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.18* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.18* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !89, !range !53
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4, !tbaa !47
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.19* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4, !tbaa !47
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.19* %this, %class.btPersistentManifold** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.18* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.18* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.18* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %3, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.18* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.18* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.18* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !89
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %5, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !90
  %7 = bitcast %class.btPersistentManifold*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.18* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !90
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.18* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.19* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.18* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.18*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.18* %this, %class.btAlignedObjectArray.18** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.18*, %class.btAlignedObjectArray.18** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %4, i32 %5
  %6 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.18, %class.btAlignedObjectArray.18* %this1, i32 0, i32 4
  %8 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4, !tbaa !47
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %8, i32 %9
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4, !tbaa !2
  store %class.btPersistentManifold* %10, %class.btPersistentManifold** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  ret void
}

define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.19* %this, i32 %n, %class.btPersistentManifold*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.19*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.19* %this, %class.btAlignedAllocator.19** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.19*, %class.btAlignedAllocator.19** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !88
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btSimplePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %call2 = call i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btSimplePair*
  store %struct.btSimplePair* %3, %struct.btSimplePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btSimplePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI12btSimplePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !87
  %5 = load %struct.btSimplePair*, %struct.btSimplePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* %5, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !29
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !88
  %7 = bitcast %struct.btSimplePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI12btSimplePairE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI12btSimplePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !29
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !29
  %call = call %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btSimplePair** null)
  %2 = bitcast %struct.btSimplePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI12btSimplePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btSimplePair* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btSimplePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !29
  store i32 %end, i32* %end.addr, align 4, !tbaa !29
  store %struct.btSimplePair* %dest, %struct.btSimplePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %start.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %end.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %4, i32 %5
  %6 = bitcast %struct.btSimplePair* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btSimplePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %9 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %8, i32 %9
  %10 = bitcast %struct.btSimplePair* %7 to i8*
  %11 = bitcast %struct.btSimplePair* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 12, i1 false), !tbaa.struct !75
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !29
  store i32 %last, i32* %last.addr, align 4, !tbaa !29
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load i32, i32* %first.addr, align 4, !tbaa !29
  store i32 %1, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !29
  %3 = load i32, i32* %last.addr, align 4, !tbaa !29
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %5 = load i32, i32* %i, align 4, !tbaa !29
  %arrayidx = getelementptr inbounds %struct.btSimplePair, %struct.btSimplePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !29
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !29
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI12btSimplePairE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data, align 4, !tbaa !34
  %tobool = icmp ne %struct.btSimplePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !87, !range !53
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btSimplePair*, %struct.btSimplePair** %m_data4, align 4, !tbaa !34
  call void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btSimplePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btSimplePair* null, %struct.btSimplePair** %m_data5, align 4, !tbaa !34
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btSimplePair* @_ZN18btAlignedAllocatorI12btSimplePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btSimplePair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btSimplePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !29
  store %struct.btSimplePair** %hint, %struct.btSimplePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !29
  %mul = mul i32 12, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btSimplePair*
  ret %struct.btSimplePair* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI12btSimplePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btSimplePair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btSimplePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btSimplePair* %ptr, %struct.btSimplePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btSimplePair*, %struct.btSimplePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btSimplePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"bool", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !3, i64 4}
!11 = !{!"_ZTS36btCollisionAlgorithmConstructionInfo", !3, i64 0, !3, i64 4}
!12 = !{!13, !3, i64 32}
!13 = !{!"_ZTS36btCompoundCompoundCollisionAlgorithm", !3, i64 8, !14, i64 12, !3, i64 32, !7, i64 36, !16, i64 40, !16, i64 44}
!14 = !{!"_ZTS20btAlignedObjectArrayI12btSimplePairE", !15, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!15 = !{!"_ZTS18btAlignedAllocatorI12btSimplePairLj16EE"}
!16 = !{!"int", !4, i64 0}
!17 = !{!13, !7, i64 36}
!18 = !{!13, !3, i64 8}
!19 = !{!13, !16, i64 40}
!20 = !{!13, !16, i64 44}
!21 = !{!22, !3, i64 4}
!22 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !16, i64 16, !16, i64 20}
!23 = !{!24, !16, i64 68}
!24 = !{!"_ZTS15btCompoundShape", !25, i64 12, !27, i64 32, !27, i64 48, !3, i64 64, !16, i64 68, !28, i64 72, !27, i64 76}
!25 = !{!"_ZTS20btAlignedObjectArrayI20btCompoundShapeChildE", !26, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!26 = !{!"_ZTS18btAlignedAllocatorI20btCompoundShapeChildLj16EE"}
!27 = !{!"_ZTS9btVector3", !4, i64 0}
!28 = !{!"float", !4, i64 0}
!29 = !{!16, !16, i64 0}
!30 = !{!4, !4, i64 0}
!31 = !{!32, !3, i64 4}
!32 = !{!"_ZTS20btCollisionAlgorithm", !3, i64 4}
!33 = !{!14, !16, i64 4}
!34 = !{!14, !3, i64 12}
!35 = !{!36, !3, i64 0}
!36 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !16, i64 8, !16, i64 12, !16, i64 16, !37, i64 20, !39, i64 40}
!37 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !38, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!38 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!39 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !40, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!40 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!41 = !{!42, !16, i64 0}
!42 = !{!"_ZTS12btSimplePair", !16, i64 0, !16, i64 4, !4, i64 8}
!43 = !{!42, !16, i64 4}
!44 = !{!45, !16, i64 4}
!45 = !{!"_ZTS20btAlignedObjectArrayIP20btPersistentManifoldE", !46, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !7, i64 16}
!46 = !{!"_ZTS18btAlignedAllocatorIP20btPersistentManifoldLj16EE"}
!47 = !{!45, !3, i64 12}
!48 = !{!49, !16, i64 748}
!49 = !{!"_ZTS20btPersistentManifold", !4, i64 4, !3, i64 740, !3, i64 744, !16, i64 748, !28, i64 752, !28, i64 756, !16, i64 760, !16, i64 764, !16, i64 768}
!50 = !{!51, !3, i64 4}
!51 = !{!"_ZTS16btManifoldResult", !3, i64 4, !3, i64 8, !3, i64 12, !16, i64 16, !16, i64 20, !16, i64 24, !16, i64 28}
!52 = !{!51, !3, i64 8}
!53 = !{i8 0, i8 2}
!54 = !{!51, !3, i64 12}
!55 = !{!24, !3, i64 64}
!56 = !{!57, !16, i64 4}
!57 = !{!"_ZTS30btCompoundCompoundLeafCallback", !16, i64 4, !3, i64 8, !3, i64 12, !3, i64 16, !3, i64 20, !3, i64 24, !3, i64 28, !3, i64 32}
!58 = !{!57, !3, i64 8}
!59 = !{!57, !3, i64 12}
!60 = !{!57, !3, i64 16}
!61 = !{!57, !3, i64 24}
!62 = !{!57, !3, i64 28}
!63 = !{!57, !3, i64 32}
!64 = !{!22, !3, i64 12}
!65 = !{i64 0, i64 4, !2, i64 4, i64 4, !2}
!66 = !{!67, !3, i64 0}
!67 = !{!"_ZTSN6btDbvt6sStkNNE", !3, i64 0, !3, i64 4}
!68 = !{!67, !3, i64 4}
!69 = !{!70, !3, i64 64}
!70 = !{!"_ZTS20btCompoundShapeChild", !71, i64 0, !3, i64 64, !16, i64 68, !28, i64 72, !3, i64 76}
!71 = !{!"_ZTS11btTransform", !72, i64 0, !27, i64 48}
!72 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!73 = !{i64 0, i64 16, !30}
!74 = !{!28, !28, i64 0}
!75 = !{i64 0, i64 4, !29, i64 4, i64 4, !29, i64 8, i64 4, !2, i64 8, i64 4, !29}
!76 = !{!49, !3, i64 740}
!77 = !{!22, !3, i64 8}
!78 = !{!57, !3, i64 20}
!79 = !{!22, !3, i64 0}
!80 = !{!22, !16, i64 16}
!81 = !{!22, !16, i64 20}
!82 = !{!37, !3, i64 12}
!83 = !{!37, !16, i64 4}
!84 = !{!37, !7, i64 16}
!85 = !{!37, !16, i64 8}
!86 = !{!25, !3, i64 12}
!87 = !{!14, !7, i64 16}
!88 = !{!14, !16, i64 8}
!89 = !{!45, !7, i64 16}
!90 = !{!45, !16, i64 8}
