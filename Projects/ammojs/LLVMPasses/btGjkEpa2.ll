; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpa2.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkEpa2.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%"struct.btGjkEpaSolver2::sResults" = type { i32, [2 x %class.btVector3], %class.btVector3, float }
%"struct.gjkepa2_impl::MinkowskiDiff" = type { [2 x %class.btConvexShape*], %class.btMatrix3x3, %class.btTransform, { i32, i32 } }
%"struct.gjkepa2_impl::GJK" = type { %"struct.gjkepa2_impl::MinkowskiDiff", %class.btVector3, float, [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"*], i32, i32, %"struct.gjkepa2_impl::GJK::sSimplex"*, i32 }
%"struct.gjkepa2_impl::GJK::sSimplex" = type { [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x float], i32 }
%"struct.gjkepa2_impl::GJK::sSV" = type { %class.btVector3, %class.btVector3 }
%"struct.gjkepa2_impl::EPA" = type { i32, %"struct.gjkepa2_impl::GJK::sSimplex", %class.btVector3, float, [64 x %"struct.gjkepa2_impl::GJK::sSV"], [128 x %"struct.gjkepa2_impl::EPA::sFace"], i32, %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList" }
%"struct.gjkepa2_impl::EPA::sFace" = type { %class.btVector3, float, [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x i8], i8 }
%"struct.gjkepa2_impl::EPA::sList" = type { %"struct.gjkepa2_impl::EPA::sFace"*, i32 }
%"struct.gjkepa2_impl::EPA::sHorizon" = type { %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"*, i32 }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }

$_ZN12gjkepa2_impl13MinkowskiDiffC2Ev = comdat any

$_ZN12gjkepa2_impl3GJKC2Ev = comdat any

$_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN12gjkepa2_impl3EPAC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3 = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN11btTransformC2ERK12btQuaternionRK9btVector3 = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x314transposeTimesERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btTransform12inverseTimesERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN12gjkepa2_impl3GJK3sSVC2Ev = comdat any

$_ZN12gjkepa2_impl3GJK10InitializeEv = comdat any

$_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3 = comdat any

$_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj = comdat any

$_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3 = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3 = comdat any

$_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3 = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_ = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN12gjkepa2_impl3EPA5sFaceC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA5sListC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA10InitializeEv = comdat any

$_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE = comdat any

$_ZN12gjkepa2_impl3GJK13EncloseOriginEv = comdat any

$_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE = comdat any

$_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_ = comdat any

$_Z6btSwapIfEvRT_S1_ = comdat any

$_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b = comdat any

$_ZN12gjkepa2_impl3EPA8findbestEv = comdat any

$_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j = comdat any

$_ZN12gjkepa2_impl3EPA8sHorizonC2Ev = comdat any

$_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3 = comdat any

$_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3 = comdat any

$_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3 = comdat any

$_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3 = comdat any

@_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3 = linkonce_odr hidden constant [3 x i32] [i32 1, i32 2, i32 0], comdat, align 4
@_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3 = linkonce_odr hidden constant [3 x i32] [i32 2, i32 0, i32 1], comdat, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4

; Function Attrs: nounwind
define hidden i32 @_ZN15btGjkEpaSolver220StackSizeRequirementEv() #0 {
entry:
  ret i32 9676
}

define hidden zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #1 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %w1 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %i = alloca i32, align 4
  %p = alloca float, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4, !tbaa !2
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %0 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #9
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %1 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %2 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %3 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  %4 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  %5 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %2, %class.btConvexShape* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %5, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext false)
  %6 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.start.p0i8(i64 380, i8* %6) #9
  %call1 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  %7 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store i32 %call2, i32* %gjk_status, align 4, !tbaa !6
  %9 = load i32, i32* %gjk_status, align 4, !tbaa !6
  %cmp = icmp eq i32 %9, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %10 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %12 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %13 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #9
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast %class.btVector3* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #9
  %18 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %19 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %20 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w1, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %21 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %25 = load i32, i32* %i, align 4, !tbaa !10
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %26 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4, !tbaa !12
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %26, i32 0, i32 2
  %27 = load i32, i32* %rank, align 4, !tbaa !18
  %cmp10 = icmp ult i32 %25, %27
  br i1 %cmp10, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %28 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %29 = bitcast float* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %m_simplex11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %30 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex11, align 4, !tbaa !12
  %p12 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %30, i32 0, i32 1
  %31 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p12, i32 0, i32 %31
  %32 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %32, float* %p, align 4, !tbaa !8
  %33 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #9
  %34 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #9
  %m_simplex15 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %35 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex15, align 4, !tbaa !12
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %35, i32 0, i32 0
  %36 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx16 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %36
  %37 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx16, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %37, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp14, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %p)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %38 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #9
  %39 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #9
  %40 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  %41 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #9
  %42 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #9
  %m_simplex21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %43 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex21, align 4, !tbaa !12
  %c22 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %43, i32 0, i32 0
  %44 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx23 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c22, i32 0, i32 %44
  %45 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx23, align 4, !tbaa !2
  %d24 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %45, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %d24)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp19, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20, i32 1)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %p)
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18)
  %46 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #9
  %47 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %48 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #9
  %49 = bitcast float* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %50 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %50, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %51 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #9
  %52 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp26, %class.btTransform* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %53 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %53, i32 0, i32 1
  %arrayidx27 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %54 = bitcast %class.btVector3* %arrayidx27 to i8*
  %55 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 16, i1 false), !tbaa.struct !20
  %56 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #9
  %57 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #9
  %58 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp28, %class.btTransform* %58, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %59 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses29 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %59, i32 0, i32 1
  %arrayidx30 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses29, i32 0, i32 1
  %60 = bitcast %class.btVector3* %arrayidx30 to i8*
  %61 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false), !tbaa.struct !20
  %62 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #9
  %63 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %63) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %64 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %64, i32 0, i32 2
  %65 = bitcast %class.btVector3* %normal to i8*
  %66 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false), !tbaa.struct !20
  %67 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #9
  %68 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal32 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %68, i32 0, i32 2
  %call33 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %normal32)
  %69 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %distance = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %69, i32 0, i32 3
  store float %call33, float* %distance, align 4, !tbaa !22
  %70 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %distance35 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %71, i32 0, i32 3
  %72 = load float, float* %distance35, align 4, !tbaa !22
  %cmp36 = fcmp ogt float %72, 0x3F1A36E2E0000000
  br i1 %cmp36, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %73 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %distance37 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %73, i32 0, i32 3
  %74 = load float, float* %distance37, align 4, !tbaa !22
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %74, %cond.true ], [ 1.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp34, align 4, !tbaa !8
  %75 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal38 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %75, i32 0, i32 2
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %normal38, float* nonnull align 4 dereferenceable(4) %ref.tmp34)
  %76 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #9
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %77 = bitcast %class.btVector3* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %77) #9
  %78 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #9
  br label %cleanup

if.else:                                          ; preds = %entry
  %79 = load i32, i32* %gjk_status, align 4, !tbaa !6
  %cmp40 = icmp eq i32 %79, 1
  %80 = zext i1 %cmp40 to i64
  %cond41 = select i1 %cmp40, i32 1, i32 2
  %81 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %81, i32 0, i32 0
  store i32 %cond41, i32* %status, align 4, !tbaa !25
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else, %cond.end
  %82 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #9
  %83 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.end.p0i8(i64 380, i8* %83) #9
  %84 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %84) #9
  %85 = load i1, i1* %retval, align 1
  ret i1 %85
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_toshape1)
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_toshape0)
  ret %"struct.gjkepa2_impl::MinkowskiDiff"* %this1
}

define internal void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext %withmargins) #1 {
entry:
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %withmargins.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca %class.btMatrix3x3, align 4
  %ref.tmp13 = alloca %class.btTransform, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %frombool = zext i1 %withmargins to i8
  store i8 %frombool, i8* %withmargins.addr, align 1, !tbaa !26
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %4, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 1
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !20
  %7 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses4 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %7, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses4, i32 0, i32 0
  %8 = bitcast %class.btVector3* %arrayidx5 to i8*
  %9 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !20
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  %14 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %14, i32 0, i32 0
  store i32 0, i32* %status, align 4, !tbaa !25
  %15 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %16 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %16, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 0
  store %class.btConvexShape* %15, %class.btConvexShape** %arrayidx6, align 4, !tbaa !2
  %17 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  %18 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %m_shapes7 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %18, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes7, i32 0, i32 1
  store %class.btConvexShape* %17, %class.btConvexShape** %arrayidx8, align 4, !tbaa !2
  %19 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %19) #9
  %20 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %20)
  %21 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %21)
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp9, %class.btMatrix3x3* %call10, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call11)
  %22 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %22, i32 0, i32 1
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_toshape1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp9)
  %23 = bitcast %class.btMatrix3x3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %23) #9
  %24 = bitcast %class.btTransform* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %24) #9
  %25 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %26 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  call void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* sret align 4 %ref.tmp13, %class.btTransform* %25, %class.btTransform* nonnull align 4 dereferenceable(64) %26)
  %27 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %27, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_toshape0, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp13)
  %28 = bitcast %class.btTransform* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %28) #9
  %29 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shape.addr, align 4, !tbaa !2
  %30 = load i8, i8* %withmargins.addr, align 1, !tbaa !26, !range !28
  %tobool = trunc i8 %30 to i1
  call void @_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb(%"struct.gjkepa2_impl::MinkowskiDiff"* %29, i1 zeroext %tobool)
  ret void
}

define linkonce_odr hidden %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  store %"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK"** %retval, align 4
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape)
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_ray)
  %m_simplices = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_store = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %array.begin = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"struct.gjkepa2_impl::GJK::sSV"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call3 = call %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  call void @_ZN12gjkepa2_impl3GJK10InitializeEv(%"struct.gjkepa2_impl::GJK"* %this1)
  %0 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %retval, align 4
  ret %"struct.gjkepa2_impl::GJK"* %0
}

define linkonce_odr hidden i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shapearg, %class.btVector3* nonnull align 4 dereferenceable(16) %guess) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %shapearg.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %iterations = alloca i32, align 4
  %sqdist = alloca float, align 4
  %alpha = alloca float, align 4
  %lastw = alloca [4 x %class.btVector3], align 16
  %clastw = alloca i32, align 4
  %sqrl = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %next = alloca i32, align 4
  %cs = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %ns = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %rl = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %w50 = alloca %class.btVector3*, align 4
  %found = alloca i8, align 1
  %i = alloca i32, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %omega = alloca float, align 4
  %weights = alloca [4 x float], align 16
  %mask = alloca i32, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %i125 = alloca i32, align 4
  %ni = alloca i32, align 4
  %ref.tmp144 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %shapearg, %"struct.gjkepa2_impl::MinkowskiDiff"** %shapearg.addr, align 4, !tbaa !2
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  store i32 0, i32* %iterations, align 4, !tbaa !10
  %1 = bitcast float* %sqdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %sqdist, align 4, !tbaa !8
  %2 = bitcast float* %alpha to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %alpha, align 4, !tbaa !8
  %3 = bitcast [4 x %class.btVector3]* %lastw to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #9
  %array.begin = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %4 = bitcast i32* %clastw to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  store i32 0, i32* %clastw, align 4, !tbaa !10
  %m_store = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store, i32 0, i32 0
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4, !tbaa !2
  %m_store3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store3, i32 0, i32 1
  %m_free5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx6 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free5, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx4, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4, !tbaa !2
  %m_store7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store7, i32 0, i32 2
  %m_free9 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx10 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free9, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx8, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx10, align 4, !tbaa !2
  %m_store11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"], [4 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_store11, i32 0, i32 3
  %m_free13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %arrayidx14 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free13, i32 0, i32 3
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx12, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx14, align 4, !tbaa !2
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  store i32 4, i32* %m_nfree, align 4, !tbaa !29
  %m_current = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 0, i32* %m_current, align 4, !tbaa !30
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 0, i32* %m_status, align 4, !tbaa !31
  %5 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %shapearg.addr, align 4, !tbaa !2
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %call15 = call nonnull align 4 dereferenceable(128) %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_(%"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %5)
  %m_distance = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance, align 4, !tbaa !32
  %m_simplices = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx16 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices, i32 0, i32 0
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx16, i32 0, i32 2
  store i32 0, i32* %rank, align 4, !tbaa !18
  %6 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %7 = bitcast %class.btVector3* %m_ray to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !20
  %9 = bitcast float* %sqrl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %m_ray17 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call18 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_ray17)
  store float %call18, float* %sqrl, align 4, !tbaa !8
  %m_simplices19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx20 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices19, i32 0, i32 0
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %11 = load float, float* %sqrl, align 4, !tbaa !8
  %cmp = fcmp ogt float %11, 0.000000e+00
  %12 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #9
  %14 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %m_ray21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray21)
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  store float 1.000000e+00, float* %ref.tmp22, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !8
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !8
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %15 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  %m_simplices26 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx27 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices26, i32 0, i32 0
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx27, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 0
  store float 1.000000e+00, float* %arrayidx28, align 4, !tbaa !8
  %m_simplices29 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %arrayidx30 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices29, i32 0, i32 0
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx30, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 0
  %19 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx31, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %19, i32 0, i32 1
  %m_ray32 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %20 = bitcast %class.btVector3* %m_ray32 to i8*
  %21 = bitcast %class.btVector3* %w to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !20
  %22 = load float, float* %sqrl, align 4, !tbaa !8
  store float %22, float* %sqdist, align 4, !tbaa !8
  %m_ray33 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %arrayidx34 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 3
  %23 = bitcast %class.btVector3* %arrayidx34 to i8*
  %24 = bitcast %class.btVector3* %m_ray33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !20
  %arrayidx35 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 2
  %25 = bitcast %class.btVector3* %arrayidx35 to i8*
  %26 = bitcast %class.btVector3* %arrayidx34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %25, i8* align 4 %26, i32 16, i1 false), !tbaa.struct !20
  %arrayidx36 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 1
  %27 = bitcast %class.btVector3* %arrayidx36 to i8*
  %28 = bitcast %class.btVector3* %arrayidx35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !20
  %arrayidx37 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 0
  %29 = bitcast %class.btVector3* %arrayidx37 to i8*
  %30 = bitcast %class.btVector3* %arrayidx36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %29, i8* align 4 %30, i32 16, i1 false), !tbaa.struct !20
  br label %do.body

do.body:                                          ; preds = %do.cond, %cond.end
  %31 = bitcast i32* %next to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %m_current38 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %32 = load i32, i32* %m_current38, align 4, !tbaa !30
  %sub = sub i32 1, %32
  store i32 %sub, i32* %next, align 4, !tbaa !10
  %33 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %cs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  %m_simplices39 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current40 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %34 = load i32, i32* %m_current40, align 4, !tbaa !30
  %arrayidx41 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices39, i32 0, i32 %34
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx41, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %35 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %ns to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #9
  %m_simplices42 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %36 = load i32, i32* %next, align 4, !tbaa !10
  %arrayidx43 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices42, i32 0, i32 %36
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx43, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %37 = bitcast float* %rl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %m_ray44 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call45 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_ray44)
  store float %call45, float* %rl, align 4, !tbaa !8
  %38 = load float, float* %rl, align 4, !tbaa !8
  %cmp46 = fcmp olt float %38, 0x3F1A36E2E0000000
  br i1 %cmp46, label %if.then, label %if.end

if.then:                                          ; preds = %do.body
  %m_status47 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 1, i32* %m_status47, align 4, !tbaa !31
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup185

if.end:                                           ; preds = %do.body
  %39 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %40 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  %m_ray49 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp48, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray49)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp48)
  %41 = bitcast %class.btVector3* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %42 = bitcast %class.btVector3** %w50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #9
  %43 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c51 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %43, i32 0, i32 0
  %44 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %rank52 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %44, i32 0, i32 2
  %45 = load i32, i32* %rank52, align 4, !tbaa !18
  %sub53 = sub i32 %45, 1
  %arrayidx54 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c51, i32 0, i32 %sub53
  %46 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx54, align 4, !tbaa !2
  %w55 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %46, i32 0, i32 1
  store %class.btVector3* %w55, %class.btVector3** %w50, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %found) #9
  store i8 0, i8* %found, align 1, !tbaa !26
  %47 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %48 = load i32, i32* %i, align 4, !tbaa !10
  %cmp56 = icmp ult i32 %48, 4
  br i1 %cmp56, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %49 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #9
  %50 = load %class.btVector3*, %class.btVector3** %w50, align 4, !tbaa !2
  %51 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx58 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 %51
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp57, %class.btVector3* nonnull align 4 dereferenceable(16) %50, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx58)
  %call59 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp57)
  %cmp60 = fcmp olt float %call59, 0x3F1A36E2E0000000
  %52 = bitcast %class.btVector3* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #9
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %for.body
  store i8 1, i8* %found, align 1, !tbaa !26
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end62:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end62
  %53 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %53, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

cleanup:                                          ; preds = %if.then61, %for.cond.cleanup
  %54 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  br label %for.end

for.end:                                          ; preds = %cleanup
  %55 = load i8, i8* %found, align 1, !tbaa !26, !range !28
  %tobool = trunc i8 %55 to i1
  br i1 %tobool, label %if.then63, label %if.else

if.then63:                                        ; preds = %for.end
  %m_simplices64 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current65 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %56 = load i32, i32* %m_current65, align 4, !tbaa !30
  %arrayidx66 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices64, i32 0, i32 %56
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx66)
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup183

if.else:                                          ; preds = %for.end
  %57 = load %class.btVector3*, %class.btVector3** %w50, align 4, !tbaa !2
  %58 = load i32, i32* %clastw, align 4, !tbaa !10
  %add = add i32 %58, 1
  %and = and i32 %add, 3
  store i32 %and, i32* %clastw, align 4, !tbaa !10
  %arrayidx67 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %lastw, i32 0, i32 %and
  %59 = bitcast %class.btVector3* %arrayidx67 to i8*
  %60 = bitcast %class.btVector3* %57 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !20
  br label %if.end68

if.end68:                                         ; preds = %if.else
  %61 = bitcast float* %omega to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #9
  %m_ray69 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %62 = load %class.btVector3*, %class.btVector3** %w50, align 4, !tbaa !2
  %call70 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_ray69, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  %63 = load float, float* %rl, align 4, !tbaa !8
  %div = fdiv float %call70, %63
  store float %div, float* %omega, align 4, !tbaa !8
  %call71 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %omega, float* nonnull align 4 dereferenceable(4) %alpha)
  %64 = load float, float* %call71, align 4, !tbaa !8
  store float %64, float* %alpha, align 4, !tbaa !8
  %65 = load float, float* %rl, align 4, !tbaa !8
  %66 = load float, float* %alpha, align 4, !tbaa !8
  %sub72 = fsub float %65, %66
  %67 = load float, float* %rl, align 4, !tbaa !8
  %mul = fmul float 0x3F1A36E2E0000000, %67
  %sub73 = fsub float %sub72, %mul
  %cmp74 = fcmp ole float %sub73, 0.000000e+00
  br i1 %cmp74, label %if.then75, label %if.end79

if.then75:                                        ; preds = %if.end68
  %m_simplices76 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current77 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %68 = load i32, i32* %m_current77, align 4, !tbaa !30
  %arrayidx78 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices76, i32 0, i32 %68
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx78)
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup182

if.end79:                                         ; preds = %if.end68
  %69 = bitcast [4 x float]* %weights to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %69) #9
  %70 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  store i32 0, i32* %mask, align 4, !tbaa !10
  %71 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %rank80 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %71, i32 0, i32 2
  %72 = load i32, i32* %rank80, align 4, !tbaa !18
  switch i32 %72, label %sw.epilog [
    i32 2, label %sw.bb
    i32 3, label %sw.bb88
    i32 4, label %sw.bb100
  ]

sw.bb:                                            ; preds = %if.end79
  %73 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c81 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %73, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c81, i32 0, i32 0
  %74 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx82, align 4, !tbaa !2
  %w83 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %74, i32 0, i32 1
  %75 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c84 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %75, i32 0, i32 0
  %arrayidx85 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c84, i32 0, i32 1
  %76 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx85, align 4, !tbaa !2
  %w86 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %76, i32 0, i32 1
  %arraydecay = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call87 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w83, %class.btVector3* nonnull align 4 dereferenceable(16) %w86, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call87, float* %sqdist, align 4, !tbaa !8
  br label %sw.epilog

sw.bb88:                                          ; preds = %if.end79
  %77 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c89 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %77, i32 0, i32 0
  %arrayidx90 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c89, i32 0, i32 0
  %78 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx90, align 4, !tbaa !2
  %w91 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %78, i32 0, i32 1
  %79 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c92 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %79, i32 0, i32 0
  %arrayidx93 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c92, i32 0, i32 1
  %80 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx93, align 4, !tbaa !2
  %w94 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %80, i32 0, i32 1
  %81 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c95 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %81, i32 0, i32 0
  %arrayidx96 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c95, i32 0, i32 2
  %82 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx96, align 4, !tbaa !2
  %w97 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %82, i32 0, i32 1
  %arraydecay98 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call99 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w91, %class.btVector3* nonnull align 4 dereferenceable(16) %w94, %class.btVector3* nonnull align 4 dereferenceable(16) %w97, float* %arraydecay98, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call99, float* %sqdist, align 4, !tbaa !8
  br label %sw.epilog

sw.bb100:                                         ; preds = %if.end79
  %83 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c101 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %83, i32 0, i32 0
  %arrayidx102 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c101, i32 0, i32 0
  %84 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx102, align 4, !tbaa !2
  %w103 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %84, i32 0, i32 1
  %85 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c104 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %85, i32 0, i32 0
  %arrayidx105 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c104, i32 0, i32 1
  %86 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx105, align 4, !tbaa !2
  %w106 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %86, i32 0, i32 1
  %87 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c107 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %87, i32 0, i32 0
  %arrayidx108 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c107, i32 0, i32 2
  %88 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx108, align 4, !tbaa !2
  %w109 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %88, i32 0, i32 1
  %89 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c110 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %89, i32 0, i32 0
  %arrayidx111 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c110, i32 0, i32 3
  %90 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx111, align 4, !tbaa !2
  %w112 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %90, i32 0, i32 1
  %arraydecay113 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 0
  %call114 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %w103, %class.btVector3* nonnull align 4 dereferenceable(16) %w106, %class.btVector3* nonnull align 4 dereferenceable(16) %w109, %class.btVector3* nonnull align 4 dereferenceable(16) %w112, float* %arraydecay113, i32* nonnull align 4 dereferenceable(4) %mask)
  store float %call114, float* %sqdist, align 4, !tbaa !8
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end79, %sw.bb100, %sw.bb88, %sw.bb
  %91 = load float, float* %sqdist, align 4, !tbaa !8
  %cmp115 = fcmp oge float %91, 0.000000e+00
  br i1 %cmp115, label %if.then116, label %if.else168

if.then116:                                       ; preds = %sw.epilog
  %92 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %rank117 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %92, i32 0, i32 2
  store i32 0, i32* %rank117, align 4, !tbaa !18
  %93 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %93) #9
  %94 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #9
  store float 0.000000e+00, float* %ref.tmp119, align 4, !tbaa !8
  %95 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #9
  store float 0.000000e+00, float* %ref.tmp120, align 4, !tbaa !8
  %96 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #9
  store float 0.000000e+00, float* %ref.tmp121, align 4, !tbaa !8
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %m_ray123 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %97 = bitcast %class.btVector3* %m_ray123 to i8*
  %98 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %97, i8* align 4 %98, i32 16, i1 false), !tbaa.struct !20
  %99 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %100 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %102) #9
  %103 = load i32, i32* %next, align 4, !tbaa !10
  %m_current124 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 %103, i32* %m_current124, align 4, !tbaa !30
  %104 = bitcast i32* %i125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #9
  store i32 0, i32* %i125, align 4, !tbaa !10
  %105 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #9
  %106 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %rank126 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %106, i32 0, i32 2
  %107 = load i32, i32* %rank126, align 4, !tbaa !18
  store i32 %107, i32* %ni, align 4, !tbaa !10
  br label %for.cond127

for.cond127:                                      ; preds = %for.inc159, %if.then116
  %108 = load i32, i32* %i125, align 4, !tbaa !10
  %109 = load i32, i32* %ni, align 4, !tbaa !10
  %cmp128 = icmp ult i32 %108, %109
  br i1 %cmp128, label %for.body130, label %for.cond.cleanup129

for.cond.cleanup129:                              ; preds = %for.cond127
  store i32 8, i32* %cleanup.dest.slot, align 4
  %110 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast i32* %i125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  br label %for.end163

for.body130:                                      ; preds = %for.cond127
  %112 = load i32, i32* %mask, align 4, !tbaa !10
  %113 = load i32, i32* %i125, align 4, !tbaa !10
  %shl = shl i32 1, %113
  %and131 = and i32 %112, %shl
  %tobool132 = icmp ne i32 %and131, 0
  br i1 %tobool132, label %if.then133, label %if.else151

if.then133:                                       ; preds = %for.body130
  %114 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c134 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %114, i32 0, i32 0
  %115 = load i32, i32* %i125, align 4, !tbaa !10
  %arrayidx135 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c134, i32 0, i32 %115
  %116 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx135, align 4, !tbaa !2
  %117 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %c136 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %117, i32 0, i32 0
  %118 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %rank137 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %118, i32 0, i32 2
  %119 = load i32, i32* %rank137, align 4, !tbaa !18
  %arrayidx138 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c136, i32 0, i32 %119
  store %"struct.gjkepa2_impl::GJK::sSV"* %116, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx138, align 4, !tbaa !2
  %120 = load i32, i32* %i125, align 4, !tbaa !10
  %arrayidx139 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 %120
  %121 = load float, float* %arrayidx139, align 4, !tbaa !8
  %122 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %p140 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %122, i32 0, i32 1
  %123 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %ns, align 4, !tbaa !2
  %rank141 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %123, i32 0, i32 2
  %124 = load i32, i32* %rank141, align 4, !tbaa !18
  %inc142 = add i32 %124, 1
  store i32 %inc142, i32* %rank141, align 4, !tbaa !18
  %arrayidx143 = getelementptr inbounds [4 x float], [4 x float]* %p140, i32 0, i32 %124
  store float %121, float* %arrayidx143, align 4, !tbaa !8
  %125 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %125) #9
  %126 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c145 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %126, i32 0, i32 0
  %127 = load i32, i32* %i125, align 4, !tbaa !10
  %arrayidx146 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c145, i32 0, i32 %127
  %128 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx146, align 4, !tbaa !2
  %w147 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %128, i32 0, i32 1
  %129 = load i32, i32* %i125, align 4, !tbaa !10
  %arrayidx148 = getelementptr inbounds [4 x float], [4 x float]* %weights, i32 0, i32 %129
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp144, %class.btVector3* nonnull align 4 dereferenceable(16) %w147, float* nonnull align 4 dereferenceable(4) %arrayidx148)
  %m_ray149 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call150 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_ray149, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp144)
  %130 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %130) #9
  br label %if.end158

if.else151:                                       ; preds = %for.body130
  %131 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %cs, align 4, !tbaa !2
  %c152 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %131, i32 0, i32 0
  %132 = load i32, i32* %i125, align 4, !tbaa !10
  %arrayidx153 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c152, i32 0, i32 %132
  %133 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx153, align 4, !tbaa !2
  %m_free154 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree155 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %134 = load i32, i32* %m_nfree155, align 4, !tbaa !29
  %inc156 = add i32 %134, 1
  store i32 %inc156, i32* %m_nfree155, align 4, !tbaa !29
  %arrayidx157 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free154, i32 0, i32 %134
  store %"struct.gjkepa2_impl::GJK::sSV"* %133, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx157, align 4, !tbaa !2
  br label %if.end158

if.end158:                                        ; preds = %if.else151, %if.then133
  br label %for.inc159

for.inc159:                                       ; preds = %if.end158
  %135 = load i32, i32* %i125, align 4, !tbaa !10
  %inc160 = add i32 %135, 1
  store i32 %inc160, i32* %i125, align 4, !tbaa !10
  br label %for.cond127

for.end163:                                       ; preds = %for.cond.cleanup129
  %136 = load i32, i32* %mask, align 4, !tbaa !10
  %cmp164 = icmp eq i32 %136, 15
  br i1 %cmp164, label %if.then165, label %if.end167

if.then165:                                       ; preds = %for.end163
  %m_status166 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 1, i32* %m_status166, align 4, !tbaa !31
  br label %if.end167

if.end167:                                        ; preds = %if.then165, %for.end163
  br label %if.end172

if.else168:                                       ; preds = %sw.epilog
  %m_simplices169 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current170 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %137 = load i32, i32* %m_current170, align 4, !tbaa !30
  %arrayidx171 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices169, i32 0, i32 %137
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %arrayidx171)
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup180

if.end172:                                        ; preds = %if.end167
  %138 = load i32, i32* %iterations, align 4, !tbaa !10
  %inc173 = add i32 %138, 1
  store i32 %inc173, i32* %iterations, align 4, !tbaa !10
  %cmp174 = icmp ult i32 %inc173, 128
  br i1 %cmp174, label %cond.true175, label %cond.false177

cond.true175:                                     ; preds = %if.end172
  %m_status176 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %139 = load i32, i32* %m_status176, align 4, !tbaa !31
  br label %cond.end178

cond.false177:                                    ; preds = %if.end172
  br label %cond.end178

cond.end178:                                      ; preds = %cond.false177, %cond.true175
  %cond = phi i32 [ %139, %cond.true175 ], [ 2, %cond.false177 ]
  %m_status179 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 %cond, i32* %m_status179, align 4, !tbaa !31
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup180

cleanup180:                                       ; preds = %cond.end178, %if.else168
  %140 = bitcast i32* %mask to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #9
  %141 = bitcast [4 x float]* %weights to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %141) #9
  br label %cleanup182

cleanup182:                                       ; preds = %cleanup180, %if.then75
  %142 = bitcast float* %omega to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #9
  br label %cleanup183

cleanup183:                                       ; preds = %cleanup182, %if.then63
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %found) #9
  %143 = bitcast %class.btVector3** %w50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #9
  br label %cleanup185

cleanup185:                                       ; preds = %cleanup183, %if.then
  %144 = bitcast float* %rl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #9
  %145 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %ns to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #9
  %146 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %cs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #9
  %147 = bitcast i32* %next to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 2, label %do.end
  ]

cleanup.cont:                                     ; preds = %cleanup185
  br label %do.cond

do.cond:                                          ; preds = %cleanup.cont
  %m_status189 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %148 = load i32, i32* %m_status189, align 4, !tbaa !31
  %cmp190 = icmp eq i32 %148, 0
  br i1 %cmp190, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %cleanup185
  %m_simplices191 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 3
  %m_current192 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  %149 = load i32, i32* %m_current192, align 4, !tbaa !30
  %arrayidx193 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::GJK::sSimplex"], [2 x %"struct.gjkepa2_impl::GJK::sSimplex"]* %m_simplices191, i32 0, i32 %149
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %arrayidx193, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4, !tbaa !12
  %m_status194 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %150 = load i32, i32* %m_status194, align 4, !tbaa !31
  switch i32 %150, label %sw.default [
    i32 0, label %sw.bb195
    i32 1, label %sw.bb199
  ]

sw.bb195:                                         ; preds = %do.end
  %m_ray196 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %call197 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_ray196)
  %m_distance198 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float %call197, float* %m_distance198, align 4, !tbaa !32
  br label %sw.epilog201

sw.bb199:                                         ; preds = %do.end
  %m_distance200 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance200, align 4, !tbaa !32
  br label %sw.epilog201

sw.default:                                       ; preds = %do.end
  br label %sw.epilog201

sw.epilog201:                                     ; preds = %sw.default, %sw.bb199, %sw.bb195
  %m_status202 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  %151 = load i32, i32* %m_status202, align 4, !tbaa !31
  store i32 1, i32* %cleanup.dest.slot, align 4
  %152 = bitcast float* %sqrl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #9
  %153 = bitcast i32* %clastw to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #9
  %154 = bitcast [4 x %class.btVector3]* %lastw to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %154) #9
  %155 = bitcast float* %alpha to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #9
  %156 = bitcast float* %sqdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #9
  %157 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #9
  ret i32 %151

unreachable:                                      ; preds = %cleanup185
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !8
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !8
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !8
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !8
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %index.addr = alloca i32, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !10
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4, !tbaa !10
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  br label %return

if.else:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  br label %return

return:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !8
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !8
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

define hidden zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results, i1 zeroext %usemargins) #1 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %usemargins.addr = alloca i8, align 1
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %epa = alloca %"struct.gjkepa2_impl::EPA", align 4
  %epa_status = alloca i32, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %i = alloca i32, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4, !tbaa !2
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %frombool = zext i1 %usemargins to i8
  store i8 %frombool, i8* %usemargins.addr, align 1, !tbaa !26
  %0 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #9
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %1 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %2 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %3 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  %4 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  %5 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %6 = load i8, i8* %usemargins.addr, align 1, !tbaa !26, !range !28
  %tobool = trunc i8 %6 to i1
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %1, %class.btTransform* nonnull align 4 dereferenceable(64) %2, %class.btConvexShape* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %5, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext %tobool)
  %7 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.start.p0i8(i64 380, i8* %7) #9
  %call1 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  %8 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %call2 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #9
  store i32 %call2, i32* %gjk_status, align 4, !tbaa !6
  %12 = load i32, i32* %gjk_status, align 4, !tbaa !6
  switch i32 %12, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb29
  ]

sw.bb:                                            ; preds = %entry
  %13 = bitcast %"struct.gjkepa2_impl::EPA"* %epa to i8*
  call void @llvm.lifetime.start.p0i8(i64 9296, i8* %13) #9
  %call3 = call %"struct.gjkepa2_impl::EPA"* @_ZN12gjkepa2_impl3EPAC2Ev(%"struct.gjkepa2_impl::EPA"* %epa)
  %14 = bitcast i32* %epa_status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #9
  %16 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %call5 = call i32 @_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3(%"struct.gjkepa2_impl::EPA"* %epa, %"struct.gjkepa2_impl::GJK"* nonnull align 4 dereferenceable(380) %gjk, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %17 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #9
  store i32 %call5, i32* %epa_status, align 4, !tbaa !33
  %18 = load i32, i32* %epa_status, align 4, !tbaa !33
  %cmp = icmp ne i32 %18, 9
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %19 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #9
  %20 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !8
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %23 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  %24 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #9
  %25 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %27 = load i32, i32* %i, align 4, !tbaa !10
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result, i32 0, i32 2
  %28 = load i32, i32* %rank, align 4, !tbaa !35
  %cmp10 = icmp ult i32 %27, %28
  br i1 %cmp10, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %29 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %30 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #9
  %31 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #9
  %m_result13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result13, i32 0, i32 0
  %32 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %32
  %33 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %33, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp12, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  %m_result14 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 1
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result14, i32 0, i32 1
  %34 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 %34
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %arrayidx15)
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %35 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #9
  %36 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %37, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %38 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %status = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %38, i32 0, i32 0
  store i32 1, i32* %status, align 4, !tbaa !25
  %39 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #9
  %40 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btTransform* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %41 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %41, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %42 = bitcast %class.btVector3* %arrayidx18 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false), !tbaa.struct !20
  %44 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #9
  %45 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #9
  %46 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #9
  %48 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #9
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 2
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %m_depth)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, %class.btTransform* %46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %49 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses22 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %49, i32 0, i32 1
  %arrayidx23 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses22, i32 0, i32 1
  %50 = bitcast %class.btVector3* %arrayidx23 to i8*
  %51 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false), !tbaa.struct !20
  %52 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #9
  %53 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #9
  %54 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #9
  %55 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #9
  %m_normal25 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal25)
  %56 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %56, i32 0, i32 2
  %57 = bitcast %class.btVector3* %normal to i8*
  %58 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false), !tbaa.struct !20
  %59 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #9
  %m_depth26 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %epa, i32 0, i32 3
  %60 = load float, float* %m_depth26, align 4, !tbaa !38
  %fneg = fneg float %60
  %61 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %distance = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %61, i32 0, i32 3
  store float %fneg, float* %distance, align 4, !tbaa !22
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %62 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #9
  br label %cleanup

if.else:                                          ; preds = %sw.bb
  %63 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %status27 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %63, i32 0, i32 0
  store i32 3, i32* %status27, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.else
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %for.end
  %64 = bitcast i32* %epa_status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %65 = bitcast %"struct.gjkepa2_impl::EPA"* %epa to i8*
  call void @llvm.lifetime.end.p0i8(i64 9296, i8* %65) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup31 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %sw.epilog

sw.bb29:                                          ; preds = %entry
  %66 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %status30 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %66, i32 0, i32 0
  store i32 2, i32* %status30, align 4, !tbaa !25
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb29, %cleanup.cont
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup31

cleanup31:                                        ; preds = %sw.epilog, %cleanup
  %67 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #9
  %68 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.end.p0i8(i64 380, i8* %68) #9
  %69 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %69) #9
  %70 = load i1, i1* %retval, align 1
  ret i1 %70
}

define linkonce_odr hidden %"struct.gjkepa2_impl::EPA"* @_ZN12gjkepa2_impl3EPAC2Ev(%"struct.gjkepa2_impl::EPA"* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  store %"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA"** %retval, align 4
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normal)
  %m_sv_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 4
  %array.begin = getelementptr inbounds [64 x %"struct.gjkepa2_impl::GJK::sSV"], [64 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_sv_store, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %array.begin, i32 64
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %"struct.gjkepa2_impl::GJK::sSV"* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.gjkepa2_impl::GJK::sSV"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_fc_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 5
  %array.begin3 = getelementptr inbounds [128 x %"struct.gjkepa2_impl::EPA::sFace"], [128 x %"struct.gjkepa2_impl::EPA::sFace"]* %m_fc_store, i32 0, i32 0
  %arrayctor.end4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %array.begin3, i32 128
  br label %arrayctor.loop5

arrayctor.loop5:                                  ; preds = %arrayctor.loop5, %arrayctor.cont
  %arrayctor.cur6 = phi %"struct.gjkepa2_impl::EPA::sFace"* [ %array.begin3, %arrayctor.cont ], [ %arrayctor.next8, %arrayctor.loop5 ]
  %call7 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA5sFaceC2Ev(%"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.cur6)
  %arrayctor.next8 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.cur6, i32 1
  %arrayctor.done9 = icmp eq %"struct.gjkepa2_impl::EPA::sFace"* %arrayctor.next8, %arrayctor.end4
  br i1 %arrayctor.done9, label %arrayctor.cont10, label %arrayctor.loop5

arrayctor.cont10:                                 ; preds = %arrayctor.loop5
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %call11 = call %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* %m_hull)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %call12 = call %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* %m_stock)
  call void @_ZN12gjkepa2_impl3EPA10InitializeEv(%"struct.gjkepa2_impl::EPA"* %this1)
  %0 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %retval, align 4
  ret %"struct.gjkepa2_impl::EPA"* %0
}

define linkonce_odr hidden i32 @_ZN12gjkepa2_impl3EPA8EvaluateERNS_3GJKERK9btVector3(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::GJK"* nonnull align 4 dereferenceable(380) %gjk, %class.btVector3* nonnull align 4 dereferenceable(16) %guess) #1 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %gjk.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %simplex = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %f = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %tetra = alloca [4 x %"struct.gjkepa2_impl::EPA::sFace"*], align 16
  %best = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %outer = alloca %"struct.gjkepa2_impl::EPA::sFace", align 4
  %pass = alloca i32, align 4
  %iterations = alloca i32, align 4
  %horizon = alloca %"struct.gjkepa2_impl::EPA::sHorizon", align 4
  %w84 = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %valid = alloca i8, align 1
  %wdist = alloca float, align 4
  %j = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %projection = alloca %class.btVector3, align 4
  %ref.tmp149 = alloca %class.btVector3, align 4
  %ref.tmp150 = alloca %class.btVector3, align 4
  %ref.tmp154 = alloca %class.btVector3, align 4
  %ref.tmp162 = alloca %class.btVector3, align 4
  %ref.tmp163 = alloca %class.btVector3, align 4
  %ref.tmp167 = alloca %class.btVector3, align 4
  %ref.tmp175 = alloca %class.btVector3, align 4
  %ref.tmp176 = alloca %class.btVector3, align 4
  %ref.tmp180 = alloca %class.btVector3, align 4
  %sum = alloca float, align 4
  %ref.tmp222 = alloca %class.btVector3, align 4
  %nl = alloca float, align 4
  %ref.tmp228 = alloca %class.btVector3, align 4
  %ref.tmp232 = alloca %class.btVector3, align 4
  %ref.tmp233 = alloca float, align 4
  %ref.tmp234 = alloca float, align 4
  %ref.tmp235 = alloca float, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4, !tbaa !2
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4, !tbaa !2
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %1, i32 0, i32 8
  %2 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4, !tbaa !12
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %2, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %3 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %3, i32 0, i32 2
  %4 = load i32, i32* %rank, align 4, !tbaa !18
  %cmp = icmp ugt i32 %4, 1
  br i1 %cmp, label %land.lhs.true, label %if.end220

land.lhs.true:                                    ; preds = %entry
  %5 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %5)
  br i1 %call, label %if.then, label %if.end220

if.then:                                          ; preds = %land.lhs.true
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull, i32 0, i32 0
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !39
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %6, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_hull2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull2, i32 0, i32 0
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4, !tbaa !39
  store %"struct.gjkepa2_impl::EPA::sFace"* %8, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  %m_hull4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull4, %"struct.gjkepa2_impl::EPA::sFace"* %9)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %10)
  %11 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 0, i32* %m_status, align 4, !tbaa !40
  %m_nextsv = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nextsv, align 4, !tbaa !41
  %12 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %14 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %14, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 0
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 1
  %16 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %16, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 3
  %17 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4, !tbaa !2
  %w7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %17, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w7)
  %18 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #9
  %19 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c9 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %19, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c9, i32 0, i32 1
  %20 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx10, align 4, !tbaa !2
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %20, i32 0, i32 1
  %21 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c12 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %21, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c12, i32 0, i32 3
  %22 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx13, align 4, !tbaa !2
  %w14 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %22, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %w11, %class.btVector3* nonnull align 4 dereferenceable(16) %w14)
  %23 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %24 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c16 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %24, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c16, i32 0, i32 2
  %25 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx17, align 4, !tbaa !2
  %w18 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %25, i32 0, i32 1
  %26 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %26, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c19, i32 0, i32 3
  %27 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx20, align 4, !tbaa !2
  %w21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %27, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %w18, %class.btVector3* nonnull align 4 dereferenceable(16) %w21)
  %call22 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %cmp23 = fcmp olt float %call22, 0.000000e+00
  %28 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  %29 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #9
  %30 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  br i1 %cmp23, label %if.then24, label %if.end

if.then24:                                        ; preds = %while.end
  %31 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c25 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %31, i32 0, i32 0
  %arrayidx26 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c25, i32 0, i32 0
  %32 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c27 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %32, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c27, i32 0, i32 1
  call void @_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_(%"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %arrayidx26, %"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %arrayidx28)
  %33 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %33, i32 0, i32 1
  %arrayidx29 = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 0
  %34 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %p30 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %34, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [4 x float], [4 x float]* %p30, i32 0, i32 1
  call void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %arrayidx29, float* nonnull align 4 dereferenceable(4) %arrayidx31)
  br label %if.end

if.end:                                           ; preds = %if.then24, %while.end
  %35 = bitcast [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %35) #9
  %arrayinit.begin = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %36 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c32 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c32, i32 0, i32 0
  %37 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx33, align 4, !tbaa !2
  %38 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c34 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %38, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c34, i32 0, i32 1
  %39 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx35, align 4, !tbaa !2
  %40 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c36 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %40, i32 0, i32 0
  %arrayidx37 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c36, i32 0, i32 2
  %41 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx37, align 4, !tbaa !2
  %call38 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %37, %"struct.gjkepa2_impl::GJK::sSV"* %39, %"struct.gjkepa2_impl::GJK::sSV"* %41, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call38, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.begin, align 4, !tbaa !2
  %arrayinit.element = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.begin, i32 1
  %42 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c39 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %42, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c39, i32 0, i32 1
  %43 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx40, align 4, !tbaa !2
  %44 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c41 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %44, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c41, i32 0, i32 0
  %45 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx42, align 4, !tbaa !2
  %46 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c43 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %46, i32 0, i32 0
  %arrayidx44 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c43, i32 0, i32 3
  %47 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx44, align 4, !tbaa !2
  %call45 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %43, %"struct.gjkepa2_impl::GJK::sSV"* %45, %"struct.gjkepa2_impl::GJK::sSV"* %47, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call45, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element, align 4, !tbaa !2
  %arrayinit.element46 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element, i32 1
  %48 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c47 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %48, i32 0, i32 0
  %arrayidx48 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c47, i32 0, i32 2
  %49 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx48, align 4, !tbaa !2
  %50 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c49 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %50, i32 0, i32 0
  %arrayidx50 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c49, i32 0, i32 1
  %51 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx50, align 4, !tbaa !2
  %52 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c51 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %52, i32 0, i32 0
  %arrayidx52 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c51, i32 0, i32 3
  %53 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx52, align 4, !tbaa !2
  %call53 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %49, %"struct.gjkepa2_impl::GJK::sSV"* %51, %"struct.gjkepa2_impl::GJK::sSV"* %53, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call53, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element46, align 4, !tbaa !2
  %arrayinit.element54 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element46, i32 1
  %54 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c55 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %54, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c55, i32 0, i32 0
  %55 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx56, align 4, !tbaa !2
  %56 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c57 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %56, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c57, i32 0, i32 2
  %57 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx58, align 4, !tbaa !2
  %58 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c59 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %58, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c59, i32 0, i32 3
  %59 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx60, align 4, !tbaa !2
  %call61 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %55, %"struct.gjkepa2_impl::GJK::sSV"* %57, %"struct.gjkepa2_impl::GJK::sSV"* %59, i1 zeroext true)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call61, %"struct.gjkepa2_impl::EPA::sFace"** %arrayinit.element54, align 4, !tbaa !2
  %m_hull62 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull62, i32 0, i32 1
  %60 = load i32, i32* %count, align 4, !tbaa !42
  %cmp63 = icmp eq i32 %60, 4
  br i1 %cmp63, label %if.then64, label %if.end216

if.then64:                                        ; preds = %if.end
  %61 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %best to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #9
  %call65 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this1)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call65, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %62 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %62) #9
  %63 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %64 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  %65 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 56, i1 false), !tbaa.struct !43
  %66 = bitcast i32* %pass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #9
  store i32 0, i32* %pass, align 4, !tbaa !10
  %67 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  store i32 0, i32* %iterations, align 4, !tbaa !10
  %arrayidx66 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %68 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx66, align 16, !tbaa !2
  %arrayidx67 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %69 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx67, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %68, i32 0, %"struct.gjkepa2_impl::EPA::sFace"* %69, i32 0)
  %arrayidx68 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %70 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx68, align 16, !tbaa !2
  %arrayidx69 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %71 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx69, align 8, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %70, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %71, i32 0)
  %arrayidx70 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 0
  %72 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx70, align 16, !tbaa !2
  %arrayidx71 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %73 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx71, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %72, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %73, i32 0)
  %arrayidx72 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %74 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx72, align 4, !tbaa !2
  %arrayidx73 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %75 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx73, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %74, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %75, i32 2)
  %arrayidx74 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 1
  %76 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx74, align 4, !tbaa !2
  %arrayidx75 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %77 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx75, align 8, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %76, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %77, i32 1)
  %arrayidx76 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 2
  %78 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx76, align 8, !tbaa !2
  %arrayidx77 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::EPA::sFace"*], [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra, i32 0, i32 3
  %79 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx77, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %78, i32 2, %"struct.gjkepa2_impl::EPA::sFace"* %79, i32 1)
  %m_status78 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 0, i32* %m_status78, align 4, !tbaa !40
  br label %for.cond

for.cond:                                         ; preds = %for.inc126, %if.then64
  %80 = load i32, i32* %iterations, align 4, !tbaa !10
  %cmp79 = icmp ult i32 %80, 255
  br i1 %cmp79, label %for.body, label %for.end128

for.body:                                         ; preds = %for.cond
  %m_nextsv80 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  %81 = load i32, i32* %m_nextsv80, align 4, !tbaa !41
  %cmp81 = icmp ult i32 %81, 64
  br i1 %cmp81, label %if.then82, label %if.else123

if.then82:                                        ; preds = %for.body
  %82 = bitcast %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %82) #9
  %call83 = call %"struct.gjkepa2_impl::EPA::sHorizon"* @_ZN12gjkepa2_impl3EPA8sHorizonC2Ev(%"struct.gjkepa2_impl::EPA::sHorizon"* %horizon)
  %83 = bitcast %"struct.gjkepa2_impl::GJK::sSV"** %w84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #9
  %m_sv_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 4
  %m_nextsv85 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  %84 = load i32, i32* %m_nextsv85, align 4, !tbaa !41
  %inc = add i32 %84, 1
  store i32 %inc, i32* %m_nextsv85, align 4, !tbaa !41
  %arrayidx86 = getelementptr inbounds [64 x %"struct.gjkepa2_impl::GJK::sSV"], [64 x %"struct.gjkepa2_impl::GJK::sSV"]* %m_sv_store, i32 0, i32 %84
  store %"struct.gjkepa2_impl::GJK::sSV"* %arrayidx86, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %valid) #9
  store i8 1, i8* %valid, align 1, !tbaa !26
  %85 = load i32, i32* %pass, align 4, !tbaa !10
  %inc87 = add i32 %85, 1
  store i32 %inc87, i32* %pass, align 4, !tbaa !10
  %conv = trunc i32 %inc87 to i8
  %86 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %pass88 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %86, i32 0, i32 6
  store i8 %conv, i8* %pass88, align 1, !tbaa !44
  %87 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %gjk.addr, align 4, !tbaa !2
  %88 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %88, i32 0, i32 0
  %89 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4, !tbaa !2
  call void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %87, %class.btVector3* nonnull align 4 dereferenceable(16) %n, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %89)
  %90 = bitcast float* %wdist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #9
  %91 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %n89 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %91, i32 0, i32 0
  %92 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4, !tbaa !2
  %w90 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %92, i32 0, i32 1
  %call91 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n89, %class.btVector3* nonnull align 4 dereferenceable(16) %w90)
  %93 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %93, i32 0, i32 1
  %94 = load float, float* %d, align 4, !tbaa !46
  %sub = fsub float %call91, %94
  store float %sub, float* %wdist, align 4, !tbaa !8
  %95 = load float, float* %wdist, align 4, !tbaa !8
  %cmp92 = fcmp ogt float %95, 0x3F1A36E2E0000000
  br i1 %cmp92, label %if.then93, label %if.else117

if.then93:                                        ; preds = %if.then82
  %96 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #9
  store i32 0, i32* %j, align 4, !tbaa !10
  br label %for.cond94

for.cond94:                                       ; preds = %for.inc, %if.then93
  %97 = load i32, i32* %j, align 4, !tbaa !10
  %cmp95 = icmp ult i32 %97, 3
  br i1 %cmp95, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond94
  %98 = load i8, i8* %valid, align 1, !tbaa !26, !range !28
  %tobool96 = trunc i8 %98 to i1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond94
  %99 = phi i1 [ false, %for.cond94 ], [ %tobool96, %land.rhs ]
  br i1 %99, label %for.body97, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %land.end
  %100 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  br label %for.end

for.body97:                                       ; preds = %land.end
  %101 = load i32, i32* %pass, align 4, !tbaa !10
  %102 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w84, align 4, !tbaa !2
  %103 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %f98 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %103, i32 0, i32 3
  %104 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx99 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f98, i32 0, i32 %104
  %105 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx99, align 4, !tbaa !2
  %106 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %e = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %106, i32 0, i32 5
  %107 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx100 = getelementptr inbounds [3 x i8], [3 x i8]* %e, i32 0, i32 %107
  %108 = load i8, i8* %arrayidx100, align 1, !tbaa !21
  %conv101 = zext i8 %108 to i32
  %call102 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %101, %"struct.gjkepa2_impl::GJK::sSV"* %102, %"struct.gjkepa2_impl::EPA::sFace"* %105, i32 %conv101, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %horizon)
  %conv103 = zext i1 %call102 to i32
  %109 = load i8, i8* %valid, align 1, !tbaa !26, !range !28
  %tobool104 = trunc i8 %109 to i1
  %conv105 = zext i1 %tobool104 to i32
  %and = and i32 %conv105, %conv103
  %tobool106 = icmp ne i32 %and, 0
  %frombool = zext i1 %tobool106 to i8
  store i8 %frombool, i8* %valid, align 1, !tbaa !26
  br label %for.inc

for.inc:                                          ; preds = %for.body97
  %110 = load i32, i32* %j, align 4, !tbaa !10
  %inc107 = add i32 %110, 1
  store i32 %inc107, i32* %j, align 4, !tbaa !10
  br label %for.cond94

for.end:                                          ; preds = %for.cond.cleanup
  %111 = load i8, i8* %valid, align 1, !tbaa !26, !range !28
  %tobool108 = trunc i8 %111 to i1
  br i1 %tobool108, label %land.lhs.true109, label %if.else

land.lhs.true109:                                 ; preds = %for.end
  %nf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 2
  %112 = load i32, i32* %nf, align 4, !tbaa !47
  %cmp110 = icmp uge i32 %112, 3
  br i1 %cmp110, label %if.then111, label %if.else

if.then111:                                       ; preds = %land.lhs.true109
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 0
  %113 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4, !tbaa !49
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, i32 0, i32 1
  %114 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4, !tbaa !50
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %113, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %114, i32 2)
  %m_hull112 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %115 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull112, %"struct.gjkepa2_impl::EPA::sFace"* %115)
  %m_stock113 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %116 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock113, %"struct.gjkepa2_impl::EPA::sFace"* %116)
  %call114 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this1)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call114, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %117 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %best, align 4, !tbaa !2
  %118 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  %119 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %117 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %118, i8* align 4 %119, i32 56, i1 false), !tbaa.struct !43
  br label %if.end116

if.else:                                          ; preds = %land.lhs.true109, %for.end
  %m_status115 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 4, i32* %m_status115, align 4, !tbaa !40
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end116:                                        ; preds = %if.then111
  br label %if.end119

if.else117:                                       ; preds = %if.then82
  %m_status118 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 7, i32* %m_status118, align 4, !tbaa !40
  store i32 4, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end119:                                        ; preds = %if.end116
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end119, %if.else117, %if.else
  %120 = bitcast float* %wdist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %valid) #9
  %121 = bitcast %"struct.gjkepa2_impl::GJK::sSV"** %w84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #9
  %122 = bitcast %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %122) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 4, label %for.end128
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end125

if.else123:                                       ; preds = %for.body
  %m_status124 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 6, i32* %m_status124, align 4, !tbaa !40
  br label %for.end128

if.end125:                                        ; preds = %cleanup.cont
  br label %for.inc126

for.inc126:                                       ; preds = %if.end125
  %123 = load i32, i32* %iterations, align 4, !tbaa !10
  %inc127 = add i32 %123, 1
  store i32 %inc127, i32* %iterations, align 4, !tbaa !10
  br label %for.cond

for.end128:                                       ; preds = %if.else123, %cleanup, %for.cond
  %124 = bitcast %class.btVector3* %projection to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #9
  %n129 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 0
  %d130 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projection, %class.btVector3* nonnull align 4 dereferenceable(16) %n129, float* nonnull align 4 dereferenceable(4) %d130)
  %n131 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 0
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %125 = bitcast %class.btVector3* %m_normal to i8*
  %126 = bitcast %class.btVector3* %n131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %125, i8* align 4 %126, i32 16, i1 false), !tbaa.struct !20
  %d132 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 1
  %127 = load float, float* %d132, align 4, !tbaa !46
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float %127, float* %m_depth, align 4, !tbaa !38
  %m_result = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %rank133 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result, i32 0, i32 2
  store i32 3, i32* %rank133, align 4, !tbaa !35
  %c134 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx135 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c134, i32 0, i32 0
  %128 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx135, align 4, !tbaa !2
  %m_result136 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c137 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result136, i32 0, i32 0
  %arrayidx138 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c137, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %128, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx138, align 4, !tbaa !2
  %c139 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx140 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c139, i32 0, i32 1
  %129 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx140, align 4, !tbaa !2
  %m_result141 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c142 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result141, i32 0, i32 0
  %arrayidx143 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c142, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %129, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx143, align 4, !tbaa !2
  %c144 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx145 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c144, i32 0, i32 2
  %130 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx145, align 4, !tbaa !2
  %m_result146 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c147 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result146, i32 0, i32 0
  %arrayidx148 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c147, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %130, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx148, align 4, !tbaa !2
  %131 = bitcast %class.btVector3* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %131) #9
  %132 = bitcast %class.btVector3* %ref.tmp150 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %132) #9
  %c151 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx152 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c151, i32 0, i32 1
  %133 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx152, align 4, !tbaa !2
  %w153 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %133, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp150, %class.btVector3* nonnull align 4 dereferenceable(16) %w153, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %134 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %134) #9
  %c155 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx156 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c155, i32 0, i32 2
  %135 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx156, align 4, !tbaa !2
  %w157 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %135, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp154, %class.btVector3* nonnull align 4 dereferenceable(16) %w157, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp149, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp150, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp154)
  %call158 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp149)
  %m_result159 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p160 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result159, i32 0, i32 1
  %arrayidx161 = getelementptr inbounds [4 x float], [4 x float]* %p160, i32 0, i32 0
  store float %call158, float* %arrayidx161, align 4, !tbaa !8
  %136 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #9
  %137 = bitcast %class.btVector3* %ref.tmp150 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %137) #9
  %138 = bitcast %class.btVector3* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %138) #9
  %139 = bitcast %class.btVector3* %ref.tmp162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %139) #9
  %140 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %140) #9
  %c164 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx165 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c164, i32 0, i32 2
  %141 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx165, align 4, !tbaa !2
  %w166 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %141, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp163, %class.btVector3* nonnull align 4 dereferenceable(16) %w166, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %142 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %142) #9
  %c168 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx169 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c168, i32 0, i32 0
  %143 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx169, align 4, !tbaa !2
  %w170 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %143, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp167, %class.btVector3* nonnull align 4 dereferenceable(16) %w170, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp162, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp163, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp167)
  %call171 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp162)
  %m_result172 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p173 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result172, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [4 x float], [4 x float]* %p173, i32 0, i32 1
  store float %call171, float* %arrayidx174, align 4, !tbaa !8
  %144 = bitcast %class.btVector3* %ref.tmp167 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %144) #9
  %145 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %145) #9
  %146 = bitcast %class.btVector3* %ref.tmp162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #9
  %147 = bitcast %class.btVector3* %ref.tmp175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %147) #9
  %148 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %148) #9
  %c177 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx178 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c177, i32 0, i32 0
  %149 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx178, align 4, !tbaa !2
  %w179 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %149, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %w179, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  %150 = bitcast %class.btVector3* %ref.tmp180 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %150) #9
  %c181 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %outer, i32 0, i32 2
  %arrayidx182 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c181, i32 0, i32 1
  %151 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx182, align 4, !tbaa !2
  %w183 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %151, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp180, %class.btVector3* nonnull align 4 dereferenceable(16) %w183, %class.btVector3* nonnull align 4 dereferenceable(16) %projection)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp175, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp176, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp180)
  %call184 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp175)
  %m_result185 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p186 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result185, i32 0, i32 1
  %arrayidx187 = getelementptr inbounds [4 x float], [4 x float]* %p186, i32 0, i32 2
  store float %call184, float* %arrayidx187, align 4, !tbaa !8
  %152 = bitcast %class.btVector3* %ref.tmp180 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %152) #9
  %153 = bitcast %class.btVector3* %ref.tmp176 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %153) #9
  %154 = bitcast %class.btVector3* %ref.tmp175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %154) #9
  %155 = bitcast float* %sum to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #9
  %m_result188 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p189 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result188, i32 0, i32 1
  %arrayidx190 = getelementptr inbounds [4 x float], [4 x float]* %p189, i32 0, i32 0
  %156 = load float, float* %arrayidx190, align 4, !tbaa !8
  %m_result191 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p192 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result191, i32 0, i32 1
  %arrayidx193 = getelementptr inbounds [4 x float], [4 x float]* %p192, i32 0, i32 1
  %157 = load float, float* %arrayidx193, align 4, !tbaa !8
  %add = fadd float %156, %157
  %m_result194 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p195 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result194, i32 0, i32 1
  %arrayidx196 = getelementptr inbounds [4 x float], [4 x float]* %p195, i32 0, i32 2
  %158 = load float, float* %arrayidx196, align 4, !tbaa !8
  %add197 = fadd float %add, %158
  store float %add197, float* %sum, align 4, !tbaa !8
  %159 = load float, float* %sum, align 4, !tbaa !8
  %m_result198 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p199 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result198, i32 0, i32 1
  %arrayidx200 = getelementptr inbounds [4 x float], [4 x float]* %p199, i32 0, i32 0
  %160 = load float, float* %arrayidx200, align 4, !tbaa !8
  %div = fdiv float %160, %159
  store float %div, float* %arrayidx200, align 4, !tbaa !8
  %161 = load float, float* %sum, align 4, !tbaa !8
  %m_result201 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p202 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result201, i32 0, i32 1
  %arrayidx203 = getelementptr inbounds [4 x float], [4 x float]* %p202, i32 0, i32 1
  %162 = load float, float* %arrayidx203, align 4, !tbaa !8
  %div204 = fdiv float %162, %161
  store float %div204, float* %arrayidx203, align 4, !tbaa !8
  %163 = load float, float* %sum, align 4, !tbaa !8
  %m_result205 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p206 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result205, i32 0, i32 1
  %arrayidx207 = getelementptr inbounds [4 x float], [4 x float]* %p206, i32 0, i32 2
  %164 = load float, float* %arrayidx207, align 4, !tbaa !8
  %div208 = fdiv float %164, %163
  store float %div208, float* %arrayidx207, align 4, !tbaa !8
  %m_status209 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  %165 = load i32, i32* %m_status209, align 4, !tbaa !40
  store i32 %165, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %166 = bitcast float* %sum to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #9
  %167 = bitcast %class.btVector3* %projection to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %167) #9
  %168 = bitcast i32* %iterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #9
  %169 = bitcast i32* %pass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #9
  %170 = bitcast %"struct.gjkepa2_impl::EPA::sFace"* %outer to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %170) #9
  %171 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %best to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #9
  br label %cleanup217

if.end216:                                        ; preds = %if.end
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup217

cleanup217:                                       ; preds = %if.end216, %for.end128
  %172 = bitcast [4 x %"struct.gjkepa2_impl::EPA::sFace"*]* %tetra to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #9
  %cleanup.dest218 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest218, label %cleanup252 [
    i32 0, label %cleanup.cont219
  ]

cleanup.cont219:                                  ; preds = %cleanup217
  br label %if.end220

if.end220:                                        ; preds = %cleanup.cont219, %land.lhs.true, %entry
  %m_status221 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 8, i32* %m_status221, align 4, !tbaa !40
  %173 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %173) #9
  %174 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp222, %class.btVector3* nonnull align 4 dereferenceable(16) %174)
  %m_normal223 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %175 = bitcast %class.btVector3* %m_normal223 to i8*
  %176 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %175, i8* align 4 %176, i32 16, i1 false), !tbaa.struct !20
  %177 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %177) #9
  %178 = bitcast float* %nl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %178) #9
  %m_normal224 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %call225 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_normal224)
  store float %call225, float* %nl, align 4, !tbaa !8
  %179 = load float, float* %nl, align 4, !tbaa !8
  %cmp226 = fcmp ogt float %179, 0.000000e+00
  br i1 %cmp226, label %if.then227, label %if.else231

if.then227:                                       ; preds = %if.end220
  %180 = bitcast %class.btVector3* %ref.tmp228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %180) #9
  %m_normal229 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp228, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal229, float* nonnull align 4 dereferenceable(4) %nl)
  %m_normal230 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %181 = bitcast %class.btVector3* %m_normal230 to i8*
  %182 = bitcast %class.btVector3* %ref.tmp228 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %181, i8* align 4 %182, i32 16, i1 false), !tbaa.struct !20
  %183 = bitcast %class.btVector3* %ref.tmp228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %183) #9
  br label %if.end238

if.else231:                                       ; preds = %if.end220
  %184 = bitcast %class.btVector3* %ref.tmp232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %184) #9
  %185 = bitcast float* %ref.tmp233 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #9
  store float 1.000000e+00, float* %ref.tmp233, align 4, !tbaa !8
  %186 = bitcast float* %ref.tmp234 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %186) #9
  store float 0.000000e+00, float* %ref.tmp234, align 4, !tbaa !8
  %187 = bitcast float* %ref.tmp235 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #9
  store float 0.000000e+00, float* %ref.tmp235, align 4, !tbaa !8
  %call236 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp232, float* nonnull align 4 dereferenceable(4) %ref.tmp233, float* nonnull align 4 dereferenceable(4) %ref.tmp234, float* nonnull align 4 dereferenceable(4) %ref.tmp235)
  %m_normal237 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %188 = bitcast %class.btVector3* %m_normal237 to i8*
  %189 = bitcast %class.btVector3* %ref.tmp232 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %188, i8* align 4 %189, i32 16, i1 false), !tbaa.struct !20
  %190 = bitcast float* %ref.tmp235 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #9
  %191 = bitcast float* %ref.tmp234 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #9
  %192 = bitcast float* %ref.tmp233 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #9
  %193 = bitcast %class.btVector3* %ref.tmp232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %193) #9
  br label %if.end238

if.end238:                                        ; preds = %if.else231, %if.then227
  %m_depth239 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %m_depth239, align 4, !tbaa !38
  %m_result240 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %rank241 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result240, i32 0, i32 2
  store i32 1, i32* %rank241, align 4, !tbaa !35
  %194 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex, align 4, !tbaa !2
  %c242 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %194, i32 0, i32 0
  %arrayidx243 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c242, i32 0, i32 0
  %195 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx243, align 4, !tbaa !2
  %m_result244 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %c245 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result244, i32 0, i32 0
  %arrayidx246 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c245, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %195, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx246, align 4, !tbaa !2
  %m_result247 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 1
  %p248 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %m_result247, i32 0, i32 1
  %arrayidx249 = getelementptr inbounds [4 x float], [4 x float]* %p248, i32 0, i32 0
  store float 1.000000e+00, float* %arrayidx249, align 4, !tbaa !8
  %m_status250 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  %196 = load i32, i32* %m_status250, align 4, !tbaa !40
  store i32 %196, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %197 = bitcast float* %nl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #9
  br label %cleanup252

cleanup252:                                       ; preds = %if.end238, %cleanup217
  %198 = bitcast %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #9
  %199 = load i32, i32* %retval, align 4
  ret i32 %199

unreachable:                                      ; preds = %cleanup
  unreachable
}

define hidden float @_ZN15btGjkEpaSolver214SignedDistanceERK9btVector3fPK13btConvexShapeRK11btTransformRNS_8sResultsE(%class.btVector3* nonnull align 4 dereferenceable(16) %position, float %margin, %class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #1 {
entry:
  %retval = alloca float, align 4
  %position.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  %shape = alloca %"struct.gjkepa2_impl::MinkowskiDiff", align 4
  %shape1 = alloca %class.btSphereShape, align 4
  %wtrs1 = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %gjk = alloca %"struct.gjkepa2_impl::GJK", align 4
  %gjk_status = alloca i32, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %w0 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %w1 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %i = alloca i32, align 4
  %p = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %delta = alloca %class.btVector3, align 4
  %margin48 = alloca float, align 4
  %length = alloca float, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp53 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %delta62 = alloca %class.btVector3, align 4
  %length67 = alloca float, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  store %class.btVector3* %position, %class.btVector3** %position.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !8
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %0 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 128, i8* %0) #9
  %call = call %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffC2Ev(%"struct.gjkepa2_impl::MinkowskiDiff"* %shape)
  %1 = bitcast %class.btSphereShape* %shape1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 52, i8* %1) #9
  %2 = load float, float* %margin.addr, align 4, !tbaa !8
  %call1 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %shape1, float %2)
  %3 = bitcast %class.btTransform* %wtrs1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %3) #9
  %4 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !8
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = load %class.btVector3*, %class.btVector3** %position.addr, align 4, !tbaa !2
  %call7 = call %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* %wtrs1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  %12 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  %13 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #9
  %14 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %16 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %17 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %18 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  call void @_ZN12gjkepa2_implL10InitializeEPK13btConvexShapeRK11btTransformS2_S5_RN15btGjkEpaSolver28sResultsERNS_13MinkowskiDiffEb(%class.btConvexShape* %15, %class.btTransform* nonnull align 4 dereferenceable(64) %16, %class.btConvexShape* %17, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %18, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, i1 zeroext false)
  %19 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.start.p0i8(i64 380, i8* %19) #9
  %call8 = call %"struct.gjkepa2_impl::GJK"* @_ZN12gjkepa2_impl3GJKC2Ev(%"struct.gjkepa2_impl::GJK"* %gjk)
  %20 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #9
  %21 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #9
  %22 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  store float 1.000000e+00, float* %ref.tmp10, align 4, !tbaa !8
  %23 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #9
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !8
  %24 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  store float 1.000000e+00, float* %ref.tmp12, align 4, !tbaa !8
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %call14 = call i32 @_ZN12gjkepa2_impl3GJK8EvaluateERKNS_13MinkowskiDiffERK9btVector3(%"struct.gjkepa2_impl::GJK"* %gjk, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %25 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  %27 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #9
  %28 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #9
  store i32 %call14, i32* %gjk_status, align 4, !tbaa !6
  %29 = load i32, i32* %gjk_status, align 4, !tbaa !6
  %cmp = icmp eq i32 %29, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %30 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #9
  %31 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0.000000e+00, float* %ref.tmp15, align 4, !tbaa !8
  %32 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #9
  store float 0.000000e+00, float* %ref.tmp16, align 4, !tbaa !8
  %33 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #9
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !8
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w0, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %34 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  %37 = bitcast %class.btVector3* %w1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #9
  %38 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #9
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !8
  %39 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  store float 0.000000e+00, float* %ref.tmp20, align 4, !tbaa !8
  %40 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #9
  store float 0.000000e+00, float* %ref.tmp21, align 4, !tbaa !8
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %w1, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %41 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #9
  %42 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #9
  %43 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #9
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %45 = load i32, i32* %i, align 4, !tbaa !10
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %46 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4, !tbaa !12
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %46, i32 0, i32 2
  %47 = load i32, i32* %rank, align 4, !tbaa !18
  %cmp23 = icmp ult i32 %45, %47
  br i1 %cmp23, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %48 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %49 = bitcast float* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %m_simplex24 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %50 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex24, align 4, !tbaa !12
  %p25 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %50, i32 0, i32 1
  %51 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p25, i32 0, i32 %51
  %52 = load float, float* %arrayidx, align 4, !tbaa !8
  store float %52, float* %p, align 4, !tbaa !8
  %53 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #9
  %54 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #9
  %m_simplex28 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %55 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex28, align 4, !tbaa !12
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %55, i32 0, i32 0
  %56 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx29 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %56
  %57 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx29, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %57, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp27, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d, i32 0)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %p)
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  %58 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #9
  %59 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #9
  %60 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #9
  %61 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #9
  %62 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #9
  %m_simplex34 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 8
  %63 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex34, align 4, !tbaa !12
  %c35 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %63, i32 0, i32 0
  %64 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx36 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c35, i32 0, i32 %64
  %65 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx36, align 4, !tbaa !2
  %d37 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %65, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %d37)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3j(%class.btVector3* sret align 4 %ref.tmp32, %"struct.gjkepa2_impl::MinkowskiDiff"* %shape, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, i32 1)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %p)
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %66 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #9
  %67 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #9
  %68 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %68) #9
  %69 = bitcast float* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %70 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %70, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %71 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %71) #9
  %72 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp39, %class.btTransform* %72, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  %73 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %73, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses, i32 0, i32 0
  %74 = bitcast %class.btVector3* %arrayidx40 to i8*
  %75 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false), !tbaa.struct !20
  %76 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #9
  %77 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %77) #9
  %78 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btTransform* %78, %class.btVector3* nonnull align 4 dereferenceable(16) %w1)
  %79 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses42 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %79, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses42, i32 0, i32 1
  %80 = bitcast %class.btVector3* %arrayidx43 to i8*
  %81 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %80, i8* align 4 %81, i32 16, i1 false), !tbaa.struct !20
  %82 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #9
  %83 = bitcast %class.btVector3* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %83) #9
  %84 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses44 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %84, i32 0, i32 1
  %arrayidx45 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses44, i32 0, i32 1
  %85 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses46 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %85, i32 0, i32 1
  %arrayidx47 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses46, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %delta, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx47)
  %86 = bitcast float* %margin48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #9
  %87 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %call49 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %87)
  %88 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %call50 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %88)
  %add = fadd float %call49, %call50
  store float %add, float* %margin48, align 4, !tbaa !8
  %89 = bitcast float* %length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #9
  %call51 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %delta)
  store float %call51, float* %length, align 4, !tbaa !8
  %90 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %90) #9
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp52, %class.btVector3* nonnull align 4 dereferenceable(16) %delta, float* nonnull align 4 dereferenceable(4) %length)
  %91 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %91, i32 0, i32 2
  %92 = bitcast %class.btVector3* %normal to i8*
  %93 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false), !tbaa.struct !20
  %94 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #9
  %95 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #9
  %96 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal54 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %96, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp53, %class.btVector3* nonnull align 4 dereferenceable(16) %normal54, float* nonnull align 4 dereferenceable(4) %margin48)
  %97 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses55 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %97, i32 0, i32 1
  %arrayidx56 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses55, i32 0, i32 0
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp53)
  %98 = bitcast %class.btVector3* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %98) #9
  %99 = load float, float* %length, align 4, !tbaa !8
  %100 = load float, float* %margin48, align 4, !tbaa !8
  %sub = fsub float %99, %100
  store float %sub, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %101 = bitcast float* %length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast float* %margin48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast %class.btVector3* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #9
  %104 = bitcast %class.btVector3* %w1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %104) #9
  %105 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %105) #9
  br label %cleanup

if.else:                                          ; preds = %entry
  %106 = load i32, i32* %gjk_status, align 4, !tbaa !6
  %cmp58 = icmp eq i32 %106, 1
  br i1 %cmp58, label %if.then59, label %if.end74

if.then59:                                        ; preds = %if.else
  %107 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %108 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %109 = bitcast %class.btSphereShape* %shape1 to %class.btConvexShape*
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %gjk, i32 0, i32 1
  %110 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %call60 = call zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %107, %class.btTransform* nonnull align 4 dereferenceable(64) %108, %class.btConvexShape* %109, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_ray, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %110, i1 zeroext true)
  br i1 %call60, label %if.then61, label %if.end73

if.then61:                                        ; preds = %if.then59
  %111 = bitcast %class.btVector3* %delta62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %111) #9
  %112 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses63 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %112, i32 0, i32 1
  %arrayidx64 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses63, i32 0, i32 0
  %113 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %witnesses65 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %113, i32 0, i32 1
  %arrayidx66 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %witnesses65, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %delta62, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx64, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx66)
  %114 = bitcast float* %length67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #9
  %call68 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %delta62)
  store float %call68, float* %length67, align 4, !tbaa !8
  %115 = load float, float* %length67, align 4, !tbaa !8
  %cmp69 = fcmp oge float %115, 0x3E80000000000000
  br i1 %cmp69, label %if.then70, label %if.end

if.then70:                                        ; preds = %if.then61
  %116 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %116) #9
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %delta62, float* nonnull align 4 dereferenceable(4) %length67)
  %117 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %normal72 = getelementptr inbounds %"struct.btGjkEpaSolver2::sResults", %"struct.btGjkEpaSolver2::sResults"* %117, i32 0, i32 2
  %118 = bitcast %class.btVector3* %normal72 to i8*
  %119 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %118, i8* align 4 %119, i32 16, i1 false), !tbaa.struct !20
  %120 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %120) #9
  br label %if.end

if.end:                                           ; preds = %if.then70, %if.then61
  %121 = load float, float* %length67, align 4, !tbaa !8
  %fneg = fneg float %121
  store float %fneg, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %122 = bitcast float* %length67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #9
  %123 = bitcast %class.btVector3* %delta62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %123) #9
  br label %cleanup

if.end73:                                         ; preds = %if.then59
  br label %if.end74

if.end74:                                         ; preds = %if.end73, %if.else
  br label %if.end75

if.end75:                                         ; preds = %if.end74
  store float 0x47EFFFFFE0000000, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end75, %if.end, %for.end
  %124 = bitcast i32* %gjk_status to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #9
  %125 = bitcast %"struct.gjkepa2_impl::GJK"* %gjk to i8*
  call void @llvm.lifetime.end.p0i8(i64 380, i8* %125) #9
  %126 = bitcast %class.btTransform* %wtrs1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %126) #9
  %call79 = call %class.btSphereShape* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev to %class.btSphereShape* (%class.btSphereShape*)*)(%class.btSphereShape* %shape1) #9
  %127 = bitcast %class.btSphereShape* %shape1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 52, i8* %127) #9
  %128 = bitcast %"struct.gjkepa2_impl::MinkowskiDiff"* %shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 128, i8* %128) #9
  %129 = load float, float* %retval, align 4
  ret float %129
}

define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4, !tbaa !2
  store float %radius, float* %radius.addr, align 4, !tbaa !8
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !51
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4, !tbaa !53
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4, !tbaa !8
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4, !tbaa !8
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4, !tbaa !55
  ret %class.btSphereShape* %this1
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK12btQuaternionRK9btVector3(%class.btTransform* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !20
  ret %class.btTransform* %this1
}

declare float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %2 = load float*, float** %s.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %div = fdiv float 1.000000e+00, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #9
  ret void
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #6

define hidden zeroext i1 @_ZN15btGjkEpaSolver214SignedDistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %shape0, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs0, %class.btConvexShape* %shape1, %class.btTransform* nonnull align 4 dereferenceable(64) %wtrs1, %class.btVector3* nonnull align 4 dereferenceable(16) %guess, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %results) #1 {
entry:
  %retval = alloca i1, align 1
  %shape0.addr = alloca %class.btConvexShape*, align 4
  %wtrs0.addr = alloca %class.btTransform*, align 4
  %shape1.addr = alloca %class.btConvexShape*, align 4
  %wtrs1.addr = alloca %class.btTransform*, align 4
  %guess.addr = alloca %class.btVector3*, align 4
  %results.addr = alloca %"struct.btGjkEpaSolver2::sResults"*, align 4
  store %class.btConvexShape* %shape0, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs0, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  store %class.btConvexShape* %shape1, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  store %class.btTransform* %wtrs1, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  store %class.btVector3* %guess, %class.btVector3** %guess.addr, align 4, !tbaa !2
  store %"struct.btGjkEpaSolver2::sResults"* %results, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %1 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %2 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  %3 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %5 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZN15btGjkEpaSolver28DistanceEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsE(%class.btConvexShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btConvexShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %5)
  br i1 %call, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %6 = load %class.btConvexShape*, %class.btConvexShape** %shape0.addr, align 4, !tbaa !2
  %7 = load %class.btTransform*, %class.btTransform** %wtrs0.addr, align 4, !tbaa !2
  %8 = load %class.btConvexShape*, %class.btConvexShape** %shape1.addr, align 4, !tbaa !2
  %9 = load %class.btTransform*, %class.btTransform** %wtrs1.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %guess.addr, align 4, !tbaa !2
  %11 = load %"struct.btGjkEpaSolver2::sResults"*, %"struct.btGjkEpaSolver2::sResults"** %results.addr, align 4, !tbaa !2
  %call1 = call zeroext i1 @_ZN15btGjkEpaSolver211PenetrationEPK13btConvexShapeRK11btTransformS2_S5_RK9btVector3RNS_8sResultsEb(%class.btConvexShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %7, %class.btConvexShape* %8, %class.btTransform* nonnull align 4 dereferenceable(64) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %"struct.btGjkEpaSolver2::sResults"* nonnull align 4 dereferenceable(56) %11, i1 zeroext false)
  store i1 %call1, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.else, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !8
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call2)
  %4 = load float, float* %call3, align 4, !tbaa !8
  %mul = fmul float %2, %4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx5)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 1)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call7)
  %7 = load float, float* %call8, align 4, !tbaa !8
  %mul9 = fmul float %5, %7
  %add = fadd float %mul, %mul9
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx11)
  %8 = load float, float* %call12, align 4, !tbaa !8
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 2)
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call13)
  %10 = load float, float* %call14, align 4, !tbaa !8
  %mul15 = fmul float %8, %10
  %add16 = fadd float %add, %mul15
  store float %add16, float* %ref.tmp, align 4, !tbaa !8
  %11 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx19)
  %12 = load float, float* %call20, align 4, !tbaa !8
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 0)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call21)
  %14 = load float, float* %call22, align 4, !tbaa !8
  %mul23 = fmul float %12, %14
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx25)
  %15 = load float, float* %call26, align 4, !tbaa !8
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %17 = load float, float* %call28, align 4, !tbaa !8
  %mul29 = fmul float %15, %17
  %add30 = fadd float %mul23, %mul29
  %m_el31 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el31, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx32)
  %18 = load float, float* %call33, align 4, !tbaa !8
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 2)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call34)
  %20 = load float, float* %call35, align 4, !tbaa !8
  %mul36 = fmul float %18, %20
  %add37 = fadd float %add30, %mul36
  store float %add37, float* %ref.tmp17, align 4, !tbaa !8
  %21 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #9
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 0
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx40)
  %22 = load float, float* %call41, align 4, !tbaa !8
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 0)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call42)
  %24 = load float, float* %call43, align 4, !tbaa !8
  %mul44 = fmul float %22, %24
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx46)
  %25 = load float, float* %call47, align 4, !tbaa !8
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %26, i32 1)
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call48)
  %27 = load float, float* %call49, align 4, !tbaa !8
  %mul50 = fmul float %25, %27
  %add51 = fadd float %mul44, %mul50
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 2
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %28 = load float, float* %call54, align 4, !tbaa !8
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call55)
  %30 = load float, float* %call56, align 4, !tbaa !8
  %mul57 = fmul float %28, %30
  %add58 = fadd float %add51, %mul57
  store float %add58, float* %ref.tmp38, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  %m_el60 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el60, i32 0, i32 0
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx61)
  %32 = load float, float* %call62, align 4, !tbaa !8
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 0)
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call63)
  %34 = load float, float* %call64, align 4, !tbaa !8
  %mul65 = fmul float %32, %34
  %m_el66 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el66, i32 0, i32 1
  %call68 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx67)
  %35 = load float, float* %call68, align 4, !tbaa !8
  %36 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %36, i32 1)
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call69)
  %37 = load float, float* %call70, align 4, !tbaa !8
  %mul71 = fmul float %35, %37
  %add72 = fadd float %mul65, %mul71
  %m_el73 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el73, i32 0, i32 2
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx74)
  %38 = load float, float* %call75, align 4, !tbaa !8
  %39 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %39, i32 2)
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call76)
  %40 = load float, float* %call77, align 4, !tbaa !8
  %mul78 = fmul float %38, %40
  %add79 = fadd float %add72, %mul78
  store float %add79, float* %ref.tmp59, align 4, !tbaa !8
  %41 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #9
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 0
  %call83 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx82)
  %42 = load float, float* %call83, align 4, !tbaa !8
  %43 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %43, i32 0)
  %call85 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call84)
  %44 = load float, float* %call85, align 4, !tbaa !8
  %mul86 = fmul float %42, %44
  %m_el87 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx88 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el87, i32 0, i32 1
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx88)
  %45 = load float, float* %call89, align 4, !tbaa !8
  %46 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %46, i32 1)
  %call91 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call90)
  %47 = load float, float* %call91, align 4, !tbaa !8
  %mul92 = fmul float %45, %47
  %add93 = fadd float %mul86, %mul92
  %m_el94 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el94, i32 0, i32 2
  %call96 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx95)
  %48 = load float, float* %call96, align 4, !tbaa !8
  %49 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %49, i32 2)
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call97)
  %50 = load float, float* %call98, align 4, !tbaa !8
  %mul99 = fmul float %48, %50
  %add100 = fadd float %add93, %mul99
  store float %add100, float* %ref.tmp80, align 4, !tbaa !8
  %51 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #9
  %m_el102 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el102, i32 0, i32 0
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx103)
  %52 = load float, float* %call104, align 4, !tbaa !8
  %53 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %53, i32 0)
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call105)
  %54 = load float, float* %call106, align 4, !tbaa !8
  %mul107 = fmul float %52, %54
  %m_el108 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el108, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx109)
  %55 = load float, float* %call110, align 4, !tbaa !8
  %56 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %56, i32 1)
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call111)
  %57 = load float, float* %call112, align 4, !tbaa !8
  %mul113 = fmul float %55, %57
  %add114 = fadd float %mul107, %mul113
  %m_el115 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el115, i32 0, i32 2
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx116)
  %58 = load float, float* %call117, align 4, !tbaa !8
  %59 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %59, i32 2)
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call118)
  %60 = load float, float* %call119, align 4, !tbaa !8
  %mul120 = fmul float %58, %60
  %add121 = fadd float %add114, %mul120
  store float %add121, float* %ref.tmp101, align 4, !tbaa !8
  %61 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #9
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 0
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx124)
  %62 = load float, float* %call125, align 4, !tbaa !8
  %63 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call126 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %63, i32 0)
  %call127 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call126)
  %64 = load float, float* %call127, align 4, !tbaa !8
  %mul128 = fmul float %62, %64
  %m_el129 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx130 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el129, i32 0, i32 1
  %call131 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx130)
  %65 = load float, float* %call131, align 4, !tbaa !8
  %66 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %66, i32 1)
  %call133 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call132)
  %67 = load float, float* %call133, align 4, !tbaa !8
  %mul134 = fmul float %65, %67
  %add135 = fadd float %mul128, %mul134
  %m_el136 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el136, i32 0, i32 2
  %call138 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx137)
  %68 = load float, float* %call138, align 4, !tbaa !8
  %69 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %69, i32 2)
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call139)
  %70 = load float, float* %call140, align 4, !tbaa !8
  %mul141 = fmul float %68, %70
  %add142 = fadd float %add135, %mul141
  store float %add142, float* %ref.tmp122, align 4, !tbaa !8
  %71 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #9
  %m_el144 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el144, i32 0, i32 0
  %call146 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx145)
  %72 = load float, float* %call146, align 4, !tbaa !8
  %73 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %73, i32 0)
  %call148 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call147)
  %74 = load float, float* %call148, align 4, !tbaa !8
  %mul149 = fmul float %72, %74
  %m_el150 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx151 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el150, i32 0, i32 1
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx151)
  %75 = load float, float* %call152, align 4, !tbaa !8
  %76 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call153 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %76, i32 1)
  %call154 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call153)
  %77 = load float, float* %call154, align 4, !tbaa !8
  %mul155 = fmul float %75, %77
  %add156 = fadd float %mul149, %mul155
  %m_el157 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx158 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el157, i32 0, i32 2
  %call159 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx158)
  %78 = load float, float* %call159, align 4, !tbaa !8
  %79 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call160 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %79, i32 2)
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call160)
  %80 = load float, float* %call161, align 4, !tbaa !8
  %mul162 = fmul float %78, %80
  %add163 = fadd float %add156, %mul162
  store float %add163, float* %ref.tmp143, align 4, !tbaa !8
  %81 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #9
  %m_el165 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx166 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el165, i32 0, i32 0
  %call167 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx166)
  %82 = load float, float* %call167, align 4, !tbaa !8
  %83 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call168 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %83, i32 0)
  %call169 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call168)
  %84 = load float, float* %call169, align 4, !tbaa !8
  %mul170 = fmul float %82, %84
  %m_el171 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el171, i32 0, i32 1
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx172)
  %85 = load float, float* %call173, align 4, !tbaa !8
  %86 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call174 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %86, i32 1)
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call174)
  %87 = load float, float* %call175, align 4, !tbaa !8
  %mul176 = fmul float %85, %87
  %add177 = fadd float %mul170, %mul176
  %m_el178 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el178, i32 0, i32 2
  %call180 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx179)
  %88 = load float, float* %call180, align 4, !tbaa !8
  %89 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call181 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %89, i32 2)
  %call182 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call181)
  %90 = load float, float* %call182, align 4, !tbaa !8
  %mul183 = fmul float %88, %90
  %add184 = fadd float %add177, %mul183
  store float %add184, float* %ref.tmp164, align 4, !tbaa !8
  %call185 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp164)
  %91 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast float* %ref.tmp122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  %94 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  %95 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  %96 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %98 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #9
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !20
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !20
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !20
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %3 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %3) #9
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %4, i32 0, i32 0
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #9
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis4)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %7) #9
  %8 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !20
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12gjkepa2_impl13MinkowskiDiff12EnableMarginEb(%"struct.gjkepa2_impl::MinkowskiDiff"* %this, i1 zeroext %enable) #0 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %enable.addr = alloca i8, align 1
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  %frombool = zext i1 %enable to i8
  store i8 %frombool, i8* %enable.addr, align 1, !tbaa !26
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = load i8, i8* %enable.addr, align 1, !tbaa !26, !range !28
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } { i32 ptrtoint (void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3 to i32), i32 0 }, { i32, i32 }* %Ls, align 4, !tbaa !57
  br label %if.end

if.else:                                          ; preds = %entry
  %Ls2 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } { i32 ptrtoint (void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3 to i32), i32 0 }, { i32, i32 }* %Ls2, align 4, !tbaa !57
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !10
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4, !tbaa !2
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !20
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !8
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !8
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #4 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !20
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !20
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !20
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

declare void @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

declare void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #5

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.gjkepa2_impl::GJK::sSV"* @_ZN12gjkepa2_impl3GJK3sSVC2Ev(%"struct.gjkepa2_impl::GJK::sSV"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"* %this, %"struct.gjkepa2_impl::GJK::sSV"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %this.addr, align 4
  %d = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %d)
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %w)
  ret %"struct.gjkepa2_impl::GJK::sSV"* %this1
}

define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK10InitializeEv(%"struct.gjkepa2_impl::GJK"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_ray = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_ray to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !20
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nfree, align 4, !tbaa !29
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 9
  store i32 2, i32* %m_status, align 4, !tbaa !31
  %m_current = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 7
  store i32 0, i32* %m_current, align 4, !tbaa !30
  %m_distance = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %m_distance, align 4, !tbaa !32
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(128) %"struct.gjkepa2_impl::MinkowskiDiff"* @_ZN12gjkepa2_impl13MinkowskiDiffaSERKS0_(%"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"* nonnull align 4 dereferenceable(128) %0) #3 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %0, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %1 = bitcast [2 x %class.btConvexShape*]* %m_shapes to i8*
  %2 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4, !tbaa !2
  %m_shapes2 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %2, i32 0, i32 0
  %3 = bitcast [2 x %class.btConvexShape*]* %m_shapes2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 8, i1 false)
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %4 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4, !tbaa !2
  %m_toshape13 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %4, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_toshape1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_toshape13)
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %5 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4, !tbaa !2
  %m_toshape04 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %5, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_toshape0, %class.btTransform* nonnull align 4 dereferenceable(64) %m_toshape04)
  %6 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %.addr, align 4, !tbaa !2
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %6, i32 0, i32 3
  %7 = load { i32, i32 }, { i32, i32 }* %Ls, align 4, !tbaa !57
  %Ls6 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  store { i32, i32 } %7, { i32, i32 }* %Ls6, align 4, !tbaa !57
  ret %"struct.gjkepa2_impl::MinkowskiDiff"* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %simplex, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %simplex.addr = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %simplex, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %p = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 1
  %1 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %1, i32 0, i32 2
  %2 = load i32, i32* %rank, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %p, i32 0, i32 %2
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_nfree, align 4, !tbaa !29
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_nfree, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 %dec
  %4 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4, !tbaa !2
  %5 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %5, i32 0, i32 0
  %6 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %rank3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %6, i32 0, i32 2
  %7 = load i32, i32* %rank3, align 4, !tbaa !18
  %arrayidx4 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %7
  store %"struct.gjkepa2_impl::GJK::sSV"* %4, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx4, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %9 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %9, i32 0, i32 0
  %10 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %rank6 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %10, i32 0, i32 2
  %11 = load i32, i32* %rank6, align 4, !tbaa !18
  %inc = add i32 %11, 1
  store i32 %inc, i32* %rank6, align 4, !tbaa !18
  %arrayidx7 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 %11
  %12 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx7, align 4, !tbaa !2
  call void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %12)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %simplex) #0 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %simplex.addr = alloca %"struct.gjkepa2_impl::GJK::sSimplex"*, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSimplex"* %simplex, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 0
  %1 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %simplex.addr, align 4, !tbaa !2
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %1, i32 0, i32 2
  %2 = load i32, i32* %rank, align 4, !tbaa !18
  %dec = add i32 %2, -1
  store i32 %dec, i32* %rank, align 4, !tbaa !18
  %arrayidx = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %dec
  %3 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4, !tbaa !2
  %m_free = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 5
  %m_nfree = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 6
  %4 = load i32, i32* %m_nfree, align 4, !tbaa !29
  %inc = add i32 %4, 1
  store i32 %inc, i32* %m_nfree, align 4, !tbaa !29
  %arrayidx2 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %m_free, i32 0, i32 %4
  store %"struct.gjkepa2_impl::GJK::sSV"* %3, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx2, align 4, !tbaa !2
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4, !tbaa !2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4, !tbaa !2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #1 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %d = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %t = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store float* %w, float** %w.addr, align 4, !tbaa !2
  store i32* %m, i32** %m.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %d)
  store float %call, float* %l, align 4, !tbaa !8
  %4 = load float, float* %l, align 4, !tbaa !8
  %cmp = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = bitcast float* %t to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load float, float* %l, align 4, !tbaa !8
  %cmp1 = fcmp ogt float %6, 0.000000e+00
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %7 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  %fneg = fneg float %call2
  %8 = load float, float* %l, align 4, !tbaa !8
  %div = fdiv float %fneg, %8
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %t, align 4, !tbaa !8
  %9 = load float, float* %t, align 4, !tbaa !8
  %cmp3 = fcmp oge float %9, 1.000000e+00
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %cond.end
  %10 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds float, float* %10, i32 0
  store float 0.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %11 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds float, float* %11, i32 1
  store float 1.000000e+00, float* %arrayidx5, align 4, !tbaa !8
  %12 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 2, i32* %12, align 4, !tbaa !10
  %13 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %13)
  store float %call6, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %cond.end
  %14 = load float, float* %t, align 4, !tbaa !8
  %cmp7 = fcmp ole float %14, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.else12

if.then8:                                         ; preds = %if.else
  %15 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx9 = getelementptr inbounds float, float* %15, i32 0
  store float 1.000000e+00, float* %arrayidx9, align 4, !tbaa !8
  %16 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds float, float* %16, i32 1
  store float 0.000000e+00, float* %arrayidx10, align 4, !tbaa !8
  %17 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 1, i32* %17, align 4, !tbaa !10
  %18 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %18)
  store float %call11, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else12:                                        ; preds = %if.else
  %19 = load float, float* %t, align 4, !tbaa !8
  %20 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx13 = getelementptr inbounds float, float* %20, i32 1
  store float %19, float* %arrayidx13, align 4, !tbaa !8
  %sub = fsub float 1.000000e+00, %19
  %21 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx14 = getelementptr inbounds float, float* %21, i32 0
  store float %sub, float* %arrayidx14, align 4, !tbaa !8
  %22 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 3, i32* %22, align 4, !tbaa !10
  %23 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %24 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %25 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #9
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %d, float* nonnull align 4 dereferenceable(4) %t)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %call16 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call16, float* %retval, align 4
  %26 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #9
  %27 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #9
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.else12, %if.then8, %if.then4
  %28 = bitcast float* %t to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #9
  br label %cleanup17

if.end:                                           ; preds = %entry
  store float -1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup17

cleanup17:                                        ; preds = %if.end, %cleanup
  %29 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #9
  %30 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #9
  %31 = load float, float* %retval, align 4
  ret float %31
}

define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #1 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %vt = alloca [3 x %class.btVector3*], align 4
  %dl = alloca [3 x %class.btVector3], align 16
  %n = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %mindist = alloca float, align 4
  %subw = alloca [2 x float], align 4
  %subm = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %subd = alloca float, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  store float* %w, float** %w.addr, align 4, !tbaa !2
  store i32* %m, i32** %m.addr, align 4, !tbaa !2
  %0 = bitcast [3 x %class.btVector3*]* %vt to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #9
  %arrayinit.begin = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %arrayinit.begin, align 4, !tbaa !2
  %arrayinit.element = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.begin, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %2, %class.btVector3** %arrayinit.element, align 4, !tbaa !2
  %arrayinit.element1 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %class.btVector3* %3, %class.btVector3** %arrayinit.element1, align 4, !tbaa !2
  %4 = bitcast [3 x %class.btVector3]* %dl to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %4) #9
  %arrayinit.begin2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %5 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.begin2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %arrayinit.element3 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin2, i32 1
  %7 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %arrayinit.element4 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element3, i32 1
  %9 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element4, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #9
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %12 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n)
  store float %call, float* %l, align 4, !tbaa !8
  %13 = load float, float* %l, align 4, !tbaa !8
  %cmp = fcmp ogt float %13, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end56

if.then:                                          ; preds = %entry
  %14 = bitcast float* %mindist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  store float -1.000000e+00, float* %mindist, align 4, !tbaa !8
  %15 = bitcast [2 x float]* %subw to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %15) #9
  %16 = bitcast [2 x float]* %subw to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %16, i8 0, i32 8, i1 false)
  %17 = bitcast i32* %subm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #9
  store i32 0, i32* %subm, align 4, !tbaa !10
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %19 = load i32, i32* %i, align 4, !tbaa !10
  %cmp6 = icmp ult i32 %19, 3
  br i1 %cmp6, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %21 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %21
  %22 = load %class.btVector3*, %class.btVector3** %arrayidx7, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #9
  %24 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %24
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %call9 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %cmp10 = fcmp ogt float %call9, 0.000000e+00
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #9
  br i1 %cmp10, label %if.then11, label %if.end32

if.then11:                                        ; preds = %for.body
  %26 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  %27 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3, i32 0, i32 %27
  %28 = load i32, i32* %arrayidx12, align 4, !tbaa !10
  store i32 %28, i32* %j, align 4, !tbaa !10
  %29 = bitcast float* %subd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %30
  %31 = load %class.btVector3*, %class.btVector3** %arrayidx13, align 4, !tbaa !2
  %32 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3*], [3 x %class.btVector3*]* %vt, i32 0, i32 %32
  %33 = load %class.btVector3*, %class.btVector3** %arrayidx14, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 0
  %call15 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %31, %class.btVector3* nonnull align 4 dereferenceable(16) %33, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %subm)
  store float %call15, float* %subd, align 4, !tbaa !8
  %34 = load float, float* %mindist, align 4, !tbaa !8
  %cmp16 = fcmp olt float %34, 0.000000e+00
  br i1 %cmp16, label %if.then18, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then11
  %35 = load float, float* %subd, align 4, !tbaa !8
  %36 = load float, float* %mindist, align 4, !tbaa !8
  %cmp17 = fcmp olt float %35, %36
  br i1 %cmp17, label %if.then18, label %if.end

if.then18:                                        ; preds = %lor.lhs.false, %if.then11
  %37 = load float, float* %subd, align 4, !tbaa !8
  store float %37, float* %mindist, align 4, !tbaa !8
  %38 = load i32, i32* %subm, align 4, !tbaa !10
  %and = and i32 %38, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then18
  %39 = load i32, i32* %i, align 4, !tbaa !10
  %shl = shl i32 1, %39
  br label %cond.end

cond.false:                                       ; preds = %if.then18
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shl, %cond.true ], [ 0, %cond.false ]
  %40 = load i32, i32* %subm, align 4, !tbaa !10
  %and19 = and i32 %40, 2
  %tobool20 = icmp ne i32 %and19, 0
  br i1 %tobool20, label %cond.true21, label %cond.false23

cond.true21:                                      ; preds = %cond.end
  %41 = load i32, i32* %j, align 4, !tbaa !10
  %shl22 = shl i32 1, %41
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true21
  %cond25 = phi i32 [ %shl22, %cond.true21 ], [ 0, %cond.false23 ]
  %add = add nsw i32 %cond, %cond25
  %42 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 %add, i32* %42, align 4, !tbaa !10
  %arrayidx26 = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 0
  %43 = load float, float* %arrayidx26, align 4, !tbaa !8
  %44 = load float*, float** %w.addr, align 4, !tbaa !2
  %45 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx27 = getelementptr inbounds float, float* %44, i32 %45
  store float %43, float* %arrayidx27, align 4, !tbaa !8
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %subw, i32 0, i32 1
  %46 = load float, float* %arrayidx28, align 4, !tbaa !8
  %47 = load float*, float** %w.addr, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx29 = getelementptr inbounds float, float* %47, i32 %48
  store float %46, float* %arrayidx29, align 4, !tbaa !8
  %49 = load float*, float** %w.addr, align 4, !tbaa !2
  %50 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx30 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRjE4imd3, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx30, align 4, !tbaa !10
  %arrayidx31 = getelementptr inbounds float, float* %49, i32 %51
  store float 0.000000e+00, float* %arrayidx31, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %cond.end24, %lor.lhs.false
  %52 = bitcast float* %subd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  %53 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #9
  br label %if.end32

if.end32:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %54 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %54, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %55 = load float, float* %mindist, align 4, !tbaa !8
  %cmp33 = fcmp olt float %55, 0.000000e+00
  br i1 %cmp33, label %if.then34, label %if.end55

if.then34:                                        ; preds = %for.end
  %56 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #9
  %57 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call35 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %57, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  store float %call35, float* %d, align 4, !tbaa !8
  %58 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #9
  %59 = load float, float* %l, align 4, !tbaa !8
  %call36 = call float @_Z6btSqrtf(float %59)
  store float %call36, float* %s, align 4, !tbaa !8
  %60 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #9
  %61 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #9
  %62 = load float, float* %d, align 4, !tbaa !8
  %63 = load float, float* %l, align 4, !tbaa !8
  %div = fdiv float %62, %63
  store float %div, float* %ref.tmp37, align 4, !tbaa !8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %p, %class.btVector3* nonnull align 4 dereferenceable(16) %n, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %64 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #9
  %call38 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  store float %call38, float* %mindist, align 4, !tbaa !8
  %65 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 7, i32* %65, align 4, !tbaa !10
  %66 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #9
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  %67 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #9
  %68 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %68, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp39, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %call42 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp39)
  %69 = load float, float* %s, align 4, !tbaa !8
  %div43 = fdiv float %call42, %69
  %70 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx44 = getelementptr inbounds float, float* %70, i32 0
  store float %div43, float* %arrayidx44, align 4, !tbaa !8
  %71 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %71) #9
  %72 = bitcast %class.btVector3* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %72) #9
  %73 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %73) #9
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 2
  %74 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #9
  %75 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %75, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47)
  %call48 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp45)
  %76 = load float, float* %s, align 4, !tbaa !8
  %div49 = fdiv float %call48, %76
  %77 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx50 = getelementptr inbounds float, float* %77, i32 1
  store float %div49, float* %arrayidx50, align 4, !tbaa !8
  %78 = bitcast %class.btVector3* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #9
  %79 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #9
  %80 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx51 = getelementptr inbounds float, float* %80, i32 0
  %81 = load float, float* %arrayidx51, align 4, !tbaa !8
  %82 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds float, float* %82, i32 1
  %83 = load float, float* %arrayidx52, align 4, !tbaa !8
  %add53 = fadd float %81, %83
  %sub = fsub float 1.000000e+00, %add53
  %84 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds float, float* %84, i32 2
  store float %sub, float* %arrayidx54, align 4, !tbaa !8
  %85 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #9
  %86 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #9
  %87 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #9
  br label %if.end55

if.end55:                                         ; preds = %if.then34, %for.end
  %88 = load float, float* %mindist, align 4, !tbaa !8
  store float %88, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %89 = bitcast i32* %subm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #9
  %90 = bitcast [2 x float]* %subw to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %90) #9
  %91 = bitcast float* %mindist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  br label %cleanup

if.end56:                                         ; preds = %entry
  store float -1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end56, %if.end55
  %92 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %93) #9
  %94 = bitcast [3 x %class.btVector3]* %dl to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %94) #9
  %95 = bitcast [3 x %class.btVector3*]* %vt to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %95) #9
  %96 = load float, float* %retval, align 4
  ret float %96
}

define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %d, float* %w, i32* nonnull align 4 dereferenceable(4) %m) #1 comdat {
entry:
  %retval = alloca float, align 4
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %w.addr = alloca float*, align 4
  %m.addr = alloca i32*, align 4
  %vt = alloca [4 x %class.btVector3*], align 16
  %dl = alloca [3 x %class.btVector3], align 16
  %vl = alloca float, align 4
  %ng = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %mindist = alloca float, align 4
  %subw = alloca [3 x float], align 4
  %subm = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %s = alloca float, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %subd = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  store float* %w, float** %w.addr, align 4, !tbaa !2
  store i32* %m, i32** %m.addr, align 4, !tbaa !2
  %0 = bitcast [4 x %class.btVector3*]* %vt to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %arrayinit.begin = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %1, %class.btVector3** %arrayinit.begin, align 4, !tbaa !2
  %arrayinit.element = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.begin, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %2, %class.btVector3** %arrayinit.element, align 4, !tbaa !2
  %arrayinit.element1 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  store %class.btVector3* %3, %class.btVector3** %arrayinit.element1, align 4, !tbaa !2
  %arrayinit.element2 = getelementptr inbounds %class.btVector3*, %class.btVector3** %arrayinit.element1, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  store %class.btVector3* %4, %class.btVector3** %arrayinit.element2, align 4, !tbaa !2
  %5 = bitcast [3 x %class.btVector3]* %dl to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %5) #9
  %arrayinit.begin3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %7 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.begin3, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %arrayinit.element4 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin3, i32 1
  %8 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element4, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %arrayinit.element5 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element4, i32 1
  %10 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %arrayinit.element5, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %12 = bitcast float* %vl to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 2
  %call = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  store float %call, float* %vl, align 4, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %ng) #9
  %13 = load float, float* %vl, align 4, !tbaa !8
  %14 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #9
  %16 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %17 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %18 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  %19 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #9
  %20 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %21 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call10 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %mul = fmul float %13, %call10
  %cmp = fcmp ole float %mul, 0.000000e+00
  %22 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #9
  %23 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #9
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #9
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %ng, align 1, !tbaa !26
  %25 = load i8, i8* %ng, align 1, !tbaa !26, !range !28
  %tobool = trunc i8 %25 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end66

land.lhs.true:                                    ; preds = %entry
  %26 = load float, float* %vl, align 4, !tbaa !8
  %call11 = call float @_Z6btFabsf(float %26)
  %cmp12 = fcmp ogt float %call11, 0.000000e+00
  br i1 %cmp12, label %if.then, label %if.end66

if.then:                                          ; preds = %land.lhs.true
  %27 = bitcast float* %mindist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #9
  store float -1.000000e+00, float* %mindist, align 4, !tbaa !8
  %28 = bitcast [3 x float]* %subw to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %28) #9
  %29 = bitcast [3 x float]* %subw to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %29, i8 0, i32 12, i1 false)
  %30 = bitcast i32* %subm to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  store i32 0, i32* %subm, align 4, !tbaa !10
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %32 = load i32, i32* %i, align 4, !tbaa !10
  %cmp13 = icmp ult i32 %32, 3
  br i1 %cmp13, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %34 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx14, align 4, !tbaa !10
  store i32 %36, i32* %j, align 4, !tbaa !10
  %37 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #9
  %38 = load float, float* %vl, align 4, !tbaa !8
  %39 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %40 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  %41 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %41
  %42 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %dl, i32 0, i32 %42
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx17)
  %call18 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %mul19 = fmul float %38, %call18
  %43 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  store float %mul19, float* %s, align 4, !tbaa !8
  %44 = load float, float* %s, align 4, !tbaa !8
  %cmp20 = fcmp ogt float %44, 0.000000e+00
  br i1 %cmp20, label %if.then21, label %if.end48

if.then21:                                        ; preds = %for.body
  %45 = bitcast float* %subd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #9
  %46 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx22 = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 %46
  %47 = load %class.btVector3*, %class.btVector3** %arrayidx22, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx23 = getelementptr inbounds [4 x %class.btVector3*], [4 x %class.btVector3*]* %vt, i32 0, i32 %48
  %49 = load %class.btVector3*, %class.btVector3** %arrayidx23, align 4, !tbaa !2
  %50 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 0
  %call24 = call float @_ZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_PfRj(%class.btVector3* nonnull align 4 dereferenceable(16) %47, %class.btVector3* nonnull align 4 dereferenceable(16) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %50, float* %arraydecay, i32* nonnull align 4 dereferenceable(4) %subm)
  store float %call24, float* %subd, align 4, !tbaa !8
  %51 = load float, float* %mindist, align 4, !tbaa !8
  %cmp25 = fcmp olt float %51, 0.000000e+00
  br i1 %cmp25, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then21
  %52 = load float, float* %subd, align 4, !tbaa !8
  %53 = load float, float* %mindist, align 4, !tbaa !8
  %cmp26 = fcmp olt float %52, %53
  br i1 %cmp26, label %if.then27, label %if.end

if.then27:                                        ; preds = %lor.lhs.false, %if.then21
  %54 = load float, float* %subd, align 4, !tbaa !8
  store float %54, float* %mindist, align 4, !tbaa !8
  %55 = load i32, i32* %subm, align 4, !tbaa !10
  %and = and i32 %55, 1
  %tobool28 = icmp ne i32 %and, 0
  br i1 %tobool28, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then27
  %56 = load i32, i32* %i, align 4, !tbaa !10
  %shl = shl i32 1, %56
  br label %cond.end

cond.false:                                       ; preds = %if.then27
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %shl, %cond.true ], [ 0, %cond.false ]
  %57 = load i32, i32* %subm, align 4, !tbaa !10
  %and29 = and i32 %57, 2
  %tobool30 = icmp ne i32 %and29, 0
  br i1 %tobool30, label %cond.true31, label %cond.false33

cond.true31:                                      ; preds = %cond.end
  %58 = load i32, i32* %j, align 4, !tbaa !10
  %shl32 = shl i32 1, %58
  br label %cond.end34

cond.false33:                                     ; preds = %cond.end
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false33, %cond.true31
  %cond35 = phi i32 [ %shl32, %cond.true31 ], [ 0, %cond.false33 ]
  %add = add nsw i32 %cond, %cond35
  %59 = load i32, i32* %subm, align 4, !tbaa !10
  %and36 = and i32 %59, 4
  %tobool37 = icmp ne i32 %and36, 0
  %60 = zext i1 %tobool37 to i64
  %cond38 = select i1 %tobool37, i32 8, i32 0
  %add39 = add nsw i32 %add, %cond38
  %61 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 %add39, i32* %61, align 4, !tbaa !10
  %arrayidx40 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 0
  %62 = load float, float* %arrayidx40, align 4, !tbaa !8
  %63 = load float*, float** %w.addr, align 4, !tbaa !2
  %64 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx41 = getelementptr inbounds float, float* %63, i32 %64
  store float %62, float* %arrayidx41, align 4, !tbaa !8
  %arrayidx42 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 1
  %65 = load float, float* %arrayidx42, align 4, !tbaa !8
  %66 = load float*, float** %w.addr, align 4, !tbaa !2
  %67 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx43 = getelementptr inbounds float, float* %66, i32 %67
  store float %65, float* %arrayidx43, align 4, !tbaa !8
  %68 = load float*, float** %w.addr, align 4, !tbaa !2
  %69 = load i32, i32* %j, align 4, !tbaa !10
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3GJK13projectoriginERK9btVector3S3_S3_S3_PfRjE4imd3, i32 0, i32 %69
  %70 = load i32, i32* %arrayidx44, align 4, !tbaa !10
  %arrayidx45 = getelementptr inbounds float, float* %68, i32 %70
  store float 0.000000e+00, float* %arrayidx45, align 4, !tbaa !8
  %arrayidx46 = getelementptr inbounds [3 x float], [3 x float]* %subw, i32 0, i32 2
  %71 = load float, float* %arrayidx46, align 4, !tbaa !8
  %72 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx47 = getelementptr inbounds float, float* %72, i32 3
  store float %71, float* %arrayidx47, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %cond.end34, %lor.lhs.false
  %73 = bitcast float* %subd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #9
  br label %if.end48

if.end48:                                         ; preds = %if.end, %for.body
  %74 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #9
  %75 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end48
  %76 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %76, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %77 = load float, float* %mindist, align 4, !tbaa !8
  %cmp49 = fcmp olt float %77, 0.000000e+00
  br i1 %cmp49, label %if.then50, label %if.end65

if.then50:                                        ; preds = %for.end
  store float 0.000000e+00, float* %mindist, align 4, !tbaa !8
  %78 = load i32*, i32** %m.addr, align 4, !tbaa !2
  store i32 15, i32* %78, align 4, !tbaa !10
  %79 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %80 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %81 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call51 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %79, %class.btVector3* nonnull align 4 dereferenceable(16) %80, %class.btVector3* nonnull align 4 dereferenceable(16) %81)
  %82 = load float, float* %vl, align 4, !tbaa !8
  %div = fdiv float %call51, %82
  %83 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx52 = getelementptr inbounds float, float* %83, i32 0
  store float %div, float* %arrayidx52, align 4, !tbaa !8
  %84 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %85 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %86 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call53 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %84, %class.btVector3* nonnull align 4 dereferenceable(16) %85, %class.btVector3* nonnull align 4 dereferenceable(16) %86)
  %87 = load float, float* %vl, align 4, !tbaa !8
  %div54 = fdiv float %call53, %87
  %88 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds float, float* %88, i32 1
  store float %div54, float* %arrayidx55, align 4, !tbaa !8
  %89 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %90 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %91 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call56 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %89, %class.btVector3* nonnull align 4 dereferenceable(16) %90, %class.btVector3* nonnull align 4 dereferenceable(16) %91)
  %92 = load float, float* %vl, align 4, !tbaa !8
  %div57 = fdiv float %call56, %92
  %93 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx58 = getelementptr inbounds float, float* %93, i32 2
  store float %div57, float* %arrayidx58, align 4, !tbaa !8
  %94 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds float, float* %94, i32 0
  %95 = load float, float* %arrayidx59, align 4, !tbaa !8
  %96 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx60 = getelementptr inbounds float, float* %96, i32 1
  %97 = load float, float* %arrayidx60, align 4, !tbaa !8
  %add61 = fadd float %95, %97
  %98 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx62 = getelementptr inbounds float, float* %98, i32 2
  %99 = load float, float* %arrayidx62, align 4, !tbaa !8
  %add63 = fadd float %add61, %99
  %sub = fsub float 1.000000e+00, %add63
  %100 = load float*, float** %w.addr, align 4, !tbaa !2
  %arrayidx64 = getelementptr inbounds float, float* %100, i32 3
  store float %sub, float* %arrayidx64, align 4, !tbaa !8
  br label %if.end65

if.end65:                                         ; preds = %if.then50, %for.end
  %101 = load float, float* %mindist, align 4, !tbaa !8
  store float %101, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %102 = bitcast i32* %subm to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast [3 x float]* %subw to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %103) #9
  %104 = bitcast float* %mindist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  br label %cleanup

if.end66:                                         ; preds = %land.lhs.true, %entry
  store float -1.000000e+00, float* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end66, %if.end65
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %ng) #9
  %105 = bitcast float* %vl to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast [3 x %class.btVector3]* %dl to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %106) #9
  %107 = bitcast [4 x %class.btVector3*]* %vt to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #9
  %108 = load float, float* %retval, align 4
  ret float %108
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !8
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define linkonce_odr hidden void @_ZNK12gjkepa2_impl3GJK10getsupportERK9btVector3RNS0_3sSVE(%"struct.gjkepa2_impl::GJK"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %"struct.gjkepa2_impl::GJK::sSV"* nonnull align 4 dereferenceable(32) %sv) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %sv.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %sv, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call, float* %ref.tmp2, align 4, !tbaa !8
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %4 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4, !tbaa !2
  %d3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %4, i32 0, i32 0
  %5 = bitcast %class.btVector3* %d3 to i8*
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !20
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %m_shape = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 0
  %10 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4, !tbaa !2
  %d5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %10, i32 0, i32 0
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %"struct.gjkepa2_impl::MinkowskiDiff"* %m_shape, %class.btVector3* nonnull align 4 dereferenceable(16) %d5)
  %11 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %sv.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %11, i32 0, i32 1
  %12 = bitcast %class.btVector3* %w to i8*
  %13 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !20
  %14 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff7SupportERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #4 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %3 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %5 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  %6 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #9
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support0ERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #4 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 0
  %0 = load %class.btConvexShape*, %class.btConvexShape** %arrayidx, align 4, !tbaa !2
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  %1 = load { i32, i32 }, { i32, i32 }* %Ls, align 4, !tbaa !57
  %memptr.adj = extractvalue { i32, i32 } %1, 1
  %memptr.adj.shifted = ashr i32 %memptr.adj, 1
  %2 = bitcast %class.btConvexShape* %0 to i8*
  %3 = getelementptr inbounds i8, i8* %2, i32 %memptr.adj.shifted
  %this.adjusted = bitcast i8* %3 to %class.btConvexShape*
  %memptr.ptr = extractvalue { i32, i32 } %1, 0
  %4 = and i32 %memptr.adj, 1
  %memptr.isvirtual = icmp ne i32 %4, 0
  br i1 %memptr.isvirtual, label %memptr.virtual, label %memptr.nonvirtual

memptr.virtual:                                   ; preds = %entry
  %5 = bitcast %class.btConvexShape* %this.adjusted to i8**
  %vtable = load i8*, i8** %5, align 4, !tbaa !51
  %6 = getelementptr i8, i8* %vtable, i32 %memptr.ptr, !nosanitize !58
  %7 = bitcast i8* %6 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, !nosanitize !58
  %memptr.virtualfn = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %7, align 4, !nosanitize !58
  br label %memptr.end

memptr.nonvirtual:                                ; preds = %entry
  %memptr.nonvirtualfn = inttoptr i32 %memptr.ptr to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*
  br label %memptr.end

memptr.end:                                       ; preds = %memptr.nonvirtual, %memptr.virtual
  %8 = phi void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* [ %memptr.virtualfn, %memptr.virtual ], [ %memptr.nonvirtualfn, %memptr.nonvirtual ]
  %9 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void %8(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %this.adjusted, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK12gjkepa2_impl13MinkowskiDiff8Support1ERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %d) #4 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::MinkowskiDiff"*, align 4
  %d.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::MinkowskiDiff"* %this, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %d, %class.btVector3** %d.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::MinkowskiDiff"*, %"struct.gjkepa2_impl::MinkowskiDiff"** %this.addr, align 4
  %m_toshape0 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %m_shapes = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x %class.btConvexShape*], [2 x %class.btConvexShape*]* %m_shapes, i32 0, i32 1
  %1 = load %class.btConvexShape*, %class.btConvexShape** %arrayidx, align 4, !tbaa !2
  %Ls = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 3
  %2 = load { i32, i32 }, { i32, i32 }* %Ls, align 4, !tbaa !57
  %memptr.adj = extractvalue { i32, i32 } %2, 1
  %memptr.adj.shifted = ashr i32 %memptr.adj, 1
  %3 = bitcast %class.btConvexShape* %1 to i8*
  %4 = getelementptr inbounds i8, i8* %3, i32 %memptr.adj.shifted
  %this.adjusted = bitcast i8* %4 to %class.btConvexShape*
  %memptr.ptr = extractvalue { i32, i32 } %2, 0
  %5 = and i32 %memptr.adj, 1
  %memptr.isvirtual = icmp ne i32 %5, 0
  br i1 %memptr.isvirtual, label %memptr.virtual, label %memptr.nonvirtual

memptr.virtual:                                   ; preds = %entry
  %6 = bitcast %class.btConvexShape* %this.adjusted to i8**
  %vtable = load i8*, i8** %6, align 4, !tbaa !51
  %7 = getelementptr i8, i8* %vtable, i32 %memptr.ptr, !nosanitize !58
  %8 = bitcast i8* %7 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, !nosanitize !58
  %memptr.virtualfn = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %8, align 4, !nosanitize !58
  br label %memptr.end

memptr.nonvirtual:                                ; preds = %entry
  %memptr.nonvirtualfn = inttoptr i32 %memptr.ptr to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*
  br label %memptr.end

memptr.end:                                       ; preds = %memptr.nonvirtual, %memptr.virtual
  %9 = phi void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)* [ %memptr.virtualfn, %memptr.virtual ], [ %memptr.nonvirtualfn, %memptr.nonvirtual ]
  %10 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #9
  %m_toshape1 = getelementptr inbounds %"struct.gjkepa2_impl::MinkowskiDiff", %"struct.gjkepa2_impl::MinkowskiDiff"* %this1, i32 0, i32 1
  %11 = load %class.btVector3*, %class.btVector3** %d.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_toshape1, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  call void %9(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %this.adjusted, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %m_toshape0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %12 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #9
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !8
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !8
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !8
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !8
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !8
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !8
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !8
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !8
  %0 = load float, float* %y.addr, align 4, !tbaa !8
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !8
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !8
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !8
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !8
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !8
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !8
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !8
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !8
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !8
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !8
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #9
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #9
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) #0 comdat {
entry:
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4, !tbaa !2
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4, !tbaa !2
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !8
  %2 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !8
  %mul = fmul float %1, %3
  %4 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !8
  %mul3 = fmul float %mul, %5
  %6 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %6)
  %7 = load float, float* %call4, align 4, !tbaa !8
  %8 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call5, align 4, !tbaa !8
  %mul6 = fmul float %7, %9
  %10 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %10)
  %11 = load float, float* %call7, align 4, !tbaa !8
  %mul8 = fmul float %mul6, %11
  %add = fadd float %mul3, %mul8
  %12 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %12)
  %13 = load float, float* %call9, align 4, !tbaa !8
  %14 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %14)
  %15 = load float, float* %call10, align 4, !tbaa !8
  %mul11 = fmul float %13, %15
  %16 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %16)
  %17 = load float, float* %call12, align 4, !tbaa !8
  %mul13 = fmul float %mul11, %17
  %sub = fsub float %add, %mul13
  %18 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call14, align 4, !tbaa !8
  %20 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %20)
  %21 = load float, float* %call15, align 4, !tbaa !8
  %mul16 = fmul float %19, %21
  %22 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %22)
  %23 = load float, float* %call17, align 4, !tbaa !8
  %mul18 = fmul float %mul16, %23
  %sub19 = fsub float %sub, %mul18
  %24 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %24)
  %25 = load float, float* %call20, align 4, !tbaa !8
  %26 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %26)
  %27 = load float, float* %call21, align 4, !tbaa !8
  %mul22 = fmul float %25, %27
  %28 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call23, align 4, !tbaa !8
  %mul24 = fmul float %mul22, %29
  %add25 = fadd float %sub19, %mul24
  %30 = load %class.btVector3*, %class.btVector3** %a.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call26, align 4, !tbaa !8
  %32 = load %class.btVector3*, %class.btVector3** %b.addr, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %32)
  %33 = load float, float* %call27, align 4, !tbaa !8
  %mul28 = fmul float %31, %33
  %34 = load %class.btVector3*, %class.btVector3** %c.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %34)
  %35 = load float, float* %call29, align 4, !tbaa !8
  %mul30 = fmul float %mul28, %35
  %sub31 = fsub float %add25, %mul30
  ret float %sub31
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !8
  %0 = load float, float* %x.addr, align 4, !tbaa !8
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !8
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !8
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !8
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !8
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !8
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !8
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !8
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !8
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA5sFaceC2Ev(%"struct.gjkepa2_impl::EPA::sFace"* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %this, %"struct.gjkepa2_impl::EPA::sFace"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %this.addr, align 4
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  ret %"struct.gjkepa2_impl::EPA::sFace"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sList"* @_ZN12gjkepa2_impl3EPA5sListC2Ev(%"struct.gjkepa2_impl::EPA::sList"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %this, %"struct.gjkepa2_impl::EPA::sList"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %this.addr, align 4
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %this1, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !59
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %this1, i32 0, i32 1
  store i32 0, i32* %count, align 4, !tbaa !60
  ret %"struct.gjkepa2_impl::EPA::sList"* %this1
}

define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA10InitializeEv(%"struct.gjkepa2_impl::EPA"* %this) #1 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %i = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 9, i32* %m_status, align 4, !tbaa !40
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_normal = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 2
  %4 = bitcast %class.btVector3* %m_normal to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !20
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #9
  %m_depth = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %m_depth, align 4, !tbaa !38
  %m_nextsv = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 6
  store i32 0, i32* %m_nextsv, align 4, !tbaa !41
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp ult i32 %11, 128
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %m_fc_store = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 5
  %13 = load i32, i32* %i, align 4, !tbaa !10
  %sub = sub i32 128, %13
  %sub5 = sub i32 %sub, 1
  %arrayidx = getelementptr inbounds [128 x %"struct.gjkepa2_impl::EPA::sFace"], [128 x %"struct.gjkepa2_impl::EPA::sFace"]* %m_fc_store, i32 0, i32 %sub5
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %list, %"struct.gjkepa2_impl::EPA::sFace"* %face) #3 comdat {
entry:
  %list.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %list, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4, !tbaa !2
  %1 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %1, i32 0, i32 0
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !59
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l1 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %3, i32 0, i32 4
  %arrayidx2 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l1, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %2, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx2, align 4, !tbaa !2
  %4 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %4, i32 0, i32 0
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4, !tbaa !59
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %7 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %7, i32 0, i32 0
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root4, align 4, !tbaa !59
  %l5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 4
  %arrayidx6 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l5, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %6, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %10 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %10, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %root7, align 4, !tbaa !59
  %11 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %11, i32 0, i32 1
  %12 = load i32, i32* %count, align 4, !tbaa !60
  %inc = add i32 %12, 1
  store i32 %inc, i32* %count, align 4, !tbaa !60
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::GJK"*, align 4
  %i = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %axis = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %d = alloca %class.btVector3, align 4
  %i23 = alloca i32, align 4
  %axis28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %p = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %ref.tmp109 = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca %class.btVector3, align 4
  store %"struct.gjkepa2_impl::GJK"* %this, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::GJK"*, %"struct.gjkepa2_impl::GJK"** %this.addr, align 4
  %m_simplex = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %0 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex, align 4, !tbaa !12
  %rank = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %0, i32 0, i32 2
  %1 = load i32, i32* %rank, align 4, !tbaa !18
  switch i32 %1, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb16
    i32 3, label %sw.bb62
    i32 4, label %sw.bb99
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  store i32 0, i32* %i, align 4, !tbaa !10
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb
  %3 = load i32, i32* %i, align 4, !tbaa !10
  %cmp = icmp ult i32 %3, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 3, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #9
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !8
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %8 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  %9 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis)
  %11 = load i32, i32* %i, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds float, float* %call4, i32 %11
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %m_simplex5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %12 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex5, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %axis)
  %call6 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  %m_simplex7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %13 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex7, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %13)
  %m_simplex8 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %14 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex8, align 4, !tbaa !12
  %15 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %axis)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %16 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #9
  %call10 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end12:                                         ; preds = %if.end
  %m_simplex13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %17 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex13, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %17)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end12, %if.then11, %if.then
  %18 = bitcast %class.btVector3* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup14 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %19 = load i32, i32* %i, align 4, !tbaa !10
  %inc = add i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !10
  br label %for.cond

cleanup14:                                        ; preds = %cleanup, %for.cond.cleanup
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  %cleanup.dest15 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest15, label %unreachable [
    i32 3, label %for.end
    i32 1, label %return
  ]

for.end:                                          ; preds = %cleanup14
  br label %sw.epilog

sw.bb16:                                          ; preds = %entry
  %21 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #9
  %m_simplex17 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %22 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex17, align 4, !tbaa !12
  %c = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %22, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 1
  %23 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx18, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %23, i32 0, i32 1
  %m_simplex19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %24 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex19, align 4, !tbaa !12
  %c20 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %24, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c20, i32 0, i32 0
  %25 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx21, align 4, !tbaa !2
  %w22 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %25, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w22)
  %26 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #9
  store i32 0, i32* %i23, align 4, !tbaa !10
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc54, %sw.bb16
  %27 = load i32, i32* %i23, align 4, !tbaa !10
  %cmp25 = icmp ult i32 %27, 3
  br i1 %cmp25, label %for.body27, label %for.cond.cleanup26

for.cond.cleanup26:                               ; preds = %for.cond24
  store i32 6, i32* %cleanup.dest.slot, align 4
  br label %cleanup56

for.body27:                                       ; preds = %for.cond24
  %28 = bitcast %class.btVector3* %axis28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #9
  %29 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  store float 0.000000e+00, float* %ref.tmp29, align 4, !tbaa !8
  %30 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #9
  store float 0.000000e+00, float* %ref.tmp30, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #9
  store float 0.000000e+00, float* %ref.tmp31, align 4, !tbaa !8
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axis28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %32 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %34 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %axis28)
  %35 = load i32, i32* %i23, align 4, !tbaa !10
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %35
  store float 1.000000e+00, float* %arrayidx34, align 4, !tbaa !8
  %36 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #9
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %p, %class.btVector3* nonnull align 4 dereferenceable(16) %d, %class.btVector3* nonnull align 4 dereferenceable(16) %axis28)
  %call35 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  %cmp36 = fcmp ogt float %call35, 0.000000e+00
  br i1 %cmp36, label %if.then37, label %if.end49

if.then37:                                        ; preds = %for.body27
  %m_simplex38 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %37 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex38, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %call39 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %if.then37
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

if.end41:                                         ; preds = %if.then37
  %m_simplex42 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %38 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex42, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %38)
  %m_simplex43 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %39 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex43, align 4, !tbaa !12
  %40 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp44)
  %41 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #9
  %call45 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.end41
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

if.end47:                                         ; preds = %if.end41
  %m_simplex48 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %42 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex48, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %42)
  br label %if.end49

if.end49:                                         ; preds = %if.end47, %for.body27
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup50

cleanup50:                                        ; preds = %if.end49, %if.then46, %if.then40
  %43 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  %44 = bitcast %class.btVector3* %axis28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %44) #9
  %cleanup.dest52 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest52, label %cleanup56 [
    i32 0, label %cleanup.cont53
  ]

cleanup.cont53:                                   ; preds = %cleanup50
  br label %for.inc54

for.inc54:                                        ; preds = %cleanup.cont53
  %45 = load i32, i32* %i23, align 4, !tbaa !10
  %inc55 = add i32 %45, 1
  store i32 %inc55, i32* %i23, align 4, !tbaa !10
  br label %for.cond24

cleanup56:                                        ; preds = %cleanup50, %for.cond.cleanup26
  %46 = bitcast i32* %i23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #9
  %cleanup.dest57 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest57, label %cleanup59 [
    i32 6, label %for.end58
  ]

for.end58:                                        ; preds = %cleanup56
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup59

cleanup59:                                        ; preds = %for.end58, %cleanup56
  %47 = bitcast %class.btVector3* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %cleanup.dest60 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest60, label %unreachable [
    i32 0, label %cleanup.cont61
    i32 1, label %return
  ]

cleanup.cont61:                                   ; preds = %cleanup59
  br label %sw.epilog

sw.bb62:                                          ; preds = %entry
  %48 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %48) #9
  %49 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #9
  %m_simplex64 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %50 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex64, align 4, !tbaa !12
  %c65 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %50, i32 0, i32 0
  %arrayidx66 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c65, i32 0, i32 1
  %51 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx66, align 4, !tbaa !2
  %w67 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %51, i32 0, i32 1
  %m_simplex68 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %52 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex68, align 4, !tbaa !12
  %c69 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %52, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c69, i32 0, i32 0
  %53 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx70, align 4, !tbaa !2
  %w71 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %53, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp63, %class.btVector3* nonnull align 4 dereferenceable(16) %w67, %class.btVector3* nonnull align 4 dereferenceable(16) %w71)
  %54 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %54) #9
  %m_simplex73 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %55 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex73, align 4, !tbaa !12
  %c74 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %55, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c74, i32 0, i32 2
  %56 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx75, align 4, !tbaa !2
  %w76 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %56, i32 0, i32 1
  %m_simplex77 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %57 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex77, align 4, !tbaa !12
  %c78 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %57, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c78, i32 0, i32 0
  %58 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx79, align 4, !tbaa !2
  %w80 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %58, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp72, %class.btVector3* nonnull align 4 dereferenceable(16) %w76, %class.btVector3* nonnull align 4 dereferenceable(16) %w80)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp72)
  %59 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %59) #9
  %60 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #9
  %call81 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n)
  %cmp82 = fcmp ogt float %call81, 0.000000e+00
  br i1 %cmp82, label %if.then83, label %if.end95

if.then83:                                        ; preds = %sw.bb62
  %m_simplex84 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %61 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex84, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %61, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %call85 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call85, label %if.then86, label %if.end87

if.then86:                                        ; preds = %if.then83
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup96

if.end87:                                         ; preds = %if.then83
  %m_simplex88 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %62 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex88, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %62)
  %m_simplex89 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %63 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex89, align 4, !tbaa !12
  %64 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  call void @_ZN12gjkepa2_impl3GJK13appendverticeERNS0_8sSimplexERK9btVector3(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %63, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %65 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %65) #9
  %call91 = call zeroext i1 @_ZN12gjkepa2_impl3GJK13EncloseOriginEv(%"struct.gjkepa2_impl::GJK"* %this1)
  br i1 %call91, label %if.then92, label %if.end93

if.then92:                                        ; preds = %if.end87
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup96

if.end93:                                         ; preds = %if.end87
  %m_simplex94 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %66 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex94, align 4, !tbaa !12
  call void @_ZN12gjkepa2_impl3GJK13removeverticeERNS0_8sSimplexE(%"struct.gjkepa2_impl::GJK"* %this1, %"struct.gjkepa2_impl::GJK::sSimplex"* nonnull align 4 dereferenceable(36) %66)
  br label %if.end95

if.end95:                                         ; preds = %if.end93, %sw.bb62
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup96

cleanup96:                                        ; preds = %if.end95, %if.then92, %if.then86
  %67 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #9
  %cleanup.dest97 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest97, label %unreachable [
    i32 0, label %cleanup.cont98
    i32 1, label %return
  ]

cleanup.cont98:                                   ; preds = %cleanup96
  br label %sw.epilog

sw.bb99:                                          ; preds = %entry
  %68 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #9
  %m_simplex101 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %69 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex101, align 4, !tbaa !12
  %c102 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %69, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c102, i32 0, i32 0
  %70 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx103, align 4, !tbaa !2
  %w104 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %70, i32 0, i32 1
  %m_simplex105 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %71 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex105, align 4, !tbaa !12
  %c106 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %71, i32 0, i32 0
  %arrayidx107 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c106, i32 0, i32 3
  %72 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx107, align 4, !tbaa !2
  %w108 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %72, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp100, %class.btVector3* nonnull align 4 dereferenceable(16) %w104, %class.btVector3* nonnull align 4 dereferenceable(16) %w108)
  %73 = bitcast %class.btVector3* %ref.tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %73) #9
  %m_simplex110 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %74 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex110, align 4, !tbaa !12
  %c111 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %74, i32 0, i32 0
  %arrayidx112 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c111, i32 0, i32 1
  %75 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx112, align 4, !tbaa !2
  %w113 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %75, i32 0, i32 1
  %m_simplex114 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %76 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex114, align 4, !tbaa !12
  %c115 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %76, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c115, i32 0, i32 3
  %77 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx116, align 4, !tbaa !2
  %w117 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %77, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp109, %class.btVector3* nonnull align 4 dereferenceable(16) %w113, %class.btVector3* nonnull align 4 dereferenceable(16) %w117)
  %78 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %78) #9
  %m_simplex119 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %79 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex119, align 4, !tbaa !12
  %c120 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %79, i32 0, i32 0
  %arrayidx121 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c120, i32 0, i32 2
  %80 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx121, align 4, !tbaa !2
  %w122 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %80, i32 0, i32 1
  %m_simplex123 = getelementptr inbounds %"struct.gjkepa2_impl::GJK", %"struct.gjkepa2_impl::GJK"* %this1, i32 0, i32 8
  %81 = load %"struct.gjkepa2_impl::GJK::sSimplex"*, %"struct.gjkepa2_impl::GJK::sSimplex"** %m_simplex123, align 4, !tbaa !12
  %c124 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSimplex", %"struct.gjkepa2_impl::GJK::sSimplex"* %81, i32 0, i32 0
  %arrayidx125 = getelementptr inbounds [4 x %"struct.gjkepa2_impl::GJK::sSV"*], [4 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c124, i32 0, i32 3
  %82 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx125, align 4, !tbaa !2
  %w126 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %82, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %w122, %class.btVector3* nonnull align 4 dereferenceable(16) %w126)
  %call127 = call float @_ZN12gjkepa2_impl3GJK3detERK9btVector3S3_S3_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp109, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp118)
  %call128 = call float @_Z6btFabsf(float %call127)
  %cmp129 = fcmp ogt float %call128, 0.000000e+00
  %83 = bitcast %class.btVector3* %ref.tmp118 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #9
  %84 = bitcast %class.btVector3* %ref.tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #9
  %85 = bitcast %class.btVector3* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #9
  br i1 %cmp129, label %if.then130, label %if.end131

if.then130:                                       ; preds = %sw.bb99
  store i1 true, i1* %retval, align 1
  br label %return

if.end131:                                        ; preds = %sw.bb99
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %if.end131, %cleanup.cont98, %cleanup.cont61, %for.end
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %sw.epilog, %if.then130, %cleanup96, %cleanup59, %cleanup14
  %86 = load i1, i1* %retval, align 1
  ret i1 %86

unreachable:                                      ; preds = %cleanup96, %cleanup59, %cleanup14
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %list, %"struct.gjkepa2_impl::EPA::sFace"* %face) #3 comdat {
entry:
  %list.addr = alloca %"struct.gjkepa2_impl::EPA::sList"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  store %"struct.gjkepa2_impl::EPA::sList"* %list, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 1
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4, !tbaa !2
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l1 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %2, i32 0, i32 4
  %arrayidx2 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l1, i32 0, i32 0
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx2, align 4, !tbaa !2
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 4
  %arrayidx4 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l3, i32 0, i32 1
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx4, align 4, !tbaa !2
  %l5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 4
  %arrayidx6 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l5, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %3, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %6, i32 0, i32 4
  %arrayidx8 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l7, i32 0, i32 0
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx8, align 4, !tbaa !2
  %tobool9 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %7, null
  br i1 %tobool9, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l11 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 4
  %arrayidx12 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l11, i32 0, i32 1
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx12, align 4, !tbaa !2
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 4
  %arrayidx14 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l13, i32 0, i32 0
  %11 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx14, align 4, !tbaa !2
  %l15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %11, i32 0, i32 4
  %arrayidx16 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l15, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx16, align 4, !tbaa !2
  br label %if.end17

if.end17:                                         ; preds = %if.then10, %if.end
  %12 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %13 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %13, i32 0, i32 0
  %14 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !59
  %cmp = icmp eq %"struct.gjkepa2_impl::EPA::sFace"* %12, %14
  br i1 %cmp, label %if.then18, label %if.end22

if.then18:                                        ; preds = %if.end17
  %15 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %l19 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %15, i32 0, i32 4
  %arrayidx20 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l19, i32 0, i32 1
  %16 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx20, align 4, !tbaa !2
  %17 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %root21 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %17, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %16, %"struct.gjkepa2_impl::EPA::sFace"** %root21, align 4, !tbaa !59
  br label %if.end22

if.end22:                                         ; preds = %if.then18, %if.end17
  %18 = load %"struct.gjkepa2_impl::EPA::sList"*, %"struct.gjkepa2_impl::EPA::sList"** %list.addr, align 4, !tbaa !2
  %count = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %18, i32 0, i32 1
  %19 = load i32, i32* %count, align 4, !tbaa !60
  %dec = add i32 %19, -1
  store i32 %dec, i32* %count, align 4, !tbaa !60
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIPN12gjkepa2_impl3GJK3sSVEEvRT_S5_(%"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %a, %"struct.gjkepa2_impl::GJK::sSV"** nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"**, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"**, align 4
  %tmp = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  store %"struct.gjkepa2_impl::GJK::sSV"** %a, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"** %b, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4, !tbaa !2
  %0 = bitcast %"struct.gjkepa2_impl::GJK::sSV"** %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4, !tbaa !2
  %2 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %1, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %2, %"struct.gjkepa2_impl::GJK::sSV"** %tmp, align 4, !tbaa !2
  %3 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4, !tbaa !2
  %4 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %3, align 4, !tbaa !2
  %5 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %a.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %4, %"struct.gjkepa2_impl::GJK::sSV"** %5, align 4, !tbaa !2
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %tmp, align 4, !tbaa !2
  %7 = load %"struct.gjkepa2_impl::GJK::sSV"**, %"struct.gjkepa2_impl::GJK::sSV"*** %b.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %6, %"struct.gjkepa2_impl::GJK::sSV"** %7, align 4, !tbaa !2
  %8 = bitcast %"struct.gjkepa2_impl::GJK::sSV"** %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %tmp = alloca float, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %a.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !8
  store float %2, float* %tmp, align 4, !tbaa !8
  %3 = load float*, float** %b.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !8
  %5 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %4, float* %5, align 4, !tbaa !8
  %6 = load float, float* %tmp, align 4, !tbaa !8
  %7 = load float*, float** %b.addr, align 4, !tbaa !2
  store float %6, float* %7, align 4, !tbaa !8
  %8 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"* %c, i1 zeroext %forced) #1 comdat {
entry:
  %retval = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %c.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %forced.addr = alloca i8, align 1
  %face = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %l = alloca float, align 4
  %v = alloca i8, align 1
  %cleanup.dest.slot = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %c, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4, !tbaa !2
  %frombool = zext i1 %forced to i8
  store i8 %frombool, i8* %forced.addr, align 1, !tbaa !26
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock, i32 0, i32 0
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !61
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %0, null
  br i1 %tobool, label %if.then, label %if.end45

if.then:                                          ; preds = %entry
  %1 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %face to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #9
  %m_stock2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock2, i32 0, i32 0
  %2 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root3, align 4, !tbaa !61
  store %"struct.gjkepa2_impl::EPA::sFace"* %2, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %m_stock4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock4, %"struct.gjkepa2_impl::EPA::sFace"* %3)
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull, %"struct.gjkepa2_impl::EPA::sFace"* %4)
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %pass = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 6
  store i8 0, i8* %pass, align 1, !tbaa !44
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %c5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %7, i32 0, i32 2
  %arrayidx = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c5, i32 0, i32 0
  store %"struct.gjkepa2_impl::GJK::sSV"* %6, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx, align 4, !tbaa !2
  %8 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %c6 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %9, i32 0, i32 2
  %arrayidx7 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c6, i32 0, i32 1
  store %"struct.gjkepa2_impl::GJK::sSV"* %8, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx7, align 4, !tbaa !2
  %10 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4, !tbaa !2
  %11 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %c8 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %11, i32 0, i32 2
  %arrayidx9 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c8, i32 0, i32 2
  store %"struct.gjkepa2_impl::GJK::sSV"* %10, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx9, align 4, !tbaa !2
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #9
  %13 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #9
  %14 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %14, i32 0, i32 1
  %15 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %15, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w11)
  %16 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #9
  %17 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4, !tbaa !2
  %w13 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %17, i32 0, i32 1
  %18 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w14 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %18, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %w13, %class.btVector3* nonnull align 4 dereferenceable(16) %w14)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %19 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %19, i32 0, i32 0
  %20 = bitcast %class.btVector3* %n to i8*
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !20
  %22 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #9
  %23 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #9
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #9
  %25 = bitcast float* %l to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #9
  %26 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %n15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %26, i32 0, i32 0
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %n15)
  store float %call, float* %l, align 4, !tbaa !8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %v) #9
  %27 = load float, float* %l, align 4, !tbaa !8
  %cmp = fcmp ogt float %27, 0x3F1A36E2E0000000
  %frombool16 = zext i1 %cmp to i8
  store i8 %frombool16, i8* %v, align 1, !tbaa !26
  %28 = load i8, i8* %v, align 1, !tbaa !26, !range !28
  %tobool17 = trunc i8 %28 to i1
  br i1 %tobool17, label %if.then18, label %if.else38

if.then18:                                        ; preds = %if.then
  %29 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %30 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %31 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %32 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %32, i32 0, i32 1
  %call19 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %29, %"struct.gjkepa2_impl::GJK::sSV"* %30, %"struct.gjkepa2_impl::GJK::sSV"* %31, float* nonnull align 4 dereferenceable(4) %d)
  br i1 %call19, label %if.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then18
  %33 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %34 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %35 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4, !tbaa !2
  %36 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %d20 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %36, i32 0, i32 1
  %call21 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %33, %"struct.gjkepa2_impl::GJK::sSV"* %34, %"struct.gjkepa2_impl::GJK::sSV"* %35, float* nonnull align 4 dereferenceable(4) %d20)
  br i1 %call21, label %if.end, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %lor.lhs.false
  %37 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %38 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %c.addr, align 4, !tbaa !2
  %39 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %40 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %d23 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %40, i32 0, i32 1
  %call24 = call zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::EPA::sFace"* %37, %"struct.gjkepa2_impl::GJK::sSV"* %38, %"struct.gjkepa2_impl::GJK::sSV"* %39, float* nonnull align 4 dereferenceable(4) %d23)
  br i1 %call24, label %if.end, label %if.then25

if.then25:                                        ; preds = %lor.lhs.false22
  %41 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w26 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %41, i32 0, i32 1
  %42 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %n27 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %42, i32 0, i32 0
  %call28 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w26, %class.btVector3* nonnull align 4 dereferenceable(16) %n27)
  %43 = load float, float* %l, align 4, !tbaa !8
  %div = fdiv float %call28, %43
  %44 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %d29 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %44, i32 0, i32 1
  store float %div, float* %d29, align 4, !tbaa !46
  br label %if.end

if.end:                                           ; preds = %if.then25, %lor.lhs.false22, %lor.lhs.false, %if.then18
  %45 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %n30 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %45, i32 0, i32 0
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %n30, float* nonnull align 4 dereferenceable(4) %l)
  %46 = load i8, i8* %forced.addr, align 1, !tbaa !26, !range !28
  %tobool32 = trunc i8 %46 to i1
  br i1 %tobool32, label %if.then36, label %lor.lhs.false33

lor.lhs.false33:                                  ; preds = %if.end
  %47 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  %d34 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %47, i32 0, i32 1
  %48 = load float, float* %d34, align 4, !tbaa !46
  %cmp35 = fcmp oge float %48, 0xBEE4F8B580000000
  br i1 %cmp35, label %if.then36, label %if.else

if.then36:                                        ; preds = %lor.lhs.false33, %if.end
  %49 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %49, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %lor.lhs.false33
  %m_status = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 3, i32* %m_status, align 4, !tbaa !40
  br label %if.end37

if.end37:                                         ; preds = %if.else
  br label %if.end40

if.else38:                                        ; preds = %if.then
  %m_status39 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 2, i32* %m_status39, align 4, !tbaa !40
  br label %if.end40

if.end40:                                         ; preds = %if.else38, %if.end37
  %m_hull41 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %50 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull41, %"struct.gjkepa2_impl::EPA::sFace"* %50)
  %m_stock42 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %51 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock42, %"struct.gjkepa2_impl::EPA::sFace"* %51)
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end40, %if.then36
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %v) #9
  %52 = bitcast float* %l to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #9
  %53 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %face to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #9
  br label %return

if.end45:                                         ; preds = %entry
  %m_stock46 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %root47 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_stock46, i32 0, i32 0
  %54 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root47, align 4, !tbaa !61
  %tobool48 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %54, null
  %55 = zext i1 %tobool48 to i64
  %cond = select i1 %tobool48, i32 6, i32 5
  %m_status49 = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 0
  store i32 %cond, i32* %m_status49, align 4, !tbaa !40
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end45, %cleanup
  %56 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %retval, align 4
  ret %"struct.gjkepa2_impl::EPA::sFace"* %56
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA8findbestEv(%"struct.gjkepa2_impl::EPA"* %this) #0 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %minf = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %mind = alloca float, align 4
  %f = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %sqd = alloca float, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %minf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %root = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sList", %"struct.gjkepa2_impl::EPA::sList"* %m_hull, i32 0, i32 0
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %root, align 4, !tbaa !39
  store %"struct.gjkepa2_impl::EPA::sFace"* %1, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %2 = bitcast float* %mind to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %3, i32 0, i32 1
  %4 = load float, float* %d, align 4, !tbaa !46
  %5 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %d2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %5, i32 0, i32 1
  %6 = load float, float* %d2, align 4, !tbaa !46
  %mul = fmul float %4, %6
  store float %mul, float* %mind, align 4, !tbaa !8
  %7 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %f to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #9
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %l = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 4
  %arrayidx = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l, i32 0, i32 1
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %10, null
  br i1 %tobool, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %f to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast float* %sqd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #9
  %13 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  %d3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %13, i32 0, i32 1
  %14 = load float, float* %d3, align 4, !tbaa !46
  %15 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  %d4 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %15, i32 0, i32 1
  %16 = load float, float* %d4, align 4, !tbaa !46
  %mul5 = fmul float %14, %16
  store float %mul5, float* %sqd, align 4, !tbaa !8
  %17 = load float, float* %sqd, align 4, !tbaa !8
  %18 = load float, float* %mind, align 4, !tbaa !8
  %cmp = fcmp olt float %17, %18
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %19 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %19, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %20 = load float, float* %sqd, align 4, !tbaa !8
  store float %20, float* %mind, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %21 = bitcast float* %sqd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #9
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %22 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  %l6 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %22, i32 0, i32 4
  %arrayidx7 = getelementptr inbounds [2 x %"struct.gjkepa2_impl::EPA::sFace"*], [2 x %"struct.gjkepa2_impl::EPA::sFace"*]* %l6, i32 0, i32 1
  %23 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx7, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %23, %"struct.gjkepa2_impl::EPA::sFace"** %f, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %24 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %minf, align 4, !tbaa !2
  %25 = bitcast float* %mind to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #9
  %26 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %minf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #9
  ret %"struct.gjkepa2_impl::EPA::sFace"* %24
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %fa, i32 %ea, %"struct.gjkepa2_impl::EPA::sFace"* %fb, i32 %eb) #3 comdat {
entry:
  %fa.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %ea.addr = alloca i32, align 4
  %fb.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %eb.addr = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA::sFace"* %fa, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4, !tbaa !2
  store i32 %ea, i32* %ea.addr, align 4, !tbaa !10
  store %"struct.gjkepa2_impl::EPA::sFace"* %fb, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4, !tbaa !2
  store i32 %eb, i32* %eb.addr, align 4, !tbaa !10
  %0 = load i32, i32* %eb.addr, align 4, !tbaa !10
  %conv = trunc i32 %0 to i8
  %1 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4, !tbaa !2
  %e = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %1, i32 0, i32 5
  %2 = load i32, i32* %ea.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x i8], [3 x i8]* %e, i32 0, i32 %2
  store i8 %conv, i8* %arrayidx, align 1, !tbaa !21
  %3 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4, !tbaa !2
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4, !tbaa !2
  %f = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 3
  %5 = load i32, i32* %ea.addr, align 4, !tbaa !10
  %arrayidx1 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f, i32 0, i32 %5
  store %"struct.gjkepa2_impl::EPA::sFace"* %3, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx1, align 4, !tbaa !2
  %6 = load i32, i32* %ea.addr, align 4, !tbaa !10
  %conv2 = trunc i32 %6 to i8
  %7 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4, !tbaa !2
  %e3 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %7, i32 0, i32 5
  %8 = load i32, i32* %eb.addr, align 4, !tbaa !10
  %arrayidx4 = getelementptr inbounds [3 x i8], [3 x i8]* %e3, i32 0, i32 %8
  store i8 %conv2, i8* %arrayidx4, align 1, !tbaa !21
  %9 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fa.addr, align 4, !tbaa !2
  %10 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %fb.addr, align 4, !tbaa !2
  %f5 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %10, i32 0, i32 3
  %11 = load i32, i32* %eb.addr, align 4, !tbaa !10
  %arrayidx6 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f5, i32 0, i32 %11
  store %"struct.gjkepa2_impl::EPA::sFace"* %9, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx6, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.gjkepa2_impl::EPA::sHorizon"* @_ZN12gjkepa2_impl3EPA8sHorizonC2Ev(%"struct.gjkepa2_impl::EPA::sHorizon"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.gjkepa2_impl::EPA::sHorizon"*, align 4
  store %"struct.gjkepa2_impl::EPA::sHorizon"* %this, %"struct.gjkepa2_impl::EPA::sHorizon"** %this.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %this.addr, align 4
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4, !tbaa !49
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* null, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4, !tbaa !50
  %nf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %this1, i32 0, i32 2
  store i32 0, i32* %nf, align 4, !tbaa !47
  ret %"struct.gjkepa2_impl::EPA::sHorizon"* %this1
}

define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this, i32 %pass, %"struct.gjkepa2_impl::GJK::sSV"* %w, %"struct.gjkepa2_impl::EPA::sFace"* %f, i32 %e, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %horizon) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %pass.addr = alloca i32, align 4
  %w.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %f.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %e.addr = alloca i32, align 4
  %horizon.addr = alloca %"struct.gjkepa2_impl::EPA::sHorizon"*, align 4
  %e1 = alloca i32, align 4
  %nf = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %e2 = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  store i32 %pass, i32* %pass.addr, align 4, !tbaa !10
  store %"struct.gjkepa2_impl::GJK::sSV"* %w, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %f, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  store i32 %e, i32* %e.addr, align 4, !tbaa !10
  store %"struct.gjkepa2_impl::EPA::sHorizon"* %horizon, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %pass2 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %0, i32 0, i32 6
  %1 = load i8, i8* %pass2, align 1, !tbaa !44
  %conv = zext i8 %1 to i32
  %2 = load i32, i32* %pass.addr, align 4, !tbaa !10
  %cmp = icmp ne i32 %conv, %2
  br i1 %cmp, label %if.then, label %if.end42

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load i32, i32* %e.addr, align 4, !tbaa !10
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i1m3, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4, !tbaa !10
  store i32 %5, i32* %e1, align 4, !tbaa !10
  %6 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %6, i32 0, i32 0
  %7 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4, !tbaa !2
  %w3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %7, i32 0, i32 1
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %w3)
  %8 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %d = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %8, i32 0, i32 1
  %9 = load float, float* %d, align 4, !tbaa !46
  %sub = fsub float %call, %9
  %cmp4 = fcmp olt float %sub, 0xBEE4F8B580000000
  br i1 %cmp4, label %if.then5, label %if.else17

if.then5:                                         ; preds = %if.then
  %10 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %nf to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %c = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %11, i32 0, i32 2
  %12 = load i32, i32* %e1, align 4, !tbaa !10
  %arrayidx6 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c, i32 0, i32 %12
  %13 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx6, align 4, !tbaa !2
  %14 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %c7 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %14, i32 0, i32 2
  %15 = load i32, i32* %e.addr, align 4, !tbaa !10
  %arrayidx8 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::GJK::sSV"*], [3 x %"struct.gjkepa2_impl::GJK::sSV"*]* %c7, i32 0, i32 %15
  %16 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %arrayidx8, align 4, !tbaa !2
  %17 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4, !tbaa !2
  %call9 = call %"struct.gjkepa2_impl::EPA::sFace"* @_ZN12gjkepa2_impl3EPA7newfaceEPNS_3GJK3sSVES3_S3_b(%"struct.gjkepa2_impl::EPA"* %this1, %"struct.gjkepa2_impl::GJK::sSV"* %13, %"struct.gjkepa2_impl::GJK::sSV"* %16, %"struct.gjkepa2_impl::GJK::sSV"* %17, i1 zeroext false)
  store %"struct.gjkepa2_impl::EPA::sFace"* %call9, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  %18 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  %tobool = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %18, null
  br i1 %tobool, label %if.then10, label %if.end16

if.then10:                                        ; preds = %if.then5
  %19 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  %20 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %21 = load i32, i32* %e.addr, align 4, !tbaa !10
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %19, i32 0, %"struct.gjkepa2_impl::EPA::sFace"* %20, i32 %21)
  %22 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %cf = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %22, i32 0, i32 0
  %23 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf, align 4, !tbaa !49
  %tobool11 = icmp ne %"struct.gjkepa2_impl::EPA::sFace"* %23, null
  br i1 %tobool11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then10
  %24 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %cf13 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %24, i32 0, i32 0
  %25 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %cf13, align 4, !tbaa !49
  %26 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA4bindEPNS0_5sFaceEjS2_j(%"struct.gjkepa2_impl::EPA::sFace"* %25, i32 1, %"struct.gjkepa2_impl::EPA::sFace"* %26, i32 2)
  br label %if.end

if.else:                                          ; preds = %if.then10
  %27 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  %28 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %ff = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %28, i32 0, i32 1
  store %"struct.gjkepa2_impl::EPA::sFace"* %27, %"struct.gjkepa2_impl::EPA::sFace"** %ff, align 4, !tbaa !50
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then12
  %29 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %nf, align 4, !tbaa !2
  %30 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %cf14 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %30, i32 0, i32 0
  store %"struct.gjkepa2_impl::EPA::sFace"* %29, %"struct.gjkepa2_impl::EPA::sFace"** %cf14, align 4, !tbaa !49
  %31 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %nf15 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sHorizon", %"struct.gjkepa2_impl::EPA::sHorizon"* %31, i32 0, i32 2
  %32 = load i32, i32* %nf15, align 4, !tbaa !47
  %inc = add i32 %32, 1
  store i32 %inc, i32* %nf15, align 4, !tbaa !47
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end16:                                         ; preds = %if.then5
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end16, %if.end
  %33 = bitcast %"struct.gjkepa2_impl::EPA::sFace"** %nf to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup39 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end38

if.else17:                                        ; preds = %if.then
  %34 = bitcast i32* %e2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load i32, i32* %e.addr, align 4, !tbaa !10
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* @_ZZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonEE4i2m3, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx18, align 4, !tbaa !10
  store i32 %36, i32* %e2, align 4, !tbaa !10
  %37 = load i32, i32* %pass.addr, align 4, !tbaa !10
  %conv19 = trunc i32 %37 to i8
  %38 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %pass20 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %38, i32 0, i32 6
  store i8 %conv19, i8* %pass20, align 1, !tbaa !44
  %39 = load i32, i32* %pass.addr, align 4, !tbaa !10
  %40 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4, !tbaa !2
  %41 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %f21 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %41, i32 0, i32 3
  %42 = load i32, i32* %e1, align 4, !tbaa !10
  %arrayidx22 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f21, i32 0, i32 %42
  %43 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx22, align 4, !tbaa !2
  %44 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %e23 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %44, i32 0, i32 5
  %45 = load i32, i32* %e1, align 4, !tbaa !10
  %arrayidx24 = getelementptr inbounds [3 x i8], [3 x i8]* %e23, i32 0, i32 %45
  %46 = load i8, i8* %arrayidx24, align 1, !tbaa !21
  %conv25 = zext i8 %46 to i32
  %47 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %call26 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %39, %"struct.gjkepa2_impl::GJK::sSV"* %40, %"struct.gjkepa2_impl::EPA::sFace"* %43, i32 %conv25, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %47)
  br i1 %call26, label %land.lhs.true, label %if.end34

land.lhs.true:                                    ; preds = %if.else17
  %48 = load i32, i32* %pass.addr, align 4, !tbaa !10
  %49 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %w.addr, align 4, !tbaa !2
  %50 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %f27 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %50, i32 0, i32 3
  %51 = load i32, i32* %e2, align 4, !tbaa !10
  %arrayidx28 = getelementptr inbounds [3 x %"struct.gjkepa2_impl::EPA::sFace"*], [3 x %"struct.gjkepa2_impl::EPA::sFace"*]* %f27, i32 0, i32 %51
  %52 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %arrayidx28, align 4, !tbaa !2
  %53 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  %e29 = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %53, i32 0, i32 5
  %54 = load i32, i32* %e2, align 4, !tbaa !10
  %arrayidx30 = getelementptr inbounds [3 x i8], [3 x i8]* %e29, i32 0, i32 %54
  %55 = load i8, i8* %arrayidx30, align 1, !tbaa !21
  %conv31 = zext i8 %55 to i32
  %56 = load %"struct.gjkepa2_impl::EPA::sHorizon"*, %"struct.gjkepa2_impl::EPA::sHorizon"** %horizon.addr, align 4, !tbaa !2
  %call32 = call zeroext i1 @_ZN12gjkepa2_impl3EPA6expandEjPNS_3GJK3sSVEPNS0_5sFaceEjRNS0_8sHorizonE(%"struct.gjkepa2_impl::EPA"* %this1, i32 %48, %"struct.gjkepa2_impl::GJK::sSV"* %49, %"struct.gjkepa2_impl::EPA::sFace"* %52, i32 %conv31, %"struct.gjkepa2_impl::EPA::sHorizon"* nonnull align 4 dereferenceable(12) %56)
  br i1 %call32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %land.lhs.true
  %m_hull = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 7
  %57 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6removeERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_hull, %"struct.gjkepa2_impl::EPA::sFace"* %57)
  %m_stock = getelementptr inbounds %"struct.gjkepa2_impl::EPA", %"struct.gjkepa2_impl::EPA"* %this1, i32 0, i32 8
  %58 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %f.addr, align 4, !tbaa !2
  call void @_ZN12gjkepa2_impl3EPA6appendERNS0_5sListEPNS0_5sFaceE(%"struct.gjkepa2_impl::EPA::sList"* nonnull align 4 dereferenceable(8) %m_stock, %"struct.gjkepa2_impl::EPA::sFace"* %58)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup35

if.end34:                                         ; preds = %land.lhs.true, %if.else17
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup35

cleanup35:                                        ; preds = %if.end34, %if.then33
  %59 = bitcast i32* %e2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #9
  %cleanup.dest36 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest36, label %cleanup39 [
    i32 0, label %cleanup.cont37
  ]

cleanup.cont37:                                   ; preds = %cleanup35
  br label %if.end38

if.end38:                                         ; preds = %cleanup.cont37, %cleanup.cont
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup39

cleanup39:                                        ; preds = %if.end38, %cleanup35, %cleanup
  %60 = bitcast i32* %e1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #9
  %cleanup.dest40 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest40, label %unreachable [
    i32 0, label %cleanup.cont41
    i32 1, label %return
  ]

cleanup.cont41:                                   ; preds = %cleanup39
  br label %if.end42

if.end42:                                         ; preds = %cleanup.cont41, %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end42, %cleanup39
  %61 = load i1, i1* %retval, align 1
  ret i1 %61

unreachable:                                      ; preds = %cleanup39
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define linkonce_odr hidden zeroext i1 @_ZN12gjkepa2_impl3EPA11getedgedistEPNS0_5sFaceEPNS_3GJK3sSVES5_Rf(%"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"* %b, float* nonnull align 4 dereferenceable(4) %dist) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %"struct.gjkepa2_impl::EPA"*, align 4
  %face.addr = alloca %"struct.gjkepa2_impl::EPA::sFace"*, align 4
  %a.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %b.addr = alloca %"struct.gjkepa2_impl::GJK::sSV"*, align 4
  %dist.addr = alloca float*, align 4
  %ba = alloca %class.btVector3, align 4
  %n_ab = alloca %class.btVector3, align 4
  %a_dot_nab = alloca float, align 4
  %ba_l2 = alloca float, align 4
  %a_dot_ba = alloca float, align 4
  %b_dot_ba = alloca float, align 4
  %a_dot_b = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %"struct.gjkepa2_impl::EPA"* %this, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::EPA::sFace"* %face, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %a, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  store %"struct.gjkepa2_impl::GJK::sSV"* %b, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  store float* %dist, float** %dist.addr, align 4, !tbaa !2
  %this1 = load %"struct.gjkepa2_impl::EPA"*, %"struct.gjkepa2_impl::EPA"** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ba to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %1, i32 0, i32 1
  %2 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w2 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %2, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ba, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %w2)
  %3 = bitcast %class.btVector3* %n_ab to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %"struct.gjkepa2_impl::EPA::sFace"*, %"struct.gjkepa2_impl::EPA::sFace"** %face.addr, align 4, !tbaa !2
  %n = getelementptr inbounds %"struct.gjkepa2_impl::EPA::sFace", %"struct.gjkepa2_impl::EPA::sFace"* %4, i32 0, i32 0
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %n_ab, %class.btVector3* nonnull align 4 dereferenceable(16) %ba, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  %5 = bitcast float* %a_dot_nab to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w3 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %6, i32 0, i32 1
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w3, %class.btVector3* nonnull align 4 dereferenceable(16) %n_ab)
  store float %call, float* %a_dot_nab, align 4, !tbaa !8
  %7 = load float, float* %a_dot_nab, align 4, !tbaa !8
  %cmp = fcmp olt float %7, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end30

if.then:                                          ; preds = %entry
  %8 = bitcast float* %ba_l2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #9
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ba)
  store float %call4, float* %ba_l2, align 4, !tbaa !8
  %9 = bitcast float* %a_dot_ba to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w5 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %10, i32 0, i32 1
  %call6 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w5, %class.btVector3* nonnull align 4 dereferenceable(16) %ba)
  store float %call6, float* %a_dot_ba, align 4, !tbaa !8
  %11 = bitcast float* %b_dot_ba to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #9
  %12 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w7 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %12, i32 0, i32 1
  %call8 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w7, %class.btVector3* nonnull align 4 dereferenceable(16) %ba)
  store float %call8, float* %b_dot_ba, align 4, !tbaa !8
  %13 = load float, float* %a_dot_ba, align 4, !tbaa !8
  %cmp9 = fcmp ogt float %13, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %if.then
  %14 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w11 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %14, i32 0, i32 1
  %call12 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %w11)
  %15 = load float*, float** %dist.addr, align 4, !tbaa !2
  store float %call12, float* %15, align 4, !tbaa !8
  br label %if.end29

if.else:                                          ; preds = %if.then
  %16 = load float, float* %b_dot_ba, align 4, !tbaa !8
  %cmp13 = fcmp olt float %16, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.else17

if.then14:                                        ; preds = %if.else
  %17 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w15 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %17, i32 0, i32 1
  %call16 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %w15)
  %18 = load float*, float** %dist.addr, align 4, !tbaa !2
  store float %call16, float* %18, align 4, !tbaa !8
  br label %if.end

if.else17:                                        ; preds = %if.else
  %19 = bitcast float* %a_dot_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w18 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %20, i32 0, i32 1
  %21 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w19 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %21, i32 0, i32 1
  %call20 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %w18, %class.btVector3* nonnull align 4 dereferenceable(16) %w19)
  store float %call20, float* %a_dot_b, align 4, !tbaa !8
  %22 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #9
  %23 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %a.addr, align 4, !tbaa !2
  %w21 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %23, i32 0, i32 1
  %call22 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %w21)
  %24 = load %"struct.gjkepa2_impl::GJK::sSV"*, %"struct.gjkepa2_impl::GJK::sSV"** %b.addr, align 4, !tbaa !2
  %w23 = getelementptr inbounds %"struct.gjkepa2_impl::GJK::sSV", %"struct.gjkepa2_impl::GJK::sSV"* %24, i32 0, i32 1
  %call24 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %w23)
  %mul = fmul float %call22, %call24
  %25 = load float, float* %a_dot_b, align 4, !tbaa !8
  %26 = load float, float* %a_dot_b, align 4, !tbaa !8
  %mul25 = fmul float %25, %26
  %sub = fsub float %mul, %mul25
  %27 = load float, float* %ba_l2, align 4, !tbaa !8
  %div = fdiv float %sub, %27
  store float %div, float* %ref.tmp, align 4, !tbaa !8
  %28 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #9
  store float 0.000000e+00, float* %ref.tmp26, align 4, !tbaa !8
  %call27 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %29 = load float, float* %call27, align 4, !tbaa !8
  %call28 = call float @_Z6btSqrtf(float %29)
  %30 = load float*, float** %dist.addr, align 4, !tbaa !2
  store float %call28, float* %30, align 4, !tbaa !8
  %31 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #9
  %32 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #9
  %33 = bitcast float* %a_dot_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #9
  br label %if.end

if.end:                                           ; preds = %if.else17, %if.then14
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then10
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %34 = bitcast float* %b_dot_ba to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #9
  %35 = bitcast float* %a_dot_ba to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #9
  %36 = bitcast float* %ba_l2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #9
  br label %cleanup

if.end30:                                         ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end30, %if.end29
  %37 = bitcast float* %a_dot_nab to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #9
  %38 = bitcast %class.btVector3* %n_ab to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #9
  %39 = bitcast %class.btVector3* %ba to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #9
  %40 = load i1, i1* %retval, align 1
  ret i1 %40
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float %_x, float* %_x.addr, align 4, !tbaa !8
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4, !tbaa !8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !8
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !8
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !8
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !8
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !8
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !8
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !8
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !8
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !8
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load float, float* %d, align 4, !tbaa !8
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !8
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !8
  %8 = load float, float* %s, align 4, !tbaa !8
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !8
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #9
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !8
  %13 = load float, float* %s, align 4, !tbaa !8
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !8
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #9
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !8
  %18 = load float, float* %s, align 4, !tbaa !8
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !8
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #9
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !8
  %23 = load float, float* %xs, align 4, !tbaa !8
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !8
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #9
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !8
  %28 = load float, float* %ys, align 4, !tbaa !8
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !8
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #9
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !8
  %33 = load float, float* %zs, align 4, !tbaa !8
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !8
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #9
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !8
  %38 = load float, float* %xs, align 4, !tbaa !8
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !8
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #9
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !8
  %43 = load float, float* %ys, align 4, !tbaa !8
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !8
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #9
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !8
  %48 = load float, float* %zs, align 4, !tbaa !8
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !8
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #9
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !8
  %53 = load float, float* %ys, align 4, !tbaa !8
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !8
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #9
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !8
  %58 = load float, float* %zs, align 4, !tbaa !8
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !8
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !8
  %63 = load float, float* %zs, align 4, !tbaa !8
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !8
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #9
  %65 = load float, float* %yy, align 4, !tbaa !8
  %66 = load float, float* %zz, align 4, !tbaa !8
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !8
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #9
  %68 = load float, float* %xy, align 4, !tbaa !8
  %69 = load float, float* %wz, align 4, !tbaa !8
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !8
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #9
  %71 = load float, float* %xz, align 4, !tbaa !8
  %72 = load float, float* %wy, align 4, !tbaa !8
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !8
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #9
  %74 = load float, float* %xy, align 4, !tbaa !8
  %75 = load float, float* %wz, align 4, !tbaa !8
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !8
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #9
  %77 = load float, float* %xx, align 4, !tbaa !8
  %78 = load float, float* %zz, align 4, !tbaa !8
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !8
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #9
  %80 = load float, float* %yz, align 4, !tbaa !8
  %81 = load float, float* %wx, align 4, !tbaa !8
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !8
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #9
  %83 = load float, float* %xz, align 4, !tbaa !8
  %84 = load float, float* %wy, align 4, !tbaa !8
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !8
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #9
  %86 = load float, float* %yz, align 4, !tbaa !8
  %87 = load float, float* %wx, align 4, !tbaa !8
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !8
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #9
  %89 = load float, float* %xx, align 4, !tbaa !8
  %90 = load float, float* %yy, align 4, !tbaa !8
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !8
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #9
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #9
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #9
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #9
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #9
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #9
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #9
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #9
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #9
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #9
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #9
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #9
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #9
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #9
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #9
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #9
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #9
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #9
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #9
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #9
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #9
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #9
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #9
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !8
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !8
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !8
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !8
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !8
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !8
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !8
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !8
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"_ZTSN12gjkepa2_impl3GJK7eStatus1_E", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{!13, !3, i64 372}
!13 = !{!"_ZTSN12gjkepa2_impl3GJKE", !14, i64 0, !17, i64 128, !9, i64 144, !4, i64 148, !4, i64 220, !4, i64 348, !11, i64 364, !11, i64 368, !3, i64 372, !7, i64 376}
!14 = !{!"_ZTSN12gjkepa2_impl13MinkowskiDiffE", !4, i64 0, !15, i64 8, !16, i64 56, !4, i64 120}
!15 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!16 = !{!"_ZTS11btTransform", !15, i64 0, !17, i64 48}
!17 = !{!"_ZTS9btVector3", !4, i64 0}
!18 = !{!19, !11, i64 32}
!19 = !{!"_ZTSN12gjkepa2_impl3GJK8sSimplexE", !4, i64 0, !4, i64 16, !11, i64 32}
!20 = !{i64 0, i64 16, !21}
!21 = !{!4, !4, i64 0}
!22 = !{!23, !9, i64 52}
!23 = !{!"_ZTSN15btGjkEpaSolver28sResultsE", !24, i64 0, !4, i64 4, !17, i64 36, !9, i64 52}
!24 = !{!"_ZTSN15btGjkEpaSolver28sResults7eStatusE", !4, i64 0}
!25 = !{!23, !24, i64 0}
!26 = !{!27, !27, i64 0}
!27 = !{!"bool", !4, i64 0}
!28 = !{i8 0, i8 2}
!29 = !{!13, !11, i64 364}
!30 = !{!13, !11, i64 368}
!31 = !{!13, !7, i64 376}
!32 = !{!13, !9, i64 144}
!33 = !{!34, !34, i64 0}
!34 = !{!"_ZTSN12gjkepa2_impl3EPA7eStatus1_E", !4, i64 0}
!35 = !{!36, !11, i64 36}
!36 = !{!"_ZTSN12gjkepa2_impl3EPAE", !34, i64 0, !19, i64 4, !17, i64 40, !9, i64 56, !4, i64 60, !4, i64 2108, !11, i64 9276, !37, i64 9280, !37, i64 9288}
!37 = !{!"_ZTSN12gjkepa2_impl3EPA5sListE", !3, i64 0, !11, i64 4}
!38 = !{!36, !9, i64 56}
!39 = !{!36, !3, i64 9280}
!40 = !{!36, !34, i64 0}
!41 = !{!36, !11, i64 9276}
!42 = !{!36, !11, i64 9284}
!43 = !{i64 0, i64 16, !21, i64 16, i64 4, !8, i64 20, i64 12, !21, i64 32, i64 12, !21, i64 44, i64 8, !21, i64 52, i64 3, !21, i64 55, i64 1, !21}
!44 = !{!45, !4, i64 55}
!45 = !{!"_ZTSN12gjkepa2_impl3EPA5sFaceE", !17, i64 0, !9, i64 16, !4, i64 20, !4, i64 32, !4, i64 44, !4, i64 52, !4, i64 55}
!46 = !{!45, !9, i64 16}
!47 = !{!48, !11, i64 8}
!48 = !{!"_ZTSN12gjkepa2_impl3EPA8sHorizonE", !3, i64 0, !3, i64 4, !11, i64 8}
!49 = !{!48, !3, i64 0}
!50 = !{!48, !3, i64 4}
!51 = !{!52, !52, i64 0}
!52 = !{!"vtable pointer", !5, i64 0}
!53 = !{!54, !11, i64 4}
!54 = !{!"_ZTS16btCollisionShape", !11, i64 4, !3, i64 8}
!55 = !{!56, !9, i64 44}
!56 = !{!"_ZTS21btConvexInternalShape", !17, i64 12, !17, i64 28, !9, i64 44, !9, i64 48}
!57 = !{!14, !4, i64 120}
!58 = !{}
!59 = !{!37, !3, i64 0}
!60 = !{!37, !11, i64 4}
!61 = !{!36, !3, i64 9288}
