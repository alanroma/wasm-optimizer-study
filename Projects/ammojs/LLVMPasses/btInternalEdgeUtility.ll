; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btInternalEdgeUtility.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionDispatch/btInternalEdgeUtility.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVector3 = type { [4 x float] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%struct.btConnectivityProcessor = type { %class.btTriangleCallback, i32, i32, %class.btVector3*, %struct.btTriangleInfoMap* }
%class.btTriangleCallback = type { i32 (...)** }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%union.anon = type { i8* }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%class.btScaledBvhTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btBvhTriangleMeshShape* }
%class.btSerializer = type { i32 (...)** }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv = comdat any

$_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap = comdat any

$_ZN19btTriangleMeshShape16getMeshInterfaceEv = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN23btConnectivityProcessorC2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector39fuzzyZeroEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_ = comdat any

$_ZN9btHashIntC2Ei = comdat any

$_ZNK15btTriangleShape10calcNormalER9btVector3 = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK11btTransform8invXformERK9btVector3 = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN23btConnectivityProcessorD0Ev = comdat any

$_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN14btTriangleInfoC2Ev = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_ = comdat any

$_ZN15btTriangleShapeC2ERK9btVector3S2_S2_ = comdat any

$_ZNK9btHashInt7getHashEv = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv = comdat any

$_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_ = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK9btHashInt6equalsERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntEixEi = comdat any

$_ZNK9btHashInt7getUid1Ev = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN15btTriangleShapeD0Ev = comdat any

$_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK15btTriangleShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i = comdat any

$_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btTriangleShape14getNumVerticesEv = comdat any

$_ZNK15btTriangleShape11getNumEdgesEv = comdat any

$_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ = comdat any

$_ZNK15btTriangleShape9getVertexEiR9btVector3 = comdat any

$_ZNK15btTriangleShape12getNumPlanesEv = comdat any

$_ZNK15btTriangleShape8getPlaneER9btVector3S1_i = comdat any

$_ZNK15btTriangleShape8isInsideERK9btVector3f = comdat any

$_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ = comdat any

$_ZN15btTriangleShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z7btAtan2ff = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z5btCosf = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZTV23btConnectivityProcessor = comdat any

$_ZTS23btConnectivityProcessor = comdat any

$_ZTI23btConnectivityProcessor = comdat any

$_ZTV15btTriangleShape = comdat any

$_ZTS15btTriangleShape = comdat any

$_ZTI15btTriangleShape = comdat any

@_ZTV23btConnectivityProcessor = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btConnectivityProcessor to i8*), i8* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to i8*), i8* bitcast (void (%struct.btConnectivityProcessor*)* @_ZN23btConnectivityProcessorD0Ev to i8*), i8* bitcast (void (%struct.btConnectivityProcessor*, %class.btVector3*, i32, i32)* @_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btConnectivityProcessor = linkonce_odr hidden constant [26 x i8] c"23btConnectivityProcessor\00", comdat, align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI23btConnectivityProcessor = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btConnectivityProcessor, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, comdat, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsA = private unnamed_addr constant [3 x i32] [i32 -1, i32 -1, i32 -1], align 4
@__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsB = private unnamed_addr constant [3 x i32] [i32 -1, i32 -1, i32 -1], align 4
@_ZTV15btTriangleShape = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShape*)* @_ZN15btTriangleShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@_ZTS15btTriangleShape = linkonce_odr hidden constant [18 x i8] c"15btTriangleShape\00", comdat, align 1
@_ZTI23btPolyhedralConvexShape = external constant i8*
@_ZTI15btTriangleShape = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btTriangleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btPolyhedralConvexShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [9 x i8] c"Triangle\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1

define hidden void @_Z26btGenerateInternalEdgeInfoP22btBvhTriangleMeshShapeP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %trimeshShape, %struct.btTriangleInfoMap* %triangleInfoMap) #0 {
entry:
  %trimeshShape.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMap.addr = alloca %struct.btTriangleInfoMap*, align 4
  %meshInterface = alloca %class.btStridingMeshInterface*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %partId = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %triangleVerts = alloca [3 x %class.btVector3], align 16
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %triangleIndex = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %graphicsbase37 = alloca double*, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp62 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %connectivityProcessor = alloca %struct.btConnectivityProcessor, align 4
  store %class.btBvhTriangleMeshShape* %trimeshShape, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4, !tbaa !2
  store %struct.btTriangleInfoMap* %triangleInfoMap, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4, !tbaa !2
  %0 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4, !tbaa !2
  %call = call %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %0)
  %tobool = icmp ne %struct.btTriangleInfoMap* %call, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4, !tbaa !2
  %2 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4, !tbaa !2
  call void @_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %1, %struct.btTriangleInfoMap* %2)
  %3 = bitcast %class.btStridingMeshInterface** %meshInterface to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4, !tbaa !2
  %5 = bitcast %class.btBvhTriangleMeshShape* %4 to %class.btTriangleMeshShape*
  %call1 = call %class.btStridingMeshInterface* @_ZN19btTriangleMeshShape16getMeshInterfaceEv(%class.btTriangleMeshShape* %5)
  store %class.btStridingMeshInterface* %call1, %class.btStridingMeshInterface** %meshInterface, align 4, !tbaa !2
  %6 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %7)
  store %class.btVector3* %call2, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %8 = bitcast i32* %partId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store i32 0, i32* %partId, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc81, %if.end
  %9 = load i32, i32* %partId, align 4, !tbaa !6
  %10 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4, !tbaa !2
  %11 = bitcast %class.btStridingMeshInterface* %10 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %11, align 4, !tbaa !8
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %12 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call3 = call i32 %12(%class.btStridingMeshInterface* %10)
  %cmp = icmp slt i32 %9, %call3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %partId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  br label %for.end83

for.body:                                         ; preds = %for.cond
  %14 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  store i8* null, i8** %vertexbase, align 4, !tbaa !2
  %15 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  store i32 0, i32* %numverts, align 4, !tbaa !6
  %16 = bitcast i32* %type to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  store i32 2, i32* %type, align 4, !tbaa !10
  %17 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  store i32 0, i32* %stride, align 4, !tbaa !6
  %18 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  store i8* null, i8** %indexbase, align 4, !tbaa !2
  %19 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  store i32 0, i32* %indexstride, align 4, !tbaa !6
  %20 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  store i32 0, i32* %numfaces, align 4, !tbaa !6
  %21 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  store i32 2, i32* %indicestype, align 4, !tbaa !10
  %22 = bitcast [3 x %class.btVector3]* %triangleVerts to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %22) #10
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body ], [ %arrayctor.next, %arrayctor.loop ]
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %23 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4, !tbaa !2
  %24 = load i32, i32* %partId, align 4, !tbaa !6
  %25 = bitcast %class.btStridingMeshInterface* %23 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable5 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %25, align 4, !tbaa !8
  %vfn6 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable5, i64 4
  %26 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn6, align 4
  call void %26(%class.btStridingMeshInterface* %23, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %24)
  %27 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #10
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %28 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #10
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %29 = bitcast i32* %triangleIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #10
  store i32 0, i32* %triangleIndex, align 4, !tbaa !6
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc79, %arrayctor.cont
  %30 = load i32, i32* %triangleIndex, align 4, !tbaa !6
  %31 = load i32, i32* %numfaces, align 4, !tbaa !6
  %cmp10 = icmp slt i32 %30, %31
  br i1 %cmp10, label %for.body12, label %for.cond.cleanup11

for.cond.cleanup11:                               ; preds = %for.cond9
  store i32 5, i32* %cleanup.dest.slot, align 4
  %32 = bitcast i32* %triangleIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #10
  br label %for.end80

for.body12:                                       ; preds = %for.cond9
  %33 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load i8*, i8** %indexbase, align 4, !tbaa !2
  %35 = load i32, i32* %triangleIndex, align 4, !tbaa !6
  %36 = load i32, i32* %indexstride, align 4, !tbaa !6
  %mul = mul nsw i32 %35, %36
  %add.ptr = getelementptr inbounds i8, i8* %34, i32 %mul
  %37 = bitcast i8* %add.ptr to i32*
  store i32* %37, i32** %gfxbase, align 4, !tbaa !2
  %38 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #10
  store i32 2, i32* %j, align 4, !tbaa !6
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc, %for.body12
  %39 = load i32, i32* %j, align 4, !tbaa !6
  %cmp14 = icmp sge i32 %39, 0
  br i1 %cmp14, label %for.body16, label %for.cond.cleanup15

for.cond.cleanup15:                               ; preds = %for.cond13
  store i32 8, i32* %cleanup.dest.slot, align 4
  %40 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #10
  br label %for.end

for.body16:                                       ; preds = %for.cond13
  %41 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #10
  %42 = load i32, i32* %indicestype, align 4, !tbaa !10
  %cmp17 = icmp eq i32 %42, 3
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body16
  %43 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %44 = bitcast i32* %43 to i16*
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i16, i16* %44, i32 %45
  %46 = load i16, i16* %arrayidx, align 2, !tbaa !12
  %conv = zext i16 %46 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body16
  %47 = load i32*, i32** %gfxbase, align 4, !tbaa !2
  %48 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx18 = getelementptr inbounds i32, i32* %47, i32 %48
  %49 = load i32, i32* %arrayidx18, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %49, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4, !tbaa !6
  %50 = load i32, i32* %type, align 4, !tbaa !10
  %cmp19 = icmp eq i32 %50, 0
  br i1 %cmp19, label %if.then20, label %if.else

if.then20:                                        ; preds = %cond.end
  %51 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #10
  %52 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %53 = load i32, i32* %graphicsindex, align 4, !tbaa !6
  %54 = load i32, i32* %stride, align 4, !tbaa !6
  %mul21 = mul nsw i32 %53, %54
  %add.ptr22 = getelementptr inbounds i8, i8* %52, i32 %mul21
  %55 = bitcast i8* %add.ptr22 to float*
  store float* %55, float** %graphicsbase, align 4, !tbaa !2
  %56 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #10
  %57 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #10
  %58 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds float, float* %58, i32 0
  %59 = load float, float* %arrayidx24, align 4, !tbaa !14
  %60 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %60)
  %61 = load float, float* %call25, align 4, !tbaa !14
  %mul26 = fmul float %59, %61
  store float %mul26, float* %ref.tmp23, align 4, !tbaa !14
  %62 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #10
  %63 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds float, float* %63, i32 1
  %64 = load float, float* %arrayidx28, align 4, !tbaa !14
  %65 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %65)
  %66 = load float, float* %call29, align 4, !tbaa !14
  %mul30 = fmul float %64, %66
  store float %mul30, float* %ref.tmp27, align 4, !tbaa !14
  %67 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #10
  %68 = load float*, float** %graphicsbase, align 4, !tbaa !2
  %arrayidx32 = getelementptr inbounds float, float* %68, i32 2
  %69 = load float, float* %arrayidx32, align 4, !tbaa !14
  %70 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %70)
  %71 = load float, float* %call33, align 4, !tbaa !14
  %mul34 = fmul float %69, %71
  store float %mul34, float* %ref.tmp31, align 4, !tbaa !14
  %call35 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %72 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx36 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %72
  %73 = bitcast %class.btVector3* %arrayidx36 to i8*
  %74 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %73, i8* align 4 %74, i32 16, i1 false), !tbaa.struct !16
  %75 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #10
  %76 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #10
  %77 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  %78 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #10
  %79 = bitcast float** %graphicsbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  br label %if.end61

if.else:                                          ; preds = %cond.end
  %80 = bitcast double** %graphicsbase37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #10
  %81 = load i8*, i8** %vertexbase, align 4, !tbaa !2
  %82 = load i32, i32* %graphicsindex, align 4, !tbaa !6
  %83 = load i32, i32* %stride, align 4, !tbaa !6
  %mul38 = mul nsw i32 %82, %83
  %add.ptr39 = getelementptr inbounds i8, i8* %81, i32 %mul38
  %84 = bitcast i8* %add.ptr39 to double*
  store double* %84, double** %graphicsbase37, align 4, !tbaa !2
  %85 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %85) #10
  %86 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #10
  %87 = load double*, double** %graphicsbase37, align 4, !tbaa !2
  %arrayidx42 = getelementptr inbounds double, double* %87, i32 0
  %88 = load double, double* %arrayidx42, align 8, !tbaa !18
  %89 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %89)
  %90 = load float, float* %call43, align 4, !tbaa !14
  %conv44 = fpext float %90 to double
  %mul45 = fmul double %88, %conv44
  %conv46 = fptrunc double %mul45 to float
  store float %conv46, float* %ref.tmp41, align 4, !tbaa !14
  %91 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #10
  %92 = load double*, double** %graphicsbase37, align 4, !tbaa !2
  %arrayidx48 = getelementptr inbounds double, double* %92, i32 1
  %93 = load double, double* %arrayidx48, align 8, !tbaa !18
  %94 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %94)
  %95 = load float, float* %call49, align 4, !tbaa !14
  %conv50 = fpext float %95 to double
  %mul51 = fmul double %93, %conv50
  %conv52 = fptrunc double %mul51 to float
  store float %conv52, float* %ref.tmp47, align 4, !tbaa !14
  %96 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #10
  %97 = load double*, double** %graphicsbase37, align 4, !tbaa !2
  %arrayidx54 = getelementptr inbounds double, double* %97, i32 2
  %98 = load double, double* %arrayidx54, align 8, !tbaa !18
  %99 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4, !tbaa !2
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %99)
  %100 = load float, float* %call55, align 4, !tbaa !14
  %conv56 = fpext float %100 to double
  %mul57 = fmul double %98, %conv56
  %conv58 = fptrunc double %mul57 to float
  store float %conv58, float* %ref.tmp53, align 4, !tbaa !14
  %call59 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %101 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx60 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %101
  %102 = bitcast %class.btVector3* %arrayidx60 to i8*
  %103 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %102, i8* align 4 %103, i32 16, i1 false), !tbaa.struct !16
  %104 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #10
  %105 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #10
  %106 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #10
  %107 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %107) #10
  %108 = bitcast double** %graphicsbase37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #10
  br label %if.end61

if.end61:                                         ; preds = %if.else, %if.then20
  %109 = bitcast i32* %graphicsindex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end61
  %110 = load i32, i32* %j, align 4, !tbaa !6
  %dec = add nsw i32 %110, -1
  store i32 %dec, i32* %j, align 4, !tbaa !6
  br label %for.cond13

for.end:                                          ; preds = %for.cond.cleanup15
  %111 = bitcast float* %ref.tmp62 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #10
  store float 0x43ABC16D60000000, float* %ref.tmp62, align 4, !tbaa !14
  %112 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #10
  store float 0x43ABC16D60000000, float* %ref.tmp63, align 4, !tbaa !14
  %113 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #10
  store float 0x43ABC16D60000000, float* %ref.tmp64, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64)
  %114 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #10
  %115 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #10
  %116 = bitcast float* %ref.tmp62 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %116) #10
  %117 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #10
  store float 0xC3ABC16D60000000, float* %ref.tmp65, align 4, !tbaa !14
  %118 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #10
  store float 0xC3ABC16D60000000, float* %ref.tmp66, align 4, !tbaa !14
  %119 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #10
  store float 0xC3ABC16D60000000, float* %ref.tmp67, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67)
  %120 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %120) #10
  %121 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #10
  %122 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #10
  %arrayidx68 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68)
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx69)
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx70)
  %arrayidx71 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71)
  %arrayidx72 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72)
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73)
  %123 = bitcast %struct.btConnectivityProcessor* %connectivityProcessor to i8*
  call void @llvm.lifetime.start.p0i8(i64 20, i8* %123) #10
  %call74 = call %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorC2Ev(%struct.btConnectivityProcessor* %connectivityProcessor) #10
  %124 = load i32, i32* %partId, align 4, !tbaa !6
  %m_partIdA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 1
  store i32 %124, i32* %m_partIdA, align 4, !tbaa !20
  %125 = load i32, i32* %triangleIndex, align 4, !tbaa !6
  %m_triangleIndexA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 2
  store i32 %125, i32* %m_triangleIndexA, align 4, !tbaa !22
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %m_triangleVerticesA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 3
  store %class.btVector3* %arrayidx75, %class.btVector3** %m_triangleVerticesA, align 4, !tbaa !23
  %126 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4, !tbaa !2
  %m_triangleInfoMap = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 4
  store %struct.btTriangleInfoMap* %126, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !24
  %127 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4, !tbaa !2
  %128 = bitcast %struct.btConnectivityProcessor* %connectivityProcessor to %class.btTriangleCallback*
  %129 = bitcast %class.btBvhTriangleMeshShape* %127 to void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable76 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %129, align 4, !tbaa !8
  %vfn77 = getelementptr inbounds void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable76, i64 16
  %130 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn77, align 4
  call void %130(%class.btBvhTriangleMeshShape* %127, %class.btTriangleCallback* %128, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %call78 = call %struct.btConnectivityProcessor* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %struct.btConnectivityProcessor* (%struct.btConnectivityProcessor*)*)(%struct.btConnectivityProcessor* %connectivityProcessor) #10
  %131 = bitcast %struct.btConnectivityProcessor* %connectivityProcessor to i8*
  call void @llvm.lifetime.end.p0i8(i64 20, i8* %131) #10
  %132 = bitcast i32** %gfxbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #10
  br label %for.inc79

for.inc79:                                        ; preds = %for.end
  %133 = load i32, i32* %triangleIndex, align 4, !tbaa !6
  %inc = add nsw i32 %133, 1
  store i32 %inc, i32* %triangleIndex, align 4, !tbaa !6
  br label %for.cond9

for.end80:                                        ; preds = %for.cond.cleanup11
  %134 = bitcast %class.btVector3* %aabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %134) #10
  %135 = bitcast %class.btVector3* %aabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %135) #10
  %136 = bitcast [3 x %class.btVector3]* %triangleVerts to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %136) #10
  %137 = bitcast i32* %indicestype to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #10
  %138 = bitcast i32* %numfaces to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #10
  %139 = bitcast i32* %indexstride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #10
  %140 = bitcast i8** %indexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #10
  %141 = bitcast i32* %stride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #10
  %142 = bitcast i32* %type to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #10
  %143 = bitcast i32* %numverts to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #10
  %144 = bitcast i8** %vertexbase to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #10
  br label %for.inc81

for.inc81:                                        ; preds = %for.end80
  %145 = load i32, i32* %partId, align 4, !tbaa !6
  %inc82 = add nsw i32 %145, 1
  store i32 %inc82, i32* %partId, align 4, !tbaa !6
  br label %for.cond

for.end83:                                        ; preds = %for.cond.cleanup
  %146 = bitcast %class.btVector3** %meshScaling to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #10
  %147 = bitcast %class.btStridingMeshInterface** %meshInterface to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #10
  br label %return

return:                                           ; preds = %for.end83, %if.then
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !25
  ret %struct.btTriangleInfoMap* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %this, %struct.btTriangleInfoMap* %triangleInfoMap) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMap.addr = alloca %struct.btTriangleInfoMap*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  store %struct.btTriangleInfoMap* %triangleInfoMap, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4, !tbaa !2
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  store %struct.btTriangleInfoMap* %0, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !25
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: nounwind
define linkonce_odr hidden %class.btStridingMeshInterface* @_ZN19btTriangleMeshShape16getMeshInterfaceEv(%class.btTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 3
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4, !tbaa !28
  ret %class.btStridingMeshInterface* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorC2Ev(%struct.btConnectivityProcessor* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %0 = bitcast %struct.btConnectivityProcessor* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #10
  %1 = bitcast %struct.btConnectivityProcessor* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btConnectivityProcessor, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  ret %struct.btConnectivityProcessor* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #5

define hidden void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %point, %class.btVector3* nonnull align 4 dereferenceable(16) %line0, %class.btVector3* nonnull align 4 dereferenceable(16) %line1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearestPoint) #0 {
entry:
  %point.addr = alloca %class.btVector3*, align 4
  %line0.addr = alloca %class.btVector3*, align 4
  %line1.addr = alloca %class.btVector3*, align 4
  %nearestPoint.addr = alloca %class.btVector3*, align 4
  %lineDelta = alloca %class.btVector3, align 4
  %delta = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store %class.btVector3* %line0, %class.btVector3** %line0.addr, align 4, !tbaa !2
  store %class.btVector3* %line1, %class.btVector3** %line1.addr, align 4, !tbaa !2
  store %class.btVector3* %nearestPoint, %class.btVector3** %nearestPoint.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %lineDelta to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %line1.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %lineDelta, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %call = call zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %lineDelta)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %nearestPoint.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !16
  br label %if.end10

if.else:                                          ; preds = %entry
  %7 = bitcast float* %delta to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #10
  %9 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta)
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %lineDelta, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta)
  %div = fdiv float %call1, %call2
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #10
  store float %div, float* %delta, align 4, !tbaa !14
  %12 = load float, float* %delta, align 4, !tbaa !14
  %cmp = fcmp olt float %12, 0.000000e+00
  br i1 %cmp, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  store float 0.000000e+00, float* %delta, align 4, !tbaa !14
  br label %if.end7

if.else4:                                         ; preds = %if.else
  %13 = load float, float* %delta, align 4, !tbaa !14
  %cmp5 = fcmp ogt float %13, 1.000000e+00
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else4
  store float 1.000000e+00, float* %delta, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else4
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then3
  %14 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #10
  %15 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4, !tbaa !2
  %16 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta, float* nonnull align 4 dereferenceable(4) %delta)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %17 = load %class.btVector3*, %class.btVector3** %nearestPoint.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !16
  %20 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #10
  %21 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %21) #10
  %22 = bitcast float* %delta to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  br label %if.end10

if.end10:                                         ; preds = %if.end7, %if.then
  %23 = bitcast %class.btVector3* %lineDelta to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !14
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !14
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !14
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !14
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !14
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !14
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

define hidden zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal_org, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB, float %correctedEdgeAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal) #0 {
entry:
  %retval = alloca i1, align 1
  %edge.addr = alloca %class.btVector3*, align 4
  %tri_normal_org.addr = alloca %class.btVector3*, align 4
  %localContactNormalOnB.addr = alloca %class.btVector3*, align 4
  %correctedEdgeAngle.addr = alloca float, align 4
  %clampedLocalNormal.addr = alloca %class.btVector3*, align 4
  %tri_normal = alloca %class.btVector3, align 4
  %edgeCross = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %curAngle = alloca float, align 4
  %diffAngle = alloca float, align 4
  %rotation = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btMatrix3x3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %diffAngle13 = alloca float, align 4
  %rotation15 = alloca %class.btQuaternion, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btMatrix3x3, align 4
  store %class.btVector3* %edge, %class.btVector3** %edge.addr, align 4, !tbaa !2
  store %class.btVector3* %tri_normal_org, %class.btVector3** %tri_normal_org.addr, align 4, !tbaa !2
  store %class.btVector3* %localContactNormalOnB, %class.btVector3** %localContactNormalOnB.addr, align 4, !tbaa !2
  store float %correctedEdgeAngle, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  store %class.btVector3* %clampedLocalNormal, %class.btVector3** %clampedLocalNormal.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %tri_normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %tri_normal_org.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %tri_normal to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !16
  %4 = bitcast %class.btVector3* %edgeCross to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #10
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4, !tbaa !2
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp)
  %7 = bitcast %class.btVector3* %edgeCross to i8*
  %8 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !16
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #10
  %10 = bitcast float* %curAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4, !tbaa !2
  %call1 = call float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %edgeCross, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  store float %call1, float* %curAngle, align 4, !tbaa !14
  %12 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %cmp = fcmp olt float %12, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %13 = load float, float* %curAngle, align 4, !tbaa !14
  %14 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %cmp2 = fcmp olt float %13, %14
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %15 = bitcast float* %diffAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %16 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %17 = load float, float* %curAngle, align 4, !tbaa !14
  %sub = fsub float %16, %17
  store float %sub, float* %diffAngle, align 4, !tbaa !14
  %18 = bitcast %class.btQuaternion* %rotation to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #10
  %19 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4, !tbaa !2
  %call4 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %19, float* nonnull align 4 dereferenceable(4) %diffAngle)
  %20 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #10
  %21 = bitcast %class.btMatrix3x3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %21) #10
  %call7 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp6, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation)
  %22 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %22)
  %23 = load %class.btVector3*, %class.btVector3** %clampedLocalNormal.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !16
  %26 = bitcast %class.btMatrix3x3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %26) #10
  %27 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #10
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %28 = bitcast %class.btQuaternion* %rotation to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #10
  %29 = bitcast float* %diffAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #10
  br label %cleanup

if.end:                                           ; preds = %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  %30 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %cmp9 = fcmp oge float %30, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end21

if.then10:                                        ; preds = %if.end8
  %31 = load float, float* %curAngle, align 4, !tbaa !14
  %32 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %cmp11 = fcmp ogt float %31, %32
  br i1 %cmp11, label %if.then12, label %if.end20

if.then12:                                        ; preds = %if.then10
  %33 = bitcast float* %diffAngle13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load float, float* %correctedEdgeAngle.addr, align 4, !tbaa !14
  %35 = load float, float* %curAngle, align 4, !tbaa !14
  %sub14 = fsub float %34, %35
  store float %sub14, float* %diffAngle13, align 4, !tbaa !14
  %36 = bitcast %class.btQuaternion* %rotation15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #10
  %37 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4, !tbaa !2
  %call16 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotation15, %class.btVector3* nonnull align 4 dereferenceable(16) %37, float* nonnull align 4 dereferenceable(4) %diffAngle13)
  %38 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #10
  %39 = bitcast %class.btMatrix3x3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %39) #10
  %call19 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp18, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation15)
  %40 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4, !tbaa !2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %40)
  %41 = load %class.btVector3*, %class.btVector3** %clampedLocalNormal.addr, align 4, !tbaa !2
  %42 = bitcast %class.btVector3* %41 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false), !tbaa.struct !16
  %44 = bitcast %class.btMatrix3x3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %44) #10
  %45 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #10
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %46 = bitcast %class.btQuaternion* %rotation15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #10
  %47 = bitcast float* %diffAngle13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #10
  br label %cleanup

if.end20:                                         ; preds = %if.then10
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.end8
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end21, %if.then12, %if.then3
  %48 = bitcast float* %curAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #10
  %49 = bitcast %class.btVector3* %edgeCross to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #10
  %50 = bitcast %class.btVector3* %tri_normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %50) #10
  %51 = load i1, i1* %retval, align 1
  ret i1 %51
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !14
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !14
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !14
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !14
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !14
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !14
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !14
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !14
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !14
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !14
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !14
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !14
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #10
  ret %class.btVector3* %call2
}

define internal float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %edgeA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB) #0 {
entry:
  %edgeA.addr = alloca %class.btVector3*, align 4
  %normalA.addr = alloca %class.btVector3*, align 4
  %normalB.addr = alloca %class.btVector3*, align 4
  %refAxis0 = alloca %class.btVector3, align 4
  %refAxis1 = alloca %class.btVector3, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  store %class.btVector3* %edgeA, %class.btVector3** %edgeA.addr, align 4, !tbaa !2
  store %class.btVector3* %normalA, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  store %class.btVector3* %normalB, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %refAxis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %edgeA.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %refAxis0 to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !16
  %4 = bitcast %class.btVector3* %refAxis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #10
  %5 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %refAxis1 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !16
  %8 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #10
  %9 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4, !tbaa !2
  %10 = bitcast %class.btVector3* %swingAxis to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !16
  %12 = bitcast float* %angle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis0)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis1)
  %call2 = call float @_Z7btAtan2ff(float %call, float %call1)
  store float %call2, float* %angle, align 4, !tbaa !14
  %13 = load float, float* %angle, align 4, !tbaa !14
  %14 = bitcast float* %angle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  %15 = bitcast %class.btVector3* %swingAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #10
  %16 = bitcast %class.btVector3* %refAxis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #10
  %17 = bitcast %class.btVector3* %refAxis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #10
  ret float %13
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4, !tbaa !2
  %2 = load float*, float** %_angle.addr, align 4, !tbaa !2
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !14
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !14
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  ret void
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

define hidden void @_Z28btAdjustInternalEdgeContactsR15btManifoldPointPK24btCollisionObjectWrapperS3_iii(%class.btManifoldPoint* nonnull align 4 dereferenceable(184) %cp, %struct.btCollisionObjectWrapper* %colObj0Wrap, %struct.btCollisionObjectWrapper* %colObj1Wrap, i32 %partId0, i32 %index0, i32 %normalAdjustFlags) #0 {
entry:
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %colObj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %colObj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  %normalAdjustFlags.addr = alloca i32, align 4
  %trimesh = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMapPtr = alloca %struct.btTriangleInfoMap*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %hash = alloca i32, align 4
  %info = alloca %struct.btTriangleInfo*, align 4
  %ref.tmp = alloca %class.btHashInt, align 4
  %frontFacing = alloca float, align 4
  %tri_shape = alloca %class.btTriangleShape*, align 4
  %v0 = alloca %class.btVector3, align 4
  %v1 = alloca %class.btVector3, align 4
  %v2 = alloca %class.btVector3, align 4
  %red = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %green = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %blue = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %white = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %black = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %tri_normal = alloca %class.btVector3, align 4
  %nearest = alloca %class.btVector3, align 4
  %contact = alloca %class.btVector3, align 4
  %isNearEdge = alloca i8, align 1
  %numConcaveEdgeHits = alloca i32, align 4
  %numConvexEdgeHits = alloca i32, align 4
  %localContactNormalOnB = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btMatrix3x3, align 4
  %bestedge = alloca i32, align 4
  %disttobestedge = alloca float, align 4
  %nearest61 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp64 = alloca %class.btVector3, align 4
  %nearest74 = alloca %class.btVector3, align 4
  %len77 = alloca float, align 4
  %ref.tmp78 = alloca %class.btVector3, align 4
  %nearest88 = alloca %class.btVector3, align 4
  %len91 = alloca float, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %len103 = alloca float, align 4
  %ref.tmp104 = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %isEdgeConvex = alloca i8, align 1
  %swapFactor = alloca float, align 4
  %nA = alloca %class.btVector3, align 4
  %orn = alloca %class.btQuaternion, align 4
  %computedNormalB = alloca %class.btVector3, align 4
  %ref.tmp124 = alloca float, align 4
  %nB = alloca %class.btVector3, align 4
  %NdotA = alloca float, align 4
  %NdotB = alloca float, align 4
  %backFacingNormal = alloca i8, align 1
  %clampedLocalNormal = alloca %class.btVector3, align 4
  %isClamped = alloca i8, align 1
  %ref.tmp139 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca %class.btVector3, align 4
  %newNormal = alloca %class.btVector3, align 4
  %ref.tmp154 = alloca %class.btVector3, align 4
  %ref.tmp155 = alloca %class.btVector3, align 4
  %ref.tmp157 = alloca %class.btVector3, align 4
  %len173 = alloca float, align 4
  %ref.tmp174 = alloca %class.btVector3, align 4
  %edge181 = alloca %class.btVector3, align 4
  %isEdgeConvex187 = alloca i8, align 1
  %swapFactor192 = alloca float, align 4
  %nA195 = alloca %class.btVector3, align 4
  %orn196 = alloca %class.btQuaternion, align 4
  %computedNormalB199 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca float, align 4
  %nB207 = alloca %class.btVector3, align 4
  %NdotA208 = alloca float, align 4
  %NdotB210 = alloca float, align 4
  %backFacingNormal212 = alloca i8, align 1
  %localContactNormalOnB225 = alloca %class.btVector3, align 4
  %ref.tmp226 = alloca %class.btMatrix3x3, align 4
  %clampedLocalNormal230 = alloca %class.btVector3, align 4
  %isClamped232 = alloca i8, align 1
  %ref.tmp233 = alloca %class.btVector3, align 4
  %ref.tmp242 = alloca %class.btVector3, align 4
  %newNormal247 = alloca %class.btVector3, align 4
  %ref.tmp251 = alloca %class.btVector3, align 4
  %ref.tmp253 = alloca %class.btVector3, align 4
  %ref.tmp257 = alloca %class.btVector3, align 4
  %len273 = alloca float, align 4
  %ref.tmp274 = alloca %class.btVector3, align 4
  %edge281 = alloca %class.btVector3, align 4
  %isEdgeConvex287 = alloca i8, align 1
  %swapFactor292 = alloca float, align 4
  %nA295 = alloca %class.btVector3, align 4
  %orn296 = alloca %class.btQuaternion, align 4
  %computedNormalB299 = alloca %class.btVector3, align 4
  %ref.tmp304 = alloca float, align 4
  %nB307 = alloca %class.btVector3, align 4
  %NdotA308 = alloca float, align 4
  %NdotB310 = alloca float, align 4
  %backFacingNormal312 = alloca i8, align 1
  %localContactNormalOnB325 = alloca %class.btVector3, align 4
  %ref.tmp326 = alloca %class.btMatrix3x3, align 4
  %clampedLocalNormal330 = alloca %class.btVector3, align 4
  %isClamped332 = alloca i8, align 1
  %ref.tmp333 = alloca %class.btVector3, align 4
  %ref.tmp342 = alloca %class.btVector3, align 4
  %newNormal347 = alloca %class.btVector3, align 4
  %ref.tmp351 = alloca %class.btVector3, align 4
  %ref.tmp353 = alloca %class.btVector3, align 4
  %ref.tmp357 = alloca %class.btVector3, align 4
  %ref.tmp378 = alloca float, align 4
  %ref.tmp381 = alloca %class.btVector3, align 4
  %newNormal386 = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %ref.tmp391 = alloca %class.btVector3, align 4
  %ref.tmp397 = alloca %class.btVector3, align 4
  %ref.tmp399 = alloca %class.btVector3, align 4
  %ref.tmp403 = alloca %class.btVector3, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %colObj0Wrap, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %colObj1Wrap, %struct.btCollisionObjectWrapper** %colObj1Wrap.addr, align 4, !tbaa !2
  store i32 %partId0, i32* %partId0.addr, align 4, !tbaa !6
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !6
  store i32 %normalAdjustFlags, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %0)
  %call1 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call)
  %cmp = icmp ne i32 %call1, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %cleanup.cont433

if.end:                                           ; preds = %entry
  %1 = bitcast %class.btBvhTriangleMeshShape** %trimesh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store %class.btBvhTriangleMeshShape* null, %class.btBvhTriangleMeshShape** %trimesh, align 4, !tbaa !2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call2 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %call3 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call2)
  %call4 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call3)
  %cmp5 = icmp eq i32 %call4, 22
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call7 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %3)
  %call8 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call7)
  %4 = bitcast %class.btCollisionShape* %call8 to %class.btScaledBvhTriangleMeshShape*
  %call9 = call %class.btBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv(%class.btScaledBvhTriangleMeshShape* %4)
  store %class.btBvhTriangleMeshShape* %call9, %class.btBvhTriangleMeshShape** %trimesh, align 4, !tbaa !2
  br label %if.end12

if.else:                                          ; preds = %if.end
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call10 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call11 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call10)
  %6 = bitcast %class.btCollisionShape* %call11 to %class.btBvhTriangleMeshShape*
  store %class.btBvhTriangleMeshShape* %6, %class.btBvhTriangleMeshShape** %trimesh, align 4, !tbaa !2
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then6
  %7 = bitcast %struct.btTriangleInfoMap** %triangleInfoMapPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimesh, align 4, !tbaa !2
  %call13 = call %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %8)
  store %struct.btTriangleInfoMap* %call13, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %9 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %tobool = icmp ne %struct.btTriangleInfoMap* %9, null
  br i1 %tobool, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.end12
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup430

if.end15:                                         ; preds = %if.end12
  %10 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load i32, i32* %partId0.addr, align 4, !tbaa !6
  %12 = load i32, i32* %index0.addr, align 4, !tbaa !6
  %call16 = call i32 @_ZL9btGetHashii(i32 %11, i32 %12)
  store i32 %call16, i32* %hash, align 4, !tbaa !6
  %13 = bitcast %struct.btTriangleInfo** %info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %15 = bitcast %struct.btTriangleInfoMap* %14 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 4
  %16 = bitcast i8* %add.ptr to %class.btHashMap*
  %17 = bitcast %class.btHashInt* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load i32, i32* %hash, align 4, !tbaa !6
  %call17 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp, i32 %18)
  %call18 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %16, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp)
  %19 = bitcast %class.btHashInt* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #10
  store %struct.btTriangleInfo* %call18, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %20 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %tobool19 = icmp ne %struct.btTriangleInfo* %20, null
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %if.end15
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup428

if.end21:                                         ; preds = %if.end15
  %21 = bitcast float* %frontFacing to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  %22 = load i32, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %and = and i32 %22, 1
  %cmp22 = icmp eq i32 %and, 0
  %23 = zext i1 %cmp22 to i64
  %cond = select i1 %cmp22, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %frontFacing, align 4, !tbaa !14
  %24 = bitcast %class.btTriangleShape** %tri_shape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call23 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %25)
  %26 = bitcast %class.btCollisionShape* %call23 to %class.btTriangleShape*
  store %class.btTriangleShape* %26, %class.btTriangleShape** %tri_shape, align 4, !tbaa !2
  %27 = bitcast %class.btVector3* %v0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #10
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v0)
  %28 = bitcast %class.btVector3* %v1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #10
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v1)
  %29 = bitcast %class.btVector3* %v2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #10
  %call26 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v2)
  %30 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4, !tbaa !2
  %31 = bitcast %class.btTriangleShape* %30 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %31, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %32 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %32(%class.btTriangleShape* %30, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %v0)
  %33 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4, !tbaa !2
  %34 = bitcast %class.btTriangleShape* %33 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable27 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %34, align 4, !tbaa !8
  %vfn28 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable27, i64 27
  %35 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn28, align 4
  call void %35(%class.btTriangleShape* %33, i32 1, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  %36 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4, !tbaa !2
  %37 = bitcast %class.btTriangleShape* %36 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable29 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %37, align 4, !tbaa !8
  %vfn30 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable29, i64 27
  %38 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn30, align 4
  call void %38(%class.btTriangleShape* %36, i32 2, %class.btVector3* nonnull align 4 dereferenceable(16) %v2)
  %39 = bitcast %class.btVector3* %red to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %39) #10
  %40 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #10
  store float 1.000000e+00, float* %ref.tmp31, align 4, !tbaa !14
  %41 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #10
  store float 0.000000e+00, float* %ref.tmp32, align 4, !tbaa !14
  %42 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #10
  store float 0.000000e+00, float* %ref.tmp33, align 4, !tbaa !14
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %red, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %43 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %43) #10
  %44 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #10
  %45 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #10
  %46 = bitcast %class.btVector3* %green to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #10
  %47 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #10
  store float 0.000000e+00, float* %ref.tmp35, align 4, !tbaa !14
  %48 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #10
  store float 1.000000e+00, float* %ref.tmp36, align 4, !tbaa !14
  %49 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #10
  store float 0.000000e+00, float* %ref.tmp37, align 4, !tbaa !14
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %green, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %50 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #10
  %51 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #10
  %52 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #10
  %53 = bitcast %class.btVector3* %blue to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %53) #10
  %54 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #10
  store float 0.000000e+00, float* %ref.tmp39, align 4, !tbaa !14
  %55 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #10
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !14
  %56 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #10
  store float 1.000000e+00, float* %ref.tmp41, align 4, !tbaa !14
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %blue, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %57 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #10
  %58 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  %60 = bitcast %class.btVector3* %white to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %60) #10
  %61 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #10
  store float 1.000000e+00, float* %ref.tmp43, align 4, !tbaa !14
  %62 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #10
  store float 1.000000e+00, float* %ref.tmp44, align 4, !tbaa !14
  %63 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  store float 1.000000e+00, float* %ref.tmp45, align 4, !tbaa !14
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %white, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %64 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  %65 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #10
  %66 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #10
  %67 = bitcast %class.btVector3* %black to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #10
  %68 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #10
  store float 0.000000e+00, float* %ref.tmp47, align 4, !tbaa !14
  %69 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #10
  store float 0.000000e+00, float* %ref.tmp48, align 4, !tbaa !14
  %70 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #10
  store float 0.000000e+00, float* %ref.tmp49, align 4, !tbaa !14
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %black, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %71 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #10
  %72 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %72) #10
  %73 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %73) #10
  %74 = bitcast %class.btVector3* %tri_normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %74) #10
  %call51 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tri_normal)
  %75 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %75, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %76 = bitcast %class.btVector3* %nearest to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %76) #10
  %call52 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest)
  %77 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %77, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %78 = bitcast %class.btVector3* %contact to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %78) #10
  %79 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB53 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %79, i32 0, i32 1
  %80 = bitcast %class.btVector3* %contact to i8*
  %81 = bitcast %class.btVector3* %m_localPointB53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %80, i8* align 4 %81, i32 16, i1 false), !tbaa.struct !16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isNearEdge) #10
  store i8 0, i8* %isNearEdge, align 1, !tbaa !31
  %82 = bitcast i32* %numConcaveEdgeHits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #10
  store i32 0, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %83 = bitcast i32* %numConvexEdgeHits to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #10
  store i32 0, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %84 = bitcast %class.btVector3* %localContactNormalOnB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %84) #10
  %85 = bitcast %class.btMatrix3x3* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %85) #10
  %86 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call55 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %86)
  %call56 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call55)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp54, %class.btMatrix3x3* %call56)
  %87 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %87, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB)
  %88 = bitcast %class.btMatrix3x3* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %88) #10
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %localContactNormalOnB)
  %89 = bitcast i32* %bestedge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #10
  store i32 -1, i32* %bestedge, align 4, !tbaa !6
  %90 = bitcast float* %disttobestedge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #10
  store float 0x43ABC16D60000000, float* %disttobestedge, align 4, !tbaa !14
  %91 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %91, i32 0, i32 1
  %92 = load float, float* %m_edgeV0V1Angle, align 4, !tbaa !32
  %call58 = call float @_Z6btFabsf(float %92)
  %93 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %93, i32 0, i32 6
  %94 = load float, float* %m_maxEdgeAngleThreshold, align 4, !tbaa !34
  %cmp59 = fcmp olt float %call58, %94
  br i1 %cmp59, label %if.then60, label %if.end69

if.then60:                                        ; preds = %if.end21
  %95 = bitcast %class.btVector3* %nearest61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #10
  %call62 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest61)
  %96 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB63 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %96, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB63, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest61)
  %97 = bitcast float* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #10
  %98 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp64, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest61)
  %call65 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp64)
  %99 = bitcast %class.btVector3* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %99) #10
  store float %call65, float* %len, align 4, !tbaa !14
  %100 = load float, float* %len, align 4, !tbaa !14
  %101 = load float, float* %disttobestedge, align 4, !tbaa !14
  %cmp66 = fcmp olt float %100, %101
  br i1 %cmp66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %if.then60
  store i32 0, i32* %bestedge, align 4, !tbaa !6
  %102 = load float, float* %len, align 4, !tbaa !14
  store float %102, float* %disttobestedge, align 4, !tbaa !14
  br label %if.end68

if.end68:                                         ; preds = %if.then67, %if.then60
  %103 = bitcast float* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #10
  %104 = bitcast %class.btVector3* %nearest61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %104) #10
  br label %if.end69

if.end69:                                         ; preds = %if.end68, %if.end21
  %105 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %105, i32 0, i32 2
  %106 = load float, float* %m_edgeV1V2Angle, align 4, !tbaa !36
  %call70 = call float @_Z6btFabsf(float %106)
  %107 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold71 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %107, i32 0, i32 6
  %108 = load float, float* %m_maxEdgeAngleThreshold71, align 4, !tbaa !34
  %cmp72 = fcmp olt float %call70, %108
  br i1 %cmp72, label %if.then73, label %if.end83

if.then73:                                        ; preds = %if.end69
  %109 = bitcast %class.btVector3* %nearest74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %109) #10
  %call75 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest74)
  %110 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB76 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %110, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB76, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest74)
  %111 = bitcast float* %len77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #10
  %112 = bitcast %class.btVector3* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %112) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp78, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest74)
  %call79 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp78)
  %113 = bitcast %class.btVector3* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %113) #10
  store float %call79, float* %len77, align 4, !tbaa !14
  %114 = load float, float* %len77, align 4, !tbaa !14
  %115 = load float, float* %disttobestedge, align 4, !tbaa !14
  %cmp80 = fcmp olt float %114, %115
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.then73
  store i32 1, i32* %bestedge, align 4, !tbaa !6
  %116 = load float, float* %len77, align 4, !tbaa !14
  store float %116, float* %disttobestedge, align 4, !tbaa !14
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %if.then73
  %117 = bitcast float* %len77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %117) #10
  %118 = bitcast %class.btVector3* %nearest74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %118) #10
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %if.end69
  %119 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %119, i32 0, i32 3
  %120 = load float, float* %m_edgeV2V0Angle, align 4, !tbaa !37
  %call84 = call float @_Z6btFabsf(float %120)
  %121 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold85 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %121, i32 0, i32 6
  %122 = load float, float* %m_maxEdgeAngleThreshold85, align 4, !tbaa !34
  %cmp86 = fcmp olt float %call84, %122
  br i1 %cmp86, label %if.then87, label %if.end97

if.then87:                                        ; preds = %if.end83
  %123 = bitcast %class.btVector3* %nearest88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %123) #10
  %call89 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest88)
  %124 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB90 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %124, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB90, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest88)
  %125 = bitcast float* %len91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #10
  %126 = bitcast %class.btVector3* %ref.tmp92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %126) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest88)
  %call93 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp92)
  %127 = bitcast %class.btVector3* %ref.tmp92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #10
  store float %call93, float* %len91, align 4, !tbaa !14
  %128 = load float, float* %len91, align 4, !tbaa !14
  %129 = load float, float* %disttobestedge, align 4, !tbaa !14
  %cmp94 = fcmp olt float %128, %129
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %if.then87
  store i32 2, i32* %bestedge, align 4, !tbaa !6
  %130 = load float, float* %len91, align 4, !tbaa !14
  store float %130, float* %disttobestedge, align 4, !tbaa !14
  br label %if.end96

if.end96:                                         ; preds = %if.then95, %if.then87
  %131 = bitcast float* %len91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #10
  %132 = bitcast %class.btVector3* %nearest88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %132) #10
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %if.end83
  %133 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle98 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %133, i32 0, i32 1
  %134 = load float, float* %m_edgeV0V1Angle98, align 4, !tbaa !32
  %call99 = call float @_Z6btFabsf(float %134)
  %135 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold100 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %135, i32 0, i32 6
  %136 = load float, float* %m_maxEdgeAngleThreshold100, align 4, !tbaa !34
  %cmp101 = fcmp olt float %call99, %136
  br i1 %cmp101, label %if.then102, label %if.end167

if.then102:                                       ; preds = %if.end97
  %137 = bitcast float* %len103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #10
  %138 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %138) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp104, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call105 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp104)
  %139 = bitcast %class.btVector3* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %139) #10
  store float %call105, float* %len103, align 4, !tbaa !14
  %140 = load float, float* %len103, align 4, !tbaa !14
  %141 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_edgeDistanceThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %141, i32 0, i32 5
  %142 = load float, float* %m_edgeDistanceThreshold, align 4, !tbaa !38
  %cmp106 = fcmp olt float %140, %142
  br i1 %cmp106, label %if.then107, label %if.end166

if.then107:                                       ; preds = %if.then102
  %143 = load i32, i32* %bestedge, align 4, !tbaa !6
  %cmp108 = icmp eq i32 %143, 0
  br i1 %cmp108, label %if.then109, label %if.end165

if.then109:                                       ; preds = %if.then107
  %144 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %144) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  store i8 1, i8* %isNearEdge, align 1, !tbaa !31
  %145 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle110 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %145, i32 0, i32 1
  %146 = load float, float* %m_edgeV0V1Angle110, align 4, !tbaa !32
  %cmp111 = fcmp oeq float %146, 0.000000e+00
  br i1 %cmp111, label %if.then112, label %if.else113

if.then112:                                       ; preds = %if.then109
  %147 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc = add nsw i32 %147, 1
  store i32 %inc, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end164

if.else113:                                       ; preds = %if.then109
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isEdgeConvex) #10
  %148 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %148, i32 0, i32 0
  %149 = load i32, i32* %m_flags, align 4, !tbaa !39
  %and114 = and i32 %149, 1
  %tobool115 = icmp ne i32 %and114, 0
  %frombool = zext i1 %tobool115 to i8
  store i8 %frombool, i8* %isEdgeConvex, align 1, !tbaa !31
  %150 = bitcast float* %swapFactor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #10
  %151 = load i8, i8* %isEdgeConvex, align 1, !tbaa !31, !range !40
  %tobool116 = trunc i8 %151 to i1
  %152 = zext i1 %tobool116 to i64
  %cond117 = select i1 %tobool116, float 1.000000e+00, float -1.000000e+00
  store float %cond117, float* %swapFactor, align 4, !tbaa !14
  %153 = bitcast %class.btVector3* %nA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %153) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %154 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %154) #10
  %155 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle118 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %155, i32 0, i32 1
  %call119 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %edge, float* nonnull align 4 dereferenceable(4) %m_edgeV0V1Angle118)
  %156 = bitcast %class.btVector3* %computedNormalB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %156) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %157 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags120 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %157, i32 0, i32 0
  %158 = load i32, i32* %m_flags120, align 4, !tbaa !39
  %and121 = and i32 %158, 8
  %tobool122 = icmp ne i32 %and121, 0
  br i1 %tobool122, label %if.then123, label %if.end126

if.then123:                                       ; preds = %if.else113
  %159 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %159) #10
  store float -1.000000e+00, float* %ref.tmp124, align 4, !tbaa !14
  %call125 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB, float* nonnull align 4 dereferenceable(4) %ref.tmp124)
  %160 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #10
  br label %if.end126

if.end126:                                        ; preds = %if.then123, %if.else113
  %161 = bitcast %class.btVector3* %nB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %161) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB)
  %162 = bitcast float* %NdotA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %162) #10
  %call127 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA)
  store float %call127, float* %NdotA, align 4, !tbaa !14
  %163 = bitcast float* %NdotB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %163) #10
  %call128 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB)
  store float %call128, float* %NdotB, align 4, !tbaa !14
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %backFacingNormal) #10
  %164 = load float, float* %NdotA, align 4, !tbaa !14
  %165 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %165, i32 0, i32 2
  %166 = load float, float* %m_convexEpsilon, align 4, !tbaa !41
  %cmp129 = fcmp olt float %164, %166
  br i1 %cmp129, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end126
  %167 = load float, float* %NdotB, align 4, !tbaa !14
  %168 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon130 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %168, i32 0, i32 2
  %169 = load float, float* %m_convexEpsilon130, align 4, !tbaa !41
  %cmp131 = fcmp olt float %167, %169
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end126
  %170 = phi i1 [ false, %if.end126 ], [ %cmp131, %land.rhs ]
  %frombool132 = zext i1 %170 to i8
  store i8 %frombool132, i8* %backFacingNormal, align 1, !tbaa !31
  %171 = load i8, i8* %backFacingNormal, align 1, !tbaa !31, !range !40
  %tobool133 = trunc i8 %171 to i1
  br i1 %tobool133, label %if.then134, label %if.else136

if.then134:                                       ; preds = %land.end
  %172 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc135 = add nsw i32 %172, 1
  store i32 %inc135, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end163

if.else136:                                       ; preds = %land.end
  %173 = load i32, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %inc137 = add nsw i32 %173, 1
  store i32 %inc137, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %174 = bitcast %class.btVector3* %clampedLocalNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %174) #10
  %call138 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isClamped) #10
  %175 = bitcast %class.btVector3* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %175) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp139, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %176 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle140 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %176, i32 0, i32 1
  %177 = load float, float* %m_edgeV0V1Angle140, align 4, !tbaa !32
  %call141 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp139, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB, float %177, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal)
  %178 = bitcast %class.btVector3* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %178) #10
  %frombool142 = zext i1 %call141 to i8
  store i8 %frombool142, i8* %isClamped, align 1, !tbaa !31
  %179 = load i8, i8* %isClamped, align 1, !tbaa !31, !range !40
  %tobool143 = trunc i8 %179 to i1
  br i1 %tobool143, label %if.then144, label %if.end162

if.then144:                                       ; preds = %if.else136
  %180 = load i32, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %and145 = and i32 %180, 4
  %cmp146 = icmp ne i32 %and145, 0
  %181 = bitcast %class.btVector3* %ref.tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %181) #10
  br i1 %cmp146, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then144
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp147, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call148 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp147)
  %cmp149 = fcmp ogt float %call148, 0.000000e+00
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then144
  %182 = phi i1 [ true, %if.then144 ], [ %cmp149, %lor.rhs ]
  %183 = bitcast %class.btVector3* %ref.tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %183) #10
  br i1 %182, label %if.then150, label %if.end161

if.then150:                                       ; preds = %lor.end
  %184 = bitcast %class.btVector3* %newNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %184) #10
  %185 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call151 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %185)
  %call152 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call151)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call152, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal)
  %186 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB153 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %186, i32 0, i32 4
  %187 = bitcast %class.btVector3* %m_normalWorldOnB153 to i8*
  %188 = bitcast %class.btVector3* %newNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %187, i8* align 4 %188, i32 16, i1 false), !tbaa.struct !16
  %189 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %189) #10
  %190 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %190, i32 0, i32 3
  %191 = bitcast %class.btVector3* %ref.tmp155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %191) #10
  %192 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB156 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %192, i32 0, i32 4
  %193 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %193, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp155, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB156, float* nonnull align 4 dereferenceable(4) %m_distance1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp154, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp155)
  %194 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %194, i32 0, i32 2
  %195 = bitcast %class.btVector3* %m_positionWorldOnB to i8*
  %196 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %195, i8* align 4 %196, i32 16, i1 false), !tbaa.struct !16
  %197 = bitcast %class.btVector3* %ref.tmp155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %197) #10
  %198 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %198) #10
  %199 = bitcast %class.btVector3* %ref.tmp157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %199) #10
  %200 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call158 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %200)
  %201 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB159 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %201, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp157, %class.btTransform* %call158, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB159)
  %202 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB160 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %202, i32 0, i32 1
  %203 = bitcast %class.btVector3* %m_localPointB160 to i8*
  %204 = bitcast %class.btVector3* %ref.tmp157 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %203, i8* align 4 %204, i32 16, i1 false), !tbaa.struct !16
  %205 = bitcast %class.btVector3* %ref.tmp157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %205) #10
  %206 = bitcast %class.btVector3* %newNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %206) #10
  br label %if.end161

if.end161:                                        ; preds = %if.then150, %lor.end
  br label %if.end162

if.end162:                                        ; preds = %if.end161, %if.else136
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isClamped) #10
  %207 = bitcast %class.btVector3* %clampedLocalNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %207) #10
  br label %if.end163

if.end163:                                        ; preds = %if.end162, %if.then134
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %backFacingNormal) #10
  %208 = bitcast float* %NdotB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #10
  %209 = bitcast float* %NdotA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #10
  %210 = bitcast %class.btVector3* %nB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %210) #10
  %211 = bitcast %class.btVector3* %computedNormalB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %211) #10
  %212 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %212) #10
  %213 = bitcast %class.btVector3* %nA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %213) #10
  %214 = bitcast float* %swapFactor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isEdgeConvex) #10
  br label %if.end164

if.end164:                                        ; preds = %if.end163, %if.then112
  %215 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %215) #10
  br label %if.end165

if.end165:                                        ; preds = %if.end164, %if.then107
  br label %if.end166

if.end166:                                        ; preds = %if.end165, %if.then102
  %216 = bitcast float* %len103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #10
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.end97
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %217 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle168 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %217, i32 0, i32 2
  %218 = load float, float* %m_edgeV1V2Angle168, align 4, !tbaa !36
  %call169 = call float @_Z6btFabsf(float %218)
  %219 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold170 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %219, i32 0, i32 6
  %220 = load float, float* %m_maxEdgeAngleThreshold170, align 4, !tbaa !34
  %cmp171 = fcmp olt float %call169, %220
  br i1 %cmp171, label %if.then172, label %if.end267

if.then172:                                       ; preds = %if.end167
  %221 = bitcast float* %len173 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #10
  %222 = bitcast %class.btVector3* %ref.tmp174 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %222) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp174, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call175 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp174)
  %223 = bitcast %class.btVector3* %ref.tmp174 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %223) #10
  store float %call175, float* %len173, align 4, !tbaa !14
  %224 = load float, float* %len173, align 4, !tbaa !14
  %225 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_edgeDistanceThreshold176 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %225, i32 0, i32 5
  %226 = load float, float* %m_edgeDistanceThreshold176, align 4, !tbaa !38
  %cmp177 = fcmp olt float %224, %226
  br i1 %cmp177, label %if.then178, label %if.end266

if.then178:                                       ; preds = %if.then172
  %227 = load i32, i32* %bestedge, align 4, !tbaa !6
  %cmp179 = icmp eq i32 %227, 1
  br i1 %cmp179, label %if.then180, label %if.end265

if.then180:                                       ; preds = %if.then178
  store i8 1, i8* %isNearEdge, align 1, !tbaa !31
  %228 = bitcast %class.btVector3* %edge181 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %228) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge181, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2)
  store i8 1, i8* %isNearEdge, align 1, !tbaa !31
  %229 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle182 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %229, i32 0, i32 2
  %230 = load float, float* %m_edgeV1V2Angle182, align 4, !tbaa !36
  %cmp183 = fcmp oeq float %230, 0.000000e+00
  br i1 %cmp183, label %if.then184, label %if.else186

if.then184:                                       ; preds = %if.then180
  %231 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc185 = add nsw i32 %231, 1
  store i32 %inc185, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end264

if.else186:                                       ; preds = %if.then180
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isEdgeConvex187) #10
  %232 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags188 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %232, i32 0, i32 0
  %233 = load i32, i32* %m_flags188, align 4, !tbaa !39
  %and189 = and i32 %233, 2
  %cmp190 = icmp ne i32 %and189, 0
  %frombool191 = zext i1 %cmp190 to i8
  store i8 %frombool191, i8* %isEdgeConvex187, align 1, !tbaa !31
  %234 = bitcast float* %swapFactor192 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %234) #10
  %235 = load i8, i8* %isEdgeConvex187, align 1, !tbaa !31, !range !40
  %tobool193 = trunc i8 %235 to i1
  %236 = zext i1 %tobool193 to i64
  %cond194 = select i1 %tobool193, float 1.000000e+00, float -1.000000e+00
  store float %cond194, float* %swapFactor192, align 4, !tbaa !14
  %237 = bitcast %class.btVector3* %nA195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %237) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA195, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %238 = bitcast %class.btQuaternion* %orn196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %238) #10
  %239 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle197 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %239, i32 0, i32 2
  %call198 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn196, %class.btVector3* nonnull align 4 dereferenceable(16) %edge181, float* nonnull align 4 dereferenceable(4) %m_edgeV1V2Angle197)
  %240 = bitcast %class.btVector3* %computedNormalB199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %240) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB199, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn196, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %241 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags200 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %241, i32 0, i32 0
  %242 = load i32, i32* %m_flags200, align 4, !tbaa !39
  %and201 = and i32 %242, 16
  %tobool202 = icmp ne i32 %and201, 0
  br i1 %tobool202, label %if.then203, label %if.end206

if.then203:                                       ; preds = %if.else186
  %243 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %243) #10
  store float -1.000000e+00, float* %ref.tmp204, align 4, !tbaa !14
  %call205 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB199, float* nonnull align 4 dereferenceable(4) %ref.tmp204)
  %244 = bitcast float* %ref.tmp204 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #10
  br label %if.end206

if.end206:                                        ; preds = %if.then203, %if.else186
  %245 = bitcast %class.btVector3* %nB207 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %245) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB207, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB199)
  %246 = bitcast float* %NdotA208 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %246) #10
  %call209 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA195)
  store float %call209, float* %NdotA208, align 4, !tbaa !14
  %247 = bitcast float* %NdotB210 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %247) #10
  %call211 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB207)
  store float %call211, float* %NdotB210, align 4, !tbaa !14
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %backFacingNormal212) #10
  %248 = load float, float* %NdotA208, align 4, !tbaa !14
  %249 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon213 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %249, i32 0, i32 2
  %250 = load float, float* %m_convexEpsilon213, align 4, !tbaa !41
  %cmp214 = fcmp olt float %248, %250
  br i1 %cmp214, label %land.rhs215, label %land.end218

land.rhs215:                                      ; preds = %if.end206
  %251 = load float, float* %NdotB210, align 4, !tbaa !14
  %252 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon216 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %252, i32 0, i32 2
  %253 = load float, float* %m_convexEpsilon216, align 4, !tbaa !41
  %cmp217 = fcmp olt float %251, %253
  br label %land.end218

land.end218:                                      ; preds = %land.rhs215, %if.end206
  %254 = phi i1 [ false, %if.end206 ], [ %cmp217, %land.rhs215 ]
  %frombool219 = zext i1 %254 to i8
  store i8 %frombool219, i8* %backFacingNormal212, align 1, !tbaa !31
  %255 = load i8, i8* %backFacingNormal212, align 1, !tbaa !31, !range !40
  %tobool220 = trunc i8 %255 to i1
  br i1 %tobool220, label %if.then221, label %if.else223

if.then221:                                       ; preds = %land.end218
  %256 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc222 = add nsw i32 %256, 1
  store i32 %inc222, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end263

if.else223:                                       ; preds = %land.end218
  %257 = load i32, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %inc224 = add nsw i32 %257, 1
  store i32 %inc224, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %258 = bitcast %class.btVector3* %localContactNormalOnB225 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %258) #10
  %259 = bitcast %class.btMatrix3x3* %ref.tmp226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %259) #10
  %260 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call227 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %260)
  %call228 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call227)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp226, %class.btMatrix3x3* %call228)
  %261 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB229 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %261, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB225, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp226, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB229)
  %262 = bitcast %class.btMatrix3x3* %ref.tmp226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %262) #10
  %263 = bitcast %class.btVector3* %clampedLocalNormal230 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %263) #10
  %call231 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal230)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isClamped232) #10
  %264 = bitcast %class.btVector3* %ref.tmp233 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %264) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp233, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %265 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle234 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %265, i32 0, i32 2
  %266 = load float, float* %m_edgeV1V2Angle234, align 4, !tbaa !36
  %call235 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge181, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp233, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB225, float %266, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal230)
  %267 = bitcast %class.btVector3* %ref.tmp233 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %267) #10
  %frombool236 = zext i1 %call235 to i8
  store i8 %frombool236, i8* %isClamped232, align 1, !tbaa !31
  %268 = load i8, i8* %isClamped232, align 1, !tbaa !31, !range !40
  %tobool237 = trunc i8 %268 to i1
  br i1 %tobool237, label %if.then238, label %if.end262

if.then238:                                       ; preds = %if.else223
  %269 = load i32, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %and239 = and i32 %269, 4
  %cmp240 = icmp ne i32 %and239, 0
  %270 = bitcast %class.btVector3* %ref.tmp242 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %270) #10
  br i1 %cmp240, label %lor.end245, label %lor.rhs241

lor.rhs241:                                       ; preds = %if.then238
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp242, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call243 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal230, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp242)
  %cmp244 = fcmp ogt float %call243, 0.000000e+00
  br label %lor.end245

lor.end245:                                       ; preds = %lor.rhs241, %if.then238
  %271 = phi i1 [ true, %if.then238 ], [ %cmp244, %lor.rhs241 ]
  %272 = bitcast %class.btVector3* %ref.tmp242 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %272) #10
  br i1 %271, label %if.then246, label %if.end261

if.then246:                                       ; preds = %lor.end245
  %273 = bitcast %class.btVector3* %newNormal247 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %273) #10
  %274 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call248 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %274)
  %call249 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call248)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal247, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call249, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal230)
  %275 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB250 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %275, i32 0, i32 4
  %276 = bitcast %class.btVector3* %m_normalWorldOnB250 to i8*
  %277 = bitcast %class.btVector3* %newNormal247 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %276, i8* align 4 %277, i32 16, i1 false), !tbaa.struct !16
  %278 = bitcast %class.btVector3* %ref.tmp251 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %278) #10
  %279 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnA252 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %279, i32 0, i32 3
  %280 = bitcast %class.btVector3* %ref.tmp253 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %280) #10
  %281 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB254 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %281, i32 0, i32 4
  %282 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_distance1255 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %282, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp253, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB254, float* nonnull align 4 dereferenceable(4) %m_distance1255)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp251, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA252, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp253)
  %283 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB256 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %283, i32 0, i32 2
  %284 = bitcast %class.btVector3* %m_positionWorldOnB256 to i8*
  %285 = bitcast %class.btVector3* %ref.tmp251 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %284, i8* align 4 %285, i32 16, i1 false), !tbaa.struct !16
  %286 = bitcast %class.btVector3* %ref.tmp253 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %286) #10
  %287 = bitcast %class.btVector3* %ref.tmp251 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %287) #10
  %288 = bitcast %class.btVector3* %ref.tmp257 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %288) #10
  %289 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call258 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %289)
  %290 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB259 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %290, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp257, %class.btTransform* %call258, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB259)
  %291 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB260 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %291, i32 0, i32 1
  %292 = bitcast %class.btVector3* %m_localPointB260 to i8*
  %293 = bitcast %class.btVector3* %ref.tmp257 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %292, i8* align 4 %293, i32 16, i1 false), !tbaa.struct !16
  %294 = bitcast %class.btVector3* %ref.tmp257 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %294) #10
  %295 = bitcast %class.btVector3* %newNormal247 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %295) #10
  br label %if.end261

if.end261:                                        ; preds = %if.then246, %lor.end245
  br label %if.end262

if.end262:                                        ; preds = %if.end261, %if.else223
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isClamped232) #10
  %296 = bitcast %class.btVector3* %clampedLocalNormal230 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %296) #10
  %297 = bitcast %class.btVector3* %localContactNormalOnB225 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %297) #10
  br label %if.end263

if.end263:                                        ; preds = %if.end262, %if.then221
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %backFacingNormal212) #10
  %298 = bitcast float* %NdotB210 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %298) #10
  %299 = bitcast float* %NdotA208 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %299) #10
  %300 = bitcast %class.btVector3* %nB207 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %300) #10
  %301 = bitcast %class.btVector3* %computedNormalB199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %301) #10
  %302 = bitcast %class.btQuaternion* %orn196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %302) #10
  %303 = bitcast %class.btVector3* %nA195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %303) #10
  %304 = bitcast float* %swapFactor192 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %304) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isEdgeConvex187) #10
  br label %if.end264

if.end264:                                        ; preds = %if.end263, %if.then184
  %305 = bitcast %class.btVector3* %edge181 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %305) #10
  br label %if.end265

if.end265:                                        ; preds = %if.end264, %if.then178
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %if.then172
  %306 = bitcast float* %len173 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %306) #10
  br label %if.end267

if.end267:                                        ; preds = %if.end266, %if.end167
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %307 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle268 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %307, i32 0, i32 3
  %308 = load float, float* %m_edgeV2V0Angle268, align 4, !tbaa !37
  %call269 = call float @_Z6btFabsf(float %308)
  %309 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_maxEdgeAngleThreshold270 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %309, i32 0, i32 6
  %310 = load float, float* %m_maxEdgeAngleThreshold270, align 4, !tbaa !34
  %cmp271 = fcmp olt float %call269, %310
  br i1 %cmp271, label %if.then272, label %if.end367

if.then272:                                       ; preds = %if.end267
  %311 = bitcast float* %len273 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %311) #10
  %312 = bitcast %class.btVector3* %ref.tmp274 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %312) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp274, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call275 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp274)
  %313 = bitcast %class.btVector3* %ref.tmp274 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %313) #10
  store float %call275, float* %len273, align 4, !tbaa !14
  %314 = load float, float* %len273, align 4, !tbaa !14
  %315 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_edgeDistanceThreshold276 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %315, i32 0, i32 5
  %316 = load float, float* %m_edgeDistanceThreshold276, align 4, !tbaa !38
  %cmp277 = fcmp olt float %314, %316
  br i1 %cmp277, label %if.then278, label %if.end366

if.then278:                                       ; preds = %if.then272
  %317 = load i32, i32* %bestedge, align 4, !tbaa !6
  %cmp279 = icmp eq i32 %317, 2
  br i1 %cmp279, label %if.then280, label %if.end365

if.then280:                                       ; preds = %if.then278
  store i8 1, i8* %isNearEdge, align 1, !tbaa !31
  %318 = bitcast %class.btVector3* %edge281 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %318) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge281, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0)
  %319 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle282 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %319, i32 0, i32 3
  %320 = load float, float* %m_edgeV2V0Angle282, align 4, !tbaa !37
  %cmp283 = fcmp oeq float %320, 0.000000e+00
  br i1 %cmp283, label %if.then284, label %if.else286

if.then284:                                       ; preds = %if.then280
  %321 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc285 = add nsw i32 %321, 1
  store i32 %inc285, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end364

if.else286:                                       ; preds = %if.then280
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isEdgeConvex287) #10
  %322 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags288 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %322, i32 0, i32 0
  %323 = load i32, i32* %m_flags288, align 4, !tbaa !39
  %and289 = and i32 %323, 4
  %cmp290 = icmp ne i32 %and289, 0
  %frombool291 = zext i1 %cmp290 to i8
  store i8 %frombool291, i8* %isEdgeConvex287, align 1, !tbaa !31
  %324 = bitcast float* %swapFactor292 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %324) #10
  %325 = load i8, i8* %isEdgeConvex287, align 1, !tbaa !31, !range !40
  %tobool293 = trunc i8 %325 to i1
  %326 = zext i1 %tobool293 to i64
  %cond294 = select i1 %tobool293, float 1.000000e+00, float -1.000000e+00
  store float %cond294, float* %swapFactor292, align 4, !tbaa !14
  %327 = bitcast %class.btVector3* %nA295 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %327) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA295, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %328 = bitcast %class.btQuaternion* %orn296 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %328) #10
  %329 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle297 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %329, i32 0, i32 3
  %call298 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn296, %class.btVector3* nonnull align 4 dereferenceable(16) %edge281, float* nonnull align 4 dereferenceable(4) %m_edgeV2V0Angle297)
  %330 = bitcast %class.btVector3* %computedNormalB299 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %330) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB299, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn296, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %331 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags300 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %331, i32 0, i32 0
  %332 = load i32, i32* %m_flags300, align 4, !tbaa !39
  %and301 = and i32 %332, 32
  %tobool302 = icmp ne i32 %and301, 0
  br i1 %tobool302, label %if.then303, label %if.end306

if.then303:                                       ; preds = %if.else286
  %333 = bitcast float* %ref.tmp304 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %333) #10
  store float -1.000000e+00, float* %ref.tmp304, align 4, !tbaa !14
  %call305 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB299, float* nonnull align 4 dereferenceable(4) %ref.tmp304)
  %334 = bitcast float* %ref.tmp304 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %334) #10
  br label %if.end306

if.end306:                                        ; preds = %if.then303, %if.else286
  %335 = bitcast %class.btVector3* %nB307 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %335) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB307, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB299)
  %336 = bitcast float* %NdotA308 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %336) #10
  %call309 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA295)
  store float %call309, float* %NdotA308, align 4, !tbaa !14
  %337 = bitcast float* %NdotB310 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %337) #10
  %call311 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB307)
  store float %call311, float* %NdotB310, align 4, !tbaa !14
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %backFacingNormal312) #10
  %338 = load float, float* %NdotA308, align 4, !tbaa !14
  %339 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon313 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %339, i32 0, i32 2
  %340 = load float, float* %m_convexEpsilon313, align 4, !tbaa !41
  %cmp314 = fcmp olt float %338, %340
  br i1 %cmp314, label %land.rhs315, label %land.end318

land.rhs315:                                      ; preds = %if.end306
  %341 = load float, float* %NdotB310, align 4, !tbaa !14
  %342 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4, !tbaa !2
  %m_convexEpsilon316 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %342, i32 0, i32 2
  %343 = load float, float* %m_convexEpsilon316, align 4, !tbaa !41
  %cmp317 = fcmp olt float %341, %343
  br label %land.end318

land.end318:                                      ; preds = %land.rhs315, %if.end306
  %344 = phi i1 [ false, %if.end306 ], [ %cmp317, %land.rhs315 ]
  %frombool319 = zext i1 %344 to i8
  store i8 %frombool319, i8* %backFacingNormal312, align 1, !tbaa !31
  %345 = load i8, i8* %backFacingNormal312, align 1, !tbaa !31, !range !40
  %tobool320 = trunc i8 %345 to i1
  br i1 %tobool320, label %if.then321, label %if.else323

if.then321:                                       ; preds = %land.end318
  %346 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %inc322 = add nsw i32 %346, 1
  store i32 %inc322, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  br label %if.end363

if.else323:                                       ; preds = %land.end318
  %347 = load i32, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %inc324 = add nsw i32 %347, 1
  store i32 %inc324, i32* %numConvexEdgeHits, align 4, !tbaa !6
  %348 = bitcast %class.btVector3* %localContactNormalOnB325 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %348) #10
  %349 = bitcast %class.btMatrix3x3* %ref.tmp326 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %349) #10
  %350 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call327 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %350)
  %call328 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call327)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp326, %class.btMatrix3x3* %call328)
  %351 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB329 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %351, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB325, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp326, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB329)
  %352 = bitcast %class.btMatrix3x3* %ref.tmp326 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %352) #10
  %353 = bitcast %class.btVector3* %clampedLocalNormal330 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %353) #10
  %call331 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal330)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isClamped332) #10
  %354 = bitcast %class.btVector3* %ref.tmp333 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %354) #10
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp333, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %355 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle334 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %355, i32 0, i32 3
  %356 = load float, float* %m_edgeV2V0Angle334, align 4, !tbaa !37
  %call335 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge281, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp333, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB325, float %356, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal330)
  %357 = bitcast %class.btVector3* %ref.tmp333 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %357) #10
  %frombool336 = zext i1 %call335 to i8
  store i8 %frombool336, i8* %isClamped332, align 1, !tbaa !31
  %358 = load i8, i8* %isClamped332, align 1, !tbaa !31, !range !40
  %tobool337 = trunc i8 %358 to i1
  br i1 %tobool337, label %if.then338, label %if.end362

if.then338:                                       ; preds = %if.else323
  %359 = load i32, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %and339 = and i32 %359, 4
  %cmp340 = icmp ne i32 %and339, 0
  %360 = bitcast %class.btVector3* %ref.tmp342 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %360) #10
  br i1 %cmp340, label %lor.end345, label %lor.rhs341

lor.rhs341:                                       ; preds = %if.then338
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp342, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call343 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal330, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp342)
  %cmp344 = fcmp ogt float %call343, 0.000000e+00
  br label %lor.end345

lor.end345:                                       ; preds = %lor.rhs341, %if.then338
  %361 = phi i1 [ true, %if.then338 ], [ %cmp344, %lor.rhs341 ]
  %362 = bitcast %class.btVector3* %ref.tmp342 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %362) #10
  br i1 %361, label %if.then346, label %if.end361

if.then346:                                       ; preds = %lor.end345
  %363 = bitcast %class.btVector3* %newNormal347 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %363) #10
  %364 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call348 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %364)
  %call349 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call348)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal347, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call349, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal330)
  %365 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB350 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %365, i32 0, i32 4
  %366 = bitcast %class.btVector3* %m_normalWorldOnB350 to i8*
  %367 = bitcast %class.btVector3* %newNormal347 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %366, i8* align 4 %367, i32 16, i1 false), !tbaa.struct !16
  %368 = bitcast %class.btVector3* %ref.tmp351 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %368) #10
  %369 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnA352 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %369, i32 0, i32 3
  %370 = bitcast %class.btVector3* %ref.tmp353 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %370) #10
  %371 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB354 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %371, i32 0, i32 4
  %372 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_distance1355 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %372, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp353, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB354, float* nonnull align 4 dereferenceable(4) %m_distance1355)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp351, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA352, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp353)
  %373 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB356 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %373, i32 0, i32 2
  %374 = bitcast %class.btVector3* %m_positionWorldOnB356 to i8*
  %375 = bitcast %class.btVector3* %ref.tmp351 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %374, i8* align 4 %375, i32 16, i1 false), !tbaa.struct !16
  %376 = bitcast %class.btVector3* %ref.tmp353 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %376) #10
  %377 = bitcast %class.btVector3* %ref.tmp351 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %377) #10
  %378 = bitcast %class.btVector3* %ref.tmp357 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %378) #10
  %379 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call358 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %379)
  %380 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB359 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %380, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp357, %class.btTransform* %call358, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB359)
  %381 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB360 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %381, i32 0, i32 1
  %382 = bitcast %class.btVector3* %m_localPointB360 to i8*
  %383 = bitcast %class.btVector3* %ref.tmp357 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %382, i8* align 4 %383, i32 16, i1 false), !tbaa.struct !16
  %384 = bitcast %class.btVector3* %ref.tmp357 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %384) #10
  %385 = bitcast %class.btVector3* %newNormal347 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %385) #10
  br label %if.end361

if.end361:                                        ; preds = %if.then346, %lor.end345
  br label %if.end362

if.end362:                                        ; preds = %if.end361, %if.else323
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isClamped332) #10
  %386 = bitcast %class.btVector3* %clampedLocalNormal330 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %386) #10
  %387 = bitcast %class.btVector3* %localContactNormalOnB325 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %387) #10
  br label %if.end363

if.end363:                                        ; preds = %if.end362, %if.then321
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %backFacingNormal312) #10
  %388 = bitcast float* %NdotB310 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %388) #10
  %389 = bitcast float* %NdotA308 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %389) #10
  %390 = bitcast %class.btVector3* %nB307 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %390) #10
  %391 = bitcast %class.btVector3* %computedNormalB299 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %391) #10
  %392 = bitcast %class.btQuaternion* %orn296 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %392) #10
  %393 = bitcast %class.btVector3* %nA295 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %393) #10
  %394 = bitcast float* %swapFactor292 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %394) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isEdgeConvex287) #10
  br label %if.end364

if.end364:                                        ; preds = %if.end363, %if.then284
  %395 = bitcast %class.btVector3* %edge281 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %395) #10
  br label %if.end365

if.end365:                                        ; preds = %if.end364, %if.then278
  br label %if.end366

if.end366:                                        ; preds = %if.end365, %if.then272
  %396 = bitcast float* %len273 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %396) #10
  br label %if.end367

if.end367:                                        ; preds = %if.end366, %if.end267
  %397 = load i8, i8* %isNearEdge, align 1, !tbaa !31, !range !40
  %tobool368 = trunc i8 %397 to i1
  br i1 %tobool368, label %if.then369, label %if.end408

if.then369:                                       ; preds = %if.end367
  %398 = load i32, i32* %numConcaveEdgeHits, align 4, !tbaa !6
  %cmp370 = icmp sgt i32 %398, 0
  br i1 %cmp370, label %if.then371, label %if.end407

if.then371:                                       ; preds = %if.then369
  %399 = load i32, i32* %normalAdjustFlags.addr, align 4, !tbaa !6
  %and372 = and i32 %399, 2
  %cmp373 = icmp ne i32 %and372, 0
  br i1 %cmp373, label %if.then374, label %if.else385

if.then374:                                       ; preds = %if.then371
  %call375 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %tri_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB)
  %cmp376 = fcmp olt float %call375, 0.000000e+00
  br i1 %cmp376, label %if.then377, label %if.end380

if.then377:                                       ; preds = %if.then374
  %400 = bitcast float* %ref.tmp378 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %400) #10
  store float -1.000000e+00, float* %ref.tmp378, align 4, !tbaa !14
  %call379 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tri_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp378)
  %401 = bitcast float* %ref.tmp378 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %401) #10
  br label %if.end380

if.end380:                                        ; preds = %if.then377, %if.then374
  %402 = bitcast %class.btVector3* %ref.tmp381 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %402) #10
  %403 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call382 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %403)
  %call383 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call382)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp381, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call383, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %404 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB384 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %404, i32 0, i32 4
  %405 = bitcast %class.btVector3* %m_normalWorldOnB384 to i8*
  %406 = bitcast %class.btVector3* %ref.tmp381 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %405, i8* align 4 %406, i32 16, i1 false), !tbaa.struct !16
  %407 = bitcast %class.btVector3* %ref.tmp381 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %407) #10
  br label %if.end396

if.else385:                                       ; preds = %if.then371
  %408 = bitcast %class.btVector3* %newNormal386 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %408) #10
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %newNormal386, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal, float* nonnull align 4 dereferenceable(4) %frontFacing)
  %409 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %409) #10
  %call387 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %newNormal386, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB)
  store float %call387, float* %d, align 4, !tbaa !14
  %410 = load float, float* %d, align 4, !tbaa !14
  %cmp388 = fcmp olt float %410, 0.000000e+00
  br i1 %cmp388, label %if.then389, label %if.end390

if.then389:                                       ; preds = %if.else385
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end390:                                        ; preds = %if.else385
  %411 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %411) #10
  %412 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call392 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %412)
  %call393 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call392)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp391, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call393, %class.btVector3* nonnull align 4 dereferenceable(16) %newNormal386)
  %413 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB394 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %413, i32 0, i32 4
  %414 = bitcast %class.btVector3* %m_normalWorldOnB394 to i8*
  %415 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %414, i8* align 4 %415, i32 16, i1 false), !tbaa.struct !16
  %416 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %416) #10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end390, %if.then389
  %417 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %417) #10
  %418 = bitcast %class.btVector3* %newNormal386 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %418) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup409 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end396

if.end396:                                        ; preds = %cleanup.cont, %if.end380
  %419 = bitcast %class.btVector3* %ref.tmp397 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %419) #10
  %420 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnA398 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %420, i32 0, i32 3
  %421 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %421) #10
  %422 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_normalWorldOnB400 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %422, i32 0, i32 4
  %423 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_distance1401 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %423, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp399, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB400, float* nonnull align 4 dereferenceable(4) %m_distance1401)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp397, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA398, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp399)
  %424 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB402 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %424, i32 0, i32 2
  %425 = bitcast %class.btVector3* %m_positionWorldOnB402 to i8*
  %426 = bitcast %class.btVector3* %ref.tmp397 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %425, i8* align 4 %426, i32 16, i1 false), !tbaa.struct !16
  %427 = bitcast %class.btVector3* %ref.tmp399 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %427) #10
  %428 = bitcast %class.btVector3* %ref.tmp397 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %428) #10
  %429 = bitcast %class.btVector3* %ref.tmp403 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %429) #10
  %430 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4, !tbaa !2
  %call404 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %430)
  %431 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_positionWorldOnB405 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %431, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp403, %class.btTransform* %call404, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB405)
  %432 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4, !tbaa !2
  %m_localPointB406 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %432, i32 0, i32 1
  %433 = bitcast %class.btVector3* %m_localPointB406 to i8*
  %434 = bitcast %class.btVector3* %ref.tmp403 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %433, i8* align 4 %434, i32 16, i1 false), !tbaa.struct !16
  %435 = bitcast %class.btVector3* %ref.tmp403 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %435) #10
  br label %if.end407

if.end407:                                        ; preds = %if.end396, %if.then369
  br label %if.end408

if.end408:                                        ; preds = %if.end407, %if.end367
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup409

cleanup409:                                       ; preds = %if.end408, %cleanup
  %436 = bitcast float* %disttobestedge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %436) #10
  %437 = bitcast i32* %bestedge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %437) #10
  %438 = bitcast %class.btVector3* %localContactNormalOnB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %438) #10
  %439 = bitcast i32* %numConvexEdgeHits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %439) #10
  %440 = bitcast i32* %numConcaveEdgeHits to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %440) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isNearEdge) #10
  %441 = bitcast %class.btVector3* %contact to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %441) #10
  %442 = bitcast %class.btVector3* %nearest to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %442) #10
  %443 = bitcast %class.btVector3* %tri_normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %443) #10
  %444 = bitcast %class.btVector3* %black to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %444) #10
  %445 = bitcast %class.btVector3* %white to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %445) #10
  %446 = bitcast %class.btVector3* %blue to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %446) #10
  %447 = bitcast %class.btVector3* %green to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %447) #10
  %448 = bitcast %class.btVector3* %red to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %448) #10
  %449 = bitcast %class.btVector3* %v2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %449) #10
  %450 = bitcast %class.btVector3* %v1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %450) #10
  %451 = bitcast %class.btVector3* %v0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %451) #10
  %452 = bitcast %class.btTriangleShape** %tri_shape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %452) #10
  %453 = bitcast float* %frontFacing to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %453) #10
  br label %cleanup428

cleanup428:                                       ; preds = %cleanup409, %if.then20
  %454 = bitcast %struct.btTriangleInfo** %info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %454) #10
  %455 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %455) #10
  br label %cleanup430

cleanup430:                                       ; preds = %cleanup428, %if.then14
  %456 = bitcast %struct.btTriangleInfoMap** %triangleInfoMapPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %456) #10
  %457 = bitcast %class.btBvhTriangleMeshShape** %trimesh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %457) #10
  %cleanup.dest432 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest432, label %unreachable [
    i32 0, label %cleanup.cont433
    i32 1, label %cleanup.cont433
  ]

cleanup.cont433:                                  ; preds = %if.then, %cleanup430, %cleanup430
  ret void

unreachable:                                      ; preds = %cleanup430
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4, !tbaa !42
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !44
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !46
  ret %class.btCollisionObject* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !47
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv(%class.btScaledBvhTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4, !tbaa !51
  ret %class.btBvhTriangleMeshShape* %0
}

; Function Attrs: nounwind
define internal i32 @_ZL9btGetHashii(i32 %partId, i32 %triangleIndex) #1 {
entry:
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %hash = alloca i32, align 4
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !6
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !6
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %partId.addr, align 4, !tbaa !6
  %shl = shl i32 %1, 21
  %2 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !6
  %or = or i32 %shl, %2
  store i32 %or, i32* %hash, align 4, !tbaa !6
  %3 = load i32, i32* %hash, align 4, !tbaa !6
  %4 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  ret i32 %3
}

define linkonce_odr hidden %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca %struct.btTriangleInfo*, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %index, align 4, !tbaa !6
  %2 = load i32, i32* %index, align 4, !tbaa !6
  %cmp = icmp eq i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btTriangleInfo* null, %struct.btTriangleInfo** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %3 = load i32, i32* %index, align 4, !tbaa !6
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %m_valueArray, i32 %3)
  store %struct.btTriangleInfo* %call2, %struct.btTriangleInfo** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %4 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %retval, align 4
  ret %struct.btTriangleInfo* %5
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* returned %this, i32 %uid) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %uid.addr = alloca i32, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4, !tbaa !2
  store i32 %uid, i32* %uid.addr, align 4, !tbaa !6
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %0 = load i32, i32* %uid.addr, align 4, !tbaa !6
  store i32 %0, i32* %m_uid, align 4, !tbaa !53
  ret %class.btHashInt* %this1
}

define linkonce_odr hidden void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #10
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %2 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #10
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !16
  %6 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #10
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #10
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #10
  %9 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %9)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #3 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4, !tbaa !55
  ret %class.btTransform* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !14
  %0 = load float, float* %x.addr, align 4, !tbaa !14
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #10
  %4 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4, !tbaa !2
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %4)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %class.btQuaternion* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #10
  %6 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %6)
  %7 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %7)
  %8 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %8)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  %9 = bitcast %class.btQuaternion* %q to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !14
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !14
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !14
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !14
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %inVec) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %inVec.addr = alloca %class.btVector3*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %inVec, %class.btVector3** %inVec.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %inVec.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %2) #10
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %3 = bitcast %class.btMatrix3x3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %3) #10
  %4 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !14
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !8
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btConnectivityProcessorD0Ev(%struct.btConnectivityProcessor* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %call = call %struct.btConnectivityProcessor* bitcast (%class.btTriangleCallback* (%class.btTriangleCallback*)* @_ZN18btTriangleCallbackD2Ev to %struct.btConnectivityProcessor* (%struct.btConnectivityProcessor*)*)(%struct.btConnectivityProcessor* %this1) #10
  %0 = bitcast %struct.btConnectivityProcessor* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

define linkonce_odr hidden void @_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii(%struct.btConnectivityProcessor* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %numshared = alloca i32, align 4
  %sharedVertsA = alloca [3 x i32], align 4
  %sharedVertsB = alloca [3 x i32], align 4
  %crossBSqr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %crossASqr = alloca float, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %tmp = alloca i32, align 4
  %hash = alloca i32, align 4
  %info = alloca %struct.btTriangleInfo*, align 4
  %ref.tmp75 = alloca %class.btHashInt, align 4
  %tmp79 = alloca %struct.btTriangleInfo, align 4
  %ref.tmp83 = alloca %class.btHashInt, align 4
  %ref.tmp87 = alloca %class.btHashInt, align 4
  %sumvertsA = alloca i32, align 4
  %otherIndexA = alloca i32, align 4
  %edge = alloca %class.btVector3, align 4
  %tA = alloca %class.btTriangleShape, align 4
  %otherIndexB = alloca i32, align 4
  %tB = alloca %class.btTriangleShape, align 4
  %normalA = alloca %class.btVector3, align 4
  %normalB = alloca %class.btVector3, align 4
  %edgeCrossA = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca %class.btVector3, align 4
  %tmp121 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca float, align 4
  %edgeCrossB = alloca %class.btVector3, align 4
  %ref.tmp133 = alloca %class.btVector3, align 4
  %tmp135 = alloca %class.btVector3, align 4
  %ref.tmp142 = alloca float, align 4
  %angle2 = alloca float, align 4
  %ang4 = alloca float, align 4
  %calculatedEdge = alloca %class.btVector3, align 4
  %len2 = alloca float, align 4
  %correctedAngle = alloca float, align 4
  %calculatedNormalB = alloca %class.btVector3, align 4
  %isConvex = alloca i8, align 1
  %calculatedNormalA = alloca %class.btVector3, align 4
  %dotA = alloca float, align 4
  %orn2 = alloca %class.btQuaternion, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp159 = alloca %class.btVector3, align 4
  %ref.tmp160 = alloca %class.btMatrix3x3, align 4
  %edge164 = alloca %class.btVector3, align 4
  %orn = alloca %class.btQuaternion, align 4
  %ref.tmp169 = alloca float, align 4
  %computedNormalB = alloca %class.btVector3, align 4
  %bla = alloca float, align 4
  %ref.tmp175 = alloca float, align 4
  %edge189 = alloca %class.btVector3, align 4
  %orn194 = alloca %class.btQuaternion, align 4
  %ref.tmp195 = alloca float, align 4
  %computedNormalB198 = alloca %class.btVector3, align 4
  %ref.tmp202 = alloca float, align 4
  %edge217 = alloca %class.btVector3, align 4
  %orn222 = alloca %class.btQuaternion, align 4
  %ref.tmp223 = alloca float, align 4
  %computedNormalB226 = alloca %class.btVector3, align 4
  %ref.tmp232 = alloca float, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  store i32 %partId, i32* %partId.addr, align 4, !tbaa !6
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4, !tbaa !6
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %m_partIdA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_partIdA, align 4, !tbaa !20
  %1 = load i32, i32* %partId.addr, align 4, !tbaa !6
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_triangleIndexA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_triangleIndexA, align 4, !tbaa !22
  %3 = load i32, i32* %triangleIndex.addr, align 4, !tbaa !6
  %cmp2 = icmp eq i32 %2, %3
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %cleanup.cont

if.end:                                           ; preds = %land.lhs.true, %entry
  %4 = bitcast i32* %numshared to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  store i32 0, i32* %numshared, align 4, !tbaa !6
  %5 = bitcast [3 x i32]* %sharedVertsA to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %5) #10
  %6 = bitcast [3 x i32]* %sharedVertsA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 bitcast ([3 x i32]* @__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsA to i8*), i32 12, i1 false)
  %7 = bitcast [3 x i32]* %sharedVertsB to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %7) #10
  %8 = bitcast [3 x i32]* %sharedVertsB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 bitcast ([3 x i32]* @__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsB to i8*), i32 12, i1 false)
  %9 = bitcast float* %crossBSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #10
  %11 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #10
  %12 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 1
  %13 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #10
  %15 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 2
  %16 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %17 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #10
  %18 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #10
  %19 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #10
  store float %call, float* %crossBSqr, align 4, !tbaa !14
  %20 = load float, float* %crossBSqr, align 4, !tbaa !14
  %m_triangleInfoMap = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %21 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4, !tbaa !24
  %m_equalVertexThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %21, i32 0, i32 4
  %22 = load float, float* %m_equalVertexThreshold, align 4, !tbaa !56
  %cmp8 = fcmp olt float %20, %22
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup269

if.end10:                                         ; preds = %if.end
  %23 = bitcast float* %crossASqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #10
  %24 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #10
  %25 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #10
  %m_triangleVerticesA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %26 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA, align 4, !tbaa !23
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %26, i32 1
  %m_triangleVerticesA14 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %27 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA14, align 4, !tbaa !23
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %27, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15)
  %28 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #10
  %m_triangleVerticesA17 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %29 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA17, align 4, !tbaa !23
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %29, i32 2
  %m_triangleVerticesA19 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %30 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA19, align 4, !tbaa !23
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx18, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %call21 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp11)
  %31 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #10
  %32 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #10
  %33 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #10
  store float %call21, float* %crossASqr, align 4, !tbaa !14
  %34 = load float, float* %crossASqr, align 4, !tbaa !14
  %m_triangleInfoMap22 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %35 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap22, align 4, !tbaa !24
  %m_equalVertexThreshold23 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %35, i32 0, i32 4
  %36 = load float, float* %m_equalVertexThreshold23, align 4, !tbaa !56
  %cmp24 = fcmp olt float %34, %36
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end10
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup268

if.end26:                                         ; preds = %if.end10
  %37 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc51, %if.end26
  %38 = load i32, i32* %i, align 4, !tbaa !6
  %cmp27 = icmp slt i32 %38, 3
  br i1 %cmp27, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

for.body:                                         ; preds = %for.cond
  %39 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #10
  store i32 0, i32* %j, align 4, !tbaa !6
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc, %for.body
  %40 = load i32, i32* %j, align 4, !tbaa !6
  %cmp29 = icmp slt i32 %40, 3
  br i1 %cmp29, label %for.body31, label %for.cond.cleanup30

for.cond.cleanup30:                               ; preds = %for.cond28
  store i32 5, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body31:                                       ; preds = %for.cond28
  %41 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #10
  %m_triangleVerticesA33 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %42 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA33, align 4, !tbaa !23
  %43 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx34 = getelementptr inbounds %class.btVector3, %class.btVector3* %42, i32 %43
  %44 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %45 = load i32, i32* %j, align 4, !tbaa !6
  %arrayidx35 = getelementptr inbounds %class.btVector3, %class.btVector3* %44, i32 %45
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx34, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx35)
  %call36 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp32)
  %m_triangleInfoMap37 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %46 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap37, align 4, !tbaa !24
  %m_equalVertexThreshold38 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %46, i32 0, i32 4
  %47 = load float, float* %m_equalVertexThreshold38, align 4, !tbaa !56
  %cmp39 = fcmp olt float %call36, %47
  %48 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #10
  br i1 %cmp39, label %if.then40, label %if.end46

if.then40:                                        ; preds = %for.body31
  %49 = load i32, i32* %i, align 4, !tbaa !6
  %50 = load i32, i32* %numshared, align 4, !tbaa !6
  %arrayidx41 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 %50
  store i32 %49, i32* %arrayidx41, align 4, !tbaa !6
  %51 = load i32, i32* %j, align 4, !tbaa !6
  %52 = load i32, i32* %numshared, align 4, !tbaa !6
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 %52
  store i32 %51, i32* %arrayidx42, align 4, !tbaa !6
  %53 = load i32, i32* %numshared, align 4, !tbaa !6
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %numshared, align 4, !tbaa !6
  %54 = load i32, i32* %numshared, align 4, !tbaa !6
  %cmp43 = icmp sge i32 %54, 3
  br i1 %cmp43, label %if.then44, label %if.end45

if.then44:                                        ; preds = %if.then40
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end45:                                         ; preds = %if.then40
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %for.body31
  br label %for.inc

for.inc:                                          ; preds = %if.end46
  %55 = load i32, i32* %j, align 4, !tbaa !6
  %inc47 = add nsw i32 %55, 1
  store i32 %inc47, i32* %j, align 4, !tbaa !6
  br label %for.cond28

cleanup:                                          ; preds = %if.then44, %for.cond.cleanup30
  %56 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup53 [
    i32 5, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  %57 = load i32, i32* %numshared, align 4, !tbaa !6
  %cmp48 = icmp sge i32 %57, 3
  br i1 %cmp48, label %if.then49, label %if.end50

if.then49:                                        ; preds = %for.end
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup53

if.end50:                                         ; preds = %for.end
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50
  %58 = load i32, i32* %i, align 4, !tbaa !6
  %inc52 = add nsw i32 %58, 1
  store i32 %inc52, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup53:                                        ; preds = %if.then49, %cleanup, %for.cond.cleanup
  %59 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  %cleanup.dest54 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest54, label %cleanup268 [
    i32 2, label %for.end55
  ]

for.end55:                                        ; preds = %cleanup53
  %60 = load i32, i32* %numshared, align 4, !tbaa !6
  switch i32 %60, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb56
    i32 2, label %sw.bb57
  ]

sw.bb:                                            ; preds = %for.end55
  br label %sw.epilog267

sw.bb56:                                          ; preds = %for.end55
  br label %sw.epilog267

sw.bb57:                                          ; preds = %for.end55
  %arrayidx58 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %61 = load i32, i32* %arrayidx58, align 4, !tbaa !6
  %cmp59 = icmp eq i32 %61, 0
  br i1 %cmp59, label %land.lhs.true60, label %if.end70

land.lhs.true60:                                  ; preds = %sw.bb57
  %arrayidx61 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %62 = load i32, i32* %arrayidx61, align 4, !tbaa !6
  %cmp62 = icmp eq i32 %62, 2
  br i1 %cmp62, label %if.then63, label %if.end70

if.then63:                                        ; preds = %land.lhs.true60
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  store i32 2, i32* %arrayidx64, align 4, !tbaa !6
  %arrayidx65 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  store i32 0, i32* %arrayidx65, align 4, !tbaa !6
  %63 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %64 = load i32, i32* %arrayidx66, align 4, !tbaa !6
  store i32 %64, i32* %tmp, align 4, !tbaa !6
  %arrayidx67 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %65 = load i32, i32* %arrayidx67, align 4, !tbaa !6
  %arrayidx68 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  store i32 %65, i32* %arrayidx68, align 4, !tbaa !6
  %66 = load i32, i32* %tmp, align 4, !tbaa !6
  %arrayidx69 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  store i32 %66, i32* %arrayidx69, align 4, !tbaa !6
  %67 = bitcast i32* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #10
  br label %if.end70

if.end70:                                         ; preds = %if.then63, %land.lhs.true60, %sw.bb57
  %68 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #10
  %m_partIdA71 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 1
  %69 = load i32, i32* %m_partIdA71, align 4, !tbaa !20
  %m_triangleIndexA72 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 2
  %70 = load i32, i32* %m_triangleIndexA72, align 4, !tbaa !22
  %call73 = call i32 @_ZL9btGetHashii(i32 %69, i32 %70)
  store i32 %call73, i32* %hash, align 4, !tbaa !6
  %71 = bitcast %struct.btTriangleInfo** %info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #10
  %m_triangleInfoMap74 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %72 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap74, align 4, !tbaa !24
  %73 = bitcast %struct.btTriangleInfoMap* %72 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %73, i32 4
  %74 = bitcast i8* %add.ptr to %class.btHashMap*
  %75 = bitcast %class.btHashInt* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #10
  %76 = load i32, i32* %hash, align 4, !tbaa !6
  %call76 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp75, i32 %76)
  %call77 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %74, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp75)
  %77 = bitcast %class.btHashInt* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  store %struct.btTriangleInfo* %call77, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %78 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %tobool = icmp ne %struct.btTriangleInfo* %78, null
  br i1 %tobool, label %if.end90, label %if.then78

if.then78:                                        ; preds = %if.end70
  %79 = bitcast %struct.btTriangleInfo* %tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %79) #10
  %call80 = call %struct.btTriangleInfo* @_ZN14btTriangleInfoC2Ev(%struct.btTriangleInfo* %tmp79)
  %m_triangleInfoMap81 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %80 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap81, align 4, !tbaa !24
  %81 = bitcast %struct.btTriangleInfoMap* %80 to i8*
  %add.ptr82 = getelementptr inbounds i8, i8* %81, i32 4
  %82 = bitcast i8* %add.ptr82 to %class.btHashMap*
  %83 = bitcast %class.btHashInt* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #10
  %84 = load i32, i32* %hash, align 4, !tbaa !6
  %call84 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp83, i32 %84)
  call void @_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_(%class.btHashMap* %82, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp83, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %tmp79)
  %85 = bitcast %class.btHashInt* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #10
  %m_triangleInfoMap85 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %86 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap85, align 4, !tbaa !24
  %87 = bitcast %struct.btTriangleInfoMap* %86 to i8*
  %add.ptr86 = getelementptr inbounds i8, i8* %87, i32 4
  %88 = bitcast i8* %add.ptr86 to %class.btHashMap*
  %89 = bitcast %class.btHashInt* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #10
  %90 = load i32, i32* %hash, align 4, !tbaa !6
  %call88 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp87, i32 %90)
  %call89 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %88, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp87)
  store %struct.btTriangleInfo* %call89, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %91 = bitcast %class.btHashInt* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #10
  %92 = bitcast %struct.btTriangleInfo* %tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #10
  br label %if.end90

if.end90:                                         ; preds = %if.then78, %if.end70
  %93 = bitcast i32* %sumvertsA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #10
  %arrayidx91 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %94 = load i32, i32* %arrayidx91, align 4, !tbaa !6
  %arrayidx92 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %95 = load i32, i32* %arrayidx92, align 4, !tbaa !6
  %add = add nsw i32 %94, %95
  store i32 %add, i32* %sumvertsA, align 4, !tbaa !6
  %96 = bitcast i32* %otherIndexA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #10
  %97 = load i32, i32* %sumvertsA, align 4, !tbaa !6
  %sub = sub nsw i32 3, %97
  store i32 %sub, i32* %otherIndexA, align 4, !tbaa !6
  %98 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #10
  %m_triangleVerticesA93 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %99 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA93, align 4, !tbaa !23
  %arrayidx94 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %100 = load i32, i32* %arrayidx94, align 4, !tbaa !6
  %arrayidx95 = getelementptr inbounds %class.btVector3, %class.btVector3* %99, i32 %100
  %m_triangleVerticesA96 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %101 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA96, align 4, !tbaa !23
  %arrayidx97 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %102 = load i32, i32* %arrayidx97, align 4, !tbaa !6
  %arrayidx98 = getelementptr inbounds %class.btVector3, %class.btVector3* %101, i32 %102
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx95, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98)
  %103 = bitcast %class.btTriangleShape* %tA to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %103) #10
  %m_triangleVerticesA99 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %104 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA99, align 4, !tbaa !23
  %arrayidx100 = getelementptr inbounds %class.btVector3, %class.btVector3* %104, i32 0
  %m_triangleVerticesA101 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %105 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA101, align 4, !tbaa !23
  %arrayidx102 = getelementptr inbounds %class.btVector3, %class.btVector3* %105, i32 1
  %m_triangleVerticesA103 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %106 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA103, align 4, !tbaa !23
  %arrayidx104 = getelementptr inbounds %class.btVector3, %class.btVector3* %106, i32 2
  %call105 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %tA, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx100, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx102, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx104)
  %107 = bitcast i32* %otherIndexB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #10
  %arrayidx106 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %108 = load i32, i32* %arrayidx106, align 4, !tbaa !6
  %arrayidx107 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %109 = load i32, i32* %arrayidx107, align 4, !tbaa !6
  %add108 = add nsw i32 %108, %109
  %sub109 = sub nsw i32 3, %add108
  store i32 %sub109, i32* %otherIndexB, align 4, !tbaa !6
  %110 = bitcast %class.btTriangleShape* %tB to i8*
  call void @llvm.lifetime.start.p0i8(i64 104, i8* %110) #10
  %111 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx110 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %112 = load i32, i32* %arrayidx110, align 4, !tbaa !6
  %arrayidx111 = getelementptr inbounds %class.btVector3, %class.btVector3* %111, i32 %112
  %113 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx112 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %114 = load i32, i32* %arrayidx112, align 4, !tbaa !6
  %arrayidx113 = getelementptr inbounds %class.btVector3, %class.btVector3* %113, i32 %114
  %115 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %116 = load i32, i32* %otherIndexB, align 4, !tbaa !6
  %arrayidx114 = getelementptr inbounds %class.btVector3, %class.btVector3* %115, i32 %116
  %call115 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx111, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx113, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx114)
  %117 = bitcast %class.btVector3* %normalA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %117) #10
  %call116 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalA)
  %118 = bitcast %class.btVector3* %normalB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %118) #10
  %call117 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalB)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %tA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edge)
  %119 = bitcast %class.btVector3* %edgeCrossA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %119) #10
  %120 = bitcast %class.btVector3* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %120) #10
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp119, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call120 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp119)
  %121 = bitcast %class.btVector3* %edgeCrossA to i8*
  %122 = bitcast %class.btVector3* %call120 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %121, i8* align 4 %122, i32 16, i1 false), !tbaa.struct !16
  %123 = bitcast %class.btVector3* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %123) #10
  %124 = bitcast %class.btVector3* %tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %124) #10
  %m_triangleVerticesA122 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %125 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA122, align 4, !tbaa !23
  %126 = load i32, i32* %otherIndexA, align 4, !tbaa !6
  %arrayidx123 = getelementptr inbounds %class.btVector3, %class.btVector3* %125, i32 %126
  %m_triangleVerticesA124 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %127 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA124, align 4, !tbaa !23
  %arrayidx125 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %128 = load i32, i32* %arrayidx125, align 4, !tbaa !6
  %arrayidx126 = getelementptr inbounds %class.btVector3, %class.btVector3* %127, i32 %128
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmp121, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx123, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx126)
  %call127 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp121)
  %cmp128 = fcmp olt float %call127, 0.000000e+00
  br i1 %cmp128, label %if.then129, label %if.end132

if.then129:                                       ; preds = %if.end90
  %129 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #10
  store float -1.000000e+00, float* %ref.tmp130, align 4, !tbaa !14
  %call131 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %edgeCrossA, float* nonnull align 4 dereferenceable(4) %ref.tmp130)
  %130 = bitcast float* %ref.tmp130 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #10
  br label %if.end132

if.end132:                                        ; preds = %if.then129, %if.end90
  %131 = bitcast %class.btVector3* %tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %131) #10
  %132 = bitcast %class.btVector3* %edgeCrossB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %132) #10
  %133 = bitcast %class.btVector3* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #10
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp133, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp133)
  %134 = bitcast %class.btVector3* %edgeCrossB to i8*
  %135 = bitcast %class.btVector3* %call134 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %134, i8* align 4 %135, i32 16, i1 false), !tbaa.struct !16
  %136 = bitcast %class.btVector3* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %136) #10
  %137 = bitcast %class.btVector3* %tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %137) #10
  %138 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %139 = load i32, i32* %otherIndexB, align 4, !tbaa !6
  %arrayidx136 = getelementptr inbounds %class.btVector3, %class.btVector3* %138, i32 %139
  %140 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4, !tbaa !2
  %arrayidx137 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %141 = load i32, i32* %arrayidx137, align 4, !tbaa !6
  %arrayidx138 = getelementptr inbounds %class.btVector3, %class.btVector3* %140, i32 %141
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmp135, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx136, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx138)
  %call139 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edgeCrossB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp135)
  %cmp140 = fcmp olt float %call139, 0.000000e+00
  br i1 %cmp140, label %if.then141, label %if.end144

if.then141:                                       ; preds = %if.end132
  %142 = bitcast float* %ref.tmp142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #10
  store float -1.000000e+00, float* %ref.tmp142, align 4, !tbaa !14
  %call143 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %edgeCrossB, float* nonnull align 4 dereferenceable(4) %ref.tmp142)
  %143 = bitcast float* %ref.tmp142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #10
  br label %if.end144

if.end144:                                        ; preds = %if.then141, %if.end132
  %144 = bitcast %class.btVector3* %tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %144) #10
  %145 = bitcast float* %angle2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #10
  store float 0.000000e+00, float* %angle2, align 4, !tbaa !14
  %146 = bitcast float* %ang4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #10
  store float 0.000000e+00, float* %ang4, align 4, !tbaa !14
  %147 = bitcast %class.btVector3* %calculatedEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %147) #10
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %calculatedEdge, %class.btVector3* %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  %148 = bitcast float* %len2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %148) #10
  %call145 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %calculatedEdge)
  store float %call145, float* %len2, align 4, !tbaa !14
  %149 = bitcast float* %correctedAngle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #10
  store float 0.000000e+00, float* %correctedAngle, align 4, !tbaa !14
  %150 = bitcast %class.btVector3* %calculatedNormalB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %150) #10
  %151 = bitcast %class.btVector3* %calculatedNormalB to i8*
  %152 = bitcast %class.btVector3* %normalA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false), !tbaa.struct !16
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isConvex) #10
  store i8 0, i8* %isConvex, align 1, !tbaa !31
  %153 = load float, float* %len2, align 4, !tbaa !14
  %m_triangleInfoMap146 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %154 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap146, align 4, !tbaa !24
  %m_planarEpsilon = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %154, i32 0, i32 3
  %155 = load float, float* %m_planarEpsilon, align 4, !tbaa !57
  %cmp147 = fcmp olt float %153, %155
  br i1 %cmp147, label %if.then148, label %if.else

if.then148:                                       ; preds = %if.end144
  store float 0.000000e+00, float* %angle2, align 4, !tbaa !14
  store float 0.000000e+00, float* %ang4, align 4, !tbaa !14
  br label %if.end162

if.else:                                          ; preds = %if.end144
  %call149 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %calculatedEdge)
  %156 = bitcast %class.btVector3* %calculatedNormalA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %156) #10
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %calculatedNormalA, %class.btVector3* %calculatedEdge, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossA)
  %call150 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %calculatedNormalA)
  %call151 = call float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %calculatedNormalA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  store float %call151, float* %angle2, align 4, !tbaa !14
  %157 = load float, float* %angle2, align 4, !tbaa !14
  %sub152 = fsub float 0x400921FB60000000, %157
  store float %sub152, float* %ang4, align 4, !tbaa !14
  %158 = bitcast float* %dotA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %158) #10
  %call153 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  store float %call153, float* %dotA, align 4, !tbaa !14
  %159 = load float, float* %dotA, align 4, !tbaa !14
  %conv = fpext float %159 to double
  %cmp154 = fcmp olt double %conv, 0.000000e+00
  %frombool = zext i1 %cmp154 to i8
  store i8 %frombool, i8* %isConvex, align 1, !tbaa !31
  %160 = load i8, i8* %isConvex, align 1, !tbaa !31, !range !40
  %tobool155 = trunc i8 %160 to i1
  br i1 %tobool155, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %161 = load float, float* %ang4, align 4, !tbaa !14
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %162 = load float, float* %ang4, align 4, !tbaa !14
  %fneg = fneg float %162
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %161, %cond.true ], [ %fneg, %cond.false ]
  store float %cond, float* %correctedAngle, align 4, !tbaa !14
  %163 = bitcast %class.btQuaternion* %orn2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %163) #10
  %164 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %164) #10
  %165 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg157 = fneg float %165
  store float %fneg157, float* %ref.tmp156, align 4, !tbaa !14
  %call158 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn2, %class.btVector3* nonnull align 4 dereferenceable(16) %calculatedEdge, float* nonnull align 4 dereferenceable(4) %ref.tmp156)
  %166 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #10
  %167 = bitcast %class.btVector3* %ref.tmp159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %167) #10
  %168 = bitcast %class.btMatrix3x3* %ref.tmp160 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %168) #10
  %call161 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp160, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn2)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp159, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp160, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %169 = bitcast %class.btVector3* %calculatedNormalB to i8*
  %170 = bitcast %class.btVector3* %ref.tmp159 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %169, i8* align 4 %170, i32 16, i1 false), !tbaa.struct !16
  %171 = bitcast %class.btMatrix3x3* %ref.tmp160 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %171) #10
  %172 = bitcast %class.btVector3* %ref.tmp159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %172) #10
  %173 = bitcast %class.btQuaternion* %orn2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %173) #10
  %174 = bitcast float* %dotA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #10
  %175 = bitcast %class.btVector3* %calculatedNormalA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %175) #10
  br label %if.end162

if.end162:                                        ; preds = %cond.end, %if.then148
  %176 = load i32, i32* %sumvertsA, align 4, !tbaa !6
  switch i32 %176, label %sw.epilog [
    i32 1, label %sw.bb163
    i32 2, label %sw.bb188
    i32 3, label %sw.bb216
  ]

sw.bb163:                                         ; preds = %if.end162
  %177 = bitcast %class.btVector3* %edge164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %177) #10
  %m_triangleVerticesA165 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %178 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA165, align 4, !tbaa !23
  %arrayidx166 = getelementptr inbounds %class.btVector3, %class.btVector3* %178, i32 0
  %m_triangleVerticesA167 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %179 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA167, align 4, !tbaa !23
  %arrayidx168 = getelementptr inbounds %class.btVector3, %class.btVector3* %179, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge164, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx166, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx168)
  %180 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %180) #10
  %181 = bitcast float* %ref.tmp169 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %181) #10
  %182 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg170 = fneg float %182
  store float %fneg170, float* %ref.tmp169, align 4, !tbaa !14
  %call171 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %edge164, float* nonnull align 4 dereferenceable(4) %ref.tmp169)
  %183 = bitcast float* %ref.tmp169 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #10
  %184 = bitcast %class.btVector3* %computedNormalB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %184) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %185 = bitcast float* %bla to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %185) #10
  %call172 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  store float %call172, float* %bla, align 4, !tbaa !14
  %186 = load float, float* %bla, align 4, !tbaa !14
  %cmp173 = fcmp olt float %186, 0.000000e+00
  br i1 %cmp173, label %if.then174, label %if.end177

if.then174:                                       ; preds = %sw.bb163
  %187 = bitcast float* %ref.tmp175 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %187) #10
  store float -1.000000e+00, float* %ref.tmp175, align 4, !tbaa !14
  %call176 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB, float* nonnull align 4 dereferenceable(4) %ref.tmp175)
  %188 = bitcast float* %ref.tmp175 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #10
  %189 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %189, i32 0, i32 0
  %190 = load i32, i32* %m_flags, align 4, !tbaa !39
  %or = or i32 %190, 8
  store i32 %or, i32* %m_flags, align 4, !tbaa !39
  br label %if.end177

if.end177:                                        ; preds = %if.then174, %sw.bb163
  %191 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg178 = fneg float %191
  %192 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %192, i32 0, i32 1
  store float %fneg178, float* %m_edgeV0V1Angle, align 4, !tbaa !32
  %193 = load i8, i8* %isConvex, align 1, !tbaa !31, !range !40
  %tobool179 = trunc i8 %193 to i1
  br i1 %tobool179, label %if.then180, label %if.end183

if.then180:                                       ; preds = %if.end177
  %194 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags181 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %194, i32 0, i32 0
  %195 = load i32, i32* %m_flags181, align 4, !tbaa !39
  %or182 = or i32 %195, 1
  store i32 %or182, i32* %m_flags181, align 4, !tbaa !39
  br label %if.end183

if.end183:                                        ; preds = %if.then180, %if.end177
  store i32 9, i32* %cleanup.dest.slot, align 4
  %196 = bitcast float* %bla to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #10
  %197 = bitcast %class.btVector3* %computedNormalB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %197) #10
  %198 = bitcast %class.btQuaternion* %orn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %198) #10
  %199 = bitcast %class.btVector3* %edge164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %199) #10
  br label %sw.epilog

sw.bb188:                                         ; preds = %if.end162
  %200 = bitcast %class.btVector3* %edge189 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %200) #10
  %m_triangleVerticesA190 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %201 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA190, align 4, !tbaa !23
  %arrayidx191 = getelementptr inbounds %class.btVector3, %class.btVector3* %201, i32 2
  %m_triangleVerticesA192 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %202 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA192, align 4, !tbaa !23
  %arrayidx193 = getelementptr inbounds %class.btVector3, %class.btVector3* %202, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge189, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx191, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx193)
  %203 = bitcast %class.btQuaternion* %orn194 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %203) #10
  %204 = bitcast float* %ref.tmp195 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %204) #10
  %205 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg196 = fneg float %205
  store float %fneg196, float* %ref.tmp195, align 4, !tbaa !14
  %call197 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn194, %class.btVector3* nonnull align 4 dereferenceable(16) %edge189, float* nonnull align 4 dereferenceable(4) %ref.tmp195)
  %206 = bitcast float* %ref.tmp195 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #10
  %207 = bitcast %class.btVector3* %computedNormalB198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %207) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB198, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn194, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call199 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB198, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %cmp200 = fcmp olt float %call199, 0.000000e+00
  br i1 %cmp200, label %if.then201, label %if.end206

if.then201:                                       ; preds = %sw.bb188
  %208 = bitcast float* %ref.tmp202 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #10
  store float -1.000000e+00, float* %ref.tmp202, align 4, !tbaa !14
  %call203 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB198, float* nonnull align 4 dereferenceable(4) %ref.tmp202)
  %209 = bitcast float* %ref.tmp202 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #10
  %210 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags204 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %210, i32 0, i32 0
  %211 = load i32, i32* %m_flags204, align 4, !tbaa !39
  %or205 = or i32 %211, 32
  store i32 %or205, i32* %m_flags204, align 4, !tbaa !39
  br label %if.end206

if.end206:                                        ; preds = %if.then201, %sw.bb188
  %212 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg207 = fneg float %212
  %213 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %213, i32 0, i32 3
  store float %fneg207, float* %m_edgeV2V0Angle, align 4, !tbaa !37
  %214 = load i8, i8* %isConvex, align 1, !tbaa !31, !range !40
  %tobool208 = trunc i8 %214 to i1
  br i1 %tobool208, label %if.then209, label %if.end212

if.then209:                                       ; preds = %if.end206
  %215 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags210 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %215, i32 0, i32 0
  %216 = load i32, i32* %m_flags210, align 4, !tbaa !39
  %or211 = or i32 %216, 4
  store i32 %or211, i32* %m_flags210, align 4, !tbaa !39
  br label %if.end212

if.end212:                                        ; preds = %if.then209, %if.end206
  store i32 9, i32* %cleanup.dest.slot, align 4
  %217 = bitcast %class.btVector3* %computedNormalB198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %217) #10
  %218 = bitcast %class.btQuaternion* %orn194 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %218) #10
  %219 = bitcast %class.btVector3* %edge189 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %219) #10
  br label %sw.epilog

sw.bb216:                                         ; preds = %if.end162
  %220 = bitcast %class.btVector3* %edge217 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %220) #10
  %m_triangleVerticesA218 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %221 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA218, align 4, !tbaa !23
  %arrayidx219 = getelementptr inbounds %class.btVector3, %class.btVector3* %221, i32 1
  %m_triangleVerticesA220 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %222 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA220, align 4, !tbaa !23
  %arrayidx221 = getelementptr inbounds %class.btVector3, %class.btVector3* %222, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge217, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx219, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx221)
  %223 = bitcast %class.btQuaternion* %orn222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %223) #10
  %224 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %224) #10
  %225 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg224 = fneg float %225
  store float %fneg224, float* %ref.tmp223, align 4, !tbaa !14
  %call225 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn222, %class.btVector3* nonnull align 4 dereferenceable(16) %edge217, float* nonnull align 4 dereferenceable(4) %ref.tmp223)
  %226 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #10
  %227 = bitcast %class.btVector3* %computedNormalB226 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %227) #10
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB226, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn222, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call227 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB226, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %cmp228 = fcmp olt float %call227, 0.000000e+00
  br i1 %cmp228, label %if.then229, label %if.end234

if.then229:                                       ; preds = %sw.bb216
  %228 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags230 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %228, i32 0, i32 0
  %229 = load i32, i32* %m_flags230, align 4, !tbaa !39
  %or231 = or i32 %229, 16
  store i32 %or231, i32* %m_flags230, align 4, !tbaa !39
  %230 = bitcast float* %ref.tmp232 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %230) #10
  store float -1.000000e+00, float* %ref.tmp232, align 4, !tbaa !14
  %call233 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB226, float* nonnull align 4 dereferenceable(4) %ref.tmp232)
  %231 = bitcast float* %ref.tmp232 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #10
  br label %if.end234

if.end234:                                        ; preds = %if.then229, %sw.bb216
  %232 = load float, float* %correctedAngle, align 4, !tbaa !14
  %fneg235 = fneg float %232
  %233 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %233, i32 0, i32 2
  store float %fneg235, float* %m_edgeV1V2Angle, align 4, !tbaa !36
  %234 = load i8, i8* %isConvex, align 1, !tbaa !31, !range !40
  %tobool236 = trunc i8 %234 to i1
  br i1 %tobool236, label %if.then237, label %if.end240

if.then237:                                       ; preds = %if.end234
  %235 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4, !tbaa !2
  %m_flags238 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %235, i32 0, i32 0
  %236 = load i32, i32* %m_flags238, align 4, !tbaa !39
  %or239 = or i32 %236, 2
  store i32 %or239, i32* %m_flags238, align 4, !tbaa !39
  br label %if.end240

if.end240:                                        ; preds = %if.then237, %if.end234
  store i32 9, i32* %cleanup.dest.slot, align 4
  %237 = bitcast %class.btVector3* %computedNormalB226 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %237) #10
  %238 = bitcast %class.btQuaternion* %orn222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %238) #10
  %239 = bitcast %class.btVector3* %edge217 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %239) #10
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end162, %if.end240, %if.end212, %if.end183
  store i32 8, i32* %cleanup.dest.slot, align 4
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isConvex) #10
  %240 = bitcast %class.btVector3* %calculatedNormalB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %240) #10
  %241 = bitcast float* %correctedAngle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #10
  %242 = bitcast float* %len2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #10
  %243 = bitcast %class.btVector3* %calculatedEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %243) #10
  %244 = bitcast float* %ang4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #10
  %245 = bitcast float* %angle2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #10
  %246 = bitcast %class.btVector3* %edgeCrossB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %246) #10
  %247 = bitcast %class.btVector3* %edgeCrossA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %247) #10
  %248 = bitcast %class.btVector3* %normalB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %248) #10
  %249 = bitcast %class.btVector3* %normalA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %249) #10
  %call256 = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %tB) #10
  %250 = bitcast %class.btTriangleShape* %tB to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %250) #10
  %251 = bitcast i32* %otherIndexB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #10
  %call260 = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %tA) #10
  %252 = bitcast %class.btTriangleShape* %tA to i8*
  call void @llvm.lifetime.end.p0i8(i64 104, i8* %252) #10
  %253 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %253) #10
  %254 = bitcast i32* %otherIndexA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #10
  %255 = bitcast i32* %sumvertsA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %255) #10
  %256 = bitcast %struct.btTriangleInfo** %info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %256) #10
  %257 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %257) #10
  br label %sw.epilog267

sw.default:                                       ; preds = %for.end55
  br label %sw.epilog267

sw.epilog267:                                     ; preds = %sw.default, %sw.epilog, %sw.bb56, %sw.bb
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup268

cleanup268:                                       ; preds = %sw.epilog267, %cleanup53, %if.then25
  %258 = bitcast float* %crossASqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %258) #10
  br label %cleanup269

cleanup269:                                       ; preds = %cleanup268, %if.then9
  %259 = bitcast float* %crossBSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %259) #10
  %260 = bitcast [3 x i32]* %sharedVertsB to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %260) #10
  %261 = bitcast [3 x i32]* %sharedVertsA to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %261) #10
  %262 = bitcast i32* %numshared to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #10
  %cleanup.dest273 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest273, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %if.then, %cleanup269, %cleanup269
  ret void

unreachable:                                      ; preds = %cleanup269
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btTriangleInfo* @_ZN14btTriangleInfoC2Ev(%struct.btTriangleInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btTriangleInfo*, align 4
  store %struct.btTriangleInfo* %this, %struct.btTriangleInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %this.addr, align 4
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 1
  store float 0x401921FB60000000, float* %m_edgeV0V1Angle, align 4, !tbaa !32
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 2
  store float 0x401921FB60000000, float* %m_edgeV1V2Angle, align 4, !tbaa !36
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 3
  store float 0x401921FB60000000, float* %m_edgeV2V0Angle, align 4, !tbaa !37
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 0
  store i32 0, i32* %m_flags, align 4, !tbaa !39
  ret %struct.btTriangleInfo* %this1
}

define linkonce_odr hidden void @_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %value) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %value.addr = alloca %struct.btTriangleInfo*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4, !tbaa !2
  store %struct.btTriangleInfo* %value, %struct.btTriangleInfo** %value.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !6
  %2 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %call3 = call i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %3)
  store i32 %call3, i32* %index, align 4, !tbaa !6
  %4 = load i32, i32* %index, align 4, !tbaa !6
  %cmp = icmp ne i32 %4, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %value.addr, align 4, !tbaa !2
  %m_valueArray4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %6 = load i32, i32* %index, align 4, !tbaa !6
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %m_valueArray4, i32 %6)
  %7 = bitcast %struct.btTriangleInfo* %call5 to i8*
  %8 = bitcast %struct.btTriangleInfo* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !58
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %9 = bitcast i32* %count to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  %m_valueArray6 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %m_valueArray6)
  store i32 %call7, i32* %count, align 4, !tbaa !6
  %10 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %m_valueArray8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray8)
  store i32 %call9, i32* %oldCapacity, align 4, !tbaa !6
  %m_valueArray10 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %11 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %value.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_(%class.btAlignedObjectArray.12* %m_valueArray10, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %11)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %12 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_(%class.btAlignedObjectArray.16* %m_keyArray, %class.btHashInt* nonnull align 4 dereferenceable(4) %12)
  %13 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %m_valueArray11 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray11)
  store i32 %call12, i32* %newCapacity, align 4, !tbaa !6
  %14 = load i32, i32* %oldCapacity, align 4, !tbaa !6
  %15 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %cmp13 = icmp slt i32 %14, %15
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end
  %16 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  call void @_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %16)
  %17 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %call15 = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %17)
  %m_valueArray16 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray16)
  %sub18 = sub nsw i32 %call17, 1
  %and19 = and i32 %call15, %sub18
  store i32 %and19, i32* %hash, align 4, !tbaa !6
  br label %if.end20

if.end20:                                         ; preds = %if.then14, %if.end
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %18 = load i32, i32* %hash, align 4, !tbaa !6
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable, i32 %18)
  %19 = load i32, i32* %call21, align 4, !tbaa !6
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %20 = load i32, i32* %count, align 4, !tbaa !6
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next, i32 %20)
  store i32 %19, i32* %call22, align 4, !tbaa !6
  %21 = load i32, i32* %count, align 4, !tbaa !6
  %m_hashTable23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %22 = load i32, i32* %hash, align 4, !tbaa !6
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable23, i32 %22)
  store i32 %21, i32* %call24, align 4, !tbaa !6
  %23 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = bitcast i32* %oldCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  %25 = bitcast i32* %count to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end20, %if.then
  %26 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  %27 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btTriangleShape*, align 4
  %this.addr = alloca %class.btTriangleShape*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4, !tbaa !2
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4, !tbaa !2
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store %class.btTriangleShape* %this1, %class.btTriangleShape** %retval, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btTriangleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV15btTriangleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !8
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btTriangleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 1, i32* %m_shapeType, align 4, !tbaa !44
  %3 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4, !tbaa !2
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !16
  %6 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4, !tbaa !2
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !16
  %9 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4, !tbaa !2
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !16
  %12 = load %class.btTriangleShape*, %class.btTriangleShape** %retval, align 4
  ret %class.btTriangleShape* %12
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %key = alloca i32, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %0 = bitcast i32* %key to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %1 = load i32, i32* %m_uid, align 4, !tbaa !53
  store i32 %1, i32* %key, align 4, !tbaa !6
  %2 = load i32, i32* %key, align 4, !tbaa !6
  %shl = shl i32 %2, 15
  %neg = xor i32 %shl, -1
  %3 = load i32, i32* %key, align 4, !tbaa !6
  %add = add nsw i32 %3, %neg
  store i32 %add, i32* %key, align 4, !tbaa !6
  %4 = load i32, i32* %key, align 4, !tbaa !6
  %shr = ashr i32 %4, 10
  %5 = load i32, i32* %key, align 4, !tbaa !6
  %xor = xor i32 %5, %shr
  store i32 %xor, i32* %key, align 4, !tbaa !6
  %6 = load i32, i32* %key, align 4, !tbaa !6
  %shl2 = shl i32 %6, 3
  %7 = load i32, i32* %key, align 4, !tbaa !6
  %add3 = add nsw i32 %7, %shl2
  store i32 %add3, i32* %key, align 4, !tbaa !6
  %8 = load i32, i32* %key, align 4, !tbaa !6
  %shr4 = ashr i32 %8, 6
  %9 = load i32, i32* %key, align 4, !tbaa !6
  %xor5 = xor i32 %9, %shr4
  store i32 %xor5, i32* %key, align 4, !tbaa !6
  %10 = load i32, i32* %key, align 4, !tbaa !6
  %shl6 = shl i32 %10, 11
  %neg7 = xor i32 %shl6, -1
  %11 = load i32, i32* %key, align 4, !tbaa !6
  %add8 = add nsw i32 %11, %neg7
  store i32 %add8, i32* %key, align 4, !tbaa !6
  %12 = load i32, i32* %key, align 4, !tbaa !6
  %shr9 = ashr i32 %12, 16
  %13 = load i32, i32* %key, align 4, !tbaa !6
  %xor10 = xor i32 %13, %shr9
  store i32 %xor10, i32* %key, align 4, !tbaa !6
  %14 = load i32, i32* %key, align 4, !tbaa !6
  %15 = bitcast i32* %key to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  ret i32 %14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !59
  ret i32 %0
}

define linkonce_odr hidden i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %hash = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %1)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4, !tbaa !6
  %2 = load i32, i32* %hash, align 4, !tbaa !6
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable)
  %cmp = icmp uge i32 %2, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %3 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_hashTable4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %hash, align 4, !tbaa !6
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable4, i32 %4)
  %5 = load i32, i32* %call5, align 4, !tbaa !6
  store i32 %5, i32* %index, align 4, !tbaa !6
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %6 = load i32, i32* %index, align 4, !tbaa !6
  %cmp6 = icmp ne i32 %6, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %7 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4, !tbaa !2
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %8 = load i32, i32* %index, align 4, !tbaa !6
  %call7 = call nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZNK20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %m_keyArray, i32 %8)
  %call8 = call zeroext i1 @_ZNK9btHashInt6equalsERKS_(%class.btHashInt* %7, %class.btHashInt* nonnull align 4 dereferenceable(4) %call7)
  %conv = zext i1 %call8 to i32
  %cmp9 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %9 = phi i1 [ false, %while.cond ], [ %cmp9, %land.rhs ]
  br i1 %9, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %10 = load i32, i32* %index, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next, i32 %10)
  %11 = load i32, i32* %call10, align 4, !tbaa !6
  store i32 %11, i32* %index, align 4, !tbaa !6
  br label %while.cond

while.end:                                        ; preds = %land.end
  %12 = load i32, i32* %index, align 4, !tbaa !6
  store i32 %12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %13 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %14 = bitcast i32* %hash to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %0, i32 %1
  ret %struct.btTriangleInfo* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !63
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.btTriangleInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store %struct.btTriangleInfo* %_Val, %struct.btTriangleInfo** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !63
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %2, i32 %3
  %4 = bitcast %struct.btTriangleInfo* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btTriangleInfo*
  %6 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btTriangleInfo* %5 to i8*
  %8 = bitcast %struct.btTriangleInfo* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !58
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !63
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !63
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_(%class.btAlignedObjectArray.16* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %_Val) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Val.addr = alloca %class.btHashInt*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %_Val, %class.btHashInt** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !67
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %2, i32 %3
  %4 = bitcast %class.btHashInt* %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btHashInt*
  %6 = load %class.btHashInt*, %class.btHashInt** %_Val.addr, align 4, !tbaa !2
  %7 = bitcast %class.btHashInt* %5 to i8*
  %8 = bitcast %class.btHashInt* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 4, i1 false), !tbaa.struct !68
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %9 = load i32, i32* %m_size5, align 4, !tbaa !67
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !67
  %10 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  ret void
}

define linkonce_odr hidden void @_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %0) #0 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %.addr = alloca %class.btHashInt*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %0, %class.btHashInt** %.addr, align 4, !tbaa !2
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %1 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  store i32 %call, i32* %newCapacity, align 4, !tbaa !6
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable)
  %2 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %cmp = icmp slt i32 %call2, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_hashTable3 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4, !tbaa !6
  %m_hashTable5 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %4 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %5 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  store i32 0, i32* %ref.tmp, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %m_hashTable5, i32 %4, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %6 = bitcast i32* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %7 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %8 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store i32 0, i32* %ref.tmp6, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %m_next, i32 %7, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast i32* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %12 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %cmp7 = icmp slt i32 %11, %12
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %13 = load i32, i32* %i, align 4, !tbaa !6
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable8, i32 %13)
  store i32 -1, i32* %call9, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %16 = load i32, i32* %newCapacity, align 4, !tbaa !6
  %cmp11 = icmp slt i32 %15, %16
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next13, i32 %17)
  store i32 -1, i32* %call14, align 4, !tbaa !6
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %18 = load i32, i32* %i, align 4, !tbaa !6
  %inc16 = add nsw i32 %18, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !6
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc31, %for.end17
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %20 = load i32, i32* %curHashtableSize, align 4, !tbaa !6
  %cmp19 = icmp slt i32 %19, %20
  br i1 %cmp19, label %for.body20, label %for.end33

for.body20:                                       ; preds = %for.cond18
  %21 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %22 = load i32, i32* %i, align 4, !tbaa !6
  %call21 = call nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZN20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %m_keyArray, i32 %22)
  %call22 = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %call21)
  %m_valueArray23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray23)
  %sub = sub nsw i32 %call24, 1
  %and = and i32 %call22, %sub
  store i32 %and, i32* %hashValue, align 4, !tbaa !6
  %m_hashTable25 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %23 = load i32, i32* %hashValue, align 4, !tbaa !6
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable25, i32 %23)
  %24 = load i32, i32* %call26, align 4, !tbaa !6
  %m_next27 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %25 = load i32, i32* %i, align 4, !tbaa !6
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next27, i32 %25)
  store i32 %24, i32* %call28, align 4, !tbaa !6
  %26 = load i32, i32* %i, align 4, !tbaa !6
  %m_hashTable29 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %27 = load i32, i32* %hashValue, align 4, !tbaa !6
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable29, i32 %27)
  store i32 %26, i32* %call30, align 4, !tbaa !6
  %28 = bitcast i32* %hashValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  br label %for.inc31

for.inc31:                                        ; preds = %for.body20
  %29 = load i32, i32* %i, align 4, !tbaa !6
  %inc32 = add nsw i32 %29, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !6
  br label %for.cond18

for.end33:                                        ; preds = %for.cond18
  %30 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #10
  %31 = bitcast i32* %curHashtableSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #10
  br label %if.end

if.end:                                           ; preds = %for.end33, %entry
  %32 = bitcast i32* %newCapacity to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !69
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !72
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !69
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

define linkonce_odr hidden zeroext i1 @_ZNK9btHashInt6equalsERKS_(%class.btHashInt* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %other) #0 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %other.addr = alloca %class.btHashInt*, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %other, %class.btHashInt** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %call = call i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %this1)
  %0 = load %class.btHashInt*, %class.btHashInt** %other.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %0)
  %cmp = icmp eq i32 %call, %call2
  ret i1 %cmp
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZNK20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %0, i32 %1
  ret %class.btHashInt* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_uid, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btTriangleInfo*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btTriangleInfo** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btTriangleInfo*
  store %struct.btTriangleInfo* %3, %struct.btTriangleInfo** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %4 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.btTriangleInfo* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !73
  %5 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btTriangleInfo* %5, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !59
  %7 = bitcast %struct.btTriangleInfo** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btTriangleInfo* @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.btTriangleInfo** null)
  %2 = bitcast %struct.btTriangleInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.btTriangleInfo* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btTriangleInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btTriangleInfo* %dest, %struct.btTriangleInfo** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %4, i32 %5
  %6 = bitcast %struct.btTriangleInfo* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btTriangleInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %8, i32 %9
  %10 = bitcast %struct.btTriangleInfo* %7 to i8*
  %11 = bitcast %struct.btTriangleInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !58
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv(%class.btAlignedObjectArray.12* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4, !tbaa !62
  %tobool = icmp ne %struct.btTriangleInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !73, !range !40
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data4, align 4, !tbaa !62
  call void @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.btTriangleInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btTriangleInfo* null, %struct.btTriangleInfo** %m_data5, align 4, !tbaa !62
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btTriangleInfo* @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.btTriangleInfo** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btTriangleInfo**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btTriangleInfo** %hint, %struct.btTriangleInfo*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btTriangleInfo*
  ret %struct.btTriangleInfo* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #7

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.btTriangleInfo* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.btTriangleInfo*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4, !tbaa !2
  store %struct.btTriangleInfo* %ptr, %struct.btTriangleInfo** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btTriangleInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !67
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !74
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btHashInt*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btHashInt** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btHashInt*
  store %class.btHashInt* %3, %class.btHashInt** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %class.btHashInt*, %class.btHashInt** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %class.btHashInt* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !75
  %5 = load %class.btHashInt*, %class.btHashInt** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btHashInt* %5, %class.btHashInt** %m_data, align 4, !tbaa !64
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !74
  %7 = bitcast %class.btHashInt** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %class.btHashInt* @_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %class.btHashInt** null)
  %2 = bitcast %class.btHashInt* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %class.btHashInt* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btHashInt*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %class.btHashInt* %dest, %class.btHashInt** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btHashInt*, %class.btHashInt** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %4, i32 %5
  %6 = bitcast %class.btHashInt* %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btHashInt*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %class.btHashInt, %class.btHashInt* %8, i32 %9
  %10 = bitcast %class.btHashInt* %7 to i8*
  %11 = bitcast %class.btHashInt* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 4, i1 false), !tbaa.struct !68
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv(%class.btAlignedObjectArray.16* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %tobool = icmp ne %class.btHashInt* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !75, !range !40
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btHashInt*, %class.btHashInt** %m_data4, align 4, !tbaa !64
  call void @_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %m_allocator, %class.btHashInt* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btHashInt* null, %class.btHashInt** %m_data5, align 4, !tbaa !64
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %class.btHashInt* @_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %this, i32 %n, %class.btHashInt** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btHashInt**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %class.btHashInt** %hint, %class.btHashInt*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btHashInt*
  ret %class.btHashInt* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %this, %class.btHashInt* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %class.btHashInt*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %class.btHashInt* %ptr, %class.btHashInt** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %class.btHashInt*, %class.btHashInt** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btHashInt* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !6
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %2 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  store i32 %4, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %6 = load i32, i32* %curSize, align 4, !tbaa !6
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !69
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !6
  store i32 %14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !6
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !69
  %19 = load i32, i32* %i6, align 4, !tbaa !6
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !6
  store i32 %23, i32* %21, align 4, !tbaa !6
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !6
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !6
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !72
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZN20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4, !tbaa !64
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %0, i32 %1
  ret %class.btHashInt* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !76
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !69
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !77
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.8* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !77
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #4 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.9* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, i32* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !69
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !6
  store i32 %10, i32* %7, align 4, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !69
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !69
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !76, !range !40
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !69
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !69
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.9* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapeD0Ev(%class.btTriangleShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btTriangleShape* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev to %class.btTriangleShape* (%class.btTriangleShape*)*)(%class.btTriangleShape* %this1) #10
  %0 = bitcast %class.btTriangleShape* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btConvexInternalShape* %0 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #7

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #7

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #7

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store float %mass, float* %mass.addr, align 4, !tbaa !14
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4, !tbaa !2
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZNK15btTriangleShape7getNameEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0)
}

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !14
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4, !tbaa !14
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4, !tbaa !78
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !78
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %2, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %3 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %4 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %4, i32 0, i32 0
  %5 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %6 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %3, i8* %5, %class.btSerializer* %6)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %8 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %8, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %9 = load float, float* %m_collisionMargin, align 4, !tbaa !78
  %10 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4, !tbaa !2
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %10, i32 0, i32 3
  store float %9, float* %m_collisionMargin4, align 4, !tbaa !80
  %11 = bitcast %struct.btConvexInternalShapeData** %shapeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #7

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #7

define linkonce_odr hidden void @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %dir) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 %call
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !16
  %4 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #10
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4)) unnamed_addr #7

define linkonce_odr hidden void @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btTriangleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dir = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  store i32 %numVectors, i32* %numVectors.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %2 = load i32, i32* %numVectors.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4, !tbaa !2
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  store %class.btVector3* %arrayidx, %class.btVector3** %dir, align 4, !tbaa !2
  %7 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #10
  %8 = load %class.btVector3*, %class.btVector3** %dir, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 1
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 %call
  %9 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4, !tbaa !2
  %10 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = bitcast %class.btVector3* %arrayidx9 to i8*
  %12 = bitcast %class.btVector3* %arrayidx8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !16
  %13 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #10
  %14 = bitcast %class.btVector3** %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 2
}

define linkonce_odr hidden void @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i32, i32* %index.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %3, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #7

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape14getNumVerticesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape11getNumEdgesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

define linkonce_odr hidden void @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4, !tbaa !2
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4, !tbaa !2
  %2 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %2, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %3 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = load i32, i32* %i.addr, align 4, !tbaa !6
  %add = add nsw i32 %4, 1
  %rem = srem i32 %add, 3
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4, !tbaa !2
  %6 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable2 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %6, align 4, !tbaa !8
  %vfn3 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable2, i64 27
  %7 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn3, align 4
  call void %7(%class.btTriangleShape* %this1, i32 %rem, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK15btTriangleShape9getVertexEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %vert) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %vert.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  store %class.btVector3* %vert, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vert.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !16
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btTriangleShape12getNumPlanesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 1
}

define linkonce_odr hidden void @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport, i32 %i) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %1 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %3 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %3, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 31
  %4 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZNK15btTriangleShape8isInsideERK9btVector3f(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, float %tolerance) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShape*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  %tolerance.addr = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %planeconst = alloca float, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %edgeNormal = alloca %class.btVector3, align 4
  %dist9 = alloca float, align 4
  %edgeConst = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4, !tbaa !2
  store float %tolerance, float* %tolerance.addr, align 4, !tbaa !14
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %1 = bitcast float* %dist to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call2, float* %dist, align 4, !tbaa !14
  %3 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call3, float* %planeconst, align 4, !tbaa !14
  %4 = load float, float* %planeconst, align 4, !tbaa !14
  %5 = load float, float* %dist, align 4, !tbaa !14
  %sub = fsub float %5, %4
  store float %sub, float* %dist, align 4, !tbaa !14
  %6 = load float, float* %dist, align 4, !tbaa !14
  %7 = load float, float* %tolerance.addr, align 4, !tbaa !14
  %fneg = fneg float %7
  %cmp = fcmp oge float %6, %fneg
  br i1 %cmp, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %entry
  %8 = load float, float* %dist, align 4, !tbaa !14
  %9 = load float, float* %tolerance.addr, align 4, !tbaa !14
  %cmp4 = fcmp ole float %8, %9
  br i1 %cmp4, label %if.then, label %if.end22

if.then:                                          ; preds = %land.lhs.true
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %11 = load i32, i32* %i, align 4, !tbaa !6
  %cmp5 = icmp slt i32 %11, 3
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #10
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %13 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #10
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %14 = load i32, i32* %i, align 4, !tbaa !6
  %15 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %15, align 4, !tbaa !8
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 26
  %16 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %16(%class.btTriangleShape* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  %17 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #10
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %pa)
  %18 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #10
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edgeNormal, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edgeNormal)
  %19 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4, !tbaa !2
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call10, float* %dist9, align 4, !tbaa !14
  %21 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call11, float* %edgeConst, align 4, !tbaa !14
  %22 = load float, float* %edgeConst, align 4, !tbaa !14
  %23 = load float, float* %dist9, align 4, !tbaa !14
  %sub12 = fsub float %23, %22
  store float %sub12, float* %dist9, align 4, !tbaa !14
  %24 = load float, float* %dist9, align 4, !tbaa !14
  %25 = load float, float* %tolerance.addr, align 4, !tbaa !14
  %fneg13 = fneg float %25
  %cmp14 = fcmp olt float %24, %fneg13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %for.body
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then15
  %26 = bitcast float* %edgeConst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  %27 = bitcast float* %dist9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  %28 = bitcast %class.btVector3* %edgeNormal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #10
  %29 = bitcast %class.btVector3* %edge to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #10
  %30 = bitcast %class.btVector3* %pb to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #10
  %31 = bitcast %class.btVector3* %pa to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup21 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %for.inc

for.inc:                                          ; preds = %cleanup.cont
  %32 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup21

cleanup21:                                        ; preds = %for.end, %cleanup
  %33 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #10
  br label %cleanup23

if.end22:                                         ; preds = %land.lhs.true, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup23

cleanup23:                                        ; preds = %if.end22, %cleanup21
  %34 = bitcast float* %planeconst to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #10
  %35 = bitcast float* %dist to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #10
  %36 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #10
  %37 = load i1, i1* %retval, align 1
  ret i1 %37
}

define linkonce_odr hidden void @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4, !tbaa !2
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !16
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btTriangleShapedlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !6
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !14
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !14
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !14
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !14
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !14
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !14
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !14
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !14
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !14
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !14
  store float %y, float* %y.addr, align 4, !tbaa !14
  %0 = load float, float* %x.addr, align 4, !tbaa !14
  %1 = load float, float* %y.addr, align 4, !tbaa !14
  %call = call float @atan2f(float %0, float %1) #12
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !2
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %1)
  store float %call, float* %d, align 4, !tbaa !14
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %mul = fmul float %4, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %5 = load float, float* %d, align 4, !tbaa !14
  %div = fdiv float %call2, %5
  store float %div, float* %s, align 4, !tbaa !14
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4, !tbaa !14
  %10 = load float, float* %s, align 4, !tbaa !14
  %mul4 = fmul float %9, %10
  store float %mul4, float* %ref.tmp, align 4, !tbaa !14
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %12)
  %13 = load float, float* %call6, align 4, !tbaa !14
  %14 = load float, float* %s, align 4, !tbaa !14
  %mul7 = fmul float %13, %14
  store float %mul7, float* %ref.tmp5, align 4, !tbaa !14
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call9, align 4, !tbaa !14
  %18 = load float, float* %s, align 4, !tbaa !14
  %mul10 = fmul float %17, %18
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !14
  %19 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load float*, float** %_angle.addr, align 4, !tbaa !2
  %21 = load float, float* %20, align 4, !tbaa !14
  %mul12 = fmul float %21, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !14
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %22 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  %26 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  %27 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !14
  %0 = load float, float* %x.addr, align 4, !tbaa !14
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !14
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !14
  %0 = load float, float* %x.addr, align 4, !tbaa !14
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #9

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !6
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !14
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load float, float* %d, align 4, !tbaa !14
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !14
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !14
  %8 = load float, float* %s, align 4, !tbaa !14
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !14
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !14
  %13 = load float, float* %s, align 4, !tbaa !14
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !14
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !14
  %18 = load float, float* %s, align 4, !tbaa !14
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !14
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !14
  %23 = load float, float* %xs, align 4, !tbaa !14
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !14
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !14
  %28 = load float, float* %ys, align 4, !tbaa !14
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !14
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #10
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !14
  %33 = load float, float* %zs, align 4, !tbaa !14
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !14
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #10
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !14
  %38 = load float, float* %xs, align 4, !tbaa !14
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !14
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #10
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !14
  %43 = load float, float* %ys, align 4, !tbaa !14
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !14
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #10
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !14
  %48 = load float, float* %zs, align 4, !tbaa !14
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !14
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #10
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !14
  %53 = load float, float* %ys, align 4, !tbaa !14
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !14
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #10
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !14
  %58 = load float, float* %zs, align 4, !tbaa !14
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !14
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #10
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !14
  %63 = load float, float* %zs, align 4, !tbaa !14
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !14
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #10
  %65 = load float, float* %yy, align 4, !tbaa !14
  %66 = load float, float* %zz, align 4, !tbaa !14
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #10
  %68 = load float, float* %xy, align 4, !tbaa !14
  %69 = load float, float* %wz, align 4, !tbaa !14
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !14
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #10
  %71 = load float, float* %xz, align 4, !tbaa !14
  %72 = load float, float* %wy, align 4, !tbaa !14
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !14
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #10
  %74 = load float, float* %xy, align 4, !tbaa !14
  %75 = load float, float* %wz, align 4, !tbaa !14
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !14
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #10
  %77 = load float, float* %xx, align 4, !tbaa !14
  %78 = load float, float* %zz, align 4, !tbaa !14
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !14
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #10
  %80 = load float, float* %yz, align 4, !tbaa !14
  %81 = load float, float* %wx, align 4, !tbaa !14
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !14
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #10
  %83 = load float, float* %xz, align 4, !tbaa !14
  %84 = load float, float* %wy, align 4, !tbaa !14
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !14
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #10
  %86 = load float, float* %yz, align 4, !tbaa !14
  %87 = load float, float* %wx, align 4, !tbaa !14
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !14
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #10
  %89 = load float, float* %xx, align 4, !tbaa !14
  %90 = load float, float* %yy, align 4, !tbaa !14
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !14
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #10
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #10
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #10
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #10
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #10
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #10
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #10
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #10
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #10
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #10
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #10
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #10
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #10
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #10
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #10
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #10
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #10
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #10
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #10
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #10
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #10
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #10
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #10
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !14
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !14
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !14
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !14
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !14
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !14
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !14
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !14
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !14
  %0 = load float, float* %y.addr, align 4, !tbaa !14
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #4 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %2 = bitcast %class.btQuaternion* %1 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %2)
  %3 = load float, float* %call, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call1, align 4, !tbaa !14
  %mul = fmul float %3, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4, !tbaa !14
  %9 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %9)
  %10 = load float, float* %call3, align 4, !tbaa !14
  %mul4 = fmul float %8, %10
  %add = fadd float %mul, %mul4
  %11 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %12 = bitcast %class.btQuaternion* %11 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %12)
  %13 = load float, float* %call5, align 4, !tbaa !14
  %14 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call6, align 4, !tbaa !14
  %mul7 = fmul float %13, %15
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %18)
  %19 = load float, float* %call9, align 4, !tbaa !14
  %20 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !14
  %mul11 = fmul float %19, %21
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %23)
  %24 = load float, float* %call12, align 4, !tbaa !14
  %25 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %25)
  %26 = load float, float* %call13, align 4, !tbaa !14
  %mul14 = fmul float %24, %26
  %add15 = fadd float %mul11, %mul14
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %28)
  %29 = load float, float* %call16, align 4, !tbaa !14
  %30 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call17, align 4, !tbaa !14
  %mul18 = fmul float %29, %31
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4, !tbaa !14
  %32 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #10
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call21, align 4, !tbaa !14
  %36 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call22, align 4, !tbaa !14
  %mul23 = fmul float %35, %37
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %39)
  %40 = load float, float* %call24, align 4, !tbaa !14
  %41 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %41)
  %42 = load float, float* %call25, align 4, !tbaa !14
  %mul26 = fmul float %40, %42
  %add27 = fadd float %mul23, %mul26
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %44)
  %45 = load float, float* %call28, align 4, !tbaa !14
  %46 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %46)
  %47 = load float, float* %call29, align 4, !tbaa !14
  %mul30 = fmul float %45, %47
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4, !tbaa !14
  %48 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #10
  %49 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %50 = bitcast %class.btQuaternion* %49 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %50)
  %51 = load float, float* %call33, align 4, !tbaa !14
  %fneg = fneg float %51
  %52 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %52)
  %53 = load float, float* %call34, align 4, !tbaa !14
  %mul35 = fmul float %fneg, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %55)
  %56 = load float, float* %call36, align 4, !tbaa !14
  %57 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %57)
  %58 = load float, float* %call37, align 4, !tbaa !14
  %mul38 = fmul float %56, %58
  %sub39 = fsub float %mul35, %mul38
  %59 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %60 = bitcast %class.btQuaternion* %59 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %60)
  %61 = load float, float* %call40, align 4, !tbaa !14
  %62 = load %class.btVector3*, %class.btVector3** %w.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %62)
  %63 = load float, float* %call41, align 4, !tbaa !14
  %mul42 = fmul float %61, %63
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4, !tbaa !14
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %64 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  %65 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #10
  %66 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #10
  %67 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #10
  ret void
}

define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %5 = load float, float* %arrayidx4, align 4, !tbaa !14
  %fneg5 = fneg float %5
  store float %fneg5, float* %ref.tmp2, align 4, !tbaa !14
  %6 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %8 = load float, float* %arrayidx8, align 4, !tbaa !14
  %fneg9 = fneg float %8
  store float %fneg9, float* %ref.tmp6, align 4, !tbaa !14
  %9 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %10 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %3 = load float, float* %arrayidx, align 4, !tbaa !14
  %4 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %5 = bitcast %class.btQuaternion* %4 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %5)
  %6 = load float, float* %call, align 4, !tbaa !14
  %mul = fmul float %3, %6
  %7 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %7, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %8 = load float, float* %arrayidx3, align 4, !tbaa !14
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %11 = load float, float* %arrayidx5, align 4, !tbaa !14
  %mul6 = fmul float %8, %11
  %add = fadd float %mul, %mul6
  %12 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %12, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %13 = load float, float* %arrayidx8, align 4, !tbaa !14
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %15)
  %16 = load float, float* %call9, align 4, !tbaa !14
  %mul10 = fmul float %13, %16
  %add11 = fadd float %add, %mul10
  %17 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %17, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %18 = load float, float* %arrayidx13, align 4, !tbaa !14
  %19 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %20 = bitcast %class.btQuaternion* %19 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %20)
  %21 = load float, float* %call14, align 4, !tbaa !14
  %mul15 = fmul float %18, %21
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %22 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %23, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %24 = load float, float* %arrayidx18, align 4, !tbaa !14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %26)
  %27 = load float, float* %call19, align 4, !tbaa !14
  %mul20 = fmul float %24, %27
  %28 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %28, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %29 = load float, float* %arrayidx22, align 4, !tbaa !14
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %32 = load float, float* %arrayidx24, align 4, !tbaa !14
  %mul25 = fmul float %29, %32
  %add26 = fadd float %mul20, %mul25
  %33 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %33, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %34 = load float, float* %arrayidx28, align 4, !tbaa !14
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call29, align 4, !tbaa !14
  %mul30 = fmul float %34, %37
  %add31 = fadd float %add26, %mul30
  %38 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %38, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %39 = load float, float* %arrayidx33, align 4, !tbaa !14
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %41)
  %42 = load float, float* %call34, align 4, !tbaa !14
  %mul35 = fmul float %39, %42
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4, !tbaa !14
  %43 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #10
  %44 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %44, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %45 = load float, float* %arrayidx39, align 4, !tbaa !14
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call40, align 4, !tbaa !14
  %mul41 = fmul float %45, %48
  %49 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %50 = load float, float* %arrayidx43, align 4, !tbaa !14
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %52, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %53 = load float, float* %arrayidx45, align 4, !tbaa !14
  %mul46 = fmul float %50, %53
  %add47 = fadd float %mul41, %mul46
  %54 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %54, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %55 = load float, float* %arrayidx49, align 4, !tbaa !14
  %56 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %57 = bitcast %class.btQuaternion* %56 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %57)
  %58 = load float, float* %call50, align 4, !tbaa !14
  %mul51 = fmul float %55, %58
  %add52 = fadd float %add47, %mul51
  %59 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %59, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %60 = load float, float* %arrayidx54, align 4, !tbaa !14
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %62)
  %63 = load float, float* %call55, align 4, !tbaa !14
  %mul56 = fmul float %60, %63
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4, !tbaa !14
  %64 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #10
  %65 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %65, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %66 = load float, float* %arrayidx60, align 4, !tbaa !14
  %67 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %68 = bitcast %class.btQuaternion* %67 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %68, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %69 = load float, float* %arrayidx62, align 4, !tbaa !14
  %mul63 = fmul float %66, %69
  %70 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %70, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %71 = load float, float* %arrayidx65, align 4, !tbaa !14
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %73)
  %74 = load float, float* %call66, align 4, !tbaa !14
  %mul67 = fmul float %71, %74
  %sub68 = fsub float %mul63, %mul67
  %75 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %75, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %76 = load float, float* %arrayidx70, align 4, !tbaa !14
  %77 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %78 = bitcast %class.btQuaternion* %77 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %78)
  %79 = load float, float* %call71, align 4, !tbaa !14
  %mul72 = fmul float %76, %79
  %sub73 = fsub float %sub68, %mul72
  %80 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %80, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %81 = load float, float* %arrayidx75, align 4, !tbaa !14
  %82 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !2
  %83 = bitcast %class.btQuaternion* %82 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %83)
  %84 = load float, float* %call76, align 4, !tbaa !14
  %mul77 = fmul float %81, %84
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4, !tbaa !14
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  %85 = bitcast float* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #10
  %86 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #10
  %87 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #10
  %88 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #10
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4, !tbaa !2
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float*, float** %_z.addr, align 4, !tbaa !2
  %4 = load float*, float** %_w.addr, align 4, !tbaa !2
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  store float* %_w, float** %_w.addr, align 4, !tbaa !2
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %6 = load float*, float** %_w.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !14
  ret %class.btQuadWord* %this1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { nounwind }
attributes #11 = { builtin nounwind }
attributes #12 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"vtable pointer", !5, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"_ZTS14PHY_ScalarType", !4, i64 0}
!12 = !{!13, !13, i64 0}
!13 = !{!"short", !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{i64 0, i64 16, !17}
!17 = !{!4, !4, i64 0}
!18 = !{!19, !19, i64 0}
!19 = !{!"double", !4, i64 0}
!20 = !{!21, !7, i64 4}
!21 = !{!"_ZTS23btConnectivityProcessor", !7, i64 4, !7, i64 8, !3, i64 12, !3, i64 16}
!22 = !{!21, !7, i64 8}
!23 = !{!21, !3, i64 12}
!24 = !{!21, !3, i64 16}
!25 = !{!26, !3, i64 56}
!26 = !{!"_ZTS22btBvhTriangleMeshShape", !3, i64 52, !3, i64 56, !27, i64 60, !27, i64 61, !4, i64 62}
!27 = !{!"bool", !4, i64 0}
!28 = !{!29, !3, i64 48}
!29 = !{!"_ZTS19btTriangleMeshShape", !30, i64 16, !30, i64 32, !3, i64 48}
!30 = !{!"_ZTS9btVector3", !4, i64 0}
!31 = !{!27, !27, i64 0}
!32 = !{!33, !15, i64 4}
!33 = !{!"_ZTS14btTriangleInfo", !7, i64 0, !15, i64 4, !15, i64 8, !15, i64 12}
!34 = !{!35, !15, i64 100}
!35 = !{!"_ZTS17btTriangleInfoMap", !15, i64 84, !15, i64 88, !15, i64 92, !15, i64 96, !15, i64 100, !15, i64 104}
!36 = !{!33, !15, i64 8}
!37 = !{!33, !15, i64 12}
!38 = !{!35, !15, i64 96}
!39 = !{!33, !7, i64 0}
!40 = !{i8 0, i8 2}
!41 = !{!35, !15, i64 84}
!42 = !{!43, !3, i64 4}
!43 = !{!"_ZTS24btCollisionObjectWrapper", !3, i64 0, !3, i64 4, !3, i64 8, !3, i64 12, !7, i64 16, !7, i64 20}
!44 = !{!45, !7, i64 4}
!45 = !{!"_ZTS16btCollisionShape", !7, i64 4, !3, i64 8}
!46 = !{!43, !3, i64 8}
!47 = !{!48, !3, i64 192}
!48 = !{!"_ZTS17btCollisionObject", !49, i64 4, !49, i64 68, !30, i64 132, !30, i64 148, !30, i64 164, !7, i64 180, !15, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !7, i64 204, !7, i64 208, !7, i64 212, !7, i64 216, !15, i64 220, !15, i64 224, !15, i64 228, !15, i64 232, !7, i64 236, !4, i64 240, !15, i64 244, !15, i64 248, !15, i64 252, !7, i64 256, !7, i64 260}
!49 = !{!"_ZTS11btTransform", !50, i64 0, !30, i64 48}
!50 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!51 = !{!52, !3, i64 32}
!52 = !{!"_ZTS28btScaledBvhTriangleMeshShape", !30, i64 16, !3, i64 32}
!53 = !{!54, !7, i64 0}
!54 = !{!"_ZTS9btHashInt", !7, i64 0}
!55 = !{!43, !3, i64 12}
!56 = !{!35, !15, i64 92}
!57 = !{!35, !15, i64 88}
!58 = !{i64 0, i64 4, !6, i64 4, i64 4, !14, i64 8, i64 4, !14, i64 12, i64 4, !14}
!59 = !{!60, !7, i64 8}
!60 = !{!"_ZTS20btAlignedObjectArrayI14btTriangleInfoE", !61, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !27, i64 16}
!61 = !{!"_ZTS18btAlignedAllocatorI14btTriangleInfoLj16EE"}
!62 = !{!60, !3, i64 12}
!63 = !{!60, !7, i64 4}
!64 = !{!65, !3, i64 12}
!65 = !{!"_ZTS20btAlignedObjectArrayI9btHashIntE", !66, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !27, i64 16}
!66 = !{!"_ZTS18btAlignedAllocatorI9btHashIntLj16EE"}
!67 = !{!65, !7, i64 4}
!68 = !{i64 0, i64 4, !6}
!69 = !{!70, !3, i64 12}
!70 = !{!"_ZTS20btAlignedObjectArrayIiE", !71, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !27, i64 16}
!71 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!72 = !{!70, !7, i64 4}
!73 = !{!60, !27, i64 16}
!74 = !{!65, !7, i64 8}
!75 = !{!65, !27, i64 16}
!76 = !{!70, !27, i64 16}
!77 = !{!70, !7, i64 8}
!78 = !{!79, !15, i64 44}
!79 = !{!"_ZTS21btConvexInternalShape", !30, i64 12, !30, i64 28, !15, i64 44, !15, i64 48}
!80 = !{!81, !15, i64 44}
!81 = !{!"_ZTS25btConvexInternalShapeData", !82, i64 0, !83, i64 12, !83, i64 28, !15, i64 44, !7, i64 48}
!82 = !{!"_ZTS20btCollisionShapeData", !3, i64 0, !7, i64 4, !4, i64 8}
!83 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
