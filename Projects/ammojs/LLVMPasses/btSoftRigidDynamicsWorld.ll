; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftRigidDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btSoftRigidDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSoftRigidDynamicsWorld = type <{ %class.btDiscreteDynamicsWorld, %class.btAlignedObjectArray.16, i32, i8, i8, i8, i8, %struct.btSoftBodyWorldInfo, %class.btSoftBodySolver*, i8, [3 x i8] }>
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.0, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.8, i32, i8, [3 x i8], %class.btAlignedObjectArray.12 }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btSimulationIslandManager = type opaque
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btActionInterface = type opaque
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i32, i32, i32, i32, i8*, i8, float, float, float, float, float, float, float, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btSoftBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray.19, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.43, %class.btAlignedObjectArray.48, %class.btAlignedObjectArray.52, %class.btAlignedObjectArray.56, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.64, %class.btAlignedObjectArray.68, %class.btAlignedObjectArray.72, %class.btAlignedObjectArray.76, %class.btAlignedObjectArray.84, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.96, %class.btAlignedObjectArray.100, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.104 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.27, %class.btAlignedObjectArray.27 }
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%class.btAlignedObjectArray.27 = type <{ %class.btAlignedAllocator.28, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.28 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.31, %class.btAlignedObjectArray.35, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.31 = type <{ %class.btAlignedAllocator.32, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.32 = type { i8 }
%class.btAlignedObjectArray.35 = type <{ %class.btAlignedAllocator.36, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.36 = type { i8 }
%class.btAlignedObjectArray.43 = type <{ %class.btAlignedAllocator.44, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.44 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.46 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.46 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.48 = type <{ %class.btAlignedAllocator.49, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.49 = type { i8 }
%class.btAlignedObjectArray.52 = type <{ %class.btAlignedAllocator.53, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.53 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.56 = type <{ %class.btAlignedAllocator.57, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.57 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.64 = type <{ %class.btAlignedAllocator.65, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.65 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btAlignedObjectArray.68 = type <{ %class.btAlignedAllocator.69, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.69 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.72 = type <{ %class.btAlignedAllocator.73, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.73 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.76 = type <{ %class.btAlignedAllocator.77, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.77 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.35, %class.btAlignedObjectArray.79, %class.btAlignedObjectArray.31, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.79 = type <{ %class.btAlignedAllocator.80, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.80 = type { i8 }
%class.btAlignedObjectArray.84 = type <{ %class.btAlignedAllocator.85, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.85 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.88, %class.btAlignedObjectArray.92 }
%class.btAlignedObjectArray.88 = type <{ %class.btAlignedAllocator.89, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.89 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.92 = type <{ %class.btAlignedAllocator.93, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.93 = type { i8 }
%class.btAlignedObjectArray.96 = type <{ %class.btAlignedAllocator.97, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.97 = type { i8 }
%class.btAlignedObjectArray.100 = type <{ %class.btAlignedAllocator.101, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.101 = type { i8 }
%class.btAlignedObjectArray.104 = type <{ %class.btAlignedAllocator.105, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.105 = type { i8 }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%struct.btSparseSdf = type { %class.btAlignedObjectArray.39, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.39 = type <{ %class.btAlignedAllocator.40, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.40 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type { [4 x [4 x [4 x float]]], [3 x i32], i32, i32, %class.btCollisionShape*, %"struct.btSparseSdf<3>::Cell"* }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type opaque
%class.btCollisionConfiguration = type opaque
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%class.btDefaultSoftBodySolver = type { %class.btSoftBodySolver, i8, [3 x i8], %class.btAlignedObjectArray.16 }
%class.CProfileSample = type { i8 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i16, i16, i32 }
%struct.btSoftSingleRayCallback = type { %struct.btBroadphaseRayCallback, %class.btVector3, %class.btVector3, %class.btTransform, %class.btTransform, %class.btVector3, %class.btSoftRigidDynamicsWorld*, %"struct.btCollisionWorld::RayResultCallback"* }
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%"struct.btSoftBody::sRayCast" = type { %class.btSoftBody*, i32, i32, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }
%"struct.btCollisionWorld::LocalRayResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, float }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev = comdat any

$_ZN19btSoftBodyWorldInfoC2Ev = comdat any

$_ZN11btSparseSdfILi3EE10InitializeEii = comdat any

$_ZN11btSparseSdfILi3EE5ResetEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN19btSoftBodyWorldInfoD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev = comdat any

$_ZN23btDiscreteDynamicsWorlddlEPv = comdat any

$_ZN14CProfileSampleC2EPKc = comdat any

$_ZN14CProfileSampleD2Ev = comdat any

$_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi = comdat any

$_ZN16btSoftBodySolver12getTimeScaleEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_ = comdat any

$_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_ = comdat any

$_ZN10btSoftBody6upcastEP17btCollisionObject = comdat any

$_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZNK16btCollisionShape10isSoftBodyEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZN11btSparseSdfILi3EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv = comdat any

$_ZN11btSparseSdfILi3EED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_ = comdat any

$_ZN23btBroadphaseRayCallbackC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN23btSoftSingleRayCallbackD0Ev = comdat any

$_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy = comdat any

$_ZN24btBroadphaseAabbCallbackC2Ev = comdat any

$_ZN23btBroadphaseRayCallbackD0Ev = comdat any

$_ZN24btBroadphaseAabbCallbackD2Ev = comdat any

$_ZN24btBroadphaseAabbCallbackD0Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN17btBroadphaseProxy10isSoftBodyEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_ = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv = comdat any

$_ZTV23btSoftSingleRayCallback = comdat any

$_ZTS23btSoftSingleRayCallback = comdat any

$_ZTS23btBroadphaseRayCallback = comdat any

$_ZTS24btBroadphaseAabbCallback = comdat any

$_ZTI24btBroadphaseAabbCallback = comdat any

$_ZTI23btBroadphaseRayCallback = comdat any

$_ZTI23btSoftSingleRayCallback = comdat any

$_ZTV23btBroadphaseRayCallback = comdat any

$_ZTV24btBroadphaseAabbCallback = comdat any

@_ZTV24btSoftRigidDynamicsWorld = hidden unnamed_addr constant { [47 x i8*] } { [47 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btSoftRigidDynamicsWorld to i8*), i8* bitcast (%class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK24btSoftRigidDynamicsWorld7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i16, i16)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btCollisionObject*)* @_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btSerializer*)* @_ZN24btSoftRigidDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i16, i16)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btSoftRigidDynamicsWorld*)* @_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, float)* @_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)* @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, float)* @_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*)] }, align 4
@.str = private unnamed_addr constant [34 x i8] c"predictUnconstraintMotionSoftBody\00", align 1
@.str.1 = private unnamed_addr constant [21 x i8] c"solveSoftConstraints\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"rayTest\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS24btSoftRigidDynamicsWorld = hidden constant [27 x i8] c"24btSoftRigidDynamicsWorld\00", align 1
@_ZTI23btDiscreteDynamicsWorld = external constant i8*
@_ZTI24btSoftRigidDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btSoftRigidDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btDiscreteDynamicsWorld to i8*) }, align 4
@_ZTV23btSoftSingleRayCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btSoftSingleRayCallback to i8*), i8* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to i8*), i8* bitcast (void (%struct.btSoftSingleRayCallback*)* @_ZN23btSoftSingleRayCallbackD0Ev to i8*), i8* bitcast (i1 (%struct.btSoftSingleRayCallback*, %struct.btBroadphaseProxy*)* @_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy to i8*)] }, comdat, align 4
@_ZTS23btSoftSingleRayCallback = linkonce_odr hidden constant [26 x i8] c"23btSoftSingleRayCallback\00", comdat, align 1
@_ZTS23btBroadphaseRayCallback = linkonce_odr hidden constant [26 x i8] c"23btBroadphaseRayCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS24btBroadphaseAabbCallback = linkonce_odr hidden constant [27 x i8] c"24btBroadphaseAabbCallback\00", comdat, align 1
@_ZTI24btBroadphaseAabbCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btBroadphaseAabbCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI23btBroadphaseRayCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btBroadphaseRayCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI24btBroadphaseAabbCallback to i8*) }, comdat, align 4
@_ZTI23btSoftSingleRayCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btSoftSingleRayCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btBroadphaseRayCallback to i8*) }, comdat, align 4
@_ZTV23btBroadphaseRayCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btBroadphaseRayCallback to i8*), i8* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to i8*), i8* bitcast (void (%struct.btBroadphaseRayCallback*)* @_ZN23btBroadphaseRayCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV24btBroadphaseAabbCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI24btBroadphaseAabbCallback to i8*), i8* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to i8*), i8* bitcast (void (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN24btSoftRigidDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver = hidden unnamed_addr alias %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*, %class.btSoftBodySolver*), %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*, %class.btSoftBodySolver*)* @_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver
@_ZN24btSoftRigidDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*), %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD2Ev

define hidden %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver(%class.btSoftRigidDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration, %class.btSoftBodySolver* %softBodySolver) unnamed_addr #0 {
entry:
  %retval = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %softBodySolver.addr = alloca %class.btSoftBodySolver*, align 4
  %ptr = alloca i8*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  store %class.btConstraintSolver* %constraintSolver, %class.btConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  store %class.btSoftBodySolver* %softBodySolver, %class.btSoftBodySolver** %softBodySolver.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %this1, %class.btSoftRigidDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  %3 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver.addr, align 4, !tbaa !2
  %4 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4, !tbaa !2
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btConstraintSolver* %3, %class.btCollisionConfiguration* %4)
  %5 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [47 x i8*] }, { [47 x i8*] }* @_ZTV24btSoftRigidDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !6
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray.16* %m_softBodies)
  %m_sbi = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %call3 = call %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoC2Ev(%struct.btSoftBodyWorldInfo* %m_sbi)
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %6 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %softBodySolver.addr, align 4, !tbaa !2
  store %class.btSoftBodySolver* %6, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  %m_ownsSolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  store i8 0, i8* %m_ownsSolver, align 4, !tbaa !20
  %m_softBodySolver4 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %7 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver4, align 4, !tbaa !8
  %tobool = icmp ne %class.btSoftBodySolver* %7, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 40, i32 16)
  store i8* %call5, i8** %ptr, align 4, !tbaa !2
  %9 = load i8*, i8** %ptr, align 4, !tbaa !2
  %10 = bitcast i8* %9 to %class.btDefaultSoftBodySolver*
  %call6 = call %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC1Ev(%class.btDefaultSoftBodySolver* %10)
  %11 = bitcast %class.btDefaultSoftBodySolver* %10 to %class.btSoftBodySolver*
  %m_softBodySolver7 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  store %class.btSoftBodySolver* %11, %class.btSoftBodySolver** %m_softBodySolver7, align 4, !tbaa !8
  %m_ownsSolver8 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  store i8 1, i8* %m_ownsSolver8, align 4, !tbaa !20
  %12 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_drawFlags = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 2
  store i32 4302, i32* %m_drawFlags, align 4, !tbaa !21
  %m_drawNodeTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 3
  store i8 1, i8* %m_drawNodeTree, align 4, !tbaa !22
  %m_drawFaceTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 4
  store i8 0, i8* %m_drawFaceTree, align 1, !tbaa !23
  %m_drawClusterTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 5
  store i8 0, i8* %m_drawClusterTree, align 2, !tbaa !24
  %13 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4, !tbaa !2
  %m_sbi9 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_broadphase = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi9, i32 0, i32 5
  store %class.btBroadphaseInterface* %13, %class.btBroadphaseInterface** %m_broadphase, align 4, !tbaa !25
  %14 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %m_sbi10 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_dispatcher = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi10, i32 0, i32 6
  store %class.btDispatcher* %14, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !26
  %m_sbi11 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi11, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %m_sparsesdf, i32 2383, i32 262144)
  %m_sbi12 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf13 = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi12, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %m_sparsesdf13)
  %m_sbi14 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %air_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi14, i32 0, i32 0
  store float 0x3FF3333340000000, float* %air_density, align 4, !tbaa !27
  %m_sbi15 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi15, i32 0, i32 1
  store float 0.000000e+00, float* %water_density, align 4, !tbaa !28
  %m_sbi16 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_offset = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi16, i32 0, i32 2
  store float 0.000000e+00, float* %water_offset, align 4, !tbaa !29
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #10
  %16 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  store float 0.000000e+00, float* %ref.tmp17, align 4, !tbaa !30
  %17 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  store float 0.000000e+00, float* %ref.tmp18, align 4, !tbaa !30
  %18 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  store float 0.000000e+00, float* %ref.tmp19, align 4, !tbaa !30
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %m_sbi21 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_normal = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi21, i32 0, i32 4
  %19 = bitcast %class.btVector3* %water_normal to i8*
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !31
  %21 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %22 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #10
  %m_sbi22 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_gravity = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi22, i32 0, i32 7
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #10
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !30
  %26 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #10
  store float -1.000000e+01, float* %ref.tmp24, align 4, !tbaa !30
  %27 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #10
  store float 0.000000e+00, float* %ref.tmp25, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %28 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  %29 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #10
  %30 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #10
  %m_sbi26 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf27 = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi26, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %m_sparsesdf27, i32 2383, i32 262144)
  %31 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %retval, align 4
  ret %class.btSoftRigidDynamicsWorld* %31
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #1

define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator.17* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

define linkonce_odr hidden %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoC2Ev(%struct.btSoftBodyWorldInfo* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %struct.btSoftBodyWorldInfo* %this, %struct.btSoftBodyWorldInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %air_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 0
  store float 0x3FF3333340000000, float* %air_density, align 4, !tbaa !33
  %water_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %water_density, align 4, !tbaa !34
  %water_offset = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %water_offset, align 4, !tbaa !35
  %m_maxDisplacement = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 3
  store float 1.000000e+03, float* %m_maxDisplacement, align 4, !tbaa !36
  %water_normal = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %water_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  %m_broadphase = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 5
  store %class.btBroadphaseInterface* null, %class.btBroadphaseInterface** %m_broadphase, align 4, !tbaa !37
  %m_dispatcher = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 6
  store %class.btDispatcher* null, %class.btDispatcher** %m_dispatcher, align 4, !tbaa !38
  %m_gravity = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 7
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  store float -1.000000e+01, float* %ref.tmp5, align 4, !tbaa !30
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !30
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 8
  %call8 = call %struct.btSparseSdf* @_ZN11btSparseSdfILi3EEC2Ev(%struct.btSparseSdf* %m_sparsesdf)
  ret %struct.btSoftBodyWorldInfo* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

declare %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC1Ev(%class.btDefaultSoftBodySolver* returned) unnamed_addr #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define linkonce_odr hidden void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %this, i32 %hashsize, i32 %clampCells) #0 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  %hashsize.addr = alloca i32, align 4
  %clampCells.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4, !tbaa !2
  store i32 %hashsize, i32* %hashsize.addr, align 4, !tbaa !39
  store i32 %clampCells, i32* %clampCells.addr, align 4, !tbaa !39
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %0 = load i32, i32* %clampCells.addr, align 4, !tbaa !39
  %m_clampCells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 4
  store i32 %0, i32* %m_clampCells, align 4, !tbaa !40
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %1 = load i32, i32* %hashsize.addr, align 4, !tbaa !39
  %2 = bitcast %"struct.btSparseSdf<3>::Cell"** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store %"struct.btSparseSdf<3>::Cell"* null, %"struct.btSparseSdf<3>::Cell"** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_(%class.btAlignedObjectArray.39* %cells, i32 %1, %"struct.btSparseSdf<3>::Cell"** nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast %"struct.btSparseSdf<3>::Cell"** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  call void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %this1)
  ret void
}

define linkonce_odr hidden void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %this) #0 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  %i = alloca i32, align 4
  %ni = alloca i32, align 4
  %pc = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  %pn = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store i32 0, i32* %i, align 4, !tbaa !39
  %1 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %cells)
  store i32 %call, i32* %ni, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %3 = load i32, i32* %ni, align 4, !tbaa !39
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %ni to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %"struct.btSparseSdf<3>::Cell"** %pc to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %cells2 = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %7 = load i32, i32* %i, align 4, !tbaa !39
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.39* %cells2, i32 %7)
  %8 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %call3, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %8, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %cells4 = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %9 = load i32, i32* %i, align 4, !tbaa !39
  %call5 = call nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.39* %cells4, i32 %9)
  store %"struct.btSparseSdf<3>::Cell"* null, %"struct.btSparseSdf<3>::Cell"** %call5, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %delete.end, %for.body
  %10 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %tobool = icmp ne %"struct.btSparseSdf<3>::Cell"* %10, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = bitcast %"struct.btSparseSdf<3>::Cell"** %pn to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  %12 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %next = getelementptr inbounds %"struct.btSparseSdf<3>::Cell", %"struct.btSparseSdf<3>::Cell"* %12, i32 0, i32 5
  %13 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %next, align 4, !tbaa !41
  store %"struct.btSparseSdf<3>::Cell"* %13, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  %14 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %isnull = icmp eq %"struct.btSparseSdf<3>::Cell"* %14, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %while.body
  %15 = bitcast %"struct.btSparseSdf<3>::Cell"* %14 to i8*
  call void @_ZdlPv(i8* %15) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %while.body
  %16 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pn, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %16, %"struct.btSparseSdf<3>::Cell"** %pc, align 4, !tbaa !2
  %17 = bitcast %"struct.btSparseSdf<3>::Cell"** %pn to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %18 = bitcast %"struct.btSparseSdf<3>::Cell"** %pc to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #10
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %19 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %voxelsz = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 1
  store float 2.500000e-01, float* %voxelsz, align 4, !tbaa !43
  %puid = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 2
  store i32 0, i32* %puid, align 4, !tbaa !44
  %ncells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 3
  store i32 0, i32* %ncells, align 4, !tbaa !45
  %nprobes = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 5
  store i32 1, i32* %nprobes, align 4, !tbaa !46
  %nqueries = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 6
  store i32 1, i32* %nqueries, align 4, !tbaa !47
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !30
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !30
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !30
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !30
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !30
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !30
  ret void
}

; Function Attrs: nounwind
define hidden %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldD2Ev(%class.btSoftRigidDynamicsWorld* returned %this) unnamed_addr #4 {
entry:
  %retval = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %this1, %class.btSoftRigidDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [47 x i8*] }, { [47 x i8*] }* @_ZTV24btSoftRigidDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_ownsSolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  %1 = load i8, i8* %m_ownsSolver, align 4, !tbaa !20, !range !48
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %2 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  %3 = bitcast %class.btSoftBodySolver* %2 to %class.btSoftBodySolver* (%class.btSoftBodySolver*)***
  %vtable = load %class.btSoftBodySolver* (%class.btSoftBodySolver*)**, %class.btSoftBodySolver* (%class.btSoftBodySolver*)*** %3, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btSoftBodySolver* (%class.btSoftBodySolver*)*, %class.btSoftBodySolver* (%class.btSoftBodySolver*)** %vtable, i64 0
  %4 = load %class.btSoftBodySolver* (%class.btSoftBodySolver*)*, %class.btSoftBodySolver* (%class.btSoftBodySolver*)** %vfn, align 4
  %call = call %class.btSoftBodySolver* %4(%class.btSoftBodySolver* %2) #10
  %m_softBodySolver2 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %5 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver2, align 4, !tbaa !8
  %6 = bitcast %class.btSoftBodySolver* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_sbi = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %call3 = call %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoD2Ev(%struct.btSoftBodyWorldInfo* %m_sbi) #10
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray.16* %m_softBodies) #10
  %7 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call5 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* %7) #10
  %8 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %retval, align 4
  ret %class.btSoftRigidDynamicsWorld* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoD2Ev(%struct.btSoftBodyWorldInfo* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  store %struct.btSoftBodyWorldInfo* %this, %struct.btSoftBodyWorldInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 8
  %call = call %struct.btSparseSdf* @_ZN11btSparseSdfILi3EED2Ev(%struct.btSparseSdf* %m_sparsesdf) #10
  ret %struct.btSoftBodyWorldInfo* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: nounwind
declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned) unnamed_addr #5

; Function Attrs: nounwind
define hidden void @_ZN24btSoftRigidDynamicsWorldD0Ev(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #4 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %call = call %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldD1Ev(%class.btSoftRigidDynamicsWorld* %this1) #10
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i8*
  call void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %0) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %ptr) #3 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld* %0, float %1)
  %2 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %2) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i32 0, i32 0))
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %3 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  %4 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %5 = bitcast %class.btSoftBodySolver* %3 to void (%class.btSoftBodySolver*, float)***
  %vtable = load void (%class.btSoftBodySolver*, float)**, void (%class.btSoftBodySolver*, float)*** %5, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vtable, i64 6
  %6 = load void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vfn, align 4
  call void %6(%class.btSoftBodySolver* %3, float %4)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %7 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %7) #10
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #1

define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* returned %this, i8* %name) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  %name.addr = alloca i8*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  store i8* %name, i8** %name.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  %0 = load i8*, i8** %name.addr, align 4, !tbaa !2
  call void @_ZN15CProfileManager13Start_ProfileEPKc(i8* %0)
  ret %class.CProfileSample* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.CProfileSample*, align 4
  store %class.CProfileSample* %this, %class.CProfileSample** %this.addr, align 4, !tbaa !2
  %this1 = load %class.CProfileSample*, %class.CProfileSample** %this.addr, align 4
  call void @_ZN15CProfileManager12Stop_ProfileEv()
  ret %class.CProfileSample* %this1
}

define hidden void @_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.16* @_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv(%class.btSoftRigidDynamicsWorld* %this1)
  %1 = bitcast %class.btSoftBodySolver* %0 to void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)***
  %vtable = load void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)**, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)*, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)** %vtable, i64 4
  %2 = load void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)*, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.16*, i1)** %vfn, align 4
  call void %2(%class.btSoftBodySolver* %0, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %call, i1 zeroext false)
  %m_softBodySolver2 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %3 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver2, align 4, !tbaa !8
  %4 = bitcast %class.btSoftBodySolver* %3 to i1 (%class.btSoftBodySolver*)***
  %vtable3 = load i1 (%class.btSoftBodySolver*)**, i1 (%class.btSoftBodySolver*)*** %4, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds i1 (%class.btSoftBodySolver*)*, i1 (%class.btSoftBodySolver*)** %vtable3, i64 3
  %5 = load i1 (%class.btSoftBodySolver*)*, i1 (%class.btSoftBodySolver*)** %vfn4, align 4
  %call5 = call zeroext i1 %5(%class.btSoftBodySolver* %3)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %7 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld* %6, float %7)
  %8 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEf(%class.btSoftRigidDynamicsWorld* %this1, float %8)
  %9 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  store i32 0, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !39
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %m_softBodies)
  %cmp = icmp slt i32 %10, %call6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %12 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %m_softBodies7 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %13 = load i32, i32* %i, align 4, !tbaa !39
  %call8 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.16* %m_softBodies7, i32 %13)
  %14 = load %class.btSoftBody*, %class.btSoftBody** %call8, align 4, !tbaa !2
  store %class.btSoftBody* %14, %class.btSoftBody** %psb, align 4, !tbaa !2
  %15 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %16 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody* %15, %class.btSoftBody* %16)
  %17 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_softBodySolver9 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %19 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver9, align 4, !tbaa !8
  %20 = bitcast %class.btSoftBodySolver* %19 to void (%class.btSoftBodySolver*)***
  %vtable10 = load void (%class.btSoftBodySolver*)**, void (%class.btSoftBodySolver*)*** %20, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds void (%class.btSoftBodySolver*)*, void (%class.btSoftBodySolver*)** %vtable10, i64 8
  %21 = load void (%class.btSoftBodySolver*)*, void (%class.btSoftBodySolver*)** %vfn11, align 4
  call void %21(%class.btSoftBodySolver* %19)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.16* @_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv(%class.btSoftRigidDynamicsWorld* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.16* %m_softBodies
}

declare void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #1

define hidden void @_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0))
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %m_softBodies)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_softBodies3 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN10btSoftBody13solveClustersERK20btAlignedObjectArrayIPS_E(%class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %m_softBodies3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  %2 = load float, float* %timeStep.addr, align 4, !tbaa !30
  %m_softBodySolver4 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %3 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver4, align 4, !tbaa !8
  %call5 = call float @_ZN16btSoftBodySolver12getTimeScaleEv(%class.btSoftBodySolver* %3)
  %mul = fmul float %2, %call5
  %4 = bitcast %class.btSoftBodySolver* %1 to void (%class.btSoftBodySolver*, float)***
  %vtable = load void (%class.btSoftBodySolver*, float)**, void (%class.btSoftBodySolver*, float)*** %4, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vtable, i64 7
  %5 = load void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vfn, align 4
  call void %5(%class.btSoftBodySolver* %1, float %mul)
  %call6 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %6 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %6) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !49
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %1 = load i32, i32* %n.addr, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %0, i32 %1
  ret %class.btSoftBody** %arrayidx
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody*, %class.btSoftBody*) #1

declare void @_ZN10btSoftBody13solveClustersERK20btAlignedObjectArrayIPS_E(%class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17)) #1

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZN16btSoftBodySolver12getTimeScaleEv(%class.btSoftBodySolver* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_timeScale = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 3
  %0 = load float, float* %m_timeScale, align 4, !tbaa !51
  ret float %0
}

define hidden void @_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyss(%class.btSoftRigidDynamicsWorld* %this, %class.btSoftBody* %body, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask) #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %body.addr = alloca %class.btSoftBody*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody* %body, %class.btSoftBody** %body.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !53
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !53
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_(%class.btAlignedObjectArray.16* %m_softBodies, %class.btSoftBody** nonnull align 4 dereferenceable(4) %body.addr)
  %0 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4, !tbaa !2
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !8
  call void @_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver(%class.btSoftBody* %0, %class.btSoftBodySolver* %1)
  %2 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %3 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4, !tbaa !2
  %4 = bitcast %class.btSoftBody* %3 to %class.btCollisionObject*
  %5 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !53
  %6 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !53
  call void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss(%class.btCollisionWorld* %2, %class.btCollisionObject* %4, i16 signext %5, i16 signext %6)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_(%class.btAlignedObjectArray.16* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %_Val) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Val.addr = alloca %class.btSoftBody**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody** %_Val, %class.btSoftBody*** %_Val.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !39
  %1 = load i32, i32* %sz, align 4, !tbaa !39
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %2, i32 %3
  %4 = bitcast %class.btSoftBody** %arrayidx to i8*
  %5 = bitcast i8* %4 to %class.btSoftBody**
  %6 = load %class.btSoftBody**, %class.btSoftBody*** %_Val.addr, align 4, !tbaa !2
  %7 = load %class.btSoftBody*, %class.btSoftBody** %6, align 4, !tbaa !2
  store %class.btSoftBody* %7, %class.btSoftBody** %5, align 4, !tbaa !2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4, !tbaa !49
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4, !tbaa !49
  %9 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver(%class.btSoftBody* %this, %class.btSoftBodySolver* %softBodySolver) #4 comdat {
entry:
  %this.addr = alloca %class.btSoftBody*, align 4
  %softBodySolver.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBody* %this, %class.btSoftBody** %this.addr, align 4, !tbaa !2
  store %class.btSoftBodySolver* %softBodySolver, %class.btSoftBodySolver** %softBodySolver.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBody*, %class.btSoftBody** %this.addr, align 4
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %softBodySolver.addr, align 4, !tbaa !2
  %m_softBodySolver = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %this1, i32 0, i32 2
  store %class.btSoftBodySolver* %0, %class.btSoftBodySolver** %m_softBodySolver, align 4, !tbaa !55
  ret void
}

declare void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss(%class.btCollisionWorld*, %class.btCollisionObject*, i16 signext, i16 signext) unnamed_addr #1

define hidden void @_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody(%class.btSoftRigidDynamicsWorld* %this, %class.btSoftBody* %body) #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %body.addr = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody* %body, %class.btSoftBody** %body.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_(%class.btAlignedObjectArray.16* %m_softBodies, %class.btSoftBody** nonnull align 4 dereferenceable(4) %body.addr)
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4, !tbaa !2
  %2 = bitcast %class.btSoftBody* %1 to %class.btCollisionObject*
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %0, %class.btCollisionObject* %2)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_(%class.btAlignedObjectArray.16* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %key) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %key.addr = alloca %class.btSoftBody**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody** %key, %class.btSoftBody*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %key.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.16* %this1, %class.btSoftBody** nonnull align 4 dereferenceable(4) %1)
  store i32 %call, i32* %findIndex, align 4, !tbaa !39
  %2 = load i32, i32* %findIndex, align 4, !tbaa !39
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %findIndex, align 4, !tbaa !39
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %sub = sub nsw i32 %call3, 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii(%class.btAlignedObjectArray.16* %this1, i32 %3, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv(%class.btAlignedObjectArray.16* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = bitcast i32* %findIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  ret void
}

declare void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld*, %class.btCollisionObject*) unnamed_addr #1

define hidden void @_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btSoftRigidDynamicsWorld* %this, %class.btCollisionObject* %collisionObject) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSoftBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %call = call %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %1)
  store %class.btSoftBody* %call, %class.btSoftBody** %body, align 4, !tbaa !2
  %2 = load %class.btSoftBody*, %class.btSoftBody** %body, align 4, !tbaa !2
  %tobool = icmp ne %class.btSoftBody* %2, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %class.btSoftBody*, %class.btSoftBody** %body, align 4, !tbaa !2
  call void @_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody(%class.btSoftRigidDynamicsWorld* %this1, %class.btSoftBody* %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  call void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld* %4, %class.btCollisionObject* %5)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %6 = bitcast %class.btSoftBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  ret void
}

define linkonce_odr hidden %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #0 comdat {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %cmp = icmp eq i32 %call, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !2
  %2 = bitcast %class.btCollisionObject* %1 to %class.btSoftBody*
  store %class.btSoftBody* %2, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %3
}

declare void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*) unnamed_addr #1

define hidden void @_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  call void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld* %0)
  %1 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %2 = bitcast %class.btCollisionWorld* %1 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %2, align 4, !tbaa !6
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call = call %class.btIDebugDraw* %3(%class.btCollisionWorld* %1)
  %tobool = icmp ne %class.btIDebugDraw* %call, null
  br i1 %tobool, label %if.then, label %if.end41

if.then:                                          ; preds = %entry
  %4 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  store i32 0, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %m_softBodies)
  %cmp = icmp slt i32 %5, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %m_softBodies3 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %7 = load i32, i32* %i, align 4, !tbaa !39
  %call4 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.16* %m_softBodies3, i32 %7)
  %8 = load %class.btSoftBody*, %class.btSoftBody** %call4, align 4, !tbaa !2
  store %class.btSoftBody* %8, %class.btSoftBody** %psb, align 4, !tbaa !2
  %9 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %10 = bitcast %class.btCollisionWorld* %9 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable5 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %10, align 4, !tbaa !6
  %vfn6 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable5, i64 5
  %11 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn6, align 4
  %call7 = call %class.btIDebugDraw* %11(%class.btCollisionWorld* %9)
  %tobool8 = icmp ne %class.btIDebugDraw* %call7, null
  br i1 %tobool8, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %12 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %13 = bitcast %class.btCollisionWorld* %12 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable9 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %13, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable9, i64 5
  %14 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn10, align 4
  %call11 = call %class.btIDebugDraw* %14(%class.btCollisionWorld* %12)
  %15 = bitcast %class.btIDebugDraw* %call11 to i32 (%class.btIDebugDraw*)***
  %vtable12 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %15, align 4, !tbaa !6
  %vfn13 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable12, i64 12
  %16 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn13, align 4
  %call14 = call i32 %16(%class.btIDebugDraw* %call11)
  %and = and i32 %call14, 1
  %tobool15 = icmp ne i32 %and, 0
  br i1 %tobool15, label %if.then16, label %if.end

if.then16:                                        ; preds = %land.lhs.true
  %17 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %18 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %18, i32 0, i32 5
  %19 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !104
  call void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody* %17, %class.btIDebugDraw* %19)
  %20 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %21 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer17 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %21, i32 0, i32 5
  %22 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer17, align 4, !tbaa !104
  %m_drawFlags = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 2
  %23 = load i32, i32* %m_drawFlags, align 4, !tbaa !21
  call void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody* %20, %class.btIDebugDraw* %22, i32 %23)
  br label %if.end

if.end:                                           ; preds = %if.then16, %land.lhs.true, %for.body
  %24 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer18 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %24, i32 0, i32 5
  %25 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer18, align 4, !tbaa !104
  %tobool19 = icmp ne %class.btIDebugDraw* %25, null
  br i1 %tobool19, label %land.lhs.true20, label %if.end40

land.lhs.true20:                                  ; preds = %if.end
  %26 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer21 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %26, i32 0, i32 5
  %27 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer21, align 4, !tbaa !104
  %28 = bitcast %class.btIDebugDraw* %27 to i32 (%class.btIDebugDraw*)***
  %vtable22 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %28, align 4, !tbaa !6
  %vfn23 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable22, i64 12
  %29 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn23, align 4
  %call24 = call i32 %29(%class.btIDebugDraw* %27)
  %and25 = and i32 %call24, 2
  %tobool26 = icmp ne i32 %and25, 0
  br i1 %tobool26, label %if.then27, label %if.end40

if.then27:                                        ; preds = %land.lhs.true20
  %m_drawNodeTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 3
  %30 = load i8, i8* %m_drawNodeTree, align 4, !tbaa !22, !range !48
  %tobool28 = trunc i8 %30 to i1
  br i1 %tobool28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.then27
  %31 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %32 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer30 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %32, i32 0, i32 5
  %33 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer30, align 4, !tbaa !104
  call void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %31, %class.btIDebugDraw* %33, i32 0, i32 -1)
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.then27
  %m_drawFaceTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 4
  %34 = load i8, i8* %m_drawFaceTree, align 1, !tbaa !23, !range !48
  %tobool32 = trunc i8 %34 to i1
  br i1 %tobool32, label %if.then33, label %if.end35

if.then33:                                        ; preds = %if.end31
  %35 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %36 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer34 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %36, i32 0, i32 5
  %37 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer34, align 4, !tbaa !104
  call void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %35, %class.btIDebugDraw* %37, i32 0, i32 -1)
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %if.end31
  %m_drawClusterTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 5
  %38 = load i8, i8* %m_drawClusterTree, align 2, !tbaa !24, !range !48
  %tobool36 = trunc i8 %38 to i1
  br i1 %tobool36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end35
  %39 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %40 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer38 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %40, i32 0, i32 5
  %41 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer38, align 4, !tbaa !104
  call void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %39, %class.btIDebugDraw* %41, i32 0, i32 -1)
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end35
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %land.lhs.true20, %if.end
  %42 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end40
  %43 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %43, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %44) #10
  br label %if.end41

if.end41:                                         ; preds = %for.end, %entry
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody*, %class.btIDebugDraw*) #1

declare void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody*, %class.btIDebugDraw*, i32) #1

declare void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #1

declare void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #1

declare void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #1

define hidden void @_ZNK24btSoftRigidDynamicsWorld7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE(%class.btSoftRigidDynamicsWorld* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %resultCallback) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %rayCB = alloca %struct.btSoftSingleRayCallback, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %0) #10
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC2EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0))
  %1 = bitcast %struct.btSoftSingleRayCallback* %rayCB to i8*
  call void @llvm.lifetime.start.p0i8(i64 220, i8* %1) #10
  %2 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  %4 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %call2 = call %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE(%struct.btSoftSingleRayCallback* %rayCB, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btSoftRigidDynamicsWorld* %this1, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %4)
  %5 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %5, i32 0, i32 4
  %6 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4, !tbaa !109
  %7 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  %9 = bitcast %struct.btSoftSingleRayCallback* %rayCB to %struct.btBroadphaseRayCallback*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #10
  %11 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %12 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !30
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %14 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #10
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !30
  %16 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  store float 0.000000e+00, float* %ref.tmp9, align 4, !tbaa !30
  %17 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !30
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %18 = bitcast %class.btBroadphaseInterface* %6 to void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %18, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %19 = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %19(%class.btBroadphaseInterface* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %20 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #10
  %21 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %22 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #10
  %24 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  %25 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  %27 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #10
  %call12 = call %struct.btSoftSingleRayCallback* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to %struct.btSoftSingleRayCallback* (%struct.btSoftSingleRayCallback*)*)(%struct.btSoftSingleRayCallback* %rayCB) #10
  %28 = bitcast %struct.btSoftSingleRayCallback* %rayCB to i8*
  call void @llvm.lifetime.end.p0i8(i64 220, i8* %28) #10
  %call13 = call %class.CProfileSample* @_ZN14CProfileSampleD2Ev(%class.CProfileSample* %__profile) #10
  %29 = bitcast %class.CProfileSample* %__profile to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %29) #10
  ret void
}

define linkonce_odr hidden %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE(%struct.btSoftSingleRayCallback* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld, %class.btSoftRigidDynamicsWorld* %world, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %resultCallback) unnamed_addr #0 comdat {
entry:
  %retval = alloca %struct.btSoftSingleRayCallback*, align 4
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  %world.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %rayDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  store %class.btSoftRigidDynamicsWorld* %world, %class.btSoftRigidDynamicsWorld** %world.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  store %struct.btSoftSingleRayCallback* %this1, %struct.btSoftSingleRayCallback** %retval, align 4
  %0 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %call = call %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackC2Ev(%struct.btBroadphaseRayCallback* %0)
  %1 = bitcast %struct.btSoftSingleRayCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btSoftSingleRayCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_rayFromWorld = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %m_rayFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !31
  %m_rayToWorld = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %m_rayToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !31
  %m_rayFromTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rayFromTrans)
  %m_rayToTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rayToTrans)
  %m_hitNormal = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormal)
  %m_world = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 6
  %8 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %world.addr, align 4, !tbaa !2
  store %class.btSoftRigidDynamicsWorld* %8, %class.btSoftRigidDynamicsWorld** %m_world, align 4, !tbaa !110
  %m_resultCallback = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %9 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::RayResultCallback"* %9, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback, align 4, !tbaa !2
  %m_rayFromTrans5 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_rayFromTrans5)
  %m_rayFromTrans6 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %m_rayFromWorld7 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rayFromTrans6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld7)
  %m_rayToTrans8 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_rayToTrans8)
  %m_rayToTrans9 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %m_rayToWorld10 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rayToTrans9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld10)
  %10 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !2
  %12 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDir)
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx = getelementptr inbounds float, float* %call12, i32 0
  %13 = load float, float* %arrayidx, align 4, !tbaa !30
  %cmp = fcmp oeq float %13, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 0
  %14 = load float, float* %arrayidx14, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x46293E5940000000, %cond.true ], [ %div, %cond.false ]
  %15 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %15, i32 0, i32 1
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 0
  store float %cond, float* %arrayidx16, align 4, !tbaa !30
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %16 = load float, float* %arrayidx18, align 4, !tbaa !30
  %cmp19 = fcmp oeq float %16, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end
  br label %cond.end25

cond.false21:                                     ; preds = %cond.end
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %17 = load float, float* %arrayidx23, align 4, !tbaa !30
  %div24 = fdiv float 1.000000e+00, %17
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false21, %cond.true20
  %cond26 = phi float [ 0x46293E5940000000, %cond.true20 ], [ %div24, %cond.false21 ]
  %18 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse27 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %18, i32 0, i32 1
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse27)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  store float %cond26, float* %arrayidx29, align 4, !tbaa !30
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %19 = load float, float* %arrayidx31, align 4, !tbaa !30
  %cmp32 = fcmp oeq float %19, 0.000000e+00
  br i1 %cmp32, label %cond.true33, label %cond.false34

cond.true33:                                      ; preds = %cond.end25
  br label %cond.end38

cond.false34:                                     ; preds = %cond.end25
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %20 = load float, float* %arrayidx36, align 4, !tbaa !30
  %div37 = fdiv float 1.000000e+00, %20
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false34, %cond.true33
  %cond39 = phi float [ 0x46293E5940000000, %cond.true33 ], [ %div37, %cond.false34 ]
  %21 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse40 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %21, i32 0, i32 1
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse40)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 2
  store float %cond39, float* %arrayidx42, align 4, !tbaa !30
  %22 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse43 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %22, i32 0, i32 1
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse43)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 0
  %23 = load float, float* %arrayidx45, align 4, !tbaa !30
  %conv = fpext float %23 to double
  %cmp46 = fcmp olt double %conv, 0.000000e+00
  %conv47 = zext i1 %cmp46 to i32
  %24 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %24, i32 0, i32 2
  %arrayidx48 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs, i32 0, i32 0
  store i32 %conv47, i32* %arrayidx48, align 4, !tbaa !39
  %25 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse49 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %25, i32 0, i32 1
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %26 = load float, float* %arrayidx51, align 4, !tbaa !30
  %conv52 = fpext float %26 to double
  %cmp53 = fcmp olt double %conv52, 0.000000e+00
  %conv54 = zext i1 %cmp53 to i32
  %27 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs55 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %27, i32 0, i32 2
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs55, i32 0, i32 1
  store i32 %conv54, i32* %arrayidx56, align 4, !tbaa !39
  %28 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse57 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %28, i32 0, i32 1
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %29 = load float, float* %arrayidx59, align 4, !tbaa !30
  %conv60 = fpext float %29 to double
  %cmp61 = fcmp olt double %conv60, 0.000000e+00
  %conv62 = zext i1 %cmp61 to i32
  %30 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs63 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %30, i32 0, i32 2
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs63, i32 0, i32 2
  store i32 %conv62, i32* %arrayidx64, align 4, !tbaa !39
  %31 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %31) #10
  %m_rayToWorld65 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  %m_rayFromWorld66 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld66)
  %call67 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %32 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_lambda_max = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %32, i32 0, i32 3
  store float %call67, float* %m_lambda_max, align 4, !tbaa !112
  %33 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #10
  %34 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #10
  %35 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %retval, align 4
  ret %struct.btSoftSingleRayCallback* %35
}

define hidden void @_ZN24btSoftRigidDynamicsWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RN16btCollisionWorld17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %rayFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %rayToTrans, %class.btCollisionObject* %collisionObject, %class.btCollisionShape* %collisionShape, %class.btTransform* nonnull align 4 dereferenceable(64) %colObjWorldTransform, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %resultCallback) #0 {
entry:
  %rayFromTrans.addr = alloca %class.btTransform*, align 4
  %rayToTrans.addr = alloca %class.btTransform*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %colObjWorldTransform.addr = alloca %class.btTransform*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %softBody = alloca %class.btSoftBody*, align 4
  %softResult = alloca %"struct.btSoftBody::sRayCast", align 4
  %shapeInfo = alloca %"struct.btCollisionWorld::LocalShapeInfo", align 4
  %rayDir = alloca %class.btVector3, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %rayResult = alloca %"struct.btCollisionWorld::LocalRayResult", align 4
  %normalInWorldSpace = alloca i8, align 1
  store %class.btTransform* %rayFromTrans, %class.btTransform** %rayFromTrans.addr, align 4, !tbaa !2
  store %class.btTransform* %rayToTrans, %class.btTransform** %rayToTrans.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  store %class.btTransform* %colObjWorldTransform, %class.btTransform** %colObjWorldTransform.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %call = call zeroext i1 @_ZNK16btCollisionShape10isSoftBodyEv(%class.btCollisionShape* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btSoftBody** %softBody to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %call1 = call %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %2)
  store %class.btSoftBody* %call1, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %3 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %tobool = icmp ne %class.btSoftBody* %3, null
  br i1 %tobool, label %if.then2, label %if.end25

if.then2:                                         ; preds = %if.then
  %4 = bitcast %"struct.btSoftBody::sRayCast"* %softResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #10
  %5 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %6 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %6)
  %7 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %7)
  %call5 = call zeroext i1 @_ZN10btSoftBody7rayTestERK9btVector3S2_RNS_8sRayCastE(%class.btSoftBody* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %"struct.btSoftBody::sRayCast"* nonnull align 4 dereferenceable(16) %softResult)
  br i1 %call5, label %if.then6, label %if.end24

if.then6:                                         ; preds = %if.then2
  %fraction = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 3
  %8 = load float, float* %fraction, align 4, !tbaa !114
  %9 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %9, i32 0, i32 1
  %10 = load float, float* %m_closestHitFraction, align 4, !tbaa !117
  %cmp = fcmp ole float %8, %10
  br i1 %cmp, label %if.then7, label %if.end23

if.then7:                                         ; preds = %if.then6
  %11 = bitcast %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %11) #10
  %m_shapePart = getelementptr inbounds %"struct.btCollisionWorld::LocalShapeInfo", %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, i32 0, i32 0
  store i32 0, i32* %m_shapePart, align 4, !tbaa !119
  %index = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 2
  %12 = load i32, i32* %index, align 4, !tbaa !121
  %m_triangleIndex = getelementptr inbounds %"struct.btCollisionWorld::LocalShapeInfo", %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, i32 0, i32 1
  store i32 %12, i32* %m_triangleIndex, align 4, !tbaa !122
  %13 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #10
  %14 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4, !tbaa !2
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %14)
  %15 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %15)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  %16 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #10
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDir)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normal)
  %feature = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 1
  %17 = load i32, i32* %feature, align 4, !tbaa !123
  %cmp11 = icmp eq i32 %17, 3
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %if.then7
  %18 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4, !tbaa !2
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %18, i32 0, i32 11
  %index13 = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 2
  %19 = load i32, i32* %index13, align 4, !tbaa !121
  %call14 = call nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.56* %m_faces, i32 %19)
  %m_normal = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %call14, i32 0, i32 2
  %20 = bitcast %class.btVector3* %normal to i8*
  %21 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !31
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDir)
  %cmp16 = fcmp ogt float %call15, 0.000000e+00
  br i1 %cmp16, label %if.then17, label %if.end

if.then17:                                        ; preds = %if.then12
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #10
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %23 = bitcast %class.btVector3* %normal to i8*
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !31
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #10
  br label %if.end

if.end:                                           ; preds = %if.then17, %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then7
  %26 = bitcast %"struct.btCollisionWorld::LocalRayResult"* %rayResult to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %26) #10
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %fraction19 = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 3
  %28 = load float, float* %fraction19, align 4, !tbaa !114
  %call20 = call %"struct.btCollisionWorld::LocalRayResult"* @_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f(%"struct.btCollisionWorld::LocalRayResult"* %rayResult, %class.btCollisionObject* %27, %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %28)
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %normalInWorldSpace) #10
  store i8 1, i8* %normalInWorldSpace, align 1, !tbaa !124
  %29 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  %30 = load i8, i8* %normalInWorldSpace, align 1, !tbaa !124, !range !48
  %tobool21 = trunc i8 %30 to i1
  %31 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %29 to float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)***
  %vtable = load float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)**, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*** %31, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)** %vtable, i64 3
  %32 = load float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)** %vfn, align 4
  %call22 = call float %32(%"struct.btCollisionWorld::RayResultCallback"* %29, %"struct.btCollisionWorld::LocalRayResult"* nonnull align 4 dereferenceable(28) %rayResult, i1 zeroext %tobool21)
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %normalInWorldSpace) #10
  %33 = bitcast %"struct.btCollisionWorld::LocalRayResult"* %rayResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %33) #10
  %34 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #10
  %35 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #10
  %36 = bitcast %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %36) #10
  br label %if.end23

if.end23:                                         ; preds = %if.end18, %if.then6
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then2
  %37 = bitcast %"struct.btSoftBody::sRayCast"* %softResult to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #10
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then
  %38 = bitcast %class.btSoftBody** %softBody to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #10
  br label %if.end26

if.else:                                          ; preds = %entry
  %39 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4, !tbaa !2
  %40 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4, !tbaa !2
  %41 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  %42 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4, !tbaa !2
  %43 = load %class.btTransform*, %class.btTransform** %colObjWorldTransform.addr, align 4, !tbaa !2
  %44 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4, !tbaa !2
  call void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %39, %class.btTransform* nonnull align 4 dereferenceable(64) %40, %class.btCollisionObject* %41, %class.btCollisionShape* %42, %class.btTransform* nonnull align 4 dereferenceable(64) %43, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %44)
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.end25
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isSoftBodyEv(%class.btCollisionShape* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isSoftBodyEi(i32 %call)
  ret i1 %call2
}

declare zeroext i1 @_ZN10btSoftBody7rayTestERK9btVector3S2_RNS_8sRayCastE(%class.btSoftBody*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btSoftBody::sRayCast"* nonnull align 4 dereferenceable(16)) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !30
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !30
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !30
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !30
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !30
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !30
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #6 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !30
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !30
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !30
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !30
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #10
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.56* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.56*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.56* %this, %class.btAlignedObjectArray.56** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.56*, %class.btAlignedObjectArray.56** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.56, %class.btAlignedObjectArray.56* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %m_data, align 4, !tbaa !125
  %1 = load i32, i32* %n.addr, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %0, i32 %1
  ret %"struct.btSoftBody::Face"* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !30
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !30
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !30
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !30
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !30
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btCollisionWorld::LocalRayResult"* @_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f(%"struct.btCollisionWorld::LocalRayResult"* returned %this, %class.btCollisionObject* %collisionObject, %"struct.btCollisionWorld::LocalShapeInfo"* %localShapeInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormalLocal, float %hitFraction) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::LocalRayResult"*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %localShapeInfo.addr = alloca %"struct.btCollisionWorld::LocalShapeInfo"*, align 4
  %hitNormalLocal.addr = alloca %class.btVector3*, align 4
  %hitFraction.addr = alloca float, align 4
  store %"struct.btCollisionWorld::LocalRayResult"* %this, %"struct.btCollisionWorld::LocalRayResult"** %this.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::LocalShapeInfo"* %localShapeInfo, %"struct.btCollisionWorld::LocalShapeInfo"** %localShapeInfo.addr, align 4, !tbaa !2
  store %class.btVector3* %hitNormalLocal, %class.btVector3** %hitNormalLocal.addr, align 4, !tbaa !2
  store float %hitFraction, float* %hitFraction.addr, align 4, !tbaa !30
  %this1 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 0
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4, !tbaa !2
  store %class.btCollisionObject* %0, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !126
  %m_localShapeInfo = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 1
  %1 = load %"struct.btCollisionWorld::LocalShapeInfo"*, %"struct.btCollisionWorld::LocalShapeInfo"** %localShapeInfo.addr, align 4, !tbaa !2
  store %"struct.btCollisionWorld::LocalShapeInfo"* %1, %"struct.btCollisionWorld::LocalShapeInfo"** %m_localShapeInfo, align 4, !tbaa !128
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %hitNormalLocal.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !31
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 3
  %5 = load float, float* %hitFraction.addr, align 4, !tbaa !30
  store float %5, float* %m_hitFraction, align 4, !tbaa !129
  ret %"struct.btCollisionWorld::LocalRayResult"* %this1
}

declare void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionObject*, %class.btCollisionShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20)) #1

define hidden void @_ZN24btSoftRigidDynamicsWorld19serializeSoftBodiesEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this, %class.btSerializer* %serializer) #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store i32 0, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !39
  %2 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %4, i32 0, i32 1
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %5)
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4, !tbaa !2
  store %class.btCollisionObject* %6, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %call4 = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %7)
  %and = and i32 %call4, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = bitcast i32* %len to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %10 = bitcast %class.btCollisionObject* %9 to i32 (%class.btCollisionObject*)***
  %vtable = load i32 (%class.btCollisionObject*)**, i32 (%class.btCollisionObject*)*** %10, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vtable, i64 4
  %11 = load i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vfn, align 4
  %call5 = call i32 %11(%class.btCollisionObject* %9)
  store i32 %call5, i32* %len, align 4, !tbaa !39
  %12 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %13 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %14 = load i32, i32* %len, align 4, !tbaa !39
  %15 = bitcast %class.btSerializer* %13 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable6 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %15, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable6, i64 4
  %16 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn7, align 4
  %call8 = call %class.btChunk* %16(%class.btSerializer* %13, i32 %14, i32 1)
  store %class.btChunk* %call8, %class.btChunk** %chunk, align 4, !tbaa !2
  %17 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %19 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %19, i32 0, i32 2
  %20 = load i8*, i8** %m_oldPtr, align 4, !tbaa !130
  %21 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %22 = bitcast %class.btCollisionObject* %18 to i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)***
  %vtable9 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*** %22, align 4, !tbaa !6
  %vfn10 = getelementptr inbounds i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vtable9, i64 5
  %23 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vfn10, align 4
  %call11 = call i8* %23(%class.btCollisionObject* %18, i8* %20, %class.btSerializer* %21)
  store i8* %call11, i8** %structType, align 4, !tbaa !2
  %24 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %25 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %26 = load i8*, i8** %structType, align 4, !tbaa !2
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4, !tbaa !2
  %28 = bitcast %class.btCollisionObject* %27 to i8*
  %29 = bitcast %class.btSerializer* %24 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable12 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %29, align 4, !tbaa !6
  %vfn13 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable12, i64 5
  %30 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn13, align 4
  call void %30(%class.btSerializer* %24, %class.btChunk* %25, i8* %26, i32 1497645651, i8* %28)
  %31 = bitcast i8** %structType to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #10
  %32 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #10
  %33 = bitcast i32* %len to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %34 = bitcast %class.btCollisionObject** %colObj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %35 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !132
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4, !tbaa !133
  %1 = load i32, i32* %n.addr, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %0 = load i32, i32* %m_internalType, align 4, !tbaa !134
  ret i32 %0
}

define hidden void @_ZN24btSoftRigidDynamicsWorld9serializeEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %1 = bitcast %class.btSerializer* %0 to void (%class.btSerializer*)***
  %vtable = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable, i64 8
  %2 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn, align 4
  call void %2(%class.btSerializer* %0)
  %3 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %4 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  call void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld* %3, %class.btSerializer* %4)
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  call void @_ZN24btSoftRigidDynamicsWorld19serializeSoftBodiesEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this1, %class.btSerializer* %5)
  %6 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %7 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  call void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld* %6, %class.btSerializer* %7)
  %8 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  call void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld* %8, %class.btSerializer* %9)
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %11 = bitcast %class.btSerializer* %10 to void (%class.btSerializer*)***
  %vtable2 = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %11, align 4, !tbaa !6
  %vfn3 = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable2, i64 9
  %12 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn3, align 4
  call void %12(%class.btSerializer* %10)
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #1

declare void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #1

declare void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) #1

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #1

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !2
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !104
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4, !tbaa !104
  ret %class.btIDebugDraw* %0
}

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i16 signext, i16 signext) unnamed_addr #1

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #1

declare i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld*, float, i32, float) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1 zeroext) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #1

declare void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* sret align 4, %class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i16 signext, i16 signext) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*) unnamed_addr #1

declare %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #1

declare %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  ret i32 4
}

declare void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo(%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84)) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #1

declare void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #1

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  store i32 %numTasks, i32* %numTasks.addr, align 4, !tbaa !39
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !30
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4, !tbaa !30
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btSparseSdf* @_ZN11btSparseSdfILi3EEC2Ev(%struct.btSparseSdf* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.39* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev(%class.btAlignedObjectArray.39* %cells)
  ret %struct.btSparseSdf* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.39* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev(%class.btAlignedObjectArray.39* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.40* @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev(%class.btAlignedAllocator.40* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.39* %this1)
  ret %class.btAlignedObjectArray.39* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.40* @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev(%class.btAlignedAllocator.40* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.40*, align 4
  store %class.btAlignedAllocator.40* %this, %class.btAlignedAllocator.40** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.40*, %class.btAlignedAllocator.40** %this.addr, align 4
  ret %class.btAlignedAllocator.40* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.39* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !136
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** null, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !138
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !139
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btSparseSdf* @_ZN11btSparseSdfILi3EED2Ev(%struct.btSparseSdf* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.39* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev(%class.btAlignedObjectArray.39* %cells) #10
  ret %struct.btSparseSdf* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.39* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev(%class.btAlignedObjectArray.39* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv(%class.btAlignedObjectArray.39* %this1)
  ret %class.btAlignedObjectArray.39* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv(%class.btAlignedObjectArray.39* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.39* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.39* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.39* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.39* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !39
  store i32 %last, i32* %last.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !39
  store i32 %1, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %3 = load i32, i32* %last.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %4 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !138
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.39* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %tobool = icmp ne %"struct.btSparseSdf<3>::Cell"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !136, !range !48
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %2 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data4, align 4, !tbaa !137
  call void @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_(%class.btAlignedAllocator.40* %m_allocator, %"struct.btSparseSdf<3>::Cell"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** null, %"struct.btSparseSdf<3>::Cell"*** %m_data5, align 4, !tbaa !137
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_(%class.btAlignedAllocator.40* %this, %"struct.btSparseSdf<3>::Cell"** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.40*, align 4
  %ptr.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  store %class.btAlignedAllocator.40* %this, %class.btAlignedAllocator.40** %this.addr, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"** %ptr, %"struct.btSparseSdf<3>::Cell"*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.40*, %class.btAlignedAllocator.40** %this.addr, align 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %"struct.btSparseSdf<3>::Cell"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_ZN15CProfileManager13Start_ProfileEPKc(i8*) #1

declare void @_ZN15CProfileManager12Stop_ProfileEv() #1

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackC2Ev(%struct.btBroadphaseRayCallback* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.btBroadphaseRayCallback* %this, %struct.btBroadphaseRayCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseRayCallback* %this1 to %struct.btBroadphaseAabbCallback*
  %call = call %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackC2Ev(%struct.btBroadphaseAabbCallback* %0) #10
  %1 = bitcast %struct.btBroadphaseRayCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btBroadphaseRayCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_rayDirectionInverse = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_rayDirectionInverse)
  ret %struct.btBroadphaseRayCallback* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #10
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !2
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !31
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN23btSoftSingleRayCallbackD0Ev(%struct.btSoftSingleRayCallback* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %call = call %struct.btSoftSingleRayCallback* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to %struct.btSoftSingleRayCallback* (%struct.btSoftSingleRayCallback*)*)(%struct.btSoftSingleRayCallback* %this1) #10
  %0 = bitcast %struct.btSoftSingleRayCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

define linkonce_odr hidden zeroext i1 @_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy(%struct.btSoftSingleRayCallback* %this, %struct.btBroadphaseProxy* %proxy) unnamed_addr #0 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %m_resultCallback = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %0 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback, align 4, !tbaa !140
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %0, i32 0, i32 1
  %1 = load float, float* %m_closestHitFraction, align 4, !tbaa !117
  %cmp = fcmp oeq float %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 0
  %4 = load i8*, i8** %m_clientObject, align 4, !tbaa !141
  %5 = bitcast i8* %4 to %class.btCollisionObject*
  store %class.btCollisionObject* %5, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %m_resultCallback2 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %6 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback2, align 4, !tbaa !140
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %7)
  %8 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %6 to i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)**, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*** %8, align 4, !tbaa !6
  %vfn = getelementptr inbounds i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %9 = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call3 = call zeroext i1 %9(%"struct.btCollisionWorld::RayResultCallback"* %6, %struct.btBroadphaseProxy* %call)
  br i1 %call3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %if.end
  %m_world = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 6
  %10 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %m_world, align 4, !tbaa !110
  %m_rayFromTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %m_rayToTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call5 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %12)
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %13)
  %m_resultCallback7 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %14 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback7, align 4, !tbaa !140
  call void @_ZN24btSoftRigidDynamicsWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RN16btCollisionWorld17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %m_rayFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rayToTrans, %class.btCollisionObject* %11, %class.btCollisionShape* %call5, %class.btTransform* nonnull align 4 dereferenceable(64) %call6, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %14)
  br label %if.end8

if.end8:                                          ; preds = %if.then4, %if.end
  store i1 true, i1* %retval, align 1
  %15 = bitcast %class.btCollisionObject** %collisionObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  br label %return

return:                                           ; preds = %if.end8, %if.then
  %16 = load i1, i1* %retval, align 1
  ret i1 %16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackC2Ev(%struct.btBroadphaseAabbCallback* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseAabbCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV24btBroadphaseAabbCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %struct.btBroadphaseAabbCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN23btBroadphaseRayCallbackD0Ev(%struct.btBroadphaseRayCallback* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.btBroadphaseRayCallback* %this, %struct.btBroadphaseRayCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackD2Ev(%struct.btBroadphaseAabbCallback* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  ret %struct.btBroadphaseAabbCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN24btBroadphaseAabbCallbackD0Ev(%struct.btBroadphaseAabbCallback* %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !30
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !30
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !30
  %3 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !30
  %4 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  store float 1.000000e+00, float* %ref.tmp5, align 4, !tbaa !30
  %5 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !30
  %6 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !30
  %7 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !30
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !30
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %9 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #10
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #10
  %11 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  %15 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4, !tbaa !143
  ret %struct.btBroadphaseProxy* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4, !tbaa !144
  ret %class.btCollisionShape* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isSoftBodyEi(i32 %proxyType) #3 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4, !tbaa !39
  %0 = load i32, i32* %proxyType.addr, align 4, !tbaa !39
  %cmp = icmp eq i32 %0, 32
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4, !tbaa !145
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !30
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !30
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #10
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !30
  %0 = load float, float* %y.addr, align 4, !tbaa !30
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !30
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !30
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !30
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !30
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !30
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !30
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !30
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !30
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !30
  ret %class.btVector3* %this1
}

declare void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld*, float) #1

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator.17* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  ret %class.btAlignedAllocator.17* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !147
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !49
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !148
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !39
  store i32 %last, i32* %last.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !39
  store i32 %1, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %3 = load i32, i32* %last.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %tobool = icmp ne %class.btSoftBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !147, !range !48
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4, !tbaa !50
  call void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %m_allocator, %class.btSoftBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data5, align 4, !tbaa !50
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.17* %this, %class.btSoftBody** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %class.btSoftBody**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody** %ptr, %class.btSoftBody*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btSoftBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_(%class.btAlignedObjectArray.39* %this, i32 %newsize, %"struct.btSparseSdf<3>::Cell"** nonnull align 4 dereferenceable(4) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !39
  store %"struct.btSparseSdf<3>::Cell"** %fillData, %"struct.btSparseSdf<3>::Cell"*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !39
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  %2 = load i32, i32* %curSize, align 4, !tbaa !39
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  store i32 %4, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %6 = load i32, i32* %curSize, align 4, !tbaa !39
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %8 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %9 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi(%class.btAlignedObjectArray.39* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !39
  store i32 %14, i32* %i6, align 4, !tbaa !39
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !39
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %18 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data11, align 4, !tbaa !137
  %19 = load i32, i32* %i6, align 4, !tbaa !39
  %arrayidx12 = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %18, i32 %19
  %20 = bitcast %"struct.btSparseSdf<3>::Cell"** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %"struct.btSparseSdf<3>::Cell"**
  %22 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %fillData.addr, align 4, !tbaa !2
  %23 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %22, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %23, %"struct.btSparseSdf<3>::Cell"** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !39
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !39
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !39
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !138
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi(%class.btAlignedObjectArray.39* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv(%class.btAlignedObjectArray.39* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %"struct.btSparseSdf<3>::Cell"*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi(%class.btAlignedObjectArray.39* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %"struct.btSparseSdf<3>::Cell"**
  store %"struct.btSparseSdf<3>::Cell"** %3, %"struct.btSparseSdf<3>::Cell"*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this1)
  %4 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_(%class.btAlignedObjectArray.39* %this1, i32 0, i32 %call3, %"struct.btSparseSdf<3>::Cell"** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.39* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.39* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.39* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !136
  %5 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** %5, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !139
  %7 = bitcast %"struct.btSparseSdf<3>::Cell"*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv(%class.btAlignedObjectArray.39* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !139
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi(%class.btAlignedObjectArray.39* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !39
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !39
  %call = call %"struct.btSparseSdf<3>::Cell"** @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_(%class.btAlignedAllocator.40* %m_allocator, i32 %1, %"struct.btSparseSdf<3>::Cell"*** null)
  %2 = bitcast %"struct.btSparseSdf<3>::Cell"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_(%class.btAlignedObjectArray.39* %this, i32 %start, i32 %end, %"struct.btSparseSdf<3>::Cell"** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !39
  store i32 %end, i32* %end.addr, align 4, !tbaa !39
  store %"struct.btSparseSdf<3>::Cell"** %dest, %"struct.btSparseSdf<3>::Cell"*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !39
  store i32 %1, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %3 = load i32, i32* %end.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %4, i32 %5
  %6 = bitcast %"struct.btSparseSdf<3>::Cell"** %arrayidx to i8*
  %7 = bitcast i8* %6 to %"struct.btSparseSdf<3>::Cell"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %8 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %9 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx2 = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %8, i32 %9
  %10 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %arrayidx2, align 4, !tbaa !2
  store %"struct.btSparseSdf<3>::Cell"* %10, %"struct.btSparseSdf<3>::Cell"** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %"struct.btSparseSdf<3>::Cell"** @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_(%class.btAlignedAllocator.40* %this, i32 %n, %"struct.btSparseSdf<3>::Cell"*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.40*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btSparseSdf<3>::Cell"***, align 4
  store %class.btAlignedAllocator.40* %this, %class.btAlignedAllocator.40** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  store %"struct.btSparseSdf<3>::Cell"*** %hint, %"struct.btSparseSdf<3>::Cell"**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.40*, %class.btAlignedAllocator.40** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !39
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btSparseSdf<3>::Cell"**
  ret %"struct.btSparseSdf<3>::Cell"** %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.39* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.39*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.39* %this, %class.btAlignedObjectArray.39** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.39*, %class.btAlignedObjectArray.39** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.39, %class.btAlignedObjectArray.39* %this1, i32 0, i32 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4, !tbaa !137
  %1 = load i32, i32* %n.addr, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %0, i32 %1
  ret %"struct.btSparseSdf<3>::Cell"** %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !148
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btSoftBody**, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btSoftBody*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btSoftBody**
  store %class.btSoftBody** %3, %class.btSoftBody*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %class.btSoftBody** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !147
  %5 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btSoftBody** %5, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !148
  %7 = bitcast %class.btSoftBody*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !39
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !39
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !39
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !39
  %call = call %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %class.btSoftBody*** null)
  %2 = bitcast %class.btSoftBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %class.btSoftBody** %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btSoftBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !39
  store i32 %end, i32* %end.addr, align 4, !tbaa !39
  store %class.btSoftBody** %dest, %class.btSoftBody*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !39
  store i32 %1, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %3 = load i32, i32* %end.addr, align 4, !tbaa !39
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %4, i32 %5
  %6 = bitcast %class.btSoftBody** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btSoftBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %9 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx2 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %8, i32 %9
  %10 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx2, align 4, !tbaa !2
  store %class.btSoftBody* %10, %class.btSoftBody** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

define linkonce_odr hidden %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.17* %this, i32 %n, %class.btSoftBody*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btSoftBody***, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !39
  store %class.btSoftBody*** %hint, %class.btSoftBody**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !39
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btSoftBody**
  ret %class.btSoftBody** %1
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.16* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %key) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %key.addr = alloca %class.btSoftBody**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody** %key, %class.btSoftBody*** %key.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %index to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %index, align 4, !tbaa !39
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  store i32 0, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !39
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp slt i32 %2, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %4 = load i32, i32* %i, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  %5 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx, align 4, !tbaa !2
  %6 = load %class.btSoftBody**, %class.btSoftBody*** %key.addr, align 4, !tbaa !2
  %7 = load %class.btSoftBody*, %class.btSoftBody** %6, align 4, !tbaa !2
  %cmp3 = icmp eq %class.btSoftBody* %5, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4, !tbaa !39
  store i32 %8, i32* %index, align 4, !tbaa !39
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4, !tbaa !39
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4, !tbaa !39
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %10 = load i32, i32* %index, align 4, !tbaa !39
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  %12 = bitcast i32* %index to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret i32 %10
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii(%class.btAlignedObjectArray.16* %this, i32 %index0, i32 %index1) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btSoftBody*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !39
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !39
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast %class.btSoftBody** %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !39
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %1, i32 %2
  %3 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx, align 4, !tbaa !2
  store %class.btSoftBody* %3, %class.btSoftBody** %temp, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %m_data2, align 4, !tbaa !50
  %5 = load i32, i32* %index1.addr, align 4, !tbaa !39
  %arrayidx3 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %4, i32 %5
  %6 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx3, align 4, !tbaa !2
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %7 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4, !tbaa !50
  %8 = load i32, i32* %index0.addr, align 4, !tbaa !39
  %arrayidx5 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %7, i32 %8
  store %class.btSoftBody* %6, %class.btSoftBody** %arrayidx5, align 4, !tbaa !2
  %9 = load %class.btSoftBody*, %class.btSoftBody** %temp, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %10 = load %class.btSoftBody**, %class.btSoftBody*** %m_data6, align 4, !tbaa !50
  %11 = load i32, i32* %index1.addr, align 4, !tbaa !39
  %arrayidx7 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %10, i32 %11
  store %class.btSoftBody* %9, %class.btSoftBody** %arrayidx7, align 4, !tbaa !2
  %12 = bitcast %class.btSoftBody** %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv(%class.btAlignedObjectArray.16* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !49
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4, !tbaa !49
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !50
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %1, i32 %2
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { nounwind }
attributes #11 = { builtin nounwind }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 452}
!9 = !{!"_ZTS24btSoftRigidDynamicsWorld", !10, i64 324, !12, i64 344, !13, i64 348, !13, i64 349, !13, i64 350, !14, i64 352, !3, i64 452, !13, i64 456}
!10 = !{!"_ZTS20btAlignedObjectArrayIP10btSoftBodyE", !11, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!11 = !{!"_ZTS18btAlignedAllocatorIP10btSoftBodyLj16EE"}
!12 = !{!"int", !4, i64 0}
!13 = !{!"bool", !4, i64 0}
!14 = !{!"_ZTS19btSoftBodyWorldInfo", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !16, i64 16, !3, i64 32, !3, i64 36, !16, i64 40, !17, i64 56}
!15 = !{!"float", !4, i64 0}
!16 = !{!"_ZTS9btVector3", !4, i64 0}
!17 = !{!"_ZTS11btSparseSdfILi3EE", !18, i64 0, !15, i64 20, !12, i64 24, !12, i64 28, !12, i64 32, !12, i64 36, !12, i64 40}
!18 = !{!"_ZTS20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE", !19, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE"}
!20 = !{!9, !13, i64 456}
!21 = !{!9, !12, i64 344}
!22 = !{!9, !13, i64 348}
!23 = !{!9, !13, i64 349}
!24 = !{!9, !13, i64 350}
!25 = !{!9, !3, i64 384}
!26 = !{!9, !3, i64 388}
!27 = !{!9, !15, i64 352}
!28 = !{!9, !15, i64 356}
!29 = !{!9, !15, i64 360}
!30 = !{!15, !15, i64 0}
!31 = !{i64 0, i64 16, !32}
!32 = !{!4, !4, i64 0}
!33 = !{!14, !15, i64 0}
!34 = !{!14, !15, i64 4}
!35 = !{!14, !15, i64 8}
!36 = !{!14, !15, i64 12}
!37 = !{!14, !3, i64 32}
!38 = !{!14, !3, i64 36}
!39 = !{!12, !12, i64 0}
!40 = !{!17, !12, i64 32}
!41 = !{!42, !3, i64 280}
!42 = !{!"_ZTSN11btSparseSdfILi3EE4CellE", !4, i64 0, !4, i64 256, !12, i64 268, !12, i64 272, !3, i64 276, !3, i64 280}
!43 = !{!17, !15, i64 20}
!44 = !{!17, !12, i64 24}
!45 = !{!17, !12, i64 28}
!46 = !{!17, !12, i64 36}
!47 = !{!17, !12, i64 40}
!48 = !{i8 0, i8 2}
!49 = !{!10, !12, i64 4}
!50 = !{!10, !3, i64 12}
!51 = !{!52, !15, i64 12}
!52 = !{!"_ZTS16btSoftBodySolver", !12, i64 4, !12, i64 8, !15, i64 12}
!53 = !{!54, !54, i64 0}
!54 = !{!"short", !4, i64 0}
!55 = !{!56, !3, i64 284}
!56 = !{!"_ZTS10btSoftBody", !57, i64 264, !3, i64 284, !59, i64 288, !65, i64 452, !66, i64 472, !3, i64 680, !3, i64 684, !72, i64 688, !74, i64 708, !76, i64 728, !78, i64 748, !80, i64 768, !82, i64 788, !84, i64 808, !86, i64 828, !88, i64 848, !90, i64 868, !15, i64 888, !4, i64 892, !13, i64 924, !92, i64 928, !92, i64 988, !92, i64 1048, !97, i64 1108, !99, i64 1128, !101, i64 1148, !16, i64 1212, !15, i64 1228, !102, i64 1232}
!57 = !{!"_ZTS20btAlignedObjectArrayIPK17btCollisionObjectE", !58, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!58 = !{!"_ZTS18btAlignedAllocatorIPK17btCollisionObjectLj16EE"}
!59 = !{!"_ZTSN10btSoftBody6ConfigE", !60, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16, !15, i64 20, !15, i64 24, !15, i64 28, !15, i64 32, !15, i64 36, !15, i64 40, !15, i64 44, !15, i64 48, !15, i64 52, !15, i64 56, !15, i64 60, !15, i64 64, !15, i64 68, !15, i64 72, !15, i64 76, !15, i64 80, !12, i64 84, !12, i64 88, !12, i64 92, !12, i64 96, !12, i64 100, !61, i64 104, !63, i64 124, !63, i64 144}
!60 = !{!"_ZTSN10btSoftBody10eAeroModel1_E", !4, i64 0}
!61 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8eVSolver1_EE", !62, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!62 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8eVSolver1_ELj16EE"}
!63 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8ePSolver1_EE", !64, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!64 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8ePSolver1_ELj16EE"}
!65 = !{!"_ZTSN10btSoftBody11SolverStateE", !15, i64 0, !15, i64 4, !15, i64 8, !15, i64 12, !15, i64 16}
!66 = !{!"_ZTSN10btSoftBody4PoseE", !13, i64 0, !13, i64 1, !15, i64 4, !67, i64 8, !69, i64 28, !16, i64 48, !71, i64 64, !71, i64 112, !71, i64 160}
!67 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !68, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!68 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!69 = !{!"_ZTS20btAlignedObjectArrayIfE", !70, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!70 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!71 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!72 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NoteEE", !73, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!73 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NoteELj16EE"}
!74 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !75, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!75 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!76 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4LinkEE", !77, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!77 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4LinkELj16EE"}
!78 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4FaceEE", !79, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!79 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4FaceELj16EE"}
!80 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody5TetraEE", !81, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!81 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody5TetraELj16EE"}
!82 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody6AnchorEE", !83, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!83 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody6AnchorELj16EE"}
!84 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8RContactEE", !85, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!85 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8RContactELj16EE"}
!86 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody8SContactEE", !87, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!87 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody8SContactELj16EE"}
!88 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody5JointEE", !89, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!89 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody5JointELj16EE"}
!90 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody8MaterialEE", !91, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!91 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody8MaterialELj16EE"}
!92 = !{!"_ZTS6btDbvt", !3, i64 0, !3, i64 4, !12, i64 8, !12, i64 12, !12, i64 16, !93, i64 20, !95, i64 40}
!93 = !{!"_ZTS20btAlignedObjectArrayIN6btDbvt6sStkNNEE", !94, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!94 = !{!"_ZTS18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE"}
!95 = !{!"_ZTS20btAlignedObjectArrayIPK10btDbvtNodeE", !96, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!96 = !{!"_ZTS18btAlignedAllocatorIPK10btDbvtNodeLj16EE"}
!97 = !{!"_ZTS20btAlignedObjectArrayIPN10btSoftBody7ClusterEE", !98, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!98 = !{!"_ZTS18btAlignedAllocatorIPN10btSoftBody7ClusterELj16EE"}
!99 = !{!"_ZTS20btAlignedObjectArrayIbE", !100, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!100 = !{!"_ZTS18btAlignedAllocatorIbLj16EE"}
!101 = !{!"_ZTS11btTransform", !71, i64 0, !16, i64 48}
!102 = !{!"_ZTS20btAlignedObjectArrayIiE", !103, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!103 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!104 = !{!105, !3, i64 72}
!105 = !{!"_ZTS16btCollisionWorld", !106, i64 4, !3, i64 24, !108, i64 28, !3, i64 68, !3, i64 72, !13, i64 76}
!106 = !{!"_ZTS20btAlignedObjectArrayIP17btCollisionObjectE", !107, i64 0, !12, i64 4, !12, i64 8, !3, i64 12, !13, i64 16}
!107 = !{!"_ZTS18btAlignedAllocatorIP17btCollisionObjectLj16EE"}
!108 = !{!"_ZTS16btDispatcherInfo", !15, i64 0, !12, i64 4, !12, i64 8, !15, i64 12, !13, i64 16, !3, i64 20, !13, i64 24, !13, i64 25, !13, i64 26, !15, i64 28, !13, i64 32, !15, i64 36}
!109 = !{!105, !3, i64 68}
!110 = !{!111, !3, i64 212}
!111 = !{!"_ZTS23btSoftSingleRayCallback", !16, i64 36, !16, i64 52, !101, i64 68, !101, i64 132, !16, i64 196, !3, i64 212, !3, i64 216}
!112 = !{!113, !15, i64 32}
!113 = !{!"_ZTS23btBroadphaseRayCallback", !16, i64 4, !4, i64 20, !15, i64 32}
!114 = !{!115, !15, i64 12}
!115 = !{!"_ZTSN10btSoftBody8sRayCastE", !3, i64 0, !116, i64 4, !12, i64 8, !15, i64 12}
!116 = !{!"_ZTSN10btSoftBody8eFeature1_E", !4, i64 0}
!117 = !{!118, !15, i64 4}
!118 = !{!"_ZTSN16btCollisionWorld17RayResultCallbackE", !15, i64 4, !3, i64 8, !54, i64 12, !54, i64 14, !12, i64 16}
!119 = !{!120, !12, i64 0}
!120 = !{!"_ZTSN16btCollisionWorld14LocalShapeInfoE", !12, i64 0, !12, i64 4}
!121 = !{!115, !12, i64 8}
!122 = !{!120, !12, i64 4}
!123 = !{!115, !116, i64 4}
!124 = !{!13, !13, i64 0}
!125 = !{!78, !3, i64 12}
!126 = !{!127, !3, i64 0}
!127 = !{!"_ZTSN16btCollisionWorld14LocalRayResultE", !3, i64 0, !3, i64 4, !16, i64 8, !15, i64 24}
!128 = !{!127, !3, i64 4}
!129 = !{!127, !15, i64 24}
!130 = !{!131, !3, i64 8}
!131 = !{!"_ZTS7btChunk", !12, i64 0, !12, i64 4, !3, i64 8, !12, i64 12, !12, i64 16}
!132 = !{!106, !12, i64 4}
!133 = !{!106, !3, i64 12}
!134 = !{!135, !12, i64 236}
!135 = !{!"_ZTS17btCollisionObject", !101, i64 4, !101, i64 68, !16, i64 132, !16, i64 148, !16, i64 164, !12, i64 180, !15, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !12, i64 204, !12, i64 208, !12, i64 212, !12, i64 216, !15, i64 220, !15, i64 224, !15, i64 228, !15, i64 232, !12, i64 236, !4, i64 240, !15, i64 244, !15, i64 248, !15, i64 252, !12, i64 256, !12, i64 260}
!136 = !{!18, !13, i64 16}
!137 = !{!18, !3, i64 12}
!138 = !{!18, !12, i64 4}
!139 = !{!18, !12, i64 8}
!140 = !{!111, !3, i64 216}
!141 = !{!142, !3, i64 0}
!142 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !54, i64 4, !54, i64 6, !3, i64 8, !12, i64 12, !16, i64 16, !16, i64 32}
!143 = !{!135, !3, i64 188}
!144 = !{!135, !3, i64 192}
!145 = !{!146, !12, i64 4}
!146 = !{!"_ZTS16btCollisionShape", !12, i64 4, !3, i64 8}
!147 = !{!10, !13, i64 16}
!148 = !{!10, !12, i64 8}
