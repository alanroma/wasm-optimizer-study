; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_memory.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/Gimpact/gim_memory.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

$_Z15gim_simd_memcpyPvPKvm = comdat any

@_ZL9g_allocfn = internal global i8* (i32)* null, align 4
@_ZL10g_allocafn = internal global i8* (i32)* null, align 4
@_ZL11g_reallocfn = internal global i8* (i8*, i32, i32)* null, align 4
@_ZL8g_freefn = internal global void (i8*)* null, align 4

; Function Attrs: nounwind
define hidden void @_Z21gim_set_alloc_handlerPFPvmE(i8* (i32)* %fn) #0 {
entry:
  %fn.addr = alloca i8* (i32)*, align 4
  store i8* (i32)* %fn, i8* (i32)** %fn.addr, align 4, !tbaa !2
  %0 = load i8* (i32)*, i8* (i32)** %fn.addr, align 4, !tbaa !2
  store i8* (i32)* %0, i8* (i32)** @_ZL9g_allocfn, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z22gim_set_alloca_handlerPFPvmE(i8* (i32)* %fn) #0 {
entry:
  %fn.addr = alloca i8* (i32)*, align 4
  store i8* (i32)* %fn, i8* (i32)** %fn.addr, align 4, !tbaa !2
  %0 = load i8* (i32)*, i8* (i32)** %fn.addr, align 4, !tbaa !2
  store i8* (i32)* %0, i8* (i32)** @_ZL10g_allocafn, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z23gim_set_realloc_handlerPFPvS_mmE(i8* (i8*, i32, i32)* %fn) #0 {
entry:
  %fn.addr = alloca i8* (i8*, i32, i32)*, align 4
  store i8* (i8*, i32, i32)* %fn, i8* (i8*, i32, i32)** %fn.addr, align 4, !tbaa !2
  %0 = load i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** %fn.addr, align 4, !tbaa !2
  store i8* (i8*, i32, i32)* %0, i8* (i8*, i32, i32)** @_ZL11g_reallocfn, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden void @_Z20gim_set_free_handlerPFvPvE(void (i8*)* %fn) #0 {
entry:
  %fn.addr = alloca void (i8*)*, align 4
  store void (i8*)* %fn, void (i8*)** %fn.addr, align 4, !tbaa !2
  %0 = load void (i8*)*, void (i8*)** %fn.addr, align 4, !tbaa !2
  store void (i8*)* %0, void (i8*)** @_ZL8g_freefn, align 4, !tbaa !2
  ret void
}

; Function Attrs: nounwind
define hidden i8* (i32)* @_Z21gim_get_alloc_handlerv() #0 {
entry:
  %0 = load i8* (i32)*, i8* (i32)** @_ZL9g_allocfn, align 4, !tbaa !2
  ret i8* (i32)* %0
}

; Function Attrs: nounwind
define hidden i8* (i32)* @_Z22gim_get_alloca_handlerv() #0 {
entry:
  %0 = load i8* (i32)*, i8* (i32)** @_ZL10g_allocafn, align 4, !tbaa !2
  ret i8* (i32)* %0
}

; Function Attrs: nounwind
define hidden i8* (i8*, i32, i32)* @_Z23gim_get_realloc_handlerv() #0 {
entry:
  %0 = load i8* (i8*, i32, i32)*, i8* (i8*, i32, i32)** @_ZL11g_reallocfn, align 4, !tbaa !2
  ret i8* (i8*, i32, i32)* %0
}

; Function Attrs: nounwind
define hidden void (i8*)* @_Z20gim_get_free_handlerv() #0 {
entry:
  %0 = load void (i8*)*, void (i8*)** @_ZL8g_freefn, align 4, !tbaa !2
  ret void (i8*)* %0
}

define hidden i8* @_Z9gim_allocm(i32 %size) #1 {
entry:
  %size.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i8* (i32)*, i8* (i32)** @_ZL9g_allocfn, align 4, !tbaa !2
  %tobool = icmp ne i8* (i32)* %1, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i8* (i32)*, i8* (i32)** @_ZL9g_allocfn, align 4, !tbaa !2
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i8* %2(i32 %3)
  store i8* %call, i8** %ptr, align 4, !tbaa !2
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call1 = call i8* @malloc(i32 %4)
  store i8* %call1, i8** %ptr, align 4, !tbaa !2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %5 = load i8*, i8** %ptr, align 4, !tbaa !2
  %6 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #5
  ret i8* %5
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

declare i8* @malloc(i32) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden i8* @_Z10gim_allocam(i32 %size) #1 {
entry:
  %retval = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %0 = load i8* (i32)*, i8* (i32)** @_ZL10g_allocafn, align 4, !tbaa !2
  %tobool = icmp ne i8* (i32)* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8* (i32)*, i8* (i32)** @_ZL10g_allocafn, align 4, !tbaa !2
  %2 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call i8* %1(i32 %2)
  store i8* %call, i8** %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call1 = call i8* @_Z9gim_allocm(i32 %3)
  store i8* %call1, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %4 = load i8*, i8** %retval, align 4
  ret i8* %4
}

define hidden i8* @_Z11gim_reallocPvmm(i8* %ptr, i32 %oldsize, i32 %newsize) #1 {
entry:
  %ptr.addr = alloca i8*, align 4
  %oldsize.addr = alloca i32, align 4
  %newsize.addr = alloca i32, align 4
  %newptr = alloca i8*, align 4
  %copysize = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  store i32 %oldsize, i32* %oldsize.addr, align 4, !tbaa !6
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !6
  %0 = bitcast i8** %newptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #5
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %call = call i8* @_Z9gim_allocm(i32 %1)
  store i8* %call, i8** %newptr, align 4, !tbaa !2
  %2 = bitcast i32* %copysize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #5
  %3 = load i32, i32* %oldsize.addr, align 4, !tbaa !6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  %cmp = icmp ult i32 %3, %4
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %oldsize.addr, align 4, !tbaa !6
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %newsize.addr, align 4, !tbaa !6
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ %6, %cond.false ]
  store i32 %cond, i32* %copysize, align 4, !tbaa !6
  %7 = load i8*, i8** %newptr, align 4, !tbaa !2
  %8 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %9 = load i32, i32* %copysize, align 4, !tbaa !6
  call void @_Z15gim_simd_memcpyPvPKvm(i8* %7, i8* %8, i32 %9)
  %10 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z8gim_freePv(i8* %10)
  %11 = load i8*, i8** %newptr, align 4, !tbaa !2
  %12 = bitcast i32* %copysize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #5
  %13 = bitcast i8** %newptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #5
  ret i8* %11
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z15gim_simd_memcpyPvPKvm(i8* %dst, i8* %src, i32 %copysize) #4 comdat {
entry:
  %dst.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %copysize.addr = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4, !tbaa !2
  store i8* %src, i8** %src.addr, align 4, !tbaa !2
  store i32 %copysize, i32* %copysize.addr, align 4, !tbaa !6
  %0 = load i8*, i8** %dst.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %src.addr, align 4, !tbaa !2
  %2 = load i32, i32* %copysize.addr, align 4, !tbaa !6
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %0, i8* align 1 %1, i32 %2, i1 false)
  ret void
}

define hidden void @_Z8gim_freePv(i8* %ptr) #1 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end3

if.end:                                           ; preds = %entry
  %1 = load void (i8*)*, void (i8*)** @_ZL8g_freefn, align 4, !tbaa !2
  %tobool1 = icmp ne void (i8*)* %1, null
  br i1 %tobool1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.end
  %2 = load void (i8*)*, void (i8*)** @_ZL8g_freefn, align 4, !tbaa !2
  %3 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void %2(i8* %3)
  br label %if.end3

if.else:                                          ; preds = %if.end
  %4 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @free(i8* %4)
  br label %if.end3

if.end3:                                          ; preds = %if.then, %if.else, %if.then2
  ret void
}

declare void @free(i8*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"long", !4, i64 0}
