; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btDefaultSoftBodySolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletSoftBody/btDefaultSoftBodySolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btDefaultSoftBodySolver = type { %class.btSoftBodySolver, i8, [3 x i8], %class.btAlignedObjectArray }
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btSoftBody**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray.0, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.44, %class.btAlignedObjectArray.52, %class.btAlignedObjectArray.56, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.68, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.80, %class.btAlignedObjectArray.84, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.88 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%union.anon = type { i8* }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.7, %class.btAlignedObjectArray.7 }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray.7 = type <{ %class.btAlignedAllocator.8, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.8 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.15, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.11 = type <{ %class.btAlignedAllocator.12, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.12 = type { i8 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type opaque
%class.btDispatcher = type opaque
%struct.btSparseSdf = type { %class.btAlignedObjectArray.19, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.26 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.26 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%class.btAlignedObjectArray.32 = type <{ %class.btAlignedAllocator.33, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.33 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float, %class.btVector3 }
%class.btAlignedObjectArray.36 = type <{ %class.btAlignedAllocator.37, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.37 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.40 = type <{ %class.btAlignedAllocator.41, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.41 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.47, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.47 = type <{ %class.btAlignedAllocator.48, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.48 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.52 = type <{ %class.btAlignedAllocator.53, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.53 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.56 = type <{ %class.btAlignedAllocator.57, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.57 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.63, %class.btAlignedObjectArray.11, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.63 = type <{ %class.btAlignedAllocator.64, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.64 = type { i8 }
%class.btAlignedObjectArray.68 = type <{ %class.btAlignedAllocator.69, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.69 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.72, %class.btAlignedObjectArray.76 }
%class.btAlignedObjectArray.72 = type <{ %class.btAlignedAllocator.73, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.73 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.76 = type <{ %class.btAlignedAllocator.77, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.77 = type { i8 }
%class.btAlignedObjectArray.80 = type <{ %class.btAlignedAllocator.81, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.81 = type { i8 }
%class.btAlignedObjectArray.84 = type <{ %class.btAlignedAllocator.85, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.85 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.88 = type <{ %class.btAlignedAllocator.89, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.89 = type { i8 }
%class.btVertexBufferDescriptor = type { i32 (...)**, i8, i8, i32, i32, i32, i32 }
%class.btCPUVertexBufferDescriptor = type { %class.btVertexBufferDescriptor, float* }
%struct.btCollisionObjectWrapper = type opaque

$_ZN16btSoftBodySolverC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK23btDefaultSoftBodySolver13getSolverTypeEv = comdat any

$_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi = comdat any

$_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv = comdat any

$_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi = comdat any

$_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv = comdat any

$_ZN16btSoftBodySolverD2Ev = comdat any

$_ZN16btSoftBodySolverD0Ev = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZTS16btSoftBodySolver = comdat any

$_ZTI16btSoftBodySolver = comdat any

$_ZTV16btSoftBodySolver = comdat any

@_ZTV23btDefaultSoftBodySolver = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btDefaultSoftBodySolver to i8*), i8* bitcast (%class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD1Ev to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD0Ev to i8*), i8* bitcast (i32 (%class.btDefaultSoftBodySolver*)* @_ZNK23btDefaultSoftBodySolver13getSolverTypeEv to i8*), i8* bitcast (i1 (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolver16checkInitializedEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btAlignedObjectArray*, i1)* @_ZN23btDefaultSoftBodySolver8optimizeER20btAlignedObjectArrayIP10btSoftBodyEb to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, i1)* @_ZN23btDefaultSoftBodySolver20copyBackToSoftBodiesEb to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, float)* @_ZN23btDefaultSoftBodySolver13predictMotionEf to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, float)* @_ZN23btDefaultSoftBodySolver16solveConstraintsEf to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolver16updateSoftBodiesEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)* @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyPK24btCollisionObjectWrapper to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)* @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyS1_ to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %class.btVertexBufferDescriptor*)* @_ZN23btDefaultSoftBodySolver26copySoftBodyToVertexBufferEPK10btSoftBodyP24btVertexBufferDescriptor to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btDefaultSoftBodySolver = hidden constant [26 x i8] c"23btDefaultSoftBodySolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS16btSoftBodySolver = linkonce_odr hidden constant [19 x i8] c"16btSoftBodySolver\00", comdat, align 1
@_ZTI16btSoftBodySolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btSoftBodySolver, i32 0, i32 0) }, comdat, align 4
@_ZTI23btDefaultSoftBodySolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btDefaultSoftBodySolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI16btSoftBodySolver to i8*) }, align 4
@_ZTV16btSoftBodySolver = linkonce_odr hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI16btSoftBodySolver to i8*), i8* bitcast (%class.btSoftBodySolver* (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolverD2Ev to i8*), i8* bitcast (void (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolverD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv to i8*)] }, comdat, align 4

@_ZN23btDefaultSoftBodySolverC1Ev = hidden unnamed_addr alias %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*), %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverC2Ev
@_ZN23btDefaultSoftBodySolverD1Ev = hidden unnamed_addr alias %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*), %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD2Ev

define hidden %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC2Ev(%class.btDefaultSoftBodySolver* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to %class.btSoftBodySolver*
  %call = call %class.btSoftBodySolver* @_ZN16btSoftBodySolverC2Ev(%class.btSoftBodySolver* %0)
  %1 = bitcast %class.btDefaultSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV23btDefaultSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray* %m_softBodySet)
  %m_updateSolverConstants = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 1
  store i8 1, i8* %m_updateSolverConstants, align 4, !tbaa !8
  ret %class.btDefaultSoftBodySolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN16btSoftBodySolverC2Ev(%class.btSoftBodySolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV16btSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 10, i32* %m_numberOfPositionIterations, align 4, !tbaa !14
  %m_timeScale = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %m_timeScale, align 4, !tbaa !17
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  store i32 0, i32* %m_numberOfVelocityIterations, align 4, !tbaa !18
  %m_numberOfPositionIterations2 = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 5, i32* %m_numberOfPositionIterations2, align 4, !tbaa !14
  ret %class.btSoftBodySolver* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverD2Ev(%class.btDefaultSoftBodySolver* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV23btDefaultSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray* %m_softBodySet) #8
  %1 = bitcast %class.btDefaultSoftBodySolver* %this1 to %class.btSoftBodySolver*
  %call2 = call %class.btSoftBodySolver* @_ZN16btSoftBodySolverD2Ev(%class.btSoftBodySolver* %1) #8
  ret %class.btDefaultSoftBodySolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN23btDefaultSoftBodySolverD0Ev(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %call = call %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverD1Ev(%class.btDefaultSoftBodySolver* %this1) #8
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #2

; Function Attrs: nounwind
define hidden void @_ZN23btDefaultSoftBodySolver20copyBackToSoftBodiesEb(%class.btDefaultSoftBodySolver* %this, i1 zeroext %bMove) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %bMove.addr = alloca i8, align 1
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %frombool = zext i1 %bMove to i8
  store i8 %frombool, i8* %bMove.addr, align 1, !tbaa !19
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret void
}

define hidden void @_ZN23btDefaultSoftBodySolver8optimizeER20btAlignedObjectArrayIP10btSoftBodyEb(%class.btDefaultSoftBodySolver* %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %softBodies, i1 zeroext %forceUpdate) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBodies.addr = alloca %class.btAlignedObjectArray*, align 4
  %forceUpdate.addr = alloca i8, align 1
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %softBodies, %class.btAlignedObjectArray** %softBodies.addr, align 4, !tbaa !2
  %frombool = zext i1 %forceUpdate to i8
  store i8 %frombool, i8* %forceUpdate.addr, align 1, !tbaa !19
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %softBodies.addr, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_(%class.btAlignedObjectArray* %m_softBodySet, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %0)
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_(%class.btAlignedObjectArray* %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %otherArray) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btSoftBody*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray* %otherArray, %class.btAlignedObjectArray** %otherArray.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4, !tbaa !2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %1)
  store i32 %call, i32* %otherSize, align 4, !tbaa !20
  %2 = load i32, i32* %otherSize, align 4, !tbaa !20
  %3 = bitcast %class.btSoftBody** %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store %class.btSoftBody* null, %class.btSoftBody** %ref.tmp, align 4, !tbaa !2
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_(%class.btAlignedObjectArray* %this1, i32 %2, %class.btSoftBody** nonnull align 4 dereferenceable(4) %ref.tmp)
  %4 = bitcast %class.btSoftBody** %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4, !tbaa !2
  %6 = load i32, i32* %otherSize, align 4, !tbaa !20
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %5, i32 0, i32 %6, %class.btSoftBody** %7)
  %8 = bitcast i32* %otherSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

define hidden void @_ZN23btDefaultSoftBodySolver16updateSoftBodiesEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !20
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4, !tbaa !20
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %4)
  %5 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4, !tbaa !2
  store %class.btSoftBody* %5, %class.btSoftBody** %psb, align 4, !tbaa !2
  %6 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %7 = bitcast %class.btSoftBody* %6 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %7)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody15integrateMotionEv(%class.btSoftBody* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %9 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !22
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !20
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %1 = load i32, i32* %n.addr, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %0, i32 %1
  ret %class.btSoftBody** %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

declare void @_ZN10btSoftBody15integrateMotionEv(%class.btSoftBody*) #6

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define hidden zeroext i1 @_ZN23btDefaultSoftBodySolver16checkInitializedEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret i1 true
}

define hidden void @_ZN23btDefaultSoftBodySolver16solveConstraintsEf(%class.btDefaultSoftBodySolver* %this, float %solverdt) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %solverdt.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store float %solverdt, float* %solverdt.addr, align 4, !tbaa !23
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !20
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4, !tbaa !20
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %4)
  %5 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4, !tbaa !2
  store %class.btSoftBody* %5, %class.btSoftBody** %psb, align 4, !tbaa !2
  %6 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %7 = bitcast %class.btSoftBody* %6 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %7)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  call void @_ZN10btSoftBody16solveConstraintsEv(%class.btSoftBody* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %9 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZN10btSoftBody16solveConstraintsEv(%class.btSoftBody*) #6

define hidden void @_ZN23btDefaultSoftBodySolver26copySoftBodyToVertexBufferEPK10btSoftBodyP24btVertexBufferDescriptor(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %class.btVertexBufferDescriptor* %vertexBuffer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %vertexBuffer.addr = alloca %class.btVertexBufferDescriptor*, align 4
  %clothVertices = alloca %class.btAlignedObjectArray.28*, align 4
  %numVertices = alloca i32, align 4
  %cpuVertexBuffer = alloca %class.btCPUVertexBufferDescriptor*, align 4
  %basePointer = alloca float*, align 4
  %vertexOffset = alloca i32, align 4
  %vertexStride = alloca i32, align 4
  %vertexPointer = alloca float*, align 4
  %vertexIndex = alloca i32, align 4
  %position = alloca %class.btVector3, align 4
  %normalOffset = alloca i32, align 4
  %normalStride = alloca i32, align 4
  %normalPointer = alloca float*, align 4
  %vertexIndex36 = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  store %class.btVertexBufferDescriptor* %vertexBuffer, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVertexBufferDescriptor* %0 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %1, align 4, !tbaa !6
  %vfn = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable, i64 4
  %2 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn, align 4
  %call = call i32 %2(%class.btVertexBufferDescriptor* %0)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end53

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btAlignedObjectArray.28** %clothVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %4, i32 0, i32 9
  store %class.btAlignedObjectArray.28* %m_nodes, %class.btAlignedObjectArray.28** %clothVertices, align 4, !tbaa !2
  %5 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.28* %6)
  store i32 %call2, i32* %numVertices, align 4, !tbaa !20
  %7 = bitcast %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVertexBufferDescriptor* %8 to %class.btCPUVertexBufferDescriptor*
  store %class.btCPUVertexBufferDescriptor* %9, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %10 = bitcast float** %basePointer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %12 = bitcast %class.btCPUVertexBufferDescriptor* %11 to float* (%class.btCPUVertexBufferDescriptor*)***
  %vtable3 = load float* (%class.btCPUVertexBufferDescriptor*)**, float* (%class.btCPUVertexBufferDescriptor*)*** %12, align 4, !tbaa !6
  %vfn4 = getelementptr inbounds float* (%class.btCPUVertexBufferDescriptor*)*, float* (%class.btCPUVertexBufferDescriptor*)** %vtable3, i64 9
  %13 = load float* (%class.btCPUVertexBufferDescriptor*)*, float* (%class.btCPUVertexBufferDescriptor*)** %vfn4, align 4
  %call5 = call float* %13(%class.btCPUVertexBufferDescriptor* %11)
  store float* %call5, float** %basePointer, align 4, !tbaa !2
  %14 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4, !tbaa !2
  %15 = bitcast %class.btVertexBufferDescriptor* %14 to i1 (%class.btVertexBufferDescriptor*)***
  %vtable6 = load i1 (%class.btVertexBufferDescriptor*)**, i1 (%class.btVertexBufferDescriptor*)*** %15, align 4, !tbaa !6
  %vfn7 = getelementptr inbounds i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vtable6, i64 2
  %16 = load i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vfn7, align 4
  %call8 = call zeroext i1 %16(%class.btVertexBufferDescriptor* %14)
  br i1 %call8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.then
  %17 = bitcast i32* %vertexOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %19 = bitcast %class.btCPUVertexBufferDescriptor* %18 to %class.btVertexBufferDescriptor*
  %20 = bitcast %class.btVertexBufferDescriptor* %19 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable10 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %20, align 4, !tbaa !6
  %vfn11 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable10, i64 5
  %21 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn11, align 4
  %call12 = call i32 %21(%class.btVertexBufferDescriptor* %19)
  store i32 %call12, i32* %vertexOffset, align 4, !tbaa !20
  %22 = bitcast i32* %vertexStride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  %23 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %24 = bitcast %class.btCPUVertexBufferDescriptor* %23 to %class.btVertexBufferDescriptor*
  %25 = bitcast %class.btVertexBufferDescriptor* %24 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable13 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %25, align 4, !tbaa !6
  %vfn14 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable13, i64 6
  %26 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn14, align 4
  %call15 = call i32 %26(%class.btVertexBufferDescriptor* %24)
  store i32 %call15, i32* %vertexStride, align 4, !tbaa !20
  %27 = bitcast float** %vertexPointer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load float*, float** %basePointer, align 4, !tbaa !2
  %29 = load i32, i32* %vertexOffset, align 4, !tbaa !20
  %add.ptr = getelementptr inbounds float, float* %28, i32 %29
  store float* %add.ptr, float** %vertexPointer, align 4, !tbaa !2
  %30 = bitcast i32* %vertexIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  store i32 0, i32* %vertexIndex, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then9
  %31 = load i32, i32* %vertexIndex, align 4, !tbaa !20
  %32 = load i32, i32* %numVertices, align 4, !tbaa !20
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %33 = bitcast i32* %vertexIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %34 = bitcast %class.btVector3* %position to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #8
  %35 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4, !tbaa !2
  %36 = load i32, i32* %vertexIndex, align 4, !tbaa !20
  %call17 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %35, i32 %36)
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %call17, i32 0, i32 1
  %37 = bitcast %class.btVector3* %position to i8*
  %38 = bitcast %class.btVector3* %m_x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !24
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %position)
  %39 = load float, float* %call18, align 4, !tbaa !23
  %40 = load float*, float** %vertexPointer, align 4, !tbaa !2
  %add.ptr19 = getelementptr inbounds float, float* %40, i32 0
  store float %39, float* %add.ptr19, align 4, !tbaa !23
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %position)
  %41 = load float, float* %call20, align 4, !tbaa !23
  %42 = load float*, float** %vertexPointer, align 4, !tbaa !2
  %add.ptr21 = getelementptr inbounds float, float* %42, i32 1
  store float %41, float* %add.ptr21, align 4, !tbaa !23
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %position)
  %43 = load float, float* %call22, align 4, !tbaa !23
  %44 = load float*, float** %vertexPointer, align 4, !tbaa !2
  %add.ptr23 = getelementptr inbounds float, float* %44, i32 2
  store float %43, float* %add.ptr23, align 4, !tbaa !23
  %45 = load i32, i32* %vertexStride, align 4, !tbaa !20
  %46 = load float*, float** %vertexPointer, align 4, !tbaa !2
  %add.ptr24 = getelementptr inbounds float, float* %46, i32 %45
  store float* %add.ptr24, float** %vertexPointer, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %position to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %vertexIndex, align 4, !tbaa !20
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %vertexIndex, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %49 = bitcast float** %vertexPointer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast i32* %vertexStride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast i32* %vertexOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %52 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4, !tbaa !2
  %53 = bitcast %class.btVertexBufferDescriptor* %52 to i1 (%class.btVertexBufferDescriptor*)***
  %vtable25 = load i1 (%class.btVertexBufferDescriptor*)**, i1 (%class.btVertexBufferDescriptor*)*** %53, align 4, !tbaa !6
  %vfn26 = getelementptr inbounds i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vtable25, i64 3
  %54 = load i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vfn26, align 4
  %call27 = call zeroext i1 %54(%class.btVertexBufferDescriptor* %52)
  br i1 %call27, label %if.then28, label %if.end52

if.then28:                                        ; preds = %if.end
  %55 = bitcast i32* %normalOffset to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #8
  %56 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %57 = bitcast %class.btCPUVertexBufferDescriptor* %56 to %class.btVertexBufferDescriptor*
  %58 = bitcast %class.btVertexBufferDescriptor* %57 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable29 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %58, align 4, !tbaa !6
  %vfn30 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable29, i64 7
  %59 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn30, align 4
  %call31 = call i32 %59(%class.btVertexBufferDescriptor* %57)
  store i32 %call31, i32* %normalOffset, align 4, !tbaa !20
  %60 = bitcast i32* %normalStride to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #8
  %61 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4, !tbaa !2
  %62 = bitcast %class.btCPUVertexBufferDescriptor* %61 to %class.btVertexBufferDescriptor*
  %63 = bitcast %class.btVertexBufferDescriptor* %62 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable32 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %63, align 4, !tbaa !6
  %vfn33 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable32, i64 8
  %64 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn33, align 4
  %call34 = call i32 %64(%class.btVertexBufferDescriptor* %62)
  store i32 %call34, i32* %normalStride, align 4, !tbaa !20
  %65 = bitcast float** %normalPointer to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #8
  %66 = load float*, float** %basePointer, align 4, !tbaa !2
  %67 = load i32, i32* %normalOffset, align 4, !tbaa !20
  %add.ptr35 = getelementptr inbounds float, float* %66, i32 %67
  store float* %add.ptr35, float** %normalPointer, align 4, !tbaa !2
  %68 = bitcast i32* %vertexIndex36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  store i32 0, i32* %vertexIndex36, align 4, !tbaa !20
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc49, %if.then28
  %69 = load i32, i32* %vertexIndex36, align 4, !tbaa !20
  %70 = load i32, i32* %numVertices, align 4, !tbaa !20
  %cmp38 = icmp slt i32 %69, %70
  br i1 %cmp38, label %for.body40, label %for.cond.cleanup39

for.cond.cleanup39:                               ; preds = %for.cond37
  %71 = bitcast i32* %vertexIndex36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #8
  br label %for.end51

for.body40:                                       ; preds = %for.cond37
  %72 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %72) #8
  %73 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4, !tbaa !2
  %74 = load i32, i32* %vertexIndex36, align 4, !tbaa !20
  %call41 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %73, i32 %74)
  %m_n = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %call41, i32 0, i32 5
  %75 = bitcast %class.btVector3* %normal to i8*
  %76 = bitcast %class.btVector3* %m_n to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false), !tbaa.struct !24
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %normal)
  %77 = load float, float* %call42, align 4, !tbaa !23
  %78 = load float*, float** %normalPointer, align 4, !tbaa !2
  %add.ptr43 = getelementptr inbounds float, float* %78, i32 0
  store float %77, float* %add.ptr43, align 4, !tbaa !23
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %normal)
  %79 = load float, float* %call44, align 4, !tbaa !23
  %80 = load float*, float** %normalPointer, align 4, !tbaa !2
  %add.ptr45 = getelementptr inbounds float, float* %80, i32 1
  store float %79, float* %add.ptr45, align 4, !tbaa !23
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %normal)
  %81 = load float, float* %call46, align 4, !tbaa !23
  %82 = load float*, float** %normalPointer, align 4, !tbaa !2
  %add.ptr47 = getelementptr inbounds float, float* %82, i32 2
  store float %81, float* %add.ptr47, align 4, !tbaa !23
  %83 = load i32, i32* %normalStride, align 4, !tbaa !20
  %84 = load float*, float** %normalPointer, align 4, !tbaa !2
  %add.ptr48 = getelementptr inbounds float, float* %84, i32 %83
  store float* %add.ptr48, float** %normalPointer, align 4, !tbaa !2
  %85 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  br label %for.inc49

for.inc49:                                        ; preds = %for.body40
  %86 = load i32, i32* %vertexIndex36, align 4, !tbaa !20
  %inc50 = add nsw i32 %86, 1
  store i32 %inc50, i32* %vertexIndex36, align 4, !tbaa !20
  br label %for.cond37

for.end51:                                        ; preds = %for.cond.cleanup39
  %87 = bitcast float** %normalPointer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast i32* %normalStride to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast i32* %normalOffset to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  br label %if.end52

if.end52:                                         ; preds = %for.end51, %if.end
  %90 = bitcast float** %basePointer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast i32* %numVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  %93 = bitcast %class.btAlignedObjectArray.28** %clothVertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.28* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !26
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !20
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_data, align 4, !tbaa !29
  %1 = load i32, i32* %n.addr, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %0, i32 %1
  ret %"struct.btSoftBody::Node"* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

define hidden void @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyS1_(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %class.btSoftBody* %otherSoftBody) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %otherSoftBody.addr = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  store %class.btSoftBody* %otherSoftBody, %class.btSoftBody** %otherSoftBody.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  %1 = load %class.btSoftBody*, %class.btSoftBody** %otherSoftBody.addr, align 4, !tbaa !2
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody* %0, %class.btSoftBody* %1)
  ret void
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody*, %class.btSoftBody*) #6

define hidden void @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyPK24btCollisionObjectWrapper(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %struct.btCollisionObjectWrapper* %collisionObjectWrap) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %collisionObjectWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  store %struct.btCollisionObjectWrapper* %collisionObjectWrap, %struct.btCollisionObjectWrapper** %collisionObjectWrap.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4, !tbaa !2
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %collisionObjectWrap.addr, align 4, !tbaa !2
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPK24btCollisionObjectWrapper(%class.btSoftBody* %0, %struct.btCollisionObjectWrapper* %1)
  ret void
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPK24btCollisionObjectWrapper(%class.btSoftBody*, %struct.btCollisionObjectWrapper*) #6

define hidden void @_ZN23btDefaultSoftBodySolver13predictMotionEf(%class.btDefaultSoftBodySolver* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !23
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !20
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4, !tbaa !20
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %4)
  %5 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4, !tbaa !2
  store %class.btSoftBody* %5, %class.btSoftBody** %psb, align 4, !tbaa !2
  %6 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %7 = bitcast %class.btSoftBody* %6 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %7)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4, !tbaa !2
  %9 = load float, float* %timeStep.addr, align 4, !tbaa !23
  call void @_ZN10btSoftBody13predictMotionEf(%class.btSoftBody* %8, float %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %10 = bitcast %class.btSoftBody** %psb to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

declare void @_ZN10btSoftBody13predictMotionEf(%class.btSoftBody*, float) #6

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK23btDefaultSoftBodySolver13getSolverTypeEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi(%class.btSoftBodySolver* %this, i32 %iterations) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  %iterations.addr = alloca i32, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  store i32 %iterations, i32* %iterations.addr, align 4, !tbaa !20
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = load i32, i32* %iterations.addr, align 4, !tbaa !20
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 %0, i32* %m_numberOfPositionIterations, align 4, !tbaa !14
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numberOfPositionIterations, align 4, !tbaa !14
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi(%class.btSoftBodySolver* %this, i32 %iterations) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  %iterations.addr = alloca i32, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  store i32 %iterations, i32* %iterations.addr, align 4, !tbaa !20
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = load i32, i32* %iterations.addr, align 4, !tbaa !20
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  store i32 %0, i32* %m_numberOfVelocityIterations, align 4, !tbaa !18
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numberOfVelocityIterations, align 4, !tbaa !18
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN16btSoftBodySolverD2Ev(%class.btSoftBodySolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  ret %class.btSoftBodySolver* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btSoftBodySolverD0Ev(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  %0 = load i32, i32* %m_activationState1, align 4, !tbaa !30
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !35
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !22
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !36
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !20
  store i32 %last, i32* %last.addr, align 4, !tbaa !20
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !20
  store i32 %1, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !20
  %3 = load i32, i32* %last.addr, align 4, !tbaa !20
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %5 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %tobool = icmp ne %class.btSoftBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !35, !range !37
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4, !tbaa !21
  call void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btSoftBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data5, align 4, !tbaa !21
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btSoftBody** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btSoftBody**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btSoftBody** %ptr, %class.btSoftBody*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btSoftBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btSoftBody** nonnull align 4 dereferenceable(4) %fillData) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btSoftBody**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !20
  store %class.btSoftBody** %fillData, %class.btSoftBody*** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !20
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  %2 = load i32, i32* %curSize, align 4, !tbaa !20
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  store i32 %4, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !20
  %6 = load i32, i32* %curSize, align 4, !tbaa !20
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %9 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !20
  store i32 %14, i32* %i6, align 4, !tbaa !20
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !20
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btSoftBody**, %class.btSoftBody*** %m_data11, align 4, !tbaa !21
  %19 = load i32, i32* %i6, align 4, !tbaa !20
  %arrayidx12 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %18, i32 %19
  %20 = bitcast %class.btSoftBody** %arrayidx12 to i8*
  %21 = bitcast i8* %20 to %class.btSoftBody**
  %22 = load %class.btSoftBody**, %class.btSoftBody*** %fillData.addr, align 4, !tbaa !2
  %23 = load %class.btSoftBody*, %class.btSoftBody** %22, align 4, !tbaa !2
  store %class.btSoftBody* %23, %class.btSoftBody** %21, align 4, !tbaa !2
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !20
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !20
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !20
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !22
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btSoftBody** %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btSoftBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !20
  store i32 %end, i32* %end.addr, align 4, !tbaa !20
  store %class.btSoftBody** %dest, %class.btSoftBody*** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !20
  store i32 %1, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !20
  %3 = load i32, i32* %end.addr, align 4, !tbaa !20
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %4, i32 %5
  %6 = bitcast %class.btSoftBody** %arrayidx to i8*
  %7 = bitcast i8* %6 to %class.btSoftBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %9 = load i32, i32* %i, align 4, !tbaa !20
  %arrayidx2 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %8, i32 %9
  %10 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx2, align 4, !tbaa !2
  store %class.btSoftBody* %10, %class.btSoftBody** %7, align 4, !tbaa !2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !20
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !20
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btSoftBody**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !20
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !20
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btSoftBody*** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !20
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btSoftBody**
  store %class.btSoftBody** %3, %class.btSoftBody*** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btSoftBody** %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !35
  %5 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** %5, %class.btSoftBody*** %m_data, align 4, !tbaa !21
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !20
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !36
  %7 = bitcast %class.btSoftBody*** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !36
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #5 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !20
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !20
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !20
  %call = call %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btSoftBody*** null)
  %2 = bitcast %class.btSoftBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

define linkonce_odr hidden %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btSoftBody*** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btSoftBody***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !20
  store %class.btSoftBody*** %hint, %class.btSoftBody**** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !20
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btSoftBody**
  ret %class.btSoftBody** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 16}
!9 = !{!"_ZTS23btDefaultSoftBodySolver", !10, i64 16, !11, i64 20}
!10 = !{!"bool", !4, i64 0}
!11 = !{!"_ZTS20btAlignedObjectArrayIP10btSoftBodyE", !12, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !10, i64 16}
!12 = !{!"_ZTS18btAlignedAllocatorIP10btSoftBodyLj16EE"}
!13 = !{!"int", !4, i64 0}
!14 = !{!15, !13, i64 4}
!15 = !{!"_ZTS16btSoftBodySolver", !13, i64 4, !13, i64 8, !16, i64 12}
!16 = !{!"float", !4, i64 0}
!17 = !{!15, !16, i64 12}
!18 = !{!15, !13, i64 8}
!19 = !{!10, !10, i64 0}
!20 = !{!13, !13, i64 0}
!21 = !{!11, !3, i64 12}
!22 = !{!11, !13, i64 4}
!23 = !{!16, !16, i64 0}
!24 = !{i64 0, i64 16, !25}
!25 = !{!4, !4, i64 0}
!26 = !{!27, !13, i64 4}
!27 = !{!"_ZTS20btAlignedObjectArrayIN10btSoftBody4NodeEE", !28, i64 0, !13, i64 4, !13, i64 8, !3, i64 12, !10, i64 16}
!28 = !{!"_ZTS18btAlignedAllocatorIN10btSoftBody4NodeELj16EE"}
!29 = !{!27, !3, i64 12}
!30 = !{!31, !13, i64 216}
!31 = !{!"_ZTS17btCollisionObject", !32, i64 4, !32, i64 68, !34, i64 132, !34, i64 148, !34, i64 164, !13, i64 180, !16, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !13, i64 204, !13, i64 208, !13, i64 212, !13, i64 216, !16, i64 220, !16, i64 224, !16, i64 228, !16, i64 232, !13, i64 236, !4, i64 240, !16, i64 244, !16, i64 248, !16, i64 252, !13, i64 256, !13, i64 260}
!32 = !{!"_ZTS11btTransform", !33, i64 0, !34, i64 48}
!33 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!34 = !{!"_ZTS9btVector3", !4, i64 0}
!35 = !{!11, !10, i64 16}
!36 = !{!11, !13, i64 8}
!37 = !{i8 0, i8 2}
