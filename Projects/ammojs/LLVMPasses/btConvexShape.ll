; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btConvexShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btBoxShape = type { %class.btPolyhedralConvexShape }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexPolyhedron = type opaque
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btCylinderShape = type { %class.btConvexInternalShape, i32 }
%class.btCapsuleShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexPointCloudShape = type { %class.btPolyhedralConvexAabbCachingShape.base, %class.btVector3*, i32 }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btConvexHullShape = type { %class.btPolyhedralConvexAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConeShape = type { %class.btConvexInternalShape, float, float, float, [3 x i32] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btSerializer = type opaque

$_ZN16btCollisionShapeC2Ev = comdat any

$_ZN16btCollisionShapeD2Ev = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv = comdat any

$_Z6btFselfff = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK15btCylinderShape9getUpAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZNK14btCapsuleShape13getHalfHeightEv = comdat any

$_ZNK14btCapsuleShape9getUpAxisEv = comdat any

$_ZNK14btCapsuleShape9getRadiusEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK21btConvexInternalShape11getMarginNVEv = comdat any

$_ZN23btConvexPointCloudShape17getUnscaledPointsEv = comdat any

$_ZNK23btConvexPointCloudShape12getNumPointsEv = comdat any

$_ZNK21btConvexInternalShape17getLocalScalingNVEv = comdat any

$_ZN17btConvexHullShape17getUnscaledPointsEv = comdat any

$_ZNK17btConvexHullShape12getNumPointsEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK13btSphereShape9getRadiusEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btFabsf = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

@_ZTV13btConvexShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI13btConvexShape to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD1Ev to i8*), i8* bitcast (void (%class.btConvexShape*)* @_ZN13btConvexShapeD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS13btConvexShape = hidden constant [16 x i8] c"13btConvexShape\00", align 1
@_ZTI16btCollisionShape = external constant i8*
@_ZTI13btConvexShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_ZTS13btConvexShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionShape to i8*) }, align 4
@_ZTV16btCollisionShape = external unnamed_addr constant { [18 x i8*] }, align 4

@_ZN13btConvexShapeD1Ev = hidden unnamed_addr alias %class.btConvexShape* (%class.btConvexShape*), %class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev

define hidden %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* %0)
  %1 = bitcast %class.btConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  ret %class.btConvexShape* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast %class.btCollisionShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV16btCollisionShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  store i32 35, i32* %m_shapeType, align 4, !tbaa !8
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* null, i8** %m_userPointer, align 4, !tbaa !11
  ret %class.btCollisionShape* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: nounwind
define hidden %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* %0) #8
  ret %class.btConvexShape* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN13btConvexShapeD0Ev(%class.btConvexShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #2

define hidden void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_(%class.btConvexShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %min, float* nonnull align 4 dereferenceable(4) %max) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %min.addr = alloca float*, align 4
  %max.addr = alloca float*, align 4
  %localAxis = alloca %class.btVector3, align 4
  %vtx1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %vtx2 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %tmp = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4, !tbaa !2
  store float* %min, float** %min.addr, align 4, !tbaa !2
  store float* %max, float** %max.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %localAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %2 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %2)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %localAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %3 = bitcast %class.btVector3* %vtx1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %6, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %7 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %localAxis)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %vtx1, %class.btTransform* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %vtx2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %localAxis)
  %13 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable4 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %13, align 4, !tbaa !6
  %vfn5 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable4, i64 16
  %14 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn5, align 4
  call void %14(%class.btVector3* sret align 4 %ref.tmp2, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %vtx2, %class.btTransform* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %15 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  %16 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  %17 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vtx1, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %18 = load float*, float** %min.addr, align 4, !tbaa !2
  store float %call6, float* %18, align 4, !tbaa !12
  %19 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4, !tbaa !2
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vtx2, %class.btVector3* nonnull align 4 dereferenceable(16) %19)
  %20 = load float*, float** %max.addr, align 4, !tbaa !2
  store float %call7, float* %20, align 4, !tbaa !12
  %21 = load float*, float** %min.addr, align 4, !tbaa !2
  %22 = load float, float* %21, align 4, !tbaa !12
  %23 = load float*, float** %max.addr, align 4, !tbaa !2
  %24 = load float, float* %23, align 4, !tbaa !12
  %cmp = fcmp ogt float %22, %24
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %25 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  %26 = load float*, float** %min.addr, align 4, !tbaa !2
  %27 = load float, float* %26, align 4, !tbaa !12
  store float %27, float* %tmp, align 4, !tbaa !12
  %28 = load float*, float** %max.addr, align 4, !tbaa !2
  %29 = load float, float* %28, align 4, !tbaa !12
  %30 = load float*, float** %min.addr, align 4, !tbaa !2
  store float %29, float* %30, align 4, !tbaa !12
  %31 = load float, float* %tmp, align 4, !tbaa !12
  %32 = load float*, float** %max.addr, align 4, !tbaa !2
  store float %31, float* %32, align 4, !tbaa !12
  %33 = bitcast float* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %34 = bitcast %class.btVector3* %vtx2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #8
  %35 = bitcast %class.btVector3* %vtx1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #8
  %36 = bitcast %class.btVector3* %localAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !12
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !12
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !12
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #4 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !12
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !12
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !12
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !12
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !12
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !12
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !12
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !12
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !12
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !12
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

define hidden void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %localDir) #0 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %localDir.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %halfExtents = alloca %class.btVector3*, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %dir = alloca %class.btVector3, align 4
  %vertices = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  %sup = alloca %class.btVector3, align 4
  %cylShape = alloca %class.btCylinderShape*, align 4
  %halfExtents39 = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %cylinderUpAxis = alloca i32, align 4
  %XX = alloca i32, align 4
  %YY = alloca i32, align 4
  %ZZ = alloca i32, align 4
  %radius = alloca float, align 4
  %halfHeight = alloca float, align 4
  %tmp = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %vec0 = alloca %class.btVector3, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %halfHeight119 = alloca float, align 4
  %capsuleUpAxis = alloca i32, align 4
  %radius122 = alloca float, align 4
  %supVec = alloca %class.btVector3, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp126 = alloca float, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp131 = alloca float, align 4
  %ref.tmp132 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %rlen = alloca float, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %pos = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp145 = alloca %class.btVector3, align 4
  %ref.tmp146 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca %class.btVector3, align 4
  %ref.tmp148 = alloca %class.btVector3, align 4
  %ref.tmp149 = alloca float, align 4
  %pos155 = alloca %class.btVector3, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp158 = alloca float, align 4
  %ref.tmp163 = alloca %class.btVector3, align 4
  %ref.tmp164 = alloca %class.btVector3, align 4
  %ref.tmp165 = alloca %class.btVector3, align 4
  %ref.tmp166 = alloca %class.btVector3, align 4
  %ref.tmp167 = alloca float, align 4
  %convexPointCloudShape = alloca %class.btConvexPointCloudShape*, align 4
  %points = alloca %class.btVector3*, align 4
  %numPoints = alloca i32, align 4
  %convexHullShape = alloca %class.btConvexHullShape*, align 4
  %points196 = alloca %class.btVector3*, align 4
  %numPoints198 = alloca i32, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %localDir, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4, !tbaa !8
  switch i32 %1, label %sw.default204 [
    i32 8, label %sw.bb
    i32 0, label %sw.bb4
    i32 1, label %sw.bb24
    i32 13, label %sw.bb38
    i32 10, label %sw.bb114
    i32 5, label %sw.bb188
    i32 4, label %sw.bb195
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !12
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !12
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %return

sw.bb4:                                           ; preds = %entry
  %8 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %9, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %10 = bitcast %class.btVector3** %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %12 = bitcast %class.btBoxShape* %11 to %class.btConvexInternalShape*
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %12)
  store %class.btVector3* %call5, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %14)
  %15 = load float, float* %call7, align 4, !tbaa !12
  %16 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %16)
  %17 = load float, float* %call8, align 4, !tbaa !12
  %18 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %18)
  %19 = load float, float* %call9, align 4, !tbaa !12
  %fneg = fneg float %19
  %call10 = call float @_Z6btFselfff(float %15, float %17, float %fneg)
  store float %call10, float* %ref.tmp6, align 4, !tbaa !12
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %21 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %21)
  %22 = load float, float* %call12, align 4, !tbaa !12
  %23 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4, !tbaa !12
  %25 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %25)
  %26 = load float, float* %call14, align 4, !tbaa !12
  %fneg15 = fneg float %26
  %call16 = call float @_Z6btFselfff(float %22, float %24, float %fneg15)
  store float %call16, float* %ref.tmp11, align 4, !tbaa !12
  %27 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call18, align 4, !tbaa !12
  %30 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %30)
  %31 = load float, float* %call19, align 4, !tbaa !12
  %32 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4, !tbaa !2
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %32)
  %33 = load float, float* %call20, align 4, !tbaa !12
  %fneg21 = fneg float %33
  %call22 = call float @_Z6btFselfff(float %29, float %31, float %fneg21)
  store float %call22, float* %ref.tmp17, align 4, !tbaa !12
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %34 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  %35 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  %36 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast %class.btVector3** %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  br label %return

sw.bb24:                                          ; preds = %entry
  %39 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %40, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %41 = bitcast %class.btVector3* %dir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  %42 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %42)
  %43 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %43)
  %44 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %44)
  %call28 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %dir, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26, float* nonnull align 4 dereferenceable(4) %call27)
  %45 = bitcast %class.btVector3** %vertices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  %46 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %46, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  store %class.btVector3* %arrayidx, %class.btVector3** %vertices, align 4, !tbaa !2
  %47 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %47) #8
  %48 = load %class.btVector3*, %class.btVector3** %vertices, align 4, !tbaa !2
  %arrayidx29 = getelementptr inbounds %class.btVector3, %class.btVector3* %48, i32 0
  %49 = load %class.btVector3*, %class.btVector3** %vertices, align 4, !tbaa !2
  %arrayidx30 = getelementptr inbounds %class.btVector3, %class.btVector3* %49, i32 1
  %50 = load %class.btVector3*, %class.btVector3** %vertices, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds %class.btVector3, %class.btVector3* %50, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %dir, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx29, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx30, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx31)
  %51 = bitcast %class.btVector3* %sup to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  %52 = load %class.btVector3*, %class.btVector3** %vertices, align 4, !tbaa !2
  %call32 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx33 = getelementptr inbounds %class.btVector3, %class.btVector3* %52, i32 %call32
  %53 = bitcast %class.btVector3* %sup to i8*
  %54 = bitcast %class.btVector3* %arrayidx33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false), !tbaa.struct !14
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %sup)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %sup)
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %sup)
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call34, float* nonnull align 4 dereferenceable(4) %call35, float* nonnull align 4 dereferenceable(4) %call36)
  %55 = bitcast %class.btVector3* %sup to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %56 = bitcast %class.btVector3* %dots to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btVector3** %vertices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast %class.btVector3* %dir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %58) #8
  %59 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #8
  br label %return

sw.bb38:                                          ; preds = %entry
  %60 = bitcast %class.btCylinderShape** %cylShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #8
  %61 = bitcast %class.btConvexShape* %this1 to %class.btCylinderShape*
  store %class.btCylinderShape* %61, %class.btCylinderShape** %cylShape, align 4, !tbaa !2
  %62 = bitcast %class.btVector3* %halfExtents39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #8
  %63 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4, !tbaa !2
  %64 = bitcast %class.btCylinderShape* %63 to %class.btConvexInternalShape*
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %64)
  %65 = bitcast %class.btVector3* %halfExtents39 to i8*
  %66 = bitcast %class.btVector3* %call40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 16, i1 false), !tbaa.struct !14
  %67 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %67) #8
  %68 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %68)
  %69 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %69)
  %70 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %70)
  %call44 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %v, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43)
  %71 = bitcast i32* %cylinderUpAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  %72 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4, !tbaa !2
  %call45 = call i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %72)
  store i32 %call45, i32* %cylinderUpAxis, align 4, !tbaa !16
  %73 = bitcast i32* %XX to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #8
  store i32 1, i32* %XX, align 4, !tbaa !16
  %74 = bitcast i32* %YY to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #8
  store i32 0, i32* %YY, align 4, !tbaa !16
  %75 = bitcast i32* %ZZ to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #8
  store i32 2, i32* %ZZ, align 4, !tbaa !16
  %76 = load i32, i32* %cylinderUpAxis, align 4, !tbaa !16
  switch i32 %76, label %sw.default [
    i32 0, label %sw.bb46
    i32 1, label %sw.bb47
    i32 2, label %sw.bb48
  ]

sw.bb46:                                          ; preds = %sw.bb38
  store i32 1, i32* %XX, align 4, !tbaa !16
  store i32 0, i32* %YY, align 4, !tbaa !16
  store i32 2, i32* %ZZ, align 4, !tbaa !16
  br label %sw.epilog

sw.bb47:                                          ; preds = %sw.bb38
  store i32 0, i32* %XX, align 4, !tbaa !16
  store i32 1, i32* %YY, align 4, !tbaa !16
  store i32 2, i32* %ZZ, align 4, !tbaa !16
  br label %sw.epilog

sw.bb48:                                          ; preds = %sw.bb38
  store i32 0, i32* %XX, align 4, !tbaa !16
  store i32 2, i32* %YY, align 4, !tbaa !16
  store i32 1, i32* %ZZ, align 4, !tbaa !16
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb38
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb48, %sw.bb47, %sw.bb46
  %77 = bitcast float* %radius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #8
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents39)
  %78 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 %78
  %79 = load float, float* %arrayidx50, align 4, !tbaa !12
  store float %79, float* %radius, align 4, !tbaa !12
  %80 = bitcast float* %halfHeight to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #8
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents39)
  %81 = load i32, i32* %cylinderUpAxis, align 4, !tbaa !16
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %81
  %82 = load float, float* %arrayidx52, align 4, !tbaa !12
  store float %82, float* %halfHeight, align 4, !tbaa !12
  %83 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %83) #8
  %call53 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmp)
  %84 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #8
  %85 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #8
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %86 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 %86
  %87 = load float, float* %arrayidx55, align 4, !tbaa !12
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %88 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 %88
  %89 = load float, float* %arrayidx57, align 4, !tbaa !12
  %mul = fmul float %87, %89
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %90 = load i32, i32* %ZZ, align 4, !tbaa !16
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %90
  %91 = load float, float* %arrayidx59, align 4, !tbaa !12
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %92 = load i32, i32* %ZZ, align 4, !tbaa !16
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 %92
  %93 = load float, float* %arrayidx61, align 4, !tbaa !12
  %mul62 = fmul float %91, %93
  %add = fadd float %mul, %mul62
  %call63 = call float @_Z6btSqrtf(float %add)
  store float %call63, float* %s, align 4, !tbaa !12
  %94 = load float, float* %s, align 4, !tbaa !12
  %cmp = fcmp une float %94, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.epilog
  %95 = load float, float* %radius, align 4, !tbaa !12
  %96 = load float, float* %s, align 4, !tbaa !12
  %div = fdiv float %95, %96
  store float %div, float* %d, align 4, !tbaa !12
  %call64 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %97 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 %97
  %98 = load float, float* %arrayidx65, align 4, !tbaa !12
  %99 = load float, float* %d, align 4, !tbaa !12
  %mul66 = fmul float %98, %99
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %100 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %100
  store float %mul66, float* %arrayidx68, align 4, !tbaa !12
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %101 = load i32, i32* %YY, align 4, !tbaa !16
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 %101
  %102 = load float, float* %arrayidx70, align 4, !tbaa !12
  %conv = fpext float %102 to double
  %cmp71 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp71, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %103 = load float, float* %halfHeight, align 4, !tbaa !12
  %fneg72 = fneg float %103
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %104 = load float, float* %halfHeight, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg72, %cond.true ], [ %104, %cond.false ]
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %105 = load i32, i32* %YY, align 4, !tbaa !16
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 %105
  store float %cond, float* %arrayidx74, align 4, !tbaa !12
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %106 = load i32, i32* %ZZ, align 4, !tbaa !16
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 %106
  %107 = load float, float* %arrayidx76, align 4, !tbaa !12
  %108 = load float, float* %d, align 4, !tbaa !12
  %mul77 = fmul float %107, %108
  %call78 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %109 = load i32, i32* %ZZ, align 4, !tbaa !16
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 %109
  store float %mul77, float* %arrayidx79, align 4, !tbaa !12
  %call80 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %tmp)
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %tmp)
  %call82 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %tmp)
  %call83 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call80, float* nonnull align 4 dereferenceable(4) %call81, float* nonnull align 4 dereferenceable(4) %call82)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %sw.epilog
  %110 = load float, float* %radius, align 4, !tbaa !12
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %111 = load i32, i32* %XX, align 4, !tbaa !16
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 %111
  store float %110, float* %arrayidx85, align 4, !tbaa !12
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %112 = load i32, i32* %YY, align 4, !tbaa !16
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %112
  %113 = load float, float* %arrayidx87, align 4, !tbaa !12
  %conv88 = fpext float %113 to double
  %cmp89 = fcmp olt double %conv88, 0.000000e+00
  br i1 %cmp89, label %cond.true90, label %cond.false92

cond.true90:                                      ; preds = %if.else
  %114 = load float, float* %halfHeight, align 4, !tbaa !12
  %fneg91 = fneg float %114
  br label %cond.end93

cond.false92:                                     ; preds = %if.else
  %115 = load float, float* %halfHeight, align 4, !tbaa !12
  br label %cond.end93

cond.end93:                                       ; preds = %cond.false92, %cond.true90
  %cond94 = phi float [ %fneg91, %cond.true90 ], [ %115, %cond.false92 ]
  %call95 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %116 = load i32, i32* %YY, align 4, !tbaa !16
  %arrayidx96 = getelementptr inbounds float, float* %call95, i32 %116
  store float %cond94, float* %arrayidx96, align 4, !tbaa !12
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %117 = load i32, i32* %ZZ, align 4, !tbaa !16
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %117
  store float 0.000000e+00, float* %arrayidx98, align 4, !tbaa !12
  %call99 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %tmp)
  %call100 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %tmp)
  %call101 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %tmp)
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call99, float* nonnull align 4 dereferenceable(4) %call100, float* nonnull align 4 dereferenceable(4) %call101)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %cond.end93, %cond.end
  %118 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %118) #8
  %119 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #8
  %120 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %120) #8
  %121 = bitcast float* %halfHeight to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #8
  %122 = bitcast float* %radius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #8
  %123 = bitcast i32* %ZZ to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #8
  %124 = bitcast i32* %YY to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #8
  %125 = bitcast i32* %XX to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #8
  %126 = bitcast i32* %cylinderUpAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  %127 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %127) #8
  %128 = bitcast %class.btVector3* %halfExtents39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %128) #8
  %129 = bitcast %class.btCylinderShape** %cylShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #8
  br label %return

sw.bb114:                                         ; preds = %entry
  %130 = bitcast %class.btVector3* %vec0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %130) #8
  %131 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call115 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %131)
  %132 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call116 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %132)
  %133 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %133)
  %call118 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vec0, float* nonnull align 4 dereferenceable(4) %call115, float* nonnull align 4 dereferenceable(4) %call116, float* nonnull align 4 dereferenceable(4) %call117)
  %134 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #8
  %135 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %135, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %136 = bitcast float* %halfHeight119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %136) #8
  %137 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call120 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %137)
  store float %call120, float* %halfHeight119, align 4, !tbaa !12
  %138 = bitcast i32* %capsuleUpAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %138) #8
  %139 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call121 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %139)
  store i32 %call121, i32* %capsuleUpAxis, align 4, !tbaa !16
  %140 = bitcast float* %radius122 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #8
  %141 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call123 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %141)
  store float %call123, float* %radius122, align 4, !tbaa !12
  %142 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %142) #8
  %143 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %143) #8
  store float 0.000000e+00, float* %ref.tmp124, align 4, !tbaa !12
  %144 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %144) #8
  store float 0.000000e+00, float* %ref.tmp125, align 4, !tbaa !12
  %145 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %145) #8
  store float 0.000000e+00, float* %ref.tmp126, align 4, !tbaa !12
  %call127 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %supVec, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125, float* nonnull align 4 dereferenceable(4) %ref.tmp126)
  %146 = bitcast float* %ref.tmp126 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #8
  %147 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #8
  %148 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #8
  %149 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %149) #8
  store float 0xC3ABC16D60000000, float* %maxDot, align 4, !tbaa !12
  %150 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %150) #8
  %151 = bitcast %class.btVector3* %vec to i8*
  %152 = bitcast %class.btVector3* %vec0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false), !tbaa.struct !14
  %153 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %153) #8
  %call128 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call128, float* %lenSqr, align 4, !tbaa !12
  %154 = load float, float* %lenSqr, align 4, !tbaa !12
  %cmp129 = fcmp olt float %154, 0x3F1A36E2E0000000
  br i1 %cmp129, label %if.then130, label %if.else134

if.then130:                                       ; preds = %sw.bb114
  %155 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %155) #8
  store float 1.000000e+00, float* %ref.tmp131, align 4, !tbaa !12
  %156 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %156) #8
  store float 0.000000e+00, float* %ref.tmp132, align 4, !tbaa !12
  %157 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #8
  store float 0.000000e+00, float* %ref.tmp133, align 4, !tbaa !12
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133)
  %158 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #8
  %159 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #8
  %160 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #8
  br label %if.end

if.else134:                                       ; preds = %sw.bb114
  %161 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %161) #8
  %162 = load float, float* %lenSqr, align 4, !tbaa !12
  %call135 = call float @_Z6btSqrtf(float %162)
  %div136 = fdiv float 1.000000e+00, %call135
  store float %div136, float* %rlen, align 4, !tbaa !12
  %call137 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  %163 = bitcast float* %rlen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #8
  br label %if.end

if.end:                                           ; preds = %if.else134, %if.then130
  %164 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %164) #8
  %call138 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  %165 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %165) #8
  %166 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %166) #8
  %167 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %167) #8
  store float 0.000000e+00, float* %ref.tmp139, align 4, !tbaa !12
  %168 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %168) #8
  store float 0.000000e+00, float* %ref.tmp140, align 4, !tbaa !12
  %169 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %169) #8
  store float 0.000000e+00, float* %ref.tmp141, align 4, !tbaa !12
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos, float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  %170 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #8
  %171 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #8
  %172 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #8
  %173 = load float, float* %halfHeight119, align 4, !tbaa !12
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos)
  %174 = load i32, i32* %capsuleUpAxis, align 4, !tbaa !16
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 %174
  store float %173, float* %arrayidx144, align 4, !tbaa !12
  %175 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %175) #8
  %176 = bitcast %class.btVector3* %ref.tmp146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %176) #8
  %177 = bitcast %class.btVector3* %ref.tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %177) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp147, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %radius122)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp146, %class.btVector3* nonnull align 4 dereferenceable(16) %pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp147)
  %178 = bitcast %class.btVector3* %ref.tmp148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %178) #8
  %179 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %179) #8
  %180 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %181 = bitcast %class.btCapsuleShape* %180 to %class.btConvexInternalShape*
  %call150 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %181)
  store float %call150, float* %ref.tmp149, align 4, !tbaa !12
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp148, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp145, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp146, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp148)
  %182 = bitcast %class.btVector3* %vtx to i8*
  %183 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %182, i8* align 4 %183, i32 16, i1 false), !tbaa.struct !14
  %184 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #8
  %185 = bitcast %class.btVector3* %ref.tmp148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %185) #8
  %186 = bitcast %class.btVector3* %ref.tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %186) #8
  %187 = bitcast %class.btVector3* %ref.tmp146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %187) #8
  %188 = bitcast %class.btVector3* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %188) #8
  %call151 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call151, float* %newDot, align 4, !tbaa !12
  %189 = load float, float* %newDot, align 4, !tbaa !12
  %190 = load float, float* %maxDot, align 4, !tbaa !12
  %cmp152 = fcmp ogt float %189, %190
  br i1 %cmp152, label %if.then153, label %if.end154

if.then153:                                       ; preds = %if.end
  %191 = load float, float* %newDot, align 4, !tbaa !12
  store float %191, float* %maxDot, align 4, !tbaa !12
  %192 = bitcast %class.btVector3* %supVec to i8*
  %193 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %192, i8* align 4 %193, i32 16, i1 false), !tbaa.struct !14
  br label %if.end154

if.end154:                                        ; preds = %if.then153, %if.end
  %194 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %194) #8
  %195 = bitcast %class.btVector3* %pos155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %195) #8
  %196 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %196) #8
  store float 0.000000e+00, float* %ref.tmp156, align 4, !tbaa !12
  %197 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %197) #8
  store float 0.000000e+00, float* %ref.tmp157, align 4, !tbaa !12
  %198 = bitcast float* %ref.tmp158 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %198) #8
  store float 0.000000e+00, float* %ref.tmp158, align 4, !tbaa !12
  %call159 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos155, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157, float* nonnull align 4 dereferenceable(4) %ref.tmp158)
  %199 = bitcast float* %ref.tmp158 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #8
  %200 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #8
  %201 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #8
  %202 = load float, float* %halfHeight119, align 4, !tbaa !12
  %fneg160 = fneg float %202
  %call161 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos155)
  %203 = load i32, i32* %capsuleUpAxis, align 4, !tbaa !16
  %arrayidx162 = getelementptr inbounds float, float* %call161, i32 %203
  store float %fneg160, float* %arrayidx162, align 4, !tbaa !12
  %204 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %204) #8
  %205 = bitcast %class.btVector3* %ref.tmp164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %205) #8
  %206 = bitcast %class.btVector3* %ref.tmp165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %206) #8
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp165, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %radius122)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp164, %class.btVector3* nonnull align 4 dereferenceable(16) %pos155, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp165)
  %207 = bitcast %class.btVector3* %ref.tmp166 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %207) #8
  %208 = bitcast float* %ref.tmp167 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #8
  %209 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %210 = bitcast %class.btCapsuleShape* %209 to %class.btConvexInternalShape*
  %call168 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %210)
  store float %call168, float* %ref.tmp167, align 4, !tbaa !12
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp166, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp167)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp163, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp164, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp166)
  %211 = bitcast %class.btVector3* %vtx to i8*
  %212 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %211, i8* align 4 %212, i32 16, i1 false), !tbaa.struct !14
  %213 = bitcast float* %ref.tmp167 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #8
  %214 = bitcast %class.btVector3* %ref.tmp166 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %214) #8
  %215 = bitcast %class.btVector3* %ref.tmp165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %215) #8
  %216 = bitcast %class.btVector3* %ref.tmp164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %216) #8
  %217 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %217) #8
  %call169 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call169, float* %newDot, align 4, !tbaa !12
  %218 = load float, float* %newDot, align 4, !tbaa !12
  %219 = load float, float* %maxDot, align 4, !tbaa !12
  %cmp170 = fcmp ogt float %218, %219
  br i1 %cmp170, label %if.then171, label %if.end172

if.then171:                                       ; preds = %if.end154
  %220 = load float, float* %newDot, align 4, !tbaa !12
  store float %220, float* %maxDot, align 4, !tbaa !12
  %221 = bitcast %class.btVector3* %supVec to i8*
  %222 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %221, i8* align 4 %222, i32 16, i1 false), !tbaa.struct !14
  br label %if.end172

if.end172:                                        ; preds = %if.then171, %if.end154
  %223 = bitcast %class.btVector3* %pos155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %223) #8
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %supVec)
  %call174 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %supVec)
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %supVec)
  %call176 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call173, float* nonnull align 4 dereferenceable(4) %call174, float* nonnull align 4 dereferenceable(4) %call175)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %224 = bitcast float* %newDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #8
  %225 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #8
  %226 = bitcast float* %lenSqr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #8
  %227 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %227) #8
  %228 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #8
  %229 = bitcast %class.btVector3* %supVec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #8
  %230 = bitcast float* %radius122 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #8
  %231 = bitcast i32* %capsuleUpAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #8
  %232 = bitcast float* %halfHeight119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #8
  %233 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #8
  %234 = bitcast %class.btVector3* %vec0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %234) #8
  br label %return

sw.bb188:                                         ; preds = %entry
  %235 = bitcast %class.btConvexPointCloudShape** %convexPointCloudShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %235) #8
  %236 = bitcast %class.btConvexShape* %this1 to %class.btConvexPointCloudShape*
  store %class.btConvexPointCloudShape* %236, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4, !tbaa !2
  %237 = bitcast %class.btVector3** %points to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %237) #8
  %238 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4, !tbaa !2
  %call189 = call %class.btVector3* @_ZN23btConvexPointCloudShape17getUnscaledPointsEv(%class.btConvexPointCloudShape* %238)
  store %class.btVector3* %call189, %class.btVector3** %points, align 4, !tbaa !2
  %239 = bitcast i32* %numPoints to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %239) #8
  %240 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4, !tbaa !2
  %call190 = call i32 @_ZNK23btConvexPointCloudShape12getNumPointsEv(%class.btConvexPointCloudShape* %240)
  store i32 %call190, i32* %numPoints, align 4, !tbaa !16
  %241 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %242 = load %class.btVector3*, %class.btVector3** %points, align 4, !tbaa !2
  %243 = load i32, i32* %numPoints, align 4, !tbaa !16
  %244 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4, !tbaa !2
  %245 = bitcast %class.btConvexPointCloudShape* %244 to %class.btConvexInternalShape*
  %call191 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %245)
  call void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %241, %class.btVector3* %242, i32 %243, %class.btVector3* nonnull align 4 dereferenceable(16) %call191)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %246 = bitcast i32* %numPoints to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #8
  %247 = bitcast %class.btVector3** %points to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #8
  %248 = bitcast %class.btConvexPointCloudShape** %convexPointCloudShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #8
  br label %return

sw.bb195:                                         ; preds = %entry
  %249 = bitcast %class.btConvexHullShape** %convexHullShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %249) #8
  %250 = bitcast %class.btConvexShape* %this1 to %class.btConvexHullShape*
  store %class.btConvexHullShape* %250, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %251 = bitcast %class.btVector3** %points196 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %251) #8
  %252 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %call197 = call %class.btVector3* @_ZN17btConvexHullShape17getUnscaledPointsEv(%class.btConvexHullShape* %252)
  store %class.btVector3* %call197, %class.btVector3** %points196, align 4, !tbaa !2
  %253 = bitcast i32* %numPoints198 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %253) #8
  %254 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %call199 = call i32 @_ZNK17btConvexHullShape12getNumPointsEv(%class.btConvexHullShape* %254)
  store i32 %call199, i32* %numPoints198, align 4, !tbaa !16
  %255 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %256 = load %class.btVector3*, %class.btVector3** %points196, align 4, !tbaa !2
  %257 = load i32, i32* %numPoints198, align 4, !tbaa !16
  %258 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4, !tbaa !2
  %259 = bitcast %class.btConvexHullShape* %258 to %class.btConvexInternalShape*
  %call200 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %259)
  call void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %255, %class.btVector3* %256, i32 %257, %class.btVector3* nonnull align 4 dereferenceable(16) %call200)
  store i32 1, i32* %cleanup.dest.slot, align 4
  %260 = bitcast i32* %numPoints198 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %260) #8
  %261 = bitcast %class.btVector3** %points196 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %261) #8
  %262 = bitcast %class.btConvexHullShape** %convexHullShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %262) #8
  br label %return

sw.default204:                                    ; preds = %entry
  %263 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %264 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %264, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 17
  %265 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %265(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %263)
  br label %return

return:                                           ; preds = %sw.default204, %sw.bb195, %sw.bb188, %if.end172, %cleanup, %sw.bb24, %sw.bb4, %sw.bb
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !12
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !12
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !12
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  ret %class.btVector3* %m_implicitShapeDimensions
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFselfff(float %a, float %b, float %c) #5 comdat {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  %c.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4, !tbaa !12
  store float %b, float* %b.addr, align 4, !tbaa !12
  store float %c, float* %c.addr, align 4, !tbaa !12
  %0 = load float, float* %a.addr, align 4, !tbaa !12
  %cmp = fcmp oge float %0, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load float, float* %b.addr, align 4, !tbaa !12
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load float, float* %c.addr, align 4, !tbaa !12
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %1, %cond.true ], [ %2, %cond.false ]
  ret float %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !12
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !12
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !12
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !12
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !12
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !12
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !12
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4, !tbaa !17
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #5 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !12
  %0 = load float, float* %y.addr, align 4, !tbaa !12
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

define linkonce_odr hidden float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4, !tbaa !19
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  ret float %2
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4, !tbaa !19
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %radiusAxis = alloca i32, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast i32* %radiusAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4, !tbaa !19
  %add = add nsw i32 %1, 2
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %radiusAxis, align 4, !tbaa !16
  %2 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %3 = load i32, i32* %radiusAxis, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !12
  %5 = bitcast i32* %radiusAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret float %4
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !12
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !12
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !12
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !12
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !12
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !12
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !12
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !12
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !12
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !12
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !12
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !12
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !12
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !12
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !12
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !12
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #4 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !12
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !12
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !12
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !12
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !12
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4, !tbaa !21
  ret float %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN23btConvexPointCloudShape17getUnscaledPointsEv(%class.btConvexPointCloudShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4, !tbaa !24
  ret %class.btVector3* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK23btConvexPointCloudShape12getNumPointsEv(%class.btConvexPointCloudShape* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_numPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numPoints, align 4, !tbaa !26
  ret i32 %0
}

define internal void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirOrg, %class.btVector3* %points, i32 %numPoints, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) #0 {
entry:
  %localDirOrg.addr = alloca %class.btVector3*, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %numPoints.addr = alloca i32, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  %maxDot = alloca float, align 4
  %ptIndex = alloca i32, align 4
  store %class.btVector3* %localDirOrg, %class.btVector3** %localDirOrg.addr, align 4, !tbaa !2
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4, !tbaa !2
  store i32 %numPoints, i32* %numPoints.addr, align 4, !tbaa !16
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %localDirOrg.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %points.addr, align 4, !tbaa !2
  %6 = load i32, i32* %numPoints.addr, align 4, !tbaa !16
  %call = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %5, i32 %6, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call, i32* %ptIndex, align 4, !tbaa !27
  %7 = load %class.btVector3*, %class.btVector3** %points.addr, align 4, !tbaa !2
  %8 = load i32, i32* %ptIndex, align 4, !tbaa !27
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

define linkonce_odr hidden %class.btVector3* @_ZN17btConvexHullShape17getUnscaledPointsEv(%class.btConvexHullShape* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints, i32 0)
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK17btConvexHullShape12getNumPointsEv(%class.btConvexHullShape* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  ret i32 %call
}

define hidden void @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %localDir) #0 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %localDir.addr = alloca %class.btVector3*, align 4
  %localDirNorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %localDir, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btVector3* %localDirNorm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %localDirNorm to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !14
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %localDirNorm)
  %cmp = fcmp olt float %call, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float -1.000000e+00, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float -1.000000e+00, float* %ref.tmp2, align 4, !tbaa !12
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float -1.000000e+00, float* %ref.tmp3, align 4, !tbaa !12
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localDirNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %localDirNorm)
  %10 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #8
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirNorm)
  %11 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %call8 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %this1)
  store float %call8, float* %ref.tmp7, align 4, !tbaa !12
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirNorm)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #8
  %16 = bitcast %class.btVector3* %localDirNorm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !12
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

define hidden float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %this) #0 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btConvexShape*, align 4
  %sphereShape = alloca %class.btSphereShape*, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %cylShape = alloca %class.btCylinderShape*, align 4
  %conShape = alloca %class.btConeShape*, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %convexHullShape = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4, !tbaa !8
  switch i32 %1, label %sw.default [
    i32 8, label %sw.bb
    i32 0, label %sw.bb2
    i32 1, label %sw.bb4
    i32 13, label %sw.bb6
    i32 11, label %sw.bb8
    i32 10, label %sw.bb10
    i32 5, label %sw.bb12
    i32 4, label %sw.bb12
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast %class.btSphereShape** %sphereShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast %class.btConvexShape* %this1 to %class.btSphereShape*
  store %class.btSphereShape* %3, %class.btSphereShape** %sphereShape, align 4, !tbaa !2
  %4 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4, !tbaa !2
  %call = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %4)
  store float %call, float* %retval, align 4
  %5 = bitcast %class.btSphereShape** %sphereShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  br label %return

sw.bb2:                                           ; preds = %entry
  %6 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %7, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %8 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %9 = bitcast %class.btBoxShape* %8 to %class.btConvexInternalShape*
  %call3 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %9)
  store float %call3, float* %retval, align 4
  %10 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  br label %return

sw.bb4:                                           ; preds = %entry
  %11 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %12 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %12, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %13 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %14 = bitcast %class.btTriangleShape* %13 to %class.btConvexInternalShape*
  %call5 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %14)
  store float %call5, float* %retval, align 4
  %15 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %return

sw.bb6:                                           ; preds = %entry
  %16 = bitcast %class.btCylinderShape** %cylShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = bitcast %class.btConvexShape* %this1 to %class.btCylinderShape*
  store %class.btCylinderShape* %17, %class.btCylinderShape** %cylShape, align 4, !tbaa !2
  %18 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4, !tbaa !2
  %19 = bitcast %class.btCylinderShape* %18 to %class.btConvexInternalShape*
  %call7 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %19)
  store float %call7, float* %retval, align 4
  %20 = bitcast %class.btCylinderShape** %cylShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  br label %return

sw.bb8:                                           ; preds = %entry
  %21 = bitcast %class.btConeShape** %conShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = bitcast %class.btConvexShape* %this1 to %class.btConeShape*
  store %class.btConeShape* %22, %class.btConeShape** %conShape, align 4, !tbaa !2
  %23 = load %class.btConeShape*, %class.btConeShape** %conShape, align 4, !tbaa !2
  %24 = bitcast %class.btConeShape* %23 to %class.btConvexInternalShape*
  %call9 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %24)
  store float %call9, float* %retval, align 4
  %25 = bitcast %class.btConeShape** %conShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  br label %return

sw.bb10:                                          ; preds = %entry
  %26 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %27 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %27, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %28 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %29 = bitcast %class.btCapsuleShape* %28 to %class.btConvexInternalShape*
  %call11 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %29)
  store float %call11, float* %retval, align 4
  %30 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  br label %return

sw.bb12:                                          ; preds = %entry, %entry
  %31 = bitcast %class.btPolyhedralConvexShape** %convexHullShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  %32 = bitcast %class.btConvexShape* %this1 to %class.btPolyhedralConvexShape*
  store %class.btPolyhedralConvexShape* %32, %class.btPolyhedralConvexShape** %convexHullShape, align 4, !tbaa !2
  %33 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %convexHullShape, align 4, !tbaa !2
  %34 = bitcast %class.btPolyhedralConvexShape* %33 to %class.btConvexInternalShape*
  %call13 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %34)
  store float %call13, float* %retval, align 4
  %35 = bitcast %class.btPolyhedralConvexShape** %convexHullShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %return

sw.default:                                       ; preds = %entry
  %36 = bitcast %class.btConvexShape* %this1 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %36, align 4, !tbaa !6
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %37 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call14 = call float %37(%class.btConvexShape* %this1)
  store float %call14, float* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb12, %sw.bb10, %sw.bb8, %sw.bb6, %sw.bb4, %sw.bb2, %sw.bb
  %38 = load float, float* %retval, align 4
  ret float %38
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_implicitShapeDimensions)
  %1 = load float, float* %call, align 4, !tbaa !12
  %2 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling)
  %3 = load float, float* %call2, align 4, !tbaa !12
  %mul = fmul float %1, %3
  ret float %mul
}

define hidden void @_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_(%class.btConvexShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %sphereShape = alloca %class.btSphereShape*, align 4
  %radius = alloca float, align 4
  %margin = alloca float, align 4
  %center = alloca %class.btVector3*, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %margin8 = alloca float, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center15 = alloca %class.btVector3, align 4
  %extent17 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %margin24 = alloca float, align 4
  %i = alloca i32, align 4
  %vec = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %sv = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %tmp = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %halfExtents49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %m_upAxis = alloca i32, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %abs_b72 = alloca %class.btMatrix3x3, align 4
  %center74 = alloca %class.btVector3, align 4
  %extent76 = alloca %class.btVector3, align 4
  %ref.tmp80 = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %convexHullShape = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %margin83 = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4, !tbaa !8
  switch i32 %1, label %sw.default [
    i32 8, label %sw.bb
    i32 13, label %sw.bb7
    i32 0, label %sw.bb7
    i32 1, label %sw.bb23
    i32 10, label %sw.bb48
    i32 5, label %sw.bb82
    i32 4, label %sw.bb82
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast %class.btSphereShape** %sphereShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast %class.btConvexShape* %this1 to %class.btSphereShape*
  store %class.btSphereShape* %3, %class.btSphereShape** %sphereShape, align 4, !tbaa !2
  %4 = bitcast float* %radius to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4, !tbaa !2
  %6 = bitcast %class.btSphereShape* %5 to %class.btConvexInternalShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %6)
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call)
  %7 = load float, float* %call2, align 4, !tbaa !12
  store float %7, float* %radius, align 4, !tbaa !12
  %8 = bitcast float* %margin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load float, float* %radius, align 4, !tbaa !12
  %10 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4, !tbaa !2
  %11 = bitcast %class.btSphereShape* %10 to %class.btConvexShape*
  %call3 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %11)
  %add = fadd float %9, %call3
  store float %add, float* %margin, align 4, !tbaa !12
  %12 = bitcast %class.btVector3** %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %13)
  store %class.btVector3* %call4, %class.btVector3** %center, align 4, !tbaa !2
  %14 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #8
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %extent, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin)
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %16 = load %class.btVector3*, %class.btVector3** %center, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %17 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %18 = bitcast %class.btVector3* %17 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !14
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #8
  %21 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = load %class.btVector3*, %class.btVector3** %center, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %23 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %23 to i8*
  %25 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !14
  %26 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  %27 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %27) #8
  %28 = bitcast %class.btVector3** %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast float* %margin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  %30 = bitcast float* %radius to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  %31 = bitcast %class.btSphereShape** %sphereShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry, %entry
  %32 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #8
  %33 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %33, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %34 = bitcast float* %margin8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %36 = bitcast %class.btBoxShape* %35 to %class.btConvexShape*
  %call9 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %36)
  store float %call9, float* %margin8, align 4, !tbaa !12
  %37 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %37) #8
  %38 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4, !tbaa !2
  %39 = bitcast %class.btBoxShape* %38 to %class.btConvexInternalShape*
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %39)
  %40 = bitcast %class.btVector3* %halfExtents to i8*
  %41 = bitcast %class.btVector3* %call10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false), !tbaa.struct !14
  %42 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp11, float* nonnull align 4 dereferenceable(4) %margin8, float* nonnull align 4 dereferenceable(4) %margin8, float* nonnull align 4 dereferenceable(4) %margin8)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %43 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %44) #8
  %45 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %45)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call14)
  %46 = bitcast %class.btVector3* %center15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %46) #8
  %47 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %47)
  %48 = bitcast %class.btVector3* %center15 to i8*
  %49 = bitcast %class.btVector3* %call16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !14
  %50 = bitcast %class.btVector3* %extent17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #8
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent17, %class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call18, %class.btVector3* nonnull align 4 dereferenceable(16) %call19, %class.btVector3* nonnull align 4 dereferenceable(16) %call20)
  %51 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %center15, %class.btVector3* nonnull align 4 dereferenceable(16) %extent17)
  %52 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %53 = bitcast %class.btVector3* %52 to i8*
  %54 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false), !tbaa.struct !14
  %55 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %56 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %center15, %class.btVector3* nonnull align 4 dereferenceable(16) %extent17)
  %57 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %58 = bitcast %class.btVector3* %57 to i8*
  %59 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false), !tbaa.struct !14
  %60 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %60) #8
  %61 = bitcast %class.btVector3* %extent17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #8
  %62 = bitcast %class.btVector3* %center15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %62) #8
  %63 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %63) #8
  %64 = bitcast %class.btVector3* %halfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast float* %margin8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  %66 = bitcast %class.btBoxShape** %convexShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #8
  br label %sw.epilog

sw.bb23:                                          ; preds = %entry
  %67 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  %68 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %68, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %69 = bitcast float* %margin24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #8
  %70 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4, !tbaa !2
  %71 = bitcast %class.btTriangleShape* %70 to %class.btConvexShape*
  %call25 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %71)
  store float %call25, float* %margin24, align 4, !tbaa !12
  %72 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #8
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb23
  %73 = load i32, i32* %i, align 4, !tbaa !16
  %cmp = icmp slt i32 %73, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %74 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %74) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %75 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %75) #8
  %76 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  store float 0.000000e+00, float* %ref.tmp26, align 4, !tbaa !12
  %77 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #8
  store float 0.000000e+00, float* %ref.tmp27, align 4, !tbaa !12
  %78 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #8
  store float 0.000000e+00, float* %ref.tmp28, align 4, !tbaa !12
  %call29 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %79 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  %80 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  %81 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #8
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vec)
  %82 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds float, float* %call30, i32 %82
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !12
  %83 = bitcast %class.btVector3* %sv to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %83) #8
  %84 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %84) #8
  %85 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call32 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %85)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call32)
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %sv, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %86 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %86) #8
  %87 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %87) #8
  %88 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %tmp, %class.btTransform* %88, %class.btVector3* nonnull align 4 dereferenceable(16) %sv)
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %89 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %89
  %90 = load float, float* %arrayidx34, align 4, !tbaa !12
  %91 = load float, float* %margin24, align 4, !tbaa !12
  %add35 = fadd float %90, %91
  %92 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %92)
  %93 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 %93
  store float %add35, float* %arrayidx37, align 4, !tbaa !12
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vec)
  %94 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %94
  store float -1.000000e+00, float* %arrayidx39, align 4, !tbaa !12
  %95 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %95) #8
  %96 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %97 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %97) #8
  %98 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #8
  %99 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %99)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call43)
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp40, %class.btTransform* %96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %100 = bitcast %class.btVector3* %tmp to i8*
  %101 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 16, i1 false), !tbaa.struct !14
  %102 = bitcast %class.btVector3* %ref.tmp42 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %102) #8
  %103 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #8
  %104 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %104) #8
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %105 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %105
  %106 = load float, float* %arrayidx45, align 4, !tbaa !12
  %107 = load float, float* %margin24, align 4, !tbaa !12
  %sub = fsub float %106, %107
  %108 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %108)
  %109 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 %109
  store float %sub, float* %arrayidx47, align 4, !tbaa !12
  %110 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %110) #8
  %111 = bitcast %class.btVector3* %sv to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #8
  %112 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %112) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %113 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %113, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %114 = bitcast float* %margin24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  %115 = bitcast %class.btTriangleShape** %triangleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #8
  br label %sw.epilog

sw.bb48:                                          ; preds = %entry
  %116 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #8
  %117 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %117, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %118 = bitcast %class.btVector3* %halfExtents49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %118) #8
  %119 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #8
  %120 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call51 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %120)
  store float %call51, float* %ref.tmp50, align 4, !tbaa !12
  %121 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #8
  %122 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call53 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %122)
  store float %call53, float* %ref.tmp52, align 4, !tbaa !12
  %123 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #8
  %124 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call55 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %124)
  store float %call55, float* %ref.tmp54, align 4, !tbaa !12
  %call56 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %halfExtents49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp54)
  %125 = bitcast float* %ref.tmp54 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %125) #8
  %126 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  %127 = bitcast float* %ref.tmp50 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %127) #8
  %128 = bitcast i32* %m_upAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #8
  %129 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call57 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %129)
  store i32 %call57, i32* %m_upAxis, align 4, !tbaa !16
  %130 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call58 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %130)
  %131 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %call59 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %131)
  %add60 = fadd float %call58, %call59
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents49)
  %132 = load i32, i32* %m_upAxis, align 4, !tbaa !16
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 %132
  store float %add60, float* %arrayidx62, align 4, !tbaa !12
  %133 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %133) #8
  %134 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %134) #8
  %135 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %136 = bitcast %class.btCapsuleShape* %135 to %class.btConvexShape*
  %call65 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %136)
  store float %call65, float* %ref.tmp64, align 4, !tbaa !12
  %137 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #8
  %138 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %139 = bitcast %class.btCapsuleShape* %138 to %class.btConvexShape*
  %call67 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %139)
  store float %call67, float* %ref.tmp66, align 4, !tbaa !12
  %140 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %140) #8
  %141 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4, !tbaa !2
  %142 = bitcast %class.btCapsuleShape* %141 to %class.btConvexShape*
  %call69 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %142)
  store float %call69, float* %ref.tmp68, align 4, !tbaa !12
  %call70 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp68)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %halfExtents49, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp63)
  %143 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #8
  %144 = bitcast float* %ref.tmp66 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #8
  %145 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #8
  %146 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %146) #8
  %147 = bitcast %class.btMatrix3x3* %abs_b72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %147) #8
  %148 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call73 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %148)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b72, %class.btMatrix3x3* %call73)
  %149 = bitcast %class.btVector3* %center74 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %149) #8
  %150 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %call75 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %150)
  %151 = bitcast %class.btVector3* %center74 to i8*
  %152 = bitcast %class.btVector3* %call75 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %151, i8* align 4 %152, i32 16, i1 false), !tbaa.struct !14
  %153 = bitcast %class.btVector3* %extent76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %153) #8
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b72, i32 0)
  %call78 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b72, i32 1)
  %call79 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b72, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent76, %class.btVector3* %halfExtents49, %class.btVector3* nonnull align 4 dereferenceable(16) %call77, %class.btVector3* nonnull align 4 dereferenceable(16) %call78, %class.btVector3* nonnull align 4 dereferenceable(16) %call79)
  %154 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %154) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp80, %class.btVector3* nonnull align 4 dereferenceable(16) %center74, %class.btVector3* nonnull align 4 dereferenceable(16) %extent76)
  %155 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %156 = bitcast %class.btVector3* %155 to i8*
  %157 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %156, i8* align 4 %157, i32 16, i1 false), !tbaa.struct !14
  %158 = bitcast %class.btVector3* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %158) #8
  %159 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %159) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp81, %class.btVector3* nonnull align 4 dereferenceable(16) %center74, %class.btVector3* nonnull align 4 dereferenceable(16) %extent76)
  %160 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %161 = bitcast %class.btVector3* %160 to i8*
  %162 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %161, i8* align 4 %162, i32 16, i1 false), !tbaa.struct !14
  %163 = bitcast %class.btVector3* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %163) #8
  %164 = bitcast %class.btVector3* %extent76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %164) #8
  %165 = bitcast %class.btVector3* %center74 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #8
  %166 = bitcast %class.btMatrix3x3* %abs_b72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %166) #8
  %167 = bitcast i32* %m_upAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #8
  %168 = bitcast %class.btVector3* %halfExtents49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %168) #8
  %169 = bitcast %class.btCapsuleShape** %capsuleShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #8
  br label %sw.epilog

sw.bb82:                                          ; preds = %entry, %entry
  %170 = bitcast %class.btPolyhedralConvexAabbCachingShape** %convexHullShape to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %170) #8
  %171 = bitcast %class.btConvexShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  store %class.btPolyhedralConvexAabbCachingShape* %171, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4, !tbaa !2
  %172 = bitcast float* %margin83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %172) #8
  %173 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4, !tbaa !2
  %174 = bitcast %class.btPolyhedralConvexAabbCachingShape* %173 to %class.btConvexShape*
  %call84 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %174)
  store float %call84, float* %margin83, align 4, !tbaa !12
  %175 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4, !tbaa !2
  %176 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %177 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %178 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %179 = load float, float* %margin83, align 4, !tbaa !12
  call void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %175, %class.btTransform* nonnull align 4 dereferenceable(64) %176, %class.btVector3* nonnull align 4 dereferenceable(16) %177, %class.btVector3* nonnull align 4 dereferenceable(16) %178, float %179)
  %180 = bitcast float* %margin83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #8
  %181 = bitcast %class.btPolyhedralConvexAabbCachingShape** %convexHullShape to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %182 = load %class.btTransform*, %class.btTransform** %t.addr, align 4, !tbaa !2
  %183 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %184 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %185 = bitcast %class.btConvexShape* %this1 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %185, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %186 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %186(%class.btConvexShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %182, %class.btVector3* nonnull align 4 dereferenceable(16) %183, %class.btVector3* nonnull align 4 dereferenceable(16) %184)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb82, %sw.bb48, %for.end, %sw.bb7, %sw.bb
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !12
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !12
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !12
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !12
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !12
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !12
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !12
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !12
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #4 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %2 = load float, float* %call, align 4, !tbaa !12
  %call2 = call float @_Z6btFabsf(float %2)
  store float %call2, float* %ref.tmp, align 4, !tbaa !12
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4, !tbaa !12
  %call7 = call float @_Z6btFabsf(float %4)
  store float %call7, float* %ref.tmp3, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %6 = load float, float* %call11, align 4, !tbaa !12
  %call12 = call float @_Z6btFabsf(float %6)
  store float %call12, float* %ref.tmp8, align 4, !tbaa !12
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %8 = load float, float* %call16, align 4, !tbaa !12
  %call17 = call float @_Z6btFabsf(float %8)
  store float %call17, float* %ref.tmp13, align 4, !tbaa !12
  %9 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %10 = load float, float* %call21, align 4, !tbaa !12
  %call22 = call float @_Z6btFabsf(float %10)
  store float %call22, float* %ref.tmp18, align 4, !tbaa !12
  %11 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %12 = load float, float* %call26, align 4, !tbaa !12
  %call27 = call float @_Z6btFabsf(float %12)
  store float %call27, float* %ref.tmp23, align 4, !tbaa !12
  %13 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %14 = load float, float* %call31, align 4, !tbaa !12
  %call32 = call float @_Z6btFabsf(float %14)
  store float %call32, float* %ref.tmp28, align 4, !tbaa !12
  %15 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %16 = load float, float* %call36, align 4, !tbaa !12
  %call37 = call float @_Z6btFabsf(float %16)
  store float %call37, float* %ref.tmp33, align 4, !tbaa !12
  %17 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %18 = load float, float* %call41, align 4, !tbaa !12
  %call42 = call float @_Z6btFabsf(float %18)
  store float %call42, float* %ref.tmp38, align 4, !tbaa !12
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  %19 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  %23 = bitcast float* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #8
  %24 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  %25 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #8
  %26 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #8
  %27 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !16
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, float %margin) #4 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !12
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %0 = load float, float* %margin.addr, align 4, !tbaa !12
  %1 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, float %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #6

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #6

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #6

define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 1.000000e+00, float* %ref.tmp, align 4, !tbaa !12
  %1 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !12
  %2 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #6

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #6

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !12
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !12
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !12
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !12
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !12
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !12
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !12
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !12
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !12
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !12
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !12
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !12
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #4 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !12
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !12
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !12
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !12
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !12
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !12
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !16
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #4 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !12
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !12
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !12
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !12
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !12
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !12
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !12
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !12
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !12
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4, !tbaa !2
  store i32 %array_count, i32* %array_count.addr, align 4, !tbaa !27
  store float* %dotOut, float** %dotOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store float 0xC7EFFFFFE0000000, float* %maxDot, align 4, !tbaa !12
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !16
  %2 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 -1, i32* %ptIndex, align 4, !tbaa !16
  store i32 0, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4, !tbaa !16
  %4 = load i32, i32* %array_count.addr, align 4, !tbaa !27
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast float* %dot to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %array.addr, align 4, !tbaa !2
  %7 = load i32, i32* %i, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4, !tbaa !12
  %8 = load float, float* %dot, align 4, !tbaa !12
  %9 = load float, float* %maxDot, align 4, !tbaa !12
  %cmp2 = fcmp ogt float %8, %9
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load float, float* %dot, align 4, !tbaa !12
  store float %10, float* %maxDot, align 4, !tbaa !12
  %11 = load i32, i32* %i, align 4, !tbaa !16
  store i32 %11, i32* %ptIndex, align 4, !tbaa !16
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %12 = bitcast float* %dot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !16
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4, !tbaa !16
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load float, float* %maxDot, align 4, !tbaa !12
  %15 = load float*, float** %dotOut.addr, align 4, !tbaa !2
  store float %14, float* %15, align 4, !tbaa !12
  %16 = load i32, i32* %ptIndex, align 4, !tbaa !16
  %17 = bitcast i32* %ptIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast float* %maxDot to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret i32 %16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !16
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !29
  %1 = load i32, i32* %n.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !33
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !12
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !12
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btFabsf(float %x) #5 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !12
  %0 = load float, float* %x.addr, align 4, !tbaa !12
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store float* %xx, float** %xx.addr, align 4, !tbaa !2
  store float* %xy, float** %xy.addr, align 4, !tbaa !2
  store float* %xz, float** %xz.addr, align 4, !tbaa !2
  store float* %yx, float** %yx.addr, align 4, !tbaa !2
  store float* %yy, float** %yy.addr, align 4, !tbaa !2
  store float* %yz, float** %yz.addr, align 4, !tbaa !2
  store float* %zx, float** %zx.addr, align 4, !tbaa !2
  store float* %zy, float** %zy.addr, align 4, !tbaa !2
  store float* %zz, float** %zz.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !2
  %1 = load float*, float** %xy.addr, align 4, !tbaa !2
  %2 = load float*, float** %xz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !2
  %4 = load float*, float** %yy.addr, align 4, !tbaa !2
  %5 = load float*, float** %yz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !2
  %7 = load float*, float** %zy.addr, align 4, !tbaa !2
  %8 = load float*, float** %zz.addr, align 4, !tbaa !2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #4 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  store float %margin, float* %margin.addr, align 4, !tbaa !12
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 5.000000e-01, float* %ref.tmp, align 4, !tbaa !12
  %2 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #8
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %5 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %8 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #8
  %9 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #8
  %10 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !12
  %11 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4, !tbaa !2
  %13 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %16) #8
  %17 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %18 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %18) #8
  %19 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4, !tbaa !2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %20 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  %21 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %22 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %22 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !14
  %25 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %27 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4, !tbaa !2
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false), !tbaa.struct !14
  %30 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast %class.btVector3* %extent to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %32 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  %33 = bitcast %class.btMatrix3x3* %abs_b to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %33) #8
  %34 = bitcast %class.btVector3* %localCenter to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #8
  %35 = bitcast %class.btVector3* %localHalfExtents to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #8
  ret void
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { cold noreturn nounwind }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !10, i64 4}
!9 = !{!"_ZTS16btCollisionShape", !10, i64 4, !3, i64 8}
!10 = !{!"int", !4, i64 0}
!11 = !{!9, !3, i64 8}
!12 = !{!13, !13, i64 0}
!13 = !{!"float", !4, i64 0}
!14 = !{i64 0, i64 16, !15}
!15 = !{!4, !4, i64 0}
!16 = !{!10, !10, i64 0}
!17 = !{!18, !10, i64 52}
!18 = !{!"_ZTS15btCylinderShape", !10, i64 52}
!19 = !{!20, !10, i64 52}
!20 = !{!"_ZTS14btCapsuleShape", !10, i64 52}
!21 = !{!22, !13, i64 44}
!22 = !{!"_ZTS21btConvexInternalShape", !23, i64 12, !23, i64 28, !13, i64 44, !13, i64 48}
!23 = !{!"_ZTS9btVector3", !4, i64 0}
!24 = !{!25, !3, i64 92}
!25 = !{!"_ZTS23btConvexPointCloudShape", !3, i64 92, !10, i64 96}
!26 = !{!25, !10, i64 96}
!27 = !{!28, !28, i64 0}
!28 = !{!"long", !4, i64 0}
!29 = !{!30, !3, i64 12}
!30 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !31, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !32, i64 16}
!31 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!32 = !{!"bool", !4, i64 0}
!33 = !{!30, !10, i64 4}
