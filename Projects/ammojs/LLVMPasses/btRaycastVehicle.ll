; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Vehicle/btRaycastVehicle.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Vehicle/btRaycastVehicle.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btRaycastVehicle = type { %class.btActionInterface, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5, i32, i32, float, float, %struct.btVehicleRaycaster*, float, float, float, %class.btRigidBody*, i32, i32, i32, %class.btAlignedObjectArray.9 }
%class.btActionInterface = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btWheelInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btWheelInfo = type { %"struct.btWheelInfo::RaycastInfo", %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i8, i8*, float, float, float, float }
%"struct.btWheelInfo::RaycastInfo" = type { %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, i8, i8* }
%"class.btRaycastVehicle::btVehicleTuning" = type { float, float, float, float, float, float }
%struct.btVehicleRaycaster = type { i32 (...)** }
%struct.btWheelInfoConstructionInfo = type <{ %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%"struct.btVehicleRaycaster::btVehicleRaycasterResult" = type { %class.btVector3, %class.btVector3, float }
%struct.btWheelContactPoint = type { %class.btRigidBody*, %class.btRigidBody*, %class.btVector3, %class.btVector3, float, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btDefaultVehicleRaycaster = type { %struct.btVehicleRaycaster, %class.btDynamicsWorld* }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray.13, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray.13 = type <{ %class.btAlignedAllocator.14, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.14 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }
%"struct.btCollisionWorld::ClosestRayResultCallback" = type { %"struct.btCollisionWorld::RayResultCallback", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i16, i16, i32 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.13, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%"struct.btCollisionWorld::LocalRayResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btRigidBodyD2Ev = comdat any

$_ZN17btActionInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN27btWheelInfoConstructionInfoC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_ = comdat any

$_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoEixEi = comdat any

$_ZNK16btRaycastVehicle12getNumWheelsEv = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btTransform8setBasisERK11btMatrix3x3 = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN16btRaycastVehicle12getRigidBodyEv = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btRigidBody23getCenterOfMassPositionEv = comdat any

$_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3 = comdat any

$_ZNK16btRaycastVehicle12getRigidBodyEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btRigidBody12applyImpulseERK9btVector3S2_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f = comdat any

$_Z6btSqrtf = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK16btRaycastVehicle12getRightAxisEv = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_ = comdat any

$_ZNK16btCollisionWorld17RayResultCallback6hasHitEv = comdat any

$_ZN11btRigidBody6upcastEPK17btCollisionObject = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZN18btVehicleRaycasterD2Ev = comdat any

$_ZN25btDefaultVehicleRaycasterD0Ev = comdat any

$_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf = comdat any

$_ZN16btRaycastVehicle19setCoordinateSystemEiii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN17btActionInterfaceD2Ev = comdat any

$_ZN17btActionInterfaceD0Ev = comdat any

$_ZN11btWheelInfo11RaycastInfoC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z5btCosf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN11btRigidBody19applyCentralImpulseERK9btVector3 = comdat any

$_ZN11btRigidBody18applyTorqueImpulseERK9btVector3 = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btRigidBody24getInvInertiaTensorWorldEv = comdat any

$_ZN16btCollisionWorld17RayResultCallbackC2Ev = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev = comdat any

$_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy = comdat any

$_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb = comdat any

$_ZN16btCollisionWorld17RayResultCallbackD2Ev = comdat any

$_ZN16btCollisionWorld17RayResultCallbackD0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi = comdat any

$_ZN11btWheelInfoC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZTS18btVehicleRaycaster = comdat any

$_ZTI18btVehicleRaycaster = comdat any

$_ZTS17btActionInterface = comdat any

$_ZTI17btActionInterface = comdat any

$_ZTV17btActionInterface = comdat any

$_ZTVN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTSN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTSN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZTIN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZTIN16btCollisionWorld24ClosestRayResultCallbackE = comdat any

$_ZTVN16btCollisionWorld17RayResultCallbackE = comdat any

@_ZZN17btActionInterface12getFixedBodyEvE7s_fixed = internal global %class.btRigidBody zeroinitializer, align 4
@_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@_ZTV16btRaycastVehicle = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btRaycastVehicle to i8*), i8* bitcast (%class.btRaycastVehicle* (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD1Ev to i8*), i8* bitcast (void (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD0Ev to i8*), i8* bitcast (void (%class.btRaycastVehicle*, %class.btCollisionWorld*, float)* @_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, %class.btIDebugDraw*)* @_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw to i8*), i8* bitcast (void (%class.btRaycastVehicle*, float)* @_ZN16btRaycastVehicle13updateVehicleEf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, float)* @_ZN16btRaycastVehicle14updateFrictionEf to i8*), i8* bitcast (void (%class.btRaycastVehicle*, i32, i32, i32)* @_ZN16btRaycastVehicle19setCoordinateSystemEiii to i8*)] }, align 4
@sideFrictionStiffness2 = hidden global float 1.000000e+00, align 4
@_ZTV25btDefaultVehicleRaycaster = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI25btDefaultVehicleRaycaster to i8*), i8* bitcast (%struct.btVehicleRaycaster* (%struct.btVehicleRaycaster*)* @_ZN18btVehicleRaycasterD2Ev to i8*), i8* bitcast (void (%class.btDefaultVehicleRaycaster*)* @_ZN25btDefaultVehicleRaycasterD0Ev to i8*), i8* bitcast (i8* (%class.btDefaultVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)* @_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS25btDefaultVehicleRaycaster = hidden constant [28 x i8] c"25btDefaultVehicleRaycaster\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btVehicleRaycaster = linkonce_odr hidden constant [21 x i8] c"18btVehicleRaycaster\00", comdat, align 1
@_ZTI18btVehicleRaycaster = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btVehicleRaycaster, i32 0, i32 0) }, comdat, align 4
@_ZTI25btDefaultVehicleRaycaster = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btDefaultVehicleRaycaster, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI18btVehicleRaycaster to i8*) }, align 4
@_ZTS16btRaycastVehicle = hidden constant [19 x i8] c"16btRaycastVehicle\00", align 1
@_ZTS17btActionInterface = linkonce_odr hidden constant [20 x i8] c"17btActionInterface\00", comdat, align 1
@_ZTI17btActionInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btActionInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI16btRaycastVehicle = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btRaycastVehicle, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*) }, align 4
@_ZTV11btRigidBody = external unnamed_addr constant { [9 x i8*] }, align 4
@_ZTV17btActionInterface = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btActionInterface to i8*), i8* bitcast (%class.btActionInterface* (%class.btActionInterface*)* @_ZN17btActionInterfaceD2Ev to i8*), i8* bitcast (void (%class.btActionInterface*)* @_ZN17btActionInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btCollisionWorld24ClosestRayResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::RayResultCallback"* (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::ClosestRayResultCallback"*)* @_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (float (%"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)* @_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb to i8*)] }, comdat, align 4
@_ZTSN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden constant [47 x i8] c"N16btCollisionWorld24ClosestRayResultCallbackE\00", comdat, align 1
@_ZTSN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden constant [40 x i8] c"N16btCollisionWorld17RayResultCallbackE\00", comdat, align 1
@_ZTIN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([40 x i8], [40 x i8]* @_ZTSN16btCollisionWorld17RayResultCallbackE, i32 0, i32 0) }, comdat, align 4
@_ZTIN16btCollisionWorld24ClosestRayResultCallbackE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([47 x i8], [47 x i8]* @_ZTSN16btCollisionWorld24ClosestRayResultCallbackE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld17RayResultCallbackE to i8*) }, comdat, align 4
@_ZTVN16btCollisionWorld17RayResultCallbackE = linkonce_odr hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (%"struct.btCollisionWorld::RayResultCallback"* (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD2Ev to i8*), i8* bitcast (void (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD0Ev to i8*), i8* bitcast (i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)* @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4

@_ZN16btRaycastVehicleC1ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster = hidden unnamed_addr alias %class.btRaycastVehicle* (%class.btRaycastVehicle*, %"class.btRaycastVehicle::btVehicleTuning"*, %class.btRigidBody*, %struct.btVehicleRaycaster*), %class.btRaycastVehicle* (%class.btRaycastVehicle*, %"class.btRaycastVehicle::btVehicleTuning"*, %class.btRigidBody*, %struct.btVehicleRaycaster*)* @_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster
@_ZN16btRaycastVehicleD1Ev = hidden unnamed_addr alias %class.btRaycastVehicle* (%class.btRaycastVehicle*), %class.btRaycastVehicle* (%class.btRaycastVehicle*)* @_ZN16btRaycastVehicleD2Ev

define hidden nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btActionInterface12getFixedBodyEv() #0 {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed) #1
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #1
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #1
  store float 0.000000e+00, float* %ref.tmp1, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !3
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btMotionState* null, %class.btCollisionShape* null, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #1
  %9 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #1
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #1
  %11 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #1
  call void @__cxa_guard_release(i32* @_ZGVZN17btActionInterface12getFixedBodyEvE7s_fixed) #1
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %12 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #1
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !3
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !3
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #1
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !3
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %16 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  %18 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #1
  %19 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %19) #1
  ret %class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store float* %_x, float** %_x.addr, align 4, !tbaa !7
  store float* %_y, float** %_y.addr, align 4, !tbaa !7
  store float* %_z, float** %_z.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !3
  %2 = load float*, float** %_y.addr, align 4, !tbaa !7
  %3 = load float, float* %2, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !3
  %4 = load float*, float** %_z.addr, align 4, !tbaa !7
  %5 = load float, float* %4, align 4, !tbaa !3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !3
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !3
  ret %class.btVector3* %this1
}

declare %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* returned, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4, !tbaa !7
  %call = call %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* @_ZZN17btActionInterface12getFixedBodyEvE7s_fixed) #1
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !9
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray* %m_constraintRefs) #1
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #1
  ret %class.btRigidBody* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #1

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #1

declare void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

define hidden %class.btRaycastVehicle* @_ZN16btRaycastVehicleC2ERKNS_15btVehicleTuningEP11btRigidBodyP18btVehicleRaycaster(%class.btRaycastVehicle* returned %this, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning, %class.btRigidBody* %chassis, %struct.btVehicleRaycaster* %raycaster) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  %chassis.addr = alloca %class.btRigidBody*, align 4
  %raycaster.addr = alloca %struct.btVehicleRaycaster*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  store %class.btRigidBody* %chassis, %class.btRigidBody** %chassis.addr, align 4, !tbaa !7
  store %struct.btVehicleRaycaster* %raycaster, %struct.btVehicleRaycaster** %raycaster.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %class.btRaycastVehicle* %this1 to %class.btActionInterface*
  %call = call %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* %0) #1
  %1 = bitcast %class.btRaycastVehicle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV16btRaycastVehicle, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !9
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.1* %m_forwardWS)
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.1* %m_axle)
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.5* %m_forwardImpulse)
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %call5 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.5* %m_sideImpulse)
  %m_vehicleRaycaster = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 9
  %2 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %raycaster.addr, align 4, !tbaa !7
  store %struct.btVehicleRaycaster* %2, %struct.btVehicleRaycaster** %m_vehicleRaycaster, align 4, !tbaa !11
  %m_pitchControl = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_pitchControl, align 4, !tbaa !21
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call6 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev(%class.btAlignedObjectArray.9* %m_wheelInfo)
  %3 = load %class.btRigidBody*, %class.btRigidBody** %chassis.addr, align 4, !tbaa !7
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  store %class.btRigidBody* %3, %class.btRigidBody** %m_chassisBody, align 4, !tbaa !22
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  store i32 0, i32* %m_indexRightAxis, align 4, !tbaa !23
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  store i32 2, i32* %m_indexUpAxis, align 4, !tbaa !24
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  store i32 1, i32* %m_indexForwardAxis, align 4, !tbaa !25
  %4 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  call void @_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE(%class.btRaycastVehicle* %this1, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %4)
  ret %class.btRaycastVehicle* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceC2Ev(%class.btActionInterface* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  %0 = bitcast %class.btActionInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV17btActionInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !9
  ret %class.btActionInterface* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.6* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI11btWheelInfoEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN16btRaycastVehicle11defaultInitERKNS_15btVehicleTuningE(%class.btRaycastVehicle* %this, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning) #5 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_currentVehicleSpeedKmHour = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_currentVehicleSpeedKmHour, align 4, !tbaa !26
  %m_steeringValue = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 11
  store float 0.000000e+00, float* %m_steeringValue, align 4, !tbaa !27
  ret void
}

; Function Attrs: nounwind
define hidden %class.btRaycastVehicle* @_ZN16btRaycastVehicleD2Ev(%class.btRaycastVehicle* returned %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %class.btRaycastVehicle* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV16btRaycastVehicle, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !9
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev(%class.btAlignedObjectArray.9* %m_wheelInfo) #1
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %call2 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.5* %m_sideImpulse) #1
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.5* %m_forwardImpulse) #1
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %call4 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.1* %m_axle) #1
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %call5 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.1* %m_forwardWS) #1
  %1 = bitcast %class.btRaycastVehicle* %this1 to %class.btActionInterface*
  %call6 = call %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* %1) #1
  ret %class.btRaycastVehicle* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayI11btWheelInfoED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN16btRaycastVehicleD0Ev(%class.btRaycastVehicle* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call %class.btRaycastVehicle* @_ZN16btRaycastVehicleD1Ev(%class.btRaycastVehicle* %this1) #1
  %0 = bitcast %class.btRaycastVehicle* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle8addWheelERK9btVector3S2_S2_ffRKNS_15btVehicleTuningEb(%class.btRaycastVehicle* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %connectionPointCS, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelDirectionCS0, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelAxleCS, float %suspensionRestLength, float %wheelRadius, %"class.btRaycastVehicle::btVehicleTuning"* nonnull align 4 dereferenceable(24) %tuning, i1 zeroext %isFrontWheel) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %connectionPointCS.addr = alloca %class.btVector3*, align 4
  %wheelDirectionCS0.addr = alloca %class.btVector3*, align 4
  %wheelAxleCS.addr = alloca %class.btVector3*, align 4
  %suspensionRestLength.addr = alloca float, align 4
  %wheelRadius.addr = alloca float, align 4
  %tuning.addr = alloca %"class.btRaycastVehicle::btVehicleTuning"*, align 4
  %isFrontWheel.addr = alloca i8, align 1
  %ci = alloca %struct.btWheelInfoConstructionInfo, align 4
  %ref.tmp = alloca %struct.btWheelInfo, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %connectionPointCS, %class.btVector3** %connectionPointCS.addr, align 4, !tbaa !7
  store %class.btVector3* %wheelDirectionCS0, %class.btVector3** %wheelDirectionCS0.addr, align 4, !tbaa !7
  store %class.btVector3* %wheelAxleCS, %class.btVector3** %wheelAxleCS.addr, align 4, !tbaa !7
  store float %suspensionRestLength, float* %suspensionRestLength.addr, align 4, !tbaa !3
  store float %wheelRadius, float* %wheelRadius.addr, align 4, !tbaa !3
  store %"class.btRaycastVehicle::btVehicleTuning"* %tuning, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %frombool = zext i1 %isFrontWheel to i8
  store i8 %frombool, i8* %isFrontWheel.addr, align 1, !tbaa !28
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %struct.btWheelInfoConstructionInfo* %ci to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %0) #1
  %call = call %struct.btWheelInfoConstructionInfo* @_ZN27btWheelInfoConstructionInfoC2Ev(%struct.btWheelInfoConstructionInfo* %ci)
  %1 = load %class.btVector3*, %class.btVector3** %connectionPointCS.addr, align 4, !tbaa !7
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 0
  %2 = bitcast %class.btVector3* %m_chassisConnectionCS to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !29
  %4 = load %class.btVector3*, %class.btVector3** %wheelDirectionCS0.addr, align 4, !tbaa !7
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_wheelDirectionCS to i8*
  %6 = bitcast %class.btVector3* %4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !29
  %7 = load %class.btVector3*, %class.btVector3** %wheelAxleCS.addr, align 4, !tbaa !7
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 2
  %8 = bitcast %class.btVector3* %m_wheelAxleCS to i8*
  %9 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !29
  %10 = load float, float* %suspensionRestLength.addr, align 4, !tbaa !3
  %m_suspensionRestLength = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 3
  store float %10, float* %m_suspensionRestLength, align 4, !tbaa !31
  %11 = load float, float* %wheelRadius.addr, align 4, !tbaa !3
  %m_wheelRadius = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 5
  store float %11, float* %m_wheelRadius, align 4, !tbaa !34
  %12 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_suspensionStiffness = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %12, i32 0, i32 0
  %13 = load float, float* %m_suspensionStiffness, align 4, !tbaa !35
  %m_suspensionStiffness2 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 6
  store float %13, float* %m_suspensionStiffness2, align 4, !tbaa !37
  %14 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_suspensionCompression = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %14, i32 0, i32 1
  %15 = load float, float* %m_suspensionCompression, align 4, !tbaa !38
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 7
  store float %15, float* %m_wheelsDampingCompression, align 4, !tbaa !39
  %16 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_suspensionDamping = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %16, i32 0, i32 2
  %17 = load float, float* %m_suspensionDamping, align 4, !tbaa !40
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 8
  store float %17, float* %m_wheelsDampingRelaxation, align 4, !tbaa !41
  %18 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_frictionSlip = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %18, i32 0, i32 4
  %19 = load float, float* %m_frictionSlip, align 4, !tbaa !42
  %m_frictionSlip3 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 9
  store float %19, float* %m_frictionSlip3, align 4, !tbaa !43
  %20 = load i8, i8* %isFrontWheel.addr, align 1, !tbaa !28, !range !44
  %tobool = trunc i8 %20 to i1
  %m_bIsFrontWheel = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 11
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %m_bIsFrontWheel, align 4, !tbaa !45
  %21 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_maxSuspensionTravelCm = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %21, i32 0, i32 3
  %22 = load float, float* %m_maxSuspensionTravelCm, align 4, !tbaa !46
  %m_maxSuspensionTravelCm5 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 4
  store float %22, float* %m_maxSuspensionTravelCm5, align 4, !tbaa !47
  %23 = load %"class.btRaycastVehicle::btVehicleTuning"*, %"class.btRaycastVehicle::btVehicleTuning"** %tuning.addr, align 4, !tbaa !7
  %m_maxSuspensionForce = getelementptr inbounds %"class.btRaycastVehicle::btVehicleTuning", %"class.btRaycastVehicle::btVehicleTuning"* %23, i32 0, i32 5
  %24 = load float, float* %m_maxSuspensionForce, align 4, !tbaa !48
  %m_maxSuspensionForce6 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %ci, i32 0, i32 10
  store float %24, float* %m_maxSuspensionForce6, align 4, !tbaa !49
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %25 = bitcast %struct.btWheelInfo* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 284, i8* %25) #1
  %call7 = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo(%struct.btWheelInfo* %ref.tmp, %struct.btWheelInfoConstructionInfo* nonnull align 4 dereferenceable(81) %ci)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_(%class.btAlignedObjectArray.9* %m_wheelInfo, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %ref.tmp)
  %26 = bitcast %struct.btWheelInfo* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 284, i8* %26) #1
  %27 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #1
  %m_wheelInfo8 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call9 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %sub = sub nsw i32 %call9, 1
  %call10 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo8, i32 %sub)
  store %struct.btWheelInfo* %call10, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %28 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %28, i1 zeroext false)
  %call11 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %sub12 = sub nsw i32 %call11, 1
  call void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this1, i32 %sub12, i1 zeroext false)
  %29 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %30 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #1
  %31 = bitcast %struct.btWheelInfoConstructionInfo* %ci to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %31) #1
  ret %struct.btWheelInfo* %29
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btWheelInfoConstructionInfo* @_ZN27btWheelInfoConstructionInfoC2Ev(%struct.btWheelInfoConstructionInfo* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfoConstructionInfo*, align 4
  store %struct.btWheelInfoConstructionInfo* %this, %struct.btWheelInfoConstructionInfo** %this.addr, align 4, !tbaa !7
  %this1 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %this.addr, align 4
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_chassisConnectionCS)
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionCS)
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleCS)
  ret %struct.btWheelInfoConstructionInfo* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE9push_backERKS0_(%class.btAlignedObjectArray.9* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %_Val) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Val.addr = alloca %struct.btWheelInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfo* %_Val, %struct.btWheelInfo** %_Val.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !50
  %1 = load i32, i32* %sz, align 4, !tbaa !50
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_size, align 4, !tbaa !52
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %2, i32 %3
  %4 = bitcast %struct.btWheelInfo* %arrayidx to i8*
  %5 = bitcast i8* %4 to %struct.btWheelInfo*
  %6 = load %struct.btWheelInfo*, %struct.btWheelInfo** %_Val.addr, align 4, !tbaa !7
  %call5 = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* %5, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %6)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size6, align 4, !tbaa !52
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size6, align 4, !tbaa !52
  %8 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #1
  ret void
}

define linkonce_odr hidden %struct.btWheelInfo* @_ZN11btWheelInfoC2ER27btWheelInfoConstructionInfo(%struct.btWheelInfo* returned %this, %struct.btWheelInfoConstructionInfo* nonnull align 4 dereferenceable(81) %ci) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfo*, align 4
  %ci.addr = alloca %struct.btWheelInfoConstructionInfo*, align 4
  store %struct.btWheelInfo* %this, %struct.btWheelInfo** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfoConstructionInfo* %ci, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %this1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %this.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 0
  %call = call %"struct.btWheelInfo::RaycastInfo"* @_ZN11btWheelInfo11RaycastInfoC2Ev(%"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo)
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_worldTransform)
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_chassisConnectionPointCS)
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionCS)
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleCS)
  %0 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_suspensionRestLength = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %0, i32 0, i32 3
  %1 = load float, float* %m_suspensionRestLength, align 4, !tbaa !31
  %m_suspensionRestLength1 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 5
  store float %1, float* %m_suspensionRestLength1, align 4, !tbaa !53
  %2 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_maxSuspensionTravelCm = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %2, i32 0, i32 4
  %3 = load float, float* %m_maxSuspensionTravelCm, align 4, !tbaa !47
  %m_maxSuspensionTravelCm6 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 6
  store float %3, float* %m_maxSuspensionTravelCm6, align 4, !tbaa !58
  %4 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_wheelRadius = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %4, i32 0, i32 5
  %5 = load float, float* %m_wheelRadius, align 4, !tbaa !34
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 7
  store float %5, float* %m_wheelsRadius, align 4, !tbaa !59
  %6 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_suspensionStiffness = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %6, i32 0, i32 6
  %7 = load float, float* %m_suspensionStiffness, align 4, !tbaa !37
  %m_suspensionStiffness7 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 8
  store float %7, float* %m_suspensionStiffness7, align 4, !tbaa !60
  %8 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %8, i32 0, i32 7
  %9 = load float, float* %m_wheelsDampingCompression, align 4, !tbaa !39
  %m_wheelsDampingCompression8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 9
  store float %9, float* %m_wheelsDampingCompression8, align 4, !tbaa !61
  %10 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %10, i32 0, i32 8
  %11 = load float, float* %m_wheelsDampingRelaxation, align 4, !tbaa !41
  %m_wheelsDampingRelaxation9 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 10
  store float %11, float* %m_wheelsDampingRelaxation9, align 4, !tbaa !62
  %12 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_chassisConnectionCS = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %12, i32 0, i32 0
  %m_chassisConnectionPointCS10 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %13 = bitcast %class.btVector3* %m_chassisConnectionPointCS10 to i8*
  %14 = bitcast %class.btVector3* %m_chassisConnectionCS to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !29
  %15 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_wheelDirectionCS11 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %15, i32 0, i32 1
  %m_wheelDirectionCS12 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 3
  %16 = bitcast %class.btVector3* %m_wheelDirectionCS12 to i8*
  %17 = bitcast %class.btVector3* %m_wheelDirectionCS11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false), !tbaa.struct !29
  %18 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_wheelAxleCS13 = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %18, i32 0, i32 2
  %m_wheelAxleCS14 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 4
  %19 = bitcast %class.btVector3* %m_wheelAxleCS14 to i8*
  %20 = bitcast %class.btVector3* %m_wheelAxleCS13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false), !tbaa.struct !29
  %21 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_frictionSlip = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %21, i32 0, i32 9
  %22 = load float, float* %m_frictionSlip, align 4, !tbaa !43
  %m_frictionSlip15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 11
  store float %22, float* %m_frictionSlip15, align 4, !tbaa !63
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_steering, align 4, !tbaa !64
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_engineForce, align 4, !tbaa !65
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_rotation, align 4, !tbaa !66
  %m_deltaRotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 14
  store float 0.000000e+00, float* %m_deltaRotation, align 4, !tbaa !67
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_brake, align 4, !tbaa !68
  %m_rollInfluence = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 15
  store float 0x3FB99999A0000000, float* %m_rollInfluence, align 4, !tbaa !69
  %23 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_bIsFrontWheel = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %23, i32 0, i32 11
  %24 = load i8, i8* %m_bIsFrontWheel, align 4, !tbaa !45, !range !44
  %tobool = trunc i8 %24 to i1
  %m_bIsFrontWheel16 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 19
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %m_bIsFrontWheel16, align 4, !tbaa !70
  %25 = load %struct.btWheelInfoConstructionInfo*, %struct.btWheelInfoConstructionInfo** %ci.addr, align 4, !tbaa !7
  %m_maxSuspensionForce = getelementptr inbounds %struct.btWheelInfoConstructionInfo, %struct.btWheelInfoConstructionInfo* %25, i32 0, i32 10
  %26 = load float, float* %m_maxSuspensionForce, align 4, !tbaa !49
  %m_maxSuspensionForce17 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 16
  store float %26, float* %m_maxSuspensionForce17, align 4, !tbaa !71
  ret %struct.btWheelInfo* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 %1
  ret %struct.btWheelInfo* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %m_wheelInfo)
  ret i32 %call
}

define hidden void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %wheel, i1 zeroext %interpolatedTransform) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca %struct.btWheelInfo*, align 4
  %interpolatedTransform.addr = alloca i8, align 1
  %chassisTrans = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfo* %wheel, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %frombool = zext i1 %interpolatedTransform to i8
  store i8 %frombool, i8* %interpolatedTransform.addr, align 1, !tbaa !28
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  store i8 0, i8* %m_isInContact, align 4, !tbaa !72
  %1 = bitcast %class.btTransform* %chassisTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %1) #1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %chassisTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  %2 = load i8, i8* %interpolatedTransform.addr, align 1, !tbaa !28, !range !44
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %call3 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call4 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %call3)
  %tobool5 = icmp ne %class.btMotionState* %call4, null
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %call6 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call7 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %call6)
  %3 = bitcast %class.btMotionState* %call7 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %3, align 4, !tbaa !9
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %4 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %4(%class.btMotionState* %call7, %class.btTransform* nonnull align 4 dereferenceable(64) %chassisTrans)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #1
  %6 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %6, i32 0, i32 2
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %chassisTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %m_chassisConnectionPointCS)
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo8, i32 0, i32 3
  %8 = bitcast %class.btVector3* %m_hardPointWS to i8*
  %9 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !29
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #1
  %11 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #1
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %chassisTrans)
  %12 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_wheelDirectionCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %12, i32 0, i32 3
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionCS)
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo11 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo11, i32 0, i32 4
  %14 = bitcast %class.btVector3* %m_wheelDirectionWS to i8*
  %15 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !29
  %16 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #1
  %17 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #1
  %call13 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %chassisTrans)
  %18 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_wheelAxleCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %18, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelAxleCS)
  %19 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo14 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %19, i32 0, i32 0
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo14, i32 0, i32 5
  %20 = bitcast %class.btVector3* %m_wheelAxleWS to i8*
  %21 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !29
  %22 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #1
  %23 = bitcast %class.btTransform* %chassisTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %23) #1
  ret void
}

define hidden void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this, i32 %wheelIndex, i1 zeroext %interpolatedTransform) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheelIndex.addr = alloca i32, align 4
  %interpolatedTransform.addr = alloca i8, align 1
  %wheel = alloca %struct.btWheelInfo*, align 4
  %up = alloca %class.btVector3, align 4
  %right = alloca %class.btVector3*, align 4
  %fwd = alloca %class.btVector3, align 4
  %steering = alloca float, align 4
  %steeringOrn = alloca %class.btQuaternion, align 4
  %steeringMat = alloca %class.btMatrix3x3, align 4
  %rotatingOrn = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca float, align 4
  %rotatingMat = alloca %class.btMatrix3x3, align 4
  %basis2 = alloca %class.btMatrix3x3, align 4
  %ref.tmp26 = alloca %class.btMatrix3x3, align 4
  %ref.tmp27 = alloca %class.btMatrix3x3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4, !tbaa !50
  %frombool = zext i1 %interpolatedTransform to i8
  store i8 %frombool, i8* %interpolatedTransform.addr, align 1, !tbaa !28
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %1 = load i32, i32* %wheelIndex.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %1)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %3 = load i8, i8* %interpolatedTransform.addr, align 1, !tbaa !28, !range !44
  %tobool = trunc i8 %3 to i1
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %2, i1 zeroext %tobool)
  %4 = bitcast %class.btVector3* %up to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #1
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %up, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS)
  %6 = bitcast %class.btVector3** %right to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2, i32 0, i32 5
  store %class.btVector3* %m_wheelAxleWS, %class.btVector3** %right, align 4, !tbaa !7
  %8 = bitcast %class.btVector3* %fwd to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #1
  %9 = load %class.btVector3*, %class.btVector3** %right, align 4, !tbaa !7
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %fwd, %class.btVector3* %up, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %fwd)
  %10 = bitcast %class.btVector3* %fwd to i8*
  %11 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !29
  %12 = bitcast float* %steering to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #1
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 12
  %14 = load float, float* %m_steering, align 4, !tbaa !64
  store float %14, float* %steering, align 4, !tbaa !3
  %15 = bitcast %class.btQuaternion* %steeringOrn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #1
  %call4 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %steeringOrn, %class.btVector3* nonnull align 4 dereferenceable(16) %up, float* nonnull align 4 dereferenceable(4) %steering)
  %16 = bitcast %class.btMatrix3x3* %steeringMat to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %16) #1
  %call5 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %steeringMat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %steeringOrn)
  %17 = bitcast %class.btQuaternion* %rotatingOrn to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #1
  %18 = load %class.btVector3*, %class.btVector3** %right, align 4, !tbaa !7
  %19 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #1
  %20 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %20, i32 0, i32 13
  %21 = load float, float* %m_rotation, align 4, !tbaa !66
  %fneg = fneg float %21
  store float %fneg, float* %ref.tmp, align 4, !tbaa !3
  %call6 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotatingOrn, %class.btVector3* nonnull align 4 dereferenceable(16) %18, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %22 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #1
  %23 = bitcast %class.btMatrix3x3* %rotatingMat to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %23) #1
  %call7 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %rotatingMat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotatingOrn)
  %24 = bitcast %class.btMatrix3x3* %basis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %24) #1
  %25 = load %class.btVector3*, %class.btVector3** %right, align 4, !tbaa !7
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %25)
  %arrayidx = getelementptr inbounds float, float* %call8, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 0
  %26 = load %class.btVector3*, %class.btVector3** %right, align 4, !tbaa !7
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 1
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %27 = load %class.btVector3*, %class.btVector3** %right, align 4, !tbaa !7
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %27)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 2
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %fwd)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %up)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  %call25 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %basis2, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx10, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx14, float* nonnull align 4 dereferenceable(4) %arrayidx16, float* nonnull align 4 dereferenceable(4) %arrayidx18, float* nonnull align 4 dereferenceable(4) %arrayidx20, float* nonnull align 4 dereferenceable(4) %arrayidx22, float* nonnull align 4 dereferenceable(4) %arrayidx24)
  %28 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %28, i32 0, i32 1
  %29 = bitcast %class.btMatrix3x3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %29) #1
  %30 = bitcast %class.btMatrix3x3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %30) #1
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp27, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %steeringMat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rotatingMat)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp26, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp27, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis2)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %m_worldTransform, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp26)
  %31 = bitcast %class.btMatrix3x3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %31) #1
  %32 = bitcast %class.btMatrix3x3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %32) #1
  %33 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_worldTransform28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %33, i32 0, i32 1
  %34 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #1
  %35 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo30 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %35, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo30, i32 0, i32 3
  %36 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #1
  %37 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo32 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %37, i32 0, i32 0
  %m_wheelDirectionWS33 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo32, i32 0, i32 4
  %38 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo34 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %38, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo34, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS33, float* nonnull align 4 dereferenceable(4) %m_suspensionLength)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp29, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hardPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_worldTransform28, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  %39 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #1
  %40 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %40) #1
  %41 = bitcast %class.btMatrix3x3* %basis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %41) #1
  %42 = bitcast %class.btMatrix3x3* %rotatingMat to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %42) #1
  %43 = bitcast %class.btQuaternion* %rotatingOrn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #1
  %44 = bitcast %class.btMatrix3x3* %steeringMat to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %44) #1
  %45 = bitcast %class.btQuaternion* %steeringOrn to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #1
  %46 = bitcast float* %steering to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #1
  %47 = bitcast %class.btVector3* %fwd to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #1
  %48 = bitcast %class.btVector3** %right to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #1
  %49 = bitcast %class.btVector3* %up to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #1
  %50 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #1
  ret void
}

define hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle19getWheelTransformWSEi(%class.btRaycastVehicle* %this, i32 %wheelIndex) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheelIndex.addr = alloca i32, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %1 = load i32, i32* %wheelIndex.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %1)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %2, i32 0, i32 1
  %3 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %1 = load i32, i32* %n.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %0, i32 %1
  ret %struct.btWheelInfo* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !3
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #1
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !3
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !3
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !3
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #1
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #1
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %1 = load float, float* %arrayidx, align 4, !tbaa !3
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %3 = load float, float* %arrayidx3, align 4, !tbaa !3
  %mul = fmul float %1, %3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %4 = load float, float* %arrayidx5, align 4, !tbaa !3
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %6 = load float, float* %arrayidx7, align 4, !tbaa !3
  %mul8 = fmul float %4, %6
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4, !tbaa !3
  %7 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #1
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %8 = load float, float* %arrayidx11, align 4, !tbaa !3
  %9 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %10 = load float, float* %arrayidx13, align 4, !tbaa !3
  %mul14 = fmul float %8, %10
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %11 = load float, float* %arrayidx16, align 4, !tbaa !3
  %12 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %13 = load float, float* %arrayidx18, align 4, !tbaa !3
  %mul19 = fmul float %11, %13
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4, !tbaa !3
  %14 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %15 = load float, float* %arrayidx23, align 4, !tbaa !3
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %17 = load float, float* %arrayidx25, align 4, !tbaa !3
  %mul26 = fmul float %15, %17
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %18 = load float, float* %arrayidx28, align 4, !tbaa !3
  %19 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %20 = load float, float* %arrayidx30, align 4, !tbaa !3
  %mul31 = fmul float %18, %20
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #1
  %22 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #1
  %23 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !3
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #1
  ret %class.btVector3* %call2
}

define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4, !tbaa !7
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !7
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4, !tbaa !7
  %2 = load float*, float** %_angle.addr, align 4, !tbaa !7
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store float* %xx, float** %xx.addr, align 4, !tbaa !7
  store float* %xy, float** %xy.addr, align 4, !tbaa !7
  store float* %xz, float** %xz.addr, align 4, !tbaa !7
  store float* %yx, float** %yx.addr, align 4, !tbaa !7
  store float* %yy, float** %yy.addr, align 4, !tbaa !7
  store float* %yz, float** %yz.addr, align 4, !tbaa !7
  store float* %zx, float** %zx.addr, align 4, !tbaa !7
  store float* %zy, float** %zy.addr, align 4, !tbaa !7
  store float* %zz, float** %zz.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4, !tbaa !7
  %1 = load float*, float** %xy.addr, align 4, !tbaa !7
  %2 = load float*, float** %xz.addr, align 4, !tbaa !7
  %3 = load float*, float** %yx.addr, align 4, !tbaa !7
  %4 = load float*, float** %yy.addr, align 4, !tbaa !7
  %5 = load float*, float** %yz.addr, align 4, !tbaa !7
  %6 = load float*, float** %zx.addr, align 4, !tbaa !7
  %7 = load float*, float** %zy.addr, align 4, !tbaa !7
  %8 = load float*, float** %zz.addr, align 4, !tbaa !7
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %basis.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  store %class.btMatrix3x3* %basis, %class.btMatrix3x3** %basis.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %basis.addr, align 4, !tbaa !7
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #7 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4, !tbaa !3
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #1
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !3
  %7 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #1
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !3
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4, !tbaa !3
  %13 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !3
  %16 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #1
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4, !tbaa !3
  %19 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #1
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4, !tbaa !3
  %22 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #1
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %24, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4, !tbaa !3
  %25 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #1
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4, !tbaa !7
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4, !tbaa !7
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4, !tbaa !3
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  %28 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #1
  %29 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #1
  %30 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #1
  %31 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #1
  %32 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #1
  %33 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #1
  %34 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #1
  %35 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #1
  %36 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4, !tbaa !7
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !29
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !7
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !3
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !3
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !3
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !3
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !3
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !3
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #1
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  store float* %s, float** %s.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %3 = load float*, float** %s.addr, align 4, !tbaa !7
  %4 = load float, float* %3, align 4, !tbaa !3
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !3
  %8 = load float*, float** %s.addr, align 4, !tbaa !7
  %9 = load float, float* %8, align 4, !tbaa !3
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !3
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !3
  %13 = load float*, float** %s.addr, align 4, !tbaa !7
  %14 = load float, float* %13, align 4, !tbaa !3
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #1
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  ret void
}

define hidden void @_ZN16btRaycastVehicle15resetSuspensionEv(%class.btRaycastVehicle* %this) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %i = alloca i32, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  store i32 0, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !50
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %m_wheelInfo)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  %m_wheelInfo2 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %3 = load i32, i32* %i, align 4, !tbaa !50
  %call3 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo2, i32 %3)
  store %struct.btWheelInfo* %call3, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %call4 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %4)
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 2
  store float %call4, float* %m_suspensionLength, align 4, !tbaa !73
  %6 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %6, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity, align 4, !tbaa !74
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #1
  %8 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo5 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %8, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo5, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS)
  %9 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo6 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %9, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo6, i32 0, i32 0
  %10 = bitcast %class.btVector3* %m_contactNormalWS to i8*
  %11 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !29
  %12 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #1
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 21
  store float 1.000000e+00, float* %m_clippedInvContactDotSuspension, align 4, !tbaa !75
  %14 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !52
  ret i32 %0
}

declare float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo*) #4

define hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %call = call %class.btRigidBody* @_ZNK16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call)
  ret %class.btTransform* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !7
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !7
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !29
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4, !tbaa !22
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4, !tbaa !76
  ret %class.btMotionState* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #7 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !7
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #7 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call1, float* %ref.tmp, align 4, !tbaa !3
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #1
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 1)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp2, align 4, !tbaa !3
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 2)
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call7, float* %ref.tmp5, align 4, !tbaa !3
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #1
  %10 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #1
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

define hidden float @_ZN16btRaycastVehicle7rayCastER11btWheelInfo(%class.btRaycastVehicle* %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %wheel) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca %struct.btWheelInfo*, align 4
  %depth = alloca float, align 4
  %raylen = alloca float, align 4
  %rayvector = alloca %class.btVector3, align 4
  %source = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %target = alloca %class.btVector3*, align 4
  %param = alloca float, align 4
  %rayResults = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult", align 4
  %object = alloca i8*, align 4
  %hitDistance = alloca float, align 4
  %minSuspensionLength = alloca float, align 4
  %maxSuspensionLength = alloca float, align 4
  %denominator = alloca float, align 4
  %chassis_velocity_at_contactPoint = alloca %class.btVector3, align 4
  %relpos = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %projVel = alloca float, align 4
  %inv = alloca float, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfo* %wheel, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  call void @_ZN16btRaycastVehicle23updateWheelTransformsWSER11btWheelInfob(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %0, i1 zeroext false)
  %1 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  store float -1.000000e+00, float* %depth, align 4, !tbaa !3
  %2 = bitcast float* %raylen to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %call = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %3)
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 7
  %5 = load float, float* %m_wheelsRadius, align 4, !tbaa !59
  %add = fadd float %call, %5
  store float %add, float* %raylen, align 4, !tbaa !3
  %6 = bitcast %class.btVector3* %rayvector to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #1
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %rayvector, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS, float* nonnull align 4 dereferenceable(4) %raylen)
  %8 = bitcast %class.btVector3** %source to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #1
  %9 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %9, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2, i32 0, i32 3
  store %class.btVector3* %m_hardPointWS, %class.btVector3** %source, align 4, !tbaa !7
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #1
  %11 = load %class.btVector3*, %class.btVector3** %source, align 4, !tbaa !7
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %rayvector)
  %12 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo3 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %12, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo3, i32 0, i32 1
  %13 = bitcast %class.btVector3* %m_contactPointWS to i8*
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !29
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %15) #1
  %16 = bitcast %class.btVector3** %target to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #1
  %17 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo4 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %17, i32 0, i32 0
  %m_contactPointWS5 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo4, i32 0, i32 1
  store %class.btVector3* %m_contactPointWS5, %class.btVector3** %target, align 4, !tbaa !7
  %18 = bitcast float* %param to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #1
  store float 0.000000e+00, float* %param, align 4, !tbaa !3
  %19 = bitcast %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults to i8*
  call void @llvm.lifetime.start.p0i8(i64 36, i8* %19) #1
  %call6 = call %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* @_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev(%"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults)
  %20 = bitcast i8** %object to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #1
  %m_vehicleRaycaster = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 9
  %21 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %m_vehicleRaycaster, align 4, !tbaa !11
  %22 = load %class.btVector3*, %class.btVector3** %source, align 4, !tbaa !7
  %23 = load %class.btVector3*, %class.btVector3** %target, align 4, !tbaa !7
  %24 = bitcast %struct.btVehicleRaycaster* %21 to i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)***
  %vtable = load i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)**, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*** %24, align 4, !tbaa !9
  %vfn = getelementptr inbounds i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)** %vtable, i64 2
  %25 = load i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)*, i8* (%struct.btVehicleRaycaster*, %class.btVector3*, %class.btVector3*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*)** %vfn, align 4
  %call7 = call i8* %25(%struct.btVehicleRaycaster* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* nonnull align 4 dereferenceable(36) %rayResults)
  store i8* %call7, i8** %object, align 4, !tbaa !7
  %26 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo8 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %26, i32 0, i32 0
  %m_groundObject = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo8, i32 0, i32 7
  store i8* null, i8** %m_groundObject, align 4, !tbaa !80
  %27 = load i8*, i8** %object, align 4, !tbaa !7
  %tobool = icmp ne i8* %27, null
  br i1 %tobool, label %if.then, label %if.else60

if.then:                                          ; preds = %entry
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 2
  %28 = load float, float* %m_distFraction, align 4, !tbaa !81
  store float %28, float* %param, align 4, !tbaa !3
  %29 = load float, float* %raylen, align 4, !tbaa !3
  %m_distFraction9 = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 2
  %30 = load float, float* %m_distFraction9, align 4, !tbaa !81
  %mul = fmul float %29, %30
  store float %mul, float* %depth, align 4, !tbaa !3
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 1
  %31 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo10 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %31, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo10, i32 0, i32 0
  %32 = bitcast %class.btVector3* %m_contactNormalWS to i8*
  %33 = bitcast %class.btVector3* %m_hitNormalInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false), !tbaa.struct !29
  %34 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo11 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %34, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo11, i32 0, i32 6
  store i8 1, i8* %m_isInContact, align 4, !tbaa !72
  %call12 = call nonnull align 4 dereferenceable(616) %class.btRigidBody* @_ZN17btActionInterface12getFixedBodyEv()
  %35 = bitcast %class.btRigidBody* %call12 to i8*
  %36 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo13 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %36, i32 0, i32 0
  %m_groundObject14 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo13, i32 0, i32 7
  store i8* %35, i8** %m_groundObject14, align 4, !tbaa !80
  %37 = bitcast float* %hitDistance to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #1
  %38 = load float, float* %param, align 4, !tbaa !3
  %39 = load float, float* %raylen, align 4, !tbaa !3
  %mul15 = fmul float %38, %39
  store float %mul15, float* %hitDistance, align 4, !tbaa !3
  %40 = load float, float* %hitDistance, align 4, !tbaa !3
  %41 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_wheelsRadius16 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %41, i32 0, i32 7
  %42 = load float, float* %m_wheelsRadius16, align 4, !tbaa !59
  %sub = fsub float %40, %42
  %43 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo17 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %43, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo17, i32 0, i32 2
  store float %sub, float* %m_suspensionLength, align 4, !tbaa !73
  %44 = bitcast float* %minSuspensionLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #1
  %45 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %call18 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %45)
  %46 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_maxSuspensionTravelCm = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %46, i32 0, i32 6
  %47 = load float, float* %m_maxSuspensionTravelCm, align 4, !tbaa !58
  %mul19 = fmul float %47, 0x3F847AE140000000
  %sub20 = fsub float %call18, %mul19
  store float %sub20, float* %minSuspensionLength, align 4, !tbaa !3
  %48 = bitcast float* %maxSuspensionLength to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #1
  %49 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %call21 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %49)
  %50 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_maxSuspensionTravelCm22 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %50, i32 0, i32 6
  %51 = load float, float* %m_maxSuspensionTravelCm22, align 4, !tbaa !58
  %mul23 = fmul float %51, 0x3F847AE140000000
  %add24 = fadd float %call21, %mul23
  store float %add24, float* %maxSuspensionLength, align 4, !tbaa !3
  %52 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo25 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %52, i32 0, i32 0
  %m_suspensionLength26 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo25, i32 0, i32 2
  %53 = load float, float* %m_suspensionLength26, align 4, !tbaa !73
  %54 = load float, float* %minSuspensionLength, align 4, !tbaa !3
  %cmp = fcmp olt float %53, %54
  br i1 %cmp, label %if.then27, label %if.end

if.then27:                                        ; preds = %if.then
  %55 = load float, float* %minSuspensionLength, align 4, !tbaa !3
  %56 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %56, i32 0, i32 0
  %m_suspensionLength29 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo28, i32 0, i32 2
  store float %55, float* %m_suspensionLength29, align 4, !tbaa !73
  br label %if.end

if.end:                                           ; preds = %if.then27, %if.then
  %57 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo30 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %57, i32 0, i32 0
  %m_suspensionLength31 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo30, i32 0, i32 2
  %58 = load float, float* %m_suspensionLength31, align 4, !tbaa !73
  %59 = load float, float* %maxSuspensionLength, align 4, !tbaa !3
  %cmp32 = fcmp ogt float %58, %59
  br i1 %cmp32, label %if.then33, label %if.end36

if.then33:                                        ; preds = %if.end
  %60 = load float, float* %maxSuspensionLength, align 4, !tbaa !3
  %61 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo34 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %61, i32 0, i32 0
  %m_suspensionLength35 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo34, i32 0, i32 2
  store float %60, float* %m_suspensionLength35, align 4, !tbaa !73
  br label %if.end36

if.end36:                                         ; preds = %if.then33, %if.end
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults, i32 0, i32 0
  %62 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo37 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %62, i32 0, i32 0
  %m_contactPointWS38 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo37, i32 0, i32 1
  %63 = bitcast %class.btVector3* %m_contactPointWS38 to i8*
  %64 = bitcast %class.btVector3* %m_hitPointInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 16, i1 false), !tbaa.struct !29
  %65 = bitcast float* %denominator to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #1
  %66 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo39 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %66, i32 0, i32 0
  %m_contactNormalWS40 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo39, i32 0, i32 0
  %67 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo41 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %67, i32 0, i32 0
  %m_wheelDirectionWS42 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo41, i32 0, i32 4
  %call43 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormalWS40, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS42)
  store float %call43, float* %denominator, align 4, !tbaa !3
  %68 = bitcast %class.btVector3* %chassis_velocity_at_contactPoint to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #1
  %call44 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %chassis_velocity_at_contactPoint)
  %69 = bitcast %class.btVector3* %relpos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %69) #1
  %70 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo45 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %70, i32 0, i32 0
  %m_contactPointWS46 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo45, i32 0, i32 1
  %call47 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call47)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS46, %class.btVector3* nonnull align 4 dereferenceable(16) %call48)
  %71 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %71) #1
  %call50 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %ref.tmp49, %class.btRigidBody* %call50, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos)
  %72 = bitcast %class.btVector3* %chassis_velocity_at_contactPoint to i8*
  %73 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 16, i1 false), !tbaa.struct !29
  %74 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %74) #1
  %75 = bitcast float* %projVel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #1
  %76 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo51 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %76, i32 0, i32 0
  %m_contactNormalWS52 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo51, i32 0, i32 0
  %call53 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_contactNormalWS52, %class.btVector3* nonnull align 4 dereferenceable(16) %chassis_velocity_at_contactPoint)
  store float %call53, float* %projVel, align 4, !tbaa !3
  %77 = load float, float* %denominator, align 4, !tbaa !3
  %cmp54 = fcmp oge float %77, 0xBFB99999A0000000
  br i1 %cmp54, label %if.then55, label %if.else

if.then55:                                        ; preds = %if.end36
  %78 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %78, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity, align 4, !tbaa !74
  %79 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %79, i32 0, i32 21
  store float 1.000000e+01, float* %m_clippedInvContactDotSuspension, align 4, !tbaa !75
  br label %if.end59

if.else:                                          ; preds = %if.end36
  %80 = bitcast float* %inv to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #1
  %81 = load float, float* %denominator, align 4, !tbaa !3
  %div = fdiv float -1.000000e+00, %81
  store float %div, float* %inv, align 4, !tbaa !3
  %82 = load float, float* %projVel, align 4, !tbaa !3
  %83 = load float, float* %inv, align 4, !tbaa !3
  %mul56 = fmul float %82, %83
  %84 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_suspensionRelativeVelocity57 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %84, i32 0, i32 22
  store float %mul56, float* %m_suspensionRelativeVelocity57, align 4, !tbaa !74
  %85 = load float, float* %inv, align 4, !tbaa !3
  %86 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_clippedInvContactDotSuspension58 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %86, i32 0, i32 21
  store float %85, float* %m_clippedInvContactDotSuspension58, align 4, !tbaa !75
  %87 = bitcast float* %inv to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #1
  br label %if.end59

if.end59:                                         ; preds = %if.else, %if.then55
  %88 = bitcast float* %projVel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #1
  %89 = bitcast %class.btVector3* %relpos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #1
  %90 = bitcast %class.btVector3* %chassis_velocity_at_contactPoint to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %90) #1
  %91 = bitcast float* %denominator to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #1
  %92 = bitcast float* %maxSuspensionLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #1
  %93 = bitcast float* %minSuspensionLength to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #1
  %94 = bitcast float* %hitDistance to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #1
  br label %if.end71

if.else60:                                        ; preds = %entry
  %95 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %call61 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %95)
  %96 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo62 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %96, i32 0, i32 0
  %m_suspensionLength63 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo62, i32 0, i32 2
  store float %call61, float* %m_suspensionLength63, align 4, !tbaa !73
  %97 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_suspensionRelativeVelocity64 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %97, i32 0, i32 22
  store float 0.000000e+00, float* %m_suspensionRelativeVelocity64, align 4, !tbaa !74
  %98 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %98) #1
  %99 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo66 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %99, i32 0, i32 0
  %m_wheelDirectionWS67 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo66, i32 0, i32 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_wheelDirectionWS67)
  %100 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_raycastInfo68 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %100, i32 0, i32 0
  %m_contactNormalWS69 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo68, i32 0, i32 0
  %101 = bitcast %class.btVector3* %m_contactNormalWS69 to i8*
  %102 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false), !tbaa.struct !29
  %103 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %103) #1
  %104 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel.addr, align 4, !tbaa !7
  %m_clippedInvContactDotSuspension70 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %104, i32 0, i32 21
  store float 1.000000e+00, float* %m_clippedInvContactDotSuspension70, align 4, !tbaa !75
  br label %if.end71

if.end71:                                         ; preds = %if.else60, %if.end59
  %105 = load float, float* %depth, align 4, !tbaa !3
  %106 = bitcast i8** %object to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #1
  %107 = bitcast %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %rayResults to i8*
  call void @llvm.lifetime.end.p0i8(i64 36, i8* %107) #1
  %108 = bitcast float* %param to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #1
  %109 = bitcast %class.btVector3** %target to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #1
  %110 = bitcast %class.btVector3** %source to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #1
  %111 = bitcast %class.btVector3* %rayvector to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %111) #1
  %112 = bitcast float* %raylen to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #1
  %113 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #1
  ret float %105
}

define linkonce_odr hidden %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* @_ZN18btVehicleRaycaster24btVehicleRaycasterResultC2Ev(%"struct.btVehicleRaycaster::btVehicleRaycasterResult"* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, align 4
  store %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %this.addr, align 4
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointInWorld)
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalInWorld)
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1, i32 0, i32 2
  store float -1.000000e+00, float* %m_distFraction, align 4, !tbaa !81
  ret %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !3
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !3
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !3
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !3
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !3
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !3
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !7
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !3
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !3
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !3
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !3
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !3
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !3
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #1
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  ret void
}

define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  ret %class.btVector3* %call
}

define linkonce_odr hidden void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #1
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !7
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #1
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btRigidBody* @_ZNK16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4, !tbaa !22
  ret %class.btRigidBody* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

define hidden void @_ZN16btRaycastVehicle13updateVehicleEf(%class.btRaycastVehicle* %this, float %step) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %step.addr = alloca float, align 4
  %i = alloca i32, align 4
  %chassisTrans = alloca %class.btTransform*, align 4
  %forwardW = alloca %class.btVector3, align 4
  %i26 = alloca i32, align 4
  %depth = alloca float, align 4
  %wheel = alloca %struct.btWheelInfo*, align 4
  %suspensionForce = alloca float, align 4
  %impulse = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %relpos = alloca %class.btVector3, align 4
  %wheel60 = alloca %struct.btWheelInfo*, align 4
  %relpos63 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %chassisWorldTransform = alloca %class.btTransform*, align 4
  %fwd = alloca %class.btVector3, align 4
  %proj = alloca float, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %proj2 = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %step, float* %step.addr, align 4, !tbaa !3
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  store i32 0, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !50
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4, !tbaa !50
  call void @_ZN16btRaycastVehicle20updateWheelTransformEib(%class.btRaycastVehicle* %this1, i32 %3, i1 zeroext false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %call2 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %call2)
  %call4 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %call3)
  %mul = fmul float 0x400CCCCCC0000000, %call4
  %m_currentVehicleSpeedKmHour = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  store float %mul, float* %m_currentVehicleSpeedKmHour, align 4, !tbaa !26
  %5 = bitcast %class.btTransform** %chassisTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  store %class.btTransform* %call5, %class.btTransform** %chassisTrans, align 4, !tbaa !7
  %6 = bitcast %class.btVector3* %forwardW to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #1
  %7 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %7)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call6, i32 0)
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call7)
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %8 = load i32, i32* %m_indexForwardAxis, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds float, float* %call8, i32 %8
  %9 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4, !tbaa !7
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %9)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call9, i32 1)
  %call11 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call10)
  %m_indexForwardAxis12 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %10 = load i32, i32* %m_indexForwardAxis12, align 4, !tbaa !25
  %arrayidx13 = getelementptr inbounds float, float* %call11, i32 %10
  %11 = load %class.btTransform*, %class.btTransform** %chassisTrans, align 4, !tbaa !7
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %11)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call14, i32 2)
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call15)
  %m_indexForwardAxis17 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %12 = load i32, i32* %m_indexForwardAxis17, align 4, !tbaa !25
  %arrayidx18 = getelementptr inbounds float, float* %call16, i32 %12
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %forwardW, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx18)
  %call20 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %call20)
  %call22 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %forwardW, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  %cmp23 = fcmp olt float %call22, 0.000000e+00
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %m_currentVehicleSpeedKmHour24 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 12
  %13 = load float, float* %m_currentVehicleSpeedKmHour24, align 4, !tbaa !26
  %mul25 = fmul float %13, -1.000000e+00
  store float %mul25, float* %m_currentVehicleSpeedKmHour24, align 4, !tbaa !26
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %14 = bitcast i32* %i26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  store i32 0, i32* %i26, align 4, !tbaa !50
  store i32 0, i32* %i26, align 4, !tbaa !50
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc34, %if.end
  %15 = load i32, i32* %i26, align 4, !tbaa !50
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call28 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %m_wheelInfo)
  %cmp29 = icmp slt i32 %15, %call28
  br i1 %cmp29, label %for.body30, label %for.end36

for.body30:                                       ; preds = %for.cond27
  %16 = bitcast float* %depth to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #1
  %m_wheelInfo31 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %17 = load i32, i32* %i26, align 4, !tbaa !50
  %call32 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo31, i32 %17)
  %call33 = call float @_ZN16btRaycastVehicle7rayCastER11btWheelInfo(%class.btRaycastVehicle* %this1, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %call32)
  store float %call33, float* %depth, align 4, !tbaa !3
  %18 = bitcast float* %depth to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #1
  br label %for.inc34

for.inc34:                                        ; preds = %for.body30
  %19 = load i32, i32* %i26, align 4, !tbaa !50
  %inc35 = add nsw i32 %19, 1
  store i32 %inc35, i32* %i26, align 4, !tbaa !50
  br label %for.cond27

for.end36:                                        ; preds = %for.cond27
  %20 = load float, float* %step.addr, align 4, !tbaa !3
  call void @_ZN16btRaycastVehicle16updateSuspensionEf(%class.btRaycastVehicle* %this1, float %20)
  store i32 0, i32* %i26, align 4, !tbaa !50
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc52, %for.end36
  %21 = load i32, i32* %i26, align 4, !tbaa !50
  %m_wheelInfo38 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call39 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %m_wheelInfo38)
  %cmp40 = icmp slt i32 %21, %call39
  br i1 %cmp40, label %for.body41, label %for.end54

for.body41:                                       ; preds = %for.cond37
  %22 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #1
  %m_wheelInfo42 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %23 = load i32, i32* %i26, align 4, !tbaa !50
  %call43 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo42, i32 %23)
  store %struct.btWheelInfo* %call43, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %24 = bitcast float* %suspensionForce to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #1
  %25 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %25, i32 0, i32 23
  %26 = load float, float* %m_wheelsSuspensionForce, align 4, !tbaa !83
  store float %26, float* %suspensionForce, align 4, !tbaa !3
  %27 = load float, float* %suspensionForce, align 4, !tbaa !3
  %28 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_maxSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %28, i32 0, i32 16
  %29 = load float, float* %m_maxSuspensionForce, align 4, !tbaa !71
  %cmp44 = fcmp ogt float %27, %29
  br i1 %cmp44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %for.body41
  %30 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_maxSuspensionForce46 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %30, i32 0, i32 16
  %31 = load float, float* %m_maxSuspensionForce46, align 4, !tbaa !71
  store float %31, float* %suspensionForce, align 4, !tbaa !3
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %for.body41
  %32 = bitcast %class.btVector3* %impulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #1
  %33 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #1
  %34 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %34, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS, float* nonnull align 4 dereferenceable(4) %suspensionForce)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %step.addr)
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #1
  %36 = bitcast %class.btVector3* %relpos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #1
  %37 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel, align 4, !tbaa !7
  %m_raycastInfo48 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %37, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo48, i32 0, i32 1
  %call49 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call49)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %call50)
  %call51 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %call51, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos)
  %38 = bitcast %class.btVector3* %relpos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %38) #1
  %39 = bitcast %class.btVector3* %impulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %39) #1
  %40 = bitcast float* %suspensionForce to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #1
  %41 = bitcast %struct.btWheelInfo** %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #1
  br label %for.inc52

for.inc52:                                        ; preds = %if.end47
  %42 = load i32, i32* %i26, align 4, !tbaa !50
  %inc53 = add nsw i32 %42, 1
  store i32 %inc53, i32* %i26, align 4, !tbaa !50
  br label %for.cond37

for.end54:                                        ; preds = %for.cond37
  %43 = load float, float* %step.addr, align 4, !tbaa !3
  %44 = bitcast %class.btRaycastVehicle* %this1 to void (%class.btRaycastVehicle*, float)***
  %vtable = load void (%class.btRaycastVehicle*, float)**, void (%class.btRaycastVehicle*, float)*** %44, align 4, !tbaa !9
  %vfn = getelementptr inbounds void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vtable, i64 5
  %45 = load void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vfn, align 4
  call void %45(%class.btRaycastVehicle* %this1, float %43)
  store i32 0, i32* %i26, align 4, !tbaa !50
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc103, %for.end54
  %46 = load i32, i32* %i26, align 4, !tbaa !50
  %m_wheelInfo56 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %call57 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %m_wheelInfo56)
  %cmp58 = icmp slt i32 %46, %call57
  br i1 %cmp58, label %for.body59, label %for.end105

for.body59:                                       ; preds = %for.cond55
  %47 = bitcast %struct.btWheelInfo** %wheel60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #1
  %m_wheelInfo61 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %48 = load i32, i32* %i26, align 4, !tbaa !50
  %call62 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo61, i32 %48)
  store %struct.btWheelInfo* %call62, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %49 = bitcast %class.btVector3* %relpos63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #1
  %50 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_raycastInfo64 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %50, i32 0, i32 0
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo64, i32 0, i32 3
  %call65 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %call65)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %relpos63, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hardPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %call66)
  %51 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #1
  %call67 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel, %class.btRigidBody* %call67, %class.btVector3* nonnull align 4 dereferenceable(16) %relpos63)
  %52 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_raycastInfo68 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %52, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo68, i32 0, i32 6
  %53 = load i8, i8* %m_isInContact, align 4, !tbaa !72, !range !44
  %tobool = trunc i8 %53 to i1
  br i1 %tobool, label %if.then69, label %if.else

if.then69:                                        ; preds = %for.body59
  %54 = bitcast %class.btTransform** %chassisWorldTransform to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #1
  %call70 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle24getChassisWorldTransformEv(%class.btRaycastVehicle* %this1)
  store %class.btTransform* %call70, %class.btTransform** %chassisWorldTransform, align 4, !tbaa !7
  %55 = bitcast %class.btVector3* %fwd to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %55) #1
  %56 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4, !tbaa !7
  %call71 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %56)
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call71, i32 0)
  %call73 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call72)
  %m_indexForwardAxis74 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %57 = load i32, i32* %m_indexForwardAxis74, align 4, !tbaa !25
  %arrayidx75 = getelementptr inbounds float, float* %call73, i32 %57
  %58 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4, !tbaa !7
  %call76 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %58)
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call76, i32 1)
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call77)
  %m_indexForwardAxis79 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %59 = load i32, i32* %m_indexForwardAxis79, align 4, !tbaa !25
  %arrayidx80 = getelementptr inbounds float, float* %call78, i32 %59
  %60 = load %class.btTransform*, %class.btTransform** %chassisWorldTransform, align 4, !tbaa !7
  %call81 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %60)
  %call82 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %call81, i32 2)
  %call83 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call82)
  %m_indexForwardAxis84 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  %61 = load i32, i32* %m_indexForwardAxis84, align 4, !tbaa !25
  %arrayidx85 = getelementptr inbounds float, float* %call83, i32 %61
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %fwd, float* nonnull align 4 dereferenceable(4) %arrayidx75, float* nonnull align 4 dereferenceable(4) %arrayidx80, float* nonnull align 4 dereferenceable(4) %arrayidx85)
  %62 = bitcast float* %proj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #1
  %63 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_raycastInfo87 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %63, i32 0, i32 0
  %m_contactNormalWS88 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo87, i32 0, i32 0
  %call89 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS88)
  store float %call89, float* %proj, align 4, !tbaa !3
  %64 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #1
  %65 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_raycastInfo91 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %65, i32 0, i32 0
  %m_contactNormalWS92 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo91, i32 0, i32 0
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp90, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactNormalWS92, float* nonnull align 4 dereferenceable(4) %proj)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %66 = bitcast %class.btVector3* %ref.tmp90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %66) #1
  %67 = bitcast float* %proj2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #1
  %call94 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %fwd, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call94, float* %proj2, align 4, !tbaa !3
  %68 = load float, float* %proj2, align 4, !tbaa !3
  %69 = load float, float* %step.addr, align 4, !tbaa !3
  %mul95 = fmul float %68, %69
  %70 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_wheelsRadius = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %70, i32 0, i32 7
  %71 = load float, float* %m_wheelsRadius, align 4, !tbaa !59
  %div = fdiv float %mul95, %71
  %72 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_deltaRotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %72, i32 0, i32 14
  store float %div, float* %m_deltaRotation, align 4, !tbaa !67
  %73 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_deltaRotation96 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %73, i32 0, i32 14
  %74 = load float, float* %m_deltaRotation96, align 4, !tbaa !67
  %75 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_rotation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %75, i32 0, i32 13
  %76 = load float, float* %m_rotation, align 4, !tbaa !66
  %add = fadd float %76, %74
  store float %add, float* %m_rotation, align 4, !tbaa !66
  %77 = bitcast float* %proj2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #1
  %78 = bitcast float* %proj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #1
  %79 = bitcast %class.btVector3* %fwd to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #1
  %80 = bitcast %class.btTransform** %chassisWorldTransform to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #1
  br label %if.end100

if.else:                                          ; preds = %for.body59
  %81 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_deltaRotation97 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %81, i32 0, i32 14
  %82 = load float, float* %m_deltaRotation97, align 4, !tbaa !67
  %83 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_rotation98 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %83, i32 0, i32 13
  %84 = load float, float* %m_rotation98, align 4, !tbaa !66
  %add99 = fadd float %84, %82
  store float %add99, float* %m_rotation98, align 4, !tbaa !66
  br label %if.end100

if.end100:                                        ; preds = %if.else, %if.then69
  %85 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel60, align 4, !tbaa !7
  %m_deltaRotation101 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %85, i32 0, i32 14
  %86 = load float, float* %m_deltaRotation101, align 4, !tbaa !67
  %mul102 = fmul float %86, 0x3FEFAE1480000000
  store float %mul102, float* %m_deltaRotation101, align 4, !tbaa !67
  %87 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %87) #1
  %88 = bitcast %class.btVector3* %relpos63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %88) #1
  %89 = bitcast %struct.btWheelInfo** %wheel60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #1
  br label %for.inc103

for.inc103:                                       ; preds = %if.end100
  %90 = load i32, i32* %i26, align 4, !tbaa !50
  %inc104 = add nsw i32 %90, 1
  store i32 %inc104, i32* %i26, align 4, !tbaa !50
  br label %for.cond55

for.end105:                                       ; preds = %for.cond55
  %91 = bitcast i32* %i26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #1
  %92 = bitcast %class.btVector3* %forwardW to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #1
  %93 = bitcast %class.btTransform** %chassisTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #1
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store i32 %i, i32* %i.addr, align 4, !tbaa !50
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

define hidden void @_ZN16btRaycastVehicle16updateSuspensionEf(%class.btRaycastVehicle* %this, float %deltaTime) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %deltaTime.addr = alloca float, align 4
  %chassisMass = alloca float, align 4
  %w_it = alloca i32, align 4
  %wheel_info = alloca %struct.btWheelInfo*, align 4
  %force = alloca float, align 4
  %susp_length = alloca float, align 4
  %current_length = alloca float, align 4
  %length_diff = alloca float, align 4
  %projected_rel_vel = alloca float, align 4
  %susp_damping = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %deltaTime, float* %deltaTime.addr, align 4, !tbaa !3
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast float* %chassisMass to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4, !tbaa !22
  %call = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %1)
  %div = fdiv float 1.000000e+00, %call
  store float %div, float* %chassisMass, align 4, !tbaa !3
  %2 = bitcast i32* %w_it to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  store i32 0, i32* %w_it, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %w_it, align 4, !tbaa !50
  %call2 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %3, %call2
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %4 = bitcast i32* %w_it to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %5 = bitcast %struct.btWheelInfo** %wheel_info to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %6 = load i32, i32* %w_it, align 4, !tbaa !50
  %call3 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %6)
  store %struct.btWheelInfo* %call3, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %7 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %7, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  %8 = load i8, i8* %m_isInContact, align 4, !tbaa !72, !range !44
  %tobool = trunc i8 %8 to i1
  br i1 %tobool, label %if.then, label %if.else17

if.then:                                          ; preds = %for.body
  %9 = bitcast float* %force to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #1
  %10 = bitcast float* %susp_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %call4 = call float @_ZNK11btWheelInfo23getSuspensionRestLengthEv(%struct.btWheelInfo* %11)
  store float %call4, float* %susp_length, align 4, !tbaa !3
  %12 = bitcast float* %current_length to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #1
  %13 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_raycastInfo5 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %13, i32 0, i32 0
  %m_suspensionLength = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo5, i32 0, i32 2
  %14 = load float, float* %m_suspensionLength, align 4, !tbaa !73
  store float %14, float* %current_length, align 4, !tbaa !3
  %15 = bitcast float* %length_diff to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #1
  %16 = load float, float* %susp_length, align 4, !tbaa !3
  %17 = load float, float* %current_length, align 4, !tbaa !3
  %sub = fsub float %16, %17
  store float %sub, float* %length_diff, align 4, !tbaa !3
  %18 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_suspensionStiffness = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %18, i32 0, i32 8
  %19 = load float, float* %m_suspensionStiffness, align 4, !tbaa !60
  %20 = load float, float* %length_diff, align 4, !tbaa !3
  %mul = fmul float %19, %20
  %21 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_clippedInvContactDotSuspension = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %21, i32 0, i32 21
  %22 = load float, float* %m_clippedInvContactDotSuspension, align 4, !tbaa !75
  %mul6 = fmul float %mul, %22
  store float %mul6, float* %force, align 4, !tbaa !3
  %23 = bitcast float* %length_diff to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #1
  %24 = bitcast float* %current_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #1
  %25 = bitcast float* %susp_length to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #1
  %26 = bitcast float* %projected_rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #1
  %27 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_suspensionRelativeVelocity = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %27, i32 0, i32 22
  %28 = load float, float* %m_suspensionRelativeVelocity, align 4, !tbaa !74
  store float %28, float* %projected_rel_vel, align 4, !tbaa !3
  %29 = bitcast float* %susp_damping to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #1
  %30 = load float, float* %projected_rel_vel, align 4, !tbaa !3
  %cmp7 = fcmp olt float %30, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then
  %31 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsDampingCompression = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %31, i32 0, i32 9
  %32 = load float, float* %m_wheelsDampingCompression, align 4, !tbaa !61
  store float %32, float* %susp_damping, align 4, !tbaa !3
  br label %if.end

if.else:                                          ; preds = %if.then
  %33 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsDampingRelaxation = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %33, i32 0, i32 10
  %34 = load float, float* %m_wheelsDampingRelaxation, align 4, !tbaa !62
  store float %34, float* %susp_damping, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  %35 = load float, float* %susp_damping, align 4, !tbaa !3
  %36 = load float, float* %projected_rel_vel, align 4, !tbaa !3
  %mul9 = fmul float %35, %36
  %37 = load float, float* %force, align 4, !tbaa !3
  %sub10 = fsub float %37, %mul9
  store float %sub10, float* %force, align 4, !tbaa !3
  %38 = bitcast float* %susp_damping to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #1
  %39 = bitcast float* %projected_rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #1
  %40 = load float, float* %force, align 4, !tbaa !3
  %41 = load float, float* %chassisMass, align 4, !tbaa !3
  %mul11 = fmul float %40, %41
  %42 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %42, i32 0, i32 23
  store float %mul11, float* %m_wheelsSuspensionForce, align 4, !tbaa !83
  %43 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsSuspensionForce12 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %43, i32 0, i32 23
  %44 = load float, float* %m_wheelsSuspensionForce12, align 4, !tbaa !83
  %cmp13 = fcmp olt float %44, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.end16

if.then14:                                        ; preds = %if.end
  %45 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsSuspensionForce15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %45, i32 0, i32 23
  store float 0.000000e+00, float* %m_wheelsSuspensionForce15, align 4, !tbaa !83
  br label %if.end16

if.end16:                                         ; preds = %if.then14, %if.end
  %46 = bitcast float* %force to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #1
  br label %if.end19

if.else17:                                        ; preds = %for.body
  %47 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheel_info, align 4, !tbaa !7
  %m_wheelsSuspensionForce18 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %47, i32 0, i32 23
  store float 0.000000e+00, float* %m_wheelsSuspensionForce18, align 4, !tbaa !83
  br label %if.end19

if.end19:                                         ; preds = %if.else17, %if.end16
  %48 = bitcast %struct.btWheelInfo** %wheel_info to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #1
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %49 = load i32, i32* %w_it, align 4, !tbaa !50
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* %w_it, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %50 = bitcast float* %chassisMass to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #1
  ret void
}

define linkonce_odr hidden void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %rel_pos.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4, !tbaa !7
  store %class.btVector3* %rel_pos, %class.btVector3** %rel_pos.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !84
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !7
  call void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_angularFactor)
  %tobool = icmp ne float* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #1
  %3 = load %class.btVector3*, %class.btVector3** %rel_pos.addr, align 4, !tbaa !7
  %4 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #1
  %5 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !7
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #1
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #1
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  br label %if.end4

if.end4:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !3
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !3
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !3
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !3
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !3
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !3
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !3
  ret %class.btVector3* %this1
}

define hidden void @_ZN16btRaycastVehicle16setSteeringValueEfi(%class.btRaycastVehicle* %this, float %steering, i32 %wheel) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %steering.addr = alloca float, align 4
  %wheel.addr = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %steering, float* %steering.addr, align 4, !tbaa !3
  store i32 %wheel, i32* %wheel.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %wheel.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %1)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %2 = load float, float* %steering.addr, align 4, !tbaa !3
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 0, i32 12
  store float %2, float* %m_steering, align 4, !tbaa !64
  %4 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #1
  ret void
}

define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this, i32 %index) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %index.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %index, i32* %index.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %0 = load i32, i32* %index.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %0)
  ret %struct.btWheelInfo* %call
}

define hidden float @_ZNK16btRaycastVehicle16getSteeringValueEi(%class.btRaycastVehicle* %this, i32 %wheel) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %wheel.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %wheel, i32* %wheel.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %wheel.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %0)
  %m_steering = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call, i32 0, i32 12
  %1 = load float, float* %m_steering, align 4, !tbaa !64
  ret float %1
}

define hidden nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this, i32 %index) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %index.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %index, i32* %index.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %0 = load i32, i32* %index.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZNK20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %0)
  ret %struct.btWheelInfo* %call
}

define hidden void @_ZN16btRaycastVehicle16applyEngineForceEfi(%class.btRaycastVehicle* %this, float %force, i32 %wheel) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %force.addr = alloca float, align 4
  %wheel.addr = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %force, float* %force.addr, align 4, !tbaa !3
  store i32 %wheel, i32* %wheel.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %wheel.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %1)
  store %struct.btWheelInfo* %call, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %2 = load float, float* %force.addr, align 4, !tbaa !3
  %3 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %3, i32 0, i32 17
  store float %2, float* %m_engineForce, align 4, !tbaa !65
  %4 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #1
  ret void
}

define hidden void @_ZN16btRaycastVehicle8setBrakeEfi(%class.btRaycastVehicle* %this, float %brake, i32 %wheelIndex) #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %brake.addr = alloca float, align 4
  %wheelIndex.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %brake, float* %brake.addr, align 4, !tbaa !3
  store i32 %wheelIndex, i32* %wheelIndex.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load float, float* %brake.addr, align 4, !tbaa !3
  %1 = load i32, i32* %wheelIndex.addr, align 4, !tbaa !50
  %call = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %1)
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call, i32 0, i32 18
  store float %0, float* %m_brake, align 4, !tbaa !68
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4, !tbaa !84
  ret float %0
}

define hidden float @_Z19calcRollingFrictionR19btWheelContactPoint(%struct.btWheelContactPoint* nonnull align 4 dereferenceable(48) %contactPoint) #0 {
entry:
  %contactPoint.addr = alloca %struct.btWheelContactPoint*, align 4
  %j1 = alloca float, align 4
  %contactPosWorld = alloca %class.btVector3*, align 4
  %rel_pos1 = alloca %class.btVector3, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %maxImpulse = alloca float, align 4
  %vel1 = alloca %class.btVector3, align 4
  %vel2 = alloca %class.btVector3, align 4
  %vel = alloca %class.btVector3, align 4
  %vrel = alloca float, align 4
  %ref.tmp = alloca float, align 4
  store %struct.btWheelContactPoint* %contactPoint, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %0 = bitcast float* %j1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  store float 0.000000e+00, float* %j1, align 4, !tbaa !3
  %1 = bitcast %class.btVector3** %contactPosWorld to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  %2 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_frictionPositionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %2, i32 0, i32 2
  store %class.btVector3* %m_frictionPositionWorld, %class.btVector3** %contactPosWorld, align 4, !tbaa !7
  %3 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #1
  %4 = load %class.btVector3*, %class.btVector3** %contactPosWorld, align 4, !tbaa !7
  %5 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_body0 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %5, i32 0, i32 0
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_body0, align 4, !tbaa !85
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %6)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %7 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #1
  %8 = load %class.btVector3*, %class.btVector3** %contactPosWorld, align 4, !tbaa !7
  %9 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_body1 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %9, i32 0, i32 1
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_body1, align 4, !tbaa !87
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %10)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %call1)
  %11 = bitcast float* %maxImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #1
  %12 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_maxImpulse = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %12, i32 0, i32 5
  %13 = load float, float* %m_maxImpulse, align 4, !tbaa !88
  store float %13, float* %maxImpulse, align 4, !tbaa !3
  %14 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %14) #1
  %15 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_body02 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %15, i32 0, i32 0
  %16 = load %class.btRigidBody*, %class.btRigidBody** %m_body02, align 4, !tbaa !85
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel1, %class.btRigidBody* %16, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos1)
  %17 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #1
  %18 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_body13 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %18, i32 0, i32 1
  %19 = load %class.btRigidBody*, %class.btRigidBody** %m_body13, align 4, !tbaa !87
  call void @_ZNK11btRigidBody23getVelocityInLocalPointERK9btVector3(%class.btVector3* sret align 4 %vel2, %class.btRigidBody* %19, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %20 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %vel, %class.btVector3* nonnull align 4 dereferenceable(16) %vel1, %class.btVector3* nonnull align 4 dereferenceable(16) %vel2)
  %21 = bitcast float* %vrel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #1
  %22 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_frictionDirectionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %22, i32 0, i32 3
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_frictionDirectionWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %vel)
  store float %call4, float* %vrel, align 4, !tbaa !3
  %23 = load float, float* %vrel, align 4, !tbaa !3
  %fneg = fneg float %23
  %24 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %contactPoint.addr, align 4, !tbaa !7
  %m_jacDiagABInv = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %24, i32 0, i32 4
  %25 = load float, float* %m_jacDiagABInv, align 4, !tbaa !89
  %mul = fmul float %fneg, %25
  store float %mul, float* %j1, align 4, !tbaa !3
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %j1, float* nonnull align 4 dereferenceable(4) %maxImpulse)
  %26 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #1
  %27 = load float, float* %maxImpulse, align 4, !tbaa !3
  %fneg5 = fneg float %27
  store float %fneg5, float* %ref.tmp, align 4, !tbaa !3
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %j1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %28 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #1
  %29 = load float, float* %j1, align 4, !tbaa !3
  %30 = bitcast float* %vrel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #1
  %31 = bitcast %class.btVector3* %vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #1
  %32 = bitcast %class.btVector3* %vel2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #1
  %33 = bitcast %class.btVector3* %vel1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #1
  %34 = bitcast float* %maxImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #1
  %35 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #1
  %36 = bitcast %class.btVector3* %rel_pos1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #1
  %37 = bitcast %class.btVector3** %contactPosWorld to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #1
  %38 = bitcast float* %j1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #1
  ret float %29
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !7
  store float* %b, float** %b.addr, align 4, !tbaa !7
  %0 = load float*, float** %b.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %2 = load float*, float** %a.addr, align 4, !tbaa !7
  %3 = load float, float* %2, align 4, !tbaa !3
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !7
  %5 = load float, float* %4, align 4, !tbaa !3
  %6 = load float*, float** %a.addr, align 4, !tbaa !7
  store float %5, float* %6, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #3 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !7
  store float* %b, float** %b.addr, align 4, !tbaa !7
  %0 = load float*, float** %a.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %2 = load float*, float** %b.addr, align 4, !tbaa !7
  %3 = load float, float* %2, align 4, !tbaa !3
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !7
  %5 = load float, float* %4, align 4, !tbaa !3
  %6 = load float*, float** %a.addr, align 4, !tbaa !7
  store float %5, float* %6, align 4, !tbaa !3
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

define hidden void @_ZN16btRaycastVehicle14updateFrictionEf(%class.btRaycastVehicle* %this, float %timeStep) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %timeStep.addr = alloca float, align 4
  %numWheel = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numWheelsOnGround = alloca i32, align 4
  %i = alloca i32, align 4
  %wheelInfo = alloca %struct.btWheelInfo*, align 4
  %groundObject = alloca %class.btRigidBody*, align 4
  %i17 = alloca i32, align 4
  %wheelInfo23 = alloca %struct.btWheelInfo*, align 4
  %groundObject26 = alloca %class.btRigidBody*, align 4
  %wheelTrans = alloca %class.btTransform*, align 4
  %wheelBasis0 = alloca %class.btMatrix3x3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %surfNormalWS = alloca %class.btVector3*, align 4
  %proj = alloca float, align 4
  %ref.tmp52 = alloca %class.btVector3, align 4
  %ref.tmp61 = alloca %class.btVector3, align 4
  %sideFactor = alloca float, align 4
  %fwdFactor = alloca float, align 4
  %sliding = alloca i8, align 1
  %wheel = alloca i32, align 4
  %wheelInfo87 = alloca %struct.btWheelInfo*, align 4
  %groundObject90 = alloca %class.btRigidBody*, align 4
  %rollingFriction = alloca float, align 4
  %defaultRollingFrictionImpulse = alloca float, align 4
  %maxImpulse = alloca float, align 4
  %contactPt = alloca %struct.btWheelContactPoint, align 4
  %maximp = alloca float, align 4
  %maximpSide = alloca float, align 4
  %maximpSquared = alloca float, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %impulseSquared = alloca float, align 4
  %factor = alloca float, align 4
  %wheel146 = alloca i32, align 4
  %wheel179 = alloca i32, align 4
  %wheelInfo185 = alloca %struct.btWheelInfo*, align 4
  %rel_pos = alloca %class.btVector3, align 4
  %ref.tmp197 = alloca %class.btVector3, align 4
  %groundObject207 = alloca %class.btRigidBody*, align 4
  %rel_pos2 = alloca %class.btVector3, align 4
  %sideImp = alloca %class.btVector3, align 4
  %vChassisWorldUp = alloca %class.btVector3, align 4
  %ref.tmp222 = alloca %class.btVector3, align 4
  %ref.tmp223 = alloca float, align 4
  %ref.tmp228 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store float %timeStep, float* %timeStep.addr, align 4, !tbaa !3
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast i32* %numWheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  store i32 %call, i32* %numWheel, align 4, !tbaa !50
  %1 = load i32, i32* %numWheel, align 4, !tbaa !50
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %m_forwardWS = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %2 = load i32, i32* %numWheel, align 4, !tbaa !50
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.1* %m_forwardWS, i32 %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #1
  %m_axle = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %5 = load i32, i32* %numWheel, align 4, !tbaa !50
  %6 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #1
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.1* %m_axle, i32 %5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %7 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %7) #1
  %m_forwardImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %8 = load i32, i32* %numWheel, align 4, !tbaa !50
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #1
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !3
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.5* %m_forwardImpulse, i32 %8, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #1
  %m_sideImpulse = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %11 = load i32, i32* %numWheel, align 4, !tbaa !50
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #1
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !3
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.5* %m_sideImpulse, i32 %11, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #1
  %14 = bitcast i32* %numWheelsOnGround to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  store i32 0, i32* %numWheelsOnGround, align 4, !tbaa !50
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #1
  store i32 0, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %16 = load i32, i32* %i, align 4, !tbaa !50
  %call7 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %16, %call7
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %17 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %18 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #1
  %m_wheelInfo = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %19 = load i32, i32* %i, align 4, !tbaa !50
  %call8 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo, i32 %19)
  store %struct.btWheelInfo* %call8, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %20 = bitcast %class.btRigidBody** %groundObject to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #1
  %21 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo, align 4, !tbaa !7
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %21, i32 0, i32 0
  %m_groundObject = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 7
  %22 = load i8*, i8** %m_groundObject, align 4, !tbaa !80
  %23 = bitcast i8* %22 to %class.btRigidBody*
  store %class.btRigidBody* %23, %class.btRigidBody** %groundObject, align 4, !tbaa !7
  %24 = load %class.btRigidBody*, %class.btRigidBody** %groundObject, align 4, !tbaa !7
  %tobool9 = icmp ne %class.btRigidBody* %24, null
  br i1 %tobool9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.body
  %25 = load i32, i32* %numWheelsOnGround, align 4, !tbaa !50
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %numWheelsOnGround, align 4, !tbaa !50
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %for.body
  %m_sideImpulse12 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %26 = load i32, i32* %i, align 4, !tbaa !50
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse12, i32 %26)
  store float 0.000000e+00, float* %call13, align 4, !tbaa !3
  %m_forwardImpulse14 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %27 = load i32, i32* %i, align 4, !tbaa !50
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse14, i32 %27)
  store float 0.000000e+00, float* %call15, align 4, !tbaa !3
  %28 = bitcast %class.btRigidBody** %groundObject to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #1
  %29 = bitcast %struct.btWheelInfo** %wheelInfo to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #1
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %30 = load i32, i32* %i, align 4, !tbaa !50
  %inc16 = add nsw i32 %30, 1
  store i32 %inc16, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %31 = bitcast i32* %i17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #1
  store i32 0, i32* %i17, align 4, !tbaa !50
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc79, %for.end
  %32 = load i32, i32* %i17, align 4, !tbaa !50
  %call19 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp20 = icmp slt i32 %32, %call19
  br i1 %cmp20, label %for.body22, label %for.cond.cleanup21

for.cond.cleanup21:                               ; preds = %for.cond18
  store i32 5, i32* %cleanup.dest.slot, align 4
  %33 = bitcast i32* %i17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #1
  br label %for.end81

for.body22:                                       ; preds = %for.cond18
  %34 = bitcast %struct.btWheelInfo** %wheelInfo23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #1
  %m_wheelInfo24 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %35 = load i32, i32* %i17, align 4, !tbaa !50
  %call25 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo24, i32 %35)
  store %struct.btWheelInfo* %call25, %struct.btWheelInfo** %wheelInfo23, align 4, !tbaa !7
  %36 = bitcast %class.btRigidBody** %groundObject26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #1
  %37 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo23, align 4, !tbaa !7
  %m_raycastInfo27 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %37, i32 0, i32 0
  %m_groundObject28 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo27, i32 0, i32 7
  %38 = load i8*, i8** %m_groundObject28, align 4, !tbaa !80
  %39 = bitcast i8* %38 to %class.btRigidBody*
  store %class.btRigidBody* %39, %class.btRigidBody** %groundObject26, align 4, !tbaa !7
  %40 = load %class.btRigidBody*, %class.btRigidBody** %groundObject26, align 4, !tbaa !7
  %tobool29 = icmp ne %class.btRigidBody* %40, null
  br i1 %tobool29, label %if.then30, label %if.end78

if.then30:                                        ; preds = %for.body22
  %41 = bitcast %class.btTransform** %wheelTrans to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #1
  %42 = load i32, i32* %i17, align 4, !tbaa !50
  %call31 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK16btRaycastVehicle19getWheelTransformWSEi(%class.btRaycastVehicle* %this1, i32 %42)
  store %class.btTransform* %call31, %class.btTransform** %wheelTrans, align 4, !tbaa !7
  %43 = bitcast %class.btMatrix3x3* %wheelBasis0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %43) #1
  %44 = load %class.btTransform*, %class.btTransform** %wheelTrans, align 4, !tbaa !7
  %call32 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %44)
  %call33 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %wheelBasis0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call32)
  %45 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #1
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 0)
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call35)
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %46 = load i32, i32* %m_indexRightAxis, align 4, !tbaa !23
  %arrayidx = getelementptr inbounds float, float* %call36, i32 %46
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 1)
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call37)
  %m_indexRightAxis39 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %47 = load i32, i32* %m_indexRightAxis39, align 4, !tbaa !23
  %arrayidx40 = getelementptr inbounds float, float* %call38, i32 %47
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %wheelBasis0, i32 2)
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call41)
  %m_indexRightAxis43 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %48 = load i32, i32* %m_indexRightAxis43, align 4, !tbaa !23
  %arrayidx44 = getelementptr inbounds float, float* %call42, i32 %48
  %call45 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp34, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx40, float* nonnull align 4 dereferenceable(4) %arrayidx44)
  %m_axle46 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %49 = load i32, i32* %i17, align 4, !tbaa !50
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle46, i32 %49)
  %50 = bitcast %class.btVector3* %call47 to i8*
  %51 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false), !tbaa.struct !29
  %52 = bitcast %class.btVector3* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #1
  %53 = bitcast %class.btVector3** %surfNormalWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #1
  %54 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo23, align 4, !tbaa !7
  %m_raycastInfo48 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %54, i32 0, i32 0
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo48, i32 0, i32 0
  store %class.btVector3* %m_contactNormalWS, %class.btVector3** %surfNormalWS, align 4, !tbaa !7
  %55 = bitcast float* %proj to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #1
  %m_axle49 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %56 = load i32, i32* %i17, align 4, !tbaa !50
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle49, i32 %56)
  %57 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4, !tbaa !7
  %call51 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call50, %class.btVector3* nonnull align 4 dereferenceable(16) %57)
  store float %call51, float* %proj, align 4, !tbaa !3
  %58 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %58) #1
  %59 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4, !tbaa !7
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp52, %class.btVector3* nonnull align 4 dereferenceable(16) %59, float* nonnull align 4 dereferenceable(4) %proj)
  %m_axle53 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %60 = load i32, i32* %i17, align 4, !tbaa !50
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle53, i32 %60)
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call54, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp52)
  %61 = bitcast %class.btVector3* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #1
  %m_axle56 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %62 = load i32, i32* %i17, align 4, !tbaa !50
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle56, i32 %62)
  %call58 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call57)
  %m_axle59 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %63 = load i32, i32* %i17, align 4, !tbaa !50
  %call60 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle59, i32 %63)
  %64 = bitcast %class.btVector3* %call60 to i8*
  %65 = bitcast %class.btVector3* %call58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 16, i1 false), !tbaa.struct !29
  %66 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %66) #1
  %67 = load %class.btVector3*, %class.btVector3** %surfNormalWS, align 4, !tbaa !7
  %m_axle62 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %68 = load i32, i32* %i17, align 4, !tbaa !50
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle62, i32 %68)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp61, %class.btVector3* %67, %class.btVector3* nonnull align 4 dereferenceable(16) %call63)
  %m_forwardWS64 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %69 = load i32, i32* %i17, align 4, !tbaa !50
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_forwardWS64, i32 %69)
  %70 = bitcast %class.btVector3* %call65 to i8*
  %71 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 16, i1 false), !tbaa.struct !29
  %72 = bitcast %class.btVector3* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %72) #1
  %m_forwardWS66 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %73 = load i32, i32* %i17, align 4, !tbaa !50
  %call67 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_forwardWS66, i32 %73)
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call67)
  %m_chassisBody = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %74 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody, align 4, !tbaa !22
  %75 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo23, align 4, !tbaa !7
  %m_raycastInfo69 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %75, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo69, i32 0, i32 1
  %76 = load %class.btRigidBody*, %class.btRigidBody** %groundObject26, align 4, !tbaa !7
  %77 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo23, align 4, !tbaa !7
  %m_raycastInfo70 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %77, i32 0, i32 0
  %m_contactPointWS71 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo70, i32 0, i32 1
  %m_axle72 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %78 = load i32, i32* %i17, align 4, !tbaa !50
  %call73 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle72, i32 %78)
  %m_sideImpulse74 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %79 = load i32, i32* %i17, align 4, !tbaa !50
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse74, i32 %79)
  %80 = load float, float* %timeStep.addr, align 4, !tbaa !3
  call void @_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff(%class.btRigidBody* nonnull align 4 dereferenceable(616) %74, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btRigidBody* nonnull align 4 dereferenceable(616) %76, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS71, float 0.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %call73, float* nonnull align 4 dereferenceable(4) %call75, float %80)
  %81 = load float, float* @sideFrictionStiffness2, align 4, !tbaa !3
  %m_sideImpulse76 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %82 = load i32, i32* %i17, align 4, !tbaa !50
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse76, i32 %82)
  %83 = load float, float* %call77, align 4, !tbaa !3
  %mul = fmul float %83, %81
  store float %mul, float* %call77, align 4, !tbaa !3
  %84 = bitcast float* %proj to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #1
  %85 = bitcast %class.btVector3** %surfNormalWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #1
  %86 = bitcast %class.btMatrix3x3* %wheelBasis0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %86) #1
  %87 = bitcast %class.btTransform** %wheelTrans to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #1
  br label %if.end78

if.end78:                                         ; preds = %if.then30, %for.body22
  %88 = bitcast %class.btRigidBody** %groundObject26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #1
  %89 = bitcast %struct.btWheelInfo** %wheelInfo23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #1
  br label %for.inc79

for.inc79:                                        ; preds = %if.end78
  %90 = load i32, i32* %i17, align 4, !tbaa !50
  %inc80 = add nsw i32 %90, 1
  store i32 %inc80, i32* %i17, align 4, !tbaa !50
  br label %for.cond18

for.end81:                                        ; preds = %for.cond.cleanup21
  %91 = bitcast float* %sideFactor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #1
  store float 1.000000e+00, float* %sideFactor, align 4, !tbaa !3
  %92 = bitcast float* %fwdFactor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #1
  store float 5.000000e-01, float* %fwdFactor, align 4, !tbaa !3
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %sliding) #1
  store i8 0, i8* %sliding, align 1, !tbaa !28
  %93 = bitcast i32* %wheel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #1
  store i32 0, i32* %wheel, align 4, !tbaa !50
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc141, %for.end81
  %94 = load i32, i32* %wheel, align 4, !tbaa !50
  %call83 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp84 = icmp slt i32 %94, %call83
  br i1 %cmp84, label %for.body86, label %for.cond.cleanup85

for.cond.cleanup85:                               ; preds = %for.cond82
  store i32 8, i32* %cleanup.dest.slot, align 4
  %95 = bitcast i32* %wheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #1
  br label %for.end143

for.body86:                                       ; preds = %for.cond82
  %96 = bitcast %struct.btWheelInfo** %wheelInfo87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #1
  %m_wheelInfo88 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %97 = load i32, i32* %wheel, align 4, !tbaa !50
  %call89 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo88, i32 %97)
  store %struct.btWheelInfo* %call89, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %98 = bitcast %class.btRigidBody** %groundObject90 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #1
  %99 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_raycastInfo91 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %99, i32 0, i32 0
  %m_groundObject92 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo91, i32 0, i32 7
  %100 = load i8*, i8** %m_groundObject92, align 4, !tbaa !80
  %101 = bitcast i8* %100 to %class.btRigidBody*
  store %class.btRigidBody* %101, %class.btRigidBody** %groundObject90, align 4, !tbaa !7
  %102 = bitcast float* %rollingFriction to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #1
  store float 0.000000e+00, float* %rollingFriction, align 4, !tbaa !3
  %103 = load %class.btRigidBody*, %class.btRigidBody** %groundObject90, align 4, !tbaa !7
  %tobool93 = icmp ne %class.btRigidBody* %103, null
  br i1 %tobool93, label %if.then94, label %if.end109

if.then94:                                        ; preds = %for.body86
  %104 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_engineForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %104, i32 0, i32 17
  %105 = load float, float* %m_engineForce, align 4, !tbaa !65
  %cmp95 = fcmp une float %105, 0.000000e+00
  br i1 %cmp95, label %if.then96, label %if.else

if.then96:                                        ; preds = %if.then94
  %106 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_engineForce97 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %106, i32 0, i32 17
  %107 = load float, float* %m_engineForce97, align 4, !tbaa !65
  %108 = load float, float* %timeStep.addr, align 4, !tbaa !3
  %mul98 = fmul float %107, %108
  store float %mul98, float* %rollingFriction, align 4, !tbaa !3
  br label %if.end108

if.else:                                          ; preds = %if.then94
  %109 = bitcast float* %defaultRollingFrictionImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #1
  store float 0.000000e+00, float* %defaultRollingFrictionImpulse, align 4, !tbaa !3
  %110 = bitcast float* %maxImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #1
  %111 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_brake = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %111, i32 0, i32 18
  %112 = load float, float* %m_brake, align 4, !tbaa !68
  %tobool99 = fcmp une float %112, 0.000000e+00
  br i1 %tobool99, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %113 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_brake100 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %113, i32 0, i32 18
  %114 = load float, float* %m_brake100, align 4, !tbaa !68
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %115 = load float, float* %defaultRollingFrictionImpulse, align 4, !tbaa !3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %114, %cond.true ], [ %115, %cond.false ]
  store float %cond, float* %maxImpulse, align 4, !tbaa !3
  %116 = bitcast %struct.btWheelContactPoint* %contactPt to i8*
  call void @llvm.lifetime.start.p0i8(i64 48, i8* %116) #1
  %m_chassisBody101 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %117 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody101, align 4, !tbaa !22
  %118 = load %class.btRigidBody*, %class.btRigidBody** %groundObject90, align 4, !tbaa !7
  %119 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_raycastInfo102 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %119, i32 0, i32 0
  %m_contactPointWS103 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo102, i32 0, i32 1
  %m_forwardWS104 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %120 = load i32, i32* %wheel, align 4, !tbaa !50
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_forwardWS104, i32 %120)
  %121 = load float, float* %maxImpulse, align 4, !tbaa !3
  %call106 = call %struct.btWheelContactPoint* @_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f(%struct.btWheelContactPoint* %contactPt, %class.btRigidBody* %117, %class.btRigidBody* %118, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS103, %class.btVector3* nonnull align 4 dereferenceable(16) %call105, float %121)
  %call107 = call float @_Z19calcRollingFrictionR19btWheelContactPoint(%struct.btWheelContactPoint* nonnull align 4 dereferenceable(48) %contactPt)
  store float %call107, float* %rollingFriction, align 4, !tbaa !3
  %122 = bitcast %struct.btWheelContactPoint* %contactPt to i8*
  call void @llvm.lifetime.end.p0i8(i64 48, i8* %122) #1
  %123 = bitcast float* %maxImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %123) #1
  %124 = bitcast float* %defaultRollingFrictionImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %124) #1
  br label %if.end108

if.end108:                                        ; preds = %cond.end, %if.then96
  br label %if.end109

if.end109:                                        ; preds = %if.end108, %for.body86
  %m_forwardImpulse110 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %125 = load i32, i32* %wheel, align 4, !tbaa !50
  %call111 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse110, i32 %125)
  store float 0.000000e+00, float* %call111, align 4, !tbaa !3
  %m_wheelInfo112 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %126 = load i32, i32* %wheel, align 4, !tbaa !50
  %call113 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo112, i32 %126)
  %m_skidInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call113, i32 0, i32 24
  store float 1.000000e+00, float* %m_skidInfo, align 4, !tbaa !90
  %127 = load %class.btRigidBody*, %class.btRigidBody** %groundObject90, align 4, !tbaa !7
  %tobool114 = icmp ne %class.btRigidBody* %127, null
  br i1 %tobool114, label %if.then115, label %if.end140

if.then115:                                       ; preds = %if.end109
  %m_wheelInfo116 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %128 = load i32, i32* %wheel, align 4, !tbaa !50
  %call117 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo116, i32 %128)
  %m_skidInfo118 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call117, i32 0, i32 24
  store float 1.000000e+00, float* %m_skidInfo118, align 4, !tbaa !90
  %129 = bitcast float* %maximp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #1
  %130 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_wheelsSuspensionForce = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %130, i32 0, i32 23
  %131 = load float, float* %m_wheelsSuspensionForce, align 4, !tbaa !83
  %132 = load float, float* %timeStep.addr, align 4, !tbaa !3
  %mul119 = fmul float %131, %132
  %133 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo87, align 4, !tbaa !7
  %m_frictionSlip = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %133, i32 0, i32 11
  %134 = load float, float* %m_frictionSlip, align 4, !tbaa !63
  %mul120 = fmul float %mul119, %134
  store float %mul120, float* %maximp, align 4, !tbaa !3
  %135 = bitcast float* %maximpSide to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #1
  %136 = load float, float* %maximp, align 4, !tbaa !3
  store float %136, float* %maximpSide, align 4, !tbaa !3
  %137 = bitcast float* %maximpSquared to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %137) #1
  %138 = load float, float* %maximp, align 4, !tbaa !3
  %139 = load float, float* %maximpSide, align 4, !tbaa !3
  %mul121 = fmul float %138, %139
  store float %mul121, float* %maximpSquared, align 4, !tbaa !3
  %140 = load float, float* %rollingFriction, align 4, !tbaa !3
  %m_forwardImpulse122 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %141 = load i32, i32* %wheel, align 4, !tbaa !50
  %call123 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse122, i32 %141)
  store float %140, float* %call123, align 4, !tbaa !3
  %142 = bitcast float* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %142) #1
  %m_forwardImpulse124 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %143 = load i32, i32* %wheel, align 4, !tbaa !50
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse124, i32 %143)
  %144 = load float, float* %call125, align 4, !tbaa !3
  %145 = load float, float* %fwdFactor, align 4, !tbaa !3
  %mul126 = fmul float %144, %145
  store float %mul126, float* %x, align 4, !tbaa !3
  %146 = bitcast float* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #1
  %m_sideImpulse127 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %147 = load i32, i32* %wheel, align 4, !tbaa !50
  %call128 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse127, i32 %147)
  %148 = load float, float* %call128, align 4, !tbaa !3
  %149 = load float, float* %sideFactor, align 4, !tbaa !3
  %mul129 = fmul float %148, %149
  store float %mul129, float* %y, align 4, !tbaa !3
  %150 = bitcast float* %impulseSquared to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %150) #1
  %151 = load float, float* %x, align 4, !tbaa !3
  %152 = load float, float* %x, align 4, !tbaa !3
  %mul130 = fmul float %151, %152
  %153 = load float, float* %y, align 4, !tbaa !3
  %154 = load float, float* %y, align 4, !tbaa !3
  %mul131 = fmul float %153, %154
  %add = fadd float %mul130, %mul131
  store float %add, float* %impulseSquared, align 4, !tbaa !3
  %155 = load float, float* %impulseSquared, align 4, !tbaa !3
  %156 = load float, float* %maximpSquared, align 4, !tbaa !3
  %cmp132 = fcmp ogt float %155, %156
  br i1 %cmp132, label %if.then133, label %if.end139

if.then133:                                       ; preds = %if.then115
  store i8 1, i8* %sliding, align 1, !tbaa !28
  %157 = bitcast float* %factor to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %157) #1
  %158 = load float, float* %maximp, align 4, !tbaa !3
  %159 = load float, float* %impulseSquared, align 4, !tbaa !3
  %call134 = call float @_Z6btSqrtf(float %159)
  %div = fdiv float %158, %call134
  store float %div, float* %factor, align 4, !tbaa !3
  %160 = load float, float* %factor, align 4, !tbaa !3
  %m_wheelInfo135 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %161 = load i32, i32* %wheel, align 4, !tbaa !50
  %call136 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo135, i32 %161)
  %m_skidInfo137 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call136, i32 0, i32 24
  %162 = load float, float* %m_skidInfo137, align 4, !tbaa !90
  %mul138 = fmul float %162, %160
  store float %mul138, float* %m_skidInfo137, align 4, !tbaa !90
  %163 = bitcast float* %factor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #1
  br label %if.end139

if.end139:                                        ; preds = %if.then133, %if.then115
  %164 = bitcast float* %impulseSquared to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #1
  %165 = bitcast float* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #1
  %166 = bitcast float* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #1
  %167 = bitcast float* %maximpSquared to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #1
  %168 = bitcast float* %maximpSide to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #1
  %169 = bitcast float* %maximp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #1
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %if.end109
  %170 = bitcast float* %rollingFriction to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #1
  %171 = bitcast %class.btRigidBody** %groundObject90 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #1
  %172 = bitcast %struct.btWheelInfo** %wheelInfo87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #1
  br label %for.inc141

for.inc141:                                       ; preds = %if.end140
  %173 = load i32, i32* %wheel, align 4, !tbaa !50
  %inc142 = add nsw i32 %173, 1
  store i32 %inc142, i32* %wheel, align 4, !tbaa !50
  br label %for.cond82

for.end143:                                       ; preds = %for.cond.cleanup85
  %174 = load i8, i8* %sliding, align 1, !tbaa !28, !range !44
  %tobool144 = trunc i8 %174 to i1
  br i1 %tobool144, label %if.then145, label %if.end178

if.then145:                                       ; preds = %for.end143
  %175 = bitcast i32* %wheel146 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %175) #1
  store i32 0, i32* %wheel146, align 4, !tbaa !50
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc175, %if.then145
  %176 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call148 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp149 = icmp slt i32 %176, %call148
  br i1 %cmp149, label %for.body151, label %for.cond.cleanup150

for.cond.cleanup150:                              ; preds = %for.cond147
  store i32 11, i32* %cleanup.dest.slot, align 4
  %177 = bitcast i32* %wheel146 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #1
  br label %for.end177

for.body151:                                      ; preds = %for.cond147
  %m_sideImpulse152 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %178 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call153 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse152, i32 %178)
  %179 = load float, float* %call153, align 4, !tbaa !3
  %cmp154 = fcmp une float %179, 0.000000e+00
  br i1 %cmp154, label %if.then155, label %if.end174

if.then155:                                       ; preds = %for.body151
  %m_wheelInfo156 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %180 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call157 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo156, i32 %180)
  %m_skidInfo158 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call157, i32 0, i32 24
  %181 = load float, float* %m_skidInfo158, align 4, !tbaa !90
  %cmp159 = fcmp olt float %181, 1.000000e+00
  br i1 %cmp159, label %if.then160, label %if.end173

if.then160:                                       ; preds = %if.then155
  %m_wheelInfo161 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %182 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call162 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo161, i32 %182)
  %m_skidInfo163 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call162, i32 0, i32 24
  %183 = load float, float* %m_skidInfo163, align 4, !tbaa !90
  %m_forwardImpulse164 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %184 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call165 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse164, i32 %184)
  %185 = load float, float* %call165, align 4, !tbaa !3
  %mul166 = fmul float %185, %183
  store float %mul166, float* %call165, align 4, !tbaa !3
  %m_wheelInfo167 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %186 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call168 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo167, i32 %186)
  %m_skidInfo169 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call168, i32 0, i32 24
  %187 = load float, float* %m_skidInfo169, align 4, !tbaa !90
  %m_sideImpulse170 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %188 = load i32, i32* %wheel146, align 4, !tbaa !50
  %call171 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse170, i32 %188)
  %189 = load float, float* %call171, align 4, !tbaa !3
  %mul172 = fmul float %189, %187
  store float %mul172, float* %call171, align 4, !tbaa !3
  br label %if.end173

if.end173:                                        ; preds = %if.then160, %if.then155
  br label %if.end174

if.end174:                                        ; preds = %if.end173, %for.body151
  br label %for.inc175

for.inc175:                                       ; preds = %if.end174
  %190 = load i32, i32* %wheel146, align 4, !tbaa !50
  %inc176 = add nsw i32 %190, 1
  store i32 %inc176, i32* %wheel146, align 4, !tbaa !50
  br label %for.cond147

for.end177:                                       ; preds = %for.cond.cleanup150
  br label %if.end178

if.end178:                                        ; preds = %for.end177, %for.end143
  %191 = bitcast i32* %wheel179 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %191) #1
  store i32 0, i32* %wheel179, align 4, !tbaa !50
  br label %for.cond180

for.cond180:                                      ; preds = %for.inc230, %if.end178
  %192 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call181 = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp182 = icmp slt i32 %192, %call181
  br i1 %cmp182, label %for.body184, label %for.cond.cleanup183

for.cond.cleanup183:                              ; preds = %for.cond180
  store i32 14, i32* %cleanup.dest.slot, align 4
  %193 = bitcast i32* %wheel179 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #1
  br label %for.end232

for.body184:                                      ; preds = %for.cond180
  %194 = bitcast %struct.btWheelInfo** %wheelInfo185 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %194) #1
  %m_wheelInfo186 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %195 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call187 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo186, i32 %195)
  store %struct.btWheelInfo* %call187, %struct.btWheelInfo** %wheelInfo185, align 4, !tbaa !7
  %196 = bitcast %class.btVector3* %rel_pos to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %196) #1
  %197 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo185, align 4, !tbaa !7
  %m_raycastInfo188 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %197, i32 0, i32 0
  %m_contactPointWS189 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo188, i32 0, i32 1
  %m_chassisBody190 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %198 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody190, align 4, !tbaa !22
  %call191 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %198)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS189, %class.btVector3* nonnull align 4 dereferenceable(16) %call191)
  %m_forwardImpulse192 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %199 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call193 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse192, i32 %199)
  %200 = load float, float* %call193, align 4, !tbaa !3
  %cmp194 = fcmp une float %200, 0.000000e+00
  br i1 %cmp194, label %if.then195, label %if.end202

if.then195:                                       ; preds = %for.body184
  %m_chassisBody196 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %201 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody196, align 4, !tbaa !22
  %202 = bitcast %class.btVector3* %ref.tmp197 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %202) #1
  %m_forwardWS198 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 1
  %203 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call199 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_forwardWS198, i32 %203)
  %m_forwardImpulse200 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 3
  %204 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call201 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_forwardImpulse200, i32 %204)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %call199, float* nonnull align 4 dereferenceable(4) %call201)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %201, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  %205 = bitcast %class.btVector3* %ref.tmp197 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %205) #1
  br label %if.end202

if.end202:                                        ; preds = %if.then195, %for.body184
  %m_sideImpulse203 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %206 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call204 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse203, i32 %206)
  %207 = load float, float* %call204, align 4, !tbaa !3
  %cmp205 = fcmp une float %207, 0.000000e+00
  br i1 %cmp205, label %if.then206, label %if.end229

if.then206:                                       ; preds = %if.end202
  %208 = bitcast %class.btRigidBody** %groundObject207 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %208) #1
  %m_wheelInfo208 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 17
  %209 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call209 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN20btAlignedObjectArrayI11btWheelInfoEixEi(%class.btAlignedObjectArray.9* %m_wheelInfo208, i32 %209)
  %m_raycastInfo210 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call209, i32 0, i32 0
  %m_groundObject211 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo210, i32 0, i32 7
  %210 = load i8*, i8** %m_groundObject211, align 4, !tbaa !80
  %211 = bitcast i8* %210 to %class.btRigidBody*
  store %class.btRigidBody* %211, %class.btRigidBody** %groundObject207, align 4, !tbaa !7
  %212 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %212) #1
  %213 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo185, align 4, !tbaa !7
  %m_raycastInfo212 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %213, i32 0, i32 0
  %m_contactPointWS213 = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo212, i32 0, i32 1
  %214 = load %class.btRigidBody*, %class.btRigidBody** %groundObject207, align 4, !tbaa !7
  %call214 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %214)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rel_pos2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS213, %class.btVector3* nonnull align 4 dereferenceable(16) %call214)
  %215 = bitcast %class.btVector3* %sideImp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %215) #1
  %m_axle215 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 2
  %216 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call216 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %m_axle215, i32 %216)
  %m_sideImpulse217 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 4
  %217 = load i32, i32* %wheel179, align 4, !tbaa !50
  %call218 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %m_sideImpulse217, i32 %217)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %sideImp, %class.btVector3* nonnull align 4 dereferenceable(16) %call216, float* nonnull align 4 dereferenceable(4) %call218)
  %218 = bitcast %class.btVector3* %vChassisWorldUp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %218) #1
  %call219 = call %class.btRigidBody* @_ZN16btRaycastVehicle12getRigidBodyEv(%class.btRaycastVehicle* %this1)
  %call220 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %call219)
  %call221 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call220)
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  %219 = load i32, i32* %m_indexUpAxis, align 4, !tbaa !24
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %vChassisWorldUp, %class.btMatrix3x3* %call221, i32 %219)
  %220 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %220) #1
  %221 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %221) #1
  %call224 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vChassisWorldUp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  %222 = load %struct.btWheelInfo*, %struct.btWheelInfo** %wheelInfo185, align 4, !tbaa !7
  %m_rollInfluence = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %222, i32 0, i32 15
  %223 = load float, float* %m_rollInfluence, align 4, !tbaa !69
  %sub = fsub float 1.000000e+00, %223
  %mul225 = fmul float %call224, %sub
  store float %mul225, float* %ref.tmp223, align 4, !tbaa !3
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp222, %class.btVector3* nonnull align 4 dereferenceable(16) %vChassisWorldUp, float* nonnull align 4 dereferenceable(4) %ref.tmp223)
  %call226 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %rel_pos, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp222)
  %224 = bitcast float* %ref.tmp223 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #1
  %225 = bitcast %class.btVector3* %ref.tmp222 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %225) #1
  %m_chassisBody227 = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 13
  %226 = load %class.btRigidBody*, %class.btRigidBody** %m_chassisBody227, align 4, !tbaa !22
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %226, %class.btVector3* nonnull align 4 dereferenceable(16) %sideImp, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos)
  %227 = load %class.btRigidBody*, %class.btRigidBody** %groundObject207, align 4, !tbaa !7
  %228 = bitcast %class.btVector3* %ref.tmp228 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %228) #1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp228, %class.btVector3* nonnull align 4 dereferenceable(16) %sideImp)
  call void @_ZN11btRigidBody12applyImpulseERK9btVector3S2_(%class.btRigidBody* %227, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp228, %class.btVector3* nonnull align 4 dereferenceable(16) %rel_pos2)
  %229 = bitcast %class.btVector3* %ref.tmp228 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %229) #1
  %230 = bitcast %class.btVector3* %vChassisWorldUp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %230) #1
  %231 = bitcast %class.btVector3* %sideImp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %231) #1
  %232 = bitcast %class.btVector3* %rel_pos2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %232) #1
  %233 = bitcast %class.btRigidBody** %groundObject207 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #1
  br label %if.end229

if.end229:                                        ; preds = %if.then206, %if.end202
  %234 = bitcast %class.btVector3* %rel_pos to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %234) #1
  %235 = bitcast %struct.btWheelInfo** %wheelInfo185 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #1
  br label %for.inc230

for.inc230:                                       ; preds = %if.end229
  %236 = load i32, i32* %wheel179, align 4, !tbaa !50
  %inc231 = add nsw i32 %236, 1
  store i32 %inc231, i32* %wheel179, align 4, !tbaa !50
  br label %for.cond180

for.end232:                                       ; preds = %for.cond.cleanup183
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %sliding) #1
  %237 = bitcast float* %fwdFactor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #1
  %238 = bitcast float* %sideFactor to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #1
  %239 = bitcast i32* %numWheelsOnGround to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #1
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end232, %if.then
  %240 = bitcast i32* %numWheel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #1
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.1* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !50
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !50
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %2 = load i32, i32* %curSize, align 4, !tbaa !50
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #1
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  store i32 %4, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %6 = load i32, i32* %curSize, align 4, !tbaa !50
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !91
  %9 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  %14 = load i32, i32* %curSize, align 4, !tbaa !50
  store i32 %14, i32* %i6, align 4, !tbaa !50
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !50
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !91
  %19 = load i32, i32* %i6, align 4, !tbaa !50
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !7
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !29
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !50
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !50
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !92
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.5* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !50
  store float* %fillData, float** %fillData.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !50
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %2 = load i32, i32* %curSize, align 4, !tbaa !50
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #1
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  store i32 %4, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %6 = load i32, i32* %curSize, align 4, !tbaa !50
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !93
  %9 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  %14 = load i32, i32* %curSize, align 4, !tbaa !50
  store i32 %14, i32* %i6, align 4, !tbaa !50
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !50
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %18 = load float*, float** %m_data11, align 4, !tbaa !93
  %19 = load i32, i32* %i6, align 4, !tbaa !50
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 %19
  %20 = bitcast float* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to float*
  %22 = load float*, float** %fillData.addr, align 4, !tbaa !7
  %23 = load float, float* %22, align 4, !tbaa !3
  store float %23, float* %21, align 4, !tbaa !3
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !50
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !50
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !50
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !94
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !93
  %1 = load i32, i32* %n.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #7 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !29
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !29
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !29
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store i32 %i, i32* %i.addr, align 4, !tbaa !50
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !91
  %1 = load i32, i32* %n.addr, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

declare void @_Z22resolveSingleBilateralR11btRigidBodyRK9btVector3S0_S3_fS3_Rff(%class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btRigidBody* nonnull align 4 dereferenceable(616), %class.btVector3* nonnull align 4 dereferenceable(16), float, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float) #4

define linkonce_odr hidden %struct.btWheelContactPoint* @_ZN19btWheelContactPointC2EP11btRigidBodyS1_RK9btVector3S4_f(%struct.btWheelContactPoint* returned %this, %class.btRigidBody* %body0, %class.btRigidBody* %body1, %class.btVector3* nonnull align 4 dereferenceable(16) %frictionPosWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %frictionDirectionWorld, float %maxImpulse) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btWheelContactPoint*, align 4
  %body0.addr = alloca %class.btRigidBody*, align 4
  %body1.addr = alloca %class.btRigidBody*, align 4
  %frictionPosWorld.addr = alloca %class.btVector3*, align 4
  %frictionDirectionWorld.addr = alloca %class.btVector3*, align 4
  %maxImpulse.addr = alloca float, align 4
  %denom0 = alloca float, align 4
  %denom1 = alloca float, align 4
  %relaxation = alloca float, align 4
  store %struct.btWheelContactPoint* %this, %struct.btWheelContactPoint** %this.addr, align 4, !tbaa !7
  store %class.btRigidBody* %body0, %class.btRigidBody** %body0.addr, align 4, !tbaa !7
  store %class.btRigidBody* %body1, %class.btRigidBody** %body1.addr, align 4, !tbaa !7
  store %class.btVector3* %frictionPosWorld, %class.btVector3** %frictionPosWorld.addr, align 4, !tbaa !7
  store %class.btVector3* %frictionDirectionWorld, %class.btVector3** %frictionDirectionWorld.addr, align 4, !tbaa !7
  store float %maxImpulse, float* %maxImpulse.addr, align 4, !tbaa !3
  %this1 = load %struct.btWheelContactPoint*, %struct.btWheelContactPoint** %this.addr, align 4
  %m_body0 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 0
  %0 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4, !tbaa !7
  store %class.btRigidBody* %0, %class.btRigidBody** %m_body0, align 4, !tbaa !85
  %m_body1 = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 1
  %1 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !7
  store %class.btRigidBody* %1, %class.btRigidBody** %m_body1, align 4, !tbaa !87
  %m_frictionPositionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4, !tbaa !7
  %3 = bitcast %class.btVector3* %m_frictionPositionWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !29
  %m_frictionDirectionWorld = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 3
  %5 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4, !tbaa !7
  %6 = bitcast %class.btVector3* %m_frictionDirectionWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !29
  %m_maxImpulse = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 5
  %8 = load float, float* %maxImpulse.addr, align 4, !tbaa !3
  store float %8, float* %m_maxImpulse, align 4, !tbaa !88
  %9 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #1
  %10 = load %class.btRigidBody*, %class.btRigidBody** %body0.addr, align 4, !tbaa !7
  %11 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4, !tbaa !7
  %12 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4, !tbaa !7
  %call = call float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  store float %call, float* %denom0, align 4, !tbaa !3
  %13 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body1.addr, align 4, !tbaa !7
  %15 = load %class.btVector3*, %class.btVector3** %frictionPosWorld.addr, align 4, !tbaa !7
  %16 = load %class.btVector3*, %class.btVector3** %frictionDirectionWorld.addr, align 4, !tbaa !7
  %call2 = call float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  store float %call2, float* %denom1, align 4, !tbaa !3
  %17 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #1
  store float 1.000000e+00, float* %relaxation, align 4, !tbaa !3
  %18 = load float, float* %relaxation, align 4, !tbaa !3
  %19 = load float, float* %denom0, align 4, !tbaa !3
  %20 = load float, float* %denom1, align 4, !tbaa !3
  %add = fadd float %19, %20
  %div = fdiv float %18, %add
  %m_jacDiagABInv = getelementptr inbounds %struct.btWheelContactPoint, %struct.btWheelContactPoint* %this1, i32 0, i32 4
  store float %div, float* %m_jacDiagABInv, align 4, !tbaa !89
  %21 = bitcast float* %relaxation to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #1
  %22 = bitcast float* %denom1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #1
  %23 = bitcast float* %denom0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #1
  ret %struct.btWheelContactPoint* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #3 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !3
  %0 = load float, float* %y.addr, align 4, !tbaa !3
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #7 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store i32 %i, i32* %i.addr, align 4, !tbaa !50
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4, !tbaa !50
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4, !tbaa !50
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4, !tbaa !50
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

define hidden void @_ZN16btRaycastVehicle9debugDrawEP12btIDebugDraw(%class.btRaycastVehicle* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  %v = alloca i32, align 4
  %wheelColor = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %wheelPosWS = alloca %class.btVector3, align 4
  %axle = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = bitcast i32* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  store i32 0, i32* %v, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %v, align 4, !tbaa !50
  %call = call i32 @_ZNK16btRaycastVehicle12getNumWheelsEv(%class.btRaycastVehicle* %this1)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #1
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = bitcast %class.btVector3* %wheelColor to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #1
  %4 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #1
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  store float 1.000000e+00, float* %ref.tmp2, align 4, !tbaa !3
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  store float 1.000000e+00, float* %ref.tmp3, align 4, !tbaa !3
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %7 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  %8 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #1
  %9 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #1
  %10 = load i32, i32* %v, align 4, !tbaa !50
  %call5 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %10)
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call5, i32 0, i32 0
  %m_isInContact = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo, i32 0, i32 6
  %11 = load i8, i8* %m_isInContact, align 4, !tbaa !72, !range !44
  %tobool = trunc i8 %11 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #1
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !3
  %13 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #1
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !3
  %14 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  store float 1.000000e+00, float* %ref.tmp8, align 4, !tbaa !3
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #1
  %16 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  br label %if.end

if.else:                                          ; preds = %for.body
  %18 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #1
  store float 1.000000e+00, float* %ref.tmp9, align 4, !tbaa !3
  %19 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #1
  store float 0.000000e+00, float* %ref.tmp10, align 4, !tbaa !3
  %20 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #1
  store float 1.000000e+00, float* %ref.tmp11, align 4, !tbaa !3
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %wheelColor, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %21 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #1
  %22 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #1
  %23 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %24 = bitcast %class.btVector3* %wheelPosWS to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #1
  %25 = load i32, i32* %v, align 4, !tbaa !50
  %call12 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %25)
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call12, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_worldTransform)
  %26 = bitcast %class.btVector3* %wheelPosWS to i8*
  %27 = bitcast %class.btVector3* %call13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !29
  %28 = bitcast %class.btVector3* %axle to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %28) #1
  %29 = load i32, i32* %v, align 4, !tbaa !50
  %call14 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %29)
  %m_worldTransform15 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform15)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call16, i32 0)
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call17)
  %call19 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx = getelementptr inbounds float, float* %call18, i32 %call19
  %30 = load i32, i32* %v, align 4, !tbaa !50
  %call20 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %30)
  %m_worldTransform21 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform21)
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call22, i32 1)
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call23)
  %call25 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx26 = getelementptr inbounds float, float* %call24, i32 %call25
  %31 = load i32, i32* %v, align 4, !tbaa !50
  %call27 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %31)
  %m_worldTransform28 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call27, i32 0, i32 1
  %call29 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform28)
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %call29, i32 2)
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call30)
  %call32 = call i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this1)
  %arrayidx33 = getelementptr inbounds float, float* %call31, i32 %call32
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %axle, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx26, float* nonnull align 4 dereferenceable(4) %arrayidx33)
  %32 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !7
  %33 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %axle)
  %34 = bitcast %class.btIDebugDraw* %32 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %34, align 4, !tbaa !9
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %35 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %35(%class.btIDebugDraw* %32, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelColor)
  %36 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %36) #1
  %37 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4, !tbaa !7
  %38 = load i32, i32* %v, align 4, !tbaa !50
  %call36 = call nonnull align 4 dereferenceable(284) %struct.btWheelInfo* @_ZN16btRaycastVehicle12getWheelInfoEi(%class.btRaycastVehicle* %this1, i32 %38)
  %m_raycastInfo37 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %call36, i32 0, i32 0
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo37, i32 0, i32 1
  %39 = bitcast %class.btIDebugDraw* %37 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable38 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %39, align 4, !tbaa !9
  %vfn39 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable38, i64 2
  %40 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn39, align 4
  call void %40(%class.btIDebugDraw* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelPosWS, %class.btVector3* nonnull align 4 dereferenceable(16) %m_contactPointWS, %class.btVector3* nonnull align 4 dereferenceable(16) %wheelColor)
  %41 = bitcast %class.btVector3* %axle to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #1
  %42 = bitcast %class.btVector3* %wheelPosWS to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #1
  %43 = bitcast %class.btVector3* %wheelColor to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #1
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %44 = load i32, i32* %v, align 4, !tbaa !50
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %v, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store float* %_x, float** %_x.addr, align 4, !tbaa !7
  store float* %_y, float** %_y.addr, align 4, !tbaa !7
  store float* %_z, float** %_z.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !3
  %2 = load float*, float** %_y.addr, align 4, !tbaa !7
  %3 = load float, float* %2, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !3
  %4 = load float*, float** %_z.addr, align 4, !tbaa !7
  %5 = load float, float* %4, align 4, !tbaa !3
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !3
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !3
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK16btRaycastVehicle12getRightAxisEv(%class.btRaycastVehicle* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_indexRightAxis, align 4, !tbaa !23
  ret i32 %0
}

define hidden i8* @_ZN25btDefaultVehicleRaycaster7castRayERK9btVector3S2_RN18btVehicleRaycaster24btVehicleRaycasterResultE(%class.btDefaultVehicleRaycaster* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* nonnull align 4 dereferenceable(36) %result) unnamed_addr #0 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btDefaultVehicleRaycaster*, align 4
  %from.addr = alloca %class.btVector3*, align 4
  %to.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, align 4
  %rayCallback = alloca %"struct.btCollisionWorld::ClosestRayResultCallback", align 4
  %body = alloca %class.btRigidBody*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btDefaultVehicleRaycaster* %this, %class.btDefaultVehicleRaycaster** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %from, %class.btVector3** %from.addr, align 4, !tbaa !7
  store %class.btVector3* %to, %class.btVector3** %to.addr, align 4, !tbaa !7
  store %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %result, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4, !tbaa !7
  %this1 = load %class.btDefaultVehicleRaycaster*, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to i8*
  call void @llvm.lifetime.start.p0i8(i64 84, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %from.addr, align 4, !tbaa !7
  %2 = load %class.btVector3*, %class.btVector3** %to.addr, align 4, !tbaa !7
  %call = call %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %m_dynamicsWorld = getelementptr inbounds %class.btDefaultVehicleRaycaster, %class.btDefaultVehicleRaycaster* %this1, i32 0, i32 1
  %3 = load %class.btDynamicsWorld*, %class.btDynamicsWorld** %m_dynamicsWorld, align 4, !tbaa !95
  %4 = bitcast %class.btDynamicsWorld* %3 to %class.btCollisionWorld*
  %5 = load %class.btVector3*, %class.btVector3** %from.addr, align 4, !tbaa !7
  %6 = load %class.btVector3*, %class.btVector3** %to.addr, align 4, !tbaa !7
  %7 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %8 = bitcast %class.btCollisionWorld* %4 to void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)***
  %vtable = load void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)**, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*** %8, align 4, !tbaa !9
  %vfn = getelementptr inbounds void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)** %vtable, i64 8
  %9 = load void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)*, void (%class.btCollisionWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)** %vfn, align 4
  call void %9(%class.btCollisionWorld* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(20) %7)
  %10 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %call2 = call zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback6hasHitEv(%"struct.btCollisionWorld::RayResultCallback"* %10)
  br i1 %call2, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %11 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #1
  %12 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %12, i32 0, i32 2
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !97
  %call3 = call %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %13)
  store %class.btRigidBody* %call3, %class.btRigidBody** %body, align 4, !tbaa !7
  %14 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !7
  %tobool = icmp ne %class.btRigidBody* %14, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %15 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !7
  %16 = bitcast %class.btRigidBody* %15 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %16)
  br i1 %call4, label %if.then5, label %if.end

if.then5:                                         ; preds = %land.lhs.true
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, i32 0, i32 4
  %17 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4, !tbaa !7
  %m_hitPointInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %17, i32 0, i32 0
  %18 = bitcast %class.btVector3* %m_hitPointInWorld to i8*
  %19 = bitcast %class.btVector3* %m_hitPointWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false), !tbaa.struct !29
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback, i32 0, i32 3
  %20 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4, !tbaa !7
  %m_hitNormalInWorld = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %20, i32 0, i32 1
  %21 = bitcast %class.btVector3* %m_hitNormalInWorld to i8*
  %22 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !29
  %23 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4, !tbaa !7
  %m_hitNormalInWorld6 = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %23, i32 0, i32 1
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %m_hitNormalInWorld6)
  %24 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to %"struct.btCollisionWorld::RayResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %24, i32 0, i32 1
  %25 = load float, float* %m_closestHitFraction, align 4, !tbaa !100
  %26 = load %"struct.btVehicleRaycaster::btVehicleRaycasterResult"*, %"struct.btVehicleRaycaster::btVehicleRaycasterResult"** %result.addr, align 4, !tbaa !7
  %m_distFraction = getelementptr inbounds %"struct.btVehicleRaycaster::btVehicleRaycasterResult", %"struct.btVehicleRaycaster::btVehicleRaycasterResult"* %26, i32 0, i32 2
  store float %25, float* %m_distFraction, align 4, !tbaa !81
  %27 = load %class.btRigidBody*, %class.btRigidBody** %body, align 4, !tbaa !7
  %28 = bitcast %class.btRigidBody* %27 to i8*
  store i8* %28, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %land.lhs.true, %if.then
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then5
  %29 = bitcast %class.btRigidBody** %body to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #1
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup9 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end8

if.end8:                                          ; preds = %cleanup.cont, %entry
  store i8* null, i8** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup9

cleanup9:                                         ; preds = %if.end8, %cleanup
  %call10 = call %"struct.btCollisionWorld::ClosestRayResultCallback"* bitcast (%"struct.btCollisionWorld::RayResultCallback"* (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD2Ev to %"struct.btCollisionWorld::ClosestRayResultCallback"* (%"struct.btCollisionWorld::ClosestRayResultCallback"*)*)(%"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback) #1
  %30 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %rayCallback to i8*
  call void @llvm.lifetime.end.p0i8(i64 84, i8* %30) #1
  %31 = load i8*, i8** %retval, align 4
  ret i8* %31
}

define linkonce_odr hidden %"struct.btCollisionWorld::ClosestRayResultCallback"* @_ZN16btCollisionWorld24ClosestRayResultCallbackC2ERK9btVector3S3_(%"struct.btCollisionWorld::ClosestRayResultCallback"* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !7
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %call = call %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackC2Ev(%"struct.btCollisionWorld::RayResultCallback"* %0)
  %1 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld24ClosestRayResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !9
  %m_rayFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4, !tbaa !7
  %3 = bitcast %class.btVector3* %m_rayFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !29
  %m_rayToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4, !tbaa !7
  %6 = bitcast %class.btVector3* %m_rayToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !29
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormalWorld)
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPointWorld)
  ret %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback6hasHitEv(%"struct.btCollisionWorld::RayResultCallback"* %this) #5 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !97
  %cmp = icmp ne %class.btCollisionObject* %0, null
  ret i1 %cmp
}

define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBody6upcastEPK17btCollisionObject(%class.btCollisionObject* %colObj) #0 comdat {
entry:
  %retval = alloca %class.btRigidBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !7
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !7
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %and = and i32 %call, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4, !tbaa !7
  %2 = bitcast %class.btCollisionObject* %1 to %class.btRigidBody*
  store %class.btRigidBody* %2, %class.btRigidBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btRigidBody* null, %class.btRigidBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btRigidBody*, %class.btRigidBody** %retval, align 4
  ret %class.btRigidBody* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4, !tbaa !101
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btVehicleRaycaster* @_ZN18btVehicleRaycasterD2Ev(%struct.btVehicleRaycaster* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %struct.btVehicleRaycaster*, align 4
  store %struct.btVehicleRaycaster* %this, %struct.btVehicleRaycaster** %this.addr, align 4, !tbaa !7
  %this1 = load %struct.btVehicleRaycaster*, %struct.btVehicleRaycaster** %this.addr, align 4
  ret %struct.btVehicleRaycaster* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN25btDefaultVehicleRaycasterD0Ev(%class.btDefaultVehicleRaycaster* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btDefaultVehicleRaycaster*, align 4
  store %class.btDefaultVehicleRaycaster* %this, %class.btDefaultVehicleRaycaster** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btDefaultVehicleRaycaster*, %class.btDefaultVehicleRaycaster** %this.addr, align 4
  %call = call %class.btDefaultVehicleRaycaster* bitcast (%struct.btVehicleRaycaster* (%struct.btVehicleRaycaster*)* @_ZN18btVehicleRaycasterD2Ev to %class.btDefaultVehicleRaycaster* (%class.btDefaultVehicleRaycaster*)*)(%class.btDefaultVehicleRaycaster* %this1) #1
  %0 = bitcast %class.btDefaultVehicleRaycaster* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

define linkonce_odr hidden void @_ZN16btRaycastVehicle12updateActionEP16btCollisionWorldf(%class.btRaycastVehicle* %this, %class.btCollisionWorld* %collisionWorld, float %step) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %step.addr = alloca float, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4, !tbaa !7
  store float %step, float* %step.addr, align 4, !tbaa !3
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load float, float* %step.addr, align 4, !tbaa !3
  %1 = bitcast %class.btRaycastVehicle* %this1 to void (%class.btRaycastVehicle*, float)***
  %vtable = load void (%class.btRaycastVehicle*, float)**, void (%class.btRaycastVehicle*, float)*** %1, align 4, !tbaa !9
  %vfn = getelementptr inbounds void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vtable, i64 4
  %2 = load void (%class.btRaycastVehicle*, float)*, void (%class.btRaycastVehicle*, float)** %vfn, align 4
  call void %2(%class.btRaycastVehicle* %this1, float %0)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btRaycastVehicle19setCoordinateSystemEiii(%class.btRaycastVehicle* %this, i32 %rightIndex, i32 %upIndex, i32 %forwardIndex) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btRaycastVehicle*, align 4
  %rightIndex.addr = alloca i32, align 4
  %upIndex.addr = alloca i32, align 4
  %forwardIndex.addr = alloca i32, align 4
  store %class.btRaycastVehicle* %this, %class.btRaycastVehicle** %this.addr, align 4, !tbaa !7
  store i32 %rightIndex, i32* %rightIndex.addr, align 4, !tbaa !50
  store i32 %upIndex, i32* %upIndex.addr, align 4, !tbaa !50
  store i32 %forwardIndex, i32* %forwardIndex.addr, align 4, !tbaa !50
  %this1 = load %class.btRaycastVehicle*, %class.btRaycastVehicle** %this.addr, align 4
  %0 = load i32, i32* %rightIndex.addr, align 4, !tbaa !50
  %m_indexRightAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 14
  store i32 %0, i32* %m_indexRightAxis, align 4, !tbaa !23
  %1 = load i32, i32* %upIndex.addr, align 4, !tbaa !50
  %m_indexUpAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 15
  store i32 %1, i32* %m_indexUpAxis, align 4, !tbaa !24
  %2 = load i32, i32* %forwardIndex.addr, align 4, !tbaa !50
  %m_indexForwardAxis = getelementptr inbounds %class.btRaycastVehicle, %class.btRaycastVehicle* %this1, i32 0, i32 16
  store i32 %2, i32* %m_indexForwardAxis, align 4, !tbaa !25
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #8

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  store i32 %first, i32* %first.addr, align 4, !tbaa !50
  store i32 %last, i32* %last.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %first.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %last.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !103
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !104
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4, !tbaa !103
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !105, !range !44
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4, !tbaa !103
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4, !tbaa !103
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !105
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4, !tbaa !103
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !104
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !106
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btTypedConstraint** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !7
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: nounwind
define linkonce_odr hidden %class.btActionInterface* @_ZN17btActionInterfaceD2Ev(%class.btActionInterface* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  ret %class.btActionInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN17btActionInterfaceD0Ev(%class.btActionInterface* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btActionInterface*, align 4
  store %class.btActionInterface* %this, %class.btActionInterface** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btActionInterface*, %class.btActionInterface** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #9

; Function Attrs: inlinehint
define linkonce_odr hidden %"struct.btWheelInfo::RaycastInfo"* @_ZN11btWheelInfo11RaycastInfoC2Ev(%"struct.btWheelInfo::RaycastInfo"* returned %this) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %"struct.btWheelInfo::RaycastInfo"*, align 4
  store %"struct.btWheelInfo::RaycastInfo"* %this, %"struct.btWheelInfo::RaycastInfo"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btWheelInfo::RaycastInfo"*, %"struct.btWheelInfo::RaycastInfo"** %this.addr, align 4
  %m_contactNormalWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactNormalWS)
  %m_contactPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_contactPointWS)
  %m_hardPointWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hardPointWS)
  %m_wheelDirectionWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelDirectionWS)
  %m_wheelAxleWS = getelementptr inbounds %"struct.btWheelInfo::RaycastInfo", %"struct.btWheelInfo::RaycastInfo"* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_wheelAxleWS)
  ret %"struct.btWheelInfo::RaycastInfo"* %this1
}

define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store float* %s, float** %s.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load float*, float** %s.addr, align 4, !tbaa !7
  %2 = load float, float* %1, align 4, !tbaa !3
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !3
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #1
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store float* %s, float** %s.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !3
  %3 = load float*, float** %s.addr, align 4, !tbaa !7
  %4 = load float, float* %3, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !3
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !3
  %6 = load float*, float** %s.addr, align 4, !tbaa !7
  %7 = load float, float* %6, align 4, !tbaa !3
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !3
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !3
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4, !tbaa !7
  store float* %_angle, float** %_angle.addr, align 4, !tbaa !7
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !7
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %1)
  store float %call, float* %d, align 4, !tbaa !3
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  %3 = load float*, float** %_angle.addr, align 4, !tbaa !7
  %4 = load float, float* %3, align 4, !tbaa !3
  %mul = fmul float %4, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %5 = load float, float* %d, align 4, !tbaa !3
  %div = fdiv float %call2, %5
  store float %div, float* %s, align 4, !tbaa !3
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #1
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !7
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4, !tbaa !3
  %10 = load float, float* %s, align 4, !tbaa !3
  %mul4 = fmul float %9, %10
  store float %mul4, float* %ref.tmp, align 4, !tbaa !3
  %11 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #1
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %12)
  %13 = load float, float* %call6, align 4, !tbaa !3
  %14 = load float, float* %s, align 4, !tbaa !3
  %mul7 = fmul float %13, %14
  store float %mul7, float* %ref.tmp5, align 4, !tbaa !3
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #1
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4, !tbaa !7
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call9, align 4, !tbaa !3
  %18 = load float, float* %s, align 4, !tbaa !3
  %mul10 = fmul float %17, %18
  store float %mul10, float* %ref.tmp8, align 4, !tbaa !3
  %19 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #1
  %20 = load float*, float** %_angle.addr, align 4, !tbaa !7
  %21 = load float, float* %20, align 4, !tbaa !3
  %mul12 = fmul float %21, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4, !tbaa !3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %22 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #1
  %23 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #1
  %24 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #1
  %25 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #1
  %26 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #1
  %27 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btSinf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !3
  %0 = load float, float* %x.addr, align 4, !tbaa !3
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  store float* %_x, float** %_x.addr, align 4, !tbaa !7
  store float* %_y, float** %_y.addr, align 4, !tbaa !7
  store float* %_z, float** %_z.addr, align 4, !tbaa !7
  store float* %_w, float** %_w.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !7
  %1 = load float, float* %0, align 4, !tbaa !3
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !3
  %2 = load float*, float** %_y.addr, align 4, !tbaa !7
  %3 = load float, float* %2, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !3
  %4 = load float*, float** %_z.addr, align 4, !tbaa !7
  %5 = load float, float* %4, align 4, !tbaa !3
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !3
  %6 = load float*, float** %_w.addr, align 4, !tbaa !7
  %7 = load float, float* %6, align 4, !tbaa !3
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4, !tbaa !3
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z5btCosf(float %x) #3 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4, !tbaa !3
  %0 = load float, float* %x.addr, align 4, !tbaa !3
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #10

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #10

define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = bitcast float* %d to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %1)
  store float %call, float* %d, align 4, !tbaa !3
  %2 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  %3 = load float, float* %d, align 4, !tbaa !3
  %div = fdiv float 2.000000e+00, %3
  store float %div, float* %s, align 4, !tbaa !3
  %4 = bitcast float* %xs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #1
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4, !tbaa !3
  %8 = load float, float* %s, align 4, !tbaa !3
  %mul = fmul float %7, %8
  store float %mul, float* %xs, align 4, !tbaa !3
  %9 = bitcast float* %ys to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #1
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %11)
  %12 = load float, float* %call3, align 4, !tbaa !3
  %13 = load float, float* %s, align 4, !tbaa !3
  %mul4 = fmul float %12, %13
  store float %mul4, float* %ys, align 4, !tbaa !3
  %14 = bitcast float* %zs to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #1
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call5, align 4, !tbaa !3
  %18 = load float, float* %s, align 4, !tbaa !3
  %mul6 = fmul float %17, %18
  store float %mul6, float* %zs, align 4, !tbaa !3
  %19 = bitcast float* %wx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #1
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %21)
  %22 = load float, float* %call7, align 4, !tbaa !3
  %23 = load float, float* %xs, align 4, !tbaa !3
  %mul8 = fmul float %22, %23
  store float %mul8, float* %wx, align 4, !tbaa !3
  %24 = bitcast float* %wy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #1
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %26)
  %27 = load float, float* %call9, align 4, !tbaa !3
  %28 = load float, float* %ys, align 4, !tbaa !3
  %mul10 = fmul float %27, %28
  store float %mul10, float* %wy, align 4, !tbaa !3
  %29 = bitcast float* %wz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #1
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call11, align 4, !tbaa !3
  %33 = load float, float* %zs, align 4, !tbaa !3
  %mul12 = fmul float %32, %33
  store float %mul12, float* %wz, align 4, !tbaa !3
  %34 = bitcast float* %xx to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #1
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call13, align 4, !tbaa !3
  %38 = load float, float* %xs, align 4, !tbaa !3
  %mul14 = fmul float %37, %38
  store float %mul14, float* %xx, align 4, !tbaa !3
  %39 = bitcast float* %xy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #1
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %41)
  %42 = load float, float* %call15, align 4, !tbaa !3
  %43 = load float, float* %ys, align 4, !tbaa !3
  %mul16 = fmul float %42, %43
  store float %mul16, float* %xy, align 4, !tbaa !3
  %44 = bitcast float* %xz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #1
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call17, align 4, !tbaa !3
  %48 = load float, float* %zs, align 4, !tbaa !3
  %mul18 = fmul float %47, %48
  store float %mul18, float* %xz, align 4, !tbaa !3
  %49 = bitcast float* %yy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #1
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call19, align 4, !tbaa !3
  %53 = load float, float* %ys, align 4, !tbaa !3
  %mul20 = fmul float %52, %53
  store float %mul20, float* %yy, align 4, !tbaa !3
  %54 = bitcast float* %yz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #1
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %56)
  %57 = load float, float* %call21, align 4, !tbaa !3
  %58 = load float, float* %zs, align 4, !tbaa !3
  %mul22 = fmul float %57, %58
  store float %mul22, float* %yz, align 4, !tbaa !3
  %59 = bitcast float* %zz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #1
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %61)
  %62 = load float, float* %call23, align 4, !tbaa !3
  %63 = load float, float* %zs, align 4, !tbaa !3
  %mul24 = fmul float %62, %63
  store float %mul24, float* %zz, align 4, !tbaa !3
  %64 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #1
  %65 = load float, float* %yy, align 4, !tbaa !3
  %66 = load float, float* %zz, align 4, !tbaa !3
  %add = fadd float %65, %66
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4, !tbaa !3
  %67 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #1
  %68 = load float, float* %xy, align 4, !tbaa !3
  %69 = load float, float* %wz, align 4, !tbaa !3
  %sub26 = fsub float %68, %69
  store float %sub26, float* %ref.tmp25, align 4, !tbaa !3
  %70 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #1
  %71 = load float, float* %xz, align 4, !tbaa !3
  %72 = load float, float* %wy, align 4, !tbaa !3
  %add28 = fadd float %71, %72
  store float %add28, float* %ref.tmp27, align 4, !tbaa !3
  %73 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #1
  %74 = load float, float* %xy, align 4, !tbaa !3
  %75 = load float, float* %wz, align 4, !tbaa !3
  %add30 = fadd float %74, %75
  store float %add30, float* %ref.tmp29, align 4, !tbaa !3
  %76 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #1
  %77 = load float, float* %xx, align 4, !tbaa !3
  %78 = load float, float* %zz, align 4, !tbaa !3
  %add32 = fadd float %77, %78
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4, !tbaa !3
  %79 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #1
  %80 = load float, float* %yz, align 4, !tbaa !3
  %81 = load float, float* %wx, align 4, !tbaa !3
  %sub35 = fsub float %80, %81
  store float %sub35, float* %ref.tmp34, align 4, !tbaa !3
  %82 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #1
  %83 = load float, float* %xz, align 4, !tbaa !3
  %84 = load float, float* %wy, align 4, !tbaa !3
  %sub37 = fsub float %83, %84
  store float %sub37, float* %ref.tmp36, align 4, !tbaa !3
  %85 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #1
  %86 = load float, float* %yz, align 4, !tbaa !3
  %87 = load float, float* %wx, align 4, !tbaa !3
  %add39 = fadd float %86, %87
  store float %add39, float* %ref.tmp38, align 4, !tbaa !3
  %88 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #1
  %89 = load float, float* %xx, align 4, !tbaa !3
  %90 = load float, float* %yy, align 4, !tbaa !3
  %add41 = fadd float %89, %90
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4, !tbaa !3
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %91 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #1
  %92 = bitcast float* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #1
  %93 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #1
  %94 = bitcast float* %ref.tmp34 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #1
  %95 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #1
  %96 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #1
  %97 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #1
  %98 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #1
  %99 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #1
  %100 = bitcast float* %zz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #1
  %101 = bitcast float* %yz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #1
  %102 = bitcast float* %yy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #1
  %103 = bitcast float* %xz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %103) #1
  %104 = bitcast float* %xy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %104) #1
  %105 = bitcast float* %xx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %105) #1
  %106 = bitcast float* %wz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %106) #1
  %107 = bitcast float* %wy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %107) #1
  %108 = bitcast float* %wx to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %108) #1
  %109 = bitcast float* %zs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %109) #1
  %110 = bitcast float* %ys to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %110) #1
  %111 = bitcast float* %xs to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %111) #1
  %112 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #1
  %113 = bitcast float* %d to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #1
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #0 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store float* %xx, float** %xx.addr, align 4, !tbaa !7
  store float* %xy, float** %xy.addr, align 4, !tbaa !7
  store float* %xz, float** %xz.addr, align 4, !tbaa !7
  store float* %yx, float** %yx.addr, align 4, !tbaa !7
  store float* %yy, float** %yy.addr, align 4, !tbaa !7
  store float* %yz, float** %yz.addr, align 4, !tbaa !7
  store float* %zx, float** %zx.addr, align 4, !tbaa !7
  store float* %zy, float** %zy.addr, align 4, !tbaa !7
  store float* %zz, float** %zz.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4, !tbaa !7
  %1 = load float*, float** %xy.addr, align 4, !tbaa !7
  %2 = load float*, float** %xz.addr, align 4, !tbaa !7
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4, !tbaa !7
  %4 = load float*, float** %yy.addr, align 4, !tbaa !7
  %5 = load float*, float** %yz.addr, align 4, !tbaa !7
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4, !tbaa !7
  %7 = load float*, float** %zy.addr, align 4, !tbaa !7
  %8 = load float*, float** %zz.addr, align 4, !tbaa !7
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #0 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4, !tbaa !7
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !3
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4, !tbaa !3
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4, !tbaa !3
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4, !tbaa !3
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4, !tbaa !3
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4, !tbaa !3
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4, !tbaa !3
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4, !tbaa !7
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4, !tbaa !3
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !29
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !29
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !29
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !3
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !3
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !3
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !3
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !3
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !3
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !3
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !3
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !3
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !3
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !3
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !3
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !3
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !3
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !3
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !3
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !3
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !3
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !7
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !7
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !7
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !3
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #1
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !3
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #1
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !3
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #1
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

define linkonce_odr hidden void @_ZN11btRigidBody19applyCentralImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %impulse) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %impulse.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %impulse, %class.btVector3** %impulse.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #1
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #1
  %2 = load %class.btVector3*, %class.btVector3** %impulse.addr, align 4, !tbaa !7
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %m_inverseMass)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #1
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #1
  ret void
}

define linkonce_odr hidden void @_ZN11btRigidBody18applyTorqueImpulseERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %torque) #0 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %torque.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %torque, %class.btVector3** %torque.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #1
  %1 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #1
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %torque.addr, align 4, !tbaa !7
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularFactor)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #1
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %4) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #7 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !7
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !3
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !3
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #1
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !3
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !3
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !3
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #1
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !3
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !7
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !3
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !3
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #1
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #1
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !3
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !3
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !3
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !3
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !3
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !3
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !3
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !3
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btRigidBody25computeImpulseDenominatorERK9btVector3S2_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pos, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #7 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %pos.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %r0 = alloca %class.btVector3, align 4
  %c0 = alloca %class.btVector3, align 4
  %vec = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %pos, %class.btVector3** %pos.addr, align 4, !tbaa !7
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btVector3* %r0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #1
  %1 = load %class.btVector3*, %class.btVector3** %pos.addr, align 4, !tbaa !7
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody23getCenterOfMassPositionEv(%class.btRigidBody* %this1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %r0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  %2 = bitcast %class.btVector3* %c0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #1
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !7
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %c0, %class.btVector3* %r0, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #1
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %5) #1
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %c0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %vec, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %r0)
  %6 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #1
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %7 = load float, float* %m_inverseMass, align 4, !tbaa !84
  %8 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4, !tbaa !7
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %vec)
  %add = fadd float %7, %call3
  %9 = bitcast %class.btVector3* %vec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %9) #1
  %10 = bitcast %class.btVector3* %c0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #1
  %11 = bitcast %class.btVector3* %r0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #1
  ret float %add
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #7 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !7
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !3
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #1
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !3
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #1
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !7
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !7
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !3
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #1
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #1
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #1
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btRigidBody24getInvInertiaTensorWorldEv(%class.btRigidBody* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  ret %class.btMatrix3x3* %m_invInertiaTensorWorld
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #10

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackC2Ev(%"struct.btCollisionWorld::RayResultCallback"* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  %0 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTVN16btCollisionWorld17RayResultCallbackE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !9
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %m_closestHitFraction, align 4, !tbaa !100
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 2
  store %class.btCollisionObject* null, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !97
  %m_collisionFilterGroup = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 3
  store i16 1, i16* %m_collisionFilterGroup, align 4, !tbaa !107
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 4
  store i16 -1, i16* %m_collisionFilterMask, align 2, !tbaa !108
  %m_flags = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 5
  store i32 0, i32* %m_flags, align 4, !tbaa !109
  ret %"struct.btCollisionWorld::RayResultCallback"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld24ClosestRayResultCallbackD0Ev(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %call = call %"struct.btCollisionWorld::ClosestRayResultCallback"* bitcast (%"struct.btCollisionWorld::RayResultCallback"* (%"struct.btCollisionWorld::RayResultCallback"*)* @_ZN16btCollisionWorld17RayResultCallbackD2Ev to %"struct.btCollisionWorld::ClosestRayResultCallback"* (%"struct.btCollisionWorld::ClosestRayResultCallback"*)*)(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this1) #1
  %0 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionWorld17RayResultCallback14needsCollisionEP17btBroadphaseProxy(%"struct.btCollisionWorld::RayResultCallback"* %this, %struct.btBroadphaseProxy* %proxy0) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collides = alloca i8, align 1
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4, !tbaa !7
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %collides) #1
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !7
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %0, i32 0, i32 1
  %1 = load i16, i16* %m_collisionFilterGroup, align 4, !tbaa !110
  %conv = sext i16 %1 to i32
  %m_collisionFilterMask = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 4
  %2 = load i16, i16* %m_collisionFilterMask, align 2, !tbaa !108
  %conv2 = sext i16 %2 to i32
  %and = and i32 %conv, %conv2
  %cmp = icmp ne i32 %and, 0
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %collides, align 1, !tbaa !28
  %3 = load i8, i8* %collides, align 1, !tbaa !28, !range !44
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_collisionFilterGroup3 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %this1, i32 0, i32 3
  %4 = load i16, i16* %m_collisionFilterGroup3, align 4, !tbaa !107
  %conv4 = sext i16 %4 to i32
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !7
  %m_collisionFilterMask5 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 2
  %6 = load i16, i16* %m_collisionFilterMask5, align 2, !tbaa !112
  %conv6 = sext i16 %6 to i32
  %and7 = and i32 %conv4, %conv6
  %tobool8 = icmp ne i32 %and7, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %7 = phi i1 [ false, %entry ], [ %tobool8, %land.rhs ]
  %frombool9 = zext i1 %7 to i8
  store i8 %frombool9, i8* %collides, align 1, !tbaa !28
  %8 = load i8, i8* %collides, align 1, !tbaa !28, !range !44
  %tobool10 = trunc i8 %8 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %collides) #1
  ret i1 %tobool10
}

define linkonce_odr hidden float @_ZN16btCollisionWorld24ClosestRayResultCallback15addSingleResultERNS_14LocalRayResultEb(%"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::LocalRayResult"* nonnull align 4 dereferenceable(28) %rayResult, i1 zeroext %normalInWorldSpace) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::ClosestRayResultCallback"*, align 4
  %rayResult.addr = alloca %"struct.btCollisionWorld::LocalRayResult"*, align 4
  %normalInWorldSpace.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  store %"struct.btCollisionWorld::ClosestRayResultCallback"* %this, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4, !tbaa !7
  store %"struct.btCollisionWorld::LocalRayResult"* %rayResult, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %frombool = zext i1 %normalInWorldSpace to i8
  store i8 %frombool, i8* %normalInWorldSpace.addr, align 1, !tbaa !28
  %this1 = load %"struct.btCollisionWorld::ClosestRayResultCallback"*, %"struct.btCollisionWorld::ClosestRayResultCallback"** %this.addr, align 4
  %0 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %0, i32 0, i32 3
  %1 = load float, float* %m_hitFraction, align 4, !tbaa !113
  %2 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %2, i32 0, i32 1
  store float %1, float* %m_closestHitFraction, align 4, !tbaa !100
  %3 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %3, i32 0, i32 0
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4, !tbaa !115
  %5 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject2 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %5, i32 0, i32 2
  store %class.btCollisionObject* %4, %class.btCollisionObject** %m_collisionObject2, align 4, !tbaa !97
  %6 = load i8, i8* %normalInWorldSpace.addr, align 1, !tbaa !28, !range !44
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %7 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %7, i32 0, i32 2
  %m_hitNormalWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %8 = bitcast %class.btVector3* %m_hitNormalWorld to i8*
  %9 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !29
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %10) #1
  %11 = bitcast %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1 to %"struct.btCollisionWorld::RayResultCallback"*
  %m_collisionObject3 = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %11, i32 0, i32 2
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject3, align 4, !tbaa !97
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %12)
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  %13 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_hitNormalLocal5 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %13, i32 0, i32 2
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_hitNormalLocal5)
  %m_hitNormalWorld6 = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 3
  %14 = bitcast %class.btVector3* %m_hitNormalWorld6 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false), !tbaa.struct !29
  %16 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %16) #1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_hitPointWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 4
  %m_rayFromWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 1
  %m_rayToWorld = getelementptr inbounds %"struct.btCollisionWorld::ClosestRayResultCallback", %"struct.btCollisionWorld::ClosestRayResultCallback"* %this1, i32 0, i32 2
  %17 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_hitFraction7 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %17, i32 0, i32 3
  %18 = load float, float* %m_hitFraction7, align 4, !tbaa !113
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %m_hitPointWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld, float %18)
  %19 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %rayResult.addr, align 4, !tbaa !7
  %m_hitFraction8 = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %19, i32 0, i32 3
  %20 = load float, float* %m_hitFraction8, align 4, !tbaa !113
  ret float %20
}

; Function Attrs: nounwind
define linkonce_odr hidden %"struct.btCollisionWorld::RayResultCallback"* @_ZN16btCollisionWorld17RayResultCallbackD2Ev(%"struct.btCollisionWorld::RayResultCallback"* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  ret %"struct.btCollisionWorld::RayResultCallback"* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btCollisionWorld17RayResultCallbackD0Ev(%"struct.btCollisionWorld::RayResultCallback"* %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %this, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4, !tbaa !7
  %this1 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %this.addr, align 4
  call void @llvm.trap() #12
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !7
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !7
  store float %rt, float* %rt.addr, align 4, !tbaa !3
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load float, float* %rt.addr, align 4, !tbaa !3
  %sub = fsub float 1.000000e+00, %1
  store float %sub, float* %s, align 4, !tbaa !3
  %2 = load float, float* %s, align 4, !tbaa !3
  %3 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !7
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !3
  %mul = fmul float %2, %4
  %5 = load float, float* %rt.addr, align 4, !tbaa !3
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4, !tbaa !3
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4, !tbaa !3
  %8 = load float, float* %s, align 4, !tbaa !3
  %9 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !7
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %10 = load float, float* %arrayidx8, align 4, !tbaa !3
  %mul9 = fmul float %8, %10
  %11 = load float, float* %rt.addr, align 4, !tbaa !3
  %12 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %13 = load float, float* %arrayidx11, align 4, !tbaa !3
  %mul12 = fmul float %11, %13
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4, !tbaa !3
  %14 = load float, float* %s, align 4, !tbaa !3
  %15 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !7
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %16 = load float, float* %arrayidx17, align 4, !tbaa !3
  %mul18 = fmul float %14, %16
  %17 = load float, float* %rt.addr, align 4, !tbaa !3
  %18 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !7
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %19 = load float, float* %arrayidx20, align 4, !tbaa !3
  %mul21 = fmul float %17, %19
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4, !tbaa !3
  %20 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #1
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %0 = load i32, i32* %m_internalType, align 4, !tbaa !116
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.1* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !117
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !91
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !92
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !118
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.6* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  ret %class.btAlignedAllocator.6* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !119
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4, !tbaa !93
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !94
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !120
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.1* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %first, i32* %first.addr, align 4, !tbaa !50
  store i32 %last, i32* %last.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %first.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %last.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !91
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !92
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.1* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !91
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !117, !range !44
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !91
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.2* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !91
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.2* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4, !tbaa !7
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.5* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.5* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %first, i32* %first.addr, align 4, !tbaa !50
  store i32 %last, i32* %last.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %first.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %last.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %4 = load float*, float** %m_data, align 4, !tbaa !93
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !94
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.5* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !93
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !119, !range !44
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4, !tbaa !93
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.6* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4, !tbaa !93
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.6* %this, float* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !7
  store float* %ptr, float** %ptr.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !121
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btWheelInfo* null, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !52
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !122
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE5clearEv(%class.btAlignedObjectArray.9* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %first, i32* %first.addr, align 4, !tbaa !50
  store i32 %last, i32* %last.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %first.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %last.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.9* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %tobool = icmp ne %struct.btWheelInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !121, !range !44
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data4, align 4, !tbaa !51
  call void @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %m_allocator, %struct.btWheelInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btWheelInfo* null, %struct.btWheelInfo** %m_data5, align 4, !tbaa !51
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %this, %struct.btWheelInfo* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %struct.btWheelInfo*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfo* %ptr, %struct.btWheelInfo** %ptr.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %struct.btWheelInfo*, %struct.btWheelInfo** %ptr.addr, align 4, !tbaa !7
  %1 = bitcast %struct.btWheelInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.9* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !122
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btWheelInfoE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btWheelInfo*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btWheelInfo** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btWheelInfo*
  store %struct.btWheelInfo* %3, %struct.btWheelInfo** %s, align 4, !tbaa !7
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %s, align 4, !tbaa !7
  call void @_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %struct.btWheelInfo* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11btWheelInfoE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11btWheelInfoE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !121
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %s, align 4, !tbaa !7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btWheelInfo* %5, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !122
  %7 = bitcast %struct.btWheelInfo** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11btWheelInfoE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %size, i32* %size.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !50
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !50
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* returned %this, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %0) unnamed_addr #7 comdat {
entry:
  %this.addr = alloca %struct.btWheelInfo*, align 4
  %.addr = alloca %struct.btWheelInfo*, align 4
  store %struct.btWheelInfo* %this, %struct.btWheelInfo** %this.addr, align 4, !tbaa !7
  store %struct.btWheelInfo* %0, %struct.btWheelInfo** %.addr, align 4, !tbaa !7
  %this1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %this.addr, align 4
  %m_raycastInfo = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 0
  %1 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4, !tbaa !7
  %m_raycastInfo2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %1, i32 0, i32 0
  %2 = bitcast %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo to i8*
  %3 = bitcast %"struct.btWheelInfo::RaycastInfo"* %m_raycastInfo2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 92, i1 false), !tbaa.struct !123
  %m_worldTransform = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 1
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4, !tbaa !7
  %m_worldTransform3 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_worldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform3)
  %m_chassisConnectionPointCS = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %this1, i32 0, i32 2
  %5 = load %struct.btWheelInfo*, %struct.btWheelInfo** %.addr, align 4
  %m_chassisConnectionPointCS4 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %5, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_chassisConnectionPointCS to i8*
  %7 = bitcast %class.btVector3* %m_chassisConnectionPointCS4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %6, i8* align 4 %7, i64 128, i1 false)
  ret %struct.btWheelInfo* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11btWheelInfoE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %size, i32* %size.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !50
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !50
  %call = call %struct.btWheelInfo* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %struct.btWheelInfo** null)
  %2 = bitcast %struct.btWheelInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11btWheelInfoE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %struct.btWheelInfo* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btWheelInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4, !tbaa !7
  store i32 %start, i32* %start.addr, align 4, !tbaa !50
  store i32 %end, i32* %end.addr, align 4, !tbaa !50
  store %struct.btWheelInfo* %dest, %struct.btWheelInfo** %dest.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %start.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %end.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btWheelInfo*, %struct.btWheelInfo** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %4, i32 %5
  %6 = bitcast %struct.btWheelInfo* %arrayidx to i8*
  %7 = bitcast i8* %6 to %struct.btWheelInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %struct.btWheelInfo*, %struct.btWheelInfo** %m_data, align 4, !tbaa !51
  %9 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx2 = getelementptr inbounds %struct.btWheelInfo, %struct.btWheelInfo* %8, i32 %9
  %call = call %struct.btWheelInfo* @_ZN11btWheelInfoC2ERKS_(%struct.btWheelInfo* %7, %struct.btWheelInfo* nonnull align 4 dereferenceable(284) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #1
  ret void
}

define linkonce_odr hidden %struct.btWheelInfo* @_ZN18btAlignedAllocatorI11btWheelInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %this, i32 %n, %struct.btWheelInfo** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btWheelInfo**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  store %struct.btWheelInfo** %hint, %struct.btWheelInfo*** %hint.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !50
  %mul = mul i32 284, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btWheelInfo*
  ret %struct.btWheelInfo* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !7
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !7
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !117
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !91
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !118
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #3 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !124
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !7
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !7
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.1* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !118
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %size, i32* %size.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !50
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !50
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.2* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, %class.btVector3* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4, !tbaa !7
  store i32 %start, i32* %start.addr, align 4, !tbaa !50
  store i32 %end, i32* %end.addr, align 4, !tbaa !50
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %start.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %end.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !91
  %9 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !29
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #1
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.2* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !50
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.5* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast float** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #1
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.5* %this1, i32 %2)
  %3 = bitcast i8* %call2 to float*
  store float* %3, float** %s, align 4, !tbaa !7
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %4 = load float*, float** %s, align 4, !tbaa !7
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call3, float* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !119
  %5 = load float*, float** %s, align 4, !tbaa !7
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store float* %5, float** %m_data, align 4, !tbaa !93
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !120
  %7 = bitcast float** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.5* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !120
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.5* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %size, i32* %size.addr, align 4, !tbaa !50
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !50
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !50
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.6* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.5* %this, i32 %start, i32 %end, float* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !7
  store i32 %start, i32* %start.addr, align 4, !tbaa !50
  store i32 %end, i32* %end.addr, align 4, !tbaa !50
  store float* %dest, float** %dest.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #1
  %1 = load i32, i32* %start.addr, align 4, !tbaa !50
  store i32 %1, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !50
  %3 = load i32, i32* %end.addr, align 4, !tbaa !50
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load float*, float** %dest.addr, align 4, !tbaa !7
  %5 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = bitcast float* %arrayidx to i8*
  %7 = bitcast i8* %6 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load float*, float** %m_data, align 4, !tbaa !93
  %9 = load i32, i32* %i, align 4, !tbaa !50
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx2, align 4, !tbaa !3
  store float %10, float* %7, align 4, !tbaa !3
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !50
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !50
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #1
  ret void
}

define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.6* %this, i32 %n, float** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !7
  store i32 %n, i32* %n.addr, align 4, !tbaa !50
  store float** %hint, float*** %hint.addr, align 4, !tbaa !7
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !50
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { cold noreturn nounwind }
attributes #10 = { nounwind readnone speculatable willreturn }
attributes #11 = { builtin nounwind }
attributes #12 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
!3 = !{!4, !4, i64 0}
!4 = !{!"float", !5, i64 0}
!5 = !{!"omnipotent char", !6, i64 0}
!6 = !{!"Simple C++ TBAA"}
!7 = !{!8, !8, i64 0}
!8 = !{!"any pointer", !5, i64 0}
!9 = !{!10, !10, i64 0}
!10 = !{!"vtable pointer", !6, i64 0}
!11 = !{!12, !8, i64 100}
!12 = !{!"_ZTS16btRaycastVehicle", !13, i64 4, !13, i64 24, !17, i64 44, !17, i64 64, !15, i64 84, !15, i64 88, !4, i64 92, !4, i64 96, !8, i64 100, !4, i64 104, !4, i64 108, !4, i64 112, !8, i64 116, !15, i64 120, !15, i64 124, !15, i64 128, !19, i64 132}
!13 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !14, i64 0, !15, i64 4, !15, i64 8, !8, i64 12, !16, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!15 = !{!"int", !5, i64 0}
!16 = !{!"bool", !5, i64 0}
!17 = !{!"_ZTS20btAlignedObjectArrayIfE", !18, i64 0, !15, i64 4, !15, i64 8, !8, i64 12, !16, i64 16}
!18 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!19 = !{!"_ZTS20btAlignedObjectArrayI11btWheelInfoE", !20, i64 0, !15, i64 4, !15, i64 8, !8, i64 12, !16, i64 16}
!20 = !{!"_ZTS18btAlignedAllocatorI11btWheelInfoLj16EE"}
!21 = !{!12, !4, i64 104}
!22 = !{!12, !8, i64 116}
!23 = !{!12, !15, i64 120}
!24 = !{!12, !15, i64 124}
!25 = !{!12, !15, i64 128}
!26 = !{!12, !4, i64 112}
!27 = !{!12, !4, i64 108}
!28 = !{!16, !16, i64 0}
!29 = !{i64 0, i64 16, !30}
!30 = !{!5, !5, i64 0}
!31 = !{!32, !4, i64 48}
!32 = !{!"_ZTS27btWheelInfoConstructionInfo", !33, i64 0, !33, i64 16, !33, i64 32, !4, i64 48, !4, i64 52, !4, i64 56, !4, i64 60, !4, i64 64, !4, i64 68, !4, i64 72, !4, i64 76, !16, i64 80}
!33 = !{!"_ZTS9btVector3", !5, i64 0}
!34 = !{!32, !4, i64 56}
!35 = !{!36, !4, i64 0}
!36 = !{!"_ZTSN16btRaycastVehicle15btVehicleTuningE", !4, i64 0, !4, i64 4, !4, i64 8, !4, i64 12, !4, i64 16, !4, i64 20}
!37 = !{!32, !4, i64 60}
!38 = !{!36, !4, i64 4}
!39 = !{!32, !4, i64 64}
!40 = !{!36, !4, i64 8}
!41 = !{!32, !4, i64 68}
!42 = !{!36, !4, i64 16}
!43 = !{!32, !4, i64 72}
!44 = !{i8 0, i8 2}
!45 = !{!32, !16, i64 80}
!46 = !{!36, !4, i64 12}
!47 = !{!32, !4, i64 52}
!48 = !{!36, !4, i64 20}
!49 = !{!32, !4, i64 76}
!50 = !{!15, !15, i64 0}
!51 = !{!19, !8, i64 12}
!52 = !{!19, !15, i64 4}
!53 = !{!54, !4, i64 204}
!54 = !{!"_ZTS11btWheelInfo", !55, i64 0, !56, i64 92, !33, i64 156, !33, i64 172, !33, i64 188, !4, i64 204, !4, i64 208, !4, i64 212, !4, i64 216, !4, i64 220, !4, i64 224, !4, i64 228, !4, i64 232, !4, i64 236, !4, i64 240, !4, i64 244, !4, i64 248, !4, i64 252, !4, i64 256, !16, i64 260, !8, i64 264, !4, i64 268, !4, i64 272, !4, i64 276, !4, i64 280}
!55 = !{!"_ZTSN11btWheelInfo11RaycastInfoE", !33, i64 0, !33, i64 16, !4, i64 32, !33, i64 36, !33, i64 52, !33, i64 68, !16, i64 84, !8, i64 88}
!56 = !{!"_ZTS11btTransform", !57, i64 0, !33, i64 48}
!57 = !{!"_ZTS11btMatrix3x3", !5, i64 0}
!58 = !{!54, !4, i64 208}
!59 = !{!54, !4, i64 212}
!60 = !{!54, !4, i64 216}
!61 = !{!54, !4, i64 220}
!62 = !{!54, !4, i64 224}
!63 = !{!54, !4, i64 228}
!64 = !{!54, !4, i64 232}
!65 = !{!54, !4, i64 252}
!66 = !{!54, !4, i64 236}
!67 = !{!54, !4, i64 240}
!68 = !{!54, !4, i64 256}
!69 = !{!54, !4, i64 244}
!70 = !{!54, !16, i64 260}
!71 = !{!54, !4, i64 248}
!72 = !{!54, !16, i64 84}
!73 = !{!54, !4, i64 32}
!74 = !{!54, !4, i64 272}
!75 = !{!54, !4, i64 268}
!76 = !{!77, !8, i64 480}
!77 = !{!"_ZTS11btRigidBody", !57, i64 264, !33, i64 312, !33, i64 328, !4, i64 344, !33, i64 348, !33, i64 364, !33, i64 380, !33, i64 396, !33, i64 412, !33, i64 428, !4, i64 444, !4, i64 448, !16, i64 452, !4, i64 456, !4, i64 460, !4, i64 464, !4, i64 468, !4, i64 472, !4, i64 476, !8, i64 480, !78, i64 484, !15, i64 504, !15, i64 508, !33, i64 512, !33, i64 528, !33, i64 544, !33, i64 560, !33, i64 576, !33, i64 592, !15, i64 608, !15, i64 612}
!78 = !{!"_ZTS20btAlignedObjectArrayIP17btTypedConstraintE", !79, i64 0, !15, i64 4, !15, i64 8, !8, i64 12, !16, i64 16}
!79 = !{!"_ZTS18btAlignedAllocatorIP17btTypedConstraintLj16EE"}
!80 = !{!54, !8, i64 88}
!81 = !{!82, !4, i64 32}
!82 = !{!"_ZTSN18btVehicleRaycaster24btVehicleRaycasterResultE", !33, i64 0, !33, i64 16, !4, i64 32}
!83 = !{!54, !4, i64 276}
!84 = !{!77, !4, i64 344}
!85 = !{!86, !8, i64 0}
!86 = !{!"_ZTS19btWheelContactPoint", !8, i64 0, !8, i64 4, !33, i64 8, !33, i64 24, !4, i64 40, !4, i64 44}
!87 = !{!86, !8, i64 4}
!88 = !{!86, !4, i64 44}
!89 = !{!86, !4, i64 40}
!90 = !{!54, !4, i64 280}
!91 = !{!13, !8, i64 12}
!92 = !{!13, !15, i64 4}
!93 = !{!17, !8, i64 12}
!94 = !{!17, !15, i64 4}
!95 = !{!96, !8, i64 4}
!96 = !{!"_ZTS25btDefaultVehicleRaycaster", !8, i64 4}
!97 = !{!98, !8, i64 8}
!98 = !{!"_ZTSN16btCollisionWorld17RayResultCallbackE", !4, i64 4, !8, i64 8, !99, i64 12, !99, i64 14, !15, i64 16}
!99 = !{!"short", !5, i64 0}
!100 = !{!98, !4, i64 4}
!101 = !{!102, !15, i64 204}
!102 = !{!"_ZTS17btCollisionObject", !56, i64 4, !56, i64 68, !33, i64 132, !33, i64 148, !33, i64 164, !15, i64 180, !4, i64 184, !8, i64 188, !8, i64 192, !8, i64 196, !8, i64 200, !15, i64 204, !15, i64 208, !15, i64 212, !15, i64 216, !4, i64 220, !4, i64 224, !4, i64 228, !4, i64 232, !15, i64 236, !5, i64 240, !4, i64 244, !4, i64 248, !4, i64 252, !15, i64 256, !15, i64 260}
!103 = !{!78, !8, i64 12}
!104 = !{!78, !15, i64 4}
!105 = !{!78, !16, i64 16}
!106 = !{!78, !15, i64 8}
!107 = !{!98, !99, i64 12}
!108 = !{!98, !99, i64 14}
!109 = !{!98, !15, i64 16}
!110 = !{!111, !99, i64 4}
!111 = !{!"_ZTS17btBroadphaseProxy", !8, i64 0, !99, i64 4, !99, i64 6, !8, i64 8, !15, i64 12, !33, i64 16, !33, i64 32}
!112 = !{!111, !99, i64 6}
!113 = !{!114, !4, i64 24}
!114 = !{!"_ZTSN16btCollisionWorld14LocalRayResultE", !8, i64 0, !8, i64 4, !33, i64 8, !4, i64 24}
!115 = !{!114, !8, i64 0}
!116 = !{!102, !15, i64 236}
!117 = !{!13, !16, i64 16}
!118 = !{!13, !15, i64 8}
!119 = !{!17, !16, i64 16}
!120 = !{!17, !15, i64 8}
!121 = !{!19, !16, i64 16}
!122 = !{!19, !15, i64 8}
!123 = !{i64 0, i64 16, !30, i64 16, i64 16, !30, i64 32, i64 4, !3, i64 36, i64 16, !30, i64 52, i64 16, !30, i64 68, i64 16, !30, i64 84, i64 1, !28, i64 88, i64 4, !7}
!124 = !{!125, !125, i64 0}
!125 = !{!"long", !5, i64 0}
