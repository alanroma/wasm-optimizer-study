; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btQuantizedBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btQuantizedBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%class.btNodeOverlapCallback = type { i32 (...)** }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_ = comdat any

$_ZN16btBvhSubtreeInfoC2Ev = comdat any

$_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK18btQuantizedBvhNode10isLeafNodeEv = comdat any

$_ZNK18btQuantizedBvhNode14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i = comdat any

$_ZNK14btQuantizedBvh10unQuantizeEPKt = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev = comdat any

$_ZN14btQuantizedBvhdlEPv = comdat any

$_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3 = comdat any

$_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3 = comdat any

$_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_ = comdat any

$_ZNK14btQuantizedBvh10getAabbMinEi = comdat any

$_ZNK14btQuantizedBvh10getAabbMaxEi = comdat any

$_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_ = comdat any

$_ZNK18btQuantizedBvhNode9getPartIdEv = comdat any

$_ZNK18btQuantizedBvhNode16getTriangleIndexEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi = comdat any

$_ZN14btQuantizedBvhnwEmPv = comdat any

$_Z12btSwapEndiani = comdat any

$_Z19btSwapVector3EndianRK9btVector3RS_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii = comdat any

$_Z12btSwapEndiant = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_Z21btUnSwapVector3EndianR9btVector3 = comdat any

$_ZN9btVector316deSerializeFloatERK18btVector3FloatData = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN18btOptimizedBvhNodeC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_ = comdat any

$_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv = comdat any

$_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z8btSelectjii = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z12btSwapEndianj = comdat any

$_Z18btSwapScalarEndianRKfRf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi = comdat any

$_ZN18btQuantizedBvhNodenwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi = comdat any

$_ZN16btBvhSubtreeInfonwEmPv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi = comdat any

$_ZN18btOptimizedBvhNodenwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

@_ZTV14btQuantizedBvh = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI14btQuantizedBvh to i8*), i8* bitcast (%class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD1Ev to i8*), i8* bitcast (void (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD0Ev to i8*), i8* bitcast (i1 (%class.btQuantizedBvh*, i8*, i32, i1)* @_ZNK14btQuantizedBvh9serializeEPvjb to i8*), i8* bitcast (i32 (%class.btQuantizedBvh*)* @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv to i8*), i8* bitcast (i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData*)* @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData*)* @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData to i8*)] }, align 4
@maxIterations = hidden global i32 0, align 4
@.str = private unnamed_addr constant [23 x i8] c"btOptimizedBvhNodeData\00", align 1
@.str.1 = private unnamed_addr constant [23 x i8] c"btQuantizedBvhNodeData\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"btBvhSubtreeInfoData\00", align 1
@.str.3 = private unnamed_addr constant [24 x i8] c"btQuantizedBvhFloatData\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS14btQuantizedBvh = hidden constant [17 x i8] c"14btQuantizedBvh\00", align 1
@_ZTI14btQuantizedBvh = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btQuantizedBvh, i32 0, i32 0) }, align 4

@_ZN14btQuantizedBvhC1Ev = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*), %class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhC2Ev
@_ZN14btQuantizedBvhD1Ev = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*), %class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD2Ev
@_ZN14btQuantizedBvhC1ERS_b = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*, %class.btQuantizedBvh*, i1), %class.btQuantizedBvh* (%class.btQuantizedBvh*, %class.btQuantizedBvh*, i1)* @_ZN14btQuantizedBvhC2ERS_b

define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* returned %this) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhAabbMin)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhAabbMax)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhQuantization)
  %m_bulletVersion = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 4
  store i32 282, i32* %m_bulletVersion, align 4, !tbaa !8
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 0, i8* %m_useQuantization, align 4, !tbaa !20
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_leafNodes)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_contiguousNodes)
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call6 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 0, i32* %m_traversalMode, align 4, !tbaa !21
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call8 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 0, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %m_bvhAabbMin9 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store float 0xC7EFFFFFE0000000, float* %ref.tmp, align 4, !tbaa !23
  %2 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0xC7EFFFFFE0000000, float* %ref.tmp10, align 4, !tbaa !23
  %3 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0xC7EFFFFFE0000000, float* %ref.tmp11, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_bvhAabbMin9, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %4 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %4) #8
  %5 = bitcast float* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %m_bvhAabbMax12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %7 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  store float 0x47EFFFFFE0000000, float* %ref.tmp13, align 4, !tbaa !23
  %8 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0x47EFFFFFE0000000, float* %ref.tmp14, align 4, !tbaa !23
  %9 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0x47EFFFFFE0000000, float* %ref.tmp15, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_bvhAabbMax12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %10 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  %11 = bitcast float* %ref.tmp14 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret %class.btQuantizedBvh* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #2

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #2

define hidden void @_ZN14btQuantizedBvh13buildInternalEv(%class.btQuantizedBvh* %this) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %numLeafNodes = alloca i32, align 4
  %ref.tmp = alloca %struct.btQuantizedBvhNode, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp9 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 1, i8* %m_useQuantization, align 4, !tbaa !20
  %0 = bitcast i32* %numLeafNodes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %numLeafNodes, align 4, !tbaa !25
  %m_useQuantization2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %1 = load i8, i8* %m_useQuantization2, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  store i32 %call, i32* %numLeafNodes, align 4, !tbaa !25
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %2 = load i32, i32* %numLeafNodes, align 4, !tbaa !25
  %mul = mul nsw i32 2, %2
  %3 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %4, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %mul, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 0, i32* %m_curNodeIndex, align 4, !tbaa !27
  %6 = load i32, i32* %numLeafNodes, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 0, i32 %6)
  %m_useQuantization3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %7 = load i8, i8* %m_useQuantization3, align 4, !tbaa !20, !range !26
  %tobool4 = trunc i8 %7 to i1
  br i1 %tobool4, label %land.lhs.true, label %if.end20

land.lhs.true:                                    ; preds = %if.end
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.end20, label %if.then7

if.then7:                                         ; preds = %land.lhs.true
  %8 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %m_SubtreeHeaders8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %9 = bitcast %class.btBvhSubtreeInfo* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %9) #8
  %call10 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp9)
  %call11 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders8, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp9)
  %10 = bitcast %class.btBvhSubtreeInfo* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %10) #8
  store %class.btBvhSubtreeInfo* %call11, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %11 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_quantizedContiguousNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes12, i32 0)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %11, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call13)
  %12 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %12, i32 0, i32 2
  store i32 0, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %m_quantizedContiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call15 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes14, i32 0)
  %call16 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %call15)
  br i1 %call16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then7
  br label %cond.end

cond.false:                                       ; preds = %if.then7
  %m_quantizedContiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call18 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes17, i32 0)
  %call19 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %call18)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call19, %cond.false ]
  %13 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %13, i32 0, i32 3
  store i32 %cond, i32* %m_subtreeSize, align 4, !tbaa !30
  %14 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  br label %if.end20

if.end20:                                         ; preds = %cond.end, %land.lhs.true, %if.end
  %m_SubtreeHeaders21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call22 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders21)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call22, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %m_quantizedLeafNodes23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes23)
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %m_leafNodes)
  %15 = bitcast i32* %numLeafNodes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !31
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !25
  store %struct.btQuantizedBvhNode* %fillData, %struct.btQuantizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !25
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %2 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  store i32 %4, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %6 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !25
  store i32 %14, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !25
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data11, align 4, !tbaa !32
  %19 = load i32, i32* %i6, align 4, !tbaa !25
  %arrayidx12 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %18, i32 %19
  %20 = bitcast %struct.btQuantizedBvhNode* %arrayidx12 to i8*
  %call13 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btQuantizedBvhNode*
  %22 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btQuantizedBvhNode* %21 to i8*
  %24 = bitcast %struct.btQuantizedBvhNode* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !33
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !25
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !31
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

define hidden void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %i = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %internalNodeIndex = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %leftChildNodexIndex = alloca i32, align 4
  %rightChildNodexIndex = alloca i32, align 4
  %escapeIndex = alloca i32, align 4
  %sizeQuantizedNode = alloca i32, align 4
  %treeSizeInBytes = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %splitAxis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %5 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %numIndices, align 4, !tbaa !25
  %6 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  store i32 %7, i32* %curIndex, align 4, !tbaa !25
  %8 = load i32, i32* %numIndices, align 4, !tbaa !25
  %cmp = icmp eq i32 %8, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %9 = load i32, i32* %m_curNodeIndex2, align 4, !tbaa !27
  %10 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii(%class.btQuantizedBvh* %this1, i32 %9, i32 %10)
  %m_curNodeIndex3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %11 = load i32, i32* %m_curNodeIndex3, align 4, !tbaa !27
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %m_curNodeIndex3, align 4, !tbaa !27
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %12 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %13 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %call = call i32 @_ZN14btQuantizedBvh17calcSplittingAxisEii(%class.btQuantizedBvh* %this1, i32 %12, i32 %13)
  store i32 %call, i32* %splitAxis, align 4, !tbaa !25
  %14 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %15 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %16 = load i32, i32* %splitAxis, align 4, !tbaa !25
  %call4 = call i32 @_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii(%class.btQuantizedBvh* %this1, i32 %14, i32 %15, i32 %16)
  store i32 %call4, i32* %splitIndex, align 4, !tbaa !25
  %17 = bitcast i32* %internalNodeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %18 = load i32, i32* %m_curNodeIndex5, align 4, !tbaa !27
  store i32 %18, i32* %internalNodeIndex, align 4, !tbaa !25
  %m_curNodeIndex6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %19 = load i32, i32* %m_curNodeIndex6, align 4, !tbaa !27
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3(%class.btQuantizedBvh* %this1, i32 %19, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %m_curNodeIndex7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %20 = load i32, i32* %m_curNodeIndex7, align 4, !tbaa !27
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3(%class.btQuantizedBvh* %this1, i32 %20, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %21 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %21, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !25
  %23 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp8 = icmp slt i32 %22, %23
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_curNodeIndex9 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %24 = load i32, i32* %m_curNodeIndex9, align 4, !tbaa !27
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %26 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp, %class.btQuantizedBvh* %this1, i32 %26)
  %27 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #8
  %28 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp10, %class.btQuantizedBvh* %this1, i32 %28)
  call void @_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_(%class.btQuantizedBvh* %this1, i32 %24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %29 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %31 = load i32, i32* %i, align 4, !tbaa !25
  %inc11 = add nsw i32 %31, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_curNodeIndex12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %32 = load i32, i32* %m_curNodeIndex12, align 4, !tbaa !27
  %inc13 = add nsw i32 %32, 1
  store i32 %inc13, i32* %m_curNodeIndex12, align 4, !tbaa !27
  %33 = bitcast i32* %leftChildNodexIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #8
  %m_curNodeIndex14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %34 = load i32, i32* %m_curNodeIndex14, align 4, !tbaa !27
  store i32 %34, i32* %leftChildNodexIndex, align 4, !tbaa !25
  %35 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %36 = load i32, i32* %splitIndex, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 %35, i32 %36)
  %37 = bitcast i32* %rightChildNodexIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %m_curNodeIndex15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %38 = load i32, i32* %m_curNodeIndex15, align 4, !tbaa !27
  store i32 %38, i32* %rightChildNodexIndex, align 4, !tbaa !25
  %39 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %40 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 %39, i32 %40)
  %41 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %m_curNodeIndex16 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %42 = load i32, i32* %m_curNodeIndex16, align 4, !tbaa !27
  %43 = load i32, i32* %curIndex, align 4, !tbaa !25
  %sub17 = sub nsw i32 %42, %43
  store i32 %sub17, i32* %escapeIndex, align 4, !tbaa !25
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %44 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %44 to i1
  br i1 %tobool, label %if.then18, label %if.else

if.then18:                                        ; preds = %for.end
  %45 = bitcast i32* %sizeQuantizedNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  store i32 16, i32* %sizeQuantizedNode, align 4, !tbaa !25
  %46 = bitcast i32* %treeSizeInBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  %47 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %mul = mul nsw i32 %47, 16
  store i32 %mul, i32* %treeSizeInBytes, align 4, !tbaa !25
  %48 = load i32, i32* %treeSizeInBytes, align 4, !tbaa !25
  %cmp19 = icmp sgt i32 %48, 2048
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.then18
  %49 = load i32, i32* %leftChildNodexIndex, align 4, !tbaa !25
  %50 = load i32, i32* %rightChildNodexIndex, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh20updateSubtreeHeadersEii(%class.btQuantizedBvh* %this1, i32 %49, i32 %50)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %if.then18
  %51 = bitcast i32* %treeSizeInBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast i32* %sizeQuantizedNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %if.end22

if.else:                                          ; preds = %for.end
  br label %if.end22

if.end22:                                         ; preds = %if.else, %if.end21
  %53 = load i32, i32* %internalNodeIndex, align 4, !tbaa !25
  %54 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii(%class.btQuantizedBvh* %this1, i32 %53, i32 %54)
  %55 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #8
  %56 = bitcast i32* %rightChildNodexIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  %57 = bitcast i32* %leftChildNodexIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast i32* %internalNodeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end22, %if.then
  %59 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #8
  %60 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #8
  %61 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  %62 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  %63 = bitcast i32* %splitAxis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %63) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !35
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %this, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillValue) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %fillValue.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store %class.btBvhSubtreeInfo* %fillValue, %class.btBvhSubtreeInfo** %fillValue.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !25
  %1 = load i32, i32* %sz, align 4, !tbaa !25
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !35
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !35
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %4 = load i32, i32* %sz, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  %5 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call5 to %class.btBvhSubtreeInfo*
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillValue.addr, align 4, !tbaa !2
  %8 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  %9 = bitcast %class.btBvhSubtreeInfo* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 32, i1 false), !tbaa.struct !37
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %10 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data6, align 4, !tbaa !36
  %11 = load i32, i32* %sz, align 4, !tbaa !25
  %arrayidx7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %10, i32 %11
  %12 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  ret %class.btBvhSubtreeInfo* %arrayidx7
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  ret %class.btBvhSubtreeInfo* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %quantizedNode) #5 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %quantizedNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %quantizedNode, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %1 = load i16, i16* %arrayidx, align 4, !tbaa !38
  %m_quantizedAabbMin2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin2, i32 0, i32 0
  store i16 %1, i16* %arrayidx3, align 4, !tbaa !38
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin4 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin4, i32 0, i32 1
  %3 = load i16, i16* %arrayidx5, align 2, !tbaa !38
  %m_quantizedAabbMin6 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin6, i32 0, i32 1
  store i16 %3, i16* %arrayidx7, align 2, !tbaa !38
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin8, i32 0, i32 2
  %5 = load i16, i16* %arrayidx9, align 4, !tbaa !38
  %m_quantizedAabbMin10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 2
  store i16 %5, i16* %arrayidx11, align 4, !tbaa !38
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %6, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %7 = load i16, i16* %arrayidx12, align 2, !tbaa !38
  %m_quantizedAabbMax13 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax13, i32 0, i32 0
  store i16 %7, i16* %arrayidx14, align 2, !tbaa !38
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax15 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax15, i32 0, i32 1
  %9 = load i16, i16* %arrayidx16, align 2, !tbaa !38
  %m_quantizedAabbMax17 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax17, i32 0, i32 1
  store i16 %9, i16* %arrayidx18, align 2, !tbaa !38
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax19 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %10, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax19, i32 0, i32 2
  %11 = load i16, i16* %arrayidx20, align 2, !tbaa !38
  %m_quantizedAabbMax21 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax21, i32 0, i32 2
  store i16 %11, i16* %arrayidx22, align 2, !tbaa !38
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

define hidden void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax, float %quantizationMargin) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %quantizationMargin.addr = alloca float, align 4
  %clampValue = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %vecIn = alloca [3 x i16], align 2
  %v = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  store float %quantizationMargin, float* %quantizationMargin.addr, align 4, !tbaa !23
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btVector3* %clampValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %clampValue, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr)
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_bvhAabbMin to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !42
  %5 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #8
  %6 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %6) #8
  %7 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4, !tbaa !2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %8 = bitcast %class.btVector3* %m_bvhAabbMax to i8*
  %9 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false), !tbaa.struct !42
  %10 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %10) #8
  %11 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %m_bvhAabbMax3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %m_bvhAabbMin4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin4)
  %12 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #8
  %14 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #8
  store float 6.553300e+04, float* %ref.tmp7, align 4, !tbaa !23
  %15 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  store float 6.553300e+04, float* %ref.tmp8, align 4, !tbaa !23
  %16 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  store float 6.553300e+04, float* %ref.tmp9, align 4, !tbaa !23
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %17 = bitcast %class.btVector3* %m_bvhQuantization to i8*
  %18 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false), !tbaa.struct !42
  %19 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %21 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %22) #8
  %23 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %23) #8
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 1, i8* %m_useQuantization, align 4, !tbaa !20
  %24 = bitcast [3 x i16]* %vecIn to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %24) #8
  %25 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %25) #8
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v)
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  %m_bvhAabbMin12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin12, i32 0)
  %26 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %arraydecay14 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp13, %class.btQuantizedBvh* %this1, i16* %arraydecay14)
  %27 = bitcast %class.btVector3* %v to i8*
  %28 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false), !tbaa.struct !42
  %29 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %m_bvhAabbMin15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %30 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_bvhAabbMin15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %31 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #8
  %arraydecay17 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  %m_bvhAabbMax18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax18, i32 1)
  %32 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %32) #8
  %arraydecay20 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp19, %class.btQuantizedBvh* %this1, i16* %arraydecay20)
  %33 = bitcast %class.btVector3* %v to i8*
  %34 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !42
  %35 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #8
  %m_bvhAabbMax21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %36 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #8
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_bvhAabbMax21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %37 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #8
  %38 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #8
  %m_bvhAabbMax24 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %m_bvhAabbMin25 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin25)
  %39 = bitcast %class.btVector3* %aabbSize to i8*
  %40 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 16, i1 false), !tbaa.struct !42
  %41 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %41) #8
  %42 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %43 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #8
  store float 6.553300e+04, float* %ref.tmp28, align 4, !tbaa !23
  %45 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #8
  store float 6.553300e+04, float* %ref.tmp29, align 4, !tbaa !23
  %46 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  store float 6.553300e+04, float* %ref.tmp30, align 4, !tbaa !23
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_bvhQuantization32 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %47 = bitcast %class.btVector3* %m_bvhQuantization32 to i8*
  %48 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 16, i1 false), !tbaa.struct !42
  %49 = bitcast float* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #8
  %50 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #8
  %52 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #8
  %53 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %53) #8
  %54 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %54) #8
  %55 = bitcast [3 x i16]* %vecIn to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %55) #8
  %56 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btVector3* %clampValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !23
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !23
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %div = fdiv float %2, %4
  store float %div, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %div8 = fdiv float %7, %9
  store float %div8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %div14 = fdiv float %12, %14
  store float %div14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #3 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i16* %out, i16** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #8
  %4 = load i32, i32* %isMax.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %5 = load float, float* %call, align 4, !tbaa !23
  %add = fadd float %5, 1.000000e+00
  %conv = fptoui float %add to i16
  %conv2 = zext i16 %conv to i32
  %or = or i32 %conv2, 1
  %conv3 = trunc i32 %or to i16
  %6 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 0
  store i16 %conv3, i16* %arrayidx, align 2, !tbaa !38
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %7 = load float, float* %call4, align 4, !tbaa !23
  %add5 = fadd float %7, 1.000000e+00
  %conv6 = fptoui float %add5 to i16
  %conv7 = zext i16 %conv6 to i32
  %or8 = or i32 %conv7, 1
  %conv9 = trunc i32 %or8 to i16
  %8 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 1
  store i16 %conv9, i16* %arrayidx10, align 2, !tbaa !38
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %9 = load float, float* %call11, align 4, !tbaa !23
  %add12 = fadd float %9, 1.000000e+00
  %conv13 = fptoui float %add12 to i16
  %conv14 = zext i16 %conv13 to i32
  %or15 = or i32 %conv14, 1
  %conv16 = trunc i32 %or15 to i16
  %10 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %10, i32 2
  store i16 %conv16, i16* %arrayidx17, align 2, !tbaa !38
  br label %if.end

if.else:                                          ; preds = %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %11 = load float, float* %call18, align 4, !tbaa !23
  %conv19 = fptoui float %11 to i16
  %conv20 = zext i16 %conv19 to i32
  %and = and i32 %conv20, 65534
  %conv21 = trunc i32 %and to i16
  %12 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx22 = getelementptr inbounds i16, i16* %12, i32 0
  store i16 %conv21, i16* %arrayidx22, align 2, !tbaa !38
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %13 = load float, float* %call23, align 4, !tbaa !23
  %conv24 = fptoui float %13 to i16
  %conv25 = zext i16 %conv24 to i32
  %and26 = and i32 %conv25, 65534
  %conv27 = trunc i32 %and26 to i16
  %14 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx28 = getelementptr inbounds i16, i16* %14, i32 1
  store i16 %conv27, i16* %arrayidx28, align 2, !tbaa !38
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %15 = load float, float* %call29, align 4, !tbaa !23
  %conv30 = fptoui float %15 to i16
  %conv31 = zext i16 %conv30 to i32
  %and32 = and i32 %conv31, 65534
  %conv33 = trunc i32 %and32 to i16
  %16 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i16, i16* %16, i32 2
  store i16 %conv33, i16* %arrayidx34, align 2, !tbaa !38
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i16* %vecIn) #3 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %vecIn.addr = alloca i16*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i16* %vecIn, i16** %vecIn.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i16*, i16** %vecIn.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 0
  %2 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %conv = uitofp i16 %2 to float
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_bvhQuantization)
  %3 = load float, float* %call2, align 4, !tbaa !23
  %div = fdiv float %conv, %3
  store float %div, float* %ref.tmp, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i16*, i16** %vecIn.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %5, i32 1
  %6 = load i16, i16* %arrayidx4, align 2, !tbaa !38
  %conv5 = uitofp i16 %6 to float
  %m_bvhQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_bvhQuantization6)
  %7 = load float, float* %call7, align 4, !tbaa !23
  %div8 = fdiv float %conv5, %7
  store float %div8, float* %ref.tmp3, align 4, !tbaa !23
  %8 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  %9 = load i16*, i16** %vecIn.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %9, i32 2
  %10 = load i16, i16* %arrayidx10, align 2, !tbaa !38
  %conv11 = uitofp i16 %10 to float
  %m_bvhQuantization12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_bvhQuantization12)
  %11 = load float, float* %call13, align 4, !tbaa !23
  %div14 = fdiv float %conv11, %11
  store float %div14, float* %ref.tmp9, align 4, !tbaa !23
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %12 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: nounwind
define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* returned %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders) #8
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes) #8
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes) #8
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* %m_contiguousNodes) #8
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* %m_leafNodes) #8
  ret %class.btQuantizedBvh* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN14btQuantizedBvhD0Ev(%class.btQuantizedBvh* %this) unnamed_addr #5 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhD1Ev(%class.btQuantizedBvh* %this1) #8
  %0 = bitcast %class.btQuantizedBvh* %this1 to i8*
  call void @_ZN14btQuantizedBvhdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN14btQuantizedBvhdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define hidden void @_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii(%class.btQuantizedBvh* %this, i32 %internalNode, i32 %leafNodeIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %internalNode.addr = alloca i32, align 4
  %leafNodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %internalNode, i32* %internalNode.addr, align 4, !tbaa !25
  store i32 %leafNodeIndex, i32* %leafNodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %leafNodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %2 = load i32, i32* %internalNode.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %2)
  %3 = bitcast %struct.btQuantizedBvhNode* %call2 to i8*
  %4 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !33
  br label %if.end

if.else:                                          ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %5 = load i32, i32* %leafNodeIndex.addr, align 4, !tbaa !25
  %call3 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %5)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %6 = load i32, i32* %internalNode.addr, align 4, !tbaa !25
  %call4 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %6)
  %7 = bitcast %struct.btOptimizedBvhNode* %call4 to i8*
  %8 = bitcast %struct.btOptimizedBvhNode* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 64, i1 false), !tbaa.struct !43
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define hidden i32 @_ZN14btQuantizedBvh17calcSplittingAxisEii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %variance = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numIndices = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %center18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %diff2 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %2 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !23
  %3 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !23
  %4 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  %7 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  %8 = bitcast %class.btVector3* %variance to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %8) #8
  %9 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp5, align 4, !tbaa !23
  %11 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !23
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %12 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #8
  %15 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %17 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 %16, %17
  store i32 %sub, i32* %numIndices, align 4, !tbaa !25
  %18 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %18, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %19 = load i32, i32* %i, align 4, !tbaa !25
  %20 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %19, %20
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %21 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #8
  %22 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #8
  store float 5.000000e-01, float* %ref.tmp8, align 4, !tbaa !23
  %23 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %23) #8
  %24 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #8
  %25 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp10, %class.btQuantizedBvh* %this1, i32 %25)
  %26 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %27 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp11, %class.btQuantizedBvh* %this1, i32 %27)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %28 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  %29 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %29) #8
  %30 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %30) #8
  %31 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #8
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %32 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %34 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load i32, i32* %numIndices, align 4, !tbaa !25
  %conv = sitofp i32 %35 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp13, align 4, !tbaa !23
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %36 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %37, i32* %i, align 4, !tbaa !25
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc25, %for.end
  %38 = load i32, i32* %i, align 4, !tbaa !25
  %39 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp16 = icmp slt i32 %38, %39
  br i1 %cmp16, label %for.body17, label %for.end27

for.body17:                                       ; preds = %for.cond15
  %40 = bitcast %class.btVector3* %center18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %41 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  store float 5.000000e-01, float* %ref.tmp19, align 4, !tbaa !23
  %42 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #8
  %43 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp21, %class.btQuantizedBvh* %this1, i32 %44)
  %45 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %45) #8
  %46 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp22, %class.btQuantizedBvh* %this1, i32 %46)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %47 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %48) #8
  %49 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %49) #8
  %50 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #8
  %51 = bitcast %class.btVector3* %diff2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %51) #8
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %center18, %class.btVector3* nonnull align 4 dereferenceable(16) %means)
  %52 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %53 = bitcast %class.btVector3* %diff2 to i8*
  %54 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false), !tbaa.struct !42
  %55 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %variance, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %56 = bitcast %class.btVector3* %diff2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btVector3* %center18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %57) #8
  br label %for.inc25

for.inc25:                                        ; preds = %for.body17
  %58 = load i32, i32* %i, align 4, !tbaa !25
  %inc26 = add nsw i32 %58, 1
  store i32 %inc26, i32* %i, align 4, !tbaa !25
  br label %for.cond15

for.end27:                                        ; preds = %for.cond15
  %59 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #8
  %60 = load i32, i32* %numIndices, align 4, !tbaa !25
  %conv29 = sitofp i32 %60 to float
  %sub30 = fsub float %conv29, 1.000000e+00
  %div31 = fdiv float 1.000000e+00, %sub30
  store float %div31, float* %ref.tmp28, align 4, !tbaa !23
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %61 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %61) #8
  %call33 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %variance)
  %62 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %62) #8
  %63 = bitcast %class.btVector3* %variance to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %63) #8
  %64 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %64) #8
  %65 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %65) #8
  ret i32 %call33
}

define hidden i32 @_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex, i32 %splitAxis) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %splitValue = alloca float, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %center15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %rangeBalancedIndices = alloca i32, align 4
  %unbalanced = alloca i8, align 1
  %unbal = alloca i8, align 1
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %startIndex, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %endIndex, i32* %endIndex.addr, align 4, !tbaa !25
  store i32 %splitAxis, i32* %splitAxis.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %2, i32* %splitIndex, align 4, !tbaa !25
  %3 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %5 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %numIndices, align 4, !tbaa !25
  %6 = bitcast float* %splitValue to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %7 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !23
  %9 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %11 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %14, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %15 = load i32, i32* %i, align 4, !tbaa !25
  %16 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %15, %16
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %17 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %17) #8
  %18 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store float 5.000000e-01, float* %ref.tmp4, align 4, !tbaa !23
  %19 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #8
  %21 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp6, %class.btQuantizedBvh* %this1, i32 %21)
  %22 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %23 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp7, %class.btQuantizedBvh* %this1, i32 %23)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %24 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %24) #8
  %25 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %26) #8
  %27 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %28 = bitcast %class.btVector3* %center to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %28) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %30 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #8
  %31 = load i32, i32* %numIndices, align 4, !tbaa !25
  %conv = sitofp i32 %31 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp9, align 4, !tbaa !23
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %32 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %32) #8
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %means)
  %33 = load i32, i32* %splitAxis.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds float, float* %call11, i32 %33
  %34 = load float, float* %arrayidx, align 4, !tbaa !23
  store float %34, float* %splitValue, align 4, !tbaa !23
  %35 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  store i32 %35, i32* %i, align 4, !tbaa !25
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc24, %for.end
  %36 = load i32, i32* %i, align 4, !tbaa !25
  %37 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp13 = icmp slt i32 %36, %37
  br i1 %cmp13, label %for.body14, label %for.end26

for.body14:                                       ; preds = %for.cond12
  %38 = bitcast %class.btVector3* %center15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #8
  %39 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  store float 5.000000e-01, float* %ref.tmp16, align 4, !tbaa !23
  %40 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %40) #8
  %41 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %41) #8
  %42 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp18, %class.btQuantizedBvh* %this1, i32 %42)
  %43 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = load i32, i32* %i, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp19, %class.btQuantizedBvh* %this1, i32 %44)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17)
  %45 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %46) #8
  %47 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #8
  %48 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #8
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center15)
  %49 = load i32, i32* %splitAxis.addr, align 4, !tbaa !25
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %49
  %50 = load float, float* %arrayidx21, align 4, !tbaa !23
  %51 = load float, float* %splitValue, align 4, !tbaa !23
  %cmp22 = fcmp ogt float %50, %51
  br i1 %cmp22, label %if.then, label %if.end

if.then:                                          ; preds = %for.body14
  %52 = load i32, i32* %i, align 4, !tbaa !25
  %53 = load i32, i32* %splitIndex, align 4, !tbaa !25
  call void @_ZN14btQuantizedBvh13swapLeafNodesEii(%class.btQuantizedBvh* %this1, i32 %52, i32 %53)
  %54 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %inc23 = add nsw i32 %54, 1
  store i32 %inc23, i32* %splitIndex, align 4, !tbaa !25
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body14
  %55 = bitcast %class.btVector3* %center15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %55) #8
  br label %for.inc24

for.inc24:                                        ; preds = %if.end
  %56 = load i32, i32* %i, align 4, !tbaa !25
  %inc25 = add nsw i32 %56, 1
  store i32 %inc25, i32* %i, align 4, !tbaa !25
  br label %for.cond12

for.end26:                                        ; preds = %for.cond12
  %57 = bitcast i32* %rangeBalancedIndices to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #8
  %58 = load i32, i32* %numIndices, align 4, !tbaa !25
  %div27 = sdiv i32 %58, 3
  store i32 %div27, i32* %rangeBalancedIndices, align 4, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %unbalanced) #8
  %59 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %60 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %61 = load i32, i32* %rangeBalancedIndices, align 4, !tbaa !25
  %add = add nsw i32 %60, %61
  %cmp28 = icmp sle i32 %59, %add
  br i1 %cmp28, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.end26
  %62 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %63 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %sub29 = sub nsw i32 %63, 1
  %64 = load i32, i32* %rangeBalancedIndices, align 4, !tbaa !25
  %sub30 = sub nsw i32 %sub29, %64
  %cmp31 = icmp sge i32 %62, %sub30
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.end26
  %65 = phi i1 [ true, %for.end26 ], [ %cmp31, %lor.rhs ]
  %frombool = zext i1 %65 to i8
  store i8 %frombool, i8* %unbalanced, align 1, !tbaa !44
  %66 = load i8, i8* %unbalanced, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %66 to i1
  br i1 %tobool, label %if.then32, label %if.end34

if.then32:                                        ; preds = %lor.end
  %67 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %68 = load i32, i32* %numIndices, align 4, !tbaa !25
  %shr = ashr i32 %68, 1
  %add33 = add nsw i32 %67, %shr
  store i32 %add33, i32* %splitIndex, align 4, !tbaa !25
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %lor.end
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %unbal) #8
  %69 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %70 = load i32, i32* %startIndex.addr, align 4, !tbaa !25
  %cmp35 = icmp eq i32 %69, %70
  br i1 %cmp35, label %lor.end38, label %lor.rhs36

lor.rhs36:                                        ; preds = %if.end34
  %71 = load i32, i32* %splitIndex, align 4, !tbaa !25
  %72 = load i32, i32* %endIndex.addr, align 4, !tbaa !25
  %cmp37 = icmp eq i32 %71, %72
  br label %lor.end38

lor.end38:                                        ; preds = %lor.rhs36, %if.end34
  %73 = phi i1 [ true, %if.end34 ], [ %cmp37, %lor.rhs36 ]
  %frombool39 = zext i1 %73 to i8
  store i8 %frombool39, i8* %unbal, align 1, !tbaa !44
  %74 = load i32, i32* %splitIndex, align 4, !tbaa !25
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %unbal) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %unbalanced) #8
  %75 = bitcast i32* %rangeBalancedIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %75) #8
  %76 = bitcast %class.btVector3* %means to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %76) #8
  %77 = bitcast float* %splitValue to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  %78 = bitcast i32* %numIndices to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = bitcast i32* %splitIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #8
  %80 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #8
  ret i32 %74
}

define linkonce_odr hidden void @_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %1)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 0)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 0
  %5 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !42
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %1)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false), !tbaa.struct !42
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %newAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %newAabbMax) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %newAabbMin.addr = alloca %class.btVector3*, align 4
  %newAabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedAabbMin = alloca [3 x i16], align 2
  %quantizedAabbMax = alloca [3 x i16], align 2
  %i = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  store %class.btVector3* %newAabbMin, %class.btVector3** %newAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %newAabbMax, %class.btVector3** %newAabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast [3 x i16]* %quantizedAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %1) #8
  %2 = bitcast [3 x i16]* %quantizedAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %2) #8
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %newAabbMin.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %newAabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i32 1)
  %5 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %6, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %8 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %8)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %conv = zext i16 %10 to i32
  %11 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 %11
  %12 = load i16, i16* %arrayidx3, align 2, !tbaa !38
  %conv4 = zext i16 %12 to i32
  %cmp5 = icmp sgt i32 %conv, %conv4
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %for.body
  %13 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 %13
  %14 = load i16, i16* %arrayidx7, align 2, !tbaa !38
  %m_quantizedContiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %15 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes8, i32 %15)
  %m_quantizedAabbMin10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call9, i32 0, i32 0
  %16 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 %16
  store i16 %14, i16* %arrayidx11, align 2, !tbaa !38
  br label %if.end

if.end:                                           ; preds = %if.then6, %for.body
  %m_quantizedContiguousNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %17 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes12, i32 %17)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call13, i32 0, i32 1
  %18 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 %18
  %19 = load i16, i16* %arrayidx14, align 2, !tbaa !38
  %conv15 = zext i16 %19 to i32
  %20 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 %20
  %21 = load i16, i16* %arrayidx16, align 2, !tbaa !38
  %conv17 = zext i16 %21 to i32
  %cmp18 = icmp slt i32 %conv15, %conv17
  br i1 %cmp18, label %if.then19, label %if.end25

if.then19:                                        ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 %22
  %23 = load i16, i16* %arrayidx20, align 2, !tbaa !38
  %m_quantizedContiguousNodes21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %24 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes21, i32 %24)
  %m_quantizedAabbMax23 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call22, i32 0, i32 1
  %25 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx24 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax23, i32 0, i32 %25
  store i16 %23, i16* %arrayidx24, align 2, !tbaa !38
  br label %if.end25

if.end25:                                         ; preds = %if.then19, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %26 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = bitcast [3 x i16]* %quantizedAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %27) #8
  %28 = bitcast [3 x i16]* %quantizedAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %28) #8
  br label %if.end29

if.else:                                          ; preds = %entry
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %29 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call26 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %29)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call26, i32 0, i32 0
  %30 = load %class.btVector3*, %class.btVector3** %newAabbMin.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %30)
  %m_contiguousNodes27 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %31 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call28 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes27, i32 %31)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call28, i32 0, i32 1
  %32 = load %class.btVector3*, %class.btVector3** %newAabbMax.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMaxOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %32)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %for.end
  ret void
}

define linkonce_odr hidden void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i32 %nodeIndex) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %agg.result, %class.btQuantizedBvh* %this1, i16* %arrayidx)
  br label %return

if.end:                                           ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %2 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %2)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 0
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !42
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

define linkonce_odr hidden void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i32 %nodeIndex) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %agg.result, %class.btQuantizedBvh* %this1, i16* %arrayidx)
  br label %return

if.end:                                           ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %2 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %2)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 1
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !42
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

define hidden void @_ZN14btQuantizedBvh20updateSubtreeHeadersEii(%class.btQuantizedBvh* %this, i32 %leftChildNodexIndex, i32 %rightChildNodexIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %leftChildNodexIndex.addr = alloca i32, align 4
  %rightChildNodexIndex.addr = alloca i32, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %leftSubTreeSize = alloca i32, align 4
  %leftSubTreeSizeInBytes = alloca i32, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightSubTreeSize = alloca i32, align 4
  %rightSubTreeSizeInBytes = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp = alloca %class.btBvhSubtreeInfo, align 4
  %subtree17 = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp19 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %leftChildNodexIndex, i32* %leftChildNodexIndex.addr, align 4, !tbaa !25
  store i32 %rightChildNodexIndex, i32* %rightChildNodexIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %leftChildNodexIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %1)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %2 = bitcast i32* %leftSubTreeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call2 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %3)
  br i1 %call2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call3 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %4)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call3, %cond.false ]
  store i32 %cond, i32* %leftSubTreeSize, align 4, !tbaa !25
  %5 = bitcast i32* %leftSubTreeSizeInBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load i32, i32* %leftSubTreeSize, align 4, !tbaa !25
  %mul = mul nsw i32 %6, 16
  store i32 %mul, i32* %leftSubTreeSizeInBytes, align 4, !tbaa !25
  %7 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %m_quantizedContiguousNodes4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %8 = load i32, i32* %rightChildNodexIndex.addr, align 4, !tbaa !25
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes4, i32 %8)
  store %struct.btQuantizedBvhNode* %call5, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %9 = bitcast i32* %rightSubTreeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %call6 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %10)
  br i1 %call6, label %cond.true7, label %cond.false8

cond.true7:                                       ; preds = %cond.end
  br label %cond.end10

cond.false8:                                      ; preds = %cond.end
  %11 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %call9 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %11)
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false8, %cond.true7
  %cond11 = phi i32 [ 1, %cond.true7 ], [ %call9, %cond.false8 ]
  store i32 %cond11, i32* %rightSubTreeSize, align 4, !tbaa !25
  %12 = bitcast i32* %rightSubTreeSizeInBytes to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  %13 = load i32, i32* %rightSubTreeSize, align 4, !tbaa !25
  %mul12 = mul nsw i32 %13, 16
  store i32 %mul12, i32* %rightSubTreeSizeInBytes, align 4, !tbaa !25
  %14 = load i32, i32* %leftSubTreeSizeInBytes, align 4, !tbaa !25
  %cmp = icmp sle i32 %14, 2048
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end10
  %15 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %16 = bitcast %class.btBvhSubtreeInfo* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %16) #8
  %call13 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp)
  %call14 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp)
  %17 = bitcast %class.btBvhSubtreeInfo* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %17) #8
  store %class.btBvhSubtreeInfo* %call14, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %18 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %19 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %18, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %19)
  %20 = load i32, i32* %leftChildNodexIndex.addr, align 4, !tbaa !25
  %21 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %21, i32 0, i32 2
  store i32 %20, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %22 = load i32, i32* %leftSubTreeSize, align 4, !tbaa !25
  %23 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %23, i32 0, i32 3
  store i32 %22, i32* %m_subtreeSize, align 4, !tbaa !30
  %24 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end10
  %25 = load i32, i32* %rightSubTreeSizeInBytes, align 4, !tbaa !25
  %cmp15 = icmp sle i32 %25, 2048
  br i1 %cmp15, label %if.then16, label %if.end24

if.then16:                                        ; preds = %if.end
  %26 = bitcast %class.btBvhSubtreeInfo** %subtree17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #8
  %m_SubtreeHeaders18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %27 = bitcast %class.btBvhSubtreeInfo* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %27) #8
  %call20 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp19)
  %call21 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders18, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp19)
  %28 = bitcast %class.btBvhSubtreeInfo* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %28) #8
  store %class.btBvhSubtreeInfo* %call21, %class.btBvhSubtreeInfo** %subtree17, align 4, !tbaa !2
  %29 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4, !tbaa !2
  %30 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %29, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %30)
  %31 = load i32, i32* %rightChildNodexIndex.addr, align 4, !tbaa !25
  %32 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4, !tbaa !2
  %m_rootNodeIndex22 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %32, i32 0, i32 2
  store i32 %31, i32* %m_rootNodeIndex22, align 4, !tbaa !28
  %33 = load i32, i32* %rightSubTreeSize, align 4, !tbaa !25
  %34 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4, !tbaa !2
  %m_subtreeSize23 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %34, i32 0, i32 3
  store i32 %33, i32* %m_subtreeSize23, align 4, !tbaa !30
  %35 = bitcast %class.btBvhSubtreeInfo** %subtree17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #8
  br label %if.end24

if.end24:                                         ; preds = %if.then16, %if.end
  %m_SubtreeHeaders25 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders25)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call26, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %36 = bitcast i32* %rightSubTreeSizeInBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  %37 = bitcast i32* %rightSubTreeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %leftSubTreeSizeInBytes to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast i32* %leftSubTreeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  ret void
}

define linkonce_odr hidden void @_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii(%class.btQuantizedBvh* %this, i32 %nodeIndex, i32 %escapeIndex) #0 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %escapeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4, !tbaa !25
  store i32 %escapeIndex, i32* %escapeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %escapeIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 0, %1
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %2 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %2)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 2
  store i32 %sub, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %escapeIndex.addr, align 4, !tbaa !25
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4, !tbaa !25
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 2
  store i32 %3, i32* %m_escapeIndex, align 4, !tbaa !45
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #3 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !23
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !23
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4, !tbaa !23
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !23
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !23
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !23
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !23
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !23
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !23
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !23
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

define hidden void @_ZN14btQuantizedBvh13swapLeafNodesEii(%class.btQuantizedBvh* %this, i32 %i, i32 %splitIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %i.addr = alloca i32, align 4
  %splitIndex.addr = alloca i32, align 4
  %tmp = alloca %struct.btQuantizedBvhNode, align 4
  %tmp8 = alloca %struct.btOptimizedBvhNode, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !25
  store i32 %splitIndex, i32* %splitIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #8
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %2 = load i32, i32* %i.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %2)
  %3 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  %4 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !33
  %m_quantizedLeafNodes2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %5 = load i32, i32* %splitIndex.addr, align 4, !tbaa !25
  %call3 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes2, i32 %5)
  %m_quantizedLeafNodes4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %6 = load i32, i32* %i.addr, align 4, !tbaa !25
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes4, i32 %6)
  %7 = bitcast %struct.btQuantizedBvhNode* %call5 to i8*
  %8 = bitcast %struct.btQuantizedBvhNode* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !33
  %m_quantizedLeafNodes6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %9 = load i32, i32* %splitIndex.addr, align 4, !tbaa !25
  %call7 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes6, i32 %9)
  %10 = bitcast %struct.btQuantizedBvhNode* %call7 to i8*
  %11 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !33
  %12 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %12) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %13 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %13) #8
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %14 = load i32, i32* %i.addr, align 4, !tbaa !25
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %14)
  %15 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  %16 = bitcast %struct.btOptimizedBvhNode* %call9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 64, i1 false), !tbaa.struct !43
  %m_leafNodes10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %17 = load i32, i32* %splitIndex.addr, align 4, !tbaa !25
  %call11 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes10, i32 %17)
  %m_leafNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %18 = load i32, i32* %i.addr, align 4, !tbaa !25
  %call13 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes12, i32 %18)
  %19 = bitcast %struct.btOptimizedBvhNode* %call13 to i8*
  %20 = bitcast %struct.btOptimizedBvhNode* %call11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 64, i1 false), !tbaa.struct !43
  %m_leafNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %21 = load i32, i32* %splitIndex.addr, align 4, !tbaa !25
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes14, i32 %21)
  %22 = bitcast %struct.btOptimizedBvhNode* %call15 to i8*
  %23 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 64, i1 false), !tbaa.struct !43
  %24 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %24) #8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #3 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !23
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !23
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !23
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !23
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4, !tbaa !23
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4, !tbaa !23
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4, !tbaa !23
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4, !tbaa !23
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

define hidden void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %1) #8
  %2 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %2) #8
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i32 1)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %5 = load i32, i32* %m_traversalMode, align 4, !tbaa !21
  switch i32 %5, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb5
    i32 2, label %sw.bb8
  ]

sw.bb:                                            ; preds = %if.then
  %6 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %arraydecay3 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay4 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  call void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %6, i16* %arraydecay3, i16* %arraydecay4, i32 0, i32 %7)
  br label %sw.epilog

sw.bb5:                                           ; preds = %if.then
  %8 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %arraydecay6 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %8, i16* %arraydecay6, i16* %arraydecay7)
  br label %sw.epilog

sw.bb8:                                           ; preds = %if.then
  %9 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 0)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %11 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %arraydecay9 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %10, %class.btNodeOverlapCallback* %11, i16* %arraydecay9, i16* %arraydecay10)
  %12 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb8, %sw.bb5, %sw.bb
  %13 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %13) #8
  %14 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %14) #8
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, i32 %isMax) #3 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %clampedPoint = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i16* %out, i16** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btVector3* %clampedPoint to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4, !tbaa !2
  %2 = bitcast %class.btVector3* %clampedPoint to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !42
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %4 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %5 = load i32, i32* %isMax.addr, align 4, !tbaa !25
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedPoint, i32 %5)
  %6 = bitcast %class.btVector3* %clampedPoint to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %6) #8
  ret void
}

define hidden void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %subTreeSize = alloca i32, align 4
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4, !tbaa !25
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  store i32 %1, i32* %curIndex, align 4, !tbaa !25
  %2 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %walkIterations, align 4, !tbaa !25
  %3 = bitcast i32* %subTreeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %5 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %subTreeSize, align 4, !tbaa !25
  %6 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %7 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %7)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %8 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isLeafNode) #8
  %9 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  br label %while.cond

while.cond:                                       ; preds = %if.end13, %entry
  %10 = load i32, i32* %curIndex, align 4, !tbaa !25
  %11 = load i32, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %10, %11
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %walkIterations, align 4, !tbaa !25
  %13 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %14 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %15 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %15, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %16 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %16, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call3 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %13, i16* %14, i16* %arraydecay, i16* %arraydecay2)
  store i32 %call3, i32* %aabbOverlap, align 4, !tbaa !25
  %17 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call4 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %17)
  %frombool = zext i1 %call4 to i8
  store i8 %frombool, i8* %isLeafNode, align 1, !tbaa !44
  %18 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %18 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %19 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %tobool5 = icmp ne i32 %19, 0
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %20 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %21 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call6 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %21)
  %22 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call7 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %22)
  %23 = bitcast %class.btNodeOverlapCallback* %20 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %23, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %24 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %24(%class.btNodeOverlapCallback* %20, i32 %call6, i32 %call7)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %25 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %cmp8 = icmp ne i32 %25, 0
  br i1 %cmp8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %26 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool9 = trunc i8 %26 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %27 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %27, i32 1
  store %struct.btQuantizedBvhNode* %incdec.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %28 = load i32, i32* %curIndex, align 4, !tbaa !25
  %inc11 = add nsw i32 %28, 1
  store i32 %inc11, i32* %curIndex, align 4, !tbaa !25
  br label %if.end13

if.else:                                          ; preds = %lor.lhs.false
  %29 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call12 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %29)
  store i32 %call12, i32* %escapeIndex, align 4, !tbaa !25
  %30 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %31 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %31, i32 %30
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %32 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %33 = load i32, i32* %curIndex, align 4, !tbaa !25
  %add = add nsw i32 %33, %32
  store i32 %add, i32* %curIndex, align 4, !tbaa !25
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %34 = load i32, i32* @maxIterations, align 4, !tbaa !25
  %35 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %cmp14 = icmp slt i32 %34, %35
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  %36 = load i32, i32* %walkIterations, align 4, !tbaa !25
  store i32 %36, i32* @maxIterations, align 4, !tbaa !25
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %while.end
  %37 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isLeafNode) #8
  %38 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast i32* %subTreeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  %41 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #8
  %42 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #8
  ret void
}

define hidden void @_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %overlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %m_SubtreeHeaders2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %3 = load i32, i32* %i, align 4, !tbaa !25
  %call3 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders2, i32 %3)
  store %class.btBvhSubtreeInfo* %call3, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %4 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %6 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %7, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %8 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %8, i32 0, i32 1
  %arraydecay4 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call5 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %5, i16* %6, i16* %arraydecay, i16* %arraydecay4)
  store i32 %call5, i32* %overlap, align 4, !tbaa !25
  %9 = load i32, i32* %overlap, align 4, !tbaa !25
  %cmp6 = icmp ne i32 %9, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %11 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %12 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %13 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %13, i32 0, i32 2
  %14 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %15 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_rootNodeIndex7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %15, i32 0, i32 2
  %16 = load i32, i32* %m_rootNodeIndex7, align 4, !tbaa !28
  %17 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %17, i32 0, i32 3
  %18 = load i32, i32* %m_subtreeSize, align 4, !tbaa !30
  %add = add nsw i32 %16, %18
  call void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %10, i16* %11, i16* %12, i32 %14, i32 %add)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %19 = bitcast i32* %overlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = bitcast %class.btBvhSubtreeInfo** %subtree to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

define hidden void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhNode* %currentNode, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %currentNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %currentNode, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isLeafNode) #8
  %0 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %2 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %1, i16* %2, i16* %arraydecay, i16* %arraydecay2)
  store i32 %call, i32* %aabbOverlap, align 4, !tbaa !25
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %call3 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %5)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %isLeafNode, align 1, !tbaa !44
  %6 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %cmp = icmp ne i32 %6, 0
  br i1 %cmp, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %7 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then
  %8 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %9 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %call5 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %9)
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %call6 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %10)
  %11 = bitcast %class.btNodeOverlapCallback* %8 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %11, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %12 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %12(%class.btNodeOverlapCallback* %8, i32 %call5, i32 %call6)
  br label %if.end

if.else:                                          ; preds = %if.then
  %13 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %14, i32 1
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %15 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %16 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %17 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %18 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %15, %class.btNodeOverlapCallback* %16, i16* %17, i16* %18)
  %19 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  %20 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call7 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %20)
  br i1 %call7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %21 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %add.ptr8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %21, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %22 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %23 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4, !tbaa !2
  %call9 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %23)
  %add.ptr10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %22, i32 %call9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btQuantizedBvhNode* [ %add.ptr8, %cond.true ], [ %add.ptr10, %cond.false ]
  store %struct.btQuantizedBvhNode* %cond, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %24 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4, !tbaa !2
  %25 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %26 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4, !tbaa !2
  %27 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4, !tbaa !2
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %24, %class.btNodeOverlapCallback* %25, i16* %26, i16* %27)
  %28 = bitcast %struct.btQuantizedBvhNode** %rightChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #8
  %29 = bitcast %struct.btQuantizedBvhNode** %leftChildNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %29) #8
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then4
  br label %if.end11

if.end11:                                         ; preds = %if.end, %entry
  %30 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %30) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isLeafNode) #8
  ret void
}

define hidden void @_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %rootNode = alloca %struct.btOptimizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %struct.btOptimizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 0)
  store %struct.btOptimizedBvhNode* %call, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %1 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %curIndex, align 4, !tbaa !25
  %3 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %walkIterations, align 4, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isLeafNode) #8
  %4 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  br label %while.cond

while.cond:                                       ; preds = %if.end10, %entry
  %5 = load i32, i32* %curIndex, align 4, !tbaa !25
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %walkIterations, align 4, !tbaa !25
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %10 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %10, i32 0, i32 0
  %11 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %11, i32 0, i32 1
  %call2 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg)
  %conv = zext i1 %call2 to i32
  store i32 %conv, i32* %aabbOverlap, align 4, !tbaa !25
  %12 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %12, i32 0, i32 2
  %13 = load i32, i32* %m_escapeIndex, align 4, !tbaa !45
  %cmp3 = icmp eq i32 %13, -1
  %frombool = zext i1 %cmp3 to i8
  store i8 %frombool, i8* %isLeafNode, align 1, !tbaa !44
  %14 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %14 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %15 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %cmp4 = icmp ne i32 %15, 0
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %16 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %17 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %17, i32 0, i32 3
  %18 = load i32, i32* %m_subPart, align 4, !tbaa !47
  %19 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %19, i32 0, i32 4
  %20 = load i32, i32* %m_triangleIndex, align 4, !tbaa !48
  %21 = bitcast %class.btNodeOverlapCallback* %16 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %21, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %22 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %22(%class.btNodeOverlapCallback* %16, i32 %18, i32 %20)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %23 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %cmp5 = icmp ne i32 %23, 0
  br i1 %cmp5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %24 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool6 = trunc i8 %24 to i1
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  %25 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %25, i32 1
  store %struct.btOptimizedBvhNode* %incdec.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %26 = load i32, i32* %curIndex, align 4, !tbaa !25
  %inc8 = add nsw i32 %26, 1
  store i32 %inc8, i32* %curIndex, align 4, !tbaa !25
  br label %if.end10

if.else:                                          ; preds = %lor.lhs.false
  %27 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_escapeIndex9 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %27, i32 0, i32 2
  %28 = load i32, i32* %m_escapeIndex9, align 4, !tbaa !45
  store i32 %28, i32* %escapeIndex, align 4, !tbaa !25
  %29 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %30 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %30, i32 %29
  store %struct.btOptimizedBvhNode* %add.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %31 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %32 = load i32, i32* %curIndex, align 4, !tbaa !25
  %add = add nsw i32 %32, %31
  store i32 %add, i32* %curIndex, align 4, !tbaa !25
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %33 = load i32, i32* @maxIterations, align 4, !tbaa !25
  %34 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %cmp11 = icmp slt i32 %33, %34
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %while.end
  %35 = load i32, i32* %walkIterations, align 4, !tbaa !25
  store i32 %35, i32* @maxIterations, align 4, !tbaa !25
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %while.end
  %36 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isLeafNode) #8
  %37 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  %38 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #8
  %40 = bitcast %struct.btOptimizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %0, i32 %1
  ret %struct.btOptimizedBvhNode* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #1 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #8
  store i8 1, i8* %overlap, align 1, !tbaa !44
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !23
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !23
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !23
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !23
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !44
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !23
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !23
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !23
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !23
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !44, !range !26
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !44
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !23
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !23
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !23
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !23
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !44, !range !26
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !44
  %27 = load i8, i8* %overlap, align 1, !tbaa !44, !range !26
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #8
  ret i1 %tobool31
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %aabbMin1, i16* %aabbMax1, i16* %aabbMin2, i16* %aabbMax2) #3 comdat {
entry:
  %aabbMin1.addr = alloca i16*, align 4
  %aabbMax1.addr = alloca i16*, align 4
  %aabbMin2.addr = alloca i16*, align 4
  %aabbMax2.addr = alloca i16*, align 4
  store i16* %aabbMin1, i16** %aabbMin1.addr, align 4, !tbaa !2
  store i16* %aabbMax1, i16** %aabbMax1.addr, align 4, !tbaa !2
  store i16* %aabbMin2, i16** %aabbMin2.addr, align 4, !tbaa !2
  store i16* %aabbMax2, i16** %aabbMax2.addr, align 4, !tbaa !2
  %0 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %conv = zext i16 %1 to i32
  %2 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i16, i16* %2, i32 0
  %3 = load i16, i16* %arrayidx1, align 2, !tbaa !38
  %conv2 = zext i16 %3 to i32
  %cmp = icmp sle i32 %conv, %conv2
  %conv3 = zext i1 %cmp to i32
  %4 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx4, align 2, !tbaa !38
  %conv5 = zext i16 %5 to i32
  %6 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i16, i16* %6, i32 0
  %7 = load i16, i16* %arrayidx6, align 2, !tbaa !38
  %conv7 = zext i16 %7 to i32
  %cmp8 = icmp sge i32 %conv5, %conv7
  %conv9 = zext i1 %cmp8 to i32
  %and = and i32 %conv3, %conv9
  %8 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 2
  %9 = load i16, i16* %arrayidx10, align 2, !tbaa !38
  %conv11 = zext i16 %9 to i32
  %10 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i16, i16* %10, i32 2
  %11 = load i16, i16* %arrayidx12, align 2, !tbaa !38
  %conv13 = zext i16 %11 to i32
  %cmp14 = icmp sle i32 %conv11, %conv13
  %conv15 = zext i1 %cmp14 to i32
  %and16 = and i32 %and, %conv15
  %12 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx17 = getelementptr inbounds i16, i16* %12, i32 2
  %13 = load i16, i16* %arrayidx17, align 2, !tbaa !38
  %conv18 = zext i16 %13 to i32
  %14 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i16, i16* %14, i32 2
  %15 = load i16, i16* %arrayidx19, align 2, !tbaa !38
  %conv20 = zext i16 %15 to i32
  %cmp21 = icmp sge i32 %conv18, %conv20
  %conv22 = zext i1 %cmp21 to i32
  %and23 = and i32 %and16, %conv22
  %16 = load i16*, i16** %aabbMin1.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i16, i16* %16, i32 1
  %17 = load i16, i16* %arrayidx24, align 2, !tbaa !38
  %conv25 = zext i16 %17 to i32
  %18 = load i16*, i16** %aabbMax2.addr, align 4, !tbaa !2
  %arrayidx26 = getelementptr inbounds i16, i16* %18, i32 1
  %19 = load i16, i16* %arrayidx26, align 2, !tbaa !38
  %conv27 = zext i16 %19 to i32
  %cmp28 = icmp sle i32 %conv25, %conv27
  %conv29 = zext i1 %cmp28 to i32
  %and30 = and i32 %and23, %conv29
  %20 = load i16*, i16** %aabbMax1.addr, align 4, !tbaa !2
  %arrayidx31 = getelementptr inbounds i16, i16* %20, i32 1
  %21 = load i16, i16* %arrayidx31, align 2, !tbaa !38
  %conv32 = zext i16 %21 to i32
  %22 = load i16*, i16** %aabbMin2.addr, align 4, !tbaa !2
  %arrayidx33 = getelementptr inbounds i16, i16* %22, i32 1
  %23 = load i16, i16* %arrayidx33, align 2, !tbaa !38
  %conv34 = zext i16 %23 to i32
  %cmp35 = icmp sge i32 %conv32, %conv34
  %conv36 = zext i1 %cmp35 to i32
  %and37 = and i32 %and30, %conv36
  %call = call i32 @_Z8btSelectjii(i32 %and37, i32 1, i32 0)
  ret i32 %call
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %shr = ashr i32 %0, 21
  ret i32 %shr
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %this) #5 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %0 = bitcast i32* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %x, align 4, !tbaa !25
  %1 = bitcast i32* %y to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %x, align 4, !tbaa !25
  %and = and i32 %2, 0
  %neg = xor i32 %and, -1
  %shl = shl i32 %neg, 21
  store i32 %shl, i32* %y, align 4, !tbaa !25
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %3 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %4 = load i32, i32* %y, align 4, !tbaa !25
  %neg2 = xor i32 %4, -1
  %and3 = and i32 %3, %neg2
  %5 = bitcast i32* %y to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  %6 = bitcast i32* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #8
  ret i32 %and3
}

define hidden void @_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %rootNode = alloca %struct.btOptimizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  %rayBoxOverlap = alloca i32, align 4
  %lambda_max = alloca float, align 4
  %rayAabbMin = alloca %class.btVector3, align 4
  %rayAabbMax = alloca %class.btVector3, align 4
  %rayDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %rayDirectionInverse = alloca %class.btVector3, align 4
  %sign = alloca [3 x i32], align 4
  %bounds = alloca [2 x %class.btVector3], align 16
  %param = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4, !tbaa !25
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %struct.btOptimizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 0)
  store %struct.btOptimizedBvhNode* %call, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %1 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %curIndex, align 4, !tbaa !25
  %3 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  store i32 0, i32* %walkIterations, align 4, !tbaa !25
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isLeafNode) #8
  %4 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store i32 0, i32* %aabbOverlap, align 4, !tbaa !25
  %5 = bitcast i32* %rayBoxOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store i32 0, i32* %rayBoxOverlap, align 4, !tbaa !25
  %6 = bitcast float* %lambda_max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 1.000000e+00, float* %lambda_max, align 4, !tbaa !23
  %7 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %rayAabbMin to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !42
  %11 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #8
  %12 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %13 = bitcast %class.btVector3* %rayAabbMax to i8*
  %14 = bitcast %class.btVector3* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false), !tbaa.struct !42
  %15 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  %16 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %17 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %18 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  %19 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %19) #8
  %20 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %21 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDir)
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #8
  %23 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store float %call5, float* %lambda_max, align 4, !tbaa !23
  %25 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %25) #8
  %26 = bitcast %class.btVector3* %rayDirectionInverse to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #8
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rayDirectionInverse)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx = getelementptr inbounds float, float* %call7, i32 0
  %27 = load float, float* %arrayidx, align 4, !tbaa !23
  %cmp = fcmp oeq float %27, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %28 = load float, float* %arrayidx9, align 4, !tbaa !23
  %div = fdiv float 1.000000e+00, %28
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x43ABC16D60000000, %cond.true ], [ %div, %cond.false ]
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  store float %cond, float* %arrayidx11, align 4, !tbaa !23
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %29 = load float, float* %arrayidx13, align 4, !tbaa !23
  %cmp14 = fcmp oeq float %29, 0.000000e+00
  br i1 %cmp14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end
  br label %cond.end20

cond.false16:                                     ; preds = %cond.end
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %30 = load float, float* %arrayidx18, align 4, !tbaa !23
  %div19 = fdiv float 1.000000e+00, %30
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false16, %cond.true15
  %cond21 = phi float [ 0x43ABC16D60000000, %cond.true15 ], [ %div19, %cond.false16 ]
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  store float %cond21, float* %arrayidx23, align 4, !tbaa !23
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %31 = load float, float* %arrayidx25, align 4, !tbaa !23
  %cmp26 = fcmp oeq float %31, 0.000000e+00
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %cond.end20
  br label %cond.end32

cond.false28:                                     ; preds = %cond.end20
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %32 = load float, float* %arrayidx30, align 4, !tbaa !23
  %div31 = fdiv float 1.000000e+00, %32
  br label %cond.end32

cond.end32:                                       ; preds = %cond.false28, %cond.true27
  %cond33 = phi float [ 0x43ABC16D60000000, %cond.true27 ], [ %div31, %cond.false28 ]
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  store float %cond33, float* %arrayidx35, align 4, !tbaa !23
  %33 = bitcast [3 x i32]* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %33) #8
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 0
  %34 = load float, float* %arrayidx37, align 4, !tbaa !23
  %conv = fpext float %34 to double
  %cmp38 = fcmp olt double %conv, 0.000000e+00
  %conv39 = zext i1 %cmp38 to i32
  store i32 %conv39, i32* %arrayinit.begin, align 4, !tbaa !25
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %35 = load float, float* %arrayidx41, align 4, !tbaa !23
  %conv42 = fpext float %35 to double
  %cmp43 = fcmp olt double %conv42, 0.000000e+00
  %conv44 = zext i1 %cmp43 to i32
  store i32 %conv44, i32* %arrayinit.element, align 4, !tbaa !25
  %arrayinit.element45 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 2
  %36 = load float, float* %arrayidx47, align 4, !tbaa !23
  %conv48 = fpext float %36 to double
  %cmp49 = fcmp olt double %conv48, 0.000000e+00
  %conv50 = zext i1 %cmp49 to i32
  store i32 %conv50, i32* %arrayinit.element45, align 4, !tbaa !25
  %37 = bitcast [2 x %class.btVector3]* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %37) #8
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %cond.end32
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %cond.end32 ], [ %arrayctor.next, %arrayctor.loop ]
  %call51 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  br label %while.cond

while.cond:                                       ; preds = %if.end78, %arrayctor.cont
  %38 = load i32, i32* %curIndex, align 4, !tbaa !25
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %39 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %cmp52 = icmp slt i32 %38, %39
  br i1 %cmp52, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %40 = bitcast float* %param to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  store float 1.000000e+00, float* %param, align 4, !tbaa !23
  %41 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %walkIterations, align 4, !tbaa !25
  %42 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %42, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %43 = bitcast %class.btVector3* %arrayidx53 to i8*
  %44 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %43, i8* align 4 %44, i32 16, i1 false), !tbaa.struct !42
  %45 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %45, i32 0, i32 1
  %arrayidx54 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %46 = bitcast %class.btVector3* %arrayidx54 to i8*
  %47 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %46, i8* align 4 %47, i32 16, i1 false), !tbaa.struct !42
  %48 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %arrayidx55 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx55, %class.btVector3* nonnull align 4 dereferenceable(16) %48)
  %49 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %arrayidx57 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %call58 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx57, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  %50 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMinOrg59 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %50, i32 0, i32 0
  %51 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_aabbMaxOrg60 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %51, i32 0, i32 1
  %call61 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg59, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg60)
  %conv62 = zext i1 %call61 to i32
  store i32 %conv62, i32* %aabbOverlap, align 4, !tbaa !25
  %52 = load i32, i32* %aabbOverlap, align 4, !tbaa !25
  %tobool = icmp ne i32 %52, 0
  br i1 %tobool, label %cond.true63, label %cond.false66

cond.true63:                                      ; preds = %while.body
  %53 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %arraydecay64 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %54 = load float, float* %lambda_max, align 4, !tbaa !23
  %call65 = call zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %53, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDirectionInverse, i32* %arraydecay, %class.btVector3* %arraydecay64, float* nonnull align 4 dereferenceable(4) %param, float 0.000000e+00, float %54)
  br label %cond.end67

cond.false66:                                     ; preds = %while.body
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true63
  %cond68 = phi i1 [ %call65, %cond.true63 ], [ false, %cond.false66 ]
  %conv69 = zext i1 %cond68 to i32
  store i32 %conv69, i32* %rayBoxOverlap, align 4, !tbaa !25
  %55 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %55, i32 0, i32 2
  %56 = load i32, i32* %m_escapeIndex, align 4, !tbaa !45
  %cmp70 = icmp eq i32 %56, -1
  %frombool = zext i1 %cmp70 to i8
  store i8 %frombool, i8* %isLeafNode, align 1, !tbaa !44
  %57 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool71 = trunc i8 %57 to i1
  br i1 %tobool71, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end67
  %58 = load i32, i32* %rayBoxOverlap, align 4, !tbaa !25
  %cmp72 = icmp ne i32 %58, 0
  br i1 %cmp72, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %59 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %60 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %60, i32 0, i32 3
  %61 = load i32, i32* %m_subPart, align 4, !tbaa !47
  %62 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %62, i32 0, i32 4
  %63 = load i32, i32* %m_triangleIndex, align 4, !tbaa !48
  %64 = bitcast %class.btNodeOverlapCallback* %59 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %64, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %65 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %65(%class.btNodeOverlapCallback* %59, i32 %61, i32 %63)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %cond.end67
  %66 = load i32, i32* %rayBoxOverlap, align 4, !tbaa !25
  %cmp73 = icmp ne i32 %66, 0
  br i1 %cmp73, label %if.then75, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %67 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool74 = trunc i8 %67 to i1
  br i1 %tobool74, label %if.then75, label %if.else

if.then75:                                        ; preds = %lor.lhs.false, %if.end
  %68 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %68, i32 1
  store %struct.btOptimizedBvhNode* %incdec.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %69 = load i32, i32* %curIndex, align 4, !tbaa !25
  %inc76 = add nsw i32 %69, 1
  store i32 %inc76, i32* %curIndex, align 4, !tbaa !25
  br label %if.end78

if.else:                                          ; preds = %lor.lhs.false
  %70 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_escapeIndex77 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %70, i32 0, i32 2
  %71 = load i32, i32* %m_escapeIndex77, align 4, !tbaa !45
  store i32 %71, i32* %escapeIndex, align 4, !tbaa !25
  %72 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %73 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %73, i32 %72
  store %struct.btOptimizedBvhNode* %add.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4, !tbaa !2
  %74 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %75 = load i32, i32* %curIndex, align 4, !tbaa !25
  %add = add nsw i32 %75, %74
  store i32 %add, i32* %curIndex, align 4, !tbaa !25
  br label %if.end78

if.end78:                                         ; preds = %if.else, %if.then75
  %76 = bitcast float* %param to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %77 = load i32, i32* @maxIterations, align 4, !tbaa !25
  %78 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %cmp79 = icmp slt i32 %77, %78
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %while.end
  %79 = load i32, i32* %walkIterations, align 4, !tbaa !25
  store i32 %79, i32* @maxIterations, align 4, !tbaa !25
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %while.end
  %80 = bitcast [2 x %class.btVector3]* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %80) #8
  %81 = bitcast [3 x i32]* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %81) #8
  %82 = bitcast %class.btVector3* %rayDirectionInverse to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %82) #8
  %83 = bitcast %class.btVector3* %rayDir to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %83) #8
  %84 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %84) #8
  %85 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %85) #8
  %86 = bitcast float* %lambda_max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast i32* %rayBoxOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast i32* %aabbOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isLeafNode) #8
  %89 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast %struct.btOptimizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !23
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #8
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !23
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !23
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !23
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !23
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !23
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !23
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !23
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4, !tbaa !23
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4, !tbaa !23
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !23
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4, !tbaa !23
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4, !tbaa !23
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !23
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4, !tbaa !23
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayInvDirection, i32* %raySign, %class.btVector3* %bounds, float* nonnull align 4 dereferenceable(4) %tmin, float %lambda_min, float %lambda_max) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayInvDirection.addr = alloca %class.btVector3*, align 4
  %raySign.addr = alloca i32*, align 4
  %bounds.addr = alloca %class.btVector3*, align 4
  %tmin.addr = alloca float*, align 4
  %lambda_min.addr = alloca float, align 4
  %lambda_max.addr = alloca float, align 4
  %tmax = alloca float, align 4
  %tymin = alloca float, align 4
  %tymax = alloca float, align 4
  %tzmin = alloca float, align 4
  %tzmax = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayInvDirection, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  store i32* %raySign, i32** %raySign.addr, align 4, !tbaa !2
  store %class.btVector3* %bounds, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  store float* %tmin, float** %tmin.addr, align 4, !tbaa !2
  store float %lambda_min, float* %lambda_min.addr, align 4, !tbaa !23
  store float %lambda_max, float* %lambda_max.addr, align 4, !tbaa !23
  %0 = bitcast float* %tmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast float* %tymin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = bitcast float* %tymax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  %3 = bitcast float* %tzmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = bitcast float* %tzmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  %5 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %6 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 0
  %7 = load i32, i32* %arrayidx, align 4, !tbaa !25
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %7
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx1)
  %8 = load float, float* %call, align 4, !tbaa !23
  %9 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %9)
  %10 = load float, float* %call2, align 4, !tbaa !23
  %sub = fsub float %8, %10
  %11 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %11)
  %12 = load float, float* %call3, align 4, !tbaa !23
  %mul = fmul float %sub, %12
  %13 = load float*, float** %tmin.addr, align 4, !tbaa !2
  store float %mul, float* %13, align 4, !tbaa !23
  %14 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %15 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i32, i32* %15, i32 0
  %16 = load i32, i32* %arrayidx4, align 4, !tbaa !25
  %sub5 = sub i32 1, %16
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %sub5
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx6)
  %17 = load float, float* %call7, align 4, !tbaa !23
  %18 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %18)
  %19 = load float, float* %call8, align 4, !tbaa !23
  %sub9 = fsub float %17, %19
  %20 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %20)
  %21 = load float, float* %call10, align 4, !tbaa !23
  %mul11 = fmul float %sub9, %21
  store float %mul11, float* %tmax, align 4, !tbaa !23
  %22 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %23 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx12 = getelementptr inbounds i32, i32* %23, i32 1
  %24 = load i32, i32* %arrayidx12, align 4, !tbaa !25
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %24
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx13)
  %25 = load float, float* %call14, align 4, !tbaa !23
  %26 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %26)
  %27 = load float, float* %call15, align 4, !tbaa !23
  %sub16 = fsub float %25, %27
  %28 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4, !tbaa !23
  %mul18 = fmul float %sub16, %29
  store float %mul18, float* %tymin, align 4, !tbaa !23
  %30 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %31 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx19 = getelementptr inbounds i32, i32* %31, i32 1
  %32 = load i32, i32* %arrayidx19, align 4, !tbaa !25
  %sub20 = sub i32 1, %32
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 %sub20
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx21)
  %33 = load float, float* %call22, align 4, !tbaa !23
  %34 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %34)
  %35 = load float, float* %call23, align 4, !tbaa !23
  %sub24 = fsub float %33, %35
  %36 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %36)
  %37 = load float, float* %call25, align 4, !tbaa !23
  %mul26 = fmul float %sub24, %37
  store float %mul26, float* %tymax, align 4, !tbaa !23
  %38 = load float*, float** %tmin.addr, align 4, !tbaa !2
  %39 = load float, float* %38, align 4, !tbaa !23
  %40 = load float, float* %tymax, align 4, !tbaa !23
  %cmp = fcmp ogt float %39, %40
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %41 = load float, float* %tymin, align 4, !tbaa !23
  %42 = load float, float* %tmax, align 4, !tbaa !23
  %cmp27 = fcmp ogt float %41, %42
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  %43 = load float, float* %tymin, align 4, !tbaa !23
  %44 = load float*, float** %tmin.addr, align 4, !tbaa !2
  %45 = load float, float* %44, align 4, !tbaa !23
  %cmp28 = fcmp ogt float %43, %45
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  %46 = load float, float* %tymin, align 4, !tbaa !23
  %47 = load float*, float** %tmin.addr, align 4, !tbaa !2
  store float %46, float* %47, align 4, !tbaa !23
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end
  %48 = load float, float* %tymax, align 4, !tbaa !23
  %49 = load float, float* %tmax, align 4, !tbaa !23
  %cmp31 = fcmp olt float %48, %49
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end30
  %50 = load float, float* %tymax, align 4, !tbaa !23
  store float %50, float* %tmax, align 4, !tbaa !23
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.end30
  %51 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %52 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx34 = getelementptr inbounds i32, i32* %52, i32 2
  %53 = load i32, i32* %arrayidx34, align 4, !tbaa !25
  %arrayidx35 = getelementptr inbounds %class.btVector3, %class.btVector3* %51, i32 %53
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx35)
  %54 = load float, float* %call36, align 4, !tbaa !23
  %55 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %55)
  %56 = load float, float* %call37, align 4, !tbaa !23
  %sub38 = fsub float %54, %56
  %57 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %57)
  %58 = load float, float* %call39, align 4, !tbaa !23
  %mul40 = fmul float %sub38, %58
  store float %mul40, float* %tzmin, align 4, !tbaa !23
  %59 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4, !tbaa !2
  %60 = load i32*, i32** %raySign.addr, align 4, !tbaa !2
  %arrayidx41 = getelementptr inbounds i32, i32* %60, i32 2
  %61 = load i32, i32* %arrayidx41, align 4, !tbaa !25
  %sub42 = sub i32 1, %61
  %arrayidx43 = getelementptr inbounds %class.btVector3, %class.btVector3* %59, i32 %sub42
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx43)
  %62 = load float, float* %call44, align 4, !tbaa !23
  %63 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %63)
  %64 = load float, float* %call45, align 4, !tbaa !23
  %sub46 = fsub float %62, %64
  %65 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4, !tbaa !2
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %65)
  %66 = load float, float* %call47, align 4, !tbaa !23
  %mul48 = fmul float %sub46, %66
  store float %mul48, float* %tzmax, align 4, !tbaa !23
  %67 = load float*, float** %tmin.addr, align 4, !tbaa !2
  %68 = load float, float* %67, align 4, !tbaa !23
  %69 = load float, float* %tzmax, align 4, !tbaa !23
  %cmp49 = fcmp ogt float %68, %69
  br i1 %cmp49, label %if.then52, label %lor.lhs.false50

lor.lhs.false50:                                  ; preds = %if.end33
  %70 = load float, float* %tzmin, align 4, !tbaa !23
  %71 = load float, float* %tmax, align 4, !tbaa !23
  %cmp51 = fcmp ogt float %70, %71
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %lor.lhs.false50, %if.end33
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %lor.lhs.false50
  %72 = load float, float* %tzmin, align 4, !tbaa !23
  %73 = load float*, float** %tmin.addr, align 4, !tbaa !2
  %74 = load float, float* %73, align 4, !tbaa !23
  %cmp54 = fcmp ogt float %72, %74
  br i1 %cmp54, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end53
  %75 = load float, float* %tzmin, align 4, !tbaa !23
  %76 = load float*, float** %tmin.addr, align 4, !tbaa !2
  store float %75, float* %76, align 4, !tbaa !23
  br label %if.end56

if.end56:                                         ; preds = %if.then55, %if.end53
  %77 = load float, float* %tzmax, align 4, !tbaa !23
  %78 = load float, float* %tmax, align 4, !tbaa !23
  %cmp57 = fcmp olt float %77, %78
  br i1 %cmp57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end56
  %79 = load float, float* %tzmax, align 4, !tbaa !23
  store float %79, float* %tmax, align 4, !tbaa !23
  br label %if.end59

if.end59:                                         ; preds = %if.then58, %if.end56
  %80 = load float*, float** %tmin.addr, align 4, !tbaa !2
  %81 = load float, float* %80, align 4, !tbaa !23
  %82 = load float, float* %lambda_max.addr, align 4, !tbaa !23
  %cmp60 = fcmp olt float %81, %82
  br i1 %cmp60, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end59
  %83 = load float, float* %tmax, align 4, !tbaa !23
  %84 = load float, float* %lambda_min.addr, align 4, !tbaa !23
  %cmp61 = fcmp ogt float %83, %84
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end59
  %85 = phi i1 [ false, %if.end59 ], [ %cmp61, %land.rhs ]
  store i1 %85, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %land.end, %if.then52, %if.then
  %86 = bitcast float* %tzmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %86) #8
  %87 = bitcast float* %tzmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %87) #8
  %88 = bitcast float* %tymax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %88) #8
  %89 = bitcast float* %tymin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %89) #8
  %90 = bitcast float* %tmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = load i1, i1* %retval, align 1
  ret i1 %91
}

define hidden void @_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %subTreeSize = alloca i32, align 4
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %boxBoxOverlap = alloca i32, align 4
  %rayBoxOverlap = alloca i32, align 4
  %lambda_max = alloca float, align 4
  %rayDirection = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %sign = alloca [3 x i32], align 4
  %rayAabbMin = alloca %class.btVector3, align 4
  %rayAabbMax = alloca %class.btVector3, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %param = alloca float, align 4
  %bounds = alloca [2 x %class.btVector3], align 16
  %ref.tmp59 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %normal = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4, !tbaa !25
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  store i32 %1, i32* %curIndex, align 4, !tbaa !25
  %2 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #8
  store i32 0, i32* %walkIterations, align 4, !tbaa !25
  %3 = bitcast i32* %subTreeSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %5 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  %sub = sub nsw i32 %4, %5
  store i32 %sub, i32* %subTreeSize, align 4, !tbaa !25
  %6 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %7 = load i32, i32* %startNodeIndex.addr, align 4, !tbaa !25
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %7)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %8 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isLeafNode) #8
  %9 = bitcast i32* %boxBoxOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store i32 0, i32* %boxBoxOverlap, align 4, !tbaa !25
  %10 = bitcast i32* %rayBoxOverlap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store i32 0, i32* %rayBoxOverlap, align 4, !tbaa !25
  %11 = bitcast float* %lambda_max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #8
  store float 1.000000e+00, float* %lambda_max, align 4, !tbaa !23
  %12 = bitcast %class.btVector3* %rayDirection to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %12) #8
  %13 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDirection)
  %15 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %15) #8
  %16 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store float %call3, float* %lambda_max, align 4, !tbaa !23
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %19 = load float, float* %arrayidx, align 4, !tbaa !23
  %cmp = fcmp oeq float %19, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %20 = load float, float* %arrayidx6, align 4, !tbaa !23
  %div = fdiv float 1.000000e+00, %20
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x43ABC16D60000000, %cond.true ], [ %div, %cond.false ]
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  store float %cond, float* %arrayidx8, align 4, !tbaa !23
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %21 = load float, float* %arrayidx10, align 4, !tbaa !23
  %cmp11 = fcmp oeq float %21, 0.000000e+00
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %cond.end
  br label %cond.end17

cond.false13:                                     ; preds = %cond.end
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %22 = load float, float* %arrayidx15, align 4, !tbaa !23
  %div16 = fdiv float 1.000000e+00, %22
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false13, %cond.true12
  %cond18 = phi float [ 0x43ABC16D60000000, %cond.true12 ], [ %div16, %cond.false13 ]
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  store float %cond18, float* %arrayidx20, align 4, !tbaa !23
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %23 = load float, float* %arrayidx22, align 4, !tbaa !23
  %cmp23 = fcmp oeq float %23, 0.000000e+00
  br i1 %cmp23, label %cond.true24, label %cond.false25

cond.true24:                                      ; preds = %cond.end17
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end17
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %24 = load float, float* %arrayidx27, align 4, !tbaa !23
  %div28 = fdiv float 1.000000e+00, %24
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true24
  %cond30 = phi float [ 0x43ABC16D60000000, %cond.true24 ], [ %div28, %cond.false25 ]
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  store float %cond30, float* %arrayidx32, align 4, !tbaa !23
  %25 = bitcast [3 x i32]* %sign to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %25) #8
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 0
  %26 = load float, float* %arrayidx34, align 4, !tbaa !23
  %conv = fpext float %26 to double
  %cmp35 = fcmp olt double %conv, 0.000000e+00
  %conv36 = zext i1 %cmp35 to i32
  store i32 %conv36, i32* %arrayinit.begin, align 4, !tbaa !25
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %27 = load float, float* %arrayidx38, align 4, !tbaa !23
  %conv39 = fpext float %27 to double
  %cmp40 = fcmp olt double %conv39, 0.000000e+00
  %conv41 = zext i1 %cmp40 to i32
  store i32 %conv41, i32* %arrayinit.element, align 4, !tbaa !25
  %arrayinit.element42 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %28 = load float, float* %arrayidx44, align 4, !tbaa !23
  %conv45 = fpext float %28 to double
  %cmp46 = fcmp olt double %conv45, 0.000000e+00
  %conv47 = zext i1 %cmp46 to i32
  store i32 %conv47, i32* %arrayinit.element42, align 4, !tbaa !25
  %29 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #8
  %30 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %31 = bitcast %class.btVector3* %rayAabbMin to i8*
  %32 = bitcast %class.btVector3* %30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false), !tbaa.struct !42
  %33 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %33) #8
  %34 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %35 = bitcast %class.btVector3* %rayAabbMax to i8*
  %36 = bitcast %class.btVector3* %34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false), !tbaa.struct !42
  %37 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  %38 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %38)
  %39 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %39)
  %40 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %40)
  %41 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %41) #8
  %42 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %42) #8
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMin, i32 0)
  %arraydecay50 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay50, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMax, i32 1)
  br label %while.cond

while.cond:                                       ; preds = %if.end87, %cond.end29
  %43 = load i32, i32* %curIndex, align 4, !tbaa !25
  %44 = load i32, i32* %endNodeIndex.addr, align 4, !tbaa !25
  %cmp51 = icmp slt i32 %43, %44
  br i1 %cmp51, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %45 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %walkIterations, align 4, !tbaa !25
  %46 = bitcast float* %param to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #8
  store float 1.000000e+00, float* %param, align 4, !tbaa !23
  store i32 0, i32* %rayBoxOverlap, align 4, !tbaa !25
  %arraydecay52 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay53 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %47 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %47, i32 0, i32 0
  %arraydecay54 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %48 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %48, i32 0, i32 1
  %arraydecay55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call56 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %arraydecay52, i16* %arraydecay53, i16* %arraydecay54, i16* %arraydecay55)
  store i32 %call56, i32* %boxBoxOverlap, align 4, !tbaa !25
  %49 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call57 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %49)
  %frombool = zext i1 %call57 to i8
  store i8 %frombool, i8* %isLeafNode, align 1, !tbaa !44
  %50 = load i32, i32* %boxBoxOverlap, align 4, !tbaa !25
  %tobool = icmp ne i32 %50, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %51 = bitcast [2 x %class.btVector3]* %bounds to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %51) #8
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then ], [ %arrayctor.next, %arrayctor.loop ]
  %call58 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %52 = bitcast %class.btVector3* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %52) #8
  %53 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMin60 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %53, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin60, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp59, %class.btQuantizedBvh* %this1, i16* %arraydecay61)
  %arrayidx62 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %54 = bitcast %class.btVector3* %arrayidx62 to i8*
  %55 = bitcast %class.btVector3* %ref.tmp59 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %54, i8* align 4 %55, i32 16, i1 false), !tbaa.struct !42
  %56 = bitcast %class.btVector3* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %56) #8
  %57 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %57) #8
  %58 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %m_quantizedAabbMax64 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %58, i32 0, i32 1
  %arraydecay65 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax64, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp63, %class.btQuantizedBvh* %this1, i16* %arraydecay65)
  %arrayidx66 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %59 = bitcast %class.btVector3* %arrayidx66 to i8*
  %60 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %59, i8* align 4 %60, i32 16, i1 false), !tbaa.struct !42
  %61 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %61) #8
  %62 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %arrayidx67 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx67, %class.btVector3* nonnull align 4 dereferenceable(16) %62)
  %63 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx69, %class.btVector3* nonnull align 4 dereferenceable(16) %63)
  %64 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %64) #8
  %call71 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  %65 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %arraydecay72 = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %arraydecay73 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %66 = load float, float* %lambda_max, align 4, !tbaa !23
  %call74 = call zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %65, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDirection, i32* %arraydecay72, %class.btVector3* %arraydecay73, float* nonnull align 4 dereferenceable(4) %param, float 0.000000e+00, float %66)
  %conv75 = zext i1 %call74 to i32
  store i32 %conv75, i32* %rayBoxOverlap, align 4, !tbaa !25
  %67 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %67) #8
  %68 = bitcast [2 x %class.btVector3]* %bounds to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %68) #8
  br label %if.end

if.end:                                           ; preds = %arrayctor.cont, %while.body
  %69 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool76 = trunc i8 %69 to i1
  br i1 %tobool76, label %land.lhs.true, label %if.end81

land.lhs.true:                                    ; preds = %if.end
  %70 = load i32, i32* %rayBoxOverlap, align 4, !tbaa !25
  %tobool77 = icmp ne i32 %70, 0
  br i1 %tobool77, label %if.then78, label %if.end81

if.then78:                                        ; preds = %land.lhs.true
  %71 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %72 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call79 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %72)
  %73 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call80 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %73)
  %74 = bitcast %class.btNodeOverlapCallback* %71 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %74, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %75 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %75(%class.btNodeOverlapCallback* %71, i32 %call79, i32 %call80)
  br label %if.end81

if.end81:                                         ; preds = %if.then78, %land.lhs.true, %if.end
  %76 = load i32, i32* %rayBoxOverlap, align 4, !tbaa !25
  %cmp82 = icmp ne i32 %76, 0
  br i1 %cmp82, label %if.then84, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end81
  %77 = load i8, i8* %isLeafNode, align 1, !tbaa !44, !range !26
  %tobool83 = trunc i8 %77 to i1
  br i1 %tobool83, label %if.then84, label %if.else

if.then84:                                        ; preds = %lor.lhs.false, %if.end81
  %78 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %78, i32 1
  store %struct.btQuantizedBvhNode* %incdec.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %79 = load i32, i32* %curIndex, align 4, !tbaa !25
  %inc85 = add nsw i32 %79, 1
  store i32 %inc85, i32* %curIndex, align 4, !tbaa !25
  br label %if.end87

if.else:                                          ; preds = %lor.lhs.false
  %80 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %call86 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %80)
  store i32 %call86, i32* %escapeIndex, align 4, !tbaa !25
  %81 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %82 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %82, i32 %81
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4, !tbaa !2
  %83 = load i32, i32* %escapeIndex, align 4, !tbaa !25
  %84 = load i32, i32* %curIndex, align 4, !tbaa !25
  %add = add nsw i32 %84, %83
  store i32 %add, i32* %curIndex, align 4, !tbaa !25
  br label %if.end87

if.end87:                                         ; preds = %if.else, %if.then84
  %85 = bitcast float* %param to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %86 = load i32, i32* @maxIterations, align 4, !tbaa !25
  %87 = load i32, i32* %walkIterations, align 4, !tbaa !25
  %cmp88 = icmp slt i32 %86, %87
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %while.end
  %88 = load i32, i32* %walkIterations, align 4, !tbaa !25
  store i32 %88, i32* @maxIterations, align 4, !tbaa !25
  br label %if.end90

if.end90:                                         ; preds = %if.then89, %while.end
  %89 = bitcast [3 x i16]* %quantizedQueryAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %89) #8
  %90 = bitcast [3 x i16]* %quantizedQueryAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %90) #8
  %91 = bitcast %class.btVector3* %rayAabbMax to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %91) #8
  %92 = bitcast %class.btVector3* %rayAabbMin to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %92) #8
  %93 = bitcast [3 x i32]* %sign to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %93) #8
  %94 = bitcast %class.btVector3* %rayDirection to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %94) #8
  %95 = bitcast float* %lambda_max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = bitcast i32* %rayBoxOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast i32* %boxBoxOverlap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isLeafNode) #8
  %98 = bitcast i32* %escapeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #8
  %99 = bitcast %struct.btQuantizedBvhNode** %rootNode to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %99) #8
  %100 = bitcast i32* %subTreeSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %100) #8
  %101 = bitcast i32* %walkIterations to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %101) #8
  %102 = bitcast i32* %curIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %102) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

define hidden void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %1 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #8
  %4 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #8
  store float 0.000000e+00, float* %ref.tmp2, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  store float 0.000000e+00, float* %ref.tmp3, align 4, !tbaa !23
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store float 0.000000e+00, float* %ref.tmp4, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %7) #8
  %8 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #8
  store float 0.000000e+00, float* %ref.tmp6, align 4, !tbaa !23
  %9 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #8
  store float 0.000000e+00, float* %ref.tmp7, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  store float 0.000000e+00, float* %ref.tmp8, align 4, !tbaa !23
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %11 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #8
  %12 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #8
  %13 = bitcast float* %ref.tmp6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #8
  %15 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %18) #8
  ret void
}

define hidden void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  call void @_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, i32 0, i32 %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4, !tbaa !2
  %9 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4, !tbaa !2
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %12 = load i32, i32* %m_curNodeIndex2, align 4, !tbaa !27
  call void @_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, i32 0, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %0, i32 %1
  ret %struct.btOptimizedBvhNode* %arrayidx
}

; Function Attrs: nounwind
define hidden i32 @_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv() #5 {
entry:
  ret i32 0
}

; Function Attrs: nounwind
define hidden i32 @_ZNK14btQuantizedBvh28calculateSerializeBufferSizeEv(%class.btQuantizedBvh* %this) #5 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %baseSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast i32* %baseSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv()
  %add = add i32 172, %call
  store i32 %add, i32* %baseSize, align 4, !tbaa !25
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %1 = load i32, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %mul = mul i32 32, %1
  %2 = load i32, i32* %baseSize, align 4, !tbaa !25
  %add2 = add i32 %2, %mul
  store i32 %add2, i32* %baseSize, align 4, !tbaa !25
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %3 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %baseSize, align 4, !tbaa !25
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %5 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %mul3 = mul i32 %5, 16
  %add4 = add i32 %4, %mul3
  store i32 %add4, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %baseSize, align 4, !tbaa !25
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex5, align 4, !tbaa !27
  %mul6 = mul i32 %7, 64
  %add7 = add i32 %6, %mul6
  store i32 %add7, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end, %if.then
  %8 = bitcast i32* %baseSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #8
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

define hidden zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh* %this, i8* %o_alignedDataBuffer, i32 %0, i1 zeroext %i_swapEndian) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %o_alignedDataBuffer.addr = alloca i8*, align 4
  %.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %targetBvh = alloca %class.btQuantizedBvh*, align 4
  %nodeData = alloca i8*, align 4
  %sizeToAdd = alloca i32, align 4
  %nodeCount = alloca i32, align 4
  %nodeIndex = alloca i32, align 4
  %nodeIndex94 = alloca i32, align 4
  %nodeIndex162 = alloca i32, align 4
  %nodeIndex199 = alloca i32, align 4
  %i = alloca i32, align 4
  %i323 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i8* %o_alignedDataBuffer, i8** %o_alignedDataBuffer.addr, align 4, !tbaa !2
  store i32 %0, i32* %.addr, align 4, !tbaa !25
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1, !tbaa !44
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %1 = bitcast %class.btQuantizedBvh** %targetBvh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i8*, i8** %o_alignedDataBuffer.addr, align 4, !tbaa !2
  %3 = bitcast i8* %2 to %class.btQuantizedBvh*
  store %class.btQuantizedBvh* %3, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %4 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %5 = bitcast %class.btQuantizedBvh* %4 to i8*
  %call2 = call i8* @_ZN14btQuantizedBvhnwEmPv(i32 172, i8* %5)
  %6 = bitcast i8* %call2 to %class.btQuantizedBvh*
  %call3 = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1Ev(%class.btQuantizedBvh* %6)
  %7 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %7 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %8 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %call4 = call i32 @_Z12btSwapEndiani(i32 %8)
  %9 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 5
  store i32 %call4, i32* %m_curNodeIndex5, align 4, !tbaa !27
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %10 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhAabbMin6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 1
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin6)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %11 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhAabbMax7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %11, i32 0, i32 2
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax7)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %12 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhQuantization8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %12, i32 0, i32 3
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization8)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %13 = load i32, i32* %m_traversalMode, align 4, !tbaa !21
  %call9 = call i32 @_Z12btSwapEndiani(i32 %13)
  %14 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_traversalMode10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %14, i32 0, i32 12
  store i32 %call9, i32* %m_traversalMode10, align 4, !tbaa !21
  %m_subtreeHeaderCount11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %15 = load i32, i32* %m_subtreeHeaderCount11, align 4, !tbaa !22
  %call12 = call i32 @_Z12btSwapEndiani(i32 %15)
  %16 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_subtreeHeaderCount13 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %16, i32 0, i32 14
  store i32 %call12, i32* %m_subtreeHeaderCount13, align 4, !tbaa !22
  br label %if.end

if.else:                                          ; preds = %entry
  %m_curNodeIndex14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %17 = load i32, i32* %m_curNodeIndex14, align 4, !tbaa !27
  %18 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_curNodeIndex15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %18, i32 0, i32 5
  store i32 %17, i32* %m_curNodeIndex15, align 4, !tbaa !27
  %m_bvhAabbMin16 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %19 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhAabbMin17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %19, i32 0, i32 1
  %20 = bitcast %class.btVector3* %m_bvhAabbMin17 to i8*
  %21 = bitcast %class.btVector3* %m_bvhAabbMin16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false), !tbaa.struct !42
  %m_bvhAabbMax18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %22 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhAabbMax19 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %22, i32 0, i32 2
  %23 = bitcast %class.btVector3* %m_bvhAabbMax19 to i8*
  %24 = bitcast %class.btVector3* %m_bvhAabbMax18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !42
  %m_bvhQuantization20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %25 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_bvhQuantization21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %25, i32 0, i32 3
  %26 = bitcast %class.btVector3* %m_bvhQuantization21 to i8*
  %27 = bitcast %class.btVector3* %m_bvhQuantization20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false), !tbaa.struct !42
  %m_traversalMode22 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %28 = load i32, i32* %m_traversalMode22, align 4, !tbaa !21
  %29 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_traversalMode23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %29, i32 0, i32 12
  store i32 %28, i32* %m_traversalMode23, align 4, !tbaa !21
  %m_subtreeHeaderCount24 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %30 = load i32, i32* %m_subtreeHeaderCount24, align 4, !tbaa !22
  %31 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_subtreeHeaderCount25 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %31, i32 0, i32 14
  store i32 %30, i32* %m_subtreeHeaderCount25, align 4, !tbaa !22
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %32 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool26 = trunc i8 %32 to i1
  %33 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_useQuantization27 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %33, i32 0, i32 6
  %frombool28 = zext i1 %tobool26 to i8
  store i8 %frombool28, i8* %m_useQuantization27, align 4, !tbaa !20
  %34 = bitcast i8** %nodeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #8
  %35 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %36 = bitcast %class.btQuantizedBvh* %35 to i8*
  store i8* %36, i8** %nodeData, align 4, !tbaa !2
  %37 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %37, i32 172
  store i8* %add.ptr, i8** %nodeData, align 4, !tbaa !2
  %38 = bitcast i32* %sizeToAdd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #8
  store i32 0, i32* %sizeToAdd, align 4, !tbaa !25
  %39 = load i32, i32* %sizeToAdd, align 4, !tbaa !25
  %40 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr29 = getelementptr inbounds i8, i8* %40, i32 %39
  store i8* %add.ptr29, i8** %nodeData, align 4, !tbaa !2
  %41 = bitcast i32* %nodeCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %m_curNodeIndex30 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %42 = load i32, i32* %m_curNodeIndex30, align 4, !tbaa !27
  store i32 %42, i32* %nodeCount, align 4, !tbaa !25
  %m_useQuantization31 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %43 = load i8, i8* %m_useQuantization31, align 4, !tbaa !20, !range !26
  %tobool32 = trunc i8 %43 to i1
  br i1 %tobool32, label %if.then33, label %if.else159

if.then33:                                        ; preds = %if.end
  %44 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %44, i32 0, i32 11
  %45 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %46 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %47 = load i32, i32* %nodeCount, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i8* %45, i32 %46, i32 %47)
  %48 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool34 = trunc i8 %48 to i1
  br i1 %tobool34, label %if.then35, label %if.else93

if.then35:                                        ; preds = %if.then33
  %49 = bitcast i32* %nodeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #8
  store i32 0, i32* %nodeIndex, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then35
  %50 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %51 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp = icmp slt i32 %50, %51
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %52 = bitcast i32* %nodeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_quantizedContiguousNodes36 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %53 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call37 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes36, i32 %53)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call37, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %54 = load i16, i16* %arrayidx, align 4, !tbaa !38
  %call38 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %54)
  %55 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes39 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %55, i32 0, i32 11
  %56 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call40 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes39, i32 %56)
  %m_quantizedAabbMin41 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call40, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin41, i32 0, i32 0
  store i16 %call38, i16* %arrayidx42, align 4, !tbaa !38
  %m_quantizedContiguousNodes43 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %57 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call44 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes43, i32 %57)
  %m_quantizedAabbMin45 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call44, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin45, i32 0, i32 1
  %58 = load i16, i16* %arrayidx46, align 2, !tbaa !38
  %call47 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %58)
  %59 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes48 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %59, i32 0, i32 11
  %60 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call49 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes48, i32 %60)
  %m_quantizedAabbMin50 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call49, i32 0, i32 0
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin50, i32 0, i32 1
  store i16 %call47, i16* %arrayidx51, align 2, !tbaa !38
  %m_quantizedContiguousNodes52 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %61 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call53 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes52, i32 %61)
  %m_quantizedAabbMin54 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call53, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin54, i32 0, i32 2
  %62 = load i16, i16* %arrayidx55, align 4, !tbaa !38
  %call56 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %62)
  %63 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes57 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %63, i32 0, i32 11
  %64 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call58 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes57, i32 %64)
  %m_quantizedAabbMin59 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call58, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin59, i32 0, i32 2
  store i16 %call56, i16* %arrayidx60, align 4, !tbaa !38
  %m_quantizedContiguousNodes61 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %65 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call62 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes61, i32 %65)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call62, i32 0, i32 1
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %66 = load i16, i16* %arrayidx63, align 2, !tbaa !38
  %call64 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %66)
  %67 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes65 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %67, i32 0, i32 11
  %68 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call66 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes65, i32 %68)
  %m_quantizedAabbMax67 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call66, i32 0, i32 1
  %arrayidx68 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax67, i32 0, i32 0
  store i16 %call64, i16* %arrayidx68, align 2, !tbaa !38
  %m_quantizedContiguousNodes69 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %69 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call70 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes69, i32 %69)
  %m_quantizedAabbMax71 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call70, i32 0, i32 1
  %arrayidx72 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax71, i32 0, i32 1
  %70 = load i16, i16* %arrayidx72, align 2, !tbaa !38
  %call73 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %70)
  %71 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes74 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %71, i32 0, i32 11
  %72 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call75 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes74, i32 %72)
  %m_quantizedAabbMax76 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call75, i32 0, i32 1
  %arrayidx77 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax76, i32 0, i32 1
  store i16 %call73, i16* %arrayidx77, align 2, !tbaa !38
  %m_quantizedContiguousNodes78 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %73 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call79 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes78, i32 %73)
  %m_quantizedAabbMax80 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call79, i32 0, i32 1
  %arrayidx81 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax80, i32 0, i32 2
  %74 = load i16, i16* %arrayidx81, align 2, !tbaa !38
  %call82 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %74)
  %75 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes83 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %75, i32 0, i32 11
  %76 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call84 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes83, i32 %76)
  %m_quantizedAabbMax85 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call84, i32 0, i32 1
  %arrayidx86 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax85, i32 0, i32 2
  store i16 %call82, i16* %arrayidx86, align 2, !tbaa !38
  %m_quantizedContiguousNodes87 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %77 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call88 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes87, i32 %77)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call88, i32 0, i32 2
  %78 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %call89 = call i32 @_Z12btSwapEndiani(i32 %78)
  %79 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes90 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %79, i32 0, i32 11
  %80 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call91 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes90, i32 %80)
  %m_escapeIndexOrTriangleIndex92 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call91, i32 0, i32 2
  store i32 %call89, i32* %m_escapeIndexOrTriangleIndex92, align 4, !tbaa !40
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %81 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %inc = add nsw i32 %81, 1
  store i32 %inc, i32* %nodeIndex, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end156

if.else93:                                        ; preds = %if.then33
  %82 = bitcast i32* %nodeIndex94 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #8
  store i32 0, i32* %nodeIndex94, align 4, !tbaa !25
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc153, %if.else93
  %83 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %84 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp96 = icmp slt i32 %83, %84
  br i1 %cmp96, label %for.body98, label %for.cond.cleanup97

for.cond.cleanup97:                               ; preds = %for.cond95
  %85 = bitcast i32* %nodeIndex94 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %85) #8
  br label %for.end155

for.body98:                                       ; preds = %for.cond95
  %m_quantizedContiguousNodes99 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %86 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call100 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes99, i32 %86)
  %m_quantizedAabbMin101 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call100, i32 0, i32 0
  %arrayidx102 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin101, i32 0, i32 0
  %87 = load i16, i16* %arrayidx102, align 4, !tbaa !38
  %88 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes103 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %88, i32 0, i32 11
  %89 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call104 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes103, i32 %89)
  %m_quantizedAabbMin105 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call104, i32 0, i32 0
  %arrayidx106 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin105, i32 0, i32 0
  store i16 %87, i16* %arrayidx106, align 4, !tbaa !38
  %m_quantizedContiguousNodes107 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %90 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call108 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes107, i32 %90)
  %m_quantizedAabbMin109 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call108, i32 0, i32 0
  %arrayidx110 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin109, i32 0, i32 1
  %91 = load i16, i16* %arrayidx110, align 2, !tbaa !38
  %92 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes111 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %92, i32 0, i32 11
  %93 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call112 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes111, i32 %93)
  %m_quantizedAabbMin113 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call112, i32 0, i32 0
  %arrayidx114 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin113, i32 0, i32 1
  store i16 %91, i16* %arrayidx114, align 2, !tbaa !38
  %m_quantizedContiguousNodes115 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %94 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call116 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes115, i32 %94)
  %m_quantizedAabbMin117 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call116, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin117, i32 0, i32 2
  %95 = load i16, i16* %arrayidx118, align 4, !tbaa !38
  %96 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes119 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %96, i32 0, i32 11
  %97 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call120 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes119, i32 %97)
  %m_quantizedAabbMin121 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call120, i32 0, i32 0
  %arrayidx122 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin121, i32 0, i32 2
  store i16 %95, i16* %arrayidx122, align 4, !tbaa !38
  %m_quantizedContiguousNodes123 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %98 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call124 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes123, i32 %98)
  %m_quantizedAabbMax125 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call124, i32 0, i32 1
  %arrayidx126 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax125, i32 0, i32 0
  %99 = load i16, i16* %arrayidx126, align 2, !tbaa !38
  %100 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes127 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %100, i32 0, i32 11
  %101 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call128 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes127, i32 %101)
  %m_quantizedAabbMax129 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call128, i32 0, i32 1
  %arrayidx130 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax129, i32 0, i32 0
  store i16 %99, i16* %arrayidx130, align 2, !tbaa !38
  %m_quantizedContiguousNodes131 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %102 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call132 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes131, i32 %102)
  %m_quantizedAabbMax133 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call132, i32 0, i32 1
  %arrayidx134 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax133, i32 0, i32 1
  %103 = load i16, i16* %arrayidx134, align 2, !tbaa !38
  %104 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes135 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %104, i32 0, i32 11
  %105 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call136 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes135, i32 %105)
  %m_quantizedAabbMax137 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call136, i32 0, i32 1
  %arrayidx138 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax137, i32 0, i32 1
  store i16 %103, i16* %arrayidx138, align 2, !tbaa !38
  %m_quantizedContiguousNodes139 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %106 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call140 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes139, i32 %106)
  %m_quantizedAabbMax141 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call140, i32 0, i32 1
  %arrayidx142 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax141, i32 0, i32 2
  %107 = load i16, i16* %arrayidx142, align 2, !tbaa !38
  %108 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes143 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %108, i32 0, i32 11
  %109 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call144 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes143, i32 %109)
  %m_quantizedAabbMax145 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call144, i32 0, i32 1
  %arrayidx146 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax145, i32 0, i32 2
  store i16 %107, i16* %arrayidx146, align 2, !tbaa !38
  %m_quantizedContiguousNodes147 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %110 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call148 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes147, i32 %110)
  %m_escapeIndexOrTriangleIndex149 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call148, i32 0, i32 2
  %111 = load i32, i32* %m_escapeIndexOrTriangleIndex149, align 4, !tbaa !40
  %112 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes150 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %112, i32 0, i32 11
  %113 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %call151 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes150, i32 %113)
  %m_escapeIndexOrTriangleIndex152 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call151, i32 0, i32 2
  store i32 %111, i32* %m_escapeIndexOrTriangleIndex152, align 4, !tbaa !40
  br label %for.inc153

for.inc153:                                       ; preds = %for.body98
  %114 = load i32, i32* %nodeIndex94, align 4, !tbaa !25
  %inc154 = add nsw i32 %114, 1
  store i32 %inc154, i32* %nodeIndex94, align 4, !tbaa !25
  br label %for.cond95

for.end155:                                       ; preds = %for.cond.cleanup97
  br label %if.end156

if.end156:                                        ; preds = %for.end155, %for.end
  %115 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %mul = mul i32 16, %115
  %116 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr157 = getelementptr inbounds i8, i8* %116, i32 %mul
  store i8* %add.ptr157, i8** %nodeData, align 4, !tbaa !2
  %117 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes158 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %117, i32 0, i32 11
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes158, i8* null, i32 0, i32 0)
  br label %if.end241

if.else159:                                       ; preds = %if.end
  %118 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %118, i32 0, i32 9
  %119 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %120 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %121 = load i32, i32* %nodeCount, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes, i8* %119, i32 %120, i32 %121)
  %122 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool160 = trunc i8 %122 to i1
  br i1 %tobool160, label %if.then161, label %if.else198

if.then161:                                       ; preds = %if.else159
  %123 = bitcast i32* %nodeIndex162 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #8
  store i32 0, i32* %nodeIndex162, align 4, !tbaa !25
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc195, %if.then161
  %124 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %125 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp164 = icmp slt i32 %124, %125
  br i1 %cmp164, label %for.body166, label %for.cond.cleanup165

for.cond.cleanup165:                              ; preds = %for.cond163
  %126 = bitcast i32* %nodeIndex162 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %126) #8
  br label %for.end197

for.body166:                                      ; preds = %for.cond163
  %m_contiguousNodes167 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %127 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call168 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes167, i32 %127)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call168, i32 0, i32 0
  %128 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes169 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %128, i32 0, i32 9
  %129 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call170 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes169, i32 %129)
  %m_aabbMinOrg171 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call170, i32 0, i32 0
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg171)
  %m_contiguousNodes172 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %130 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call173 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes172, i32 %130)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call173, i32 0, i32 1
  %131 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes174 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %131, i32 0, i32 9
  %132 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call175 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes174, i32 %132)
  %m_aabbMaxOrg176 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call175, i32 0, i32 1
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg176)
  %m_contiguousNodes177 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %133 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call178 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes177, i32 %133)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call178, i32 0, i32 2
  %134 = load i32, i32* %m_escapeIndex, align 4, !tbaa !45
  %call179 = call i32 @_Z12btSwapEndiani(i32 %134)
  %135 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes180 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %135, i32 0, i32 9
  %136 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call181 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes180, i32 %136)
  %m_escapeIndex182 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call181, i32 0, i32 2
  store i32 %call179, i32* %m_escapeIndex182, align 4, !tbaa !45
  %m_contiguousNodes183 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %137 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call184 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes183, i32 %137)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call184, i32 0, i32 3
  %138 = load i32, i32* %m_subPart, align 4, !tbaa !47
  %call185 = call i32 @_Z12btSwapEndiani(i32 %138)
  %139 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes186 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %139, i32 0, i32 9
  %140 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call187 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes186, i32 %140)
  %m_subPart188 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call187, i32 0, i32 3
  store i32 %call185, i32* %m_subPart188, align 4, !tbaa !47
  %m_contiguousNodes189 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %141 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call190 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes189, i32 %141)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call190, i32 0, i32 4
  %142 = load i32, i32* %m_triangleIndex, align 4, !tbaa !48
  %call191 = call i32 @_Z12btSwapEndiani(i32 %142)
  %143 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes192 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %143, i32 0, i32 9
  %144 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %call193 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes192, i32 %144)
  %m_triangleIndex194 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call193, i32 0, i32 4
  store i32 %call191, i32* %m_triangleIndex194, align 4, !tbaa !48
  br label %for.inc195

for.inc195:                                       ; preds = %for.body166
  %145 = load i32, i32* %nodeIndex162, align 4, !tbaa !25
  %inc196 = add nsw i32 %145, 1
  store i32 %inc196, i32* %nodeIndex162, align 4, !tbaa !25
  br label %for.cond163

for.end197:                                       ; preds = %for.cond.cleanup165
  br label %if.end237

if.else198:                                       ; preds = %if.else159
  %146 = bitcast i32* %nodeIndex199 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %146) #8
  store i32 0, i32* %nodeIndex199, align 4, !tbaa !25
  br label %for.cond200

for.cond200:                                      ; preds = %for.inc234, %if.else198
  %147 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %148 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp201 = icmp slt i32 %147, %148
  br i1 %cmp201, label %for.body203, label %for.cond.cleanup202

for.cond.cleanup202:                              ; preds = %for.cond200
  %149 = bitcast i32* %nodeIndex199 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #8
  br label %for.end236

for.body203:                                      ; preds = %for.cond200
  %m_contiguousNodes204 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %150 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call205 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes204, i32 %150)
  %m_aabbMinOrg206 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call205, i32 0, i32 0
  %151 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes207 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %151, i32 0, i32 9
  %152 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call208 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes207, i32 %152)
  %m_aabbMinOrg209 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call208, i32 0, i32 0
  %153 = bitcast %class.btVector3* %m_aabbMinOrg209 to i8*
  %154 = bitcast %class.btVector3* %m_aabbMinOrg206 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %153, i8* align 4 %154, i32 16, i1 false), !tbaa.struct !42
  %m_contiguousNodes210 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %155 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call211 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes210, i32 %155)
  %m_aabbMaxOrg212 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call211, i32 0, i32 1
  %156 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes213 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %156, i32 0, i32 9
  %157 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call214 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes213, i32 %157)
  %m_aabbMaxOrg215 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call214, i32 0, i32 1
  %158 = bitcast %class.btVector3* %m_aabbMaxOrg215 to i8*
  %159 = bitcast %class.btVector3* %m_aabbMaxOrg212 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %158, i8* align 4 %159, i32 16, i1 false), !tbaa.struct !42
  %m_contiguousNodes216 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %160 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call217 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes216, i32 %160)
  %m_escapeIndex218 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call217, i32 0, i32 2
  %161 = load i32, i32* %m_escapeIndex218, align 4, !tbaa !45
  %162 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes219 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %162, i32 0, i32 9
  %163 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call220 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes219, i32 %163)
  %m_escapeIndex221 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call220, i32 0, i32 2
  store i32 %161, i32* %m_escapeIndex221, align 4, !tbaa !45
  %m_contiguousNodes222 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %164 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call223 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes222, i32 %164)
  %m_subPart224 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call223, i32 0, i32 3
  %165 = load i32, i32* %m_subPart224, align 4, !tbaa !47
  %166 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes225 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %166, i32 0, i32 9
  %167 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call226 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes225, i32 %167)
  %m_subPart227 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call226, i32 0, i32 3
  store i32 %165, i32* %m_subPart227, align 4, !tbaa !47
  %m_contiguousNodes228 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %168 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call229 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes228, i32 %168)
  %m_triangleIndex230 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call229, i32 0, i32 4
  %169 = load i32, i32* %m_triangleIndex230, align 4, !tbaa !48
  %170 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes231 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %170, i32 0, i32 9
  %171 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %call232 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes231, i32 %171)
  %m_triangleIndex233 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call232, i32 0, i32 4
  store i32 %169, i32* %m_triangleIndex233, align 4, !tbaa !48
  br label %for.inc234

for.inc234:                                       ; preds = %for.body203
  %172 = load i32, i32* %nodeIndex199, align 4, !tbaa !25
  %inc235 = add nsw i32 %172, 1
  store i32 %inc235, i32* %nodeIndex199, align 4, !tbaa !25
  br label %for.cond200

for.end236:                                       ; preds = %for.cond.cleanup202
  br label %if.end237

if.end237:                                        ; preds = %for.end236, %for.end197
  %173 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %mul238 = mul i32 64, %173
  %174 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr239 = getelementptr inbounds i8, i8* %174, i32 %mul238
  store i8* %add.ptr239, i8** %nodeData, align 4, !tbaa !2
  %175 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_contiguousNodes240 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %175, i32 0, i32 9
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes240, i8* null, i32 0, i32 0)
  br label %if.end241

if.end241:                                        ; preds = %if.end237, %if.end156
  store i32 0, i32* %sizeToAdd, align 4, !tbaa !25
  %176 = load i32, i32* %sizeToAdd, align 4, !tbaa !25
  %177 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr242 = getelementptr inbounds i8, i8* %177, i32 %176
  store i8* %add.ptr242, i8** %nodeData, align 4, !tbaa !2
  %178 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders243 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %178, i32 0, i32 13
  %179 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %m_subtreeHeaderCount244 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %180 = load i32, i32* %m_subtreeHeaderCount244, align 4, !tbaa !22
  %m_subtreeHeaderCount245 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %181 = load i32, i32* %m_subtreeHeaderCount245, align 4, !tbaa !22
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders243, i8* %179, i32 %180, i32 %181)
  %182 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool246 = trunc i8 %182 to i1
  br i1 %tobool246, label %if.then247, label %if.else322

if.then247:                                       ; preds = %if.end241
  %183 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %183) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond248

for.cond248:                                      ; preds = %for.inc319, %if.then247
  %184 = load i32, i32* %i, align 4, !tbaa !25
  %m_subtreeHeaderCount249 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %185 = load i32, i32* %m_subtreeHeaderCount249, align 4, !tbaa !22
  %cmp250 = icmp slt i32 %184, %185
  br i1 %cmp250, label %for.body252, label %for.cond.cleanup251

for.cond.cleanup251:                              ; preds = %for.cond248
  %186 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #8
  br label %for.end321

for.body252:                                      ; preds = %for.cond248
  %m_SubtreeHeaders253 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %187 = load i32, i32* %i, align 4, !tbaa !25
  %call254 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders253, i32 %187)
  %m_quantizedAabbMin255 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call254, i32 0, i32 0
  %arrayidx256 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin255, i32 0, i32 0
  %188 = load i16, i16* %arrayidx256, align 4, !tbaa !38
  %call257 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %188)
  %189 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders258 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %189, i32 0, i32 13
  %190 = load i32, i32* %i, align 4, !tbaa !25
  %call259 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders258, i32 %190)
  %m_quantizedAabbMin260 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call259, i32 0, i32 0
  %arrayidx261 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin260, i32 0, i32 0
  store i16 %call257, i16* %arrayidx261, align 4, !tbaa !38
  %m_SubtreeHeaders262 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %191 = load i32, i32* %i, align 4, !tbaa !25
  %call263 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders262, i32 %191)
  %m_quantizedAabbMin264 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call263, i32 0, i32 0
  %arrayidx265 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin264, i32 0, i32 1
  %192 = load i16, i16* %arrayidx265, align 2, !tbaa !38
  %call266 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %192)
  %193 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders267 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %193, i32 0, i32 13
  %194 = load i32, i32* %i, align 4, !tbaa !25
  %call268 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders267, i32 %194)
  %m_quantizedAabbMin269 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call268, i32 0, i32 0
  %arrayidx270 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin269, i32 0, i32 1
  store i16 %call266, i16* %arrayidx270, align 2, !tbaa !38
  %m_SubtreeHeaders271 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %195 = load i32, i32* %i, align 4, !tbaa !25
  %call272 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders271, i32 %195)
  %m_quantizedAabbMin273 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call272, i32 0, i32 0
  %arrayidx274 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin273, i32 0, i32 2
  %196 = load i16, i16* %arrayidx274, align 4, !tbaa !38
  %call275 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %196)
  %197 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders276 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %197, i32 0, i32 13
  %198 = load i32, i32* %i, align 4, !tbaa !25
  %call277 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders276, i32 %198)
  %m_quantizedAabbMin278 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call277, i32 0, i32 0
  %arrayidx279 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin278, i32 0, i32 2
  store i16 %call275, i16* %arrayidx279, align 4, !tbaa !38
  %m_SubtreeHeaders280 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %199 = load i32, i32* %i, align 4, !tbaa !25
  %call281 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders280, i32 %199)
  %m_quantizedAabbMax282 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call281, i32 0, i32 1
  %arrayidx283 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax282, i32 0, i32 0
  %200 = load i16, i16* %arrayidx283, align 2, !tbaa !38
  %call284 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %200)
  %201 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders285 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %201, i32 0, i32 13
  %202 = load i32, i32* %i, align 4, !tbaa !25
  %call286 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders285, i32 %202)
  %m_quantizedAabbMax287 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call286, i32 0, i32 1
  %arrayidx288 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax287, i32 0, i32 0
  store i16 %call284, i16* %arrayidx288, align 2, !tbaa !38
  %m_SubtreeHeaders289 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %203 = load i32, i32* %i, align 4, !tbaa !25
  %call290 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders289, i32 %203)
  %m_quantizedAabbMax291 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call290, i32 0, i32 1
  %arrayidx292 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax291, i32 0, i32 1
  %204 = load i16, i16* %arrayidx292, align 2, !tbaa !38
  %call293 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %204)
  %205 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders294 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %205, i32 0, i32 13
  %206 = load i32, i32* %i, align 4, !tbaa !25
  %call295 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders294, i32 %206)
  %m_quantizedAabbMax296 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call295, i32 0, i32 1
  %arrayidx297 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax296, i32 0, i32 1
  store i16 %call293, i16* %arrayidx297, align 2, !tbaa !38
  %m_SubtreeHeaders298 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %207 = load i32, i32* %i, align 4, !tbaa !25
  %call299 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders298, i32 %207)
  %m_quantizedAabbMax300 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call299, i32 0, i32 1
  %arrayidx301 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax300, i32 0, i32 2
  %208 = load i16, i16* %arrayidx301, align 2, !tbaa !38
  %call302 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %208)
  %209 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders303 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %209, i32 0, i32 13
  %210 = load i32, i32* %i, align 4, !tbaa !25
  %call304 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders303, i32 %210)
  %m_quantizedAabbMax305 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call304, i32 0, i32 1
  %arrayidx306 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax305, i32 0, i32 2
  store i16 %call302, i16* %arrayidx306, align 2, !tbaa !38
  %m_SubtreeHeaders307 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %211 = load i32, i32* %i, align 4, !tbaa !25
  %call308 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders307, i32 %211)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call308, i32 0, i32 2
  %212 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %call309 = call i32 @_Z12btSwapEndiani(i32 %212)
  %213 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders310 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %213, i32 0, i32 13
  %214 = load i32, i32* %i, align 4, !tbaa !25
  %call311 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders310, i32 %214)
  %m_rootNodeIndex312 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call311, i32 0, i32 2
  store i32 %call309, i32* %m_rootNodeIndex312, align 4, !tbaa !28
  %m_SubtreeHeaders313 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %215 = load i32, i32* %i, align 4, !tbaa !25
  %call314 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders313, i32 %215)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call314, i32 0, i32 3
  %216 = load i32, i32* %m_subtreeSize, align 4, !tbaa !30
  %call315 = call i32 @_Z12btSwapEndiani(i32 %216)
  %217 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders316 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %217, i32 0, i32 13
  %218 = load i32, i32* %i, align 4, !tbaa !25
  %call317 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders316, i32 %218)
  %m_subtreeSize318 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call317, i32 0, i32 3
  store i32 %call315, i32* %m_subtreeSize318, align 4, !tbaa !30
  br label %for.inc319

for.inc319:                                       ; preds = %for.body252
  %219 = load i32, i32* %i, align 4, !tbaa !25
  %inc320 = add nsw i32 %219, 1
  store i32 %inc320, i32* %i, align 4, !tbaa !25
  br label %for.cond248

for.end321:                                       ; preds = %for.cond.cleanup251
  br label %if.end403

if.else322:                                       ; preds = %if.end241
  %220 = bitcast i32* %i323 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %220) #8
  store i32 0, i32* %i323, align 4, !tbaa !25
  br label %for.cond324

for.cond324:                                      ; preds = %for.inc400, %if.else322
  %221 = load i32, i32* %i323, align 4, !tbaa !25
  %m_subtreeHeaderCount325 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %222 = load i32, i32* %m_subtreeHeaderCount325, align 4, !tbaa !22
  %cmp326 = icmp slt i32 %221, %222
  br i1 %cmp326, label %for.body328, label %for.cond.cleanup327

for.cond.cleanup327:                              ; preds = %for.cond324
  %223 = bitcast i32* %i323 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #8
  br label %for.end402

for.body328:                                      ; preds = %for.cond324
  %m_SubtreeHeaders329 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %224 = load i32, i32* %i323, align 4, !tbaa !25
  %call330 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders329, i32 %224)
  %m_quantizedAabbMin331 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call330, i32 0, i32 0
  %arrayidx332 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin331, i32 0, i32 0
  %225 = load i16, i16* %arrayidx332, align 4, !tbaa !38
  %226 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders333 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %226, i32 0, i32 13
  %227 = load i32, i32* %i323, align 4, !tbaa !25
  %call334 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders333, i32 %227)
  %m_quantizedAabbMin335 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call334, i32 0, i32 0
  %arrayidx336 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin335, i32 0, i32 0
  store i16 %225, i16* %arrayidx336, align 4, !tbaa !38
  %m_SubtreeHeaders337 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %228 = load i32, i32* %i323, align 4, !tbaa !25
  %call338 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders337, i32 %228)
  %m_quantizedAabbMin339 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call338, i32 0, i32 0
  %arrayidx340 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin339, i32 0, i32 1
  %229 = load i16, i16* %arrayidx340, align 2, !tbaa !38
  %230 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders341 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %230, i32 0, i32 13
  %231 = load i32, i32* %i323, align 4, !tbaa !25
  %call342 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders341, i32 %231)
  %m_quantizedAabbMin343 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call342, i32 0, i32 0
  %arrayidx344 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin343, i32 0, i32 1
  store i16 %229, i16* %arrayidx344, align 2, !tbaa !38
  %m_SubtreeHeaders345 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %232 = load i32, i32* %i323, align 4, !tbaa !25
  %call346 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders345, i32 %232)
  %m_quantizedAabbMin347 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call346, i32 0, i32 0
  %arrayidx348 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin347, i32 0, i32 2
  %233 = load i16, i16* %arrayidx348, align 4, !tbaa !38
  %234 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders349 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %234, i32 0, i32 13
  %235 = load i32, i32* %i323, align 4, !tbaa !25
  %call350 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders349, i32 %235)
  %m_quantizedAabbMin351 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call350, i32 0, i32 0
  %arrayidx352 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin351, i32 0, i32 2
  store i16 %233, i16* %arrayidx352, align 4, !tbaa !38
  %m_SubtreeHeaders353 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %236 = load i32, i32* %i323, align 4, !tbaa !25
  %call354 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders353, i32 %236)
  %m_quantizedAabbMax355 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call354, i32 0, i32 1
  %arrayidx356 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax355, i32 0, i32 0
  %237 = load i16, i16* %arrayidx356, align 2, !tbaa !38
  %238 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders357 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %238, i32 0, i32 13
  %239 = load i32, i32* %i323, align 4, !tbaa !25
  %call358 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders357, i32 %239)
  %m_quantizedAabbMax359 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call358, i32 0, i32 1
  %arrayidx360 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax359, i32 0, i32 0
  store i16 %237, i16* %arrayidx360, align 2, !tbaa !38
  %m_SubtreeHeaders361 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %240 = load i32, i32* %i323, align 4, !tbaa !25
  %call362 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders361, i32 %240)
  %m_quantizedAabbMax363 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call362, i32 0, i32 1
  %arrayidx364 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax363, i32 0, i32 1
  %241 = load i16, i16* %arrayidx364, align 2, !tbaa !38
  %242 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders365 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %242, i32 0, i32 13
  %243 = load i32, i32* %i323, align 4, !tbaa !25
  %call366 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders365, i32 %243)
  %m_quantizedAabbMax367 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call366, i32 0, i32 1
  %arrayidx368 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax367, i32 0, i32 1
  store i16 %241, i16* %arrayidx368, align 2, !tbaa !38
  %m_SubtreeHeaders369 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %244 = load i32, i32* %i323, align 4, !tbaa !25
  %call370 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders369, i32 %244)
  %m_quantizedAabbMax371 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call370, i32 0, i32 1
  %arrayidx372 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax371, i32 0, i32 2
  %245 = load i16, i16* %arrayidx372, align 2, !tbaa !38
  %246 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders373 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %246, i32 0, i32 13
  %247 = load i32, i32* %i323, align 4, !tbaa !25
  %call374 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders373, i32 %247)
  %m_quantizedAabbMax375 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call374, i32 0, i32 1
  %arrayidx376 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax375, i32 0, i32 2
  store i16 %245, i16* %arrayidx376, align 2, !tbaa !38
  %m_SubtreeHeaders377 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %248 = load i32, i32* %i323, align 4, !tbaa !25
  %call378 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders377, i32 %248)
  %m_rootNodeIndex379 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call378, i32 0, i32 2
  %249 = load i32, i32* %m_rootNodeIndex379, align 4, !tbaa !28
  %250 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders380 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %250, i32 0, i32 13
  %251 = load i32, i32* %i323, align 4, !tbaa !25
  %call381 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders380, i32 %251)
  %m_rootNodeIndex382 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call381, i32 0, i32 2
  store i32 %249, i32* %m_rootNodeIndex382, align 4, !tbaa !28
  %m_SubtreeHeaders383 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %252 = load i32, i32* %i323, align 4, !tbaa !25
  %call384 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders383, i32 %252)
  %m_subtreeSize385 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call384, i32 0, i32 3
  %253 = load i32, i32* %m_subtreeSize385, align 4, !tbaa !30
  %254 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders386 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %254, i32 0, i32 13
  %255 = load i32, i32* %i323, align 4, !tbaa !25
  %call387 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders386, i32 %255)
  %m_subtreeSize388 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call387, i32 0, i32 3
  store i32 %253, i32* %m_subtreeSize388, align 4, !tbaa !30
  %256 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders389 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %256, i32 0, i32 13
  %257 = load i32, i32* %i323, align 4, !tbaa !25
  %call390 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders389, i32 %257)
  %m_padding = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call390, i32 0, i32 4
  %arrayidx391 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding, i32 0, i32 0
  store i32 0, i32* %arrayidx391, align 4, !tbaa !25
  %258 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders392 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %258, i32 0, i32 13
  %259 = load i32, i32* %i323, align 4, !tbaa !25
  %call393 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders392, i32 %259)
  %m_padding394 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call393, i32 0, i32 4
  %arrayidx395 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding394, i32 0, i32 1
  store i32 0, i32* %arrayidx395, align 4, !tbaa !25
  %260 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders396 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %260, i32 0, i32 13
  %261 = load i32, i32* %i323, align 4, !tbaa !25
  %call397 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders396, i32 %261)
  %m_padding398 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call397, i32 0, i32 4
  %arrayidx399 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding398, i32 0, i32 2
  store i32 0, i32* %arrayidx399, align 4, !tbaa !25
  br label %for.inc400

for.inc400:                                       ; preds = %for.body328
  %262 = load i32, i32* %i323, align 4, !tbaa !25
  %inc401 = add nsw i32 %262, 1
  store i32 %inc401, i32* %i323, align 4, !tbaa !25
  br label %for.cond324

for.end402:                                       ; preds = %for.cond.cleanup327
  br label %if.end403

if.end403:                                        ; preds = %for.end402, %for.end321
  %m_subtreeHeaderCount404 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %263 = load i32, i32* %m_subtreeHeaderCount404, align 4, !tbaa !22
  %mul405 = mul i32 32, %263
  %264 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr406 = getelementptr inbounds i8, i8* %264, i32 %mul405
  store i8* %add.ptr406, i8** %nodeData, align 4, !tbaa !2
  %265 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4, !tbaa !2
  %m_SubtreeHeaders407 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %265, i32 0, i32 13
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders407, i8* null, i32 0, i32 0)
  %266 = load i8*, i8** %o_alignedDataBuffer.addr, align 4, !tbaa !2
  %267 = bitcast i8* %266 to i8**
  store i8* null, i8** %267, align 4, !tbaa !2
  %268 = bitcast i32* %nodeCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %268) #8
  %269 = bitcast i32* %sizeToAdd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %269) #8
  %270 = bitcast i8** %nodeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %270) #8
  %271 = bitcast %class.btQuantizedBvh** %targetBvh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %271) #8
  ret i1 true
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN14btQuantizedBvhnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !50
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i32 @_Z12btSwapEndiani(i32 %val) #3 comdat {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !25
  %0 = load i32, i32* %val.addr, align 4, !tbaa !25
  %call = call i32 @_Z12btSwapEndianj(i32 %0)
  ret i32 %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %sourceVec, %class.btVector3* nonnull align 4 dereferenceable(16) %destVec) #3 comdat {
entry:
  %sourceVec.addr = alloca %class.btVector3*, align 4
  %destVec.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %sourceVec, %class.btVector3** %sourceVec.addr, align 4, !tbaa !2
  store %class.btVector3* %destVec, %class.btVector3** %destVec.addr, align 4, !tbaa !2
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %sourceVec.addr, align 4, !tbaa !2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %4 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds float, float* %call, i32 %4
  %5 = load %class.btVector3*, %class.btVector3** %destVec.addr, align 4, !tbaa !2
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 %6
  call void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %this, i8* %buffer, i32 %size, i32 %capacity) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  store i32 %capacity, i32* %capacity.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4, !tbaa !52
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %1 = bitcast i8* %0 to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %1, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %2 = load i32, i32* %size.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4, !tbaa !31
  %3 = load i32, i32* %capacity.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4, !tbaa !53
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i16 @_Z12btSwapEndiant(i16 zeroext %val) #1 comdat {
entry:
  %val.addr = alloca i16, align 2
  store i16 %val, i16* %val.addr, align 2, !tbaa !38
  %0 = load i16, i16* %val.addr, align 2, !tbaa !38
  %conv = zext i16 %0 to i32
  %and = and i32 %conv, 65280
  %shr = ashr i32 %and, 8
  %1 = load i16, i16* %val.addr, align 2, !tbaa !38
  %conv1 = zext i16 %1 to i32
  %and2 = and i32 %conv1, 255
  %shl = shl i32 %and2, 8
  %or = or i32 %shr, %shl
  %conv3 = trunc i32 %or to i16
  ret i16 %conv3
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %this, i8* %buffer, i32 %size, i32 %capacity) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  store i32 %capacity, i32* %capacity.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4, !tbaa !54
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %1 = bitcast i8* %0 to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %1, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %2 = load i32, i32* %size.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4, !tbaa !55
  %3 = load i32, i32* %capacity.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4, !tbaa !56
  ret void
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %this, i8* %buffer, i32 %size, i32 %capacity) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i8* %buffer, i8** %buffer.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  store i32 %capacity, i32* %capacity.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4, !tbaa !57
  %0 = load i8*, i8** %buffer.addr, align 4, !tbaa !2
  %1 = bitcast i8* %0 to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %1, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %2 = load i32, i32* %size.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4, !tbaa !35
  %3 = load i32, i32* %capacity.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4, !tbaa !58
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8* %i_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) #0 {
entry:
  %retval = alloca %class.btQuantizedBvh*, align 4
  %i_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %bvh = alloca %class.btQuantizedBvh*, align 4
  %calculatedBufSize = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %nodeData = alloca i8*, align 4
  %sizeToAdd = alloca i32, align 4
  %nodeCount = alloca i32, align 4
  %nodeIndex = alloca i32, align 4
  %nodeIndex82 = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %i_alignedDataBuffer, i8** %i_alignedDataBuffer.addr, align 4, !tbaa !2
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4, !tbaa !25
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1, !tbaa !44
  %0 = load i8*, i8** %i_alignedDataBuffer.addr, align 4, !tbaa !2
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %class.btQuantizedBvh* null, %class.btQuantizedBvh** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %class.btQuantizedBvh** %bvh to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i8*, i8** %i_alignedDataBuffer.addr, align 4, !tbaa !2
  %3 = bitcast i8* %2 to %class.btQuantizedBvh*
  store %class.btQuantizedBvh* %3, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %4 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %if.then1, label %if.end7

if.then1:                                         ; preds = %if.end
  %5 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %5, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %call = call i32 @_Z12btSwapEndiani(i32 %6)
  %7 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %7, i32 0, i32 5
  store i32 %call, i32* %m_curNodeIndex2, align 4, !tbaa !27
  %8 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 1
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %9 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 2
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %10 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 3
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %11 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %11, i32 0, i32 12
  %12 = load i32, i32* %m_traversalMode, align 4, !tbaa !21
  %call3 = call i32 @_Z12btSwapEndiani(i32 %12)
  %13 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_traversalMode4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %13, i32 0, i32 12
  store i32 %call3, i32* %m_traversalMode4, align 4, !tbaa !21
  %14 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %14, i32 0, i32 14
  %15 = load i32, i32* %m_subtreeHeaderCount, align 4, !tbaa !22
  %call5 = call i32 @_Z12btSwapEndiani(i32 %15)
  %16 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_subtreeHeaderCount6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %16, i32 0, i32 14
  store i32 %call5, i32* %m_subtreeHeaderCount6, align 4, !tbaa !22
  br label %if.end7

if.end7:                                          ; preds = %if.then1, %if.end
  %17 = bitcast i32* %calculatedBufSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #8
  %18 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %call8 = call i32 @_ZNK14btQuantizedBvh28calculateSerializeBufferSizeEv(%class.btQuantizedBvh* %18)
  store i32 %call8, i32* %calculatedBufSize, align 4, !tbaa !25
  %19 = load i32, i32* %calculatedBufSize, align 4, !tbaa !25
  %20 = load i32, i32* %i_dataBufferSize.addr, align 4, !tbaa !25
  %cmp9 = icmp ugt i32 %19, %20
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store %class.btQuantizedBvh* null, %class.btQuantizedBvh** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end11:                                         ; preds = %if.end7
  %21 = bitcast i8** %nodeData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %23 = bitcast %class.btQuantizedBvh* %22 to i8*
  store i8* %23, i8** %nodeData, align 4, !tbaa !2
  %24 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr = getelementptr inbounds i8, i8* %24, i32 172
  store i8* %add.ptr, i8** %nodeData, align 4, !tbaa !2
  %25 = bitcast i32* %sizeToAdd to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #8
  store i32 0, i32* %sizeToAdd, align 4, !tbaa !25
  %26 = load i32, i32* %sizeToAdd, align 4, !tbaa !25
  %27 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr12 = getelementptr inbounds i8, i8* %27, i32 %26
  store i8* %add.ptr12, i8** %nodeData, align 4, !tbaa !2
  %28 = bitcast i32* %nodeCount to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #8
  %29 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_curNodeIndex13 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %29, i32 0, i32 5
  %30 = load i32, i32* %m_curNodeIndex13, align 4, !tbaa !27
  store i32 %30, i32* %nodeCount, align 4, !tbaa !25
  %31 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %32 = bitcast %class.btQuantizedBvh* %31 to i8*
  %call14 = call i8* @_ZN14btQuantizedBvhnwEmPv(i32 172, i8* %32)
  %33 = bitcast i8* %call14 to %class.btQuantizedBvh*
  %34 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %call15 = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1ERS_b(%class.btQuantizedBvh* %33, %class.btQuantizedBvh* nonnull align 4 dereferenceable(172) %34, i1 zeroext false)
  %35 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %35, i32 0, i32 6
  %36 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool16 = trunc i8 %36 to i1
  br i1 %tobool16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end11
  %37 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %37, i32 0, i32 11
  %38 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %39 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %40 = load i32, i32* %nodeCount, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i8* %38, i32 %39, i32 %40)
  %41 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool18 = trunc i8 %41 to i1
  br i1 %tobool18, label %if.then19, label %if.end78

if.then19:                                        ; preds = %if.then17
  %42 = bitcast i32* %nodeIndex to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #8
  store i32 0, i32* %nodeIndex, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then19
  %43 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %44 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp20 = icmp slt i32 %43, %44
  br i1 %cmp20, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  %45 = bitcast i32* %nodeIndex to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %45) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %46 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %46, i32 0, i32 11
  %47 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes21, i32 %47)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call22, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %48 = load i16, i16* %arrayidx, align 4, !tbaa !38
  %call23 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %48)
  %49 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes24 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %49, i32 0, i32 11
  %50 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call25 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes24, i32 %50)
  %m_quantizedAabbMin26 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call25, i32 0, i32 0
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin26, i32 0, i32 0
  store i16 %call23, i16* %arrayidx27, align 4, !tbaa !38
  %51 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes28 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %51, i32 0, i32 11
  %52 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call29 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes28, i32 %52)
  %m_quantizedAabbMin30 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call29, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin30, i32 0, i32 1
  %53 = load i16, i16* %arrayidx31, align 2, !tbaa !38
  %call32 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %53)
  %54 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes33 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %54, i32 0, i32 11
  %55 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call34 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes33, i32 %55)
  %m_quantizedAabbMin35 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call34, i32 0, i32 0
  %arrayidx36 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin35, i32 0, i32 1
  store i16 %call32, i16* %arrayidx36, align 2, !tbaa !38
  %56 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes37 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %56, i32 0, i32 11
  %57 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call38 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes37, i32 %57)
  %m_quantizedAabbMin39 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call38, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin39, i32 0, i32 2
  %58 = load i16, i16* %arrayidx40, align 4, !tbaa !38
  %call41 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %58)
  %59 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes42 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %59, i32 0, i32 11
  %60 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call43 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes42, i32 %60)
  %m_quantizedAabbMin44 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call43, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin44, i32 0, i32 2
  store i16 %call41, i16* %arrayidx45, align 4, !tbaa !38
  %61 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes46 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %61, i32 0, i32 11
  %62 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call47 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes46, i32 %62)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call47, i32 0, i32 1
  %arrayidx48 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %63 = load i16, i16* %arrayidx48, align 2, !tbaa !38
  %call49 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %63)
  %64 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes50 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %64, i32 0, i32 11
  %65 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call51 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes50, i32 %65)
  %m_quantizedAabbMax52 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call51, i32 0, i32 1
  %arrayidx53 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax52, i32 0, i32 0
  store i16 %call49, i16* %arrayidx53, align 2, !tbaa !38
  %66 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes54 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %66, i32 0, i32 11
  %67 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call55 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes54, i32 %67)
  %m_quantizedAabbMax56 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call55, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax56, i32 0, i32 1
  %68 = load i16, i16* %arrayidx57, align 2, !tbaa !38
  %call58 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %68)
  %69 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes59 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %69, i32 0, i32 11
  %70 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call60 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes59, i32 %70)
  %m_quantizedAabbMax61 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call60, i32 0, i32 1
  %arrayidx62 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax61, i32 0, i32 1
  store i16 %call58, i16* %arrayidx62, align 2, !tbaa !38
  %71 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes63 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %71, i32 0, i32 11
  %72 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call64 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes63, i32 %72)
  %m_quantizedAabbMax65 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call64, i32 0, i32 1
  %arrayidx66 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax65, i32 0, i32 2
  %73 = load i16, i16* %arrayidx66, align 2, !tbaa !38
  %call67 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %73)
  %74 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes68 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %74, i32 0, i32 11
  %75 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call69 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes68, i32 %75)
  %m_quantizedAabbMax70 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call69, i32 0, i32 1
  %arrayidx71 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax70, i32 0, i32 2
  store i16 %call67, i16* %arrayidx71, align 2, !tbaa !38
  %76 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes72 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %76, i32 0, i32 11
  %77 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call73 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes72, i32 %77)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call73, i32 0, i32 2
  %78 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %call74 = call i32 @_Z12btSwapEndiani(i32 %78)
  %79 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_quantizedContiguousNodes75 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %79, i32 0, i32 11
  %80 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %call76 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes75, i32 %80)
  %m_escapeIndexOrTriangleIndex77 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call76, i32 0, i32 2
  store i32 %call74, i32* %m_escapeIndexOrTriangleIndex77, align 4, !tbaa !40
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %81 = load i32, i32* %nodeIndex, align 4, !tbaa !25
  %inc = add nsw i32 %81, 1
  store i32 %inc, i32* %nodeIndex, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end78

if.end78:                                         ; preds = %for.end, %if.then17
  %82 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %mul = mul i32 16, %82
  %83 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr79 = getelementptr inbounds i8, i8* %83, i32 %mul
  store i8* %add.ptr79, i8** %nodeData, align 4, !tbaa !2
  br label %if.end115

if.else:                                          ; preds = %if.end11
  %84 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %84, i32 0, i32 9
  %85 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %86 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %87 = load i32, i32* %nodeCount, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes, i8* %85, i32 %86, i32 %87)
  %88 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool80 = trunc i8 %88 to i1
  br i1 %tobool80, label %if.then81, label %if.end112

if.then81:                                        ; preds = %if.else
  %89 = bitcast i32* %nodeIndex82 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #8
  store i32 0, i32* %nodeIndex82, align 4, !tbaa !25
  br label %for.cond83

for.cond83:                                       ; preds = %for.inc109, %if.then81
  %90 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %91 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %cmp84 = icmp slt i32 %90, %91
  br i1 %cmp84, label %for.body86, label %for.cond.cleanup85

for.cond.cleanup85:                               ; preds = %for.cond83
  store i32 5, i32* %cleanup.dest.slot, align 4
  %92 = bitcast i32* %nodeIndex82 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  br label %for.end111

for.body86:                                       ; preds = %for.cond83
  %93 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes87 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %93, i32 0, i32 9
  %94 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call88 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes87, i32 %94)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call88, i32 0, i32 0
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg)
  %95 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes89 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %95, i32 0, i32 9
  %96 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call90 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes89, i32 %96)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call90, i32 0, i32 1
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg)
  %97 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes91 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %97, i32 0, i32 9
  %98 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call92 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes91, i32 %98)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call92, i32 0, i32 2
  %99 = load i32, i32* %m_escapeIndex, align 4, !tbaa !45
  %call93 = call i32 @_Z12btSwapEndiani(i32 %99)
  %100 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes94 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %100, i32 0, i32 9
  %101 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call95 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes94, i32 %101)
  %m_escapeIndex96 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call95, i32 0, i32 2
  store i32 %call93, i32* %m_escapeIndex96, align 4, !tbaa !45
  %102 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes97 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %102, i32 0, i32 9
  %103 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call98 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes97, i32 %103)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call98, i32 0, i32 3
  %104 = load i32, i32* %m_subPart, align 4, !tbaa !47
  %call99 = call i32 @_Z12btSwapEndiani(i32 %104)
  %105 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes100 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %105, i32 0, i32 9
  %106 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call101 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes100, i32 %106)
  %m_subPart102 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call101, i32 0, i32 3
  store i32 %call99, i32* %m_subPart102, align 4, !tbaa !47
  %107 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes103 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %107, i32 0, i32 9
  %108 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call104 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes103, i32 %108)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call104, i32 0, i32 4
  %109 = load i32, i32* %m_triangleIndex, align 4, !tbaa !48
  %call105 = call i32 @_Z12btSwapEndiani(i32 %109)
  %110 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_contiguousNodes106 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %110, i32 0, i32 9
  %111 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %call107 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes106, i32 %111)
  %m_triangleIndex108 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call107, i32 0, i32 4
  store i32 %call105, i32* %m_triangleIndex108, align 4, !tbaa !48
  br label %for.inc109

for.inc109:                                       ; preds = %for.body86
  %112 = load i32, i32* %nodeIndex82, align 4, !tbaa !25
  %inc110 = add nsw i32 %112, 1
  store i32 %inc110, i32* %nodeIndex82, align 4, !tbaa !25
  br label %for.cond83

for.end111:                                       ; preds = %for.cond.cleanup85
  br label %if.end112

if.end112:                                        ; preds = %for.end111, %if.else
  %113 = load i32, i32* %nodeCount, align 4, !tbaa !25
  %mul113 = mul i32 64, %113
  %114 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr114 = getelementptr inbounds i8, i8* %114, i32 %mul113
  store i8* %add.ptr114, i8** %nodeData, align 4, !tbaa !2
  br label %if.end115

if.end115:                                        ; preds = %if.end112, %if.end78
  store i32 0, i32* %sizeToAdd, align 4, !tbaa !25
  %115 = load i32, i32* %sizeToAdd, align 4, !tbaa !25
  %116 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %add.ptr116 = getelementptr inbounds i8, i8* %116, i32 %115
  store i8* %add.ptr116, i8** %nodeData, align 4, !tbaa !2
  %117 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %117, i32 0, i32 13
  %118 = load i8*, i8** %nodeData, align 4, !tbaa !2
  %119 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_subtreeHeaderCount117 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %119, i32 0, i32 14
  %120 = load i32, i32* %m_subtreeHeaderCount117, align 4, !tbaa !22
  %121 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_subtreeHeaderCount118 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %121, i32 0, i32 14
  %122 = load i32, i32* %m_subtreeHeaderCount118, align 4, !tbaa !22
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i8* %118, i32 %120, i32 %122)
  %123 = load i8, i8* %i_swapEndian.addr, align 1, !tbaa !44, !range !26
  %tobool119 = trunc i8 %123 to i1
  br i1 %tobool119, label %if.then120, label %if.end195

if.then120:                                       ; preds = %if.end115
  %124 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc192, %if.then120
  %125 = load i32, i32* %i, align 4, !tbaa !25
  %126 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_subtreeHeaderCount122 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %126, i32 0, i32 14
  %127 = load i32, i32* %m_subtreeHeaderCount122, align 4, !tbaa !22
  %cmp123 = icmp slt i32 %125, %127
  br i1 %cmp123, label %for.body125, label %for.cond.cleanup124

for.cond.cleanup124:                              ; preds = %for.cond121
  store i32 8, i32* %cleanup.dest.slot, align 4
  %128 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %128) #8
  br label %for.end194

for.body125:                                      ; preds = %for.cond121
  %129 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders126 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %129, i32 0, i32 13
  %130 = load i32, i32* %i, align 4, !tbaa !25
  %call127 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders126, i32 %130)
  %m_quantizedAabbMin128 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call127, i32 0, i32 0
  %arrayidx129 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin128, i32 0, i32 0
  %131 = load i16, i16* %arrayidx129, align 4, !tbaa !38
  %call130 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %131)
  %132 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders131 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %132, i32 0, i32 13
  %133 = load i32, i32* %i, align 4, !tbaa !25
  %call132 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders131, i32 %133)
  %m_quantizedAabbMin133 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call132, i32 0, i32 0
  %arrayidx134 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin133, i32 0, i32 0
  store i16 %call130, i16* %arrayidx134, align 4, !tbaa !38
  %134 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders135 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %134, i32 0, i32 13
  %135 = load i32, i32* %i, align 4, !tbaa !25
  %call136 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders135, i32 %135)
  %m_quantizedAabbMin137 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call136, i32 0, i32 0
  %arrayidx138 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin137, i32 0, i32 1
  %136 = load i16, i16* %arrayidx138, align 2, !tbaa !38
  %call139 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %136)
  %137 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders140 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %137, i32 0, i32 13
  %138 = load i32, i32* %i, align 4, !tbaa !25
  %call141 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders140, i32 %138)
  %m_quantizedAabbMin142 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call141, i32 0, i32 0
  %arrayidx143 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin142, i32 0, i32 1
  store i16 %call139, i16* %arrayidx143, align 2, !tbaa !38
  %139 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders144 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %139, i32 0, i32 13
  %140 = load i32, i32* %i, align 4, !tbaa !25
  %call145 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders144, i32 %140)
  %m_quantizedAabbMin146 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call145, i32 0, i32 0
  %arrayidx147 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin146, i32 0, i32 2
  %141 = load i16, i16* %arrayidx147, align 4, !tbaa !38
  %call148 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %141)
  %142 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders149 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %142, i32 0, i32 13
  %143 = load i32, i32* %i, align 4, !tbaa !25
  %call150 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders149, i32 %143)
  %m_quantizedAabbMin151 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call150, i32 0, i32 0
  %arrayidx152 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin151, i32 0, i32 2
  store i16 %call148, i16* %arrayidx152, align 4, !tbaa !38
  %144 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders153 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %144, i32 0, i32 13
  %145 = load i32, i32* %i, align 4, !tbaa !25
  %call154 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders153, i32 %145)
  %m_quantizedAabbMax155 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call154, i32 0, i32 1
  %arrayidx156 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax155, i32 0, i32 0
  %146 = load i16, i16* %arrayidx156, align 2, !tbaa !38
  %call157 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %146)
  %147 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders158 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %147, i32 0, i32 13
  %148 = load i32, i32* %i, align 4, !tbaa !25
  %call159 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders158, i32 %148)
  %m_quantizedAabbMax160 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call159, i32 0, i32 1
  %arrayidx161 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax160, i32 0, i32 0
  store i16 %call157, i16* %arrayidx161, align 2, !tbaa !38
  %149 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders162 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %149, i32 0, i32 13
  %150 = load i32, i32* %i, align 4, !tbaa !25
  %call163 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders162, i32 %150)
  %m_quantizedAabbMax164 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call163, i32 0, i32 1
  %arrayidx165 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax164, i32 0, i32 1
  %151 = load i16, i16* %arrayidx165, align 2, !tbaa !38
  %call166 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %151)
  %152 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders167 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %152, i32 0, i32 13
  %153 = load i32, i32* %i, align 4, !tbaa !25
  %call168 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders167, i32 %153)
  %m_quantizedAabbMax169 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call168, i32 0, i32 1
  %arrayidx170 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax169, i32 0, i32 1
  store i16 %call166, i16* %arrayidx170, align 2, !tbaa !38
  %154 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders171 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %154, i32 0, i32 13
  %155 = load i32, i32* %i, align 4, !tbaa !25
  %call172 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders171, i32 %155)
  %m_quantizedAabbMax173 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call172, i32 0, i32 1
  %arrayidx174 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax173, i32 0, i32 2
  %156 = load i16, i16* %arrayidx174, align 2, !tbaa !38
  %call175 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %156)
  %157 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders176 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %157, i32 0, i32 13
  %158 = load i32, i32* %i, align 4, !tbaa !25
  %call177 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders176, i32 %158)
  %m_quantizedAabbMax178 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call177, i32 0, i32 1
  %arrayidx179 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax178, i32 0, i32 2
  store i16 %call175, i16* %arrayidx179, align 2, !tbaa !38
  %159 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders180 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %159, i32 0, i32 13
  %160 = load i32, i32* %i, align 4, !tbaa !25
  %call181 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders180, i32 %160)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call181, i32 0, i32 2
  %161 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %call182 = call i32 @_Z12btSwapEndiani(i32 %161)
  %162 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders183 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %162, i32 0, i32 13
  %163 = load i32, i32* %i, align 4, !tbaa !25
  %call184 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders183, i32 %163)
  %m_rootNodeIndex185 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call184, i32 0, i32 2
  store i32 %call182, i32* %m_rootNodeIndex185, align 4, !tbaa !28
  %164 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders186 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %164, i32 0, i32 13
  %165 = load i32, i32* %i, align 4, !tbaa !25
  %call187 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders186, i32 %165)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call187, i32 0, i32 3
  %166 = load i32, i32* %m_subtreeSize, align 4, !tbaa !30
  %call188 = call i32 @_Z12btSwapEndiani(i32 %166)
  %167 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  %m_SubtreeHeaders189 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %167, i32 0, i32 13
  %168 = load i32, i32* %i, align 4, !tbaa !25
  %call190 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders189, i32 %168)
  %m_subtreeSize191 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call190, i32 0, i32 3
  store i32 %call188, i32* %m_subtreeSize191, align 4, !tbaa !30
  br label %for.inc192

for.inc192:                                       ; preds = %for.body125
  %169 = load i32, i32* %i, align 4, !tbaa !25
  %inc193 = add nsw i32 %169, 1
  store i32 %inc193, i32* %i, align 4, !tbaa !25
  br label %for.cond121

for.end194:                                       ; preds = %for.cond.cleanup124
  br label %if.end195

if.end195:                                        ; preds = %for.end194, %if.end115
  %170 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4, !tbaa !2
  store %class.btQuantizedBvh* %170, %class.btQuantizedBvh** %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  %171 = bitcast i32* %nodeCount to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #8
  %172 = bitcast i32* %sizeToAdd to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #8
  %173 = bitcast i8** %nodeData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #8
  br label %cleanup

cleanup:                                          ; preds = %if.end195, %if.then10
  %174 = bitcast i32* %calculatedBufSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #8
  %175 = bitcast %class.btQuantizedBvh** %bvh to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #8
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %176 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %retval, align 4
  ret %class.btQuantizedBvh* %176
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %vector) #3 comdat {
entry:
  %vector.addr = alloca %class.btVector3*, align 4
  %swappedVec = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %vector, %class.btVector3** %vector.addr, align 4, !tbaa !2
  %0 = bitcast %class.btVector3* %swappedVec to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #8
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swappedVec)
  %1 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %vector.addr, align 4, !tbaa !2
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %4)
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds float, float* %call1, i32 %5
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %swappedVec)
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %6
  call void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %8 = load %class.btVector3*, %class.btVector3** %vector.addr, align 4, !tbaa !2
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %swappedVec to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !42
  %11 = bitcast %class.btVector3* %swappedVec to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %11) #8
  ret void
}

define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2ERS_b(%class.btQuantizedBvh* returned %this, %class.btQuantizedBvh* nonnull align 4 dereferenceable(172) %self, i1 zeroext %0) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %self.addr = alloca %class.btQuantizedBvh*, align 4
  %.addr = alloca i8, align 1
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %class.btQuantizedBvh* %self, %class.btQuantizedBvh** %self.addr, align 4, !tbaa !2
  %frombool = zext i1 %0 to i8
  store i8 %frombool, i8* %.addr, align 1, !tbaa !44
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %1 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %2 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4, !tbaa !2
  %m_bvhAabbMin2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %2, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_bvhAabbMin to i8*
  %4 = bitcast %class.btVector3* %m_bvhAabbMin2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !42
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %5 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4, !tbaa !2
  %m_bvhAabbMax3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %5, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_bvhAabbMax to i8*
  %7 = bitcast %class.btVector3* %m_bvhAabbMax3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !42
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %8 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4, !tbaa !2
  %m_bvhQuantization4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 3
  %9 = bitcast %class.btVector3* %m_bvhQuantization to i8*
  %10 = bitcast %class.btVector3* %m_bvhQuantization4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false), !tbaa.struct !42
  %m_bulletVersion = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 4
  store i32 282, i32* %m_bulletVersion, align 4, !tbaa !8
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_leafNodes)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_contiguousNodes)
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call6 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call8 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  ret %class.btQuantizedBvh* %this1
}

define hidden void @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhFloatData* nonnull align 4 dereferenceable(84) %quantizedBvhFloatData) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %quantizedBvhFloatData.addr = alloca %struct.btQuantizedBvhFloatData*, align 4
  %numElem = alloca i32, align 4
  %ref.tmp = alloca %struct.btOptimizedBvhNode, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeFloatData*, align 4
  %i = alloca i32, align 4
  %numElem23 = alloca i32, align 4
  %ref.tmp24 = alloca %struct.btQuantizedBvhNode, align 4
  %memPtr27 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i28 = alloca i32, align 4
  %numElem75 = alloca i32, align 4
  %ref.tmp76 = alloca %class.btBvhSubtreeInfo, align 4
  %memPtr80 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i81 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhFloatData* %quantizedBvhFloatData, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %0 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %0, i32 0, i32 1
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %1, i32 0, i32 0
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %2 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %2, i32 0, i32 2
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhQuantization, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhQuantization4)
  %3 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_curNodeIndex = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %3, i32 0, i32 3
  %4 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !59
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 %4, i32* %m_curNodeIndex5, align 4, !tbaa !27
  %5 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_useQuantization = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %5, i32 0, i32 4
  %6 = load i32, i32* %m_useQuantization, align 4, !tbaa !62
  %cmp = icmp ne i32 %6, 0
  %m_useQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %m_useQuantization6, align 4, !tbaa !20
  %7 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %8, i32 0, i32 5
  %9 = load i32, i32* %m_numContiguousLeafNodes, align 4, !tbaa !63
  store i32 %9, i32* %numElem, align 4, !tbaa !25
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %10 = load i32, i32* %numElem, align 4, !tbaa !25
  %11 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %11) #8
  %12 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 64, i1 false)
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %10, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp)
  %13 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %13) #8
  %14 = load i32, i32* %numElem, align 4, !tbaa !25
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = bitcast %struct.btOptimizedBvhNodeFloatData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %16, i32 0, i32 7
  %17 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr, align 4, !tbaa !64
  store %struct.btOptimizedBvhNodeFloatData* %17, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %19 = load i32, i32* %i, align 4, !tbaa !25
  %20 = load i32, i32* %numElem, align 4, !tbaa !25
  %cmp7 = icmp slt i32 %19, %20
  br i1 %cmp7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %22 = load i32, i32* %i, align 4, !tbaa !25
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes8, i32 %22)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call9, i32 0, i32 1
  %23 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_aabbMaxOrg10 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %23, i32 0, i32 1
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg10)
  %m_contiguousNodes11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %24 = load i32, i32* %i, align 4, !tbaa !25
  %call12 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes11, i32 %24)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call12, i32 0, i32 0
  %25 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_aabbMinOrg13 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %25, i32 0, i32 0
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMinOrg13)
  %26 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %26, i32 0, i32 2
  %27 = load i32, i32* %m_escapeIndex, align 4, !tbaa !65
  %m_contiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %28 = load i32, i32* %i, align 4, !tbaa !25
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes14, i32 %28)
  %m_escapeIndex16 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call15, i32 0, i32 2
  store i32 %27, i32* %m_escapeIndex16, align 4, !tbaa !45
  %29 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %29, i32 0, i32 3
  %30 = load i32, i32* %m_subPart, align 4, !tbaa !67
  %m_contiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %31 = load i32, i32* %i, align 4, !tbaa !25
  %call18 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes17, i32 %31)
  %m_subPart19 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call18, i32 0, i32 3
  store i32 %30, i32* %m_subPart19, align 4, !tbaa !47
  %32 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %32, i32 0, i32 4
  %33 = load i32, i32* %m_triangleIndex, align 4, !tbaa !68
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %34 = load i32, i32* %i, align 4, !tbaa !25
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %34)
  %m_triangleIndex22 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 4
  store i32 %33, i32* %m_triangleIndex22, align 4, !tbaa !48
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  %36 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %36, i32 1
  store %struct.btOptimizedBvhNodeFloatData* %incdec.ptr, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %37 = bitcast %struct.btOptimizedBvhNodeFloatData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %38 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %numElem23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %40, i32 0, i32 6
  %41 = load i32, i32* %m_numQuantizedContiguousNodes, align 4, !tbaa !69
  store i32 %41, i32* %numElem23, align 4, !tbaa !25
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %42 = load i32, i32* %numElem23, align 4, !tbaa !25
  %43 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %44, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %42, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %45 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = load i32, i32* %numElem23, align 4, !tbaa !25
  %tobool25 = icmp ne i32 %46, 0
  br i1 %tobool25, label %if.then26, label %if.end73

if.then26:                                        ; preds = %if.end
  %47 = bitcast %struct.btQuantizedBvhNodeData** %memPtr27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  %48 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %48, i32 0, i32 8
  %49 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4, !tbaa !70
  store %struct.btQuantizedBvhNodeData* %49, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %50 = bitcast i32* %i28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  store i32 0, i32* %i28, align 4, !tbaa !25
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc69, %if.then26
  %51 = load i32, i32* %i28, align 4, !tbaa !25
  %52 = load i32, i32* %numElem23, align 4, !tbaa !25
  %cmp30 = icmp slt i32 %51, %52
  br i1 %cmp30, label %for.body32, label %for.cond.cleanup31

for.cond.cleanup31:                               ; preds = %for.cond29
  %53 = bitcast i32* %i28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  br label %for.end72

for.body32:                                       ; preds = %for.cond29
  %54 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %54, i32 0, i32 2
  %55 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !71
  %m_quantizedContiguousNodes33 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %56 = load i32, i32* %i28, align 4, !tbaa !25
  %call34 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes33, i32 %56)
  %m_escapeIndexOrTriangleIndex35 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call34, i32 0, i32 2
  store i32 %55, i32* %m_escapeIndexOrTriangleIndex35, align 4, !tbaa !40
  %57 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %57, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %58 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %m_quantizedContiguousNodes36 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %59 = load i32, i32* %i28, align 4, !tbaa !25
  %call37 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes36, i32 %59)
  %m_quantizedAabbMax38 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call37, i32 0, i32 1
  %arrayidx39 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax38, i32 0, i32 0
  store i16 %58, i16* %arrayidx39, align 2, !tbaa !38
  %60 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax40 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %60, i32 0, i32 1
  %arrayidx41 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax40, i32 0, i32 1
  %61 = load i16, i16* %arrayidx41, align 2, !tbaa !38
  %m_quantizedContiguousNodes42 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %62 = load i32, i32* %i28, align 4, !tbaa !25
  %call43 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes42, i32 %62)
  %m_quantizedAabbMax44 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call43, i32 0, i32 1
  %arrayidx45 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax44, i32 0, i32 1
  store i16 %61, i16* %arrayidx45, align 2, !tbaa !38
  %63 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax46 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %63, i32 0, i32 1
  %arrayidx47 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax46, i32 0, i32 2
  %64 = load i16, i16* %arrayidx47, align 2, !tbaa !38
  %m_quantizedContiguousNodes48 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %65 = load i32, i32* %i28, align 4, !tbaa !25
  %call49 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes48, i32 %65)
  %m_quantizedAabbMax50 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call49, i32 0, i32 1
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax50, i32 0, i32 2
  store i16 %64, i16* %arrayidx51, align 2, !tbaa !38
  %66 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %66, i32 0, i32 0
  %arrayidx52 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %67 = load i16, i16* %arrayidx52, align 4, !tbaa !38
  %m_quantizedContiguousNodes53 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %68 = load i32, i32* %i28, align 4, !tbaa !25
  %call54 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes53, i32 %68)
  %m_quantizedAabbMin55 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call54, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin55, i32 0, i32 0
  store i16 %67, i16* %arrayidx56, align 4, !tbaa !38
  %69 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin57 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %69, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin57, i32 0, i32 1
  %70 = load i16, i16* %arrayidx58, align 2, !tbaa !38
  %m_quantizedContiguousNodes59 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %71 = load i32, i32* %i28, align 4, !tbaa !25
  %call60 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes59, i32 %71)
  %m_quantizedAabbMin61 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call60, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin61, i32 0, i32 1
  store i16 %70, i16* %arrayidx62, align 2, !tbaa !38
  %72 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin63 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %72, i32 0, i32 0
  %arrayidx64 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin63, i32 0, i32 2
  %73 = load i16, i16* %arrayidx64, align 4, !tbaa !38
  %m_quantizedContiguousNodes65 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %74 = load i32, i32* %i28, align 4, !tbaa !25
  %call66 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes65, i32 %74)
  %m_quantizedAabbMin67 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call66, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin67, i32 0, i32 2
  store i16 %73, i16* %arrayidx68, align 4, !tbaa !38
  br label %for.inc69

for.inc69:                                        ; preds = %for.body32
  %75 = load i32, i32* %i28, align 4, !tbaa !25
  %inc70 = add nsw i32 %75, 1
  store i32 %inc70, i32* %i28, align 4, !tbaa !25
  %76 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %incdec.ptr71 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %76, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr71, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  br label %for.cond29

for.end72:                                        ; preds = %for.cond.cleanup31
  %77 = bitcast %struct.btQuantizedBvhNodeData** %memPtr27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  br label %if.end73

if.end73:                                         ; preds = %for.end72, %if.end
  %78 = bitcast i32* %numElem23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_traversalMode = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %79, i32 0, i32 10
  %80 = load i32, i32* %m_traversalMode, align 4, !tbaa !73
  %m_traversalMode74 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 %80, i32* %m_traversalMode74, align 4, !tbaa !21
  %81 = bitcast i32* %numElem75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #8
  %82 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %82, i32 0, i32 11
  %83 = load i32, i32* %m_numSubtreeHeaders, align 4, !tbaa !74
  store i32 %83, i32* %numElem75, align 4, !tbaa !25
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %84 = load i32, i32* %numElem75, align 4, !tbaa !25
  %85 = bitcast %class.btBvhSubtreeInfo* %ref.tmp76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %85) #8
  %call77 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp76)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i32 %84, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp76)
  %86 = bitcast %class.btBvhSubtreeInfo* %ref.tmp76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %86) #8
  %87 = load i32, i32* %numElem75, align 4, !tbaa !25
  %tobool78 = icmp ne i32 %87, 0
  br i1 %tobool78, label %if.then79, label %if.end132

if.then79:                                        ; preds = %if.end73
  %88 = bitcast %struct.btBvhSubtreeInfoData** %memPtr80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  %89 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4, !tbaa !2
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %89, i32 0, i32 9
  %90 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 4, !tbaa !75
  store %struct.btBvhSubtreeInfoData* %90, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %91 = bitcast i32* %i81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #8
  store i32 0, i32* %i81, align 4, !tbaa !25
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc128, %if.then79
  %92 = load i32, i32* %i81, align 4, !tbaa !25
  %93 = load i32, i32* %numElem75, align 4, !tbaa !25
  %cmp83 = icmp slt i32 %92, %93
  br i1 %cmp83, label %for.body85, label %for.cond.cleanup84

for.cond.cleanup84:                               ; preds = %for.cond82
  %94 = bitcast i32* %i81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  br label %for.end131

for.body85:                                       ; preds = %for.cond82
  %95 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax86 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %95, i32 0, i32 3
  %arrayidx87 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax86, i32 0, i32 0
  %96 = load i16, i16* %arrayidx87, align 2, !tbaa !38
  %m_SubtreeHeaders88 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %97 = load i32, i32* %i81, align 4, !tbaa !25
  %call89 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders88, i32 %97)
  %m_quantizedAabbMax90 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call89, i32 0, i32 1
  %arrayidx91 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax90, i32 0, i32 0
  store i16 %96, i16* %arrayidx91, align 2, !tbaa !38
  %98 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax92 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %98, i32 0, i32 3
  %arrayidx93 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax92, i32 0, i32 1
  %99 = load i16, i16* %arrayidx93, align 2, !tbaa !38
  %m_SubtreeHeaders94 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %100 = load i32, i32* %i81, align 4, !tbaa !25
  %call95 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders94, i32 %100)
  %m_quantizedAabbMax96 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call95, i32 0, i32 1
  %arrayidx97 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax96, i32 0, i32 1
  store i16 %99, i16* %arrayidx97, align 2, !tbaa !38
  %101 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax98 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %101, i32 0, i32 3
  %arrayidx99 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax98, i32 0, i32 2
  %102 = load i16, i16* %arrayidx99, align 2, !tbaa !38
  %m_SubtreeHeaders100 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %103 = load i32, i32* %i81, align 4, !tbaa !25
  %call101 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders100, i32 %103)
  %m_quantizedAabbMax102 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call101, i32 0, i32 1
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax102, i32 0, i32 2
  store i16 %102, i16* %arrayidx103, align 2, !tbaa !38
  %104 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin104 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %104, i32 0, i32 2
  %arrayidx105 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin104, i32 0, i32 0
  %105 = load i16, i16* %arrayidx105, align 4, !tbaa !38
  %m_SubtreeHeaders106 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %106 = load i32, i32* %i81, align 4, !tbaa !25
  %call107 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders106, i32 %106)
  %m_quantizedAabbMin108 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call107, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin108, i32 0, i32 0
  store i16 %105, i16* %arrayidx109, align 4, !tbaa !38
  %107 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin110 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %107, i32 0, i32 2
  %arrayidx111 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin110, i32 0, i32 1
  %108 = load i16, i16* %arrayidx111, align 2, !tbaa !38
  %m_SubtreeHeaders112 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %109 = load i32, i32* %i81, align 4, !tbaa !25
  %call113 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders112, i32 %109)
  %m_quantizedAabbMin114 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call113, i32 0, i32 0
  %arrayidx115 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin114, i32 0, i32 1
  store i16 %108, i16* %arrayidx115, align 2, !tbaa !38
  %110 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin116 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %110, i32 0, i32 2
  %arrayidx117 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin116, i32 0, i32 2
  %111 = load i16, i16* %arrayidx117, align 4, !tbaa !38
  %m_SubtreeHeaders118 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %112 = load i32, i32* %i81, align 4, !tbaa !25
  %call119 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders118, i32 %112)
  %m_quantizedAabbMin120 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call119, i32 0, i32 0
  %arrayidx121 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin120, i32 0, i32 2
  store i16 %111, i16* %arrayidx121, align 4, !tbaa !38
  %113 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %113, i32 0, i32 0
  %114 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !76
  %m_SubtreeHeaders122 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %115 = load i32, i32* %i81, align 4, !tbaa !25
  %call123 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders122, i32 %115)
  %m_rootNodeIndex124 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call123, i32 0, i32 2
  store i32 %114, i32* %m_rootNodeIndex124, align 4, !tbaa !28
  %116 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %116, i32 0, i32 1
  %117 = load i32, i32* %m_subtreeSize, align 4, !tbaa !78
  %m_SubtreeHeaders125 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %118 = load i32, i32* %i81, align 4, !tbaa !25
  %call126 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders125, i32 %118)
  %m_subtreeSize127 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call126, i32 0, i32 3
  store i32 %117, i32* %m_subtreeSize127, align 4, !tbaa !30
  br label %for.inc128

for.inc128:                                       ; preds = %for.body85
  %119 = load i32, i32* %i81, align 4, !tbaa !25
  %inc129 = add nsw i32 %119, 1
  store i32 %inc129, i32* %i81, align 4, !tbaa !25
  %120 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %incdec.ptr130 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %120, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr130, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  br label %for.cond82

for.end131:                                       ; preds = %for.cond.cleanup84
  %121 = bitcast %struct.btBvhSubtreeInfoData** %memPtr80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #8
  br label %if.end132

if.end132:                                        ; preds = %for.end131, %if.end73
  %122 = bitcast i32* %numElem75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataIn) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataIn.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataIn, %struct.btVector3FloatData** %dataIn.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataIn.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %4
  %5 = load float, float* %arrayidx, align 4, !tbaa !23
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %5, float* %arrayidx3, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !25
  store %struct.btOptimizedBvhNode* %fillData, %struct.btOptimizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !25
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %2 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  store i32 %4, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %6 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !25
  store i32 %14, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !25
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data11, align 4, !tbaa !49
  %19 = load i32, i32* %i6, align 4, !tbaa !25
  %arrayidx12 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %18, i32 %19
  %20 = bitcast %struct.btOptimizedBvhNode* %arrayidx12 to i8*
  %call13 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btOptimizedBvhNode*
  %22 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %struct.btOptimizedBvhNode* %21 to i8*
  %24 = bitcast %struct.btOptimizedBvhNode* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 64, i1 false), !tbaa.struct !43
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !25
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !55
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* returned %this) unnamed_addr #3 comdat {
entry:
  %this.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %struct.btOptimizedBvhNode* %this, %struct.btOptimizedBvhNode** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %this.addr, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMinOrg)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMaxOrg)
  ret %struct.btOptimizedBvhNode* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillData) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !25
  store %class.btBvhSubtreeInfo* %fillData, %class.btBvhSubtreeInfo** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !25
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %2 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  store i32 %4, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %6 = load i32, i32* %curSize, align 4, !tbaa !25
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #8
  %14 = load i32, i32* %curSize, align 4, !tbaa !25
  store i32 %14, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !25
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %18 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data11, align 4, !tbaa !36
  %19 = load i32, i32* %i6, align 4, !tbaa !25
  %arrayidx12 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %18, i32 %19
  %20 = bitcast %class.btBvhSubtreeInfo* %arrayidx12 to i8*
  %call13 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %20)
  %21 = bitcast i8* %call13 to %class.btBvhSubtreeInfo*
  %22 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btBvhSubtreeInfo* %21 to i8*
  %24 = bitcast %class.btBvhSubtreeInfo* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 32, i1 false), !tbaa.struct !37
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !25
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !25
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !25
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !35
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #8
  ret void
}

define hidden void @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhDoubleData* nonnull align 8 dereferenceable(136) %quantizedBvhDoubleData) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %quantizedBvhDoubleData.addr = alloca %struct.btQuantizedBvhDoubleData*, align 4
  %numElem = alloca i32, align 4
  %ref.tmp = alloca %struct.btOptimizedBvhNode, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeDoubleData*, align 4
  %i = alloca i32, align 4
  %numElem23 = alloca i32, align 4
  %ref.tmp24 = alloca %struct.btQuantizedBvhNode, align 4
  %memPtr27 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i28 = alloca i32, align 4
  %numElem75 = alloca i32, align 4
  %ref.tmp76 = alloca %class.btBvhSubtreeInfo, align 4
  %memPtr80 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i81 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhDoubleData* %quantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %0 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %0, i32 0, i32 1
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %1, i32 0, i32 0
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %2 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %2, i32 0, i32 2
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhQuantization, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhQuantization4)
  %3 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_curNodeIndex = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %3, i32 0, i32 3
  %4 = load i32, i32* %m_curNodeIndex, align 8, !tbaa !79
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 %4, i32* %m_curNodeIndex5, align 4, !tbaa !27
  %5 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_useQuantization = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %5, i32 0, i32 4
  %6 = load i32, i32* %m_useQuantization, align 4, !tbaa !82
  %cmp = icmp ne i32 %6, 0
  %m_useQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %m_useQuantization6, align 4, !tbaa !20
  %7 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #8
  %8 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %8, i32 0, i32 5
  %9 = load i32, i32* %m_numContiguousLeafNodes, align 8, !tbaa !83
  store i32 %9, i32* %numElem, align 4, !tbaa !25
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %10 = load i32, i32* %numElem, align 4, !tbaa !25
  %11 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %11) #8
  %12 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %12, i8 0, i32 64, i1 false)
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %10, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp)
  %13 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %13) #8
  %14 = load i32, i32* %numElem, align 4, !tbaa !25
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = bitcast %struct.btOptimizedBvhNodeDoubleData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #8
  %16 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %16, i32 0, i32 7
  %17 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %m_contiguousNodesPtr, align 8, !tbaa !84
  store %struct.btOptimizedBvhNodeDoubleData* %17, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %19 = load i32, i32* %i, align 4, !tbaa !25
  %20 = load i32, i32* %numElem, align 4, !tbaa !25
  %cmp7 = icmp slt i32 %19, %20
  br i1 %cmp7, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %21 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %22 = load i32, i32* %i, align 4, !tbaa !25
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes8, i32 %22)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call9, i32 0, i32 1
  %23 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %m_aabbMaxOrg10 = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %23, i32 0, i32 1
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_aabbMaxOrg10)
  %m_contiguousNodes11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %24 = load i32, i32* %i, align 4, !tbaa !25
  %call12 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes11, i32 %24)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call12, i32 0, i32 0
  %25 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %m_aabbMinOrg13 = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %25, i32 0, i32 0
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_aabbMinOrg13)
  %26 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %26, i32 0, i32 2
  %27 = load i32, i32* %m_escapeIndex, align 8, !tbaa !85
  %m_contiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %28 = load i32, i32* %i, align 4, !tbaa !25
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes14, i32 %28)
  %m_escapeIndex16 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call15, i32 0, i32 2
  store i32 %27, i32* %m_escapeIndex16, align 4, !tbaa !45
  %29 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %29, i32 0, i32 3
  %30 = load i32, i32* %m_subPart, align 4, !tbaa !87
  %m_contiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %31 = load i32, i32* %i, align 4, !tbaa !25
  %call18 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes17, i32 %31)
  %m_subPart19 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call18, i32 0, i32 3
  store i32 %30, i32* %m_subPart19, align 4, !tbaa !47
  %32 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %32, i32 0, i32 4
  %33 = load i32, i32* %m_triangleIndex, align 8, !tbaa !88
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %34 = load i32, i32* %i, align 4, !tbaa !25
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %34)
  %m_triangleIndex22 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 4
  store i32 %33, i32* %m_triangleIndex22, align 4, !tbaa !48
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %35 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  %36 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %36, i32 1
  store %struct.btOptimizedBvhNodeDoubleData* %incdec.ptr, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %37 = bitcast %struct.btOptimizedBvhNodeDoubleData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %37) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %38 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %38) #8
  %39 = bitcast i32* %numElem23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #8
  %40 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %40, i32 0, i32 6
  %41 = load i32, i32* %m_numQuantizedContiguousNodes, align 4, !tbaa !89
  store i32 %41, i32* %numElem23, align 4, !tbaa !25
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %42 = load i32, i32* %numElem23, align 4, !tbaa !25
  %43 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %43) #8
  %44 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %44, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %42, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %45 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %45) #8
  %46 = load i32, i32* %numElem23, align 4, !tbaa !25
  %tobool25 = icmp ne i32 %46, 0
  br i1 %tobool25, label %if.then26, label %if.end73

if.then26:                                        ; preds = %if.end
  %47 = bitcast %struct.btQuantizedBvhNodeData** %memPtr27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #8
  %48 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %48, i32 0, i32 8
  %49 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4, !tbaa !90
  store %struct.btQuantizedBvhNodeData* %49, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %50 = bitcast i32* %i28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #8
  store i32 0, i32* %i28, align 4, !tbaa !25
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc69, %if.then26
  %51 = load i32, i32* %i28, align 4, !tbaa !25
  %52 = load i32, i32* %numElem23, align 4, !tbaa !25
  %cmp30 = icmp slt i32 %51, %52
  br i1 %cmp30, label %for.body32, label %for.cond.cleanup31

for.cond.cleanup31:                               ; preds = %for.cond29
  %53 = bitcast i32* %i28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #8
  br label %for.end72

for.body32:                                       ; preds = %for.cond29
  %54 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %54, i32 0, i32 2
  %55 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !71
  %m_quantizedContiguousNodes33 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %56 = load i32, i32* %i28, align 4, !tbaa !25
  %call34 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes33, i32 %56)
  %m_escapeIndexOrTriangleIndex35 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call34, i32 0, i32 2
  store i32 %55, i32* %m_escapeIndexOrTriangleIndex35, align 4, !tbaa !40
  %57 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %57, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %58 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %m_quantizedContiguousNodes36 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %59 = load i32, i32* %i28, align 4, !tbaa !25
  %call37 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes36, i32 %59)
  %m_quantizedAabbMax38 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call37, i32 0, i32 1
  %arrayidx39 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax38, i32 0, i32 0
  store i16 %58, i16* %arrayidx39, align 2, !tbaa !38
  %60 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax40 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %60, i32 0, i32 1
  %arrayidx41 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax40, i32 0, i32 1
  %61 = load i16, i16* %arrayidx41, align 2, !tbaa !38
  %m_quantizedContiguousNodes42 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %62 = load i32, i32* %i28, align 4, !tbaa !25
  %call43 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes42, i32 %62)
  %m_quantizedAabbMax44 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call43, i32 0, i32 1
  %arrayidx45 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax44, i32 0, i32 1
  store i16 %61, i16* %arrayidx45, align 2, !tbaa !38
  %63 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMax46 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %63, i32 0, i32 1
  %arrayidx47 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax46, i32 0, i32 2
  %64 = load i16, i16* %arrayidx47, align 2, !tbaa !38
  %m_quantizedContiguousNodes48 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %65 = load i32, i32* %i28, align 4, !tbaa !25
  %call49 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes48, i32 %65)
  %m_quantizedAabbMax50 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call49, i32 0, i32 1
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax50, i32 0, i32 2
  store i16 %64, i16* %arrayidx51, align 2, !tbaa !38
  %66 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %66, i32 0, i32 0
  %arrayidx52 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %67 = load i16, i16* %arrayidx52, align 4, !tbaa !38
  %m_quantizedContiguousNodes53 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %68 = load i32, i32* %i28, align 4, !tbaa !25
  %call54 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes53, i32 %68)
  %m_quantizedAabbMin55 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call54, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin55, i32 0, i32 0
  store i16 %67, i16* %arrayidx56, align 4, !tbaa !38
  %69 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin57 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %69, i32 0, i32 0
  %arrayidx58 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin57, i32 0, i32 1
  %70 = load i16, i16* %arrayidx58, align 2, !tbaa !38
  %m_quantizedContiguousNodes59 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %71 = load i32, i32* %i28, align 4, !tbaa !25
  %call60 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes59, i32 %71)
  %m_quantizedAabbMin61 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call60, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin61, i32 0, i32 1
  store i16 %70, i16* %arrayidx62, align 2, !tbaa !38
  %72 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %m_quantizedAabbMin63 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %72, i32 0, i32 0
  %arrayidx64 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin63, i32 0, i32 2
  %73 = load i16, i16* %arrayidx64, align 4, !tbaa !38
  %m_quantizedContiguousNodes65 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %74 = load i32, i32* %i28, align 4, !tbaa !25
  %call66 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes65, i32 %74)
  %m_quantizedAabbMin67 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call66, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin67, i32 0, i32 2
  store i16 %73, i16* %arrayidx68, align 4, !tbaa !38
  br label %for.inc69

for.inc69:                                        ; preds = %for.body32
  %75 = load i32, i32* %i28, align 4, !tbaa !25
  %inc70 = add nsw i32 %75, 1
  store i32 %inc70, i32* %i28, align 4, !tbaa !25
  %76 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  %incdec.ptr71 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %76, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr71, %struct.btQuantizedBvhNodeData** %memPtr27, align 4, !tbaa !2
  br label %for.cond29

for.end72:                                        ; preds = %for.cond.cleanup31
  %77 = bitcast %struct.btQuantizedBvhNodeData** %memPtr27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #8
  br label %if.end73

if.end73:                                         ; preds = %for.end72, %if.end
  %78 = bitcast i32* %numElem23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #8
  %79 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_traversalMode = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %79, i32 0, i32 9
  %80 = load i32, i32* %m_traversalMode, align 8, !tbaa !91
  %m_traversalMode74 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 %80, i32* %m_traversalMode74, align 4, !tbaa !21
  %81 = bitcast i32* %numElem75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #8
  %82 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %82, i32 0, i32 10
  %83 = load i32, i32* %m_numSubtreeHeaders, align 4, !tbaa !92
  store i32 %83, i32* %numElem75, align 4, !tbaa !25
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %84 = load i32, i32* %numElem75, align 4, !tbaa !25
  %85 = bitcast %class.btBvhSubtreeInfo* %ref.tmp76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 32, i8* %85) #8
  %call77 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp76)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i32 %84, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp76)
  %86 = bitcast %class.btBvhSubtreeInfo* %ref.tmp76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 32, i8* %86) #8
  %87 = load i32, i32* %numElem75, align 4, !tbaa !25
  %tobool78 = icmp ne i32 %87, 0
  br i1 %tobool78, label %if.then79, label %if.end132

if.then79:                                        ; preds = %if.end73
  %88 = bitcast %struct.btBvhSubtreeInfoData** %memPtr80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #8
  %89 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4, !tbaa !2
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %89, i32 0, i32 11
  %90 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 8, !tbaa !93
  store %struct.btBvhSubtreeInfoData* %90, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %91 = bitcast i32* %i81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #8
  store i32 0, i32* %i81, align 4, !tbaa !25
  br label %for.cond82

for.cond82:                                       ; preds = %for.inc128, %if.then79
  %92 = load i32, i32* %i81, align 4, !tbaa !25
  %93 = load i32, i32* %numElem75, align 4, !tbaa !25
  %cmp83 = icmp slt i32 %92, %93
  br i1 %cmp83, label %for.body85, label %for.cond.cleanup84

for.cond.cleanup84:                               ; preds = %for.cond82
  %94 = bitcast i32* %i81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  br label %for.end131

for.body85:                                       ; preds = %for.cond82
  %95 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax86 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %95, i32 0, i32 3
  %arrayidx87 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax86, i32 0, i32 0
  %96 = load i16, i16* %arrayidx87, align 2, !tbaa !38
  %m_SubtreeHeaders88 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %97 = load i32, i32* %i81, align 4, !tbaa !25
  %call89 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders88, i32 %97)
  %m_quantizedAabbMax90 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call89, i32 0, i32 1
  %arrayidx91 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax90, i32 0, i32 0
  store i16 %96, i16* %arrayidx91, align 2, !tbaa !38
  %98 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax92 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %98, i32 0, i32 3
  %arrayidx93 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax92, i32 0, i32 1
  %99 = load i16, i16* %arrayidx93, align 2, !tbaa !38
  %m_SubtreeHeaders94 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %100 = load i32, i32* %i81, align 4, !tbaa !25
  %call95 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders94, i32 %100)
  %m_quantizedAabbMax96 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call95, i32 0, i32 1
  %arrayidx97 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax96, i32 0, i32 1
  store i16 %99, i16* %arrayidx97, align 2, !tbaa !38
  %101 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMax98 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %101, i32 0, i32 3
  %arrayidx99 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax98, i32 0, i32 2
  %102 = load i16, i16* %arrayidx99, align 2, !tbaa !38
  %m_SubtreeHeaders100 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %103 = load i32, i32* %i81, align 4, !tbaa !25
  %call101 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders100, i32 %103)
  %m_quantizedAabbMax102 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call101, i32 0, i32 1
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax102, i32 0, i32 2
  store i16 %102, i16* %arrayidx103, align 2, !tbaa !38
  %104 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin104 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %104, i32 0, i32 2
  %arrayidx105 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin104, i32 0, i32 0
  %105 = load i16, i16* %arrayidx105, align 4, !tbaa !38
  %m_SubtreeHeaders106 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %106 = load i32, i32* %i81, align 4, !tbaa !25
  %call107 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders106, i32 %106)
  %m_quantizedAabbMin108 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call107, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin108, i32 0, i32 0
  store i16 %105, i16* %arrayidx109, align 4, !tbaa !38
  %107 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin110 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %107, i32 0, i32 2
  %arrayidx111 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin110, i32 0, i32 1
  %108 = load i16, i16* %arrayidx111, align 2, !tbaa !38
  %m_SubtreeHeaders112 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %109 = load i32, i32* %i81, align 4, !tbaa !25
  %call113 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders112, i32 %109)
  %m_quantizedAabbMin114 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call113, i32 0, i32 0
  %arrayidx115 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin114, i32 0, i32 1
  store i16 %108, i16* %arrayidx115, align 2, !tbaa !38
  %110 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_quantizedAabbMin116 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %110, i32 0, i32 2
  %arrayidx117 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin116, i32 0, i32 2
  %111 = load i16, i16* %arrayidx117, align 4, !tbaa !38
  %m_SubtreeHeaders118 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %112 = load i32, i32* %i81, align 4, !tbaa !25
  %call119 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders118, i32 %112)
  %m_quantizedAabbMin120 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call119, i32 0, i32 0
  %arrayidx121 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin120, i32 0, i32 2
  store i16 %111, i16* %arrayidx121, align 4, !tbaa !38
  %113 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_rootNodeIndex = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %113, i32 0, i32 0
  %114 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !76
  %m_SubtreeHeaders122 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %115 = load i32, i32* %i81, align 4, !tbaa !25
  %call123 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders122, i32 %115)
  %m_rootNodeIndex124 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call123, i32 0, i32 2
  store i32 %114, i32* %m_rootNodeIndex124, align 4, !tbaa !28
  %116 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %m_subtreeSize = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %116, i32 0, i32 1
  %117 = load i32, i32* %m_subtreeSize, align 4, !tbaa !78
  %m_SubtreeHeaders125 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %118 = load i32, i32* %i81, align 4, !tbaa !25
  %call126 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders125, i32 %118)
  %m_subtreeSize127 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call126, i32 0, i32 3
  store i32 %117, i32* %m_subtreeSize127, align 4, !tbaa !30
  br label %for.inc128

for.inc128:                                       ; preds = %for.body85
  %119 = load i32, i32* %i81, align 4, !tbaa !25
  %inc129 = add nsw i32 %119, 1
  store i32 %inc129, i32* %i81, align 4, !tbaa !25
  %120 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  %incdec.ptr130 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %120, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr130, %struct.btBvhSubtreeInfoData** %memPtr80, align 4, !tbaa !2
  br label %for.cond82

for.end131:                                       ; preds = %for.cond.cleanup84
  %121 = bitcast %struct.btBvhSubtreeInfoData** %memPtr80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %121) #8
  br label %if.end132

if.end132:                                        ; preds = %for.end131, %if.end73
  %122 = bitcast i32* %numElem75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %122) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %this, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %dataIn) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataIn.addr = alloca %struct.btVector3DoubleData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3DoubleData* %dataIn, %struct.btVector3DoubleData** %dataIn.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %dataIn.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [4 x double], [4 x double]* %m_floats, i32 0, i32 %4
  %5 = load double, double* %arrayidx, align 8, !tbaa !94
  %conv = fptrunc double %5 to float
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %conv, float* %arrayidx3, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

define hidden i8* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer(%class.btQuantizedBvh* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %quantizedData = alloca %struct.btQuantizedBvhFloatData*, align 4
  %sz = alloca i32, align 4
  %numElem = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeFloatData*, align 4
  %i = alloca i32, align 4
  %sz55 = alloca i32, align 4
  %numElem56 = alloca i32, align 4
  %chunk59 = alloca %class.btChunk*, align 4
  %memPtr63 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i65 = alloca i32, align 4
  %sz132 = alloca i32, align 4
  %numElem133 = alloca i32, align 4
  %chunk136 = alloca %class.btChunk*, align 4
  %memPtr140 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i142 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4, !tbaa !2
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %struct.btQuantizedBvhFloatData** %quantizedData to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i8*, i8** %dataBuffer.addr, align 4, !tbaa !2
  %2 = bitcast i8* %1 to %struct.btQuantizedBvhFloatData*
  store %struct.btQuantizedBvhFloatData* %2, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %3 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %3, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %4 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %4, i32 0, i32 0
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %5 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %5, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhQuantization, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhQuantization4)
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex, align 4, !tbaa !27
  %7 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_curNodeIndex5 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %7, i32 0, i32 3
  store i32 %6, i32* %m_curNodeIndex5, align 4, !tbaa !59
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %8 = load i8, i8* %m_useQuantization, align 4, !tbaa !20, !range !26
  %tobool = trunc i8 %8 to i1
  %conv = zext i1 %tobool to i32
  %9 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_useQuantization6 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %9, i32 0, i32 4
  store i32 %conv, i32* %m_useQuantization6, align 4, !tbaa !62
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes)
  %10 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %10, i32 0, i32 5
  store i32 %call, i32* %m_numContiguousLeafNodes, align 4, !tbaa !63
  %m_contiguousNodes7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes7)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_contiguousNodes10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call11 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes10, i32 0)
  %12 = bitcast %struct.btOptimizedBvhNode* %call11 to i8*
  %13 = bitcast %class.btSerializer* %11 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %13, align 4, !tbaa !6
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %14 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call12 = call i8* %14(%class.btSerializer* %11, i8* %12)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call12, %cond.true ], [ null, %cond.false ]
  %15 = bitcast i8* %cond to %struct.btOptimizedBvhNodeFloatData*
  %16 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %16, i32 0, i32 7
  store %struct.btOptimizedBvhNodeFloatData* %15, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr, align 4, !tbaa !64
  %17 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_contiguousNodesPtr13 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %17, i32 0, i32 7
  %18 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr13, align 4, !tbaa !64
  %tobool14 = icmp ne %struct.btOptimizedBvhNodeFloatData* %18, null
  br i1 %tobool14, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %19 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #8
  store i32 48, i32* %sz, align 4, !tbaa !25
  %20 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #8
  %m_contiguousNodes15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes15)
  store i32 %call16, i32* %numElem, align 4, !tbaa !25
  %21 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #8
  %22 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %23 = load i32, i32* %sz, align 4, !tbaa !25
  %24 = load i32, i32* %numElem, align 4, !tbaa !25
  %25 = bitcast %class.btSerializer* %22 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable17 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %25, align 4, !tbaa !6
  %vfn18 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable17, i64 4
  %26 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn18, align 4
  %call19 = call %class.btChunk* %26(%class.btSerializer* %22, i32 %23, i32 %24)
  store %class.btChunk* %call19, %class.btChunk** %chunk, align 4, !tbaa !2
  %27 = bitcast %struct.btOptimizedBvhNodeFloatData** %memPtr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #8
  %28 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %28, i32 0, i32 2
  %29 = load i8*, i8** %m_oldPtr, align 4, !tbaa !96
  %30 = bitcast i8* %29 to %struct.btOptimizedBvhNodeFloatData*
  store %struct.btOptimizedBvhNodeFloatData* %30, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %31 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %32 = load i32, i32* %i, align 4, !tbaa !25
  %33 = load i32, i32* %numElem, align 4, !tbaa !25
  %cmp = icmp slt i32 %32, %33
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %34) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %35 = load i32, i32* %i, align 4, !tbaa !25
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %35)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 1
  %36 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_aabbMaxOrg22 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %36, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg22)
  %m_contiguousNodes23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %37 = load i32, i32* %i, align 4, !tbaa !25
  %call24 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes23, i32 %37)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call24, i32 0, i32 0
  %38 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_aabbMinOrg25 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %38, i32 0, i32 0
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMinOrg25)
  %m_contiguousNodes26 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %39 = load i32, i32* %i, align 4, !tbaa !25
  %call27 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes26, i32 %39)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call27, i32 0, i32 2
  %40 = load i32, i32* %m_escapeIndex, align 4, !tbaa !45
  %41 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_escapeIndex28 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %41, i32 0, i32 2
  store i32 %40, i32* %m_escapeIndex28, align 4, !tbaa !65
  %m_contiguousNodes29 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %42 = load i32, i32* %i, align 4, !tbaa !25
  %call30 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes29, i32 %42)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call30, i32 0, i32 3
  %43 = load i32, i32* %m_subPart, align 4, !tbaa !47
  %44 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_subPart31 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %44, i32 0, i32 3
  store i32 %43, i32* %m_subPart31, align 4, !tbaa !67
  %m_contiguousNodes32 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %45 = load i32, i32* %i, align 4, !tbaa !25
  %call33 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes32, i32 %45)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call33, i32 0, i32 4
  %46 = load i32, i32* %m_triangleIndex, align 4, !tbaa !48
  %47 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %m_triangleIndex34 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %47, i32 0, i32 4
  store i32 %46, i32* %m_triangleIndex34, align 4, !tbaa !68
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  %49 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %49, i32 1
  store %struct.btOptimizedBvhNodeFloatData* %incdec.ptr, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4, !tbaa !2
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %50 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %51 = load %class.btChunk*, %class.btChunk** %chunk, align 4, !tbaa !2
  %m_contiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call36 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes35, i32 0)
  %52 = bitcast %struct.btOptimizedBvhNode* %call36 to i8*
  %53 = bitcast %class.btSerializer* %50 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable37 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %53, align 4, !tbaa !6
  %vfn38 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable37, i64 5
  %54 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn38, align 4
  call void %54(%class.btSerializer* %50, %class.btChunk* %51, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %52)
  %55 = bitcast %struct.btOptimizedBvhNodeFloatData** %memPtr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #8
  %56 = bitcast %class.btChunk** %chunk to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #8
  %57 = bitcast i32* %numElem to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #8
  %58 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #8
  br label %if.end

if.end:                                           ; preds = %for.end, %cond.end
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call39 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %59 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %59, i32 0, i32 6
  store i32 %call39, i32* %m_numQuantizedContiguousNodes, align 4, !tbaa !69
  %m_quantizedContiguousNodes40 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call41 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes40)
  %tobool42 = icmp ne i32 %call41, 0
  br i1 %tobool42, label %cond.true43, label %cond.false49

cond.true43:                                      ; preds = %if.end
  %60 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_quantizedContiguousNodes44 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call45 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes44, i32 0)
  %61 = bitcast %struct.btQuantizedBvhNode* %call45 to i8*
  %62 = bitcast %class.btSerializer* %60 to i8* (%class.btSerializer*, i8*)***
  %vtable46 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %62, align 4, !tbaa !6
  %vfn47 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable46, i64 7
  %63 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn47, align 4
  %call48 = call i8* %63(%class.btSerializer* %60, i8* %61)
  br label %cond.end50

cond.false49:                                     ; preds = %if.end
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false49, %cond.true43
  %cond51 = phi i8* [ %call48, %cond.true43 ], [ null, %cond.false49 ]
  %64 = bitcast i8* %cond51 to %struct.btQuantizedBvhNodeData*
  %65 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %65, i32 0, i32 8
  store %struct.btQuantizedBvhNodeData* %64, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4, !tbaa !70
  %66 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_quantizedContiguousNodesPtr52 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %66, i32 0, i32 8
  %67 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr52, align 4, !tbaa !70
  %tobool53 = icmp ne %struct.btQuantizedBvhNodeData* %67, null
  br i1 %tobool53, label %if.then54, label %if.end114

if.then54:                                        ; preds = %cond.end50
  %68 = bitcast i32* %sz55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #8
  store i32 16, i32* %sz55, align 4, !tbaa !25
  %69 = bitcast i32* %numElem56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #8
  %m_quantizedContiguousNodes57 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes57)
  store i32 %call58, i32* %numElem56, align 4, !tbaa !25
  %70 = bitcast %class.btChunk** %chunk59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #8
  %71 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %72 = load i32, i32* %sz55, align 4, !tbaa !25
  %73 = load i32, i32* %numElem56, align 4, !tbaa !25
  %74 = bitcast %class.btSerializer* %71 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable60 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %74, align 4, !tbaa !6
  %vfn61 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable60, i64 4
  %75 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn61, align 4
  %call62 = call %class.btChunk* %75(%class.btSerializer* %71, i32 %72, i32 %73)
  store %class.btChunk* %call62, %class.btChunk** %chunk59, align 4, !tbaa !2
  %76 = bitcast %struct.btQuantizedBvhNodeData** %memPtr63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #8
  %77 = load %class.btChunk*, %class.btChunk** %chunk59, align 4, !tbaa !2
  %m_oldPtr64 = getelementptr inbounds %class.btChunk, %class.btChunk* %77, i32 0, i32 2
  %78 = load i8*, i8** %m_oldPtr64, align 4, !tbaa !96
  %79 = bitcast i8* %78 to %struct.btQuantizedBvhNodeData*
  store %struct.btQuantizedBvhNodeData* %79, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %80 = bitcast i32* %i65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #8
  store i32 0, i32* %i65, align 4, !tbaa !25
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc106, %if.then54
  %81 = load i32, i32* %i65, align 4, !tbaa !25
  %82 = load i32, i32* %numElem56, align 4, !tbaa !25
  %cmp67 = icmp slt i32 %81, %82
  br i1 %cmp67, label %for.body69, label %for.cond.cleanup68

for.cond.cleanup68:                               ; preds = %for.cond66
  %83 = bitcast i32* %i65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #8
  br label %for.end109

for.body69:                                       ; preds = %for.cond66
  %m_quantizedContiguousNodes70 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %84 = load i32, i32* %i65, align 4, !tbaa !25
  %call71 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes70, i32 %84)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call71, i32 0, i32 2
  %85 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4, !tbaa !40
  %86 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_escapeIndexOrTriangleIndex72 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %86, i32 0, i32 2
  store i32 %85, i32* %m_escapeIndexOrTriangleIndex72, align 4, !tbaa !71
  %m_quantizedContiguousNodes73 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %87 = load i32, i32* %i65, align 4, !tbaa !25
  %call74 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes73, i32 %87)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call74, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %88 = load i16, i16* %arrayidx, align 2, !tbaa !38
  %89 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMax75 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %89, i32 0, i32 1
  %arrayidx76 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax75, i32 0, i32 0
  store i16 %88, i16* %arrayidx76, align 2, !tbaa !38
  %m_quantizedContiguousNodes77 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %90 = load i32, i32* %i65, align 4, !tbaa !25
  %call78 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes77, i32 %90)
  %m_quantizedAabbMax79 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call78, i32 0, i32 1
  %arrayidx80 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax79, i32 0, i32 1
  %91 = load i16, i16* %arrayidx80, align 2, !tbaa !38
  %92 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMax81 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %92, i32 0, i32 1
  %arrayidx82 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax81, i32 0, i32 1
  store i16 %91, i16* %arrayidx82, align 2, !tbaa !38
  %m_quantizedContiguousNodes83 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %93 = load i32, i32* %i65, align 4, !tbaa !25
  %call84 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes83, i32 %93)
  %m_quantizedAabbMax85 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call84, i32 0, i32 1
  %arrayidx86 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax85, i32 0, i32 2
  %94 = load i16, i16* %arrayidx86, align 2, !tbaa !38
  %95 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMax87 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %95, i32 0, i32 1
  %arrayidx88 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax87, i32 0, i32 2
  store i16 %94, i16* %arrayidx88, align 2, !tbaa !38
  %m_quantizedContiguousNodes89 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %96 = load i32, i32* %i65, align 4, !tbaa !25
  %call90 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes89, i32 %96)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call90, i32 0, i32 0
  %arrayidx91 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %97 = load i16, i16* %arrayidx91, align 4, !tbaa !38
  %98 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMin92 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %98, i32 0, i32 0
  %arrayidx93 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin92, i32 0, i32 0
  store i16 %97, i16* %arrayidx93, align 4, !tbaa !38
  %m_quantizedContiguousNodes94 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %99 = load i32, i32* %i65, align 4, !tbaa !25
  %call95 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes94, i32 %99)
  %m_quantizedAabbMin96 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call95, i32 0, i32 0
  %arrayidx97 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin96, i32 0, i32 1
  %100 = load i16, i16* %arrayidx97, align 2, !tbaa !38
  %101 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMin98 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %101, i32 0, i32 0
  %arrayidx99 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin98, i32 0, i32 1
  store i16 %100, i16* %arrayidx99, align 2, !tbaa !38
  %m_quantizedContiguousNodes100 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %102 = load i32, i32* %i65, align 4, !tbaa !25
  %call101 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes100, i32 %102)
  %m_quantizedAabbMin102 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call101, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin102, i32 0, i32 2
  %103 = load i16, i16* %arrayidx103, align 4, !tbaa !38
  %104 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %m_quantizedAabbMin104 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %104, i32 0, i32 0
  %arrayidx105 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin104, i32 0, i32 2
  store i16 %103, i16* %arrayidx105, align 4, !tbaa !38
  br label %for.inc106

for.inc106:                                       ; preds = %for.body69
  %105 = load i32, i32* %i65, align 4, !tbaa !25
  %inc107 = add nsw i32 %105, 1
  store i32 %inc107, i32* %i65, align 4, !tbaa !25
  %106 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  %incdec.ptr108 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %106, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr108, %struct.btQuantizedBvhNodeData** %memPtr63, align 4, !tbaa !2
  br label %for.cond66

for.end109:                                       ; preds = %for.cond.cleanup68
  %107 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %108 = load %class.btChunk*, %class.btChunk** %chunk59, align 4, !tbaa !2
  %m_quantizedContiguousNodes110 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call111 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes110, i32 0)
  %109 = bitcast %struct.btQuantizedBvhNode* %call111 to i8*
  %110 = bitcast %class.btSerializer* %107 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable112 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %110, align 4, !tbaa !6
  %vfn113 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable112, i64 5
  %111 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn113, align 4
  call void %111(%class.btSerializer* %107, %class.btChunk* %108, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.1, i32 0, i32 0), i32 1497453121, i8* %109)
  %112 = bitcast %struct.btQuantizedBvhNodeData** %memPtr63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %112) #8
  %113 = bitcast %class.btChunk** %chunk59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %113) #8
  %114 = bitcast i32* %numElem56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %114) #8
  %115 = bitcast i32* %sz55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %115) #8
  br label %if.end114

if.end114:                                        ; preds = %for.end109, %cond.end50
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %116 = load i32, i32* %m_traversalMode, align 4, !tbaa !21
  %117 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_traversalMode115 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %117, i32 0, i32 10
  store i32 %116, i32* %m_traversalMode115, align 4, !tbaa !73
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call116 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %118 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %118, i32 0, i32 11
  store i32 %call116, i32* %m_numSubtreeHeaders, align 4, !tbaa !74
  %m_SubtreeHeaders117 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call118 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders117)
  %tobool119 = icmp ne i32 %call118, 0
  br i1 %tobool119, label %cond.true120, label %cond.false126

cond.true120:                                     ; preds = %if.end114
  %119 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %m_SubtreeHeaders121 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call122 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders121, i32 0)
  %120 = bitcast %class.btBvhSubtreeInfo* %call122 to i8*
  %121 = bitcast %class.btSerializer* %119 to i8* (%class.btSerializer*, i8*)***
  %vtable123 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %121, align 4, !tbaa !6
  %vfn124 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable123, i64 7
  %122 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn124, align 4
  %call125 = call i8* %122(%class.btSerializer* %119, i8* %120)
  br label %cond.end127

cond.false126:                                    ; preds = %if.end114
  br label %cond.end127

cond.end127:                                      ; preds = %cond.false126, %cond.true120
  %cond128 = phi i8* [ %call125, %cond.true120 ], [ null, %cond.false126 ]
  %123 = bitcast i8* %cond128 to %struct.btBvhSubtreeInfoData*
  %124 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %124, i32 0, i32 9
  store %struct.btBvhSubtreeInfoData* %123, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 4, !tbaa !75
  %125 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4, !tbaa !2
  %m_subTreeInfoPtr129 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %125, i32 0, i32 9
  %126 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr129, align 4, !tbaa !75
  %tobool130 = icmp ne %struct.btBvhSubtreeInfoData* %126, null
  br i1 %tobool130, label %if.then131, label %if.end197

if.then131:                                       ; preds = %cond.end127
  %127 = bitcast i32* %sz132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #8
  store i32 20, i32* %sz132, align 4, !tbaa !25
  %128 = bitcast i32* %numElem133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #8
  %m_SubtreeHeaders134 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call135 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders134)
  store i32 %call135, i32* %numElem133, align 4, !tbaa !25
  %129 = bitcast %class.btChunk** %chunk136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #8
  %130 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %131 = load i32, i32* %sz132, align 4, !tbaa !25
  %132 = load i32, i32* %numElem133, align 4, !tbaa !25
  %133 = bitcast %class.btSerializer* %130 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable137 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %133, align 4, !tbaa !6
  %vfn138 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable137, i64 4
  %134 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn138, align 4
  %call139 = call %class.btChunk* %134(%class.btSerializer* %130, i32 %131, i32 %132)
  store %class.btChunk* %call139, %class.btChunk** %chunk136, align 4, !tbaa !2
  %135 = bitcast %struct.btBvhSubtreeInfoData** %memPtr140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %135) #8
  %136 = load %class.btChunk*, %class.btChunk** %chunk136, align 4, !tbaa !2
  %m_oldPtr141 = getelementptr inbounds %class.btChunk, %class.btChunk* %136, i32 0, i32 2
  %137 = load i8*, i8** %m_oldPtr141, align 4, !tbaa !96
  %138 = bitcast i8* %137 to %struct.btBvhSubtreeInfoData*
  store %struct.btBvhSubtreeInfoData* %138, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %139 = bitcast i32* %i142 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %139) #8
  store i32 0, i32* %i142, align 4, !tbaa !25
  br label %for.cond143

for.cond143:                                      ; preds = %for.inc189, %if.then131
  %140 = load i32, i32* %i142, align 4, !tbaa !25
  %141 = load i32, i32* %numElem133, align 4, !tbaa !25
  %cmp144 = icmp slt i32 %140, %141
  br i1 %cmp144, label %for.body146, label %for.cond.cleanup145

for.cond.cleanup145:                              ; preds = %for.cond143
  %142 = bitcast i32* %i142 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #8
  br label %for.end192

for.body146:                                      ; preds = %for.cond143
  %m_SubtreeHeaders147 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %143 = load i32, i32* %i142, align 4, !tbaa !25
  %call148 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders147, i32 %143)
  %m_quantizedAabbMax149 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call148, i32 0, i32 1
  %arrayidx150 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax149, i32 0, i32 0
  %144 = load i16, i16* %arrayidx150, align 2, !tbaa !38
  %145 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMax151 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %145, i32 0, i32 3
  %arrayidx152 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax151, i32 0, i32 0
  store i16 %144, i16* %arrayidx152, align 2, !tbaa !38
  %m_SubtreeHeaders153 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %146 = load i32, i32* %i142, align 4, !tbaa !25
  %call154 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders153, i32 %146)
  %m_quantizedAabbMax155 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call154, i32 0, i32 1
  %arrayidx156 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax155, i32 0, i32 1
  %147 = load i16, i16* %arrayidx156, align 2, !tbaa !38
  %148 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMax157 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %148, i32 0, i32 3
  %arrayidx158 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax157, i32 0, i32 1
  store i16 %147, i16* %arrayidx158, align 2, !tbaa !38
  %m_SubtreeHeaders159 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %149 = load i32, i32* %i142, align 4, !tbaa !25
  %call160 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders159, i32 %149)
  %m_quantizedAabbMax161 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call160, i32 0, i32 1
  %arrayidx162 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax161, i32 0, i32 2
  %150 = load i16, i16* %arrayidx162, align 2, !tbaa !38
  %151 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMax163 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %151, i32 0, i32 3
  %arrayidx164 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax163, i32 0, i32 2
  store i16 %150, i16* %arrayidx164, align 2, !tbaa !38
  %m_SubtreeHeaders165 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %152 = load i32, i32* %i142, align 4, !tbaa !25
  %call166 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders165, i32 %152)
  %m_quantizedAabbMin167 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call166, i32 0, i32 0
  %arrayidx168 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin167, i32 0, i32 0
  %153 = load i16, i16* %arrayidx168, align 4, !tbaa !38
  %154 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMin169 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %154, i32 0, i32 2
  %arrayidx170 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin169, i32 0, i32 0
  store i16 %153, i16* %arrayidx170, align 4, !tbaa !38
  %m_SubtreeHeaders171 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %155 = load i32, i32* %i142, align 4, !tbaa !25
  %call172 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders171, i32 %155)
  %m_quantizedAabbMin173 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call172, i32 0, i32 0
  %arrayidx174 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin173, i32 0, i32 1
  %156 = load i16, i16* %arrayidx174, align 2, !tbaa !38
  %157 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMin175 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %157, i32 0, i32 2
  %arrayidx176 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin175, i32 0, i32 1
  store i16 %156, i16* %arrayidx176, align 2, !tbaa !38
  %m_SubtreeHeaders177 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %158 = load i32, i32* %i142, align 4, !tbaa !25
  %call178 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders177, i32 %158)
  %m_quantizedAabbMin179 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call178, i32 0, i32 0
  %arrayidx180 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin179, i32 0, i32 2
  %159 = load i16, i16* %arrayidx180, align 4, !tbaa !38
  %160 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_quantizedAabbMin181 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %160, i32 0, i32 2
  %arrayidx182 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin181, i32 0, i32 2
  store i16 %159, i16* %arrayidx182, align 4, !tbaa !38
  %m_SubtreeHeaders183 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %161 = load i32, i32* %i142, align 4, !tbaa !25
  %call184 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders183, i32 %161)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call184, i32 0, i32 2
  %162 = load i32, i32* %m_rootNodeIndex, align 4, !tbaa !28
  %163 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_rootNodeIndex185 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %163, i32 0, i32 0
  store i32 %162, i32* %m_rootNodeIndex185, align 4, !tbaa !76
  %m_SubtreeHeaders186 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %164 = load i32, i32* %i142, align 4, !tbaa !25
  %call187 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders186, i32 %164)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call187, i32 0, i32 3
  %165 = load i32, i32* %m_subtreeSize, align 4, !tbaa !30
  %166 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %m_subtreeSize188 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %166, i32 0, i32 1
  store i32 %165, i32* %m_subtreeSize188, align 4, !tbaa !78
  br label %for.inc189

for.inc189:                                       ; preds = %for.body146
  %167 = load i32, i32* %i142, align 4, !tbaa !25
  %inc190 = add nsw i32 %167, 1
  store i32 %inc190, i32* %i142, align 4, !tbaa !25
  %168 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  %incdec.ptr191 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %168, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr191, %struct.btBvhSubtreeInfoData** %memPtr140, align 4, !tbaa !2
  br label %for.cond143

for.end192:                                       ; preds = %for.cond.cleanup145
  %169 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4, !tbaa !2
  %170 = load %class.btChunk*, %class.btChunk** %chunk136, align 4, !tbaa !2
  %m_SubtreeHeaders193 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call194 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders193, i32 0)
  %171 = bitcast %class.btBvhSubtreeInfo* %call194 to i8*
  %172 = bitcast %class.btSerializer* %169 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable195 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %172, align 4, !tbaa !6
  %vfn196 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable195, i64 5
  %173 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn196, align 4
  call void %173(%class.btSerializer* %169, %class.btChunk* %170, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0), i32 1497453121, i8* %171)
  %174 = bitcast %struct.btBvhSubtreeInfoData** %memPtr140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #8
  %175 = bitcast %class.btChunk** %chunk136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #8
  %176 = bitcast i32* %numElem133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #8
  %177 = bitcast i32* %sz132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #8
  br label %if.end197

if.end197:                                        ; preds = %for.end192, %cond.end127
  %178 = bitcast %struct.btQuantizedBvhFloatData** %quantizedData to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #8
  ret i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  store i32 0, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4, !tbaa !25
  %cmp = icmp slt i32 %1, 4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %2) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %3
  %4 = load float, float* %arrayidx, align 4, !tbaa !23
  %5 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %5, i32 0, i32 0
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %6
  store float %4, float* %arrayidx3, align 4, !tbaa !23
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !55
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv(%class.btQuantizedBvh* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  ret i32 84
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %b.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %2 = load float*, float** %a.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4, !tbaa !2
  store float* %b, float** %b.addr, align 4, !tbaa !2
  %0 = load float*, float** %a.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !23
  %2 = load float*, float** %b.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !23
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !23
  %6 = load float*, float** %a.addr, align 4, !tbaa !2
  store float %5, float* %6, align 4, !tbaa !23
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #6

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #3 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !23
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !23
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !23
  %5 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #8
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4, !tbaa !23
  %8 = load float*, float** %s.addr, align 4, !tbaa !2
  %9 = load float, float* %8, align 4, !tbaa !23
  %mul4 = fmul float %7, %9
  store float %mul4, float* %ref.tmp1, align 4, !tbaa !23
  %10 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #8
  %11 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %12 = load float, float* %arrayidx7, align 4, !tbaa !23
  %13 = load float*, float** %s.addr, align 4, !tbaa !2
  %14 = load float, float* %13, align 4, !tbaa !23
  %mul8 = fmul float %12, %14
  store float %mul8, float* %ref.tmp5, align 4, !tbaa !23
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %15 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  %16 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #8
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z8btSelectjii(i32 %condition, i32 %valueIfConditionNonZero, i32 %valueIfConditionZero) #1 comdat {
entry:
  %condition.addr = alloca i32, align 4
  %valueIfConditionNonZero.addr = alloca i32, align 4
  %valueIfConditionZero.addr = alloca i32, align 4
  %testNz = alloca i32, align 4
  %testEqz = alloca i32, align 4
  store i32 %condition, i32* %condition.addr, align 4, !tbaa !25
  store i32 %valueIfConditionNonZero, i32* %valueIfConditionNonZero.addr, align 4, !tbaa !25
  store i32 %valueIfConditionZero, i32* %valueIfConditionZero.addr, align 4, !tbaa !25
  %0 = bitcast i32* %testNz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %condition.addr, align 4, !tbaa !25
  %2 = load i32, i32* %condition.addr, align 4, !tbaa !25
  %sub = sub nsw i32 0, %2
  %or = or i32 %1, %sub
  %shr = ashr i32 %or, 31
  store i32 %shr, i32* %testNz, align 4, !tbaa !25
  %3 = bitcast i32* %testEqz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load i32, i32* %testNz, align 4, !tbaa !25
  %neg = xor i32 %4, -1
  store i32 %neg, i32* %testEqz, align 4, !tbaa !25
  %5 = load i32, i32* %valueIfConditionNonZero.addr, align 4, !tbaa !25
  %6 = load i32, i32* %testNz, align 4, !tbaa !25
  %and = and i32 %5, %6
  %7 = load i32, i32* %valueIfConditionZero.addr, align 4, !tbaa !25
  %8 = load i32, i32* %testEqz, align 4, !tbaa !25
  %and1 = and i32 %7, %8
  %or2 = or i32 %and, %and1
  %9 = bitcast i32* %testEqz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #8
  %10 = bitcast i32* %testNz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #8
  ret i32 %or2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !23
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !23
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #8
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !23
  %0 = load float, float* %y.addr, align 4, !tbaa !23
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_Z12btSwapEndianj(i32 %val) #1 comdat {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4, !tbaa !25
  %0 = load i32, i32* %val.addr, align 4, !tbaa !25
  %and = and i32 %0, -16777216
  %shr = lshr i32 %and, 24
  %1 = load i32, i32* %val.addr, align 4, !tbaa !25
  %and1 = and i32 %1, 16711680
  %shr2 = lshr i32 %and1, 8
  %or = or i32 %shr, %shr2
  %2 = load i32, i32* %val.addr, align 4, !tbaa !25
  %and3 = and i32 %2, 65280
  %shl = shl i32 %and3, 8
  %or4 = or i32 %or, %shl
  %3 = load i32, i32* %val.addr, align 4, !tbaa !25
  %and5 = and i32 %3, 255
  %shl6 = shl i32 %and5, 24
  %or7 = or i32 %or4, %shl6
  ret i32 %or7
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %sourceVal, float* nonnull align 4 dereferenceable(4) %destVal) #1 comdat {
entry:
  %sourceVal.addr = alloca float*, align 4
  %destVal.addr = alloca float*, align 4
  %dest = alloca i8*, align 4
  %src = alloca i8*, align 4
  store float* %sourceVal, float** %sourceVal.addr, align 4, !tbaa !2
  store float* %destVal, float** %destVal.addr, align 4, !tbaa !2
  %0 = bitcast i8** %dest to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load float*, float** %destVal.addr, align 4, !tbaa !2
  %2 = bitcast float* %1 to i8*
  store i8* %2, i8** %dest, align 4, !tbaa !2
  %3 = bitcast i8** %src to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #8
  %4 = load float*, float** %sourceVal.addr, align 4, !tbaa !2
  %5 = bitcast float* %4 to i8*
  store i8* %5, i8** %src, align 4, !tbaa !2
  %6 = load i8*, i8** %src, align 4, !tbaa !2
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 3
  %7 = load i8, i8* %arrayidx, align 1, !tbaa !34
  %8 = load i8*, i8** %dest, align 4, !tbaa !2
  %arrayidx1 = getelementptr inbounds i8, i8* %8, i32 0
  store i8 %7, i8* %arrayidx1, align 1, !tbaa !34
  %9 = load i8*, i8** %src, align 4, !tbaa !2
  %arrayidx2 = getelementptr inbounds i8, i8* %9, i32 2
  %10 = load i8, i8* %arrayidx2, align 1, !tbaa !34
  %11 = load i8*, i8** %dest, align 4, !tbaa !2
  %arrayidx3 = getelementptr inbounds i8, i8* %11, i32 1
  store i8 %10, i8* %arrayidx3, align 1, !tbaa !34
  %12 = load i8*, i8** %src, align 4, !tbaa !2
  %arrayidx4 = getelementptr inbounds i8, i8* %12, i32 1
  %13 = load i8, i8* %arrayidx4, align 1, !tbaa !34
  %14 = load i8*, i8** %dest, align 4, !tbaa !2
  %arrayidx5 = getelementptr inbounds i8, i8* %14, i32 2
  store i8 %13, i8* %arrayidx5, align 1, !tbaa !34
  %15 = load i8*, i8** %src, align 4, !tbaa !2
  %arrayidx6 = getelementptr inbounds i8, i8* %15, i32 0
  %16 = load i8, i8* %arrayidx6, align 1, !tbaa !34
  %17 = load i8*, i8** %dest, align 4, !tbaa !2
  %arrayidx7 = getelementptr inbounds i8, i8* %17, i32 3
  store i8 %16, i8* %arrayidx7, align 1, !tbaa !34
  %18 = bitcast i8** %src to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %19 = bitcast i8** %dest to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !54
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !55
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !56
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !52
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !31
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !53
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !57
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !35
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !58
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %tobool = icmp ne %class.btBvhSubtreeInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !57, !range !26
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data4, align 4, !tbaa !36
  call void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %class.btBvhSubtreeInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data5, align 4, !tbaa !36
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %class.btBvhSubtreeInfo* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store %class.btBvhSubtreeInfo* %ptr, %class.btBvhSubtreeInfo** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btBvhSubtreeInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btQuantizedBvhNode*
  store %struct.btQuantizedBvhNode* %3, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btQuantizedBvhNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !52
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %5, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !53
  %7 = bitcast %struct.btQuantizedBvhNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !50
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !53
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btQuantizedBvhNode** null)
  %2 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btQuantizedBvhNode* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store %struct.btQuantizedBvhNode* %dest, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  %6 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 %9
  %10 = bitcast %struct.btQuantizedBvhNode* %7 to i8*
  %11 = bitcast %struct.btQuantizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !33
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4, !tbaa !32
  %tobool = icmp ne %struct.btQuantizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !52, !range !26
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data4, align 4, !tbaa !32
  call void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btQuantizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data5, align 4, !tbaa !32
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btQuantizedBvhNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btQuantizedBvhNode**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store %struct.btQuantizedBvhNode** %hint, %struct.btQuantizedBvhNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  ret %struct.btQuantizedBvhNode* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #6

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btQuantizedBvhNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store %struct.btQuantizedBvhNode* %ptr, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btQuantizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !58
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btBvhSubtreeInfo** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btBvhSubtreeInfo*
  store %class.btBvhSubtreeInfo* %3, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btBvhSubtreeInfo* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !57
  %5 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %5, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !58
  %7 = bitcast %class.btBvhSubtreeInfo** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !50
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btBvhSubtreeInfo** null)
  %2 = bitcast %class.btBvhSubtreeInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btBvhSubtreeInfo* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store %class.btBvhSubtreeInfo* %dest, %class.btBvhSubtreeInfo** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %4, i32 %5
  %6 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %6)
  %7 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %8 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %8, i32 %9
  %10 = bitcast %class.btBvhSubtreeInfo* %7 to i8*
  %11 = bitcast %class.btBvhSubtreeInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 32, i1 false), !tbaa.struct !37
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btBvhSubtreeInfo** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btBvhSubtreeInfo**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store %class.btBvhSubtreeInfo** %hint, %class.btBvhSubtreeInfo*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  ret %class.btBvhSubtreeInfo* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !25
  store i32 %last, i32* %last.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %last.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %tobool = icmp ne %struct.btOptimizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !54, !range !26
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data4, align 4, !tbaa !49
  call void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btOptimizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data5, align 4, !tbaa !49
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btOptimizedBvhNode* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %struct.btOptimizedBvhNode* %ptr, %struct.btOptimizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btOptimizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btOptimizedBvhNode** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btOptimizedBvhNode*
  store %struct.btOptimizedBvhNode* %3, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btOptimizedBvhNode* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !54
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %5, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !25
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !56
  %7 = bitcast %struct.btOptimizedBvhNode** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !50
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !56
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #3 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !25
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !25
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !25
  %call = call %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btOptimizedBvhNode** null)
  %2 = bitcast %struct.btOptimizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btOptimizedBvhNode* %dest) #3 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !25
  store i32 %end, i32* %end.addr, align 4, !tbaa !25
  store %struct.btOptimizedBvhNode* %dest, %struct.btOptimizedBvhNode** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !25
  store i32 %1, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !25
  %3 = load i32, i32* %end.addr, align 4, !tbaa !25
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %4, i32 %5
  %6 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %6)
  %7 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4, !tbaa !49
  %9 = load i32, i32* %i, align 4, !tbaa !25
  %arrayidx2 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %8, i32 %9
  %10 = bitcast %struct.btOptimizedBvhNode* %7 to i8*
  %11 = bitcast %struct.btOptimizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 64, i1 false), !tbaa.struct !43
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !25
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !25
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btOptimizedBvhNode** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btOptimizedBvhNode**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !25
  store %struct.btOptimizedBvhNode** %hint, %struct.btOptimizedBvhNode*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !25
  %mul = mul i32 64, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  ret %struct.btOptimizedBvhNode* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn writeonly }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !11, i64 52}
!9 = !{!"_ZTS14btQuantizedBvh", !10, i64 4, !10, i64 20, !10, i64 36, !11, i64 52, !11, i64 56, !12, i64 60, !13, i64 64, !13, i64 84, !15, i64 104, !15, i64 124, !17, i64 144, !18, i64 148, !11, i64 168}
!10 = !{!"_ZTS9btVector3", !4, i64 0}
!11 = !{!"int", !4, i64 0}
!12 = !{!"bool", !4, i64 0}
!13 = !{!"_ZTS20btAlignedObjectArrayI18btOptimizedBvhNodeE", !14, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !12, i64 16}
!14 = !{!"_ZTS18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE"}
!15 = !{!"_ZTS20btAlignedObjectArrayI18btQuantizedBvhNodeE", !16, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !12, i64 16}
!16 = !{!"_ZTS18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE"}
!17 = !{!"_ZTSN14btQuantizedBvh15btTraversalModeE", !4, i64 0}
!18 = !{!"_ZTS20btAlignedObjectArrayI16btBvhSubtreeInfoE", !19, i64 0, !11, i64 4, !11, i64 8, !3, i64 12, !12, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE"}
!20 = !{!9, !12, i64 60}
!21 = !{!9, !17, i64 144}
!22 = !{!9, !11, i64 168}
!23 = !{!24, !24, i64 0}
!24 = !{!"float", !4, i64 0}
!25 = !{!11, !11, i64 0}
!26 = !{i8 0, i8 2}
!27 = !{!9, !11, i64 56}
!28 = !{!29, !11, i64 12}
!29 = !{!"_ZTS16btBvhSubtreeInfo", !4, i64 0, !4, i64 6, !11, i64 12, !11, i64 16, !4, i64 20}
!30 = !{!29, !11, i64 16}
!31 = !{!15, !11, i64 4}
!32 = !{!15, !3, i64 12}
!33 = !{i64 0, i64 6, !34, i64 6, i64 6, !34, i64 12, i64 4, !25}
!34 = !{!4, !4, i64 0}
!35 = !{!18, !11, i64 4}
!36 = !{!18, !3, i64 12}
!37 = !{i64 0, i64 6, !34, i64 6, i64 6, !34, i64 12, i64 4, !25, i64 16, i64 4, !25, i64 20, i64 12, !34}
!38 = !{!39, !39, i64 0}
!39 = !{!"short", !4, i64 0}
!40 = !{!41, !11, i64 12}
!41 = !{!"_ZTS18btQuantizedBvhNode", !4, i64 0, !4, i64 6, !11, i64 12}
!42 = !{i64 0, i64 16, !34}
!43 = !{i64 0, i64 16, !34, i64 16, i64 16, !34, i64 32, i64 4, !25, i64 36, i64 4, !25, i64 40, i64 4, !25, i64 44, i64 20, !34}
!44 = !{!12, !12, i64 0}
!45 = !{!46, !11, i64 32}
!46 = !{!"_ZTS18btOptimizedBvhNode", !10, i64 0, !10, i64 16, !11, i64 32, !11, i64 36, !11, i64 40, !4, i64 44}
!47 = !{!46, !11, i64 36}
!48 = !{!46, !11, i64 40}
!49 = !{!13, !3, i64 12}
!50 = !{!51, !51, i64 0}
!51 = !{!"long", !4, i64 0}
!52 = !{!15, !12, i64 16}
!53 = !{!15, !11, i64 8}
!54 = !{!13, !12, i64 16}
!55 = !{!13, !11, i64 4}
!56 = !{!13, !11, i64 8}
!57 = !{!18, !12, i64 16}
!58 = !{!18, !11, i64 8}
!59 = !{!60, !11, i64 48}
!60 = !{!"_ZTS23btQuantizedBvhFloatData", !61, i64 0, !61, i64 16, !61, i64 32, !11, i64 48, !11, i64 52, !11, i64 56, !11, i64 60, !3, i64 64, !3, i64 68, !3, i64 72, !11, i64 76, !11, i64 80}
!61 = !{!"_ZTS18btVector3FloatData", !4, i64 0}
!62 = !{!60, !11, i64 52}
!63 = !{!60, !11, i64 56}
!64 = !{!60, !3, i64 64}
!65 = !{!66, !11, i64 32}
!66 = !{!"_ZTS27btOptimizedBvhNodeFloatData", !61, i64 0, !61, i64 16, !11, i64 32, !11, i64 36, !11, i64 40, !4, i64 44}
!67 = !{!66, !11, i64 36}
!68 = !{!66, !11, i64 40}
!69 = !{!60, !11, i64 60}
!70 = !{!60, !3, i64 68}
!71 = !{!72, !11, i64 12}
!72 = !{!"_ZTS22btQuantizedBvhNodeData", !4, i64 0, !4, i64 6, !11, i64 12}
!73 = !{!60, !11, i64 76}
!74 = !{!60, !11, i64 80}
!75 = !{!60, !3, i64 72}
!76 = !{!77, !11, i64 0}
!77 = !{!"_ZTS20btBvhSubtreeInfoData", !11, i64 0, !11, i64 4, !4, i64 8, !4, i64 14}
!78 = !{!77, !11, i64 4}
!79 = !{!80, !11, i64 96}
!80 = !{!"_ZTS24btQuantizedBvhDoubleData", !81, i64 0, !81, i64 32, !81, i64 64, !11, i64 96, !11, i64 100, !11, i64 104, !11, i64 108, !3, i64 112, !3, i64 116, !11, i64 120, !11, i64 124, !3, i64 128}
!81 = !{!"_ZTS19btVector3DoubleData", !4, i64 0}
!82 = !{!80, !11, i64 100}
!83 = !{!80, !11, i64 104}
!84 = !{!80, !3, i64 112}
!85 = !{!86, !11, i64 64}
!86 = !{!"_ZTS28btOptimizedBvhNodeDoubleData", !81, i64 0, !81, i64 32, !11, i64 64, !11, i64 68, !11, i64 72, !4, i64 76}
!87 = !{!86, !11, i64 68}
!88 = !{!86, !11, i64 72}
!89 = !{!80, !11, i64 108}
!90 = !{!80, !3, i64 116}
!91 = !{!80, !11, i64 120}
!92 = !{!80, !11, i64 124}
!93 = !{!80, !3, i64 128}
!94 = !{!95, !95, i64 0}
!95 = !{!"double", !4, i64 0}
!96 = !{!97, !3, i64 8}
!97 = !{!"_ZTS7btChunk", !11, i64 0, !11, i64 4, !3, i64 8, !11, i64 12, !11, i64 16}
