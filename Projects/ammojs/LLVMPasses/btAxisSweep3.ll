; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btAxisSweep3.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/BroadphaseCollision/btAxisSweep3.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btAxisSweep3 = type { %class.btAxisSweep3Internal }
%class.btAxisSweep3Internal = type { %class.btBroadphaseInterface, i16, i16, %class.btVector3, %class.btVector3, %class.btVector3, i16, i16, %"class.btAxisSweep3Internal<unsigned short>::Handle"*, i16, [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x i8*], %class.btOverlappingPairCache*, %class.btOverlappingPairCallback*, i8, i32, %struct.btDbvtBroadphase*, %class.btOverlappingPairCache* }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%"class.btAxisSweep3Internal<unsigned short>::Handle" = type { %struct.btBroadphaseProxy, [3 x i16], [3 x i16], %struct.btBroadphaseProxy* }
%struct.btBroadphaseProxy = type { i8*, i16, i16, i8*, i32, %class.btVector3, %class.btVector3 }
%"class.btAxisSweep3Internal<unsigned short>::Edge" = type { i16, i16 }
%class.btOverlappingPairCallback = type { i32 (...)** }
%struct.btDbvtBroadphase = type <{ %class.btBroadphaseInterface, [2 x %struct.btDbvt], [3 x %struct.btDbvtProxy*], %class.btOverlappingPairCache*, float, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i8, i8, i8, i8 }>
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btDbvtProxy = type { %struct.btBroadphaseProxy, %struct.btDbvtNode*, [2 x %struct.btDbvtProxy*], i32 }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.bt32BitAxisSweep3 = type { %class.btAxisSweep3Internal.4 }
%class.btAxisSweep3Internal.4 = type { %class.btBroadphaseInterface, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, %"class.btAxisSweep3Internal<unsigned int>::Handle"*, i32, [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x i8*], %class.btOverlappingPairCache*, %class.btOverlappingPairCallback*, i8, i32, %struct.btDbvtBroadphase*, %class.btOverlappingPairCache* }
%"class.btAxisSweep3Internal<unsigned int>::Handle" = type { %struct.btBroadphaseProxy, [3 x i32], [3 x i32], %struct.btBroadphaseProxy* }
%"class.btAxisSweep3Internal<unsigned int>::Edge" = type { i32, i32 }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.5, %struct.btOverlapFilterCallback*, i8, [3 x i8], %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.8 }
%class.btCollisionAlgorithm = type opaque
%union.anon.8 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btNullPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.5 }
%class.btDispatcher = type opaque
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%class.btBroadphasePairSortPredicate = type { i8 }
%struct.btOverlapCallback = type { i32 (...)** }

$_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb = comdat any

$_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb = comdat any

$_ZN20btAxisSweep3InternalItED2Ev = comdat any

$_ZN12btAxisSweep3D0Ev = comdat any

$_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ = comdat any

$_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ = comdat any

$_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ = comdat any

$_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback = comdat any

$_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ = comdat any

$_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE10printStatsEv = comdat any

$_ZN17bt32BitAxisSweep3D0Ev = comdat any

$_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ = comdat any

$_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher = comdat any

$_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ = comdat any

$_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ = comdat any

$_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback = comdat any

$_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv = comdat any

$_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ = comdat any

$_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE10printStatsEv = comdat any

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN15btNullPairCacheC2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAxisSweep3InternalItE6HandlenaEm = comdat any

$_ZN20btAxisSweep3InternalItE6HandleC2Ev = comdat any

$_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt = comdat any

$_ZN20btAxisSweep3InternalItED0Ev = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN22btOverlappingPairCacheC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev = comdat any

$_ZN15btNullPairCacheD2Ev = comdat any

$_ZN15btNullPairCacheD0Ev = comdat any

$_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher = comdat any

$_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN15btNullPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv = comdat any

$_ZN15btNullPairCache23getOverlappingPairArrayEv = comdat any

$_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher = comdat any

$_ZNK15btNullPairCache22getNumOverlappingPairsEv = comdat any

$_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher = comdat any

$_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback = comdat any

$_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher = comdat any

$_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_ = comdat any

$_ZN15btNullPairCache18hasDeferredRemovalEv = comdat any

$_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback = comdat any

$_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher = comdat any

$_ZN25btOverlappingPairCallbackC2Ev = comdat any

$_ZN25btOverlappingPairCallbackD2Ev = comdat any

$_ZN22btOverlappingPairCacheD0Ev = comdat any

$_ZN25btOverlappingPairCallbackD0Ev = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZN17btBroadphaseProxyC2Ev = comdat any

$_ZN20btAxisSweep3InternalItE6HandledaEPv = comdat any

$_ZN20btAxisSweep3InternalItEdlEPv = comdat any

$_ZN20btAxisSweep3InternalIjE6HandlenaEm = comdat any

$_ZN20btAxisSweep3InternalIjE6HandleC2Ev = comdat any

$_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj = comdat any

$_ZN20btAxisSweep3InternalIjED2Ev = comdat any

$_ZN20btAxisSweep3InternalIjED0Ev = comdat any

$_ZN20btAxisSweep3InternalIjE6HandledaEPv = comdat any

$_ZN20btAxisSweep3InternalIjEdlEPv = comdat any

$_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_ = comdat any

$_ZNK20btAxisSweep3InternalItE9getHandleEt = comdat any

$_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i = comdat any

$_ZN20btAxisSweep3InternalItE11allocHandleEv = comdat any

$_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv = comdat any

$_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv = comdat any

$_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii = comdat any

$_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalItE10freeHandleEt = comdat any

$_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_ = comdat any

$_ZNK20btAxisSweep3InternalIjE9getHandleEj = comdat any

$_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i = comdat any

$_ZN20btAxisSweep3InternalIjE11allocHandleEv = comdat any

$_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb = comdat any

$_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv = comdat any

$_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv = comdat any

$_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii = comdat any

$_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb = comdat any

$_ZN20btAxisSweep3InternalIjE10freeHandleEj = comdat any

$_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher = comdat any

$_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_ = comdat any

$_ZTV12btAxisSweep3 = comdat any

$_ZTV17bt32BitAxisSweep3 = comdat any

$_ZTS12btAxisSweep3 = comdat any

$_ZTS20btAxisSweep3InternalItE = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTI20btAxisSweep3InternalItE = comdat any

$_ZTI12btAxisSweep3 = comdat any

$_ZTS17bt32BitAxisSweep3 = comdat any

$_ZTS20btAxisSweep3InternalIjE = comdat any

$_ZTI20btAxisSweep3InternalIjE = comdat any

$_ZTI17bt32BitAxisSweep3 = comdat any

$_ZTV20btAxisSweep3InternalItE = comdat any

$_ZTV21btBroadphaseInterface = comdat any

$_ZTV15btNullPairCache = comdat any

$_ZTS15btNullPairCache = comdat any

$_ZTS22btOverlappingPairCache = comdat any

$_ZTS25btOverlappingPairCallback = comdat any

$_ZTI25btOverlappingPairCallback = comdat any

$_ZTI22btOverlappingPairCache = comdat any

$_ZTI15btNullPairCache = comdat any

$_ZTV22btOverlappingPairCache = comdat any

$_ZTV25btOverlappingPairCallback = comdat any

$_ZTV20btAxisSweep3InternalIjE = comdat any

@_ZTV12btAxisSweep3 = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI12btAxisSweep3 to i8*), i8* bitcast (%class.btAxisSweep3Internal* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3*)* @_ZN12btAxisSweep3D0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE10printStatsEv to i8*)] }, comdat, align 4
@_ZTV17bt32BitAxisSweep3 = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17bt32BitAxisSweep3 to i8*), i8* bitcast (%class.btAxisSweep3Internal.4* (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjED2Ev to i8*), i8* bitcast (void (%class.bt32BitAxisSweep3*)* @_ZN17bt32BitAxisSweep3D0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.4*)* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjE10printStatsEv to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS12btAxisSweep3 = linkonce_odr hidden constant [15 x i8] c"12btAxisSweep3\00", comdat, align 1
@_ZTS20btAxisSweep3InternalItE = linkonce_odr hidden constant [26 x i8] c"20btAxisSweep3InternalItE\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI20btAxisSweep3InternalItE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS20btAxisSweep3InternalItE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, comdat, align 4
@_ZTI12btAxisSweep3 = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @_ZTS12btAxisSweep3, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalItE to i8*) }, comdat, align 4
@_ZTS17bt32BitAxisSweep3 = linkonce_odr hidden constant [20 x i8] c"17bt32BitAxisSweep3\00", comdat, align 1
@_ZTS20btAxisSweep3InternalIjE = linkonce_odr hidden constant [26 x i8] c"20btAxisSweep3InternalIjE\00", comdat, align 1
@_ZTI20btAxisSweep3InternalIjE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS20btAxisSweep3InternalIjE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, comdat, align 4
@_ZTI17bt32BitAxisSweep3 = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17bt32BitAxisSweep3, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalIjE to i8*) }, comdat, align 4
@_ZTV20btAxisSweep3InternalItE = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalItE to i8*), i8* bitcast (%class.btAxisSweep3Internal* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal*)* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItE10printStatsEv to i8*)] }, comdat, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV15btNullPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btNullPairCache to i8*), i8* bitcast (%class.btNullPairCache* (%class.btNullPairCache*)* @_ZN15btNullPairCacheD2Ev to i8*), i8* bitcast (void (%class.btNullPairCache*)* @_ZN15btNullPairCacheD0Ev to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i8* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*)* @_ZN15btNullPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*)* @_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv to i8*), i8* bitcast (%class.btAlignedObjectArray.5* (%class.btNullPairCache*)* @_ZN15btNullPairCache23getOverlappingPairArrayEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)* @_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher to i8*), i8* bitcast (i32 (%class.btNullPairCache*)* @_ZNK15btNullPairCache22getNumOverlappingPairsEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btOverlapFilterCallback*)* @_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback to i8*), i8* bitcast (void (%class.btNullPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)* @_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher to i8*), i8* bitcast (%struct.btBroadphasePair* (%class.btNullPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)* @_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_ to i8*), i8* bitcast (i1 (%class.btNullPairCache*)* @_ZN15btNullPairCache18hasDeferredRemovalEv to i8*), i8* bitcast (void (%class.btNullPairCache*, %class.btOverlappingPairCallback*)* @_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback to i8*), i8* bitcast (void (%class.btNullPairCache*, %class.btDispatcher*)* @_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher to i8*)] }, comdat, align 4
@_ZTS15btNullPairCache = linkonce_odr hidden constant [18 x i8] c"15btNullPairCache\00", comdat, align 1
@_ZTS22btOverlappingPairCache = linkonce_odr hidden constant [25 x i8] c"22btOverlappingPairCache\00", comdat, align 1
@_ZTS25btOverlappingPairCallback = linkonce_odr hidden constant [28 x i8] c"25btOverlappingPairCallback\00", comdat, align 1
@_ZTI25btOverlappingPairCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btOverlappingPairCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI22btOverlappingPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btOverlappingPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*) }, comdat, align 4
@_ZTI15btNullPairCache = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btNullPairCache, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*) }, comdat, align 4
@_ZTV22btOverlappingPairCache = linkonce_odr hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btOverlappingPairCache to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCache*)* @_ZN22btOverlappingPairCacheD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV25btOverlappingPairCallback = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btOverlappingPairCallback to i8*), i8* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV20btAxisSweep3InternalIjE = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btAxisSweep3InternalIjE to i8*), i8* bitcast (%class.btAxisSweep3Internal.4* (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjED2Ev to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjED0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btAxisSweep3Internal.4*)* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btVector3*, %class.btVector3*)* @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_ to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*, %class.btDispatcher*)* @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjE10printStatsEv to i8*)] }, comdat, align 4
@gOverlappingPairs = external global i32, align 4

@_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb = hidden unnamed_addr alias %class.btAxisSweep3* (%class.btAxisSweep3*, %class.btVector3*, %class.btVector3*, i16, %class.btOverlappingPairCache*, i1), %class.btAxisSweep3* (%class.btAxisSweep3*, %class.btVector3*, %class.btVector3*, i16, %class.btOverlappingPairCache*, i1)* @_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
@_ZN17bt32BitAxisSweep3C1ERK9btVector3S2_jP22btOverlappingPairCacheb = hidden unnamed_addr alias %class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*, %class.btVector3*, %class.btVector3*, i32, %class.btOverlappingPairCache*, i1), %class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*, %class.btVector3*, %class.btVector3*, i32, %class.btOverlappingPairCache*, i1)* @_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb

define hidden %class.btAxisSweep3* @_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb(%class.btAxisSweep3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i16 zeroext %maxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btAxisSweep3*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %maxHandles.addr = alloca i16, align 2
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  store %class.btAxisSweep3* %this, %class.btAxisSweep3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  store i16 %maxHandles, i16* %maxHandles.addr, align 2, !tbaa !6
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3*, %class.btAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.btAxisSweep3* %this1 to %class.btAxisSweep3Internal*
  %1 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  %3 = load i16, i16* %maxHandles.addr, align 2, !tbaa !6
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %5 = load i8, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %5 to i1
  %call = call %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb(%class.btAxisSweep3Internal* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i16 zeroext -2, i16 zeroext -1, i16 zeroext %3, %class.btOverlappingPairCache* %4, i1 zeroext %tobool)
  %6 = bitcast %class.btAxisSweep3* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV12btAxisSweep3, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4, !tbaa !11
  ret %class.btAxisSweep3* %this1
}

define linkonce_odr hidden %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb(%class.btAxisSweep3Internal* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i16 zeroext %handleMask, i16 zeroext %handleSentinel, i16 zeroext %userMaxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %handleMask.addr = alloca i16, align 2
  %handleSentinel.addr = alloca i16, align 2
  %userMaxHandles.addr = alloca i16, align 2
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  %maxHandles = alloca i16, align 2
  %ptr = alloca i8*, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %maxInt = alloca i16, align 2
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %i = alloca i16, align 2
  %i48 = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  store i16 %handleMask, i16* %handleMask.addr, align 2, !tbaa !6
  store i16 %handleSentinel, i16* %handleSentinel.addr, align 2, !tbaa !6
  store i16 %userMaxHandles, i16* %userMaxHandles.addr, align 2, !tbaa !6
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btAxisSweep3Internal* %this1, %class.btAxisSweep3Internal** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #10
  %1 = bitcast %class.btAxisSweep3Internal* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalItE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !11
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %2 = load i16, i16* %handleMask.addr, align 2, !tbaa !6
  store i16 %2, i16* %m_bpHandleMask, align 4, !tbaa !13
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %3 = load i16, i16* %handleSentinel.addr, align 2, !tbaa !6
  store i16 %3, i16* %m_handleSentinel, align 2, !tbaa !17
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMin)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMax)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_quantize)
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %4, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !19
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  store i8 0, i8* %m_ownsPairCache, align 4, !tbaa !20
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair, align 4, !tbaa !21
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* null, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %5 = bitcast i16* %maxHandles to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %5) #10
  %6 = load i16, i16* %userMaxHandles.addr, align 2, !tbaa !6
  %conv = zext i16 %6 to i32
  %add = add nsw i32 %conv, 1
  %conv5 = trunc i32 %add to i16
  store i16 %conv5, i16* %maxHandles, align 2, !tbaa !6
  %m_pairCache6 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %7 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache6, align 4, !tbaa !18
  %tobool = icmp ne %class.btOverlappingPairCache* %7, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 76, i32 16)
  store i8* %call7, i8** %ptr, align 4, !tbaa !2
  %9 = load i8*, i8** %ptr, align 4, !tbaa !2
  %10 = bitcast i8* %9 to %class.btHashedOverlappingPairCache*
  %call8 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %10)
  %11 = bitcast %class.btHashedOverlappingPairCache* %10 to %class.btOverlappingPairCache*
  %m_pairCache9 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  store %class.btOverlappingPairCache* %11, %class.btOverlappingPairCache** %m_pairCache9, align 4, !tbaa !18
  %m_ownsPairCache10 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  store i8 1, i8* %m_ownsPairCache10, align 4, !tbaa !20
  %12 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load i8, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8, !range !10
  %tobool11 = trunc i8 %13 to i1
  br i1 %tobool11, label %if.end20, label %if.then12

if.then12:                                        ; preds = %if.end
  %call13 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  %14 = bitcast i8* %call13 to %class.btNullPairCache*
  %15 = bitcast %class.btNullPairCache* %14 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %15, i8 0, i32 24, i1 false)
  %call14 = call %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* %14)
  %16 = bitcast %class.btNullPairCache* %14 to %class.btOverlappingPairCache*
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  store %class.btOverlappingPairCache* %16, %class.btOverlappingPairCache** %m_nullPairCache, align 4, !tbaa !23
  %call15 = call i8* @_Z22btAlignedAllocInternalmi(i32 196, i32 16)
  %17 = bitcast i8* %call15 to %struct.btDbvtBroadphase*
  %m_nullPairCache16 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %18 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache16, align 4, !tbaa !23
  %call17 = call %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* %17, %class.btOverlappingPairCache* %18)
  %m_raycastAccelerator18 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* %17, %struct.btDbvtBroadphase** %m_raycastAccelerator18, align 4, !tbaa !22
  %m_raycastAccelerator19 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator19, align 4, !tbaa !22
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %19, i32 0, i32 18
  store i8 1, i8* %m_deferedcollide, align 1, !tbaa !24
  br label %if.end20

if.end20:                                         ; preds = %if.then12, %if.end
  %20 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  %m_worldAabbMin21 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %21 = bitcast %class.btVector3* %m_worldAabbMin21 to i8*
  %22 = bitcast %class.btVector3* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !27
  %23 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  %m_worldAabbMax22 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %24 = bitcast %class.btVector3* %m_worldAabbMax22 to i8*
  %25 = bitcast %class.btVector3* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !27
  %26 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #10
  %m_worldAabbMax23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %m_worldAabbMin24 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMax23, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin24)
  %27 = bitcast i16* %maxInt to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %27) #10
  %m_handleSentinel25 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %28 = load i16, i16* %m_handleSentinel25, align 2, !tbaa !17
  store i16 %28, i16* %maxInt, align 2, !tbaa !6
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #10
  %30 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #10
  %31 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #10
  %32 = load i16, i16* %maxInt, align 2, !tbaa !6
  %conv28 = uitofp i16 %32 to float
  store float %conv28, float* %ref.tmp27, align 4, !tbaa !29
  %33 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load i16, i16* %maxInt, align 2, !tbaa !6
  %conv30 = uitofp i16 %34 to float
  store float %conv30, float* %ref.tmp29, align 4, !tbaa !29
  %35 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #10
  %36 = load i16, i16* %maxInt, align 2, !tbaa !6
  %conv32 = uitofp i16 %36 to float
  store float %conv32, float* %ref.tmp31, align 4, !tbaa !29
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_quantize34 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  %37 = bitcast %class.btVector3* %m_quantize34 to i8*
  %38 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !27
  %39 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #10
  %40 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #10
  %41 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #10
  %42 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #10
  %43 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #10
  %44 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %conv35 = zext i16 %44 to i32
  %45 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %conv35, i32 64)
  %46 = extractvalue { i32, i1 } %45, 1
  %47 = extractvalue { i32, i1 } %45, 0
  %48 = select i1 %46, i32 -1, i32 %47
  %call36 = call i8* @_ZN20btAxisSweep3InternalItE6HandlenaEm(i32 %48)
  %49 = bitcast i8* %call36 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  %isempty = icmp eq i32 %conv35, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end20
  %arrayctor.end = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %49, i32 %conv35
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %"class.btAxisSweep3Internal<unsigned short>::Handle"* [ %49, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call37 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZN20btAxisSweep3InternalItE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end20, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %49, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %50 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  store i16 %50, i16* %m_maxHandles, align 2, !tbaa !31
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  store i16 0, i16* %m_numHandles, align 4, !tbaa !32
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 1, i16* %m_firstFreeHandle, align 4, !tbaa !33
  %51 = bitcast i16* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %51) #10
  %m_firstFreeHandle38 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %52 = load i16, i16* %m_firstFreeHandle38, align 4, !tbaa !33
  store i16 %52, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %53 = load i16, i16* %i, align 2, !tbaa !6
  %conv39 = zext i16 %53 to i32
  %54 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %conv40 = zext i16 %54 to i32
  %cmp = icmp slt i32 %conv39, %conv40
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %55 = bitcast i16* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %55) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles41 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %56 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles41, align 4, !tbaa !30
  %57 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom = zext i16 %57 to i32
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %56, i32 %idxprom
  %58 = load i16, i16* %i, align 2, !tbaa !6
  %conv42 = zext i16 %58 to i32
  %add43 = add nsw i32 %conv42, 1
  %conv44 = trunc i32 %add43 to i16
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i16 zeroext %conv44)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %59 = load i16, i16* %i, align 2, !tbaa !6
  %inc = add i16 %59, 1
  store i16 %inc, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles45 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles45, align 4, !tbaa !30
  %61 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %conv46 = zext i16 %61 to i32
  %sub = sub nsw i32 %conv46, 1
  %arrayidx47 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %60, i32 %sub
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx47, i16 zeroext 0)
  %62 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #10
  store i32 0, i32* %i48, align 4, !tbaa !34
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc62, %for.end
  %63 = load i32, i32* %i48, align 4, !tbaa !34
  %cmp50 = icmp slt i32 %63, 3
  br i1 %cmp50, label %for.body52, label %for.cond.cleanup51

for.cond.cleanup51:                               ; preds = %for.cond49
  %64 = bitcast i32* %i48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  br label %for.end64

for.body52:                                       ; preds = %for.cond49
  %65 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %conv53 = zext i16 %65 to i32
  %mul = mul i32 4, %conv53
  %mul54 = mul i32 %mul, 2
  %call55 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul54, i32 16)
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %66 = load i32, i32* %i48, align 4, !tbaa !34
  %arrayidx56 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %66
  store i8* %call55, i8** %arrayidx56, align 4, !tbaa !2
  %67 = load i16, i16* %maxHandles, align 2, !tbaa !6
  %conv57 = zext i16 %67 to i32
  %mul58 = mul nsw i32 %conv57, 2
  %68 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %mul58, i32 4)
  %69 = extractvalue { i32, i1 } %68, 1
  %70 = extractvalue { i32, i1 } %68, 0
  %71 = select i1 %69, i32 -1, i32 %70
  %m_pEdgesRawPtr59 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %72 = load i32, i32* %i48, align 4, !tbaa !34
  %arrayidx60 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr59, i32 0, i32 %72
  %73 = load i8*, i8** %arrayidx60, align 4, !tbaa !2
  %74 = bitcast i8* %73 to %"class.btAxisSweep3Internal<unsigned short>::Edge"*
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %75 = load i32, i32* %i48, align 4, !tbaa !34
  %arrayidx61 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %75
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %74, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx61, align 4, !tbaa !2
  br label %for.inc62

for.inc62:                                        ; preds = %for.body52
  %76 = load i32, i32* %i48, align 4, !tbaa !34
  %inc63 = add nsw i32 %76, 1
  store i32 %inc63, i32* %i48, align 4, !tbaa !34
  br label %for.cond49

for.end64:                                        ; preds = %for.cond.cleanup51
  %m_pHandles65 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %77 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles65, align 4, !tbaa !30
  %arrayidx66 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %77, i32 0
  %78 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx66 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %78, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4, !tbaa !35
  %79 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc92, %for.end64
  %80 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp68 = icmp slt i32 %80, 3
  br i1 %cmp68, label %for.body70, label %for.cond.cleanup69

for.cond.cleanup69:                               ; preds = %for.cond67
  %81 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  br label %for.end94

for.body70:                                       ; preds = %for.cond67
  %m_pHandles71 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %82 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles71, align 4, !tbaa !30
  %arrayidx72 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %82, i32 0
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx72, i32 0, i32 1
  %83 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx73 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %83
  store i16 0, i16* %arrayidx73, align 2, !tbaa !6
  %m_pHandles74 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %84 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles74, align 4, !tbaa !30
  %arrayidx75 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %84, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx75, i32 0, i32 2
  %85 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx76 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %85
  store i16 1, i16* %arrayidx76, align 2, !tbaa !6
  %m_pEdges77 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %86 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx78 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges77, i32 0, i32 %86
  %87 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx78, align 4, !tbaa !2
  %arrayidx79 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %87, i32 0
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx79, i32 0, i32 0
  store i16 0, i16* %m_pos, align 2, !tbaa !37
  %m_pEdges80 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %88 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx81 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges80, i32 0, i32 %88
  %89 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx81, align 4, !tbaa !2
  %arrayidx82 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %89, i32 0
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx82, i32 0, i32 1
  store i16 0, i16* %m_handle, align 2, !tbaa !39
  %m_handleSentinel83 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %90 = load i16, i16* %m_handleSentinel83, align 2, !tbaa !17
  %m_pEdges84 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %91 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx85 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges84, i32 0, i32 %91
  %92 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx85, align 4, !tbaa !2
  %arrayidx86 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %92, i32 1
  %m_pos87 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx86, i32 0, i32 0
  store i16 %90, i16* %m_pos87, align 2, !tbaa !37
  %m_pEdges88 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %93 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx89 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges88, i32 0, i32 %93
  %94 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx89, align 4, !tbaa !2
  %arrayidx90 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %94, i32 1
  %m_handle91 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx90, i32 0, i32 1
  store i16 0, i16* %m_handle91, align 2, !tbaa !39
  br label %for.inc92

for.inc92:                                        ; preds = %for.body70
  %95 = load i32, i32* %axis, align 4, !tbaa !34
  %inc93 = add nsw i32 %95, 1
  store i32 %inc93, i32* %axis, align 4, !tbaa !34
  br label %for.cond67

for.end94:                                        ; preds = %for.cond.cleanup69
  %96 = bitcast i16* %maxInt to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %96) #10
  %97 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #10
  %98 = bitcast i16* %maxHandles to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %98) #10
  %99 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %retval, align 4
  ret %class.btAxisSweep3Internal* %99
}

define hidden %class.bt32BitAxisSweep3* @_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb(%class.bt32BitAxisSweep3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i32 %maxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.bt32BitAxisSweep3*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %maxHandles.addr = alloca i32, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  store %class.bt32BitAxisSweep3* %this, %class.bt32BitAxisSweep3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  store i32 %maxHandles, i32* %maxHandles.addr, align 4, !tbaa !34
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8
  %this1 = load %class.bt32BitAxisSweep3*, %class.bt32BitAxisSweep3** %this.addr, align 4
  %0 = bitcast %class.bt32BitAxisSweep3* %this1 to %class.btAxisSweep3Internal.4*
  %1 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  %3 = load i32, i32* %maxHandles.addr, align 4, !tbaa !34
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %5 = load i8, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %5 to i1
  %call = call %class.btAxisSweep3Internal.4* @_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb(%class.btAxisSweep3Internal.4* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 -2, i32 2147483647, i32 %3, %class.btOverlappingPairCache* %4, i1 zeroext %tobool)
  %6 = bitcast %class.bt32BitAxisSweep3* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV17bt32BitAxisSweep3, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4, !tbaa !11
  ret %class.bt32BitAxisSweep3* %this1
}

define linkonce_odr hidden %class.btAxisSweep3Internal.4* @_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb(%class.btAxisSweep3Internal.4* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %worldAabbMax, i32 %handleMask, i32 %handleSentinel, i32 %userMaxHandles, %class.btOverlappingPairCache* %pairCache, i1 zeroext %disableRaycastAccelerator) unnamed_addr #0 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal.4*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %worldAabbMin.addr = alloca %class.btVector3*, align 4
  %worldAabbMax.addr = alloca %class.btVector3*, align 4
  %handleMask.addr = alloca i32, align 4
  %handleSentinel.addr = alloca i32, align 4
  %userMaxHandles.addr = alloca i32, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %disableRaycastAccelerator.addr = alloca i8, align 1
  %maxHandles = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %maxInt = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %i = alloca i32, align 4
  %i40 = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMin, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %worldAabbMax, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  store i32 %handleMask, i32* %handleMask.addr, align 4, !tbaa !34
  store i32 %handleSentinel, i32* %handleSentinel.addr, align 4, !tbaa !34
  store i32 %userMaxHandles, i32* %userMaxHandles.addr, align 4, !tbaa !34
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  %frombool = zext i1 %disableRaycastAccelerator to i8
  store i8 %frombool, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  store %class.btAxisSweep3Internal.4* %this1, %class.btAxisSweep3Internal.4** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal.4* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #10
  %1 = bitcast %class.btAxisSweep3Internal.4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalIjE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !11
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %2 = load i32, i32* %handleMask.addr, align 4, !tbaa !34
  store i32 %2, i32* %m_bpHandleMask, align 4, !tbaa !40
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %3 = load i32, i32* %handleSentinel.addr, align 4, !tbaa !34
  store i32 %3, i32* %m_handleSentinel, align 4, !tbaa !42
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMin)
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_worldAabbMax)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_quantize)
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCache* %4, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  store %class.btOverlappingPairCallback* null, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !44
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 14
  store i8 0, i8* %m_ownsPairCache, align 4, !tbaa !45
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair, align 4, !tbaa !46
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* null, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %5 = bitcast i32* %maxHandles to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load i32, i32* %userMaxHandles.addr, align 4, !tbaa !34
  %add = add i32 %6, 1
  store i32 %add, i32* %maxHandles, align 4, !tbaa !34
  %m_pairCache5 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %7 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache5, align 4, !tbaa !43
  %tobool = icmp ne %class.btOverlappingPairCache* %7, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 76, i32 16)
  store i8* %call6, i8** %ptr, align 4, !tbaa !2
  %9 = load i8*, i8** %ptr, align 4, !tbaa !2
  %10 = bitcast i8* %9 to %class.btHashedOverlappingPairCache*
  %call7 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %10)
  %11 = bitcast %class.btHashedOverlappingPairCache* %10 to %class.btOverlappingPairCache*
  %m_pairCache8 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  store %class.btOverlappingPairCache* %11, %class.btOverlappingPairCache** %m_pairCache8, align 4, !tbaa !43
  %m_ownsPairCache9 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 14
  store i8 1, i8* %m_ownsPairCache9, align 4, !tbaa !45
  %12 = bitcast i8** %ptr to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load i8, i8* %disableRaycastAccelerator.addr, align 1, !tbaa !8, !range !10
  %tobool10 = trunc i8 %13 to i1
  br i1 %tobool10, label %if.end19, label %if.then11

if.then11:                                        ; preds = %if.end
  %call12 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  %14 = bitcast i8* %call12 to %class.btNullPairCache*
  %15 = bitcast %class.btNullPairCache* %14 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %15, i8 0, i32 24, i1 false)
  %call13 = call %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* %14)
  %16 = bitcast %class.btNullPairCache* %14 to %class.btOverlappingPairCache*
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 17
  store %class.btOverlappingPairCache* %16, %class.btOverlappingPairCache** %m_nullPairCache, align 4, !tbaa !48
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 196, i32 16)
  %17 = bitcast i8* %call14 to %struct.btDbvtBroadphase*
  %m_nullPairCache15 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 17
  %18 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache15, align 4, !tbaa !48
  %call16 = call %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* %17, %class.btOverlappingPairCache* %18)
  %m_raycastAccelerator17 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  store %struct.btDbvtBroadphase* %17, %struct.btDbvtBroadphase** %m_raycastAccelerator17, align 4, !tbaa !47
  %m_raycastAccelerator18 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator18, align 4, !tbaa !47
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %19, i32 0, i32 18
  store i8 1, i8* %m_deferedcollide, align 1, !tbaa !24
  br label %if.end19

if.end19:                                         ; preds = %if.then11, %if.end
  %20 = load %class.btVector3*, %class.btVector3** %worldAabbMin.addr, align 4, !tbaa !2
  %m_worldAabbMin20 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 3
  %21 = bitcast %class.btVector3* %m_worldAabbMin20 to i8*
  %22 = bitcast %class.btVector3* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false), !tbaa.struct !27
  %23 = load %class.btVector3*, %class.btVector3** %worldAabbMax.addr, align 4, !tbaa !2
  %m_worldAabbMax21 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 4
  %24 = bitcast %class.btVector3* %m_worldAabbMax21 to i8*
  %25 = bitcast %class.btVector3* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !27
  %26 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #10
  %m_worldAabbMax22 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 4
  %m_worldAabbMin23 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMax22, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin23)
  %27 = bitcast i32* %maxInt to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #10
  %m_handleSentinel24 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %28 = load i32, i32* %m_handleSentinel24, align 4, !tbaa !42
  store i32 %28, i32* %maxInt, align 4, !tbaa !34
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %29) #10
  %30 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %30) #10
  %31 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #10
  %32 = load i32, i32* %maxInt, align 4, !tbaa !34
  %conv = uitofp i32 %32 to float
  store float %conv, float* %ref.tmp26, align 4, !tbaa !29
  %33 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load i32, i32* %maxInt, align 4, !tbaa !34
  %conv28 = uitofp i32 %34 to float
  store float %conv28, float* %ref.tmp27, align 4, !tbaa !29
  %35 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #10
  %36 = load i32, i32* %maxInt, align 4, !tbaa !34
  %conv30 = uitofp i32 %36 to float
  store float %conv30, float* %ref.tmp29, align 4, !tbaa !29
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_quantize32 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 5
  %37 = bitcast %class.btVector3* %m_quantize32 to i8*
  %38 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false), !tbaa.struct !27
  %39 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %39) #10
  %40 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %40) #10
  %41 = bitcast float* %ref.tmp26 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %41) #10
  %42 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #10
  %43 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #10
  %44 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %45 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %44, i32 76)
  %46 = extractvalue { i32, i1 } %45, 1
  %47 = extractvalue { i32, i1 } %45, 0
  %48 = select i1 %46, i32 -1, i32 %47
  %call33 = call i8* @_ZN20btAxisSweep3InternalIjE6HandlenaEm(i32 %48)
  %49 = bitcast i8* %call33 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  %isempty = icmp eq i32 %44, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end19
  %arrayctor.end = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %49, i32 %44
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %"class.btAxisSweep3Internal<unsigned int>::Handle"* [ %49, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call34 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZN20btAxisSweep3InternalIjE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end19, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %49, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %50 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 7
  store i32 %50, i32* %m_maxHandles, align 4, !tbaa !50
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  store i32 0, i32* %m_numHandles, align 4, !tbaa !51
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  store i32 1, i32* %m_firstFreeHandle, align 4, !tbaa !52
  %51 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #10
  %m_firstFreeHandle35 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  %52 = load i32, i32* %m_firstFreeHandle35, align 4, !tbaa !52
  store i32 %52, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %53 = load i32, i32* %i, align 4, !tbaa !34
  %54 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %cmp = icmp ult i32 %53, %54
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %55 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles36 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %56 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles36, align 4, !tbaa !49
  %57 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %56, i32 %57
  %58 = load i32, i32* %i, align 4, !tbaa !34
  %add37 = add i32 %58, 1
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 %add37)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %59 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add i32 %59, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles38 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles38, align 4, !tbaa !49
  %61 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %sub = sub i32 %61, 1
  %arrayidx39 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %60, i32 %sub
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx39, i32 0)
  %62 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #10
  store i32 0, i32* %i40, align 4, !tbaa !34
  br label %for.cond41

for.cond41:                                       ; preds = %for.inc52, %for.end
  %63 = load i32, i32* %i40, align 4, !tbaa !34
  %cmp42 = icmp slt i32 %63, 3
  br i1 %cmp42, label %for.body44, label %for.cond.cleanup43

for.cond.cleanup43:                               ; preds = %for.cond41
  %64 = bitcast i32* %i40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  br label %for.end54

for.body44:                                       ; preds = %for.cond41
  %65 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %mul = mul i32 8, %65
  %mul45 = mul i32 %mul, 2
  %call46 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul45, i32 16)
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 11
  %66 = load i32, i32* %i40, align 4, !tbaa !34
  %arrayidx47 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %66
  store i8* %call46, i8** %arrayidx47, align 4, !tbaa !2
  %67 = load i32, i32* %maxHandles, align 4, !tbaa !34
  %mul48 = mul i32 %67, 2
  %68 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %mul48, i32 8)
  %69 = extractvalue { i32, i1 } %68, 1
  %70 = extractvalue { i32, i1 } %68, 0
  %71 = select i1 %69, i32 -1, i32 %70
  %m_pEdgesRawPtr49 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 11
  %72 = load i32, i32* %i40, align 4, !tbaa !34
  %arrayidx50 = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr49, i32 0, i32 %72
  %73 = load i8*, i8** %arrayidx50, align 4, !tbaa !2
  %74 = bitcast i8* %73 to %"class.btAxisSweep3Internal<unsigned int>::Edge"*
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %75 = load i32, i32* %i40, align 4, !tbaa !34
  %arrayidx51 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %75
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %74, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx51, align 4, !tbaa !2
  br label %for.inc52

for.inc52:                                        ; preds = %for.body44
  %76 = load i32, i32* %i40, align 4, !tbaa !34
  %inc53 = add nsw i32 %76, 1
  store i32 %inc53, i32* %i40, align 4, !tbaa !34
  br label %for.cond41

for.end54:                                        ; preds = %for.cond.cleanup43
  %m_pHandles55 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %77 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles55, align 4, !tbaa !49
  %arrayidx56 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %77, i32 0
  %78 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx56 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %78, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4, !tbaa !35
  %79 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc82, %for.end54
  %80 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp58 = icmp slt i32 %80, 3
  br i1 %cmp58, label %for.body60, label %for.cond.cleanup59

for.cond.cleanup59:                               ; preds = %for.cond57
  %81 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  br label %for.end84

for.body60:                                       ; preds = %for.cond57
  %m_pHandles61 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %82 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles61, align 4, !tbaa !49
  %arrayidx62 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %82, i32 0
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx62, i32 0, i32 1
  %83 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx63 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %83
  store i32 0, i32* %arrayidx63, align 4, !tbaa !34
  %m_pHandles64 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %84 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles64, align 4, !tbaa !49
  %arrayidx65 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %84, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx65, i32 0, i32 2
  %85 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %85
  store i32 1, i32* %arrayidx66, align 4, !tbaa !34
  %m_pEdges67 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %86 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx68 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges67, i32 0, i32 %86
  %87 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx68, align 4, !tbaa !2
  %arrayidx69 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %87, i32 0
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx69, i32 0, i32 0
  store i32 0, i32* %m_pos, align 4, !tbaa !53
  %m_pEdges70 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %88 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx71 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges70, i32 0, i32 %88
  %89 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx71, align 4, !tbaa !2
  %arrayidx72 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %89, i32 0
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx72, i32 0, i32 1
  store i32 0, i32* %m_handle, align 4, !tbaa !55
  %m_handleSentinel73 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %90 = load i32, i32* %m_handleSentinel73, align 4, !tbaa !42
  %m_pEdges74 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %91 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx75 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges74, i32 0, i32 %91
  %92 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx75, align 4, !tbaa !2
  %arrayidx76 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %92, i32 1
  %m_pos77 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx76, i32 0, i32 0
  store i32 %90, i32* %m_pos77, align 4, !tbaa !53
  %m_pEdges78 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %93 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx79 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges78, i32 0, i32 %93
  %94 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx79, align 4, !tbaa !2
  %arrayidx80 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %94, i32 1
  %m_handle81 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx80, i32 0, i32 1
  store i32 0, i32* %m_handle81, align 4, !tbaa !55
  br label %for.inc82

for.inc82:                                        ; preds = %for.body60
  %95 = load i32, i32* %axis, align 4, !tbaa !34
  %inc83 = add nsw i32 %95, 1
  store i32 %inc83, i32* %axis, align 4, !tbaa !34
  br label %for.cond57

for.end84:                                        ; preds = %for.cond.cleanup59
  %96 = bitcast i32* %maxInt to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #10
  %97 = bitcast %class.btVector3* %aabbSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %97) #10
  %98 = bitcast i32* %maxHandles to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %98) #10
  %99 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %retval, align 4
  ret %class.btAxisSweep3Internal.4* %99
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItED2Ev(%class.btAxisSweep3Internal* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  store %class.btAxisSweep3Internal* %this1, %class.btAxisSweep3Internal** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalItE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache, align 4, !tbaa !23
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !11
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #10
  %m_nullPairCache2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 17
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache2, align 4, !tbaa !23
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4, !tbaa !22
  %8 = bitcast %struct.btDbvtBroadphase* %7 to %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)***
  %vtable4 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)**, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*** %8, align 4, !tbaa !11
  %vfn5 = getelementptr inbounds %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vtable4, i64 0
  %9 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vfn5, align 4
  %call6 = call %struct.btDbvtBroadphase* %9(%struct.btDbvtBroadphase* %7) #10
  %m_raycastAccelerator7 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator7, align 4, !tbaa !22
  %11 = bitcast %struct.btDbvtBroadphase* %10 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  store i32 2, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !34
  %cmp = icmp sge i32 %13, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 11
  %15 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %15
  %16 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %16)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !34
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %isnull = icmp eq %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.end
  %19 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18 to i8*
  call void @_ZN20btAxisSweep3InternalItE6HandledaEPv(i8* %19) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.end
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 14
  %20 = load i8, i8* %m_ownsPairCache, align 4, !tbaa !20, !range !10
  %tobool8 = trunc i8 %20 to i1
  br i1 %tobool8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %delete.end
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %21 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %22 = bitcast %class.btOverlappingPairCache* %21 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable10 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %22, align 4, !tbaa !11
  %vfn11 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable10, i64 0
  %23 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn11, align 4
  %call12 = call %class.btOverlappingPairCache* %23(%class.btOverlappingPairCache* %21) #10
  %m_pairCache13 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %24 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache13, align 4, !tbaa !18
  %25 = bitcast %class.btOverlappingPairCache* %24 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %25)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %delete.end
  %26 = bitcast %class.btAxisSweep3Internal* %this1 to %class.btBroadphaseInterface*
  %call15 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %26) #10
  %27 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %retval, align 4
  ret %class.btAxisSweep3Internal* %27
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN12btAxisSweep3D0Ev(%class.btAxisSweep3* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3*, align 4
  store %class.btAxisSweep3* %this, %class.btAxisSweep3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3*, %class.btAxisSweep3** %this.addr, align 4
  %call = call %class.btAxisSweep3* bitcast (%class.btAxisSweep3Internal* (%class.btAxisSweep3Internal*)* @_ZN20btAxisSweep3InternalItED2Ev to %class.btAxisSweep3* (%class.btAxisSweep3*)*)(%class.btAxisSweep3* %this1) #10
  %0 = bitcast %class.btAxisSweep3* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %dispatcher, i8* %multiSapProxy) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %multiSapProxy.addr = alloca i8*, align 4
  %handleId = alloca i16, align 2
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %rayProxy = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !34
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast i16* %handleId to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %4 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %5 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %call = call zeroext i16 @_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_(%class.btAxisSweep3Internal* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i16 signext %4, i16 signext %5, %class.btDispatcher* %6, i8* %7)
  store i16 %call, i16* %handleId, align 2, !tbaa !6
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %9 = load i16, i16* %handleId, align 2, !tbaa !6
  %call2 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %9)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = bitcast %struct.btBroadphaseProxy** %rayProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %12 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4, !tbaa !22
  %13 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %15 = load i32, i32* %shapeType.addr, align 4, !tbaa !34
  %16 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %17 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %18 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %19 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %20 = bitcast %struct.btDbvtBroadphase* %12 to %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)***
  %vtable = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)**, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*** %20, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vtable, i64 2
  %21 = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vfn, align 4
  %call4 = call %struct.btBroadphaseProxy* %21(%struct.btDbvtBroadphase* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, i32 %15, i8* %16, i16 signext %17, i16 signext %18, %class.btDispatcher* %19, i8* null)
  store %struct.btBroadphaseProxy* %call4, %struct.btBroadphaseProxy** %rayProxy, align 4, !tbaa !2
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %rayProxy, align 4, !tbaa !2
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %23, i32 0, i32 3
  store %struct.btBroadphaseProxy* %22, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !56
  %24 = bitcast %struct.btBroadphaseProxy** %rayProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %25 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %25 to %struct.btBroadphaseProxy*
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  %28 = bitcast i16* %handleId to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %28) #10
  ret %struct.btBroadphaseProxy* %26
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %3 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %4 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !22
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %5, i32 0, i32 3
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !56
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %8 = bitcast %struct.btDbvtBroadphase* %4 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %8, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %9 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %9(%struct.btDbvtBroadphase* %4, %struct.btBroadphaseProxy* %6, %class.btDispatcher* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %11 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %10 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 4
  %12 = load i32, i32* %m_uniqueId, align 4, !tbaa !58
  %conv = trunc i32 %12 to i16
  %13 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher(%class.btAxisSweep3Internal* %this1, i16 zeroext %conv, %class.btDispatcher* %13)
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %4 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 5
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  %7 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !27
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %10 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %9 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 6
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  %12 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !27
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %13 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 4
  %15 = load i32, i32* %m_uniqueId, align 4, !tbaa !58
  %conv = trunc i32 %15 to i16
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %18 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal* %this1, i16 zeroext %conv, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btDispatcher* %18)
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %19, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %20 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !22
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %21, i32 0, i32 3
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !56
  %23 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %25 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %26 = bitcast %struct.btDbvtBroadphase* %20 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %26, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable, i64 4
  %27 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn, align 4
  call void %27(%struct.btDbvtBroadphase* %20, %struct.btBroadphaseProxy* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btDispatcher* %25)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %28 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %3 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 5
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !27
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 6
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !27
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %axis = alloca i16, align 2
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !22
  %2 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %7, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %8 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  br label %if.end17

if.else:                                          ; preds = %entry
  %9 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #10
  store i16 0, i16* %axis, align 2, !tbaa !6
  %10 = bitcast i16* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %10) #10
  store i16 1, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %11 = load i16, i16* %i, align 2, !tbaa !6
  %conv = zext i16 %11 to i32
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %12 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %conv3 = zext i16 %12 to i32
  %mul = mul nsw i32 %conv3, 2
  %add = add nsw i32 %mul, 1
  %cmp = icmp slt i32 %conv, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i16* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %13) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %14 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom = zext i16 %14 to i32
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %16 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom4 = zext i16 %16 to i32
  %arrayidx5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %15, i32 %idxprom4
  %call = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx5)
  %tobool6 = icmp ne i16 %call, 0
  br i1 %tobool6, label %if.then7, label %if.end

if.then7:                                         ; preds = %for.body
  %17 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %18 = bitcast %struct.btBroadphaseRayCallback* %17 to %struct.btBroadphaseAabbCallback*
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %19 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom9 = zext i16 %19 to i32
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges8, i32 0, i32 %idxprom9
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx10, align 4, !tbaa !2
  %21 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom11 = zext i16 %21 to i32
  %arrayidx12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %20, i32 %idxprom11
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx12, i32 0, i32 1
  %22 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call13 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %22)
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call13 to %struct.btBroadphaseProxy*
  %24 = bitcast %struct.btBroadphaseAabbCallback* %18 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable14 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %24, align 4, !tbaa !11
  %vfn15 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable14, i64 2
  %25 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn15, align 4
  %call16 = call zeroext i1 %25(%struct.btBroadphaseAabbCallback* %18, %struct.btBroadphaseProxy* %23)
  br label %if.end

if.end:                                           ; preds = %if.then7, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i16, i16* %i, align 2, !tbaa !6
  %inc = add i16 %26, 1
  store i16 %inc, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %27) #10
  br label %if.end17

if.end17:                                         ; preds = %for.end, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %axis = alloca i16, align 2
  %i = alloca i16, align 2
  %handle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !22
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !22
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*** %5, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vtable, i64 7
  %6 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vfn, align 4
  call void %6(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %4)
  br label %if.end20

if.else:                                          ; preds = %entry
  %7 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %7) #10
  store i16 0, i16* %axis, align 2, !tbaa !6
  %8 = bitcast i16* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %8) #10
  store i16 1, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %9 = load i16, i16* %i, align 2, !tbaa !6
  %conv = zext i16 %9 to i32
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %10 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %conv3 = zext i16 %10 to i32
  %mul = mul nsw i32 %conv3, 2
  %add = add nsw i32 %mul, 1
  %cmp = icmp slt i32 %conv, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i16* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %11) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %12 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom = zext i16 %12 to i32
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %14 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom4 = zext i16 %14 to i32
  %arrayidx5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 %idxprom4
  %call = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx5)
  %tobool6 = icmp ne i16 %call, 0
  br i1 %tobool6, label %if.then7, label %if.end19

if.then7:                                         ; preds = %for.body
  %15 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %16 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom9 = zext i16 %16 to i32
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges8, i32 0, i32 %idxprom9
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx10, align 4, !tbaa !2
  %18 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom11 = zext i16 %18 to i32
  %arrayidx12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %17, i32 %idxprom11
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx12, i32 0, i32 1
  %19 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call13 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %19)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call13, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %20 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %21 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %22 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %22 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %23, i32 0, i32 5
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %25 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %24 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 6
  %call14 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then7
  %26 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %27 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle, align 4, !tbaa !2
  %28 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %27 to %struct.btBroadphaseProxy*
  %29 = bitcast %struct.btBroadphaseAabbCallback* %26 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable16 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %29, align 4, !tbaa !11
  %vfn17 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable16, i64 2
  %30 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn17, align 4
  %call18 = call zeroext i1 %30(%struct.btBroadphaseAabbCallback* %26, %struct.btBroadphaseProxy* %28)
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.then7
  %31 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #10
  br label %if.end19

if.end19:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %32 = load i16, i16* %i, align 2, !tbaa !6
  %inc = add i16 %32, 1
  store i16 %inc, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %33) #10
  br label %if.end20

if.end20:                                         ; preds = %for.end, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.5*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp7 = alloca %struct.btBroadphasePair, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp33 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp37 = alloca %struct.btBroadphasePair, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %1 = bitcast %class.btOverlappingPairCache* %0 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %1, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %2 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %2(%class.btOverlappingPairCache* %0)
  br i1 %call, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btAlignedObjectArray.5** %overlappingPairArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_pairCache2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4, !tbaa !18
  %5 = bitcast %class.btOverlappingPairCache* %4 to %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)***
  %vtable3 = load %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*** %5, align 4, !tbaa !11
  %vfn4 = getelementptr inbounds %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)** %vtable3, i64 7
  %6 = load %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.5* %6(%class.btOverlappingPairCache* %4)
  store %class.btAlignedObjectArray.5* %call5, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %7 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %8 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %8) #10
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.5* %7, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %9 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %9) #10
  %10 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %11 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %11)
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %12 = load i32, i32* %m_invalidPair, align 4, !tbaa !21
  %sub = sub nsw i32 %call6, %12
  %13 = bitcast %struct.btBroadphasePair* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #10
  %call8 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp7)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %10, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %14 = bitcast %struct.btBroadphasePair* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #10
  %m_invalidPair9 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair9, align 4, !tbaa !21
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %16 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #10
  %call10 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !62
  store i32 0, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %17 = load i32, i32* %i, align 4, !tbaa !34
  %18 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %18)
  %cmp = icmp slt i32 %17, %call11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !34
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %20, i32 %21)
  store %struct.btBroadphasePair* %call12, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isDuplicate) #10
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %call13 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %22, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call13 to i8
  store i8 %frombool, i8* %isDuplicate, align 1, !tbaa !8
  %23 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %24 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %25 = bitcast %struct.btBroadphasePair* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !63
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %needsRemoval) #10
  store i8 0, i8* %needsRemoval, align 1, !tbaa !8
  %26 = load i8, i8* %isDuplicate, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %26 to i1
  br i1 %tobool, label %if.else21, label %if.then14

if.then14:                                        ; preds = %for.body
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasOverlap) #10
  %27 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy015 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %27, i32 0, i32 0
  %28 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy015, align 4, !tbaa !59
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy116 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %29, i32 0, i32 1
  %30 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy116, align 4, !tbaa !61
  %call17 = call zeroext i1 @_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal* %this1, %struct.btBroadphaseProxy* %28, %struct.btBroadphaseProxy* %30)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %hasOverlap, align 1, !tbaa !8
  %31 = load i8, i8* %hasOverlap, align 1, !tbaa !8, !range !10
  %tobool19 = trunc i8 %31 to i1
  br i1 %tobool19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.then14
  store i8 0, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end

if.else:                                          ; preds = %if.then14
  store i8 1, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then20
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasOverlap) #10
  br label %if.end22

if.else21:                                        ; preds = %for.body
  store i8 1, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end22

if.end22:                                         ; preds = %if.else21, %if.end
  %32 = load i8, i8* %needsRemoval, align 1, !tbaa !8, !range !10
  %tobool23 = trunc i8 %32 to i1
  br i1 %tobool23, label %if.then24, label %if.end31

if.then24:                                        ; preds = %if.end22
  %m_pairCache25 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %33 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4, !tbaa !18
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %35 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %36 = bitcast %class.btOverlappingPairCache* %33 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable26 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %36, align 4, !tbaa !11
  %vfn27 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable26, i64 8
  %37 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn27, align 4
  call void %37(%class.btOverlappingPairCache* %33, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %34, %class.btDispatcher* %35)
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy028 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy028, align 4, !tbaa !59
  %39 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy129 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %39, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy129, align 4, !tbaa !61
  %m_invalidPair30 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %40 = load i32, i32* %m_invalidPair30, align 4, !tbaa !21
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %m_invalidPair30, align 4, !tbaa !21
  %41 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !34
  %dec = add nsw i32 %41, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !34
  br label %if.end31

if.end31:                                         ; preds = %if.then24, %if.end22
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %needsRemoval) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isDuplicate) #10
  %42 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %43 = load i32, i32* %i, align 4, !tbaa !34
  %inc32 = add nsw i32 %43, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %45 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %45) #10
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.5* %44, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp33)
  %46 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %46) #10
  %47 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %48 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call34 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %48)
  %m_invalidPair35 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  %49 = load i32, i32* %m_invalidPair35, align 4, !tbaa !21
  %sub36 = sub nsw i32 %call34, %49
  %50 = bitcast %struct.btBroadphasePair* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #10
  %call38 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp37)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %47, i32 %sub36, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %51 = bitcast %struct.btBroadphasePair* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #10
  %m_invalidPair39 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair39, align 4, !tbaa !21
  %52 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #10
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  %54 = bitcast %class.btAlignedObjectArray.5** %overlappingPairArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  br label %if.end40

if.end40:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_worldAabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !27
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_worldAabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !27
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher(%class.btAxisSweep3Internal* %this, %class.btDispatcher* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %1 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %conv = zext i16 %1 to i32
  %cmp = icmp eq i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 1, i16* %m_firstFreeHandle, align 4, !tbaa !33
  %2 = bitcast i16* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %2) #10
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %3 = load i16, i16* %m_firstFreeHandle2, align 4, !tbaa !33
  store i16 %3, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i16, i16* %i, align 2, !tbaa !6
  %conv3 = zext i16 %4 to i32
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  %5 = load i16, i16* %m_maxHandles, align 2, !tbaa !31
  %conv4 = zext i16 %5 to i32
  %cmp5 = icmp slt i32 %conv3, %conv4
  br i1 %cmp5, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i16* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %6) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %8 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom = zext i16 %8 to i32
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %7, i32 %idxprom
  %9 = load i16, i16* %i, align 2, !tbaa !6
  %conv6 = zext i16 %9 to i32
  %add = add nsw i32 %conv6, 1
  %conv7 = trunc i32 %add to i16
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i16 zeroext %conv7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i16, i16* %i, align 2, !tbaa !6
  %inc = add i16 %10, 1
  store i16 %inc, i16* %i, align 2, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles8, align 4, !tbaa !30
  %m_maxHandles9 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 7
  %12 = load i16, i16* %m_maxHandles9, align 2, !tbaa !31
  %conv10 = zext i16 %12 to i32
  %sub = sub nsw i32 %conv10, 1
  %arrayidx11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %11, i32 %sub
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx11, i16 zeroext 0)
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE10printStatsEv(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN17bt32BitAxisSweep3D0Ev(%class.bt32BitAxisSweep3* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.bt32BitAxisSweep3*, align 4
  store %class.bt32BitAxisSweep3* %this, %class.bt32BitAxisSweep3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.bt32BitAxisSweep3*, %class.bt32BitAxisSweep3** %this.addr, align 4
  %call = call %class.bt32BitAxisSweep3* bitcast (%class.btAxisSweep3Internal.4* (%class.btAxisSweep3Internal.4*)* @_ZN20btAxisSweep3InternalIjED2Ev to %class.bt32BitAxisSweep3* (%class.bt32BitAxisSweep3*)*)(%class.bt32BitAxisSweep3* %this1) #10
  %0 = bitcast %class.bt32BitAxisSweep3* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %0) #10
  ret void
}

define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_(%class.btAxisSweep3Internal.4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %dispatcher, i8* %multiSapProxy) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %multiSapProxy.addr = alloca i8*, align 4
  %handleId = alloca i32, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %rayProxy = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i32 %shapeType, i32* %shapeType.addr, align 4, !tbaa !34
  store i8* %userPtr, i8** %userPtr.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast i32* %handleId to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %3 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %4 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %5 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %7 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %call = call i32 @_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_(%class.btAxisSweep3Internal.4* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i16 signext %4, i16 signext %5, %class.btDispatcher* %6, i8* %7)
  store i32 %call, i32* %handleId, align 4, !tbaa !34
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  %9 = load i32, i32* %handleId, align 4, !tbaa !34
  %call2 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %9)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %10, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = bitcast %struct.btBroadphaseProxy** %rayProxy to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #10
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %12 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4, !tbaa !47
  %13 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %14 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %15 = load i32, i32* %shapeType.addr, align 4, !tbaa !34
  %16 = load i8*, i8** %userPtr.addr, align 4, !tbaa !2
  %17 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %18 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %19 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %20 = bitcast %struct.btDbvtBroadphase* %12 to %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)***
  %vtable = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)**, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*** %20, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vtable, i64 2
  %21 = load %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)*, %struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i16, i16, %class.btDispatcher*, i8*)** %vfn, align 4
  %call4 = call %struct.btBroadphaseProxy* %21(%struct.btDbvtBroadphase* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, i32 %15, i8* %16, i16 signext %17, i16 signext %18, %class.btDispatcher* %19, i8* null)
  store %struct.btBroadphaseProxy* %call4, %struct.btBroadphaseProxy** %rayProxy, align 4, !tbaa !2
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %rayProxy, align 4, !tbaa !2
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %23, i32 0, i32 3
  store %struct.btBroadphaseProxy* %22, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !64
  %24 = bitcast %struct.btBroadphaseProxy** %rayProxy to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %24) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %25 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %26 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %25 to %struct.btBroadphaseProxy*
  %27 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  %28 = bitcast i32* %handleId to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  ret %struct.btBroadphaseProxy* %26
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btAxisSweep3Internal.4* %this, %struct.btBroadphaseProxy* %proxy, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %3 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %4 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !47
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %5, i32 0, i32 3
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !64
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %8 = bitcast %struct.btDbvtBroadphase* %4 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %8, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %9 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %9(%struct.btDbvtBroadphase* %4, %struct.btBroadphaseProxy* %6, %class.btDispatcher* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %11 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %10 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %11, i32 0, i32 4
  %12 = load i32, i32* %m_uniqueId, align 4, !tbaa !58
  %13 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher(%class.btAxisSweep3Internal.4* %this1, i32 %12, %class.btDispatcher* %13)
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher(%class.btAxisSweep3Internal.4* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %4 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %4 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 5
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  %7 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !27
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %10 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %9 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 6
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  %12 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !27
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %13 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 4
  %15 = load i32, i32* %m_uniqueId, align 4, !tbaa !58
  %16 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %17 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %18 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal.4* %this1, i32 %15, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btDispatcher* %18)
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %19, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %20 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !47
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %m_dbvtProxy = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %21, i32 0, i32 3
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_dbvtProxy, align 4, !tbaa !64
  %23 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %24 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %25 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %26 = bitcast %struct.btDbvtBroadphase* %20 to void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)**, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*** %26, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vtable, i64 4
  %27 = load void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)*, void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)** %vfn, align 4
  call void %27(%struct.btDbvtBroadphase* %20, %struct.btBroadphaseProxy* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btDispatcher* %25)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %28 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %28) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_(%class.btAxisSweep3Internal.4* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %3 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 5
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !27
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 6
  %10 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !27
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_(%class.btAxisSweep3Internal.4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %axis = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !47
  %2 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %7 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %7, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %8 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %8(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  br label %if.end13

if.else:                                          ; preds = %entry
  %9 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  store i32 1, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %11 = load i32, i32* %i, align 4, !tbaa !34
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %12 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %mul = mul i32 %12, 2
  %add = add i32 %mul, 1
  %cmp = icmp ult i32 %11, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %14 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %14
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %16 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %15, i32 %16
  %call = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx3)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end

if.then5:                                         ; preds = %for.body
  %17 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4, !tbaa !2
  %18 = bitcast %struct.btBroadphaseRayCallback* %17 to %struct.btBroadphaseAabbCallback*
  %m_pEdges6 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %19 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges6, i32 0, i32 %19
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx7, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %20, i32 %21
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx8, i32 0, i32 1
  %22 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call9 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %22)
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call9 to %struct.btBroadphaseProxy*
  %24 = bitcast %struct.btBroadphaseAabbCallback* %18 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable10 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %24, align 4, !tbaa !11
  %vfn11 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable10, i64 2
  %25 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn11, align 4
  %call12 = call zeroext i1 %25(%struct.btBroadphaseAabbCallback* %18, %struct.btBroadphaseProxy* %23)
  br label %if.end

if.end:                                           ; preds = %if.then5, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add i32 %26, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %27 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #10
  br label %if.end13

if.end13:                                         ; preds = %for.end, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE8aabbTestERK9btVector3S3_R24btBroadphaseAabbCallback(%class.btAxisSweep3Internal.4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %axis = alloca i32, align 4
  %i = alloca i32, align 4
  %handle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %0 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %0, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_raycastAccelerator2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator2, align 4, !tbaa !47
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btDbvtBroadphase* %1 to void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)***
  %vtable = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)**, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*** %5, align 4, !tbaa !11
  %vfn = getelementptr inbounds void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vtable, i64 7
  %6 = load void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)*, void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)** %vfn, align 4
  call void %6(%struct.btDbvtBroadphase* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %4)
  br label %if.end16

if.else:                                          ; preds = %entry
  %7 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  %8 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #10
  store i32 1, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %9 = load i32, i32* %i, align 4, !tbaa !34
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %10 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %mul = mul i32 %10, 2
  %add = add i32 %mul, 1
  %cmp = icmp ult i32 %9, %add
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %12 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %12
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %14 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 %14
  %call = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx3)
  %tobool4 = icmp ne i32 %call, 0
  br i1 %tobool4, label %if.then5, label %if.end15

if.then5:                                         ; preds = %for.body
  %15 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %m_pEdges6 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %16 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges6, i32 0, i32 %16
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx7, align 4, !tbaa !2
  %18 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %17, i32 %18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx8, i32 0, i32 1
  %19 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call9 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %19)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call9, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %20 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %21 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %22 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %22 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %23, i32 0, i32 5
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %25 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %24 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 6
  %call10 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then5
  %26 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4, !tbaa !2
  %27 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle, align 4, !tbaa !2
  %28 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %27 to %struct.btBroadphaseProxy*
  %29 = bitcast %struct.btBroadphaseAabbCallback* %26 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable12 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %29, align 4, !tbaa !11
  %vfn13 = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable12, i64 2
  %30 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn13, align 4
  %call14 = call zeroext i1 %30(%struct.btBroadphaseAabbCallback* %26, %struct.btBroadphaseProxy* %28)
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then5
  %31 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %31) #10
  br label %if.end15

if.end15:                                         ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end15
  %32 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add i32 %32, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %33 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %33) #10
  br label %if.end16

if.end16:                                         ; preds = %for.end, %if.then
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher(%class.btAxisSweep3Internal.4* %this, %class.btDispatcher* %dispatcher) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.5*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp7 = alloca %struct.btBroadphasePair, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp33 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp37 = alloca %struct.btBroadphasePair, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %1 = bitcast %class.btOverlappingPairCache* %0 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %1, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %2 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %2(%class.btOverlappingPairCache* %0)
  br i1 %call, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  %3 = bitcast %class.btAlignedObjectArray.5** %overlappingPairArray to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %m_pairCache2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4, !tbaa !43
  %5 = bitcast %class.btOverlappingPairCache* %4 to %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)***
  %vtable3 = load %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*** %5, align 4, !tbaa !11
  %vfn4 = getelementptr inbounds %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)** %vtable3, i64 7
  %6 = load %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.5* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.5* %6(%class.btOverlappingPairCache* %4)
  store %class.btAlignedObjectArray.5* %call5, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %7 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %8 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %8) #10
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.5* %7, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %9 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %9) #10
  %10 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %11 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call6 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %11)
  %m_invalidPair = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  %12 = load i32, i32* %m_invalidPair, align 4, !tbaa !46
  %sub = sub nsw i32 %call6, %12
  %13 = bitcast %struct.btBroadphasePair* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %13) #10
  %call8 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp7)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %10, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %14 = bitcast %struct.btBroadphasePair* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #10
  %m_invalidPair9 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair9, align 4, !tbaa !46
  %15 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #10
  %16 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %16) #10
  %call10 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !62
  store i32 0, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %17 = load i32, i32* %i, align 4, !tbaa !34
  %18 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call11 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %18)
  %cmp = icmp slt i32 %17, %call11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %19 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #10
  %20 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %21 = load i32, i32* %i, align 4, !tbaa !34
  %call12 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %20, i32 %21)
  store %struct.btBroadphasePair* %call12, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %isDuplicate) #10
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %call13 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %22, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call13 to i8
  store i8 %frombool, i8* %isDuplicate, align 1, !tbaa !8
  %23 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %24 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %25 = bitcast %struct.btBroadphasePair* %23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false), !tbaa.struct !63
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %needsRemoval) #10
  store i8 0, i8* %needsRemoval, align 1, !tbaa !8
  %26 = load i8, i8* %isDuplicate, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %26 to i1
  br i1 %tobool, label %if.else21, label %if.then14

if.then14:                                        ; preds = %for.body
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasOverlap) #10
  %27 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy015 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %27, i32 0, i32 0
  %28 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy015, align 4, !tbaa !59
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy116 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %29, i32 0, i32 1
  %30 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy116, align 4, !tbaa !61
  %call17 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal.4* %this1, %struct.btBroadphaseProxy* %28, %struct.btBroadphaseProxy* %30)
  %frombool18 = zext i1 %call17 to i8
  store i8 %frombool18, i8* %hasOverlap, align 1, !tbaa !8
  %31 = load i8, i8* %hasOverlap, align 1, !tbaa !8, !range !10
  %tobool19 = trunc i8 %31 to i1
  br i1 %tobool19, label %if.then20, label %if.else

if.then20:                                        ; preds = %if.then14
  store i8 0, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end

if.else:                                          ; preds = %if.then14
  store i8 1, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then20
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasOverlap) #10
  br label %if.end22

if.else21:                                        ; preds = %for.body
  store i8 1, i8* %needsRemoval, align 1, !tbaa !8
  br label %if.end22

if.end22:                                         ; preds = %if.else21, %if.end
  %32 = load i8, i8* %needsRemoval, align 1, !tbaa !8, !range !10
  %tobool23 = trunc i8 %32 to i1
  br i1 %tobool23, label %if.then24, label %if.end31

if.then24:                                        ; preds = %if.end22
  %m_pairCache25 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %33 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4, !tbaa !43
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %35 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %36 = bitcast %class.btOverlappingPairCache* %33 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable26 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %36, align 4, !tbaa !11
  %vfn27 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable26, i64 8
  %37 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn27, align 4
  call void %37(%class.btOverlappingPairCache* %33, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %34, %class.btDispatcher* %35)
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy028 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy028, align 4, !tbaa !59
  %39 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4, !tbaa !2
  %m_pProxy129 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %39, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy129, align 4, !tbaa !61
  %m_invalidPair30 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  %40 = load i32, i32* %m_invalidPair30, align 4, !tbaa !46
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %m_invalidPair30, align 4, !tbaa !46
  %41 = load i32, i32* @gOverlappingPairs, align 4, !tbaa !34
  %dec = add nsw i32 %41, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4, !tbaa !34
  br label %if.end31

if.end31:                                         ; preds = %if.then24, %if.end22
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %needsRemoval) #10
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %isDuplicate) #10
  %42 = bitcast %struct.btBroadphasePair** %pair to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %42) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %43 = load i32, i32* %i, align 4, !tbaa !34
  %inc32 = add nsw i32 %43, 1
  store i32 %inc32, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %44 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %45 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %45) #10
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.5* %44, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp33)
  %46 = bitcast %class.btBroadphasePairSortPredicate* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %46) #10
  %47 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %48 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %overlappingPairArray, align 4, !tbaa !2
  %call34 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %48)
  %m_invalidPair35 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  %49 = load i32, i32* %m_invalidPair35, align 4, !tbaa !46
  %sub36 = sub nsw i32 %call34, %49
  %50 = bitcast %struct.btBroadphasePair* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %50) #10
  %call38 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp37)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %47, i32 %sub36, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %51 = bitcast %struct.btBroadphasePair* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %51) #10
  %m_invalidPair39 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 15
  store i32 0, i32* %m_invalidPair39, align 4, !tbaa !46
  %52 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %52) #10
  %53 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  %54 = bitcast %class.btAlignedObjectArray.5** %overlappingPairArray to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  br label %if.end40

if.end40:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal.4* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv(%class.btAxisSweep3Internal.4* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_(%class.btAxisSweep3Internal.4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 3
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %m_worldAabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !27
  %m_worldAabbMax = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_worldAabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !27
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher(%class.btAxisSweep3Internal.4* %this, %class.btDispatcher* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  store i32 1, i32* %m_firstFreeHandle, align 4, !tbaa !52
  %2 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  %3 = load i32, i32* %m_firstFreeHandle2, align 4, !tbaa !52
  store i32 %3, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4, !tbaa !34
  %m_maxHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 7
  %5 = load i32, i32* %m_maxHandles, align 4, !tbaa !50
  %cmp3 = icmp ult i32 %4, %5
  br i1 %cmp3, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %8 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %7, i32 %8
  %9 = load i32, i32* %i, align 4, !tbaa !34
  %add = add i32 %9, 1
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 %add)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles4 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles4, align 4, !tbaa !49
  %m_maxHandles5 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 7
  %12 = load i32, i32* %m_maxHandles5, align 4, !tbaa !50
  %sub = sub i32 %12, 1
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %11, i32 %sub
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx6, i32 0)
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE10printStatsEv(%class.btAxisSweep3Internal.4* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btNullPairCache* @_ZN15btNullPairCacheC2Ev(%class.btNullPairCache* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %0 = bitcast %class.btNullPairCache* %this1 to %class.btOverlappingPairCache*
  %call = call %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* %0) #10
  %1 = bitcast %class.btNullPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV15btNullPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !11
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray.5* %m_overlappingPairArray)
  ret %class.btNullPairCache* %this1
}

declare %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache(%struct.btDbvtBroadphase* returned, %class.btOverlappingPairCache*) unnamed_addr #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !29
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !29
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !29
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !29
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !29
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !29
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !29
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !29
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !29
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !29
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !29
  %div = fdiv float %2, %4
  store float %div, float* %ref.tmp, align 4, !tbaa !29
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !29
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !29
  %div8 = fdiv float %7, %9
  store float %div8, float* %ref.tmp3, align 4, !tbaa !29
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !29
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !29
  %div14 = fdiv float %12, %14
  store float %div14, float* %ref.tmp9, align 4, !tbaa !29
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !29
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !29
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !29
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !29
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !29
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !29
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !29
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #7

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAxisSweep3InternalItE6HandlenaEm(i32 %sizeInBytes) #6 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !66
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !66
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZN20btAxisSweep3InternalItE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned short>::Handle"* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, i16 zeroext %next) #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %next.addr = alloca i16, align 2
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4, !tbaa !2
  store i16 %next, i16* %next.addr, align 2, !tbaa !6
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %0 = load i16, i16* %next.addr, align 2, !tbaa !6
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 0
  store i16 %0, i16* %arrayidx, align 4, !tbaa !6
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItED0Ev(%class.btAxisSweep3Internal* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %call = call %class.btAxisSweep3Internal* @_ZN20btAxisSweep3InternalItED2Ev(%class.btAxisSweep3Internal* %this1) #10
  %0 = bitcast %class.btAxisSweep3Internal* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN22btOverlappingPairCacheC2Ev(%class.btOverlappingPairCache* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCache* %this1 to %class.btOverlappingPairCallback*
  %call = call %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* %0) #10
  %1 = bitcast %class.btOverlappingPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV22btOverlappingPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !11
  ret %class.btOverlappingPairCache* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI16btBroadphasePairEC2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator.6* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btNullPairCache* @_ZN15btNullPairCacheD2Ev(%class.btNullPairCache* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %0 = bitcast %class.btNullPairCache* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV15btNullPairCache, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray.5* %m_overlappingPairArray) #10
  %1 = bitcast %class.btNullPairCache* %this1 to %class.btOverlappingPairCache*
  %call2 = call %class.btOverlappingPairCache* bitcast (%class.btOverlappingPairCallback* (%class.btOverlappingPairCallback*)* @_ZN25btOverlappingPairCallbackD2Ev to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*)(%class.btOverlappingPairCache* %1) #10
  ret %class.btNullPairCache* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN15btNullPairCacheD0Ev(%class.btNullPairCache* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %call = call %class.btNullPairCache* @_ZN15btNullPairCacheD2Ev(%class.btNullPairCache* %this1) #10
  %0 = bitcast %class.btNullPairCache* %this1 to i8*
  call void @_ZdlPv(i8* %0) #12
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret %struct.btBroadphasePair* null
}

; Function Attrs: nounwind
define linkonce_odr hidden i8* @_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1, %class.btDispatcher* %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  %.addr2 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4, !tbaa !2
  store %class.btDispatcher* %2, %class.btDispatcher** %.addr2, align 4, !tbaa !2
  %this3 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i8* null
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache26getOverlappingPairArrayPtrEv(%class.btNullPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv(%class.btNullPairCache* %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %m_overlappingPairArray, i32 0)
  ret %struct.btBroadphasePair* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.5* @_ZN15btNullPairCache23getOverlappingPairArrayEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  %m_overlappingPairArray = getelementptr inbounds %class.btNullPairCache, %class.btNullPairCache* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.5* %m_overlappingPairArray
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphasePair*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %0, %struct.btBroadphasePair** %.addr, align 4, !tbaa !2
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK15btNullPairCache22getNumOverlappingPairsEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i32 0
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback(%class.btNullPairCache* %this, %struct.btOverlapFilterCallback* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btOverlapFilterCallback*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapFilterCallback* %0, %struct.btOverlapFilterCallback** %.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher(%class.btNullPairCache* %this, %struct.btOverlapCallback* %0, %class.btDispatcher* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btOverlapCallback*, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btOverlapCallback* %0, %struct.btOverlapCallback** %.addr, align 4, !tbaa !2
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_(%class.btNullPairCache* %this, %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %struct.btBroadphaseProxy*, align 4
  %.addr1 = alloca %struct.btBroadphaseProxy*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %0, %struct.btBroadphaseProxy** %.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %.addr1, align 4, !tbaa !2
  %this2 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret %struct.btBroadphasePair* null
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN15btNullPairCache18hasDeferredRemovalEv(%class.btNullPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret i1 true
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback(%class.btNullPairCache* %this, %class.btOverlappingPairCallback* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %class.btOverlappingPairCallback* %0, %class.btOverlappingPairCallback** %.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher(%class.btNullPairCache* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btNullPairCache*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btNullPairCache* %this, %class.btNullPairCache** %this.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btNullPairCache*, %class.btNullPairCache** %this.addr, align 4
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackC2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  %0 = bitcast %class.btOverlappingPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV25btOverlappingPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btOverlappingPairCallback* @_ZN25btOverlappingPairCallbackD2Ev(%class.btOverlappingPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  ret %class.btOverlappingPairCallback* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN22btOverlappingPairCacheD0Ev(%class.btOverlappingPairCache* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCache*, align 4
  store %class.btOverlappingPairCache* %this, %class.btOverlappingPairCache** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN25btOverlappingPairCallbackD0Ev(%class.btOverlappingPairCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btOverlappingPairCallback*, align 4
  store %class.btOverlappingPairCallback* %this, %class.btOverlappingPairCallback** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EEC2Ev(%class.btAlignedAllocator.6* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  ret %class.btAlignedAllocator.6* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !68
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !72
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !73
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayI16btBroadphasePairED2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE5clearEv(%class.btAlignedObjectArray.5* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.5* %this, i32 %first, i32 %last) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !34
  store i32 %last, i32* %last.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %first.addr, align 4, !tbaa !34
  store i32 %1, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !34
  %3 = load i32, i32* %last.addr, align 4, !tbaa !34
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %5 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !72
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.5* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !68, !range !10
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !71
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4, !tbaa !71
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.6* %this, %struct.btBroadphasePair* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #9

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %1 = load i32, i32* %n.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZNK20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %1 = load i32, i32* %n.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4, !tbaa !35
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 3
  store i8* null, i8** %m_multiSapParentProxy, align 4, !tbaa !74
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 6
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE6HandledaEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItEdlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAxisSweep3InternalIjE6HandlenaEm(i32 %sizeInBytes) #6 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4, !tbaa !66
  %0 = load i32, i32* %sizeInBytes.addr, align 4, !tbaa !66
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: inlinehint
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZN20btAxisSweep3InternalIjE6HandleC2Ev(%"class.btAxisSweep3Internal<unsigned int>::Handle"* returned %this) unnamed_addr #6 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, i32 %next) #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %next.addr = alloca i32, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4, !tbaa !2
  store i32 %next, i32* %next.addr, align 4, !tbaa !34
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %0 = load i32, i32* %next.addr, align 4, !tbaa !34
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 0
  store i32 %0, i32* %arrayidx, align 4, !tbaa !34
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAxisSweep3Internal.4* @_ZN20btAxisSweep3InternalIjED2Ev(%class.btAxisSweep3Internal.4* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btAxisSweep3Internal.4*, align 4
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  store %class.btAxisSweep3Internal.4* %this1, %class.btAxisSweep3Internal.4** %retval, align 4
  %0 = bitcast %class.btAxisSweep3Internal.4* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV20btAxisSweep3InternalIjE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !11
  %m_raycastAccelerator = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator, align 4, !tbaa !47
  %tobool = icmp ne %struct.btDbvtBroadphase* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_nullPairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 17
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache, align 4, !tbaa !48
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !11
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #10
  %m_nullPairCache2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 17
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_nullPairCache2, align 4, !tbaa !48
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  %m_raycastAccelerator3 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %7 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator3, align 4, !tbaa !47
  %8 = bitcast %struct.btDbvtBroadphase* %7 to %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)***
  %vtable4 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)**, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*** %8, align 4, !tbaa !11
  %vfn5 = getelementptr inbounds %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vtable4, i64 0
  %9 = load %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)*, %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)** %vfn5, align 4
  %call6 = call %struct.btDbvtBroadphase* %9(%struct.btDbvtBroadphase* %7) #10
  %m_raycastAccelerator7 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 16
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %m_raycastAccelerator7, align 4, !tbaa !47
  %11 = bitcast %struct.btDbvtBroadphase* %10 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  store i32 2, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %13 = load i32, i32* %i, align 4, !tbaa !34
  %cmp = icmp sge i32 %13, 0
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %14 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %14) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pEdgesRawPtr = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 11
  %15 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i8*], [3 x i8*]* %m_pEdgesRawPtr, i32 0, i32 %15
  %16 = load i8*, i8** %arrayidx, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %16)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4, !tbaa !34
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %isnull = icmp eq %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.end
  %19 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18 to i8*
  call void @_ZN20btAxisSweep3InternalIjE6HandledaEPv(i8* %19) #10
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.end
  %m_ownsPairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 14
  %20 = load i8, i8* %m_ownsPairCache, align 4, !tbaa !45, !range !10
  %tobool8 = trunc i8 %20 to i1
  br i1 %tobool8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %delete.end
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %21 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %22 = bitcast %class.btOverlappingPairCache* %21 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable10 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %22, align 4, !tbaa !11
  %vfn11 = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable10, i64 0
  %23 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn11, align 4
  %call12 = call %class.btOverlappingPairCache* %23(%class.btOverlappingPairCache* %21) #10
  %m_pairCache13 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %24 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache13, align 4, !tbaa !43
  %25 = bitcast %class.btOverlappingPairCache* %24 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %25)
  br label %if.end14

if.end14:                                         ; preds = %if.then9, %delete.end
  %26 = bitcast %class.btAxisSweep3Internal.4* %this1 to %class.btBroadphaseInterface*
  %call15 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %26) #10
  %27 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %retval, align 4
  ret %class.btAxisSweep3Internal.4* %27
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjED0Ev(%class.btAxisSweep3Internal.4* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %call = call %class.btAxisSweep3Internal.4* @_ZN20btAxisSweep3InternalIjED2Ev(%class.btAxisSweep3Internal.4* %this1) #10
  %0 = bitcast %class.btAxisSweep3Internal.4* %this1 to i8*
  call void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %0) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE6HandledaEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjEdlEPv(i8* %ptr) #2 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %0 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

define linkonce_odr hidden zeroext i16 @_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_(%class.btAxisSweep3Internal* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %pOwner, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %dispatcher, i8* %multiSapProxy) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pOwner.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %multiSapProxy.addr = alloca i8*, align 4
  %min = alloca [3 x i16], align 2
  %max = alloca [3 x i16], align 2
  %handle = alloca i16, align 2
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %limit = alloca i16, align 2
  %axis = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i8* %pOwner, i8** %pOwner.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast [3 x i16]* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %0) #10
  %1 = bitcast [3 x i16]* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %1) #10
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 1)
  %4 = bitcast i16* %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %4) #10
  %call = call zeroext i16 @_ZN20btAxisSweep3InternalItE11allocHandleEv(%class.btAxisSweep3Internal* %this1)
  store i16 %call, i16* %handle, align 2, !tbaa !6
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load i16, i16* %handle, align 2, !tbaa !6
  %call3 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %6)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call3, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %7 = load i16, i16* %handle, align 2, !tbaa !6
  %conv = zext i16 %7 to i32
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 4
  store i32 %conv, i32* %m_uniqueId, align 4, !tbaa !58
  %10 = load i8*, i8** %pOwner.addr, align 4, !tbaa !2
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %12 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %11 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %12, i32 0, i32 0
  store i8* %10, i8** %m_clientObject, align 4, !tbaa !35
  %13 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %15 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %14 to %struct.btBroadphaseProxy*
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %15, i32 0, i32 1
  store i16 %13, i16* %m_collisionFilterGroup, align 4, !tbaa !75
  %16 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %18 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %17 to %struct.btBroadphaseProxy*
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 2
  store i16 %16, i16* %m_collisionFilterMask, align 2, !tbaa !76
  %19 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %20 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %20 to %struct.btBroadphaseProxy*
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 3
  store i8* %19, i8** %m_multiSapParentProxy, align 4, !tbaa !74
  %22 = bitcast i16* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %22) #10
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %23 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %conv4 = zext i16 %23 to i32
  %mul = mul nsw i32 %conv4, 2
  %conv5 = trunc i32 %mul to i16
  store i16 %conv5, i16* %limit, align 2, !tbaa !6
  %24 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #10
  store i16 0, i16* %axis, align 2, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %25 = load i16, i16* %axis, align 2, !tbaa !6
  %conv6 = zext i16 %25 to i32
  %cmp = icmp slt i32 %conv6, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %26 = bitcast i16* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %26) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %27 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %27, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i32 0, i32 2
  %28 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom = zext i16 %28 to i32
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %idxprom
  %29 = load i16, i16* %arrayidx7, align 2, !tbaa !6
  %conv8 = zext i16 %29 to i32
  %add = add nsw i32 %conv8, 2
  %conv9 = trunc i32 %add to i16
  store i16 %conv9, i16* %arrayidx7, align 2, !tbaa !6
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %30 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom10 = zext i16 %30 to i32
  %arrayidx11 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %idxprom10
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx11, align 4, !tbaa !2
  %32 = load i16, i16* %limit, align 2, !tbaa !6
  %conv12 = zext i16 %32 to i32
  %sub = sub nsw i32 %conv12, 1
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %31, i32 %sub
  %m_pEdges14 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %33 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom15 = zext i16 %33 to i32
  %arrayidx16 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges14, i32 0, i32 %idxprom15
  %34 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx16, align 4, !tbaa !2
  %35 = load i16, i16* %limit, align 2, !tbaa !6
  %conv17 = zext i16 %35 to i32
  %add18 = add nsw i32 %conv17, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %34, i32 %add18
  %36 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx19 to i8*
  %37 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %36, i8* align 2 %37, i32 4, i1 false), !tbaa.struct !77
  %38 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom20 = zext i16 %38 to i32
  %arrayidx21 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %idxprom20
  %39 = load i16, i16* %arrayidx21, align 2, !tbaa !6
  %m_pEdges22 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %40 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom23 = zext i16 %40 to i32
  %arrayidx24 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges22, i32 0, i32 %idxprom23
  %41 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx24, align 4, !tbaa !2
  %42 = load i16, i16* %limit, align 2, !tbaa !6
  %conv25 = zext i16 %42 to i32
  %sub26 = sub nsw i32 %conv25, 1
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %41, i32 %sub26
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx27, i32 0, i32 0
  store i16 %39, i16* %m_pos, align 2, !tbaa !37
  %43 = load i16, i16* %handle, align 2, !tbaa !6
  %m_pEdges28 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %44 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom29 = zext i16 %44 to i32
  %arrayidx30 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges28, i32 0, i32 %idxprom29
  %45 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx30, align 4, !tbaa !2
  %46 = load i16, i16* %limit, align 2, !tbaa !6
  %conv31 = zext i16 %46 to i32
  %sub32 = sub nsw i32 %conv31, 1
  %arrayidx33 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %45, i32 %sub32
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx33, i32 0, i32 1
  store i16 %43, i16* %m_handle, align 2, !tbaa !39
  %47 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom34 = zext i16 %47 to i32
  %arrayidx35 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %idxprom34
  %48 = load i16, i16* %arrayidx35, align 2, !tbaa !6
  %m_pEdges36 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %49 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom37 = zext i16 %49 to i32
  %arrayidx38 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges36, i32 0, i32 %idxprom37
  %50 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx38, align 4, !tbaa !2
  %51 = load i16, i16* %limit, align 2, !tbaa !6
  %idxprom39 = zext i16 %51 to i32
  %arrayidx40 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %50, i32 %idxprom39
  %m_pos41 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx40, i32 0, i32 0
  store i16 %48, i16* %m_pos41, align 2, !tbaa !37
  %52 = load i16, i16* %handle, align 2, !tbaa !6
  %m_pEdges42 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %53 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom43 = zext i16 %53 to i32
  %arrayidx44 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges42, i32 0, i32 %idxprom43
  %54 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx44, align 4, !tbaa !2
  %55 = load i16, i16* %limit, align 2, !tbaa !6
  %idxprom45 = zext i16 %55 to i32
  %arrayidx46 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %54, i32 %idxprom45
  %m_handle47 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx46, i32 0, i32 1
  store i16 %52, i16* %m_handle47, align 2, !tbaa !39
  %56 = load i16, i16* %limit, align 2, !tbaa !6
  %conv48 = zext i16 %56 to i32
  %sub49 = sub nsw i32 %conv48, 1
  %conv50 = trunc i32 %sub49 to i16
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %57, i32 0, i32 1
  %58 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom51 = zext i16 %58 to i32
  %arrayidx52 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %idxprom51
  store i16 %conv50, i16* %arrayidx52, align 2, !tbaa !6
  %59 = load i16, i16* %limit, align 2, !tbaa !6
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges53 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %60, i32 0, i32 2
  %61 = load i16, i16* %axis, align 2, !tbaa !6
  %idxprom54 = zext i16 %61 to i32
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges53, i32 0, i32 %idxprom54
  store i16 %59, i16* %arrayidx55, align 2, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %62 = load i16, i16* %axis, align 2, !tbaa !6
  %inc = add i16 %62, 1
  store i16 %inc, i16* %axis, align 2, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %63 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges56 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %63, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges56, i32 0, i32 0
  %64 = load i16, i16* %arrayidx57, align 4, !tbaa !6
  %65 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 0, i16 zeroext %64, %class.btDispatcher* %65, i1 zeroext false)
  %66 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges58 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %66, i32 0, i32 2
  %arrayidx59 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges58, i32 0, i32 0
  %67 = load i16, i16* %arrayidx59, align 2, !tbaa !6
  %68 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 0, i16 zeroext %67, %class.btDispatcher* %68, i1 zeroext false)
  %69 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges60 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %69, i32 0, i32 1
  %arrayidx61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges60, i32 0, i32 1
  %70 = load i16, i16* %arrayidx61, align 2, !tbaa !6
  %71 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 1, i16 zeroext %70, %class.btDispatcher* %71, i1 zeroext false)
  %72 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges62 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %72, i32 0, i32 2
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges62, i32 0, i32 1
  %73 = load i16, i16* %arrayidx63, align 2, !tbaa !6
  %74 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 1, i16 zeroext %73, %class.btDispatcher* %74, i1 zeroext false)
  %75 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges64 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %75, i32 0, i32 1
  %arrayidx65 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges64, i32 0, i32 2
  %76 = load i16, i16* %arrayidx65, align 4, !tbaa !6
  %77 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 2, i16 zeroext %76, %class.btDispatcher* %77, i1 zeroext true)
  %78 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges66 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %78, i32 0, i32 2
  %arrayidx67 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges66, i32 0, i32 2
  %79 = load i16, i16* %arrayidx67, align 2, !tbaa !6
  %80 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 2, i16 zeroext %79, %class.btDispatcher* %80, i1 zeroext true)
  %81 = load i16, i16* %handle, align 2, !tbaa !6
  %82 = bitcast i16* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %82) #10
  %83 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #10
  %84 = bitcast i16* %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %84) #10
  %85 = bitcast [3 x i16]* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %85) #10
  %86 = bitcast [3 x i16]* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %86) #10
  ret i16 %81
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this, i16 zeroext %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %index.addr = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i16 %index, i16* %index.addr, align 2, !tbaa !6
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %0 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %1 = load i16, i16* %index.addr, align 2, !tbaa !6
  %conv = zext i16 %1 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %0, i32 %conv
  ret %"class.btAxisSweep3Internal<unsigned short>::Handle"* %add.ptr
}

define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i16* %out, i16** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #10
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_quantize)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #10
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !29
  %cmp = fcmp ole float %4, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %conv = trunc i32 %5 to i16
  br label %cond.end22

cond.false:                                       ; preds = %entry
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %6 = load float, float* %arrayidx3, align 4, !tbaa !29
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %7 = load i16, i16* %m_handleSentinel, align 2, !tbaa !17
  %conv4 = zext i16 %7 to i32
  %conv5 = sitofp i32 %conv4 to float
  %cmp6 = fcmp oge float %6, %conv5
  br i1 %cmp6, label %cond.true7, label %cond.false12

cond.true7:                                       ; preds = %cond.false
  %m_handleSentinel8 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %8 = load i16, i16* %m_handleSentinel8, align 2, !tbaa !17
  %conv9 = zext i16 %8 to i32
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %9 = load i16, i16* %m_bpHandleMask, align 4, !tbaa !13
  %conv10 = zext i16 %9 to i32
  %and = and i32 %conv9, %conv10
  %10 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or = or i32 %and, %10
  %conv11 = trunc i32 %or to i16
  br label %cond.end

cond.false12:                                     ; preds = %cond.false
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 0
  %11 = load float, float* %arrayidx14, align 4, !tbaa !29
  %conv15 = fptoui float %11 to i16
  %conv16 = zext i16 %conv15 to i32
  %m_bpHandleMask17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %12 = load i16, i16* %m_bpHandleMask17, align 4, !tbaa !13
  %conv18 = zext i16 %12 to i32
  %and19 = and i32 %conv16, %conv18
  %13 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or20 = or i32 %and19, %13
  %conv21 = trunc i32 %or20 to i16
  br label %cond.end

cond.end:                                         ; preds = %cond.false12, %cond.true7
  %cond = phi i16 [ %conv11, %cond.true7 ], [ %conv21, %cond.false12 ]
  br label %cond.end22

cond.end22:                                       ; preds = %cond.end, %cond.true
  %cond23 = phi i16 [ %conv, %cond.true ], [ %cond, %cond.end ]
  %14 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx24 = getelementptr inbounds i16, i16* %14, i32 0
  store i16 %cond23, i16* %arrayidx24, align 2, !tbaa !6
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %15 = load float, float* %arrayidx26, align 4, !tbaa !29
  %cmp27 = fcmp ole float %15, 0.000000e+00
  br i1 %cmp27, label %cond.true28, label %cond.false30

cond.true28:                                      ; preds = %cond.end22
  %16 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %conv29 = trunc i32 %16 to i16
  br label %cond.end57

cond.false30:                                     ; preds = %cond.end22
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %17 = load float, float* %arrayidx32, align 4, !tbaa !29
  %m_handleSentinel33 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %18 = load i16, i16* %m_handleSentinel33, align 2, !tbaa !17
  %conv34 = zext i16 %18 to i32
  %conv35 = sitofp i32 %conv34 to float
  %cmp36 = fcmp oge float %17, %conv35
  br i1 %cmp36, label %cond.true37, label %cond.false45

cond.true37:                                      ; preds = %cond.false30
  %m_handleSentinel38 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %19 = load i16, i16* %m_handleSentinel38, align 2, !tbaa !17
  %conv39 = zext i16 %19 to i32
  %m_bpHandleMask40 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %20 = load i16, i16* %m_bpHandleMask40, align 4, !tbaa !13
  %conv41 = zext i16 %20 to i32
  %and42 = and i32 %conv39, %conv41
  %21 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or43 = or i32 %and42, %21
  %conv44 = trunc i32 %or43 to i16
  br label %cond.end55

cond.false45:                                     ; preds = %cond.false30
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 1
  %22 = load float, float* %arrayidx47, align 4, !tbaa !29
  %conv48 = fptoui float %22 to i16
  %conv49 = zext i16 %conv48 to i32
  %m_bpHandleMask50 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %23 = load i16, i16* %m_bpHandleMask50, align 4, !tbaa !13
  %conv51 = zext i16 %23 to i32
  %and52 = and i32 %conv49, %conv51
  %24 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or53 = or i32 %and52, %24
  %conv54 = trunc i32 %or53 to i16
  br label %cond.end55

cond.end55:                                       ; preds = %cond.false45, %cond.true37
  %cond56 = phi i16 [ %conv44, %cond.true37 ], [ %conv54, %cond.false45 ]
  br label %cond.end57

cond.end57:                                       ; preds = %cond.end55, %cond.true28
  %cond58 = phi i16 [ %conv29, %cond.true28 ], [ %cond56, %cond.end55 ]
  %25 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx59 = getelementptr inbounds i16, i16* %25, i32 1
  store i16 %cond58, i16* %arrayidx59, align 2, !tbaa !6
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %26 = load float, float* %arrayidx61, align 4, !tbaa !29
  %cmp62 = fcmp ole float %26, 0.000000e+00
  br i1 %cmp62, label %cond.true63, label %cond.false65

cond.true63:                                      ; preds = %cond.end57
  %27 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %conv64 = trunc i32 %27 to i16
  br label %cond.end92

cond.false65:                                     ; preds = %cond.end57
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %28 = load float, float* %arrayidx67, align 4, !tbaa !29
  %m_handleSentinel68 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %29 = load i16, i16* %m_handleSentinel68, align 2, !tbaa !17
  %conv69 = zext i16 %29 to i32
  %conv70 = sitofp i32 %conv69 to float
  %cmp71 = fcmp oge float %28, %conv70
  br i1 %cmp71, label %cond.true72, label %cond.false80

cond.true72:                                      ; preds = %cond.false65
  %m_handleSentinel73 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %30 = load i16, i16* %m_handleSentinel73, align 2, !tbaa !17
  %conv74 = zext i16 %30 to i32
  %m_bpHandleMask75 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %31 = load i16, i16* %m_bpHandleMask75, align 4, !tbaa !13
  %conv76 = zext i16 %31 to i32
  %and77 = and i32 %conv74, %conv76
  %32 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or78 = or i32 %and77, %32
  %conv79 = trunc i32 %or78 to i16
  br label %cond.end90

cond.false80:                                     ; preds = %cond.false65
  %call81 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %33 = load float, float* %arrayidx82, align 4, !tbaa !29
  %conv83 = fptoui float %33 to i16
  %conv84 = zext i16 %conv83 to i32
  %m_bpHandleMask85 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 1
  %34 = load i16, i16* %m_bpHandleMask85, align 4, !tbaa !13
  %conv86 = zext i16 %34 to i32
  %and87 = and i32 %conv84, %conv86
  %35 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or88 = or i32 %and87, %35
  %conv89 = trunc i32 %or88 to i16
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false80, %cond.true72
  %cond91 = phi i16 [ %conv79, %cond.true72 ], [ %conv89, %cond.false80 ]
  br label %cond.end92

cond.end92:                                       ; preds = %cond.end90, %cond.true63
  %cond93 = phi i16 [ %conv64, %cond.true63 ], [ %cond91, %cond.end90 ]
  %36 = load i16*, i16** %out.addr, align 4, !tbaa !2
  %arrayidx94 = getelementptr inbounds i16, i16* %36, i32 2
  store i16 %cond93, i16* %arrayidx94, align 2, !tbaa !6
  %37 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #10
  ret void
}

define linkonce_odr hidden zeroext i16 @_ZN20btAxisSweep3InternalItE11allocHandleEv(%class.btAxisSweep3Internal* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast i16* %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %0) #10
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %1 = load i16, i16* %m_firstFreeHandle, align 4, !tbaa !33
  store i16 %1, i16* %handle, align 2, !tbaa !6
  %2 = load i16, i16* %handle, align 2, !tbaa !6
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %2)
  %call2 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %call)
  %m_firstFreeHandle3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 %call2, i16* %m_firstFreeHandle3, align 4, !tbaa !33
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %3 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %inc = add i16 %3, 1
  store i16 %inc, i16* %m_numHandles, align 4, !tbaa !32
  %4 = load i16, i16* %handle, align 2, !tbaa !6
  %5 = bitcast i16* %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %5) #10
  ret i16 %4
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i16 %edge, i16* %edge.addr, align 2, !tbaa !6
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %1 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %2
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %4 = load i16, i16* %edge.addr, align 2, !tbaa !6
  %conv = zext i16 %4 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %3, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %6, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %8, i32 0, i32 1
  %9 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %9)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end25, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %10, i32 0, i32 0
  %11 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv3 = zext i16 %11 to i32
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %12, i32 0, i32 0
  %13 = load i16, i16* %m_pos4, align 2, !tbaa !37
  %conv5 = zext i16 %13 to i32
  %cmp = icmp slt i32 %conv3, %conv5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %15, i32 0, i32 1
  %16 = load i16, i16* %m_handle6, align 2, !tbaa !39
  %call7 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %16)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call7, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %call8 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %17)
  %tobool = icmp ne i16 %call8, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %18 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  %19 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %19
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %20 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl9 = shl i32 1, %21
  %and10 = and i32 %shl9, 3
  store i32 %and10, i32* %axis2, align 4, !tbaa !34
  %22 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool11 = trunc i8 %22 to i1
  br i1 %tobool11, label %land.lhs.true, label %if.end21

land.lhs.true:                                    ; preds = %if.then
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %25 = load i32, i32* %axis1, align 4, !tbaa !34
  %26 = load i32, i32* %axis2, align 4, !tbaa !34
  %call12 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %23, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %24, i32 %25, i32 %26)
  br i1 %call12, label %if.then13, label %if.end21

if.then13:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %27 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %28 = bitcast %class.btOverlappingPairCache* %27 to %class.btOverlappingPairCallback*
  %29 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %29 to %struct.btBroadphaseProxy*
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = bitcast %class.btOverlappingPairCallback* %28 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %33, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %34 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call14 = call %struct.btBroadphasePair* %34(%class.btOverlappingPairCallback* %28, %struct.btBroadphaseProxy* %30, %struct.btBroadphaseProxy* %32)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %35 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !19
  %tobool15 = icmp ne %class.btOverlappingPairCallback* %35, null
  br i1 %tobool15, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.then13
  %m_userPairCallback17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback17, align 4, !tbaa !19
  %37 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %40 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %39 to %struct.btBroadphaseProxy*
  %41 = bitcast %class.btOverlappingPairCallback* %36 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable18 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %41, align 4, !tbaa !11
  %vfn19 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable18, i64 2
  %42 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn19, align 4
  %call20 = call %struct.btBroadphasePair* %42(%class.btOverlappingPairCallback* %36, %struct.btBroadphaseProxy* %38, %struct.btBroadphaseProxy* %40)
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then13
  br label %if.end21

if.end21:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %43 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %43, i32 0, i32 2
  %44 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %44
  %45 = load i16, i16* %arrayidx22, align 2, !tbaa !6
  %inc = add i16 %45, 1
  store i16 %inc, i16* %arrayidx22, align 2, !tbaa !6
  %46 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #10
  %47 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #10
  br label %if.end25

if.else:                                          ; preds = %while.body
  %48 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %48, i32 0, i32 1
  %49 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx23 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %49
  %50 = load i16, i16* %arrayidx23, align 2, !tbaa !6
  %inc24 = add i16 %50, 1
  store i16 %inc24, i16* %arrayidx23, align 2, !tbaa !6
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.end21
  %51 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_minEdges26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %51, i32 0, i32 1
  %52 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges26, i32 0, i32 %52
  %53 = load i16, i16* %arrayidx27, align 2, !tbaa !6
  %dec = add i16 %53, -1
  store i16 %dec, i16* %arrayidx27, align 2, !tbaa !6
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #10
  %55 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %57 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %56, i8* align 2 %57, i32 4, i1 false), !tbaa.struct !77
  %58 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %59 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %59 to i8*
  %61 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %60, i8* align 2 %61, i32 4, i1 false), !tbaa.struct !77
  %62 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %62 to i8*
  %64 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %63, i8* align 2 %64, i32 4, i1 false), !tbaa.struct !77
  %65 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %65, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %66 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %incdec.ptr28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %66, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr28, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %67 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %67) #10
  %68 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #10
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #10
  %71 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i16 %edge, i16* %edge.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %3 = load i16, i16* %edge.addr, align 2, !tbaa !6
  %conv = zext i16 %3 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %2, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %5, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %6 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %7, i32 0, i32 1
  %8 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %8)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %9, i32 0, i32 0
  %10 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv3 = zext i16 %10 to i32
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %11, i32 0, i32 0
  %12 = load i16, i16* %m_pos4, align 2, !tbaa !37
  %conv5 = zext i16 %12 to i32
  %cmp = icmp slt i32 %conv3, %conv5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %14, i32 0, i32 1
  %15 = load i16, i16* %m_handle6, align 2, !tbaa !39
  %call7 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %15)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call7, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %16 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %call8 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %16)
  %tobool = icmp ne i16 %call8, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %18, i32 0, i32 1
  %19 = load i16, i16* %m_handle9, align 2, !tbaa !39
  %call10 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %19)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %21, i32 0, i32 1
  %22 = load i16, i16* %m_handle11, align 2, !tbaa !39
  %call12 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %22)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %23 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #10
  %24 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %24
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %25 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #10
  %26 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl13 = shl i32 1, %26
  %and14 = and i32 %shl13, 3
  store i32 %and14, i32* %axis2, align 4, !tbaa !34
  %27 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool15 = trunc i8 %27 to i1
  br i1 %tobool15, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %28 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %29 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %30 = load i32, i32* %axis1, align 4, !tbaa !34
  %31 = load i32, i32* %axis2, align 4, !tbaa !34
  %call16 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %28, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %29, i32 %30, i32 %31)
  br i1 %call16, label %if.then17, label %if.end25

if.then17:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %32 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %33 = bitcast %class.btOverlappingPairCache* %32 to %class.btOverlappingPairCallback*
  %34 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %35 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %34 to %struct.btBroadphaseProxy*
  %36 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %37 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %36 to %struct.btBroadphaseProxy*
  %38 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %39 = bitcast %class.btOverlappingPairCallback* %33 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %39, align 4, !tbaa !11
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %40 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call18 = call i8* %40(%class.btOverlappingPairCallback* %33, %struct.btBroadphaseProxy* %35, %struct.btBroadphaseProxy* %37, %class.btDispatcher* %38)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %41 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !19
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %41, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then17
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %42 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4, !tbaa !19
  %43 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %44 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %43 to %struct.btBroadphaseProxy*
  %45 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %46 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %45 to %struct.btBroadphaseProxy*
  %47 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %48 = bitcast %class.btOverlappingPairCallback* %42 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable22 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %48, align 4, !tbaa !11
  %vfn23 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable22, i64 3
  %49 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn23, align 4
  %call24 = call i8* %49(%class.btOverlappingPairCallback* %42, %struct.btBroadphaseProxy* %44, %struct.btBroadphaseProxy* %46, %class.btDispatcher* %47)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then17
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %50 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %50, i32 0, i32 1
  %51 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx26 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %51
  %52 = load i16, i16* %arrayidx26, align 2, !tbaa !6
  %inc = add i16 %52, 1
  store i16 %inc, i16* %arrayidx26, align 2, !tbaa !6
  %53 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  %54 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  br label %if.end29

if.else:                                          ; preds = %while.body
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %57, i32 0, i32 2
  %58 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %58
  %59 = load i16, i16* %arrayidx27, align 2, !tbaa !6
  %inc28 = add i16 %59, 1
  store i16 %inc28, i16* %arrayidx27, align 2, !tbaa !6
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_maxEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %60, i32 0, i32 2
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx31 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges30, i32 0, i32 %61
  %62 = load i16, i16* %arrayidx31, align 2, !tbaa !6
  %dec = add i16 %62, -1
  store i16 %dec, i16* %arrayidx31, align 2, !tbaa !6
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  %64 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %65 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %65, i8* align 2 %66, i32 4, i1 false), !tbaa.struct !77
  %67 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %68 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %68 to i8*
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %69, i8* align 2 %70, i32 4, i1 false), !tbaa.struct !77
  %71 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %72, i8* align 2 %73, i32 4, i1 false), !tbaa.struct !77
  %74 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %74, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %75 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %75, i32 -1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev, align 4, !tbaa !2
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #10
  %77 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %78 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #10
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #6 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !29
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !29
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4, !tbaa !29
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !29
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !29
  %mul8 = fmul float %7, %9
  store float %mul8, float* %ref.tmp3, align 4, !tbaa !29
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #10
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !29
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !29
  %mul14 = fmul float %12, %14
  store float %mul14, float* %ref.tmp9, align 4, !tbaa !29
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #10
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #10
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i16 @_ZNK20btAxisSweep3InternalItE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %this.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 0
  %0 = load i16, i16* %arrayidx, align 4, !tbaa !6
  ret i16 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %this, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %this.addr, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv = zext i16 %0 to i32
  %and = and i32 %conv, 1
  %conv2 = trunc i32 %and to i16
  ret i16 %conv2
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleB, i32 %axis0, i32 %axis1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %pHandleA.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleB.addr = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis0.addr = alloca i32, align 4
  %axis1.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %pHandleB, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  store i32 %axis0, i32* %axis0.addr, align 4, !tbaa !34
  store i32 %axis1, i32* %axis1.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %0, i32 0, i32 2
  %1 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %1
  %2 = load i16, i16* %arrayidx, align 2, !tbaa !6
  %conv = zext i16 %2 to i32
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %3, i32 0, i32 1
  %4 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %4
  %5 = load i16, i16* %arrayidx2, align 2, !tbaa !6
  %conv3 = zext i16 %5 to i32
  %cmp = icmp slt i32 %conv, %conv3
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_maxEdges4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %6, i32 0, i32 2
  %7 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges4, i32 0, i32 %7
  %8 = load i16, i16* %arrayidx5, align 2, !tbaa !6
  %conv6 = zext i16 %8 to i32
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_minEdges7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %9, i32 0, i32 1
  %10 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx8 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges7, i32 0, i32 %10
  %11 = load i16, i16* %arrayidx8, align 2, !tbaa !6
  %conv9 = zext i16 %11 to i32
  %cmp10 = icmp slt i32 %conv6, %conv9
  br i1 %cmp10, label %if.then, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %lor.lhs.false
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_maxEdges12 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %12, i32 0, i32 2
  %13 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx13 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges12, i32 0, i32 %13
  %14 = load i16, i16* %arrayidx13, align 2, !tbaa !6
  %conv14 = zext i16 %14 to i32
  %15 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_minEdges15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %15, i32 0, i32 1
  %16 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges15, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx16, align 2, !tbaa !6
  %conv17 = zext i16 %17 to i32
  %cmp18 = icmp slt i32 %conv14, %conv17
  br i1 %cmp18, label %if.then, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false11
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_maxEdges20 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %18, i32 0, i32 2
  %19 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx21 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges20, i32 0, i32 %19
  %20 = load i16, i16* %arrayidx21, align 2, !tbaa !6
  %conv22 = zext i16 %20 to i32
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_minEdges23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %21, i32 0, i32 1
  %22 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx24 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges23, i32 0, i32 %22
  %23 = load i16, i16* %arrayidx24, align 2, !tbaa !6
  %conv25 = zext i16 %23 to i32
  %cmp26 = icmp slt i32 %conv22, %conv25
  br i1 %cmp26, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false19, %lor.lhs.false11, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false19
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher(%class.btAxisSweep3Internal* %this, i16 zeroext %handle, %class.btDispatcher* %dispatcher) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  %pEdges = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %max = alloca i16, align 2
  %i = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i16 %handle, i16* %handle.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i16, i16* %handle.addr, align 2, !tbaa !6
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %1)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %3 = bitcast %class.btOverlappingPairCache* %2 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %4 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call zeroext i1 %4(%class.btOverlappingPairCache* %2)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_pairCache3 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache3, align 4, !tbaa !18
  %6 = bitcast %class.btOverlappingPairCache* %5 to %class.btOverlappingPairCallback*
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %7 to %struct.btBroadphaseProxy*
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %10 = bitcast %class.btOverlappingPairCallback* %6 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable4 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %10, align 4, !tbaa !11
  %vfn5 = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable4, i64 4
  %11 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn5, align 4
  call void %11(%class.btOverlappingPairCallback* %6, %struct.btBroadphaseProxy* %8, %class.btDispatcher* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %13 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %conv = zext i16 %13 to i32
  %mul = mul nsw i32 %conv, 2
  store i32 %mul, i32* %limit, align 4, !tbaa !34
  %14 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %15, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 8
  %16 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %m_pHandles, align 4, !tbaa !30
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %16, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %arrayidx, i32 0, i32 2
  %17 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %17
  %18 = load i16, i16* %arrayidx6, align 2, !tbaa !6
  %conv7 = zext i16 %18 to i32
  %sub = sub nsw i32 %conv7, 2
  %conv8 = trunc i32 %sub to i16
  store i16 %conv8, i16* %arrayidx6, align 2, !tbaa !6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc27, %for.end
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp10 = icmp slt i32 %20, 3
  br i1 %cmp10, label %for.body11, label %for.end29

for.body11:                                       ; preds = %for.cond9
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %22 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx12 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %22
  %23 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx12, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %23, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4, !tbaa !2
  %24 = bitcast i16* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %24) #10
  %25 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %25, i32 0, i32 2
  %26 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges13, i32 0, i32 %26
  %27 = load i16, i16* %arrayidx14, align 2, !tbaa !6
  store i16 %27, i16* %max, align 2, !tbaa !6
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %28 = load i16, i16* %m_handleSentinel, align 2, !tbaa !17
  %29 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4, !tbaa !2
  %30 = load i16, i16* %max, align 2, !tbaa !6
  %idxprom = zext i16 %30 to i32
  %arrayidx15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %29, i32 %idxprom
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx15, i32 0, i32 0
  store i16 %28, i16* %m_pos, align 2, !tbaa !37
  %31 = load i32, i32* %axis, align 4, !tbaa !34
  %32 = load i16, i16* %max, align 2, !tbaa !6
  %33 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %31, i16 zeroext %32, %class.btDispatcher* %33, i1 zeroext false)
  %34 = bitcast i16* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %34) #10
  %35 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %35, i32 0, i32 1
  %36 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %36
  %37 = load i16, i16* %arrayidx16, align 2, !tbaa !6
  store i16 %37, i16* %i, align 2, !tbaa !6
  %m_handleSentinel17 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %38 = load i16, i16* %m_handleSentinel17, align 2, !tbaa !17
  %39 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4, !tbaa !2
  %40 = load i16, i16* %i, align 2, !tbaa !6
  %idxprom18 = zext i16 %40 to i32
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %39, i32 %idxprom18
  %m_pos20 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx19, i32 0, i32 0
  store i16 %38, i16* %m_pos20, align 2, !tbaa !37
  %41 = load i32, i32* %axis, align 4, !tbaa !34
  %42 = load i16, i16* %i, align 2, !tbaa !6
  %43 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %41, i16 zeroext %42, %class.btDispatcher* %43, i1 zeroext false)
  %44 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4, !tbaa !2
  %45 = load i32, i32* %limit, align 4, !tbaa !34
  %sub21 = sub nsw i32 %45, 1
  %arrayidx22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %44, i32 %sub21
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx22, i32 0, i32 1
  store i16 0, i16* %m_handle, align 2, !tbaa !39
  %m_handleSentinel23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 2
  %46 = load i16, i16* %m_handleSentinel23, align 2, !tbaa !17
  %47 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges, align 4, !tbaa !2
  %48 = load i32, i32* %limit, align 4, !tbaa !34
  %sub24 = sub nsw i32 %48, 1
  %arrayidx25 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %47, i32 %sub24
  %m_pos26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx25, i32 0, i32 0
  store i16 %46, i16* %m_pos26, align 2, !tbaa !37
  %49 = bitcast i16* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %49) #10
  %50 = bitcast i16* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %50) #10
  %51 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdges to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #10
  br label %for.inc27

for.inc27:                                        ; preds = %for.body11
  %52 = load i32, i32* %axis, align 4, !tbaa !34
  %inc28 = add nsw i32 %52, 1
  store i32 %inc28, i32* %axis, align 4, !tbaa !34
  br label %for.cond9

for.end29:                                        ; preds = %for.cond9
  %53 = load i16, i16* %handle.addr, align 2, !tbaa !6
  call void @_ZN20btAxisSweep3InternalItE10freeHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %53)
  %54 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  %55 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i16 %edge, i16* %edge.addr, align 2, !tbaa !6
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %1 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %2
  %3 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %4 = load i16, i16* %edge.addr, align 2, !tbaa !6
  %conv = zext i16 %4 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %3, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %6, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %8, i32 0, i32 1
  %9 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %9)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end31, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %10, i32 0, i32 1
  %11 = load i16, i16* %m_handle3, align 2, !tbaa !39
  %tobool = icmp ne i16 %11, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %12 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %12, i32 0, i32 0
  %13 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv4 = zext i16 %13 to i32
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_pos5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %14, i32 0, i32 0
  %15 = load i16, i16* %m_pos5, align 2, !tbaa !37
  %conv6 = zext i16 %15 to i32
  %cmp = icmp sge i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %16 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %16, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %18, i32 0, i32 1
  %19 = load i16, i16* %m_handle7, align 2, !tbaa !39
  %call8 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %19)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %20 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %21
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %22 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %23 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl9 = shl i32 1, %23
  %and10 = and i32 %shl9, 3
  store i32 %and10, i32* %axis2, align 4, !tbaa !34
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %call11 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %24)
  %tobool12 = icmp ne i16 %call11, 0
  br i1 %tobool12, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %25 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool13 = trunc i8 %25 to i1
  br i1 %tobool13, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.then
  %26 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %27 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %28 = load i32, i32* %axis1, align 4, !tbaa !34
  %29 = load i32, i32* %axis2, align 4, !tbaa !34
  %call14 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %26, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %27, i32 %28, i32 %29)
  br i1 %call14, label %if.then15, label %if.end27

if.then15:                                        ; preds = %land.lhs.true
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #10
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %31, i32 0, i32 1
  %32 = load i16, i16* %m_handle16, align 2, !tbaa !39
  %call17 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %32)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call17, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %33 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle18 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %34, i32 0, i32 1
  %35 = load i16, i16* %m_handle18, align 2, !tbaa !39
  %call19 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %35)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call19, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %36 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %37 = bitcast %class.btOverlappingPairCache* %36 to %class.btOverlappingPairCallback*
  %38 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = bitcast %class.btOverlappingPairCallback* %37 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %42, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %43 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call20 = call %struct.btBroadphasePair* %43(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %44 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !19
  %tobool21 = icmp ne %class.btOverlappingPairCallback* %44, null
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then15
  %m_userPairCallback23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %45 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback23, align 4, !tbaa !19
  %46 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %46 to %struct.btBroadphaseProxy*
  %48 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %49 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %48 to %struct.btBroadphaseProxy*
  %50 = bitcast %class.btOverlappingPairCallback* %45 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable24 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %50, align 4, !tbaa !11
  %vfn25 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable24, i64 2
  %51 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn25, align 4
  %call26 = call %struct.btBroadphasePair* %51(%class.btOverlappingPairCallback* %45, %struct.btBroadphaseProxy* %47, %struct.btBroadphaseProxy* %49)
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then15
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #10
  %53 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  br label %if.end27

if.end27:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %54 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %54, i32 0, i32 1
  %55 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx28 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %55
  %56 = load i16, i16* %arrayidx28, align 2, !tbaa !6
  %dec = add i16 %56, -1
  store i16 %dec, i16* %arrayidx28, align 2, !tbaa !6
  br label %if.end31

if.else:                                          ; preds = %while.body
  %57 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %57, i32 0, i32 2
  %58 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx29 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %58
  %59 = load i16, i16* %arrayidx29, align 2, !tbaa !6
  %dec30 = add i16 %59, -1
  store i16 %dec30, i16* %arrayidx29, align 2, !tbaa !6
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.end27
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_maxEdges32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %60, i32 0, i32 2
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx33 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges32, i32 0, i32 %61
  %62 = load i16, i16* %arrayidx33, align 2, !tbaa !6
  %inc = add i16 %62, 1
  store i16 %inc, i16* %arrayidx33, align 2, !tbaa !6
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #10
  %64 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %65 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %65, i8* align 2 %66, i32 4, i1 false), !tbaa.struct !77
  %67 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %68 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %68 to i8*
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %69, i8* align 2 %70, i32 4, i1 false), !tbaa.struct !77
  %71 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %72, i8* align 2 %73, i32 4, i1 false), !tbaa.struct !77
  %74 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %74, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %75 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %incdec.ptr34 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %75, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr34, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %76) #10
  %77 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  %78 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #10
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  br label %while.cond

while.end:                                        ; preds = %land.end
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  %81 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  %82 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this, i32 %axis, i16 zeroext %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned short>::Edge", align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i16 %edge, i16* %edge.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx, align 4, !tbaa !2
  %3 = load i16, i16* %edge.addr, align 2, !tbaa !6
  %conv = zext i16 %3 to i32
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %2, i32 %conv
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %5, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %6 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %7, i32 0, i32 1
  %8 = load i16, i16* %m_handle, align 2, !tbaa !39
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %8)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end31, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %9, i32 0, i32 1
  %10 = load i16, i16* %m_handle3, align 2, !tbaa !39
  %tobool = icmp ne i16 %10, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %11, i32 0, i32 0
  %12 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv4 = zext i16 %12 to i32
  %13 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_pos5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %13, i32 0, i32 0
  %14 = load i16, i16* %m_pos5, align 2, !tbaa !37
  %conv6 = zext i16 %14 to i32
  %cmp = icmp sge i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %15 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %15, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %16 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %17, i32 0, i32 1
  %18 = load i16, i16* %m_handle7, align 2, !tbaa !39
  %call8 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %18)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %19 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %call9 = call zeroext i16 @_ZNK20btAxisSweep3InternalItE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned short>::Edge"* %19)
  %tobool10 = icmp ne i16 %call9, 0
  br i1 %tobool10, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %21, i32 0, i32 1
  %22 = load i16, i16* %m_handle11, align 2, !tbaa !39
  %call12 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %22)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #10
  %24 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %24, i32 0, i32 1
  %25 = load i16, i16* %m_handle13, align 2, !tbaa !39
  %call14 = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %25)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call14, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %26 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #10
  %27 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %27
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %28 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #10
  %29 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl15 = shl i32 1, %29
  %and16 = and i32 %shl15, 3
  store i32 %and16, i32* %axis2, align 4, !tbaa !34
  %30 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool17 = trunc i8 %30 to i1
  br i1 %tobool17, label %land.lhs.true, label %if.end27

land.lhs.true:                                    ; preds = %if.then
  %31 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %32 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %33 = load i32, i32* %axis1, align 4, !tbaa !34
  %34 = load i32, i32* %axis2, align 4, !tbaa !34
  %call18 = call zeroext i1 @_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal* %this1, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %31, %"class.btAxisSweep3Internal<unsigned short>::Handle"* %32, i32 %33, i32 %34)
  br i1 %call18, label %if.then19, label %if.end27

if.then19:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 12
  %35 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !18
  %36 = bitcast %class.btOverlappingPairCache* %35 to %class.btOverlappingPairCallback*
  %37 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %40 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %39 to %struct.btBroadphaseProxy*
  %41 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %42 = bitcast %class.btOverlappingPairCallback* %36 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %42, align 4, !tbaa !11
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %43 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call20 = call i8* %43(%class.btOverlappingPairCallback* %36, %struct.btBroadphaseProxy* %38, %struct.btBroadphaseProxy* %40, %class.btDispatcher* %41)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %44 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !19
  %tobool21 = icmp ne %class.btOverlappingPairCallback* %44, null
  br i1 %tobool21, label %if.then22, label %if.end

if.then22:                                        ; preds = %if.then19
  %m_userPairCallback23 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 13
  %45 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback23, align 4, !tbaa !19
  %46 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0, align 4, !tbaa !2
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %46 to %struct.btBroadphaseProxy*
  %48 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1, align 4, !tbaa !2
  %49 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"* %48 to %struct.btBroadphaseProxy*
  %50 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %51 = bitcast %class.btOverlappingPairCallback* %45 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable24 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %51, align 4, !tbaa !11
  %vfn25 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable24, i64 3
  %52 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn25, align 4
  %call26 = call i8* %52(%class.btOverlappingPairCallback* %45, %struct.btBroadphaseProxy* %47, %struct.btBroadphaseProxy* %49, %class.btDispatcher* %50)
  br label %if.end

if.end:                                           ; preds = %if.then22, %if.then19
  br label %if.end27

if.end27:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %53 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %53, i32 0, i32 2
  %54 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx28 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %54
  %55 = load i16, i16* %arrayidx28, align 2, !tbaa !6
  %dec = add i16 %55, -1
  store i16 %dec, i16* %arrayidx28, align 2, !tbaa !6
  %56 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  %57 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #10
  %58 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  br label %if.end31

if.else:                                          ; preds = %while.body
  %60 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %60, i32 0, i32 1
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx29 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %61
  %62 = load i16, i16* %arrayidx29, align 2, !tbaa !6
  %dec30 = add i16 %62, -1
  store i16 %dec30, i16* %arrayidx29, align 2, !tbaa !6
  br label %if.end31

if.end31:                                         ; preds = %if.else, %if.end27
  %63 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_minEdges32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %63, i32 0, i32 1
  %64 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx33 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges32, i32 0, i32 %64
  %65 = load i16, i16* %arrayidx33, align 2, !tbaa !6
  %inc = add i16 %65, 1
  store i16 %inc, i16* %arrayidx33, align 2, !tbaa !6
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #10
  %67 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %68 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %68, i8* align 2 %69, i32 4, i1 false), !tbaa.struct !77
  %70 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %71 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %72, i8* align 2 %73, i32 4, i1 false), !tbaa.struct !77
  %74 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %75 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %74 to i8*
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %75, i8* align 2 %76, i32 4, i1 false), !tbaa.struct !77
  %77 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %77, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge, align 4, !tbaa !2
  %78 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %incdec.ptr34 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %78, i32 1
  store %"class.btAxisSweep3Internal<unsigned short>::Edge"* %incdec.ptr34, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext, align 4, !tbaa !2
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  br label %while.cond

while.end:                                        ; preds = %land.end
  %81 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  %82 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #10
  %83 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE10freeHandleEt(%class.btAxisSweep3Internal* %this, i16 zeroext %handle) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i16 %handle, i16* %handle.addr, align 2, !tbaa !6
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = load i16, i16* %handle.addr, align 2, !tbaa !6
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %0)
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  %1 = load i16, i16* %m_firstFreeHandle, align 4, !tbaa !33
  call void @_ZN20btAxisSweep3InternalItE6Handle11SetNextFreeEt(%"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, i16 zeroext %1)
  %2 = load i16, i16* %handle.addr, align 2, !tbaa !6
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 9
  store i16 %2, i16* %m_firstFreeHandle2, align 4, !tbaa !33
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 6
  %3 = load i16, i16* %m_numHandles, align 4, !tbaa !32
  %dec = add i16 %3, -1
  store i16 %dec, i16* %m_numHandles, align 4, !tbaa !32
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal* %this, i16 zeroext %handle, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %handle.addr = alloca i16, align 2
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %min = alloca [3 x i16], align 2
  %max = alloca [3 x i16], align 2
  %axis = alloca i32, align 4
  %emin = alloca i16, align 2
  %emax = alloca i16, align 2
  %dmin = alloca i32, align 4
  %dmax = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store i16 %handle, i16* %handle.addr, align 2, !tbaa !6
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i16, i16* %handle.addr, align 2, !tbaa !6
  %call = call %"class.btAxisSweep3Internal<unsigned short>::Handle"* @_ZNK20btAxisSweep3InternalItE9getHandleEt(%class.btAxisSweep3Internal* %this1, i16 zeroext %1)
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %2 = bitcast [3 x i16]* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %2) #10
  %3 = bitcast [3 x i16]* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 6, i8* %3) #10
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 0
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i(%class.btAxisSweep3Internal* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, i32 1)
  %6 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast i16* %emin to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %9) #10
  %10 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %10, i32 0, i32 1
  %11 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %11
  %12 = load i16, i16* %arrayidx, align 2, !tbaa !6
  store i16 %12, i16* %emin, align 2, !tbaa !6
  %13 = bitcast i16* %emax to i8*
  call void @llvm.lifetime.start.p0i8(i64 2, i8* %13) #10
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %14, i32 0, i32 2
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %15
  %16 = load i16, i16* %arrayidx3, align 2, !tbaa !6
  store i16 %16, i16* %emax, align 2, !tbaa !6
  %17 = bitcast i32* %dmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx4 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %18
  %19 = load i16, i16* %arrayidx4, align 2, !tbaa !6
  %conv = zext i16 %19 to i32
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges, i32 0, i32 %20
  %21 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx5, align 4, !tbaa !2
  %22 = load i16, i16* %emin, align 2, !tbaa !6
  %idxprom = zext i16 %22 to i32
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %21, i32 %idxprom
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx6, i32 0, i32 0
  %23 = load i16, i16* %m_pos, align 2, !tbaa !37
  %conv7 = zext i16 %23 to i32
  %sub = sub nsw i32 %conv, %conv7
  store i32 %sub, i32* %dmin, align 4, !tbaa !34
  %24 = bitcast i32* %dmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx8 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %25
  %26 = load i16, i16* %arrayidx8, align 2, !tbaa !6
  %conv9 = zext i16 %26 to i32
  %m_pEdges10 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %27 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx11 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges10, i32 0, i32 %27
  %28 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx11, align 4, !tbaa !2
  %29 = load i16, i16* %emax, align 2, !tbaa !6
  %idxprom12 = zext i16 %29 to i32
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %28, i32 %idxprom12
  %m_pos14 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx13, i32 0, i32 0
  %30 = load i16, i16* %m_pos14, align 2, !tbaa !37
  %conv15 = zext i16 %30 to i32
  %sub16 = sub nsw i32 %conv9, %conv15
  store i32 %sub16, i32* %dmax, align 4, !tbaa !34
  %31 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx17 = getelementptr inbounds [3 x i16], [3 x i16]* %min, i32 0, i32 %31
  %32 = load i16, i16* %arrayidx17, align 2, !tbaa !6
  %m_pEdges18 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %33 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx19 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges18, i32 0, i32 %33
  %34 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx19, align 4, !tbaa !2
  %35 = load i16, i16* %emin, align 2, !tbaa !6
  %idxprom20 = zext i16 %35 to i32
  %arrayidx21 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %34, i32 %idxprom20
  %m_pos22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx21, i32 0, i32 0
  store i16 %32, i16* %m_pos22, align 2, !tbaa !37
  %36 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx23 = getelementptr inbounds [3 x i16], [3 x i16]* %max, i32 0, i32 %36
  %37 = load i16, i16* %arrayidx23, align 2, !tbaa !6
  %m_pEdges24 = getelementptr inbounds %class.btAxisSweep3Internal, %class.btAxisSweep3Internal* %this1, i32 0, i32 10
  %38 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx25 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned short>::Edge"*]* %m_pEdges24, i32 0, i32 %38
  %39 = load %"class.btAxisSweep3Internal<unsigned short>::Edge"*, %"class.btAxisSweep3Internal<unsigned short>::Edge"** %arrayidx25, align 4, !tbaa !2
  %40 = load i16, i16* %emax, align 2, !tbaa !6
  %idxprom26 = zext i16 %40 to i32
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %39, i32 %idxprom26
  %m_pos28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Edge", %"class.btAxisSweep3Internal<unsigned short>::Edge"* %arrayidx27, i32 0, i32 0
  store i16 %37, i16* %m_pos28, align 2, !tbaa !37
  %41 = load i32, i32* %dmin, align 4, !tbaa !34
  %cmp29 = icmp slt i32 %41, 0
  br i1 %cmp29, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %42 = load i32, i32* %axis, align 4, !tbaa !34
  %43 = load i16, i16* %emin, align 2, !tbaa !6
  %44 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %42, i16 zeroext %43, %class.btDispatcher* %44, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %45 = load i32, i32* %dmax, align 4, !tbaa !34
  %cmp30 = icmp sgt i32 %45, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end
  %46 = load i32, i32* %axis, align 4, !tbaa !34
  %47 = load i16, i16* %emax, align 2, !tbaa !6
  %48 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %46, i16 zeroext %47, %class.btDispatcher* %48, i1 zeroext true)
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end
  %49 = load i32, i32* %dmin, align 4, !tbaa !34
  %cmp33 = icmp sgt i32 %49, 0
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end32
  %50 = load i32, i32* %axis, align 4, !tbaa !34
  %51 = load i16, i16* %emin, align 2, !tbaa !6
  %52 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %50, i16 zeroext %51, %class.btDispatcher* %52, i1 zeroext true)
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end32
  %53 = load i32, i32* %dmax, align 4, !tbaa !34
  %cmp36 = icmp slt i32 %53, 0
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end35
  %54 = load i32, i32* %axis, align 4, !tbaa !34
  %55 = load i16, i16* %emax, align 2, !tbaa !6
  %56 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb(%class.btAxisSweep3Internal* %this1, i32 %54, i16 zeroext %55, %class.btDispatcher* %56, i1 zeroext true)
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %if.end35
  %57 = bitcast i32* %dmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #10
  %58 = bitcast i32* %dmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast i16* %emax to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %59) #10
  %60 = bitcast i16* %emin to i8*
  call void @llvm.lifetime.end.p0i8(i64 2, i8* %60) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end38
  %61 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %62 = bitcast [3 x i16]* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %62) #10
  %63 = bitcast [3 x i16]* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 6, i8* %63) #10
  %64 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #6 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %overlap) #10
  store i8 1, i8* %overlap, align 1, !tbaa !8
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4, !tbaa !29
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4, !tbaa !29
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4, !tbaa !29
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4, !tbaa !29
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1, !tbaa !8, !range !10
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1, !tbaa !8
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4, !tbaa !29
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4, !tbaa !29
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4, !tbaa !29
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4, !tbaa !29
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1, !tbaa !8, !range !10
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1, !tbaa !8
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4, !tbaa !2
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4, !tbaa !29
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4, !tbaa !2
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4, !tbaa !29
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4, !tbaa !2
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4, !tbaa !29
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4, !tbaa !2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4, !tbaa !29
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1, !tbaa !8, !range !10
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1, !tbaa !8
  %27 = load i8, i8* %overlap, align 1, !tbaa !8, !range !10
  %tobool31 = trunc i8 %27 to i1
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %overlap) #10
  ret i1 %tobool31
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.5* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.5* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.5* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !34
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !34
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  %2 = load i32, i32* %curSize, align 4, !tbaa !34
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  store i32 %4, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !34
  %6 = load i32, i32* %curSize, align 4, !tbaa !34
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %9 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end18

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load i32, i32* %curSize, align 4, !tbaa !34
  store i32 %14, i32* %i6, align 4, !tbaa !34
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc15, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !34
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #10
  br label %for.end17

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data11, align 4, !tbaa !71
  %19 = load i32, i32* %i6, align 4, !tbaa !34
  %arrayidx12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %18, i32 %19
  %20 = bitcast %struct.btBroadphasePair* %arrayidx12 to i8*
  %call13 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %struct.btBroadphasePair*
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4, !tbaa !2
  %call14 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %21, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %22)
  br label %for.inc15

for.inc15:                                        ; preds = %for.body10
  %23 = load i32, i32* %i6, align 4, !tbaa !34
  %inc16 = add nsw i32 %23, 1
  store i32 %inc16, i32* %i6, align 4, !tbaa !34
  br label %for.cond7

for.end17:                                        ; preds = %for.cond.cleanup9
  br label %if.end18

if.end18:                                         ; preds = %for.end17, %for.end
  %24 = load i32, i32* %newsize.addr, align 4, !tbaa !34
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 %24, i32* %m_size, align 4, !tbaa !72
  %25 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %25) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !62
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.8* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4, !tbaa !28
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #2 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4, !tbaa !59
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4, !tbaa !61
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pHandleA = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %pHandleB = alloca %"class.btAxisSweep3Internal<unsigned short>::Handle"*, align 4
  %axis = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btAxisSweep3Internal* %this, %class.btAxisSweep3Internal** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal*, %class.btAxisSweep3Internal** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4, !tbaa !2
  %3 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btBroadphaseProxy* %4 to %"class.btAxisSweep3Internal<unsigned short>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned short>::Handle"* %5, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4, !tbaa !2
  %6 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %8 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %8, i32 0, i32 2
  %9 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges, i32 0, i32 %9
  %10 = load i16, i16* %arrayidx, align 2, !tbaa !6
  %conv = zext i16 %10 to i32
  %11 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %11, i32 0, i32 1
  %12 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges, i32 0, i32 %12
  %13 = load i16, i16* %arrayidx2, align 2, !tbaa !6
  %conv3 = zext i16 %13 to i32
  %cmp4 = icmp slt i32 %conv, %conv3
  br i1 %cmp4, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %14 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB, align 4, !tbaa !2
  %m_maxEdges5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %14, i32 0, i32 2
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_maxEdges5, i32 0, i32 %15
  %16 = load i16, i16* %arrayidx6, align 2, !tbaa !6
  %conv7 = zext i16 %16 to i32
  %17 = load %"class.btAxisSweep3Internal<unsigned short>::Handle"*, %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA, align 4, !tbaa !2
  %m_minEdges8 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned short>::Handle", %"class.btAxisSweep3Internal<unsigned short>::Handle"* %17, i32 0, i32 1
  %18 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_minEdges8, i32 0, i32 %18
  %19 = load i16, i16* %arrayidx9, align 2, !tbaa !6
  %conv10 = zext i16 %19 to i32
  %cmp11 = icmp slt i32 %conv7, %conv10
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %21 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup12 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup12

cleanup12:                                        ; preds = %for.end, %cleanup
  %22 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned short>::Handle"** %pHandleA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.5* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  store i32 %lo, i32* %lo.addr, align 4, !tbaa !34
  store i32 %hi, i32* %hi.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %lo.addr, align 4, !tbaa !34
  store i32 %1, i32* %i, align 4, !tbaa !34
  %2 = bitcast i32* %j to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #10
  %3 = load i32, i32* %hi.addr, align 4, !tbaa !34
  store i32 %3, i32* %j, align 4, !tbaa !34
  %4 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %4) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %6 = load i32, i32* %lo.addr, align 4, !tbaa !34
  %7 = load i32, i32* %hi.addr, align 4, !tbaa !34
  %add = add nsw i32 %6, %7
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %8 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !71
  %10 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %8, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %12 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !71
  %14 = load i32, i32* %j, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 %14
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %12, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %15 = load i32, i32* %j, align 4, !tbaa !34
  %dec = add nsw i32 %15, -1
  store i32 %dec, i32* %j, align 4, !tbaa !34
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %16 = load i32, i32* %i, align 4, !tbaa !34
  %17 = load i32, i32* %j, align 4, !tbaa !34
  %cmp = icmp sle i32 %16, %17
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %18 = load i32, i32* %i, align 4, !tbaa !34
  %19 = load i32, i32* %j, align 4, !tbaa !34
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.5* %this1, i32 %18, i32 %19)
  %20 = load i32, i32* %i, align 4, !tbaa !34
  %inc11 = add nsw i32 %20, 1
  store i32 %inc11, i32* %i, align 4, !tbaa !34
  %21 = load i32, i32* %j, align 4, !tbaa !34
  %dec12 = add nsw i32 %21, -1
  store i32 %dec12, i32* %j, align 4, !tbaa !34
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %22 = load i32, i32* %i, align 4, !tbaa !34
  %23 = load i32, i32* %j, align 4, !tbaa !34
  %cmp13 = icmp sle i32 %22, %23
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %24 = load i32, i32* %lo.addr, align 4, !tbaa !34
  %25 = load i32, i32* %j, align 4, !tbaa !34
  %cmp14 = icmp slt i32 %24, %25
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %26 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %27 = load i32, i32* %lo.addr, align 4, !tbaa !34
  %28 = load i32, i32* %j, align 4, !tbaa !34
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.5* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %26, i32 %27, i32 %28)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %29 = load i32, i32* %i, align 4, !tbaa !34
  %30 = load i32, i32* %hi.addr, align 4, !tbaa !34
  %cmp17 = icmp slt i32 %29, %30
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %31 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4, !tbaa !2
  %32 = load i32, i32* %i, align 4, !tbaa !34
  %33 = load i32, i32* %hi.addr, align 4, !tbaa !34
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.5* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %31, i32 %32, i32 %33)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  %34 = bitcast %struct.btBroadphasePair* %x to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %34) #10
  %35 = bitcast i32* %j to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %35) #10
  %36 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %36) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !59
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4, !tbaa !61
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4, !tbaa !62
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !62
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.8* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4, !tbaa !2
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon.8* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4, !tbaa !28
  store i8* %9, i8** %m_internalInfo1, align 4, !tbaa !28
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 0, i32 0
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4, !tbaa !59
  %tobool = icmp ne %struct.btBroadphaseProxy* %2, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 0, i32 0
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4, !tbaa !59
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = load i32, i32* %m_uniqueId, align 4, !tbaa !58
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4, !tbaa !34
  %6 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4, !tbaa !59
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %8, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 0, i32 0
  %10 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4, !tbaa !59
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 4
  %11 = load i32, i32* %m_uniqueId7, align 4, !tbaa !58
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %11, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4, !tbaa !34
  %12 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 1
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4, !tbaa !61
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %14, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4, !tbaa !61
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %16, i32 0, i32 4
  %17 = load i32, i32* %m_uniqueId14, align 4, !tbaa !58
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %17, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4, !tbaa !34
  %18 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 1
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4, !tbaa !61
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %20, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %21 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %21, i32 0, i32 1
  %22 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4, !tbaa !61
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %22, i32 0, i32 4
  %23 = load i32, i32* %m_uniqueId22, align 4, !tbaa !58
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %23, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4, !tbaa !34
  %24 = load i32, i32* %uidA0, align 4, !tbaa !34
  %25 = load i32, i32* %uidB0, align 4, !tbaa !34
  %cmp = icmp sgt i32 %24, %25
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %26 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %26, i32 0, i32 0
  %27 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4, !tbaa !59
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4, !tbaa !59
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %27, %29
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %30 = load i32, i32* %uidA1, align 4, !tbaa !34
  %31 = load i32, i32* %uidB1, align 4, !tbaa !34
  %cmp29 = icmp sgt i32 %30, %31
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 0
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4, !tbaa !59
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 0
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4, !tbaa !59
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 1
  %37 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4, !tbaa !61
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 1
  %39 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4, !tbaa !61
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %37, %39
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %40 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4, !tbaa !2
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %40, i32 0, i32 2
  %41 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4, !tbaa !62
  %42 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4, !tbaa !2
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %42, i32 0, i32 2
  %43 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4, !tbaa !62
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %41, %43
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %44 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %45 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %44, %land.end ]
  %46 = bitcast i32* %uidB1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #10
  %47 = bitcast i32* %uidA1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #10
  %48 = bitcast i32* %uidB0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %48) #10
  %49 = bitcast i32* %uidA0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #10
  ret i1 %45
}

define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.5* %this, i32 %index0, i32 %index1) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %index0, i32* %index0.addr, align 4, !tbaa !34
  store i32 %index1, i32* %index1.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %2 = load i32, i32* %index0.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %1, i32 %2
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4, !tbaa !71
  %4 = load i32, i32* %index1.addr, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4, !tbaa !71
  %6 = load i32, i32* %index0.addr, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  %7 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %8 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !63
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %9 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4, !tbaa !71
  %10 = load i32, i32* %index1.addr, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %9, i32 %10
  %11 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %12 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false), !tbaa.struct !63
  %13 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %13) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.5* %this, i32 %_Count) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !34
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !34
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.5* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %3, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !68
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %5, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !34
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !73
  %7 = bitcast %struct.btBroadphasePair** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #10
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #2 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !66
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !73
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.5* %this, i32 %size) #6 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !34
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !34
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !34
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.5* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !34
  store i32 %end, i32* %end.addr, align 4, !tbaa !34
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %start.addr, align 4, !tbaa !34
  store i32 %1, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !34
  %3 = load i32, i32* %end.addr, align 4, !tbaa !34
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4, !tbaa !71
  %9 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %7, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !34
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #10
  ret void
}

define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.6* %this, i32 %n, %struct.btBroadphasePair** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !34
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !34
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

define linkonce_odr hidden i32 @_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_(%class.btAxisSweep3Internal.4* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %pOwner, i16 signext %collisionFilterGroup, i16 signext %collisionFilterMask, %class.btDispatcher* %dispatcher, i8* %multiSapProxy) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %pOwner.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i16, align 2
  %collisionFilterMask.addr = alloca i16, align 2
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %multiSapProxy.addr = alloca i8*, align 4
  %min = alloca [3 x i32], align 4
  %max = alloca [3 x i32], align 4
  %handle = alloca i32, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store i8* %pOwner, i8** %pOwner.addr, align 4, !tbaa !2
  store i16 %collisionFilterGroup, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  store i16 %collisionFilterMask, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  store i8* %multiSapProxy, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast [3 x i32]* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %0) #10
  %1 = bitcast [3 x i32]* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %1) #10
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.4* %this1, i32* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 0
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.4* %this1, i32* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, i32 1)
  %4 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %call = call i32 @_ZN20btAxisSweep3InternalIjE11allocHandleEv(%class.btAxisSweep3Internal.4* %this1)
  store i32 %call, i32* %handle, align 4, !tbaa !34
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load i32, i32* %handle, align 4, !tbaa !34
  %call3 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %6)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call3, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %7 = load i32, i32* %handle, align 4, !tbaa !34
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %9 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %8 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 4
  store i32 %7, i32* %m_uniqueId, align 4, !tbaa !58
  %10 = load i8*, i8** %pOwner.addr, align 4, !tbaa !2
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %12 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %11 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %12, i32 0, i32 0
  store i8* %10, i8** %m_clientObject, align 4, !tbaa !35
  %13 = load i16, i16* %collisionFilterGroup.addr, align 2, !tbaa !6
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %15 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %14 to %struct.btBroadphaseProxy*
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %15, i32 0, i32 1
  store i16 %13, i16* %m_collisionFilterGroup, align 4, !tbaa !75
  %16 = load i16, i16* %collisionFilterMask.addr, align 2, !tbaa !6
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %18 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %17 to %struct.btBroadphaseProxy*
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 2
  store i16 %16, i16* %m_collisionFilterMask, align 2, !tbaa !76
  %19 = load i8*, i8** %multiSapProxy.addr, align 4, !tbaa !2
  %20 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %20 to %struct.btBroadphaseProxy*
  %m_multiSapParentProxy = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 3
  store i8* %19, i8** %m_multiSapParentProxy, align 4, !tbaa !74
  %22 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %23 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %mul = mul i32 %23, 2
  store i32 %mul, i32* %limit, align 4, !tbaa !34
  %24 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %25 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp ult i32 %25, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %26 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %27 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %27, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 0, i32 2
  %28 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx4, align 4, !tbaa !34
  %add = add i32 %29, 2
  store i32 %add, i32* %arrayidx4, align 4, !tbaa !34
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %30 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %30
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx5, align 4, !tbaa !2
  %32 = load i32, i32* %limit, align 4, !tbaa !34
  %sub = sub i32 %32, 1
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %31, i32 %sub
  %m_pEdges7 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %33 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx8 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges7, i32 0, i32 %33
  %34 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx8, align 4, !tbaa !2
  %35 = load i32, i32* %limit, align 4, !tbaa !34
  %add9 = add i32 %35, 1
  %arrayidx10 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %34, i32 %add9
  %36 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx10 to i8*
  %37 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 8, i1 false), !tbaa.struct !78
  %38 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx11, align 4, !tbaa !34
  %m_pEdges12 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %40 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx13 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges12, i32 0, i32 %40
  %41 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx13, align 4, !tbaa !2
  %42 = load i32, i32* %limit, align 4, !tbaa !34
  %sub14 = sub i32 %42, 1
  %arrayidx15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %41, i32 %sub14
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx15, i32 0, i32 0
  store i32 %39, i32* %m_pos, align 4, !tbaa !53
  %43 = load i32, i32* %handle, align 4, !tbaa !34
  %m_pEdges16 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %44 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx17 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges16, i32 0, i32 %44
  %45 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx17, align 4, !tbaa !2
  %46 = load i32, i32* %limit, align 4, !tbaa !34
  %sub18 = sub i32 %46, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %45, i32 %sub18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx19, i32 0, i32 1
  store i32 %43, i32* %m_handle, align 4, !tbaa !55
  %47 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %47
  %48 = load i32, i32* %arrayidx20, align 4, !tbaa !34
  %m_pEdges21 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %49 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx22 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges21, i32 0, i32 %49
  %50 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx22, align 4, !tbaa !2
  %51 = load i32, i32* %limit, align 4, !tbaa !34
  %arrayidx23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %50, i32 %51
  %m_pos24 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx23, i32 0, i32 0
  store i32 %48, i32* %m_pos24, align 4, !tbaa !53
  %52 = load i32, i32* %handle, align 4, !tbaa !34
  %m_pEdges25 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %53 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx26 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges25, i32 0, i32 %53
  %54 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx26, align 4, !tbaa !2
  %55 = load i32, i32* %limit, align 4, !tbaa !34
  %arrayidx27 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %54, i32 %55
  %m_handle28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx27, i32 0, i32 1
  store i32 %52, i32* %m_handle28, align 4, !tbaa !55
  %56 = load i32, i32* %limit, align 4, !tbaa !34
  %sub29 = sub i32 %56, 1
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %57, i32 0, i32 1
  %58 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx30 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %58
  store i32 %sub29, i32* %arrayidx30, align 4, !tbaa !34
  %59 = load i32, i32* %limit, align 4, !tbaa !34
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges31 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %60, i32 0, i32 2
  %61 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges31, i32 0, i32 %61
  store i32 %59, i32* %arrayidx32, align 4, !tbaa !34
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %62 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add i32 %62, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %63 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges33 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %63, i32 0, i32 1
  %arrayidx34 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges33, i32 0, i32 0
  %64 = load i32, i32* %arrayidx34, align 4, !tbaa !34
  %65 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 0, i32 %64, %class.btDispatcher* %65, i1 zeroext false)
  %66 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges35 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %66, i32 0, i32 2
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges35, i32 0, i32 0
  %67 = load i32, i32* %arrayidx36, align 4, !tbaa !34
  %68 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 0, i32 %67, %class.btDispatcher* %68, i1 zeroext false)
  %69 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges37 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %69, i32 0, i32 1
  %arrayidx38 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges37, i32 0, i32 1
  %70 = load i32, i32* %arrayidx38, align 4, !tbaa !34
  %71 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 1, i32 %70, %class.btDispatcher* %71, i1 zeroext false)
  %72 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges39 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %72, i32 0, i32 2
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges39, i32 0, i32 1
  %73 = load i32, i32* %arrayidx40, align 4, !tbaa !34
  %74 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 1, i32 %73, %class.btDispatcher* %74, i1 zeroext false)
  %75 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges41 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %75, i32 0, i32 1
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges41, i32 0, i32 2
  %76 = load i32, i32* %arrayidx42, align 4, !tbaa !34
  %77 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 2, i32 %76, %class.btDispatcher* %77, i1 zeroext true)
  %78 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges43 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %78, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges43, i32 0, i32 2
  %79 = load i32, i32* %arrayidx44, align 4, !tbaa !34
  %80 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 2, i32 %79, %class.btDispatcher* %80, i1 zeroext true)
  %81 = load i32, i32* %handle, align 4, !tbaa !34
  %82 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #10
  %83 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #10
  %84 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %84) #10
  %85 = bitcast [3 x i32]* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %85) #10
  %86 = bitcast [3 x i32]* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %86) #10
  ret i32 %81
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %0 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %1 = load i32, i32* %index.addr, align 4, !tbaa !34
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %0, i32 %1
  ret %"class.btAxisSweep3Internal<unsigned int>::Handle"* %add.ptr
}

define linkonce_odr hidden void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.4* %this, i32* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %out.addr = alloca i32*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32* %out, i32** %out.addr, align 4, !tbaa !2
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4, !tbaa !2
  store i32 %isMax, i32* %isMax.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #10
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #10
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4, !tbaa !2
  %m_worldAabbMin = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_worldAabbMin)
  %m_quantize = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_quantize)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %3) #10
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !29
  %cmp = fcmp ole float %4, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  br label %cond.end14

cond.false:                                       ; preds = %entry
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %6 = load float, float* %arrayidx3, align 4, !tbaa !29
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_handleSentinel, align 4, !tbaa !42
  %conv = uitofp i32 %7 to float
  %cmp4 = fcmp oge float %6, %conv
  br i1 %cmp4, label %cond.true5, label %cond.false7

cond.true5:                                       ; preds = %cond.false
  %m_handleSentinel6 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_handleSentinel6, align 4, !tbaa !42
  %m_bpHandleMask = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %9 = load i32, i32* %m_bpHandleMask, align 4, !tbaa !40
  %and = and i32 %8, %9
  %10 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or = or i32 %and, %10
  br label %cond.end

cond.false7:                                      ; preds = %cond.false
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %11 = load float, float* %arrayidx9, align 4, !tbaa !29
  %conv10 = fptoui float %11 to i32
  %m_bpHandleMask11 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %12 = load i32, i32* %m_bpHandleMask11, align 4, !tbaa !40
  %and12 = and i32 %conv10, %12
  %13 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or13 = or i32 %and12, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false7, %cond.true5
  %cond = phi i32 [ %or, %cond.true5 ], [ %or13, %cond.false7 ]
  br label %cond.end14

cond.end14:                                       ; preds = %cond.end, %cond.true
  %cond15 = phi i32 [ %5, %cond.true ], [ %cond, %cond.end ]
  %14 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx16 = getelementptr inbounds i32, i32* %14, i32 0
  store i32 %cond15, i32* %arrayidx16, align 4, !tbaa !34
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %15 = load float, float* %arrayidx18, align 4, !tbaa !29
  %cmp19 = fcmp ole float %15, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end14
  %16 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  br label %cond.end41

cond.false21:                                     ; preds = %cond.end14
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %17 = load float, float* %arrayidx23, align 4, !tbaa !29
  %m_handleSentinel24 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %18 = load i32, i32* %m_handleSentinel24, align 4, !tbaa !42
  %conv25 = uitofp i32 %18 to float
  %cmp26 = fcmp oge float %17, %conv25
  br i1 %cmp26, label %cond.true27, label %cond.false32

cond.true27:                                      ; preds = %cond.false21
  %m_handleSentinel28 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %19 = load i32, i32* %m_handleSentinel28, align 4, !tbaa !42
  %m_bpHandleMask29 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %20 = load i32, i32* %m_bpHandleMask29, align 4, !tbaa !40
  %and30 = and i32 %19, %20
  %21 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or31 = or i32 %and30, %21
  br label %cond.end39

cond.false32:                                     ; preds = %cond.false21
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  %22 = load float, float* %arrayidx34, align 4, !tbaa !29
  %conv35 = fptoui float %22 to i32
  %m_bpHandleMask36 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %23 = load i32, i32* %m_bpHandleMask36, align 4, !tbaa !40
  %and37 = and i32 %conv35, %23
  %24 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or38 = or i32 %and37, %24
  br label %cond.end39

cond.end39:                                       ; preds = %cond.false32, %cond.true27
  %cond40 = phi i32 [ %or31, %cond.true27 ], [ %or38, %cond.false32 ]
  br label %cond.end41

cond.end41:                                       ; preds = %cond.end39, %cond.true20
  %cond42 = phi i32 [ %16, %cond.true20 ], [ %cond40, %cond.end39 ]
  %25 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx43 = getelementptr inbounds i32, i32* %25, i32 1
  store i32 %cond42, i32* %arrayidx43, align 4, !tbaa !34
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 2
  %26 = load float, float* %arrayidx45, align 4, !tbaa !29
  %cmp46 = fcmp ole float %26, 0.000000e+00
  br i1 %cmp46, label %cond.true47, label %cond.false48

cond.true47:                                      ; preds = %cond.end41
  %27 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  br label %cond.end68

cond.false48:                                     ; preds = %cond.end41
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 2
  %28 = load float, float* %arrayidx50, align 4, !tbaa !29
  %m_handleSentinel51 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %29 = load i32, i32* %m_handleSentinel51, align 4, !tbaa !42
  %conv52 = uitofp i32 %29 to float
  %cmp53 = fcmp oge float %28, %conv52
  br i1 %cmp53, label %cond.true54, label %cond.false59

cond.true54:                                      ; preds = %cond.false48
  %m_handleSentinel55 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %30 = load i32, i32* %m_handleSentinel55, align 4, !tbaa !42
  %m_bpHandleMask56 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %31 = load i32, i32* %m_bpHandleMask56, align 4, !tbaa !40
  %and57 = and i32 %30, %31
  %32 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or58 = or i32 %and57, %32
  br label %cond.end66

cond.false59:                                     ; preds = %cond.false48
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 2
  %33 = load float, float* %arrayidx61, align 4, !tbaa !29
  %conv62 = fptoui float %33 to i32
  %m_bpHandleMask63 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 1
  %34 = load i32, i32* %m_bpHandleMask63, align 4, !tbaa !40
  %and64 = and i32 %conv62, %34
  %35 = load i32, i32* %isMax.addr, align 4, !tbaa !34
  %or65 = or i32 %and64, %35
  br label %cond.end66

cond.end66:                                       ; preds = %cond.false59, %cond.true54
  %cond67 = phi i32 [ %or58, %cond.true54 ], [ %or65, %cond.false59 ]
  br label %cond.end68

cond.end68:                                       ; preds = %cond.end66, %cond.true47
  %cond69 = phi i32 [ %27, %cond.true47 ], [ %cond67, %cond.end66 ]
  %36 = load i32*, i32** %out.addr, align 4, !tbaa !2
  %arrayidx70 = getelementptr inbounds i32, i32* %36, i32 2
  store i32 %cond69, i32* %arrayidx70, align 4, !tbaa !34
  %37 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %37) #10
  ret void
}

define linkonce_odr hidden i32 @_ZN20btAxisSweep3InternalIjE11allocHandleEv(%class.btAxisSweep3Internal.4* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %handle = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  %1 = load i32, i32* %m_firstFreeHandle, align 4, !tbaa !52
  store i32 %1, i32* %handle, align 4, !tbaa !34
  %2 = load i32, i32* %handle, align 4, !tbaa !34
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %2)
  %call2 = call i32 @_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %call)
  %m_firstFreeHandle3 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  store i32 %call2, i32* %m_firstFreeHandle3, align 4, !tbaa !52
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %inc = add i32 %3, 1
  store i32 %inc, i32* %m_numHandles, align 4, !tbaa !51
  %4 = load i32, i32* %handle, align 4, !tbaa !34
  %5 = bitcast i32* %handle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #10
  ret i32 %4
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this, i32 %axis, i32 %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i32 %edge, i32* %edge.addr, align 4, !tbaa !34
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %1 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %2
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %4 = load i32, i32* %edge.addr, align 4, !tbaa !34
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %3, i32 %4
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %6, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %8, i32 0, i32 1
  %9 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %9)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end23, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %10, i32 0, i32 0
  %11 = load i32, i32* %m_pos, align 4, !tbaa !53
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %m_pos3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %12, i32 0, i32 0
  %13 = load i32, i32* %m_pos3, align 4, !tbaa !53
  %cmp = icmp ult i32 %11, %13
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %14 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %15, i32 0, i32 1
  %16 = load i32, i32* %m_handle4, align 4, !tbaa !55
  %call5 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %16)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call5, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %call6 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %17)
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %18 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #10
  %19 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %19
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %20 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl7 = shl i32 1, %21
  %and8 = and i32 %shl7, 3
  store i32 %and8, i32* %axis2, align 4, !tbaa !34
  %22 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool9 = trunc i8 %22 to i1
  br i1 %tobool9, label %land.lhs.true, label %if.end19

land.lhs.true:                                    ; preds = %if.then
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %25 = load i32, i32* %axis1, align 4, !tbaa !34
  %26 = load i32, i32* %axis2, align 4, !tbaa !34
  %call10 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.4* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %23, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %24, i32 %25, i32 %26)
  br i1 %call10, label %if.then11, label %if.end19

if.then11:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %27 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %28 = bitcast %class.btOverlappingPairCache* %27 to %class.btOverlappingPairCallback*
  %29 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %29 to %struct.btBroadphaseProxy*
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %32 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %31 to %struct.btBroadphaseProxy*
  %33 = bitcast %class.btOverlappingPairCallback* %28 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %33, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %34 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call12 = call %struct.btBroadphasePair* %34(%class.btOverlappingPairCallback* %28, %struct.btBroadphaseProxy* %30, %struct.btBroadphaseProxy* %32)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %35 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !44
  %tobool13 = icmp ne %class.btOverlappingPairCallback* %35, null
  br i1 %tobool13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then11
  %m_userPairCallback15 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %36 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback15, align 4, !tbaa !44
  %37 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %40 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %39 to %struct.btBroadphaseProxy*
  %41 = bitcast %class.btOverlappingPairCallback* %36 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable16 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %41, align 4, !tbaa !11
  %vfn17 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable16, i64 2
  %42 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn17, align 4
  %call18 = call %struct.btBroadphasePair* %42(%class.btOverlappingPairCallback* %36, %struct.btBroadphaseProxy* %38, %struct.btBroadphaseProxy* %40)
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then11
  br label %if.end19

if.end19:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %43 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %43, i32 0, i32 2
  %44 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %44
  %45 = load i32, i32* %arrayidx20, align 4, !tbaa !34
  %inc = add i32 %45, 1
  store i32 %inc, i32* %arrayidx20, align 4, !tbaa !34
  %46 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %46) #10
  %47 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %47) #10
  br label %if.end23

if.else:                                          ; preds = %while.body
  %48 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %48, i32 0, i32 1
  %49 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx21, align 4, !tbaa !34
  %inc22 = add i32 %50, 1
  store i32 %inc22, i32* %arrayidx21, align 4, !tbaa !34
  br label %if.end23

if.end23:                                         ; preds = %if.else, %if.end19
  %51 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_minEdges24 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %51, i32 0, i32 1
  %52 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges24, i32 0, i32 %52
  %53 = load i32, i32* %arrayidx25, align 4, !tbaa !34
  %dec = add i32 %53, -1
  store i32 %dec, i32* %arrayidx25, align 4, !tbaa !34
  %54 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %54) #10
  %55 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %57 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 8, i1 false), !tbaa.struct !78
  %58 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %59 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %60 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %59 to i8*
  %61 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 8, i1 false), !tbaa.struct !78
  %62 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %62 to i8*
  %64 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %63, i8* align 4 %64, i32 8, i1 false), !tbaa.struct !78
  %65 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %65, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %66 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %incdec.ptr26 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %66, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr26, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %67 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %67) #10
  %68 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %68) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %69) #10
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %70) #10
  %71 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %71) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this, i32 %axis, i32 %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pPrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandlePrev = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i32 %edge, i32* %edge.addr, align 4, !tbaa !34
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %3 = load i32, i32* %edge.addr, align 4, !tbaa !34
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %2, i32 %3
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %5, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %6 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %7, i32 0, i32 1
  %8 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %8)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end27, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_pos, align 4, !tbaa !53
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %m_pos3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %11, i32 0, i32 0
  %12 = load i32, i32* %m_pos3, align 4, !tbaa !53
  %cmp = icmp ult i32 %10, %12
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %13 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %14, i32 0, i32 1
  %15 = load i32, i32* %m_handle4, align 4, !tbaa !55
  %call5 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %15)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call5, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %16 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %call6 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %16)
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle7 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %18, i32 0, i32 1
  %19 = load i32, i32* %m_handle7, align 4, !tbaa !55
  %call8 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %19)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call8, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %21, i32 0, i32 1
  %22 = load i32, i32* %m_handle9, align 4, !tbaa !55
  %call10 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %22)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %23 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #10
  %24 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %24
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %25 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #10
  %26 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl11 = shl i32 1, %26
  %and12 = and i32 %shl11, 3
  store i32 %and12, i32* %axis2, align 4, !tbaa !34
  %27 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool13 = trunc i8 %27 to i1
  br i1 %tobool13, label %land.lhs.true, label %if.end23

land.lhs.true:                                    ; preds = %if.then
  %28 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %29 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %30 = load i32, i32* %axis1, align 4, !tbaa !34
  %31 = load i32, i32* %axis2, align 4, !tbaa !34
  %call14 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.4* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %28, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %29, i32 %30, i32 %31)
  br i1 %call14, label %if.then15, label %if.end23

if.then15:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %32 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %33 = bitcast %class.btOverlappingPairCache* %32 to %class.btOverlappingPairCallback*
  %34 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %35 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %34 to %struct.btBroadphaseProxy*
  %36 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %37 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %36 to %struct.btBroadphaseProxy*
  %38 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %39 = bitcast %class.btOverlappingPairCallback* %33 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %39, align 4, !tbaa !11
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %40 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call16 = call i8* %40(%class.btOverlappingPairCallback* %33, %struct.btBroadphaseProxy* %35, %struct.btBroadphaseProxy* %37, %class.btDispatcher* %38)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %41 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !44
  %tobool17 = icmp ne %class.btOverlappingPairCallback* %41, null
  br i1 %tobool17, label %if.then18, label %if.end

if.then18:                                        ; preds = %if.then15
  %m_userPairCallback19 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %42 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback19, align 4, !tbaa !44
  %43 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %44 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %43 to %struct.btBroadphaseProxy*
  %45 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %46 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %45 to %struct.btBroadphaseProxy*
  %47 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %48 = bitcast %class.btOverlappingPairCallback* %42 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable20 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %48, align 4, !tbaa !11
  %vfn21 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable20, i64 3
  %49 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn21, align 4
  %call22 = call i8* %49(%class.btOverlappingPairCallback* %42, %struct.btBroadphaseProxy* %44, %struct.btBroadphaseProxy* %46, %class.btDispatcher* %47)
  br label %if.end

if.end:                                           ; preds = %if.then18, %if.then15
  br label %if.end23

if.end23:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %50 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %50, i32 0, i32 1
  %51 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx24 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx24, align 4, !tbaa !34
  %inc = add i32 %52, 1
  store i32 %inc, i32* %arrayidx24, align 4, !tbaa !34
  %53 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  %54 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  %55 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  br label %if.end27

if.else:                                          ; preds = %while.body
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %57, i32 0, i32 2
  %58 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx25, align 4, !tbaa !34
  %inc26 = add i32 %59, 1
  store i32 %inc26, i32* %arrayidx25, align 4, !tbaa !34
  br label %if.end27

if.end27:                                         ; preds = %if.else, %if.end23
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_maxEdges28 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %60, i32 0, i32 2
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges28, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx29, align 4, !tbaa !34
  %dec = add i32 %62, -1
  store i32 %dec, i32* %arrayidx29, align 4, !tbaa !34
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %63) #10
  %64 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %65 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 8, i1 false), !tbaa.struct !78
  %67 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %68 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %68 to i8*
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 8, i1 false), !tbaa.struct !78
  %71 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 8, i1 false), !tbaa.struct !78
  %74 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %74, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %75 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %incdec.ptr30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %75, i32 -1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr30, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev, align 4, !tbaa !2
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %76) #10
  %77 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandlePrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %78 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #10
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pPrev to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAxisSweep3InternalIjE6Handle11GetNextFreeEv(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %this.addr, align 4
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 0
  %0 = load i32, i32* %arrayidx, align 4, !tbaa !34
  ret i32 %0
}

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %this, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %this.addr, align 4, !tbaa !2
  %this1 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %this.addr, align 4
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_pos, align 4, !tbaa !53
  %and = and i32 %0, 1
  ret i32 %and
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.4* %this, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleB, i32 %axis0, i32 %axis1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %pHandleA.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleB.addr = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis0.addr = alloca i32, align 4
  %axis1.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleA, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %pHandleB, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  store i32 %axis0, i32* %axis0.addr, align 4, !tbaa !34
  store i32 %axis1, i32* %axis1.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %0, i32 0, i32 2
  %1 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4, !tbaa !34
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %3, i32 0, i32 1
  %4 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx2, align 4, !tbaa !34
  %cmp = icmp ult i32 %2, %5
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_maxEdges3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %6, i32 0, i32 2
  %7 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges3, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx4, align 4, !tbaa !34
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_minEdges5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %9, i32 0, i32 1
  %10 = load i32, i32* %axis0.addr, align 4, !tbaa !34
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges5, i32 0, i32 %10
  %11 = load i32, i32* %arrayidx6, align 4, !tbaa !34
  %cmp7 = icmp ult i32 %8, %11
  br i1 %cmp7, label %if.then, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_maxEdges9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %12, i32 0, i32 2
  %13 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx10 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges9, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx10, align 4, !tbaa !34
  %15 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_minEdges11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %15, i32 0, i32 1
  %16 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges11, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx12, align 4, !tbaa !34
  %cmp13 = icmp ult i32 %14, %17
  br i1 %cmp13, label %if.then, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %lor.lhs.false8
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB.addr, align 4, !tbaa !2
  %m_maxEdges15 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %18, i32 0, i32 2
  %19 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx16 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges15, i32 0, i32 %19
  %20 = load i32, i32* %arrayidx16, align 4, !tbaa !34
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA.addr, align 4, !tbaa !2
  %m_minEdges17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %21, i32 0, i32 1
  %22 = load i32, i32* %axis1.addr, align 4, !tbaa !34
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges17, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx18, align 4, !tbaa !34
  %cmp19 = icmp ult i32 %20, %23
  br i1 %cmp19, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false14, %lor.lhs.false8, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false14
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher(%class.btAxisSweep3Internal.4* %this, i32 %handle, %class.btDispatcher* %dispatcher) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %handle.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %limit = alloca i32, align 4
  %axis = alloca i32, align 4
  %pEdges = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %max = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %handle, i32* %handle.addr, align 4, !tbaa !34
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %handle.addr, align 4, !tbaa !34
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %1)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %3 = bitcast %class.btOverlappingPairCache* %2 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %3, align 4, !tbaa !11
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %4 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call zeroext i1 %4(%class.btOverlappingPairCache* %2)
  br i1 %call2, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_pairCache3 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache3, align 4, !tbaa !43
  %6 = bitcast %class.btOverlappingPairCache* %5 to %class.btOverlappingPairCallback*
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %8 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %7 to %struct.btBroadphaseProxy*
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %10 = bitcast %class.btOverlappingPairCallback* %6 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable4 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %10, align 4, !tbaa !11
  %vfn5 = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable4, i64 4
  %11 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn5, align 4
  call void %11(%class.btOverlappingPairCallback* %6, %struct.btBroadphaseProxy* %8, %class.btDispatcher* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #10
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %13 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %mul = mul i32 %13, 2
  store i32 %mul, i32* %limit, align 4, !tbaa !34
  %14 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %15, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 8
  %16 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %m_pHandles, align 4, !tbaa !49
  %arrayidx = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %16, i32 0
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %arrayidx, i32 0, i32 2
  %17 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx6, align 4, !tbaa !34
  %sub = sub i32 %18, 2
  store i32 %sub, i32* %arrayidx6, align 4, !tbaa !34
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc24, %for.end
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp8 = icmp slt i32 %20, 3
  br i1 %cmp8, label %for.body9, label %for.end26

for.body9:                                        ; preds = %for.cond7
  %21 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %22 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx10 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %22
  %23 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx10, align 4, !tbaa !2
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %23, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4, !tbaa !2
  %24 = bitcast i32* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %25, i32 0, i32 2
  %26 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges11, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx12, align 4, !tbaa !34
  store i32 %27, i32* %max, align 4, !tbaa !34
  %m_handleSentinel = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %28 = load i32, i32* %m_handleSentinel, align 4, !tbaa !42
  %29 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4, !tbaa !2
  %30 = load i32, i32* %max, align 4, !tbaa !34
  %arrayidx13 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %29, i32 %30
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx13, i32 0, i32 0
  store i32 %28, i32* %m_pos, align 4, !tbaa !53
  %31 = load i32, i32* %axis, align 4, !tbaa !34
  %32 = load i32, i32* %max, align 4, !tbaa !34
  %33 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %31, i32 %32, %class.btDispatcher* %33, i1 zeroext false)
  %34 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #10
  %35 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %35, i32 0, i32 1
  %36 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %36
  %37 = load i32, i32* %arrayidx14, align 4, !tbaa !34
  store i32 %37, i32* %i, align 4, !tbaa !34
  %m_handleSentinel15 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %38 = load i32, i32* %m_handleSentinel15, align 4, !tbaa !42
  %39 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4, !tbaa !2
  %40 = load i32, i32* %i, align 4, !tbaa !34
  %arrayidx16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %39, i32 %40
  %m_pos17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx16, i32 0, i32 0
  store i32 %38, i32* %m_pos17, align 4, !tbaa !53
  %41 = load i32, i32* %axis, align 4, !tbaa !34
  %42 = load i32, i32* %i, align 4, !tbaa !34
  %43 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %41, i32 %42, %class.btDispatcher* %43, i1 zeroext false)
  %44 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4, !tbaa !2
  %45 = load i32, i32* %limit, align 4, !tbaa !34
  %sub18 = sub nsw i32 %45, 1
  %arrayidx19 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %44, i32 %sub18
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx19, i32 0, i32 1
  store i32 0, i32* %m_handle, align 4, !tbaa !55
  %m_handleSentinel20 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 2
  %46 = load i32, i32* %m_handleSentinel20, align 4, !tbaa !42
  %47 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges, align 4, !tbaa !2
  %48 = load i32, i32* %limit, align 4, !tbaa !34
  %sub21 = sub nsw i32 %48, 1
  %arrayidx22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %47, i32 %sub21
  %m_pos23 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx22, i32 0, i32 0
  store i32 %46, i32* %m_pos23, align 4, !tbaa !53
  %49 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %49) #10
  %50 = bitcast i32* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %50) #10
  %51 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdges to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %51) #10
  br label %for.inc24

for.inc24:                                        ; preds = %for.body9
  %52 = load i32, i32* %axis, align 4, !tbaa !34
  %inc25 = add nsw i32 %52, 1
  store i32 %inc25, i32* %axis, align 4, !tbaa !34
  br label %for.cond7

for.end26:                                        ; preds = %for.cond7
  %53 = load i32, i32* %handle.addr, align 4, !tbaa !34
  call void @_ZN20btAxisSweep3InternalIjE10freeHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %53)
  %54 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #10
  %55 = bitcast i32* %limit to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #10
  %56 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this, i32 %axis, i32 %edge, %class.btDispatcher* %0, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i32 %edge, i32* %edge.addr, align 4, !tbaa !34
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %1 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %2 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %2
  %3 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %4 = load i32, i32* %edge.addr, align 4, !tbaa !34
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %3, i32 %4
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %5 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #10
  %6 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %6, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %7 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #10
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %8, i32 0, i32 1
  %9 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %9)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %10, i32 0, i32 1
  %11 = load i32, i32* %m_handle3, align 4, !tbaa !55
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %12 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %12, i32 0, i32 0
  %13 = load i32, i32* %m_pos, align 4, !tbaa !53
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %14, i32 0, i32 0
  %15 = load i32, i32* %m_pos4, align 4, !tbaa !53
  %cmp = icmp uge i32 %13, %15
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %16 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %16, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %17 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %18, i32 0, i32 1
  %19 = load i32, i32* %m_handle5, align 4, !tbaa !55
  %call6 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %19)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call6, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %20 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %21
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %22 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #10
  %23 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl7 = shl i32 1, %23
  %and8 = and i32 %shl7, 3
  store i32 %and8, i32* %axis2, align 4, !tbaa !34
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %call9 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %24)
  %tobool10 = icmp ne i32 %call9, 0
  br i1 %tobool10, label %if.else, label %if.then

if.then:                                          ; preds = %while.body
  %25 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool11 = trunc i8 %25 to i1
  br i1 %tobool11, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %26 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %27 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %28 = load i32, i32* %axis1, align 4, !tbaa !34
  %29 = load i32, i32* %axis2, align 4, !tbaa !34
  %call12 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.4* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %26, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %27, i32 %28, i32 %29)
  br i1 %call12, label %if.then13, label %if.end25

if.then13:                                        ; preds = %land.lhs.true
  %30 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #10
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle14 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %31, i32 0, i32 1
  %32 = load i32, i32* %m_handle14, align 4, !tbaa !55
  %call15 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %32)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call15, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %33 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #10
  %34 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %34, i32 0, i32 1
  %35 = load i32, i32* %m_handle16, align 4, !tbaa !55
  %call17 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %35)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call17, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %36 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %37 = bitcast %class.btOverlappingPairCache* %36 to %class.btOverlappingPairCallback*
  %38 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %39 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %38 to %struct.btBroadphaseProxy*
  %40 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %41 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %40 to %struct.btBroadphaseProxy*
  %42 = bitcast %class.btOverlappingPairCallback* %37 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %42, align 4, !tbaa !11
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %43 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call18 = call %struct.btBroadphasePair* %43(%class.btOverlappingPairCallback* %37, %struct.btBroadphaseProxy* %39, %struct.btBroadphaseProxy* %41)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %44 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !44
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %44, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then13
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %45 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4, !tbaa !44
  %46 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %46 to %struct.btBroadphaseProxy*
  %48 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %49 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %48 to %struct.btBroadphaseProxy*
  %50 = bitcast %class.btOverlappingPairCallback* %45 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable22 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %50, align 4, !tbaa !11
  %vfn23 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable22, i64 2
  %51 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn23, align 4
  %call24 = call %struct.btBroadphasePair* %51(%class.btOverlappingPairCallback* %45, %struct.btBroadphaseProxy* %47, %struct.btBroadphaseProxy* %49)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then13
  %52 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %52) #10
  %53 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #10
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %54 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %54, i32 0, i32 1
  %55 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %55
  %56 = load i32, i32* %arrayidx26, align 4, !tbaa !34
  %dec = add i32 %56, -1
  store i32 %dec, i32* %arrayidx26, align 4, !tbaa !34
  br label %if.end29

if.else:                                          ; preds = %while.body
  %57 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %57, i32 0, i32 2
  %58 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %58
  %59 = load i32, i32* %arrayidx27, align 4, !tbaa !34
  %dec28 = add i32 %59, -1
  store i32 %dec28, i32* %arrayidx27, align 4, !tbaa !34
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_maxEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %60, i32 0, i32 2
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges30, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx31, align 4, !tbaa !34
  %inc = add i32 %62, 1
  store i32 %inc, i32* %arrayidx31, align 4, !tbaa !34
  %63 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %63) #10
  %64 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %65 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %65, i8* align 4 %66, i32 8, i1 false), !tbaa.struct !78
  %67 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %68 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %68 to i8*
  %70 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 8, i1 false), !tbaa.struct !78
  %71 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 8, i1 false), !tbaa.struct !78
  %74 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %74, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %75 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %75, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %76) #10
  %77 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %77) #10
  %78 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %78) #10
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %79) #10
  br label %while.cond

while.end:                                        ; preds = %land.end
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  %81 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  %82 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #10
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this, i32 %axis, i32 %edge, %class.btDispatcher* %dispatcher, i1 zeroext %updateOverlaps) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %axis.addr = alloca i32, align 4
  %edge.addr = alloca i32, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %updateOverlaps.addr = alloca i8, align 1
  %pEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge"*, align 4
  %pHandleEdge = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleNext = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle0 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %handle1 = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis1 = alloca i32, align 4
  %axis2 = alloca i32, align 4
  %swap = alloca %"class.btAxisSweep3Internal<unsigned int>::Edge", align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %axis, i32* %axis.addr, align 4, !tbaa !34
  store i32 %edge, i32* %edge.addr, align 4, !tbaa !34
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %frombool = zext i1 %updateOverlaps to i8
  store i8 %frombool, i8* %updateOverlaps.addr, align 1, !tbaa !8
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %1 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %1
  %2 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx, align 4, !tbaa !2
  %3 = load i32, i32* %edge.addr, align 4, !tbaa !34
  %add.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %2, i32 %3
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %4 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #10
  %5 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %add.ptr2 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %5, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %add.ptr2, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %6 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  %7 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %7, i32 0, i32 1
  %8 = load i32, i32* %m_handle, align 4, !tbaa !55
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %8)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  br label %while.cond

while.cond:                                       ; preds = %if.end29, %entry
  %9 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle3 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %9, i32 0, i32 1
  %10 = load i32, i32* %m_handle3, align 4, !tbaa !55
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %11, i32 0, i32 0
  %12 = load i32, i32* %m_pos, align 4, !tbaa !53
  %13 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_pos4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %13, i32 0, i32 0
  %14 = load i32, i32* %m_pos4, align 4, !tbaa !53
  %cmp = icmp uge i32 %12, %14
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %15 = phi i1 [ false, %while.cond ], [ %cmp, %land.rhs ]
  br i1 %15, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %16 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #10
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle5 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %17, i32 0, i32 1
  %18 = load i32, i32* %m_handle5, align 4, !tbaa !55
  %call6 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %18)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call6, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %19 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %call7 = call i32 @_ZNK20btAxisSweep3InternalIjE4Edge5IsMaxEv(%"class.btAxisSweep3Internal<unsigned int>::Edge"* %19)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %20 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #10
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %m_handle9 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %21, i32 0, i32 1
  %22 = load i32, i32* %m_handle9, align 4, !tbaa !55
  %call10 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %22)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call10, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #10
  %24 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %m_handle11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %24, i32 0, i32 1
  %25 = load i32, i32* %m_handle11, align 4, !tbaa !55
  %call12 = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %25)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call12, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %26 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #10
  %27 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %shl = shl i32 1, %27
  %and = and i32 %shl, 3
  store i32 %and, i32* %axis1, align 4, !tbaa !34
  %28 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #10
  %29 = load i32, i32* %axis1, align 4, !tbaa !34
  %shl13 = shl i32 1, %29
  %and14 = and i32 %shl13, 3
  store i32 %and14, i32* %axis2, align 4, !tbaa !34
  %30 = load i8, i8* %updateOverlaps.addr, align 1, !tbaa !8, !range !10
  %tobool15 = trunc i8 %30 to i1
  br i1 %tobool15, label %land.lhs.true, label %if.end25

land.lhs.true:                                    ; preds = %if.then
  %31 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %32 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %33 = load i32, i32* %axis1, align 4, !tbaa !34
  %34 = load i32, i32* %axis2, align 4, !tbaa !34
  %call16 = call zeroext i1 @_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii(%class.btAxisSweep3Internal.4* %this1, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %31, %"class.btAxisSweep3Internal<unsigned int>::Handle"* %32, i32 %33, i32 %34)
  br i1 %call16, label %if.then17, label %if.end25

if.then17:                                        ; preds = %land.lhs.true
  %m_pairCache = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 12
  %35 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4, !tbaa !43
  %36 = bitcast %class.btOverlappingPairCache* %35 to %class.btOverlappingPairCallback*
  %37 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %38 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %37 to %struct.btBroadphaseProxy*
  %39 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %40 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %39 to %struct.btBroadphaseProxy*
  %41 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %42 = bitcast %class.btOverlappingPairCallback* %36 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %42, align 4, !tbaa !11
  %vfn = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 3
  %43 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  %call18 = call i8* %43(%class.btOverlappingPairCallback* %36, %struct.btBroadphaseProxy* %38, %struct.btBroadphaseProxy* %40, %class.btDispatcher* %41)
  %m_userPairCallback = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %44 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback, align 4, !tbaa !44
  %tobool19 = icmp ne %class.btOverlappingPairCallback* %44, null
  br i1 %tobool19, label %if.then20, label %if.end

if.then20:                                        ; preds = %if.then17
  %m_userPairCallback21 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 13
  %45 = load %class.btOverlappingPairCallback*, %class.btOverlappingPairCallback** %m_userPairCallback21, align 4, !tbaa !44
  %46 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0, align 4, !tbaa !2
  %47 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %46 to %struct.btBroadphaseProxy*
  %48 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1, align 4, !tbaa !2
  %49 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"* %48 to %struct.btBroadphaseProxy*
  %50 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %51 = bitcast %class.btOverlappingPairCallback* %45 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable22 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %51, align 4, !tbaa !11
  %vfn23 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable22, i64 3
  %52 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn23, align 4
  %call24 = call i8* %52(%class.btOverlappingPairCallback* %45, %struct.btBroadphaseProxy* %47, %struct.btBroadphaseProxy* %49, %class.btDispatcher* %50)
  br label %if.end

if.end:                                           ; preds = %if.then20, %if.then17
  br label %if.end25

if.end25:                                         ; preds = %if.end, %land.lhs.true, %if.then
  %53 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %53, i32 0, i32 2
  %54 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %54
  %55 = load i32, i32* %arrayidx26, align 4, !tbaa !34
  %dec = add i32 %55, -1
  store i32 %dec, i32* %arrayidx26, align 4, !tbaa !34
  %56 = bitcast i32* %axis2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %56) #10
  %57 = bitcast i32* %axis1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #10
  %58 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %handle0 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  br label %if.end29

if.else:                                          ; preds = %while.body
  %60 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %60, i32 0, i32 1
  %61 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx27 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx27, align 4, !tbaa !34
  %dec28 = add i32 %62, -1
  store i32 %dec28, i32* %arrayidx27, align 4, !tbaa !34
  br label %if.end29

if.end29:                                         ; preds = %if.else, %if.end25
  %63 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge, align 4, !tbaa !2
  %m_minEdges30 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %63, i32 0, i32 1
  %64 = load i32, i32* %axis.addr, align 4, !tbaa !34
  %arrayidx31 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges30, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx31, align 4, !tbaa !34
  %inc = add i32 %65, 1
  store i32 %inc, i32* %arrayidx31, align 4, !tbaa !34
  %66 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* %66) #10
  %67 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %68 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  %69 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %68, i8* align 4 %69, i32 8, i1 false), !tbaa.struct !78
  %70 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %71 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %72 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %71 to i8*
  %73 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %70 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 8, i1 false), !tbaa.struct !78
  %74 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %75 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %74 to i8*
  %76 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 8, i1 false), !tbaa.struct !78
  %77 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %incdec.ptr = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %77, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge, align 4, !tbaa !2
  %78 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %incdec.ptr32 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %78, i32 1
  store %"class.btAxisSweep3Internal<unsigned int>::Edge"* %incdec.ptr32, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext, align 4, !tbaa !2
  %79 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"* %swap to i8*
  call void @llvm.lifetime.end.p0i8(i64 8, i8* %79) #10
  %80 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %80) #10
  br label %while.cond

while.end:                                        ; preds = %land.end
  %81 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %81) #10
  %82 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pNext to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %82) #10
  %83 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Edge"** %pEdge to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %83) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE10freeHandleEj(%class.btAxisSweep3Internal.4* %this, i32 %handle) #1 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %handle.addr = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %handle, i32* %handle.addr, align 4, !tbaa !34
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = load i32, i32* %handle.addr, align 4, !tbaa !34
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %0)
  %m_firstFreeHandle = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  %1 = load i32, i32* %m_firstFreeHandle, align 4, !tbaa !52
  call void @_ZN20btAxisSweep3InternalIjE6Handle11SetNextFreeEj(%"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, i32 %1)
  %2 = load i32, i32* %handle.addr, align 4, !tbaa !34
  %m_firstFreeHandle2 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 9
  store i32 %2, i32* %m_firstFreeHandle2, align 4, !tbaa !52
  %m_numHandles = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_numHandles, align 4, !tbaa !51
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_numHandles, align 4, !tbaa !51
  ret void
}

define linkonce_odr hidden void @_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher(%class.btAxisSweep3Internal.4* %this, i32 %handle, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %dispatcher) #0 comdat {
entry:
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %handle.addr = alloca i32, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pHandle = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %min = alloca [3 x i32], align 4
  %max = alloca [3 x i32], align 4
  %axis = alloca i32, align 4
  %emin = alloca i32, align 4
  %emax = alloca i32, align 4
  %dmin = alloca i32, align 4
  %dmax = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store i32 %handle, i32* %handle.addr, align 4, !tbaa !34
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load i32, i32* %handle.addr, align 4, !tbaa !34
  %call = call %"class.btAxisSweep3Internal<unsigned int>::Handle"* @_ZNK20btAxisSweep3InternalIjE9getHandleEj(%class.btAxisSweep3Internal.4* %this1, i32 %1)
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %call, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %2 = bitcast [3 x i32]* %min to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %2) #10
  %3 = bitcast [3 x i32]* %max to i8*
  call void @llvm.lifetime.start.p0i8(i64 12, i8* %3) #10
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 0
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.4* %this1, i32* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 0
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4, !tbaa !2
  call void @_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i(%class.btAxisSweep3Internal.4* %this1, i32* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, i32 1)
  %6 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %8 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #10
  br label %for.end

for.body:                                         ; preds = %for.cond
  %9 = bitcast i32* %emin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #10
  %10 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %10, i32 0, i32 1
  %11 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx, align 4, !tbaa !34
  store i32 %12, i32* %emin, align 4, !tbaa !34
  %13 = bitcast i32* %emax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #10
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %14, i32 0, i32 2
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx3, align 4, !tbaa !34
  store i32 %16, i32* %emax, align 4, !tbaa !34
  %17 = bitcast i32* %dmin to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #10
  %18 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx4, align 4, !tbaa !34
  %m_pEdges = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges, i32 0, i32 %20
  %21 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx5, align 4, !tbaa !2
  %22 = load i32, i32* %emin, align 4, !tbaa !34
  %arrayidx6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %21, i32 %22
  %m_pos = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx6, i32 0, i32 0
  %23 = load i32, i32* %m_pos, align 4, !tbaa !53
  %sub = sub nsw i32 %19, %23
  store i32 %sub, i32* %dmin, align 4, !tbaa !34
  %24 = bitcast i32* %dmax to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #10
  %25 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %25
  %26 = load i32, i32* %arrayidx7, align 4, !tbaa !34
  %m_pEdges8 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %27 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx9 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges8, i32 0, i32 %27
  %28 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx9, align 4, !tbaa !2
  %29 = load i32, i32* %emax, align 4, !tbaa !34
  %arrayidx10 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %28, i32 %29
  %m_pos11 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx10, i32 0, i32 0
  %30 = load i32, i32* %m_pos11, align 4, !tbaa !53
  %sub12 = sub nsw i32 %26, %30
  store i32 %sub12, i32* %dmax, align 4, !tbaa !34
  %31 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %min, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx13, align 4, !tbaa !34
  %m_pEdges14 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %33 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx15 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges14, i32 0, i32 %33
  %34 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx15, align 4, !tbaa !2
  %35 = load i32, i32* %emin, align 4, !tbaa !34
  %arrayidx16 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %34, i32 %35
  %m_pos17 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx16, i32 0, i32 0
  store i32 %32, i32* %m_pos17, align 4, !tbaa !53
  %36 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx18 = getelementptr inbounds [3 x i32], [3 x i32]* %max, i32 0, i32 %36
  %37 = load i32, i32* %arrayidx18, align 4, !tbaa !34
  %m_pEdges19 = getelementptr inbounds %class.btAxisSweep3Internal.4, %class.btAxisSweep3Internal.4* %this1, i32 0, i32 10
  %38 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx20 = getelementptr inbounds [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*], [3 x %"class.btAxisSweep3Internal<unsigned int>::Edge"*]* %m_pEdges19, i32 0, i32 %38
  %39 = load %"class.btAxisSweep3Internal<unsigned int>::Edge"*, %"class.btAxisSweep3Internal<unsigned int>::Edge"** %arrayidx20, align 4, !tbaa !2
  %40 = load i32, i32* %emax, align 4, !tbaa !34
  %arrayidx21 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %39, i32 %40
  %m_pos22 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Edge", %"class.btAxisSweep3Internal<unsigned int>::Edge"* %arrayidx21, i32 0, i32 0
  store i32 %37, i32* %m_pos22, align 4, !tbaa !53
  %41 = load i32, i32* %dmin, align 4, !tbaa !34
  %cmp23 = icmp slt i32 %41, 0
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %42 = load i32, i32* %axis, align 4, !tbaa !34
  %43 = load i32, i32* %emin, align 4, !tbaa !34
  %44 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %42, i32 %43, %class.btDispatcher* %44, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %45 = load i32, i32* %dmax, align 4, !tbaa !34
  %cmp24 = icmp sgt i32 %45, 0
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end
  %46 = load i32, i32* %axis, align 4, !tbaa !34
  %47 = load i32, i32* %emax, align 4, !tbaa !34
  %48 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %46, i32 %47, %class.btDispatcher* %48, i1 zeroext true)
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end
  %49 = load i32, i32* %dmin, align 4, !tbaa !34
  %cmp27 = icmp sgt i32 %49, 0
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %if.end26
  %50 = load i32, i32* %axis, align 4, !tbaa !34
  %51 = load i32, i32* %emin, align 4, !tbaa !34
  %52 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %50, i32 %51, %class.btDispatcher* %52, i1 zeroext true)
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %if.end26
  %53 = load i32, i32* %dmax, align 4, !tbaa !34
  %cmp30 = icmp slt i32 %53, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end29
  %54 = load i32, i32* %axis, align 4, !tbaa !34
  %55 = load i32, i32* %emax, align 4, !tbaa !34
  %56 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4, !tbaa !2
  call void @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb(%class.btAxisSweep3Internal.4* %this1, i32 %54, i32 %55, %class.btDispatcher* %56, i1 zeroext true)
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end29
  %57 = bitcast i32* %dmax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %57) #10
  %58 = bitcast i32* %dmin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %58) #10
  %59 = bitcast i32* %emax to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %59) #10
  %60 = bitcast i32* %emin to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %60) #10
  br label %for.inc

for.inc:                                          ; preds = %if.end32
  %61 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  %62 = bitcast [3 x i32]* %max to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %62) #10
  %63 = bitcast [3 x i32]* %min to i8*
  call void @llvm.lifetime.end.p0i8(i64 12, i8* %63) #10
  %64 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandle to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #10
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden zeroext i1 @_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_(%class.btAxisSweep3Internal.4* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btAxisSweep3Internal.4*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %pHandleA = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %pHandleB = alloca %"class.btAxisSweep3Internal<unsigned int>::Handle"*, align 4
  %axis = alloca i32, align 4
  %cleanup.dest.slot = alloca i32, align 4
  store %class.btAxisSweep3Internal.4* %this, %class.btAxisSweep3Internal.4** %this.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %this1 = load %class.btAxisSweep3Internal.4*, %class.btAxisSweep3Internal.4** %this.addr, align 4
  %0 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #10
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4, !tbaa !2
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %2, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4, !tbaa !2
  %3 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #10
  %4 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4, !tbaa !2
  %5 = bitcast %struct.btBroadphaseProxy* %4 to %"class.btAxisSweep3Internal<unsigned int>::Handle"*
  store %"class.btAxisSweep3Internal<unsigned int>::Handle"* %5, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4, !tbaa !2
  %6 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #10
  store i32 0, i32* %axis, align 4, !tbaa !34
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %axis, align 4, !tbaa !34
  %cmp = icmp slt i32 %7, 3
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %8 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4, !tbaa !2
  %m_maxEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %8, i32 0, i32 2
  %9 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx, align 4, !tbaa !34
  %11 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4, !tbaa !2
  %m_minEdges = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %11, i32 0, i32 1
  %12 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx2, align 4, !tbaa !34
  %cmp3 = icmp ult i32 %10, %13
  br i1 %cmp3, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %14 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB, align 4, !tbaa !2
  %m_maxEdges4 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %14, i32 0, i32 2
  %15 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %m_maxEdges4, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx5, align 4, !tbaa !34
  %17 = load %"class.btAxisSweep3Internal<unsigned int>::Handle"*, %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA, align 4, !tbaa !2
  %m_minEdges6 = getelementptr inbounds %"class.btAxisSweep3Internal<unsigned int>::Handle", %"class.btAxisSweep3Internal<unsigned int>::Handle"* %17, i32 0, i32 1
  %18 = load i32, i32* %axis, align 4, !tbaa !34
  %arrayidx7 = getelementptr inbounds [3 x i32], [3 x i32]* %m_minEdges6, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx7, align 4, !tbaa !34
  %cmp8 = icmp ult i32 %16, %19
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %20 = load i32, i32* %axis, align 4, !tbaa !34
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %axis, align 4, !tbaa !34
  br label %for.cond

cleanup:                                          ; preds = %if.then, %for.cond.cleanup
  %21 = bitcast i32* %axis to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #10
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup9 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup9

cleanup9:                                         ; preds = %for.end, %cleanup
  %22 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleB to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #10
  %23 = bitcast %"class.btAxisSweep3Internal<unsigned int>::Handle"** %pHandleA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %23) #10
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { nounwind }
attributes #11 = { noreturn nounwind }
attributes #12 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"short", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"bool", !4, i64 0}
!10 = !{i8 0, i8 2}
!11 = !{!12, !12, i64 0}
!12 = !{!"vtable pointer", !5, i64 0}
!13 = !{!14, !7, i64 4}
!14 = !{!"_ZTS20btAxisSweep3InternalItE", !7, i64 4, !7, i64 6, !15, i64 8, !15, i64 24, !15, i64 40, !7, i64 56, !7, i64 58, !3, i64 60, !7, i64 64, !4, i64 68, !4, i64 80, !3, i64 92, !3, i64 96, !9, i64 100, !16, i64 104, !3, i64 108, !3, i64 112}
!15 = !{!"_ZTS9btVector3", !4, i64 0}
!16 = !{!"int", !4, i64 0}
!17 = !{!14, !7, i64 6}
!18 = !{!14, !3, i64 92}
!19 = !{!14, !3, i64 96}
!20 = !{!14, !9, i64 100}
!21 = !{!14, !16, i64 104}
!22 = !{!14, !3, i64 108}
!23 = !{!14, !3, i64 112}
!24 = !{!25, !9, i64 193}
!25 = !{!"_ZTS16btDbvtBroadphase", !4, i64 4, !4, i64 124, !3, i64 136, !26, i64 140, !16, i64 144, !16, i64 148, !16, i64 152, !16, i64 156, !16, i64 160, !16, i64 164, !16, i64 168, !16, i64 172, !26, i64 176, !16, i64 180, !16, i64 184, !16, i64 188, !9, i64 192, !9, i64 193, !9, i64 194}
!26 = !{!"float", !4, i64 0}
!27 = !{i64 0, i64 16, !28}
!28 = !{!4, !4, i64 0}
!29 = !{!26, !26, i64 0}
!30 = !{!14, !3, i64 60}
!31 = !{!14, !7, i64 58}
!32 = !{!14, !7, i64 56}
!33 = !{!14, !7, i64 64}
!34 = !{!16, !16, i64 0}
!35 = !{!36, !3, i64 0}
!36 = !{!"_ZTS17btBroadphaseProxy", !3, i64 0, !7, i64 4, !7, i64 6, !3, i64 8, !16, i64 12, !15, i64 16, !15, i64 32}
!37 = !{!38, !7, i64 0}
!38 = !{!"_ZTSN20btAxisSweep3InternalItE4EdgeE", !7, i64 0, !7, i64 2}
!39 = !{!38, !7, i64 2}
!40 = !{!41, !16, i64 4}
!41 = !{!"_ZTS20btAxisSweep3InternalIjE", !16, i64 4, !16, i64 8, !15, i64 12, !15, i64 28, !15, i64 44, !16, i64 60, !16, i64 64, !3, i64 68, !16, i64 72, !4, i64 76, !4, i64 88, !3, i64 100, !3, i64 104, !9, i64 108, !16, i64 112, !3, i64 116, !3, i64 120}
!42 = !{!41, !16, i64 8}
!43 = !{!41, !3, i64 100}
!44 = !{!41, !3, i64 104}
!45 = !{!41, !9, i64 108}
!46 = !{!41, !16, i64 112}
!47 = !{!41, !3, i64 116}
!48 = !{!41, !3, i64 120}
!49 = !{!41, !3, i64 68}
!50 = !{!41, !16, i64 64}
!51 = !{!41, !16, i64 60}
!52 = !{!41, !16, i64 72}
!53 = !{!54, !16, i64 0}
!54 = !{!"_ZTSN20btAxisSweep3InternalIjE4EdgeE", !16, i64 0, !16, i64 4}
!55 = !{!54, !16, i64 4}
!56 = !{!57, !3, i64 60}
!57 = !{!"_ZTSN20btAxisSweep3InternalItE6HandleE", !4, i64 48, !4, i64 54, !3, i64 60}
!58 = !{!36, !16, i64 12}
!59 = !{!60, !3, i64 0}
!60 = !{!"_ZTS16btBroadphasePair", !3, i64 0, !3, i64 4, !3, i64 8, !4, i64 12}
!61 = !{!60, !3, i64 4}
!62 = !{!60, !3, i64 8}
!63 = !{i64 0, i64 4, !2, i64 4, i64 4, !2, i64 8, i64 4, !2, i64 12, i64 4, !2, i64 12, i64 4, !34}
!64 = !{!65, !3, i64 72}
!65 = !{!"_ZTSN20btAxisSweep3InternalIjE6HandleE", !4, i64 48, !4, i64 60, !3, i64 72}
!66 = !{!67, !67, i64 0}
!67 = !{!"long", !4, i64 0}
!68 = !{!69, !9, i64 16}
!69 = !{!"_ZTS20btAlignedObjectArrayI16btBroadphasePairE", !70, i64 0, !16, i64 4, !16, i64 8, !3, i64 12, !9, i64 16}
!70 = !{!"_ZTS18btAlignedAllocatorI16btBroadphasePairLj16EE"}
!71 = !{!69, !3, i64 12}
!72 = !{!69, !16, i64 4}
!73 = !{!69, !16, i64 8}
!74 = !{!36, !3, i64 8}
!75 = !{!36, !7, i64 4}
!76 = !{!36, !7, i64 6}
!77 = !{i64 0, i64 2, !6, i64 2, i64 2, !6}
!78 = !{i64 0, i64 4, !34, i64 4, i64 4, !34}
