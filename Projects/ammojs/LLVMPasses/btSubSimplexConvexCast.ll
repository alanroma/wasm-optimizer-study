; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btSubSimplexConvexCast.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/NarrowPhaseCollision/btSubSimplexConvexCast.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btSubsimplexConvexCast = type { %class.btConvexCast, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape* }
%class.btConvexCast = type { i32 (...)** }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%"struct.btConvexCast::CastResult" = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, float, %class.btIDebugDraw*, float }
%class.btIDebugDraw = type { i32 (...)** }

$_ZN12btConvexCastC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN22btSubsimplexConvexCastD0Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

@_ZTV22btSubsimplexConvexCast = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btSubsimplexConvexCast to i8*), i8* bitcast (%class.btConvexCast* (%class.btConvexCast*)* @_ZN12btConvexCastD2Ev to i8*), i8* bitcast (void (%class.btSubsimplexConvexCast*)* @_ZN22btSubsimplexConvexCastD0Ev to i8*), i8* bitcast (i1 (%class.btSubsimplexConvexCast*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %"struct.btConvexCast::CastResult"*)* @_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btSubsimplexConvexCast = hidden constant [25 x i8] c"22btSubsimplexConvexCast\00", align 1
@_ZTI12btConvexCast = external constant i8*
@_ZTI22btSubsimplexConvexCast = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btSubsimplexConvexCast, i32 0, i32 0), i8* bitcast (i8** @_ZTI12btConvexCast to i8*) }, align 4
@_ZTV12btConvexCast = external unnamed_addr constant { [5 x i8*] }, align 4

@_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver = hidden unnamed_addr alias %class.btSubsimplexConvexCast* (%class.btSubsimplexConvexCast*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*), %class.btSubsimplexConvexCast* (%class.btSubsimplexConvexCast*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*)* @_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver

; Function Attrs: nounwind
define hidden %class.btSubsimplexConvexCast* @_ZN22btSubsimplexConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btSubsimplexConvexCast* returned %this, %class.btConvexShape* %convexA, %class.btConvexShape* %convexB, %class.btVoronoiSimplexSolver* %simplexSolver) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btSubsimplexConvexCast*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %convexB.addr = alloca %class.btConvexShape*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btSubsimplexConvexCast* %this, %class.btSubsimplexConvexCast** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %convexB, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  %this1 = load %class.btSubsimplexConvexCast*, %class.btSubsimplexConvexCast** %this.addr, align 4
  %0 = bitcast %class.btSubsimplexConvexCast* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* %0) #9
  %1 = bitcast %class.btSubsimplexConvexCast* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV22btSubsimplexConvexCast, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4, !tbaa !6
  %m_simplexSolver = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %2 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4, !tbaa !2
  store %class.btVoronoiSimplexSolver* %2, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !8
  %m_convexA = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 2
  %3 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4, !tbaa !2
  store %class.btConvexShape* %3, %class.btConvexShape** %m_convexA, align 4, !tbaa !10
  %m_convexB = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 3
  %4 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4, !tbaa !2
  store %class.btConvexShape* %4, %class.btConvexShape** %m_convexB, align 4, !tbaa !11
  ret %class.btSubsimplexConvexCast* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexCast*, align 4
  store %class.btConvexCast* %this, %class.btConvexCast** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btConvexCast*, %class.btConvexCast** %this.addr, align 4
  %0 = bitcast %class.btConvexCast* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV12btConvexCast, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4, !tbaa !6
  ret %class.btConvexCast* %this1
}

define hidden zeroext i1 @_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btSubsimplexConvexCast* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %fromA, %class.btTransform* nonnull align 4 dereferenceable(64) %toA, %class.btTransform* nonnull align 4 dereferenceable(64) %fromB, %class.btTransform* nonnull align 4 dereferenceable(64) %toB, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %result) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btSubsimplexConvexCast*, align 4
  %fromA.addr = alloca %class.btTransform*, align 4
  %toA.addr = alloca %class.btTransform*, align 4
  %fromB.addr = alloca %class.btTransform*, align 4
  %toB.addr = alloca %class.btTransform*, align 4
  %result.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %linVelA = alloca %class.btVector3, align 4
  %linVelB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %lambda = alloca float, align 4
  %interpolatedTransA = alloca %class.btTransform, align 4
  %interpolatedTransB = alloca %class.btTransform, align 4
  %r = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %supVertexA = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %supVertexB = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %maxIter = alloca i32, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %hasResult = alloca i8, align 1
  %c = alloca %class.btVector3, align 4
  %lastLambda = alloca float, align 4
  %dist2 = alloca float, align 4
  %epsilon = alloca float, align 4
  %w = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %VdotR = alloca float, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %VdotW = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp58 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca %class.btVector3, align 4
  %ref.tmp77 = alloca %class.btVector3, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %hitA = alloca %class.btVector3, align 4
  %hitB = alloca %class.btVector3, align 4
  store %class.btSubsimplexConvexCast* %this, %class.btSubsimplexConvexCast** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %fromA, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  store %class.btTransform* %toA, %class.btTransform** %toA.addr, align 4, !tbaa !2
  store %class.btTransform* %fromB, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  store %class.btTransform* %toB, %class.btTransform** %toB.addr, align 4, !tbaa !2
  store %"struct.btConvexCast::CastResult"* %result, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %this1 = load %class.btSubsimplexConvexCast*, %class.btSubsimplexConvexCast** %this.addr, align 4
  %m_simplexSolver = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4, !tbaa !8
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %0)
  %1 = bitcast %class.btVector3* %linVelA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %1) #9
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelA)
  %2 = bitcast %class.btVector3* %linVelB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %2) #9
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelB)
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %3) #9
  %4 = load %class.btTransform*, %class.btTransform** %toA.addr, align 4, !tbaa !2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %4)
  %5 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %6 = bitcast %class.btVector3* %linVelA to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false), !tbaa.struct !12
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %8) #9
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %9) #9
  %10 = load %class.btTransform*, %class.btTransform** %toB.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %10)
  %11 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %11)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %call7)
  %12 = bitcast %class.btVector3* %linVelB to i8*
  %13 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false), !tbaa.struct !12
  %14 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %14) #9
  %15 = bitcast float* %lambda to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #9
  store float 0.000000e+00, float* %lambda, align 4, !tbaa !14
  %16 = bitcast %class.btTransform* %interpolatedTransA to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %16) #9
  %17 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  %call8 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %interpolatedTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %17)
  %18 = bitcast %class.btTransform* %interpolatedTransB to i8*
  call void @llvm.lifetime.start.p0i8(i64 64, i8* %18) #9
  %19 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  %call9 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %interpolatedTransB, %class.btTransform* nonnull align 4 dereferenceable(64) %19)
  %20 = bitcast %class.btVector3* %r to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %20) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %r, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB)
  %21 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %21) #9
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v)
  %22 = bitcast %class.btVector3* %supVertexA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %22) #9
  %23 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  %24 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %24) #9
  %m_convexA = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 2
  %25 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA, align 4, !tbaa !10
  %26 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %26) #9
  %27 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %r)
  %28 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %28)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call14)
  %29 = bitcast %class.btConvexShape* %25 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %29, align 4, !tbaa !6
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %30 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %30(%class.btVector3* sret align 4 %ref.tmp11, %class.btConvexShape* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %supVertexA, %class.btTransform* %23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %31 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %31) #9
  %32 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %32) #9
  %33 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %33) #9
  %34 = bitcast %class.btVector3* %supVertexB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %34) #9
  %35 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  %36 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %36) #9
  %m_convexB = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 3
  %37 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB, align 4, !tbaa !11
  %38 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %38) #9
  %39 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  %call17 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %39)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %r, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call17)
  %40 = bitcast %class.btConvexShape* %37 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable18 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %40, align 4, !tbaa !6
  %vfn19 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable18, i64 16
  %41 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn19, align 4
  call void %41(%class.btVector3* sret align 4 %ref.tmp15, %class.btConvexShape* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %supVertexB, %class.btTransform* %35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %42 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %42) #9
  %43 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #9
  %44 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %44) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexA, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexB)
  %45 = bitcast %class.btVector3* %v to i8*
  %46 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false), !tbaa.struct !12
  %47 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %47) #9
  %48 = bitcast i32* %maxIter to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #9
  store i32 32, i32* %maxIter, align 4, !tbaa !16
  %49 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %49) #9
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  %50 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #9
  store float 0.000000e+00, float* %ref.tmp22, align 4, !tbaa !14
  %51 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #9
  store float 0.000000e+00, float* %ref.tmp23, align 4, !tbaa !14
  %52 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #9
  store float 0.000000e+00, float* %ref.tmp24, align 4, !tbaa !14
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %n, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %53 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %53) #9
  %54 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #9
  %55 = bitcast float* %ref.tmp22 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %55) #9
  call void @llvm.lifetime.start.p0i8(i64 1, i8* %hasResult) #9
  store i8 0, i8* %hasResult, align 1, !tbaa !18
  %56 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %56) #9
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %c)
  %57 = bitcast float* %lastLambda to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #9
  %58 = load float, float* %lambda, align 4, !tbaa !14
  store float %58, float* %lastLambda, align 4, !tbaa !14
  %59 = bitcast float* %dist2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #9
  %call26 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %v)
  store float %call26, float* %dist2, align 4, !tbaa !14
  %60 = bitcast float* %epsilon to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #9
  store float 0x3F1A36E2E0000000, float* %epsilon, align 4, !tbaa !14
  %61 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %61) #9
  %call27 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %w)
  %62 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %62) #9
  %call28 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %63 = bitcast float* %VdotR to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #9
  br label %while.cond

while.cond:                                       ; preds = %cleanup.cont, %entry
  %64 = load float, float* %dist2, align 4, !tbaa !14
  %65 = load float, float* %epsilon, align 4, !tbaa !14
  %cmp = fcmp ogt float %64, %65
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %66 = load i32, i32* %maxIter, align 4, !tbaa !16
  %dec = add nsw i32 %66, -1
  store i32 %dec, i32* %maxIter, align 4, !tbaa !16
  %tobool = icmp ne i32 %66, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %67 = phi i1 [ false, %while.cond ], [ %tobool, %land.rhs ]
  br i1 %67, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %68 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %68) #9
  %69 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %69) #9
  %m_convexA31 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 2
  %70 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA31, align 4, !tbaa !10
  %71 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %71) #9
  %72 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %72) #9
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %interpolatedTransA)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34)
  %73 = bitcast %class.btConvexShape* %70 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable35 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %73, align 4, !tbaa !6
  %vfn36 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable35, i64 16
  %74 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn36, align 4
  call void %74(%class.btVector3* sret align 4 %ref.tmp30, %class.btConvexShape* %70, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp29, %class.btTransform* %interpolatedTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  %75 = bitcast %class.btVector3* %supVertexA to i8*
  %76 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false), !tbaa.struct !12
  %77 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %77) #9
  %78 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %78) #9
  %79 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %79) #9
  %80 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %80) #9
  %81 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %81) #9
  %82 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %82) #9
  %m_convexB39 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 3
  %83 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB39, align 4, !tbaa !11
  %84 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %84) #9
  %call41 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %interpolatedTransB)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp40, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call41)
  %85 = bitcast %class.btConvexShape* %83 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable42 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %85, align 4, !tbaa !6
  %vfn43 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable42, i64 16
  %86 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn43, align 4
  call void %86(%class.btVector3* sret align 4 %ref.tmp38, %class.btConvexShape* %83, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp40)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp37, %class.btTransform* %interpolatedTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  %87 = bitcast %class.btVector3* %supVertexB to i8*
  %88 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %87, i8* align 4 %88, i32 16, i1 false), !tbaa.struct !12
  %89 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %89) #9
  %90 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %90) #9
  %91 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %91) #9
  %92 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %92) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexA, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexB)
  %93 = bitcast %class.btVector3* %w to i8*
  %94 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 4 %94, i32 16, i1 false), !tbaa.struct !12
  %95 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %95) #9
  %96 = bitcast float* %VdotW to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #9
  %call45 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  store float %call45, float* %VdotW, align 4, !tbaa !14
  %97 = load float, float* %lambda, align 4, !tbaa !14
  %cmp46 = fcmp ogt float %97, 1.000000e+00
  br i1 %cmp46, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  %98 = load float, float* %VdotW, align 4, !tbaa !14
  %cmp47 = fcmp ogt float %98, 0.000000e+00
  br i1 %cmp47, label %if.then48, label %if.end60

if.then48:                                        ; preds = %if.end
  %call49 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %r)
  store float %call49, float* %VdotR, align 4, !tbaa !14
  %99 = load float, float* %VdotR, align 4, !tbaa !14
  %cmp50 = fcmp oge float %99, 0xBD10000000000000
  br i1 %cmp50, label %if.then51, label %if.else

if.then51:                                        ; preds = %if.then48
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else:                                          ; preds = %if.then48
  %100 = load float, float* %lambda, align 4, !tbaa !14
  %101 = load float, float* %VdotW, align 4, !tbaa !14
  %102 = load float, float* %VdotR, align 4, !tbaa !14
  %div = fdiv float %101, %102
  %sub = fsub float %100, %div
  store float %sub, float* %lambda, align 4, !tbaa !14
  %call52 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %interpolatedTransA)
  %103 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4, !tbaa !2
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %103)
  %104 = load %class.btTransform*, %class.btTransform** %toA.addr, align 4, !tbaa !2
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %104)
  %105 = load float, float* %lambda, align 4, !tbaa !14
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %call52, %class.btVector3* nonnull align 4 dereferenceable(16) %call53, %class.btVector3* nonnull align 4 dereferenceable(16) %call54, float %105)
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %interpolatedTransB)
  %106 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4, !tbaa !2
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %106)
  %107 = load %class.btTransform*, %class.btTransform** %toB.addr, align 4, !tbaa !2
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %107)
  %108 = load float, float* %lambda, align 4, !tbaa !14
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %call55, %class.btVector3* nonnull align 4 dereferenceable(16) %call56, %class.btVector3* nonnull align 4 dereferenceable(16) %call57, float %108)
  %109 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %109) #9
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp58, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexA, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexB)
  %110 = bitcast %class.btVector3* %w to i8*
  %111 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %110, i8* align 4 %111, i32 16, i1 false), !tbaa.struct !12
  %112 = bitcast %class.btVector3* %ref.tmp58 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %112) #9
  %113 = load float, float* %lambda, align 4, !tbaa !14
  store float %113, float* %lastLambda, align 4, !tbaa !14
  %114 = bitcast %class.btVector3* %n to i8*
  %115 = bitcast %class.btVector3* %v to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false), !tbaa.struct !12
  store i8 1, i8* %hasResult, align 1, !tbaa !18
  br label %if.end59

if.end59:                                         ; preds = %if.else
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %if.end
  %m_simplexSolver61 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %116 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver61, align 4, !tbaa !8
  %call62 = call zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver* %116, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  br i1 %call62, label %if.end65, label %if.then63

if.then63:                                        ; preds = %if.end60
  %m_simplexSolver64 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %117 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver64, align 4, !tbaa !8
  call void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver* %117, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexA, %class.btVector3* nonnull align 4 dereferenceable(16) %supVertexB)
  br label %if.end65

if.end65:                                         ; preds = %if.then63, %if.end60
  %m_simplexSolver66 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %118 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver66, align 4, !tbaa !8
  %call67 = call zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver* %118, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  br i1 %call67, label %if.then68, label %if.else70

if.then68:                                        ; preds = %if.end65
  %call69 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %v)
  store float %call69, float* %dist2, align 4, !tbaa !14
  store i8 1, i8* %hasResult, align 1, !tbaa !18
  br label %if.end71

if.else70:                                        ; preds = %if.end65
  store float 0.000000e+00, float* %dist2, align 4, !tbaa !14
  br label %if.end71

if.end71:                                         ; preds = %if.else70, %if.then68
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end71, %if.then51, %if.then
  %119 = bitcast float* %VdotW to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %119) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup94 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %while.cond

while.end:                                        ; preds = %land.end
  %120 = load float, float* %lambda, align 4, !tbaa !14
  %121 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %121, i32 0, i32 5
  store float %120, float* %m_fraction, align 4, !tbaa !20
  %call72 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %n)
  %cmp73 = fcmp oge float %call72, 0x3D10000000000000
  br i1 %cmp73, label %if.then74, label %if.else76

if.then74:                                        ; preds = %while.end
  %122 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %122) #9
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp75, %class.btVector3* %n)
  %123 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_normal = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %123, i32 0, i32 3
  %124 = bitcast %class.btVector3* %m_normal to i8*
  %125 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %124, i8* align 4 %125, i32 16, i1 false), !tbaa.struct !12
  %126 = bitcast %class.btVector3* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %126) #9
  br label %if.end83

if.else76:                                        ; preds = %while.end
  %127 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %127) #9
  %128 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #9
  store float 0.000000e+00, float* %ref.tmp78, align 4, !tbaa !14
  %129 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %129) #9
  store float 0.000000e+00, float* %ref.tmp79, align 4, !tbaa !14
  %130 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %130) #9
  store float 0.000000e+00, float* %ref.tmp80, align 4, !tbaa !14
  %call81 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp77, float* nonnull align 4 dereferenceable(4) %ref.tmp78, float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80)
  %131 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_normal82 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %131, i32 0, i32 3
  %132 = bitcast %class.btVector3* %m_normal82 to i8*
  %133 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %132, i8* align 4 %133, i32 16, i1 false), !tbaa.struct !12
  %134 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #9
  %135 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #9
  %136 = bitcast float* %ref.tmp78 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #9
  %137 = bitcast %class.btVector3* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %137) #9
  br label %if.end83

if.end83:                                         ; preds = %if.else76, %if.then74
  %138 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_normal84 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %138, i32 0, i32 3
  %call85 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_normal84, %class.btVector3* nonnull align 4 dereferenceable(16) %r)
  %139 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_allowedPenetration = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %139, i32 0, i32 7
  %140 = load float, float* %m_allowedPenetration, align 4, !tbaa !25
  %fneg = fneg float %140
  %cmp86 = fcmp oge float %call85, %fneg
  br i1 %cmp86, label %if.then87, label %if.end88

if.then87:                                        ; preds = %if.end83
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup94

if.end88:                                         ; preds = %if.end83
  %141 = bitcast %class.btVector3* %hitA to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %141) #9
  %call89 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hitA)
  %142 = bitcast %class.btVector3* %hitB to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %142) #9
  %call90 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %hitB)
  %m_simplexSolver91 = getelementptr inbounds %class.btSubsimplexConvexCast, %class.btSubsimplexConvexCast* %this1, i32 0, i32 1
  %143 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver91, align 4, !tbaa !8
  call void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver* %143, %class.btVector3* nonnull align 4 dereferenceable(16) %hitA, %class.btVector3* nonnull align 4 dereferenceable(16) %hitB)
  %144 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4, !tbaa !2
  %m_hitPoint = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %144, i32 0, i32 4
  %145 = bitcast %class.btVector3* %m_hitPoint to i8*
  %146 = bitcast %class.btVector3* %hitB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %145, i8* align 4 %146, i32 16, i1 false), !tbaa.struct !12
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  %147 = bitcast %class.btVector3* %hitB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %147) #9
  %148 = bitcast %class.btVector3* %hitA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %148) #9
  br label %cleanup94

cleanup94:                                        ; preds = %if.end88, %if.then87, %cleanup
  %149 = bitcast float* %VdotR to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #9
  %150 = bitcast %class.btVector3* %p to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %150) #9
  %151 = bitcast %class.btVector3* %w to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %151) #9
  %152 = bitcast float* %epsilon to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #9
  %153 = bitcast float* %dist2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #9
  %154 = bitcast float* %lastLambda to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #9
  %155 = bitcast %class.btVector3* %c to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %155) #9
  call void @llvm.lifetime.end.p0i8(i64 1, i8* %hasResult) #9
  %156 = bitcast %class.btVector3* %n to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %156) #9
  %157 = bitcast i32* %maxIter to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #9
  %158 = bitcast %class.btVector3* %supVertexB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %158) #9
  %159 = bitcast %class.btVector3* %supVertexA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %159) #9
  %160 = bitcast %class.btVector3* %v to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %160) #9
  %161 = bitcast %class.btVector3* %r to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %161) #9
  %162 = bitcast %class.btTransform* %interpolatedTransB to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %162) #9
  %163 = bitcast %class.btTransform* %interpolatedTransA to i8*
  call void @llvm.lifetime.end.p0i8(i64 64, i8* %163) #9
  %164 = bitcast float* %lambda to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #9
  %165 = bitcast %class.btVector3* %linVelB to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %165) #9
  %166 = bitcast %class.btVector3* %linVelA to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %166) #9
  %167 = load i1, i1* %retval, align 1
  ret i1 %167
}

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %sub8 = fsub float %7, %9
  store float %sub8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %sub14 = fsub float %12, %14
  store float %sub14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #4

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4, !tbaa !2
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false), !tbaa.struct !12
  ret %class.btTransform* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #5 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4, !tbaa !2
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %2) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %ref.tmp1, align 4, !tbaa !14
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4, !tbaa !2
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  store float %call4, float* %ref.tmp3, align 4, !tbaa !14
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %9 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %fneg = fneg float %2
  store float %fneg, float* %ref.tmp, align 4, !tbaa !14
  %3 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #9
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !14
  %fneg4 = fneg float %5
  store float %fneg4, float* %ref.tmp1, align 4, !tbaa !14
  %6 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #9
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %8 = load float, float* %arrayidx7, align 4, !tbaa !14
  %fneg8 = fneg float %8
  store float %fneg8, float* %ref.tmp5, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %9) #9
  %10 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #9
  %11 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %11) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4, !tbaa !14
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4, !tbaa !14
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store float %rt, float* %rt.addr, align 4, !tbaa !14
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float, float* %rt.addr, align 4, !tbaa !14
  %sub = fsub float 1.000000e+00, %1
  store float %sub, float* %s, align 4, !tbaa !14
  %2 = load float, float* %s, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %4 = load float, float* %arrayidx, align 4, !tbaa !14
  %mul = fmul float %2, %4
  %5 = load float, float* %rt.addr, align 4, !tbaa !14
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul4 = fmul float %5, %7
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4, !tbaa !14
  %8 = load float, float* %s, align 4, !tbaa !14
  %9 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %10 = load float, float* %arrayidx8, align 4, !tbaa !14
  %mul9 = fmul float %8, %10
  %11 = load float, float* %rt.addr, align 4, !tbaa !14
  %12 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %13 = load float, float* %arrayidx11, align 4, !tbaa !14
  %mul12 = fmul float %11, %13
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4, !tbaa !14
  %14 = load float, float* %s, align 4, !tbaa !14
  %15 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %16 = load float, float* %arrayidx17, align 4, !tbaa !14
  %mul18 = fmul float %14, %16
  %17 = load float, float* %rt.addr, align 4, !tbaa !14
  %18 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %19 = load float, float* %arrayidx20, align 4, !tbaa !14
  %mul21 = fmul float %17, %19
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4, !tbaa !14
  %20 = bitcast float* %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #9
  ret void
}

declare zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %norm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %0) #9
  %1 = bitcast %class.btVector3* %norm to i8*
  %2 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %norm)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false), !tbaa.struct !12
  %5 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %5) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !14
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !14
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !14
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !14
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

declare void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: nounwind
declare %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned) unnamed_addr #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN22btSubsimplexConvexCastD0Ev(%class.btSubsimplexConvexCast* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSubsimplexConvexCast*, align 4
  store %class.btSubsimplexConvexCast* %this, %class.btSubsimplexConvexCast** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btSubsimplexConvexCast*, %class.btSubsimplexConvexCast** %this.addr, align 4
  %call = call %class.btSubsimplexConvexCast* bitcast (%class.btConvexCast* (%class.btConvexCast*)* @_ZN12btConvexCastD2Ev to %class.btSubsimplexConvexCast* (%class.btSubsimplexConvexCast*)*)(%class.btSubsimplexConvexCast* %this1) #9
  %0 = bitcast %class.btSubsimplexConvexCast* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #5 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false), !tbaa.struct !12
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false), !tbaa.struct !12
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4, !tbaa !2
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false), !tbaa.struct !12
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %3 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %4 = load float, float* %arrayidx2, align 4, !tbaa !14
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4, !tbaa !14
  %5 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #9
  %6 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %7 = load float, float* %arrayidx5, align 4, !tbaa !14
  %8 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %9 = load float, float* %arrayidx7, align 4, !tbaa !14
  %add8 = fadd float %7, %9
  store float %add8, float* %ref.tmp3, align 4, !tbaa !14
  %10 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #9
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %12 = load float, float* %arrayidx11, align 4, !tbaa !14
  %13 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %14 = load float, float* %arrayidx13, align 4, !tbaa !14
  %add14 = fadd float %12, %14
  store float %add14, float* %ref.tmp9, align 4, !tbaa !14
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %15 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #9
  %16 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %16) #9
  %17 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #9
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4, !tbaa !2
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4, !tbaa !2
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4, !tbaa !2
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %2 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #9
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4, !tbaa !2
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call3, float* %ref.tmp2, align 4, !tbaa !14
  %4 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #9
  %5 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4, !tbaa !2
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call5, float* %ref.tmp4, align 4, !tbaa !14
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %6 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %6) #9
  %7 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #9
  %8 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %8) #9
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store i32 %i, i32* %i.addr, align 4, !tbaa !16
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4, !tbaa !16
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #5 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !14
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !14
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !14
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !14
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4, !tbaa !2
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4, !tbaa !14
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4, !tbaa !14
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4, !tbaa !14
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4, !tbaa !14
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4, !tbaa !14
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4, !tbaa !2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4, !tbaa !14
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4, !tbaa !14
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %1) #9
  ret %class.btVector3* %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #5 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #9
  %1 = load float*, float** %s.addr, align 4, !tbaa !2
  %2 = load float, float* %1, align 4, !tbaa !14
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4, !tbaa !14
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %3) #9
  ret %class.btVector3* %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4, !tbaa !14
  %0 = load float, float* %y.addr, align 4, !tbaa !14
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %s, float** %s.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !14
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4, !tbaa !14
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4, !tbaa !14
  %3 = load float*, float** %s.addr, align 4, !tbaa !2
  %4 = load float, float* %3, align 4, !tbaa !14
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4, !tbaa !14
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4, !tbaa !14
  %6 = load float*, float** %s.addr, align 4, !tbaa !2
  %7 = load float, float* %6, align 4, !tbaa !14
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4, !tbaa !14
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4, !tbaa !14
  ret %class.btVector3* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

attributes #0 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"vtable pointer", !5, i64 0}
!8 = !{!9, !3, i64 4}
!9 = !{!"_ZTS22btSubsimplexConvexCast", !3, i64 4, !3, i64 8, !3, i64 12}
!10 = !{!9, !3, i64 8}
!11 = !{!9, !3, i64 12}
!12 = !{i64 0, i64 16, !13}
!13 = !{!4, !4, i64 0}
!14 = !{!15, !15, i64 0}
!15 = !{!"float", !4, i64 0}
!16 = !{!17, !17, i64 0}
!17 = !{!"int", !4, i64 0}
!18 = !{!19, !19, i64 0}
!19 = !{!"bool", !4, i64 0}
!20 = !{!21, !15, i64 164}
!21 = !{!"_ZTSN12btConvexCast10CastResultE", !22, i64 4, !22, i64 68, !24, i64 132, !24, i64 148, !15, i64 164, !3, i64 168, !15, i64 172}
!22 = !{!"_ZTS11btTransform", !23, i64 0, !24, i64 48}
!23 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!24 = !{!"_ZTS9btVector3", !4, i64 0}
!25 = !{!21, !15, i64 172}
