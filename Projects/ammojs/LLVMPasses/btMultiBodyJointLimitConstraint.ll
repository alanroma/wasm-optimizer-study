; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btMultiBodyJointLimitConstraint = type { %class.btMultiBodyConstraint, float, float }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, float, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btMultiBody = type <{ %class.btMultiBodyLinkCollider*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i32, float, float, i8, [3 x i8], float, i8, [3 x i8] }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, float, float, float, float, i32, %union.anon, float, float, float, i32, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%union.anon = type { i8* }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btMultibodyLink = type { float, float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i8, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, float, %class.btMultiBodyLinkCollider*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, %class.btVector3, %class.btVector3, i32, i32, %class.btVector3, %class.btVector3, i32, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.19, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32 }
%union.anon.19 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.21*, i32 }
%class.btAlignedObjectArray.21 = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float }

$_ZN21btMultiBodyConstraint9jacobianAEi = comdat any

$_ZN21btMultiBodyConstraint9jacobianBEi = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN21btMultiBodyConstraint11setPositionEif = comdat any

$_ZNK21btMultiBodyConstraint10getNumRowsEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZNK21btMultiBodyConstraint11getPositionEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

@_ZTV31btMultiBodyJointLimitConstraint = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI31btMultiBodyJointLimitConstraint to i8*), i8* bitcast (%class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD0Ev to i8*), i8* bitcast (i32 (%class.btMultiBodyJointLimitConstraint*)* @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdAEv to i8*), i8* bitcast (i32 (%class.btMultiBodyJointLimitConstraint*)* @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdBEv to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*, %class.btAlignedObjectArray.16*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)* @_ZN31btMultiBodyJointLimitConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS31btMultiBodyJointLimitConstraint = hidden constant [34 x i8] c"31btMultiBodyJointLimitConstraint\00", align 1
@_ZTI21btMultiBodyConstraint = external constant i8*
@_ZTI31btMultiBodyJointLimitConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_ZTS31btMultiBodyJointLimitConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btMultiBodyConstraint to i8*) }, align 4

@_ZN31btMultiBodyJointLimitConstraintC1EP11btMultiBodyiff = hidden unnamed_addr alias %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*, %class.btMultiBody*, i32, float, float), %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*, %class.btMultiBody*, i32, float, float)* @_ZN31btMultiBodyJointLimitConstraintC2EP11btMultiBodyiff
@_ZN31btMultiBodyJointLimitConstraintD1Ev = hidden unnamed_addr alias %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*), %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD2Ev

define hidden %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintC2EP11btMultiBodyiff(%class.btMultiBodyJointLimitConstraint* returned %this, %class.btMultiBody* %body, i32 %link, float %lower, float %upper) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %lower.addr = alloca float, align 4
  %upper.addr = alloca float, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  store i32 %link, i32* %link.addr, align 4, !tbaa !6
  store float %lower, float* %lower.addr, align 4, !tbaa !8
  store float %upper, float* %upper.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4, !tbaa !2
  %3 = load i32, i32* %link.addr, align 4, !tbaa !6
  %4 = load i32, i32* %link.addr, align 4, !tbaa !6
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* %2, i32 %3, i32 %4, i32 2, i1 zeroext true)
  %5 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV31btMultiBodyJointLimitConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4, !tbaa !10
  %m_lowerBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 1
  %6 = load float, float* %lower.addr, align 4, !tbaa !8
  store float %6, float* %m_lowerBound, align 4, !tbaa !12
  %m_upperBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 2
  %7 = load float, float* %upper.addr, align 4, !tbaa !8
  store float %7, float* %m_upperBound, align 4, !tbaa !14
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call2 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %8, i32 0)
  %9 = load i32, i32* %link.addr, align 4, !tbaa !6
  %add = add nsw i32 6, %9
  %arrayidx = getelementptr inbounds float, float* %call2, i32 %add
  store float 1.000000e+00, float* %arrayidx, align 4, !tbaa !8
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call3 = call float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %10, i32 1)
  %11 = load i32, i32* %link.addr, align 4, !tbaa !6
  %add4 = add nsw i32 6, %11
  %arrayidx5 = getelementptr inbounds float, float* %call3, i32 %add4
  store float -1.000000e+00, float* %arrayidx5, align 4, !tbaa !8
  ret %class.btMultiBodyJointLimitConstraint* %this1
}

declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i1 zeroext) unnamed_addr #1

define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %this, i32 %row) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !15
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %m_jac_size_both = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jac_size_both, align 4, !tbaa !20
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add)
  ret float* %call
}

; Function Attrs: nounwind
define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %this, i32 %row) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !15
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %m_jac_size_both = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jac_size_both, align 4, !tbaa !20
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %m_jac_size_A = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_jac_size_A, align 4, !tbaa !21
  %add2 = add nsw i32 %add, %3
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add2)
  ret float* %call
}

; Function Attrs: nounwind
declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned) unnamed_addr #3

; Function Attrs: nounwind
define hidden %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintD2Ev(%class.btMultiBodyJointLimitConstraint* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* %0) #8
  ret %class.btMultiBodyJointLimitConstraint* %this1
}

; Function Attrs: nounwind
define hidden void @_ZN31btMultiBodyJointLimitConstraintD0Ev(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %call = call %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintD1Ev(%class.btMultiBodyJointLimitConstraint* %this1) #8
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

define hidden i32 @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdAEv(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 1
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !22
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %2)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %5 = bitcast %class.btMultiBodyLinkCollider* %4 to %class.btCollisionObject*
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

if.end:                                           ; preds = %entry
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 1
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA3, align 4, !tbaa !22
  %call4 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA5, align 4, !tbaa !22
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call6, i32 0, i32 15
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !23
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool7, label %if.then8, label %if.end13

if.then8:                                         ; preds = %for.body
  %14 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA9 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %14, i32 0, i32 1
  %15 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA9, align 4, !tbaa !22
  %16 = load i32, i32* %i, align 4, !tbaa !6
  %call10 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %15, i32 %16)
  %m_collider11 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call10, i32 0, i32 15
  %17 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider11, align 4, !tbaa !23
  %18 = bitcast %class.btMultiBodyLinkCollider* %17 to %class.btCollisionObject*
  %call12 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %18)
  store i32 %call12, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %19 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then8, %for.cond.cleanup
  %20 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %20) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup14 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup14

cleanup14:                                        ; preds = %for.end, %cleanup, %if.then
  %21 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %21) #8
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #5

; Function Attrs: nounwind
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 0
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4, !tbaa !27
  ret %class.btMultiBodyLinkCollider* %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4, !tbaa !38
  ret i32 %0
}

define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %links)
  ret i32 %call
}

define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4, !tbaa !2
  store i32 %index, i32* %index.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 7
  %0 = load i32, i32* %index.addr, align 4, !tbaa !6
  %call = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #5

define hidden i32 @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdBEv(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #0 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 2
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !41
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %2)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %3 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool = icmp ne %class.btMultiBodyLinkCollider* %3, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %5 = bitcast %class.btMultiBodyLinkCollider* %4 to %class.btCollisionObject*
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  store i32 %call2, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

if.end:                                           ; preds = %entry
  %6 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #8
  store i32 0, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4, !tbaa !6
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 2
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB3, align 4, !tbaa !41
  %call4 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call4
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  store i32 2, i32* %cleanup.dest.slot, align 4
  br label %cleanup

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 2
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB5, align 4, !tbaa !41
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %call6 = call nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call6, i32 0, i32 15
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4, !tbaa !23
  store %class.btMultiBodyLinkCollider* %13, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %14, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %for.body
  %15 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4, !tbaa !2
  %16 = bitcast %class.btMultiBodyLinkCollider* %15 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %16)
  store i32 %call9, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end10:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %17 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

cleanup:                                          ; preds = %if.then8, %for.cond.cleanup
  %18 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %18) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup11 [
    i32 2, label %for.end
  ]

for.end:                                          ; preds = %cleanup
  store i32 -1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup11

cleanup11:                                        ; preds = %for.end, %cleanup, %if.then
  %19 = bitcast %class.btMultiBodyLinkCollider** %col to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %19) #8
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

define hidden void @_ZN31btMultiBodyJointLimitConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo(%class.btMultiBodyJointLimitConstraint* %this, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %constraintRows, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %infoGlobal) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %constraintRows.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %row = alloca i32, align 4
  %constraintRow = alloca %struct.btMultiBodySolverConstraint*, align 4
  %rel_vel = alloca float, align 4
  %penetration = alloca float, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4, !tbaa !2
  store %class.btAlignedObjectArray.16* %constraintRows, %class.btAlignedObjectArray.16** %constraintRows.addr, align 4, !tbaa !2
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %1 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 1
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4, !tbaa !22
  %3 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 3
  %4 = load i32, i32* %m_linkA, align 4, !tbaa !42
  %call = call float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody* %2, i32 %4)
  %m_lowerBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 1
  %5 = load float, float* %m_lowerBound, align 4, !tbaa !12
  %sub = fsub float %call, %5
  call void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %0, i32 0, float %sub)
  %6 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_upperBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 2
  %7 = load float, float* %m_upperBound, align 4, !tbaa !14
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 1
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA2, align 4, !tbaa !22
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 3
  %11 = load i32, i32* %m_linkA3, align 4, !tbaa !42
  %call4 = call float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody* %9, i32 %11)
  %sub5 = fsub float %7, %call4
  call void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %6, i32 1, float %sub5)
  %12 = bitcast i32* %row to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #8
  store i32 0, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %13 = load i32, i32* %row, align 4, !tbaa !6
  %14 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call6 = call i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %14)
  %cmp = icmp slt i32 %13, %call6
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %15 = bitcast i32* %row to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %15) #8
  br label %for.end

for.body:                                         ; preds = %for.cond
  %16 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #8
  %17 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %constraintRows.addr, align 4, !tbaa !2
  %call7 = call nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.16* %17)
  store %struct.btMultiBodySolverConstraint* %call7, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %18 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA8 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %18, i32 0, i32 1
  %19 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA8, align 4, !tbaa !22
  %20 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %20, i32 0, i32 23
  store %class.btMultiBody* %19, %class.btMultiBody** %m_multiBodyA, align 4, !tbaa !43
  %21 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %21, i32 0, i32 2
  %22 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4, !tbaa !41
  %23 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %23, i32 0, i32 26
  store %class.btMultiBody* %22, %class.btMultiBody** %m_multiBodyB, align 4, !tbaa !45
  %24 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #8
  %25 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %26 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %27 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4, !tbaa !2
  %28 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %29 = load i32, i32* %row, align 4, !tbaa !6
  %call9 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %28, i32 %29)
  %30 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %31 = load i32, i32* %row, align 4, !tbaa !6
  %call10 = call float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %30, i32 %31)
  %32 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %33 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %33, i32 0, i32 10
  %34 = load float, float* %m_maxAppliedImpulse, align 4, !tbaa !46
  %fneg = fneg float %34
  %35 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %35, i32 0, i32 10
  %36 = load float, float* %m_maxAppliedImpulse11, align 4, !tbaa !46
  %call12 = call float @_ZN21btMultiBodyConstraint35fillConstraintRowMultiBodyMultiBodyER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK19btContactSolverInfofff(%class.btMultiBodyConstraint* %25, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184) %26, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %27, float* %call9, float* %call10, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84) %32, float 0.000000e+00, float %fneg, float %36)
  store float %call12, float* %rel_vel, align 4, !tbaa !8
  %37 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #8
  %38 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %39 = load i32, i32* %row, align 4, !tbaa !6
  %call13 = call float @_ZNK21btMultiBodyConstraint11getPositionEi(%class.btMultiBodyConstraint* %38, i32 %39)
  store float %call13, float* %penetration, align 4, !tbaa !8
  %40 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #8
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !8
  %41 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #8
  %42 = load float, float* %rel_vel, align 4, !tbaa !8
  %fneg14 = fneg float %42
  store float %fneg14, float* %velocityError, align 4, !tbaa !8
  %43 = bitcast float* %erp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #8
  %44 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %45 = bitcast %struct.btContactSolverInfo* %44 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %45, i32 0, i32 9
  %46 = load float, float* %m_erp2, align 4, !tbaa !47
  store float %46, float* %erp, align 4, !tbaa !8
  %47 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %48 = bitcast %struct.btContactSolverInfo* %47 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %48, i32 0, i32 11
  %49 = load i32, i32* %m_splitImpulse, align 4, !tbaa !49
  %tobool = icmp ne i32 %49, 0
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %for.body
  %50 = load float, float* %penetration, align 4, !tbaa !8
  %51 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %52 = bitcast %struct.btContactSolverInfo* %51 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %52, i32 0, i32 12
  %53 = load float, float* %m_splitImpulsePenetrationThreshold, align 4, !tbaa !50
  %cmp15 = fcmp ogt float %50, %53
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  %54 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %55 = bitcast %struct.btContactSolverInfo* %54 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %55, i32 0, i32 8
  %56 = load float, float* %m_erp, align 4, !tbaa !51
  store float %56, float* %erp, align 4, !tbaa !8
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  %57 = load float, float* %penetration, align 4, !tbaa !8
  %cmp16 = fcmp ogt float %57, 0.000000e+00
  br i1 %cmp16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end
  store float 0.000000e+00, float* %positionalError, align 4, !tbaa !8
  %58 = load float, float* %penetration, align 4, !tbaa !8
  %fneg18 = fneg float %58
  %59 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %60 = bitcast %struct.btContactSolverInfo* %59 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %60, i32 0, i32 3
  %61 = load float, float* %m_timeStep, align 4, !tbaa !52
  %div = fdiv float %fneg18, %61
  store float %div, float* %velocityError, align 4, !tbaa !8
  br label %if.end22

if.else:                                          ; preds = %if.end
  %62 = load float, float* %penetration, align 4, !tbaa !8
  %fneg19 = fneg float %62
  %63 = load float, float* %erp, align 4, !tbaa !8
  %mul = fmul float %fneg19, %63
  %64 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %65 = bitcast %struct.btContactSolverInfo* %64 to %struct.btContactSolverInfoData*
  %m_timeStep20 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %65, i32 0, i32 3
  %66 = load float, float* %m_timeStep20, align 4, !tbaa !52
  %div21 = fdiv float %mul, %66
  store float %div21, float* %positionalError, align 4, !tbaa !8
  br label %if.end22

if.end22:                                         ; preds = %if.else, %if.then17
  %67 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #8
  %68 = load float, float* %positionalError, align 4, !tbaa !8
  %69 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %69, i32 0, i32 13
  %70 = load float, float* %m_jacDiagABInv, align 4, !tbaa !53
  %mul23 = fmul float %68, %70
  store float %mul23, float* %penetrationImpulse, align 4, !tbaa !8
  %71 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #8
  %72 = load float, float* %velocityError, align 4, !tbaa !8
  %73 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_jacDiagABInv24 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %73, i32 0, i32 13
  %74 = load float, float* %m_jacDiagABInv24, align 4, !tbaa !53
  %mul25 = fmul float %72, %74
  store float %mul25, float* %velocityImpulse, align 4, !tbaa !8
  %75 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %76 = bitcast %struct.btContactSolverInfo* %75 to %struct.btContactSolverInfoData*
  %m_splitImpulse26 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %76, i32 0, i32 11
  %77 = load i32, i32* %m_splitImpulse26, align 4, !tbaa !49
  %tobool27 = icmp ne i32 %77, 0
  br i1 %tobool27, label %lor.lhs.false28, label %if.then31

lor.lhs.false28:                                  ; preds = %if.end22
  %78 = load float, float* %penetration, align 4, !tbaa !8
  %79 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4, !tbaa !2
  %80 = bitcast %struct.btContactSolverInfo* %79 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold29 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %80, i32 0, i32 12
  %81 = load float, float* %m_splitImpulsePenetrationThreshold29, align 4, !tbaa !50
  %cmp30 = fcmp ogt float %78, %81
  br i1 %cmp30, label %if.then31, label %if.else32

if.then31:                                        ; preds = %lor.lhs.false28, %if.end22
  %82 = load float, float* %penetrationImpulse, align 4, !tbaa !8
  %83 = load float, float* %velocityImpulse, align 4, !tbaa !8
  %add = fadd float %82, %83
  %84 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %84, i32 0, i32 14
  store float %add, float* %m_rhs, align 4, !tbaa !54
  %85 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %85, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4, !tbaa !55
  br label %if.end35

if.else32:                                        ; preds = %lor.lhs.false28
  %86 = load float, float* %velocityImpulse, align 4, !tbaa !8
  %87 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_rhs33 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %87, i32 0, i32 14
  store float %86, float* %m_rhs33, align 4, !tbaa !54
  %88 = load float, float* %penetrationImpulse, align 4, !tbaa !8
  %89 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4, !tbaa !2
  %m_rhsPenetration34 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %89, i32 0, i32 18
  store float %88, float* %m_rhsPenetration34, align 4, !tbaa !55
  br label %if.end35

if.end35:                                         ; preds = %if.else32, %if.then31
  %90 = bitcast float* %velocityImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %90) #8
  %91 = bitcast float* %penetrationImpulse to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %91) #8
  %92 = bitcast float* %erp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %92) #8
  %93 = bitcast float* %velocityError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %93) #8
  %94 = bitcast float* %positionalError to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %94) #8
  %95 = bitcast float* %penetration to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %95) #8
  %96 = bitcast float* %rel_vel to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %96) #8
  %97 = bitcast %struct.btMultiBodySolverConstraint** %constraintRow to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %97) #8
  br label %for.inc

for.inc:                                          ; preds = %if.end35
  %98 = load i32, i32* %row, align 4, !tbaa !6
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %row, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %this, i32 %row, float %pos) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  %pos.addr = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  store float %pos, float* %pos.addr, align 4, !tbaa !8
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = load float, float* %pos.addr, align 4, !tbaa !8
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_pos_offset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  %1 = load i32, i32* %m_pos_offset, align 4, !tbaa !56
  %2 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add = add nsw i32 %1, %2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add)
  store float %0, float* %call, align 4, !tbaa !8
  ret void
}

declare float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody*, i32) #1

; Function Attrs: nounwind
define linkonce_odr hidden i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_num_rows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_num_rows, align 4, !tbaa !15
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden nonnull align 4 dereferenceable(184) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4, !tbaa !6
  %1 = load i32, i32* %sz, align 4, !tbaa !6
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %1, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4, !tbaa !57
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %m_size, align 4, !tbaa !57
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !60
  %4 = load i32, i32* %sz, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast i32* %sz to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %5) #8
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

declare float @_ZN21btMultiBodyConstraint35fillConstraintRowMultiBodyMultiBodyER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK19btContactSolverInfofff(%class.btMultiBodyConstraint*, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(184), %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128), float*, float*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(84), float, float, float) #1

define linkonce_odr hidden float @_ZNK21btMultiBodyConstraint11getPositionEi(%class.btMultiBodyConstraint* %this, i32 %row) #0 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4, !tbaa !2
  store i32 %row, i32* %row.addr, align 4, !tbaa !6
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 11
  %m_pos_offset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  %0 = load i32, i32* %m_pos_offset, align 4, !tbaa !56
  %1 = load i32, i32* %row.addr, align 4, !tbaa !6
  %add = add nsw i32 %0, %1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_data, i32 %add)
  %2 = load float, float* %call, align 4, !tbaa !8
  ret float %2
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !61
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !62
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(188) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4, !tbaa !63
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4, !tbaa !61
  %1 = load i32, i32* %n.addr, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !57
  ret i32 %0
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !64
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #8
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %3, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !65
  %5 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %5, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !60
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !6
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !64
  %7 = bitcast %struct.btMultiBodySolverConstraint** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #7 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !6
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !6
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !6
  store i32 %end, i32* %end.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %start.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %end.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  %6 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 184, i8* %6)
  %7 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !60
  %9 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 %9
  %10 = bitcast %struct.btMultiBodySolverConstraint* %7 to i8*
  %11 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 184, i1 false), !tbaa.struct !66
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #8
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #6 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !6
  store i32 %last, i32* %last.addr, align 4, !tbaa !6
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #8
  %1 = load i32, i32* %first.addr, align 4, !tbaa !6
  store i32 %1, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !6
  %3 = load i32, i32* %last.addr, align 4, !tbaa !6
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !60
  %5 = load i32, i32* %i, align 4, !tbaa !6
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !6
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !6
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #8
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.16* %this) #7 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4, !tbaa !60
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !65, !range !68
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4, !tbaa !60
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4, !tbaa !60
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !6
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !6
  %mul = mul i32 184, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #1

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #6 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !69
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %this, %struct.btMultiBodySolverConstraint* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4, !tbaa !2
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #1

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !7, i64 0}
!7 = !{!"int", !4, i64 0}
!8 = !{!9, !9, i64 0}
!9 = !{!"float", !4, i64 0}
!10 = !{!11, !11, i64 0}
!11 = !{!"vtable pointer", !5, i64 0}
!12 = !{!13, !9, i64 64}
!13 = !{!"_ZTS31btMultiBodyJointLimitConstraint", !9, i64 64, !9, i64 68}
!14 = !{!13, !9, i64 68}
!15 = !{!16, !7, i64 20}
!16 = !{!"_ZTS21btMultiBodyConstraint", !3, i64 4, !3, i64 8, !7, i64 12, !7, i64 16, !7, i64 20, !7, i64 24, !7, i64 28, !7, i64 32, !17, i64 36, !9, i64 40, !18, i64 44}
!17 = !{!"bool", !4, i64 0}
!18 = !{!"_ZTS20btAlignedObjectArrayIfE", !19, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!19 = !{!"_ZTS18btAlignedAllocatorIfLj16EE"}
!20 = !{!16, !7, i64 28}
!21 = !{!16, !7, i64 24}
!22 = !{!16, !3, i64 4}
!23 = !{!24, !3, i64 180}
!24 = !{!"_ZTS15btMultibodyLink", !9, i64 0, !9, i64 4, !25, i64 8, !7, i64 24, !26, i64 28, !25, i64 44, !25, i64 60, !25, i64 76, !25, i64 92, !17, i64 108, !26, i64 112, !25, i64 128, !25, i64 144, !25, i64 160, !9, i64 176, !3, i64 180, !7, i64 184}
!25 = !{!"_ZTS9btVector3", !4, i64 0}
!26 = !{!"_ZTS12btQuaternion"}
!27 = !{!28, !3, i64 0}
!28 = !{!"_ZTS11btMultiBody", !3, i64 0, !25, i64 4, !26, i64 20, !9, i64 36, !25, i64 40, !25, i64 56, !25, i64 72, !29, i64 88, !31, i64 108, !18, i64 128, !33, i64 148, !35, i64 168, !37, i64 188, !37, i64 236, !37, i64 284, !37, i64 332, !17, i64 380, !17, i64 381, !17, i64 382, !9, i64 384, !7, i64 388, !9, i64 392, !9, i64 396, !17, i64 400, !9, i64 404, !17, i64 408}
!29 = !{!"_ZTS20btAlignedObjectArrayI15btMultibodyLinkE", !30, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!30 = !{!"_ZTS18btAlignedAllocatorI15btMultibodyLinkLj16EE"}
!31 = !{!"_ZTS20btAlignedObjectArrayIP23btMultiBodyLinkColliderE", !32, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!32 = !{!"_ZTS18btAlignedAllocatorIP23btMultiBodyLinkColliderLj16EE"}
!33 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !34, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!34 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!35 = !{!"_ZTS20btAlignedObjectArrayI11btMatrix3x3E", !36, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!36 = !{!"_ZTS18btAlignedAllocatorI11btMatrix3x3Lj16EE"}
!37 = !{!"_ZTS11btMatrix3x3", !4, i64 0}
!38 = !{!39, !7, i64 208}
!39 = !{!"_ZTS17btCollisionObject", !40, i64 4, !40, i64 68, !25, i64 132, !25, i64 148, !25, i64 164, !7, i64 180, !9, i64 184, !3, i64 188, !3, i64 192, !3, i64 196, !3, i64 200, !7, i64 204, !7, i64 208, !7, i64 212, !7, i64 216, !9, i64 220, !9, i64 224, !9, i64 228, !9, i64 232, !7, i64 236, !4, i64 240, !9, i64 244, !9, i64 248, !9, i64 252, !7, i64 256, !7, i64 260}
!40 = !{!"_ZTS11btTransform", !37, i64 0, !25, i64 48}
!41 = !{!16, !3, i64 8}
!42 = !{!16, !7, i64 12}
!43 = !{!44, !3, i64 164}
!44 = !{!"_ZTS27btMultiBodySolverConstraint", !7, i64 0, !25, i64 4, !25, i64 20, !7, i64 36, !7, i64 40, !25, i64 44, !25, i64 60, !7, i64 76, !25, i64 80, !25, i64 96, !9, i64 112, !9, i64 116, !9, i64 120, !9, i64 124, !9, i64 128, !9, i64 132, !9, i64 136, !9, i64 140, !9, i64 144, !4, i64 148, !7, i64 152, !7, i64 156, !7, i64 160, !3, i64 164, !7, i64 168, !7, i64 172, !3, i64 176, !7, i64 180}
!45 = !{!44, !3, i64 176}
!46 = !{!16, !9, i64 40}
!47 = !{!48, !9, i64 36}
!48 = !{!"_ZTS23btContactSolverInfoData", !9, i64 0, !9, i64 4, !9, i64 8, !9, i64 12, !9, i64 16, !7, i64 20, !9, i64 24, !9, i64 28, !9, i64 32, !9, i64 36, !9, i64 40, !7, i64 44, !9, i64 48, !9, i64 52, !9, i64 56, !9, i64 60, !7, i64 64, !7, i64 68, !7, i64 72, !9, i64 76, !9, i64 80}
!49 = !{!48, !7, i64 44}
!50 = !{!48, !9, i64 48}
!51 = !{!48, !9, i64 32}
!52 = !{!48, !9, i64 12}
!53 = !{!44, !9, i64 124}
!54 = !{!44, !9, i64 128}
!55 = !{!44, !9, i64 144}
!56 = !{!16, !7, i64 32}
!57 = !{!58, !7, i64 4}
!58 = !{!"_ZTS20btAlignedObjectArrayI27btMultiBodySolverConstraintE", !59, i64 0, !7, i64 4, !7, i64 8, !3, i64 12, !17, i64 16}
!59 = !{!"_ZTS18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE"}
!60 = !{!58, !3, i64 12}
!61 = !{!18, !3, i64 12}
!62 = !{!29, !7, i64 4}
!63 = !{!29, !3, i64 12}
!64 = !{!58, !7, i64 8}
!65 = !{!58, !17, i64 16}
!66 = !{i64 0, i64 4, !6, i64 4, i64 16, !67, i64 20, i64 16, !67, i64 36, i64 4, !6, i64 40, i64 4, !6, i64 44, i64 16, !67, i64 60, i64 16, !67, i64 76, i64 4, !6, i64 80, i64 16, !67, i64 96, i64 16, !67, i64 112, i64 4, !8, i64 116, i64 4, !8, i64 120, i64 4, !8, i64 124, i64 4, !8, i64 128, i64 4, !8, i64 132, i64 4, !8, i64 136, i64 4, !8, i64 140, i64 4, !8, i64 144, i64 4, !8, i64 148, i64 4, !2, i64 148, i64 4, !8, i64 152, i64 4, !6, i64 156, i64 4, !6, i64 160, i64 4, !6, i64 164, i64 4, !2, i64 168, i64 4, !6, i64 172, i64 4, !6, i64 176, i64 4, !2, i64 180, i64 4, !6}
!67 = !{!4, !4, i64 0}
!68 = !{i8 0, i8 2}
!69 = !{!70, !70, i64 0}
!70 = !{!"long", !4, i64 0}
