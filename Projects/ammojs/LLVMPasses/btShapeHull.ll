; ModuleID = '/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btShapeHull.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/ammojs/Source/bullet/src/BulletCollision/CollisionShapes/btShapeHull.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%class.btVector3 = type { [4 x float] }
%class.btShapeHull = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, i32, %class.btConvexShape* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8* }
%class.HullDesc = type { i32, i32, %class.btVector3*, i32, float, i32, i32 }
%class.HullLibrary = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btHullTriangle**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btHullTriangle = type opaque
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.HullResult = type { i8, i32, %class.btAlignedObjectArray, i32, i32, %class.btAlignedObjectArray.0 }

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN8HullDescC2Ev = comdat any

$_ZN11HullLibraryC2Ev = comdat any

$_ZN10HullResultC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN20btAlignedObjectArrayIjE6resizeEiRKj = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN10HullResultD2Ev = comdat any

$_ZN11HullLibraryD2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIjE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIjE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIjE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIjE4copyEiiPj = comdat any

$_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj = comdat any

@_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints = internal global [62 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints = internal global i32 0, align 4

@_ZN11btShapeHullC1EPK13btConvexShape = hidden unnamed_addr alias %class.btShapeHull* (%class.btShapeHull*, %class.btConvexShape*), %class.btShapeHull* (%class.btShapeHull*, %class.btConvexShape*)* @_ZN11btShapeHullC2EPK13btConvexShape
@_ZN11btShapeHullD1Ev = hidden unnamed_addr alias %class.btShapeHull* (%class.btShapeHull*), %class.btShapeHull* (%class.btShapeHull*)* @_ZN11btShapeHullD2Ev

define hidden %class.btShapeHull* @_ZN11btShapeHullC2EPK13btConvexShape(%class.btShapeHull* returned %this, %class.btConvexShape* %shape) unnamed_addr #0 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  %shape.addr = alloca %class.btConvexShape*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  store %class.btConvexShape* %shape, %class.btConvexShape** %shape.addr, align 4, !tbaa !2
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_vertices)
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* %m_indices)
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape.addr, align 4, !tbaa !2
  %m_shape = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  store %class.btConvexShape* %0, %class.btConvexShape** %m_shape, align 4, !tbaa !6
  %m_vertices3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %m_vertices3)
  %m_indices4 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %m_indices4)
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  store i32 0, i32* %m_numIndices, align 4, !tbaa !14
  ret %class.btShapeHull* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: nounwind
define hidden %class.btShapeHull* @_ZN11btShapeHullD2Ev(%class.btShapeHull* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %m_indices)
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %m_vertices)
  %m_indices2 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* %m_indices2) #6
  %m_vertices3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_vertices3) #6
  ret %class.btShapeHull* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

define hidden zeroext i1 @_ZN11btShapeHull9buildHullEf(%class.btShapeHull* %this, float %0) #0 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btShapeHull*, align 4
  %.addr = alloca float, align 4
  %numSampleDirections = alloca i32, align 4
  %numPDA = alloca i32, align 4
  %i = alloca i32, align 4
  %norm = alloca %class.btVector3, align 4
  %supportPoints = alloca [62 x %class.btVector3], align 16
  %i9 = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %hd = alloca %class.HullDesc, align 4
  %hl = alloca %class.HullLibrary, align 4
  %hr = alloca %class.HullResult, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca i32, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  store float %0, float* %.addr, align 4, !tbaa !15
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %1 = bitcast i32* %numSampleDirections to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  store i32 42, i32* %numSampleDirections, align 4, !tbaa !17
  %2 = bitcast i32* %numPDA to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %2) #6
  %m_shape = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %3 = load %class.btConvexShape*, %class.btConvexShape** %m_shape, align 4, !tbaa !6
  %4 = bitcast %class.btConvexShape* %3 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %4, align 4, !tbaa !18
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %5 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call = call i32 %5(%class.btConvexShape* %3)
  store i32 %call, i32* %numPDA, align 4, !tbaa !17
  %6 = load i32, i32* %numPDA, align 4, !tbaa !17
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store i32 0, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %i, align 4, !tbaa !17
  %9 = load i32, i32* %numPDA, align 4, !tbaa !17
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %10 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %10) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %11 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %11) #6
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm)
  %m_shape3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %12 = load %class.btConvexShape*, %class.btConvexShape** %m_shape3, align 4, !tbaa !6
  %13 = load i32, i32* %i, align 4, !tbaa !17
  %14 = bitcast %class.btConvexShape* %12 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable4 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %14, align 4, !tbaa !18
  %vfn5 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable4, i64 22
  %15 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn5, align 4
  call void %15(%class.btConvexShape* %12, i32 %13, %class.btVector3* nonnull align 4 dereferenceable(16) %norm)
  %call6 = call %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv()
  %16 = load i32, i32* %numSampleDirections, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call6, i32 %16
  %17 = bitcast %class.btVector3* %arrayidx to i8*
  %18 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false), !tbaa.struct !20
  %19 = load i32, i32* %numSampleDirections, align 4, !tbaa !17
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %numSampleDirections, align 4, !tbaa !17
  %20 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %20) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4, !tbaa !17
  %inc7 = add nsw i32 %21, 1
  store i32 %inc7, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %22 = bitcast i32* %numPDA to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %22) #6
  %23 = bitcast [62 x %class.btVector3]* %supportPoints to i8*
  call void @llvm.lifetime.start.p0i8(i64 992, i8* %23) #6
  %array.begin = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 62
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %24 = bitcast i32* %i9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store i32 0, i32* %i9, align 4, !tbaa !17
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc19, %arrayctor.cont
  %25 = load i32, i32* %i9, align 4, !tbaa !17
  %26 = load i32, i32* %numSampleDirections, align 4, !tbaa !17
  %cmp11 = icmp slt i32 %25, %26
  br i1 %cmp11, label %for.body12, label %for.end21

for.body12:                                       ; preds = %for.cond10
  %27 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %27) #6
  %m_shape13 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %28 = load %class.btConvexShape*, %class.btConvexShape** %m_shape13, align 4, !tbaa !6
  %call14 = call %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv()
  %29 = load i32, i32* %i9, align 4, !tbaa !17
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %call14, i32 %29
  %30 = bitcast %class.btConvexShape* %28 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable16 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %30, align 4, !tbaa !18
  %vfn17 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable16, i64 16
  %31 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn17, align 4
  call void %31(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15)
  %32 = load i32, i32* %i9, align 4, !tbaa !17
  %arrayidx18 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 %32
  %33 = bitcast %class.btVector3* %arrayidx18 to i8*
  %34 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %33, i8* align 4 %34, i32 16, i1 false), !tbaa.struct !20
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %35) #6
  br label %for.inc19

for.inc19:                                        ; preds = %for.body12
  %36 = load i32, i32* %i9, align 4, !tbaa !17
  %inc20 = add nsw i32 %36, 1
  store i32 %inc20, i32* %i9, align 4, !tbaa !17
  br label %for.cond10

for.end21:                                        ; preds = %for.cond10
  %37 = bitcast %class.HullDesc* %hd to i8*
  call void @llvm.lifetime.start.p0i8(i64 28, i8* %37) #6
  %call22 = call %class.HullDesc* @_ZN8HullDescC2Ev(%class.HullDesc* %hd)
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 0
  store i32 1, i32* %mFlags, align 4, !tbaa !22
  %38 = load i32, i32* %numSampleDirections, align 4, !tbaa !17
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 1
  store i32 %38, i32* %mVcount, align 4, !tbaa !24
  %arrayidx23 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 0
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 2
  store %class.btVector3* %arrayidx23, %class.btVector3** %mVertices, align 4, !tbaa !25
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 3
  store i32 16, i32* %mVertexStride, align 4, !tbaa !26
  %39 = bitcast %class.HullLibrary* %hl to i8*
  call void @llvm.lifetime.start.p0i8(i64 40, i8* %39) #6
  %call24 = call %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* %hl)
  %40 = bitcast %class.HullResult* %hr to i8*
  call void @llvm.lifetime.start.p0i8(i64 56, i8* %40) #6
  %call25 = call %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* %hr)
  %call26 = call i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary* %hl, %class.HullDesc* nonnull align 4 dereferenceable(28) %hd, %class.HullResult* nonnull align 4 dereferenceable(56) %hr)
  %cmp27 = icmp eq i32 %call26, 1
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.end21
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %for.end21
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 1
  %41 = load i32, i32* %mNumOutputVertices, align 4, !tbaa !27
  %42 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.start.p0i8(i64 16, i8* %42) #6
  %call31 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp30)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_vertices, i32 %41, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  %43 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.lifetime.end.p0i8(i64 16, i8* %43) #6
  store i32 0, i32* %i9, align 4, !tbaa !17
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc39, %if.end29
  %44 = load i32, i32* %i9, align 4, !tbaa !17
  %mNumOutputVertices33 = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 1
  %45 = load i32, i32* %mNumOutputVertices33, align 4, !tbaa !27
  %cmp34 = icmp slt i32 %44, %45
  br i1 %cmp34, label %for.body35, label %for.end41

for.body35:                                       ; preds = %for.cond32
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 2
  %46 = load i32, i32* %i9, align 4, !tbaa !17
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_OutputVertices, i32 %46)
  %m_vertices37 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %47 = load i32, i32* %i9, align 4, !tbaa !17
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices37, i32 %47)
  %48 = bitcast %class.btVector3* %call38 to i8*
  %49 = bitcast %class.btVector3* %call36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false), !tbaa.struct !20
  br label %for.inc39

for.inc39:                                        ; preds = %for.body35
  %50 = load i32, i32* %i9, align 4, !tbaa !17
  %inc40 = add nsw i32 %50, 1
  store i32 %inc40, i32* %i9, align 4, !tbaa !17
  br label %for.cond32

for.end41:                                        ; preds = %for.cond32
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 4
  %51 = load i32, i32* %mNumIndices, align 4, !tbaa !29
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  store i32 %51, i32* %m_numIndices, align 4, !tbaa !14
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %m_numIndices42 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %52 = load i32, i32* %m_numIndices42, align 4, !tbaa !14
  %53 = bitcast i32* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  store i32 0, i32* %ref.tmp43, align 4, !tbaa !17
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.0* %m_indices, i32 %52, i32* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %54 = bitcast i32* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %54) #6
  store i32 0, i32* %i9, align 4, !tbaa !17
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc51, %for.end41
  %55 = load i32, i32* %i9, align 4, !tbaa !17
  %m_numIndices45 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %56 = load i32, i32* %m_numIndices45, align 4, !tbaa !14
  %cmp46 = icmp slt i32 %55, %56
  br i1 %cmp46, label %for.body47, label %for.end53

for.body47:                                       ; preds = %for.cond44
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 5
  %57 = load i32, i32* %i9, align 4, !tbaa !17
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %m_Indices, i32 %57)
  %58 = load i32, i32* %call48, align 4, !tbaa !17
  %m_indices49 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %59 = load i32, i32* %i9, align 4, !tbaa !17
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %m_indices49, i32 %59)
  store i32 %58, i32* %call50, align 4, !tbaa !17
  br label %for.inc51

for.inc51:                                        ; preds = %for.body47
  %60 = load i32, i32* %i9, align 4, !tbaa !17
  %inc52 = add nsw i32 %60, 1
  store i32 %inc52, i32* %i9, align 4, !tbaa !17
  br label %for.cond44

for.end53:                                        ; preds = %for.cond44
  %call54 = call i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary* %hl, %class.HullResult* nonnull align 4 dereferenceable(56) %hr)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end53, %if.then28
  %call55 = call %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* %hr) #6
  %61 = bitcast %class.HullResult* %hr to i8*
  call void @llvm.lifetime.end.p0i8(i64 56, i8* %61) #6
  %call58 = call %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* %hl) #6
  %62 = bitcast %class.HullLibrary* %hl to i8*
  call void @llvm.lifetime.end.p0i8(i64 40, i8* %62) #6
  %63 = bitcast %class.HullDesc* %hd to i8*
  call void @llvm.lifetime.end.p0i8(i64 28, i8* %63) #6
  %64 = bitcast i32* %i9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %64) #6
  %65 = bitcast [62 x %class.btVector3]* %supportPoints to i8*
  call void @llvm.lifetime.end.p0i8(i64 992, i8* %65) #6
  %66 = bitcast i32* %numSampleDirections to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %66) #6
  %67 = load i1, i1* %retval, align 1
  ret i1 %67
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

define hidden %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv() #0 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %ref.tmp85 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp92 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp99 = alloca float, align 4
  %ref.tmp100 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp109 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %ref.tmp117 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca float, align 4
  %ref.tmp132 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp136 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp152 = alloca float, align 4
  %ref.tmp153 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp160 = alloca float, align 4
  %ref.tmp161 = alloca float, align 4
  %ref.tmp163 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %ref.tmp165 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !30

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %3 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  store float 0.000000e+00, float* %ref.tmp, align 4, !tbaa !15
  %4 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %4) #6
  store float -0.000000e+00, float* %ref.tmp1, align 4, !tbaa !15
  %5 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %5) #6
  store float -1.000000e+00, float* %ref.tmp2, align 4, !tbaa !15
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %6 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %6) #6
  store float 0x3FE727CC00000000, float* %ref.tmp3, align 4, !tbaa !15
  %7 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %7) #6
  store float 0xBFE0D2BD40000000, float* %ref.tmp4, align 4, !tbaa !15
  %8 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %8) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp5, align 4, !tbaa !15
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %9 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %9) #6
  store float 0xBFD1B05740000000, float* %ref.tmp7, align 4, !tbaa !15
  %10 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %10) #6
  store float 0xBFEB388440000000, float* %ref.tmp8, align 4, !tbaa !15
  %11 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %11) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp9, align 4, !tbaa !15
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %12 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %12) #6
  store float 0xBFEC9F2340000000, float* %ref.tmp11, align 4, !tbaa !15
  %13 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  store float -0.000000e+00, float* %ref.tmp12, align 4, !tbaa !15
  %14 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %14) #6
  store float 0xBFDC9F2FE0000000, float* %ref.tmp13, align 4, !tbaa !15
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %15 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %15) #6
  store float 0xBFD1B05740000000, float* %ref.tmp15, align 4, !tbaa !15
  %16 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %16) #6
  store float 0x3FEB388440000000, float* %ref.tmp16, align 4, !tbaa !15
  %17 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %17) #6
  store float 0xBFDC9F40A0000000, float* %ref.tmp17, align 4, !tbaa !15
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %18 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %18) #6
  store float 0x3FE727CC00000000, float* %ref.tmp19, align 4, !tbaa !15
  %19 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %19) #6
  store float 0x3FE0D2BD40000000, float* %ref.tmp20, align 4, !tbaa !15
  %20 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %20) #6
  store float 0xBFDC9F3C80000000, float* %ref.tmp21, align 4, !tbaa !15
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %21 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %21) #6
  store float 0x3FD1B05740000000, float* %ref.tmp23, align 4, !tbaa !15
  %22 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %22) #6
  store float 0xBFEB388440000000, float* %ref.tmp24, align 4, !tbaa !15
  %23 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %23) #6
  store float 0x3FDC9F40A0000000, float* %ref.tmp25, align 4, !tbaa !15
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 6), float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %24 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %24) #6
  store float 0xBFE727CC00000000, float* %ref.tmp27, align 4, !tbaa !15
  %25 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %25) #6
  store float 0xBFE0D2BD40000000, float* %ref.tmp28, align 4, !tbaa !15
  %26 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %26) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp29, align 4, !tbaa !15
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 7), float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %27 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %27) #6
  store float 0xBFE727CC00000000, float* %ref.tmp31, align 4, !tbaa !15
  %28 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %28) #6
  store float 0x3FE0D2BD40000000, float* %ref.tmp32, align 4, !tbaa !15
  %29 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %29) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp33, align 4, !tbaa !15
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 8), float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %30 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %30) #6
  store float 0x3FD1B05740000000, float* %ref.tmp35, align 4, !tbaa !15
  %31 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %31) #6
  store float 0x3FEB388440000000, float* %ref.tmp36, align 4, !tbaa !15
  %32 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %32) #6
  store float 0x3FDC9F3C80000000, float* %ref.tmp37, align 4, !tbaa !15
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 9), float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  %33 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %33) #6
  store float 0x3FEC9F2340000000, float* %ref.tmp39, align 4, !tbaa !15
  %34 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %34) #6
  store float 0.000000e+00, float* %ref.tmp40, align 4, !tbaa !15
  %35 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %35) #6
  store float 0x3FDC9F2FE0000000, float* %ref.tmp41, align 4, !tbaa !15
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 10), float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %36 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %36) #6
  store float -0.000000e+00, float* %ref.tmp43, align 4, !tbaa !15
  %37 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %37) #6
  store float 0.000000e+00, float* %ref.tmp44, align 4, !tbaa !15
  %38 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %38) #6
  store float 1.000000e+00, float* %ref.tmp45, align 4, !tbaa !15
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 11), float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %39 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %39) #6
  store float 0x3FDB387E00000000, float* %ref.tmp47, align 4, !tbaa !15
  %40 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %40) #6
  store float 0xBFD3C6D620000000, float* %ref.tmp48, align 4, !tbaa !15
  %41 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %41) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp49, align 4, !tbaa !15
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 12), float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %42 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %42) #6
  store float 0xBFC4CB5BC0000000, float* %ref.tmp51, align 4, !tbaa !15
  %43 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %43) #6
  store float 0xBFDFFFEB00000000, float* %ref.tmp52, align 4, !tbaa !15
  %44 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %44) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp53, align 4, !tbaa !15
  %call54 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 13), float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %45 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %45) #6
  store float 0x3FD0D2D880000000, float* %ref.tmp55, align 4, !tbaa !15
  %46 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %46) #6
  store float 0xBFE9E36D20000000, float* %ref.tmp56, align 4, !tbaa !15
  %47 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %47) #6
  store float 0xBFE0D2D880000000, float* %ref.tmp57, align 4, !tbaa !15
  %call58 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 14), float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  %48 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %48) #6
  store float 0x3FDB387E00000000, float* %ref.tmp59, align 4, !tbaa !15
  %49 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %49) #6
  store float 0x3FD3C6D620000000, float* %ref.tmp60, align 4, !tbaa !15
  %50 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %50) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp61, align 4, !tbaa !15
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 15), float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %51 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %51) #6
  store float 0x3FEB388220000000, float* %ref.tmp63, align 4, !tbaa !15
  %52 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %52) #6
  store float -0.000000e+00, float* %ref.tmp64, align 4, !tbaa !15
  %53 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %53) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp65, align 4, !tbaa !15
  %call66 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 16), float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  %54 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %54) #6
  store float 0xBFE0D2C7C0000000, float* %ref.tmp67, align 4, !tbaa !15
  %55 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %55) #6
  store float -0.000000e+00, float* %ref.tmp68, align 4, !tbaa !15
  %56 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %56) #6
  store float 0xBFEB388A80000000, float* %ref.tmp69, align 4, !tbaa !15
  %call70 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 17), float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %57 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %57) #6
  store float 0xBFE605A700000000, float* %ref.tmp71, align 4, !tbaa !15
  %58 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %58) #6
  store float 0xBFDFFFF360000000, float* %ref.tmp72, align 4, !tbaa !15
  %59 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %59) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp73, align 4, !tbaa !15
  %call74 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 18), float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  %60 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %60) #6
  store float 0xBFC4CB5BC0000000, float* %ref.tmp75, align 4, !tbaa !15
  %61 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %61) #6
  store float 0x3FDFFFEB00000000, float* %ref.tmp76, align 4, !tbaa !15
  %62 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %62) #6
  store float 0xBFEB388EC0000000, float* %ref.tmp77, align 4, !tbaa !15
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 19), float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  %63 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %63) #6
  store float 0xBFE605A700000000, float* %ref.tmp79, align 4, !tbaa !15
  %64 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %64) #6
  store float 0x3FDFFFF360000000, float* %ref.tmp80, align 4, !tbaa !15
  %65 = bitcast float* %ref.tmp81 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %65) #6
  store float 0xBFE0D2D440000000, float* %ref.tmp81, align 4, !tbaa !15
  %call82 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 20), float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp81)
  %66 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %66) #6
  store float 0x3FD0D2D880000000, float* %ref.tmp83, align 4, !tbaa !15
  %67 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %67) #6
  store float 0x3FE9E36D20000000, float* %ref.tmp84, align 4, !tbaa !15
  %68 = bitcast float* %ref.tmp85 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %68) #6
  store float 0xBFE0D2D880000000, float* %ref.tmp85, align 4, !tbaa !15
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 21), float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84, float* nonnull align 4 dereferenceable(4) %ref.tmp85)
  %69 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %69) #6
  store float 0x3FEE6F1120000000, float* %ref.tmp87, align 4, !tbaa !15
  %70 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %70) #6
  store float 0x3FD3C6DE80000000, float* %ref.tmp88, align 4, !tbaa !15
  %71 = bitcast float* %ref.tmp89 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %71) #6
  store float 0.000000e+00, float* %ref.tmp89, align 4, !tbaa !15
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 22), float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  %72 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %72) #6
  store float 0x3FEE6F1120000000, float* %ref.tmp91, align 4, !tbaa !15
  %73 = bitcast float* %ref.tmp92 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %73) #6
  store float 0xBFD3C6DE80000000, float* %ref.tmp92, align 4, !tbaa !15
  %74 = bitcast float* %ref.tmp93 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %74) #6
  store float 0.000000e+00, float* %ref.tmp93, align 4, !tbaa !15
  %call94 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 23), float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp92, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  %75 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %75) #6
  store float 0x3FE2CF24A0000000, float* %ref.tmp95, align 4, !tbaa !15
  %76 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %76) #6
  store float 0xBFE9E377A0000000, float* %ref.tmp96, align 4, !tbaa !15
  %77 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %77) #6
  store float 0.000000e+00, float* %ref.tmp97, align 4, !tbaa !15
  %call98 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 24), float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  %78 = bitcast float* %ref.tmp99 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %78) #6
  store float 0.000000e+00, float* %ref.tmp99, align 4, !tbaa !15
  %79 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %79) #6
  store float -1.000000e+00, float* %ref.tmp100, align 4, !tbaa !15
  %80 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %80) #6
  store float 0.000000e+00, float* %ref.tmp101, align 4, !tbaa !15
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 25), float* nonnull align 4 dereferenceable(4) %ref.tmp99, float* nonnull align 4 dereferenceable(4) %ref.tmp100, float* nonnull align 4 dereferenceable(4) %ref.tmp101)
  %81 = bitcast float* %ref.tmp103 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %81) #6
  store float 0xBFE2CF24A0000000, float* %ref.tmp103, align 4, !tbaa !15
  %82 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %82) #6
  store float 0xBFE9E377A0000000, float* %ref.tmp104, align 4, !tbaa !15
  %83 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %83) #6
  store float 0.000000e+00, float* %ref.tmp105, align 4, !tbaa !15
  %call106 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 26), float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  %84 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %84) #6
  store float 0xBFEE6F1120000000, float* %ref.tmp107, align 4, !tbaa !15
  %85 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %85) #6
  store float 0xBFD3C6DE80000000, float* %ref.tmp108, align 4, !tbaa !15
  %86 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %86) #6
  store float -0.000000e+00, float* %ref.tmp109, align 4, !tbaa !15
  %call110 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 27), float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp109)
  %87 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %87) #6
  store float 0xBFEE6F1120000000, float* %ref.tmp111, align 4, !tbaa !15
  %88 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %88) #6
  store float 0x3FD3C6DE80000000, float* %ref.tmp112, align 4, !tbaa !15
  %89 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %89) #6
  store float -0.000000e+00, float* %ref.tmp113, align 4, !tbaa !15
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 28), float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  %90 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %90) #6
  store float 0xBFE2CF24A0000000, float* %ref.tmp115, align 4, !tbaa !15
  %91 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %91) #6
  store float 0x3FE9E377A0000000, float* %ref.tmp116, align 4, !tbaa !15
  %92 = bitcast float* %ref.tmp117 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %92) #6
  store float -0.000000e+00, float* %ref.tmp117, align 4, !tbaa !15
  %call118 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 29), float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116, float* nonnull align 4 dereferenceable(4) %ref.tmp117)
  %93 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %93) #6
  store float -0.000000e+00, float* %ref.tmp119, align 4, !tbaa !15
  %94 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %94) #6
  store float 1.000000e+00, float* %ref.tmp120, align 4, !tbaa !15
  %95 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %95) #6
  store float -0.000000e+00, float* %ref.tmp121, align 4, !tbaa !15
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 30), float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  %96 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %96) #6
  store float 0x3FE2CF24A0000000, float* %ref.tmp123, align 4, !tbaa !15
  %97 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %97) #6
  store float 0x3FE9E377A0000000, float* %ref.tmp124, align 4, !tbaa !15
  %98 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %98) #6
  store float -0.000000e+00, float* %ref.tmp125, align 4, !tbaa !15
  %call126 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 31), float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125)
  %99 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %99) #6
  store float 0x3FE605A700000000, float* %ref.tmp127, align 4, !tbaa !15
  %100 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %100) #6
  store float 0xBFDFFFF360000000, float* %ref.tmp128, align 4, !tbaa !15
  %101 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %101) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp129, align 4, !tbaa !15
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 32), float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  %102 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %102) #6
  store float 0xBFD0D2D880000000, float* %ref.tmp131, align 4, !tbaa !15
  %103 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %103) #6
  store float 0xBFE9E36D20000000, float* %ref.tmp132, align 4, !tbaa !15
  %104 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %104) #6
  store float 0x3FE0D2D880000000, float* %ref.tmp133, align 4, !tbaa !15
  %call134 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 33), float* nonnull align 4 dereferenceable(4) %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133)
  %105 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %105) #6
  store float 0xBFEB388220000000, float* %ref.tmp135, align 4, !tbaa !15
  %106 = bitcast float* %ref.tmp136 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %106) #6
  store float 0.000000e+00, float* %ref.tmp136, align 4, !tbaa !15
  %107 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %107) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp137, align 4, !tbaa !15
  %call138 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 34), float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp136, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  %108 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %108) #6
  store float 0xBFD0D2D880000000, float* %ref.tmp139, align 4, !tbaa !15
  %109 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %109) #6
  store float 0x3FE9E36D20000000, float* %ref.tmp140, align 4, !tbaa !15
  %110 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %110) #6
  store float 0x3FE0D2D880000000, float* %ref.tmp141, align 4, !tbaa !15
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 35), float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  %111 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %111) #6
  store float 0x3FE605A700000000, float* %ref.tmp143, align 4, !tbaa !15
  %112 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %112) #6
  store float 0x3FDFFFF360000000, float* %ref.tmp144, align 4, !tbaa !15
  %113 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %113) #6
  store float 0x3FE0D2D440000000, float* %ref.tmp145, align 4, !tbaa !15
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 36), float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  %114 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %114) #6
  store float 0x3FE0D2C7C0000000, float* %ref.tmp147, align 4, !tbaa !15
  %115 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %115) #6
  store float 0.000000e+00, float* %ref.tmp148, align 4, !tbaa !15
  %116 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %116) #6
  store float 0x3FEB388A80000000, float* %ref.tmp149, align 4, !tbaa !15
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 37), float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  %117 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %117) #6
  store float 0x3FC4CB5BC0000000, float* %ref.tmp151, align 4, !tbaa !15
  %118 = bitcast float* %ref.tmp152 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %118) #6
  store float 0xBFDFFFEB00000000, float* %ref.tmp152, align 4, !tbaa !15
  %119 = bitcast float* %ref.tmp153 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %119) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp153, align 4, !tbaa !15
  %call154 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 38), float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp152, float* nonnull align 4 dereferenceable(4) %ref.tmp153)
  %120 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %120) #6
  store float 0xBFDB387E00000000, float* %ref.tmp155, align 4, !tbaa !15
  %121 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %121) #6
  store float 0xBFD3C6D620000000, float* %ref.tmp156, align 4, !tbaa !15
  %122 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %122) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp157, align 4, !tbaa !15
  %call158 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 39), float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157)
  %123 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %123) #6
  store float 0xBFDB387E00000000, float* %ref.tmp159, align 4, !tbaa !15
  %124 = bitcast float* %ref.tmp160 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %124) #6
  store float 0x3FD3C6D620000000, float* %ref.tmp160, align 4, !tbaa !15
  %125 = bitcast float* %ref.tmp161 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %125) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp161, align 4, !tbaa !15
  %call162 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 40), float* nonnull align 4 dereferenceable(4) %ref.tmp159, float* nonnull align 4 dereferenceable(4) %ref.tmp160, float* nonnull align 4 dereferenceable(4) %ref.tmp161)
  %126 = bitcast float* %ref.tmp163 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %126) #6
  store float 0x3FC4CB5BC0000000, float* %ref.tmp163, align 4, !tbaa !15
  %127 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %127) #6
  store float 0x3FDFFFEB00000000, float* %ref.tmp164, align 4, !tbaa !15
  %128 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %128) #6
  store float 0x3FEB388EC0000000, float* %ref.tmp165, align 4, !tbaa !15
  %call166 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 41), float* nonnull align 4 dereferenceable(4) %ref.tmp163, float* nonnull align 4 dereferenceable(4) %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165)
  br label %arrayinit.body

arrayinit.body:                                   ; preds = %arrayinit.body, %init
  %arrayinit.cur = phi %class.btVector3* [ getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 42), %init ], [ %arrayinit.next, %arrayinit.body ]
  %call167 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayinit.cur)
  %arrayinit.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.cur, i32 1
  %arrayinit.done = icmp eq %class.btVector3* %arrayinit.next, getelementptr inbounds (%class.btVector3, %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0), i32 62)
  br i1 %arrayinit.done, label %arrayinit.end, label %arrayinit.body

arrayinit.end:                                    ; preds = %arrayinit.body
  %129 = bitcast float* %ref.tmp165 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %129) #6
  %130 = bitcast float* %ref.tmp164 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %130) #6
  %131 = bitcast float* %ref.tmp163 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %131) #6
  %132 = bitcast float* %ref.tmp161 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %132) #6
  %133 = bitcast float* %ref.tmp160 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %133) #6
  %134 = bitcast float* %ref.tmp159 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %134) #6
  %135 = bitcast float* %ref.tmp157 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %135) #6
  %136 = bitcast float* %ref.tmp156 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %136) #6
  %137 = bitcast float* %ref.tmp155 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %137) #6
  %138 = bitcast float* %ref.tmp153 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %138) #6
  %139 = bitcast float* %ref.tmp152 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %139) #6
  %140 = bitcast float* %ref.tmp151 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %140) #6
  %141 = bitcast float* %ref.tmp149 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %141) #6
  %142 = bitcast float* %ref.tmp148 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %142) #6
  %143 = bitcast float* %ref.tmp147 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %143) #6
  %144 = bitcast float* %ref.tmp145 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %144) #6
  %145 = bitcast float* %ref.tmp144 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %145) #6
  %146 = bitcast float* %ref.tmp143 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %146) #6
  %147 = bitcast float* %ref.tmp141 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %147) #6
  %148 = bitcast float* %ref.tmp140 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %148) #6
  %149 = bitcast float* %ref.tmp139 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %149) #6
  %150 = bitcast float* %ref.tmp137 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %150) #6
  %151 = bitcast float* %ref.tmp136 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %151) #6
  %152 = bitcast float* %ref.tmp135 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %152) #6
  %153 = bitcast float* %ref.tmp133 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %153) #6
  %154 = bitcast float* %ref.tmp132 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %154) #6
  %155 = bitcast float* %ref.tmp131 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %155) #6
  %156 = bitcast float* %ref.tmp129 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %156) #6
  %157 = bitcast float* %ref.tmp128 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %157) #6
  %158 = bitcast float* %ref.tmp127 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %158) #6
  %159 = bitcast float* %ref.tmp125 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %159) #6
  %160 = bitcast float* %ref.tmp124 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %160) #6
  %161 = bitcast float* %ref.tmp123 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %161) #6
  %162 = bitcast float* %ref.tmp121 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %162) #6
  %163 = bitcast float* %ref.tmp120 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %163) #6
  %164 = bitcast float* %ref.tmp119 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %164) #6
  %165 = bitcast float* %ref.tmp117 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %165) #6
  %166 = bitcast float* %ref.tmp116 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %166) #6
  %167 = bitcast float* %ref.tmp115 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %167) #6
  %168 = bitcast float* %ref.tmp113 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %168) #6
  %169 = bitcast float* %ref.tmp112 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %169) #6
  %170 = bitcast float* %ref.tmp111 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %170) #6
  %171 = bitcast float* %ref.tmp109 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %171) #6
  %172 = bitcast float* %ref.tmp108 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %172) #6
  %173 = bitcast float* %ref.tmp107 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %173) #6
  %174 = bitcast float* %ref.tmp105 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %174) #6
  %175 = bitcast float* %ref.tmp104 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %175) #6
  %176 = bitcast float* %ref.tmp103 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %176) #6
  %177 = bitcast float* %ref.tmp101 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %177) #6
  %178 = bitcast float* %ref.tmp100 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %178) #6
  %179 = bitcast float* %ref.tmp99 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %179) #6
  %180 = bitcast float* %ref.tmp97 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %180) #6
  %181 = bitcast float* %ref.tmp96 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %181) #6
  %182 = bitcast float* %ref.tmp95 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %182) #6
  %183 = bitcast float* %ref.tmp93 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %183) #6
  %184 = bitcast float* %ref.tmp92 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %184) #6
  %185 = bitcast float* %ref.tmp91 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %185) #6
  %186 = bitcast float* %ref.tmp89 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %186) #6
  %187 = bitcast float* %ref.tmp88 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %187) #6
  %188 = bitcast float* %ref.tmp87 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %188) #6
  %189 = bitcast float* %ref.tmp85 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %189) #6
  %190 = bitcast float* %ref.tmp84 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %190) #6
  %191 = bitcast float* %ref.tmp83 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %191) #6
  %192 = bitcast float* %ref.tmp81 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %192) #6
  %193 = bitcast float* %ref.tmp80 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %193) #6
  %194 = bitcast float* %ref.tmp79 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %194) #6
  %195 = bitcast float* %ref.tmp77 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %195) #6
  %196 = bitcast float* %ref.tmp76 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %196) #6
  %197 = bitcast float* %ref.tmp75 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %197) #6
  %198 = bitcast float* %ref.tmp73 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %198) #6
  %199 = bitcast float* %ref.tmp72 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %199) #6
  %200 = bitcast float* %ref.tmp71 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %200) #6
  %201 = bitcast float* %ref.tmp69 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %201) #6
  %202 = bitcast float* %ref.tmp68 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %202) #6
  %203 = bitcast float* %ref.tmp67 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %203) #6
  %204 = bitcast float* %ref.tmp65 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %204) #6
  %205 = bitcast float* %ref.tmp64 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %205) #6
  %206 = bitcast float* %ref.tmp63 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %206) #6
  %207 = bitcast float* %ref.tmp61 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %207) #6
  %208 = bitcast float* %ref.tmp60 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %208) #6
  %209 = bitcast float* %ref.tmp59 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %209) #6
  %210 = bitcast float* %ref.tmp57 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %210) #6
  %211 = bitcast float* %ref.tmp56 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %211) #6
  %212 = bitcast float* %ref.tmp55 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %212) #6
  %213 = bitcast float* %ref.tmp53 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %213) #6
  %214 = bitcast float* %ref.tmp52 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %214) #6
  %215 = bitcast float* %ref.tmp51 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %215) #6
  %216 = bitcast float* %ref.tmp49 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %216) #6
  %217 = bitcast float* %ref.tmp48 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %217) #6
  %218 = bitcast float* %ref.tmp47 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %218) #6
  %219 = bitcast float* %ref.tmp45 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %219) #6
  %220 = bitcast float* %ref.tmp44 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %220) #6
  %221 = bitcast float* %ref.tmp43 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %221) #6
  %222 = bitcast float* %ref.tmp41 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %222) #6
  %223 = bitcast float* %ref.tmp40 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %223) #6
  %224 = bitcast float* %ref.tmp39 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %224) #6
  %225 = bitcast float* %ref.tmp37 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %225) #6
  %226 = bitcast float* %ref.tmp36 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %226) #6
  %227 = bitcast float* %ref.tmp35 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %227) #6
  %228 = bitcast float* %ref.tmp33 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %228) #6
  %229 = bitcast float* %ref.tmp32 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %229) #6
  %230 = bitcast float* %ref.tmp31 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %230) #6
  %231 = bitcast float* %ref.tmp29 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %231) #6
  %232 = bitcast float* %ref.tmp28 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %232) #6
  %233 = bitcast float* %ref.tmp27 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %233) #6
  %234 = bitcast float* %ref.tmp25 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %234) #6
  %235 = bitcast float* %ref.tmp24 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %235) #6
  %236 = bitcast float* %ref.tmp23 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %236) #6
  %237 = bitcast float* %ref.tmp21 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %237) #6
  %238 = bitcast float* %ref.tmp20 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %238) #6
  %239 = bitcast float* %ref.tmp19 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %239) #6
  %240 = bitcast float* %ref.tmp17 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %240) #6
  %241 = bitcast float* %ref.tmp16 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %241) #6
  %242 = bitcast float* %ref.tmp15 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %242) #6
  %243 = bitcast float* %ref.tmp13 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %243) #6
  %244 = bitcast float* %ref.tmp12 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %244) #6
  %245 = bitcast float* %ref.tmp11 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %245) #6
  %246 = bitcast float* %ref.tmp9 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %246) #6
  %247 = bitcast float* %ref.tmp8 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %247) #6
  %248 = bitcast float* %ref.tmp7 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %248) #6
  %249 = bitcast float* %ref.tmp5 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %249) #6
  %250 = bitcast float* %ref.tmp4 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %250) #6
  %251 = bitcast float* %ref.tmp3 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %251) #6
  %252 = bitcast float* %ref.tmp2 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %252) #6
  %253 = bitcast float* %ref.tmp1 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %253) #6
  %254 = bitcast float* %ref.tmp to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %254) #6
  call void @__cxa_guard_release(i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints) #6
  br label %init.end

init.end:                                         ; preds = %arrayinit.end, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0)
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #3

; Function Attrs: nounwind
define linkonce_odr hidden %class.HullDesc* @_ZN8HullDescC2Ev(%class.HullDesc* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullDesc*, align 4
  store %class.HullDesc* %this, %class.HullDesc** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullDesc*, %class.HullDesc** %this.addr, align 4
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 0
  store i32 1, i32* %mFlags, align 4, !tbaa !22
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 1
  store i32 0, i32* %mVcount, align 4, !tbaa !24
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 2
  store %class.btVector3* null, %class.btVector3** %mVertices, align 4, !tbaa !25
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 3
  store i32 16, i32* %mVertexStride, align 4, !tbaa !26
  %mNormalEpsilon = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 4
  store float 0x3F50624DE0000000, float* %mNormalEpsilon, align 4, !tbaa !31
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 5
  store i32 4096, i32* %mMaxVertices, align 4, !tbaa !32
  %mMaxFaces = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 6
  store i32 4096, i32* %mMaxFaces, align 4, !tbaa !33
  ret %class.HullDesc* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.4* %m_tris)
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.8* %m_vertexIndexMapping)
  ret %class.HullLibrary* %this1
}

define linkonce_odr hidden %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_OutputVertices)
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* %m_Indices)
  %mPolygons = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 0
  store i8 1, i8* %mPolygons, align 4, !tbaa !34
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 1
  store i32 0, i32* %mNumOutputVertices, align 4, !tbaa !27
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 3
  store i32 0, i32* %mNumFaces, align 4, !tbaa !35
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 4
  store i32 0, i32* %mNumIndices, align 4, !tbaa !29
  ret %class.HullResult* %this1
}

declare i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary*, %class.HullDesc* nonnull align 4 dereferenceable(28), %class.HullResult* nonnull align 4 dereferenceable(56)) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !17
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !17
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %2 = load i32, i32* %curSize, align 4, !tbaa !17
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  store i32 %4, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %6 = load i32, i32* %curSize, align 4, !tbaa !17
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end17

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %curSize, align 4, !tbaa !17
  store i32 %14, i32* %i6, align 4, !tbaa !17
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc14, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !17
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end16

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %18 = load %class.btVector3*, %class.btVector3** %m_data11, align 4, !tbaa !36
  %19 = load i32, i32* %i6, align 4, !tbaa !17
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 %19
  %20 = bitcast %class.btVector3* %arrayidx12 to i8*
  %call13 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %20)
  %21 = bitcast i8* %call13 to %class.btVector3*
  %22 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4, !tbaa !2
  %23 = bitcast %class.btVector3* %21 to i8*
  %24 = bitcast %class.btVector3* %22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false), !tbaa.struct !20
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %25 = load i32, i32* %i6, align 4, !tbaa !17
  %inc15 = add nsw i32 %25, 1
  store i32 %inc15, i32* %i6, align 4, !tbaa !17
  br label %for.cond7

for.end16:                                        ; preds = %for.cond.cleanup9
  br label %if.end17

if.end17:                                         ; preds = %for.end16, %for.end
  %26 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %26, i32* %m_size, align 4, !tbaa !37
  %27 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %27) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !36
  %1 = load i32, i32* %n.addr, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.0* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %newsize, i32* %newsize.addr, align 4, !tbaa !17
  store i32* %fillData, i32** %fillData.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4, !tbaa !17
  %1 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %2 = load i32, i32* %curSize, align 4, !tbaa !17
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %3) #6
  %4 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  store i32 %4, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %6 = load i32, i32* %curSize, align 4, !tbaa !17
  %cmp2 = icmp slt i32 %5, %6
  br i1 %cmp2, label %for.body, label %for.cond.cleanup

for.cond.cleanup:                                 ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond.cleanup
  br label %if.end16

if.else:                                          ; preds = %entry
  %11 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp4 = icmp sgt i32 %11, %call3
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %12 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  call void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %13 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %13) #6
  %14 = load i32, i32* %curSize, align 4, !tbaa !17
  store i32 %14, i32* %i6, align 4, !tbaa !17
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %15 = load i32, i32* %i6, align 4, !tbaa !17
  %16 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %cmp8 = icmp slt i32 %15, %16
  br i1 %cmp8, label %for.body10, label %for.cond.cleanup9

for.cond.cleanup9:                                ; preds = %for.cond7
  %17 = bitcast i32* %i6 to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %17) #6
  br label %for.end15

for.body10:                                       ; preds = %for.cond7
  %m_data11 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %18 = load i32*, i32** %m_data11, align 4, !tbaa !38
  %19 = load i32, i32* %i6, align 4, !tbaa !17
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 %19
  %20 = bitcast i32* %arrayidx12 to i8*
  %21 = bitcast i8* %20 to i32*
  %22 = load i32*, i32** %fillData.addr, align 4, !tbaa !2
  %23 = load i32, i32* %22, align 4, !tbaa !17
  store i32 %23, i32* %21, align 4, !tbaa !17
  br label %for.inc13

for.inc13:                                        ; preds = %for.body10
  %24 = load i32, i32* %i6, align 4, !tbaa !17
  %inc14 = add nsw i32 %24, 1
  store i32 %inc14, i32* %i6, align 4, !tbaa !17
  br label %for.cond7

for.end15:                                        ; preds = %for.cond.cleanup9
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %25 = load i32, i32* %newsize.addr, align 4, !tbaa !17
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %25, i32* %m_size, align 4, !tbaa !39
  %26 = bitcast i32* %curSize to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %26) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !38
  %1 = load i32, i32* %n.addr, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

declare i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary*, %class.HullResult* nonnull align 4 dereferenceable(56)) #5

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* %m_Indices) #6
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_OutputVertices) #6
  ret %class.HullResult* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* returned %this) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4, !tbaa !2
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.8* %m_vertexIndexMapping) #6
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call2 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.4* %m_tris) #6
  ret %class.HullLibrary* %this1
}

; Function Attrs: nounwind
define hidden i32 @_ZNK11btShapeHull12numTrianglesEv(%class.btShapeHull* %this) #2 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numIndices, align 4, !tbaa !14
  %div = udiv i32 %0, 3
  ret i32 %div
}

define hidden i32 @_ZNK11btShapeHull11numVerticesEv(%class.btShapeHull* %this) #0 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices)
  ret i32 %call
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !37
  ret i32 %0
}

; Function Attrs: nounwind
define hidden i32 @_ZNK11btShapeHull10numIndicesEv(%class.btShapeHull* %this) #2 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numIndices, align 4, !tbaa !14
  ret i32 %0
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #4 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4, !tbaa !2
  store float* %_x, float** %_x.addr, align 4, !tbaa !2
  store float* %_y, float** %_y.addr, align 4, !tbaa !2
  store float* %_z, float** %_z.addr, align 4, !tbaa !2
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4, !tbaa !2
  %1 = load float, float* %0, align 4, !tbaa !15
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4, !tbaa !15
  %2 = load float*, float** %_y.addr, align 4, !tbaa !2
  %3 = load float, float* %2, align 4, !tbaa !15
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4, !tbaa !15
  %4 = load float*, float** %_z.addr, align 4, !tbaa !2
  %5 = load float, float* %4, align 4, !tbaa !15
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4, !tbaa !15
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4, !tbaa !15
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !40
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data, align 4, !tbaa !43
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !44
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !45
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !46
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !49
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !50
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !51
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !17
  store i32 %last, i32* %last.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %last.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !49
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !50
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !49
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !46, !range !52
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !49
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !49
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !17
  store i32 %last, i32* %last.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %last.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4, !tbaa !43
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.4* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !44
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4, !tbaa !43
  %tobool = icmp ne %class.btHullTriangle** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !40, !range !52
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data4, align 4, !tbaa !43
  call void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btHullTriangle** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data5, align 4, !tbaa !43
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btHullTriangle** %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4, !tbaa !2
  store %class.btHullTriangle** %ptr, %class.btHullTriangle*** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btHullTriangle** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !53
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4, !tbaa !36
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !37
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !54
  ret void
}

; Function Attrs: nounwind
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4, !tbaa !38
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4, !tbaa !39
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4, !tbaa !56
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !17
  store i32 %last, i32* %last.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %last.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !36
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !36
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !53, !range !52
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4, !tbaa !36
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4, !tbaa !36
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %first, i32* %first.addr, align 4, !tbaa !17
  store i32 %last, i32* %last.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %first.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %last.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4, !tbaa !38
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4, !tbaa !39
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4, !tbaa !38
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4, !tbaa !55, !range !52
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4, !tbaa !38
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.1* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4, !tbaa !38
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.1* %this, i32* %ptr) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32* %ptr, i32** %ptr.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4, !tbaa !2
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %2)
  %3 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %3, %class.btVector3** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !53
  %5 = load %class.btVector3*, %class.btVector3** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %5, %class.btVector3** %m_data, align 4, !tbaa !36
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !54
  %7 = bitcast %class.btVector3** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #4 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4, !tbaa !57
  store i8* %ptr, i8** %ptr.addr, align 4, !tbaa !2
  %1 = load i8*, i8** %ptr.addr, align 4, !tbaa !2
  ret i8* %1
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !54
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !17
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !17
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !17
  store i32 %end, i32* %end.addr, align 4, !tbaa !17
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %start.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %end.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %6)
  %7 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %m_data, align 4, !tbaa !36
  %9 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = bitcast %class.btVector3* %7 to i8*
  %11 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false), !tbaa.struct !20
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %13) #6
  ret void
}

define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !17
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !17
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: inlinehint
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %_Count, i32* %_Count.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = bitcast i32** %s to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %1) #6
  %2 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %call2 = call i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %2)
  %3 = bitcast i8* %call2 to i32*
  store i32* %3, i32** %s, align 4, !tbaa !2
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %4 = load i32*, i32** %s, align 4, !tbaa !2
  call void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, i32* %4)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4, !tbaa !55
  %5 = load i32*, i32** %s, align 4, !tbaa !2
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* %5, i32** %m_data, align 4, !tbaa !38
  %6 = load i32, i32* %_Count.addr, align 4, !tbaa !17
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %6, i32* %m_capacity, align 4, !tbaa !56
  %7 = bitcast i32** %s to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %7) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.0* %this) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4, !tbaa !56
  ret i32 %0
}

; Function Attrs: inlinehint
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %size, i32* %size.addr, align 4, !tbaa !17
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4, !tbaa !17
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4, !tbaa !17
  %call = call i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.1* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: inlinehint nounwind
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, i32* %dest) #4 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4, !tbaa !2
  store i32 %start, i32* %start.addr, align 4, !tbaa !17
  store i32 %end, i32* %end.addr, align 4, !tbaa !17
  store i32* %dest, i32** %dest.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = bitcast i32* %i to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* %0) #6
  %1 = load i32, i32* %start.addr, align 4, !tbaa !17
  store i32 %1, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4, !tbaa !17
  %3 = load i32, i32* %end.addr, align 4, !tbaa !17
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %dest.addr, align 4, !tbaa !2
  %5 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = bitcast i32* %arrayidx to i8*
  %7 = bitcast i8* %6 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load i32*, i32** %m_data, align 4, !tbaa !38
  %9 = load i32, i32* %i, align 4, !tbaa !17
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4, !tbaa !17
  store i32 %10, i32* %7, align 4, !tbaa !17
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4, !tbaa !17
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4, !tbaa !17
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = bitcast i32* %i to i8*
  call void @llvm.lifetime.end.p0i8(i64 4, i8* %12) #6
  ret void
}

define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.1* %this, i32 %n, i32** %hint) #0 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4, !tbaa !2
  store i32 %n, i32* %n.addr, align 4, !tbaa !17
  store i32** %hint, i32*** %hint.addr, align 4, !tbaa !2
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4, !tbaa !17
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

attributes #0 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { inlinehint "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { inlinehint nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!3, !3, i64 0}
!3 = !{!"any pointer", !4, i64 0}
!4 = !{!"omnipotent char", !5, i64 0}
!5 = !{!"Simple C++ TBAA"}
!6 = !{!7, !3, i64 44}
!7 = !{!"_ZTS11btShapeHull", !8, i64 0, !12, i64 20, !10, i64 40, !3, i64 44}
!8 = !{!"_ZTS20btAlignedObjectArrayI9btVector3E", !9, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !11, i64 16}
!9 = !{!"_ZTS18btAlignedAllocatorI9btVector3Lj16EE"}
!10 = !{!"int", !4, i64 0}
!11 = !{!"bool", !4, i64 0}
!12 = !{!"_ZTS20btAlignedObjectArrayIjE", !13, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !11, i64 16}
!13 = !{!"_ZTS18btAlignedAllocatorIjLj16EE"}
!14 = !{!7, !10, i64 40}
!15 = !{!16, !16, i64 0}
!16 = !{!"float", !4, i64 0}
!17 = !{!10, !10, i64 0}
!18 = !{!19, !19, i64 0}
!19 = !{!"vtable pointer", !5, i64 0}
!20 = !{i64 0, i64 16, !21}
!21 = !{!4, !4, i64 0}
!22 = !{!23, !10, i64 0}
!23 = !{!"_ZTS8HullDesc", !10, i64 0, !10, i64 4, !3, i64 8, !10, i64 12, !16, i64 16, !10, i64 20, !10, i64 24}
!24 = !{!23, !10, i64 4}
!25 = !{!23, !3, i64 8}
!26 = !{!23, !10, i64 12}
!27 = !{!28, !10, i64 4}
!28 = !{!"_ZTS10HullResult", !11, i64 0, !10, i64 4, !8, i64 8, !10, i64 28, !10, i64 32, !12, i64 36}
!29 = !{!28, !10, i64 32}
!30 = !{!"branch_weights", i32 1, i32 1048575}
!31 = !{!23, !16, i64 16}
!32 = !{!23, !10, i64 20}
!33 = !{!23, !10, i64 24}
!34 = !{!28, !11, i64 0}
!35 = !{!28, !10, i64 28}
!36 = !{!8, !3, i64 12}
!37 = !{!8, !10, i64 4}
!38 = !{!12, !3, i64 12}
!39 = !{!12, !10, i64 4}
!40 = !{!41, !11, i64 16}
!41 = !{!"_ZTS20btAlignedObjectArrayIP14btHullTriangleE", !42, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !11, i64 16}
!42 = !{!"_ZTS18btAlignedAllocatorIP14btHullTriangleLj16EE"}
!43 = !{!41, !3, i64 12}
!44 = !{!41, !10, i64 4}
!45 = !{!41, !10, i64 8}
!46 = !{!47, !11, i64 16}
!47 = !{!"_ZTS20btAlignedObjectArrayIiE", !48, i64 0, !10, i64 4, !10, i64 8, !3, i64 12, !11, i64 16}
!48 = !{!"_ZTS18btAlignedAllocatorIiLj16EE"}
!49 = !{!47, !3, i64 12}
!50 = !{!47, !10, i64 4}
!51 = !{!47, !10, i64 8}
!52 = !{i8 0, i8 2}
!53 = !{!8, !11, i64 16}
!54 = !{!8, !10, i64 8}
!55 = !{!12, !11, i64 16}
!56 = !{!12, !10, i64 8}
!57 = !{!58, !58, i64 0}
!58 = !{!"long", !4, i64 0}
