/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// The Module object: Our interface to the outside world. We import
// and export values on it. There are various ways Module can be used:
// 1. Not defined. We create it here
// 2. A function parameter, function(Module) { ..generated code.. }
// 3. pre-run appended it, var Module = {}; ..generated code..
// 4. External script tag defines var Module.
// We need to check if Module already exists (e.g. case 3 above).
// Substitution will be replaced with actual code on later stage of the build,
// this way Closure Compiler will not mangle it (e.g. case 4. above).
// Note that if you want to run closure, and also to use Module
// after the generated code, you will need to define   var Module = {};
// before the code. Then that object will be used in the code, and you
// can continue to use Module afterwards as well.
var Module = typeof Ammo !== 'undefined' ? Ammo : {};

// Include a Promise polyfill for legacy browsers. This is needed either for
// wasm2js, where we polyfill the wasm API which needs Promises, or when using
// modularize which creates a Promise for when the module is ready.

// Promise polyfill from https://github.com/taylorhakes/promise-polyfill
// License:
//==============================================================================
// Copyright (c) 2014 Taylor Hakes
// Copyright (c) 2014 Forbes Lindesay
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//==============================================================================

/** @suppress{duplicate} This is already defined in from Closure's built-in
    externs.zip//es6.js, Closure should not yell when seeing this again. */
var Promise = (function() {
  function noop() {}

  // Polyfill for Function.prototype.bind
  function bind(fn, thisArg) {
    return function() {
      fn.apply(thisArg, arguments);
    };
  }

  /**
   * @constructor
   * @param {Function} fn
   */
  function Promise(fn) {
    if (!(this instanceof Promise))
      throw new TypeError('Promises must be constructed via new');
    if (typeof fn !== 'function') throw new TypeError('not a function');
    /** @type {!number} */
    this._state = 0;
    /** @type {!boolean} */
    this._handled = false;
    /** @type {Promise|undefined} */
    this._value = undefined;
    /** @type {!Array<!Function>} */
    this._deferreds = [];

    doResolve(fn, this);
  }

  function handle(self, deferred) {
    while (self._state === 3) {
      self = self._value;
    }
    if (self._state === 0) {
      self._deferreds.push(deferred);
      return;
    }
    self._handled = true;
    Promise._immediateFn(function() {
      var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
      if (cb === null) {
        (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
        return;
      }
      var ret;
      try {
        ret = cb(self._value);
      } catch (e) {
        reject(deferred.promise, e);
        return;
      }
      resolve(deferred.promise, ret);
    });
  }

  function resolve(self, newValue) {
    try {
      // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
      if (newValue === self)
        throw new TypeError('A promise cannot be resolved with itself.');
      if (
        newValue &&
        (typeof newValue === 'object' || typeof newValue === 'function')
      ) {
        var then = newValue.then;
        if (newValue instanceof Promise) {
          self._state = 3;
          self._value = newValue;
          finale(self);
          return;
        } else if (typeof then === 'function') {
          doResolve(bind(then, newValue), self);
          return;
        }
      }
      self._state = 1;
      self._value = newValue;
      finale(self);
    } catch (e) {
      reject(self, e);
    }
  }

  function reject(self, newValue) {
    self._state = 2;
    self._value = newValue;
    finale(self);
  }

  function finale(self) {
    if (self._state === 2 && self._deferreds.length === 0) {
      Promise._immediateFn(function() {
        if (!self._handled) {
          Promise._unhandledRejectionFn(self._value);
        }
      });
    }

    for (var i = 0, len = self._deferreds.length; i < len; i++) {
      handle(self, self._deferreds[i]);
    }
    self._deferreds = null;
  }

  /**
   * @constructor
   */
  function Handler(onFulfilled, onRejected, promise) {
    this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
    this.onRejected = typeof onRejected === 'function' ? onRejected : null;
    this.promise = promise;
  }

  /**
   * Take a potentially misbehaving resolver function and make sure
   * onFulfilled and onRejected are only called once.
   *
   * Makes no guarantees about asynchrony.
   */
  function doResolve(fn, self) {
    var done = false;
    try {
      fn(
        function(value) {
          if (done) return;
          done = true;
          resolve(self, value);
        },
        function(reason) {
          if (done) return;
          done = true;
          reject(self, reason);
        }
      );
    } catch (ex) {
      if (done) return;
      done = true;
      reject(self, ex);
    }
  }

  Promise.prototype['catch'] = function(onRejected) {
    return this.then(null, onRejected);
  };

  Promise.prototype.then = function(onFulfilled, onRejected) {
    // @ts-ignore
    var prom = new this.constructor(noop);

    handle(this, new Handler(onFulfilled, onRejected, prom));
    return prom;
  };

  Promise.all = function(arr) {
    return new Promise(function(resolve, reject) {
      if (!Array.isArray(arr)) {
        return reject(new TypeError('Promise.all accepts an array'));
      }

      var args = Array.prototype.slice.call(arr);
      if (args.length === 0) return resolve([]);
      var remaining = args.length;

      function res(i, val) {
        try {
          if (val && (typeof val === 'object' || typeof val === 'function')) {
            var then = val.then;
            if (typeof then === 'function') {
              then.call(
                val,
                function(val) {
                  res(i, val);
                },
                reject
              );
              return;
            }
          }
          args[i] = val;
          if (--remaining === 0) {
            resolve(args);
          }
        } catch (ex) {
          reject(ex);
        }
      }

      for (var i = 0; i < args.length; i++) {
        res(i, args[i]);
      }
    });
  };

  Promise.resolve = function(value) {
    if (value && typeof value === 'object' && value.constructor === Promise) {
      return value;
    }

    return new Promise(function(resolve) {
      resolve(value);
    });
  };

  Promise.reject = function(value) {
    return new Promise(function(resolve, reject) {
      reject(value);
    });
  };

  Promise.race = function(arr) {
    return new Promise(function(resolve, reject) {
      if (!Array.isArray(arr)) {
        return reject(new TypeError('Promise.race accepts an array'));
      }

      for (var i = 0, len = arr.length; i < len; i++) {
        Promise.resolve(arr[i]).then(resolve, reject);
      }
    });
  };

  // Use polyfill for setImmediate for performance gains
  Promise._immediateFn =
    // @ts-ignore
    (typeof setImmediate === 'function' &&
      function(fn) {
        // @ts-ignore
        setImmediate(fn);
      }) ||
    function(fn) {
      setTimeout(fn, 0); // XXX EMSCRIPTEN: just use setTimeout
    };

  Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
    if (typeof console !== 'undefined' && console) {
      console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
    }
  };

  return Promise;
})();




// Set up the promise that indicates the Module is initialized
var readyPromiseResolve, readyPromiseReject;
Module['ready'] = new Promise(function(resolve, reject) {
  readyPromiseResolve = resolve;
  readyPromiseReject = reject;
});

// --pre-jses are emitted after the Module integration code, so that they can
// refer to Module (if they choose; they can also define Module)
// {{PRE_JSES}}

// Sometimes an existing Module object exists with properties
// meant to overwrite the default module functionality. Here
// we collect those properties and reapply _after_ we configure
// the current environment's defaults to avoid having to be so
// defensive during initialization.
var moduleOverrides = {};
var key;
for (key in Module) {
  if (Module.hasOwnProperty(key)) {
    moduleOverrides[key] = Module[key];
  }
}

var arguments_ = [];
var thisProgram = './this.program';
var quit_ = function(status, toThrow) {
  throw toThrow;
};

// Determine the runtime environment we are in. You can customize this by
// setting the ENVIRONMENT setting at compile time (see settings.js).

var ENVIRONMENT_IS_WEB = false;
var ENVIRONMENT_IS_WORKER = false;
var ENVIRONMENT_IS_NODE = false;
var ENVIRONMENT_IS_SHELL = false;
ENVIRONMENT_IS_WEB = typeof window === 'object';
ENVIRONMENT_IS_WORKER = typeof importScripts === 'function';
// N.b. Electron.js environment is simultaneously a NODE-environment, but
// also a web environment.
ENVIRONMENT_IS_NODE = typeof process === 'object' && typeof process.versions === 'object' && typeof process.versions.node === 'string';
ENVIRONMENT_IS_SHELL = !ENVIRONMENT_IS_WEB && !ENVIRONMENT_IS_NODE && !ENVIRONMENT_IS_WORKER;




// `/` should be present at the end if `scriptDirectory` is not empty
var scriptDirectory = '';
function locateFile(path) {
  if (Module['locateFile']) {
    return Module['locateFile'](path, scriptDirectory);
  }
  return scriptDirectory + path;
}

// Hooks that are implemented differently in different runtime environments.
var read_,
    readAsync,
    readBinary,
    setWindowTitle;

var nodeFS;
var nodePath;

if (ENVIRONMENT_IS_NODE) {
  if (ENVIRONMENT_IS_WORKER) {
    scriptDirectory = require('path').dirname(scriptDirectory) + '/';
  } else {
    scriptDirectory = __dirname + '/';
  }


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(filename, binary) {
    var ret = tryParseAsDataURI(filename);
    if (ret) {
      return binary ? ret : ret.toString();
    }
    if (!nodeFS) nodeFS = require('fs');
    if (!nodePath) nodePath = require('path');
    filename = nodePath['normalize'](filename);
    return nodeFS['readFileSync'](filename, binary ? null : 'utf8');
  };

  readBinary = function readBinary(filename) {
    var ret = read_(filename, true);
    if (!ret.buffer) {
      ret = new Uint8Array(ret);
    }
    assert(ret.buffer);
    return ret;
  };




  if (process['argv'].length > 1) {
    thisProgram = process['argv'][1].replace(/\\/g, '/');
  }

  arguments_ = process['argv'].slice(2);

  // MODULARIZE will export the module in the proper place outside, we don't need to export here

  process['on']('uncaughtException', function(ex) {
    // suppress ExitStatus exceptions from showing an error
    if (!(ex instanceof ExitStatus)) {
      throw ex;
    }
  });

  process['on']('unhandledRejection', abort);

  quit_ = function(status) {
    process['exit'](status);
  };

  Module['inspect'] = function () { return '[Emscripten Module object]'; };



} else
if (ENVIRONMENT_IS_SHELL) {


  if (typeof read != 'undefined') {
    read_ = function shell_read(f) {
      var data = tryParseAsDataURI(f);
      if (data) {
        return intArrayToString(data);
      }
      return read(f);
    };
  }

  readBinary = function readBinary(f) {
    var data;
    data = tryParseAsDataURI(f);
    if (data) {
      return data;
    }
    if (typeof readbuffer === 'function') {
      return new Uint8Array(readbuffer(f));
    }
    data = read(f, 'binary');
    assert(typeof data === 'object');
    return data;
  };

  if (typeof scriptArgs != 'undefined') {
    arguments_ = scriptArgs;
  } else if (typeof arguments != 'undefined') {
    arguments_ = arguments;
  }

  if (typeof quit === 'function') {
    quit_ = function(status) {
      quit(status);
    };
  }

  if (typeof print !== 'undefined') {
    // Prefer to use print/printErr where they exist, as they usually work better.
    if (typeof console === 'undefined') console = /** @type{!Console} */({});
    console.log = /** @type{!function(this:Console, ...*): undefined} */ (print);
    console.warn = console.error = /** @type{!function(this:Console, ...*): undefined} */ (typeof printErr !== 'undefined' ? printErr : print);
  }


} else

// Note that this includes Node.js workers when relevant (pthreads is enabled).
// Node.js workers are detected as a combination of ENVIRONMENT_IS_WORKER and
// ENVIRONMENT_IS_NODE.
if (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) {
  if (ENVIRONMENT_IS_WORKER) { // Check worker, not web, since window could be polyfilled
    scriptDirectory = self.location.href;
  } else if (document.currentScript) { // web
    scriptDirectory = document.currentScript.src;
  }
  // When MODULARIZE, this JS may be executed later, after document.currentScript
  // is gone, so we saved it, and we use it here instead of any other info.
  if (_scriptDir) {
    scriptDirectory = _scriptDir;
  }
  // blob urls look like blob:http://site.com/etc/etc and we cannot infer anything from them.
  // otherwise, slice off the final part of the url to find the script directory.
  // if scriptDirectory does not contain a slash, lastIndexOf will return -1,
  // and scriptDirectory will correctly be replaced with an empty string.
  if (scriptDirectory.indexOf('blob:') !== 0) {
    scriptDirectory = scriptDirectory.substr(0, scriptDirectory.lastIndexOf('/')+1);
  } else {
    scriptDirectory = '';
  }


  // Differentiate the Web Worker from the Node Worker case, as reading must
  // be done differently.
  {


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

  read_ = function shell_read(url) {
    try {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url, false);
      xhr.send(null);
      return xhr.responseText;
    } catch (err) {
      var data = tryParseAsDataURI(url);
      if (data) {
        return intArrayToString(data);
      }
      throw err;
    }
  };

  if (ENVIRONMENT_IS_WORKER) {
    readBinary = function readBinary(url) {
      try {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, false);
        xhr.responseType = 'arraybuffer';
        xhr.send(null);
        return new Uint8Array(/** @type{!ArrayBuffer} */(xhr.response));
      } catch (err) {
        var data = tryParseAsDataURI(url);
        if (data) {
          return data;
        }
        throw err;
      }
    };
  }

  readAsync = function readAsync(url, onload, onerror) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function xhr_onload() {
      if (xhr.status == 200 || (xhr.status == 0 && xhr.response)) { // file URLs can return 0
        onload(xhr.response);
        return;
      }
      var data = tryParseAsDataURI(url);
      if (data) {
        onload(data.buffer);
        return;
      }
      onerror();
    };
    xhr.onerror = onerror;
    xhr.send(null);
  };




  }

  setWindowTitle = function(title) { document.title = title };
} else
{
}


// Set up the out() and err() hooks, which are how we can print to stdout or
// stderr, respectively.
var out = Module['print'] || console.log.bind(console);
var err = Module['printErr'] || console.warn.bind(console);

// Merge back in the overrides
for (key in moduleOverrides) {
  if (moduleOverrides.hasOwnProperty(key)) {
    Module[key] = moduleOverrides[key];
  }
}
// Free the object hierarchy contained in the overrides, this lets the GC
// reclaim data used e.g. in memoryInitializerRequest, which is a large typed array.
moduleOverrides = null;

// Emit code to handle expected values on the Module object. This applies Module.x
// to the proper local x. This has two benefits: first, we only emit it if it is
// expected to arrive, and second, by using a local everywhere else that can be
// minified.
if (Module['arguments']) arguments_ = Module['arguments'];
if (Module['thisProgram']) thisProgram = Module['thisProgram'];
if (Module['quit']) quit_ = Module['quit'];

// perform assertions in shell.js after we set up out() and err(), as otherwise if an assertion fails it cannot print the message



/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// {{PREAMBLE_ADDITIONS}}

var STACK_ALIGN = 16;

function dynamicAlloc(size) {
  var ret = HEAP32[DYNAMICTOP_PTR>>2];
  var end = (ret + size + 15) & -16;
  HEAP32[DYNAMICTOP_PTR>>2] = end;
  return ret;
}

function alignMemory(size, factor) {
  if (!factor) factor = STACK_ALIGN; // stack alignment (16-byte) by default
  return Math.ceil(size / factor) * factor;
}

function getNativeTypeSize(type) {
  switch (type) {
    case 'i1': case 'i8': return 1;
    case 'i16': return 2;
    case 'i32': return 4;
    case 'i64': return 8;
    case 'float': return 4;
    case 'double': return 8;
    default: {
      if (type[type.length-1] === '*') {
        return 4; // A pointer
      } else if (type[0] === 'i') {
        var bits = Number(type.substr(1));
        assert(bits % 8 === 0, 'getNativeTypeSize invalid bits ' + bits + ', type ' + type);
        return bits / 8;
      } else {
        return 0;
      }
    }
  }
}

function warnOnce(text) {
  if (!warnOnce.shown) warnOnce.shown = {};
  if (!warnOnce.shown[text]) {
    warnOnce.shown[text] = 1;
    err(text);
  }
}





/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */


// Wraps a JS function as a wasm function with a given signature.
function convertJsFunctionToWasm(func, sig) {
  return func;
}

var freeTableIndexes = [];

// Weak map of functions in the table to their indexes, created on first use.
var functionsInTableMap;

// Add a wasm function to the table.
function addFunctionWasm(func, sig) {
  var table = wasmTable;

  // Check if the function is already in the table, to ensure each function
  // gets a unique index. First, create the map if this is the first use.
  if (!functionsInTableMap) {
    functionsInTableMap = new WeakMap();
    for (var i = 0; i < table.length; i++) {
      var item = table.get(i);
      // Ignore null values.
      if (item) {
        functionsInTableMap.set(item, i);
      }
    }
  }
  if (functionsInTableMap.has(func)) {
    return functionsInTableMap.get(func);
  }

  // It's not in the table, add it now.


  var ret;
  // Reuse a free index if there is one, otherwise grow.
  if (freeTableIndexes.length) {
    ret = freeTableIndexes.pop();
  } else {
    ret = table.length;
    // Grow the table
    try {
      table.grow(1);
    } catch (err) {
      if (!(err instanceof RangeError)) {
        throw err;
      }
      throw 'Unable to grow wasm table. Set ALLOW_TABLE_GROWTH.';
    }
  }

  // Set the new value.
  try {
    // Attempting to call this with JS function will cause of table.set() to fail
    table.set(ret, func);
  } catch (err) {
    if (!(err instanceof TypeError)) {
      throw err;
    }
    var wrapped = convertJsFunctionToWasm(func, sig);
    table.set(ret, wrapped);
  }

  functionsInTableMap.set(func, ret);

  return ret;
}

function removeFunctionWasm(index) {
  functionsInTableMap.delete(wasmTable.get(index));
  freeTableIndexes.push(index);
}

// 'sig' parameter is required for the llvm backend but only when func is not
// already a WebAssembly function.
function addFunction(func, sig) {

  return addFunctionWasm(func, sig);
}

function removeFunction(index) {
  removeFunctionWasm(index);
}



var funcWrappers = {};

function getFuncWrapper(func, sig) {
  if (!func) return; // on null pointer, return undefined
  assert(sig);
  if (!funcWrappers[sig]) {
    funcWrappers[sig] = {};
  }
  var sigCache = funcWrappers[sig];
  if (!sigCache[func]) {
    // optimize away arguments usage in common cases
    if (sig.length === 1) {
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func);
      };
    } else if (sig.length === 2) {
      sigCache[func] = function dynCall_wrapper(arg) {
        return dynCall(sig, func, [arg]);
      };
    } else {
      // general case
      sigCache[func] = function dynCall_wrapper() {
        return dynCall(sig, func, Array.prototype.slice.call(arguments));
      };
    }
  }
  return sigCache[func];
}


/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function makeBigInt(low, high, unsigned) {
  return unsigned ? ((+((low>>>0)))+((+((high>>>0)))*4294967296.0)) : ((+((low>>>0)))+((+((high|0)))*4294967296.0));
}

/** @param {Array=} args */
function dynCall(sig, ptr, args) {
  if (args && args.length) {
    return Module['dynCall_' + sig].apply(null, [ptr].concat(args));
  } else {
    return Module['dynCall_' + sig].call(null, ptr);
  }
}

var tempRet0 = 0;

var setTempRet0 = function(value) {
  tempRet0 = value;
};

var getTempRet0 = function() {
  return tempRet0;
};


// The address globals begin at. Very low in memory, for code size and optimization opportunities.
// Above 0 is static memory, starting with globals.
// Then the stack.
// Then 'dynamic' memory for sbrk.
var GLOBAL_BASE = 1024;



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Preamble library stuff ===

// Documentation for the public APIs defined in this file must be updated in:
//    site/source/docs/api_reference/preamble.js.rst
// A prebuilt local version of the documentation is available at:
//    site/build/text/docs/api_reference/preamble.js.txt
// You can also build docs locally as HTML or other formats in site/
// An online HTML version (which may be of a different version of Emscripten)
//    is up at http://kripken.github.io/emscripten-site/docs/api_reference/preamble.js.html


var wasmBinary;if (Module['wasmBinary']) wasmBinary = Module['wasmBinary'];
var noExitRuntime;if (Module['noExitRuntime']) noExitRuntime = Module['noExitRuntime'];


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// wasm2js.js - enough of a polyfill for the WebAssembly object so that we can load
// wasm2js code that way.

// Emit "var WebAssembly" if definitely using wasm2js. Otherwise, in MAYBE_WASM2JS
// mode, we can't use a "var" since it would prevent normal wasm from working.
/** @suppress{const} */
var
WebAssembly = {
  Memory: /** @constructor */ function(opts) {
    return {
      buffer: new ArrayBuffer(opts['initial'] * 65536),
      grow: function(amount) {
        var ret = __growWasmMemory(amount);
        return ret;
      }
    };
  },

  Table: function(opts) {
    var ret = new Array(opts['initial']);
    ret.grow = function(by) {
      if (ret.length >= 930 + 0) {
        abort('Unable to grow wasm table. Use a higher value for RESERVED_FUNCTION_POINTERS or set ALLOW_TABLE_GROWTH.')
      }
      ret.push(null);
    };
    ret.set = function(i, func) {
      ret[i] = func;
    };
    ret.get = function(i) {
      return ret[i];
    };
    return ret;
  },

  Module: function(binary) {
    // TODO: use the binary and info somehow - right now the wasm2js output is embedded in
    // the main JS
    return {};
  },

  Instance: function(module, info) {
    // TODO: use the module and info somehow - right now the wasm2js output is embedded in
    // the main JS
    // This will be replaced by the actual wasm2js code.
    var exports = Module['__wasm2jsInstantiate__'](asmLibraryArg, wasmMemory, wasmTable);
    return {
      'exports': exports
    };
  },

  instantiate: /** @suppress{checkTypes} */ function(binary, info) {
    return {
      then: function(ok) {
        ok({
          'instance': new WebAssembly.Instance(new WebAssembly.Module(binary))
        });
      }
    };
  },

  RuntimeError: Error
};

// We don't need to actually download a wasm binary, mark it as present but empty.
wasmBinary = [];



if (typeof WebAssembly !== 'object') {
  err('no native wasm support detected');
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// In MINIMAL_RUNTIME, setValue() and getValue() are only available when building with safe heap enabled, for heap safety checking.
// In traditional runtime, setValue() and getValue() are always available (although their use is highly discouraged due to perf penalties)

/** @param {number} ptr
    @param {number} value
    @param {string} type
    @param {number|boolean=} noSafe */
function setValue(ptr, value, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': HEAP8[((ptr)>>0)]=value; break;
      case 'i8': HEAP8[((ptr)>>0)]=value; break;
      case 'i16': HEAP16[((ptr)>>1)]=value; break;
      case 'i32': HEAP32[((ptr)>>2)]=value; break;
      case 'i64': (tempI64 = [value>>>0,(tempDouble=value,(+(Math_abs(tempDouble))) >= 1.0 ? (tempDouble > 0.0 ? ((Math_min((+(Math_floor((tempDouble)/4294967296.0))), 4294967295.0))|0)>>>0 : (~~((+(Math_ceil((tempDouble - +(((~~(tempDouble)))>>>0))/4294967296.0)))))>>>0) : 0)],HEAP32[((ptr)>>2)]=tempI64[0],HEAP32[(((ptr)+(4))>>2)]=tempI64[1]); break;
      case 'float': HEAPF32[((ptr)>>2)]=value; break;
      case 'double': HEAPF64[((ptr)>>3)]=value; break;
      default: abort('invalid type for setValue: ' + type);
    }
}

/** @param {number} ptr
    @param {string} type
    @param {number|boolean=} noSafe */
function getValue(ptr, type, noSafe) {
  type = type || 'i8';
  if (type.charAt(type.length-1) === '*') type = 'i32'; // pointers are 32-bit
    switch(type) {
      case 'i1': return HEAP8[((ptr)>>0)];
      case 'i8': return HEAP8[((ptr)>>0)];
      case 'i16': return HEAP16[((ptr)>>1)];
      case 'i32': return HEAP32[((ptr)>>2)];
      case 'i64': return HEAP32[((ptr)>>2)];
      case 'float': return HEAPF32[((ptr)>>2)];
      case 'double': return HEAPF64[((ptr)>>3)];
      default: abort('invalid type for getValue: ' + type);
    }
  return null;
}






// Wasm globals

var wasmMemory;

// In fastcomp asm.js, we don't need a wasm Table at all.
// In the wasm backend, we polyfill the WebAssembly object,
// so this creates a (non-native-wasm) table for us.
var wasmTable = new WebAssembly.Table({
  'initial': 930,
  'maximum': 930 + 0,
  'element': 'anyfunc'
});


//========================================
// Runtime essentials
//========================================

// whether we are quitting the application. no code should run after this.
// set in exit() and abort()
var ABORT = false;

// set by exit() and abort().  Passed to 'onExit' handler.
// NOTE: This is also used as the process return code code in shell environments
// but only when noExitRuntime is false.
var EXITSTATUS = 0;

/** @type {function(*, string=)} */
function assert(condition, text) {
  if (!condition) {
    abort('Assertion failed: ' + text);
  }
}

// Returns the C function with a specified identifier (for C++, you need to do manual name mangling)
function getCFunc(ident) {
  var func = Module['_' + ident]; // closure exported function
  assert(func, 'Cannot call unknown function ' + ident + ', make sure it is exported');
  return func;
}

// C calling interface.
/** @param {string|null=} returnType
    @param {Array=} argTypes
    @param {Arguments|Array=} args
    @param {Object=} opts */
function ccall(ident, returnType, argTypes, args, opts) {
  // For fast lookup of conversion functions
  var toC = {
    'string': function(str) {
      var ret = 0;
      if (str !== null && str !== undefined && str !== 0) { // null string
        // at most 4 bytes per UTF-8 code point, +1 for the trailing '\0'
        var len = (str.length << 2) + 1;
        ret = stackAlloc(len);
        stringToUTF8(str, ret, len);
      }
      return ret;
    },
    'array': function(arr) {
      var ret = stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    }
  };

  function convertReturnValue(ret) {
    if (returnType === 'string') return UTF8ToString(ret);
    if (returnType === 'boolean') return Boolean(ret);
    return ret;
  }

  var func = getCFunc(ident);
  var cArgs = [];
  var stack = 0;
  if (args) {
    for (var i = 0; i < args.length; i++) {
      var converter = toC[argTypes[i]];
      if (converter) {
        if (stack === 0) stack = stackSave();
        cArgs[i] = converter(args[i]);
      } else {
        cArgs[i] = args[i];
      }
    }
  }
  var ret = func.apply(null, cArgs);

  ret = convertReturnValue(ret);
  if (stack !== 0) stackRestore(stack);
  return ret;
}

/** @param {string=} returnType
    @param {Array=} argTypes
    @param {Object=} opts */
function cwrap(ident, returnType, argTypes, opts) {
  argTypes = argTypes || [];
  // When the function takes numbers and returns a number, we can just return
  // the original function
  var numericArgs = argTypes.every(function(type){ return type === 'number'});
  var numericRet = returnType !== 'string';
  if (numericRet && numericArgs && !opts) {
    return getCFunc(ident);
  }
  return function() {
    return ccall(ident, returnType, argTypes, arguments, opts);
  }
}

var ALLOC_NORMAL = 0; // Tries to use _malloc()
var ALLOC_STACK = 1; // Lives for the duration of the current function call
var ALLOC_DYNAMIC = 2; // Cannot be freed except through sbrk
var ALLOC_NONE = 3; // Do not allocate

// allocate(): This is for internal use. You can use it yourself as well, but the interface
//             is a little tricky (see docs right below). The reason is that it is optimized
//             for multiple syntaxes to save space in generated code. So you should
//             normally not use allocate(), and instead allocate memory using _malloc(),
//             initialize it with setValue(), and so forth.
// @slab: An array of data, or a number. If a number, then the size of the block to allocate,
//        in *bytes* (note that this is sometimes confusing: the next parameter does not
//        affect this!)
// @types: Either an array of types, one for each byte (or 0 if no type at that position),
//         or a single type which is used for the entire block. This only matters if there
//         is initial data - if @slab is a number, then this does not matter at all and is
//         ignored.
// @allocator: How to allocate memory, see ALLOC_*
/** @type {function((TypedArray|Array<number>|number), string, number, number=)} */
function allocate(slab, types, allocator, ptr) {
  var zeroinit, size;
  if (typeof slab === 'number') {
    zeroinit = true;
    size = slab;
  } else {
    zeroinit = false;
    size = slab.length;
  }

  var singleType = typeof types === 'string' ? types : null;

  var ret;
  if (allocator == ALLOC_NONE) {
    ret = ptr;
  } else {
    ret = [_malloc,
    stackAlloc,
    dynamicAlloc][allocator](Math.max(size, singleType ? 1 : types.length));
  }

  if (zeroinit) {
    var stop;
    ptr = ret;
    assert((ret & 3) == 0);
    stop = ret + (size & ~3);
    for (; ptr < stop; ptr += 4) {
      HEAP32[((ptr)>>2)]=0;
    }
    stop = ret + size;
    while (ptr < stop) {
      HEAP8[((ptr++)>>0)]=0;
    }
    return ret;
  }

  if (singleType === 'i8') {
    if (slab.subarray || slab.slice) {
      HEAPU8.set(/** @type {!Uint8Array} */ (slab), ret);
    } else {
      HEAPU8.set(new Uint8Array(slab), ret);
    }
    return ret;
  }

  var i = 0, type, typeSize, previousType;
  while (i < size) {
    var curr = slab[i];

    type = singleType || types[i];
    if (type === 0) {
      i++;
      continue;
    }

    if (type == 'i64') type = 'i32'; // special case: we have one i32 here, and one i32 later

    setValue(ret+i, curr, type);

    // no need to look up size unless type changes, so cache it
    if (previousType !== type) {
      typeSize = getNativeTypeSize(type);
      previousType = type;
    }
    i += typeSize;
  }

  return ret;
}

// Allocate memory during any stage of startup - static memory early on, dynamic memory later, malloc when ready
function getMemory(size) {
  if (!runtimeInitialized) return dynamicAlloc(size);
  return _malloc(size);
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings.js: Strings related runtime functions that are part of both MINIMAL_RUNTIME and regular runtime.

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the given array that contains uint8 values, returns
// a copy of that string as a Javascript String object.

var UTF8Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf8') : undefined;

/**
 * @param {number} idx
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ArrayToString(heap, idx, maxBytesToRead) {
  var endIdx = idx + maxBytesToRead;
  var endPtr = idx;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  // (As a tiny code save trick, compare endPtr against endIdx using a negation, so that undefined means Infinity)
  while (heap[endPtr] && !(endPtr >= endIdx)) ++endPtr;

  if (endPtr - idx > 16 && heap.subarray && UTF8Decoder) {
    return UTF8Decoder.decode(heap.subarray(idx, endPtr));
  } else {
    var str = '';
    // If building with TextDecoder, we have already computed the string length above, so test loop end condition against that
    while (idx < endPtr) {
      // For UTF8 byte structure, see:
      // http://en.wikipedia.org/wiki/UTF-8#Description
      // https://www.ietf.org/rfc/rfc2279.txt
      // https://tools.ietf.org/html/rfc3629
      var u0 = heap[idx++];
      if (!(u0 & 0x80)) { str += String.fromCharCode(u0); continue; }
      var u1 = heap[idx++] & 63;
      if ((u0 & 0xE0) == 0xC0) { str += String.fromCharCode(((u0 & 31) << 6) | u1); continue; }
      var u2 = heap[idx++] & 63;
      if ((u0 & 0xF0) == 0xE0) {
        u0 = ((u0 & 15) << 12) | (u1 << 6) | u2;
      } else {
        u0 = ((u0 & 7) << 18) | (u1 << 12) | (u2 << 6) | (heap[idx++] & 63);
      }

      if (u0 < 0x10000) {
        str += String.fromCharCode(u0);
      } else {
        var ch = u0 - 0x10000;
        str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
      }
    }
  }
  return str;
}

// Given a pointer 'ptr' to a null-terminated UTF8-encoded string in the emscripten HEAP, returns a
// copy of that string as a Javascript String object.
// maxBytesToRead: an optional length that specifies the maximum number of bytes to read. You can omit
//                 this parameter to scan the string until the first \0 byte. If maxBytesToRead is
//                 passed, and the string at [ptr, ptr+maxBytesToReadr[ contains a null byte in the
//                 middle, then the string will cut short at that byte index (i.e. maxBytesToRead will
//                 not produce a string of exact length [ptr, ptr+maxBytesToRead[)
//                 N.B. mixing frequent uses of UTF8ToString() with and without maxBytesToRead may
//                 throw JS JIT optimizations off, so it is worth to consider consistently using one
//                 style or the other.
/**
 * @param {number} ptr
 * @param {number=} maxBytesToRead
 * @return {string}
 */
function UTF8ToString(ptr, maxBytesToRead) {
  return ptr ? UTF8ArrayToString(HEAPU8, ptr, maxBytesToRead) : '';
}

// Copies the given Javascript String object 'str' to the given byte array at address 'outIdx',
// encoded in UTF8 form and null-terminated. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   heap: the array to copy to. Each index in this array is assumed to be one 8-byte element.
//   outIdx: The starting offset in the array to begin the copying.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array.
//                    This count should include the null terminator,
//                    i.e. if maxBytesToWrite=1, only the null terminator will be written and nothing else.
//                    maxBytesToWrite=0 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8Array(str, heap, outIdx, maxBytesToWrite) {
  if (!(maxBytesToWrite > 0)) // Parameter maxBytesToWrite is not optional. Negative values, 0, null, undefined and false each don't write out any bytes.
    return 0;

  var startIdx = outIdx;
  var endIdx = outIdx + maxBytesToWrite - 1; // -1 for string null terminator.
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    // For UTF8 byte structure, see http://en.wikipedia.org/wiki/UTF-8#Description and https://www.ietf.org/rfc/rfc2279.txt and https://tools.ietf.org/html/rfc3629
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) {
      var u1 = str.charCodeAt(++i);
      u = 0x10000 + ((u & 0x3FF) << 10) | (u1 & 0x3FF);
    }
    if (u <= 0x7F) {
      if (outIdx >= endIdx) break;
      heap[outIdx++] = u;
    } else if (u <= 0x7FF) {
      if (outIdx + 1 >= endIdx) break;
      heap[outIdx++] = 0xC0 | (u >> 6);
      heap[outIdx++] = 0x80 | (u & 63);
    } else if (u <= 0xFFFF) {
      if (outIdx + 2 >= endIdx) break;
      heap[outIdx++] = 0xE0 | (u >> 12);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    } else {
      if (outIdx + 3 >= endIdx) break;
      heap[outIdx++] = 0xF0 | (u >> 18);
      heap[outIdx++] = 0x80 | ((u >> 12) & 63);
      heap[outIdx++] = 0x80 | ((u >> 6) & 63);
      heap[outIdx++] = 0x80 | (u & 63);
    }
  }
  // Null-terminate the pointer to the buffer.
  heap[outIdx] = 0;
  return outIdx - startIdx;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF8 form. The copy will require at most str.length*4+1 bytes of space in the HEAP.
// Use the function lengthBytesUTF8 to compute the exact number of bytes (excluding null terminator) that this function will write.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF8(str, outPtr, maxBytesToWrite) {
  return stringToUTF8Array(str, HEAPU8,outPtr, maxBytesToWrite);
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF8 byte array, EXCLUDING the null terminator byte.
function lengthBytesUTF8(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! So decode UTF16->UTF32->UTF8.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var u = str.charCodeAt(i); // possibly a lead surrogate
    if (u >= 0xD800 && u <= 0xDFFF) u = 0x10000 + ((u & 0x3FF) << 10) | (str.charCodeAt(++i) & 0x3FF);
    if (u <= 0x7F) ++len;
    else if (u <= 0x7FF) len += 2;
    else if (u <= 0xFFFF) len += 3;
    else len += 4;
  }
  return len;
}



/**
 * @license
 * Copyright 2020 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// runtime_strings_extra.js: Strings related runtime functions that are available only in regular runtime.

// Given a pointer 'ptr' to a null-terminated ASCII-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

function AsciiToString(ptr) {
  var str = '';
  while (1) {
    var ch = HEAPU8[((ptr++)>>0)];
    if (!ch) return str;
    str += String.fromCharCode(ch);
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in ASCII form. The copy will require at most str.length+1 bytes of space in the HEAP.

function stringToAscii(str, outPtr) {
  return writeAsciiToMemory(str, outPtr, false);
}

// Given a pointer 'ptr' to a null-terminated UTF16LE-encoded string in the emscripten HEAP, returns
// a copy of that string as a Javascript String object.

var UTF16Decoder = typeof TextDecoder !== 'undefined' ? new TextDecoder('utf-16le') : undefined;

function UTF16ToString(ptr, maxBytesToRead) {
  var endPtr = ptr;
  // TextDecoder needs to know the byte length in advance, it doesn't stop on null terminator by itself.
  // Also, use the length info to avoid running tiny strings through TextDecoder, since .subarray() allocates garbage.
  var idx = endPtr >> 1;
  var maxIdx = idx + maxBytesToRead / 2;
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(idx >= maxIdx) && HEAPU16[idx]) ++idx;
  endPtr = idx << 1;

  if (endPtr - ptr > 32 && UTF16Decoder) {
    return UTF16Decoder.decode(HEAPU8.subarray(ptr, endPtr));
  } else {
    var i = 0;

    var str = '';
    while (1) {
      var codeUnit = HEAP16[(((ptr)+(i*2))>>1)];
      if (codeUnit == 0 || i == maxBytesToRead / 2) return str;
      ++i;
      // fromCharCode constructs a character from a UTF-16 code unit, so we can pass the UTF16 string right through.
      str += String.fromCharCode(codeUnit);
    }
  }
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF16 form. The copy will require at most str.length*4+2 bytes of space in the HEAP.
// Use the function lengthBytesUTF16() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=2, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<2 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF16(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 2) return 0;
  maxBytesToWrite -= 2; // Null terminator.
  var startPtr = outPtr;
  var numCharsToWrite = (maxBytesToWrite < str.length*2) ? (maxBytesToWrite / 2) : str.length;
  for (var i = 0; i < numCharsToWrite; ++i) {
    // charCodeAt returns a UTF-16 encoded code unit, so it can be directly written to the HEAP.
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    HEAP16[((outPtr)>>1)]=codeUnit;
    outPtr += 2;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP16[((outPtr)>>1)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF16(str) {
  return str.length*2;
}

function UTF32ToString(ptr, maxBytesToRead) {
  var i = 0;

  var str = '';
  // If maxBytesToRead is not passed explicitly, it will be undefined, and this
  // will always evaluate to true. This saves on code size.
  while (!(i >= maxBytesToRead / 4)) {
    var utf32 = HEAP32[(((ptr)+(i*4))>>2)];
    if (utf32 == 0) break;
    ++i;
    // Gotcha: fromCharCode constructs a character from a UTF-16 encoded code (pair), not from a Unicode code point! So encode the code point to UTF-16 for constructing.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    if (utf32 >= 0x10000) {
      var ch = utf32 - 0x10000;
      str += String.fromCharCode(0xD800 | (ch >> 10), 0xDC00 | (ch & 0x3FF));
    } else {
      str += String.fromCharCode(utf32);
    }
  }
  return str;
}

// Copies the given Javascript String object 'str' to the emscripten HEAP at address 'outPtr',
// null-terminated and encoded in UTF32 form. The copy will require at most str.length*4+4 bytes of space in the HEAP.
// Use the function lengthBytesUTF32() to compute the exact number of bytes (excluding null terminator) that this function will write.
// Parameters:
//   str: the Javascript string to copy.
//   outPtr: Byte address in Emscripten HEAP where to write the string to.
//   maxBytesToWrite: The maximum number of bytes this function can write to the array. This count should include the null
//                    terminator, i.e. if maxBytesToWrite=4, only the null terminator will be written and nothing else.
//                    maxBytesToWrite<4 does not write any bytes to the output, not even the null terminator.
// Returns the number of bytes written, EXCLUDING the null terminator.

function stringToUTF32(str, outPtr, maxBytesToWrite) {
  // Backwards compatibility: if max bytes is not specified, assume unsafe unbounded write is allowed.
  if (maxBytesToWrite === undefined) {
    maxBytesToWrite = 0x7FFFFFFF;
  }
  if (maxBytesToWrite < 4) return 0;
  var startPtr = outPtr;
  var endPtr = startPtr + maxBytesToWrite - 4;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i); // possibly a lead surrogate
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) {
      var trailSurrogate = str.charCodeAt(++i);
      codeUnit = 0x10000 + ((codeUnit & 0x3FF) << 10) | (trailSurrogate & 0x3FF);
    }
    HEAP32[((outPtr)>>2)]=codeUnit;
    outPtr += 4;
    if (outPtr + 4 > endPtr) break;
  }
  // Null-terminate the pointer to the HEAP.
  HEAP32[((outPtr)>>2)]=0;
  return outPtr - startPtr;
}

// Returns the number of bytes the given Javascript string takes if encoded as a UTF16 byte array, EXCLUDING the null terminator byte.

function lengthBytesUTF32(str) {
  var len = 0;
  for (var i = 0; i < str.length; ++i) {
    // Gotcha: charCodeAt returns a 16-bit word that is a UTF-16 encoded code unit, not a Unicode code point of the character! We must decode the string to UTF-32 to the heap.
    // See http://unicode.org/faq/utf_bom.html#utf16-3
    var codeUnit = str.charCodeAt(i);
    if (codeUnit >= 0xD800 && codeUnit <= 0xDFFF) ++i; // possibly a lead surrogate, so skip over the tail surrogate.
    len += 4;
  }

  return len;
}

// Allocate heap space for a JS string, and write it there.
// It is the responsibility of the caller to free() that memory.
function allocateUTF8(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = _malloc(size);
  if (ret) stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Allocate stack space for a JS string, and write it there.
function allocateUTF8OnStack(str) {
  var size = lengthBytesUTF8(str) + 1;
  var ret = stackAlloc(size);
  stringToUTF8Array(str, HEAP8, ret, size);
  return ret;
}

// Deprecated: This function should not be called because it is unsafe and does not provide
// a maximum length limit of how many bytes it is allowed to write. Prefer calling the
// function stringToUTF8Array() instead, which takes in a maximum length that can be used
// to be secure from out of bounds writes.
/** @deprecated
    @param {boolean=} dontAddNull */
function writeStringToMemory(string, buffer, dontAddNull) {
  warnOnce('writeStringToMemory is deprecated and should not be called! Use stringToUTF8() instead!');

  var /** @type {number} */ lastChar, /** @type {number} */ end;
  if (dontAddNull) {
    // stringToUTF8Array always appends null. If we don't want to do that, remember the
    // character that existed at the location where the null will be placed, and restore
    // that after the write (below).
    end = buffer + lengthBytesUTF8(string);
    lastChar = HEAP8[end];
  }
  stringToUTF8(string, buffer, Infinity);
  if (dontAddNull) HEAP8[end] = lastChar; // Restore the value under the null character.
}

function writeArrayToMemory(array, buffer) {
  HEAP8.set(array, buffer);
}

/** @param {boolean=} dontAddNull */
function writeAsciiToMemory(str, buffer, dontAddNull) {
  for (var i = 0; i < str.length; ++i) {
    HEAP8[((buffer++)>>0)]=str.charCodeAt(i);
  }
  // Null-terminate the pointer to the HEAP.
  if (!dontAddNull) HEAP8[((buffer)>>0)]=0;
}



// Memory management

var PAGE_SIZE = 16384;
var WASM_PAGE_SIZE = 65536;
var ASMJS_PAGE_SIZE = 16777216;

function alignUp(x, multiple) {
  if (x % multiple > 0) {
    x += multiple - (x % multiple);
  }
  return x;
}

var HEAP,
/** @type {ArrayBuffer} */
  buffer,
/** @type {Int8Array} */
  HEAP8,
/** @type {Uint8Array} */
  HEAPU8,
/** @type {Int16Array} */
  HEAP16,
/** @type {Uint16Array} */
  HEAPU16,
/** @type {Int32Array} */
  HEAP32,
/** @type {Uint32Array} */
  HEAPU32,
/** @type {Float32Array} */
  HEAPF32,
/** @type {Float64Array} */
  HEAPF64;

function updateGlobalBufferAndViews(buf) {
  buffer = buf;
  Module['HEAP8'] = HEAP8 = new Int8Array(buf);
  Module['HEAP16'] = HEAP16 = new Int16Array(buf);
  Module['HEAP32'] = HEAP32 = new Int32Array(buf);
  Module['HEAPU8'] = HEAPU8 = new Uint8Array(buf);
  Module['HEAPU16'] = HEAPU16 = new Uint16Array(buf);
  Module['HEAPU32'] = HEAPU32 = new Uint32Array(buf);
  Module['HEAPF32'] = HEAPF32 = new Float32Array(buf);
  Module['HEAPF64'] = HEAPF64 = new Float64Array(buf);
}

var STATIC_BASE = 1024,
    STACK_BASE = 5274448,
    STACKTOP = STACK_BASE,
    STACK_MAX = 31568,
    DYNAMIC_BASE = 5274448,
    DYNAMICTOP_PTR = 31408;



var TOTAL_STACK = 5242880;

var INITIAL_INITIAL_MEMORY = Module['INITIAL_MEMORY'] || 67108864;




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




// In non-standalone/normal mode, we create the memory here.

/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// Create the main memory. (Note: this isn't used in STANDALONE_WASM mode since the wasm
// memory is created in the wasm, not in JS.)

  if (Module['wasmMemory']) {
    wasmMemory = Module['wasmMemory'];
  } else
  {
    wasmMemory = new WebAssembly.Memory({
      'initial': INITIAL_INITIAL_MEMORY / WASM_PAGE_SIZE
      ,
      'maximum': INITIAL_INITIAL_MEMORY / WASM_PAGE_SIZE
    });
  }


if (wasmMemory) {
  buffer = wasmMemory.buffer;
}

// If the user provides an incorrect length, just use that length instead rather than providing the user to
// specifically provide the memory length with Module['INITIAL_MEMORY'].
INITIAL_INITIAL_MEMORY = buffer.byteLength;
updateGlobalBufferAndViews(buffer);

HEAP32[DYNAMICTOP_PTR>>2] = DYNAMIC_BASE;




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */




function callRuntimeCallbacks(callbacks) {
  while(callbacks.length > 0) {
    var callback = callbacks.shift();
    if (typeof callback == 'function') {
      callback(Module); // Pass the module as the first argument.
      continue;
    }
    var func = callback.func;
    if (typeof func === 'number') {
      if (callback.arg === undefined) {
        Module['dynCall_v'](func);
      } else {
        Module['dynCall_vi'](func, callback.arg);
      }
    } else {
      func(callback.arg === undefined ? null : callback.arg);
    }
  }
}

var __ATPRERUN__  = []; // functions called before the runtime is initialized
var __ATINIT__    = []; // functions called during startup
var __ATMAIN__    = []; // functions called when main() is to be run
var __ATEXIT__    = []; // functions called during shutdown
var __ATPOSTRUN__ = []; // functions called after the main() is called

var runtimeInitialized = false;
var runtimeExited = false;


function preRun() {

  if (Module['preRun']) {
    if (typeof Module['preRun'] == 'function') Module['preRun'] = [Module['preRun']];
    while (Module['preRun'].length) {
      addOnPreRun(Module['preRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPRERUN__);
}

function initRuntime() {
  runtimeInitialized = true;
  
  callRuntimeCallbacks(__ATINIT__);
}

function preMain() {
  
  callRuntimeCallbacks(__ATMAIN__);
}

function exitRuntime() {
  runtimeExited = true;
}

function postRun() {

  if (Module['postRun']) {
    if (typeof Module['postRun'] == 'function') Module['postRun'] = [Module['postRun']];
    while (Module['postRun'].length) {
      addOnPostRun(Module['postRun'].shift());
    }
  }

  callRuntimeCallbacks(__ATPOSTRUN__);
}

function addOnPreRun(cb) {
  __ATPRERUN__.unshift(cb);
}

function addOnInit(cb) {
  __ATINIT__.unshift(cb);
}

function addOnPreMain(cb) {
  __ATMAIN__.unshift(cb);
}

function addOnExit(cb) {
}

function addOnPostRun(cb) {
  __ATPOSTRUN__.unshift(cb);
}

/** @param {number|boolean=} ignore */
function unSign(value, bits, ignore) {
  if (value >= 0) {
    return value;
  }
  return bits <= 32 ? 2*Math.abs(1 << (bits-1)) + value // Need some trickery, since if bits == 32, we are right at the limit of the bits JS uses in bitshifts
                    : Math.pow(2, bits)         + value;
}
/** @param {number|boolean=} ignore */
function reSign(value, bits, ignore) {
  if (value <= 0) {
    return value;
  }
  var half = bits <= 32 ? Math.abs(1 << (bits-1)) // abs is needed if bits == 32
                        : Math.pow(2, bits-1);
  if (value >= half && (bits <= 32 || value > half)) { // for huge values, we can hit the precision limit and always get true here. so don't do that
                                                       // but, in general there is no perfect solution here. With 64-bit ints, we get rounding and errors
                                                       // TODO: In i64 mode 1, resign the two parts separately and safely
    value = -2*half + value; // Cannot bitshift half, as it may be at the limit of the bits JS uses in bitshifts
  }
  return value;
}


/**
 * @license
 * Copyright 2019 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/imul
// || MIN_NODE_VERSION < 0.12
// check for imul support, and also for correctness ( https://bugs.webkit.org/show_bug.cgi?id=126345 )
if (!Math.imul || Math.imul(0xffffffff, 5) !== -5) Math.imul = function imul(a, b) {
  var ah  = a >>> 16;
  var al = a & 0xffff;
  var bh  = b >>> 16;
  var bl = b & 0xffff;
  return (al*bl + ((ah*bl + al*bh) << 16))|0;
};

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/fround
if (!Math.fround) {
  var froundBuffer = new Float32Array(1);
  Math.fround = function(x) { froundBuffer[0] = x; return froundBuffer[0] };
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/clz32
if (!Math.clz32) Math.clz32 = function(x) {
  var n = 32;
  var y = x >> 16; if (y) { n -= 16; x = y; }
  y = x >> 8; if (y) { n -= 8; x = y; }
  y = x >> 4; if (y) { n -= 4; x = y; }
  y = x >> 2; if (y) { n -= 2; x = y; }
  y = x >> 1; if (y) return n - 2;
  return n - x;
};

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/trunc
if (!Math.trunc) Math.trunc = function(x) {
  return x < 0 ? Math.ceil(x) : Math.floor(x);
};


var Math_abs = Math.abs;
var Math_cos = Math.cos;
var Math_sin = Math.sin;
var Math_tan = Math.tan;
var Math_acos = Math.acos;
var Math_asin = Math.asin;
var Math_atan = Math.atan;
var Math_atan2 = Math.atan2;
var Math_exp = Math.exp;
var Math_log = Math.log;
var Math_sqrt = Math.sqrt;
var Math_ceil = Math.ceil;
var Math_floor = Math.floor;
var Math_pow = Math.pow;
var Math_imul = Math.imul;
var Math_fround = Math.fround;
var Math_round = Math.round;
var Math_min = Math.min;
var Math_max = Math.max;
var Math_clz32 = Math.clz32;
var Math_trunc = Math.trunc;



// A counter of dependencies for calling run(). If we need to
// do asynchronous work before running, increment this and
// decrement it. Incrementing must happen in a place like
// Module.preRun (used by emcc to add file preloading).
// Note that you can add dependencies in preRun, even though
// it happens right before run - run will be postponed until
// the dependencies are met.
var runDependencies = 0;
var runDependencyWatcher = null;
var dependenciesFulfilled = null; // overridden to take different actions when all run dependencies are fulfilled

function getUniqueRunDependency(id) {
  return id;
}

function addRunDependency(id) {
  runDependencies++;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

}

function removeRunDependency(id) {
  runDependencies--;

  if (Module['monitorRunDependencies']) {
    Module['monitorRunDependencies'](runDependencies);
  }

  if (runDependencies == 0) {
    if (runDependencyWatcher !== null) {
      clearInterval(runDependencyWatcher);
      runDependencyWatcher = null;
    }
    if (dependenciesFulfilled) {
      var callback = dependenciesFulfilled;
      dependenciesFulfilled = null;
      callback(); // can add another dependenciesFulfilled
    }
  }
}

Module["preloadedImages"] = {}; // maps url to image data
Module["preloadedAudios"] = {}; // maps url to audio data

/** @param {string|number=} what */
function abort(what) {
  if (Module['onAbort']) {
    Module['onAbort'](what);
  }

  what += '';
  out(what);
  err(what);

  ABORT = true;
  EXITSTATUS = 1;

  what = 'abort(' + what + '). Build with -s ASSERTIONS=1 for more info.';

  // Throw a wasm runtime error, because a JS error might be seen as a foreign
  // exception, which means we'd run destructors on it. We need the error to
  // simply make the program stop.
  throw new WebAssembly.RuntimeError(what);
}


var memoryInitializer = null;


/**
 * @license
 * Copyright 2015 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */







/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

function hasPrefix(str, prefix) {
  return String.prototype.startsWith ?
      str.startsWith(prefix) :
      str.indexOf(prefix) === 0;
}

// Prefix of data URIs emitted by SINGLE_FILE and related options.
var dataURIPrefix = 'data:application/octet-stream;base64,';

// Indicates whether filename is a base64 data URI.
function isDataURI(filename) {
  return hasPrefix(filename, dataURIPrefix);
}

var fileURIPrefix = "file://";

// Indicates whether filename is delivered via file protocol (as opposed to http/https)
function isFileURI(filename) {
  return hasPrefix(filename, fileURIPrefix);
}




var wasmBinaryFile = '{{{ FILENAME_REPLACEMENT_STRINGS_WASM_BINARY_FILE }}}';
if (!isDataURI(wasmBinaryFile)) {
  wasmBinaryFile = locateFile(wasmBinaryFile);
}

function getBinary() {
  try {
    if (wasmBinary) {
      return new Uint8Array(wasmBinary);
    }

    var binary = tryParseAsDataURI(wasmBinaryFile);
    if (binary) {
      return binary;
    }
    if (readBinary) {
      return readBinary(wasmBinaryFile);
    } else {
      throw "both async and sync fetching of the wasm failed";
    }
  }
  catch (err) {
    abort(err);
  }
}

function getBinaryPromise() {
  // If we don't have the binary yet, and have the Fetch api, use that;
  // in some environments, like Electron's render process, Fetch api may be present, but have a different context than expected, let's only use it on the Web
  if (!wasmBinary && (ENVIRONMENT_IS_WEB || ENVIRONMENT_IS_WORKER) && typeof fetch === 'function'
      // Let's not use fetch to get objects over file:// as it's most likely Cordova which doesn't support fetch for file://
      && !isFileURI(wasmBinaryFile)
      ) {
    return fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function(response) {
      if (!response['ok']) {
        throw "failed to load wasm binary file at '" + wasmBinaryFile + "'";
      }
      return response['arrayBuffer']();
    }).catch(function () {
      return getBinary();
    });
  }
  // Otherwise, getBinary should be able to get it synchronously
  return new Promise(function(resolve, reject) {
    resolve(getBinary());
  });
}



// Create the wasm instance.
// Receives the wasm imports, returns the exports.
function createWasm() {
  // prepare imports
  var info = {
    'env': asmLibraryArg,
    'wasi_snapshot_preview1': asmLibraryArg
  };
  // Load the wasm module and create an instance of using native support in the JS engine.
  // handle a generated wasm instance, receiving its exports and
  // performing other necessary setup
  /** @param {WebAssembly.Module=} module*/
  function receiveInstance(instance, module) {
    var exports = instance.exports;
    Module['asm'] = exports;
    removeRunDependency('wasm-instantiate');
  }
  // we can't run yet (except in a pthread, where we have a custom sync instantiator)
  addRunDependency('wasm-instantiate');


  function receiveInstantiatedSource(output) {
    // 'output' is a WebAssemblyInstantiatedSource object which has both the module and instance.
    // receiveInstance() will swap in the exports (to Module.asm) so they can be called
    // TODO: Due to Closure regression https://github.com/google/closure-compiler/issues/3193, the above line no longer optimizes out down to the following line.
    // When the regression is fixed, can restore the above USE_PTHREADS-enabled path.
    receiveInstance(output['instance']);
  }


  function instantiateArrayBuffer(receiver) {
    return getBinaryPromise().then(function(binary) {
      return WebAssembly.instantiate(binary, info);
    }).then(receiver, function(reason) {
      err('failed to asynchronously prepare wasm: ' + reason);
      abort(reason);
    });
  }

  // Prefer streaming instantiation if available.
  function instantiateAsync() {
    if (!wasmBinary &&
        typeof WebAssembly.instantiateStreaming === 'function' &&
        !isDataURI(wasmBinaryFile) &&
        // Don't use streaming for file:// delivered objects in a webview, fetch them synchronously.
        !isFileURI(wasmBinaryFile) &&
        typeof fetch === 'function') {
      fetch(wasmBinaryFile, { credentials: 'same-origin' }).then(function (response) {
        var result = WebAssembly.instantiateStreaming(response, info);
        return result.then(receiveInstantiatedSource, function(reason) {
            // We expect the most common failure cause to be a bad MIME type for the binary,
            // in which case falling back to ArrayBuffer instantiation should work.
            err('wasm streaming compile failed: ' + reason);
            err('falling back to ArrayBuffer instantiation');
            return instantiateArrayBuffer(receiveInstantiatedSource);
          });
      });
    } else {
      return instantiateArrayBuffer(receiveInstantiatedSource);
    }
  }
  // User shell pages can write their own Module.instantiateWasm = function(imports, successCallback) callback
  // to manually instantiate the Wasm module themselves. This allows pages to run the instantiation parallel
  // to any other async startup actions they are performing.
  if (Module['instantiateWasm']) {
    try {
      var exports = Module['instantiateWasm'](info, receiveInstance);
      return exports;
    } catch(e) {
      err('Module.instantiateWasm callback failed with error: ' + e);
      return false;
    }
  }

  instantiateAsync();
  return {}; // no exports yet; we'll fill them in later
}


// Globals used by JS i64 conversions
var tempDouble;
var tempI64;

// === Body ===

var ASM_CONSTS = {
  1960: function($0, $1, $2, $3, $4, $5, $6, $7) {var self = Module['getCache'](Module['ConcreteContactResultCallback'])[$0]; if (!self.hasOwnProperty('addSingleResult')) throw 'a JSImplementation must implement all functions, you forgot ConcreteContactResultCallback::addSingleResult.'; return self['addSingleResult']($1,$2,$3,$4,$5,$6,$7);},  
 2520: function($0, $1, $2, $3) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('drawLine')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::drawLine.'; self['drawLine']($1,$2,$3);},  
 2745: function($0, $1, $2, $3, $4, $5) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('drawContactPoint')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::drawContactPoint.'; self['drawContactPoint']($1,$2,$3,$4,$5);},  
 3002: function($0, $1) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('reportErrorWarning')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::reportErrorWarning.'; self['reportErrorWarning']($1);},  
 3249: function($0, $1, $2) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('draw3dText')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::draw3dText.'; self['draw3dText']($1,$2);},  
 3476: function($0, $1) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('setDebugMode')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::setDebugMode.'; self['setDebugMode']($1);},  
 3705: function($0) {var self = Module['getCache'](Module['DebugDrawer'])[$0]; if (!self.hasOwnProperty('getDebugMode')) throw 'a JSImplementation must implement all functions, you forgot DebugDrawer::getDebugMode.'; return self['getDebugMode']();}
};

function _emscripten_asm_const_dii(code, sigPtr, argbuf) {
  var args = readAsmConstArgs(sigPtr, argbuf);
  return ASM_CONSTS[code].apply(null, args);
}

function _emscripten_asm_const_iii(code, sigPtr, argbuf) {
  var args = readAsmConstArgs(sigPtr, argbuf);
  return ASM_CONSTS[code].apply(null, args);
}function array_bounds_check_error(idx,size){ throw 'Array index ' + idx + ' out of bounds: [0,' + size + ')'; }



// STATICTOP = STATIC_BASE + 30544;
/* global initializers */  __ATINIT__.push({ func: function() { ___wasm_call_ctors() } });




/* no memory initializer */
// {{PRE_LIBRARY}}


  function demangle(func) {
      return func;
    }

  function demangleAll(text) {
      var regex =
        /\b_Z[\w\d_]+/g;
      return text.replace(regex,
        function(x) {
          var y = demangle(x);
          return x === y ? x : (y + ' [' + x + ']');
        });
    }

  function jsStackTrace() {
      var err = new Error();
      if (!err.stack) {
        // IE10+ special cases: It does have callstack info, but it is only populated if an Error object is thrown,
        // so try that as a special-case.
        try {
          throw new Error();
        } catch(e) {
          err = e;
        }
        if (!err.stack) {
          return '(no stack trace available)';
        }
      }
      return err.stack.toString();
    }

  function stackTrace() {
      var js = jsStackTrace();
      if (Module['extraStackTrace']) js += '\n' + Module['extraStackTrace']();
      return demangleAll(js);
    }

  
  function _atexit(func, arg) {
      __ATEXIT__.unshift({ func: func, arg: arg });
    }function ___cxa_atexit(a0,a1
  ) {
  return _atexit(a0,a1);
  }

  function _abort() {
      abort();
    }

  function _emscripten_get_sbrk_ptr() {
      return 31408;
    }

  var _emscripten_memcpy_big= Uint8Array.prototype.copyWithin
    ? function(dest, src, num) { HEAPU8.copyWithin(dest, src, src + num); }
    : function(dest, src, num) { HEAPU8.set(HEAPU8.subarray(src, src+num), dest); }
  ;

  
  function _emscripten_get_heap_size() {
      return HEAPU8.length;
    }
  
  function abortOnCannotGrowMemory(requestedSize) {
      abort('OOM');
    }function _emscripten_resize_heap(requestedSize) {
      requestedSize = requestedSize >>> 0;
      abortOnCannotGrowMemory(requestedSize);
    }

  function _gettimeofday(ptr) {
      var now = Date.now();
      HEAP32[((ptr)>>2)]=(now/1000)|0; // seconds
      HEAP32[(((ptr)+(4))>>2)]=((now % 1000)*1000)|0; // microseconds
      return 0;
    }

  
  var __readAsmConstArgsArray=[];function readAsmConstArgs(sigPtr, buf) {
      __readAsmConstArgsArray.length = 0;
      var ch;
      buf >>= 2; // Align buf up front to index Int32Array (HEAP32)
      while (ch = HEAPU8[sigPtr++]) {
        __readAsmConstArgsArray.push(ch < 105 ? HEAPF64[++buf >> 1] : HEAP32[buf]);
        ++buf;
      }
      return __readAsmConstArgsArray;
    }
var ASSERTIONS = false;

/**
 * @license
 * Copyright 2017 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

/** @type {function(string, boolean=, number=)} */
function intArrayFromString(stringy, dontAddNull, length) {
  var len = length > 0 ? length : lengthBytesUTF8(stringy)+1;
  var u8array = new Array(len);
  var numBytesWritten = stringToUTF8Array(stringy, u8array, 0, u8array.length);
  if (dontAddNull) u8array.length = numBytesWritten;
  return u8array;
}

function intArrayToString(array) {
  var ret = [];
  for (var i = 0; i < array.length; i++) {
    var chr = array[i];
    if (chr > 0xFF) {
      if (ASSERTIONS) {
        assert(false, 'Character code ' + chr + ' (' + String.fromCharCode(chr) + ')  at offset ' + i + ' not in 0x00-0xFF.');
      }
      chr &= 0xFF;
    }
    ret.push(String.fromCharCode(chr));
  }
  return ret.join('');
}


// Copied from https://github.com/strophe/strophejs/blob/e06d027/src/polyfills.js#L149

// This code was written by Tyler Akins and has been placed in the
// public domain.  It would be nice if you left this header intact.
// Base64 code from Tyler Akins -- http://rumkin.com

/**
 * Decodes a base64 string.
 * @param {string} input The string to decode.
 */
var decodeBase64 = typeof atob === 'function' ? atob : function (input) {
  var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

  var output = '';
  var chr1, chr2, chr3;
  var enc1, enc2, enc3, enc4;
  var i = 0;
  // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
  input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');
  do {
    enc1 = keyStr.indexOf(input.charAt(i++));
    enc2 = keyStr.indexOf(input.charAt(i++));
    enc3 = keyStr.indexOf(input.charAt(i++));
    enc4 = keyStr.indexOf(input.charAt(i++));

    chr1 = (enc1 << 2) | (enc2 >> 4);
    chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
    chr3 = ((enc3 & 3) << 6) | enc4;

    output = output + String.fromCharCode(chr1);

    if (enc3 !== 64) {
      output = output + String.fromCharCode(chr2);
    }
    if (enc4 !== 64) {
      output = output + String.fromCharCode(chr3);
    }
  } while (i < input.length);
  return output;
};

// Converts a string of base64 into a byte array.
// Throws error on invalid input.
function intArrayFromBase64(s) {
  if (typeof ENVIRONMENT_IS_NODE === 'boolean' && ENVIRONMENT_IS_NODE) {
    var buf;
    try {
      // TODO: Update Node.js externs, Closure does not recognize the following Buffer.from()
      /**@suppress{checkTypes}*/
      buf = Buffer.from(s, 'base64');
    } catch (_) {
      buf = new Buffer(s, 'base64');
    }
    return new Uint8Array(buf['buffer'], buf['byteOffset'], buf['byteLength']);
  }

  try {
    var decoded = decodeBase64(s);
    var bytes = new Uint8Array(decoded.length);
    for (var i = 0 ; i < decoded.length ; ++i) {
      bytes[i] = decoded.charCodeAt(i);
    }
    return bytes;
  } catch (_) {
    throw new Error('Converting base64 string to bytes failed.');
  }
}

// If filename is a base64 data URI, parses and returns data (Buffer on node,
// Uint8Array otherwise). If filename is not a base64 data URI, returns undefined.
function tryParseAsDataURI(filename) {
  if (!isDataURI(filename)) {
    return;
  }

  return intArrayFromBase64(filename.slice(dataURIPrefix.length));
}


var asmGlobalArg = {};
var asmLibraryArg = { "__cxa_atexit": ___cxa_atexit, "abort": _abort, "array_bounds_check_error": array_bounds_check_error, "emscripten_asm_const_dii": _emscripten_asm_const_dii, "emscripten_asm_const_iii": _emscripten_asm_const_iii, "emscripten_get_sbrk_ptr": _emscripten_get_sbrk_ptr, "emscripten_memcpy_big": _emscripten_memcpy_big, "emscripten_resize_heap": _emscripten_resize_heap, "getTempRet0": getTempRet0, "gettimeofday": _gettimeofday, "memory": wasmMemory, "setTempRet0": setTempRet0, "table": wasmTable };
var asm = createWasm();
/** @type {function(...*):?} */
var ___wasm_call_ctors = Module["___wasm_call_ctors"] = function() {
  return (___wasm_call_ctors = Module["___wasm_call_ctors"] = Module["asm"]["__wasm_call_ctors"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var ___em_js__array_bounds_check_error = Module["___em_js__array_bounds_check_error"] = function() {
  return (___em_js__array_bounds_check_error = Module["___em_js__array_bounds_check_error"] = Module["asm"]["__em_js__array_bounds_check_error"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_getDispatcher_0 = Module["_emscripten_bind_btCollisionWorld_getDispatcher_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_getDispatcher_0 = Module["_emscripten_bind_btCollisionWorld_getDispatcher_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_getDispatcher_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_rayTest_3 = Module["_emscripten_bind_btCollisionWorld_rayTest_3"] = function() {
  return (_emscripten_bind_btCollisionWorld_rayTest_3 = Module["_emscripten_bind_btCollisionWorld_rayTest_3"] = Module["asm"]["emscripten_bind_btCollisionWorld_rayTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_getPairCache_0 = Module["_emscripten_bind_btCollisionWorld_getPairCache_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_getPairCache_0 = Module["_emscripten_bind_btCollisionWorld_getPairCache_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_getPairCache_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_getDispatchInfo_0 = Module["_emscripten_bind_btCollisionWorld_getDispatchInfo_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_getDispatchInfo_0 = Module["_emscripten_bind_btCollisionWorld_getDispatchInfo_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_getDispatchInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_addCollisionObject_1 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_1"] = function() {
  return (_emscripten_bind_btCollisionWorld_addCollisionObject_1 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_1"] = Module["asm"]["emscripten_bind_btCollisionWorld_addCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_addCollisionObject_2 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_2"] = function() {
  return (_emscripten_bind_btCollisionWorld_addCollisionObject_2 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_2"] = Module["asm"]["emscripten_bind_btCollisionWorld_addCollisionObject_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_addCollisionObject_3 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_3"] = function() {
  return (_emscripten_bind_btCollisionWorld_addCollisionObject_3 = Module["_emscripten_bind_btCollisionWorld_addCollisionObject_3"] = Module["asm"]["emscripten_bind_btCollisionWorld_addCollisionObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_removeCollisionObject_1 = Module["_emscripten_bind_btCollisionWorld_removeCollisionObject_1"] = function() {
  return (_emscripten_bind_btCollisionWorld_removeCollisionObject_1 = Module["_emscripten_bind_btCollisionWorld_removeCollisionObject_1"] = Module["asm"]["emscripten_bind_btCollisionWorld_removeCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_getBroadphase_0 = Module["_emscripten_bind_btCollisionWorld_getBroadphase_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_getBroadphase_0 = Module["_emscripten_bind_btCollisionWorld_getBroadphase_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_getBroadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_convexSweepTest_5 = Module["_emscripten_bind_btCollisionWorld_convexSweepTest_5"] = function() {
  return (_emscripten_bind_btCollisionWorld_convexSweepTest_5 = Module["_emscripten_bind_btCollisionWorld_convexSweepTest_5"] = Module["asm"]["emscripten_bind_btCollisionWorld_convexSweepTest_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_contactPairTest_3 = Module["_emscripten_bind_btCollisionWorld_contactPairTest_3"] = function() {
  return (_emscripten_bind_btCollisionWorld_contactPairTest_3 = Module["_emscripten_bind_btCollisionWorld_contactPairTest_3"] = Module["asm"]["emscripten_bind_btCollisionWorld_contactPairTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_contactTest_2 = Module["_emscripten_bind_btCollisionWorld_contactTest_2"] = function() {
  return (_emscripten_bind_btCollisionWorld_contactTest_2 = Module["_emscripten_bind_btCollisionWorld_contactTest_2"] = Module["asm"]["emscripten_bind_btCollisionWorld_contactTest_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_updateSingleAabb_1 = Module["_emscripten_bind_btCollisionWorld_updateSingleAabb_1"] = function() {
  return (_emscripten_bind_btCollisionWorld_updateSingleAabb_1 = Module["_emscripten_bind_btCollisionWorld_updateSingleAabb_1"] = Module["asm"]["emscripten_bind_btCollisionWorld_updateSingleAabb_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_setDebugDrawer_1 = Module["_emscripten_bind_btCollisionWorld_setDebugDrawer_1"] = function() {
  return (_emscripten_bind_btCollisionWorld_setDebugDrawer_1 = Module["_emscripten_bind_btCollisionWorld_setDebugDrawer_1"] = Module["asm"]["emscripten_bind_btCollisionWorld_setDebugDrawer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_getDebugDrawer_0 = Module["_emscripten_bind_btCollisionWorld_getDebugDrawer_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_getDebugDrawer_0 = Module["_emscripten_bind_btCollisionWorld_getDebugDrawer_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_getDebugDrawer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_debugDrawWorld_0 = Module["_emscripten_bind_btCollisionWorld_debugDrawWorld_0"] = function() {
  return (_emscripten_bind_btCollisionWorld_debugDrawWorld_0 = Module["_emscripten_bind_btCollisionWorld_debugDrawWorld_0"] = Module["asm"]["emscripten_bind_btCollisionWorld_debugDrawWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld_debugDrawObject_3 = Module["_emscripten_bind_btCollisionWorld_debugDrawObject_3"] = function() {
  return (_emscripten_bind_btCollisionWorld_debugDrawObject_3 = Module["_emscripten_bind_btCollisionWorld_debugDrawObject_3"] = Module["asm"]["emscripten_bind_btCollisionWorld_debugDrawObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionWorld___destroy___0 = Module["_emscripten_bind_btCollisionWorld___destroy___0"] = function() {
  return (_emscripten_bind_btCollisionWorld___destroy___0 = Module["_emscripten_bind_btCollisionWorld___destroy___0"] = Module["asm"]["emscripten_bind_btCollisionWorld___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape_setLocalScaling_1 = Module["_emscripten_bind_btCollisionShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCollisionShape_setLocalScaling_1 = Module["_emscripten_bind_btCollisionShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCollisionShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape_getLocalScaling_0 = Module["_emscripten_bind_btCollisionShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCollisionShape_getLocalScaling_0 = Module["_emscripten_bind_btCollisionShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCollisionShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCollisionShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCollisionShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCollisionShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCollisionShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape_setMargin_1 = Module["_emscripten_bind_btCollisionShape_setMargin_1"] = function() {
  return (_emscripten_bind_btCollisionShape_setMargin_1 = Module["_emscripten_bind_btCollisionShape_setMargin_1"] = Module["asm"]["emscripten_bind_btCollisionShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape_getMargin_0 = Module["_emscripten_bind_btCollisionShape_getMargin_0"] = function() {
  return (_emscripten_bind_btCollisionShape_getMargin_0 = Module["_emscripten_bind_btCollisionShape_getMargin_0"] = Module["asm"]["emscripten_bind_btCollisionShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionShape___destroy___0 = Module["_emscripten_bind_btCollisionShape___destroy___0"] = function() {
  return (_emscripten_bind_btCollisionShape___destroy___0 = Module["_emscripten_bind_btCollisionShape___destroy___0"] = Module["asm"]["emscripten_bind_btCollisionShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btCollisionObject_setAnisotropicFriction_2"] = function() {
  return (_emscripten_bind_btCollisionObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btCollisionObject_setAnisotropicFriction_2"] = Module["asm"]["emscripten_bind_btCollisionObject_setAnisotropicFriction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getCollisionShape_0 = Module["_emscripten_bind_btCollisionObject_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getCollisionShape_0 = Module["_emscripten_bind_btCollisionObject_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btCollisionObject_setContactProcessingThreshold_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btCollisionObject_setContactProcessingThreshold_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setContactProcessingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setActivationState_1 = Module["_emscripten_bind_btCollisionObject_setActivationState_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setActivationState_1 = Module["_emscripten_bind_btCollisionObject_setActivationState_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_forceActivationState_1 = Module["_emscripten_bind_btCollisionObject_forceActivationState_1"] = function() {
  return (_emscripten_bind_btCollisionObject_forceActivationState_1 = Module["_emscripten_bind_btCollisionObject_forceActivationState_1"] = Module["asm"]["emscripten_bind_btCollisionObject_forceActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_activate_0 = Module["_emscripten_bind_btCollisionObject_activate_0"] = function() {
  return (_emscripten_bind_btCollisionObject_activate_0 = Module["_emscripten_bind_btCollisionObject_activate_0"] = Module["asm"]["emscripten_bind_btCollisionObject_activate_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_activate_1 = Module["_emscripten_bind_btCollisionObject_activate_1"] = function() {
  return (_emscripten_bind_btCollisionObject_activate_1 = Module["_emscripten_bind_btCollisionObject_activate_1"] = Module["asm"]["emscripten_bind_btCollisionObject_activate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_isActive_0 = Module["_emscripten_bind_btCollisionObject_isActive_0"] = function() {
  return (_emscripten_bind_btCollisionObject_isActive_0 = Module["_emscripten_bind_btCollisionObject_isActive_0"] = Module["asm"]["emscripten_bind_btCollisionObject_isActive_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_isKinematicObject_0 = Module["_emscripten_bind_btCollisionObject_isKinematicObject_0"] = function() {
  return (_emscripten_bind_btCollisionObject_isKinematicObject_0 = Module["_emscripten_bind_btCollisionObject_isKinematicObject_0"] = Module["asm"]["emscripten_bind_btCollisionObject_isKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_isStaticObject_0 = Module["_emscripten_bind_btCollisionObject_isStaticObject_0"] = function() {
  return (_emscripten_bind_btCollisionObject_isStaticObject_0 = Module["_emscripten_bind_btCollisionObject_isStaticObject_0"] = Module["asm"]["emscripten_bind_btCollisionObject_isStaticObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btCollisionObject_isStaticOrKinematicObject_0"] = function() {
  return (_emscripten_bind_btCollisionObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btCollisionObject_isStaticOrKinematicObject_0"] = Module["asm"]["emscripten_bind_btCollisionObject_isStaticOrKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getRestitution_0 = Module["_emscripten_bind_btCollisionObject_getRestitution_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getRestitution_0 = Module["_emscripten_bind_btCollisionObject_getRestitution_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getRestitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getFriction_0 = Module["_emscripten_bind_btCollisionObject_getFriction_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getFriction_0 = Module["_emscripten_bind_btCollisionObject_getFriction_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getRollingFriction_0 = Module["_emscripten_bind_btCollisionObject_getRollingFriction_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getRollingFriction_0 = Module["_emscripten_bind_btCollisionObject_getRollingFriction_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getRollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setRestitution_1 = Module["_emscripten_bind_btCollisionObject_setRestitution_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setRestitution_1 = Module["_emscripten_bind_btCollisionObject_setRestitution_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setRestitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setFriction_1 = Module["_emscripten_bind_btCollisionObject_setFriction_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setFriction_1 = Module["_emscripten_bind_btCollisionObject_setFriction_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setRollingFriction_1 = Module["_emscripten_bind_btCollisionObject_setRollingFriction_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setRollingFriction_1 = Module["_emscripten_bind_btCollisionObject_setRollingFriction_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setRollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getWorldTransform_0 = Module["_emscripten_bind_btCollisionObject_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getWorldTransform_0 = Module["_emscripten_bind_btCollisionObject_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getCollisionFlags_0 = Module["_emscripten_bind_btCollisionObject_getCollisionFlags_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getCollisionFlags_0 = Module["_emscripten_bind_btCollisionObject_getCollisionFlags_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getCollisionFlags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setCollisionFlags_1 = Module["_emscripten_bind_btCollisionObject_setCollisionFlags_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setCollisionFlags_1 = Module["_emscripten_bind_btCollisionObject_setCollisionFlags_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setCollisionFlags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setWorldTransform_1 = Module["_emscripten_bind_btCollisionObject_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setWorldTransform_1 = Module["_emscripten_bind_btCollisionObject_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setCollisionShape_1 = Module["_emscripten_bind_btCollisionObject_setCollisionShape_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setCollisionShape_1 = Module["_emscripten_bind_btCollisionObject_setCollisionShape_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setCollisionShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btCollisionObject_setCcdMotionThreshold_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btCollisionObject_setCcdMotionThreshold_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setCcdMotionThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btCollisionObject_setCcdSweptSphereRadius_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btCollisionObject_setCcdSweptSphereRadius_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setCcdSweptSphereRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getUserIndex_0 = Module["_emscripten_bind_btCollisionObject_getUserIndex_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getUserIndex_0 = Module["_emscripten_bind_btCollisionObject_getUserIndex_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getUserIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setUserIndex_1 = Module["_emscripten_bind_btCollisionObject_setUserIndex_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setUserIndex_1 = Module["_emscripten_bind_btCollisionObject_setUserIndex_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setUserIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getUserPointer_0 = Module["_emscripten_bind_btCollisionObject_getUserPointer_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getUserPointer_0 = Module["_emscripten_bind_btCollisionObject_getUserPointer_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getUserPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_setUserPointer_1 = Module["_emscripten_bind_btCollisionObject_setUserPointer_1"] = function() {
  return (_emscripten_bind_btCollisionObject_setUserPointer_1 = Module["_emscripten_bind_btCollisionObject_setUserPointer_1"] = Module["asm"]["emscripten_bind_btCollisionObject_setUserPointer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btCollisionObject_getBroadphaseHandle_0"] = function() {
  return (_emscripten_bind_btCollisionObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btCollisionObject_getBroadphaseHandle_0"] = Module["asm"]["emscripten_bind_btCollisionObject_getBroadphaseHandle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObject___destroy___0 = Module["_emscripten_bind_btCollisionObject___destroy___0"] = function() {
  return (_emscripten_bind_btCollisionObject___destroy___0 = Module["_emscripten_bind_btCollisionObject___destroy___0"] = Module["asm"]["emscripten_bind_btCollisionObject___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_addAction_1 = Module["_emscripten_bind_btDynamicsWorld_addAction_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_addAction_1 = Module["_emscripten_bind_btDynamicsWorld_addAction_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_addAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btDynamicsWorld_removeAction_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btDynamicsWorld_removeAction_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_removeAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btDynamicsWorld_getSolverInfo_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btDynamicsWorld_getSolverInfo_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getSolverInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_setInternalTickCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_2"] = function() {
  return (_emscripten_bind_btDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_2"] = Module["asm"]["emscripten_bind_btDynamicsWorld_setInternalTickCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_3"] = function() {
  return (_emscripten_bind_btDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btDynamicsWorld_setInternalTickCallback_3"] = Module["asm"]["emscripten_bind_btDynamicsWorld_setInternalTickCallback_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btDynamicsWorld_getDispatcher_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btDynamicsWorld_getDispatcher_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getDispatcher_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btDynamicsWorld_rayTest_3"] = function() {
  return (_emscripten_bind_btDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btDynamicsWorld_rayTest_3"] = Module["asm"]["emscripten_bind_btDynamicsWorld_rayTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btDynamicsWorld_getPairCache_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btDynamicsWorld_getPairCache_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getPairCache_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btDynamicsWorld_getDispatchInfo_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btDynamicsWorld_getDispatchInfo_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getDispatchInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_addCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_2"] = function() {
  return (_emscripten_bind_btDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_2"] = Module["asm"]["emscripten_bind_btDynamicsWorld_addCollisionObject_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_3"] = function() {
  return (_emscripten_bind_btDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btDynamicsWorld_addCollisionObject_3"] = Module["asm"]["emscripten_bind_btDynamicsWorld_addCollisionObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btDynamicsWorld_removeCollisionObject_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btDynamicsWorld_removeCollisionObject_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_removeCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btDynamicsWorld_getBroadphase_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btDynamicsWorld_getBroadphase_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getBroadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btDynamicsWorld_convexSweepTest_5"] = function() {
  return (_emscripten_bind_btDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btDynamicsWorld_convexSweepTest_5"] = Module["asm"]["emscripten_bind_btDynamicsWorld_convexSweepTest_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btDynamicsWorld_contactPairTest_3"] = function() {
  return (_emscripten_bind_btDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btDynamicsWorld_contactPairTest_3"] = Module["asm"]["emscripten_bind_btDynamicsWorld_contactPairTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btDynamicsWorld_contactTest_2"] = function() {
  return (_emscripten_bind_btDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btDynamicsWorld_contactTest_2"] = Module["asm"]["emscripten_bind_btDynamicsWorld_contactTest_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btDynamicsWorld_updateSingleAabb_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btDynamicsWorld_updateSingleAabb_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_updateSingleAabb_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btDynamicsWorld_setDebugDrawer_1"] = function() {
  return (_emscripten_bind_btDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btDynamicsWorld_setDebugDrawer_1"] = Module["asm"]["emscripten_bind_btDynamicsWorld_setDebugDrawer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btDynamicsWorld_getDebugDrawer_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btDynamicsWorld_getDebugDrawer_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_getDebugDrawer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btDynamicsWorld_debugDrawWorld_0"] = function() {
  return (_emscripten_bind_btDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btDynamicsWorld_debugDrawWorld_0"] = Module["asm"]["emscripten_bind_btDynamicsWorld_debugDrawWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btDynamicsWorld_debugDrawObject_3"] = function() {
  return (_emscripten_bind_btDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btDynamicsWorld_debugDrawObject_3"] = Module["asm"]["emscripten_bind_btDynamicsWorld_debugDrawObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDynamicsWorld___destroy___0 = Module["_emscripten_bind_btDynamicsWorld___destroy___0"] = function() {
  return (_emscripten_bind_btDynamicsWorld___destroy___0 = Module["_emscripten_bind_btDynamicsWorld___destroy___0"] = Module["asm"]["emscripten_bind_btDynamicsWorld___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint_enableFeedback_1 = Module["_emscripten_bind_btTypedConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btTypedConstraint_enableFeedback_1 = Module["_emscripten_bind_btTypedConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btTypedConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btTypedConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btTypedConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btTypedConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btTypedConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btTypedConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btTypedConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btTypedConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btTypedConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint_getParam_2 = Module["_emscripten_bind_btTypedConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btTypedConstraint_getParam_2 = Module["_emscripten_bind_btTypedConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btTypedConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint_setParam_3 = Module["_emscripten_bind_btTypedConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btTypedConstraint_setParam_3 = Module["_emscripten_bind_btTypedConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btTypedConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTypedConstraint___destroy___0 = Module["_emscripten_bind_btTypedConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btTypedConstraint___destroy___0 = Module["_emscripten_bind_btTypedConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btTypedConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConcaveShape_setLocalScaling_1 = Module["_emscripten_bind_btConcaveShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConcaveShape_setLocalScaling_1 = Module["_emscripten_bind_btConcaveShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConcaveShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConcaveShape_getLocalScaling_0 = Module["_emscripten_bind_btConcaveShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConcaveShape_getLocalScaling_0 = Module["_emscripten_bind_btConcaveShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConcaveShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConcaveShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConcaveShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConcaveShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConcaveShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConcaveShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConcaveShape___destroy___0 = Module["_emscripten_bind_btConcaveShape___destroy___0"] = function() {
  return (_emscripten_bind_btConcaveShape___destroy___0 = Module["_emscripten_bind_btConcaveShape___destroy___0"] = Module["asm"]["emscripten_bind_btConcaveShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_btCapsuleShape_2 = Module["_emscripten_bind_btCapsuleShape_btCapsuleShape_2"] = function() {
  return (_emscripten_bind_btCapsuleShape_btCapsuleShape_2 = Module["_emscripten_bind_btCapsuleShape_btCapsuleShape_2"] = Module["asm"]["emscripten_bind_btCapsuleShape_btCapsuleShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_setMargin_1 = Module["_emscripten_bind_btCapsuleShape_setMargin_1"] = function() {
  return (_emscripten_bind_btCapsuleShape_setMargin_1 = Module["_emscripten_bind_btCapsuleShape_setMargin_1"] = Module["asm"]["emscripten_bind_btCapsuleShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_getMargin_0 = Module["_emscripten_bind_btCapsuleShape_getMargin_0"] = function() {
  return (_emscripten_bind_btCapsuleShape_getMargin_0 = Module["_emscripten_bind_btCapsuleShape_getMargin_0"] = Module["asm"]["emscripten_bind_btCapsuleShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShape_getUpAxis_0"] = function() {
  return (_emscripten_bind_btCapsuleShape_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShape_getUpAxis_0"] = Module["asm"]["emscripten_bind_btCapsuleShape_getUpAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_getRadius_0 = Module["_emscripten_bind_btCapsuleShape_getRadius_0"] = function() {
  return (_emscripten_bind_btCapsuleShape_getRadius_0 = Module["_emscripten_bind_btCapsuleShape_getRadius_0"] = Module["asm"]["emscripten_bind_btCapsuleShape_getRadius_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShape_getHalfHeight_0"] = function() {
  return (_emscripten_bind_btCapsuleShape_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShape_getHalfHeight_0"] = Module["asm"]["emscripten_bind_btCapsuleShape_getHalfHeight_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCapsuleShape_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCapsuleShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCapsuleShape_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCapsuleShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCapsuleShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCapsuleShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShape___destroy___0 = Module["_emscripten_bind_btCapsuleShape___destroy___0"] = function() {
  return (_emscripten_bind_btCapsuleShape___destroy___0 = Module["_emscripten_bind_btCapsuleShape___destroy___0"] = Module["asm"]["emscripten_bind_btCapsuleShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_drawLine_3 = Module["_emscripten_bind_btIDebugDraw_drawLine_3"] = function() {
  return (_emscripten_bind_btIDebugDraw_drawLine_3 = Module["_emscripten_bind_btIDebugDraw_drawLine_3"] = Module["asm"]["emscripten_bind_btIDebugDraw_drawLine_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_drawContactPoint_5 = Module["_emscripten_bind_btIDebugDraw_drawContactPoint_5"] = function() {
  return (_emscripten_bind_btIDebugDraw_drawContactPoint_5 = Module["_emscripten_bind_btIDebugDraw_drawContactPoint_5"] = Module["asm"]["emscripten_bind_btIDebugDraw_drawContactPoint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_reportErrorWarning_1 = Module["_emscripten_bind_btIDebugDraw_reportErrorWarning_1"] = function() {
  return (_emscripten_bind_btIDebugDraw_reportErrorWarning_1 = Module["_emscripten_bind_btIDebugDraw_reportErrorWarning_1"] = Module["asm"]["emscripten_bind_btIDebugDraw_reportErrorWarning_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_draw3dText_2 = Module["_emscripten_bind_btIDebugDraw_draw3dText_2"] = function() {
  return (_emscripten_bind_btIDebugDraw_draw3dText_2 = Module["_emscripten_bind_btIDebugDraw_draw3dText_2"] = Module["asm"]["emscripten_bind_btIDebugDraw_draw3dText_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_setDebugMode_1 = Module["_emscripten_bind_btIDebugDraw_setDebugMode_1"] = function() {
  return (_emscripten_bind_btIDebugDraw_setDebugMode_1 = Module["_emscripten_bind_btIDebugDraw_setDebugMode_1"] = Module["asm"]["emscripten_bind_btIDebugDraw_setDebugMode_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw_getDebugMode_0 = Module["_emscripten_bind_btIDebugDraw_getDebugMode_0"] = function() {
  return (_emscripten_bind_btIDebugDraw_getDebugMode_0 = Module["_emscripten_bind_btIDebugDraw_getDebugMode_0"] = Module["asm"]["emscripten_bind_btIDebugDraw_getDebugMode_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIDebugDraw___destroy___0 = Module["_emscripten_bind_btIDebugDraw___destroy___0"] = function() {
  return (_emscripten_bind_btIDebugDraw___destroy___0 = Module["_emscripten_bind_btIDebugDraw___destroy___0"] = Module["asm"]["emscripten_bind_btIDebugDraw___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_0 = Module["_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_0"] = function() {
  return (_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_0 = Module["_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_0"] = Module["asm"]["emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_1 = Module["_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_1"] = function() {
  return (_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_1 = Module["_emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_1"] = Module["asm"]["emscripten_bind_btDefaultCollisionConfiguration_btDefaultCollisionConfiguration_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btDefaultCollisionConfiguration___destroy___0"] = function() {
  return (_emscripten_bind_btDefaultCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btDefaultCollisionConfiguration___destroy___0"] = Module["asm"]["emscripten_bind_btDefaultCollisionConfiguration___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btTriangleMeshShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btTriangleMeshShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btTriangleMeshShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btTriangleMeshShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btTriangleMeshShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btTriangleMeshShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btTriangleMeshShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btTriangleMeshShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btTriangleMeshShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btTriangleMeshShape___destroy___0"] = function() {
  return (_emscripten_bind_btTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btTriangleMeshShape___destroy___0"] = Module["asm"]["emscripten_bind_btTriangleMeshShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_btGhostObject_0 = Module["_emscripten_bind_btGhostObject_btGhostObject_0"] = function() {
  return (_emscripten_bind_btGhostObject_btGhostObject_0 = Module["_emscripten_bind_btGhostObject_btGhostObject_0"] = Module["asm"]["emscripten_bind_btGhostObject_btGhostObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getNumOverlappingObjects_0 = Module["_emscripten_bind_btGhostObject_getNumOverlappingObjects_0"] = function() {
  return (_emscripten_bind_btGhostObject_getNumOverlappingObjects_0 = Module["_emscripten_bind_btGhostObject_getNumOverlappingObjects_0"] = Module["asm"]["emscripten_bind_btGhostObject_getNumOverlappingObjects_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getOverlappingObject_1 = Module["_emscripten_bind_btGhostObject_getOverlappingObject_1"] = function() {
  return (_emscripten_bind_btGhostObject_getOverlappingObject_1 = Module["_emscripten_bind_btGhostObject_getOverlappingObject_1"] = Module["asm"]["emscripten_bind_btGhostObject_getOverlappingObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btGhostObject_setAnisotropicFriction_2"] = function() {
  return (_emscripten_bind_btGhostObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btGhostObject_setAnisotropicFriction_2"] = Module["asm"]["emscripten_bind_btGhostObject_setAnisotropicFriction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getCollisionShape_0 = Module["_emscripten_bind_btGhostObject_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btGhostObject_getCollisionShape_0 = Module["_emscripten_bind_btGhostObject_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btGhostObject_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btGhostObject_setContactProcessingThreshold_1"] = function() {
  return (_emscripten_bind_btGhostObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btGhostObject_setContactProcessingThreshold_1"] = Module["asm"]["emscripten_bind_btGhostObject_setContactProcessingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setActivationState_1 = Module["_emscripten_bind_btGhostObject_setActivationState_1"] = function() {
  return (_emscripten_bind_btGhostObject_setActivationState_1 = Module["_emscripten_bind_btGhostObject_setActivationState_1"] = Module["asm"]["emscripten_bind_btGhostObject_setActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_forceActivationState_1 = Module["_emscripten_bind_btGhostObject_forceActivationState_1"] = function() {
  return (_emscripten_bind_btGhostObject_forceActivationState_1 = Module["_emscripten_bind_btGhostObject_forceActivationState_1"] = Module["asm"]["emscripten_bind_btGhostObject_forceActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_activate_0 = Module["_emscripten_bind_btGhostObject_activate_0"] = function() {
  return (_emscripten_bind_btGhostObject_activate_0 = Module["_emscripten_bind_btGhostObject_activate_0"] = Module["asm"]["emscripten_bind_btGhostObject_activate_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_activate_1 = Module["_emscripten_bind_btGhostObject_activate_1"] = function() {
  return (_emscripten_bind_btGhostObject_activate_1 = Module["_emscripten_bind_btGhostObject_activate_1"] = Module["asm"]["emscripten_bind_btGhostObject_activate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_isActive_0 = Module["_emscripten_bind_btGhostObject_isActive_0"] = function() {
  return (_emscripten_bind_btGhostObject_isActive_0 = Module["_emscripten_bind_btGhostObject_isActive_0"] = Module["asm"]["emscripten_bind_btGhostObject_isActive_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_isKinematicObject_0 = Module["_emscripten_bind_btGhostObject_isKinematicObject_0"] = function() {
  return (_emscripten_bind_btGhostObject_isKinematicObject_0 = Module["_emscripten_bind_btGhostObject_isKinematicObject_0"] = Module["asm"]["emscripten_bind_btGhostObject_isKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_isStaticObject_0 = Module["_emscripten_bind_btGhostObject_isStaticObject_0"] = function() {
  return (_emscripten_bind_btGhostObject_isStaticObject_0 = Module["_emscripten_bind_btGhostObject_isStaticObject_0"] = Module["asm"]["emscripten_bind_btGhostObject_isStaticObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btGhostObject_isStaticOrKinematicObject_0"] = function() {
  return (_emscripten_bind_btGhostObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btGhostObject_isStaticOrKinematicObject_0"] = Module["asm"]["emscripten_bind_btGhostObject_isStaticOrKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getRestitution_0 = Module["_emscripten_bind_btGhostObject_getRestitution_0"] = function() {
  return (_emscripten_bind_btGhostObject_getRestitution_0 = Module["_emscripten_bind_btGhostObject_getRestitution_0"] = Module["asm"]["emscripten_bind_btGhostObject_getRestitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getFriction_0 = Module["_emscripten_bind_btGhostObject_getFriction_0"] = function() {
  return (_emscripten_bind_btGhostObject_getFriction_0 = Module["_emscripten_bind_btGhostObject_getFriction_0"] = Module["asm"]["emscripten_bind_btGhostObject_getFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getRollingFriction_0 = Module["_emscripten_bind_btGhostObject_getRollingFriction_0"] = function() {
  return (_emscripten_bind_btGhostObject_getRollingFriction_0 = Module["_emscripten_bind_btGhostObject_getRollingFriction_0"] = Module["asm"]["emscripten_bind_btGhostObject_getRollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setRestitution_1 = Module["_emscripten_bind_btGhostObject_setRestitution_1"] = function() {
  return (_emscripten_bind_btGhostObject_setRestitution_1 = Module["_emscripten_bind_btGhostObject_setRestitution_1"] = Module["asm"]["emscripten_bind_btGhostObject_setRestitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setFriction_1 = Module["_emscripten_bind_btGhostObject_setFriction_1"] = function() {
  return (_emscripten_bind_btGhostObject_setFriction_1 = Module["_emscripten_bind_btGhostObject_setFriction_1"] = Module["asm"]["emscripten_bind_btGhostObject_setFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setRollingFriction_1 = Module["_emscripten_bind_btGhostObject_setRollingFriction_1"] = function() {
  return (_emscripten_bind_btGhostObject_setRollingFriction_1 = Module["_emscripten_bind_btGhostObject_setRollingFriction_1"] = Module["asm"]["emscripten_bind_btGhostObject_setRollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getWorldTransform_0 = Module["_emscripten_bind_btGhostObject_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btGhostObject_getWorldTransform_0 = Module["_emscripten_bind_btGhostObject_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btGhostObject_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getCollisionFlags_0 = Module["_emscripten_bind_btGhostObject_getCollisionFlags_0"] = function() {
  return (_emscripten_bind_btGhostObject_getCollisionFlags_0 = Module["_emscripten_bind_btGhostObject_getCollisionFlags_0"] = Module["asm"]["emscripten_bind_btGhostObject_getCollisionFlags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setCollisionFlags_1 = Module["_emscripten_bind_btGhostObject_setCollisionFlags_1"] = function() {
  return (_emscripten_bind_btGhostObject_setCollisionFlags_1 = Module["_emscripten_bind_btGhostObject_setCollisionFlags_1"] = Module["asm"]["emscripten_bind_btGhostObject_setCollisionFlags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setWorldTransform_1 = Module["_emscripten_bind_btGhostObject_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btGhostObject_setWorldTransform_1 = Module["_emscripten_bind_btGhostObject_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btGhostObject_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setCollisionShape_1 = Module["_emscripten_bind_btGhostObject_setCollisionShape_1"] = function() {
  return (_emscripten_bind_btGhostObject_setCollisionShape_1 = Module["_emscripten_bind_btGhostObject_setCollisionShape_1"] = Module["asm"]["emscripten_bind_btGhostObject_setCollisionShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btGhostObject_setCcdMotionThreshold_1"] = function() {
  return (_emscripten_bind_btGhostObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btGhostObject_setCcdMotionThreshold_1"] = Module["asm"]["emscripten_bind_btGhostObject_setCcdMotionThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btGhostObject_setCcdSweptSphereRadius_1"] = function() {
  return (_emscripten_bind_btGhostObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btGhostObject_setCcdSweptSphereRadius_1"] = Module["asm"]["emscripten_bind_btGhostObject_setCcdSweptSphereRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getUserIndex_0 = Module["_emscripten_bind_btGhostObject_getUserIndex_0"] = function() {
  return (_emscripten_bind_btGhostObject_getUserIndex_0 = Module["_emscripten_bind_btGhostObject_getUserIndex_0"] = Module["asm"]["emscripten_bind_btGhostObject_getUserIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setUserIndex_1 = Module["_emscripten_bind_btGhostObject_setUserIndex_1"] = function() {
  return (_emscripten_bind_btGhostObject_setUserIndex_1 = Module["_emscripten_bind_btGhostObject_setUserIndex_1"] = Module["asm"]["emscripten_bind_btGhostObject_setUserIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getUserPointer_0 = Module["_emscripten_bind_btGhostObject_getUserPointer_0"] = function() {
  return (_emscripten_bind_btGhostObject_getUserPointer_0 = Module["_emscripten_bind_btGhostObject_getUserPointer_0"] = Module["asm"]["emscripten_bind_btGhostObject_getUserPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_setUserPointer_1 = Module["_emscripten_bind_btGhostObject_setUserPointer_1"] = function() {
  return (_emscripten_bind_btGhostObject_setUserPointer_1 = Module["_emscripten_bind_btGhostObject_setUserPointer_1"] = Module["asm"]["emscripten_bind_btGhostObject_setUserPointer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btGhostObject_getBroadphaseHandle_0"] = function() {
  return (_emscripten_bind_btGhostObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btGhostObject_getBroadphaseHandle_0"] = Module["asm"]["emscripten_bind_btGhostObject_getBroadphaseHandle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostObject___destroy___0 = Module["_emscripten_bind_btGhostObject___destroy___0"] = function() {
  return (_emscripten_bind_btGhostObject___destroy___0 = Module["_emscripten_bind_btGhostObject___destroy___0"] = Module["asm"]["emscripten_bind_btGhostObject___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShape_btConeShape_2 = Module["_emscripten_bind_btConeShape_btConeShape_2"] = function() {
  return (_emscripten_bind_btConeShape_btConeShape_2 = Module["_emscripten_bind_btConeShape_btConeShape_2"] = Module["asm"]["emscripten_bind_btConeShape_btConeShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShape_setLocalScaling_1 = Module["_emscripten_bind_btConeShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConeShape_setLocalScaling_1 = Module["_emscripten_bind_btConeShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConeShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShape_getLocalScaling_0 = Module["_emscripten_bind_btConeShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConeShape_getLocalScaling_0 = Module["_emscripten_bind_btConeShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConeShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConeShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConeShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShape___destroy___0 = Module["_emscripten_bind_btConeShape___destroy___0"] = function() {
  return (_emscripten_bind_btConeShape___destroy___0 = Module["_emscripten_bind_btConeShape___destroy___0"] = Module["asm"]["emscripten_bind_btConeShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btActionInterface_updateAction_2 = Module["_emscripten_bind_btActionInterface_updateAction_2"] = function() {
  return (_emscripten_bind_btActionInterface_updateAction_2 = Module["_emscripten_bind_btActionInterface_updateAction_2"] = Module["asm"]["emscripten_bind_btActionInterface_updateAction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btActionInterface___destroy___0 = Module["_emscripten_bind_btActionInterface___destroy___0"] = function() {
  return (_emscripten_bind_btActionInterface___destroy___0 = Module["_emscripten_bind_btActionInterface___destroy___0"] = Module["asm"]["emscripten_bind_btActionInterface___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_btVector3_0 = Module["_emscripten_bind_btVector3_btVector3_0"] = function() {
  return (_emscripten_bind_btVector3_btVector3_0 = Module["_emscripten_bind_btVector3_btVector3_0"] = Module["asm"]["emscripten_bind_btVector3_btVector3_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_btVector3_3 = Module["_emscripten_bind_btVector3_btVector3_3"] = function() {
  return (_emscripten_bind_btVector3_btVector3_3 = Module["_emscripten_bind_btVector3_btVector3_3"] = Module["asm"]["emscripten_bind_btVector3_btVector3_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_length_0 = Module["_emscripten_bind_btVector3_length_0"] = function() {
  return (_emscripten_bind_btVector3_length_0 = Module["_emscripten_bind_btVector3_length_0"] = Module["asm"]["emscripten_bind_btVector3_length_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_x_0 = Module["_emscripten_bind_btVector3_x_0"] = function() {
  return (_emscripten_bind_btVector3_x_0 = Module["_emscripten_bind_btVector3_x_0"] = Module["asm"]["emscripten_bind_btVector3_x_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_y_0 = Module["_emscripten_bind_btVector3_y_0"] = function() {
  return (_emscripten_bind_btVector3_y_0 = Module["_emscripten_bind_btVector3_y_0"] = Module["asm"]["emscripten_bind_btVector3_y_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_z_0 = Module["_emscripten_bind_btVector3_z_0"] = function() {
  return (_emscripten_bind_btVector3_z_0 = Module["_emscripten_bind_btVector3_z_0"] = Module["asm"]["emscripten_bind_btVector3_z_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_setX_1 = Module["_emscripten_bind_btVector3_setX_1"] = function() {
  return (_emscripten_bind_btVector3_setX_1 = Module["_emscripten_bind_btVector3_setX_1"] = Module["asm"]["emscripten_bind_btVector3_setX_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_setY_1 = Module["_emscripten_bind_btVector3_setY_1"] = function() {
  return (_emscripten_bind_btVector3_setY_1 = Module["_emscripten_bind_btVector3_setY_1"] = Module["asm"]["emscripten_bind_btVector3_setY_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_setZ_1 = Module["_emscripten_bind_btVector3_setZ_1"] = function() {
  return (_emscripten_bind_btVector3_setZ_1 = Module["_emscripten_bind_btVector3_setZ_1"] = Module["asm"]["emscripten_bind_btVector3_setZ_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_setValue_3 = Module["_emscripten_bind_btVector3_setValue_3"] = function() {
  return (_emscripten_bind_btVector3_setValue_3 = Module["_emscripten_bind_btVector3_setValue_3"] = Module["asm"]["emscripten_bind_btVector3_setValue_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_normalize_0 = Module["_emscripten_bind_btVector3_normalize_0"] = function() {
  return (_emscripten_bind_btVector3_normalize_0 = Module["_emscripten_bind_btVector3_normalize_0"] = Module["asm"]["emscripten_bind_btVector3_normalize_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_rotate_2 = Module["_emscripten_bind_btVector3_rotate_2"] = function() {
  return (_emscripten_bind_btVector3_rotate_2 = Module["_emscripten_bind_btVector3_rotate_2"] = Module["asm"]["emscripten_bind_btVector3_rotate_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_dot_1 = Module["_emscripten_bind_btVector3_dot_1"] = function() {
  return (_emscripten_bind_btVector3_dot_1 = Module["_emscripten_bind_btVector3_dot_1"] = Module["asm"]["emscripten_bind_btVector3_dot_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_op_mul_1 = Module["_emscripten_bind_btVector3_op_mul_1"] = function() {
  return (_emscripten_bind_btVector3_op_mul_1 = Module["_emscripten_bind_btVector3_op_mul_1"] = Module["asm"]["emscripten_bind_btVector3_op_mul_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_op_add_1 = Module["_emscripten_bind_btVector3_op_add_1"] = function() {
  return (_emscripten_bind_btVector3_op_add_1 = Module["_emscripten_bind_btVector3_op_add_1"] = Module["asm"]["emscripten_bind_btVector3_op_add_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3_op_sub_1 = Module["_emscripten_bind_btVector3_op_sub_1"] = function() {
  return (_emscripten_bind_btVector3_op_sub_1 = Module["_emscripten_bind_btVector3_op_sub_1"] = Module["asm"]["emscripten_bind_btVector3_op_sub_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3___destroy___0 = Module["_emscripten_bind_btVector3___destroy___0"] = function() {
  return (_emscripten_bind_btVector3___destroy___0 = Module["_emscripten_bind_btVector3___destroy___0"] = Module["asm"]["emscripten_bind_btVector3___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycaster_castRay_3 = Module["_emscripten_bind_btVehicleRaycaster_castRay_3"] = function() {
  return (_emscripten_bind_btVehicleRaycaster_castRay_3 = Module["_emscripten_bind_btVehicleRaycaster_castRay_3"] = Module["asm"]["emscripten_bind_btVehicleRaycaster_castRay_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycaster___destroy___0 = Module["_emscripten_bind_btVehicleRaycaster___destroy___0"] = function() {
  return (_emscripten_bind_btVehicleRaycaster___destroy___0 = Module["_emscripten_bind_btVehicleRaycaster___destroy___0"] = Module["asm"]["emscripten_bind_btVehicleRaycaster___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_x_0 = Module["_emscripten_bind_btQuadWord_x_0"] = function() {
  return (_emscripten_bind_btQuadWord_x_0 = Module["_emscripten_bind_btQuadWord_x_0"] = Module["asm"]["emscripten_bind_btQuadWord_x_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_y_0 = Module["_emscripten_bind_btQuadWord_y_0"] = function() {
  return (_emscripten_bind_btQuadWord_y_0 = Module["_emscripten_bind_btQuadWord_y_0"] = Module["asm"]["emscripten_bind_btQuadWord_y_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_z_0 = Module["_emscripten_bind_btQuadWord_z_0"] = function() {
  return (_emscripten_bind_btQuadWord_z_0 = Module["_emscripten_bind_btQuadWord_z_0"] = Module["asm"]["emscripten_bind_btQuadWord_z_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_w_0 = Module["_emscripten_bind_btQuadWord_w_0"] = function() {
  return (_emscripten_bind_btQuadWord_w_0 = Module["_emscripten_bind_btQuadWord_w_0"] = Module["asm"]["emscripten_bind_btQuadWord_w_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_setX_1 = Module["_emscripten_bind_btQuadWord_setX_1"] = function() {
  return (_emscripten_bind_btQuadWord_setX_1 = Module["_emscripten_bind_btQuadWord_setX_1"] = Module["asm"]["emscripten_bind_btQuadWord_setX_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_setY_1 = Module["_emscripten_bind_btQuadWord_setY_1"] = function() {
  return (_emscripten_bind_btQuadWord_setY_1 = Module["_emscripten_bind_btQuadWord_setY_1"] = Module["asm"]["emscripten_bind_btQuadWord_setY_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_setZ_1 = Module["_emscripten_bind_btQuadWord_setZ_1"] = function() {
  return (_emscripten_bind_btQuadWord_setZ_1 = Module["_emscripten_bind_btQuadWord_setZ_1"] = Module["asm"]["emscripten_bind_btQuadWord_setZ_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord_setW_1 = Module["_emscripten_bind_btQuadWord_setW_1"] = function() {
  return (_emscripten_bind_btQuadWord_setW_1 = Module["_emscripten_bind_btQuadWord_setW_1"] = Module["asm"]["emscripten_bind_btQuadWord_setW_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuadWord___destroy___0 = Module["_emscripten_bind_btQuadWord___destroy___0"] = function() {
  return (_emscripten_bind_btQuadWord___destroy___0 = Module["_emscripten_bind_btQuadWord___destroy___0"] = Module["asm"]["emscripten_bind_btQuadWord___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_btCylinderShape_1 = Module["_emscripten_bind_btCylinderShape_btCylinderShape_1"] = function() {
  return (_emscripten_bind_btCylinderShape_btCylinderShape_1 = Module["_emscripten_bind_btCylinderShape_btCylinderShape_1"] = Module["asm"]["emscripten_bind_btCylinderShape_btCylinderShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_setMargin_1 = Module["_emscripten_bind_btCylinderShape_setMargin_1"] = function() {
  return (_emscripten_bind_btCylinderShape_setMargin_1 = Module["_emscripten_bind_btCylinderShape_setMargin_1"] = Module["asm"]["emscripten_bind_btCylinderShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_getMargin_0 = Module["_emscripten_bind_btCylinderShape_getMargin_0"] = function() {
  return (_emscripten_bind_btCylinderShape_getMargin_0 = Module["_emscripten_bind_btCylinderShape_getMargin_0"] = Module["asm"]["emscripten_bind_btCylinderShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCylinderShape_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCylinderShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCylinderShape_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCylinderShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCylinderShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCylinderShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShape___destroy___0 = Module["_emscripten_bind_btCylinderShape___destroy___0"] = function() {
  return (_emscripten_bind_btCylinderShape___destroy___0 = Module["_emscripten_bind_btCylinderShape___destroy___0"] = Module["asm"]["emscripten_bind_btCylinderShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_btDiscreteDynamicsWorld_4 = Module["_emscripten_bind_btDiscreteDynamicsWorld_btDiscreteDynamicsWorld_4"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_btDiscreteDynamicsWorld_4 = Module["_emscripten_bind_btDiscreteDynamicsWorld_btDiscreteDynamicsWorld_4"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_btDiscreteDynamicsWorld_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setGravity_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setGravity_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setGravity_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setGravity_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setGravity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getGravity_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getGravity_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getGravity_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getGravity_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getGravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addRigidBody_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_removeRigidBody_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeRigidBody_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_removeRigidBody_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeRigidBody_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_removeRigidBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addConstraint_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addConstraint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addConstraint_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_2"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addConstraint_2"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addConstraint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_removeConstraint_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeConstraint_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_removeConstraint_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeConstraint_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_removeConstraint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_2"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_2"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_stepSimulation_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setContactAddedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactAddedCallback_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setContactAddedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactAddedCallback_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setContactAddedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setContactProcessedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactProcessedCallback_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setContactProcessedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactProcessedCallback_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setContactProcessedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setContactDestroyedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactDestroyedCallback_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setContactDestroyedCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setContactDestroyedCallback_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setContactDestroyedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDispatcher_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDispatcher_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getDispatcher_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_rayTest_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_rayTest_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_rayTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getPairCache_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getPairCache_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getPairCache_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDispatchInfo_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDispatchInfo_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getDispatchInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_2"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_2"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addCollisionObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeCollisionObject_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeCollisionObject_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_removeCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getBroadphase_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getBroadphase_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getBroadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btDiscreteDynamicsWorld_convexSweepTest_5"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btDiscreteDynamicsWorld_convexSweepTest_5"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_convexSweepTest_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_contactPairTest_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_contactPairTest_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_contactPairTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_contactTest_2"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_contactTest_2"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_contactTest_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_updateSingleAabb_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_updateSingleAabb_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_updateSingleAabb_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setDebugDrawer_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setDebugDrawer_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setDebugDrawer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDebugDrawer_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getDebugDrawer_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getDebugDrawer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_debugDrawWorld_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_debugDrawWorld_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_debugDrawWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_debugDrawObject_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_debugDrawObject_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_debugDrawObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_addAction_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addAction_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_addAction_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_addAction_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_addAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeAction_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_removeAction_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_removeAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getSolverInfo_0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btDiscreteDynamicsWorld_getSolverInfo_0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_getSolverInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_1"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_1"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_2"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_2"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_3"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_3"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld_setInternalTickCallback_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDiscreteDynamicsWorld___destroy___0 = Module["_emscripten_bind_btDiscreteDynamicsWorld___destroy___0"] = function() {
  return (_emscripten_bind_btDiscreteDynamicsWorld___destroy___0 = Module["_emscripten_bind_btDiscreteDynamicsWorld___destroy___0"] = Module["asm"]["emscripten_bind_btDiscreteDynamicsWorld___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConvexShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConvexShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConvexShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConvexShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConvexShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConvexShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape_setMargin_1 = Module["_emscripten_bind_btConvexShape_setMargin_1"] = function() {
  return (_emscripten_bind_btConvexShape_setMargin_1 = Module["_emscripten_bind_btConvexShape_setMargin_1"] = Module["asm"]["emscripten_bind_btConvexShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape_getMargin_0 = Module["_emscripten_bind_btConvexShape_getMargin_0"] = function() {
  return (_emscripten_bind_btConvexShape_getMargin_0 = Module["_emscripten_bind_btConvexShape_getMargin_0"] = Module["asm"]["emscripten_bind_btConvexShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexShape___destroy___0 = Module["_emscripten_bind_btConvexShape___destroy___0"] = function() {
  return (_emscripten_bind_btConvexShape___destroy___0 = Module["_emscripten_bind_btConvexShape___destroy___0"] = Module["asm"]["emscripten_bind_btConvexShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcher_getNumManifolds_0 = Module["_emscripten_bind_btDispatcher_getNumManifolds_0"] = function() {
  return (_emscripten_bind_btDispatcher_getNumManifolds_0 = Module["_emscripten_bind_btDispatcher_getNumManifolds_0"] = Module["asm"]["emscripten_bind_btDispatcher_getNumManifolds_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcher_getManifoldByIndexInternal_1 = Module["_emscripten_bind_btDispatcher_getManifoldByIndexInternal_1"] = function() {
  return (_emscripten_bind_btDispatcher_getManifoldByIndexInternal_1 = Module["_emscripten_bind_btDispatcher_getManifoldByIndexInternal_1"] = Module["asm"]["emscripten_bind_btDispatcher_getManifoldByIndexInternal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcher___destroy___0 = Module["_emscripten_bind_btDispatcher___destroy___0"] = function() {
  return (_emscripten_bind_btDispatcher___destroy___0 = Module["_emscripten_bind_btDispatcher___destroy___0"] = Module["asm"]["emscripten_bind_btDispatcher___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_3 = Module["_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_3"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_3 = Module["_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_3"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_5 = Module["_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_5"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_5 = Module["_emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_5"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_btGeneric6DofConstraint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setLinearLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setLinearLowerLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setLinearLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setLinearLowerLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setLinearLowerLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setLinearUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setLinearUpperLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setLinearUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setLinearUpperLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setLinearUpperLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setAngularLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setAngularLowerLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setAngularLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setAngularLowerLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setAngularLowerLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setAngularUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setAngularUpperLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setAngularUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setAngularUpperLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setAngularUpperLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_getFrameOffsetA_0 = Module["_emscripten_bind_btGeneric6DofConstraint_getFrameOffsetA_0"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_getFrameOffsetA_0 = Module["_emscripten_bind_btGeneric6DofConstraint_getFrameOffsetA_0"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_getFrameOffsetA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_enableFeedback_1 = Module["_emscripten_bind_btGeneric6DofConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_enableFeedback_1 = Module["_emscripten_bind_btGeneric6DofConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btGeneric6DofConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btGeneric6DofConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btGeneric6DofConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_getParam_2 = Module["_emscripten_bind_btGeneric6DofConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_getParam_2 = Module["_emscripten_bind_btGeneric6DofConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint_setParam_3 = Module["_emscripten_bind_btGeneric6DofConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint_setParam_3 = Module["_emscripten_bind_btGeneric6DofConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofConstraint___destroy___0 = Module["_emscripten_bind_btGeneric6DofConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btGeneric6DofConstraint___destroy___0 = Module["_emscripten_bind_btGeneric6DofConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btGeneric6DofConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStridingMeshInterface_setScaling_1 = Module["_emscripten_bind_btStridingMeshInterface_setScaling_1"] = function() {
  return (_emscripten_bind_btStridingMeshInterface_setScaling_1 = Module["_emscripten_bind_btStridingMeshInterface_setScaling_1"] = Module["asm"]["emscripten_bind_btStridingMeshInterface_setScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStridingMeshInterface___destroy___0 = Module["_emscripten_bind_btStridingMeshInterface___destroy___0"] = function() {
  return (_emscripten_bind_btStridingMeshInterface___destroy___0 = Module["_emscripten_bind_btStridingMeshInterface___destroy___0"] = Module["asm"]["emscripten_bind_btStridingMeshInterface___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMotionState_getWorldTransform_1 = Module["_emscripten_bind_btMotionState_getWorldTransform_1"] = function() {
  return (_emscripten_bind_btMotionState_getWorldTransform_1 = Module["_emscripten_bind_btMotionState_getWorldTransform_1"] = Module["asm"]["emscripten_bind_btMotionState_getWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMotionState_setWorldTransform_1 = Module["_emscripten_bind_btMotionState_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btMotionState_setWorldTransform_1 = Module["_emscripten_bind_btMotionState_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btMotionState_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMotionState___destroy___0 = Module["_emscripten_bind_btMotionState___destroy___0"] = function() {
  return (_emscripten_bind_btMotionState___destroy___0 = Module["_emscripten_bind_btMotionState___destroy___0"] = Module["asm"]["emscripten_bind_btMotionState___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_hasHit_0 = Module["_emscripten_bind_ConvexResultCallback_hasHit_0"] = function() {
  return (_emscripten_bind_ConvexResultCallback_hasHit_0 = Module["_emscripten_bind_ConvexResultCallback_hasHit_0"] = Module["asm"]["emscripten_bind_ConvexResultCallback_hasHit_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_ConvexResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_ConvexResultCallback_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_ConvexResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_ConvexResultCallback_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_ConvexResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_ConvexResultCallback_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_ConvexResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_ConvexResultCallback_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_closestHitFraction_0"] = function() {
  return (_emscripten_bind_ConvexResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ConvexResultCallback_get_m_closestHitFraction_0"] = Module["asm"]["emscripten_bind_ConvexResultCallback_get_m_closestHitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_closestHitFraction_1"] = function() {
  return (_emscripten_bind_ConvexResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ConvexResultCallback_set_m_closestHitFraction_1"] = Module["asm"]["emscripten_bind_ConvexResultCallback_set_m_closestHitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConvexResultCallback___destroy___0 = Module["_emscripten_bind_ConvexResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_ConvexResultCallback___destroy___0 = Module["_emscripten_bind_ConvexResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_ConvexResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ContactResultCallback_addSingleResult_7 = Module["_emscripten_bind_ContactResultCallback_addSingleResult_7"] = function() {
  return (_emscripten_bind_ContactResultCallback_addSingleResult_7 = Module["_emscripten_bind_ContactResultCallback_addSingleResult_7"] = Module["asm"]["emscripten_bind_ContactResultCallback_addSingleResult_7"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ContactResultCallback___destroy___0 = Module["_emscripten_bind_ContactResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_ContactResultCallback___destroy___0 = Module["_emscripten_bind_ContactResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_ContactResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodySolver___destroy___0 = Module["_emscripten_bind_btSoftBodySolver___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBodySolver___destroy___0 = Module["_emscripten_bind_btSoftBodySolver___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBodySolver___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_hasHit_0 = Module["_emscripten_bind_RayResultCallback_hasHit_0"] = function() {
  return (_emscripten_bind_RayResultCallback_hasHit_0 = Module["_emscripten_bind_RayResultCallback_hasHit_0"] = Module["asm"]["emscripten_bind_RayResultCallback_hasHit_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_RayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_RayResultCallback_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_RayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_RayResultCallback_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_RayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_RayResultCallback_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_RayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_RayResultCallback_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_RayResultCallback_get_m_closestHitFraction_0"] = function() {
  return (_emscripten_bind_RayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_RayResultCallback_get_m_closestHitFraction_0"] = Module["asm"]["emscripten_bind_RayResultCallback_get_m_closestHitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_RayResultCallback_set_m_closestHitFraction_1"] = function() {
  return (_emscripten_bind_RayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_RayResultCallback_set_m_closestHitFraction_1"] = Module["asm"]["emscripten_bind_RayResultCallback_set_m_closestHitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionObject_0"] = function() {
  return (_emscripten_bind_RayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_RayResultCallback_get_m_collisionObject_0"] = Module["asm"]["emscripten_bind_RayResultCallback_get_m_collisionObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionObject_1"] = function() {
  return (_emscripten_bind_RayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_RayResultCallback_set_m_collisionObject_1"] = Module["asm"]["emscripten_bind_RayResultCallback_set_m_collisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RayResultCallback___destroy___0 = Module["_emscripten_bind_RayResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_RayResultCallback___destroy___0 = Module["_emscripten_bind_RayResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_RayResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMatrix3x3_setEulerZYX_3 = Module["_emscripten_bind_btMatrix3x3_setEulerZYX_3"] = function() {
  return (_emscripten_bind_btMatrix3x3_setEulerZYX_3 = Module["_emscripten_bind_btMatrix3x3_setEulerZYX_3"] = Module["asm"]["emscripten_bind_btMatrix3x3_setEulerZYX_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMatrix3x3_getRotation_1 = Module["_emscripten_bind_btMatrix3x3_getRotation_1"] = function() {
  return (_emscripten_bind_btMatrix3x3_getRotation_1 = Module["_emscripten_bind_btMatrix3x3_getRotation_1"] = Module["asm"]["emscripten_bind_btMatrix3x3_getRotation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMatrix3x3_getRow_1 = Module["_emscripten_bind_btMatrix3x3_getRow_1"] = function() {
  return (_emscripten_bind_btMatrix3x3_getRow_1 = Module["_emscripten_bind_btMatrix3x3_getRow_1"] = Module["asm"]["emscripten_bind_btMatrix3x3_getRow_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMatrix3x3___destroy___0 = Module["_emscripten_bind_btMatrix3x3___destroy___0"] = function() {
  return (_emscripten_bind_btMatrix3x3___destroy___0 = Module["_emscripten_bind_btMatrix3x3___destroy___0"] = Module["asm"]["emscripten_bind_btMatrix3x3___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btScalarArray_size_0 = Module["_emscripten_bind_btScalarArray_size_0"] = function() {
  return (_emscripten_bind_btScalarArray_size_0 = Module["_emscripten_bind_btScalarArray_size_0"] = Module["asm"]["emscripten_bind_btScalarArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btScalarArray_at_1 = Module["_emscripten_bind_btScalarArray_at_1"] = function() {
  return (_emscripten_bind_btScalarArray_at_1 = Module["_emscripten_bind_btScalarArray_at_1"] = Module["asm"]["emscripten_bind_btScalarArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btScalarArray___destroy___0 = Module["_emscripten_bind_btScalarArray___destroy___0"] = function() {
  return (_emscripten_bind_btScalarArray___destroy___0 = Module["_emscripten_bind_btScalarArray___destroy___0"] = Module["asm"]["emscripten_bind_btScalarArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_get_m_kLST_0 = Module["_emscripten_bind_Material_get_m_kLST_0"] = function() {
  return (_emscripten_bind_Material_get_m_kLST_0 = Module["_emscripten_bind_Material_get_m_kLST_0"] = Module["asm"]["emscripten_bind_Material_get_m_kLST_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_set_m_kLST_1 = Module["_emscripten_bind_Material_set_m_kLST_1"] = function() {
  return (_emscripten_bind_Material_set_m_kLST_1 = Module["_emscripten_bind_Material_set_m_kLST_1"] = Module["asm"]["emscripten_bind_Material_set_m_kLST_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_get_m_kAST_0 = Module["_emscripten_bind_Material_get_m_kAST_0"] = function() {
  return (_emscripten_bind_Material_get_m_kAST_0 = Module["_emscripten_bind_Material_get_m_kAST_0"] = Module["asm"]["emscripten_bind_Material_get_m_kAST_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_set_m_kAST_1 = Module["_emscripten_bind_Material_set_m_kAST_1"] = function() {
  return (_emscripten_bind_Material_set_m_kAST_1 = Module["_emscripten_bind_Material_set_m_kAST_1"] = Module["asm"]["emscripten_bind_Material_set_m_kAST_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_get_m_kVST_0 = Module["_emscripten_bind_Material_get_m_kVST_0"] = function() {
  return (_emscripten_bind_Material_get_m_kVST_0 = Module["_emscripten_bind_Material_get_m_kVST_0"] = Module["asm"]["emscripten_bind_Material_get_m_kVST_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_set_m_kVST_1 = Module["_emscripten_bind_Material_set_m_kVST_1"] = function() {
  return (_emscripten_bind_Material_set_m_kVST_1 = Module["_emscripten_bind_Material_set_m_kVST_1"] = Module["asm"]["emscripten_bind_Material_set_m_kVST_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_get_m_flags_0 = Module["_emscripten_bind_Material_get_m_flags_0"] = function() {
  return (_emscripten_bind_Material_get_m_flags_0 = Module["_emscripten_bind_Material_get_m_flags_0"] = Module["asm"]["emscripten_bind_Material_get_m_flags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material_set_m_flags_1 = Module["_emscripten_bind_Material_set_m_flags_1"] = function() {
  return (_emscripten_bind_Material_set_m_flags_1 = Module["_emscripten_bind_Material_set_m_flags_1"] = Module["asm"]["emscripten_bind_Material_set_m_flags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Material___destroy___0 = Module["_emscripten_bind_Material___destroy___0"] = function() {
  return (_emscripten_bind_Material___destroy___0 = Module["_emscripten_bind_Material___destroy___0"] = Module["asm"]["emscripten_bind_Material___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_timeStep_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_timeStep_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_timeStep_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_timeStep_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_timeStep_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_timeStep_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_timeStep_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_timeStep_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_timeStep_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_timeStep_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_stepCount_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_stepCount_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_stepCount_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_stepCount_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_stepCount_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_stepCount_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_stepCount_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_stepCount_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_stepCount_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_stepCount_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_dispatchFunc_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_dispatchFunc_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_dispatchFunc_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_dispatchFunc_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_dispatchFunc_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_dispatchFunc_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_dispatchFunc_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_dispatchFunc_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_dispatchFunc_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_dispatchFunc_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_timeOfImpact_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_timeOfImpact_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_timeOfImpact_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_timeOfImpact_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_timeOfImpact_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_timeOfImpact_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_timeOfImpact_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_timeOfImpact_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_timeOfImpact_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_timeOfImpact_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_useContinuous_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useContinuous_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_useContinuous_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useContinuous_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_useContinuous_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_useContinuous_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useContinuous_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_useContinuous_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useContinuous_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_useContinuous_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_enableSatConvex_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_enableSatConvex_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_enableSatConvex_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_enableSatConvex_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_enableSatConvex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_enableSatConvex_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_enableSatConvex_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_enableSatConvex_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_enableSatConvex_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_enableSatConvex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_enableSPU_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_enableSPU_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_enableSPU_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_enableSPU_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_enableSPU_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_enableSPU_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_enableSPU_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_enableSPU_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_enableSPU_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_enableSPU_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_useEpa_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useEpa_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_useEpa_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useEpa_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_useEpa_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_useEpa_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useEpa_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_useEpa_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useEpa_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_useEpa_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_allowedCcdPenetration_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_allowedCcdPenetration_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_allowedCcdPenetration_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_allowedCcdPenetration_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_allowedCcdPenetration_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_allowedCcdPenetration_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_allowedCcdPenetration_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_allowedCcdPenetration_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_allowedCcdPenetration_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_allowedCcdPenetration_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_useConvexConservativeDistanceUtil_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useConvexConservativeDistanceUtil_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_useConvexConservativeDistanceUtil_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_useConvexConservativeDistanceUtil_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_useConvexConservativeDistanceUtil_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_useConvexConservativeDistanceUtil_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useConvexConservativeDistanceUtil_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_useConvexConservativeDistanceUtil_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_useConvexConservativeDistanceUtil_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_useConvexConservativeDistanceUtil_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_get_m_convexConservativeDistanceThreshold_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_convexConservativeDistanceThreshold_0"] = function() {
  return (_emscripten_bind_btDispatcherInfo_get_m_convexConservativeDistanceThreshold_0 = Module["_emscripten_bind_btDispatcherInfo_get_m_convexConservativeDistanceThreshold_0"] = Module["asm"]["emscripten_bind_btDispatcherInfo_get_m_convexConservativeDistanceThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo_set_m_convexConservativeDistanceThreshold_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_convexConservativeDistanceThreshold_1"] = function() {
  return (_emscripten_bind_btDispatcherInfo_set_m_convexConservativeDistanceThreshold_1 = Module["_emscripten_bind_btDispatcherInfo_set_m_convexConservativeDistanceThreshold_1"] = Module["asm"]["emscripten_bind_btDispatcherInfo_set_m_convexConservativeDistanceThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDispatcherInfo___destroy___0 = Module["_emscripten_bind_btDispatcherInfo___destroy___0"] = function() {
  return (_emscripten_bind_btDispatcherInfo___destroy___0 = Module["_emscripten_bind_btDispatcherInfo___destroy___0"] = Module["asm"]["emscripten_bind_btDispatcherInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_chassisConnectionCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_chassisConnectionCS_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_chassisConnectionCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_chassisConnectionCS_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_chassisConnectionCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_chassisConnectionCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_chassisConnectionCS_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_chassisConnectionCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_chassisConnectionCS_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_chassisConnectionCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelDirectionCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelDirectionCS_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelDirectionCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelDirectionCS_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelDirectionCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelDirectionCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelDirectionCS_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelDirectionCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelDirectionCS_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelDirectionCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelAxleCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelAxleCS_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelAxleCS_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelAxleCS_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelAxleCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelAxleCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelAxleCS_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelAxleCS_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelAxleCS_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelAxleCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionRestLength_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionRestLength_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionRestLength_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionRestLength_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionRestLength_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionRestLength_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionRestLength_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionRestLength_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionRestLength_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionRestLength_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionTravelCm_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionTravelCm_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionTravelCm_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionTravelCm_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionTravelCm_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionTravelCm_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelRadius_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelRadius_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelRadius_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelRadius_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelRadius_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelRadius_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelRadius_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelRadius_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelRadius_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionStiffness_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionStiffness_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_suspensionStiffness_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionStiffness_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionStiffness_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_suspensionStiffness_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingCompression_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingCompression_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingCompression_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingCompression_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingCompression_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingCompression_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingCompression_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingCompression_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingCompression_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingCompression_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingRelaxation_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingRelaxation_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingRelaxation_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingRelaxation_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_wheelsDampingRelaxation_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingRelaxation_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingRelaxation_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingRelaxation_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingRelaxation_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_wheelsDampingRelaxation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_frictionSlip_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_frictionSlip_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_frictionSlip_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_frictionSlip_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_frictionSlip_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_frictionSlip_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_frictionSlip_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_frictionSlip_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_frictionSlip_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_frictionSlip_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionForce_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionForce_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_maxSuspensionForce_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionForce_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionForce_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_maxSuspensionForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_get_m_bIsFrontWheel_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_bIsFrontWheel_0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_get_m_bIsFrontWheel_0 = Module["_emscripten_bind_btWheelInfoConstructionInfo_get_m_bIsFrontWheel_0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_get_m_bIsFrontWheel_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo_set_m_bIsFrontWheel_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_bIsFrontWheel_1"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo_set_m_bIsFrontWheel_1 = Module["_emscripten_bind_btWheelInfoConstructionInfo_set_m_bIsFrontWheel_1"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo_set_m_bIsFrontWheel_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfoConstructionInfo___destroy___0 = Module["_emscripten_bind_btWheelInfoConstructionInfo___destroy___0"] = function() {
  return (_emscripten_bind_btWheelInfoConstructionInfo___destroy___0 = Module["_emscripten_bind_btWheelInfoConstructionInfo___destroy___0"] = Module["asm"]["emscripten_bind_btWheelInfoConstructionInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_1"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_1"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_2 = Module["_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_2"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_2 = Module["_emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_2"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_btConvexTriangleMeshShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexTriangleMeshShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexTriangleMeshShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexTriangleMeshShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexTriangleMeshShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_setMargin_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_setMargin_1"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_setMargin_1 = Module["_emscripten_bind_btConvexTriangleMeshShape_setMargin_1"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape_getMargin_0 = Module["_emscripten_bind_btConvexTriangleMeshShape_getMargin_0"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape_getMargin_0 = Module["_emscripten_bind_btConvexTriangleMeshShape_getMargin_0"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btConvexTriangleMeshShape___destroy___0"] = function() {
  return (_emscripten_bind_btConvexTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btConvexTriangleMeshShape___destroy___0"] = Module["asm"]["emscripten_bind_btConvexTriangleMeshShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseInterface_getOverlappingPairCache_0 = Module["_emscripten_bind_btBroadphaseInterface_getOverlappingPairCache_0"] = function() {
  return (_emscripten_bind_btBroadphaseInterface_getOverlappingPairCache_0 = Module["_emscripten_bind_btBroadphaseInterface_getOverlappingPairCache_0"] = Module["asm"]["emscripten_bind_btBroadphaseInterface_getOverlappingPairCache_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseInterface___destroy___0 = Module["_emscripten_bind_btBroadphaseInterface___destroy___0"] = function() {
  return (_emscripten_bind_btBroadphaseInterface___destroy___0 = Module["_emscripten_bind_btBroadphaseInterface___destroy___0"] = Module["asm"]["emscripten_bind_btBroadphaseInterface___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_3 = Module["_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_3"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_3 = Module["_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_3"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_4 = Module["_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_4"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_4 = Module["_emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_4"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_btRigidBodyConstructionInfo_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_linearDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearDamping_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearDamping_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_linearDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_linearDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearDamping_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearDamping_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_linearDamping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_angularDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularDamping_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularDamping_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_angularDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_angularDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularDamping_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularDamping_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_angularDamping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_friction_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_friction_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_friction_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_friction_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_friction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_friction_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_friction_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_friction_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_friction_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_friction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_rollingFriction_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_rollingFriction_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_rollingFriction_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_rollingFriction_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_rollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_rollingFriction_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_rollingFriction_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_rollingFriction_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_rollingFriction_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_rollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_restitution_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_restitution_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_restitution_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_restitution_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_restitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_restitution_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_restitution_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_restitution_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_restitution_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_restitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_linearSleepingThreshold_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearSleepingThreshold_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearSleepingThreshold_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_linearSleepingThreshold_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_linearSleepingThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_linearSleepingThreshold_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearSleepingThreshold_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearSleepingThreshold_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_linearSleepingThreshold_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_linearSleepingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_angularSleepingThreshold_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularSleepingThreshold_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularSleepingThreshold_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_angularSleepingThreshold_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_angularSleepingThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_angularSleepingThreshold_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularSleepingThreshold_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularSleepingThreshold_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_angularSleepingThreshold_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_angularSleepingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDamping_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDamping_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDamping_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDamping_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDamping_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDamping_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDamping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDampingFactor_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDampingFactor_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDampingFactor_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDampingFactor_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalDampingFactor_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDampingFactor_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDampingFactor_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDampingFactor_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDampingFactor_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalDampingFactor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalLinearDampingThresholdSqr_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalLinearDampingThresholdSqr_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalLinearDampingThresholdSqr_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalLinearDampingThresholdSqr_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalLinearDampingThresholdSqr_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalLinearDampingThresholdSqr_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalLinearDampingThresholdSqr_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalLinearDampingThresholdSqr_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalLinearDampingThresholdSqr_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalLinearDampingThresholdSqr_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingThresholdSqr_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingThresholdSqr_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingThresholdSqr_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingThresholdSqr_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingThresholdSqr_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingThresholdSqr_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingThresholdSqr_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingThresholdSqr_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingThresholdSqr_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingThresholdSqr_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingFactor_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingFactor_0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingFactor_0 = Module["_emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingFactor_0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_get_m_additionalAngularDampingFactor_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingFactor_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingFactor_1"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingFactor_1 = Module["_emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingFactor_1"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo_set_m_additionalAngularDampingFactor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBodyConstructionInfo___destroy___0 = Module["_emscripten_bind_btRigidBodyConstructionInfo___destroy___0"] = function() {
  return (_emscripten_bind_btRigidBodyConstructionInfo___destroy___0 = Module["_emscripten_bind_btRigidBodyConstructionInfo___destroy___0"] = Module["asm"]["emscripten_bind_btRigidBodyConstructionInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btCollisionConfiguration___destroy___0"] = function() {
  return (_emscripten_bind_btCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btCollisionConfiguration___destroy___0"] = Module["asm"]["emscripten_bind_btCollisionConfiguration___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold_btPersistentManifold_0 = Module["_emscripten_bind_btPersistentManifold_btPersistentManifold_0"] = function() {
  return (_emscripten_bind_btPersistentManifold_btPersistentManifold_0 = Module["_emscripten_bind_btPersistentManifold_btPersistentManifold_0"] = Module["asm"]["emscripten_bind_btPersistentManifold_btPersistentManifold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold_getBody0_0 = Module["_emscripten_bind_btPersistentManifold_getBody0_0"] = function() {
  return (_emscripten_bind_btPersistentManifold_getBody0_0 = Module["_emscripten_bind_btPersistentManifold_getBody0_0"] = Module["asm"]["emscripten_bind_btPersistentManifold_getBody0_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold_getBody1_0 = Module["_emscripten_bind_btPersistentManifold_getBody1_0"] = function() {
  return (_emscripten_bind_btPersistentManifold_getBody1_0 = Module["_emscripten_bind_btPersistentManifold_getBody1_0"] = Module["asm"]["emscripten_bind_btPersistentManifold_getBody1_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold_getNumContacts_0 = Module["_emscripten_bind_btPersistentManifold_getNumContacts_0"] = function() {
  return (_emscripten_bind_btPersistentManifold_getNumContacts_0 = Module["_emscripten_bind_btPersistentManifold_getNumContacts_0"] = Module["asm"]["emscripten_bind_btPersistentManifold_getNumContacts_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold_getContactPoint_1 = Module["_emscripten_bind_btPersistentManifold_getContactPoint_1"] = function() {
  return (_emscripten_bind_btPersistentManifold_getContactPoint_1 = Module["_emscripten_bind_btPersistentManifold_getContactPoint_1"] = Module["asm"]["emscripten_bind_btPersistentManifold_getContactPoint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPersistentManifold___destroy___0 = Module["_emscripten_bind_btPersistentManifold___destroy___0"] = function() {
  return (_emscripten_bind_btPersistentManifold___destroy___0 = Module["_emscripten_bind_btPersistentManifold___destroy___0"] = Module["asm"]["emscripten_bind_btPersistentManifold___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_btCompoundShape_0 = Module["_emscripten_bind_btCompoundShape_btCompoundShape_0"] = function() {
  return (_emscripten_bind_btCompoundShape_btCompoundShape_0 = Module["_emscripten_bind_btCompoundShape_btCompoundShape_0"] = Module["asm"]["emscripten_bind_btCompoundShape_btCompoundShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_btCompoundShape_1 = Module["_emscripten_bind_btCompoundShape_btCompoundShape_1"] = function() {
  return (_emscripten_bind_btCompoundShape_btCompoundShape_1 = Module["_emscripten_bind_btCompoundShape_btCompoundShape_1"] = Module["asm"]["emscripten_bind_btCompoundShape_btCompoundShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_addChildShape_2 = Module["_emscripten_bind_btCompoundShape_addChildShape_2"] = function() {
  return (_emscripten_bind_btCompoundShape_addChildShape_2 = Module["_emscripten_bind_btCompoundShape_addChildShape_2"] = Module["asm"]["emscripten_bind_btCompoundShape_addChildShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_removeChildShape_1 = Module["_emscripten_bind_btCompoundShape_removeChildShape_1"] = function() {
  return (_emscripten_bind_btCompoundShape_removeChildShape_1 = Module["_emscripten_bind_btCompoundShape_removeChildShape_1"] = Module["asm"]["emscripten_bind_btCompoundShape_removeChildShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_removeChildShapeByIndex_1 = Module["_emscripten_bind_btCompoundShape_removeChildShapeByIndex_1"] = function() {
  return (_emscripten_bind_btCompoundShape_removeChildShapeByIndex_1 = Module["_emscripten_bind_btCompoundShape_removeChildShapeByIndex_1"] = Module["asm"]["emscripten_bind_btCompoundShape_removeChildShapeByIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_getNumChildShapes_0 = Module["_emscripten_bind_btCompoundShape_getNumChildShapes_0"] = function() {
  return (_emscripten_bind_btCompoundShape_getNumChildShapes_0 = Module["_emscripten_bind_btCompoundShape_getNumChildShapes_0"] = Module["asm"]["emscripten_bind_btCompoundShape_getNumChildShapes_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_getChildShape_1 = Module["_emscripten_bind_btCompoundShape_getChildShape_1"] = function() {
  return (_emscripten_bind_btCompoundShape_getChildShape_1 = Module["_emscripten_bind_btCompoundShape_getChildShape_1"] = Module["asm"]["emscripten_bind_btCompoundShape_getChildShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_updateChildTransform_2 = Module["_emscripten_bind_btCompoundShape_updateChildTransform_2"] = function() {
  return (_emscripten_bind_btCompoundShape_updateChildTransform_2 = Module["_emscripten_bind_btCompoundShape_updateChildTransform_2"] = Module["asm"]["emscripten_bind_btCompoundShape_updateChildTransform_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_updateChildTransform_3 = Module["_emscripten_bind_btCompoundShape_updateChildTransform_3"] = function() {
  return (_emscripten_bind_btCompoundShape_updateChildTransform_3 = Module["_emscripten_bind_btCompoundShape_updateChildTransform_3"] = Module["asm"]["emscripten_bind_btCompoundShape_updateChildTransform_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_setMargin_1 = Module["_emscripten_bind_btCompoundShape_setMargin_1"] = function() {
  return (_emscripten_bind_btCompoundShape_setMargin_1 = Module["_emscripten_bind_btCompoundShape_setMargin_1"] = Module["asm"]["emscripten_bind_btCompoundShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_getMargin_0 = Module["_emscripten_bind_btCompoundShape_getMargin_0"] = function() {
  return (_emscripten_bind_btCompoundShape_getMargin_0 = Module["_emscripten_bind_btCompoundShape_getMargin_0"] = Module["asm"]["emscripten_bind_btCompoundShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_setLocalScaling_1 = Module["_emscripten_bind_btCompoundShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCompoundShape_setLocalScaling_1 = Module["_emscripten_bind_btCompoundShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCompoundShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_getLocalScaling_0 = Module["_emscripten_bind_btCompoundShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCompoundShape_getLocalScaling_0 = Module["_emscripten_bind_btCompoundShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCompoundShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCompoundShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCompoundShape_calculateLocalInertia_2 = Module["_emscripten_bind_btCompoundShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCompoundShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCompoundShape___destroy___0 = Module["_emscripten_bind_btCompoundShape___destroy___0"] = function() {
  return (_emscripten_bind_btCompoundShape___destroy___0 = Module["_emscripten_bind_btCompoundShape___destroy___0"] = Module["asm"]["emscripten_bind_btCompoundShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_ClosestConvexResultCallback_2 = Module["_emscripten_bind_ClosestConvexResultCallback_ClosestConvexResultCallback_2"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_ClosestConvexResultCallback_2 = Module["_emscripten_bind_ClosestConvexResultCallback_ClosestConvexResultCallback_2"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_ClosestConvexResultCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_hasHit_0 = Module["_emscripten_bind_ClosestConvexResultCallback_hasHit_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_hasHit_0 = Module["_emscripten_bind_ClosestConvexResultCallback_hasHit_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_hasHit_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_convexFromWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_convexFromWorld_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_convexFromWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_convexFromWorld_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_convexFromWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_convexFromWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_convexFromWorld_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_convexFromWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_convexFromWorld_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_convexFromWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_convexToWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_convexToWorld_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_convexToWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_convexToWorld_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_convexToWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_convexToWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_convexToWorld_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_convexToWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_convexToWorld_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_convexToWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_hitNormalWorld_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_hitNormalWorld_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_hitNormalWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_hitNormalWorld_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_hitNormalWorld_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_hitNormalWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_hitPointWorld_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_hitPointWorld_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_hitPointWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_hitPointWorld_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_hitPointWorld_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_hitPointWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_closestHitFraction_0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ClosestConvexResultCallback_get_m_closestHitFraction_0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_get_m_closestHitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_closestHitFraction_1"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ClosestConvexResultCallback_set_m_closestHitFraction_1"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback_set_m_closestHitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestConvexResultCallback___destroy___0 = Module["_emscripten_bind_ClosestConvexResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_ClosestConvexResultCallback___destroy___0 = Module["_emscripten_bind_ClosestConvexResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_ClosestConvexResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_AllHitsRayResultCallback_2 = Module["_emscripten_bind_AllHitsRayResultCallback_AllHitsRayResultCallback_2"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_AllHitsRayResultCallback_2 = Module["_emscripten_bind_AllHitsRayResultCallback_AllHitsRayResultCallback_2"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_AllHitsRayResultCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_hasHit_0 = Module["_emscripten_bind_AllHitsRayResultCallback_hasHit_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_hasHit_0 = Module["_emscripten_bind_AllHitsRayResultCallback_hasHit_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_hasHit_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_collisionObjects_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObjects_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObjects_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObjects_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_collisionObjects_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_collisionObjects_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObjects_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObjects_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObjects_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_collisionObjects_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_rayFromWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_rayFromWorld_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_rayFromWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_rayFromWorld_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_rayFromWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_rayFromWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_rayFromWorld_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_rayFromWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_rayFromWorld_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_rayFromWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_rayToWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_rayToWorld_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_rayToWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_rayToWorld_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_rayToWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_rayToWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_rayToWorld_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_rayToWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_rayToWorld_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_rayToWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitNormalWorld_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitNormalWorld_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_hitNormalWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitNormalWorld_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitNormalWorld_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_hitNormalWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitPointWorld_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitPointWorld_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_hitPointWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitPointWorld_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitPointWorld_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_hitPointWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_hitFractions_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitFractions_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_hitFractions_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_hitFractions_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_hitFractions_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_hitFractions_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitFractions_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_hitFractions_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_hitFractions_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_hitFractions_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_closestHitFraction_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_closestHitFraction_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_closestHitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_closestHitFraction_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_closestHitFraction_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_closestHitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObject_0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_AllHitsRayResultCallback_get_m_collisionObject_0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_get_m_collisionObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObject_1"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_AllHitsRayResultCallback_set_m_collisionObject_1"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback_set_m_collisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_AllHitsRayResultCallback___destroy___0 = Module["_emscripten_bind_AllHitsRayResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_AllHitsRayResultCallback___destroy___0 = Module["_emscripten_bind_AllHitsRayResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_AllHitsRayResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tMaterialArray_size_0 = Module["_emscripten_bind_tMaterialArray_size_0"] = function() {
  return (_emscripten_bind_tMaterialArray_size_0 = Module["_emscripten_bind_tMaterialArray_size_0"] = Module["asm"]["emscripten_bind_tMaterialArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tMaterialArray_at_1 = Module["_emscripten_bind_tMaterialArray_at_1"] = function() {
  return (_emscripten_bind_tMaterialArray_at_1 = Module["_emscripten_bind_tMaterialArray_at_1"] = Module["asm"]["emscripten_bind_tMaterialArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tMaterialArray___destroy___0 = Module["_emscripten_bind_tMaterialArray___destroy___0"] = function() {
  return (_emscripten_bind_tMaterialArray___destroy___0 = Module["_emscripten_bind_tMaterialArray___destroy___0"] = Module["asm"]["emscripten_bind_tMaterialArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultVehicleRaycaster_btDefaultVehicleRaycaster_1 = Module["_emscripten_bind_btDefaultVehicleRaycaster_btDefaultVehicleRaycaster_1"] = function() {
  return (_emscripten_bind_btDefaultVehicleRaycaster_btDefaultVehicleRaycaster_1 = Module["_emscripten_bind_btDefaultVehicleRaycaster_btDefaultVehicleRaycaster_1"] = Module["asm"]["emscripten_bind_btDefaultVehicleRaycaster_btDefaultVehicleRaycaster_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultVehicleRaycaster_castRay_3 = Module["_emscripten_bind_btDefaultVehicleRaycaster_castRay_3"] = function() {
  return (_emscripten_bind_btDefaultVehicleRaycaster_castRay_3 = Module["_emscripten_bind_btDefaultVehicleRaycaster_castRay_3"] = Module["asm"]["emscripten_bind_btDefaultVehicleRaycaster_castRay_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultVehicleRaycaster___destroy___0 = Module["_emscripten_bind_btDefaultVehicleRaycaster___destroy___0"] = function() {
  return (_emscripten_bind_btDefaultVehicleRaycaster___destroy___0 = Module["_emscripten_bind_btDefaultVehicleRaycaster___destroy___0"] = Module["asm"]["emscripten_bind_btDefaultVehicleRaycaster___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btEmptyShape_btEmptyShape_0 = Module["_emscripten_bind_btEmptyShape_btEmptyShape_0"] = function() {
  return (_emscripten_bind_btEmptyShape_btEmptyShape_0 = Module["_emscripten_bind_btEmptyShape_btEmptyShape_0"] = Module["asm"]["emscripten_bind_btEmptyShape_btEmptyShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btEmptyShape_setLocalScaling_1 = Module["_emscripten_bind_btEmptyShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btEmptyShape_setLocalScaling_1 = Module["_emscripten_bind_btEmptyShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btEmptyShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btEmptyShape_getLocalScaling_0 = Module["_emscripten_bind_btEmptyShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btEmptyShape_getLocalScaling_0 = Module["_emscripten_bind_btEmptyShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btEmptyShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btEmptyShape_calculateLocalInertia_2 = Module["_emscripten_bind_btEmptyShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btEmptyShape_calculateLocalInertia_2 = Module["_emscripten_bind_btEmptyShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btEmptyShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btEmptyShape___destroy___0 = Module["_emscripten_bind_btEmptyShape___destroy___0"] = function() {
  return (_emscripten_bind_btEmptyShape___destroy___0 = Module["_emscripten_bind_btEmptyShape___destroy___0"] = Module["asm"]["emscripten_bind_btEmptyShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_btConstraintSetting_0 = Module["_emscripten_bind_btConstraintSetting_btConstraintSetting_0"] = function() {
  return (_emscripten_bind_btConstraintSetting_btConstraintSetting_0 = Module["_emscripten_bind_btConstraintSetting_btConstraintSetting_0"] = Module["asm"]["emscripten_bind_btConstraintSetting_btConstraintSetting_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_get_m_tau_0 = Module["_emscripten_bind_btConstraintSetting_get_m_tau_0"] = function() {
  return (_emscripten_bind_btConstraintSetting_get_m_tau_0 = Module["_emscripten_bind_btConstraintSetting_get_m_tau_0"] = Module["asm"]["emscripten_bind_btConstraintSetting_get_m_tau_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_set_m_tau_1 = Module["_emscripten_bind_btConstraintSetting_set_m_tau_1"] = function() {
  return (_emscripten_bind_btConstraintSetting_set_m_tau_1 = Module["_emscripten_bind_btConstraintSetting_set_m_tau_1"] = Module["asm"]["emscripten_bind_btConstraintSetting_set_m_tau_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_get_m_damping_0 = Module["_emscripten_bind_btConstraintSetting_get_m_damping_0"] = function() {
  return (_emscripten_bind_btConstraintSetting_get_m_damping_0 = Module["_emscripten_bind_btConstraintSetting_get_m_damping_0"] = Module["asm"]["emscripten_bind_btConstraintSetting_get_m_damping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_set_m_damping_1 = Module["_emscripten_bind_btConstraintSetting_set_m_damping_1"] = function() {
  return (_emscripten_bind_btConstraintSetting_set_m_damping_1 = Module["_emscripten_bind_btConstraintSetting_set_m_damping_1"] = Module["asm"]["emscripten_bind_btConstraintSetting_set_m_damping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_get_m_impulseClamp_0 = Module["_emscripten_bind_btConstraintSetting_get_m_impulseClamp_0"] = function() {
  return (_emscripten_bind_btConstraintSetting_get_m_impulseClamp_0 = Module["_emscripten_bind_btConstraintSetting_get_m_impulseClamp_0"] = Module["asm"]["emscripten_bind_btConstraintSetting_get_m_impulseClamp_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting_set_m_impulseClamp_1 = Module["_emscripten_bind_btConstraintSetting_set_m_impulseClamp_1"] = function() {
  return (_emscripten_bind_btConstraintSetting_set_m_impulseClamp_1 = Module["_emscripten_bind_btConstraintSetting_set_m_impulseClamp_1"] = Module["asm"]["emscripten_bind_btConstraintSetting_set_m_impulseClamp_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSetting___destroy___0 = Module["_emscripten_bind_btConstraintSetting___destroy___0"] = function() {
  return (_emscripten_bind_btConstraintSetting___destroy___0 = Module["_emscripten_bind_btConstraintSetting___destroy___0"] = Module["asm"]["emscripten_bind_btConstraintSetting___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalShapeInfo_get_m_shapePart_0 = Module["_emscripten_bind_LocalShapeInfo_get_m_shapePart_0"] = function() {
  return (_emscripten_bind_LocalShapeInfo_get_m_shapePart_0 = Module["_emscripten_bind_LocalShapeInfo_get_m_shapePart_0"] = Module["asm"]["emscripten_bind_LocalShapeInfo_get_m_shapePart_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalShapeInfo_set_m_shapePart_1 = Module["_emscripten_bind_LocalShapeInfo_set_m_shapePart_1"] = function() {
  return (_emscripten_bind_LocalShapeInfo_set_m_shapePart_1 = Module["_emscripten_bind_LocalShapeInfo_set_m_shapePart_1"] = Module["asm"]["emscripten_bind_LocalShapeInfo_set_m_shapePart_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalShapeInfo_get_m_triangleIndex_0 = Module["_emscripten_bind_LocalShapeInfo_get_m_triangleIndex_0"] = function() {
  return (_emscripten_bind_LocalShapeInfo_get_m_triangleIndex_0 = Module["_emscripten_bind_LocalShapeInfo_get_m_triangleIndex_0"] = Module["asm"]["emscripten_bind_LocalShapeInfo_get_m_triangleIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalShapeInfo_set_m_triangleIndex_1 = Module["_emscripten_bind_LocalShapeInfo_set_m_triangleIndex_1"] = function() {
  return (_emscripten_bind_LocalShapeInfo_set_m_triangleIndex_1 = Module["_emscripten_bind_LocalShapeInfo_set_m_triangleIndex_1"] = Module["asm"]["emscripten_bind_LocalShapeInfo_set_m_triangleIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalShapeInfo___destroy___0 = Module["_emscripten_bind_LocalShapeInfo___destroy___0"] = function() {
  return (_emscripten_bind_LocalShapeInfo___destroy___0 = Module["_emscripten_bind_LocalShapeInfo___destroy___0"] = Module["asm"]["emscripten_bind_LocalShapeInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_btRigidBody_1 = Module["_emscripten_bind_btRigidBody_btRigidBody_1"] = function() {
  return (_emscripten_bind_btRigidBody_btRigidBody_1 = Module["_emscripten_bind_btRigidBody_btRigidBody_1"] = Module["asm"]["emscripten_bind_btRigidBody_btRigidBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getCenterOfMassTransform_0 = Module["_emscripten_bind_btRigidBody_getCenterOfMassTransform_0"] = function() {
  return (_emscripten_bind_btRigidBody_getCenterOfMassTransform_0 = Module["_emscripten_bind_btRigidBody_getCenterOfMassTransform_0"] = Module["asm"]["emscripten_bind_btRigidBody_getCenterOfMassTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setCenterOfMassTransform_1 = Module["_emscripten_bind_btRigidBody_setCenterOfMassTransform_1"] = function() {
  return (_emscripten_bind_btRigidBody_setCenterOfMassTransform_1 = Module["_emscripten_bind_btRigidBody_setCenterOfMassTransform_1"] = Module["asm"]["emscripten_bind_btRigidBody_setCenterOfMassTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setSleepingThresholds_2 = Module["_emscripten_bind_btRigidBody_setSleepingThresholds_2"] = function() {
  return (_emscripten_bind_btRigidBody_setSleepingThresholds_2 = Module["_emscripten_bind_btRigidBody_setSleepingThresholds_2"] = Module["asm"]["emscripten_bind_btRigidBody_setSleepingThresholds_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getLinearDamping_0 = Module["_emscripten_bind_btRigidBody_getLinearDamping_0"] = function() {
  return (_emscripten_bind_btRigidBody_getLinearDamping_0 = Module["_emscripten_bind_btRigidBody_getLinearDamping_0"] = Module["asm"]["emscripten_bind_btRigidBody_getLinearDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getAngularDamping_0 = Module["_emscripten_bind_btRigidBody_getAngularDamping_0"] = function() {
  return (_emscripten_bind_btRigidBody_getAngularDamping_0 = Module["_emscripten_bind_btRigidBody_getAngularDamping_0"] = Module["asm"]["emscripten_bind_btRigidBody_getAngularDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setDamping_2 = Module["_emscripten_bind_btRigidBody_setDamping_2"] = function() {
  return (_emscripten_bind_btRigidBody_setDamping_2 = Module["_emscripten_bind_btRigidBody_setDamping_2"] = Module["asm"]["emscripten_bind_btRigidBody_setDamping_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setMassProps_2 = Module["_emscripten_bind_btRigidBody_setMassProps_2"] = function() {
  return (_emscripten_bind_btRigidBody_setMassProps_2 = Module["_emscripten_bind_btRigidBody_setMassProps_2"] = Module["asm"]["emscripten_bind_btRigidBody_setMassProps_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getLinearFactor_0 = Module["_emscripten_bind_btRigidBody_getLinearFactor_0"] = function() {
  return (_emscripten_bind_btRigidBody_getLinearFactor_0 = Module["_emscripten_bind_btRigidBody_getLinearFactor_0"] = Module["asm"]["emscripten_bind_btRigidBody_getLinearFactor_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setLinearFactor_1 = Module["_emscripten_bind_btRigidBody_setLinearFactor_1"] = function() {
  return (_emscripten_bind_btRigidBody_setLinearFactor_1 = Module["_emscripten_bind_btRigidBody_setLinearFactor_1"] = Module["asm"]["emscripten_bind_btRigidBody_setLinearFactor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyTorque_1 = Module["_emscripten_bind_btRigidBody_applyTorque_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyTorque_1 = Module["_emscripten_bind_btRigidBody_applyTorque_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyTorque_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyLocalTorque_1 = Module["_emscripten_bind_btRigidBody_applyLocalTorque_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyLocalTorque_1 = Module["_emscripten_bind_btRigidBody_applyLocalTorque_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyLocalTorque_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyForce_2 = Module["_emscripten_bind_btRigidBody_applyForce_2"] = function() {
  return (_emscripten_bind_btRigidBody_applyForce_2 = Module["_emscripten_bind_btRigidBody_applyForce_2"] = Module["asm"]["emscripten_bind_btRigidBody_applyForce_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyCentralForce_1 = Module["_emscripten_bind_btRigidBody_applyCentralForce_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyCentralForce_1 = Module["_emscripten_bind_btRigidBody_applyCentralForce_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyCentralForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyCentralLocalForce_1 = Module["_emscripten_bind_btRigidBody_applyCentralLocalForce_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyCentralLocalForce_1 = Module["_emscripten_bind_btRigidBody_applyCentralLocalForce_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyCentralLocalForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyTorqueImpulse_1 = Module["_emscripten_bind_btRigidBody_applyTorqueImpulse_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyTorqueImpulse_1 = Module["_emscripten_bind_btRigidBody_applyTorqueImpulse_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyTorqueImpulse_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyImpulse_2 = Module["_emscripten_bind_btRigidBody_applyImpulse_2"] = function() {
  return (_emscripten_bind_btRigidBody_applyImpulse_2 = Module["_emscripten_bind_btRigidBody_applyImpulse_2"] = Module["asm"]["emscripten_bind_btRigidBody_applyImpulse_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyCentralImpulse_1 = Module["_emscripten_bind_btRigidBody_applyCentralImpulse_1"] = function() {
  return (_emscripten_bind_btRigidBody_applyCentralImpulse_1 = Module["_emscripten_bind_btRigidBody_applyCentralImpulse_1"] = Module["asm"]["emscripten_bind_btRigidBody_applyCentralImpulse_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_updateInertiaTensor_0 = Module["_emscripten_bind_btRigidBody_updateInertiaTensor_0"] = function() {
  return (_emscripten_bind_btRigidBody_updateInertiaTensor_0 = Module["_emscripten_bind_btRigidBody_updateInertiaTensor_0"] = Module["asm"]["emscripten_bind_btRigidBody_updateInertiaTensor_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getLinearVelocity_0 = Module["_emscripten_bind_btRigidBody_getLinearVelocity_0"] = function() {
  return (_emscripten_bind_btRigidBody_getLinearVelocity_0 = Module["_emscripten_bind_btRigidBody_getLinearVelocity_0"] = Module["asm"]["emscripten_bind_btRigidBody_getLinearVelocity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getAngularVelocity_0 = Module["_emscripten_bind_btRigidBody_getAngularVelocity_0"] = function() {
  return (_emscripten_bind_btRigidBody_getAngularVelocity_0 = Module["_emscripten_bind_btRigidBody_getAngularVelocity_0"] = Module["asm"]["emscripten_bind_btRigidBody_getAngularVelocity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setLinearVelocity_1 = Module["_emscripten_bind_btRigidBody_setLinearVelocity_1"] = function() {
  return (_emscripten_bind_btRigidBody_setLinearVelocity_1 = Module["_emscripten_bind_btRigidBody_setLinearVelocity_1"] = Module["asm"]["emscripten_bind_btRigidBody_setLinearVelocity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setAngularVelocity_1 = Module["_emscripten_bind_btRigidBody_setAngularVelocity_1"] = function() {
  return (_emscripten_bind_btRigidBody_setAngularVelocity_1 = Module["_emscripten_bind_btRigidBody_setAngularVelocity_1"] = Module["asm"]["emscripten_bind_btRigidBody_setAngularVelocity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getMotionState_0 = Module["_emscripten_bind_btRigidBody_getMotionState_0"] = function() {
  return (_emscripten_bind_btRigidBody_getMotionState_0 = Module["_emscripten_bind_btRigidBody_getMotionState_0"] = Module["asm"]["emscripten_bind_btRigidBody_getMotionState_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setMotionState_1 = Module["_emscripten_bind_btRigidBody_setMotionState_1"] = function() {
  return (_emscripten_bind_btRigidBody_setMotionState_1 = Module["_emscripten_bind_btRigidBody_setMotionState_1"] = Module["asm"]["emscripten_bind_btRigidBody_setMotionState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getAngularFactor_0 = Module["_emscripten_bind_btRigidBody_getAngularFactor_0"] = function() {
  return (_emscripten_bind_btRigidBody_getAngularFactor_0 = Module["_emscripten_bind_btRigidBody_getAngularFactor_0"] = Module["asm"]["emscripten_bind_btRigidBody_getAngularFactor_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setAngularFactor_1 = Module["_emscripten_bind_btRigidBody_setAngularFactor_1"] = function() {
  return (_emscripten_bind_btRigidBody_setAngularFactor_1 = Module["_emscripten_bind_btRigidBody_setAngularFactor_1"] = Module["asm"]["emscripten_bind_btRigidBody_setAngularFactor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_upcast_1 = Module["_emscripten_bind_btRigidBody_upcast_1"] = function() {
  return (_emscripten_bind_btRigidBody_upcast_1 = Module["_emscripten_bind_btRigidBody_upcast_1"] = Module["asm"]["emscripten_bind_btRigidBody_upcast_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getAabb_2 = Module["_emscripten_bind_btRigidBody_getAabb_2"] = function() {
  return (_emscripten_bind_btRigidBody_getAabb_2 = Module["_emscripten_bind_btRigidBody_getAabb_2"] = Module["asm"]["emscripten_bind_btRigidBody_getAabb_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_applyGravity_0 = Module["_emscripten_bind_btRigidBody_applyGravity_0"] = function() {
  return (_emscripten_bind_btRigidBody_applyGravity_0 = Module["_emscripten_bind_btRigidBody_applyGravity_0"] = Module["asm"]["emscripten_bind_btRigidBody_applyGravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getGravity_0 = Module["_emscripten_bind_btRigidBody_getGravity_0"] = function() {
  return (_emscripten_bind_btRigidBody_getGravity_0 = Module["_emscripten_bind_btRigidBody_getGravity_0"] = Module["asm"]["emscripten_bind_btRigidBody_getGravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setGravity_1 = Module["_emscripten_bind_btRigidBody_setGravity_1"] = function() {
  return (_emscripten_bind_btRigidBody_setGravity_1 = Module["_emscripten_bind_btRigidBody_setGravity_1"] = Module["asm"]["emscripten_bind_btRigidBody_setGravity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getBroadphaseProxy_0 = Module["_emscripten_bind_btRigidBody_getBroadphaseProxy_0"] = function() {
  return (_emscripten_bind_btRigidBody_getBroadphaseProxy_0 = Module["_emscripten_bind_btRigidBody_getBroadphaseProxy_0"] = Module["asm"]["emscripten_bind_btRigidBody_getBroadphaseProxy_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_clearForces_0 = Module["_emscripten_bind_btRigidBody_clearForces_0"] = function() {
  return (_emscripten_bind_btRigidBody_clearForces_0 = Module["_emscripten_bind_btRigidBody_clearForces_0"] = Module["asm"]["emscripten_bind_btRigidBody_clearForces_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setAnisotropicFriction_2 = Module["_emscripten_bind_btRigidBody_setAnisotropicFriction_2"] = function() {
  return (_emscripten_bind_btRigidBody_setAnisotropicFriction_2 = Module["_emscripten_bind_btRigidBody_setAnisotropicFriction_2"] = Module["asm"]["emscripten_bind_btRigidBody_setAnisotropicFriction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getCollisionShape_0 = Module["_emscripten_bind_btRigidBody_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btRigidBody_getCollisionShape_0 = Module["_emscripten_bind_btRigidBody_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btRigidBody_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setContactProcessingThreshold_1 = Module["_emscripten_bind_btRigidBody_setContactProcessingThreshold_1"] = function() {
  return (_emscripten_bind_btRigidBody_setContactProcessingThreshold_1 = Module["_emscripten_bind_btRigidBody_setContactProcessingThreshold_1"] = Module["asm"]["emscripten_bind_btRigidBody_setContactProcessingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setActivationState_1 = Module["_emscripten_bind_btRigidBody_setActivationState_1"] = function() {
  return (_emscripten_bind_btRigidBody_setActivationState_1 = Module["_emscripten_bind_btRigidBody_setActivationState_1"] = Module["asm"]["emscripten_bind_btRigidBody_setActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_forceActivationState_1 = Module["_emscripten_bind_btRigidBody_forceActivationState_1"] = function() {
  return (_emscripten_bind_btRigidBody_forceActivationState_1 = Module["_emscripten_bind_btRigidBody_forceActivationState_1"] = Module["asm"]["emscripten_bind_btRigidBody_forceActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_activate_0 = Module["_emscripten_bind_btRigidBody_activate_0"] = function() {
  return (_emscripten_bind_btRigidBody_activate_0 = Module["_emscripten_bind_btRigidBody_activate_0"] = Module["asm"]["emscripten_bind_btRigidBody_activate_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_activate_1 = Module["_emscripten_bind_btRigidBody_activate_1"] = function() {
  return (_emscripten_bind_btRigidBody_activate_1 = Module["_emscripten_bind_btRigidBody_activate_1"] = Module["asm"]["emscripten_bind_btRigidBody_activate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_isActive_0 = Module["_emscripten_bind_btRigidBody_isActive_0"] = function() {
  return (_emscripten_bind_btRigidBody_isActive_0 = Module["_emscripten_bind_btRigidBody_isActive_0"] = Module["asm"]["emscripten_bind_btRigidBody_isActive_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_isKinematicObject_0 = Module["_emscripten_bind_btRigidBody_isKinematicObject_0"] = function() {
  return (_emscripten_bind_btRigidBody_isKinematicObject_0 = Module["_emscripten_bind_btRigidBody_isKinematicObject_0"] = Module["asm"]["emscripten_bind_btRigidBody_isKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_isStaticObject_0 = Module["_emscripten_bind_btRigidBody_isStaticObject_0"] = function() {
  return (_emscripten_bind_btRigidBody_isStaticObject_0 = Module["_emscripten_bind_btRigidBody_isStaticObject_0"] = Module["asm"]["emscripten_bind_btRigidBody_isStaticObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btRigidBody_isStaticOrKinematicObject_0"] = function() {
  return (_emscripten_bind_btRigidBody_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btRigidBody_isStaticOrKinematicObject_0"] = Module["asm"]["emscripten_bind_btRigidBody_isStaticOrKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getRestitution_0 = Module["_emscripten_bind_btRigidBody_getRestitution_0"] = function() {
  return (_emscripten_bind_btRigidBody_getRestitution_0 = Module["_emscripten_bind_btRigidBody_getRestitution_0"] = Module["asm"]["emscripten_bind_btRigidBody_getRestitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getFriction_0 = Module["_emscripten_bind_btRigidBody_getFriction_0"] = function() {
  return (_emscripten_bind_btRigidBody_getFriction_0 = Module["_emscripten_bind_btRigidBody_getFriction_0"] = Module["asm"]["emscripten_bind_btRigidBody_getFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getRollingFriction_0 = Module["_emscripten_bind_btRigidBody_getRollingFriction_0"] = function() {
  return (_emscripten_bind_btRigidBody_getRollingFriction_0 = Module["_emscripten_bind_btRigidBody_getRollingFriction_0"] = Module["asm"]["emscripten_bind_btRigidBody_getRollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setRestitution_1 = Module["_emscripten_bind_btRigidBody_setRestitution_1"] = function() {
  return (_emscripten_bind_btRigidBody_setRestitution_1 = Module["_emscripten_bind_btRigidBody_setRestitution_1"] = Module["asm"]["emscripten_bind_btRigidBody_setRestitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setFriction_1 = Module["_emscripten_bind_btRigidBody_setFriction_1"] = function() {
  return (_emscripten_bind_btRigidBody_setFriction_1 = Module["_emscripten_bind_btRigidBody_setFriction_1"] = Module["asm"]["emscripten_bind_btRigidBody_setFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setRollingFriction_1 = Module["_emscripten_bind_btRigidBody_setRollingFriction_1"] = function() {
  return (_emscripten_bind_btRigidBody_setRollingFriction_1 = Module["_emscripten_bind_btRigidBody_setRollingFriction_1"] = Module["asm"]["emscripten_bind_btRigidBody_setRollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getWorldTransform_0 = Module["_emscripten_bind_btRigidBody_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btRigidBody_getWorldTransform_0 = Module["_emscripten_bind_btRigidBody_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btRigidBody_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getCollisionFlags_0 = Module["_emscripten_bind_btRigidBody_getCollisionFlags_0"] = function() {
  return (_emscripten_bind_btRigidBody_getCollisionFlags_0 = Module["_emscripten_bind_btRigidBody_getCollisionFlags_0"] = Module["asm"]["emscripten_bind_btRigidBody_getCollisionFlags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setCollisionFlags_1 = Module["_emscripten_bind_btRigidBody_setCollisionFlags_1"] = function() {
  return (_emscripten_bind_btRigidBody_setCollisionFlags_1 = Module["_emscripten_bind_btRigidBody_setCollisionFlags_1"] = Module["asm"]["emscripten_bind_btRigidBody_setCollisionFlags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setWorldTransform_1 = Module["_emscripten_bind_btRigidBody_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btRigidBody_setWorldTransform_1 = Module["_emscripten_bind_btRigidBody_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btRigidBody_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setCollisionShape_1 = Module["_emscripten_bind_btRigidBody_setCollisionShape_1"] = function() {
  return (_emscripten_bind_btRigidBody_setCollisionShape_1 = Module["_emscripten_bind_btRigidBody_setCollisionShape_1"] = Module["asm"]["emscripten_bind_btRigidBody_setCollisionShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setCcdMotionThreshold_1 = Module["_emscripten_bind_btRigidBody_setCcdMotionThreshold_1"] = function() {
  return (_emscripten_bind_btRigidBody_setCcdMotionThreshold_1 = Module["_emscripten_bind_btRigidBody_setCcdMotionThreshold_1"] = Module["asm"]["emscripten_bind_btRigidBody_setCcdMotionThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btRigidBody_setCcdSweptSphereRadius_1"] = function() {
  return (_emscripten_bind_btRigidBody_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btRigidBody_setCcdSweptSphereRadius_1"] = Module["asm"]["emscripten_bind_btRigidBody_setCcdSweptSphereRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getUserIndex_0 = Module["_emscripten_bind_btRigidBody_getUserIndex_0"] = function() {
  return (_emscripten_bind_btRigidBody_getUserIndex_0 = Module["_emscripten_bind_btRigidBody_getUserIndex_0"] = Module["asm"]["emscripten_bind_btRigidBody_getUserIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setUserIndex_1 = Module["_emscripten_bind_btRigidBody_setUserIndex_1"] = function() {
  return (_emscripten_bind_btRigidBody_setUserIndex_1 = Module["_emscripten_bind_btRigidBody_setUserIndex_1"] = Module["asm"]["emscripten_bind_btRigidBody_setUserIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getUserPointer_0 = Module["_emscripten_bind_btRigidBody_getUserPointer_0"] = function() {
  return (_emscripten_bind_btRigidBody_getUserPointer_0 = Module["_emscripten_bind_btRigidBody_getUserPointer_0"] = Module["asm"]["emscripten_bind_btRigidBody_getUserPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_setUserPointer_1 = Module["_emscripten_bind_btRigidBody_setUserPointer_1"] = function() {
  return (_emscripten_bind_btRigidBody_setUserPointer_1 = Module["_emscripten_bind_btRigidBody_setUserPointer_1"] = Module["asm"]["emscripten_bind_btRigidBody_setUserPointer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody_getBroadphaseHandle_0 = Module["_emscripten_bind_btRigidBody_getBroadphaseHandle_0"] = function() {
  return (_emscripten_bind_btRigidBody_getBroadphaseHandle_0 = Module["_emscripten_bind_btRigidBody_getBroadphaseHandle_0"] = Module["asm"]["emscripten_bind_btRigidBody_getBroadphaseHandle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRigidBody___destroy___0 = Module["_emscripten_bind_btRigidBody___destroy___0"] = function() {
  return (_emscripten_bind_btRigidBody___destroy___0 = Module["_emscripten_bind_btRigidBody___destroy___0"] = Module["asm"]["emscripten_bind_btRigidBody___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMeshArray_size_0 = Module["_emscripten_bind_btIndexedMeshArray_size_0"] = function() {
  return (_emscripten_bind_btIndexedMeshArray_size_0 = Module["_emscripten_bind_btIndexedMeshArray_size_0"] = Module["asm"]["emscripten_bind_btIndexedMeshArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMeshArray_at_1 = Module["_emscripten_bind_btIndexedMeshArray_at_1"] = function() {
  return (_emscripten_bind_btIndexedMeshArray_at_1 = Module["_emscripten_bind_btIndexedMeshArray_at_1"] = Module["asm"]["emscripten_bind_btIndexedMeshArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMeshArray___destroy___0 = Module["_emscripten_bind_btIndexedMeshArray___destroy___0"] = function() {
  return (_emscripten_bind_btIndexedMeshArray___destroy___0 = Module["_emscripten_bind_btIndexedMeshArray___destroy___0"] = Module["asm"]["emscripten_bind_btIndexedMeshArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDbvtBroadphase_btDbvtBroadphase_0 = Module["_emscripten_bind_btDbvtBroadphase_btDbvtBroadphase_0"] = function() {
  return (_emscripten_bind_btDbvtBroadphase_btDbvtBroadphase_0 = Module["_emscripten_bind_btDbvtBroadphase_btDbvtBroadphase_0"] = Module["asm"]["emscripten_bind_btDbvtBroadphase_btDbvtBroadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDbvtBroadphase___destroy___0 = Module["_emscripten_bind_btDbvtBroadphase___destroy___0"] = function() {
  return (_emscripten_bind_btDbvtBroadphase___destroy___0 = Module["_emscripten_bind_btDbvtBroadphase___destroy___0"] = Module["asm"]["emscripten_bind_btDbvtBroadphase___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_btHeightfieldTerrainShape_9 = Module["_emscripten_bind_btHeightfieldTerrainShape_btHeightfieldTerrainShape_9"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_btHeightfieldTerrainShape_9 = Module["_emscripten_bind_btHeightfieldTerrainShape_btHeightfieldTerrainShape_9"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_btHeightfieldTerrainShape_9"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_setMargin_1 = Module["_emscripten_bind_btHeightfieldTerrainShape_setMargin_1"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_setMargin_1 = Module["_emscripten_bind_btHeightfieldTerrainShape_setMargin_1"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_getMargin_0 = Module["_emscripten_bind_btHeightfieldTerrainShape_getMargin_0"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_getMargin_0 = Module["_emscripten_bind_btHeightfieldTerrainShape_getMargin_0"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_setLocalScaling_1 = Module["_emscripten_bind_btHeightfieldTerrainShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_setLocalScaling_1 = Module["_emscripten_bind_btHeightfieldTerrainShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_getLocalScaling_0 = Module["_emscripten_bind_btHeightfieldTerrainShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_getLocalScaling_0 = Module["_emscripten_bind_btHeightfieldTerrainShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape_calculateLocalInertia_2 = Module["_emscripten_bind_btHeightfieldTerrainShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape_calculateLocalInertia_2 = Module["_emscripten_bind_btHeightfieldTerrainShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHeightfieldTerrainShape___destroy___0 = Module["_emscripten_bind_btHeightfieldTerrainShape___destroy___0"] = function() {
  return (_emscripten_bind_btHeightfieldTerrainShape___destroy___0 = Module["_emscripten_bind_btHeightfieldTerrainShape___destroy___0"] = Module["asm"]["emscripten_bind_btHeightfieldTerrainShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultSoftBodySolver_btDefaultSoftBodySolver_0 = Module["_emscripten_bind_btDefaultSoftBodySolver_btDefaultSoftBodySolver_0"] = function() {
  return (_emscripten_bind_btDefaultSoftBodySolver_btDefaultSoftBodySolver_0 = Module["_emscripten_bind_btDefaultSoftBodySolver_btDefaultSoftBodySolver_0"] = Module["asm"]["emscripten_bind_btDefaultSoftBodySolver_btDefaultSoftBodySolver_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultSoftBodySolver___destroy___0 = Module["_emscripten_bind_btDefaultSoftBodySolver___destroy___0"] = function() {
  return (_emscripten_bind_btDefaultSoftBodySolver___destroy___0 = Module["_emscripten_bind_btDefaultSoftBodySolver___destroy___0"] = Module["asm"]["emscripten_bind_btDefaultSoftBodySolver___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionDispatcher_btCollisionDispatcher_1 = Module["_emscripten_bind_btCollisionDispatcher_btCollisionDispatcher_1"] = function() {
  return (_emscripten_bind_btCollisionDispatcher_btCollisionDispatcher_1 = Module["_emscripten_bind_btCollisionDispatcher_btCollisionDispatcher_1"] = Module["asm"]["emscripten_bind_btCollisionDispatcher_btCollisionDispatcher_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionDispatcher_getNumManifolds_0 = Module["_emscripten_bind_btCollisionDispatcher_getNumManifolds_0"] = function() {
  return (_emscripten_bind_btCollisionDispatcher_getNumManifolds_0 = Module["_emscripten_bind_btCollisionDispatcher_getNumManifolds_0"] = Module["asm"]["emscripten_bind_btCollisionDispatcher_getNumManifolds_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionDispatcher_getManifoldByIndexInternal_1 = Module["_emscripten_bind_btCollisionDispatcher_getManifoldByIndexInternal_1"] = function() {
  return (_emscripten_bind_btCollisionDispatcher_getManifoldByIndexInternal_1 = Module["_emscripten_bind_btCollisionDispatcher_getManifoldByIndexInternal_1"] = Module["asm"]["emscripten_bind_btCollisionDispatcher_getManifoldByIndexInternal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionDispatcher___destroy___0 = Module["_emscripten_bind_btCollisionDispatcher___destroy___0"] = function() {
  return (_emscripten_bind_btCollisionDispatcher___destroy___0 = Module["_emscripten_bind_btCollisionDispatcher___destroy___0"] = Module["asm"]["emscripten_bind_btCollisionDispatcher___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btAxisSweep3_btAxisSweep3_2 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_2"] = function() {
  return (_emscripten_bind_btAxisSweep3_btAxisSweep3_2 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_2"] = Module["asm"]["emscripten_bind_btAxisSweep3_btAxisSweep3_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btAxisSweep3_btAxisSweep3_3 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_3"] = function() {
  return (_emscripten_bind_btAxisSweep3_btAxisSweep3_3 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_3"] = Module["asm"]["emscripten_bind_btAxisSweep3_btAxisSweep3_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btAxisSweep3_btAxisSweep3_4 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_4"] = function() {
  return (_emscripten_bind_btAxisSweep3_btAxisSweep3_4 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_4"] = Module["asm"]["emscripten_bind_btAxisSweep3_btAxisSweep3_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btAxisSweep3_btAxisSweep3_5 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_5"] = function() {
  return (_emscripten_bind_btAxisSweep3_btAxisSweep3_5 = Module["_emscripten_bind_btAxisSweep3_btAxisSweep3_5"] = Module["asm"]["emscripten_bind_btAxisSweep3_btAxisSweep3_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btAxisSweep3___destroy___0 = Module["_emscripten_bind_btAxisSweep3___destroy___0"] = function() {
  return (_emscripten_bind_btAxisSweep3___destroy___0 = Module["_emscripten_bind_btAxisSweep3___destroy___0"] = Module["asm"]["emscripten_bind_btAxisSweep3___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_VoidPtr___destroy___0 = Module["_emscripten_bind_VoidPtr___destroy___0"] = function() {
  return (_emscripten_bind_VoidPtr___destroy___0 = Module["_emscripten_bind_VoidPtr___destroy___0"] = Module["asm"]["emscripten_bind_VoidPtr___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_btSoftBodyWorldInfo_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_btSoftBodyWorldInfo_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_btSoftBodyWorldInfo_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_btSoftBodyWorldInfo_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_btSoftBodyWorldInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_air_density_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_air_density_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_air_density_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_air_density_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_air_density_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_air_density_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_air_density_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_air_density_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_air_density_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_air_density_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_water_density_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_density_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_water_density_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_density_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_water_density_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_water_density_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_density_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_water_density_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_density_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_water_density_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_water_offset_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_offset_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_water_offset_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_offset_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_water_offset_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_water_offset_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_offset_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_water_offset_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_offset_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_water_offset_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_m_maxDisplacement_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_maxDisplacement_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_m_maxDisplacement_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_maxDisplacement_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_m_maxDisplacement_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_m_maxDisplacement_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_maxDisplacement_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_m_maxDisplacement_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_maxDisplacement_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_m_maxDisplacement_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_water_normal_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_normal_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_water_normal_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_water_normal_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_water_normal_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_water_normal_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_normal_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_water_normal_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_water_normal_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_water_normal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_m_broadphase_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_broadphase_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_m_broadphase_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_broadphase_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_m_broadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_m_broadphase_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_broadphase_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_m_broadphase_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_broadphase_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_m_broadphase_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_m_dispatcher_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_dispatcher_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_m_dispatcher_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_dispatcher_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_m_dispatcher_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_m_dispatcher_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_dispatcher_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_m_dispatcher_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_dispatcher_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_m_dispatcher_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_get_m_gravity_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_gravity_0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_get_m_gravity_0 = Module["_emscripten_bind_btSoftBodyWorldInfo_get_m_gravity_0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_get_m_gravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo_set_m_gravity_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_gravity_1"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo_set_m_gravity_1 = Module["_emscripten_bind_btSoftBodyWorldInfo_set_m_gravity_1"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo_set_m_gravity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyWorldInfo___destroy___0 = Module["_emscripten_bind_btSoftBodyWorldInfo___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBodyWorldInfo___destroy___0 = Module["_emscripten_bind_btSoftBodyWorldInfo___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBodyWorldInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_2 = Module["_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_2"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_2 = Module["_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_2"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_4 = Module["_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_4"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_4 = Module["_emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_4"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_btConeTwistConstraint_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setLimit_2 = Module["_emscripten_bind_btConeTwistConstraint_setLimit_2"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setLimit_2 = Module["_emscripten_bind_btConeTwistConstraint_setLimit_2"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setLimit_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setAngularOnly_1 = Module["_emscripten_bind_btConeTwistConstraint_setAngularOnly_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setAngularOnly_1 = Module["_emscripten_bind_btConeTwistConstraint_setAngularOnly_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setAngularOnly_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setDamping_1 = Module["_emscripten_bind_btConeTwistConstraint_setDamping_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setDamping_1 = Module["_emscripten_bind_btConeTwistConstraint_setDamping_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setDamping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_enableMotor_1 = Module["_emscripten_bind_btConeTwistConstraint_enableMotor_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_enableMotor_1 = Module["_emscripten_bind_btConeTwistConstraint_enableMotor_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_enableMotor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setMaxMotorImpulse_1 = Module["_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulse_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulse_1 = Module["_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulse_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setMaxMotorImpulse_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setMaxMotorImpulseNormalized_1 = Module["_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulseNormalized_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulseNormalized_1 = Module["_emscripten_bind_btConeTwistConstraint_setMaxMotorImpulseNormalized_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setMaxMotorImpulseNormalized_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setMotorTarget_1 = Module["_emscripten_bind_btConeTwistConstraint_setMotorTarget_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setMotorTarget_1 = Module["_emscripten_bind_btConeTwistConstraint_setMotorTarget_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setMotorTarget_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setMotorTargetInConstraintSpace_1 = Module["_emscripten_bind_btConeTwistConstraint_setMotorTargetInConstraintSpace_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setMotorTargetInConstraintSpace_1 = Module["_emscripten_bind_btConeTwistConstraint_setMotorTargetInConstraintSpace_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setMotorTargetInConstraintSpace_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_enableFeedback_1 = Module["_emscripten_bind_btConeTwistConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_enableFeedback_1 = Module["_emscripten_bind_btConeTwistConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btConeTwistConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btConeTwistConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btConeTwistConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btConeTwistConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_getParam_2 = Module["_emscripten_bind_btConeTwistConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_getParam_2 = Module["_emscripten_bind_btConeTwistConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint_setParam_3 = Module["_emscripten_bind_btConeTwistConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btConeTwistConstraint_setParam_3 = Module["_emscripten_bind_btConeTwistConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btConeTwistConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeTwistConstraint___destroy___0 = Module["_emscripten_bind_btConeTwistConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btConeTwistConstraint___destroy___0 = Module["_emscripten_bind_btConeTwistConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btConeTwistConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_2 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_2"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_2 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_2"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_3 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_3"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_3 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_3"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_4 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_4"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_4 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_4"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_5 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_5"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_5 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_5"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_6 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_6"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_6 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_6"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_6"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_btHingeConstraint_7 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_7"] = function() {
  return (_emscripten_bind_btHingeConstraint_btHingeConstraint_7 = Module["_emscripten_bind_btHingeConstraint_btHingeConstraint_7"] = Module["asm"]["emscripten_bind_btHingeConstraint_btHingeConstraint_7"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setLimit_4 = Module["_emscripten_bind_btHingeConstraint_setLimit_4"] = function() {
  return (_emscripten_bind_btHingeConstraint_setLimit_4 = Module["_emscripten_bind_btHingeConstraint_setLimit_4"] = Module["asm"]["emscripten_bind_btHingeConstraint_setLimit_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setLimit_5 = Module["_emscripten_bind_btHingeConstraint_setLimit_5"] = function() {
  return (_emscripten_bind_btHingeConstraint_setLimit_5 = Module["_emscripten_bind_btHingeConstraint_setLimit_5"] = Module["asm"]["emscripten_bind_btHingeConstraint_setLimit_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_enableAngularMotor_3 = Module["_emscripten_bind_btHingeConstraint_enableAngularMotor_3"] = function() {
  return (_emscripten_bind_btHingeConstraint_enableAngularMotor_3 = Module["_emscripten_bind_btHingeConstraint_enableAngularMotor_3"] = Module["asm"]["emscripten_bind_btHingeConstraint_enableAngularMotor_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setAngularOnly_1 = Module["_emscripten_bind_btHingeConstraint_setAngularOnly_1"] = function() {
  return (_emscripten_bind_btHingeConstraint_setAngularOnly_1 = Module["_emscripten_bind_btHingeConstraint_setAngularOnly_1"] = Module["asm"]["emscripten_bind_btHingeConstraint_setAngularOnly_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_enableMotor_1 = Module["_emscripten_bind_btHingeConstraint_enableMotor_1"] = function() {
  return (_emscripten_bind_btHingeConstraint_enableMotor_1 = Module["_emscripten_bind_btHingeConstraint_enableMotor_1"] = Module["asm"]["emscripten_bind_btHingeConstraint_enableMotor_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setMaxMotorImpulse_1 = Module["_emscripten_bind_btHingeConstraint_setMaxMotorImpulse_1"] = function() {
  return (_emscripten_bind_btHingeConstraint_setMaxMotorImpulse_1 = Module["_emscripten_bind_btHingeConstraint_setMaxMotorImpulse_1"] = Module["asm"]["emscripten_bind_btHingeConstraint_setMaxMotorImpulse_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setMotorTarget_2 = Module["_emscripten_bind_btHingeConstraint_setMotorTarget_2"] = function() {
  return (_emscripten_bind_btHingeConstraint_setMotorTarget_2 = Module["_emscripten_bind_btHingeConstraint_setMotorTarget_2"] = Module["asm"]["emscripten_bind_btHingeConstraint_setMotorTarget_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_enableFeedback_1 = Module["_emscripten_bind_btHingeConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btHingeConstraint_enableFeedback_1 = Module["_emscripten_bind_btHingeConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btHingeConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btHingeConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btHingeConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btHingeConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btHingeConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btHingeConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btHingeConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btHingeConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btHingeConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_getParam_2 = Module["_emscripten_bind_btHingeConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btHingeConstraint_getParam_2 = Module["_emscripten_bind_btHingeConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btHingeConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint_setParam_3 = Module["_emscripten_bind_btHingeConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btHingeConstraint_setParam_3 = Module["_emscripten_bind_btHingeConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btHingeConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btHingeConstraint___destroy___0 = Module["_emscripten_bind_btHingeConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btHingeConstraint___destroy___0 = Module["_emscripten_bind_btHingeConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btHingeConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeZ_btConeShapeZ_2 = Module["_emscripten_bind_btConeShapeZ_btConeShapeZ_2"] = function() {
  return (_emscripten_bind_btConeShapeZ_btConeShapeZ_2 = Module["_emscripten_bind_btConeShapeZ_btConeShapeZ_2"] = Module["asm"]["emscripten_bind_btConeShapeZ_btConeShapeZ_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btConeShapeZ_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConeShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btConeShapeZ_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConeShapeZ_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btConeShapeZ_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConeShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btConeShapeZ_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConeShapeZ_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShapeZ_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConeShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShapeZ_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConeShapeZ_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeZ___destroy___0 = Module["_emscripten_bind_btConeShapeZ___destroy___0"] = function() {
  return (_emscripten_bind_btConeShapeZ___destroy___0 = Module["_emscripten_bind_btConeShapeZ___destroy___0"] = Module["asm"]["emscripten_bind_btConeShapeZ___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeX_btConeShapeX_2 = Module["_emscripten_bind_btConeShapeX_btConeShapeX_2"] = function() {
  return (_emscripten_bind_btConeShapeX_btConeShapeX_2 = Module["_emscripten_bind_btConeShapeX_btConeShapeX_2"] = Module["asm"]["emscripten_bind_btConeShapeX_btConeShapeX_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeX_setLocalScaling_1 = Module["_emscripten_bind_btConeShapeX_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConeShapeX_setLocalScaling_1 = Module["_emscripten_bind_btConeShapeX_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConeShapeX_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeX_getLocalScaling_0 = Module["_emscripten_bind_btConeShapeX_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConeShapeX_getLocalScaling_0 = Module["_emscripten_bind_btConeShapeX_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConeShapeX_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShapeX_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConeShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btConeShapeX_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConeShapeX_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConeShapeX___destroy___0 = Module["_emscripten_bind_btConeShapeX___destroy___0"] = function() {
  return (_emscripten_bind_btConeShapeX___destroy___0 = Module["_emscripten_bind_btConeShapeX___destroy___0"] = Module["asm"]["emscripten_bind_btConeShapeX___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_btTriangleMesh_0 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_0"] = function() {
  return (_emscripten_bind_btTriangleMesh_btTriangleMesh_0 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_0"] = Module["asm"]["emscripten_bind_btTriangleMesh_btTriangleMesh_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_btTriangleMesh_1 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_1"] = function() {
  return (_emscripten_bind_btTriangleMesh_btTriangleMesh_1 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_1"] = Module["asm"]["emscripten_bind_btTriangleMesh_btTriangleMesh_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_btTriangleMesh_2 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_2"] = function() {
  return (_emscripten_bind_btTriangleMesh_btTriangleMesh_2 = Module["_emscripten_bind_btTriangleMesh_btTriangleMesh_2"] = Module["asm"]["emscripten_bind_btTriangleMesh_btTriangleMesh_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_addTriangle_3 = Module["_emscripten_bind_btTriangleMesh_addTriangle_3"] = function() {
  return (_emscripten_bind_btTriangleMesh_addTriangle_3 = Module["_emscripten_bind_btTriangleMesh_addTriangle_3"] = Module["asm"]["emscripten_bind_btTriangleMesh_addTriangle_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_addTriangle_4 = Module["_emscripten_bind_btTriangleMesh_addTriangle_4"] = function() {
  return (_emscripten_bind_btTriangleMesh_addTriangle_4 = Module["_emscripten_bind_btTriangleMesh_addTriangle_4"] = Module["asm"]["emscripten_bind_btTriangleMesh_addTriangle_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_findOrAddVertex_2 = Module["_emscripten_bind_btTriangleMesh_findOrAddVertex_2"] = function() {
  return (_emscripten_bind_btTriangleMesh_findOrAddVertex_2 = Module["_emscripten_bind_btTriangleMesh_findOrAddVertex_2"] = Module["asm"]["emscripten_bind_btTriangleMesh_findOrAddVertex_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_addIndex_1 = Module["_emscripten_bind_btTriangleMesh_addIndex_1"] = function() {
  return (_emscripten_bind_btTriangleMesh_addIndex_1 = Module["_emscripten_bind_btTriangleMesh_addIndex_1"] = Module["asm"]["emscripten_bind_btTriangleMesh_addIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_getIndexedMeshArray_0 = Module["_emscripten_bind_btTriangleMesh_getIndexedMeshArray_0"] = function() {
  return (_emscripten_bind_btTriangleMesh_getIndexedMeshArray_0 = Module["_emscripten_bind_btTriangleMesh_getIndexedMeshArray_0"] = Module["asm"]["emscripten_bind_btTriangleMesh_getIndexedMeshArray_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh_setScaling_1 = Module["_emscripten_bind_btTriangleMesh_setScaling_1"] = function() {
  return (_emscripten_bind_btTriangleMesh_setScaling_1 = Module["_emscripten_bind_btTriangleMesh_setScaling_1"] = Module["asm"]["emscripten_bind_btTriangleMesh_setScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTriangleMesh___destroy___0 = Module["_emscripten_bind_btTriangleMesh___destroy___0"] = function() {
  return (_emscripten_bind_btTriangleMesh___destroy___0 = Module["_emscripten_bind_btTriangleMesh___destroy___0"] = Module["asm"]["emscripten_bind_btTriangleMesh___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_btConvexHullShape_0 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_btConvexHullShape_0 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_btConvexHullShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_btConvexHullShape_1 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_1"] = function() {
  return (_emscripten_bind_btConvexHullShape_btConvexHullShape_1 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_1"] = Module["asm"]["emscripten_bind_btConvexHullShape_btConvexHullShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_btConvexHullShape_2 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_2"] = function() {
  return (_emscripten_bind_btConvexHullShape_btConvexHullShape_2 = Module["_emscripten_bind_btConvexHullShape_btConvexHullShape_2"] = Module["asm"]["emscripten_bind_btConvexHullShape_btConvexHullShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_addPoint_1 = Module["_emscripten_bind_btConvexHullShape_addPoint_1"] = function() {
  return (_emscripten_bind_btConvexHullShape_addPoint_1 = Module["_emscripten_bind_btConvexHullShape_addPoint_1"] = Module["asm"]["emscripten_bind_btConvexHullShape_addPoint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_addPoint_2 = Module["_emscripten_bind_btConvexHullShape_addPoint_2"] = function() {
  return (_emscripten_bind_btConvexHullShape_addPoint_2 = Module["_emscripten_bind_btConvexHullShape_addPoint_2"] = Module["asm"]["emscripten_bind_btConvexHullShape_addPoint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_setMargin_1 = Module["_emscripten_bind_btConvexHullShape_setMargin_1"] = function() {
  return (_emscripten_bind_btConvexHullShape_setMargin_1 = Module["_emscripten_bind_btConvexHullShape_setMargin_1"] = Module["asm"]["emscripten_bind_btConvexHullShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_getMargin_0 = Module["_emscripten_bind_btConvexHullShape_getMargin_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_getMargin_0 = Module["_emscripten_bind_btConvexHullShape_getMargin_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_getNumVertices_0 = Module["_emscripten_bind_btConvexHullShape_getNumVertices_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_getNumVertices_0 = Module["_emscripten_bind_btConvexHullShape_getNumVertices_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_getNumVertices_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_initializePolyhedralFeatures_1 = Module["_emscripten_bind_btConvexHullShape_initializePolyhedralFeatures_1"] = function() {
  return (_emscripten_bind_btConvexHullShape_initializePolyhedralFeatures_1 = Module["_emscripten_bind_btConvexHullShape_initializePolyhedralFeatures_1"] = Module["asm"]["emscripten_bind_btConvexHullShape_initializePolyhedralFeatures_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_recalcLocalAabb_0 = Module["_emscripten_bind_btConvexHullShape_recalcLocalAabb_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_recalcLocalAabb_0 = Module["_emscripten_bind_btConvexHullShape_recalcLocalAabb_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_recalcLocalAabb_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_getConvexPolyhedron_0 = Module["_emscripten_bind_btConvexHullShape_getConvexPolyhedron_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_getConvexPolyhedron_0 = Module["_emscripten_bind_btConvexHullShape_getConvexPolyhedron_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_getConvexPolyhedron_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexHullShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btConvexHullShape_setLocalScaling_1 = Module["_emscripten_bind_btConvexHullShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btConvexHullShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexHullShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btConvexHullShape_getLocalScaling_0 = Module["_emscripten_bind_btConvexHullShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btConvexHullShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexHullShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btConvexHullShape_calculateLocalInertia_2 = Module["_emscripten_bind_btConvexHullShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btConvexHullShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexHullShape___destroy___0 = Module["_emscripten_bind_btConvexHullShape___destroy___0"] = function() {
  return (_emscripten_bind_btConvexHullShape___destroy___0 = Module["_emscripten_bind_btConvexHullShape___destroy___0"] = Module["asm"]["emscripten_bind_btConvexHullShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_btVehicleTuning_0 = Module["_emscripten_bind_btVehicleTuning_btVehicleTuning_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_btVehicleTuning_0 = Module["_emscripten_bind_btVehicleTuning_btVehicleTuning_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_btVehicleTuning_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionStiffness_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionStiffness_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_suspensionStiffness_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionStiffness_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionStiffness_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_suspensionStiffness_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_suspensionCompression_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionCompression_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_suspensionCompression_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionCompression_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_suspensionCompression_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_suspensionCompression_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionCompression_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_suspensionCompression_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionCompression_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_suspensionCompression_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_suspensionDamping_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionDamping_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_suspensionDamping_0 = Module["_emscripten_bind_btVehicleTuning_get_m_suspensionDamping_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_suspensionDamping_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_suspensionDamping_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionDamping_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_suspensionDamping_1 = Module["_emscripten_bind_btVehicleTuning_set_m_suspensionDamping_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_suspensionDamping_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btVehicleTuning_get_m_maxSuspensionTravelCm_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btVehicleTuning_get_m_maxSuspensionTravelCm_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_maxSuspensionTravelCm_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btVehicleTuning_set_m_maxSuspensionTravelCm_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btVehicleTuning_set_m_maxSuspensionTravelCm_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_maxSuspensionTravelCm_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_frictionSlip_0 = Module["_emscripten_bind_btVehicleTuning_get_m_frictionSlip_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_frictionSlip_0 = Module["_emscripten_bind_btVehicleTuning_get_m_frictionSlip_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_frictionSlip_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_frictionSlip_1 = Module["_emscripten_bind_btVehicleTuning_set_m_frictionSlip_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_frictionSlip_1 = Module["_emscripten_bind_btVehicleTuning_set_m_frictionSlip_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_frictionSlip_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btVehicleTuning_get_m_maxSuspensionForce_0"] = function() {
  return (_emscripten_bind_btVehicleTuning_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btVehicleTuning_get_m_maxSuspensionForce_0"] = Module["asm"]["emscripten_bind_btVehicleTuning_get_m_maxSuspensionForce_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleTuning_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btVehicleTuning_set_m_maxSuspensionForce_1"] = function() {
  return (_emscripten_bind_btVehicleTuning_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btVehicleTuning_set_m_maxSuspensionForce_1"] = Module["asm"]["emscripten_bind_btVehicleTuning_set_m_maxSuspensionForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObjectWrapper_getWorldTransform_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btCollisionObjectWrapper_getWorldTransform_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btCollisionObjectWrapper_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObjectWrapper_getCollisionObject_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getCollisionObject_0"] = function() {
  return (_emscripten_bind_btCollisionObjectWrapper_getCollisionObject_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getCollisionObject_0"] = Module["asm"]["emscripten_bind_btCollisionObjectWrapper_getCollisionObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCollisionObjectWrapper_getCollisionShape_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btCollisionObjectWrapper_getCollisionShape_0 = Module["_emscripten_bind_btCollisionObjectWrapper_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btCollisionObjectWrapper_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btShapeHull_btShapeHull_1 = Module["_emscripten_bind_btShapeHull_btShapeHull_1"] = function() {
  return (_emscripten_bind_btShapeHull_btShapeHull_1 = Module["_emscripten_bind_btShapeHull_btShapeHull_1"] = Module["asm"]["emscripten_bind_btShapeHull_btShapeHull_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btShapeHull_buildHull_1 = Module["_emscripten_bind_btShapeHull_buildHull_1"] = function() {
  return (_emscripten_bind_btShapeHull_buildHull_1 = Module["_emscripten_bind_btShapeHull_buildHull_1"] = Module["asm"]["emscripten_bind_btShapeHull_buildHull_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btShapeHull_numVertices_0 = Module["_emscripten_bind_btShapeHull_numVertices_0"] = function() {
  return (_emscripten_bind_btShapeHull_numVertices_0 = Module["_emscripten_bind_btShapeHull_numVertices_0"] = Module["asm"]["emscripten_bind_btShapeHull_numVertices_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btShapeHull_getVertexPointer_0 = Module["_emscripten_bind_btShapeHull_getVertexPointer_0"] = function() {
  return (_emscripten_bind_btShapeHull_getVertexPointer_0 = Module["_emscripten_bind_btShapeHull_getVertexPointer_0"] = Module["asm"]["emscripten_bind_btShapeHull_getVertexPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btShapeHull___destroy___0 = Module["_emscripten_bind_btShapeHull___destroy___0"] = function() {
  return (_emscripten_bind_btShapeHull___destroy___0 = Module["_emscripten_bind_btShapeHull___destroy___0"] = Module["asm"]["emscripten_bind_btShapeHull___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_btDefaultMotionState_0 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_0"] = function() {
  return (_emscripten_bind_btDefaultMotionState_btDefaultMotionState_0 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_0"] = Module["asm"]["emscripten_bind_btDefaultMotionState_btDefaultMotionState_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_btDefaultMotionState_1 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_1"] = function() {
  return (_emscripten_bind_btDefaultMotionState_btDefaultMotionState_1 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_1"] = Module["asm"]["emscripten_bind_btDefaultMotionState_btDefaultMotionState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_btDefaultMotionState_2 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_2"] = function() {
  return (_emscripten_bind_btDefaultMotionState_btDefaultMotionState_2 = Module["_emscripten_bind_btDefaultMotionState_btDefaultMotionState_2"] = Module["asm"]["emscripten_bind_btDefaultMotionState_btDefaultMotionState_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_getWorldTransform_1 = Module["_emscripten_bind_btDefaultMotionState_getWorldTransform_1"] = function() {
  return (_emscripten_bind_btDefaultMotionState_getWorldTransform_1 = Module["_emscripten_bind_btDefaultMotionState_getWorldTransform_1"] = Module["asm"]["emscripten_bind_btDefaultMotionState_getWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_setWorldTransform_1 = Module["_emscripten_bind_btDefaultMotionState_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btDefaultMotionState_setWorldTransform_1 = Module["_emscripten_bind_btDefaultMotionState_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btDefaultMotionState_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_get_m_graphicsWorldTrans_0 = Module["_emscripten_bind_btDefaultMotionState_get_m_graphicsWorldTrans_0"] = function() {
  return (_emscripten_bind_btDefaultMotionState_get_m_graphicsWorldTrans_0 = Module["_emscripten_bind_btDefaultMotionState_get_m_graphicsWorldTrans_0"] = Module["asm"]["emscripten_bind_btDefaultMotionState_get_m_graphicsWorldTrans_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState_set_m_graphicsWorldTrans_1 = Module["_emscripten_bind_btDefaultMotionState_set_m_graphicsWorldTrans_1"] = function() {
  return (_emscripten_bind_btDefaultMotionState_set_m_graphicsWorldTrans_1 = Module["_emscripten_bind_btDefaultMotionState_set_m_graphicsWorldTrans_1"] = Module["asm"]["emscripten_bind_btDefaultMotionState_set_m_graphicsWorldTrans_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultMotionState___destroy___0 = Module["_emscripten_bind_btDefaultMotionState___destroy___0"] = function() {
  return (_emscripten_bind_btDefaultMotionState___destroy___0 = Module["_emscripten_bind_btDefaultMotionState___destroy___0"] = Module["asm"]["emscripten_bind_btDefaultMotionState___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_btWheelInfo_1 = Module["_emscripten_bind_btWheelInfo_btWheelInfo_1"] = function() {
  return (_emscripten_bind_btWheelInfo_btWheelInfo_1 = Module["_emscripten_bind_btWheelInfo_btWheelInfo_1"] = Module["asm"]["emscripten_bind_btWheelInfo_btWheelInfo_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_getSuspensionRestLength_0 = Module["_emscripten_bind_btWheelInfo_getSuspensionRestLength_0"] = function() {
  return (_emscripten_bind_btWheelInfo_getSuspensionRestLength_0 = Module["_emscripten_bind_btWheelInfo_getSuspensionRestLength_0"] = Module["asm"]["emscripten_bind_btWheelInfo_getSuspensionRestLength_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_updateWheel_2 = Module["_emscripten_bind_btWheelInfo_updateWheel_2"] = function() {
  return (_emscripten_bind_btWheelInfo_updateWheel_2 = Module["_emscripten_bind_btWheelInfo_updateWheel_2"] = Module["asm"]["emscripten_bind_btWheelInfo_updateWheel_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionStiffness_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_suspensionStiffness_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionStiffness_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_suspensionStiffness_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionStiffness_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_suspensionStiffness_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionStiffness_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_suspensionStiffness_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_frictionSlip_0 = Module["_emscripten_bind_btWheelInfo_get_m_frictionSlip_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_frictionSlip_0 = Module["_emscripten_bind_btWheelInfo_get_m_frictionSlip_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_frictionSlip_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_frictionSlip_1 = Module["_emscripten_bind_btWheelInfo_set_m_frictionSlip_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_frictionSlip_1 = Module["_emscripten_bind_btWheelInfo_set_m_frictionSlip_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_frictionSlip_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_engineForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_engineForce_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_engineForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_engineForce_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_engineForce_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_engineForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_engineForce_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_engineForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_engineForce_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_engineForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_rollInfluence_0 = Module["_emscripten_bind_btWheelInfo_get_m_rollInfluence_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_rollInfluence_0 = Module["_emscripten_bind_btWheelInfo_get_m_rollInfluence_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_rollInfluence_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_rollInfluence_1 = Module["_emscripten_bind_btWheelInfo_set_m_rollInfluence_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_rollInfluence_1 = Module["_emscripten_bind_btWheelInfo_set_m_rollInfluence_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_rollInfluence_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_suspensionRestLength1_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionRestLength1_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_suspensionRestLength1_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionRestLength1_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_suspensionRestLength1_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_suspensionRestLength1_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionRestLength1_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_suspensionRestLength1_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionRestLength1_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_suspensionRestLength1_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelsRadius_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsRadius_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelsRadius_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsRadius_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelsRadius_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelsRadius_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsRadius_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelsRadius_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsRadius_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelsRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelsDampingCompression_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsDampingCompression_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelsDampingCompression_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsDampingCompression_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelsDampingCompression_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelsDampingCompression_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsDampingCompression_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelsDampingCompression_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsDampingCompression_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelsDampingCompression_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelsDampingRelaxation_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsDampingRelaxation_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelsDampingRelaxation_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsDampingRelaxation_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelsDampingRelaxation_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelsDampingRelaxation_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsDampingRelaxation_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelsDampingRelaxation_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsDampingRelaxation_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelsDampingRelaxation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_steering_0 = Module["_emscripten_bind_btWheelInfo_get_m_steering_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_steering_0 = Module["_emscripten_bind_btWheelInfo_get_m_steering_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_steering_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_steering_1 = Module["_emscripten_bind_btWheelInfo_set_m_steering_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_steering_1 = Module["_emscripten_bind_btWheelInfo_set_m_steering_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_steering_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_maxSuspensionForce_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_maxSuspensionForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_maxSuspensionForce_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_maxSuspensionForce_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_maxSuspensionForce_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_maxSuspensionForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_maxSuspensionForce_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_maxSuspensionForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btWheelInfo_get_m_maxSuspensionTravelCm_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_maxSuspensionTravelCm_0 = Module["_emscripten_bind_btWheelInfo_get_m_maxSuspensionTravelCm_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_maxSuspensionTravelCm_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btWheelInfo_set_m_maxSuspensionTravelCm_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_maxSuspensionTravelCm_1 = Module["_emscripten_bind_btWheelInfo_set_m_maxSuspensionTravelCm_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_maxSuspensionTravelCm_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelsSuspensionForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsSuspensionForce_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelsSuspensionForce_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelsSuspensionForce_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelsSuspensionForce_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelsSuspensionForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsSuspensionForce_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelsSuspensionForce_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelsSuspensionForce_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelsSuspensionForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_bIsFrontWheel_0 = Module["_emscripten_bind_btWheelInfo_get_m_bIsFrontWheel_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_bIsFrontWheel_0 = Module["_emscripten_bind_btWheelInfo_get_m_bIsFrontWheel_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_bIsFrontWheel_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_bIsFrontWheel_1 = Module["_emscripten_bind_btWheelInfo_set_m_bIsFrontWheel_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_bIsFrontWheel_1 = Module["_emscripten_bind_btWheelInfo_set_m_bIsFrontWheel_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_bIsFrontWheel_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_raycastInfo_0 = Module["_emscripten_bind_btWheelInfo_get_m_raycastInfo_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_raycastInfo_0 = Module["_emscripten_bind_btWheelInfo_get_m_raycastInfo_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_raycastInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_raycastInfo_1 = Module["_emscripten_bind_btWheelInfo_set_m_raycastInfo_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_raycastInfo_1 = Module["_emscripten_bind_btWheelInfo_set_m_raycastInfo_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_raycastInfo_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_chassisConnectionPointCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_chassisConnectionPointCS_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_chassisConnectionPointCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_chassisConnectionPointCS_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_chassisConnectionPointCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_chassisConnectionPointCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_chassisConnectionPointCS_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_chassisConnectionPointCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_chassisConnectionPointCS_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_chassisConnectionPointCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_worldTransform_0 = Module["_emscripten_bind_btWheelInfo_get_m_worldTransform_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_worldTransform_0 = Module["_emscripten_bind_btWheelInfo_get_m_worldTransform_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_worldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_worldTransform_1 = Module["_emscripten_bind_btWheelInfo_set_m_worldTransform_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_worldTransform_1 = Module["_emscripten_bind_btWheelInfo_set_m_worldTransform_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_worldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelDirectionCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelDirectionCS_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelDirectionCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelDirectionCS_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelDirectionCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelDirectionCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelDirectionCS_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelDirectionCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelDirectionCS_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelDirectionCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_wheelAxleCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelAxleCS_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_wheelAxleCS_0 = Module["_emscripten_bind_btWheelInfo_get_m_wheelAxleCS_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_wheelAxleCS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_wheelAxleCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelAxleCS_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_wheelAxleCS_1 = Module["_emscripten_bind_btWheelInfo_set_m_wheelAxleCS_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_wheelAxleCS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_rotation_0 = Module["_emscripten_bind_btWheelInfo_get_m_rotation_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_rotation_0 = Module["_emscripten_bind_btWheelInfo_get_m_rotation_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_rotation_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_rotation_1 = Module["_emscripten_bind_btWheelInfo_set_m_rotation_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_rotation_1 = Module["_emscripten_bind_btWheelInfo_set_m_rotation_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_rotation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_deltaRotation_0 = Module["_emscripten_bind_btWheelInfo_get_m_deltaRotation_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_deltaRotation_0 = Module["_emscripten_bind_btWheelInfo_get_m_deltaRotation_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_deltaRotation_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_deltaRotation_1 = Module["_emscripten_bind_btWheelInfo_set_m_deltaRotation_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_deltaRotation_1 = Module["_emscripten_bind_btWheelInfo_set_m_deltaRotation_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_deltaRotation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_brake_0 = Module["_emscripten_bind_btWheelInfo_get_m_brake_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_brake_0 = Module["_emscripten_bind_btWheelInfo_get_m_brake_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_brake_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_brake_1 = Module["_emscripten_bind_btWheelInfo_set_m_brake_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_brake_1 = Module["_emscripten_bind_btWheelInfo_set_m_brake_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_brake_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_clippedInvContactDotSuspension_0 = Module["_emscripten_bind_btWheelInfo_get_m_clippedInvContactDotSuspension_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_clippedInvContactDotSuspension_0 = Module["_emscripten_bind_btWheelInfo_get_m_clippedInvContactDotSuspension_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_clippedInvContactDotSuspension_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_clippedInvContactDotSuspension_1 = Module["_emscripten_bind_btWheelInfo_set_m_clippedInvContactDotSuspension_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_clippedInvContactDotSuspension_1 = Module["_emscripten_bind_btWheelInfo_set_m_clippedInvContactDotSuspension_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_clippedInvContactDotSuspension_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_suspensionRelativeVelocity_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionRelativeVelocity_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_suspensionRelativeVelocity_0 = Module["_emscripten_bind_btWheelInfo_get_m_suspensionRelativeVelocity_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_suspensionRelativeVelocity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_suspensionRelativeVelocity_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionRelativeVelocity_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_suspensionRelativeVelocity_1 = Module["_emscripten_bind_btWheelInfo_set_m_suspensionRelativeVelocity_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_suspensionRelativeVelocity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_get_m_skidInfo_0 = Module["_emscripten_bind_btWheelInfo_get_m_skidInfo_0"] = function() {
  return (_emscripten_bind_btWheelInfo_get_m_skidInfo_0 = Module["_emscripten_bind_btWheelInfo_get_m_skidInfo_0"] = Module["asm"]["emscripten_bind_btWheelInfo_get_m_skidInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo_set_m_skidInfo_1 = Module["_emscripten_bind_btWheelInfo_set_m_skidInfo_1"] = function() {
  return (_emscripten_bind_btWheelInfo_set_m_skidInfo_1 = Module["_emscripten_bind_btWheelInfo_set_m_skidInfo_1"] = Module["asm"]["emscripten_bind_btWheelInfo_set_m_skidInfo_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btWheelInfo___destroy___0 = Module["_emscripten_bind_btWheelInfo___destroy___0"] = function() {
  return (_emscripten_bind_btWheelInfo___destroy___0 = Module["_emscripten_bind_btWheelInfo___destroy___0"] = Module["asm"]["emscripten_bind_btWheelInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_btVector4_0 = Module["_emscripten_bind_btVector4_btVector4_0"] = function() {
  return (_emscripten_bind_btVector4_btVector4_0 = Module["_emscripten_bind_btVector4_btVector4_0"] = Module["asm"]["emscripten_bind_btVector4_btVector4_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_btVector4_4 = Module["_emscripten_bind_btVector4_btVector4_4"] = function() {
  return (_emscripten_bind_btVector4_btVector4_4 = Module["_emscripten_bind_btVector4_btVector4_4"] = Module["asm"]["emscripten_bind_btVector4_btVector4_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_w_0 = Module["_emscripten_bind_btVector4_w_0"] = function() {
  return (_emscripten_bind_btVector4_w_0 = Module["_emscripten_bind_btVector4_w_0"] = Module["asm"]["emscripten_bind_btVector4_w_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_setValue_4 = Module["_emscripten_bind_btVector4_setValue_4"] = function() {
  return (_emscripten_bind_btVector4_setValue_4 = Module["_emscripten_bind_btVector4_setValue_4"] = Module["asm"]["emscripten_bind_btVector4_setValue_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_length_0 = Module["_emscripten_bind_btVector4_length_0"] = function() {
  return (_emscripten_bind_btVector4_length_0 = Module["_emscripten_bind_btVector4_length_0"] = Module["asm"]["emscripten_bind_btVector4_length_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_x_0 = Module["_emscripten_bind_btVector4_x_0"] = function() {
  return (_emscripten_bind_btVector4_x_0 = Module["_emscripten_bind_btVector4_x_0"] = Module["asm"]["emscripten_bind_btVector4_x_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_y_0 = Module["_emscripten_bind_btVector4_y_0"] = function() {
  return (_emscripten_bind_btVector4_y_0 = Module["_emscripten_bind_btVector4_y_0"] = Module["asm"]["emscripten_bind_btVector4_y_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_z_0 = Module["_emscripten_bind_btVector4_z_0"] = function() {
  return (_emscripten_bind_btVector4_z_0 = Module["_emscripten_bind_btVector4_z_0"] = Module["asm"]["emscripten_bind_btVector4_z_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_setX_1 = Module["_emscripten_bind_btVector4_setX_1"] = function() {
  return (_emscripten_bind_btVector4_setX_1 = Module["_emscripten_bind_btVector4_setX_1"] = Module["asm"]["emscripten_bind_btVector4_setX_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_setY_1 = Module["_emscripten_bind_btVector4_setY_1"] = function() {
  return (_emscripten_bind_btVector4_setY_1 = Module["_emscripten_bind_btVector4_setY_1"] = Module["asm"]["emscripten_bind_btVector4_setY_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_setZ_1 = Module["_emscripten_bind_btVector4_setZ_1"] = function() {
  return (_emscripten_bind_btVector4_setZ_1 = Module["_emscripten_bind_btVector4_setZ_1"] = Module["asm"]["emscripten_bind_btVector4_setZ_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_normalize_0 = Module["_emscripten_bind_btVector4_normalize_0"] = function() {
  return (_emscripten_bind_btVector4_normalize_0 = Module["_emscripten_bind_btVector4_normalize_0"] = Module["asm"]["emscripten_bind_btVector4_normalize_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_rotate_2 = Module["_emscripten_bind_btVector4_rotate_2"] = function() {
  return (_emscripten_bind_btVector4_rotate_2 = Module["_emscripten_bind_btVector4_rotate_2"] = Module["asm"]["emscripten_bind_btVector4_rotate_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_dot_1 = Module["_emscripten_bind_btVector4_dot_1"] = function() {
  return (_emscripten_bind_btVector4_dot_1 = Module["_emscripten_bind_btVector4_dot_1"] = Module["asm"]["emscripten_bind_btVector4_dot_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_op_mul_1 = Module["_emscripten_bind_btVector4_op_mul_1"] = function() {
  return (_emscripten_bind_btVector4_op_mul_1 = Module["_emscripten_bind_btVector4_op_mul_1"] = Module["asm"]["emscripten_bind_btVector4_op_mul_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_op_add_1 = Module["_emscripten_bind_btVector4_op_add_1"] = function() {
  return (_emscripten_bind_btVector4_op_add_1 = Module["_emscripten_bind_btVector4_op_add_1"] = Module["asm"]["emscripten_bind_btVector4_op_add_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4_op_sub_1 = Module["_emscripten_bind_btVector4_op_sub_1"] = function() {
  return (_emscripten_bind_btVector4_op_sub_1 = Module["_emscripten_bind_btVector4_op_sub_1"] = Module["asm"]["emscripten_bind_btVector4_op_sub_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector4___destroy___0 = Module["_emscripten_bind_btVector4___destroy___0"] = function() {
  return (_emscripten_bind_btVector4___destroy___0 = Module["_emscripten_bind_btVector4___destroy___0"] = Module["asm"]["emscripten_bind_btVector4___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultCollisionConstructionInfo_btDefaultCollisionConstructionInfo_0 = Module["_emscripten_bind_btDefaultCollisionConstructionInfo_btDefaultCollisionConstructionInfo_0"] = function() {
  return (_emscripten_bind_btDefaultCollisionConstructionInfo_btDefaultCollisionConstructionInfo_0 = Module["_emscripten_bind_btDefaultCollisionConstructionInfo_btDefaultCollisionConstructionInfo_0"] = Module["asm"]["emscripten_bind_btDefaultCollisionConstructionInfo_btDefaultCollisionConstructionInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btDefaultCollisionConstructionInfo___destroy___0 = Module["_emscripten_bind_btDefaultCollisionConstructionInfo___destroy___0"] = function() {
  return (_emscripten_bind_btDefaultCollisionConstructionInfo___destroy___0 = Module["_emscripten_bind_btDefaultCollisionConstructionInfo___destroy___0"] = Module["asm"]["emscripten_bind_btDefaultCollisionConstructionInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_node_0 = Module["_emscripten_bind_Anchor_get_m_node_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_node_0 = Module["_emscripten_bind_Anchor_get_m_node_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_node_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_node_1 = Module["_emscripten_bind_Anchor_set_m_node_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_node_1 = Module["_emscripten_bind_Anchor_set_m_node_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_node_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_local_0 = Module["_emscripten_bind_Anchor_get_m_local_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_local_0 = Module["_emscripten_bind_Anchor_get_m_local_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_local_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_local_1 = Module["_emscripten_bind_Anchor_set_m_local_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_local_1 = Module["_emscripten_bind_Anchor_set_m_local_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_local_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_body_0 = Module["_emscripten_bind_Anchor_get_m_body_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_body_0 = Module["_emscripten_bind_Anchor_get_m_body_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_body_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_body_1 = Module["_emscripten_bind_Anchor_set_m_body_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_body_1 = Module["_emscripten_bind_Anchor_set_m_body_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_body_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_influence_0 = Module["_emscripten_bind_Anchor_get_m_influence_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_influence_0 = Module["_emscripten_bind_Anchor_get_m_influence_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_influence_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_influence_1 = Module["_emscripten_bind_Anchor_set_m_influence_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_influence_1 = Module["_emscripten_bind_Anchor_set_m_influence_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_influence_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_c0_0 = Module["_emscripten_bind_Anchor_get_m_c0_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_c0_0 = Module["_emscripten_bind_Anchor_get_m_c0_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_c0_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_c0_1 = Module["_emscripten_bind_Anchor_set_m_c0_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_c0_1 = Module["_emscripten_bind_Anchor_set_m_c0_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_c0_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_c1_0 = Module["_emscripten_bind_Anchor_get_m_c1_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_c1_0 = Module["_emscripten_bind_Anchor_get_m_c1_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_c1_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_c1_1 = Module["_emscripten_bind_Anchor_set_m_c1_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_c1_1 = Module["_emscripten_bind_Anchor_set_m_c1_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_c1_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_get_m_c2_0 = Module["_emscripten_bind_Anchor_get_m_c2_0"] = function() {
  return (_emscripten_bind_Anchor_get_m_c2_0 = Module["_emscripten_bind_Anchor_get_m_c2_0"] = Module["asm"]["emscripten_bind_Anchor_get_m_c2_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor_set_m_c2_1 = Module["_emscripten_bind_Anchor_set_m_c2_1"] = function() {
  return (_emscripten_bind_Anchor_set_m_c2_1 = Module["_emscripten_bind_Anchor_set_m_c2_1"] = Module["asm"]["emscripten_bind_Anchor_set_m_c2_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Anchor___destroy___0 = Module["_emscripten_bind_Anchor___destroy___0"] = function() {
  return (_emscripten_bind_Anchor___destroy___0 = Module["_emscripten_bind_Anchor___destroy___0"] = Module["asm"]["emscripten_bind_Anchor___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_get_m_hitPointInWorld_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_hitPointInWorld_0"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_get_m_hitPointInWorld_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_hitPointInWorld_0"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_get_m_hitPointInWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_set_m_hitPointInWorld_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_hitPointInWorld_1"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_set_m_hitPointInWorld_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_hitPointInWorld_1"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_set_m_hitPointInWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_get_m_hitNormalInWorld_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_hitNormalInWorld_0"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_get_m_hitNormalInWorld_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_hitNormalInWorld_0"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_get_m_hitNormalInWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_set_m_hitNormalInWorld_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_hitNormalInWorld_1"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_set_m_hitNormalInWorld_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_hitNormalInWorld_1"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_set_m_hitNormalInWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_get_m_distFraction_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_distFraction_0"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_get_m_distFraction_0 = Module["_emscripten_bind_btVehicleRaycasterResult_get_m_distFraction_0"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_get_m_distFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult_set_m_distFraction_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_distFraction_1"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult_set_m_distFraction_1 = Module["_emscripten_bind_btVehicleRaycasterResult_set_m_distFraction_1"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult_set_m_distFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVehicleRaycasterResult___destroy___0 = Module["_emscripten_bind_btVehicleRaycasterResult___destroy___0"] = function() {
  return (_emscripten_bind_btVehicleRaycasterResult___destroy___0 = Module["_emscripten_bind_btVehicleRaycasterResult___destroy___0"] = Module["asm"]["emscripten_bind_btVehicleRaycasterResult___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3Array_size_0 = Module["_emscripten_bind_btVector3Array_size_0"] = function() {
  return (_emscripten_bind_btVector3Array_size_0 = Module["_emscripten_bind_btVector3Array_size_0"] = Module["asm"]["emscripten_bind_btVector3Array_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3Array_at_1 = Module["_emscripten_bind_btVector3Array_at_1"] = function() {
  return (_emscripten_bind_btVector3Array_at_1 = Module["_emscripten_bind_btVector3Array_at_1"] = Module["asm"]["emscripten_bind_btVector3Array_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btVector3Array___destroy___0 = Module["_emscripten_bind_btVector3Array___destroy___0"] = function() {
  return (_emscripten_bind_btVector3Array___destroy___0 = Module["_emscripten_bind_btVector3Array___destroy___0"] = Module["asm"]["emscripten_bind_btVector3Array___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstraintSolver___destroy___0 = Module["_emscripten_bind_btConstraintSolver___destroy___0"] = function() {
  return (_emscripten_bind_btConstraintSolver___destroy___0 = Module["_emscripten_bind_btConstraintSolver___destroy___0"] = Module["asm"]["emscripten_bind_btConstraintSolver___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_btRaycastVehicle_3 = Module["_emscripten_bind_btRaycastVehicle_btRaycastVehicle_3"] = function() {
  return (_emscripten_bind_btRaycastVehicle_btRaycastVehicle_3 = Module["_emscripten_bind_btRaycastVehicle_btRaycastVehicle_3"] = Module["asm"]["emscripten_bind_btRaycastVehicle_btRaycastVehicle_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_applyEngineForce_2 = Module["_emscripten_bind_btRaycastVehicle_applyEngineForce_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_applyEngineForce_2 = Module["_emscripten_bind_btRaycastVehicle_applyEngineForce_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_applyEngineForce_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setSteeringValue_2 = Module["_emscripten_bind_btRaycastVehicle_setSteeringValue_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setSteeringValue_2 = Module["_emscripten_bind_btRaycastVehicle_setSteeringValue_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setSteeringValue_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getWheelTransformWS_1 = Module["_emscripten_bind_btRaycastVehicle_getWheelTransformWS_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getWheelTransformWS_1 = Module["_emscripten_bind_btRaycastVehicle_getWheelTransformWS_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getWheelTransformWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateWheelTransform_2 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransform_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateWheelTransform_2 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransform_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateWheelTransform_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_addWheel_7 = Module["_emscripten_bind_btRaycastVehicle_addWheel_7"] = function() {
  return (_emscripten_bind_btRaycastVehicle_addWheel_7 = Module["_emscripten_bind_btRaycastVehicle_addWheel_7"] = Module["asm"]["emscripten_bind_btRaycastVehicle_addWheel_7"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getNumWheels_0 = Module["_emscripten_bind_btRaycastVehicle_getNumWheels_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getNumWheels_0 = Module["_emscripten_bind_btRaycastVehicle_getNumWheels_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getNumWheels_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getRigidBody_0 = Module["_emscripten_bind_btRaycastVehicle_getRigidBody_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getRigidBody_0 = Module["_emscripten_bind_btRaycastVehicle_getRigidBody_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getRigidBody_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getWheelInfo_1 = Module["_emscripten_bind_btRaycastVehicle_getWheelInfo_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getWheelInfo_1 = Module["_emscripten_bind_btRaycastVehicle_getWheelInfo_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getWheelInfo_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setBrake_2 = Module["_emscripten_bind_btRaycastVehicle_setBrake_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setBrake_2 = Module["_emscripten_bind_btRaycastVehicle_setBrake_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setBrake_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setCoordinateSystem_3 = Module["_emscripten_bind_btRaycastVehicle_setCoordinateSystem_3"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setCoordinateSystem_3 = Module["_emscripten_bind_btRaycastVehicle_setCoordinateSystem_3"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setCoordinateSystem_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getCurrentSpeedKmHour_0 = Module["_emscripten_bind_btRaycastVehicle_getCurrentSpeedKmHour_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getCurrentSpeedKmHour_0 = Module["_emscripten_bind_btRaycastVehicle_getCurrentSpeedKmHour_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getCurrentSpeedKmHour_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getChassisWorldTransform_0 = Module["_emscripten_bind_btRaycastVehicle_getChassisWorldTransform_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getChassisWorldTransform_0 = Module["_emscripten_bind_btRaycastVehicle_getChassisWorldTransform_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getChassisWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_rayCast_1 = Module["_emscripten_bind_btRaycastVehicle_rayCast_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_rayCast_1 = Module["_emscripten_bind_btRaycastVehicle_rayCast_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_rayCast_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateVehicle_1 = Module["_emscripten_bind_btRaycastVehicle_updateVehicle_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateVehicle_1 = Module["_emscripten_bind_btRaycastVehicle_updateVehicle_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateVehicle_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_resetSuspension_0 = Module["_emscripten_bind_btRaycastVehicle_resetSuspension_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_resetSuspension_0 = Module["_emscripten_bind_btRaycastVehicle_resetSuspension_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_resetSuspension_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getSteeringValue_1 = Module["_emscripten_bind_btRaycastVehicle_getSteeringValue_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getSteeringValue_1 = Module["_emscripten_bind_btRaycastVehicle_getSteeringValue_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getSteeringValue_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_1 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_1 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_2 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_2 = Module["_emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateWheelTransformsWS_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setPitchControl_1 = Module["_emscripten_bind_btRaycastVehicle_setPitchControl_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setPitchControl_1 = Module["_emscripten_bind_btRaycastVehicle_setPitchControl_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setPitchControl_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateSuspension_1 = Module["_emscripten_bind_btRaycastVehicle_updateSuspension_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateSuspension_1 = Module["_emscripten_bind_btRaycastVehicle_updateSuspension_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateSuspension_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateFriction_1 = Module["_emscripten_bind_btRaycastVehicle_updateFriction_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateFriction_1 = Module["_emscripten_bind_btRaycastVehicle_updateFriction_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getRightAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getRightAxis_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getRightAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getRightAxis_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getRightAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getUpAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getUpAxis_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getUpAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getUpAxis_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getUpAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getForwardAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getForwardAxis_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getForwardAxis_0 = Module["_emscripten_bind_btRaycastVehicle_getForwardAxis_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getForwardAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getForwardVector_0 = Module["_emscripten_bind_btRaycastVehicle_getForwardVector_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getForwardVector_0 = Module["_emscripten_bind_btRaycastVehicle_getForwardVector_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getForwardVector_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getUserConstraintType_0 = Module["_emscripten_bind_btRaycastVehicle_getUserConstraintType_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getUserConstraintType_0 = Module["_emscripten_bind_btRaycastVehicle_getUserConstraintType_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getUserConstraintType_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setUserConstraintType_1 = Module["_emscripten_bind_btRaycastVehicle_setUserConstraintType_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setUserConstraintType_1 = Module["_emscripten_bind_btRaycastVehicle_setUserConstraintType_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setUserConstraintType_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_setUserConstraintId_1 = Module["_emscripten_bind_btRaycastVehicle_setUserConstraintId_1"] = function() {
  return (_emscripten_bind_btRaycastVehicle_setUserConstraintId_1 = Module["_emscripten_bind_btRaycastVehicle_setUserConstraintId_1"] = Module["asm"]["emscripten_bind_btRaycastVehicle_setUserConstraintId_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_getUserConstraintId_0 = Module["_emscripten_bind_btRaycastVehicle_getUserConstraintId_0"] = function() {
  return (_emscripten_bind_btRaycastVehicle_getUserConstraintId_0 = Module["_emscripten_bind_btRaycastVehicle_getUserConstraintId_0"] = Module["asm"]["emscripten_bind_btRaycastVehicle_getUserConstraintId_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle_updateAction_2 = Module["_emscripten_bind_btRaycastVehicle_updateAction_2"] = function() {
  return (_emscripten_bind_btRaycastVehicle_updateAction_2 = Module["_emscripten_bind_btRaycastVehicle_updateAction_2"] = Module["asm"]["emscripten_bind_btRaycastVehicle_updateAction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btRaycastVehicle___destroy___0 = Module["_emscripten_bind_btRaycastVehicle___destroy___0"] = function() {
  return (_emscripten_bind_btRaycastVehicle___destroy___0 = Module["_emscripten_bind_btRaycastVehicle___destroy___0"] = Module["asm"]["emscripten_bind_btRaycastVehicle___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_btCylinderShapeX_1 = Module["_emscripten_bind_btCylinderShapeX_btCylinderShapeX_1"] = function() {
  return (_emscripten_bind_btCylinderShapeX_btCylinderShapeX_1 = Module["_emscripten_bind_btCylinderShapeX_btCylinderShapeX_1"] = Module["asm"]["emscripten_bind_btCylinderShapeX_btCylinderShapeX_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_setMargin_1 = Module["_emscripten_bind_btCylinderShapeX_setMargin_1"] = function() {
  return (_emscripten_bind_btCylinderShapeX_setMargin_1 = Module["_emscripten_bind_btCylinderShapeX_setMargin_1"] = Module["asm"]["emscripten_bind_btCylinderShapeX_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_getMargin_0 = Module["_emscripten_bind_btCylinderShapeX_getMargin_0"] = function() {
  return (_emscripten_bind_btCylinderShapeX_getMargin_0 = Module["_emscripten_bind_btCylinderShapeX_getMargin_0"] = Module["asm"]["emscripten_bind_btCylinderShapeX_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShapeX_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCylinderShapeX_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShapeX_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCylinderShapeX_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShapeX_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCylinderShapeX_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShapeX_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCylinderShapeX_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShapeX_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCylinderShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShapeX_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCylinderShapeX_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeX___destroy___0 = Module["_emscripten_bind_btCylinderShapeX___destroy___0"] = function() {
  return (_emscripten_bind_btCylinderShapeX___destroy___0 = Module["_emscripten_bind_btCylinderShapeX___destroy___0"] = Module["asm"]["emscripten_bind_btCylinderShapeX___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_btCylinderShapeZ_1 = Module["_emscripten_bind_btCylinderShapeZ_btCylinderShapeZ_1"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_btCylinderShapeZ_1 = Module["_emscripten_bind_btCylinderShapeZ_btCylinderShapeZ_1"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_btCylinderShapeZ_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_setMargin_1 = Module["_emscripten_bind_btCylinderShapeZ_setMargin_1"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_setMargin_1 = Module["_emscripten_bind_btCylinderShapeZ_setMargin_1"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_getMargin_0 = Module["_emscripten_bind_btCylinderShapeZ_getMargin_0"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_getMargin_0 = Module["_emscripten_bind_btCylinderShapeZ_getMargin_0"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShapeZ_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btCylinderShapeZ_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShapeZ_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btCylinderShapeZ_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShapeZ_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCylinderShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btCylinderShapeZ_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCylinderShapeZ_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCylinderShapeZ___destroy___0 = Module["_emscripten_bind_btCylinderShapeZ___destroy___0"] = function() {
  return (_emscripten_bind_btCylinderShapeZ___destroy___0 = Module["_emscripten_bind_btCylinderShapeZ___destroy___0"] = Module["asm"]["emscripten_bind_btCylinderShapeZ___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexPolyhedron_get_m_vertices_0 = Module["_emscripten_bind_btConvexPolyhedron_get_m_vertices_0"] = function() {
  return (_emscripten_bind_btConvexPolyhedron_get_m_vertices_0 = Module["_emscripten_bind_btConvexPolyhedron_get_m_vertices_0"] = Module["asm"]["emscripten_bind_btConvexPolyhedron_get_m_vertices_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexPolyhedron_set_m_vertices_1 = Module["_emscripten_bind_btConvexPolyhedron_set_m_vertices_1"] = function() {
  return (_emscripten_bind_btConvexPolyhedron_set_m_vertices_1 = Module["_emscripten_bind_btConvexPolyhedron_set_m_vertices_1"] = Module["asm"]["emscripten_bind_btConvexPolyhedron_set_m_vertices_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexPolyhedron_get_m_faces_0 = Module["_emscripten_bind_btConvexPolyhedron_get_m_faces_0"] = function() {
  return (_emscripten_bind_btConvexPolyhedron_get_m_faces_0 = Module["_emscripten_bind_btConvexPolyhedron_get_m_faces_0"] = Module["asm"]["emscripten_bind_btConvexPolyhedron_get_m_faces_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexPolyhedron_set_m_faces_1 = Module["_emscripten_bind_btConvexPolyhedron_set_m_faces_1"] = function() {
  return (_emscripten_bind_btConvexPolyhedron_set_m_faces_1 = Module["_emscripten_bind_btConvexPolyhedron_set_m_faces_1"] = Module["asm"]["emscripten_bind_btConvexPolyhedron_set_m_faces_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConvexPolyhedron___destroy___0 = Module["_emscripten_bind_btConvexPolyhedron___destroy___0"] = function() {
  return (_emscripten_bind_btConvexPolyhedron___destroy___0 = Module["_emscripten_bind_btConvexPolyhedron___destroy___0"] = Module["asm"]["emscripten_bind_btConvexPolyhedron___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSequentialImpulseConstraintSolver_btSequentialImpulseConstraintSolver_0 = Module["_emscripten_bind_btSequentialImpulseConstraintSolver_btSequentialImpulseConstraintSolver_0"] = function() {
  return (_emscripten_bind_btSequentialImpulseConstraintSolver_btSequentialImpulseConstraintSolver_0 = Module["_emscripten_bind_btSequentialImpulseConstraintSolver_btSequentialImpulseConstraintSolver_0"] = Module["asm"]["emscripten_bind_btSequentialImpulseConstraintSolver_btSequentialImpulseConstraintSolver_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSequentialImpulseConstraintSolver___destroy___0 = Module["_emscripten_bind_btSequentialImpulseConstraintSolver___destroy___0"] = function() {
  return (_emscripten_bind_btSequentialImpulseConstraintSolver___destroy___0 = Module["_emscripten_bind_btSequentialImpulseConstraintSolver___destroy___0"] = Module["asm"]["emscripten_bind_btSequentialImpulseConstraintSolver___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray_size_0 = Module["_emscripten_bind_tAnchorArray_size_0"] = function() {
  return (_emscripten_bind_tAnchorArray_size_0 = Module["_emscripten_bind_tAnchorArray_size_0"] = Module["asm"]["emscripten_bind_tAnchorArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray_at_1 = Module["_emscripten_bind_tAnchorArray_at_1"] = function() {
  return (_emscripten_bind_tAnchorArray_at_1 = Module["_emscripten_bind_tAnchorArray_at_1"] = Module["asm"]["emscripten_bind_tAnchorArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray_clear_0 = Module["_emscripten_bind_tAnchorArray_clear_0"] = function() {
  return (_emscripten_bind_tAnchorArray_clear_0 = Module["_emscripten_bind_tAnchorArray_clear_0"] = Module["asm"]["emscripten_bind_tAnchorArray_clear_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray_push_back_1 = Module["_emscripten_bind_tAnchorArray_push_back_1"] = function() {
  return (_emscripten_bind_tAnchorArray_push_back_1 = Module["_emscripten_bind_tAnchorArray_push_back_1"] = Module["asm"]["emscripten_bind_tAnchorArray_push_back_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray_pop_back_0 = Module["_emscripten_bind_tAnchorArray_pop_back_0"] = function() {
  return (_emscripten_bind_tAnchorArray_pop_back_0 = Module["_emscripten_bind_tAnchorArray_pop_back_0"] = Module["asm"]["emscripten_bind_tAnchorArray_pop_back_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tAnchorArray___destroy___0 = Module["_emscripten_bind_tAnchorArray___destroy___0"] = function() {
  return (_emscripten_bind_tAnchorArray___destroy___0 = Module["_emscripten_bind_tAnchorArray___destroy___0"] = Module["asm"]["emscripten_bind_tAnchorArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_contactNormalWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_contactNormalWS_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_contactNormalWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_contactNormalWS_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_contactNormalWS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_contactNormalWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_contactNormalWS_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_contactNormalWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_contactNormalWS_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_contactNormalWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_contactPointWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_contactPointWS_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_contactPointWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_contactPointWS_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_contactPointWS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_contactPointWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_contactPointWS_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_contactPointWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_contactPointWS_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_contactPointWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_suspensionLength_0 = Module["_emscripten_bind_RaycastInfo_get_m_suspensionLength_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_suspensionLength_0 = Module["_emscripten_bind_RaycastInfo_get_m_suspensionLength_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_suspensionLength_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_suspensionLength_1 = Module["_emscripten_bind_RaycastInfo_set_m_suspensionLength_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_suspensionLength_1 = Module["_emscripten_bind_RaycastInfo_set_m_suspensionLength_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_suspensionLength_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_hardPointWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_hardPointWS_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_hardPointWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_hardPointWS_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_hardPointWS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_hardPointWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_hardPointWS_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_hardPointWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_hardPointWS_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_hardPointWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_wheelDirectionWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_wheelDirectionWS_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_wheelDirectionWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_wheelDirectionWS_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_wheelDirectionWS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_wheelDirectionWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_wheelDirectionWS_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_wheelDirectionWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_wheelDirectionWS_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_wheelDirectionWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_wheelAxleWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_wheelAxleWS_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_wheelAxleWS_0 = Module["_emscripten_bind_RaycastInfo_get_m_wheelAxleWS_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_wheelAxleWS_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_wheelAxleWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_wheelAxleWS_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_wheelAxleWS_1 = Module["_emscripten_bind_RaycastInfo_set_m_wheelAxleWS_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_wheelAxleWS_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_isInContact_0 = Module["_emscripten_bind_RaycastInfo_get_m_isInContact_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_isInContact_0 = Module["_emscripten_bind_RaycastInfo_get_m_isInContact_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_isInContact_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_isInContact_1 = Module["_emscripten_bind_RaycastInfo_set_m_isInContact_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_isInContact_1 = Module["_emscripten_bind_RaycastInfo_set_m_isInContact_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_isInContact_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_get_m_groundObject_0 = Module["_emscripten_bind_RaycastInfo_get_m_groundObject_0"] = function() {
  return (_emscripten_bind_RaycastInfo_get_m_groundObject_0 = Module["_emscripten_bind_RaycastInfo_get_m_groundObject_0"] = Module["asm"]["emscripten_bind_RaycastInfo_get_m_groundObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo_set_m_groundObject_1 = Module["_emscripten_bind_RaycastInfo_set_m_groundObject_1"] = function() {
  return (_emscripten_bind_RaycastInfo_set_m_groundObject_1 = Module["_emscripten_bind_RaycastInfo_set_m_groundObject_1"] = Module["asm"]["emscripten_bind_RaycastInfo_set_m_groundObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_RaycastInfo___destroy___0 = Module["_emscripten_bind_RaycastInfo___destroy___0"] = function() {
  return (_emscripten_bind_RaycastInfo___destroy___0 = Module["_emscripten_bind_RaycastInfo___destroy___0"] = Module["asm"]["emscripten_bind_RaycastInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMultiSphereShape_btMultiSphereShape_3 = Module["_emscripten_bind_btMultiSphereShape_btMultiSphereShape_3"] = function() {
  return (_emscripten_bind_btMultiSphereShape_btMultiSphereShape_3 = Module["_emscripten_bind_btMultiSphereShape_btMultiSphereShape_3"] = Module["asm"]["emscripten_bind_btMultiSphereShape_btMultiSphereShape_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMultiSphereShape_setLocalScaling_1 = Module["_emscripten_bind_btMultiSphereShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btMultiSphereShape_setLocalScaling_1 = Module["_emscripten_bind_btMultiSphereShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btMultiSphereShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMultiSphereShape_getLocalScaling_0 = Module["_emscripten_bind_btMultiSphereShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btMultiSphereShape_getLocalScaling_0 = Module["_emscripten_bind_btMultiSphereShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btMultiSphereShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMultiSphereShape_calculateLocalInertia_2 = Module["_emscripten_bind_btMultiSphereShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btMultiSphereShape_calculateLocalInertia_2 = Module["_emscripten_bind_btMultiSphereShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btMultiSphereShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btMultiSphereShape___destroy___0 = Module["_emscripten_bind_btMultiSphereShape___destroy___0"] = function() {
  return (_emscripten_bind_btMultiSphereShape___destroy___0 = Module["_emscripten_bind_btMultiSphereShape___destroy___0"] = Module["asm"]["emscripten_bind_btMultiSphereShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_btSoftBody_4 = Module["_emscripten_bind_btSoftBody_btSoftBody_4"] = function() {
  return (_emscripten_bind_btSoftBody_btSoftBody_4 = Module["_emscripten_bind_btSoftBody_btSoftBody_4"] = Module["asm"]["emscripten_bind_btSoftBody_btSoftBody_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_checkLink_2 = Module["_emscripten_bind_btSoftBody_checkLink_2"] = function() {
  return (_emscripten_bind_btSoftBody_checkLink_2 = Module["_emscripten_bind_btSoftBody_checkLink_2"] = Module["asm"]["emscripten_bind_btSoftBody_checkLink_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_checkFace_3 = Module["_emscripten_bind_btSoftBody_checkFace_3"] = function() {
  return (_emscripten_bind_btSoftBody_checkFace_3 = Module["_emscripten_bind_btSoftBody_checkFace_3"] = Module["asm"]["emscripten_bind_btSoftBody_checkFace_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendMaterial_0 = Module["_emscripten_bind_btSoftBody_appendMaterial_0"] = function() {
  return (_emscripten_bind_btSoftBody_appendMaterial_0 = Module["_emscripten_bind_btSoftBody_appendMaterial_0"] = Module["asm"]["emscripten_bind_btSoftBody_appendMaterial_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendNode_2 = Module["_emscripten_bind_btSoftBody_appendNode_2"] = function() {
  return (_emscripten_bind_btSoftBody_appendNode_2 = Module["_emscripten_bind_btSoftBody_appendNode_2"] = Module["asm"]["emscripten_bind_btSoftBody_appendNode_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendLink_4 = Module["_emscripten_bind_btSoftBody_appendLink_4"] = function() {
  return (_emscripten_bind_btSoftBody_appendLink_4 = Module["_emscripten_bind_btSoftBody_appendLink_4"] = Module["asm"]["emscripten_bind_btSoftBody_appendLink_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendFace_4 = Module["_emscripten_bind_btSoftBody_appendFace_4"] = function() {
  return (_emscripten_bind_btSoftBody_appendFace_4 = Module["_emscripten_bind_btSoftBody_appendFace_4"] = Module["asm"]["emscripten_bind_btSoftBody_appendFace_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendTetra_5 = Module["_emscripten_bind_btSoftBody_appendTetra_5"] = function() {
  return (_emscripten_bind_btSoftBody_appendTetra_5 = Module["_emscripten_bind_btSoftBody_appendTetra_5"] = Module["asm"]["emscripten_bind_btSoftBody_appendTetra_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_appendAnchor_4 = Module["_emscripten_bind_btSoftBody_appendAnchor_4"] = function() {
  return (_emscripten_bind_btSoftBody_appendAnchor_4 = Module["_emscripten_bind_btSoftBody_appendAnchor_4"] = Module["asm"]["emscripten_bind_btSoftBody_appendAnchor_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_addForce_1 = Module["_emscripten_bind_btSoftBody_addForce_1"] = function() {
  return (_emscripten_bind_btSoftBody_addForce_1 = Module["_emscripten_bind_btSoftBody_addForce_1"] = Module["asm"]["emscripten_bind_btSoftBody_addForce_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_addForce_2 = Module["_emscripten_bind_btSoftBody_addForce_2"] = function() {
  return (_emscripten_bind_btSoftBody_addForce_2 = Module["_emscripten_bind_btSoftBody_addForce_2"] = Module["asm"]["emscripten_bind_btSoftBody_addForce_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_addAeroForceToNode_2 = Module["_emscripten_bind_btSoftBody_addAeroForceToNode_2"] = function() {
  return (_emscripten_bind_btSoftBody_addAeroForceToNode_2 = Module["_emscripten_bind_btSoftBody_addAeroForceToNode_2"] = Module["asm"]["emscripten_bind_btSoftBody_addAeroForceToNode_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getTotalMass_0 = Module["_emscripten_bind_btSoftBody_getTotalMass_0"] = function() {
  return (_emscripten_bind_btSoftBody_getTotalMass_0 = Module["_emscripten_bind_btSoftBody_getTotalMass_0"] = Module["asm"]["emscripten_bind_btSoftBody_getTotalMass_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setTotalMass_2 = Module["_emscripten_bind_btSoftBody_setTotalMass_2"] = function() {
  return (_emscripten_bind_btSoftBody_setTotalMass_2 = Module["_emscripten_bind_btSoftBody_setTotalMass_2"] = Module["asm"]["emscripten_bind_btSoftBody_setTotalMass_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setMass_2 = Module["_emscripten_bind_btSoftBody_setMass_2"] = function() {
  return (_emscripten_bind_btSoftBody_setMass_2 = Module["_emscripten_bind_btSoftBody_setMass_2"] = Module["asm"]["emscripten_bind_btSoftBody_setMass_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_transform_1 = Module["_emscripten_bind_btSoftBody_transform_1"] = function() {
  return (_emscripten_bind_btSoftBody_transform_1 = Module["_emscripten_bind_btSoftBody_transform_1"] = Module["asm"]["emscripten_bind_btSoftBody_transform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_translate_1 = Module["_emscripten_bind_btSoftBody_translate_1"] = function() {
  return (_emscripten_bind_btSoftBody_translate_1 = Module["_emscripten_bind_btSoftBody_translate_1"] = Module["asm"]["emscripten_bind_btSoftBody_translate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_rotate_1 = Module["_emscripten_bind_btSoftBody_rotate_1"] = function() {
  return (_emscripten_bind_btSoftBody_rotate_1 = Module["_emscripten_bind_btSoftBody_rotate_1"] = Module["asm"]["emscripten_bind_btSoftBody_rotate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_scale_1 = Module["_emscripten_bind_btSoftBody_scale_1"] = function() {
  return (_emscripten_bind_btSoftBody_scale_1 = Module["_emscripten_bind_btSoftBody_scale_1"] = Module["asm"]["emscripten_bind_btSoftBody_scale_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_generateClusters_1 = Module["_emscripten_bind_btSoftBody_generateClusters_1"] = function() {
  return (_emscripten_bind_btSoftBody_generateClusters_1 = Module["_emscripten_bind_btSoftBody_generateClusters_1"] = Module["asm"]["emscripten_bind_btSoftBody_generateClusters_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_generateClusters_2 = Module["_emscripten_bind_btSoftBody_generateClusters_2"] = function() {
  return (_emscripten_bind_btSoftBody_generateClusters_2 = Module["_emscripten_bind_btSoftBody_generateClusters_2"] = Module["asm"]["emscripten_bind_btSoftBody_generateClusters_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_generateBendingConstraints_2 = Module["_emscripten_bind_btSoftBody_generateBendingConstraints_2"] = function() {
  return (_emscripten_bind_btSoftBody_generateBendingConstraints_2 = Module["_emscripten_bind_btSoftBody_generateBendingConstraints_2"] = Module["asm"]["emscripten_bind_btSoftBody_generateBendingConstraints_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_upcast_1 = Module["_emscripten_bind_btSoftBody_upcast_1"] = function() {
  return (_emscripten_bind_btSoftBody_upcast_1 = Module["_emscripten_bind_btSoftBody_upcast_1"] = Module["asm"]["emscripten_bind_btSoftBody_upcast_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setAnisotropicFriction_2 = Module["_emscripten_bind_btSoftBody_setAnisotropicFriction_2"] = function() {
  return (_emscripten_bind_btSoftBody_setAnisotropicFriction_2 = Module["_emscripten_bind_btSoftBody_setAnisotropicFriction_2"] = Module["asm"]["emscripten_bind_btSoftBody_setAnisotropicFriction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getCollisionShape_0 = Module["_emscripten_bind_btSoftBody_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btSoftBody_getCollisionShape_0 = Module["_emscripten_bind_btSoftBody_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btSoftBody_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setContactProcessingThreshold_1 = Module["_emscripten_bind_btSoftBody_setContactProcessingThreshold_1"] = function() {
  return (_emscripten_bind_btSoftBody_setContactProcessingThreshold_1 = Module["_emscripten_bind_btSoftBody_setContactProcessingThreshold_1"] = Module["asm"]["emscripten_bind_btSoftBody_setContactProcessingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setActivationState_1 = Module["_emscripten_bind_btSoftBody_setActivationState_1"] = function() {
  return (_emscripten_bind_btSoftBody_setActivationState_1 = Module["_emscripten_bind_btSoftBody_setActivationState_1"] = Module["asm"]["emscripten_bind_btSoftBody_setActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_forceActivationState_1 = Module["_emscripten_bind_btSoftBody_forceActivationState_1"] = function() {
  return (_emscripten_bind_btSoftBody_forceActivationState_1 = Module["_emscripten_bind_btSoftBody_forceActivationState_1"] = Module["asm"]["emscripten_bind_btSoftBody_forceActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_activate_0 = Module["_emscripten_bind_btSoftBody_activate_0"] = function() {
  return (_emscripten_bind_btSoftBody_activate_0 = Module["_emscripten_bind_btSoftBody_activate_0"] = Module["asm"]["emscripten_bind_btSoftBody_activate_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_activate_1 = Module["_emscripten_bind_btSoftBody_activate_1"] = function() {
  return (_emscripten_bind_btSoftBody_activate_1 = Module["_emscripten_bind_btSoftBody_activate_1"] = Module["asm"]["emscripten_bind_btSoftBody_activate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_isActive_0 = Module["_emscripten_bind_btSoftBody_isActive_0"] = function() {
  return (_emscripten_bind_btSoftBody_isActive_0 = Module["_emscripten_bind_btSoftBody_isActive_0"] = Module["asm"]["emscripten_bind_btSoftBody_isActive_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_isKinematicObject_0 = Module["_emscripten_bind_btSoftBody_isKinematicObject_0"] = function() {
  return (_emscripten_bind_btSoftBody_isKinematicObject_0 = Module["_emscripten_bind_btSoftBody_isKinematicObject_0"] = Module["asm"]["emscripten_bind_btSoftBody_isKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_isStaticObject_0 = Module["_emscripten_bind_btSoftBody_isStaticObject_0"] = function() {
  return (_emscripten_bind_btSoftBody_isStaticObject_0 = Module["_emscripten_bind_btSoftBody_isStaticObject_0"] = Module["asm"]["emscripten_bind_btSoftBody_isStaticObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btSoftBody_isStaticOrKinematicObject_0"] = function() {
  return (_emscripten_bind_btSoftBody_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btSoftBody_isStaticOrKinematicObject_0"] = Module["asm"]["emscripten_bind_btSoftBody_isStaticOrKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getRestitution_0 = Module["_emscripten_bind_btSoftBody_getRestitution_0"] = function() {
  return (_emscripten_bind_btSoftBody_getRestitution_0 = Module["_emscripten_bind_btSoftBody_getRestitution_0"] = Module["asm"]["emscripten_bind_btSoftBody_getRestitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getFriction_0 = Module["_emscripten_bind_btSoftBody_getFriction_0"] = function() {
  return (_emscripten_bind_btSoftBody_getFriction_0 = Module["_emscripten_bind_btSoftBody_getFriction_0"] = Module["asm"]["emscripten_bind_btSoftBody_getFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getRollingFriction_0 = Module["_emscripten_bind_btSoftBody_getRollingFriction_0"] = function() {
  return (_emscripten_bind_btSoftBody_getRollingFriction_0 = Module["_emscripten_bind_btSoftBody_getRollingFriction_0"] = Module["asm"]["emscripten_bind_btSoftBody_getRollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setRestitution_1 = Module["_emscripten_bind_btSoftBody_setRestitution_1"] = function() {
  return (_emscripten_bind_btSoftBody_setRestitution_1 = Module["_emscripten_bind_btSoftBody_setRestitution_1"] = Module["asm"]["emscripten_bind_btSoftBody_setRestitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setFriction_1 = Module["_emscripten_bind_btSoftBody_setFriction_1"] = function() {
  return (_emscripten_bind_btSoftBody_setFriction_1 = Module["_emscripten_bind_btSoftBody_setFriction_1"] = Module["asm"]["emscripten_bind_btSoftBody_setFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setRollingFriction_1 = Module["_emscripten_bind_btSoftBody_setRollingFriction_1"] = function() {
  return (_emscripten_bind_btSoftBody_setRollingFriction_1 = Module["_emscripten_bind_btSoftBody_setRollingFriction_1"] = Module["asm"]["emscripten_bind_btSoftBody_setRollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getWorldTransform_0 = Module["_emscripten_bind_btSoftBody_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btSoftBody_getWorldTransform_0 = Module["_emscripten_bind_btSoftBody_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btSoftBody_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getCollisionFlags_0 = Module["_emscripten_bind_btSoftBody_getCollisionFlags_0"] = function() {
  return (_emscripten_bind_btSoftBody_getCollisionFlags_0 = Module["_emscripten_bind_btSoftBody_getCollisionFlags_0"] = Module["asm"]["emscripten_bind_btSoftBody_getCollisionFlags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setCollisionFlags_1 = Module["_emscripten_bind_btSoftBody_setCollisionFlags_1"] = function() {
  return (_emscripten_bind_btSoftBody_setCollisionFlags_1 = Module["_emscripten_bind_btSoftBody_setCollisionFlags_1"] = Module["asm"]["emscripten_bind_btSoftBody_setCollisionFlags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setWorldTransform_1 = Module["_emscripten_bind_btSoftBody_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btSoftBody_setWorldTransform_1 = Module["_emscripten_bind_btSoftBody_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btSoftBody_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setCollisionShape_1 = Module["_emscripten_bind_btSoftBody_setCollisionShape_1"] = function() {
  return (_emscripten_bind_btSoftBody_setCollisionShape_1 = Module["_emscripten_bind_btSoftBody_setCollisionShape_1"] = Module["asm"]["emscripten_bind_btSoftBody_setCollisionShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setCcdMotionThreshold_1 = Module["_emscripten_bind_btSoftBody_setCcdMotionThreshold_1"] = function() {
  return (_emscripten_bind_btSoftBody_setCcdMotionThreshold_1 = Module["_emscripten_bind_btSoftBody_setCcdMotionThreshold_1"] = Module["asm"]["emscripten_bind_btSoftBody_setCcdMotionThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btSoftBody_setCcdSweptSphereRadius_1"] = function() {
  return (_emscripten_bind_btSoftBody_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btSoftBody_setCcdSweptSphereRadius_1"] = Module["asm"]["emscripten_bind_btSoftBody_setCcdSweptSphereRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getUserIndex_0 = Module["_emscripten_bind_btSoftBody_getUserIndex_0"] = function() {
  return (_emscripten_bind_btSoftBody_getUserIndex_0 = Module["_emscripten_bind_btSoftBody_getUserIndex_0"] = Module["asm"]["emscripten_bind_btSoftBody_getUserIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setUserIndex_1 = Module["_emscripten_bind_btSoftBody_setUserIndex_1"] = function() {
  return (_emscripten_bind_btSoftBody_setUserIndex_1 = Module["_emscripten_bind_btSoftBody_setUserIndex_1"] = Module["asm"]["emscripten_bind_btSoftBody_setUserIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getUserPointer_0 = Module["_emscripten_bind_btSoftBody_getUserPointer_0"] = function() {
  return (_emscripten_bind_btSoftBody_getUserPointer_0 = Module["_emscripten_bind_btSoftBody_getUserPointer_0"] = Module["asm"]["emscripten_bind_btSoftBody_getUserPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_setUserPointer_1 = Module["_emscripten_bind_btSoftBody_setUserPointer_1"] = function() {
  return (_emscripten_bind_btSoftBody_setUserPointer_1 = Module["_emscripten_bind_btSoftBody_setUserPointer_1"] = Module["asm"]["emscripten_bind_btSoftBody_setUserPointer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_getBroadphaseHandle_0 = Module["_emscripten_bind_btSoftBody_getBroadphaseHandle_0"] = function() {
  return (_emscripten_bind_btSoftBody_getBroadphaseHandle_0 = Module["_emscripten_bind_btSoftBody_getBroadphaseHandle_0"] = Module["asm"]["emscripten_bind_btSoftBody_getBroadphaseHandle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_get_m_cfg_0 = Module["_emscripten_bind_btSoftBody_get_m_cfg_0"] = function() {
  return (_emscripten_bind_btSoftBody_get_m_cfg_0 = Module["_emscripten_bind_btSoftBody_get_m_cfg_0"] = Module["asm"]["emscripten_bind_btSoftBody_get_m_cfg_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_set_m_cfg_1 = Module["_emscripten_bind_btSoftBody_set_m_cfg_1"] = function() {
  return (_emscripten_bind_btSoftBody_set_m_cfg_1 = Module["_emscripten_bind_btSoftBody_set_m_cfg_1"] = Module["asm"]["emscripten_bind_btSoftBody_set_m_cfg_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_get_m_nodes_0 = Module["_emscripten_bind_btSoftBody_get_m_nodes_0"] = function() {
  return (_emscripten_bind_btSoftBody_get_m_nodes_0 = Module["_emscripten_bind_btSoftBody_get_m_nodes_0"] = Module["asm"]["emscripten_bind_btSoftBody_get_m_nodes_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_set_m_nodes_1 = Module["_emscripten_bind_btSoftBody_set_m_nodes_1"] = function() {
  return (_emscripten_bind_btSoftBody_set_m_nodes_1 = Module["_emscripten_bind_btSoftBody_set_m_nodes_1"] = Module["asm"]["emscripten_bind_btSoftBody_set_m_nodes_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_get_m_faces_0 = Module["_emscripten_bind_btSoftBody_get_m_faces_0"] = function() {
  return (_emscripten_bind_btSoftBody_get_m_faces_0 = Module["_emscripten_bind_btSoftBody_get_m_faces_0"] = Module["asm"]["emscripten_bind_btSoftBody_get_m_faces_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_set_m_faces_1 = Module["_emscripten_bind_btSoftBody_set_m_faces_1"] = function() {
  return (_emscripten_bind_btSoftBody_set_m_faces_1 = Module["_emscripten_bind_btSoftBody_set_m_faces_1"] = Module["asm"]["emscripten_bind_btSoftBody_set_m_faces_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_get_m_materials_0 = Module["_emscripten_bind_btSoftBody_get_m_materials_0"] = function() {
  return (_emscripten_bind_btSoftBody_get_m_materials_0 = Module["_emscripten_bind_btSoftBody_get_m_materials_0"] = Module["asm"]["emscripten_bind_btSoftBody_get_m_materials_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_set_m_materials_1 = Module["_emscripten_bind_btSoftBody_set_m_materials_1"] = function() {
  return (_emscripten_bind_btSoftBody_set_m_materials_1 = Module["_emscripten_bind_btSoftBody_set_m_materials_1"] = Module["asm"]["emscripten_bind_btSoftBody_set_m_materials_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_get_m_anchors_0 = Module["_emscripten_bind_btSoftBody_get_m_anchors_0"] = function() {
  return (_emscripten_bind_btSoftBody_get_m_anchors_0 = Module["_emscripten_bind_btSoftBody_get_m_anchors_0"] = Module["asm"]["emscripten_bind_btSoftBody_get_m_anchors_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody_set_m_anchors_1 = Module["_emscripten_bind_btSoftBody_set_m_anchors_1"] = function() {
  return (_emscripten_bind_btSoftBody_set_m_anchors_1 = Module["_emscripten_bind_btSoftBody_set_m_anchors_1"] = Module["asm"]["emscripten_bind_btSoftBody_set_m_anchors_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBody___destroy___0 = Module["_emscripten_bind_btSoftBody___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBody___destroy___0 = Module["_emscripten_bind_btSoftBody___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBody___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIntArray_size_0 = Module["_emscripten_bind_btIntArray_size_0"] = function() {
  return (_emscripten_bind_btIntArray_size_0 = Module["_emscripten_bind_btIntArray_size_0"] = Module["asm"]["emscripten_bind_btIntArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIntArray_at_1 = Module["_emscripten_bind_btIntArray_at_1"] = function() {
  return (_emscripten_bind_btIntArray_at_1 = Module["_emscripten_bind_btIntArray_at_1"] = Module["asm"]["emscripten_bind_btIntArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIntArray___destroy___0 = Module["_emscripten_bind_btIntArray___destroy___0"] = function() {
  return (_emscripten_bind_btIntArray___destroy___0 = Module["_emscripten_bind_btIntArray___destroy___0"] = Module["asm"]["emscripten_bind_btIntArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kVCF_0 = Module["_emscripten_bind_Config_get_kVCF_0"] = function() {
  return (_emscripten_bind_Config_get_kVCF_0 = Module["_emscripten_bind_Config_get_kVCF_0"] = Module["asm"]["emscripten_bind_Config_get_kVCF_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kVCF_1 = Module["_emscripten_bind_Config_set_kVCF_1"] = function() {
  return (_emscripten_bind_Config_set_kVCF_1 = Module["_emscripten_bind_Config_set_kVCF_1"] = Module["asm"]["emscripten_bind_Config_set_kVCF_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kDP_0 = Module["_emscripten_bind_Config_get_kDP_0"] = function() {
  return (_emscripten_bind_Config_get_kDP_0 = Module["_emscripten_bind_Config_get_kDP_0"] = Module["asm"]["emscripten_bind_Config_get_kDP_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kDP_1 = Module["_emscripten_bind_Config_set_kDP_1"] = function() {
  return (_emscripten_bind_Config_set_kDP_1 = Module["_emscripten_bind_Config_set_kDP_1"] = Module["asm"]["emscripten_bind_Config_set_kDP_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kDG_0 = Module["_emscripten_bind_Config_get_kDG_0"] = function() {
  return (_emscripten_bind_Config_get_kDG_0 = Module["_emscripten_bind_Config_get_kDG_0"] = Module["asm"]["emscripten_bind_Config_get_kDG_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kDG_1 = Module["_emscripten_bind_Config_set_kDG_1"] = function() {
  return (_emscripten_bind_Config_set_kDG_1 = Module["_emscripten_bind_Config_set_kDG_1"] = Module["asm"]["emscripten_bind_Config_set_kDG_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kLF_0 = Module["_emscripten_bind_Config_get_kLF_0"] = function() {
  return (_emscripten_bind_Config_get_kLF_0 = Module["_emscripten_bind_Config_get_kLF_0"] = Module["asm"]["emscripten_bind_Config_get_kLF_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kLF_1 = Module["_emscripten_bind_Config_set_kLF_1"] = function() {
  return (_emscripten_bind_Config_set_kLF_1 = Module["_emscripten_bind_Config_set_kLF_1"] = Module["asm"]["emscripten_bind_Config_set_kLF_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kPR_0 = Module["_emscripten_bind_Config_get_kPR_0"] = function() {
  return (_emscripten_bind_Config_get_kPR_0 = Module["_emscripten_bind_Config_get_kPR_0"] = Module["asm"]["emscripten_bind_Config_get_kPR_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kPR_1 = Module["_emscripten_bind_Config_set_kPR_1"] = function() {
  return (_emscripten_bind_Config_set_kPR_1 = Module["_emscripten_bind_Config_set_kPR_1"] = Module["asm"]["emscripten_bind_Config_set_kPR_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kVC_0 = Module["_emscripten_bind_Config_get_kVC_0"] = function() {
  return (_emscripten_bind_Config_get_kVC_0 = Module["_emscripten_bind_Config_get_kVC_0"] = Module["asm"]["emscripten_bind_Config_get_kVC_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kVC_1 = Module["_emscripten_bind_Config_set_kVC_1"] = function() {
  return (_emscripten_bind_Config_set_kVC_1 = Module["_emscripten_bind_Config_set_kVC_1"] = Module["asm"]["emscripten_bind_Config_set_kVC_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kDF_0 = Module["_emscripten_bind_Config_get_kDF_0"] = function() {
  return (_emscripten_bind_Config_get_kDF_0 = Module["_emscripten_bind_Config_get_kDF_0"] = Module["asm"]["emscripten_bind_Config_get_kDF_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kDF_1 = Module["_emscripten_bind_Config_set_kDF_1"] = function() {
  return (_emscripten_bind_Config_set_kDF_1 = Module["_emscripten_bind_Config_set_kDF_1"] = Module["asm"]["emscripten_bind_Config_set_kDF_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kMT_0 = Module["_emscripten_bind_Config_get_kMT_0"] = function() {
  return (_emscripten_bind_Config_get_kMT_0 = Module["_emscripten_bind_Config_get_kMT_0"] = Module["asm"]["emscripten_bind_Config_get_kMT_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kMT_1 = Module["_emscripten_bind_Config_set_kMT_1"] = function() {
  return (_emscripten_bind_Config_set_kMT_1 = Module["_emscripten_bind_Config_set_kMT_1"] = Module["asm"]["emscripten_bind_Config_set_kMT_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kCHR_0 = Module["_emscripten_bind_Config_get_kCHR_0"] = function() {
  return (_emscripten_bind_Config_get_kCHR_0 = Module["_emscripten_bind_Config_get_kCHR_0"] = Module["asm"]["emscripten_bind_Config_get_kCHR_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kCHR_1 = Module["_emscripten_bind_Config_set_kCHR_1"] = function() {
  return (_emscripten_bind_Config_set_kCHR_1 = Module["_emscripten_bind_Config_set_kCHR_1"] = Module["asm"]["emscripten_bind_Config_set_kCHR_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kKHR_0 = Module["_emscripten_bind_Config_get_kKHR_0"] = function() {
  return (_emscripten_bind_Config_get_kKHR_0 = Module["_emscripten_bind_Config_get_kKHR_0"] = Module["asm"]["emscripten_bind_Config_get_kKHR_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kKHR_1 = Module["_emscripten_bind_Config_set_kKHR_1"] = function() {
  return (_emscripten_bind_Config_set_kKHR_1 = Module["_emscripten_bind_Config_set_kKHR_1"] = Module["asm"]["emscripten_bind_Config_set_kKHR_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSHR_0 = Module["_emscripten_bind_Config_get_kSHR_0"] = function() {
  return (_emscripten_bind_Config_get_kSHR_0 = Module["_emscripten_bind_Config_get_kSHR_0"] = Module["asm"]["emscripten_bind_Config_get_kSHR_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSHR_1 = Module["_emscripten_bind_Config_set_kSHR_1"] = function() {
  return (_emscripten_bind_Config_set_kSHR_1 = Module["_emscripten_bind_Config_set_kSHR_1"] = Module["asm"]["emscripten_bind_Config_set_kSHR_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kAHR_0 = Module["_emscripten_bind_Config_get_kAHR_0"] = function() {
  return (_emscripten_bind_Config_get_kAHR_0 = Module["_emscripten_bind_Config_get_kAHR_0"] = Module["asm"]["emscripten_bind_Config_get_kAHR_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kAHR_1 = Module["_emscripten_bind_Config_set_kAHR_1"] = function() {
  return (_emscripten_bind_Config_set_kAHR_1 = Module["_emscripten_bind_Config_set_kAHR_1"] = Module["asm"]["emscripten_bind_Config_set_kAHR_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSRHR_CL_0 = Module["_emscripten_bind_Config_get_kSRHR_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSRHR_CL_0 = Module["_emscripten_bind_Config_get_kSRHR_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSRHR_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSRHR_CL_1 = Module["_emscripten_bind_Config_set_kSRHR_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSRHR_CL_1 = Module["_emscripten_bind_Config_set_kSRHR_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSRHR_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSKHR_CL_0 = Module["_emscripten_bind_Config_get_kSKHR_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSKHR_CL_0 = Module["_emscripten_bind_Config_get_kSKHR_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSKHR_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSKHR_CL_1 = Module["_emscripten_bind_Config_set_kSKHR_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSKHR_CL_1 = Module["_emscripten_bind_Config_set_kSKHR_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSKHR_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSSHR_CL_0 = Module["_emscripten_bind_Config_get_kSSHR_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSSHR_CL_0 = Module["_emscripten_bind_Config_get_kSSHR_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSSHR_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSSHR_CL_1 = Module["_emscripten_bind_Config_set_kSSHR_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSSHR_CL_1 = Module["_emscripten_bind_Config_set_kSSHR_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSSHR_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSR_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSR_SPLT_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSR_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSR_SPLT_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSR_SPLT_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSR_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSR_SPLT_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSR_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSR_SPLT_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSR_SPLT_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSK_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSK_SPLT_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSK_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSK_SPLT_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSK_SPLT_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSK_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSK_SPLT_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSK_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSK_SPLT_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSK_SPLT_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_kSS_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSS_SPLT_CL_0"] = function() {
  return (_emscripten_bind_Config_get_kSS_SPLT_CL_0 = Module["_emscripten_bind_Config_get_kSS_SPLT_CL_0"] = Module["asm"]["emscripten_bind_Config_get_kSS_SPLT_CL_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_kSS_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSS_SPLT_CL_1"] = function() {
  return (_emscripten_bind_Config_set_kSS_SPLT_CL_1 = Module["_emscripten_bind_Config_set_kSS_SPLT_CL_1"] = Module["asm"]["emscripten_bind_Config_set_kSS_SPLT_CL_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_maxvolume_0 = Module["_emscripten_bind_Config_get_maxvolume_0"] = function() {
  return (_emscripten_bind_Config_get_maxvolume_0 = Module["_emscripten_bind_Config_get_maxvolume_0"] = Module["asm"]["emscripten_bind_Config_get_maxvolume_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_maxvolume_1 = Module["_emscripten_bind_Config_set_maxvolume_1"] = function() {
  return (_emscripten_bind_Config_set_maxvolume_1 = Module["_emscripten_bind_Config_set_maxvolume_1"] = Module["asm"]["emscripten_bind_Config_set_maxvolume_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_timescale_0 = Module["_emscripten_bind_Config_get_timescale_0"] = function() {
  return (_emscripten_bind_Config_get_timescale_0 = Module["_emscripten_bind_Config_get_timescale_0"] = Module["asm"]["emscripten_bind_Config_get_timescale_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_timescale_1 = Module["_emscripten_bind_Config_set_timescale_1"] = function() {
  return (_emscripten_bind_Config_set_timescale_1 = Module["_emscripten_bind_Config_set_timescale_1"] = Module["asm"]["emscripten_bind_Config_set_timescale_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_viterations_0 = Module["_emscripten_bind_Config_get_viterations_0"] = function() {
  return (_emscripten_bind_Config_get_viterations_0 = Module["_emscripten_bind_Config_get_viterations_0"] = Module["asm"]["emscripten_bind_Config_get_viterations_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_viterations_1 = Module["_emscripten_bind_Config_set_viterations_1"] = function() {
  return (_emscripten_bind_Config_set_viterations_1 = Module["_emscripten_bind_Config_set_viterations_1"] = Module["asm"]["emscripten_bind_Config_set_viterations_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_piterations_0 = Module["_emscripten_bind_Config_get_piterations_0"] = function() {
  return (_emscripten_bind_Config_get_piterations_0 = Module["_emscripten_bind_Config_get_piterations_0"] = Module["asm"]["emscripten_bind_Config_get_piterations_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_piterations_1 = Module["_emscripten_bind_Config_set_piterations_1"] = function() {
  return (_emscripten_bind_Config_set_piterations_1 = Module["_emscripten_bind_Config_set_piterations_1"] = Module["asm"]["emscripten_bind_Config_set_piterations_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_diterations_0 = Module["_emscripten_bind_Config_get_diterations_0"] = function() {
  return (_emscripten_bind_Config_get_diterations_0 = Module["_emscripten_bind_Config_get_diterations_0"] = Module["asm"]["emscripten_bind_Config_get_diterations_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_diterations_1 = Module["_emscripten_bind_Config_set_diterations_1"] = function() {
  return (_emscripten_bind_Config_set_diterations_1 = Module["_emscripten_bind_Config_set_diterations_1"] = Module["asm"]["emscripten_bind_Config_set_diterations_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_citerations_0 = Module["_emscripten_bind_Config_get_citerations_0"] = function() {
  return (_emscripten_bind_Config_get_citerations_0 = Module["_emscripten_bind_Config_get_citerations_0"] = Module["asm"]["emscripten_bind_Config_get_citerations_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_citerations_1 = Module["_emscripten_bind_Config_set_citerations_1"] = function() {
  return (_emscripten_bind_Config_set_citerations_1 = Module["_emscripten_bind_Config_set_citerations_1"] = Module["asm"]["emscripten_bind_Config_set_citerations_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_get_collisions_0 = Module["_emscripten_bind_Config_get_collisions_0"] = function() {
  return (_emscripten_bind_Config_get_collisions_0 = Module["_emscripten_bind_Config_get_collisions_0"] = Module["asm"]["emscripten_bind_Config_get_collisions_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config_set_collisions_1 = Module["_emscripten_bind_Config_set_collisions_1"] = function() {
  return (_emscripten_bind_Config_set_collisions_1 = Module["_emscripten_bind_Config_set_collisions_1"] = Module["asm"]["emscripten_bind_Config_set_collisions_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Config___destroy___0 = Module["_emscripten_bind_Config___destroy___0"] = function() {
  return (_emscripten_bind_Config___destroy___0 = Module["_emscripten_bind_Config___destroy___0"] = Module["asm"]["emscripten_bind_Config___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_x_0 = Module["_emscripten_bind_Node_get_m_x_0"] = function() {
  return (_emscripten_bind_Node_get_m_x_0 = Module["_emscripten_bind_Node_get_m_x_0"] = Module["asm"]["emscripten_bind_Node_get_m_x_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_x_1 = Module["_emscripten_bind_Node_set_m_x_1"] = function() {
  return (_emscripten_bind_Node_set_m_x_1 = Module["_emscripten_bind_Node_set_m_x_1"] = Module["asm"]["emscripten_bind_Node_set_m_x_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_q_0 = Module["_emscripten_bind_Node_get_m_q_0"] = function() {
  return (_emscripten_bind_Node_get_m_q_0 = Module["_emscripten_bind_Node_get_m_q_0"] = Module["asm"]["emscripten_bind_Node_get_m_q_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_q_1 = Module["_emscripten_bind_Node_set_m_q_1"] = function() {
  return (_emscripten_bind_Node_set_m_q_1 = Module["_emscripten_bind_Node_set_m_q_1"] = Module["asm"]["emscripten_bind_Node_set_m_q_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_v_0 = Module["_emscripten_bind_Node_get_m_v_0"] = function() {
  return (_emscripten_bind_Node_get_m_v_0 = Module["_emscripten_bind_Node_get_m_v_0"] = Module["asm"]["emscripten_bind_Node_get_m_v_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_v_1 = Module["_emscripten_bind_Node_set_m_v_1"] = function() {
  return (_emscripten_bind_Node_set_m_v_1 = Module["_emscripten_bind_Node_set_m_v_1"] = Module["asm"]["emscripten_bind_Node_set_m_v_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_f_0 = Module["_emscripten_bind_Node_get_m_f_0"] = function() {
  return (_emscripten_bind_Node_get_m_f_0 = Module["_emscripten_bind_Node_get_m_f_0"] = Module["asm"]["emscripten_bind_Node_get_m_f_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_f_1 = Module["_emscripten_bind_Node_set_m_f_1"] = function() {
  return (_emscripten_bind_Node_set_m_f_1 = Module["_emscripten_bind_Node_set_m_f_1"] = Module["asm"]["emscripten_bind_Node_set_m_f_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_n_0 = Module["_emscripten_bind_Node_get_m_n_0"] = function() {
  return (_emscripten_bind_Node_get_m_n_0 = Module["_emscripten_bind_Node_get_m_n_0"] = Module["asm"]["emscripten_bind_Node_get_m_n_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_n_1 = Module["_emscripten_bind_Node_set_m_n_1"] = function() {
  return (_emscripten_bind_Node_set_m_n_1 = Module["_emscripten_bind_Node_set_m_n_1"] = Module["asm"]["emscripten_bind_Node_set_m_n_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_im_0 = Module["_emscripten_bind_Node_get_m_im_0"] = function() {
  return (_emscripten_bind_Node_get_m_im_0 = Module["_emscripten_bind_Node_get_m_im_0"] = Module["asm"]["emscripten_bind_Node_get_m_im_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_im_1 = Module["_emscripten_bind_Node_set_m_im_1"] = function() {
  return (_emscripten_bind_Node_set_m_im_1 = Module["_emscripten_bind_Node_set_m_im_1"] = Module["asm"]["emscripten_bind_Node_set_m_im_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_get_m_area_0 = Module["_emscripten_bind_Node_get_m_area_0"] = function() {
  return (_emscripten_bind_Node_get_m_area_0 = Module["_emscripten_bind_Node_get_m_area_0"] = Module["asm"]["emscripten_bind_Node_get_m_area_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node_set_m_area_1 = Module["_emscripten_bind_Node_set_m_area_1"] = function() {
  return (_emscripten_bind_Node_set_m_area_1 = Module["_emscripten_bind_Node_set_m_area_1"] = Module["asm"]["emscripten_bind_Node_set_m_area_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Node___destroy___0 = Module["_emscripten_bind_Node___destroy___0"] = function() {
  return (_emscripten_bind_Node___destroy___0 = Module["_emscripten_bind_Node___destroy___0"] = Module["asm"]["emscripten_bind_Node___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostPairCallback_btGhostPairCallback_0 = Module["_emscripten_bind_btGhostPairCallback_btGhostPairCallback_0"] = function() {
  return (_emscripten_bind_btGhostPairCallback_btGhostPairCallback_0 = Module["_emscripten_bind_btGhostPairCallback_btGhostPairCallback_0"] = Module["asm"]["emscripten_bind_btGhostPairCallback_btGhostPairCallback_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGhostPairCallback___destroy___0 = Module["_emscripten_bind_btGhostPairCallback___destroy___0"] = function() {
  return (_emscripten_bind_btGhostPairCallback___destroy___0 = Module["_emscripten_bind_btGhostPairCallback___destroy___0"] = Module["asm"]["emscripten_bind_btGhostPairCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btOverlappingPairCallback___destroy___0 = Module["_emscripten_bind_btOverlappingPairCallback___destroy___0"] = function() {
  return (_emscripten_bind_btOverlappingPairCallback___destroy___0 = Module["_emscripten_bind_btOverlappingPairCallback___destroy___0"] = Module["asm"]["emscripten_bind_btOverlappingPairCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_3 = Module["_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_3"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_3 = Module["_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_3"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_4 = Module["_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_4"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_4 = Module["_emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_4"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_btKinematicCharacterController_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setUpAxis_1 = Module["_emscripten_bind_btKinematicCharacterController_setUpAxis_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setUpAxis_1 = Module["_emscripten_bind_btKinematicCharacterController_setUpAxis_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setUpAxis_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setWalkDirection_1 = Module["_emscripten_bind_btKinematicCharacterController_setWalkDirection_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setWalkDirection_1 = Module["_emscripten_bind_btKinematicCharacterController_setWalkDirection_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setWalkDirection_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setVelocityForTimeInterval_2 = Module["_emscripten_bind_btKinematicCharacterController_setVelocityForTimeInterval_2"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setVelocityForTimeInterval_2 = Module["_emscripten_bind_btKinematicCharacterController_setVelocityForTimeInterval_2"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setVelocityForTimeInterval_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_warp_1 = Module["_emscripten_bind_btKinematicCharacterController_warp_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_warp_1 = Module["_emscripten_bind_btKinematicCharacterController_warp_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_warp_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_preStep_1 = Module["_emscripten_bind_btKinematicCharacterController_preStep_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_preStep_1 = Module["_emscripten_bind_btKinematicCharacterController_preStep_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_preStep_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_playerStep_2 = Module["_emscripten_bind_btKinematicCharacterController_playerStep_2"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_playerStep_2 = Module["_emscripten_bind_btKinematicCharacterController_playerStep_2"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_playerStep_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setFallSpeed_1 = Module["_emscripten_bind_btKinematicCharacterController_setFallSpeed_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setFallSpeed_1 = Module["_emscripten_bind_btKinematicCharacterController_setFallSpeed_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setFallSpeed_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setJumpSpeed_1 = Module["_emscripten_bind_btKinematicCharacterController_setJumpSpeed_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setJumpSpeed_1 = Module["_emscripten_bind_btKinematicCharacterController_setJumpSpeed_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setJumpSpeed_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setMaxJumpHeight_1 = Module["_emscripten_bind_btKinematicCharacterController_setMaxJumpHeight_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setMaxJumpHeight_1 = Module["_emscripten_bind_btKinematicCharacterController_setMaxJumpHeight_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setMaxJumpHeight_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_canJump_0 = Module["_emscripten_bind_btKinematicCharacterController_canJump_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_canJump_0 = Module["_emscripten_bind_btKinematicCharacterController_canJump_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_canJump_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_jump_0 = Module["_emscripten_bind_btKinematicCharacterController_jump_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_jump_0 = Module["_emscripten_bind_btKinematicCharacterController_jump_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_jump_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setGravity_1 = Module["_emscripten_bind_btKinematicCharacterController_setGravity_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setGravity_1 = Module["_emscripten_bind_btKinematicCharacterController_setGravity_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setGravity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_getGravity_0 = Module["_emscripten_bind_btKinematicCharacterController_getGravity_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_getGravity_0 = Module["_emscripten_bind_btKinematicCharacterController_getGravity_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_getGravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setMaxSlope_1 = Module["_emscripten_bind_btKinematicCharacterController_setMaxSlope_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setMaxSlope_1 = Module["_emscripten_bind_btKinematicCharacterController_setMaxSlope_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setMaxSlope_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_getMaxSlope_0 = Module["_emscripten_bind_btKinematicCharacterController_getMaxSlope_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_getMaxSlope_0 = Module["_emscripten_bind_btKinematicCharacterController_getMaxSlope_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_getMaxSlope_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_getGhostObject_0 = Module["_emscripten_bind_btKinematicCharacterController_getGhostObject_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_getGhostObject_0 = Module["_emscripten_bind_btKinematicCharacterController_getGhostObject_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_getGhostObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setUseGhostSweepTest_1 = Module["_emscripten_bind_btKinematicCharacterController_setUseGhostSweepTest_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setUseGhostSweepTest_1 = Module["_emscripten_bind_btKinematicCharacterController_setUseGhostSweepTest_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setUseGhostSweepTest_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_onGround_0 = Module["_emscripten_bind_btKinematicCharacterController_onGround_0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_onGround_0 = Module["_emscripten_bind_btKinematicCharacterController_onGround_0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_onGround_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_setUpInterpolate_1 = Module["_emscripten_bind_btKinematicCharacterController_setUpInterpolate_1"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_setUpInterpolate_1 = Module["_emscripten_bind_btKinematicCharacterController_setUpInterpolate_1"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_setUpInterpolate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController_updateAction_2 = Module["_emscripten_bind_btKinematicCharacterController_updateAction_2"] = function() {
  return (_emscripten_bind_btKinematicCharacterController_updateAction_2 = Module["_emscripten_bind_btKinematicCharacterController_updateAction_2"] = Module["asm"]["emscripten_bind_btKinematicCharacterController_updateAction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btKinematicCharacterController___destroy___0 = Module["_emscripten_bind_btKinematicCharacterController___destroy___0"] = function() {
  return (_emscripten_bind_btKinematicCharacterController___destroy___0 = Module["_emscripten_bind_btKinematicCharacterController___destroy___0"] = Module["asm"]["emscripten_bind_btKinematicCharacterController___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyArray_size_0 = Module["_emscripten_bind_btSoftBodyArray_size_0"] = function() {
  return (_emscripten_bind_btSoftBodyArray_size_0 = Module["_emscripten_bind_btSoftBodyArray_size_0"] = Module["asm"]["emscripten_bind_btSoftBodyArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyArray_at_1 = Module["_emscripten_bind_btSoftBodyArray_at_1"] = function() {
  return (_emscripten_bind_btSoftBodyArray_at_1 = Module["_emscripten_bind_btSoftBodyArray_at_1"] = Module["asm"]["emscripten_bind_btSoftBodyArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyArray___destroy___0 = Module["_emscripten_bind_btSoftBodyArray___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBodyArray___destroy___0 = Module["_emscripten_bind_btSoftBodyArray___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBodyArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFaceArray_size_0 = Module["_emscripten_bind_btFaceArray_size_0"] = function() {
  return (_emscripten_bind_btFaceArray_size_0 = Module["_emscripten_bind_btFaceArray_size_0"] = Module["asm"]["emscripten_bind_btFaceArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFaceArray_at_1 = Module["_emscripten_bind_btFaceArray_at_1"] = function() {
  return (_emscripten_bind_btFaceArray_at_1 = Module["_emscripten_bind_btFaceArray_at_1"] = Module["asm"]["emscripten_bind_btFaceArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFaceArray___destroy___0 = Module["_emscripten_bind_btFaceArray___destroy___0"] = function() {
  return (_emscripten_bind_btFaceArray___destroy___0 = Module["_emscripten_bind_btFaceArray___destroy___0"] = Module["asm"]["emscripten_bind_btFaceArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStaticPlaneShape_btStaticPlaneShape_2 = Module["_emscripten_bind_btStaticPlaneShape_btStaticPlaneShape_2"] = function() {
  return (_emscripten_bind_btStaticPlaneShape_btStaticPlaneShape_2 = Module["_emscripten_bind_btStaticPlaneShape_btStaticPlaneShape_2"] = Module["asm"]["emscripten_bind_btStaticPlaneShape_btStaticPlaneShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStaticPlaneShape_setLocalScaling_1 = Module["_emscripten_bind_btStaticPlaneShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btStaticPlaneShape_setLocalScaling_1 = Module["_emscripten_bind_btStaticPlaneShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btStaticPlaneShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStaticPlaneShape_getLocalScaling_0 = Module["_emscripten_bind_btStaticPlaneShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btStaticPlaneShape_getLocalScaling_0 = Module["_emscripten_bind_btStaticPlaneShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btStaticPlaneShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStaticPlaneShape_calculateLocalInertia_2 = Module["_emscripten_bind_btStaticPlaneShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btStaticPlaneShape_calculateLocalInertia_2 = Module["_emscripten_bind_btStaticPlaneShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btStaticPlaneShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btStaticPlaneShape___destroy___0 = Module["_emscripten_bind_btStaticPlaneShape___destroy___0"] = function() {
  return (_emscripten_bind_btStaticPlaneShape___destroy___0 = Module["_emscripten_bind_btStaticPlaneShape___destroy___0"] = Module["asm"]["emscripten_bind_btStaticPlaneShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btOverlappingPairCache_setInternalGhostPairCallback_1 = Module["_emscripten_bind_btOverlappingPairCache_setInternalGhostPairCallback_1"] = function() {
  return (_emscripten_bind_btOverlappingPairCache_setInternalGhostPairCallback_1 = Module["_emscripten_bind_btOverlappingPairCache_setInternalGhostPairCallback_1"] = Module["asm"]["emscripten_bind_btOverlappingPairCache_setInternalGhostPairCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btOverlappingPairCache_getNumOverlappingPairs_0 = Module["_emscripten_bind_btOverlappingPairCache_getNumOverlappingPairs_0"] = function() {
  return (_emscripten_bind_btOverlappingPairCache_getNumOverlappingPairs_0 = Module["_emscripten_bind_btOverlappingPairCache_getNumOverlappingPairs_0"] = Module["asm"]["emscripten_bind_btOverlappingPairCache_getNumOverlappingPairs_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btOverlappingPairCache___destroy___0 = Module["_emscripten_bind_btOverlappingPairCache___destroy___0"] = function() {
  return (_emscripten_bind_btOverlappingPairCache___destroy___0 = Module["_emscripten_bind_btOverlappingPairCache___destroy___0"] = Module["asm"]["emscripten_bind_btOverlappingPairCache___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMesh_get_m_numTriangles_0 = Module["_emscripten_bind_btIndexedMesh_get_m_numTriangles_0"] = function() {
  return (_emscripten_bind_btIndexedMesh_get_m_numTriangles_0 = Module["_emscripten_bind_btIndexedMesh_get_m_numTriangles_0"] = Module["asm"]["emscripten_bind_btIndexedMesh_get_m_numTriangles_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMesh_set_m_numTriangles_1 = Module["_emscripten_bind_btIndexedMesh_set_m_numTriangles_1"] = function() {
  return (_emscripten_bind_btIndexedMesh_set_m_numTriangles_1 = Module["_emscripten_bind_btIndexedMesh_set_m_numTriangles_1"] = Module["asm"]["emscripten_bind_btIndexedMesh_set_m_numTriangles_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btIndexedMesh___destroy___0 = Module["_emscripten_bind_btIndexedMesh___destroy___0"] = function() {
  return (_emscripten_bind_btIndexedMesh___destroy___0 = Module["_emscripten_bind_btIndexedMesh___destroy___0"] = Module["asm"]["emscripten_bind_btIndexedMesh___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_btSoftRigidDynamicsWorld_5 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_btSoftRigidDynamicsWorld_5"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_btSoftRigidDynamicsWorld_5 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_btSoftRigidDynamicsWorld_5"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_btSoftRigidDynamicsWorld_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addSoftBody_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addSoftBody_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addSoftBody_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addSoftBody_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addSoftBody_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_removeSoftBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeSoftBody_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_removeSoftBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeSoftBody_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_removeSoftBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeCollisionObject_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_removeCollisionObject_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeCollisionObject_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_removeCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getWorldInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getWorldInfo_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getWorldInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getWorldInfo_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getWorldInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getSoftBodyArray_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getSoftBodyArray_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getSoftBodyArray_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getSoftBodyArray_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getSoftBodyArray_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDispatcher_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getDispatcher_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDispatcher_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getDispatcher_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_rayTest_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_rayTest_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_rayTest_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_rayTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getPairCache_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getPairCache_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getPairCache_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getPairCache_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDispatchInfo_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getDispatchInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDispatchInfo_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getDispatchInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_2"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_2"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addCollisionObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getBroadphase_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getBroadphase_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getBroadphase_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getBroadphase_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_convexSweepTest_5"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_convexSweepTest_5 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_convexSweepTest_5"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_convexSweepTest_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_contactPairTest_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_contactPairTest_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_contactPairTest_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_contactPairTest_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_contactTest_2"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_contactTest_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_contactTest_2"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_contactTest_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_updateSingleAabb_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_updateSingleAabb_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_updateSingleAabb_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_updateSingleAabb_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setDebugDrawer_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setDebugDrawer_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setDebugDrawer_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setDebugDrawer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDebugDrawer_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getDebugDrawer_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getDebugDrawer_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getDebugDrawer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawWorld_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawWorld_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawWorld_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_debugDrawWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawObject_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawObject_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_debugDrawObject_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_debugDrawObject_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setGravity_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setGravity_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setGravity_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setGravity_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setGravity_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getGravity_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getGravity_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getGravity_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getGravity_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getGravity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addRigidBody_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_removeRigidBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeRigidBody_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_removeRigidBody_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeRigidBody_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_removeRigidBody_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_2"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_2"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addConstraint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_removeConstraint_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeConstraint_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_removeConstraint_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeConstraint_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_removeConstraint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_2"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_2"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_stepSimulation_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setContactAddedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactAddedCallback_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setContactAddedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactAddedCallback_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setContactAddedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setContactProcessedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactProcessedCallback_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setContactProcessedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactProcessedCallback_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setContactProcessedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setContactDestroyedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactDestroyedCallback_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setContactDestroyedCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setContactDestroyedCallback_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setContactDestroyedCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_addAction_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addAction_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_addAction_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_addAction_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_addAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeAction_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_removeAction_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_removeAction_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_removeAction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getSolverInfo_0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_getSolverInfo_0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_getSolverInfo_0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_getSolverInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_1"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_1 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_1"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_2"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_2 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_2"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_3"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_3 = Module["_emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_3"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld_setInternalTickCallback_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftRigidDynamicsWorld___destroy___0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld___destroy___0"] = function() {
  return (_emscripten_bind_btSoftRigidDynamicsWorld___destroy___0 = Module["_emscripten_bind_btSoftRigidDynamicsWorld___destroy___0"] = Module["asm"]["emscripten_bind_btSoftRigidDynamicsWorld___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_btFixedConstraint_4 = Module["_emscripten_bind_btFixedConstraint_btFixedConstraint_4"] = function() {
  return (_emscripten_bind_btFixedConstraint_btFixedConstraint_4 = Module["_emscripten_bind_btFixedConstraint_btFixedConstraint_4"] = Module["asm"]["emscripten_bind_btFixedConstraint_btFixedConstraint_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_enableFeedback_1 = Module["_emscripten_bind_btFixedConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btFixedConstraint_enableFeedback_1 = Module["_emscripten_bind_btFixedConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btFixedConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btFixedConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btFixedConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btFixedConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btFixedConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btFixedConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btFixedConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btFixedConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btFixedConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_getParam_2 = Module["_emscripten_bind_btFixedConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btFixedConstraint_getParam_2 = Module["_emscripten_bind_btFixedConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btFixedConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint_setParam_3 = Module["_emscripten_bind_btFixedConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btFixedConstraint_setParam_3 = Module["_emscripten_bind_btFixedConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btFixedConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFixedConstraint___destroy___0 = Module["_emscripten_bind_btFixedConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btFixedConstraint___destroy___0 = Module["_emscripten_bind_btFixedConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btFixedConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_btTransform_0 = Module["_emscripten_bind_btTransform_btTransform_0"] = function() {
  return (_emscripten_bind_btTransform_btTransform_0 = Module["_emscripten_bind_btTransform_btTransform_0"] = Module["asm"]["emscripten_bind_btTransform_btTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_btTransform_2 = Module["_emscripten_bind_btTransform_btTransform_2"] = function() {
  return (_emscripten_bind_btTransform_btTransform_2 = Module["_emscripten_bind_btTransform_btTransform_2"] = Module["asm"]["emscripten_bind_btTransform_btTransform_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_setIdentity_0 = Module["_emscripten_bind_btTransform_setIdentity_0"] = function() {
  return (_emscripten_bind_btTransform_setIdentity_0 = Module["_emscripten_bind_btTransform_setIdentity_0"] = Module["asm"]["emscripten_bind_btTransform_setIdentity_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_setOrigin_1 = Module["_emscripten_bind_btTransform_setOrigin_1"] = function() {
  return (_emscripten_bind_btTransform_setOrigin_1 = Module["_emscripten_bind_btTransform_setOrigin_1"] = Module["asm"]["emscripten_bind_btTransform_setOrigin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_setRotation_1 = Module["_emscripten_bind_btTransform_setRotation_1"] = function() {
  return (_emscripten_bind_btTransform_setRotation_1 = Module["_emscripten_bind_btTransform_setRotation_1"] = Module["asm"]["emscripten_bind_btTransform_setRotation_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_getOrigin_0 = Module["_emscripten_bind_btTransform_getOrigin_0"] = function() {
  return (_emscripten_bind_btTransform_getOrigin_0 = Module["_emscripten_bind_btTransform_getOrigin_0"] = Module["asm"]["emscripten_bind_btTransform_getOrigin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_getRotation_0 = Module["_emscripten_bind_btTransform_getRotation_0"] = function() {
  return (_emscripten_bind_btTransform_getRotation_0 = Module["_emscripten_bind_btTransform_getRotation_0"] = Module["asm"]["emscripten_bind_btTransform_getRotation_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_getBasis_0 = Module["_emscripten_bind_btTransform_getBasis_0"] = function() {
  return (_emscripten_bind_btTransform_getBasis_0 = Module["_emscripten_bind_btTransform_getBasis_0"] = Module["asm"]["emscripten_bind_btTransform_getBasis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_setFromOpenGLMatrix_1 = Module["_emscripten_bind_btTransform_setFromOpenGLMatrix_1"] = function() {
  return (_emscripten_bind_btTransform_setFromOpenGLMatrix_1 = Module["_emscripten_bind_btTransform_setFromOpenGLMatrix_1"] = Module["asm"]["emscripten_bind_btTransform_setFromOpenGLMatrix_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_inverse_0 = Module["_emscripten_bind_btTransform_inverse_0"] = function() {
  return (_emscripten_bind_btTransform_inverse_0 = Module["_emscripten_bind_btTransform_inverse_0"] = Module["asm"]["emscripten_bind_btTransform_inverse_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform_op_mul_1 = Module["_emscripten_bind_btTransform_op_mul_1"] = function() {
  return (_emscripten_bind_btTransform_op_mul_1 = Module["_emscripten_bind_btTransform_op_mul_1"] = Module["asm"]["emscripten_bind_btTransform_op_mul_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btTransform___destroy___0 = Module["_emscripten_bind_btTransform___destroy___0"] = function() {
  return (_emscripten_bind_btTransform___destroy___0 = Module["_emscripten_bind_btTransform___destroy___0"] = Module["asm"]["emscripten_bind_btTransform___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_ClosestRayResultCallback_2 = Module["_emscripten_bind_ClosestRayResultCallback_ClosestRayResultCallback_2"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_ClosestRayResultCallback_2 = Module["_emscripten_bind_ClosestRayResultCallback_ClosestRayResultCallback_2"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_ClosestRayResultCallback_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_hasHit_0 = Module["_emscripten_bind_ClosestRayResultCallback_hasHit_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_hasHit_0 = Module["_emscripten_bind_ClosestRayResultCallback_hasHit_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_hasHit_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_rayFromWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_rayFromWorld_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_rayFromWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_rayFromWorld_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_rayFromWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_rayFromWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_rayFromWorld_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_rayFromWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_rayFromWorld_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_rayFromWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_rayToWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_rayToWorld_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_rayToWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_rayToWorld_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_rayToWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_rayToWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_rayToWorld_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_rayToWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_rayToWorld_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_rayToWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_hitNormalWorld_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_hitNormalWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_hitNormalWorld_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_hitNormalWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_hitNormalWorld_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_hitNormalWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_hitNormalWorld_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_hitNormalWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_hitPointWorld_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_hitPointWorld_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_hitPointWorld_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_hitPointWorld_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_hitPointWorld_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_hitPointWorld_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_hitPointWorld_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_hitPointWorld_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterMask_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterMask_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_closestHitFraction_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_closestHitFraction_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_closestHitFraction_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_closestHitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_closestHitFraction_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_closestHitFraction_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_closestHitFraction_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_closestHitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionObject_0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_get_m_collisionObject_0 = Module["_emscripten_bind_ClosestRayResultCallback_get_m_collisionObject_0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_get_m_collisionObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionObject_1"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback_set_m_collisionObject_1 = Module["_emscripten_bind_ClosestRayResultCallback_set_m_collisionObject_1"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback_set_m_collisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ClosestRayResultCallback___destroy___0 = Module["_emscripten_bind_ClosestRayResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_ClosestRayResultCallback___destroy___0 = Module["_emscripten_bind_ClosestRayResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_ClosestRayResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_0 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_0"] = function() {
  return (_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_0 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_0"] = Module["asm"]["emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_1 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_1"] = function() {
  return (_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_1 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_1"] = Module["asm"]["emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration_btSoftBodyRigidBodyCollisionConfiguration_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration___destroy___0 = Module["_emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBodyRigidBodyCollisionConfiguration___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConcreteContactResultCallback_ConcreteContactResultCallback_0 = Module["_emscripten_bind_ConcreteContactResultCallback_ConcreteContactResultCallback_0"] = function() {
  return (_emscripten_bind_ConcreteContactResultCallback_ConcreteContactResultCallback_0 = Module["_emscripten_bind_ConcreteContactResultCallback_ConcreteContactResultCallback_0"] = Module["asm"]["emscripten_bind_ConcreteContactResultCallback_ConcreteContactResultCallback_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConcreteContactResultCallback_addSingleResult_7 = Module["_emscripten_bind_ConcreteContactResultCallback_addSingleResult_7"] = function() {
  return (_emscripten_bind_ConcreteContactResultCallback_addSingleResult_7 = Module["_emscripten_bind_ConcreteContactResultCallback_addSingleResult_7"] = Module["asm"]["emscripten_bind_ConcreteContactResultCallback_addSingleResult_7"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_ConcreteContactResultCallback___destroy___0 = Module["_emscripten_bind_ConcreteContactResultCallback___destroy___0"] = function() {
  return (_emscripten_bind_ConcreteContactResultCallback___destroy___0 = Module["_emscripten_bind_ConcreteContactResultCallback___destroy___0"] = Module["asm"]["emscripten_bind_ConcreteContactResultCallback___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_2 = Module["_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_2"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_2 = Module["_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_2"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_3 = Module["_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_3"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_3 = Module["_emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_3"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape_btBvhTriangleMeshShape_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btBvhTriangleMeshShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape_setLocalScaling_1 = Module["_emscripten_bind_btBvhTriangleMeshShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btBvhTriangleMeshShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape_getLocalScaling_0 = Module["_emscripten_bind_btBvhTriangleMeshShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btBvhTriangleMeshShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape_calculateLocalInertia_2 = Module["_emscripten_bind_btBvhTriangleMeshShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBvhTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btBvhTriangleMeshShape___destroy___0"] = function() {
  return (_emscripten_bind_btBvhTriangleMeshShape___destroy___0 = Module["_emscripten_bind_btBvhTriangleMeshShape___destroy___0"] = Module["asm"]["emscripten_bind_btBvhTriangleMeshShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstCollisionObjectArray_size_0 = Module["_emscripten_bind_btConstCollisionObjectArray_size_0"] = function() {
  return (_emscripten_bind_btConstCollisionObjectArray_size_0 = Module["_emscripten_bind_btConstCollisionObjectArray_size_0"] = Module["asm"]["emscripten_bind_btConstCollisionObjectArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstCollisionObjectArray_at_1 = Module["_emscripten_bind_btConstCollisionObjectArray_at_1"] = function() {
  return (_emscripten_bind_btConstCollisionObjectArray_at_1 = Module["_emscripten_bind_btConstCollisionObjectArray_at_1"] = Module["asm"]["emscripten_bind_btConstCollisionObjectArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btConstCollisionObjectArray___destroy___0 = Module["_emscripten_bind_btConstCollisionObjectArray___destroy___0"] = function() {
  return (_emscripten_bind_btConstCollisionObjectArray___destroy___0 = Module["_emscripten_bind_btConstCollisionObjectArray___destroy___0"] = Module["asm"]["emscripten_bind_btConstCollisionObjectArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_btSliderConstraint_3 = Module["_emscripten_bind_btSliderConstraint_btSliderConstraint_3"] = function() {
  return (_emscripten_bind_btSliderConstraint_btSliderConstraint_3 = Module["_emscripten_bind_btSliderConstraint_btSliderConstraint_3"] = Module["asm"]["emscripten_bind_btSliderConstraint_btSliderConstraint_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_btSliderConstraint_5 = Module["_emscripten_bind_btSliderConstraint_btSliderConstraint_5"] = function() {
  return (_emscripten_bind_btSliderConstraint_btSliderConstraint_5 = Module["_emscripten_bind_btSliderConstraint_btSliderConstraint_5"] = Module["asm"]["emscripten_bind_btSliderConstraint_btSliderConstraint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setLowerLinLimit_1 = Module["_emscripten_bind_btSliderConstraint_setLowerLinLimit_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_setLowerLinLimit_1 = Module["_emscripten_bind_btSliderConstraint_setLowerLinLimit_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_setLowerLinLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setUpperLinLimit_1 = Module["_emscripten_bind_btSliderConstraint_setUpperLinLimit_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_setUpperLinLimit_1 = Module["_emscripten_bind_btSliderConstraint_setUpperLinLimit_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_setUpperLinLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setLowerAngLimit_1 = Module["_emscripten_bind_btSliderConstraint_setLowerAngLimit_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_setLowerAngLimit_1 = Module["_emscripten_bind_btSliderConstraint_setLowerAngLimit_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_setLowerAngLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setUpperAngLimit_1 = Module["_emscripten_bind_btSliderConstraint_setUpperAngLimit_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_setUpperAngLimit_1 = Module["_emscripten_bind_btSliderConstraint_setUpperAngLimit_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_setUpperAngLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_enableFeedback_1 = Module["_emscripten_bind_btSliderConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_enableFeedback_1 = Module["_emscripten_bind_btSliderConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btSliderConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btSliderConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btSliderConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btSliderConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btSliderConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btSliderConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btSliderConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btSliderConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_getParam_2 = Module["_emscripten_bind_btSliderConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btSliderConstraint_getParam_2 = Module["_emscripten_bind_btSliderConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btSliderConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint_setParam_3 = Module["_emscripten_bind_btSliderConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btSliderConstraint_setParam_3 = Module["_emscripten_bind_btSliderConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btSliderConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSliderConstraint___destroy___0 = Module["_emscripten_bind_btSliderConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btSliderConstraint___destroy___0 = Module["_emscripten_bind_btSliderConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btSliderConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_btPairCachingGhostObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_btPairCachingGhostObject_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_btPairCachingGhostObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_btPairCachingGhostObject_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_btPairCachingGhostObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btPairCachingGhostObject_setAnisotropicFriction_2"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setAnisotropicFriction_2 = Module["_emscripten_bind_btPairCachingGhostObject_setAnisotropicFriction_2"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setAnisotropicFriction_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getCollisionShape_0 = Module["_emscripten_bind_btPairCachingGhostObject_getCollisionShape_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getCollisionShape_0 = Module["_emscripten_bind_btPairCachingGhostObject_getCollisionShape_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getCollisionShape_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btPairCachingGhostObject_setContactProcessingThreshold_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setContactProcessingThreshold_1 = Module["_emscripten_bind_btPairCachingGhostObject_setContactProcessingThreshold_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setContactProcessingThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setActivationState_1 = Module["_emscripten_bind_btPairCachingGhostObject_setActivationState_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setActivationState_1 = Module["_emscripten_bind_btPairCachingGhostObject_setActivationState_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_forceActivationState_1 = Module["_emscripten_bind_btPairCachingGhostObject_forceActivationState_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_forceActivationState_1 = Module["_emscripten_bind_btPairCachingGhostObject_forceActivationState_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_forceActivationState_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_activate_0 = Module["_emscripten_bind_btPairCachingGhostObject_activate_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_activate_0 = Module["_emscripten_bind_btPairCachingGhostObject_activate_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_activate_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_activate_1 = Module["_emscripten_bind_btPairCachingGhostObject_activate_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_activate_1 = Module["_emscripten_bind_btPairCachingGhostObject_activate_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_activate_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_isActive_0 = Module["_emscripten_bind_btPairCachingGhostObject_isActive_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_isActive_0 = Module["_emscripten_bind_btPairCachingGhostObject_isActive_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_isActive_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_isKinematicObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isKinematicObject_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_isKinematicObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isKinematicObject_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_isKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_isStaticObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isStaticObject_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_isStaticObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isStaticObject_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_isStaticObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isStaticOrKinematicObject_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_isStaticOrKinematicObject_0 = Module["_emscripten_bind_btPairCachingGhostObject_isStaticOrKinematicObject_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_isStaticOrKinematicObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getRestitution_0 = Module["_emscripten_bind_btPairCachingGhostObject_getRestitution_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getRestitution_0 = Module["_emscripten_bind_btPairCachingGhostObject_getRestitution_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getRestitution_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getFriction_0 = Module["_emscripten_bind_btPairCachingGhostObject_getFriction_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getFriction_0 = Module["_emscripten_bind_btPairCachingGhostObject_getFriction_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getRollingFriction_0 = Module["_emscripten_bind_btPairCachingGhostObject_getRollingFriction_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getRollingFriction_0 = Module["_emscripten_bind_btPairCachingGhostObject_getRollingFriction_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getRollingFriction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setRestitution_1 = Module["_emscripten_bind_btPairCachingGhostObject_setRestitution_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setRestitution_1 = Module["_emscripten_bind_btPairCachingGhostObject_setRestitution_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setRestitution_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setFriction_1 = Module["_emscripten_bind_btPairCachingGhostObject_setFriction_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setFriction_1 = Module["_emscripten_bind_btPairCachingGhostObject_setFriction_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setRollingFriction_1 = Module["_emscripten_bind_btPairCachingGhostObject_setRollingFriction_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setRollingFriction_1 = Module["_emscripten_bind_btPairCachingGhostObject_setRollingFriction_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setRollingFriction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getWorldTransform_0 = Module["_emscripten_bind_btPairCachingGhostObject_getWorldTransform_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getWorldTransform_0 = Module["_emscripten_bind_btPairCachingGhostObject_getWorldTransform_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getWorldTransform_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getCollisionFlags_0 = Module["_emscripten_bind_btPairCachingGhostObject_getCollisionFlags_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getCollisionFlags_0 = Module["_emscripten_bind_btPairCachingGhostObject_getCollisionFlags_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getCollisionFlags_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setCollisionFlags_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCollisionFlags_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setCollisionFlags_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCollisionFlags_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setCollisionFlags_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setWorldTransform_1 = Module["_emscripten_bind_btPairCachingGhostObject_setWorldTransform_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setWorldTransform_1 = Module["_emscripten_bind_btPairCachingGhostObject_setWorldTransform_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setWorldTransform_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setCollisionShape_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCollisionShape_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setCollisionShape_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCollisionShape_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setCollisionShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCcdMotionThreshold_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setCcdMotionThreshold_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCcdMotionThreshold_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setCcdMotionThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCcdSweptSphereRadius_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setCcdSweptSphereRadius_1 = Module["_emscripten_bind_btPairCachingGhostObject_setCcdSweptSphereRadius_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setCcdSweptSphereRadius_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getUserIndex_0 = Module["_emscripten_bind_btPairCachingGhostObject_getUserIndex_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getUserIndex_0 = Module["_emscripten_bind_btPairCachingGhostObject_getUserIndex_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getUserIndex_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setUserIndex_1 = Module["_emscripten_bind_btPairCachingGhostObject_setUserIndex_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setUserIndex_1 = Module["_emscripten_bind_btPairCachingGhostObject_setUserIndex_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setUserIndex_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getUserPointer_0 = Module["_emscripten_bind_btPairCachingGhostObject_getUserPointer_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getUserPointer_0 = Module["_emscripten_bind_btPairCachingGhostObject_getUserPointer_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getUserPointer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_setUserPointer_1 = Module["_emscripten_bind_btPairCachingGhostObject_setUserPointer_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_setUserPointer_1 = Module["_emscripten_bind_btPairCachingGhostObject_setUserPointer_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_setUserPointer_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btPairCachingGhostObject_getBroadphaseHandle_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getBroadphaseHandle_0 = Module["_emscripten_bind_btPairCachingGhostObject_getBroadphaseHandle_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getBroadphaseHandle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getNumOverlappingObjects_0 = Module["_emscripten_bind_btPairCachingGhostObject_getNumOverlappingObjects_0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getNumOverlappingObjects_0 = Module["_emscripten_bind_btPairCachingGhostObject_getNumOverlappingObjects_0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getNumOverlappingObjects_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject_getOverlappingObject_1 = Module["_emscripten_bind_btPairCachingGhostObject_getOverlappingObject_1"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject_getOverlappingObject_1 = Module["_emscripten_bind_btPairCachingGhostObject_getOverlappingObject_1"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject_getOverlappingObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPairCachingGhostObject___destroy___0 = Module["_emscripten_bind_btPairCachingGhostObject___destroy___0"] = function() {
  return (_emscripten_bind_btPairCachingGhostObject___destroy___0 = Module["_emscripten_bind_btPairCachingGhostObject___destroy___0"] = Module["asm"]["emscripten_bind_btPairCachingGhostObject___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_getPositionWorldOnA_0 = Module["_emscripten_bind_btManifoldPoint_getPositionWorldOnA_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_getPositionWorldOnA_0 = Module["_emscripten_bind_btManifoldPoint_getPositionWorldOnA_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_getPositionWorldOnA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_getPositionWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_getPositionWorldOnB_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_getPositionWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_getPositionWorldOnB_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_getPositionWorldOnB_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_getAppliedImpulse_0 = Module["_emscripten_bind_btManifoldPoint_getAppliedImpulse_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_getAppliedImpulse_0 = Module["_emscripten_bind_btManifoldPoint_getAppliedImpulse_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_getAppliedImpulse_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_getDistance_0 = Module["_emscripten_bind_btManifoldPoint_getDistance_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_getDistance_0 = Module["_emscripten_bind_btManifoldPoint_getDistance_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_getDistance_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_localPointA_0 = Module["_emscripten_bind_btManifoldPoint_get_m_localPointA_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_localPointA_0 = Module["_emscripten_bind_btManifoldPoint_get_m_localPointA_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_localPointA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_localPointA_1 = Module["_emscripten_bind_btManifoldPoint_set_m_localPointA_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_localPointA_1 = Module["_emscripten_bind_btManifoldPoint_set_m_localPointA_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_localPointA_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_localPointB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_localPointB_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_localPointB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_localPointB_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_localPointB_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_localPointB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_localPointB_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_localPointB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_localPointB_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_localPointB_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_positionWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_positionWorldOnB_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_positionWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_positionWorldOnB_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_positionWorldOnB_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_positionWorldOnB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_positionWorldOnB_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_positionWorldOnB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_positionWorldOnB_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_positionWorldOnB_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_positionWorldOnA_0 = Module["_emscripten_bind_btManifoldPoint_get_m_positionWorldOnA_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_positionWorldOnA_0 = Module["_emscripten_bind_btManifoldPoint_get_m_positionWorldOnA_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_positionWorldOnA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_positionWorldOnA_1 = Module["_emscripten_bind_btManifoldPoint_set_m_positionWorldOnA_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_positionWorldOnA_1 = Module["_emscripten_bind_btManifoldPoint_set_m_positionWorldOnA_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_positionWorldOnA_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_normalWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_normalWorldOnB_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_normalWorldOnB_0 = Module["_emscripten_bind_btManifoldPoint_get_m_normalWorldOnB_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_normalWorldOnB_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_normalWorldOnB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_normalWorldOnB_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_normalWorldOnB_1 = Module["_emscripten_bind_btManifoldPoint_set_m_normalWorldOnB_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_normalWorldOnB_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_get_m_userPersistentData_0 = Module["_emscripten_bind_btManifoldPoint_get_m_userPersistentData_0"] = function() {
  return (_emscripten_bind_btManifoldPoint_get_m_userPersistentData_0 = Module["_emscripten_bind_btManifoldPoint_get_m_userPersistentData_0"] = Module["asm"]["emscripten_bind_btManifoldPoint_get_m_userPersistentData_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint_set_m_userPersistentData_1 = Module["_emscripten_bind_btManifoldPoint_set_m_userPersistentData_1"] = function() {
  return (_emscripten_bind_btManifoldPoint_set_m_userPersistentData_1 = Module["_emscripten_bind_btManifoldPoint_set_m_userPersistentData_1"] = Module["asm"]["emscripten_bind_btManifoldPoint_set_m_userPersistentData_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btManifoldPoint___destroy___0 = Module["_emscripten_bind_btManifoldPoint___destroy___0"] = function() {
  return (_emscripten_bind_btManifoldPoint___destroy___0 = Module["_emscripten_bind_btManifoldPoint___destroy___0"] = Module["asm"]["emscripten_bind_btManifoldPoint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_2 = Module["_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_2"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_2 = Module["_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_2"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_4 = Module["_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_4"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_4 = Module["_emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_4"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_btPoint2PointConstraint_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_setPivotA_1 = Module["_emscripten_bind_btPoint2PointConstraint_setPivotA_1"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_setPivotA_1 = Module["_emscripten_bind_btPoint2PointConstraint_setPivotA_1"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_setPivotA_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_setPivotB_1 = Module["_emscripten_bind_btPoint2PointConstraint_setPivotB_1"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_setPivotB_1 = Module["_emscripten_bind_btPoint2PointConstraint_setPivotB_1"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_setPivotB_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_getPivotInA_0 = Module["_emscripten_bind_btPoint2PointConstraint_getPivotInA_0"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_getPivotInA_0 = Module["_emscripten_bind_btPoint2PointConstraint_getPivotInA_0"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_getPivotInA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_getPivotInB_0 = Module["_emscripten_bind_btPoint2PointConstraint_getPivotInB_0"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_getPivotInB_0 = Module["_emscripten_bind_btPoint2PointConstraint_getPivotInB_0"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_getPivotInB_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_enableFeedback_1 = Module["_emscripten_bind_btPoint2PointConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_enableFeedback_1 = Module["_emscripten_bind_btPoint2PointConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btPoint2PointConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btPoint2PointConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btPoint2PointConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btPoint2PointConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_getParam_2 = Module["_emscripten_bind_btPoint2PointConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_getParam_2 = Module["_emscripten_bind_btPoint2PointConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_setParam_3 = Module["_emscripten_bind_btPoint2PointConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_setParam_3 = Module["_emscripten_bind_btPoint2PointConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_get_m_setting_0 = Module["_emscripten_bind_btPoint2PointConstraint_get_m_setting_0"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_get_m_setting_0 = Module["_emscripten_bind_btPoint2PointConstraint_get_m_setting_0"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_get_m_setting_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint_set_m_setting_1 = Module["_emscripten_bind_btPoint2PointConstraint_set_m_setting_1"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint_set_m_setting_1 = Module["_emscripten_bind_btPoint2PointConstraint_set_m_setting_1"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint_set_m_setting_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btPoint2PointConstraint___destroy___0 = Module["_emscripten_bind_btPoint2PointConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btPoint2PointConstraint___destroy___0 = Module["_emscripten_bind_btPoint2PointConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btPoint2PointConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_btSoftBodyHelpers_0 = Module["_emscripten_bind_btSoftBodyHelpers_btSoftBodyHelpers_0"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_btSoftBodyHelpers_0 = Module["_emscripten_bind_btSoftBodyHelpers_btSoftBodyHelpers_0"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_btSoftBodyHelpers_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreateRope_5 = Module["_emscripten_bind_btSoftBodyHelpers_CreateRope_5"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreateRope_5 = Module["_emscripten_bind_btSoftBodyHelpers_CreateRope_5"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreateRope_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreatePatch_9 = Module["_emscripten_bind_btSoftBodyHelpers_CreatePatch_9"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreatePatch_9 = Module["_emscripten_bind_btSoftBodyHelpers_CreatePatch_9"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreatePatch_9"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreatePatchUV_10 = Module["_emscripten_bind_btSoftBodyHelpers_CreatePatchUV_10"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreatePatchUV_10 = Module["_emscripten_bind_btSoftBodyHelpers_CreatePatchUV_10"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreatePatchUV_10"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreateEllipsoid_4 = Module["_emscripten_bind_btSoftBodyHelpers_CreateEllipsoid_4"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreateEllipsoid_4 = Module["_emscripten_bind_btSoftBodyHelpers_CreateEllipsoid_4"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreateEllipsoid_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreateFromTriMesh_5 = Module["_emscripten_bind_btSoftBodyHelpers_CreateFromTriMesh_5"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreateFromTriMesh_5 = Module["_emscripten_bind_btSoftBodyHelpers_CreateFromTriMesh_5"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreateFromTriMesh_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers_CreateFromConvexHull_4 = Module["_emscripten_bind_btSoftBodyHelpers_CreateFromConvexHull_4"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers_CreateFromConvexHull_4 = Module["_emscripten_bind_btSoftBodyHelpers_CreateFromConvexHull_4"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers_CreateFromConvexHull_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSoftBodyHelpers___destroy___0 = Module["_emscripten_bind_btSoftBodyHelpers___destroy___0"] = function() {
  return (_emscripten_bind_btSoftBodyHelpers___destroy___0 = Module["_emscripten_bind_btSoftBodyHelpers___destroy___0"] = Module["asm"]["emscripten_bind_btSoftBodyHelpers___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseProxy_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterGroup_0"] = function() {
  return (_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterGroup_0 = Module["_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterGroup_0"] = Module["asm"]["emscripten_bind_btBroadphaseProxy_get_m_collisionFilterGroup_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseProxy_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterGroup_1"] = function() {
  return (_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterGroup_1 = Module["_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterGroup_1"] = Module["asm"]["emscripten_bind_btBroadphaseProxy_set_m_collisionFilterGroup_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseProxy_get_m_collisionFilterMask_0 = Module["_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterMask_0"] = function() {
  return (_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterMask_0 = Module["_emscripten_bind_btBroadphaseProxy_get_m_collisionFilterMask_0"] = Module["asm"]["emscripten_bind_btBroadphaseProxy_get_m_collisionFilterMask_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseProxy_set_m_collisionFilterMask_1 = Module["_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterMask_1"] = function() {
  return (_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterMask_1 = Module["_emscripten_bind_btBroadphaseProxy_set_m_collisionFilterMask_1"] = Module["asm"]["emscripten_bind_btBroadphaseProxy_set_m_collisionFilterMask_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBroadphaseProxy___destroy___0 = Module["_emscripten_bind_btBroadphaseProxy___destroy___0"] = function() {
  return (_emscripten_bind_btBroadphaseProxy___destroy___0 = Module["_emscripten_bind_btBroadphaseProxy___destroy___0"] = Module["asm"]["emscripten_bind_btBroadphaseProxy___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tNodeArray_size_0 = Module["_emscripten_bind_tNodeArray_size_0"] = function() {
  return (_emscripten_bind_tNodeArray_size_0 = Module["_emscripten_bind_tNodeArray_size_0"] = Module["asm"]["emscripten_bind_tNodeArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tNodeArray_at_1 = Module["_emscripten_bind_tNodeArray_at_1"] = function() {
  return (_emscripten_bind_tNodeArray_at_1 = Module["_emscripten_bind_tNodeArray_at_1"] = Module["asm"]["emscripten_bind_tNodeArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tNodeArray___destroy___0 = Module["_emscripten_bind_tNodeArray___destroy___0"] = function() {
  return (_emscripten_bind_tNodeArray___destroy___0 = Module["_emscripten_bind_tNodeArray___destroy___0"] = Module["asm"]["emscripten_bind_tNodeArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_btBoxShape_1 = Module["_emscripten_bind_btBoxShape_btBoxShape_1"] = function() {
  return (_emscripten_bind_btBoxShape_btBoxShape_1 = Module["_emscripten_bind_btBoxShape_btBoxShape_1"] = Module["asm"]["emscripten_bind_btBoxShape_btBoxShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_setMargin_1 = Module["_emscripten_bind_btBoxShape_setMargin_1"] = function() {
  return (_emscripten_bind_btBoxShape_setMargin_1 = Module["_emscripten_bind_btBoxShape_setMargin_1"] = Module["asm"]["emscripten_bind_btBoxShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_getMargin_0 = Module["_emscripten_bind_btBoxShape_getMargin_0"] = function() {
  return (_emscripten_bind_btBoxShape_getMargin_0 = Module["_emscripten_bind_btBoxShape_getMargin_0"] = Module["asm"]["emscripten_bind_btBoxShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_setLocalScaling_1 = Module["_emscripten_bind_btBoxShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btBoxShape_setLocalScaling_1 = Module["_emscripten_bind_btBoxShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btBoxShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_getLocalScaling_0 = Module["_emscripten_bind_btBoxShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btBoxShape_getLocalScaling_0 = Module["_emscripten_bind_btBoxShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btBoxShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape_calculateLocalInertia_2 = Module["_emscripten_bind_btBoxShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btBoxShape_calculateLocalInertia_2 = Module["_emscripten_bind_btBoxShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btBoxShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btBoxShape___destroy___0 = Module["_emscripten_bind_btBoxShape___destroy___0"] = function() {
  return (_emscripten_bind_btBoxShape___destroy___0 = Module["_emscripten_bind_btBoxShape___destroy___0"] = Module["asm"]["emscripten_bind_btBoxShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFace_get_m_indices_0 = Module["_emscripten_bind_btFace_get_m_indices_0"] = function() {
  return (_emscripten_bind_btFace_get_m_indices_0 = Module["_emscripten_bind_btFace_get_m_indices_0"] = Module["asm"]["emscripten_bind_btFace_get_m_indices_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFace_set_m_indices_1 = Module["_emscripten_bind_btFace_set_m_indices_1"] = function() {
  return (_emscripten_bind_btFace_set_m_indices_1 = Module["_emscripten_bind_btFace_set_m_indices_1"] = Module["asm"]["emscripten_bind_btFace_set_m_indices_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFace_get_m_plane_1 = Module["_emscripten_bind_btFace_get_m_plane_1"] = function() {
  return (_emscripten_bind_btFace_get_m_plane_1 = Module["_emscripten_bind_btFace_get_m_plane_1"] = Module["asm"]["emscripten_bind_btFace_get_m_plane_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFace_set_m_plane_2 = Module["_emscripten_bind_btFace_set_m_plane_2"] = function() {
  return (_emscripten_bind_btFace_set_m_plane_2 = Module["_emscripten_bind_btFace_set_m_plane_2"] = Module["asm"]["emscripten_bind_btFace_set_m_plane_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btFace___destroy___0 = Module["_emscripten_bind_btFace___destroy___0"] = function() {
  return (_emscripten_bind_btFace___destroy___0 = Module["_emscripten_bind_btFace___destroy___0"] = Module["asm"]["emscripten_bind_btFace___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_DebugDrawer_0 = Module["_emscripten_bind_DebugDrawer_DebugDrawer_0"] = function() {
  return (_emscripten_bind_DebugDrawer_DebugDrawer_0 = Module["_emscripten_bind_DebugDrawer_DebugDrawer_0"] = Module["asm"]["emscripten_bind_DebugDrawer_DebugDrawer_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_drawLine_3 = Module["_emscripten_bind_DebugDrawer_drawLine_3"] = function() {
  return (_emscripten_bind_DebugDrawer_drawLine_3 = Module["_emscripten_bind_DebugDrawer_drawLine_3"] = Module["asm"]["emscripten_bind_DebugDrawer_drawLine_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_drawContactPoint_5 = Module["_emscripten_bind_DebugDrawer_drawContactPoint_5"] = function() {
  return (_emscripten_bind_DebugDrawer_drawContactPoint_5 = Module["_emscripten_bind_DebugDrawer_drawContactPoint_5"] = Module["asm"]["emscripten_bind_DebugDrawer_drawContactPoint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_reportErrorWarning_1 = Module["_emscripten_bind_DebugDrawer_reportErrorWarning_1"] = function() {
  return (_emscripten_bind_DebugDrawer_reportErrorWarning_1 = Module["_emscripten_bind_DebugDrawer_reportErrorWarning_1"] = Module["asm"]["emscripten_bind_DebugDrawer_reportErrorWarning_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_draw3dText_2 = Module["_emscripten_bind_DebugDrawer_draw3dText_2"] = function() {
  return (_emscripten_bind_DebugDrawer_draw3dText_2 = Module["_emscripten_bind_DebugDrawer_draw3dText_2"] = Module["asm"]["emscripten_bind_DebugDrawer_draw3dText_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_setDebugMode_1 = Module["_emscripten_bind_DebugDrawer_setDebugMode_1"] = function() {
  return (_emscripten_bind_DebugDrawer_setDebugMode_1 = Module["_emscripten_bind_DebugDrawer_setDebugMode_1"] = Module["asm"]["emscripten_bind_DebugDrawer_setDebugMode_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer_getDebugMode_0 = Module["_emscripten_bind_DebugDrawer_getDebugMode_0"] = function() {
  return (_emscripten_bind_DebugDrawer_getDebugMode_0 = Module["_emscripten_bind_DebugDrawer_getDebugMode_0"] = Module["asm"]["emscripten_bind_DebugDrawer_getDebugMode_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_DebugDrawer___destroy___0 = Module["_emscripten_bind_DebugDrawer___destroy___0"] = function() {
  return (_emscripten_bind_DebugDrawer___destroy___0 = Module["_emscripten_bind_DebugDrawer___destroy___0"] = Module["asm"]["emscripten_bind_DebugDrawer___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_btCapsuleShapeX_2 = Module["_emscripten_bind_btCapsuleShapeX_btCapsuleShapeX_2"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_btCapsuleShapeX_2 = Module["_emscripten_bind_btCapsuleShapeX_btCapsuleShapeX_2"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_btCapsuleShapeX_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_setMargin_1 = Module["_emscripten_bind_btCapsuleShapeX_setMargin_1"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_setMargin_1 = Module["_emscripten_bind_btCapsuleShapeX_setMargin_1"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_getMargin_0 = Module["_emscripten_bind_btCapsuleShapeX_getMargin_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_getMargin_0 = Module["_emscripten_bind_btCapsuleShapeX_getMargin_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShapeX_getUpAxis_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShapeX_getUpAxis_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_getUpAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_getRadius_0 = Module["_emscripten_bind_btCapsuleShapeX_getRadius_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_getRadius_0 = Module["_emscripten_bind_btCapsuleShapeX_getRadius_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_getRadius_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShapeX_getHalfHeight_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShapeX_getHalfHeight_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_getHalfHeight_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShapeX_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShapeX_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShapeX_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShapeX_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShapeX_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCapsuleShapeX_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShapeX_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCapsuleShapeX_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeX___destroy___0 = Module["_emscripten_bind_btCapsuleShapeX___destroy___0"] = function() {
  return (_emscripten_bind_btCapsuleShapeX___destroy___0 = Module["_emscripten_bind_btCapsuleShapeX___destroy___0"] = Module["asm"]["emscripten_bind_btCapsuleShapeX___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_btQuaternion_4 = Module["_emscripten_bind_btQuaternion_btQuaternion_4"] = function() {
  return (_emscripten_bind_btQuaternion_btQuaternion_4 = Module["_emscripten_bind_btQuaternion_btQuaternion_4"] = Module["asm"]["emscripten_bind_btQuaternion_btQuaternion_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setValue_4 = Module["_emscripten_bind_btQuaternion_setValue_4"] = function() {
  return (_emscripten_bind_btQuaternion_setValue_4 = Module["_emscripten_bind_btQuaternion_setValue_4"] = Module["asm"]["emscripten_bind_btQuaternion_setValue_4"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setEulerZYX_3 = Module["_emscripten_bind_btQuaternion_setEulerZYX_3"] = function() {
  return (_emscripten_bind_btQuaternion_setEulerZYX_3 = Module["_emscripten_bind_btQuaternion_setEulerZYX_3"] = Module["asm"]["emscripten_bind_btQuaternion_setEulerZYX_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setRotation_2 = Module["_emscripten_bind_btQuaternion_setRotation_2"] = function() {
  return (_emscripten_bind_btQuaternion_setRotation_2 = Module["_emscripten_bind_btQuaternion_setRotation_2"] = Module["asm"]["emscripten_bind_btQuaternion_setRotation_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_normalize_0 = Module["_emscripten_bind_btQuaternion_normalize_0"] = function() {
  return (_emscripten_bind_btQuaternion_normalize_0 = Module["_emscripten_bind_btQuaternion_normalize_0"] = Module["asm"]["emscripten_bind_btQuaternion_normalize_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_length2_0 = Module["_emscripten_bind_btQuaternion_length2_0"] = function() {
  return (_emscripten_bind_btQuaternion_length2_0 = Module["_emscripten_bind_btQuaternion_length2_0"] = Module["asm"]["emscripten_bind_btQuaternion_length2_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_length_0 = Module["_emscripten_bind_btQuaternion_length_0"] = function() {
  return (_emscripten_bind_btQuaternion_length_0 = Module["_emscripten_bind_btQuaternion_length_0"] = Module["asm"]["emscripten_bind_btQuaternion_length_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_dot_1 = Module["_emscripten_bind_btQuaternion_dot_1"] = function() {
  return (_emscripten_bind_btQuaternion_dot_1 = Module["_emscripten_bind_btQuaternion_dot_1"] = Module["asm"]["emscripten_bind_btQuaternion_dot_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_normalized_0 = Module["_emscripten_bind_btQuaternion_normalized_0"] = function() {
  return (_emscripten_bind_btQuaternion_normalized_0 = Module["_emscripten_bind_btQuaternion_normalized_0"] = Module["asm"]["emscripten_bind_btQuaternion_normalized_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_getAxis_0 = Module["_emscripten_bind_btQuaternion_getAxis_0"] = function() {
  return (_emscripten_bind_btQuaternion_getAxis_0 = Module["_emscripten_bind_btQuaternion_getAxis_0"] = Module["asm"]["emscripten_bind_btQuaternion_getAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_inverse_0 = Module["_emscripten_bind_btQuaternion_inverse_0"] = function() {
  return (_emscripten_bind_btQuaternion_inverse_0 = Module["_emscripten_bind_btQuaternion_inverse_0"] = Module["asm"]["emscripten_bind_btQuaternion_inverse_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_getAngle_0 = Module["_emscripten_bind_btQuaternion_getAngle_0"] = function() {
  return (_emscripten_bind_btQuaternion_getAngle_0 = Module["_emscripten_bind_btQuaternion_getAngle_0"] = Module["asm"]["emscripten_bind_btQuaternion_getAngle_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_getAngleShortestPath_0 = Module["_emscripten_bind_btQuaternion_getAngleShortestPath_0"] = function() {
  return (_emscripten_bind_btQuaternion_getAngleShortestPath_0 = Module["_emscripten_bind_btQuaternion_getAngleShortestPath_0"] = Module["asm"]["emscripten_bind_btQuaternion_getAngleShortestPath_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_angle_1 = Module["_emscripten_bind_btQuaternion_angle_1"] = function() {
  return (_emscripten_bind_btQuaternion_angle_1 = Module["_emscripten_bind_btQuaternion_angle_1"] = Module["asm"]["emscripten_bind_btQuaternion_angle_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_angleShortestPath_1 = Module["_emscripten_bind_btQuaternion_angleShortestPath_1"] = function() {
  return (_emscripten_bind_btQuaternion_angleShortestPath_1 = Module["_emscripten_bind_btQuaternion_angleShortestPath_1"] = Module["asm"]["emscripten_bind_btQuaternion_angleShortestPath_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_op_add_1 = Module["_emscripten_bind_btQuaternion_op_add_1"] = function() {
  return (_emscripten_bind_btQuaternion_op_add_1 = Module["_emscripten_bind_btQuaternion_op_add_1"] = Module["asm"]["emscripten_bind_btQuaternion_op_add_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_op_sub_1 = Module["_emscripten_bind_btQuaternion_op_sub_1"] = function() {
  return (_emscripten_bind_btQuaternion_op_sub_1 = Module["_emscripten_bind_btQuaternion_op_sub_1"] = Module["asm"]["emscripten_bind_btQuaternion_op_sub_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_op_mul_1 = Module["_emscripten_bind_btQuaternion_op_mul_1"] = function() {
  return (_emscripten_bind_btQuaternion_op_mul_1 = Module["_emscripten_bind_btQuaternion_op_mul_1"] = Module["asm"]["emscripten_bind_btQuaternion_op_mul_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_op_mulq_1 = Module["_emscripten_bind_btQuaternion_op_mulq_1"] = function() {
  return (_emscripten_bind_btQuaternion_op_mulq_1 = Module["_emscripten_bind_btQuaternion_op_mulq_1"] = Module["asm"]["emscripten_bind_btQuaternion_op_mulq_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_op_div_1 = Module["_emscripten_bind_btQuaternion_op_div_1"] = function() {
  return (_emscripten_bind_btQuaternion_op_div_1 = Module["_emscripten_bind_btQuaternion_op_div_1"] = Module["asm"]["emscripten_bind_btQuaternion_op_div_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_x_0 = Module["_emscripten_bind_btQuaternion_x_0"] = function() {
  return (_emscripten_bind_btQuaternion_x_0 = Module["_emscripten_bind_btQuaternion_x_0"] = Module["asm"]["emscripten_bind_btQuaternion_x_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_y_0 = Module["_emscripten_bind_btQuaternion_y_0"] = function() {
  return (_emscripten_bind_btQuaternion_y_0 = Module["_emscripten_bind_btQuaternion_y_0"] = Module["asm"]["emscripten_bind_btQuaternion_y_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_z_0 = Module["_emscripten_bind_btQuaternion_z_0"] = function() {
  return (_emscripten_bind_btQuaternion_z_0 = Module["_emscripten_bind_btQuaternion_z_0"] = Module["asm"]["emscripten_bind_btQuaternion_z_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_w_0 = Module["_emscripten_bind_btQuaternion_w_0"] = function() {
  return (_emscripten_bind_btQuaternion_w_0 = Module["_emscripten_bind_btQuaternion_w_0"] = Module["asm"]["emscripten_bind_btQuaternion_w_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setX_1 = Module["_emscripten_bind_btQuaternion_setX_1"] = function() {
  return (_emscripten_bind_btQuaternion_setX_1 = Module["_emscripten_bind_btQuaternion_setX_1"] = Module["asm"]["emscripten_bind_btQuaternion_setX_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setY_1 = Module["_emscripten_bind_btQuaternion_setY_1"] = function() {
  return (_emscripten_bind_btQuaternion_setY_1 = Module["_emscripten_bind_btQuaternion_setY_1"] = Module["asm"]["emscripten_bind_btQuaternion_setY_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setZ_1 = Module["_emscripten_bind_btQuaternion_setZ_1"] = function() {
  return (_emscripten_bind_btQuaternion_setZ_1 = Module["_emscripten_bind_btQuaternion_setZ_1"] = Module["asm"]["emscripten_bind_btQuaternion_setZ_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion_setW_1 = Module["_emscripten_bind_btQuaternion_setW_1"] = function() {
  return (_emscripten_bind_btQuaternion_setW_1 = Module["_emscripten_bind_btQuaternion_setW_1"] = Module["asm"]["emscripten_bind_btQuaternion_setW_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btQuaternion___destroy___0 = Module["_emscripten_bind_btQuaternion___destroy___0"] = function() {
  return (_emscripten_bind_btQuaternion___destroy___0 = Module["_emscripten_bind_btQuaternion___destroy___0"] = Module["asm"]["emscripten_bind_btQuaternion___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_btCapsuleShapeZ_2 = Module["_emscripten_bind_btCapsuleShapeZ_btCapsuleShapeZ_2"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_btCapsuleShapeZ_2 = Module["_emscripten_bind_btCapsuleShapeZ_btCapsuleShapeZ_2"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_btCapsuleShapeZ_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_setMargin_1 = Module["_emscripten_bind_btCapsuleShapeZ_setMargin_1"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_setMargin_1 = Module["_emscripten_bind_btCapsuleShapeZ_setMargin_1"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_getMargin_0 = Module["_emscripten_bind_btCapsuleShapeZ_getMargin_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_getMargin_0 = Module["_emscripten_bind_btCapsuleShapeZ_getMargin_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShapeZ_getUpAxis_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_getUpAxis_0 = Module["_emscripten_bind_btCapsuleShapeZ_getUpAxis_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_getUpAxis_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_getRadius_0 = Module["_emscripten_bind_btCapsuleShapeZ_getRadius_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_getRadius_0 = Module["_emscripten_bind_btCapsuleShapeZ_getRadius_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_getRadius_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShapeZ_getHalfHeight_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_getHalfHeight_0 = Module["_emscripten_bind_btCapsuleShapeZ_getHalfHeight_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_getHalfHeight_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShapeZ_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_setLocalScaling_1 = Module["_emscripten_bind_btCapsuleShapeZ_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShapeZ_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_getLocalScaling_0 = Module["_emscripten_bind_btCapsuleShapeZ_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShapeZ_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ_calculateLocalInertia_2 = Module["_emscripten_bind_btCapsuleShapeZ_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btCapsuleShapeZ___destroy___0 = Module["_emscripten_bind_btCapsuleShapeZ___destroy___0"] = function() {
  return (_emscripten_bind_btCapsuleShapeZ___destroy___0 = Module["_emscripten_bind_btCapsuleShapeZ___destroy___0"] = Module["asm"]["emscripten_bind_btCapsuleShapeZ___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_get_m_splitImpulse_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_splitImpulse_0"] = function() {
  return (_emscripten_bind_btContactSolverInfo_get_m_splitImpulse_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_splitImpulse_0"] = Module["asm"]["emscripten_bind_btContactSolverInfo_get_m_splitImpulse_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_set_m_splitImpulse_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_splitImpulse_1"] = function() {
  return (_emscripten_bind_btContactSolverInfo_set_m_splitImpulse_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_splitImpulse_1"] = Module["asm"]["emscripten_bind_btContactSolverInfo_set_m_splitImpulse_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_get_m_splitImpulsePenetrationThreshold_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_splitImpulsePenetrationThreshold_0"] = function() {
  return (_emscripten_bind_btContactSolverInfo_get_m_splitImpulsePenetrationThreshold_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_splitImpulsePenetrationThreshold_0"] = Module["asm"]["emscripten_bind_btContactSolverInfo_get_m_splitImpulsePenetrationThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_set_m_splitImpulsePenetrationThreshold_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_splitImpulsePenetrationThreshold_1"] = function() {
  return (_emscripten_bind_btContactSolverInfo_set_m_splitImpulsePenetrationThreshold_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_splitImpulsePenetrationThreshold_1"] = Module["asm"]["emscripten_bind_btContactSolverInfo_set_m_splitImpulsePenetrationThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_get_m_numIterations_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_numIterations_0"] = function() {
  return (_emscripten_bind_btContactSolverInfo_get_m_numIterations_0 = Module["_emscripten_bind_btContactSolverInfo_get_m_numIterations_0"] = Module["asm"]["emscripten_bind_btContactSolverInfo_get_m_numIterations_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo_set_m_numIterations_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_numIterations_1"] = function() {
  return (_emscripten_bind_btContactSolverInfo_set_m_numIterations_1 = Module["_emscripten_bind_btContactSolverInfo_set_m_numIterations_1"] = Module["asm"]["emscripten_bind_btContactSolverInfo_set_m_numIterations_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btContactSolverInfo___destroy___0 = Module["_emscripten_bind_btContactSolverInfo___destroy___0"] = function() {
  return (_emscripten_bind_btContactSolverInfo___destroy___0 = Module["_emscripten_bind_btContactSolverInfo___destroy___0"] = Module["asm"]["emscripten_bind_btContactSolverInfo___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_3 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_3"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_3 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_3"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_5 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_5"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_5 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_5"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_btGeneric6DofSpringConstraint_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_enableSpring_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_enableSpring_2"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_enableSpring_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_enableSpring_2"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_enableSpring_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setStiffness_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setStiffness_2"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setStiffness_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setStiffness_2"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setStiffness_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setDamping_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setDamping_2"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setDamping_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setDamping_2"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setDamping_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_0"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_0"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_2"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_2"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setEquilibriumPoint_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setLinearLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setLinearLowerLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setLinearLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setLinearLowerLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setLinearLowerLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setLinearUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setLinearUpperLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setLinearUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setLinearUpperLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setLinearUpperLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setAngularLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setAngularLowerLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setAngularLowerLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setAngularLowerLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setAngularLowerLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setAngularUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setAngularUpperLimit_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setAngularUpperLimit_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setAngularUpperLimit_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setAngularUpperLimit_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_getFrameOffsetA_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getFrameOffsetA_0"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_getFrameOffsetA_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getFrameOffsetA_0"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_getFrameOffsetA_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_enableFeedback_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_enableFeedback_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_enableFeedback_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_enableFeedback_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_enableFeedback_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getBreakingImpulseThreshold_0"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_getBreakingImpulseThreshold_0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getBreakingImpulseThreshold_0"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_getBreakingImpulseThreshold_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setBreakingImpulseThreshold_1"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setBreakingImpulseThreshold_1 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setBreakingImpulseThreshold_1"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setBreakingImpulseThreshold_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_getParam_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getParam_2"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_getParam_2 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_getParam_2"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_getParam_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint_setParam_3 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setParam_3"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint_setParam_3 = Module["_emscripten_bind_btGeneric6DofSpringConstraint_setParam_3"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint_setParam_3"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btGeneric6DofSpringConstraint___destroy___0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint___destroy___0"] = function() {
  return (_emscripten_bind_btGeneric6DofSpringConstraint___destroy___0 = Module["_emscripten_bind_btGeneric6DofSpringConstraint___destroy___0"] = Module["asm"]["emscripten_bind_btGeneric6DofSpringConstraint___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_btSphereShape_1 = Module["_emscripten_bind_btSphereShape_btSphereShape_1"] = function() {
  return (_emscripten_bind_btSphereShape_btSphereShape_1 = Module["_emscripten_bind_btSphereShape_btSphereShape_1"] = Module["asm"]["emscripten_bind_btSphereShape_btSphereShape_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_setMargin_1 = Module["_emscripten_bind_btSphereShape_setMargin_1"] = function() {
  return (_emscripten_bind_btSphereShape_setMargin_1 = Module["_emscripten_bind_btSphereShape_setMargin_1"] = Module["asm"]["emscripten_bind_btSphereShape_setMargin_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_getMargin_0 = Module["_emscripten_bind_btSphereShape_getMargin_0"] = function() {
  return (_emscripten_bind_btSphereShape_getMargin_0 = Module["_emscripten_bind_btSphereShape_getMargin_0"] = Module["asm"]["emscripten_bind_btSphereShape_getMargin_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_setLocalScaling_1 = Module["_emscripten_bind_btSphereShape_setLocalScaling_1"] = function() {
  return (_emscripten_bind_btSphereShape_setLocalScaling_1 = Module["_emscripten_bind_btSphereShape_setLocalScaling_1"] = Module["asm"]["emscripten_bind_btSphereShape_setLocalScaling_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_getLocalScaling_0 = Module["_emscripten_bind_btSphereShape_getLocalScaling_0"] = function() {
  return (_emscripten_bind_btSphereShape_getLocalScaling_0 = Module["_emscripten_bind_btSphereShape_getLocalScaling_0"] = Module["asm"]["emscripten_bind_btSphereShape_getLocalScaling_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape_calculateLocalInertia_2 = Module["_emscripten_bind_btSphereShape_calculateLocalInertia_2"] = function() {
  return (_emscripten_bind_btSphereShape_calculateLocalInertia_2 = Module["_emscripten_bind_btSphereShape_calculateLocalInertia_2"] = Module["asm"]["emscripten_bind_btSphereShape_calculateLocalInertia_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_btSphereShape___destroy___0 = Module["_emscripten_bind_btSphereShape___destroy___0"] = function() {
  return (_emscripten_bind_btSphereShape___destroy___0 = Module["_emscripten_bind_btSphereShape___destroy___0"] = Module["asm"]["emscripten_bind_btSphereShape___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_get_m_n_1 = Module["_emscripten_bind_Face_get_m_n_1"] = function() {
  return (_emscripten_bind_Face_get_m_n_1 = Module["_emscripten_bind_Face_get_m_n_1"] = Module["asm"]["emscripten_bind_Face_get_m_n_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_set_m_n_2 = Module["_emscripten_bind_Face_set_m_n_2"] = function() {
  return (_emscripten_bind_Face_set_m_n_2 = Module["_emscripten_bind_Face_set_m_n_2"] = Module["asm"]["emscripten_bind_Face_set_m_n_2"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_get_m_normal_0 = Module["_emscripten_bind_Face_get_m_normal_0"] = function() {
  return (_emscripten_bind_Face_get_m_normal_0 = Module["_emscripten_bind_Face_get_m_normal_0"] = Module["asm"]["emscripten_bind_Face_get_m_normal_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_set_m_normal_1 = Module["_emscripten_bind_Face_set_m_normal_1"] = function() {
  return (_emscripten_bind_Face_set_m_normal_1 = Module["_emscripten_bind_Face_set_m_normal_1"] = Module["asm"]["emscripten_bind_Face_set_m_normal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_get_m_ra_0 = Module["_emscripten_bind_Face_get_m_ra_0"] = function() {
  return (_emscripten_bind_Face_get_m_ra_0 = Module["_emscripten_bind_Face_get_m_ra_0"] = Module["asm"]["emscripten_bind_Face_get_m_ra_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face_set_m_ra_1 = Module["_emscripten_bind_Face_set_m_ra_1"] = function() {
  return (_emscripten_bind_Face_set_m_ra_1 = Module["_emscripten_bind_Face_set_m_ra_1"] = Module["asm"]["emscripten_bind_Face_set_m_ra_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_Face___destroy___0 = Module["_emscripten_bind_Face___destroy___0"] = function() {
  return (_emscripten_bind_Face___destroy___0 = Module["_emscripten_bind_Face___destroy___0"] = Module["asm"]["emscripten_bind_Face___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tFaceArray_size_0 = Module["_emscripten_bind_tFaceArray_size_0"] = function() {
  return (_emscripten_bind_tFaceArray_size_0 = Module["_emscripten_bind_tFaceArray_size_0"] = Module["asm"]["emscripten_bind_tFaceArray_size_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tFaceArray_at_1 = Module["_emscripten_bind_tFaceArray_at_1"] = function() {
  return (_emscripten_bind_tFaceArray_at_1 = Module["_emscripten_bind_tFaceArray_at_1"] = Module["asm"]["emscripten_bind_tFaceArray_at_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_tFaceArray___destroy___0 = Module["_emscripten_bind_tFaceArray___destroy___0"] = function() {
  return (_emscripten_bind_tFaceArray___destroy___0 = Module["_emscripten_bind_tFaceArray___destroy___0"] = Module["asm"]["emscripten_bind_tFaceArray___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_LocalConvexResult_5 = Module["_emscripten_bind_LocalConvexResult_LocalConvexResult_5"] = function() {
  return (_emscripten_bind_LocalConvexResult_LocalConvexResult_5 = Module["_emscripten_bind_LocalConvexResult_LocalConvexResult_5"] = Module["asm"]["emscripten_bind_LocalConvexResult_LocalConvexResult_5"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_get_m_hitCollisionObject_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitCollisionObject_0"] = function() {
  return (_emscripten_bind_LocalConvexResult_get_m_hitCollisionObject_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitCollisionObject_0"] = Module["asm"]["emscripten_bind_LocalConvexResult_get_m_hitCollisionObject_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_set_m_hitCollisionObject_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitCollisionObject_1"] = function() {
  return (_emscripten_bind_LocalConvexResult_set_m_hitCollisionObject_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitCollisionObject_1"] = Module["asm"]["emscripten_bind_LocalConvexResult_set_m_hitCollisionObject_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_get_m_localShapeInfo_0 = Module["_emscripten_bind_LocalConvexResult_get_m_localShapeInfo_0"] = function() {
  return (_emscripten_bind_LocalConvexResult_get_m_localShapeInfo_0 = Module["_emscripten_bind_LocalConvexResult_get_m_localShapeInfo_0"] = Module["asm"]["emscripten_bind_LocalConvexResult_get_m_localShapeInfo_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_set_m_localShapeInfo_1 = Module["_emscripten_bind_LocalConvexResult_set_m_localShapeInfo_1"] = function() {
  return (_emscripten_bind_LocalConvexResult_set_m_localShapeInfo_1 = Module["_emscripten_bind_LocalConvexResult_set_m_localShapeInfo_1"] = Module["asm"]["emscripten_bind_LocalConvexResult_set_m_localShapeInfo_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_get_m_hitNormalLocal_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitNormalLocal_0"] = function() {
  return (_emscripten_bind_LocalConvexResult_get_m_hitNormalLocal_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitNormalLocal_0"] = Module["asm"]["emscripten_bind_LocalConvexResult_get_m_hitNormalLocal_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_set_m_hitNormalLocal_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitNormalLocal_1"] = function() {
  return (_emscripten_bind_LocalConvexResult_set_m_hitNormalLocal_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitNormalLocal_1"] = Module["asm"]["emscripten_bind_LocalConvexResult_set_m_hitNormalLocal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_get_m_hitPointLocal_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitPointLocal_0"] = function() {
  return (_emscripten_bind_LocalConvexResult_get_m_hitPointLocal_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitPointLocal_0"] = Module["asm"]["emscripten_bind_LocalConvexResult_get_m_hitPointLocal_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_set_m_hitPointLocal_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitPointLocal_1"] = function() {
  return (_emscripten_bind_LocalConvexResult_set_m_hitPointLocal_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitPointLocal_1"] = Module["asm"]["emscripten_bind_LocalConvexResult_set_m_hitPointLocal_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_get_m_hitFraction_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitFraction_0"] = function() {
  return (_emscripten_bind_LocalConvexResult_get_m_hitFraction_0 = Module["_emscripten_bind_LocalConvexResult_get_m_hitFraction_0"] = Module["asm"]["emscripten_bind_LocalConvexResult_get_m_hitFraction_0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult_set_m_hitFraction_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitFraction_1"] = function() {
  return (_emscripten_bind_LocalConvexResult_set_m_hitFraction_1 = Module["_emscripten_bind_LocalConvexResult_set_m_hitFraction_1"] = Module["asm"]["emscripten_bind_LocalConvexResult_set_m_hitFraction_1"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_bind_LocalConvexResult___destroy___0 = Module["_emscripten_bind_LocalConvexResult___destroy___0"] = function() {
  return (_emscripten_bind_LocalConvexResult___destroy___0 = Module["_emscripten_bind_LocalConvexResult___destroy___0"] = Module["asm"]["emscripten_bind_LocalConvexResult___destroy___0"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_btConstraintParams_BT_CONSTRAINT_ERP = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_ERP"] = function() {
  return (_emscripten_enum_btConstraintParams_BT_CONSTRAINT_ERP = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_ERP"] = Module["asm"]["emscripten_enum_btConstraintParams_BT_CONSTRAINT_ERP"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_ERP = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_ERP"] = function() {
  return (_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_ERP = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_ERP"] = Module["asm"]["emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_ERP"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_btConstraintParams_BT_CONSTRAINT_CFM = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_CFM"] = function() {
  return (_emscripten_enum_btConstraintParams_BT_CONSTRAINT_CFM = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_CFM"] = Module["asm"]["emscripten_enum_btConstraintParams_BT_CONSTRAINT_CFM"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_CFM = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_CFM"] = function() {
  return (_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_CFM = Module["_emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_CFM"] = Module["asm"]["emscripten_enum_btConstraintParams_BT_CONSTRAINT_STOP_CFM"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_FLOAT = Module["_emscripten_enum_PHY_ScalarType_PHY_FLOAT"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_FLOAT = Module["_emscripten_enum_PHY_ScalarType_PHY_FLOAT"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_FLOAT"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_DOUBLE = Module["_emscripten_enum_PHY_ScalarType_PHY_DOUBLE"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_DOUBLE = Module["_emscripten_enum_PHY_ScalarType_PHY_DOUBLE"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_DOUBLE"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_INTEGER = Module["_emscripten_enum_PHY_ScalarType_PHY_INTEGER"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_INTEGER = Module["_emscripten_enum_PHY_ScalarType_PHY_INTEGER"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_INTEGER"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_SHORT = Module["_emscripten_enum_PHY_ScalarType_PHY_SHORT"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_SHORT = Module["_emscripten_enum_PHY_ScalarType_PHY_SHORT"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_SHORT"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_FIXEDPOINT88 = Module["_emscripten_enum_PHY_ScalarType_PHY_FIXEDPOINT88"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_FIXEDPOINT88 = Module["_emscripten_enum_PHY_ScalarType_PHY_FIXEDPOINT88"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_FIXEDPOINT88"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _emscripten_enum_PHY_ScalarType_PHY_UCHAR = Module["_emscripten_enum_PHY_ScalarType_PHY_UCHAR"] = function() {
  return (_emscripten_enum_PHY_ScalarType_PHY_UCHAR = Module["_emscripten_enum_PHY_ScalarType_PHY_UCHAR"] = Module["asm"]["emscripten_enum_PHY_ScalarType_PHY_UCHAR"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _malloc = Module["_malloc"] = function() {
  return (_malloc = Module["_malloc"] = Module["asm"]["malloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var _free = Module["_free"] = function() {
  return (_free = Module["_free"] = Module["asm"]["free"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var ___errno_location = Module["___errno_location"] = function() {
  return (___errno_location = Module["___errno_location"] = Module["asm"]["__errno_location"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackSave = Module["stackSave"] = function() {
  return (stackSave = Module["stackSave"] = Module["asm"]["stackSave"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackRestore = Module["stackRestore"] = function() {
  return (stackRestore = Module["stackRestore"] = Module["asm"]["stackRestore"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var stackAlloc = Module["stackAlloc"] = function() {
  return (stackAlloc = Module["stackAlloc"] = Module["asm"]["stackAlloc"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var __growWasmMemory = Module["__growWasmMemory"] = function() {
  return (__growWasmMemory = Module["__growWasmMemory"] = Module["asm"]["__growWasmMemory"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_ii = Module["dynCall_ii"] = function() {
  return (dynCall_ii = Module["dynCall_ii"] = Module["asm"]["dynCall_ii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_vi = Module["dynCall_vi"] = function() {
  return (dynCall_vi = Module["dynCall_vi"] = Module["asm"]["dynCall_vi"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iii = Module["dynCall_iii"] = function() {
  return (dynCall_iii = Module["dynCall_iii"] = Module["asm"]["dynCall_iii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiii = Module["dynCall_fiii"] = function() {
  return (dynCall_fiii = Module["dynCall_fiii"] = Module["asm"]["dynCall_fiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_v = Module["dynCall_v"] = function() {
  return (dynCall_v = Module["dynCall_v"] = Module["asm"]["dynCall_v"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_vii = Module["dynCall_vii"] = function() {
  return (dynCall_vii = Module["dynCall_vii"] = Module["asm"]["dynCall_vii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiii = Module["dynCall_iiii"] = function() {
  return (dynCall_iiii = Module["dynCall_iiii"] = Module["asm"]["dynCall_iiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiiii = Module["dynCall_iiiii"] = function() {
  return (dynCall_iiiii = Module["dynCall_iiiii"] = Module["asm"]["dynCall_iiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viii = Module["dynCall_viii"] = function() {
  return (dynCall_viii = Module["dynCall_viii"] = Module["asm"]["dynCall_viii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiiiiiii = Module["dynCall_fiiiiiiii"] = function() {
  return (dynCall_fiiiiiiii = Module["dynCall_fiiiiiiii"] = Module["asm"]["dynCall_fiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiii = Module["dynCall_viiii"] = function() {
  return (dynCall_viiii = Module["dynCall_viiii"] = Module["asm"]["dynCall_viiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiii = Module["dynCall_viiiii"] = function() {
  return (dynCall_viiiii = Module["dynCall_viiiii"] = Module["asm"]["dynCall_viiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_vifii = Module["dynCall_vifii"] = function() {
  return (dynCall_vifii = Module["dynCall_vifii"] = Module["asm"]["dynCall_vifii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viifi = Module["dynCall_viifi"] = function() {
  return (dynCall_viifi = Module["dynCall_viifi"] = Module["asm"]["dynCall_viifi"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiiiiif = Module["dynCall_viiiiiiiif"] = function() {
  return (dynCall_viiiiiiiif = Module["dynCall_viiiiiiiif"] = Module["asm"]["dynCall_viiiiiiiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiif = Module["dynCall_viiiiif"] = function() {
  return (dynCall_viiiiif = Module["dynCall_viiiiif"] = Module["asm"]["dynCall_viiiiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiifii = Module["dynCall_viiifii"] = function() {
  return (dynCall_viiifii = Module["dynCall_viiifii"] = Module["asm"]["dynCall_viiifii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viif = Module["dynCall_viif"] = function() {
  return (dynCall_viif = Module["dynCall_viif"] = Module["asm"]["dynCall_viif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiffffiif = Module["dynCall_viiiiffffiif"] = function() {
  return (dynCall_viiiiffffiif = Module["dynCall_viiiiffffiif"] = Module["asm"]["dynCall_viiiiffffiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiifffffifi = Module["dynCall_viiiifffffifi"] = function() {
  return (dynCall_viiiifffffifi = Module["dynCall_viiiifffffifi"] = Module["asm"]["dynCall_viiiifffffifi"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viffiii = Module["dynCall_viffiii"] = function() {
  return (dynCall_viffiii = Module["dynCall_viffiii"] = Module["asm"]["dynCall_viffiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viifii = Module["dynCall_viifii"] = function() {
  return (dynCall_viifii = Module["dynCall_viifii"] = Module["asm"]["dynCall_viifii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiiiiiiiii = Module["dynCall_iiiiiiiiii"] = function() {
  return (dynCall_iiiiiiiiii = Module["dynCall_iiiiiiiiii"] = Module["asm"]["dynCall_iiiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiii = Module["dynCall_viiiiii"] = function() {
  return (dynCall_viiiiii = Module["dynCall_viiiiii"] = Module["asm"]["dynCall_viiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_vif = Module["dynCall_vif"] = function() {
  return (dynCall_vif = Module["dynCall_vif"] = Module["asm"]["dynCall_vif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiifii = Module["dynCall_fiifii"] = function() {
  return (dynCall_fiifii = Module["dynCall_fiifii"] = Module["asm"]["dynCall_fiifii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiifii = Module["dynCall_fiiifii"] = function() {
  return (dynCall_fiiifii = Module["dynCall_fiiifii"] = Module["asm"]["dynCall_fiiifii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiif = Module["dynCall_viiif"] = function() {
  return (dynCall_viiif = Module["dynCall_viiif"] = Module["asm"]["dynCall_viiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiiii = Module["dynCall_fiiiii"] = function() {
  return (dynCall_fiiiii = Module["dynCall_fiiiii"] = Module["asm"]["dynCall_fiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fi = Module["dynCall_fi"] = function() {
  return (dynCall_fi = Module["dynCall_fi"] = Module["asm"]["dynCall_fi"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fif = Module["dynCall_fif"] = function() {
  return (dynCall_fif = Module["dynCall_fif"] = Module["asm"]["dynCall_fif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_vifi = Module["dynCall_vifi"] = function() {
  return (dynCall_vifi = Module["dynCall_vifi"] = Module["asm"]["dynCall_vifi"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiif = Module["dynCall_iiif"] = function() {
  return (dynCall_iiif = Module["dynCall_iiif"] = Module["asm"]["dynCall_iiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiiii = Module["dynCall_viiiiiii"] = function() {
  return (dynCall_viiiiiii = Module["dynCall_viiiiiii"] = Module["asm"]["dynCall_viiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiiiiiii = Module["dynCall_viiiiiiiiii"] = function() {
  return (dynCall_viiiiiiiiii = Module["dynCall_viiiiiiiiii"] = Module["asm"]["dynCall_viiiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiiiiii = Module["dynCall_iiiiiii"] = function() {
  return (dynCall_iiiiiii = Module["dynCall_iiiiiii"] = Module["asm"]["dynCall_iiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iiiiiiiiiii = Module["dynCall_iiiiiiiiiii"] = function() {
  return (dynCall_iiiiiiiiiii = Module["dynCall_iiiiiiiiiii"] = Module["asm"]["dynCall_iiiiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiif = Module["dynCall_viiiif"] = function() {
  return (dynCall_viiiif = Module["dynCall_viiiif"] = Module["asm"]["dynCall_viiiif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiiiiiiiii = Module["dynCall_fiiiiiiiiii"] = function() {
  return (dynCall_fiiiiiiiiii = Module["dynCall_fiiiiiiiiii"] = Module["asm"]["dynCall_fiiiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viiiiiiiii = Module["dynCall_viiiiiiiii"] = function() {
  return (dynCall_viiiiiiiii = Module["dynCall_viiiiiiiii"] = Module["asm"]["dynCall_viiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiii = Module["dynCall_fiiii"] = function() {
  return (dynCall_fiiii = Module["dynCall_fiiii"] = Module["asm"]["dynCall_fiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_fiiiiiiiii = Module["dynCall_fiiiiiiiii"] = function() {
  return (dynCall_fiiiiiiiii = Module["dynCall_fiiiiiiiii"] = Module["asm"]["dynCall_fiiiiiiiii"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_iifif = Module["dynCall_iifif"] = function() {
  return (dynCall_iifif = Module["dynCall_iifif"] = Module["asm"]["dynCall_iifif"]).apply(null, arguments);
};

/** @type {function(...*):?} */
var dynCall_viff = Module["dynCall_viff"] = function() {
  return (dynCall_viff = Module["dynCall_viff"] = Module["asm"]["dynCall_viff"]).apply(null, arguments);
};



/**
 * @license
 * Copyright 2010 The Emscripten Authors
 * SPDX-License-Identifier: MIT
 */

// === Auto-generated postamble setup entry stuff ===











Module["UTF8ToString"] = UTF8ToString;




























































































































var calledRun;

/**
 * @constructor
 * @this {ExitStatus}
 */
function ExitStatus(status) {
  this.name = "ExitStatus";
  this.message = "Program terminated with exit(" + status + ")";
  this.status = status;
}

var calledMain = false;


dependenciesFulfilled = function runCaller() {
  // If run has never been called, and we should call run (INVOKE_RUN is true, and Module.noInitialRun is not false)
  if (!calledRun) run();
  if (!calledRun) dependenciesFulfilled = runCaller; // try this again later, after new deps are fulfilled
};





/** @type {function(Array=)} */
function run(args) {
  args = args || arguments_;

  if (runDependencies > 0) {
    return;
  }


  preRun();

  if (runDependencies > 0) return; // a preRun added a dependency, run will be called later

  function doRun() {
    // run may have just been called through dependencies being fulfilled just in this very frame,
    // or while the async setStatus time below was happening
    if (calledRun) return;
    calledRun = true;
    Module['calledRun'] = true;

    if (ABORT) return;

    initRuntime();

    preMain();

    readyPromiseResolve(Module);
    if (Module['onRuntimeInitialized']) Module['onRuntimeInitialized']();


    postRun();
  }

  if (Module['setStatus']) {
    Module['setStatus']('Running...');
    setTimeout(function() {
      setTimeout(function() {
        Module['setStatus']('');
      }, 1);
      doRun();
    }, 1);
  } else
  {
    doRun();
  }
}
Module['run'] = run;


/** @param {boolean|number=} implicit */
function exit(status, implicit) {

  // if this is just main exit-ing implicitly, and the status is 0, then we
  // don't need to do anything here and can just leave. if the status is
  // non-zero, though, then we need to report it.
  // (we may have warned about this earlier, if a situation justifies doing so)
  if (implicit && noExitRuntime && status === 0) {
    return;
  }

  if (noExitRuntime) {
  } else {

    ABORT = true;
    EXITSTATUS = status;

    exitRuntime();

    if (Module['onExit']) Module['onExit'](status);
  }

  quit_(status, new ExitStatus(status));
}

if (Module['preInit']) {
  if (typeof Module['preInit'] == 'function') Module['preInit'] = [Module['preInit']];
  while (Module['preInit'].length > 0) {
    Module['preInit'].pop()();
  }
}


  noExitRuntime = true;

run();






// {{MODULE_ADDITIONS}}



