[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    0.000147553 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.0016879 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    0.000119643 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00881816 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.0137896 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000185173 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0542987 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0521453 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00794596 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0401875 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0125446 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0340288 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.0078816 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0158893 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.112859 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0667778 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0166589 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0350269 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0564866 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.139382 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0616388 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0143337 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0578267 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0142757 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.060396 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0347145 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0382816 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00722754 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0337362 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0302048 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0338282 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.0438526 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0593558 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0843658 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0536069 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00664999 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   8.5791e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-globals-optimizing...    0.00502241 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 1.31626 seconds.
[PassRunner] (final validation)
