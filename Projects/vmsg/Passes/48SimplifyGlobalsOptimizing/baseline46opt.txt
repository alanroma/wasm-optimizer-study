[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    2.8703e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000734713 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.9675e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.0033357 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00297139 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000187973 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0306696 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0422473 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00361793 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.0143698 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.00725077 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0165963 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-added-constants...       0.00408881 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: code-pushing...                   0.0245847 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals-nostructure...    0.06621 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0369964 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.0139567 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0130392 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0143918 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: simplify-locals...                0.0389283 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0212683 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00491714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: coalesce-locals...                0.0135346 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: reorder-locals...                 0.00458086 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.020806 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.0112581 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0252094 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00276838 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: merge-blocks...                   0.00969254 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute...                     0.0103042 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.00992763 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: rse...                            0.010321 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: vacuum...                         0.0183007 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dae-optimizing...                 0.0424617 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inlining-optimizing...            0.0261714 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00148038 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-import-elimination...   1.6018e-05 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.567244 seconds.
[PassRunner] (final validation)
