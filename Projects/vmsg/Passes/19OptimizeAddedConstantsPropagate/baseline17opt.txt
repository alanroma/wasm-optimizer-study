[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    1.8766e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000764943 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    2.0682e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00325656 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00298703 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000187219 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: ssa-nomerge...                    0.0959756 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: dce...                            0.0316841 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-brs...              0.0426511 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: remove-unused-names...            0.00371051 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: optimize-instructions...          0.014288 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: pick-load-signs...                0.0071342 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: precompute-propagate...           0.263237 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.465916 seconds.
[PassRunner] (final validation)
