[PassRunner] running passes...
[PassRunner]   running pass: strip-dwarf...                    1.4135e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: post-emscripten...                0.000765435 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: inline-main...                    1.9032e-05 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: no-exit-runtime...                0.00323344 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: duplicate-function-elimination... 0.00301182 seconds.
[PassRunner]   (validating)
[PassRunner]   running pass: memory-packing...                 0.000160579 seconds.
[PassRunner]   (validating)
[PassRunner] passes took 0.00720444 seconds.
[PassRunner] (final validation)
