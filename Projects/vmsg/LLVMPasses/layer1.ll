; ModuleID = 'layer1.c'
source_filename = "layer1.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.mpstr_tag = type { %struct.buf*, %struct.buf*, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.frame, %struct.III_sideinfo, [2 x [3904 x i8]], [2 x [2 x [576 x float]]], [2 x i32], i32, i32, [2 x [2 x [272 x float]]], i32, i32, i32, i8*, %struct.plotting_data*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.buf = type { i8*, i32, i32, %struct.buf*, %struct.buf* }
%struct.frame = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, %struct.al_table2*, i32, i32 }
%struct.al_table2 = type opaque
%struct.III_sideinfo = type { i32, i32, [2 x %struct.anon] }
%struct.anon = type { [2 x %struct.gr_info_s] }
%struct.gr_info_s = type { i32, i32, i32, i32, i32, i32, [3 x i32], [3 x i32], [3 x i32], i32, i32, i32, i32, i32, i32, i32, [3 x float*], float* }
%struct.plotting_data = type opaque
%struct.sideinfo_layer_I_struct = type { [32 x [2 x i8]], [32 x [2 x i8]] }

@gd_are_hip_tables_layer1_initialized = internal global i32 0, align 4
@.str = private unnamed_addr constant [60 x i8] c"hip: Aborting layer 1 decode, illegal bit allocation value\0A\00", align 1
@muls = external global [27 x [64 x float]], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @hip_init_tables_layer1() #0 {
entry:
  %0 = load i32, i32* @gd_are_hip_tables_layer1_initialized, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  store i32 1, i32* @gd_are_hip_tables_layer1_initialized, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @decode_layer1_sideinfo(%struct.mpstr_tag* %mp) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @decode_layer1_frame(%struct.mpstr_tag* %mp, i8* %pcm_sample, i32* %pcm_point) #0 {
entry:
  %retval = alloca i32, align 4
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %pcm_sample.addr = alloca i8*, align 4
  %pcm_point.addr = alloca i32*, align 4
  %fraction = alloca [2 x [32 x float]], align 16
  %si = alloca %struct.sideinfo_layer_I_struct, align 1
  %fr = alloca %struct.frame*, align 4
  %single = alloca i32, align 4
  %i = alloca i32, align 4
  %clip = alloca i32, align 4
  %p1 = alloca i32, align 4
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store i8* %pcm_sample, i8** %pcm_sample.addr, align 4
  store i32* %pcm_point, i32** %pcm_point.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 17
  store %struct.frame* %fr1, %struct.frame** %fr, align 4
  %1 = load %struct.frame*, %struct.frame** %fr, align 4
  %single2 = getelementptr inbounds %struct.frame, %struct.frame* %1, i32 0, i32 1
  %2 = load i32, i32* %single2, align 4
  store i32 %2, i32* %single, align 4
  store i32 0, i32* %clip, align 4
  %3 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call = call i32 @I_step_one(%struct.mpstr_tag* %3, %struct.sideinfo_layer_I_struct* %si)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %report_err = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %4, i32 0, i32 32
  %5 = load void (i8*, i8*)*, void (i8*, i8*)** %report_err, align 4
  call void (void (i8*, i8*)*, i8*, ...) @lame_report_fnc(void (i8*, i8*)* %5, i8* getelementptr inbounds ([60 x i8], [60 x i8]* @.str, i32 0, i32 0))
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load %struct.frame*, %struct.frame** %fr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %6, i32 0, i32 0
  %7 = load i32, i32* %stereo, align 4
  %cmp = icmp eq i32 %7, 1
  br i1 %cmp, label %if.then4, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %8 = load i32, i32* %single, align 4
  %cmp3 = icmp eq i32 %8, 3
  br i1 %cmp3, label %if.then4, label %if.end5

if.then4:                                         ; preds = %lor.lhs.false, %if.end
  store i32 0, i32* %single, align 4
  br label %if.end5

if.end5:                                          ; preds = %if.then4, %lor.lhs.false
  %9 = load i32, i32* %single, align 4
  %cmp6 = icmp sge i32 %9, 0
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then7
  %10 = load i32, i32* %i, align 4
  %cmp8 = icmp slt i32 %10, 12
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arraydecay = getelementptr inbounds [2 x [32 x float]], [2 x [32 x float]]* %fraction, i32 0, i32 0
  call void @I_step_two(%struct.mpstr_tag* %11, %struct.sideinfo_layer_I_struct* %si, [32 x float]* %arraydecay)
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %13 = load i32, i32* %single, align 4
  %arrayidx = getelementptr inbounds [2 x [32 x float]], [2 x [32 x float]]* %fraction, i32 0, i32 %13
  %arraydecay9 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx, i32 0, i32 0
  %14 = load i8*, i8** %pcm_sample.addr, align 4
  %15 = load i32*, i32** %pcm_point.addr, align 4
  %call10 = call i32 @synth_1to1_mono(%struct.mpstr_tag* %12, float* %arraydecay9, i8* %14, i32* %15)
  %16 = load i32, i32* %clip, align 4
  %add = add nsw i32 %16, %call10
  store i32 %add, i32* %clip, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end26

if.else:                                          ; preds = %if.end5
  store i32 0, i32* %i, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc23, %if.else
  %18 = load i32, i32* %i, align 4
  %cmp12 = icmp slt i32 %18, 12
  br i1 %cmp12, label %for.body13, label %for.end25

for.body13:                                       ; preds = %for.cond11
  %19 = load i32*, i32** %pcm_point.addr, align 4
  %20 = load i32, i32* %19, align 4
  store i32 %20, i32* %p1, align 4
  %21 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arraydecay14 = getelementptr inbounds [2 x [32 x float]], [2 x [32 x float]]* %fraction, i32 0, i32 0
  call void @I_step_two(%struct.mpstr_tag* %21, %struct.sideinfo_layer_I_struct* %si, [32 x float]* %arraydecay14)
  %22 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arrayidx15 = getelementptr inbounds [2 x [32 x float]], [2 x [32 x float]]* %fraction, i32 0, i32 0
  %arraydecay16 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx15, i32 0, i32 0
  %23 = load i8*, i8** %pcm_sample.addr, align 4
  %call17 = call i32 @synth_1to1(%struct.mpstr_tag* %22, float* %arraydecay16, i32 0, i8* %23, i32* %p1)
  %24 = load i32, i32* %clip, align 4
  %add18 = add nsw i32 %24, %call17
  store i32 %add18, i32* %clip, align 4
  %25 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %arrayidx19 = getelementptr inbounds [2 x [32 x float]], [2 x [32 x float]]* %fraction, i32 0, i32 1
  %arraydecay20 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx19, i32 0, i32 0
  %26 = load i8*, i8** %pcm_sample.addr, align 4
  %27 = load i32*, i32** %pcm_point.addr, align 4
  %call21 = call i32 @synth_1to1(%struct.mpstr_tag* %25, float* %arraydecay20, i32 1, i8* %26, i32* %27)
  %28 = load i32, i32* %clip, align 4
  %add22 = add nsw i32 %28, %call21
  store i32 %add22, i32* %clip, align 4
  br label %for.inc23

for.inc23:                                        ; preds = %for.body13
  %29 = load i32, i32* %i, align 4
  %inc24 = add nsw i32 %29, 1
  store i32 %inc24, i32* %i, align 4
  br label %for.cond11

for.end25:                                        ; preds = %for.cond11
  br label %if.end26

if.end26:                                         ; preds = %for.end25, %for.end
  %30 = load i32, i32* %clip, align 4
  store i32 %30, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end26, %if.then
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define internal i32 @I_step_one(%struct.mpstr_tag* %mp, %struct.sideinfo_layer_I_struct* %si) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %si.addr = alloca %struct.sideinfo_layer_I_struct*, align 4
  %fr = alloca %struct.frame*, align 4
  %jsbound = alloca i32, align 4
  %i = alloca i32, align 4
  %illegal_value_detected = alloca i32, align 4
  %ba15 = alloca i8, align 1
  %b0 = alloca i8, align 1
  %b1 = alloca i8, align 1
  %b = alloca i8, align 1
  %n0 = alloca i8, align 1
  %n1 = alloca i8, align 1
  %b044 = alloca i8, align 1
  %b153 = alloca i8, align 1
  %b075 = alloca i8, align 1
  %n092 = alloca i8, align 1
  %b096 = alloca i8, align 1
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store %struct.sideinfo_layer_I_struct* %si, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 17
  store %struct.frame* %fr1, %struct.frame** %fr, align 4
  %1 = load %struct.frame*, %struct.frame** %fr, align 4
  %mode = getelementptr inbounds %struct.frame, %struct.frame* %1, i32 0, i32 11
  %2 = load i32, i32* %mode, align 4
  %cmp = icmp eq i32 %2, 1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %struct.frame*, %struct.frame** %fr, align 4
  %mode_ext = getelementptr inbounds %struct.frame, %struct.frame* %3, i32 0, i32 12
  %4 = load i32, i32* %mode_ext, align 4
  %shl = shl i32 %4, 2
  %add = add nsw i32 %shl, 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 32, %cond.false ]
  store i32 %cond, i32* %jsbound, align 4
  store i32 0, i32* %illegal_value_detected, align 4
  store i8 15, i8* %ba15, align 1
  %5 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %6 = bitcast %struct.sideinfo_layer_I_struct* %5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %6, i8 0, i32 128, i1 false)
  %7 = load %struct.frame*, %struct.frame** %fr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %7, i32 0, i32 0
  %8 = load i32, i32* %stereo, align 4
  %cmp2 = icmp eq i32 %8, 2
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %cond.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %jsbound, align 4
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %11, i32 4)
  store i8 %call, i8* %b0, align 1
  %12 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call4 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %12, i32 4)
  store i8 %call4, i8* %b1, align 1
  %13 = load i8, i8* %b0, align 1
  %14 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %14, i32 0, i32 0
  %15 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation, i32 0, i32 %15
  %arrayidx5 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx, i32 0, i32 0
  store i8 %13, i8* %arrayidx5, align 1
  %16 = load i8, i8* %b1, align 1
  %17 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation6 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation6, i32 0, i32 %18
  %arrayidx8 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx7, i32 0, i32 1
  store i8 %16, i8* %arrayidx8, align 1
  %19 = load i8, i8* %b0, align 1
  %conv = zext i8 %19 to i32
  %cmp9 = icmp eq i32 %conv, 15
  br i1 %cmp9, label %if.then14, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %20 = load i8, i8* %b1, align 1
  %conv11 = zext i8 %20 to i32
  %cmp12 = icmp eq i32 %conv11, 15
  br i1 %cmp12, label %if.then14, label %if.end

if.then14:                                        ; preds = %lor.lhs.false, %for.body
  store i32 1, i32* %illegal_value_detected, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load i32, i32* %jsbound, align 4
  store i32 %22, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc31, %for.end
  %23 = load i32, i32* %i, align 4
  %cmp16 = icmp slt i32 %23, 32
  br i1 %cmp16, label %for.body18, label %for.end33

for.body18:                                       ; preds = %for.cond15
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call19 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %24, i32 4)
  store i8 %call19, i8* %b, align 1
  %25 = load i8, i8* %b, align 1
  %26 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation20 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %26, i32 0, i32 0
  %27 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation20, i32 0, i32 %27
  %arrayidx22 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx21, i32 0, i32 0
  store i8 %25, i8* %arrayidx22, align 1
  %28 = load i8, i8* %b, align 1
  %29 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation23 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %29, i32 0, i32 0
  %30 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation23, i32 0, i32 %30
  %arrayidx25 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx24, i32 0, i32 1
  store i8 %28, i8* %arrayidx25, align 1
  %31 = load i8, i8* %b, align 1
  %conv26 = zext i8 %31 to i32
  %cmp27 = icmp eq i32 %conv26, 15
  br i1 %cmp27, label %if.then29, label %if.end30

if.then29:                                        ; preds = %for.body18
  store i32 1, i32* %illegal_value_detected, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %for.body18
  br label %for.inc31

for.inc31:                                        ; preds = %if.end30
  %32 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %32, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond15

for.end33:                                        ; preds = %for.cond15
  store i32 0, i32* %i, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc68, %for.end33
  %33 = load i32, i32* %i, align 4
  %cmp35 = icmp slt i32 %33, 32
  br i1 %cmp35, label %for.body37, label %for.end70

for.body37:                                       ; preds = %for.cond34
  %34 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation38 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %34, i32 0, i32 0
  %35 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation38, i32 0, i32 %35
  %arrayidx40 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx39, i32 0, i32 0
  %36 = load i8, i8* %arrayidx40, align 1
  store i8 %36, i8* %n0, align 1
  %37 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation41 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %37, i32 0, i32 0
  %38 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation41, i32 0, i32 %38
  %arrayidx43 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx42, i32 0, i32 1
  %39 = load i8, i8* %arrayidx43, align 1
  store i8 %39, i8* %n1, align 1
  %40 = load i8, i8* %n0, align 1
  %conv45 = zext i8 %40 to i32
  %tobool = icmp ne i32 %conv45, 0
  br i1 %tobool, label %cond.true46, label %cond.false49

cond.true46:                                      ; preds = %for.body37
  %41 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call47 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %41, i32 6)
  %conv48 = zext i8 %call47 to i32
  br label %cond.end50

cond.false49:                                     ; preds = %for.body37
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false49, %cond.true46
  %cond51 = phi i32 [ %conv48, %cond.true46 ], [ 0, %cond.false49 ]
  %conv52 = trunc i32 %cond51 to i8
  store i8 %conv52, i8* %b044, align 1
  %42 = load i8, i8* %n1, align 1
  %conv54 = zext i8 %42 to i32
  %tobool55 = icmp ne i32 %conv54, 0
  br i1 %tobool55, label %cond.true56, label %cond.false59

cond.true56:                                      ; preds = %cond.end50
  %43 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call57 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %43, i32 6)
  %conv58 = zext i8 %call57 to i32
  br label %cond.end60

cond.false59:                                     ; preds = %cond.end50
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false59, %cond.true56
  %cond61 = phi i32 [ %conv58, %cond.true56 ], [ 0, %cond.false59 ]
  %conv62 = trunc i32 %cond61 to i8
  store i8 %conv62, i8* %b153, align 1
  %44 = load i8, i8* %b044, align 1
  %45 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %45, i32 0, i32 1
  %46 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor, i32 0, i32 %46
  %arrayidx64 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx63, i32 0, i32 0
  store i8 %44, i8* %arrayidx64, align 1
  %47 = load i8, i8* %b153, align 1
  %48 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor65 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %48, i32 0, i32 1
  %49 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor65, i32 0, i32 %49
  %arrayidx67 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx66, i32 0, i32 1
  store i8 %47, i8* %arrayidx67, align 1
  br label %for.inc68

for.inc68:                                        ; preds = %cond.end60
  %50 = load i32, i32* %i, align 4
  %inc69 = add nsw i32 %50, 1
  store i32 %inc69, i32* %i, align 4
  br label %for.cond34

for.end70:                                        ; preds = %for.cond34
  br label %if.end112

if.else:                                          ; preds = %cond.end
  store i32 0, i32* %i, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc85, %if.else
  %51 = load i32, i32* %i, align 4
  %cmp72 = icmp slt i32 %51, 32
  br i1 %cmp72, label %for.body74, label %for.end87

for.body74:                                       ; preds = %for.cond71
  %52 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call76 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %52, i32 4)
  store i8 %call76, i8* %b075, align 1
  %53 = load i8, i8* %b075, align 1
  %54 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation77 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %54, i32 0, i32 0
  %55 = load i32, i32* %i, align 4
  %arrayidx78 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation77, i32 0, i32 %55
  %arrayidx79 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx78, i32 0, i32 0
  store i8 %53, i8* %arrayidx79, align 1
  %56 = load i8, i8* %b075, align 1
  %conv80 = zext i8 %56 to i32
  %cmp81 = icmp eq i32 %conv80, 15
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %for.body74
  store i32 1, i32* %illegal_value_detected, align 4
  br label %if.end84

if.end84:                                         ; preds = %if.then83, %for.body74
  br label %for.inc85

for.inc85:                                        ; preds = %if.end84
  %57 = load i32, i32* %i, align 4
  %inc86 = add nsw i32 %57, 1
  store i32 %inc86, i32* %i, align 4
  br label %for.cond71

for.end87:                                        ; preds = %for.cond71
  store i32 0, i32* %i, align 4
  br label %for.cond88

for.cond88:                                       ; preds = %for.inc109, %for.end87
  %58 = load i32, i32* %i, align 4
  %cmp89 = icmp slt i32 %58, 32
  br i1 %cmp89, label %for.body91, label %for.end111

for.body91:                                       ; preds = %for.cond88
  %59 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation93 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %59, i32 0, i32 0
  %60 = load i32, i32* %i, align 4
  %arrayidx94 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation93, i32 0, i32 %60
  %arrayidx95 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx94, i32 0, i32 0
  %61 = load i8, i8* %arrayidx95, align 1
  store i8 %61, i8* %n092, align 1
  %62 = load i8, i8* %n092, align 1
  %conv97 = zext i8 %62 to i32
  %tobool98 = icmp ne i32 %conv97, 0
  br i1 %tobool98, label %cond.true99, label %cond.false102

cond.true99:                                      ; preds = %for.body91
  %63 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %call100 = call zeroext i8 @get_leq_8_bits(%struct.mpstr_tag* %63, i32 6)
  %conv101 = zext i8 %call100 to i32
  br label %cond.end103

cond.false102:                                    ; preds = %for.body91
  br label %cond.end103

cond.end103:                                      ; preds = %cond.false102, %cond.true99
  %cond104 = phi i32 [ %conv101, %cond.true99 ], [ 0, %cond.false102 ]
  %conv105 = trunc i32 %cond104 to i8
  store i8 %conv105, i8* %b096, align 1
  %64 = load i8, i8* %b096, align 1
  %65 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor106 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %65, i32 0, i32 1
  %66 = load i32, i32* %i, align 4
  %arrayidx107 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor106, i32 0, i32 %66
  %arrayidx108 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx107, i32 0, i32 0
  store i8 %64, i8* %arrayidx108, align 1
  br label %for.inc109

for.inc109:                                       ; preds = %cond.end103
  %67 = load i32, i32* %i, align 4
  %inc110 = add nsw i32 %67, 1
  store i32 %inc110, i32* %i, align 4
  br label %for.cond88

for.end111:                                       ; preds = %for.cond88
  br label %if.end112

if.end112:                                        ; preds = %for.end111, %for.end70
  %68 = load i32, i32* %illegal_value_detected, align 4
  ret i32 %68
}

declare void @lame_report_fnc(void (i8*, i8*)*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define internal void @I_step_two(%struct.mpstr_tag* %mp, %struct.sideinfo_layer_I_struct* %si, [32 x float]* %fraction) #0 {
entry:
  %mp.addr = alloca %struct.mpstr_tag*, align 4
  %si.addr = alloca %struct.sideinfo_layer_I_struct*, align 4
  %fraction.addr = alloca [32 x float]*, align 4
  %r0 = alloca double, align 8
  %r1 = alloca double, align 8
  %fr = alloca %struct.frame*, align 4
  %ds_limit = alloca i32, align 4
  %i = alloca i32, align 4
  %jsbound = alloca i32, align 4
  %i0 = alloca i8, align 1
  %i1 = alloca i8, align 1
  %n0 = alloca i8, align 1
  %n1 = alloca i8, align 1
  %v = alloca i16, align 2
  %v33 = alloca i16, align 2
  %i062 = alloca i8, align 1
  %i166 = alloca i8, align 1
  %n = alloca i8, align 1
  %v77 = alloca i16, align 2
  %w = alloca i32, align 4
  %n129 = alloca i8, align 1
  %j = alloca i8, align 1
  %v140 = alloca i16, align 2
  store %struct.mpstr_tag* %mp, %struct.mpstr_tag** %mp.addr, align 4
  store %struct.sideinfo_layer_I_struct* %si, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  store [32 x float]* %fraction, [32 x float]** %fraction.addr, align 4
  %0 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %fr1 = getelementptr inbounds %struct.mpstr_tag, %struct.mpstr_tag* %0, i32 0, i32 17
  store %struct.frame* %fr1, %struct.frame** %fr, align 4
  %1 = load %struct.frame*, %struct.frame** %fr, align 4
  %down_sample_sblimit = getelementptr inbounds %struct.frame, %struct.frame* %1, i32 0, i32 19
  %2 = load i32, i32* %down_sample_sblimit, align 4
  store i32 %2, i32* %ds_limit, align 4
  %3 = load %struct.frame*, %struct.frame** %fr, align 4
  %stereo = getelementptr inbounds %struct.frame, %struct.frame* %3, i32 0, i32 0
  %4 = load i32, i32* %stereo, align 4
  %cmp = icmp eq i32 %4, 2
  br i1 %cmp, label %if.then, label %if.else124

if.then:                                          ; preds = %entry
  %5 = load %struct.frame*, %struct.frame** %fr, align 4
  %mode = getelementptr inbounds %struct.frame, %struct.frame* %5, i32 0, i32 11
  %6 = load i32, i32* %mode, align 4
  %cmp2 = icmp eq i32 %6, 1
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %7 = load %struct.frame*, %struct.frame** %fr, align 4
  %mode_ext = getelementptr inbounds %struct.frame, %struct.frame* %7, i32 0, i32 12
  %8 = load i32, i32* %mode_ext, align 4
  %shl = shl i32 %8, 2
  %add = add nsw i32 %shl, 4
  br label %cond.end

cond.false:                                       ; preds = %if.then
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add, %cond.true ], [ 32, %cond.false ]
  store i32 %cond, i32* %jsbound, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %jsbound, align 4
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %11, i32 0, i32 1
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor, i32 0, i32 %12
  %arrayidx4 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx, i32 0, i32 0
  %13 = load i8, i8* %arrayidx4, align 1
  store i8 %13, i8* %i0, align 1
  %14 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor5 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %14, i32 0, i32 1
  %15 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor5, i32 0, i32 %15
  %arrayidx7 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx6, i32 0, i32 1
  %16 = load i8, i8* %arrayidx7, align 1
  store i8 %16, i8* %i1, align 1
  %17 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation, i32 0, i32 %18
  %arrayidx9 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx8, i32 0, i32 0
  %19 = load i8, i8* %arrayidx9, align 1
  store i8 %19, i8* %n0, align 1
  %20 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation10 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %20, i32 0, i32 0
  %21 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation10, i32 0, i32 %21
  %arrayidx12 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx11, i32 0, i32 1
  %22 = load i8, i8* %arrayidx12, align 1
  store i8 %22, i8* %n1, align 1
  %23 = load i8, i8* %n0, align 1
  %conv = zext i8 %23 to i32
  %cmp13 = icmp sgt i32 %conv, 0
  br i1 %cmp13, label %if.then15, label %if.else

if.then15:                                        ; preds = %for.body
  %24 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %25 = load i8, i8* %n0, align 1
  %conv16 = zext i8 %25 to i32
  %add17 = add nsw i32 %conv16, 1
  %call = call zeroext i16 @get_leq_16_bits(%struct.mpstr_tag* %24, i32 %add17)
  store i16 %call, i16* %v, align 2
  %26 = load i8, i8* %n0, align 1
  %conv18 = zext i8 %26 to i32
  %shl19 = shl i32 -1, %conv18
  %27 = load i16, i16* %v, align 2
  %conv20 = zext i16 %27 to i32
  %add21 = add nsw i32 %shl19, %conv20
  %add22 = add nsw i32 %add21, 1
  %conv23 = sitofp i32 %add22 to float
  %28 = load i8, i8* %n0, align 1
  %conv24 = zext i8 %28 to i32
  %add25 = add nsw i32 %conv24, 1
  %arrayidx26 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %add25
  %29 = load i8, i8* %i0, align 1
  %idxprom = zext i8 %29 to i32
  %arrayidx27 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx26, i32 0, i32 %idxprom
  %30 = load float, float* %arrayidx27, align 4
  %mul = fmul float %conv23, %30
  %conv28 = fpext float %mul to double
  store double %conv28, double* %r0, align 8
  br label %if.end

if.else:                                          ; preds = %for.body
  store double 0.000000e+00, double* %r0, align 8
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then15
  %31 = load i8, i8* %n1, align 1
  %conv29 = zext i8 %31 to i32
  %cmp30 = icmp sgt i32 %conv29, 0
  br i1 %cmp30, label %if.then32, label %if.else50

if.then32:                                        ; preds = %if.end
  %32 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %33 = load i8, i8* %n1, align 1
  %conv34 = zext i8 %33 to i32
  %add35 = add nsw i32 %conv34, 1
  %call36 = call zeroext i16 @get_leq_16_bits(%struct.mpstr_tag* %32, i32 %add35)
  store i16 %call36, i16* %v33, align 2
  %34 = load i8, i8* %n1, align 1
  %conv37 = zext i8 %34 to i32
  %shl38 = shl i32 -1, %conv37
  %35 = load i16, i16* %v33, align 2
  %conv39 = zext i16 %35 to i32
  %add40 = add nsw i32 %shl38, %conv39
  %add41 = add nsw i32 %add40, 1
  %conv42 = sitofp i32 %add41 to float
  %36 = load i8, i8* %n1, align 1
  %conv43 = zext i8 %36 to i32
  %add44 = add nsw i32 %conv43, 1
  %arrayidx45 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %add44
  %37 = load i8, i8* %i1, align 1
  %idxprom46 = zext i8 %37 to i32
  %arrayidx47 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx45, i32 0, i32 %idxprom46
  %38 = load float, float* %arrayidx47, align 4
  %mul48 = fmul float %conv42, %38
  %conv49 = fpext float %mul48 to double
  store double %conv49, double* %r1, align 8
  br label %if.end51

if.else50:                                        ; preds = %if.end
  store double 0.000000e+00, double* %r1, align 8
  br label %if.end51

if.end51:                                         ; preds = %if.else50, %if.then32
  %39 = load double, double* %r0, align 8
  %conv52 = fptrunc double %39 to float
  %40 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx53 = getelementptr inbounds [32 x float], [32 x float]* %40, i32 0
  %41 = load i32, i32* %i, align 4
  %arrayidx54 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx53, i32 0, i32 %41
  store float %conv52, float* %arrayidx54, align 4
  %42 = load double, double* %r1, align 8
  %conv55 = fptrunc double %42 to float
  %43 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx56 = getelementptr inbounds [32 x float], [32 x float]* %43, i32 1
  %44 = load i32, i32* %i, align 4
  %arrayidx57 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx56, i32 0, i32 %44
  store float %conv55, float* %arrayidx57, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end51
  %45 = load i32, i32* %i, align 4
  %inc = add nsw i32 %45, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %46 = load i32, i32* %jsbound, align 4
  store i32 %46, i32* %i, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc110, %for.end
  %47 = load i32, i32* %i, align 4
  %cmp59 = icmp slt i32 %47, 32
  br i1 %cmp59, label %for.body61, label %for.end112

for.body61:                                       ; preds = %for.cond58
  %48 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor63 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %48, i32 0, i32 1
  %49 = load i32, i32* %i, align 4
  %arrayidx64 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor63, i32 0, i32 %49
  %arrayidx65 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx64, i32 0, i32 0
  %50 = load i8, i8* %arrayidx65, align 1
  store i8 %50, i8* %i062, align 1
  %51 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor67 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %51, i32 0, i32 1
  %52 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor67, i32 0, i32 %52
  %arrayidx69 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx68, i32 0, i32 1
  %53 = load i8, i8* %arrayidx69, align 1
  store i8 %53, i8* %i166, align 1
  %54 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation70 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %54, i32 0, i32 0
  %55 = load i32, i32* %i, align 4
  %arrayidx71 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation70, i32 0, i32 %55
  %arrayidx72 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx71, i32 0, i32 0
  %56 = load i8, i8* %arrayidx72, align 1
  store i8 %56, i8* %n, align 1
  %57 = load i8, i8* %n, align 1
  %conv73 = zext i8 %57 to i32
  %cmp74 = icmp sgt i32 %conv73, 0
  br i1 %cmp74, label %if.then76, label %if.else102

if.then76:                                        ; preds = %for.body61
  %58 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %59 = load i8, i8* %n, align 1
  %conv78 = zext i8 %59 to i32
  %add79 = add nsw i32 %conv78, 1
  %call80 = call zeroext i16 @get_leq_16_bits(%struct.mpstr_tag* %58, i32 %add79)
  store i16 %call80, i16* %v77, align 2
  %60 = load i8, i8* %n, align 1
  %conv81 = zext i8 %60 to i32
  %shl82 = shl i32 -1, %conv81
  %61 = load i16, i16* %v77, align 2
  %conv83 = zext i16 %61 to i32
  %add84 = add nsw i32 %shl82, %conv83
  %add85 = add nsw i32 %add84, 1
  store i32 %add85, i32* %w, align 4
  %62 = load i32, i32* %w, align 4
  %conv86 = uitofp i32 %62 to float
  %63 = load i8, i8* %n, align 1
  %conv87 = zext i8 %63 to i32
  %add88 = add nsw i32 %conv87, 1
  %arrayidx89 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %add88
  %64 = load i8, i8* %i062, align 1
  %idxprom90 = zext i8 %64 to i32
  %arrayidx91 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx89, i32 0, i32 %idxprom90
  %65 = load float, float* %arrayidx91, align 4
  %mul92 = fmul float %conv86, %65
  %conv93 = fpext float %mul92 to double
  store double %conv93, double* %r0, align 8
  %66 = load i32, i32* %w, align 4
  %conv94 = uitofp i32 %66 to float
  %67 = load i8, i8* %n, align 1
  %conv95 = zext i8 %67 to i32
  %add96 = add nsw i32 %conv95, 1
  %arrayidx97 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %add96
  %68 = load i8, i8* %i166, align 1
  %idxprom98 = zext i8 %68 to i32
  %arrayidx99 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx97, i32 0, i32 %idxprom98
  %69 = load float, float* %arrayidx99, align 4
  %mul100 = fmul float %conv94, %69
  %conv101 = fpext float %mul100 to double
  store double %conv101, double* %r1, align 8
  br label %if.end103

if.else102:                                       ; preds = %for.body61
  store double 0.000000e+00, double* %r1, align 8
  store double 0.000000e+00, double* %r0, align 8
  br label %if.end103

if.end103:                                        ; preds = %if.else102, %if.then76
  %70 = load double, double* %r0, align 8
  %conv104 = fptrunc double %70 to float
  %71 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx105 = getelementptr inbounds [32 x float], [32 x float]* %71, i32 0
  %72 = load i32, i32* %i, align 4
  %arrayidx106 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx105, i32 0, i32 %72
  store float %conv104, float* %arrayidx106, align 4
  %73 = load double, double* %r1, align 8
  %conv107 = fptrunc double %73 to float
  %74 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx108 = getelementptr inbounds [32 x float], [32 x float]* %74, i32 1
  %75 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx108, i32 0, i32 %75
  store float %conv107, float* %arrayidx109, align 4
  br label %for.inc110

for.inc110:                                       ; preds = %if.end103
  %76 = load i32, i32* %i, align 4
  %inc111 = add nsw i32 %76, 1
  store i32 %inc111, i32* %i, align 4
  br label %for.cond58

for.end112:                                       ; preds = %for.cond58
  %77 = load i32, i32* %ds_limit, align 4
  store i32 %77, i32* %i, align 4
  br label %for.cond113

for.cond113:                                      ; preds = %for.inc121, %for.end112
  %78 = load i32, i32* %i, align 4
  %cmp114 = icmp slt i32 %78, 32
  br i1 %cmp114, label %for.body116, label %for.end123

for.body116:                                      ; preds = %for.cond113
  %79 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx117 = getelementptr inbounds [32 x float], [32 x float]* %79, i32 0
  %80 = load i32, i32* %i, align 4
  %arrayidx118 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx117, i32 0, i32 %80
  store float 0.000000e+00, float* %arrayidx118, align 4
  %81 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx119 = getelementptr inbounds [32 x float], [32 x float]* %81, i32 1
  %82 = load i32, i32* %i, align 4
  %arrayidx120 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx119, i32 0, i32 %82
  store float 0.000000e+00, float* %arrayidx120, align 4
  br label %for.inc121

for.inc121:                                       ; preds = %for.body116
  %83 = load i32, i32* %i, align 4
  %inc122 = add nsw i32 %83, 1
  store i32 %inc122, i32* %i, align 4
  br label %for.cond113

for.end123:                                       ; preds = %for.cond113
  br label %if.end174

if.else124:                                       ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc162, %if.else124
  %84 = load i32, i32* %i, align 4
  %cmp126 = icmp slt i32 %84, 32
  br i1 %cmp126, label %for.body128, label %for.end164

for.body128:                                      ; preds = %for.cond125
  %85 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %allocation130 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %85, i32 0, i32 0
  %86 = load i32, i32* %i, align 4
  %arrayidx131 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %allocation130, i32 0, i32 %86
  %arrayidx132 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx131, i32 0, i32 0
  %87 = load i8, i8* %arrayidx132, align 1
  store i8 %87, i8* %n129, align 1
  %88 = load %struct.sideinfo_layer_I_struct*, %struct.sideinfo_layer_I_struct** %si.addr, align 4
  %scalefactor133 = getelementptr inbounds %struct.sideinfo_layer_I_struct, %struct.sideinfo_layer_I_struct* %88, i32 0, i32 1
  %89 = load i32, i32* %i, align 4
  %arrayidx134 = getelementptr inbounds [32 x [2 x i8]], [32 x [2 x i8]]* %scalefactor133, i32 0, i32 %89
  %arrayidx135 = getelementptr inbounds [2 x i8], [2 x i8]* %arrayidx134, i32 0, i32 0
  %90 = load i8, i8* %arrayidx135, align 1
  store i8 %90, i8* %j, align 1
  %91 = load i8, i8* %n129, align 1
  %conv136 = zext i8 %91 to i32
  %cmp137 = icmp sgt i32 %conv136, 0
  br i1 %cmp137, label %if.then139, label %if.else157

if.then139:                                       ; preds = %for.body128
  %92 = load %struct.mpstr_tag*, %struct.mpstr_tag** %mp.addr, align 4
  %93 = load i8, i8* %n129, align 1
  %conv141 = zext i8 %93 to i32
  %add142 = add nsw i32 %conv141, 1
  %call143 = call zeroext i16 @get_leq_16_bits(%struct.mpstr_tag* %92, i32 %add142)
  store i16 %call143, i16* %v140, align 2
  %94 = load i8, i8* %n129, align 1
  %conv144 = zext i8 %94 to i32
  %shl145 = shl i32 -1, %conv144
  %95 = load i16, i16* %v140, align 2
  %conv146 = zext i16 %95 to i32
  %add147 = add nsw i32 %shl145, %conv146
  %add148 = add nsw i32 %add147, 1
  %conv149 = sitofp i32 %add148 to float
  %96 = load i8, i8* %n129, align 1
  %conv150 = zext i8 %96 to i32
  %add151 = add nsw i32 %conv150, 1
  %arrayidx152 = getelementptr inbounds [27 x [64 x float]], [27 x [64 x float]]* @muls, i32 0, i32 %add151
  %97 = load i8, i8* %j, align 1
  %idxprom153 = zext i8 %97 to i32
  %arrayidx154 = getelementptr inbounds [64 x float], [64 x float]* %arrayidx152, i32 0, i32 %idxprom153
  %98 = load float, float* %arrayidx154, align 4
  %mul155 = fmul float %conv149, %98
  %conv156 = fpext float %mul155 to double
  store double %conv156, double* %r0, align 8
  br label %if.end158

if.else157:                                       ; preds = %for.body128
  store double 0.000000e+00, double* %r0, align 8
  br label %if.end158

if.end158:                                        ; preds = %if.else157, %if.then139
  %99 = load double, double* %r0, align 8
  %conv159 = fptrunc double %99 to float
  %100 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx160 = getelementptr inbounds [32 x float], [32 x float]* %100, i32 0
  %101 = load i32, i32* %i, align 4
  %arrayidx161 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx160, i32 0, i32 %101
  store float %conv159, float* %arrayidx161, align 4
  br label %for.inc162

for.inc162:                                       ; preds = %if.end158
  %102 = load i32, i32* %i, align 4
  %inc163 = add nsw i32 %102, 1
  store i32 %inc163, i32* %i, align 4
  br label %for.cond125

for.end164:                                       ; preds = %for.cond125
  %103 = load i32, i32* %ds_limit, align 4
  store i32 %103, i32* %i, align 4
  br label %for.cond165

for.cond165:                                      ; preds = %for.inc171, %for.end164
  %104 = load i32, i32* %i, align 4
  %cmp166 = icmp slt i32 %104, 32
  br i1 %cmp166, label %for.body168, label %for.end173

for.body168:                                      ; preds = %for.cond165
  %105 = load [32 x float]*, [32 x float]** %fraction.addr, align 4
  %arrayidx169 = getelementptr inbounds [32 x float], [32 x float]* %105, i32 0
  %106 = load i32, i32* %i, align 4
  %arrayidx170 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx169, i32 0, i32 %106
  store float 0.000000e+00, float* %arrayidx170, align 4
  br label %for.inc171

for.inc171:                                       ; preds = %for.body168
  %107 = load i32, i32* %i, align 4
  %inc172 = add nsw i32 %107, 1
  store i32 %inc172, i32* %i, align 4
  br label %for.cond165

for.end173:                                       ; preds = %for.cond165
  br label %if.end174

if.end174:                                        ; preds = %for.end173, %for.end123
  ret void
}

declare i32 @synth_1to1_mono(%struct.mpstr_tag*, float*, i8*, i32*) #1

declare i32 @synth_1to1(%struct.mpstr_tag*, float*, i32, i8*, i32*) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

declare zeroext i8 @get_leq_8_bits(%struct.mpstr_tag*, i32) #1

declare zeroext i16 @get_leq_16_bits(%struct.mpstr_tag*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
