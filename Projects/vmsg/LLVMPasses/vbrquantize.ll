; ModuleID = 'vbrquantize.c'
source_filename = "vbrquantize.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.algo_s = type { void (%struct.algo_s*, i32*, i32*, i32)*, i8 (float*, float*, float, i32, i8)*, float*, %struct.lame_internal_flags*, %struct.gr_info*, i32, [3 x i32] }
%struct.calc_noise_cache = type { i32, float }
%struct.calc_noise_data_t = type { i32, i32, [39 x i32], [39 x float], [39 x float] }
%union.fi_union = type { float }

@__const.VBR_encode_frame.use_nbits_ch = private unnamed_addr constant [2 x [2 x i32]] [[2 x i32] [i32 4096, i32 4096], [2 x i32] [i32 4096, i32 4096]], align 16
@__const.VBR_encode_frame.use_nbits_gr = private unnamed_addr constant [2 x i32] [i32 7681, i32 7681], align 4
@.str = private unnamed_addr constant [86 x i8] c"INTERNAL ERROR IN VBR NEW CODE (1313), please send bug report\0Amaxbits=%d usedbits=%d\0A\00", align 1
@pow20 = external global [374 x float], align 16
@ipow20 = external global [257 x float], align 16
@pow43 = external global [8208 x float], align 16
@adj43asm = external global [8208 x float], align 16
@max_range_short = internal constant [39 x i8] c"\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\07\07\07\07\07\07\07\07\07\07\07\07\07\07\07\07\07\07\00\00\00", align 16
@pretab = external constant [22 x i32], align 16
@max_range_long = internal constant [22 x i8] c"\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\0F\07\07\07\07\07\07\07\07\07\07\00", align 16
@max_range_long_lsf_pretab = internal constant <{ [11 x i8], [11 x i8] }> <{ [11 x i8] c"\07\07\07\07\07\07\03\03\03\03\03", [11 x i8] zeroinitializer }>, align 16
@.str.2 = private unnamed_addr constant [62 x i8] c"INTERNAL ERROR IN VBR NEW CODE (986), please send bug report\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @VBR_encode_frame(%struct.lame_internal_flags* %gfc, [2 x [576 x float]]* %xr34orig, [2 x [39 x float]]* %l3_xmin, [2 x i32]* %max_bits) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %xr34orig.addr = alloca [2 x [576 x float]]*, align 4
  %l3_xmin.addr = alloca [2 x [39 x float]]*, align 4
  %max_bits.addr = alloca [2 x i32]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %sfwork_ = alloca [2 x [2 x [39 x i32]]], align 16
  %vbrsfmin_ = alloca [2 x [2 x [39 x i32]]], align 16
  %that_ = alloca [2 x [2 x %struct.algo_s]], align 16
  %ngr = alloca i32, align 4
  %nch = alloca i32, align 4
  %max_nbits_ch = alloca [2 x [2 x i32]], align 16
  %max_nbits_gr = alloca [2 x i32], align 4
  %max_nbits_fr = alloca i32, align 4
  %use_nbits_ch = alloca [2 x [2 x i32]], align 16
  %use_nbits_gr = alloca [2 x i32], align 4
  %use_nbits_fr = alloca i32, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %ok = alloca i32, align 4
  %sum_fr = alloca i32, align 4
  %that = alloca %struct.algo_s*, align 4
  %sfwork = alloca i32*, align 4
  %vbrsfmin = alloca i32*, align 4
  %vbrmax = alloca i32, align 4
  %that81 = alloca %struct.algo_s*, align 4
  %f = alloca [2 x float], align 4
  %s = alloca float, align 4
  %f290 = alloca [2 x float], align 4
  %s291 = alloca float, align 4
  %f386 = alloca [2 x float], align 4
  %s387 = alloca float, align 4
  %sum_gr = alloca i32, align 4
  %that607 = alloca %struct.algo_s*, align 4
  %sfwork617 = alloca i32*, align 4
  %vbrsfmin621 = alloca i32*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x [576 x float]]* %xr34orig, [2 x [576 x float]]** %xr34orig.addr, align 4
  store [2 x [39 x float]]* %l3_xmin, [2 x [39 x float]]** %l3_xmin.addr, align 4
  store [2 x i32]* %max_bits, [2 x i32]** %max_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %1, i32 0, i32 15
  %2 = load i32, i32* %mode_gr, align 4
  store i32 %2, i32* %ngr, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 14
  %4 = load i32, i32* %channels_out, align 4
  store i32 %4, i32* %nch, align 4
  %5 = bitcast [2 x [2 x i32]]* %max_nbits_ch to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %5, i8 0, i32 16, i1 false)
  %6 = bitcast [2 x i32]* %max_nbits_gr to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %6, i8 0, i32 8, i1 false)
  store i32 0, i32* %max_nbits_fr, align 4
  %7 = bitcast [2 x [2 x i32]]* %use_nbits_ch to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %7, i8* align 16 bitcast ([2 x [2 x i32]]* @__const.VBR_encode_frame.use_nbits_ch to i8*), i32 16, i1 false)
  %8 = bitcast [2 x i32]* %use_nbits_gr to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 bitcast ([2 x i32]* @__const.VBR_encode_frame.use_nbits_gr to i8*), i32 8, i1 false)
  store i32 15360, i32* %use_nbits_fr, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc41, %entry
  %9 = load i32, i32* %gr, align 4
  %10 = load i32, i32* %ngr, align 4
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.end43

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %11
  store i32 0, i32* %arrayidx, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %12 = load i32, i32* %ch, align 4
  %13 = load i32, i32* %nch, align 4
  %cmp3 = icmp slt i32 %12, %13
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %14 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %15 = load i32, i32* %gr, align 4
  %arrayidx5 = getelementptr inbounds [2 x i32], [2 x i32]* %14, i32 %15
  %16 = load i32, i32* %ch, align 4
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx5, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx6, align 4
  %18 = load i32, i32* %gr, align 4
  %arrayidx7 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %18
  %19 = load i32, i32* %ch, align 4
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx7, i32 0, i32 %19
  store i32 %17, i32* %arrayidx8, align 4
  %20 = load i32, i32* %gr, align 4
  %arrayidx9 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %20
  %21 = load i32, i32* %ch, align 4
  %arrayidx10 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx9, i32 0, i32 %21
  store i32 0, i32* %arrayidx10, align 4
  %22 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %23 = load i32, i32* %gr, align 4
  %arrayidx11 = getelementptr inbounds [2 x i32], [2 x i32]* %22, i32 %23
  %24 = load i32, i32* %ch, align 4
  %arrayidx12 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx11, i32 0, i32 %24
  %25 = load i32, i32* %arrayidx12, align 4
  %26 = load i32, i32* %gr, align 4
  %arrayidx13 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx13, align 4
  %add = add nsw i32 %27, %25
  store i32 %add, i32* %arrayidx13, align 4
  %28 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %29 = load i32, i32* %gr, align 4
  %arrayidx14 = getelementptr inbounds [2 x i32], [2 x i32]* %28, i32 %29
  %30 = load i32, i32* %ch, align 4
  %arrayidx15 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx14, i32 0, i32 %30
  %31 = load i32, i32* %arrayidx15, align 4
  %32 = load i32, i32* %max_nbits_fr, align 4
  %add16 = add nsw i32 %32, %31
  store i32 %add16, i32* %max_nbits_fr, align 4
  %33 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %33, i32 0, i32 8
  %34 = load i32, i32* %full_outer_loop, align 4
  %cmp17 = icmp slt i32 %34, 0
  %35 = zext i1 %cmp17 to i64
  %cond = select i1 %cmp17, i8 (float*, float*, float, i32, i8)* @guess_scalefac_x34, i8 (float*, float*, float, i32, i8)* @find_scalefac_x34
  %36 = load i32, i32* %gr, align 4
  %arrayidx18 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %36
  %37 = load i32, i32* %ch, align 4
  %arrayidx19 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx18, i32 0, i32 %37
  %find = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx19, i32 0, i32 1
  store i8 (float*, float*, float, i32, i8)* %cond, i8 (float*, float*, float, i32, i8)** %find, align 4
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %39 = load i32, i32* %gr, align 4
  %arrayidx20 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %39
  %40 = load i32, i32* %ch, align 4
  %arrayidx21 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx20, i32 0, i32 %40
  %gfc22 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx21, i32 0, i32 3
  store %struct.lame_internal_flags* %38, %struct.lame_internal_flags** %gfc22, align 4
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %42 = load i32, i32* %gr, align 4
  %arrayidx23 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %42
  %43 = load i32, i32* %ch, align 4
  %arrayidx24 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx23, i32 0, i32 %43
  %44 = load i32, i32* %gr, align 4
  %arrayidx25 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %44
  %45 = load i32, i32* %ch, align 4
  %arrayidx26 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx25, i32 0, i32 %45
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx26, i32 0, i32 4
  store %struct.gr_info* %arrayidx24, %struct.gr_info** %cod_info, align 4
  %46 = load [2 x [576 x float]]*, [2 x [576 x float]]** %xr34orig.addr, align 4
  %47 = load i32, i32* %gr, align 4
  %arrayidx27 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %46, i32 %47
  %48 = load i32, i32* %ch, align 4
  %arrayidx28 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %arrayidx27, i32 0, i32 %48
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %arrayidx28, i32 0, i32 0
  %49 = load i32, i32* %gr, align 4
  %arrayidx29 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %49
  %50 = load i32, i32* %ch, align 4
  %arrayidx30 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx29, i32 0, i32 %50
  %xr34orig31 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx30, i32 0, i32 2
  store float* %arraydecay, float** %xr34orig31, align 4
  %51 = load i32, i32* %gr, align 4
  %arrayidx32 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %51
  %52 = load i32, i32* %ch, align 4
  %arrayidx33 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx32, i32 0, i32 %52
  %cod_info34 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx33, i32 0, i32 4
  %53 = load %struct.gr_info*, %struct.gr_info** %cod_info34, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %53, i32 0, i32 9
  %54 = load i32, i32* %block_type, align 4
  %cmp35 = icmp eq i32 %54, 2
  br i1 %cmp35, label %if.then, label %if.else

if.then:                                          ; preds = %for.body4
  %55 = load i32, i32* %gr, align 4
  %arrayidx36 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %55
  %56 = load i32, i32* %ch, align 4
  %arrayidx37 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx36, i32 0, i32 %56
  %alloc = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx37, i32 0, i32 0
  store void (%struct.algo_s*, i32*, i32*, i32)* @short_block_constrain, void (%struct.algo_s*, i32*, i32*, i32)** %alloc, align 4
  br label %if.end

if.else:                                          ; preds = %for.body4
  %57 = load i32, i32* %gr, align 4
  %arrayidx38 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %57
  %58 = load i32, i32* %ch, align 4
  %arrayidx39 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx38, i32 0, i32 %58
  %alloc40 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %arrayidx39, i32 0, i32 0
  store void (%struct.algo_s*, i32*, i32*, i32)* @long_block_constrain, void (%struct.algo_s*, i32*, i32*, i32)** %alloc40, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %59 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %59, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc41

for.inc41:                                        ; preds = %for.end
  %60 = load i32, i32* %gr, align 4
  %inc42 = add nsw i32 %60, 1
  store i32 %inc42, i32* %gr, align 4
  br label %for.cond

for.end43:                                        ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc71, %for.end43
  %61 = load i32, i32* %gr, align 4
  %62 = load i32, i32* %ngr, align 4
  %cmp45 = icmp slt i32 %61, %62
  br i1 %cmp45, label %for.body46, label %for.end73

for.body46:                                       ; preds = %for.cond44
  store i32 0, i32* %ch, align 4
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc68, %for.body46
  %63 = load i32, i32* %ch, align 4
  %64 = load i32, i32* %nch, align 4
  %cmp48 = icmp slt i32 %63, %64
  br i1 %cmp48, label %for.body49, label %for.end70

for.body49:                                       ; preds = %for.cond47
  %65 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %66 = load i32, i32* %gr, align 4
  %arrayidx50 = getelementptr inbounds [2 x i32], [2 x i32]* %65, i32 %66
  %67 = load i32, i32* %ch, align 4
  %arrayidx51 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx50, i32 0, i32 %67
  %68 = load i32, i32* %arrayidx51, align 4
  %cmp52 = icmp sgt i32 %68, 0
  br i1 %cmp52, label %if.then53, label %if.else66

if.then53:                                        ; preds = %for.body49
  %69 = load i32, i32* %gr, align 4
  %arrayidx54 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %69
  %70 = load i32, i32* %ch, align 4
  %arrayidx55 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx54, i32 0, i32 %70
  store %struct.algo_s* %arrayidx55, %struct.algo_s** %that, align 4
  %71 = load i32, i32* %gr, align 4
  %arrayidx56 = getelementptr inbounds [2 x [2 x [39 x i32]]], [2 x [2 x [39 x i32]]]* %sfwork_, i32 0, i32 %71
  %72 = load i32, i32* %ch, align 4
  %arrayidx57 = getelementptr inbounds [2 x [39 x i32]], [2 x [39 x i32]]* %arrayidx56, i32 0, i32 %72
  %arraydecay58 = getelementptr inbounds [39 x i32], [39 x i32]* %arrayidx57, i32 0, i32 0
  store i32* %arraydecay58, i32** %sfwork, align 4
  %73 = load i32, i32* %gr, align 4
  %arrayidx59 = getelementptr inbounds [2 x [2 x [39 x i32]]], [2 x [2 x [39 x i32]]]* %vbrsfmin_, i32 0, i32 %73
  %74 = load i32, i32* %ch, align 4
  %arrayidx60 = getelementptr inbounds [2 x [39 x i32]], [2 x [39 x i32]]* %arrayidx59, i32 0, i32 %74
  %arraydecay61 = getelementptr inbounds [39 x i32], [39 x i32]* %arrayidx60, i32 0, i32 0
  store i32* %arraydecay61, i32** %vbrsfmin, align 4
  %75 = load %struct.algo_s*, %struct.algo_s** %that, align 4
  %76 = load [2 x [39 x float]]*, [2 x [39 x float]]** %l3_xmin.addr, align 4
  %77 = load i32, i32* %gr, align 4
  %arrayidx62 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %76, i32 %77
  %78 = load i32, i32* %ch, align 4
  %arrayidx63 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx62, i32 0, i32 %78
  %arraydecay64 = getelementptr inbounds [39 x float], [39 x float]* %arrayidx63, i32 0, i32 0
  %79 = load i32*, i32** %sfwork, align 4
  %80 = load i32*, i32** %vbrsfmin, align 4
  %call = call i32 @block_sf(%struct.algo_s* %75, float* %arraydecay64, i32* %79, i32* %80)
  store i32 %call, i32* %vbrmax, align 4
  %81 = load %struct.algo_s*, %struct.algo_s** %that, align 4
  %alloc65 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %81, i32 0, i32 0
  %82 = load void (%struct.algo_s*, i32*, i32*, i32)*, void (%struct.algo_s*, i32*, i32*, i32)** %alloc65, align 4
  %83 = load %struct.algo_s*, %struct.algo_s** %that, align 4
  %84 = load i32*, i32** %sfwork, align 4
  %85 = load i32*, i32** %vbrsfmin, align 4
  %86 = load i32, i32* %vbrmax, align 4
  call void %82(%struct.algo_s* %83, i32* %84, i32* %85, i32 %86)
  %87 = load %struct.algo_s*, %struct.algo_s** %that, align 4
  call void @bitcount(%struct.algo_s* %87)
  br label %if.end67

if.else66:                                        ; preds = %for.body49
  br label %if.end67

if.end67:                                         ; preds = %if.else66, %if.then53
  br label %for.inc68

for.inc68:                                        ; preds = %if.end67
  %88 = load i32, i32* %ch, align 4
  %inc69 = add nsw i32 %88, 1
  store i32 %inc69, i32* %ch, align 4
  br label %for.cond47

for.end70:                                        ; preds = %for.cond47
  br label %for.inc71

for.inc71:                                        ; preds = %for.end70
  %89 = load i32, i32* %gr, align 4
  %inc72 = add nsw i32 %89, 1
  store i32 %inc72, i32* %gr, align 4
  br label %for.cond44

for.end73:                                        ; preds = %for.cond44
  store i32 0, i32* %use_nbits_fr, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc105, %for.end73
  %90 = load i32, i32* %gr, align 4
  %91 = load i32, i32* %ngr, align 4
  %cmp75 = icmp slt i32 %90, %91
  br i1 %cmp75, label %for.body76, label %for.end107

for.body76:                                       ; preds = %for.cond74
  %92 = load i32, i32* %gr, align 4
  %arrayidx77 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %92
  store i32 0, i32* %arrayidx77, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond78

for.cond78:                                       ; preds = %for.inc100, %for.body76
  %93 = load i32, i32* %ch, align 4
  %94 = load i32, i32* %nch, align 4
  %cmp79 = icmp slt i32 %93, %94
  br i1 %cmp79, label %for.body80, label %for.end102

for.body80:                                       ; preds = %for.cond78
  %95 = load i32, i32* %gr, align 4
  %arrayidx82 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %95
  %96 = load i32, i32* %ch, align 4
  %arrayidx83 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx82, i32 0, i32 %96
  store %struct.algo_s* %arrayidx83, %struct.algo_s** %that81, align 4
  %97 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %98 = load i32, i32* %gr, align 4
  %arrayidx84 = getelementptr inbounds [2 x i32], [2 x i32]* %97, i32 %98
  %99 = load i32, i32* %ch, align 4
  %arrayidx85 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx84, i32 0, i32 %99
  %100 = load i32, i32* %arrayidx85, align 4
  %cmp86 = icmp sgt i32 %100, 0
  br i1 %cmp86, label %if.then87, label %if.else91

if.then87:                                        ; preds = %for.body80
  %101 = load %struct.algo_s*, %struct.algo_s** %that81, align 4
  %cod_info88 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %101, i32 0, i32 4
  %102 = load %struct.gr_info*, %struct.gr_info** %cod_info88, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %102, i32 0, i32 1
  %arrayidx89 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  %103 = bitcast i32* %arrayidx89 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %103, i8 0, i32 2304, i1 false)
  %104 = load %struct.algo_s*, %struct.algo_s** %that81, align 4
  %call90 = call i32 @quantizeAndCountBits(%struct.algo_s* %104)
  br label %if.end92

if.else91:                                        ; preds = %for.body80
  br label %if.end92

if.end92:                                         ; preds = %if.else91, %if.then87
  %105 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %106 = load i32, i32* %gr, align 4
  %107 = load i32, i32* %ch, align 4
  %call93 = call i32 @reduce_bit_usage(%struct.lame_internal_flags* %105, i32 %106, i32 %107)
  %108 = load i32, i32* %gr, align 4
  %arrayidx94 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %108
  %109 = load i32, i32* %ch, align 4
  %arrayidx95 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx94, i32 0, i32 %109
  store i32 %call93, i32* %arrayidx95, align 4
  %110 = load i32, i32* %gr, align 4
  %arrayidx96 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %110
  %111 = load i32, i32* %ch, align 4
  %arrayidx97 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx96, i32 0, i32 %111
  %112 = load i32, i32* %arrayidx97, align 4
  %113 = load i32, i32* %gr, align 4
  %arrayidx98 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %113
  %114 = load i32, i32* %arrayidx98, align 4
  %add99 = add nsw i32 %114, %112
  store i32 %add99, i32* %arrayidx98, align 4
  br label %for.inc100

for.inc100:                                       ; preds = %if.end92
  %115 = load i32, i32* %ch, align 4
  %inc101 = add nsw i32 %115, 1
  store i32 %inc101, i32* %ch, align 4
  br label %for.cond78

for.end102:                                       ; preds = %for.cond78
  %116 = load i32, i32* %gr, align 4
  %arrayidx103 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %116
  %117 = load i32, i32* %arrayidx103, align 4
  %118 = load i32, i32* %use_nbits_fr, align 4
  %add104 = add nsw i32 %118, %117
  store i32 %add104, i32* %use_nbits_fr, align 4
  br label %for.inc105

for.inc105:                                       ; preds = %for.end102
  %119 = load i32, i32* %gr, align 4
  %inc106 = add nsw i32 %119, 1
  store i32 %inc106, i32* %gr, align 4
  br label %for.cond74

for.end107:                                       ; preds = %for.cond74
  %120 = load i32, i32* %use_nbits_fr, align 4
  %121 = load i32, i32* %max_nbits_fr, align 4
  %cmp108 = icmp sle i32 %120, %121
  br i1 %cmp108, label %if.then109, label %if.end133

if.then109:                                       ; preds = %for.end107
  store i32 1, i32* %ok, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond110

for.cond110:                                      ; preds = %for.inc128, %if.then109
  %122 = load i32, i32* %gr, align 4
  %123 = load i32, i32* %ngr, align 4
  %cmp111 = icmp slt i32 %122, %123
  br i1 %cmp111, label %for.body112, label %for.end130

for.body112:                                      ; preds = %for.cond110
  %124 = load i32, i32* %gr, align 4
  %arrayidx113 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %124
  %125 = load i32, i32* %arrayidx113, align 4
  %cmp114 = icmp sgt i32 %125, 7680
  br i1 %cmp114, label %if.then115, label %if.end116

if.then115:                                       ; preds = %for.body112
  store i32 0, i32* %ok, align 4
  br label %if.end116

if.end116:                                        ; preds = %if.then115, %for.body112
  store i32 0, i32* %ch, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc125, %if.end116
  %126 = load i32, i32* %ch, align 4
  %127 = load i32, i32* %nch, align 4
  %cmp118 = icmp slt i32 %126, %127
  br i1 %cmp118, label %for.body119, label %for.end127

for.body119:                                      ; preds = %for.cond117
  %128 = load i32, i32* %gr, align 4
  %arrayidx120 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %128
  %129 = load i32, i32* %ch, align 4
  %arrayidx121 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx120, i32 0, i32 %129
  %130 = load i32, i32* %arrayidx121, align 4
  %cmp122 = icmp sgt i32 %130, 4095
  br i1 %cmp122, label %if.then123, label %if.end124

if.then123:                                       ; preds = %for.body119
  store i32 0, i32* %ok, align 4
  br label %if.end124

if.end124:                                        ; preds = %if.then123, %for.body119
  br label %for.inc125

for.inc125:                                       ; preds = %if.end124
  %131 = load i32, i32* %ch, align 4
  %inc126 = add nsw i32 %131, 1
  store i32 %inc126, i32* %ch, align 4
  br label %for.cond117

for.end127:                                       ; preds = %for.cond117
  br label %for.inc128

for.inc128:                                       ; preds = %for.end127
  %132 = load i32, i32* %gr, align 4
  %inc129 = add nsw i32 %132, 1
  store i32 %inc129, i32* %gr, align 4
  br label %for.cond110

for.end130:                                       ; preds = %for.cond110
  %133 = load i32, i32* %ok, align 4
  %tobool = icmp ne i32 %133, 0
  br i1 %tobool, label %if.then131, label %if.end132

if.then131:                                       ; preds = %for.end130
  %134 = load i32, i32* %use_nbits_fr, align 4
  store i32 %134, i32* %retval, align 4
  br label %return

if.end132:                                        ; preds = %for.end130
  br label %if.end133

if.end133:                                        ; preds = %if.end132, %for.end107
  store i32 1, i32* %ok, align 4
  store i32 0, i32* %sum_fr, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond134

for.cond134:                                      ; preds = %for.inc284, %if.end133
  %135 = load i32, i32* %gr, align 4
  %136 = load i32, i32* %ngr, align 4
  %cmp135 = icmp slt i32 %135, %136
  br i1 %cmp135, label %for.body136, label %for.end286

for.body136:                                      ; preds = %for.cond134
  %137 = load i32, i32* %gr, align 4
  %arrayidx137 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %137
  store i32 0, i32* %arrayidx137, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond138

for.cond138:                                      ; preds = %for.inc157, %for.body136
  %138 = load i32, i32* %ch, align 4
  %139 = load i32, i32* %nch, align 4
  %cmp139 = icmp slt i32 %138, %139
  br i1 %cmp139, label %for.body140, label %for.end159

for.body140:                                      ; preds = %for.cond138
  %140 = load i32, i32* %gr, align 4
  %arrayidx141 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %140
  %141 = load i32, i32* %ch, align 4
  %arrayidx142 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx141, i32 0, i32 %141
  %142 = load i32, i32* %arrayidx142, align 4
  %cmp143 = icmp sgt i32 %142, 4095
  br i1 %cmp143, label %if.then144, label %if.else147

if.then144:                                       ; preds = %for.body140
  %143 = load i32, i32* %gr, align 4
  %arrayidx145 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %143
  %144 = load i32, i32* %ch, align 4
  %arrayidx146 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx145, i32 0, i32 %144
  store i32 4095, i32* %arrayidx146, align 4
  br label %if.end152

if.else147:                                       ; preds = %for.body140
  %145 = load i32, i32* %gr, align 4
  %arrayidx148 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %145
  %146 = load i32, i32* %ch, align 4
  %arrayidx149 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx148, i32 0, i32 %146
  %147 = load i32, i32* %arrayidx149, align 4
  %148 = load i32, i32* %gr, align 4
  %arrayidx150 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %148
  %149 = load i32, i32* %ch, align 4
  %arrayidx151 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx150, i32 0, i32 %149
  store i32 %147, i32* %arrayidx151, align 4
  br label %if.end152

if.end152:                                        ; preds = %if.else147, %if.then144
  %150 = load i32, i32* %gr, align 4
  %arrayidx153 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %150
  %151 = load i32, i32* %ch, align 4
  %arrayidx154 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx153, i32 0, i32 %151
  %152 = load i32, i32* %arrayidx154, align 4
  %153 = load i32, i32* %gr, align 4
  %arrayidx155 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %153
  %154 = load i32, i32* %arrayidx155, align 4
  %add156 = add nsw i32 %154, %152
  store i32 %add156, i32* %arrayidx155, align 4
  br label %for.inc157

for.inc157:                                       ; preds = %if.end152
  %155 = load i32, i32* %ch, align 4
  %inc158 = add nsw i32 %155, 1
  store i32 %inc158, i32* %ch, align 4
  br label %for.cond138

for.end159:                                       ; preds = %for.cond138
  %156 = load i32, i32* %gr, align 4
  %arrayidx160 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %156
  %157 = load i32, i32* %arrayidx160, align 4
  %cmp161 = icmp sgt i32 %157, 7680
  br i1 %cmp161, label %if.then162, label %if.end281

if.then162:                                       ; preds = %for.end159
  %158 = bitcast [2 x float]* %f to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %158, i8 0, i32 8, i1 false)
  store float 0.000000e+00, float* %s, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc179, %if.then162
  %159 = load i32, i32* %ch, align 4
  %160 = load i32, i32* %nch, align 4
  %cmp164 = icmp slt i32 %159, %160
  br i1 %cmp164, label %for.body165, label %for.end181

for.body165:                                      ; preds = %for.cond163
  %161 = load i32, i32* %gr, align 4
  %arrayidx166 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %161
  %162 = load i32, i32* %ch, align 4
  %arrayidx167 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx166, i32 0, i32 %162
  %163 = load i32, i32* %arrayidx167, align 4
  %cmp168 = icmp sgt i32 %163, 0
  br i1 %cmp168, label %if.then169, label %if.else176

if.then169:                                       ; preds = %for.body165
  %164 = load i32, i32* %gr, align 4
  %arrayidx170 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %164
  %165 = load i32, i32* %ch, align 4
  %arrayidx171 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx170, i32 0, i32 %165
  %166 = load i32, i32* %arrayidx171, align 4
  %conv = sitofp i32 %166 to double
  %167 = call double @llvm.sqrt.f64(double %conv)
  %168 = call double @llvm.sqrt.f64(double %167)
  %conv172 = fptrunc double %168 to float
  %169 = load i32, i32* %ch, align 4
  %arrayidx173 = getelementptr inbounds [2 x float], [2 x float]* %f, i32 0, i32 %169
  store float %conv172, float* %arrayidx173, align 4
  %170 = load i32, i32* %ch, align 4
  %arrayidx174 = getelementptr inbounds [2 x float], [2 x float]* %f, i32 0, i32 %170
  %171 = load float, float* %arrayidx174, align 4
  %172 = load float, float* %s, align 4
  %add175 = fadd float %172, %171
  store float %add175, float* %s, align 4
  br label %if.end178

if.else176:                                       ; preds = %for.body165
  %173 = load i32, i32* %ch, align 4
  %arrayidx177 = getelementptr inbounds [2 x float], [2 x float]* %f, i32 0, i32 %173
  store float 0.000000e+00, float* %arrayidx177, align 4
  br label %if.end178

if.end178:                                        ; preds = %if.else176, %if.then169
  br label %for.inc179

for.inc179:                                       ; preds = %if.end178
  %174 = load i32, i32* %ch, align 4
  %inc180 = add nsw i32 %174, 1
  store i32 %inc180, i32* %ch, align 4
  br label %for.cond163

for.end181:                                       ; preds = %for.cond163
  store i32 0, i32* %ch, align 4
  br label %for.cond182

for.cond182:                                      ; preds = %for.inc197, %for.end181
  %175 = load i32, i32* %ch, align 4
  %176 = load i32, i32* %nch, align 4
  %cmp183 = icmp slt i32 %175, %176
  br i1 %cmp183, label %for.body185, label %for.end199

for.body185:                                      ; preds = %for.cond182
  %177 = load float, float* %s, align 4
  %cmp186 = fcmp ogt float %177, 0.000000e+00
  br i1 %cmp186, label %if.then188, label %if.else193

if.then188:                                       ; preds = %for.body185
  %178 = load i32, i32* %ch, align 4
  %arrayidx189 = getelementptr inbounds [2 x float], [2 x float]* %f, i32 0, i32 %178
  %179 = load float, float* %arrayidx189, align 4
  %mul = fmul float 7.680000e+03, %179
  %180 = load float, float* %s, align 4
  %div = fdiv float %mul, %180
  %conv190 = fptosi float %div to i32
  %181 = load i32, i32* %gr, align 4
  %arrayidx191 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %181
  %182 = load i32, i32* %ch, align 4
  %arrayidx192 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx191, i32 0, i32 %182
  store i32 %conv190, i32* %arrayidx192, align 4
  br label %if.end196

if.else193:                                       ; preds = %for.body185
  %183 = load i32, i32* %gr, align 4
  %arrayidx194 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %183
  %184 = load i32, i32* %ch, align 4
  %arrayidx195 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx194, i32 0, i32 %184
  store i32 0, i32* %arrayidx195, align 4
  br label %if.end196

if.end196:                                        ; preds = %if.else193, %if.then188
  br label %for.inc197

for.inc197:                                       ; preds = %if.end196
  %185 = load i32, i32* %ch, align 4
  %inc198 = add nsw i32 %185, 1
  store i32 %inc198, i32* %ch, align 4
  br label %for.cond182

for.end199:                                       ; preds = %for.cond182
  %186 = load i32, i32* %nch, align 4
  %cmp200 = icmp sgt i32 %186, 1
  br i1 %cmp200, label %if.then202, label %if.end268

if.then202:                                       ; preds = %for.end199
  %187 = load i32, i32* %gr, align 4
  %arrayidx203 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %187
  %arrayidx204 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx203, i32 0, i32 0
  %188 = load i32, i32* %arrayidx204, align 8
  %189 = load i32, i32* %gr, align 4
  %arrayidx205 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %189
  %arrayidx206 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx205, i32 0, i32 0
  %190 = load i32, i32* %arrayidx206, align 8
  %add207 = add nsw i32 %190, 32
  %cmp208 = icmp sgt i32 %188, %add207
  br i1 %cmp208, label %if.then210, label %if.end226

if.then210:                                       ; preds = %if.then202
  %191 = load i32, i32* %gr, align 4
  %arrayidx211 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %191
  %arrayidx212 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx211, i32 0, i32 0
  %192 = load i32, i32* %arrayidx212, align 8
  %193 = load i32, i32* %gr, align 4
  %arrayidx213 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %193
  %arrayidx214 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx213, i32 0, i32 1
  %194 = load i32, i32* %arrayidx214, align 4
  %add215 = add nsw i32 %194, %192
  store i32 %add215, i32* %arrayidx214, align 4
  %195 = load i32, i32* %gr, align 4
  %arrayidx216 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %195
  %arrayidx217 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx216, i32 0, i32 0
  %196 = load i32, i32* %arrayidx217, align 8
  %add218 = add nsw i32 %196, 32
  %197 = load i32, i32* %gr, align 4
  %arrayidx219 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %197
  %arrayidx220 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx219, i32 0, i32 1
  %198 = load i32, i32* %arrayidx220, align 4
  %sub = sub nsw i32 %198, %add218
  store i32 %sub, i32* %arrayidx220, align 4
  %199 = load i32, i32* %gr, align 4
  %arrayidx221 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %199
  %arrayidx222 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx221, i32 0, i32 0
  %200 = load i32, i32* %arrayidx222, align 8
  %add223 = add nsw i32 %200, 32
  %201 = load i32, i32* %gr, align 4
  %arrayidx224 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %201
  %arrayidx225 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx224, i32 0, i32 0
  store i32 %add223, i32* %arrayidx225, align 8
  br label %if.end226

if.end226:                                        ; preds = %if.then210, %if.then202
  %202 = load i32, i32* %gr, align 4
  %arrayidx227 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %202
  %arrayidx228 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx227, i32 0, i32 1
  %203 = load i32, i32* %arrayidx228, align 4
  %204 = load i32, i32* %gr, align 4
  %arrayidx229 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %204
  %arrayidx230 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx229, i32 0, i32 1
  %205 = load i32, i32* %arrayidx230, align 4
  %add231 = add nsw i32 %205, 32
  %cmp232 = icmp sgt i32 %203, %add231
  br i1 %cmp232, label %if.then234, label %if.end251

if.then234:                                       ; preds = %if.end226
  %206 = load i32, i32* %gr, align 4
  %arrayidx235 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %206
  %arrayidx236 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx235, i32 0, i32 1
  %207 = load i32, i32* %arrayidx236, align 4
  %208 = load i32, i32* %gr, align 4
  %arrayidx237 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %208
  %arrayidx238 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx237, i32 0, i32 0
  %209 = load i32, i32* %arrayidx238, align 8
  %add239 = add nsw i32 %209, %207
  store i32 %add239, i32* %arrayidx238, align 8
  %210 = load i32, i32* %gr, align 4
  %arrayidx240 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %210
  %arrayidx241 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx240, i32 0, i32 1
  %211 = load i32, i32* %arrayidx241, align 4
  %add242 = add nsw i32 %211, 32
  %212 = load i32, i32* %gr, align 4
  %arrayidx243 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %212
  %arrayidx244 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx243, i32 0, i32 0
  %213 = load i32, i32* %arrayidx244, align 8
  %sub245 = sub nsw i32 %213, %add242
  store i32 %sub245, i32* %arrayidx244, align 8
  %214 = load i32, i32* %gr, align 4
  %arrayidx246 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %214
  %arrayidx247 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx246, i32 0, i32 1
  %215 = load i32, i32* %arrayidx247, align 4
  %add248 = add nsw i32 %215, 32
  %216 = load i32, i32* %gr, align 4
  %arrayidx249 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %216
  %arrayidx250 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx249, i32 0, i32 1
  store i32 %add248, i32* %arrayidx250, align 4
  br label %if.end251

if.end251:                                        ; preds = %if.then234, %if.end226
  %217 = load i32, i32* %gr, align 4
  %arrayidx252 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %217
  %arrayidx253 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx252, i32 0, i32 0
  %218 = load i32, i32* %arrayidx253, align 8
  %cmp254 = icmp sgt i32 %218, 4095
  br i1 %cmp254, label %if.then256, label %if.end259

if.then256:                                       ; preds = %if.end251
  %219 = load i32, i32* %gr, align 4
  %arrayidx257 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %219
  %arrayidx258 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx257, i32 0, i32 0
  store i32 4095, i32* %arrayidx258, align 8
  br label %if.end259

if.end259:                                        ; preds = %if.then256, %if.end251
  %220 = load i32, i32* %gr, align 4
  %arrayidx260 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %220
  %arrayidx261 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx260, i32 0, i32 1
  %221 = load i32, i32* %arrayidx261, align 4
  %cmp262 = icmp sgt i32 %221, 4095
  br i1 %cmp262, label %if.then264, label %if.end267

if.then264:                                       ; preds = %if.end259
  %222 = load i32, i32* %gr, align 4
  %arrayidx265 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %222
  %arrayidx266 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx265, i32 0, i32 1
  store i32 4095, i32* %arrayidx266, align 4
  br label %if.end267

if.end267:                                        ; preds = %if.then264, %if.end259
  br label %if.end268

if.end268:                                        ; preds = %if.end267, %for.end199
  %223 = load i32, i32* %gr, align 4
  %arrayidx269 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %223
  store i32 0, i32* %arrayidx269, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond270

for.cond270:                                      ; preds = %for.inc278, %if.end268
  %224 = load i32, i32* %ch, align 4
  %225 = load i32, i32* %nch, align 4
  %cmp271 = icmp slt i32 %224, %225
  br i1 %cmp271, label %for.body273, label %for.end280

for.body273:                                      ; preds = %for.cond270
  %226 = load i32, i32* %gr, align 4
  %arrayidx274 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %226
  %227 = load i32, i32* %ch, align 4
  %arrayidx275 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx274, i32 0, i32 %227
  %228 = load i32, i32* %arrayidx275, align 4
  %229 = load i32, i32* %gr, align 4
  %arrayidx276 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %229
  %230 = load i32, i32* %arrayidx276, align 4
  %add277 = add nsw i32 %230, %228
  store i32 %add277, i32* %arrayidx276, align 4
  br label %for.inc278

for.inc278:                                       ; preds = %for.body273
  %231 = load i32, i32* %ch, align 4
  %inc279 = add nsw i32 %231, 1
  store i32 %inc279, i32* %ch, align 4
  br label %for.cond270

for.end280:                                       ; preds = %for.cond270
  br label %if.end281

if.end281:                                        ; preds = %for.end280, %for.end159
  %232 = load i32, i32* %gr, align 4
  %arrayidx282 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %232
  %233 = load i32, i32* %arrayidx282, align 4
  %234 = load i32, i32* %sum_fr, align 4
  %add283 = add nsw i32 %234, %233
  store i32 %add283, i32* %sum_fr, align 4
  br label %for.inc284

for.inc284:                                       ; preds = %if.end281
  %235 = load i32, i32* %gr, align 4
  %inc285 = add nsw i32 %235, 1
  store i32 %inc285, i32* %gr, align 4
  br label %for.cond134

for.end286:                                       ; preds = %for.cond134
  %236 = load i32, i32* %sum_fr, align 4
  %237 = load i32, i32* %max_nbits_fr, align 4
  %cmp287 = icmp sgt i32 %236, %237
  br i1 %cmp287, label %if.then289, label %if.end504

if.then289:                                       ; preds = %for.end286
  %238 = bitcast [2 x float]* %f290 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %238, i8 0, i32 8, i1 false)
  store float 0.000000e+00, float* %s291, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond292

for.cond292:                                      ; preds = %for.inc309, %if.then289
  %239 = load i32, i32* %gr, align 4
  %240 = load i32, i32* %ngr, align 4
  %cmp293 = icmp slt i32 %239, %240
  br i1 %cmp293, label %for.body295, label %for.end311

for.body295:                                      ; preds = %for.cond292
  %241 = load i32, i32* %gr, align 4
  %arrayidx296 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %241
  %242 = load i32, i32* %arrayidx296, align 4
  %cmp297 = icmp sgt i32 %242, 0
  br i1 %cmp297, label %if.then299, label %if.else306

if.then299:                                       ; preds = %for.body295
  %243 = load i32, i32* %gr, align 4
  %arrayidx300 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %243
  %244 = load i32, i32* %arrayidx300, align 4
  %conv301 = sitofp i32 %244 to double
  %245 = call double @llvm.sqrt.f64(double %conv301)
  %conv302 = fptrunc double %245 to float
  %246 = load i32, i32* %gr, align 4
  %arrayidx303 = getelementptr inbounds [2 x float], [2 x float]* %f290, i32 0, i32 %246
  store float %conv302, float* %arrayidx303, align 4
  %247 = load i32, i32* %gr, align 4
  %arrayidx304 = getelementptr inbounds [2 x float], [2 x float]* %f290, i32 0, i32 %247
  %248 = load float, float* %arrayidx304, align 4
  %249 = load float, float* %s291, align 4
  %add305 = fadd float %249, %248
  store float %add305, float* %s291, align 4
  br label %if.end308

if.else306:                                       ; preds = %for.body295
  %250 = load i32, i32* %gr, align 4
  %arrayidx307 = getelementptr inbounds [2 x float], [2 x float]* %f290, i32 0, i32 %250
  store float 0.000000e+00, float* %arrayidx307, align 4
  br label %if.end308

if.end308:                                        ; preds = %if.else306, %if.then299
  br label %for.inc309

for.inc309:                                       ; preds = %if.end308
  %251 = load i32, i32* %gr, align 4
  %inc310 = add nsw i32 %251, 1
  store i32 %inc310, i32* %gr, align 4
  br label %for.cond292

for.end311:                                       ; preds = %for.cond292
  store i32 0, i32* %gr, align 4
  br label %for.cond312

for.cond312:                                      ; preds = %for.inc328, %for.end311
  %252 = load i32, i32* %gr, align 4
  %253 = load i32, i32* %ngr, align 4
  %cmp313 = icmp slt i32 %252, %253
  br i1 %cmp313, label %for.body315, label %for.end330

for.body315:                                      ; preds = %for.cond312
  %254 = load float, float* %s291, align 4
  %cmp316 = fcmp ogt float %254, 0.000000e+00
  br i1 %cmp316, label %if.then318, label %if.else325

if.then318:                                       ; preds = %for.body315
  %255 = load i32, i32* %max_nbits_fr, align 4
  %conv319 = sitofp i32 %255 to float
  %256 = load i32, i32* %gr, align 4
  %arrayidx320 = getelementptr inbounds [2 x float], [2 x float]* %f290, i32 0, i32 %256
  %257 = load float, float* %arrayidx320, align 4
  %mul321 = fmul float %conv319, %257
  %258 = load float, float* %s291, align 4
  %div322 = fdiv float %mul321, %258
  %conv323 = fptosi float %div322 to i32
  %259 = load i32, i32* %gr, align 4
  %arrayidx324 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %259
  store i32 %conv323, i32* %arrayidx324, align 4
  br label %if.end327

if.else325:                                       ; preds = %for.body315
  %260 = load i32, i32* %gr, align 4
  %arrayidx326 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %260
  store i32 0, i32* %arrayidx326, align 4
  br label %if.end327

if.end327:                                        ; preds = %if.else325, %if.then318
  br label %for.inc328

for.inc328:                                       ; preds = %if.end327
  %261 = load i32, i32* %gr, align 4
  %inc329 = add nsw i32 %261, 1
  store i32 %inc329, i32* %gr, align 4
  br label %for.cond312

for.end330:                                       ; preds = %for.cond312
  %262 = load i32, i32* %ngr, align 4
  %cmp331 = icmp sgt i32 %262, 1
  br i1 %cmp331, label %if.then333, label %if.end381

if.then333:                                       ; preds = %for.end330
  %arrayidx334 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 0
  %263 = load i32, i32* %arrayidx334, align 4
  %arrayidx335 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 0
  %264 = load i32, i32* %arrayidx335, align 4
  %add336 = add nsw i32 %264, 125
  %cmp337 = icmp sgt i32 %263, %add336
  br i1 %cmp337, label %if.then339, label %if.end350

if.then339:                                       ; preds = %if.then333
  %arrayidx340 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 0
  %265 = load i32, i32* %arrayidx340, align 4
  %arrayidx341 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 1
  %266 = load i32, i32* %arrayidx341, align 4
  %add342 = add nsw i32 %266, %265
  store i32 %add342, i32* %arrayidx341, align 4
  %arrayidx343 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 0
  %267 = load i32, i32* %arrayidx343, align 4
  %add344 = add nsw i32 %267, 125
  %arrayidx345 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 1
  %268 = load i32, i32* %arrayidx345, align 4
  %sub346 = sub nsw i32 %268, %add344
  store i32 %sub346, i32* %arrayidx345, align 4
  %arrayidx347 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 0
  %269 = load i32, i32* %arrayidx347, align 4
  %add348 = add nsw i32 %269, 125
  %arrayidx349 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 0
  store i32 %add348, i32* %arrayidx349, align 4
  br label %if.end350

if.end350:                                        ; preds = %if.then339, %if.then333
  %arrayidx351 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 1
  %270 = load i32, i32* %arrayidx351, align 4
  %arrayidx352 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 1
  %271 = load i32, i32* %arrayidx352, align 4
  %add353 = add nsw i32 %271, 125
  %cmp354 = icmp sgt i32 %270, %add353
  br i1 %cmp354, label %if.then356, label %if.end367

if.then356:                                       ; preds = %if.end350
  %arrayidx357 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 1
  %272 = load i32, i32* %arrayidx357, align 4
  %arrayidx358 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 0
  %273 = load i32, i32* %arrayidx358, align 4
  %add359 = add nsw i32 %273, %272
  store i32 %add359, i32* %arrayidx358, align 4
  %arrayidx360 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 1
  %274 = load i32, i32* %arrayidx360, align 4
  %add361 = add nsw i32 %274, 125
  %arrayidx362 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 0
  %275 = load i32, i32* %arrayidx362, align 4
  %sub363 = sub nsw i32 %275, %add361
  store i32 %sub363, i32* %arrayidx362, align 4
  %arrayidx364 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 1
  %276 = load i32, i32* %arrayidx364, align 4
  %add365 = add nsw i32 %276, 125
  %arrayidx366 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 1
  store i32 %add365, i32* %arrayidx366, align 4
  br label %if.end367

if.end367:                                        ; preds = %if.then356, %if.end350
  store i32 0, i32* %gr, align 4
  br label %for.cond368

for.cond368:                                      ; preds = %for.inc378, %if.end367
  %277 = load i32, i32* %gr, align 4
  %278 = load i32, i32* %ngr, align 4
  %cmp369 = icmp slt i32 %277, %278
  br i1 %cmp369, label %for.body371, label %for.end380

for.body371:                                      ; preds = %for.cond368
  %279 = load i32, i32* %gr, align 4
  %arrayidx372 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %279
  %280 = load i32, i32* %arrayidx372, align 4
  %cmp373 = icmp sgt i32 %280, 7680
  br i1 %cmp373, label %if.then375, label %if.end377

if.then375:                                       ; preds = %for.body371
  %281 = load i32, i32* %gr, align 4
  %arrayidx376 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %281
  store i32 7680, i32* %arrayidx376, align 4
  br label %if.end377

if.end377:                                        ; preds = %if.then375, %for.body371
  br label %for.inc378

for.inc378:                                       ; preds = %if.end377
  %282 = load i32, i32* %gr, align 4
  %inc379 = add nsw i32 %282, 1
  store i32 %inc379, i32* %gr, align 4
  br label %for.cond368

for.end380:                                       ; preds = %for.cond368
  br label %if.end381

if.end381:                                        ; preds = %for.end380, %for.end330
  store i32 0, i32* %gr, align 4
  br label %for.cond382

for.cond382:                                      ; preds = %for.inc501, %if.end381
  %283 = load i32, i32* %gr, align 4
  %284 = load i32, i32* %ngr, align 4
  %cmp383 = icmp slt i32 %283, %284
  br i1 %cmp383, label %for.body385, label %for.end503

for.body385:                                      ; preds = %for.cond382
  %285 = bitcast [2 x float]* %f386 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %285, i8 0, i32 8, i1 false)
  store float 0.000000e+00, float* %s387, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond388

for.cond388:                                      ; preds = %for.inc407, %for.body385
  %286 = load i32, i32* %ch, align 4
  %287 = load i32, i32* %nch, align 4
  %cmp389 = icmp slt i32 %286, %287
  br i1 %cmp389, label %for.body391, label %for.end409

for.body391:                                      ; preds = %for.cond388
  %288 = load i32, i32* %gr, align 4
  %arrayidx392 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %288
  %289 = load i32, i32* %ch, align 4
  %arrayidx393 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx392, i32 0, i32 %289
  %290 = load i32, i32* %arrayidx393, align 4
  %cmp394 = icmp sgt i32 %290, 0
  br i1 %cmp394, label %if.then396, label %if.else404

if.then396:                                       ; preds = %for.body391
  %291 = load i32, i32* %gr, align 4
  %arrayidx397 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %291
  %292 = load i32, i32* %ch, align 4
  %arrayidx398 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx397, i32 0, i32 %292
  %293 = load i32, i32* %arrayidx398, align 4
  %conv399 = sitofp i32 %293 to double
  %294 = call double @llvm.sqrt.f64(double %conv399)
  %conv400 = fptrunc double %294 to float
  %295 = load i32, i32* %ch, align 4
  %arrayidx401 = getelementptr inbounds [2 x float], [2 x float]* %f386, i32 0, i32 %295
  store float %conv400, float* %arrayidx401, align 4
  %296 = load i32, i32* %ch, align 4
  %arrayidx402 = getelementptr inbounds [2 x float], [2 x float]* %f386, i32 0, i32 %296
  %297 = load float, float* %arrayidx402, align 4
  %298 = load float, float* %s387, align 4
  %add403 = fadd float %298, %297
  store float %add403, float* %s387, align 4
  br label %if.end406

if.else404:                                       ; preds = %for.body391
  %299 = load i32, i32* %ch, align 4
  %arrayidx405 = getelementptr inbounds [2 x float], [2 x float]* %f386, i32 0, i32 %299
  store float 0.000000e+00, float* %arrayidx405, align 4
  br label %if.end406

if.end406:                                        ; preds = %if.else404, %if.then396
  br label %for.inc407

for.inc407:                                       ; preds = %if.end406
  %300 = load i32, i32* %ch, align 4
  %inc408 = add nsw i32 %300, 1
  store i32 %inc408, i32* %ch, align 4
  br label %for.cond388

for.end409:                                       ; preds = %for.cond388
  store i32 0, i32* %ch, align 4
  br label %for.cond410

for.cond410:                                      ; preds = %for.inc429, %for.end409
  %301 = load i32, i32* %ch, align 4
  %302 = load i32, i32* %nch, align 4
  %cmp411 = icmp slt i32 %301, %302
  br i1 %cmp411, label %for.body413, label %for.end431

for.body413:                                      ; preds = %for.cond410
  %303 = load float, float* %s387, align 4
  %cmp414 = fcmp ogt float %303, 0.000000e+00
  br i1 %cmp414, label %if.then416, label %if.else425

if.then416:                                       ; preds = %for.body413
  %304 = load i32, i32* %gr, align 4
  %arrayidx417 = getelementptr inbounds [2 x i32], [2 x i32]* %max_nbits_gr, i32 0, i32 %304
  %305 = load i32, i32* %arrayidx417, align 4
  %conv418 = sitofp i32 %305 to float
  %306 = load i32, i32* %ch, align 4
  %arrayidx419 = getelementptr inbounds [2 x float], [2 x float]* %f386, i32 0, i32 %306
  %307 = load float, float* %arrayidx419, align 4
  %mul420 = fmul float %conv418, %307
  %308 = load float, float* %s387, align 4
  %div421 = fdiv float %mul420, %308
  %conv422 = fptosi float %div421 to i32
  %309 = load i32, i32* %gr, align 4
  %arrayidx423 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %309
  %310 = load i32, i32* %ch, align 4
  %arrayidx424 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx423, i32 0, i32 %310
  store i32 %conv422, i32* %arrayidx424, align 4
  br label %if.end428

if.else425:                                       ; preds = %for.body413
  %311 = load i32, i32* %gr, align 4
  %arrayidx426 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %311
  %312 = load i32, i32* %ch, align 4
  %arrayidx427 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx426, i32 0, i32 %312
  store i32 0, i32* %arrayidx427, align 4
  br label %if.end428

if.end428:                                        ; preds = %if.else425, %if.then416
  br label %for.inc429

for.inc429:                                       ; preds = %if.end428
  %313 = load i32, i32* %ch, align 4
  %inc430 = add nsw i32 %313, 1
  store i32 %inc430, i32* %ch, align 4
  br label %for.cond410

for.end431:                                       ; preds = %for.cond410
  %314 = load i32, i32* %nch, align 4
  %cmp432 = icmp sgt i32 %314, 1
  br i1 %cmp432, label %if.then434, label %if.end500

if.then434:                                       ; preds = %for.end431
  %315 = load i32, i32* %gr, align 4
  %arrayidx435 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %315
  %arrayidx436 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx435, i32 0, i32 0
  %316 = load i32, i32* %arrayidx436, align 8
  %317 = load i32, i32* %gr, align 4
  %arrayidx437 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %317
  %arrayidx438 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx437, i32 0, i32 0
  %318 = load i32, i32* %arrayidx438, align 8
  %add439 = add nsw i32 %318, 32
  %cmp440 = icmp sgt i32 %316, %add439
  br i1 %cmp440, label %if.then442, label %if.end459

if.then442:                                       ; preds = %if.then434
  %319 = load i32, i32* %gr, align 4
  %arrayidx443 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %319
  %arrayidx444 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx443, i32 0, i32 0
  %320 = load i32, i32* %arrayidx444, align 8
  %321 = load i32, i32* %gr, align 4
  %arrayidx445 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %321
  %arrayidx446 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx445, i32 0, i32 1
  %322 = load i32, i32* %arrayidx446, align 4
  %add447 = add nsw i32 %322, %320
  store i32 %add447, i32* %arrayidx446, align 4
  %323 = load i32, i32* %gr, align 4
  %arrayidx448 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %323
  %arrayidx449 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx448, i32 0, i32 0
  %324 = load i32, i32* %arrayidx449, align 8
  %add450 = add nsw i32 %324, 32
  %325 = load i32, i32* %gr, align 4
  %arrayidx451 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %325
  %arrayidx452 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx451, i32 0, i32 1
  %326 = load i32, i32* %arrayidx452, align 4
  %sub453 = sub nsw i32 %326, %add450
  store i32 %sub453, i32* %arrayidx452, align 4
  %327 = load i32, i32* %gr, align 4
  %arrayidx454 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %327
  %arrayidx455 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx454, i32 0, i32 0
  %328 = load i32, i32* %arrayidx455, align 8
  %add456 = add nsw i32 %328, 32
  %329 = load i32, i32* %gr, align 4
  %arrayidx457 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %329
  %arrayidx458 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx457, i32 0, i32 0
  store i32 %add456, i32* %arrayidx458, align 8
  br label %if.end459

if.end459:                                        ; preds = %if.then442, %if.then434
  %330 = load i32, i32* %gr, align 4
  %arrayidx460 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %330
  %arrayidx461 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx460, i32 0, i32 1
  %331 = load i32, i32* %arrayidx461, align 4
  %332 = load i32, i32* %gr, align 4
  %arrayidx462 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %332
  %arrayidx463 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx462, i32 0, i32 1
  %333 = load i32, i32* %arrayidx463, align 4
  %add464 = add nsw i32 %333, 32
  %cmp465 = icmp sgt i32 %331, %add464
  br i1 %cmp465, label %if.then467, label %if.end484

if.then467:                                       ; preds = %if.end459
  %334 = load i32, i32* %gr, align 4
  %arrayidx468 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %334
  %arrayidx469 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx468, i32 0, i32 1
  %335 = load i32, i32* %arrayidx469, align 4
  %336 = load i32, i32* %gr, align 4
  %arrayidx470 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %336
  %arrayidx471 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx470, i32 0, i32 0
  %337 = load i32, i32* %arrayidx471, align 8
  %add472 = add nsw i32 %337, %335
  store i32 %add472, i32* %arrayidx471, align 8
  %338 = load i32, i32* %gr, align 4
  %arrayidx473 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %338
  %arrayidx474 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx473, i32 0, i32 1
  %339 = load i32, i32* %arrayidx474, align 4
  %add475 = add nsw i32 %339, 32
  %340 = load i32, i32* %gr, align 4
  %arrayidx476 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %340
  %arrayidx477 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx476, i32 0, i32 0
  %341 = load i32, i32* %arrayidx477, align 8
  %sub478 = sub nsw i32 %341, %add475
  store i32 %sub478, i32* %arrayidx477, align 8
  %342 = load i32, i32* %gr, align 4
  %arrayidx479 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %342
  %arrayidx480 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx479, i32 0, i32 1
  %343 = load i32, i32* %arrayidx480, align 4
  %add481 = add nsw i32 %343, 32
  %344 = load i32, i32* %gr, align 4
  %arrayidx482 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %344
  %arrayidx483 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx482, i32 0, i32 1
  store i32 %add481, i32* %arrayidx483, align 4
  br label %if.end484

if.end484:                                        ; preds = %if.then467, %if.end459
  store i32 0, i32* %ch, align 4
  br label %for.cond485

for.cond485:                                      ; preds = %for.inc497, %if.end484
  %345 = load i32, i32* %ch, align 4
  %346 = load i32, i32* %nch, align 4
  %cmp486 = icmp slt i32 %345, %346
  br i1 %cmp486, label %for.body488, label %for.end499

for.body488:                                      ; preds = %for.cond485
  %347 = load i32, i32* %gr, align 4
  %arrayidx489 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %347
  %348 = load i32, i32* %ch, align 4
  %arrayidx490 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx489, i32 0, i32 %348
  %349 = load i32, i32* %arrayidx490, align 4
  %cmp491 = icmp sgt i32 %349, 4095
  br i1 %cmp491, label %if.then493, label %if.end496

if.then493:                                       ; preds = %for.body488
  %350 = load i32, i32* %gr, align 4
  %arrayidx494 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %350
  %351 = load i32, i32* %ch, align 4
  %arrayidx495 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx494, i32 0, i32 %351
  store i32 4095, i32* %arrayidx495, align 4
  br label %if.end496

if.end496:                                        ; preds = %if.then493, %for.body488
  br label %for.inc497

for.inc497:                                       ; preds = %if.end496
  %352 = load i32, i32* %ch, align 4
  %inc498 = add nsw i32 %352, 1
  store i32 %inc498, i32* %ch, align 4
  br label %for.cond485

for.end499:                                       ; preds = %for.cond485
  br label %if.end500

if.end500:                                        ; preds = %for.end499, %for.end431
  br label %for.inc501

for.inc501:                                       ; preds = %if.end500
  %353 = load i32, i32* %gr, align 4
  %inc502 = add nsw i32 %353, 1
  store i32 %inc502, i32* %gr, align 4
  br label %for.cond382

for.end503:                                       ; preds = %for.cond382
  br label %if.end504

if.end504:                                        ; preds = %for.end503, %for.end286
  store i32 0, i32* %sum_fr, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond505

for.cond505:                                      ; preds = %for.inc530, %if.end504
  %354 = load i32, i32* %gr, align 4
  %355 = load i32, i32* %ngr, align 4
  %cmp506 = icmp slt i32 %354, %355
  br i1 %cmp506, label %for.body508, label %for.end532

for.body508:                                      ; preds = %for.cond505
  store i32 0, i32* %sum_gr, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond509

for.cond509:                                      ; preds = %for.inc522, %for.body508
  %356 = load i32, i32* %ch, align 4
  %357 = load i32, i32* %nch, align 4
  %cmp510 = icmp slt i32 %356, %357
  br i1 %cmp510, label %for.body512, label %for.end524

for.body512:                                      ; preds = %for.cond509
  %358 = load i32, i32* %gr, align 4
  %arrayidx513 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %358
  %359 = load i32, i32* %ch, align 4
  %arrayidx514 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx513, i32 0, i32 %359
  %360 = load i32, i32* %arrayidx514, align 4
  %361 = load i32, i32* %sum_gr, align 4
  %add515 = add nsw i32 %361, %360
  store i32 %add515, i32* %sum_gr, align 4
  %362 = load i32, i32* %gr, align 4
  %arrayidx516 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %362
  %363 = load i32, i32* %ch, align 4
  %arrayidx517 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx516, i32 0, i32 %363
  %364 = load i32, i32* %arrayidx517, align 4
  %cmp518 = icmp sgt i32 %364, 4095
  br i1 %cmp518, label %if.then520, label %if.end521

if.then520:                                       ; preds = %for.body512
  store i32 0, i32* %ok, align 4
  br label %if.end521

if.end521:                                        ; preds = %if.then520, %for.body512
  br label %for.inc522

for.inc522:                                       ; preds = %if.end521
  %365 = load i32, i32* %ch, align 4
  %inc523 = add nsw i32 %365, 1
  store i32 %inc523, i32* %ch, align 4
  br label %for.cond509

for.end524:                                       ; preds = %for.cond509
  %366 = load i32, i32* %sum_gr, align 4
  %367 = load i32, i32* %sum_fr, align 4
  %add525 = add nsw i32 %367, %366
  store i32 %add525, i32* %sum_fr, align 4
  %368 = load i32, i32* %sum_gr, align 4
  %cmp526 = icmp sgt i32 %368, 7680
  br i1 %cmp526, label %if.then528, label %if.end529

if.then528:                                       ; preds = %for.end524
  store i32 0, i32* %ok, align 4
  br label %if.end529

if.end529:                                        ; preds = %if.then528, %for.end524
  br label %for.inc530

for.inc530:                                       ; preds = %if.end529
  %369 = load i32, i32* %gr, align 4
  %inc531 = add nsw i32 %369, 1
  store i32 %inc531, i32* %gr, align 4
  br label %for.cond505

for.end532:                                       ; preds = %for.cond505
  %370 = load i32, i32* %sum_fr, align 4
  %371 = load i32, i32* %max_nbits_fr, align 4
  %cmp533 = icmp sgt i32 %370, %371
  br i1 %cmp533, label %if.then535, label %if.end536

if.then535:                                       ; preds = %for.end532
  store i32 0, i32* %ok, align 4
  br label %if.end536

if.end536:                                        ; preds = %if.then535, %for.end532
  %372 = load i32, i32* %ok, align 4
  %tobool537 = icmp ne i32 %372, 0
  br i1 %tobool537, label %if.end557, label %if.then538

if.then538:                                       ; preds = %if.end536
  store i32 0, i32* %gr, align 4
  br label %for.cond539

for.cond539:                                      ; preds = %for.inc554, %if.then538
  %373 = load i32, i32* %gr, align 4
  %374 = load i32, i32* %ngr, align 4
  %cmp540 = icmp slt i32 %373, %374
  br i1 %cmp540, label %for.body542, label %for.end556

for.body542:                                      ; preds = %for.cond539
  store i32 0, i32* %ch, align 4
  br label %for.cond543

for.cond543:                                      ; preds = %for.inc551, %for.body542
  %375 = load i32, i32* %ch, align 4
  %376 = load i32, i32* %nch, align 4
  %cmp544 = icmp slt i32 %375, %376
  br i1 %cmp544, label %for.body546, label %for.end553

for.body546:                                      ; preds = %for.cond543
  %377 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %378 = load i32, i32* %gr, align 4
  %arrayidx547 = getelementptr inbounds [2 x i32], [2 x i32]* %377, i32 %378
  %379 = load i32, i32* %ch, align 4
  %arrayidx548 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx547, i32 0, i32 %379
  %380 = load i32, i32* %arrayidx548, align 4
  %381 = load i32, i32* %gr, align 4
  %arrayidx549 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %381
  %382 = load i32, i32* %ch, align 4
  %arrayidx550 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx549, i32 0, i32 %382
  store i32 %380, i32* %arrayidx550, align 4
  br label %for.inc551

for.inc551:                                       ; preds = %for.body546
  %383 = load i32, i32* %ch, align 4
  %inc552 = add nsw i32 %383, 1
  store i32 %inc552, i32* %ch, align 4
  br label %for.cond543

for.end553:                                       ; preds = %for.cond543
  br label %for.inc554

for.inc554:                                       ; preds = %for.end553
  %384 = load i32, i32* %gr, align 4
  %inc555 = add nsw i32 %384, 1
  store i32 %inc555, i32* %gr, align 4
  br label %for.cond539

for.end556:                                       ; preds = %for.cond539
  br label %if.end557

if.end557:                                        ; preds = %for.end556, %if.end536
  store i32 0, i32* %ch, align 4
  br label %for.cond558

for.cond558:                                      ; preds = %for.inc577, %if.end557
  %385 = load i32, i32* %ch, align 4
  %386 = load i32, i32* %nch, align 4
  %cmp559 = icmp slt i32 %385, %386
  br i1 %cmp559, label %for.body561, label %for.end579

for.body561:                                      ; preds = %for.cond558
  %387 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side562 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %387, i32 0, i32 7
  %scfsi = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side562, i32 0, i32 5
  %388 = load i32, i32* %ch, align 4
  %arrayidx563 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi, i32 0, i32 %388
  %arrayidx564 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx563, i32 0, i32 0
  store i32 0, i32* %arrayidx564, align 4
  %389 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side565 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %389, i32 0, i32 7
  %scfsi566 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side565, i32 0, i32 5
  %390 = load i32, i32* %ch, align 4
  %arrayidx567 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi566, i32 0, i32 %390
  %arrayidx568 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx567, i32 0, i32 1
  store i32 0, i32* %arrayidx568, align 4
  %391 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side569 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %391, i32 0, i32 7
  %scfsi570 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side569, i32 0, i32 5
  %392 = load i32, i32* %ch, align 4
  %arrayidx571 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi570, i32 0, i32 %392
  %arrayidx572 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx571, i32 0, i32 2
  store i32 0, i32* %arrayidx572, align 4
  %393 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side573 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %393, i32 0, i32 7
  %scfsi574 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side573, i32 0, i32 5
  %394 = load i32, i32* %ch, align 4
  %arrayidx575 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi574, i32 0, i32 %394
  %arrayidx576 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx575, i32 0, i32 3
  store i32 0, i32* %arrayidx576, align 4
  br label %for.inc577

for.inc577:                                       ; preds = %for.body561
  %395 = load i32, i32* %ch, align 4
  %inc578 = add nsw i32 %395, 1
  store i32 %inc578, i32* %ch, align 4
  br label %for.cond558

for.end579:                                       ; preds = %for.cond558
  store i32 0, i32* %gr, align 4
  br label %for.cond580

for.cond580:                                      ; preds = %for.inc595, %for.end579
  %396 = load i32, i32* %gr, align 4
  %397 = load i32, i32* %ngr, align 4
  %cmp581 = icmp slt i32 %396, %397
  br i1 %cmp581, label %for.body583, label %for.end597

for.body583:                                      ; preds = %for.cond580
  store i32 0, i32* %ch, align 4
  br label %for.cond584

for.cond584:                                      ; preds = %for.inc592, %for.body583
  %398 = load i32, i32* %ch, align 4
  %399 = load i32, i32* %nch, align 4
  %cmp585 = icmp slt i32 %398, %399
  br i1 %cmp585, label %for.body587, label %for.end594

for.body587:                                      ; preds = %for.cond584
  %400 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side588 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %400, i32 0, i32 7
  %tt589 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side588, i32 0, i32 0
  %401 = load i32, i32* %gr, align 4
  %arrayidx590 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt589, i32 0, i32 %401
  %402 = load i32, i32* %ch, align 4
  %arrayidx591 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx590, i32 0, i32 %402
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx591, i32 0, i32 8
  store i32 0, i32* %scalefac_compress, align 4
  br label %for.inc592

for.inc592:                                       ; preds = %for.body587
  %403 = load i32, i32* %ch, align 4
  %inc593 = add nsw i32 %403, 1
  store i32 %inc593, i32* %ch, align 4
  br label %for.cond584

for.end594:                                       ; preds = %for.cond584
  br label %for.inc595

for.inc595:                                       ; preds = %for.end594
  %404 = load i32, i32* %gr, align 4
  %inc596 = add nsw i32 %404, 1
  store i32 %inc596, i32* %gr, align 4
  br label %for.cond580

for.end597:                                       ; preds = %for.cond580
  store i32 0, i32* %use_nbits_fr, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond598

for.cond598:                                      ; preds = %for.inc641, %for.end597
  %405 = load i32, i32* %gr, align 4
  %406 = load i32, i32* %ngr, align 4
  %cmp599 = icmp slt i32 %405, %406
  br i1 %cmp599, label %for.body601, label %for.end643

for.body601:                                      ; preds = %for.cond598
  %407 = load i32, i32* %gr, align 4
  %arrayidx602 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %407
  store i32 0, i32* %arrayidx602, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond603

for.cond603:                                      ; preds = %for.inc636, %for.body601
  %408 = load i32, i32* %ch, align 4
  %409 = load i32, i32* %nch, align 4
  %cmp604 = icmp slt i32 %408, %409
  br i1 %cmp604, label %for.body606, label %for.end638

for.body606:                                      ; preds = %for.cond603
  %410 = load i32, i32* %gr, align 4
  %arrayidx608 = getelementptr inbounds [2 x [2 x %struct.algo_s]], [2 x [2 x %struct.algo_s]]* %that_, i32 0, i32 %410
  %411 = load i32, i32* %ch, align 4
  %arrayidx609 = getelementptr inbounds [2 x %struct.algo_s], [2 x %struct.algo_s]* %arrayidx608, i32 0, i32 %411
  store %struct.algo_s* %arrayidx609, %struct.algo_s** %that607, align 4
  %412 = load i32, i32* %gr, align 4
  %arrayidx610 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %412
  %413 = load i32, i32* %ch, align 4
  %arrayidx611 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx610, i32 0, i32 %413
  store i32 0, i32* %arrayidx611, align 4
  %414 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %415 = load i32, i32* %gr, align 4
  %arrayidx612 = getelementptr inbounds [2 x i32], [2 x i32]* %414, i32 %415
  %416 = load i32, i32* %ch, align 4
  %arrayidx613 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx612, i32 0, i32 %416
  %417 = load i32, i32* %arrayidx613, align 4
  %cmp614 = icmp sgt i32 %417, 0
  br i1 %cmp614, label %if.then616, label %if.end628

if.then616:                                       ; preds = %for.body606
  %418 = load i32, i32* %gr, align 4
  %arrayidx618 = getelementptr inbounds [2 x [2 x [39 x i32]]], [2 x [2 x [39 x i32]]]* %sfwork_, i32 0, i32 %418
  %419 = load i32, i32* %ch, align 4
  %arrayidx619 = getelementptr inbounds [2 x [39 x i32]], [2 x [39 x i32]]* %arrayidx618, i32 0, i32 %419
  %arraydecay620 = getelementptr inbounds [39 x i32], [39 x i32]* %arrayidx619, i32 0, i32 0
  store i32* %arraydecay620, i32** %sfwork617, align 4
  %420 = load i32, i32* %gr, align 4
  %arrayidx622 = getelementptr inbounds [2 x [2 x [39 x i32]]], [2 x [2 x [39 x i32]]]* %vbrsfmin_, i32 0, i32 %420
  %421 = load i32, i32* %ch, align 4
  %arrayidx623 = getelementptr inbounds [2 x [39 x i32]], [2 x [39 x i32]]* %arrayidx622, i32 0, i32 %421
  %arraydecay624 = getelementptr inbounds [39 x i32], [39 x i32]* %arrayidx623, i32 0, i32 0
  store i32* %arraydecay624, i32** %vbrsfmin621, align 4
  %422 = load i32*, i32** %sfwork617, align 4
  %423 = load i32*, i32** %sfwork617, align 4
  %424 = load %struct.algo_s*, %struct.algo_s** %that607, align 4
  %cod_info625 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %424, i32 0, i32 4
  %425 = load %struct.gr_info*, %struct.gr_info** %cod_info625, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %425, i32 0, i32 7
  %426 = load i32, i32* %global_gain, align 4
  call void @cutDistribution(i32* %422, i32* %423, i32 %426)
  %427 = load %struct.algo_s*, %struct.algo_s** %that607, align 4
  %428 = load i32*, i32** %sfwork617, align 4
  %429 = load i32*, i32** %vbrsfmin621, align 4
  %430 = load i32, i32* %gr, align 4
  %arrayidx626 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_nbits_ch, i32 0, i32 %430
  %431 = load i32, i32* %ch, align 4
  %arrayidx627 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx626, i32 0, i32 %431
  %432 = load i32, i32* %arrayidx627, align 4
  call void @outOfBitsStrategy(%struct.algo_s* %427, i32* %428, i32* %429, i32 %432)
  br label %if.end628

if.end628:                                        ; preds = %if.then616, %for.body606
  %433 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %434 = load i32, i32* %gr, align 4
  %435 = load i32, i32* %ch, align 4
  %call629 = call i32 @reduce_bit_usage(%struct.lame_internal_flags* %433, i32 %434, i32 %435)
  %436 = load i32, i32* %gr, align 4
  %arrayidx630 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %436
  %437 = load i32, i32* %ch, align 4
  %arrayidx631 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx630, i32 0, i32 %437
  store i32 %call629, i32* %arrayidx631, align 4
  %438 = load i32, i32* %gr, align 4
  %arrayidx632 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %use_nbits_ch, i32 0, i32 %438
  %439 = load i32, i32* %ch, align 4
  %arrayidx633 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx632, i32 0, i32 %439
  %440 = load i32, i32* %arrayidx633, align 4
  %441 = load i32, i32* %gr, align 4
  %arrayidx634 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %441
  %442 = load i32, i32* %arrayidx634, align 4
  %add635 = add nsw i32 %442, %440
  store i32 %add635, i32* %arrayidx634, align 4
  br label %for.inc636

for.inc636:                                       ; preds = %if.end628
  %443 = load i32, i32* %ch, align 4
  %inc637 = add nsw i32 %443, 1
  store i32 %inc637, i32* %ch, align 4
  br label %for.cond603

for.end638:                                       ; preds = %for.cond603
  %444 = load i32, i32* %gr, align 4
  %arrayidx639 = getelementptr inbounds [2 x i32], [2 x i32]* %use_nbits_gr, i32 0, i32 %444
  %445 = load i32, i32* %arrayidx639, align 4
  %446 = load i32, i32* %use_nbits_fr, align 4
  %add640 = add nsw i32 %446, %445
  store i32 %add640, i32* %use_nbits_fr, align 4
  br label %for.inc641

for.inc641:                                       ; preds = %for.end638
  %447 = load i32, i32* %gr, align 4
  %inc642 = add nsw i32 %447, 1
  store i32 %inc642, i32* %gr, align 4
  br label %for.cond598

for.end643:                                       ; preds = %for.cond598
  %448 = load i32, i32* %use_nbits_fr, align 4
  %449 = load i32, i32* %max_nbits_fr, align 4
  %cmp644 = icmp sle i32 %448, %449
  br i1 %cmp644, label %if.then646, label %if.end647

if.then646:                                       ; preds = %for.end643
  %450 = load i32, i32* %use_nbits_fr, align 4
  store i32 %450, i32* %retval, align 4
  br label %return

if.end647:                                        ; preds = %for.end643
  %451 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %452 = load i32, i32* %max_nbits_fr, align 4
  %453 = load i32, i32* %use_nbits_fr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %451, i8* getelementptr inbounds ([86 x i8], [86 x i8]* @.str, i32 0, i32 0), i32 %452, i32 %453)
  call void @exit(i32 -1) #6
  unreachable

return:                                           ; preds = %if.then646, %if.then131
  %454 = load i32, i32* %retval, align 4
  ret i32 %454
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal zeroext i8 @guess_scalefac_x34(float* %xr, float* %xr34, float %l3_xmin, i32 %bw, i8 zeroext %sf_min) #0 {
entry:
  %retval = alloca i8, align 1
  %xr.addr = alloca float*, align 4
  %xr34.addr = alloca float*, align 4
  %l3_xmin.addr = alloca float, align 4
  %bw.addr = alloca i32, align 4
  %sf_min.addr = alloca i8, align 1
  %guess = alloca i32, align 4
  store float* %xr, float** %xr.addr, align 4
  store float* %xr34, float** %xr34.addr, align 4
  store float %l3_xmin, float* %l3_xmin.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store i8 %sf_min, i8* %sf_min.addr, align 1
  %0 = load float, float* %l3_xmin.addr, align 4
  %1 = load i32, i32* %bw.addr, align 4
  %call = call i32 @calc_scalefac(float %0, i32 %1)
  store i32 %call, i32* %guess, align 4
  %2 = load i32, i32* %guess, align 4
  %3 = load i8, i8* %sf_min.addr, align 1
  %conv = zext i8 %3 to i32
  %cmp = icmp slt i32 %2, %conv
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load i8, i8* %sf_min.addr, align 1
  store i8 %4, i8* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %guess, align 4
  %cmp2 = icmp sge i32 %5, 255
  br i1 %cmp2, label %if.then4, label %if.end5

if.then4:                                         ; preds = %if.end
  store i8 -1, i8* %retval, align 1
  br label %return

if.end5:                                          ; preds = %if.end
  %6 = load float*, float** %xr.addr, align 4
  %7 = load float*, float** %xr34.addr, align 4
  %8 = load i32, i32* %guess, align 4
  %conv6 = trunc i32 %8 to i8
  store i8 %conv6, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end5, %if.then4, %if.then
  %9 = load i8, i8* %retval, align 1
  ret i8 %9
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i8 @find_scalefac_x34(float* %xr, float* %xr34, float %l3_xmin, i32 %bw, i8 zeroext %sf_min) #0 {
entry:
  %xr.addr = alloca float*, align 4
  %xr34.addr = alloca float*, align 4
  %l3_xmin.addr = alloca float, align 4
  %bw.addr = alloca i32, align 4
  %sf_min.addr = alloca i8, align 1
  %did_it = alloca [256 x %struct.calc_noise_cache], align 16
  %sf = alloca i8, align 1
  %sf_ok = alloca i8, align 1
  %delsf = alloca i8, align 1
  %seen_good_one = alloca i8, align 1
  %i = alloca i8, align 1
  %bad = alloca i8, align 1
  store float* %xr, float** %xr.addr, align 4
  store float* %xr34, float** %xr34.addr, align 4
  store float %l3_xmin, float* %l3_xmin.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store i8 %sf_min, i8* %sf_min.addr, align 1
  store i8 -128, i8* %sf, align 1
  store i8 -1, i8* %sf_ok, align 1
  store i8 -128, i8* %delsf, align 1
  store i8 0, i8* %seen_good_one, align 1
  %arraydecay = getelementptr inbounds [256 x %struct.calc_noise_cache], [256 x %struct.calc_noise_cache]* %did_it, i32 0, i32 0
  %0 = bitcast %struct.calc_noise_cache* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %0, i8 0, i32 2048, i1 false)
  store i8 0, i8* %i, align 1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i8, i8* %i, align 1
  %conv = zext i8 %1 to i32
  %cmp = icmp slt i32 %conv, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8, i8* %delsf, align 1
  %conv2 = zext i8 %2 to i32
  %shr = ashr i32 %conv2, 1
  %conv3 = trunc i32 %shr to i8
  store i8 %conv3, i8* %delsf, align 1
  %3 = load i8, i8* %sf, align 1
  %conv4 = zext i8 %3 to i32
  %4 = load i8, i8* %sf_min.addr, align 1
  %conv5 = zext i8 %4 to i32
  %cmp6 = icmp sle i32 %conv4, %conv5
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %5 = load i8, i8* %delsf, align 1
  %conv8 = zext i8 %5 to i32
  %6 = load i8, i8* %sf, align 1
  %conv9 = zext i8 %6 to i32
  %add = add nsw i32 %conv9, %conv8
  %conv10 = trunc i32 %add to i8
  store i8 %conv10, i8* %sf, align 1
  br label %if.end21

if.else:                                          ; preds = %for.body
  %7 = load float*, float** %xr.addr, align 4
  %8 = load float*, float** %xr34.addr, align 4
  %9 = load float, float* %l3_xmin.addr, align 4
  %10 = load i32, i32* %bw.addr, align 4
  %11 = load i8, i8* %sf, align 1
  %arraydecay11 = getelementptr inbounds [256 x %struct.calc_noise_cache], [256 x %struct.calc_noise_cache]* %did_it, i32 0, i32 0
  %call = call zeroext i8 @tri_calc_sfb_noise_x34(float* %7, float* %8, float %9, i32 %10, i8 zeroext %11, %struct.calc_noise_cache* %arraydecay11)
  store i8 %call, i8* %bad, align 1
  %12 = load i8, i8* %bad, align 1
  %tobool = icmp ne i8 %12, 0
  br i1 %tobool, label %if.then12, label %if.else16

if.then12:                                        ; preds = %if.else
  %13 = load i8, i8* %delsf, align 1
  %conv13 = zext i8 %13 to i32
  %14 = load i8, i8* %sf, align 1
  %conv14 = zext i8 %14 to i32
  %sub = sub nsw i32 %conv14, %conv13
  %conv15 = trunc i32 %sub to i8
  store i8 %conv15, i8* %sf, align 1
  br label %if.end

if.else16:                                        ; preds = %if.else
  %15 = load i8, i8* %sf, align 1
  store i8 %15, i8* %sf_ok, align 1
  %16 = load i8, i8* %delsf, align 1
  %conv17 = zext i8 %16 to i32
  %17 = load i8, i8* %sf, align 1
  %conv18 = zext i8 %17 to i32
  %add19 = add nsw i32 %conv18, %conv17
  %conv20 = trunc i32 %add19 to i8
  store i8 %conv20, i8* %sf, align 1
  store i8 1, i8* %seen_good_one, align 1
  br label %if.end

if.end:                                           ; preds = %if.else16, %if.then12
  br label %if.end21

if.end21:                                         ; preds = %if.end, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %18 = load i8, i8* %i, align 1
  %inc = add i8 %18, 1
  store i8 %inc, i8* %i, align 1
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %19 = load i8, i8* %seen_good_one, align 1
  %conv22 = zext i8 %19 to i32
  %cmp23 = icmp sgt i32 %conv22, 0
  br i1 %cmp23, label %if.then25, label %if.end26

if.then25:                                        ; preds = %for.end
  %20 = load i8, i8* %sf_ok, align 1
  store i8 %20, i8* %sf, align 1
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %for.end
  %21 = load i8, i8* %sf, align 1
  %conv27 = zext i8 %21 to i32
  %22 = load i8, i8* %sf_min.addr, align 1
  %conv28 = zext i8 %22 to i32
  %cmp29 = icmp sle i32 %conv27, %conv28
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end26
  %23 = load i8, i8* %sf_min.addr, align 1
  store i8 %23, i8* %sf, align 1
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end26
  %24 = load i8, i8* %sf, align 1
  ret i8 %24
}

; Function Attrs: noinline nounwind optnone
define internal void @short_block_constrain(%struct.algo_s* %that, i32* %vbrsf, i32* %vbrsfmin, i32 %vbrmax) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %vbrsf.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %vbrmax.addr = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %maxminsfb = alloca i32, align 4
  %mover = alloca i32, align 4
  %maxover0 = alloca i32, align 4
  %maxover1 = alloca i32, align 4
  %delta = alloca i32, align 4
  %v = alloca i32, align 4
  %v0 = alloca i32, align 4
  %v1 = alloca i32, align 4
  %sfb = alloca i32, align 4
  %psymax = alloca i32, align 4
  %sf_temp = alloca [39 x i32], align 16
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %vbrsf, i32** %vbrsf.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %vbrmax, i32* %vbrmax.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info1, align 4
  store %struct.gr_info* %1, %struct.gr_info** %cod_info, align 4
  %2 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %gfc2 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %2, i32 0, i32 3
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc2, align 4
  store %struct.lame_internal_flags* %3, %struct.lame_internal_flags** %gfc, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg3, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_l = getelementptr inbounds %struct.algo_s, %struct.algo_s* %5, i32 0, i32 5
  %6 = load i32, i32* %mingain_l, align 4
  store i32 %6, i32* %maxminsfb, align 4
  store i32 0, i32* %maxover0, align 4
  store i32 0, i32* %maxover1, align 4
  store i32 0, i32* %delta, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %psymax4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 23
  %8 = load i32, i32* %psymax4, align 4
  store i32 %8, i32* %psymax, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %sfb, align 4
  %10 = load i32, i32* %psymax, align 4
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %vbrmax.addr, align 4
  %12 = load i32*, i32** %vbrsf.addr, align 4
  %13 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds i32, i32* %12, i32 %13
  %14 = load i32, i32* %arrayidx, align 4
  %sub = sub nsw i32 %11, %14
  store i32 %sub, i32* %v, align 4
  %15 = load i32, i32* %delta, align 4
  %16 = load i32, i32* %v, align 4
  %cmp5 = icmp slt i32 %15, %16
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = load i32, i32* %v, align 4
  store i32 %17, i32* %delta, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %18 = load i32, i32* %v, align 4
  %19 = load i32, i32* %sfb, align 4
  %arrayidx6 = getelementptr inbounds [39 x i8], [39 x i8]* @max_range_short, i32 0, i32 %19
  %20 = load i8, i8* %arrayidx6, align 1
  %conv = zext i8 %20 to i32
  %mul = mul nsw i32 2, %conv
  %add = add nsw i32 56, %mul
  %sub7 = sub nsw i32 %18, %add
  store i32 %sub7, i32* %v0, align 4
  %21 = load i32, i32* %v, align 4
  %22 = load i32, i32* %sfb, align 4
  %arrayidx8 = getelementptr inbounds [39 x i8], [39 x i8]* @max_range_short, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %23 to i32
  %mul10 = mul nsw i32 4, %conv9
  %add11 = add nsw i32 56, %mul10
  %sub12 = sub nsw i32 %21, %add11
  store i32 %sub12, i32* %v1, align 4
  %24 = load i32, i32* %maxover0, align 4
  %25 = load i32, i32* %v0, align 4
  %cmp13 = icmp slt i32 %24, %25
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  %26 = load i32, i32* %v0, align 4
  store i32 %26, i32* %maxover0, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %if.end
  %27 = load i32, i32* %maxover1, align 4
  %28 = load i32, i32* %v1, align 4
  %cmp17 = icmp slt i32 %27, %28
  br i1 %cmp17, label %if.then19, label %if.end20

if.then19:                                        ; preds = %if.end16
  %29 = load i32, i32* %v1, align 4
  store i32 %29, i32* %maxover1, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %if.end16
  br label %for.inc

for.inc:                                          ; preds = %if.end20
  %30 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %31 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %31, i32 0, i32 3
  %32 = load i32, i32* %noise_shaping, align 4
  %cmp21 = icmp eq i32 %32, 2
  br i1 %cmp21, label %if.then23, label %if.else

if.then23:                                        ; preds = %for.end
  %33 = load i32, i32* %maxover0, align 4
  %34 = load i32, i32* %maxover1, align 4
  %cmp24 = icmp slt i32 %33, %34
  br i1 %cmp24, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then23
  %35 = load i32, i32* %maxover0, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then23
  %36 = load i32, i32* %maxover1, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %35, %cond.true ], [ %36, %cond.false ]
  store i32 %cond, i32* %mover, align 4
  br label %if.end26

if.else:                                          ; preds = %for.end
  %37 = load i32, i32* %maxover0, align 4
  store i32 %37, i32* %mover, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.else, %cond.end
  %38 = load i32, i32* %delta, align 4
  %39 = load i32, i32* %mover, align 4
  %cmp27 = icmp sgt i32 %38, %39
  br i1 %cmp27, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end26
  %40 = load i32, i32* %mover, align 4
  store i32 %40, i32* %delta, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end26
  %41 = load i32, i32* %delta, align 4
  %42 = load i32, i32* %vbrmax.addr, align 4
  %sub31 = sub nsw i32 %42, %41
  store i32 %sub31, i32* %vbrmax.addr, align 4
  %43 = load i32, i32* %mover, align 4
  %44 = load i32, i32* %maxover0, align 4
  %sub32 = sub nsw i32 %44, %43
  store i32 %sub32, i32* %maxover0, align 4
  %45 = load i32, i32* %mover, align 4
  %46 = load i32, i32* %maxover1, align 4
  %sub33 = sub nsw i32 %46, %45
  store i32 %sub33, i32* %maxover1, align 4
  %47 = load i32, i32* %maxover0, align 4
  %cmp34 = icmp eq i32 %47, 0
  br i1 %cmp34, label %if.then36, label %if.else37

if.then36:                                        ; preds = %if.end30
  %48 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %48, i32 0, i32 16
  store i32 0, i32* %scalefac_scale, align 4
  br label %if.end43

if.else37:                                        ; preds = %if.end30
  %49 = load i32, i32* %maxover1, align 4
  %cmp38 = icmp eq i32 %49, 0
  br i1 %cmp38, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.else37
  %50 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale41 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %50, i32 0, i32 16
  store i32 1, i32* %scalefac_scale41, align 4
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %if.else37
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.then36
  %51 = load i32, i32* %vbrmax.addr, align 4
  %52 = load i32, i32* %maxminsfb, align 4
  %cmp44 = icmp slt i32 %51, %52
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.end43
  %53 = load i32, i32* %maxminsfb, align 4
  store i32 %53, i32* %vbrmax.addr, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %if.end43
  %54 = load i32, i32* %vbrmax.addr, align 4
  %55 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %55, i32 0, i32 7
  store i32 %54, i32* %global_gain, align 4
  %56 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain48 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %56, i32 0, i32 7
  %57 = load i32, i32* %global_gain48, align 4
  %cmp49 = icmp slt i32 %57, 0
  br i1 %cmp49, label %if.then51, label %if.else53

if.then51:                                        ; preds = %if.end47
  %58 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain52 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %58, i32 0, i32 7
  store i32 0, i32* %global_gain52, align 4
  br label %if.end60

if.else53:                                        ; preds = %if.end47
  %59 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain54 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %59, i32 0, i32 7
  %60 = load i32, i32* %global_gain54, align 4
  %cmp55 = icmp sgt i32 %60, 255
  br i1 %cmp55, label %if.then57, label %if.end59

if.then57:                                        ; preds = %if.else53
  %61 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain58 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %61, i32 0, i32 7
  store i32 255, i32* %global_gain58, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.then57, %if.else53
  br label %if.end60

if.end60:                                         ; preds = %if.end59, %if.then51
  store i32 0, i32* %sfb, align 4
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc68, %if.end60
  %62 = load i32, i32* %sfb, align 4
  %cmp62 = icmp slt i32 %62, 39
  br i1 %cmp62, label %for.body64, label %for.end70

for.body64:                                       ; preds = %for.cond61
  %63 = load i32*, i32** %vbrsf.addr, align 4
  %64 = load i32, i32* %sfb, align 4
  %arrayidx65 = getelementptr inbounds i32, i32* %63, i32 %64
  %65 = load i32, i32* %arrayidx65, align 4
  %66 = load i32, i32* %vbrmax.addr, align 4
  %sub66 = sub nsw i32 %65, %66
  %67 = load i32, i32* %sfb, align 4
  %arrayidx67 = getelementptr inbounds [39 x i32], [39 x i32]* %sf_temp, i32 0, i32 %67
  store i32 %sub66, i32* %arrayidx67, align 4
  br label %for.inc68

for.inc68:                                        ; preds = %for.body64
  %68 = load i32, i32* %sfb, align 4
  %inc69 = add nsw i32 %68, 1
  store i32 %inc69, i32* %sfb, align 4
  br label %for.cond61

for.end70:                                        ; preds = %for.cond61
  %69 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %70 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s = getelementptr inbounds %struct.algo_s, %struct.algo_s* %70, i32 0, i32 6
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s, i32 0, i32 0
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %sf_temp, i32 0, i32 0
  call void @set_subblock_gain(%struct.gr_info* %69, i32* %arrayidx71, i32* %arraydecay)
  %71 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %72 = load i32*, i32** %vbrsfmin.addr, align 4
  %arraydecay72 = getelementptr inbounds [39 x i32], [39 x i32]* %sf_temp, i32 0, i32 0
  call void @set_scalefacs(%struct.gr_info* %71, i32* %72, i32* %arraydecay72, i8* getelementptr inbounds ([39 x i8], [39 x i8]* @max_range_short, i32 0, i32 0))
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @long_block_constrain(%struct.algo_s* %that, i32* %vbrsf, i32* %vbrsfmin, i32 %vbrmax) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %vbrsf.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %vbrmax.addr = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %max_rangep = alloca i8*, align 4
  %maxminsfb = alloca i32, align 4
  %sfb = alloca i32, align 4
  %maxover0 = alloca i32, align 4
  %maxover1 = alloca i32, align 4
  %maxover0p = alloca i32, align 4
  %maxover1p = alloca i32, align 4
  %mover = alloca i32, align 4
  %delta = alloca i32, align 4
  %v = alloca i32, align 4
  %v0 = alloca i32, align 4
  %v1 = alloca i32, align 4
  %v0p = alloca i32, align 4
  %v1p = alloca i32, align 4
  %vm0p = alloca i32, align 4
  %vm1p = alloca i32, align 4
  %psymax = alloca i32, align 4
  %gain = alloca i32, align 4
  %a = alloca i32, align 4
  %gain68 = alloca i32, align 4
  %b = alloca i32, align 4
  %sf_temp = alloca [39 x i32], align 16
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %vbrsf, i32** %vbrsf.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %vbrmax, i32* %vbrmax.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info1, align 4
  store %struct.gr_info* %1, %struct.gr_info** %cod_info, align 4
  %2 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %gfc2 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %2, i32 0, i32 3
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc2, align 4
  store %struct.lame_internal_flags* %3, %struct.lame_internal_flags** %gfc, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg3, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_l = getelementptr inbounds %struct.algo_s, %struct.algo_s* %5, i32 0, i32 5
  %6 = load i32, i32* %mingain_l, align 4
  store i32 %6, i32* %maxminsfb, align 4
  store i32 0, i32* %delta, align 4
  store i32 1, i32* %vm0p, align 4
  store i32 1, i32* %vm1p, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %psymax4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 23
  %8 = load i32, i32* %psymax4, align 4
  store i32 %8, i32* %psymax, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 15
  %10 = load i32, i32* %mode_gr, align 4
  %cmp = icmp eq i32 %10, 2
  %11 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i8* getelementptr inbounds ([22 x i8], [22 x i8]* @max_range_long, i32 0, i32 0), i8* getelementptr inbounds ([22 x i8], [22 x i8]* bitcast (<{ [11 x i8], [11 x i8] }>* @max_range_long_lsf_pretab to [22 x i8]*), i32 0, i32 0)
  store i8* %cond, i8** %max_rangep, align 4
  store i32 0, i32* %maxover0, align 4
  store i32 0, i32* %maxover1, align 4
  store i32 0, i32* %maxover0p, align 4
  store i32 0, i32* %maxover1p, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %sfb, align 4
  %13 = load i32, i32* %psymax, align 4
  %cmp5 = icmp slt i32 %12, %13
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load i32, i32* %vbrmax.addr, align 4
  %15 = load i32*, i32** %vbrsf.addr, align 4
  %16 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx, align 4
  %sub = sub nsw i32 %14, %17
  store i32 %sub, i32* %v, align 4
  %18 = load i32, i32* %delta, align 4
  %19 = load i32, i32* %v, align 4
  %cmp6 = icmp slt i32 %18, %19
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %20 = load i32, i32* %v, align 4
  store i32 %20, i32* %delta, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %21 = load i32, i32* %v, align 4
  %22 = load i32, i32* %sfb, align 4
  %arrayidx7 = getelementptr inbounds [22 x i8], [22 x i8]* @max_range_long, i32 0, i32 %22
  %23 = load i8, i8* %arrayidx7, align 1
  %conv = zext i8 %23 to i32
  %mul = mul nsw i32 2, %conv
  %sub8 = sub nsw i32 %21, %mul
  store i32 %sub8, i32* %v0, align 4
  %24 = load i32, i32* %v, align 4
  %25 = load i32, i32* %sfb, align 4
  %arrayidx9 = getelementptr inbounds [22 x i8], [22 x i8]* @max_range_long, i32 0, i32 %25
  %26 = load i8, i8* %arrayidx9, align 1
  %conv10 = zext i8 %26 to i32
  %mul11 = mul nsw i32 4, %conv10
  %sub12 = sub nsw i32 %24, %mul11
  store i32 %sub12, i32* %v1, align 4
  %27 = load i32, i32* %v, align 4
  %28 = load i8*, i8** %max_rangep, align 4
  %29 = load i32, i32* %sfb, align 4
  %arrayidx13 = getelementptr inbounds i8, i8* %28, i32 %29
  %30 = load i8, i8* %arrayidx13, align 1
  %conv14 = zext i8 %30 to i32
  %31 = load i32, i32* %sfb, align 4
  %arrayidx15 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx15, align 4
  %add = add nsw i32 %conv14, %32
  %mul16 = mul nsw i32 2, %add
  %sub17 = sub nsw i32 %27, %mul16
  store i32 %sub17, i32* %v0p, align 4
  %33 = load i32, i32* %v, align 4
  %34 = load i8*, i8** %max_rangep, align 4
  %35 = load i32, i32* %sfb, align 4
  %arrayidx18 = getelementptr inbounds i8, i8* %34, i32 %35
  %36 = load i8, i8* %arrayidx18, align 1
  %conv19 = zext i8 %36 to i32
  %37 = load i32, i32* %sfb, align 4
  %arrayidx20 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx20, align 4
  %add21 = add nsw i32 %conv19, %38
  %mul22 = mul nsw i32 4, %add21
  %sub23 = sub nsw i32 %33, %mul22
  store i32 %sub23, i32* %v1p, align 4
  %39 = load i32, i32* %maxover0, align 4
  %40 = load i32, i32* %v0, align 4
  %cmp24 = icmp slt i32 %39, %40
  br i1 %cmp24, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end
  %41 = load i32, i32* %v0, align 4
  store i32 %41, i32* %maxover0, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %if.end
  %42 = load i32, i32* %maxover1, align 4
  %43 = load i32, i32* %v1, align 4
  %cmp28 = icmp slt i32 %42, %43
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.end27
  %44 = load i32, i32* %v1, align 4
  store i32 %44, i32* %maxover1, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.end27
  %45 = load i32, i32* %maxover0p, align 4
  %46 = load i32, i32* %v0p, align 4
  %cmp32 = icmp slt i32 %45, %46
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  %47 = load i32, i32* %v0p, align 4
  store i32 %47, i32* %maxover0p, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end31
  %48 = load i32, i32* %maxover1p, align 4
  %49 = load i32, i32* %v1p, align 4
  %cmp36 = icmp slt i32 %48, %49
  br i1 %cmp36, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.end35
  %50 = load i32, i32* %v1p, align 4
  store i32 %50, i32* %maxover1p, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.then38, %if.end35
  br label %for.inc

for.inc:                                          ; preds = %if.end39
  %51 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %51, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %52 = load i32, i32* %vm0p, align 4
  %cmp40 = icmp eq i32 %52, 1
  br i1 %cmp40, label %if.then42, label %if.end64

if.then42:                                        ; preds = %for.end
  %53 = load i32, i32* %vbrmax.addr, align 4
  %54 = load i32, i32* %maxover0p, align 4
  %sub43 = sub nsw i32 %53, %54
  store i32 %sub43, i32* %gain, align 4
  %55 = load i32, i32* %gain, align 4
  %56 = load i32, i32* %maxminsfb, align 4
  %cmp44 = icmp slt i32 %55, %56
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.then42
  %57 = load i32, i32* %maxminsfb, align 4
  store i32 %57, i32* %gain, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %if.then42
  store i32 0, i32* %sfb, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc61, %if.end47
  %58 = load i32, i32* %sfb, align 4
  %59 = load i32, i32* %psymax, align 4
  %cmp49 = icmp slt i32 %58, %59
  br i1 %cmp49, label %for.body51, label %for.end63

for.body51:                                       ; preds = %for.cond48
  %60 = load i32, i32* %gain, align 4
  %61 = load i32*, i32** %vbrsfmin.addr, align 4
  %62 = load i32, i32* %sfb, align 4
  %arrayidx52 = getelementptr inbounds i32, i32* %61, i32 %62
  %63 = load i32, i32* %arrayidx52, align 4
  %sub53 = sub nsw i32 %60, %63
  %64 = load i32, i32* %sfb, align 4
  %arrayidx54 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %64
  %65 = load i32, i32* %arrayidx54, align 4
  %mul55 = mul nsw i32 2, %65
  %sub56 = sub nsw i32 %sub53, %mul55
  store i32 %sub56, i32* %a, align 4
  %66 = load i32, i32* %a, align 4
  %cmp57 = icmp sle i32 %66, 0
  br i1 %cmp57, label %if.then59, label %if.end60

if.then59:                                        ; preds = %for.body51
  store i32 0, i32* %vm0p, align 4
  store i32 0, i32* %vm1p, align 4
  br label %for.end63

if.end60:                                         ; preds = %for.body51
  br label %for.inc61

for.inc61:                                        ; preds = %if.end60
  %67 = load i32, i32* %sfb, align 4
  %inc62 = add nsw i32 %67, 1
  store i32 %inc62, i32* %sfb, align 4
  br label %for.cond48

for.end63:                                        ; preds = %if.then59, %for.cond48
  br label %if.end64

if.end64:                                         ; preds = %for.end63, %for.end
  %68 = load i32, i32* %vm1p, align 4
  %cmp65 = icmp eq i32 %68, 1
  br i1 %cmp65, label %if.then67, label %if.end90

if.then67:                                        ; preds = %if.end64
  %69 = load i32, i32* %vbrmax.addr, align 4
  %70 = load i32, i32* %maxover1p, align 4
  %sub69 = sub nsw i32 %69, %70
  store i32 %sub69, i32* %gain68, align 4
  %71 = load i32, i32* %gain68, align 4
  %72 = load i32, i32* %maxminsfb, align 4
  %cmp70 = icmp slt i32 %71, %72
  br i1 %cmp70, label %if.then72, label %if.end73

if.then72:                                        ; preds = %if.then67
  %73 = load i32, i32* %maxminsfb, align 4
  store i32 %73, i32* %gain68, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.then72, %if.then67
  store i32 0, i32* %sfb, align 4
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc87, %if.end73
  %74 = load i32, i32* %sfb, align 4
  %75 = load i32, i32* %psymax, align 4
  %cmp75 = icmp slt i32 %74, %75
  br i1 %cmp75, label %for.body77, label %for.end89

for.body77:                                       ; preds = %for.cond74
  %76 = load i32, i32* %gain68, align 4
  %77 = load i32*, i32** %vbrsfmin.addr, align 4
  %78 = load i32, i32* %sfb, align 4
  %arrayidx78 = getelementptr inbounds i32, i32* %77, i32 %78
  %79 = load i32, i32* %arrayidx78, align 4
  %sub79 = sub nsw i32 %76, %79
  %80 = load i32, i32* %sfb, align 4
  %arrayidx80 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %80
  %81 = load i32, i32* %arrayidx80, align 4
  %mul81 = mul nsw i32 4, %81
  %sub82 = sub nsw i32 %sub79, %mul81
  store i32 %sub82, i32* %b, align 4
  %82 = load i32, i32* %b, align 4
  %cmp83 = icmp sle i32 %82, 0
  br i1 %cmp83, label %if.then85, label %if.end86

if.then85:                                        ; preds = %for.body77
  store i32 0, i32* %vm1p, align 4
  br label %for.end89

if.end86:                                         ; preds = %for.body77
  br label %for.inc87

for.inc87:                                        ; preds = %if.end86
  %83 = load i32, i32* %sfb, align 4
  %inc88 = add nsw i32 %83, 1
  store i32 %inc88, i32* %sfb, align 4
  br label %for.cond74

for.end89:                                        ; preds = %if.then85, %for.cond74
  br label %if.end90

if.end90:                                         ; preds = %for.end89, %if.end64
  %84 = load i32, i32* %vm0p, align 4
  %cmp91 = icmp eq i32 %84, 0
  br i1 %cmp91, label %if.then93, label %if.end94

if.then93:                                        ; preds = %if.end90
  %85 = load i32, i32* %maxover0, align 4
  store i32 %85, i32* %maxover0p, align 4
  br label %if.end94

if.end94:                                         ; preds = %if.then93, %if.end90
  %86 = load i32, i32* %vm1p, align 4
  %cmp95 = icmp eq i32 %86, 0
  br i1 %cmp95, label %if.then97, label %if.end98

if.then97:                                        ; preds = %if.end94
  %87 = load i32, i32* %maxover1, align 4
  store i32 %87, i32* %maxover1p, align 4
  br label %if.end98

if.end98:                                         ; preds = %if.then97, %if.end94
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 3
  %89 = load i32, i32* %noise_shaping, align 4
  %cmp99 = icmp ne i32 %89, 2
  br i1 %cmp99, label %if.then101, label %if.end102

if.then101:                                       ; preds = %if.end98
  %90 = load i32, i32* %maxover0, align 4
  store i32 %90, i32* %maxover1, align 4
  %91 = load i32, i32* %maxover0p, align 4
  store i32 %91, i32* %maxover1p, align 4
  br label %if.end102

if.end102:                                        ; preds = %if.then101, %if.end98
  %92 = load i32, i32* %maxover0, align 4
  %93 = load i32, i32* %maxover0p, align 4
  %cmp103 = icmp slt i32 %92, %93
  br i1 %cmp103, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end102
  %94 = load i32, i32* %maxover0, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end102
  %95 = load i32, i32* %maxover0p, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond105 = phi i32 [ %94, %cond.true ], [ %95, %cond.false ]
  store i32 %cond105, i32* %mover, align 4
  %96 = load i32, i32* %mover, align 4
  %97 = load i32, i32* %maxover1, align 4
  %cmp106 = icmp slt i32 %96, %97
  br i1 %cmp106, label %cond.true108, label %cond.false109

cond.true108:                                     ; preds = %cond.end
  %98 = load i32, i32* %mover, align 4
  br label %cond.end110

cond.false109:                                    ; preds = %cond.end
  %99 = load i32, i32* %maxover1, align 4
  br label %cond.end110

cond.end110:                                      ; preds = %cond.false109, %cond.true108
  %cond111 = phi i32 [ %98, %cond.true108 ], [ %99, %cond.false109 ]
  store i32 %cond111, i32* %mover, align 4
  %100 = load i32, i32* %mover, align 4
  %101 = load i32, i32* %maxover1p, align 4
  %cmp112 = icmp slt i32 %100, %101
  br i1 %cmp112, label %cond.true114, label %cond.false115

cond.true114:                                     ; preds = %cond.end110
  %102 = load i32, i32* %mover, align 4
  br label %cond.end116

cond.false115:                                    ; preds = %cond.end110
  %103 = load i32, i32* %maxover1p, align 4
  br label %cond.end116

cond.end116:                                      ; preds = %cond.false115, %cond.true114
  %cond117 = phi i32 [ %102, %cond.true114 ], [ %103, %cond.false115 ]
  store i32 %cond117, i32* %mover, align 4
  %104 = load i32, i32* %delta, align 4
  %105 = load i32, i32* %mover, align 4
  %cmp118 = icmp sgt i32 %104, %105
  br i1 %cmp118, label %if.then120, label %if.end121

if.then120:                                       ; preds = %cond.end116
  %106 = load i32, i32* %mover, align 4
  store i32 %106, i32* %delta, align 4
  br label %if.end121

if.end121:                                        ; preds = %if.then120, %cond.end116
  %107 = load i32, i32* %delta, align 4
  %108 = load i32, i32* %vbrmax.addr, align 4
  %sub122 = sub nsw i32 %108, %107
  store i32 %sub122, i32* %vbrmax.addr, align 4
  %109 = load i32, i32* %vbrmax.addr, align 4
  %110 = load i32, i32* %maxminsfb, align 4
  %cmp123 = icmp slt i32 %109, %110
  br i1 %cmp123, label %if.then125, label %if.end126

if.then125:                                       ; preds = %if.end121
  %111 = load i32, i32* %maxminsfb, align 4
  store i32 %111, i32* %vbrmax.addr, align 4
  br label %if.end126

if.end126:                                        ; preds = %if.then125, %if.end121
  %112 = load i32, i32* %mover, align 4
  %113 = load i32, i32* %maxover0, align 4
  %sub127 = sub nsw i32 %113, %112
  store i32 %sub127, i32* %maxover0, align 4
  %114 = load i32, i32* %mover, align 4
  %115 = load i32, i32* %maxover0p, align 4
  %sub128 = sub nsw i32 %115, %114
  store i32 %sub128, i32* %maxover0p, align 4
  %116 = load i32, i32* %mover, align 4
  %117 = load i32, i32* %maxover1, align 4
  %sub129 = sub nsw i32 %117, %116
  store i32 %sub129, i32* %maxover1, align 4
  %118 = load i32, i32* %mover, align 4
  %119 = load i32, i32* %maxover1p, align 4
  %sub130 = sub nsw i32 %119, %118
  store i32 %sub130, i32* %maxover1p, align 4
  %120 = load i32, i32* %maxover0, align 4
  %cmp131 = icmp eq i32 %120, 0
  br i1 %cmp131, label %if.then133, label %if.else

if.then133:                                       ; preds = %if.end126
  %121 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %121, i32 0, i32 16
  store i32 0, i32* %scalefac_scale, align 4
  %122 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %122, i32 0, i32 15
  store i32 0, i32* %preflag, align 4
  store i8* getelementptr inbounds ([22 x i8], [22 x i8]* @max_range_long, i32 0, i32 0), i8** %max_rangep, align 4
  br label %if.end155

if.else:                                          ; preds = %if.end126
  %123 = load i32, i32* %maxover0p, align 4
  %cmp134 = icmp eq i32 %123, 0
  br i1 %cmp134, label %if.then136, label %if.else139

if.then136:                                       ; preds = %if.else
  %124 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale137 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %124, i32 0, i32 16
  store i32 0, i32* %scalefac_scale137, align 4
  %125 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %preflag138 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %125, i32 0, i32 15
  store i32 1, i32* %preflag138, align 4
  br label %if.end154

if.else139:                                       ; preds = %if.else
  %126 = load i32, i32* %maxover1, align 4
  %cmp140 = icmp eq i32 %126, 0
  br i1 %cmp140, label %if.then142, label %if.else145

if.then142:                                       ; preds = %if.else139
  %127 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale143 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %127, i32 0, i32 16
  store i32 1, i32* %scalefac_scale143, align 4
  %128 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %preflag144 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %128, i32 0, i32 15
  store i32 0, i32* %preflag144, align 4
  store i8* getelementptr inbounds ([22 x i8], [22 x i8]* @max_range_long, i32 0, i32 0), i8** %max_rangep, align 4
  br label %if.end153

if.else145:                                       ; preds = %if.else139
  %129 = load i32, i32* %maxover1p, align 4
  %cmp146 = icmp eq i32 %129, 0
  br i1 %cmp146, label %if.then148, label %if.else151

if.then148:                                       ; preds = %if.else145
  %130 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale149 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %130, i32 0, i32 16
  store i32 1, i32* %scalefac_scale149, align 4
  %131 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %preflag150 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %131, i32 0, i32 15
  store i32 1, i32* %preflag150, align 4
  br label %if.end152

if.else151:                                       ; preds = %if.else145
  br label %if.end152

if.end152:                                        ; preds = %if.else151, %if.then148
  br label %if.end153

if.end153:                                        ; preds = %if.end152, %if.then142
  br label %if.end154

if.end154:                                        ; preds = %if.end153, %if.then136
  br label %if.end155

if.end155:                                        ; preds = %if.end154, %if.then133
  %132 = load i32, i32* %vbrmax.addr, align 4
  %133 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %133, i32 0, i32 7
  store i32 %132, i32* %global_gain, align 4
  %134 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain156 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %134, i32 0, i32 7
  %135 = load i32, i32* %global_gain156, align 4
  %cmp157 = icmp slt i32 %135, 0
  br i1 %cmp157, label %if.then159, label %if.else161

if.then159:                                       ; preds = %if.end155
  %136 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain160 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %136, i32 0, i32 7
  store i32 0, i32* %global_gain160, align 4
  br label %if.end168

if.else161:                                       ; preds = %if.end155
  %137 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain162 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %137, i32 0, i32 7
  %138 = load i32, i32* %global_gain162, align 4
  %cmp163 = icmp sgt i32 %138, 255
  br i1 %cmp163, label %if.then165, label %if.end167

if.then165:                                       ; preds = %if.else161
  %139 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain166 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %139, i32 0, i32 7
  store i32 255, i32* %global_gain166, align 4
  br label %if.end167

if.end167:                                        ; preds = %if.then165, %if.else161
  br label %if.end168

if.end168:                                        ; preds = %if.end167, %if.then159
  store i32 0, i32* %sfb, align 4
  br label %for.cond169

for.cond169:                                      ; preds = %for.inc176, %if.end168
  %140 = load i32, i32* %sfb, align 4
  %cmp170 = icmp slt i32 %140, 39
  br i1 %cmp170, label %for.body172, label %for.end178

for.body172:                                      ; preds = %for.cond169
  %141 = load i32*, i32** %vbrsf.addr, align 4
  %142 = load i32, i32* %sfb, align 4
  %arrayidx173 = getelementptr inbounds i32, i32* %141, i32 %142
  %143 = load i32, i32* %arrayidx173, align 4
  %144 = load i32, i32* %vbrmax.addr, align 4
  %sub174 = sub nsw i32 %143, %144
  %145 = load i32, i32* %sfb, align 4
  %arrayidx175 = getelementptr inbounds [39 x i32], [39 x i32]* %sf_temp, i32 0, i32 %145
  store i32 %sub174, i32* %arrayidx175, align 4
  br label %for.inc176

for.inc176:                                       ; preds = %for.body172
  %146 = load i32, i32* %sfb, align 4
  %inc177 = add nsw i32 %146, 1
  store i32 %inc177, i32* %sfb, align 4
  br label %for.cond169

for.end178:                                       ; preds = %for.cond169
  %147 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %148 = load i32*, i32** %vbrsfmin.addr, align 4
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %sf_temp, i32 0, i32 0
  %149 = load i8*, i8** %max_rangep, align 4
  call void @set_scalefacs(%struct.gr_info* %147, i32* %148, i32* %arraydecay, i8* %149)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @block_sf(%struct.algo_s* %that, float* %l3_xmin, i32* %vbrsf, i32* %vbrsfmin) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %l3_xmin.addr = alloca float*, align 4
  %vbrsf.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %max_xr34 = alloca float, align 4
  %xr = alloca float*, align 4
  %xr34_orig = alloca float*, align 4
  %width = alloca i32*, align 4
  %energy_above_cutoff = alloca i8*, align 4
  %max_nonzero_coeff = alloca i32, align 4
  %maxsf = alloca i8, align 1
  %sfb = alloca i32, align 4
  %m_o = alloca i32, align 4
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %psymax = alloca i32, align 4
  %w = alloca i32, align 4
  %m = alloca i32, align 4
  %l = alloca i32, align 4
  %m1 = alloca i8, align 1
  %m2 = alloca i8, align 1
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store float* %l3_xmin, float** %l3_xmin.addr, align 4
  store i32* %vbrsf, i32** %vbrsf.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %xr1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [576 x float], [576 x float]* %xr1, i32 0, i32 0
  store float* %arrayidx, float** %xr, align 4
  %2 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %xr34orig = getelementptr inbounds %struct.algo_s, %struct.algo_s* %2, i32 0, i32 2
  %3 = load float*, float** %xr34orig, align 4
  %arrayidx2 = getelementptr inbounds float, float* %3, i32 0
  store float* %arrayidx2, float** %xr34_orig, align 4
  %4 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info3 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %4, i32 0, i32 4
  %5 = load %struct.gr_info*, %struct.gr_info** %cod_info3, align 4
  %width4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 25
  %arrayidx5 = getelementptr inbounds [39 x i32], [39 x i32]* %width4, i32 0, i32 0
  store i32* %arrayidx5, i32** %width, align 4
  %6 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info6 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %6, i32 0, i32 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info6, align 4
  %energy_above_cutoff7 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 31
  %arrayidx8 = getelementptr inbounds [39 x i8], [39 x i8]* %energy_above_cutoff7, i32 0, i32 0
  store i8* %arrayidx8, i8** %energy_above_cutoff, align 4
  %8 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info9 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %8, i32 0, i32 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info9, align 4
  %max_nonzero_coeff10 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 30
  %10 = load i32, i32* %max_nonzero_coeff10, align 4
  store i32 %10, i32* %max_nonzero_coeff, align 4
  store i8 0, i8* %maxsf, align 1
  store i32 0, i32* %sfb, align 4
  store i32 -1, i32* %m_o, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %i, align 4
  %11 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info11 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %11, i32 0, i32 4
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info11, align 4
  %psymax12 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 23
  %13 = load i32, i32* %psymax12, align 4
  store i32 %13, i32* %psymax, align 4
  %14 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_l = getelementptr inbounds %struct.algo_s, %struct.algo_s* %14, i32 0, i32 5
  store i32 0, i32* %mingain_l, align 4
  %15 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s = getelementptr inbounds %struct.algo_s, %struct.algo_s* %15, i32 0, i32 6
  %arrayidx13 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s, i32 0, i32 0
  store i32 0, i32* %arrayidx13, align 4
  %16 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s14 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %16, i32 0, i32 6
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s14, i32 0, i32 1
  store i32 0, i32* %arrayidx15, align 4
  %17 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s16 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %17, i32 0, i32 6
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s16, i32 0, i32 2
  store i32 0, i32* %arrayidx17, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end80, %entry
  %18 = load i32, i32* %j, align 4
  %19 = load i32, i32* %max_nonzero_coeff, align 4
  %cmp = icmp ule i32 %18, %19
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load i32*, i32** %width, align 4
  %21 = load i32, i32* %sfb, align 4
  %arrayidx18 = getelementptr inbounds i32, i32* %20, i32 %21
  %22 = load i32, i32* %arrayidx18, align 4
  store i32 %22, i32* %w, align 4
  %23 = load i32, i32* %max_nonzero_coeff, align 4
  %24 = load i32, i32* %j, align 4
  %sub = sub i32 %23, %24
  %add = add i32 %sub, 1
  store i32 %add, i32* %m, align 4
  %25 = load i32, i32* %w, align 4
  store i32 %25, i32* %l, align 4
  %26 = load i32, i32* %l, align 4
  %27 = load i32, i32* %m, align 4
  %cmp19 = icmp ugt i32 %26, %27
  br i1 %cmp19, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %28 = load i32, i32* %m, align 4
  store i32 %28, i32* %l, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %29 = load float*, float** %xr34_orig, align 4
  %30 = load i32, i32* %j, align 4
  %arrayidx20 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load i32, i32* %l, align 4
  %call = call float @vec_max_c(float* %arrayidx20, i32 %31)
  store float %call, float* %max_xr34, align 4
  %32 = load float, float* %max_xr34, align 4
  %call21 = call zeroext i8 @find_lowest_scalefac(float %32)
  store i8 %call21, i8* %m1, align 1
  %33 = load i8, i8* %m1, align 1
  %conv = zext i8 %33 to i32
  %34 = load i32*, i32** %vbrsfmin.addr, align 4
  %35 = load i32, i32* %sfb, align 4
  %arrayidx22 = getelementptr inbounds i32, i32* %34, i32 %35
  store i32 %conv, i32* %arrayidx22, align 4
  %36 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_l23 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %36, i32 0, i32 5
  %37 = load i32, i32* %mingain_l23, align 4
  %38 = load i8, i8* %m1, align 1
  %conv24 = zext i8 %38 to i32
  %cmp25 = icmp slt i32 %37, %conv24
  br i1 %cmp25, label %if.then27, label %if.end30

if.then27:                                        ; preds = %if.end
  %39 = load i8, i8* %m1, align 1
  %conv28 = zext i8 %39 to i32
  %40 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_l29 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %40, i32 0, i32 5
  store i32 %conv28, i32* %mingain_l29, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then27, %if.end
  %41 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s31 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %41, i32 0, i32 6
  %42 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s31, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx32, align 4
  %44 = load i8, i8* %m1, align 1
  %conv33 = zext i8 %44 to i32
  %cmp34 = icmp slt i32 %43, %conv33
  br i1 %cmp34, label %if.then36, label %if.end40

if.then36:                                        ; preds = %if.end30
  %45 = load i8, i8* %m1, align 1
  %conv37 = zext i8 %45 to i32
  %46 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %mingain_s38 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %46, i32 0, i32 6
  %47 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr inbounds [3 x i32], [3 x i32]* %mingain_s38, i32 0, i32 %47
  store i32 %conv37, i32* %arrayidx39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then36, %if.end30
  %48 = load i32, i32* %i, align 4
  %inc = add i32 %48, 1
  store i32 %inc, i32* %i, align 4
  %cmp41 = icmp ugt i32 %inc, 2
  br i1 %cmp41, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.end40
  store i32 0, i32* %i, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then43, %if.end40
  %49 = load i32, i32* %sfb, align 4
  %50 = load i32, i32* %psymax, align 4
  %cmp45 = icmp slt i32 %49, %50
  br i1 %cmp45, label %land.lhs.true, label %if.else73

land.lhs.true:                                    ; preds = %if.end44
  %51 = load i32, i32* %w, align 4
  %cmp47 = icmp ugt i32 %51, 2
  br i1 %cmp47, label %if.then49, label %if.else73

if.then49:                                        ; preds = %land.lhs.true
  %52 = load i8*, i8** %energy_above_cutoff, align 4
  %53 = load i32, i32* %sfb, align 4
  %arrayidx50 = getelementptr inbounds i8, i8* %52, i32 %53
  %54 = load i8, i8* %arrayidx50, align 1
  %tobool = icmp ne i8 %54, 0
  br i1 %tobool, label %if.then51, label %if.else

if.then51:                                        ; preds = %if.then49
  %55 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %find = getelementptr inbounds %struct.algo_s, %struct.algo_s* %55, i32 0, i32 1
  %56 = load i8 (float*, float*, float, i32, i8)*, i8 (float*, float*, float, i32, i8)** %find, align 4
  %57 = load float*, float** %xr, align 4
  %58 = load i32, i32* %j, align 4
  %arrayidx52 = getelementptr inbounds float, float* %57, i32 %58
  %59 = load float*, float** %xr34_orig, align 4
  %60 = load i32, i32* %j, align 4
  %arrayidx53 = getelementptr inbounds float, float* %59, i32 %60
  %61 = load float*, float** %l3_xmin.addr, align 4
  %62 = load i32, i32* %sfb, align 4
  %arrayidx54 = getelementptr inbounds float, float* %61, i32 %62
  %63 = load float, float* %arrayidx54, align 4
  %64 = load i32, i32* %l, align 4
  %65 = load i8, i8* %m1, align 1
  %call55 = call zeroext i8 %56(float* %arrayidx52, float* %arrayidx53, float %63, i32 %64, i8 zeroext %65)
  store i8 %call55, i8* %m2, align 1
  %66 = load i8, i8* %maxsf, align 1
  %conv56 = zext i8 %66 to i32
  %67 = load i8, i8* %m2, align 1
  %conv57 = zext i8 %67 to i32
  %cmp58 = icmp slt i32 %conv56, %conv57
  br i1 %cmp58, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.then51
  %68 = load i8, i8* %m2, align 1
  store i8 %68, i8* %maxsf, align 1
  br label %if.end61

if.end61:                                         ; preds = %if.then60, %if.then51
  %69 = load i32, i32* %m_o, align 4
  %70 = load i8, i8* %m2, align 1
  %conv62 = zext i8 %70 to i32
  %cmp63 = icmp slt i32 %69, %conv62
  br i1 %cmp63, label %land.lhs.true65, label %if.end71

land.lhs.true65:                                  ; preds = %if.end61
  %71 = load i8, i8* %m2, align 1
  %conv66 = zext i8 %71 to i32
  %cmp67 = icmp slt i32 %conv66, 255
  br i1 %cmp67, label %if.then69, label %if.end71

if.then69:                                        ; preds = %land.lhs.true65
  %72 = load i8, i8* %m2, align 1
  %conv70 = zext i8 %72 to i32
  store i32 %conv70, i32* %m_o, align 4
  br label %if.end71

if.end71:                                         ; preds = %if.then69, %land.lhs.true65, %if.end61
  br label %if.end72

if.else:                                          ; preds = %if.then49
  store i8 -1, i8* %m2, align 1
  store i8 -1, i8* %maxsf, align 1
  br label %if.end72

if.end72:                                         ; preds = %if.else, %if.end71
  br label %if.end80

if.else73:                                        ; preds = %land.lhs.true, %if.end44
  %73 = load i8, i8* %maxsf, align 1
  %conv74 = zext i8 %73 to i32
  %74 = load i8, i8* %m1, align 1
  %conv75 = zext i8 %74 to i32
  %cmp76 = icmp slt i32 %conv74, %conv75
  br i1 %cmp76, label %if.then78, label %if.end79

if.then78:                                        ; preds = %if.else73
  %75 = load i8, i8* %m1, align 1
  store i8 %75, i8* %maxsf, align 1
  br label %if.end79

if.end79:                                         ; preds = %if.then78, %if.else73
  %76 = load i8, i8* %maxsf, align 1
  store i8 %76, i8* %m2, align 1
  br label %if.end80

if.end80:                                         ; preds = %if.end79, %if.end72
  %77 = load i8, i8* %m2, align 1
  %conv81 = zext i8 %77 to i32
  %78 = load i32*, i32** %vbrsf.addr, align 4
  %79 = load i32, i32* %sfb, align 4
  %arrayidx82 = getelementptr inbounds i32, i32* %78, i32 %79
  store i32 %conv81, i32* %arrayidx82, align 4
  %80 = load i32, i32* %sfb, align 4
  %inc83 = add nsw i32 %80, 1
  store i32 %inc83, i32* %sfb, align 4
  %81 = load i32, i32* %w, align 4
  %82 = load i32, i32* %j, align 4
  %add84 = add i32 %82, %81
  store i32 %add84, i32* %j, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.end
  %83 = load i32, i32* %sfb, align 4
  %cmp85 = icmp slt i32 %83, 39
  br i1 %cmp85, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %84 = load i8, i8* %maxsf, align 1
  %conv87 = zext i8 %84 to i32
  %85 = load i32*, i32** %vbrsf.addr, align 4
  %86 = load i32, i32* %sfb, align 4
  %arrayidx88 = getelementptr inbounds i32, i32* %85, i32 %86
  store i32 %conv87, i32* %arrayidx88, align 4
  %87 = load i32*, i32** %vbrsfmin.addr, align 4
  %88 = load i32, i32* %sfb, align 4
  %arrayidx89 = getelementptr inbounds i32, i32* %87, i32 %88
  store i32 0, i32* %arrayidx89, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %89 = load i32, i32* %sfb, align 4
  %inc90 = add nsw i32 %89, 1
  store i32 %inc90, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %90 = load i32, i32* %m_o, align 4
  %cmp91 = icmp sgt i32 %90, -1
  br i1 %cmp91, label %if.then93, label %if.end108

if.then93:                                        ; preds = %for.end
  %91 = load i32, i32* %m_o, align 4
  %conv94 = trunc i32 %91 to i8
  store i8 %conv94, i8* %maxsf, align 1
  store i32 0, i32* %sfb, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc105, %if.then93
  %92 = load i32, i32* %sfb, align 4
  %cmp96 = icmp slt i32 %92, 39
  br i1 %cmp96, label %for.body98, label %for.end107

for.body98:                                       ; preds = %for.cond95
  %93 = load i32*, i32** %vbrsf.addr, align 4
  %94 = load i32, i32* %sfb, align 4
  %arrayidx99 = getelementptr inbounds i32, i32* %93, i32 %94
  %95 = load i32, i32* %arrayidx99, align 4
  %cmp100 = icmp eq i32 %95, 255
  br i1 %cmp100, label %if.then102, label %if.end104

if.then102:                                       ; preds = %for.body98
  %96 = load i32, i32* %m_o, align 4
  %97 = load i32*, i32** %vbrsf.addr, align 4
  %98 = load i32, i32* %sfb, align 4
  %arrayidx103 = getelementptr inbounds i32, i32* %97, i32 %98
  store i32 %96, i32* %arrayidx103, align 4
  br label %if.end104

if.end104:                                        ; preds = %if.then102, %for.body98
  br label %for.inc105

for.inc105:                                       ; preds = %if.end104
  %99 = load i32, i32* %sfb, align 4
  %inc106 = add nsw i32 %99, 1
  store i32 %inc106, i32* %sfb, align 4
  br label %for.cond95

for.end107:                                       ; preds = %for.cond95
  br label %if.end108

if.end108:                                        ; preds = %for.end107, %for.end
  %100 = load i8, i8* %maxsf, align 1
  %conv109 = zext i8 %100 to i32
  ret i32 %conv109
}

; Function Attrs: noinline nounwind optnone
define internal void @bitcount(%struct.algo_s* %that) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %rc = alloca i32, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %gfc = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 3
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %2, i32 0, i32 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %call = call i32 @scale_bitcount(%struct.lame_internal_flags* %1, %struct.gr_info* %3)
  store i32 %call, i32* %rc, align 4
  %4 = load i32, i32* %rc, align 4
  %cmp = icmp eq i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  ret void

if.end:                                           ; preds = %entry
  %5 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %gfc1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %5, i32 0, i32 3
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc1, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %6, i8* getelementptr inbounds ([62 x i8], [62 x i8]* @.str.2, i32 0, i32 0))
  call void @exit(i32 -1) #6
  unreachable
}

; Function Attrs: noinline nounwind optnone
define internal i32 @quantizeAndCountBits(%struct.algo_s* %that) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  call void @quantize_x34(%struct.algo_s* %0)
  %1 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %gfc = getelementptr inbounds %struct.algo_s, %struct.algo_s* %1, i32 0, i32 3
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %3, i32 0, i32 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %call = call i32 @noquant_count_bits(%struct.lame_internal_flags* %2, %struct.gr_info* %4, %struct.calc_noise_data_t* null)
  %5 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %5, i32 0, i32 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info1, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 4
  store i32 %call, i32* %part2_3_length, align 4
  %7 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info2 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %7, i32 0, i32 4
  %8 = load %struct.gr_info*, %struct.gr_info** %cod_info2, align 4
  %part2_3_length3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 4
  %9 = load i32, i32* %part2_3_length3, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i32 @reduce_bit_usage(%struct.lame_internal_flags* %gfc, i32 %gr, i32 %ch) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gr.addr = alloca i32, align 4
  %ch.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %2 = load i32, i32* %gr.addr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %2
  %3 = load i32, i32* %ch.addr, align 4
  %arrayidx2 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %3
  store %struct.gr_info* %arrayidx2, %struct.gr_info** %cod_info, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %5 = load i32, i32* %gr.addr, align 4
  %6 = load i32, i32* %ch.addr, align 4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 7
  call void @best_scalefac_store(%struct.lame_internal_flags* %4, i32 %5, i32 %6, %struct.III_side_info_t* %l3_side3)
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 5
  %9 = load i32, i32* %use_best_huffman, align 4
  %cmp = icmp eq i32 %9, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @best_huffman_divide(%struct.lame_internal_flags* %10, %struct.gr_info* %11)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 4
  %13 = load i32, i32* %part2_3_length, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 18
  %15 = load i32, i32* %part2_length, align 4
  %add = add nsw i32 %13, %15
  ret i32 %add
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #3

; Function Attrs: noinline nounwind optnone
define internal void @cutDistribution(i32* %sfwork, i32* %sf_out, i32 %cut) #0 {
entry:
  %sfwork.addr = alloca i32*, align 4
  %sf_out.addr = alloca i32*, align 4
  %cut.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca i32, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32* %sf_out, i32** %sf_out.addr, align 4
  store i32 %cut, i32* %cut.addr, align 4
  store i32 39, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %j, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32*, i32** %sfwork.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  store i32 %3, i32* %x, align 4
  %4 = load i32, i32* %x, align 4
  %5 = load i32, i32* %cut.addr, align 4
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %6 = load i32, i32* %x, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %7 = load i32, i32* %cut.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %6, %cond.true ], [ %7, %cond.false ]
  %8 = load i32*, i32** %sf_out.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 %cond, i32* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %10 = load i32, i32* %j, align 4
  %dec = add i32 %10, -1
  store i32 %dec, i32* %j, align 4
  %11 = load i32, i32* %i, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @outOfBitsStrategy(%struct.algo_s* %that, i32* %sfwork, i32* %vbrsfmin, i32 %target) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %sfwork.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %target.addr = alloca i32, align 4
  %wrk = alloca [39 x i32], align 16
  %dm = alloca i32, align 4
  %p = alloca i32, align 4
  %nbits = alloca i32, align 4
  %bi = alloca i32, align 4
  %bi_ok = alloca i32, align 4
  %bu = alloca i32, align 4
  %bo = alloca i32, align 4
  %sfmax = alloca i32, align 4
  %sfmax14 = alloca i32, align 4
  %bi21 = alloca i32, align 4
  %bi_ok24 = alloca i32, align 4
  %bu25 = alloca i32, align 4
  %bo26 = alloca i32, align 4
  %sfmax28 = alloca i32, align 4
  %sfmax50 = alloca i32, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %target, i32* %target.addr, align 4
  %0 = load i32*, i32** %sfwork.addr, align 4
  %call = call i32 @sfDepth(i32* %0)
  store i32 %call, i32* %dm, align 4
  %1 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %1, i32 0, i32 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 7
  %3 = load i32, i32* %global_gain, align 4
  store i32 %3, i32* %p, align 4
  %4 = load i32, i32* %dm, align 4
  %div = sdiv i32 %4, 2
  store i32 %div, i32* %bi, align 4
  store i32 -1, i32* %bi_ok, align 4
  store i32 0, i32* %bu, align 4
  %5 = load i32, i32* %dm, align 4
  store i32 %5, i32* %bo, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end9, %entry
  %6 = load i32*, i32** %sfwork.addr, align 4
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %7 = load i32, i32* %dm, align 4
  %8 = load i32, i32* %bi, align 4
  %9 = load i32, i32* %p, align 4
  %call1 = call i32 @flattenDistribution(i32* %6, i32* %arraydecay, i32 %7, i32 %8, i32 %9)
  store i32 %call1, i32* %sfmax, align 4
  %10 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay2 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %11 = load i32*, i32** %vbrsfmin.addr, align 4
  %12 = load i32, i32* %sfmax, align 4
  %call3 = call i32 @tryThatOne(%struct.algo_s* %10, i32* %arraydecay2, i32* %11, i32 %12)
  store i32 %call3, i32* %nbits, align 4
  %13 = load i32, i32* %nbits, align 4
  %14 = load i32, i32* %target.addr, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %for.cond
  %15 = load i32, i32* %bi, align 4
  store i32 %15, i32* %bi_ok, align 4
  %16 = load i32, i32* %bi, align 4
  %sub = sub nsw i32 %16, 1
  store i32 %sub, i32* %bo, align 4
  br label %if.end

if.else:                                          ; preds = %for.cond
  %17 = load i32, i32* %bi, align 4
  %add = add nsw i32 %17, 1
  store i32 %add, i32* %bu, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %18 = load i32, i32* %bu, align 4
  %19 = load i32, i32* %bo, align 4
  %cmp4 = icmp sle i32 %18, %19
  br i1 %cmp4, label %if.then5, label %if.else8

if.then5:                                         ; preds = %if.end
  %20 = load i32, i32* %bu, align 4
  %21 = load i32, i32* %bo, align 4
  %add6 = add nsw i32 %20, %21
  %div7 = sdiv i32 %add6, 2
  store i32 %div7, i32* %bi, align 4
  br label %if.end9

if.else8:                                         ; preds = %if.end
  br label %for.end

if.end9:                                          ; preds = %if.then5
  br label %for.cond

for.end:                                          ; preds = %if.else8
  %22 = load i32, i32* %bi_ok, align 4
  %cmp10 = icmp sge i32 %22, 0
  br i1 %cmp10, label %if.then11, label %if.end20

if.then11:                                        ; preds = %for.end
  %23 = load i32, i32* %bi, align 4
  %24 = load i32, i32* %bi_ok, align 4
  %cmp12 = icmp ne i32 %23, %24
  br i1 %cmp12, label %if.then13, label %if.end19

if.then13:                                        ; preds = %if.then11
  %25 = load i32*, i32** %sfwork.addr, align 4
  %arraydecay15 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %26 = load i32, i32* %dm, align 4
  %27 = load i32, i32* %bi_ok, align 4
  %28 = load i32, i32* %p, align 4
  %call16 = call i32 @flattenDistribution(i32* %25, i32* %arraydecay15, i32 %26, i32 %27, i32 %28)
  store i32 %call16, i32* %sfmax14, align 4
  %29 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay17 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %30 = load i32*, i32** %vbrsfmin.addr, align 4
  %31 = load i32, i32* %sfmax14, align 4
  %call18 = call i32 @tryThatOne(%struct.algo_s* %29, i32* %arraydecay17, i32* %30, i32 %31)
  store i32 %call18, i32* %nbits, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then13, %if.then11
  br label %return

if.end20:                                         ; preds = %for.end
  %32 = load i32, i32* %p, align 4
  %add22 = add nsw i32 255, %32
  %div23 = sdiv i32 %add22, 2
  store i32 %div23, i32* %bi21, align 4
  store i32 -1, i32* %bi_ok24, align 4
  %33 = load i32, i32* %p, align 4
  store i32 %33, i32* %bu25, align 4
  store i32 255, i32* %bo26, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %if.end44, %if.end20
  %34 = load i32*, i32** %sfwork.addr, align 4
  %arraydecay29 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %35 = load i32, i32* %dm, align 4
  %36 = load i32, i32* %dm, align 4
  %37 = load i32, i32* %bi21, align 4
  %call30 = call i32 @flattenDistribution(i32* %34, i32* %arraydecay29, i32 %35, i32 %36, i32 %37)
  store i32 %call30, i32* %sfmax28, align 4
  %38 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay31 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %39 = load i32*, i32** %vbrsfmin.addr, align 4
  %40 = load i32, i32* %sfmax28, align 4
  %call32 = call i32 @tryThatOne(%struct.algo_s* %38, i32* %arraydecay31, i32* %39, i32 %40)
  store i32 %call32, i32* %nbits, align 4
  %41 = load i32, i32* %nbits, align 4
  %42 = load i32, i32* %target.addr, align 4
  %cmp33 = icmp sle i32 %41, %42
  br i1 %cmp33, label %if.then34, label %if.else36

if.then34:                                        ; preds = %for.cond27
  %43 = load i32, i32* %bi21, align 4
  store i32 %43, i32* %bi_ok24, align 4
  %44 = load i32, i32* %bi21, align 4
  %sub35 = sub nsw i32 %44, 1
  store i32 %sub35, i32* %bo26, align 4
  br label %if.end38

if.else36:                                        ; preds = %for.cond27
  %45 = load i32, i32* %bi21, align 4
  %add37 = add nsw i32 %45, 1
  store i32 %add37, i32* %bu25, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.else36, %if.then34
  %46 = load i32, i32* %bu25, align 4
  %47 = load i32, i32* %bo26, align 4
  %cmp39 = icmp sle i32 %46, %47
  br i1 %cmp39, label %if.then40, label %if.else43

if.then40:                                        ; preds = %if.end38
  %48 = load i32, i32* %bu25, align 4
  %49 = load i32, i32* %bo26, align 4
  %add41 = add nsw i32 %48, %49
  %div42 = sdiv i32 %add41, 2
  store i32 %div42, i32* %bi21, align 4
  br label %if.end44

if.else43:                                        ; preds = %if.end38
  br label %for.end45

if.end44:                                         ; preds = %if.then40
  br label %for.cond27

for.end45:                                        ; preds = %if.else43
  %50 = load i32, i32* %bi_ok24, align 4
  %cmp46 = icmp sge i32 %50, 0
  br i1 %cmp46, label %if.then47, label %if.end56

if.then47:                                        ; preds = %for.end45
  %51 = load i32, i32* %bi21, align 4
  %52 = load i32, i32* %bi_ok24, align 4
  %cmp48 = icmp ne i32 %51, %52
  br i1 %cmp48, label %if.then49, label %if.end55

if.then49:                                        ; preds = %if.then47
  %53 = load i32*, i32** %sfwork.addr, align 4
  %arraydecay51 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %54 = load i32, i32* %dm, align 4
  %55 = load i32, i32* %dm, align 4
  %56 = load i32, i32* %bi_ok24, align 4
  %call52 = call i32 @flattenDistribution(i32* %53, i32* %arraydecay51, i32 %54, i32 %55, i32 %56)
  store i32 %call52, i32* %sfmax50, align 4
  %57 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay53 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %58 = load i32*, i32** %vbrsfmin.addr, align 4
  %59 = load i32, i32* %sfmax50, align 4
  %call54 = call i32 @tryThatOne(%struct.algo_s* %57, i32* %arraydecay53, i32* %58, i32 %59)
  store i32 %call54, i32* %nbits, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then49, %if.then47
  br label %return

if.end56:                                         ; preds = %for.end45
  %60 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay57 = getelementptr inbounds [39 x i32], [39 x i32]* %wrk, i32 0, i32 0
  %61 = load i32*, i32** %vbrsfmin.addr, align 4
  %62 = load i32, i32* %target.addr, align 4
  call void @searchGlobalStepsizeMax(%struct.algo_s* %60, i32* %arraydecay57, i32* %61, i32 %62)
  br label %return

return:                                           ; preds = %if.end56, %if.end55, %if.end19
  ret void
}

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #4

; Function Attrs: noreturn
declare void @exit(i32) #5

; Function Attrs: noinline nounwind optnone
define internal i32 @calc_scalefac(float %l3_xmin, i32 %bw) #0 {
entry:
  %l3_xmin.addr = alloca float, align 4
  %bw.addr = alloca i32, align 4
  %c = alloca float, align 4
  store float %l3_xmin, float* %l3_xmin.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store float 0x4017325260000000, float* %c, align 4
  %0 = load float, float* %l3_xmin.addr, align 4
  %1 = load i32, i32* %bw.addr, align 4
  %conv = sitofp i32 %1 to float
  %div = fdiv float %0, %conv
  %2 = call float @llvm.log10.f32(float %div)
  %mul = fmul float 0x4017325260000000, %2
  %sub = fsub float %mul, 5.000000e-01
  %conv1 = fptosi float %sub to i32
  %add = add nsw i32 210, %conv1
  ret i32 %add
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.log10.f32(float) #3

; Function Attrs: noinline nounwind optnone
define internal zeroext i8 @tri_calc_sfb_noise_x34(float* %xr, float* %xr34, float %l3_xmin, i32 %bw, i8 zeroext %sf, %struct.calc_noise_cache* %did_it) #0 {
entry:
  %retval = alloca i8, align 1
  %xr.addr = alloca float*, align 4
  %xr34.addr = alloca float*, align 4
  %l3_xmin.addr = alloca float, align 4
  %bw.addr = alloca i32, align 4
  %sf.addr = alloca i8, align 1
  %did_it.addr = alloca %struct.calc_noise_cache*, align 4
  %sf_x = alloca i8, align 1
  %sf_x43 = alloca i8, align 1
  store float* %xr, float** %xr.addr, align 4
  store float* %xr34, float** %xr34.addr, align 4
  store float %l3_xmin, float* %l3_xmin.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store i8 %sf, i8* %sf.addr, align 1
  store %struct.calc_noise_cache* %did_it, %struct.calc_noise_cache** %did_it.addr, align 4
  %0 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %1 = load i8, i8* %sf.addr, align 1
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %0, i32 %idxprom
  %valid = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx, i32 0, i32 0
  %2 = load i32, i32* %valid, align 4
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %4 = load i8, i8* %sf.addr, align 1
  %idxprom1 = zext i8 %4 to i32
  %arrayidx2 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %3, i32 %idxprom1
  %valid3 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx2, i32 0, i32 0
  store i32 1, i32* %valid3, align 4
  %5 = load float*, float** %xr.addr, align 4
  %6 = load float*, float** %xr34.addr, align 4
  %7 = load i32, i32* %bw.addr, align 4
  %8 = load i8, i8* %sf.addr, align 1
  %call = call float @calc_sfb_noise_x34(float* %5, float* %6, i32 %7, i8 zeroext %8)
  %9 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %10 = load i8, i8* %sf.addr, align 1
  %idxprom4 = zext i8 %10 to i32
  %arrayidx5 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %9, i32 %idxprom4
  %value = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx5, i32 0, i32 1
  store float %call, float* %value, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load float, float* %l3_xmin.addr, align 4
  %12 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %13 = load i8, i8* %sf.addr, align 1
  %idxprom6 = zext i8 %13 to i32
  %arrayidx7 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %12, i32 %idxprom6
  %value8 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx7, i32 0, i32 1
  %14 = load float, float* %value8, align 4
  %cmp9 = fcmp olt float %11, %14
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end
  store i8 1, i8* %retval, align 1
  br label %return

if.end11:                                         ; preds = %if.end
  %15 = load i8, i8* %sf.addr, align 1
  %conv = zext i8 %15 to i32
  %cmp12 = icmp slt i32 %conv, 255
  br i1 %cmp12, label %if.then14, label %if.end38

if.then14:                                        ; preds = %if.end11
  %16 = load i8, i8* %sf.addr, align 1
  %conv15 = zext i8 %16 to i32
  %add = add nsw i32 %conv15, 1
  %conv16 = trunc i32 %add to i8
  store i8 %conv16, i8* %sf_x, align 1
  %17 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %18 = load i8, i8* %sf_x, align 1
  %idxprom17 = zext i8 %18 to i32
  %arrayidx18 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %17, i32 %idxprom17
  %valid19 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx18, i32 0, i32 0
  %19 = load i32, i32* %valid19, align 4
  %cmp20 = icmp eq i32 %19, 0
  br i1 %cmp20, label %if.then22, label %if.end30

if.then22:                                        ; preds = %if.then14
  %20 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %21 = load i8, i8* %sf_x, align 1
  %idxprom23 = zext i8 %21 to i32
  %arrayidx24 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %20, i32 %idxprom23
  %valid25 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx24, i32 0, i32 0
  store i32 1, i32* %valid25, align 4
  %22 = load float*, float** %xr.addr, align 4
  %23 = load float*, float** %xr34.addr, align 4
  %24 = load i32, i32* %bw.addr, align 4
  %25 = load i8, i8* %sf_x, align 1
  %call26 = call float @calc_sfb_noise_x34(float* %22, float* %23, i32 %24, i8 zeroext %25)
  %26 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %27 = load i8, i8* %sf_x, align 1
  %idxprom27 = zext i8 %27 to i32
  %arrayidx28 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %26, i32 %idxprom27
  %value29 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx28, i32 0, i32 1
  store float %call26, float* %value29, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then22, %if.then14
  %28 = load float, float* %l3_xmin.addr, align 4
  %29 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %30 = load i8, i8* %sf_x, align 1
  %idxprom31 = zext i8 %30 to i32
  %arrayidx32 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %29, i32 %idxprom31
  %value33 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx32, i32 0, i32 1
  %31 = load float, float* %value33, align 4
  %cmp34 = fcmp olt float %28, %31
  br i1 %cmp34, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.end30
  store i8 1, i8* %retval, align 1
  br label %return

if.end37:                                         ; preds = %if.end30
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.end11
  %32 = load i8, i8* %sf.addr, align 1
  %conv39 = zext i8 %32 to i32
  %cmp40 = icmp sgt i32 %conv39, 0
  br i1 %cmp40, label %if.then42, label %if.end67

if.then42:                                        ; preds = %if.end38
  %33 = load i8, i8* %sf.addr, align 1
  %conv44 = zext i8 %33 to i32
  %sub = sub nsw i32 %conv44, 1
  %conv45 = trunc i32 %sub to i8
  store i8 %conv45, i8* %sf_x43, align 1
  %34 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %35 = load i8, i8* %sf_x43, align 1
  %idxprom46 = zext i8 %35 to i32
  %arrayidx47 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %34, i32 %idxprom46
  %valid48 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx47, i32 0, i32 0
  %36 = load i32, i32* %valid48, align 4
  %cmp49 = icmp eq i32 %36, 0
  br i1 %cmp49, label %if.then51, label %if.end59

if.then51:                                        ; preds = %if.then42
  %37 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %38 = load i8, i8* %sf_x43, align 1
  %idxprom52 = zext i8 %38 to i32
  %arrayidx53 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %37, i32 %idxprom52
  %valid54 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx53, i32 0, i32 0
  store i32 1, i32* %valid54, align 4
  %39 = load float*, float** %xr.addr, align 4
  %40 = load float*, float** %xr34.addr, align 4
  %41 = load i32, i32* %bw.addr, align 4
  %42 = load i8, i8* %sf_x43, align 1
  %call55 = call float @calc_sfb_noise_x34(float* %39, float* %40, i32 %41, i8 zeroext %42)
  %43 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %44 = load i8, i8* %sf_x43, align 1
  %idxprom56 = zext i8 %44 to i32
  %arrayidx57 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %43, i32 %idxprom56
  %value58 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx57, i32 0, i32 1
  store float %call55, float* %value58, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.then51, %if.then42
  %45 = load float, float* %l3_xmin.addr, align 4
  %46 = load %struct.calc_noise_cache*, %struct.calc_noise_cache** %did_it.addr, align 4
  %47 = load i8, i8* %sf_x43, align 1
  %idxprom60 = zext i8 %47 to i32
  %arrayidx61 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %46, i32 %idxprom60
  %value62 = getelementptr inbounds %struct.calc_noise_cache, %struct.calc_noise_cache* %arrayidx61, i32 0, i32 1
  %48 = load float, float* %value62, align 4
  %cmp63 = fcmp olt float %45, %48
  br i1 %cmp63, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end59
  store i8 1, i8* %retval, align 1
  br label %return

if.end66:                                         ; preds = %if.end59
  br label %if.end67

if.end67:                                         ; preds = %if.end66, %if.end38
  store i8 0, i8* %retval, align 1
  br label %return

return:                                           ; preds = %if.end67, %if.then65, %if.then36, %if.then10
  %49 = load i8, i8* %retval, align 1
  ret i8 %49
}

; Function Attrs: noinline nounwind optnone
define internal float @calc_sfb_noise_x34(float* %xr, float* %xr34, i32 %bw, i8 zeroext %sf) #0 {
entry:
  %xr.addr = alloca float*, align 4
  %xr34.addr = alloca float*, align 4
  %bw.addr = alloca i32, align 4
  %sf.addr = alloca i8, align 1
  %x = alloca [4 x double], align 16
  %l3 = alloca [4 x i32], align 16
  %sfpow = alloca float, align 4
  %sfpow34 = alloca float, align 4
  %xfsf = alloca float, align 4
  %i = alloca i32, align 4
  %remaining = alloca i32, align 4
  store float* %xr, float** %xr.addr, align 4
  store float* %xr34, float** %xr34.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store i8 %sf, i8* %sf.addr, align 1
  %0 = load i8, i8* %sf.addr, align 1
  %conv = zext i8 %0 to i32
  %add = add nsw i32 %conv, 116
  %arrayidx = getelementptr inbounds [374 x float], [374 x float]* @pow20, i32 0, i32 %add
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %sfpow, align 4
  %2 = load i8, i8* %sf.addr, align 1
  %idxprom = zext i8 %2 to i32
  %arrayidx1 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %idxprom
  %3 = load float, float* %arrayidx1, align 4
  store float %3, float* %sfpow34, align 4
  store float 0.000000e+00, float* %xfsf, align 4
  %4 = load i32, i32* %bw.addr, align 4
  %shr = lshr i32 %4, 2
  store i32 %shr, i32* %i, align 4
  %5 = load i32, i32* %bw.addr, align 4
  %and = and i32 %5, 3
  store i32 %and, i32* %remaining, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %6 = load i32, i32* %i, align 4
  %dec = add i32 %6, -1
  store i32 %dec, i32* %i, align 4
  %cmp = icmp ugt i32 %6, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load float, float* %sfpow34, align 4
  %8 = load float*, float** %xr34.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx3, align 4
  %mul = fmul float %7, %9
  %conv4 = fpext float %mul to double
  %arrayidx5 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv4, double* %arrayidx5, align 16
  %10 = load float, float* %sfpow34, align 4
  %11 = load float*, float** %xr34.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %10, %12
  %conv8 = fpext float %mul7 to double
  %arrayidx9 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv8, double* %arrayidx9, align 8
  %13 = load float, float* %sfpow34, align 4
  %14 = load float*, float** %xr34.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float %13, %15
  %conv12 = fpext float %mul11 to double
  %arrayidx13 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv12, double* %arrayidx13, align 16
  %16 = load float, float* %sfpow34, align 4
  %17 = load float*, float** %xr34.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %17, i32 3
  %18 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %18
  %conv16 = fpext float %mul15 to double
  %arrayidx17 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double %conv16, double* %arrayidx17, align 8
  %arraydecay = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %arraydecay18 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 0
  call void @k_34_4(double* %arraydecay, i32* %arraydecay18)
  %19 = load float*, float** %xr.addr, align 4
  %arrayidx19 = getelementptr inbounds float, float* %19, i32 0
  %20 = load float, float* %arrayidx19, align 4
  %21 = call float @llvm.fabs.f32(float %20)
  %22 = load float, float* %sfpow, align 4
  %arrayidx20 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 0
  %23 = load i32, i32* %arrayidx20, align 16
  %arrayidx21 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %23
  %24 = load float, float* %arrayidx21, align 4
  %mul22 = fmul float %22, %24
  %sub = fsub float %21, %mul22
  %conv23 = fpext float %sub to double
  %arrayidx24 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv23, double* %arrayidx24, align 16
  %25 = load float*, float** %xr.addr, align 4
  %arrayidx25 = getelementptr inbounds float, float* %25, i32 1
  %26 = load float, float* %arrayidx25, align 4
  %27 = call float @llvm.fabs.f32(float %26)
  %28 = load float, float* %sfpow, align 4
  %arrayidx26 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 1
  %29 = load i32, i32* %arrayidx26, align 4
  %arrayidx27 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %29
  %30 = load float, float* %arrayidx27, align 4
  %mul28 = fmul float %28, %30
  %sub29 = fsub float %27, %mul28
  %conv30 = fpext float %sub29 to double
  %arrayidx31 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv30, double* %arrayidx31, align 8
  %31 = load float*, float** %xr.addr, align 4
  %arrayidx32 = getelementptr inbounds float, float* %31, i32 2
  %32 = load float, float* %arrayidx32, align 4
  %33 = call float @llvm.fabs.f32(float %32)
  %34 = load float, float* %sfpow, align 4
  %arrayidx33 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 2
  %35 = load i32, i32* %arrayidx33, align 8
  %arrayidx34 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %35
  %36 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %34, %36
  %sub36 = fsub float %33, %mul35
  %conv37 = fpext float %sub36 to double
  %arrayidx38 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv37, double* %arrayidx38, align 16
  %37 = load float*, float** %xr.addr, align 4
  %arrayidx39 = getelementptr inbounds float, float* %37, i32 3
  %38 = load float, float* %arrayidx39, align 4
  %39 = call float @llvm.fabs.f32(float %38)
  %40 = load float, float* %sfpow, align 4
  %arrayidx40 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 3
  %41 = load i32, i32* %arrayidx40, align 4
  %arrayidx41 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %41
  %42 = load float, float* %arrayidx41, align 4
  %mul42 = fmul float %40, %42
  %sub43 = fsub float %39, %mul42
  %conv44 = fpext float %sub43 to double
  %arrayidx45 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double %conv44, double* %arrayidx45, align 8
  %arrayidx46 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %43 = load double, double* %arrayidx46, align 16
  %arrayidx47 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %44 = load double, double* %arrayidx47, align 16
  %mul48 = fmul double %43, %44
  %arrayidx49 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  %45 = load double, double* %arrayidx49, align 8
  %arrayidx50 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  %46 = load double, double* %arrayidx50, align 8
  %mul51 = fmul double %45, %46
  %add52 = fadd double %mul48, %mul51
  %arrayidx53 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  %47 = load double, double* %arrayidx53, align 16
  %arrayidx54 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  %48 = load double, double* %arrayidx54, align 16
  %mul55 = fmul double %47, %48
  %arrayidx56 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  %49 = load double, double* %arrayidx56, align 8
  %arrayidx57 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  %50 = load double, double* %arrayidx57, align 8
  %mul58 = fmul double %49, %50
  %add59 = fadd double %mul55, %mul58
  %add60 = fadd double %add52, %add59
  %51 = load float, float* %xfsf, align 4
  %conv61 = fpext float %51 to double
  %add62 = fadd double %conv61, %add60
  %conv63 = fptrunc double %add62 to float
  store float %conv63, float* %xfsf, align 4
  %52 = load float*, float** %xr.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %52, i32 4
  store float* %add.ptr, float** %xr.addr, align 4
  %53 = load float*, float** %xr34.addr, align 4
  %add.ptr64 = getelementptr inbounds float, float* %53, i32 4
  store float* %add.ptr64, float** %xr34.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %54 = load i32, i32* %remaining, align 4
  %tobool = icmp ne i32 %54, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %arrayidx65 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double 0.000000e+00, double* %arrayidx65, align 8
  %arrayidx66 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double 0.000000e+00, double* %arrayidx66, align 16
  %arrayidx67 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx67, align 8
  %arrayidx68 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double 0.000000e+00, double* %arrayidx68, align 16
  %55 = load i32, i32* %remaining, align 4
  switch i32 %55, label %sw.epilog [
    i32 3, label %sw.bb
    i32 2, label %sw.bb73
    i32 1, label %sw.bb78
  ]

sw.bb:                                            ; preds = %if.then
  %56 = load float, float* %sfpow34, align 4
  %57 = load float*, float** %xr34.addr, align 4
  %arrayidx69 = getelementptr inbounds float, float* %57, i32 2
  %58 = load float, float* %arrayidx69, align 4
  %mul70 = fmul float %56, %58
  %conv71 = fpext float %mul70 to double
  %arrayidx72 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv71, double* %arrayidx72, align 16
  br label %sw.bb73

sw.bb73:                                          ; preds = %if.then, %sw.bb
  %59 = load float, float* %sfpow34, align 4
  %60 = load float*, float** %xr34.addr, align 4
  %arrayidx74 = getelementptr inbounds float, float* %60, i32 1
  %61 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %59, %61
  %conv76 = fpext float %mul75 to double
  %arrayidx77 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv76, double* %arrayidx77, align 8
  br label %sw.bb78

sw.bb78:                                          ; preds = %if.then, %sw.bb73
  %62 = load float, float* %sfpow34, align 4
  %63 = load float*, float** %xr34.addr, align 4
  %arrayidx79 = getelementptr inbounds float, float* %63, i32 0
  %64 = load float, float* %arrayidx79, align 4
  %mul80 = fmul float %62, %64
  %conv81 = fpext float %mul80 to double
  %arrayidx82 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv81, double* %arrayidx82, align 16
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb78, %if.then
  %arraydecay83 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %arraydecay84 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 0
  call void @k_34_4(double* %arraydecay83, i32* %arraydecay84)
  %arrayidx85 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double 0.000000e+00, double* %arrayidx85, align 8
  %arrayidx86 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double 0.000000e+00, double* %arrayidx86, align 16
  %arrayidx87 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx87, align 8
  %arrayidx88 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double 0.000000e+00, double* %arrayidx88, align 16
  %65 = load i32, i32* %remaining, align 4
  switch i32 %65, label %sw.epilog113 [
    i32 3, label %sw.bb89
    i32 2, label %sw.bb97
    i32 1, label %sw.bb105
  ]

sw.bb89:                                          ; preds = %sw.epilog
  %66 = load float*, float** %xr.addr, align 4
  %arrayidx90 = getelementptr inbounds float, float* %66, i32 2
  %67 = load float, float* %arrayidx90, align 4
  %68 = call float @llvm.fabs.f32(float %67)
  %69 = load float, float* %sfpow, align 4
  %arrayidx91 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 2
  %70 = load i32, i32* %arrayidx91, align 8
  %arrayidx92 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %70
  %71 = load float, float* %arrayidx92, align 4
  %mul93 = fmul float %69, %71
  %sub94 = fsub float %68, %mul93
  %conv95 = fpext float %sub94 to double
  %arrayidx96 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv95, double* %arrayidx96, align 16
  br label %sw.bb97

sw.bb97:                                          ; preds = %sw.epilog, %sw.bb89
  %72 = load float*, float** %xr.addr, align 4
  %arrayidx98 = getelementptr inbounds float, float* %72, i32 1
  %73 = load float, float* %arrayidx98, align 4
  %74 = call float @llvm.fabs.f32(float %73)
  %75 = load float, float* %sfpow, align 4
  %arrayidx99 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 1
  %76 = load i32, i32* %arrayidx99, align 4
  %arrayidx100 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %76
  %77 = load float, float* %arrayidx100, align 4
  %mul101 = fmul float %75, %77
  %sub102 = fsub float %74, %mul101
  %conv103 = fpext float %sub102 to double
  %arrayidx104 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv103, double* %arrayidx104, align 8
  br label %sw.bb105

sw.bb105:                                         ; preds = %sw.epilog, %sw.bb97
  %78 = load float*, float** %xr.addr, align 4
  %arrayidx106 = getelementptr inbounds float, float* %78, i32 0
  %79 = load float, float* %arrayidx106, align 4
  %80 = call float @llvm.fabs.f32(float %79)
  %81 = load float, float* %sfpow, align 4
  %arrayidx107 = getelementptr inbounds [4 x i32], [4 x i32]* %l3, i32 0, i32 0
  %82 = load i32, i32* %arrayidx107, align 16
  %arrayidx108 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %82
  %83 = load float, float* %arrayidx108, align 4
  %mul109 = fmul float %81, %83
  %sub110 = fsub float %80, %mul109
  %conv111 = fpext float %sub110 to double
  %arrayidx112 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv111, double* %arrayidx112, align 16
  br label %sw.epilog113

sw.epilog113:                                     ; preds = %sw.bb105, %sw.epilog
  %arrayidx114 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %84 = load double, double* %arrayidx114, align 16
  %arrayidx115 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %85 = load double, double* %arrayidx115, align 16
  %mul116 = fmul double %84, %85
  %arrayidx117 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  %86 = load double, double* %arrayidx117, align 8
  %arrayidx118 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  %87 = load double, double* %arrayidx118, align 8
  %mul119 = fmul double %86, %87
  %add120 = fadd double %mul116, %mul119
  %arrayidx121 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  %88 = load double, double* %arrayidx121, align 16
  %arrayidx122 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  %89 = load double, double* %arrayidx122, align 16
  %mul123 = fmul double %88, %89
  %arrayidx124 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  %90 = load double, double* %arrayidx124, align 8
  %arrayidx125 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  %91 = load double, double* %arrayidx125, align 8
  %mul126 = fmul double %90, %91
  %add127 = fadd double %mul123, %mul126
  %add128 = fadd double %add120, %add127
  %92 = load float, float* %xfsf, align 4
  %conv129 = fpext float %92 to double
  %add130 = fadd double %conv129, %add128
  %conv131 = fptrunc double %add130 to float
  store float %conv131, float* %xfsf, align 4
  br label %if.end

if.end:                                           ; preds = %sw.epilog113, %while.end
  %93 = load float, float* %xfsf, align 4
  ret float %93
}

; Function Attrs: noinline nounwind optnone
define internal void @k_34_4(double* %x, i32* %l3) #0 {
entry:
  %x.addr = alloca double*, align 4
  %l3.addr = alloca i32*, align 4
  %fi = alloca [4 x %union.fi_union], align 16
  store double* %x, double** %x.addr, align 4
  store i32* %l3, i32** %l3.addr, align 4
  %0 = load double*, double** %x.addr, align 4
  %arrayidx = getelementptr inbounds double, double* %0, i32 0
  %1 = load double, double* %arrayidx, align 8
  %add = fadd double %1, 0x4160000000000000
  store double %add, double* %arrayidx, align 8
  %2 = load double*, double** %x.addr, align 4
  %arrayidx1 = getelementptr inbounds double, double* %2, i32 0
  %3 = load double, double* %arrayidx1, align 8
  %conv = fptrunc double %3 to float
  %arrayidx2 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 0
  %f = bitcast %union.fi_union* %arrayidx2 to float*
  store float %conv, float* %f, align 16
  %4 = load double*, double** %x.addr, align 4
  %arrayidx3 = getelementptr inbounds double, double* %4, i32 1
  %5 = load double, double* %arrayidx3, align 8
  %add4 = fadd double %5, 0x4160000000000000
  store double %add4, double* %arrayidx3, align 8
  %6 = load double*, double** %x.addr, align 4
  %arrayidx5 = getelementptr inbounds double, double* %6, i32 1
  %7 = load double, double* %arrayidx5, align 8
  %conv6 = fptrunc double %7 to float
  %arrayidx7 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 1
  %f8 = bitcast %union.fi_union* %arrayidx7 to float*
  store float %conv6, float* %f8, align 4
  %8 = load double*, double** %x.addr, align 4
  %arrayidx9 = getelementptr inbounds double, double* %8, i32 2
  %9 = load double, double* %arrayidx9, align 8
  %add10 = fadd double %9, 0x4160000000000000
  store double %add10, double* %arrayidx9, align 8
  %10 = load double*, double** %x.addr, align 4
  %arrayidx11 = getelementptr inbounds double, double* %10, i32 2
  %11 = load double, double* %arrayidx11, align 8
  %conv12 = fptrunc double %11 to float
  %arrayidx13 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 2
  %f14 = bitcast %union.fi_union* %arrayidx13 to float*
  store float %conv12, float* %f14, align 8
  %12 = load double*, double** %x.addr, align 4
  %arrayidx15 = getelementptr inbounds double, double* %12, i32 3
  %13 = load double, double* %arrayidx15, align 8
  %add16 = fadd double %13, 0x4160000000000000
  store double %add16, double* %arrayidx15, align 8
  %14 = load double*, double** %x.addr, align 4
  %arrayidx17 = getelementptr inbounds double, double* %14, i32 3
  %15 = load double, double* %arrayidx17, align 8
  %conv18 = fptrunc double %15 to float
  %arrayidx19 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 3
  %f20 = bitcast %union.fi_union* %arrayidx19 to float*
  store float %conv18, float* %f20, align 4
  %16 = load double*, double** %x.addr, align 4
  %arrayidx21 = getelementptr inbounds double, double* %16, i32 0
  %17 = load double, double* %arrayidx21, align 8
  %arrayidx22 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 0
  %i = bitcast %union.fi_union* %arrayidx22 to i32*
  %18 = load i32, i32* %i, align 16
  %sub = sub nsw i32 %18, 1258291200
  %arrayidx23 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub
  %19 = load float, float* %arrayidx23, align 4
  %conv24 = fpext float %19 to double
  %add25 = fadd double %17, %conv24
  %conv26 = fptrunc double %add25 to float
  %arrayidx27 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 0
  %f28 = bitcast %union.fi_union* %arrayidx27 to float*
  store float %conv26, float* %f28, align 16
  %20 = load double*, double** %x.addr, align 4
  %arrayidx29 = getelementptr inbounds double, double* %20, i32 1
  %21 = load double, double* %arrayidx29, align 8
  %arrayidx30 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 1
  %i31 = bitcast %union.fi_union* %arrayidx30 to i32*
  %22 = load i32, i32* %i31, align 4
  %sub32 = sub nsw i32 %22, 1258291200
  %arrayidx33 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub32
  %23 = load float, float* %arrayidx33, align 4
  %conv34 = fpext float %23 to double
  %add35 = fadd double %21, %conv34
  %conv36 = fptrunc double %add35 to float
  %arrayidx37 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 1
  %f38 = bitcast %union.fi_union* %arrayidx37 to float*
  store float %conv36, float* %f38, align 4
  %24 = load double*, double** %x.addr, align 4
  %arrayidx39 = getelementptr inbounds double, double* %24, i32 2
  %25 = load double, double* %arrayidx39, align 8
  %arrayidx40 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 2
  %i41 = bitcast %union.fi_union* %arrayidx40 to i32*
  %26 = load i32, i32* %i41, align 8
  %sub42 = sub nsw i32 %26, 1258291200
  %arrayidx43 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub42
  %27 = load float, float* %arrayidx43, align 4
  %conv44 = fpext float %27 to double
  %add45 = fadd double %25, %conv44
  %conv46 = fptrunc double %add45 to float
  %arrayidx47 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 2
  %f48 = bitcast %union.fi_union* %arrayidx47 to float*
  store float %conv46, float* %f48, align 8
  %28 = load double*, double** %x.addr, align 4
  %arrayidx49 = getelementptr inbounds double, double* %28, i32 3
  %29 = load double, double* %arrayidx49, align 8
  %arrayidx50 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 3
  %i51 = bitcast %union.fi_union* %arrayidx50 to i32*
  %30 = load i32, i32* %i51, align 4
  %sub52 = sub nsw i32 %30, 1258291200
  %arrayidx53 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %sub52
  %31 = load float, float* %arrayidx53, align 4
  %conv54 = fpext float %31 to double
  %add55 = fadd double %29, %conv54
  %conv56 = fptrunc double %add55 to float
  %arrayidx57 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 3
  %f58 = bitcast %union.fi_union* %arrayidx57 to float*
  store float %conv56, float* %f58, align 4
  %arrayidx59 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 0
  %i60 = bitcast %union.fi_union* %arrayidx59 to i32*
  %32 = load i32, i32* %i60, align 16
  %sub61 = sub nsw i32 %32, 1258291200
  %33 = load i32*, i32** %l3.addr, align 4
  %arrayidx62 = getelementptr inbounds i32, i32* %33, i32 0
  store i32 %sub61, i32* %arrayidx62, align 4
  %arrayidx63 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 1
  %i64 = bitcast %union.fi_union* %arrayidx63 to i32*
  %34 = load i32, i32* %i64, align 4
  %sub65 = sub nsw i32 %34, 1258291200
  %35 = load i32*, i32** %l3.addr, align 4
  %arrayidx66 = getelementptr inbounds i32, i32* %35, i32 1
  store i32 %sub65, i32* %arrayidx66, align 4
  %arrayidx67 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 2
  %i68 = bitcast %union.fi_union* %arrayidx67 to i32*
  %36 = load i32, i32* %i68, align 8
  %sub69 = sub nsw i32 %36, 1258291200
  %37 = load i32*, i32** %l3.addr, align 4
  %arrayidx70 = getelementptr inbounds i32, i32* %37, i32 2
  store i32 %sub69, i32* %arrayidx70, align 4
  %arrayidx71 = getelementptr inbounds [4 x %union.fi_union], [4 x %union.fi_union]* %fi, i32 0, i32 3
  %i72 = bitcast %union.fi_union* %arrayidx71 to i32*
  %38 = load i32, i32* %i72, align 4
  %sub73 = sub nsw i32 %38, 1258291200
  %39 = load i32*, i32** %l3.addr, align 4
  %arrayidx74 = getelementptr inbounds i32, i32* %39, i32 3
  store i32 %sub73, i32* %arrayidx74, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #3

; Function Attrs: noinline nounwind optnone
define internal void @set_subblock_gain(%struct.gr_info* %cod_info, i32* %mingain_s, i32* %sf) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %mingain_s.addr = alloca i32*, align 4
  %sf.addr = alloca i32*, align 4
  %maxrange1 = alloca i32, align 4
  %maxrange2 = alloca i32, align 4
  %ifqstepShift = alloca i32, align 4
  %sbg = alloca i32*, align 4
  %psymax = alloca i32, align 4
  %psydiv = alloca i32, align 4
  %sbg0 = alloca i32, align 4
  %sbg1 = alloca i32, align 4
  %sbg2 = alloca i32, align 4
  %sfb = alloca i32, align 4
  %i = alloca i32, align 4
  %min_sbg = alloca i32, align 4
  %maxsf1 = alloca i32, align 4
  %maxsf2 = alloca i32, align 4
  %minsf = alloca i32, align 4
  %v = alloca i32, align 4
  %v16 = alloca i32, align 4
  %m1 = alloca i32, align 4
  %m2 = alloca i32, align 4
  %m140 = alloca i32, align 4
  %m242 = alloca i32, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32* %mingain_s, i32** %mingain_s.addr, align 4
  store i32* %sf, i32** %sf.addr, align 4
  store i32 15, i32* %maxrange1, align 4
  store i32 7, i32* %maxrange2, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 16
  %1 = load i32, i32* %scalefac_scale, align 4
  %cmp = icmp eq i32 %1, 0
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 2
  store i32 %cond, i32* %ifqstepShift, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 0
  store i32* %arraydecay, i32** %sbg, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 23
  %5 = load i32, i32* %psymax1, align 4
  store i32 %5, i32* %psymax, align 4
  store i32 18, i32* %psydiv, align 4
  store i32 7, i32* %min_sbg, align 4
  %6 = load i32, i32* %psydiv, align 4
  %7 = load i32, i32* %psymax, align 4
  %cmp2 = icmp ugt i32 %6, %7
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load i32, i32* %psymax, align 4
  store i32 %8, i32* %psydiv, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %if.end
  %9 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %9, 3
  br i1 %cmp3, label %for.body, label %for.end76

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %maxsf1, align 4
  store i32 0, i32* %maxsf2, align 4
  store i32 1000, i32* %minsf, align 4
  %10 = load i32, i32* %i, align 4
  store i32 %10, i32* %sfb, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %sfb, align 4
  %12 = load i32, i32* %psydiv, align 4
  %cmp5 = icmp ult i32 %11, %12
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %13 = load i32*, i32** %sf.addr, align 4
  %14 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds i32, i32* %13, i32 %14
  %15 = load i32, i32* %arrayidx, align 4
  %sub = sub nsw i32 0, %15
  store i32 %sub, i32* %v, align 4
  %16 = load i32, i32* %maxsf1, align 4
  %17 = load i32, i32* %v, align 4
  %cmp7 = icmp slt i32 %16, %17
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %for.body6
  %18 = load i32, i32* %v, align 4
  store i32 %18, i32* %maxsf1, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %for.body6
  %19 = load i32, i32* %minsf, align 4
  %20 = load i32, i32* %v, align 4
  %cmp10 = icmp sgt i32 %19, %20
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  %21 = load i32, i32* %v, align 4
  store i32 %21, i32* %minsf, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end9
  br label %for.inc

for.inc:                                          ; preds = %if.end12
  %22 = load i32, i32* %sfb, align 4
  %add = add i32 %22, 3
  store i32 %add, i32* %sfb, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc25, %for.end
  %23 = load i32, i32* %sfb, align 4
  %cmp14 = icmp ult i32 %23, 39
  br i1 %cmp14, label %for.body15, label %for.end27

for.body15:                                       ; preds = %for.cond13
  %24 = load i32*, i32** %sf.addr, align 4
  %25 = load i32, i32* %sfb, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %24, i32 %25
  %26 = load i32, i32* %arrayidx17, align 4
  %sub18 = sub nsw i32 0, %26
  store i32 %sub18, i32* %v16, align 4
  %27 = load i32, i32* %maxsf2, align 4
  %28 = load i32, i32* %v16, align 4
  %cmp19 = icmp slt i32 %27, %28
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %for.body15
  %29 = load i32, i32* %v16, align 4
  store i32 %29, i32* %maxsf2, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %for.body15
  %30 = load i32, i32* %minsf, align 4
  %31 = load i32, i32* %v16, align 4
  %cmp22 = icmp sgt i32 %30, %31
  br i1 %cmp22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end21
  %32 = load i32, i32* %v16, align 4
  store i32 %32, i32* %minsf, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end21
  br label %for.inc25

for.inc25:                                        ; preds = %if.end24
  %33 = load i32, i32* %sfb, align 4
  %add26 = add i32 %33, 3
  store i32 %add26, i32* %sfb, align 4
  br label %for.cond13

for.end27:                                        ; preds = %for.cond13
  %34 = load i32, i32* %maxsf1, align 4
  %35 = load i32, i32* %ifqstepShift, align 4
  %shl = shl i32 15, %35
  %sub28 = sub nsw i32 %34, %shl
  store i32 %sub28, i32* %m1, align 4
  %36 = load i32, i32* %maxsf2, align 4
  %37 = load i32, i32* %ifqstepShift, align 4
  %shl29 = shl i32 7, %37
  %sub30 = sub nsw i32 %36, %shl29
  store i32 %sub30, i32* %m2, align 4
  %38 = load i32, i32* %m1, align 4
  %39 = load i32, i32* %m2, align 4
  %cmp31 = icmp sgt i32 %38, %39
  br i1 %cmp31, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end27
  %40 = load i32, i32* %m1, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.end27
  %41 = load i32, i32* %m2, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond32 = phi i32 [ %40, %cond.true ], [ %41, %cond.false ]
  store i32 %cond32, i32* %maxsf1, align 4
  %42 = load i32, i32* %minsf, align 4
  %cmp33 = icmp sgt i32 %42, 0
  br i1 %cmp33, label %if.then34, label %if.else

if.then34:                                        ; preds = %cond.end
  %43 = load i32, i32* %minsf, align 4
  %shr = ashr i32 %43, 3
  %44 = load i32*, i32** %sbg, align 4
  %45 = load i32, i32* %i, align 4
  %arrayidx35 = getelementptr inbounds i32, i32* %44, i32 %45
  store i32 %shr, i32* %arrayidx35, align 4
  br label %if.end37

if.else:                                          ; preds = %cond.end
  %46 = load i32*, i32** %sbg, align 4
  %47 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds i32, i32* %46, i32 %47
  store i32 0, i32* %arrayidx36, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else, %if.then34
  %48 = load i32, i32* %maxsf1, align 4
  %cmp38 = icmp sgt i32 %48, 0
  br i1 %cmp38, label %if.then39, label %if.end51

if.then39:                                        ; preds = %if.end37
  %49 = load i32*, i32** %sbg, align 4
  %50 = load i32, i32* %i, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %49, i32 %50
  %51 = load i32, i32* %arrayidx41, align 4
  store i32 %51, i32* %m140, align 4
  %52 = load i32, i32* %maxsf1, align 4
  %add43 = add nsw i32 %52, 7
  %shr44 = ashr i32 %add43, 3
  store i32 %shr44, i32* %m242, align 4
  %53 = load i32, i32* %m140, align 4
  %54 = load i32, i32* %m242, align 4
  %cmp45 = icmp sgt i32 %53, %54
  br i1 %cmp45, label %cond.true46, label %cond.false47

cond.true46:                                      ; preds = %if.then39
  %55 = load i32, i32* %m140, align 4
  br label %cond.end48

cond.false47:                                     ; preds = %if.then39
  %56 = load i32, i32* %m242, align 4
  br label %cond.end48

cond.end48:                                       ; preds = %cond.false47, %cond.true46
  %cond49 = phi i32 [ %55, %cond.true46 ], [ %56, %cond.false47 ]
  %57 = load i32*, i32** %sbg, align 4
  %58 = load i32, i32* %i, align 4
  %arrayidx50 = getelementptr inbounds i32, i32* %57, i32 %58
  store i32 %cond49, i32* %arrayidx50, align 4
  br label %if.end51

if.end51:                                         ; preds = %cond.end48, %if.end37
  %59 = load i32*, i32** %sbg, align 4
  %60 = load i32, i32* %i, align 4
  %arrayidx52 = getelementptr inbounds i32, i32* %59, i32 %60
  %61 = load i32, i32* %arrayidx52, align 4
  %cmp53 = icmp sgt i32 %61, 0
  br i1 %cmp53, label %land.lhs.true, label %if.end64

land.lhs.true:                                    ; preds = %if.end51
  %62 = load i32*, i32** %mingain_s.addr, align 4
  %63 = load i32, i32* %i, align 4
  %arrayidx54 = getelementptr inbounds i32, i32* %62, i32 %63
  %64 = load i32, i32* %arrayidx54, align 4
  %65 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %65, i32 0, i32 7
  %66 = load i32, i32* %global_gain, align 4
  %67 = load i32*, i32** %sbg, align 4
  %68 = load i32, i32* %i, align 4
  %arrayidx55 = getelementptr inbounds i32, i32* %67, i32 %68
  %69 = load i32, i32* %arrayidx55, align 4
  %mul = mul nsw i32 %69, 8
  %sub56 = sub nsw i32 %66, %mul
  %cmp57 = icmp sgt i32 %64, %sub56
  br i1 %cmp57, label %if.then58, label %if.end64

if.then58:                                        ; preds = %land.lhs.true
  %70 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain59 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %70, i32 0, i32 7
  %71 = load i32, i32* %global_gain59, align 4
  %72 = load i32*, i32** %mingain_s.addr, align 4
  %73 = load i32, i32* %i, align 4
  %arrayidx60 = getelementptr inbounds i32, i32* %72, i32 %73
  %74 = load i32, i32* %arrayidx60, align 4
  %sub61 = sub nsw i32 %71, %74
  %shr62 = ashr i32 %sub61, 3
  %75 = load i32*, i32** %sbg, align 4
  %76 = load i32, i32* %i, align 4
  %arrayidx63 = getelementptr inbounds i32, i32* %75, i32 %76
  store i32 %shr62, i32* %arrayidx63, align 4
  br label %if.end64

if.end64:                                         ; preds = %if.then58, %land.lhs.true, %if.end51
  %77 = load i32*, i32** %sbg, align 4
  %78 = load i32, i32* %i, align 4
  %arrayidx65 = getelementptr inbounds i32, i32* %77, i32 %78
  %79 = load i32, i32* %arrayidx65, align 4
  %cmp66 = icmp sgt i32 %79, 7
  br i1 %cmp66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.end64
  %80 = load i32*, i32** %sbg, align 4
  %81 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds i32, i32* %80, i32 %81
  store i32 7, i32* %arrayidx68, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.end64
  %82 = load i32, i32* %min_sbg, align 4
  %83 = load i32*, i32** %sbg, align 4
  %84 = load i32, i32* %i, align 4
  %arrayidx70 = getelementptr inbounds i32, i32* %83, i32 %84
  %85 = load i32, i32* %arrayidx70, align 4
  %cmp71 = icmp sgt i32 %82, %85
  br i1 %cmp71, label %if.then72, label %if.end74

if.then72:                                        ; preds = %if.end69
  %86 = load i32*, i32** %sbg, align 4
  %87 = load i32, i32* %i, align 4
  %arrayidx73 = getelementptr inbounds i32, i32* %86, i32 %87
  %88 = load i32, i32* %arrayidx73, align 4
  store i32 %88, i32* %min_sbg, align 4
  br label %if.end74

if.end74:                                         ; preds = %if.then72, %if.end69
  br label %for.inc75

for.inc75:                                        ; preds = %if.end74
  %89 = load i32, i32* %i, align 4
  %inc = add i32 %89, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end76:                                        ; preds = %for.cond
  %90 = load i32*, i32** %sbg, align 4
  %arrayidx77 = getelementptr inbounds i32, i32* %90, i32 0
  %91 = load i32, i32* %arrayidx77, align 4
  %mul78 = mul nsw i32 %91, 8
  store i32 %mul78, i32* %sbg0, align 4
  %92 = load i32*, i32** %sbg, align 4
  %arrayidx79 = getelementptr inbounds i32, i32* %92, i32 1
  %93 = load i32, i32* %arrayidx79, align 4
  %mul80 = mul nsw i32 %93, 8
  store i32 %mul80, i32* %sbg1, align 4
  %94 = load i32*, i32** %sbg, align 4
  %arrayidx81 = getelementptr inbounds i32, i32* %94, i32 2
  %95 = load i32, i32* %arrayidx81, align 4
  %mul82 = mul nsw i32 %95, 8
  store i32 %mul82, i32* %sbg2, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond83

for.cond83:                                       ; preds = %for.inc95, %for.end76
  %96 = load i32, i32* %sfb, align 4
  %cmp84 = icmp ult i32 %96, 39
  br i1 %cmp84, label %for.body85, label %for.end97

for.body85:                                       ; preds = %for.cond83
  %97 = load i32, i32* %sbg0, align 4
  %98 = load i32*, i32** %sf.addr, align 4
  %99 = load i32, i32* %sfb, align 4
  %add86 = add i32 %99, 0
  %arrayidx87 = getelementptr inbounds i32, i32* %98, i32 %add86
  %100 = load i32, i32* %arrayidx87, align 4
  %add88 = add nsw i32 %100, %97
  store i32 %add88, i32* %arrayidx87, align 4
  %101 = load i32, i32* %sbg1, align 4
  %102 = load i32*, i32** %sf.addr, align 4
  %103 = load i32, i32* %sfb, align 4
  %add89 = add i32 %103, 1
  %arrayidx90 = getelementptr inbounds i32, i32* %102, i32 %add89
  %104 = load i32, i32* %arrayidx90, align 4
  %add91 = add nsw i32 %104, %101
  store i32 %add91, i32* %arrayidx90, align 4
  %105 = load i32, i32* %sbg2, align 4
  %106 = load i32*, i32** %sf.addr, align 4
  %107 = load i32, i32* %sfb, align 4
  %add92 = add i32 %107, 2
  %arrayidx93 = getelementptr inbounds i32, i32* %106, i32 %add92
  %108 = load i32, i32* %arrayidx93, align 4
  %add94 = add nsw i32 %108, %105
  store i32 %add94, i32* %arrayidx93, align 4
  br label %for.inc95

for.inc95:                                        ; preds = %for.body85
  %109 = load i32, i32* %sfb, align 4
  %add96 = add i32 %109, 3
  store i32 %add96, i32* %sfb, align 4
  br label %for.cond83

for.end97:                                        ; preds = %for.cond83
  %110 = load i32, i32* %min_sbg, align 4
  %cmp98 = icmp sgt i32 %110, 0
  br i1 %cmp98, label %if.then99, label %if.end111

if.then99:                                        ; preds = %for.end97
  store i32 0, i32* %i, align 4
  br label %for.cond100

for.cond100:                                      ; preds = %for.inc105, %if.then99
  %111 = load i32, i32* %i, align 4
  %cmp101 = icmp ult i32 %111, 3
  br i1 %cmp101, label %for.body102, label %for.end107

for.body102:                                      ; preds = %for.cond100
  %112 = load i32, i32* %min_sbg, align 4
  %113 = load i32*, i32** %sbg, align 4
  %114 = load i32, i32* %i, align 4
  %arrayidx103 = getelementptr inbounds i32, i32* %113, i32 %114
  %115 = load i32, i32* %arrayidx103, align 4
  %sub104 = sub nsw i32 %115, %112
  store i32 %sub104, i32* %arrayidx103, align 4
  br label %for.inc105

for.inc105:                                       ; preds = %for.body102
  %116 = load i32, i32* %i, align 4
  %inc106 = add i32 %116, 1
  store i32 %inc106, i32* %i, align 4
  br label %for.cond100

for.end107:                                       ; preds = %for.cond100
  %117 = load i32, i32* %min_sbg, align 4
  %mul108 = mul nsw i32 %117, 8
  %118 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain109 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %118, i32 0, i32 7
  %119 = load i32, i32* %global_gain109, align 4
  %sub110 = sub nsw i32 %119, %mul108
  store i32 %sub110, i32* %global_gain109, align 4
  br label %if.end111

if.end111:                                        ; preds = %for.end107, %for.end97
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @set_scalefacs(%struct.gr_info* %cod_info, i32* %vbrsfmin, i32* %sf, i8* %max_range) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %sf.addr = alloca i32*, align 4
  %max_range.addr = alloca i8*, align 4
  %ifqstep = alloca i32, align 4
  %ifqstepShift = alloca i32, align 4
  %scalefac = alloca i32*, align 4
  %sfbmax = alloca i32, align 4
  %sfb = alloca i32, align 4
  %sbg = alloca i32*, align 4
  %window = alloca i32*, align 4
  %preflag = alloca i32, align 4
  %gain = alloca i32, align 4
  %m = alloca i32, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32* %sf, i32** %sf.addr, align 4
  store i8* %max_range, i8** %max_range.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 16
  %1 = load i32, i32* %scalefac_scale, align 4
  %cmp = icmp eq i32 %1, 0
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 2, i32 4
  store i32 %cond, i32* %ifqstep, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 16
  %4 = load i32, i32* %scalefac_scale1, align 4
  %cmp2 = icmp eq i32 %4, 0
  %5 = zext i1 %cmp2 to i64
  %cond3 = select i1 %cmp2, i32 1, i32 2
  store i32 %cond3, i32* %ifqstepShift, align 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac4, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax5 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 22
  %8 = load i32, i32* %sfbmax5, align 4
  store i32 %8, i32* %sfbmax, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 12
  %arraydecay6 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 0
  store i32* %arraydecay6, i32** %sbg, align 4
  %10 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window7 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %10, i32 0, i32 26
  %arraydecay8 = getelementptr inbounds [39 x i32], [39 x i32]* %window7, i32 0, i32 0
  store i32* %arraydecay8, i32** %window, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag9 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 15
  %12 = load i32, i32* %preflag9, align 4
  store i32 %12, i32* %preflag, align 4
  %13 = load i32, i32* %preflag, align 4
  %tobool = icmp ne i32 %13, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 11, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %14 = load i32, i32* %sfb, align 4
  %15 = load i32, i32* %sfbmax, align 4
  %cmp10 = icmp slt i32 %14, %15
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %16
  %17 = load i32, i32* %arrayidx, align 4
  %18 = load i32, i32* %ifqstep, align 4
  %mul = mul nsw i32 %17, %18
  %19 = load i32*, i32** %sf.addr, align 4
  %20 = load i32, i32* %sfb, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %19, i32 %20
  %21 = load i32, i32* %arrayidx11, align 4
  %add = add nsw i32 %21, %mul
  store i32 %add, i32* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  store i32 0, i32* %sfb, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc53, %if.end
  %23 = load i32, i32* %sfb, align 4
  %24 = load i32, i32* %sfbmax, align 4
  %cmp13 = icmp slt i32 %23, %24
  br i1 %cmp13, label %for.body14, label %for.end55

for.body14:                                       ; preds = %for.cond12
  %25 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 7
  %26 = load i32, i32* %global_gain, align 4
  %27 = load i32*, i32** %sbg, align 4
  %28 = load i32*, i32** %window, align 4
  %29 = load i32, i32* %sfb, align 4
  %arrayidx15 = getelementptr inbounds i32, i32* %28, i32 %29
  %30 = load i32, i32* %arrayidx15, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %27, i32 %30
  %31 = load i32, i32* %arrayidx16, align 4
  %mul17 = mul nsw i32 %31, 8
  %sub = sub nsw i32 %26, %mul17
  %32 = load i32, i32* %preflag, align 4
  %tobool18 = icmp ne i32 %32, 0
  br i1 %tobool18, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body14
  %33 = load i32, i32* %sfb, align 4
  %arrayidx19 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %33
  %34 = load i32, i32* %arrayidx19, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond20 = phi i32 [ %34, %cond.true ], [ 0, %cond.false ]
  %35 = load i32, i32* %ifqstep, align 4
  %mul21 = mul nsw i32 %cond20, %35
  %sub22 = sub nsw i32 %sub, %mul21
  store i32 %sub22, i32* %gain, align 4
  %36 = load i32*, i32** %sf.addr, align 4
  %37 = load i32, i32* %sfb, align 4
  %arrayidx23 = getelementptr inbounds i32, i32* %36, i32 %37
  %38 = load i32, i32* %arrayidx23, align 4
  %cmp24 = icmp slt i32 %38, 0
  br i1 %cmp24, label %if.then25, label %if.else

if.then25:                                        ; preds = %cond.end
  %39 = load i32, i32* %gain, align 4
  %40 = load i32*, i32** %vbrsfmin.addr, align 4
  %41 = load i32, i32* %sfb, align 4
  %arrayidx26 = getelementptr inbounds i32, i32* %40, i32 %41
  %42 = load i32, i32* %arrayidx26, align 4
  %sub27 = sub nsw i32 %39, %42
  store i32 %sub27, i32* %m, align 4
  %43 = load i32, i32* %ifqstep, align 4
  %sub28 = sub nsw i32 %43, 1
  %44 = load i32*, i32** %sf.addr, align 4
  %45 = load i32, i32* %sfb, align 4
  %arrayidx29 = getelementptr inbounds i32, i32* %44, i32 %45
  %46 = load i32, i32* %arrayidx29, align 4
  %sub30 = sub nsw i32 %sub28, %46
  %47 = load i32, i32* %ifqstepShift, align 4
  %shr = ashr i32 %sub30, %47
  %48 = load i32*, i32** %scalefac, align 4
  %49 = load i32, i32* %sfb, align 4
  %arrayidx31 = getelementptr inbounds i32, i32* %48, i32 %49
  store i32 %shr, i32* %arrayidx31, align 4
  %50 = load i32*, i32** %scalefac, align 4
  %51 = load i32, i32* %sfb, align 4
  %arrayidx32 = getelementptr inbounds i32, i32* %50, i32 %51
  %52 = load i32, i32* %arrayidx32, align 4
  %53 = load i8*, i8** %max_range.addr, align 4
  %54 = load i32, i32* %sfb, align 4
  %arrayidx33 = getelementptr inbounds i8, i8* %53, i32 %54
  %55 = load i8, i8* %arrayidx33, align 1
  %conv = zext i8 %55 to i32
  %cmp34 = icmp sgt i32 %52, %conv
  br i1 %cmp34, label %if.then36, label %if.end40

if.then36:                                        ; preds = %if.then25
  %56 = load i8*, i8** %max_range.addr, align 4
  %57 = load i32, i32* %sfb, align 4
  %arrayidx37 = getelementptr inbounds i8, i8* %56, i32 %57
  %58 = load i8, i8* %arrayidx37, align 1
  %conv38 = zext i8 %58 to i32
  %59 = load i32*, i32** %scalefac, align 4
  %60 = load i32, i32* %sfb, align 4
  %arrayidx39 = getelementptr inbounds i32, i32* %59, i32 %60
  store i32 %conv38, i32* %arrayidx39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then36, %if.then25
  %61 = load i32*, i32** %scalefac, align 4
  %62 = load i32, i32* %sfb, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %61, i32 %62
  %63 = load i32, i32* %arrayidx41, align 4
  %cmp42 = icmp sgt i32 %63, 0
  br i1 %cmp42, label %land.lhs.true, label %if.end50

land.lhs.true:                                    ; preds = %if.end40
  %64 = load i32*, i32** %scalefac, align 4
  %65 = load i32, i32* %sfb, align 4
  %arrayidx44 = getelementptr inbounds i32, i32* %64, i32 %65
  %66 = load i32, i32* %arrayidx44, align 4
  %67 = load i32, i32* %ifqstepShift, align 4
  %shl = shl i32 %66, %67
  %68 = load i32, i32* %m, align 4
  %cmp45 = icmp sgt i32 %shl, %68
  br i1 %cmp45, label %if.then47, label %if.end50

if.then47:                                        ; preds = %land.lhs.true
  %69 = load i32, i32* %m, align 4
  %70 = load i32, i32* %ifqstepShift, align 4
  %shr48 = ashr i32 %69, %70
  %71 = load i32*, i32** %scalefac, align 4
  %72 = load i32, i32* %sfb, align 4
  %arrayidx49 = getelementptr inbounds i32, i32* %71, i32 %72
  store i32 %shr48, i32* %arrayidx49, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.then47, %land.lhs.true, %if.end40
  br label %if.end52

if.else:                                          ; preds = %cond.end
  %73 = load i32*, i32** %scalefac, align 4
  %74 = load i32, i32* %sfb, align 4
  %arrayidx51 = getelementptr inbounds i32, i32* %73, i32 %74
  store i32 0, i32* %arrayidx51, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.else, %if.end50
  br label %for.inc53

for.inc53:                                        ; preds = %if.end52
  %75 = load i32, i32* %sfb, align 4
  %inc54 = add nsw i32 %75, 1
  store i32 %inc54, i32* %sfb, align 4
  br label %for.cond12

for.end55:                                        ; preds = %for.cond12
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc61, %for.end55
  %76 = load i32, i32* %sfb, align 4
  %cmp57 = icmp slt i32 %76, 39
  br i1 %cmp57, label %for.body59, label %for.end63

for.body59:                                       ; preds = %for.cond56
  %77 = load i32*, i32** %scalefac, align 4
  %78 = load i32, i32* %sfb, align 4
  %arrayidx60 = getelementptr inbounds i32, i32* %77, i32 %78
  store i32 0, i32* %arrayidx60, align 4
  br label %for.inc61

for.inc61:                                        ; preds = %for.body59
  %79 = load i32, i32* %sfb, align 4
  %inc62 = add nsw i32 %79, 1
  store i32 %inc62, i32* %sfb, align 4
  br label %for.cond56

for.end63:                                        ; preds = %for.cond56
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal float @vec_max_c(float* %xr34, i32 %bw) #0 {
entry:
  %xr34.addr = alloca float*, align 4
  %bw.addr = alloca i32, align 4
  %xfsf = alloca float, align 4
  %i = alloca i32, align 4
  %remaining = alloca i32, align 4
  store float* %xr34, float** %xr34.addr, align 4
  store i32 %bw, i32* %bw.addr, align 4
  store float 0.000000e+00, float* %xfsf, align 4
  %0 = load i32, i32* %bw.addr, align 4
  %shr = lshr i32 %0, 2
  store i32 %shr, i32* %i, align 4
  %1 = load i32, i32* %bw.addr, align 4
  %and = and i32 %1, 3
  store i32 %and, i32* %remaining, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end17, %entry
  %2 = load i32, i32* %i, align 4
  %dec = add i32 %2, -1
  store i32 %dec, i32* %i, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load float, float* %xfsf, align 4
  %4 = load float*, float** %xr34.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %4, i32 0
  %5 = load float, float* %arrayidx, align 4
  %cmp1 = fcmp olt float %3, %5
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %6 = load float*, float** %xr34.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx2, align 4
  store float %7, float* %xfsf, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %8 = load float, float* %xfsf, align 4
  %9 = load float*, float** %xr34.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 1
  %10 = load float, float* %arrayidx3, align 4
  %cmp4 = fcmp olt float %8, %10
  br i1 %cmp4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %11 = load float*, float** %xr34.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float, float* %arrayidx6, align 4
  store float %12, float* %xfsf, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %13 = load float, float* %xfsf, align 4
  %14 = load float*, float** %xr34.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx8, align 4
  %cmp9 = fcmp olt float %13, %15
  br i1 %cmp9, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.end7
  %16 = load float*, float** %xr34.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %16, i32 2
  %17 = load float, float* %arrayidx11, align 4
  store float %17, float* %xfsf, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then10, %if.end7
  %18 = load float, float* %xfsf, align 4
  %19 = load float*, float** %xr34.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %19, i32 3
  %20 = load float, float* %arrayidx13, align 4
  %cmp14 = fcmp olt float %18, %20
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.end12
  %21 = load float*, float** %xr34.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %21, i32 3
  %22 = load float, float* %arrayidx16, align 4
  store float %22, float* %xfsf, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then15, %if.end12
  %23 = load float*, float** %xr34.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %23, i32 4
  store float* %add.ptr, float** %xr34.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %24 = load i32, i32* %remaining, align 4
  switch i32 %24, label %sw.default [
    i32 3, label %sw.bb
    i32 2, label %sw.bb23
    i32 1, label %sw.bb29
  ]

sw.bb:                                            ; preds = %while.end
  %25 = load float, float* %xfsf, align 4
  %26 = load float*, float** %xr34.addr, align 4
  %arrayidx18 = getelementptr inbounds float, float* %26, i32 2
  %27 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp olt float %25, %27
  br i1 %cmp19, label %if.then20, label %if.end22

if.then20:                                        ; preds = %sw.bb
  %28 = load float*, float** %xr34.addr, align 4
  %arrayidx21 = getelementptr inbounds float, float* %28, i32 2
  %29 = load float, float* %arrayidx21, align 4
  store float %29, float* %xfsf, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %sw.bb
  br label %sw.bb23

sw.bb23:                                          ; preds = %while.end, %if.end22
  %30 = load float, float* %xfsf, align 4
  %31 = load float*, float** %xr34.addr, align 4
  %arrayidx24 = getelementptr inbounds float, float* %31, i32 1
  %32 = load float, float* %arrayidx24, align 4
  %cmp25 = fcmp olt float %30, %32
  br i1 %cmp25, label %if.then26, label %if.end28

if.then26:                                        ; preds = %sw.bb23
  %33 = load float*, float** %xr34.addr, align 4
  %arrayidx27 = getelementptr inbounds float, float* %33, i32 1
  %34 = load float, float* %arrayidx27, align 4
  store float %34, float* %xfsf, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %sw.bb23
  br label %sw.bb29

sw.bb29:                                          ; preds = %while.end, %if.end28
  %35 = load float, float* %xfsf, align 4
  %36 = load float*, float** %xr34.addr, align 4
  %arrayidx30 = getelementptr inbounds float, float* %36, i32 0
  %37 = load float, float* %arrayidx30, align 4
  %cmp31 = fcmp olt float %35, %37
  br i1 %cmp31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %sw.bb29
  %38 = load float*, float** %xr34.addr, align 4
  %arrayidx33 = getelementptr inbounds float, float* %38, i32 0
  %39 = load float, float* %arrayidx33, align 4
  store float %39, float* %xfsf, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %sw.bb29
  br label %sw.default

sw.default:                                       ; preds = %while.end, %if.end34
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default
  %40 = load float, float* %xfsf, align 4
  ret float %40
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i8 @find_lowest_scalefac(float %xr34) #0 {
entry:
  %xr34.addr = alloca float, align 4
  %sf_ok = alloca i8, align 1
  %sf = alloca i8, align 1
  %delsf = alloca i8, align 1
  %i = alloca i8, align 1
  %ixmax_val = alloca float, align 4
  %xfsf = alloca float, align 4
  store float %xr34, float* %xr34.addr, align 4
  store i8 -1, i8* %sf_ok, align 1
  store i8 -128, i8* %sf, align 1
  store i8 64, i8* %delsf, align 1
  store float 8.206000e+03, float* %ixmax_val, align 4
  store i8 0, i8* %i, align 1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8, i8* %i, align 1
  %conv = zext i8 %0 to i32
  %cmp = icmp slt i32 %conv, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8, i8* %sf, align 1
  %idxprom = zext i8 %1 to i32
  %arrayidx = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %idxprom
  %2 = load float, float* %arrayidx, align 4
  %3 = load float, float* %xr34.addr, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %xfsf, align 4
  %4 = load float, float* %xfsf, align 4
  %cmp2 = fcmp ole float %4, 8.206000e+03
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %5 = load i8, i8* %sf, align 1
  store i8 %5, i8* %sf_ok, align 1
  %6 = load i8, i8* %delsf, align 1
  %conv4 = zext i8 %6 to i32
  %7 = load i8, i8* %sf, align 1
  %conv5 = zext i8 %7 to i32
  %sub = sub nsw i32 %conv5, %conv4
  %conv6 = trunc i32 %sub to i8
  store i8 %conv6, i8* %sf, align 1
  br label %if.end

if.else:                                          ; preds = %for.body
  %8 = load i8, i8* %delsf, align 1
  %conv7 = zext i8 %8 to i32
  %9 = load i8, i8* %sf, align 1
  %conv8 = zext i8 %9 to i32
  %add = add nsw i32 %conv8, %conv7
  %conv9 = trunc i32 %add to i8
  store i8 %conv9, i8* %sf, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %10 = load i8, i8* %delsf, align 1
  %conv10 = zext i8 %10 to i32
  %shr = ashr i32 %conv10, 1
  %conv11 = trunc i32 %shr to i8
  store i8 %conv11, i8* %delsf, align 1
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i8, i8* %i, align 1
  %inc = add i8 %11, 1
  store i8 %inc, i8* %i, align 1
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load i8, i8* %sf_ok, align 1
  ret i8 %12
}

declare i32 @scale_bitcount(%struct.lame_internal_flags*, %struct.gr_info*) #4

; Function Attrs: noinline nounwind optnone
define internal void @quantize_x34(%struct.algo_s* %that) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %x = alloca [4 x double], align 16
  %xr34_orig = alloca float*, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %ifqstep = alloca i32, align 4
  %l3 = alloca i32*, align 4
  %j = alloca i32, align 4
  %sfb = alloca i32, align 4
  %max_nonzero_coeff = alloca i32, align 4
  %s = alloca i32, align 4
  %sfac = alloca i8, align 1
  %sfpow34 = alloca float, align 4
  %w = alloca i32, align 4
  %m = alloca i32, align 4
  %i = alloca i32, align 4
  %remaining = alloca i32, align 4
  %tmp_l3 = alloca [4 x i32], align 16
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %xr34orig = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 2
  %1 = load float*, float** %xr34orig, align 4
  store float* %1, float** %xr34_orig, align 4
  %2 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %2, i32 0, i32 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info1, align 4
  store %struct.gr_info* %3, %struct.gr_info** %cod_info, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 16
  %5 = load i32, i32* %scalefac_scale, align 4
  %cmp = icmp eq i32 %5, 0
  %6 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 2, i32 4
  store i32 %cond, i32* %ifqstep, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  store i32* %arraydecay, i32** %l3, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %sfb, align 4
  %8 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %max_nonzero_coeff2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 30
  %9 = load i32, i32* %max_nonzero_coeff2, align 4
  store i32 %9, i32* %max_nonzero_coeff, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %10 = load i32, i32* %j, align 4
  %11 = load i32, i32* %max_nonzero_coeff, align 4
  %cmp3 = icmp ule i32 %10, %11
  br i1 %cmp3, label %while.body, label %while.end76

while.body:                                       ; preds = %while.cond
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 2
  %13 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx, align 4
  %15 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 15
  %16 = load i32, i32* %preflag, align 4
  %tobool = icmp ne i32 %16, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %17 = load i32, i32* %sfb, align 4
  %arrayidx4 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx4, align 4
  br label %cond.end

cond.false:                                       ; preds = %while.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond5 = phi i32 [ %18, %cond.true ], [ 0, %cond.false ]
  %add = add nsw i32 %14, %cond5
  %19 = load i32, i32* %ifqstep, align 4
  %mul = mul nsw i32 %add, %19
  %20 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %20, i32 0, i32 12
  %21 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %window = getelementptr inbounds %struct.gr_info, %struct.gr_info* %21, i32 0, i32 26
  %22 = load i32, i32* %sfb, align 4
  %arrayidx6 = getelementptr inbounds [39 x i32], [39 x i32]* %window, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx6, align 4
  %arrayidx7 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx7, align 4
  %mul8 = mul nsw i32 %24, 8
  %add9 = add nsw i32 %mul, %mul8
  store i32 %add9, i32* %s, align 4
  %25 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 7
  %26 = load i32, i32* %global_gain, align 4
  %27 = load i32, i32* %s, align 4
  %sub = sub nsw i32 %26, %27
  %conv = trunc i32 %sub to i8
  store i8 %conv, i8* %sfac, align 1
  %28 = load i8, i8* %sfac, align 1
  %idxprom = zext i8 %28 to i32
  %arrayidx10 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %idxprom
  %29 = load float, float* %arrayidx10, align 4
  store float %29, float* %sfpow34, align 4
  %30 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %width = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 25
  %31 = load i32, i32* %sfb, align 4
  %arrayidx11 = getelementptr inbounds [39 x i32], [39 x i32]* %width, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx11, align 4
  store i32 %32, i32* %w, align 4
  %33 = load i32, i32* %max_nonzero_coeff, align 4
  %34 = load i32, i32* %j, align 4
  %sub12 = sub i32 %33, %34
  %add13 = add i32 %sub12, 1
  store i32 %add13, i32* %m, align 4
  %35 = load i32, i32* %w, align 4
  %36 = load i32, i32* %j, align 4
  %add14 = add i32 %36, %35
  store i32 %add14, i32* %j, align 4
  %37 = load i32, i32* %sfb, align 4
  %inc = add i32 %37, 1
  store i32 %inc, i32* %sfb, align 4
  %38 = load i32, i32* %w, align 4
  %39 = load i32, i32* %m, align 4
  %cmp15 = icmp ule i32 %38, %39
  br i1 %cmp15, label %cond.true17, label %cond.false18

cond.true17:                                      ; preds = %cond.end
  %40 = load i32, i32* %w, align 4
  br label %cond.end19

cond.false18:                                     ; preds = %cond.end
  %41 = load i32, i32* %m, align 4
  br label %cond.end19

cond.end19:                                       ; preds = %cond.false18, %cond.true17
  %cond20 = phi i32 [ %40, %cond.true17 ], [ %41, %cond.false18 ]
  store i32 %cond20, i32* %i, align 4
  %42 = load i32, i32* %i, align 4
  %and = and i32 %42, 3
  store i32 %and, i32* %remaining, align 4
  %43 = load i32, i32* %i, align 4
  %shr = lshr i32 %43, 2
  store i32 %shr, i32* %i, align 4
  br label %while.cond21

while.cond21:                                     ; preds = %while.body24, %cond.end19
  %44 = load i32, i32* %i, align 4
  %dec = add i32 %44, -1
  store i32 %dec, i32* %i, align 4
  %cmp22 = icmp ugt i32 %44, 0
  br i1 %cmp22, label %while.body24, label %while.end

while.body24:                                     ; preds = %while.cond21
  %45 = load float, float* %sfpow34, align 4
  %46 = load float*, float** %xr34_orig, align 4
  %arrayidx25 = getelementptr inbounds float, float* %46, i32 0
  %47 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %45, %47
  %conv27 = fpext float %mul26 to double
  %arrayidx28 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv27, double* %arrayidx28, align 16
  %48 = load float, float* %sfpow34, align 4
  %49 = load float*, float** %xr34_orig, align 4
  %arrayidx29 = getelementptr inbounds float, float* %49, i32 1
  %50 = load float, float* %arrayidx29, align 4
  %mul30 = fmul float %48, %50
  %conv31 = fpext float %mul30 to double
  %arrayidx32 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv31, double* %arrayidx32, align 8
  %51 = load float, float* %sfpow34, align 4
  %52 = load float*, float** %xr34_orig, align 4
  %arrayidx33 = getelementptr inbounds float, float* %52, i32 2
  %53 = load float, float* %arrayidx33, align 4
  %mul34 = fmul float %51, %53
  %conv35 = fpext float %mul34 to double
  %arrayidx36 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv35, double* %arrayidx36, align 16
  %54 = load float, float* %sfpow34, align 4
  %55 = load float*, float** %xr34_orig, align 4
  %arrayidx37 = getelementptr inbounds float, float* %55, i32 3
  %56 = load float, float* %arrayidx37, align 4
  %mul38 = fmul float %54, %56
  %conv39 = fpext float %mul38 to double
  %arrayidx40 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double %conv39, double* %arrayidx40, align 8
  %arraydecay41 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %57 = load i32*, i32** %l3, align 4
  call void @k_34_4(double* %arraydecay41, i32* %57)
  %58 = load i32*, i32** %l3, align 4
  %add.ptr = getelementptr inbounds i32, i32* %58, i32 4
  store i32* %add.ptr, i32** %l3, align 4
  %59 = load float*, float** %xr34_orig, align 4
  %add.ptr42 = getelementptr inbounds float, float* %59, i32 4
  store float* %add.ptr42, float** %xr34_orig, align 4
  br label %while.cond21

while.end:                                        ; preds = %while.cond21
  %60 = load i32, i32* %remaining, align 4
  %tobool43 = icmp ne i32 %60, 0
  br i1 %tobool43, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %arrayidx44 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 3
  store double 0.000000e+00, double* %arrayidx44, align 8
  %arrayidx45 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double 0.000000e+00, double* %arrayidx45, align 16
  %arrayidx46 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double 0.000000e+00, double* %arrayidx46, align 8
  %arrayidx47 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double 0.000000e+00, double* %arrayidx47, align 16
  %61 = load i32, i32* %remaining, align 4
  switch i32 %61, label %sw.epilog [
    i32 3, label %sw.bb
    i32 2, label %sw.bb52
    i32 1, label %sw.bb57
  ]

sw.bb:                                            ; preds = %if.then
  %62 = load float, float* %sfpow34, align 4
  %63 = load float*, float** %xr34_orig, align 4
  %arrayidx48 = getelementptr inbounds float, float* %63, i32 2
  %64 = load float, float* %arrayidx48, align 4
  %mul49 = fmul float %62, %64
  %conv50 = fpext float %mul49 to double
  %arrayidx51 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 2
  store double %conv50, double* %arrayidx51, align 16
  br label %sw.bb52

sw.bb52:                                          ; preds = %if.then, %sw.bb
  %65 = load float, float* %sfpow34, align 4
  %66 = load float*, float** %xr34_orig, align 4
  %arrayidx53 = getelementptr inbounds float, float* %66, i32 1
  %67 = load float, float* %arrayidx53, align 4
  %mul54 = fmul float %65, %67
  %conv55 = fpext float %mul54 to double
  %arrayidx56 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 1
  store double %conv55, double* %arrayidx56, align 8
  br label %sw.bb57

sw.bb57:                                          ; preds = %if.then, %sw.bb52
  %68 = load float, float* %sfpow34, align 4
  %69 = load float*, float** %xr34_orig, align 4
  %arrayidx58 = getelementptr inbounds float, float* %69, i32 0
  %70 = load float, float* %arrayidx58, align 4
  %mul59 = fmul float %68, %70
  %conv60 = fpext float %mul59 to double
  %arrayidx61 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  store double %conv60, double* %arrayidx61, align 16
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb57, %if.then
  %arraydecay62 = getelementptr inbounds [4 x double], [4 x double]* %x, i32 0, i32 0
  %arraydecay63 = getelementptr inbounds [4 x i32], [4 x i32]* %tmp_l3, i32 0, i32 0
  call void @k_34_4(double* %arraydecay62, i32* %arraydecay63)
  %71 = load i32, i32* %remaining, align 4
  switch i32 %71, label %sw.epilog73 [
    i32 3, label %sw.bb64
    i32 2, label %sw.bb67
    i32 1, label %sw.bb70
  ]

sw.bb64:                                          ; preds = %sw.epilog
  %arrayidx65 = getelementptr inbounds [4 x i32], [4 x i32]* %tmp_l3, i32 0, i32 2
  %72 = load i32, i32* %arrayidx65, align 8
  %73 = load i32*, i32** %l3, align 4
  %arrayidx66 = getelementptr inbounds i32, i32* %73, i32 2
  store i32 %72, i32* %arrayidx66, align 4
  br label %sw.bb67

sw.bb67:                                          ; preds = %sw.epilog, %sw.bb64
  %arrayidx68 = getelementptr inbounds [4 x i32], [4 x i32]* %tmp_l3, i32 0, i32 1
  %74 = load i32, i32* %arrayidx68, align 4
  %75 = load i32*, i32** %l3, align 4
  %arrayidx69 = getelementptr inbounds i32, i32* %75, i32 1
  store i32 %74, i32* %arrayidx69, align 4
  br label %sw.bb70

sw.bb70:                                          ; preds = %sw.epilog, %sw.bb67
  %arrayidx71 = getelementptr inbounds [4 x i32], [4 x i32]* %tmp_l3, i32 0, i32 0
  %76 = load i32, i32* %arrayidx71, align 16
  %77 = load i32*, i32** %l3, align 4
  %arrayidx72 = getelementptr inbounds i32, i32* %77, i32 0
  store i32 %76, i32* %arrayidx72, align 4
  br label %sw.epilog73

sw.epilog73:                                      ; preds = %sw.bb70, %sw.epilog
  %78 = load i32, i32* %remaining, align 4
  %79 = load i32*, i32** %l3, align 4
  %add.ptr74 = getelementptr inbounds i32, i32* %79, i32 %78
  store i32* %add.ptr74, i32** %l3, align 4
  %80 = load i32, i32* %remaining, align 4
  %81 = load float*, float** %xr34_orig, align 4
  %add.ptr75 = getelementptr inbounds float, float* %81, i32 %80
  store float* %add.ptr75, float** %xr34_orig, align 4
  br label %if.end

if.end:                                           ; preds = %sw.epilog73, %while.end
  br label %while.cond

while.end76:                                      ; preds = %while.cond
  ret void
}

declare i32 @noquant_count_bits(%struct.lame_internal_flags*, %struct.gr_info*, %struct.calc_noise_data_t*) #4

declare void @best_scalefac_store(%struct.lame_internal_flags*, i32, i32, %struct.III_side_info_t*) #4

declare void @best_huffman_divide(%struct.lame_internal_flags*, %struct.gr_info*) #4

; Function Attrs: noinline nounwind optnone
define internal i32 @sfDepth(i32* %sfwork) #0 {
entry:
  %sfwork.addr = alloca i32*, align 4
  %m = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %di = alloca i32, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32 0, i32* %m, align 4
  store i32 39, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %j, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32*, i32** %sfwork.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  %sub = sub nsw i32 255, %3
  store i32 %sub, i32* %di, align 4
  %4 = load i32, i32* %m, align 4
  %5 = load i32, i32* %di, align 4
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %di, align 4
  store i32 %6, i32* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %j, align 4
  %dec = add i32 %7, -1
  store i32 %dec, i32* %j, align 4
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i32, i32* %m, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i32 @flattenDistribution(i32* %sfwork, i32* %sf_out, i32 %dm, i32 %k, i32 %p) #0 {
entry:
  %sfwork.addr = alloca i32*, align 4
  %sf_out.addr = alloca i32*, align 4
  %dm.addr = alloca i32, align 4
  %k.addr = alloca i32, align 4
  %p.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca i32, align 4
  %sfmax = alloca i32, align 4
  %di = alloca i32, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32* %sf_out, i32** %sf_out.addr, align 4
  store i32 %dm, i32* %dm.addr, align 4
  store i32 %k, i32* %k.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i32 0, i32* %sfmax, align 4
  %0 = load i32, i32* %dm.addr, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else12

if.then:                                          ; preds = %entry
  store i32 39, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %1 = load i32, i32* %j, align 4
  %cmp1 = icmp ugt i32 %1, 0
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %p.addr, align 4
  %3 = load i32*, i32** %sfwork.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %sub = sub nsw i32 %2, %5
  store i32 %sub, i32* %di, align 4
  %6 = load i32*, i32** %sfwork.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %6, i32 %7
  %8 = load i32, i32* %arrayidx2, align 4
  %9 = load i32, i32* %k.addr, align 4
  %10 = load i32, i32* %di, align 4
  %mul = mul nsw i32 %9, %10
  %11 = load i32, i32* %dm.addr, align 4
  %div = sdiv i32 %mul, %11
  %add = add nsw i32 %8, %div
  store i32 %add, i32* %x, align 4
  %12 = load i32, i32* %x, align 4
  %cmp3 = icmp slt i32 %12, 0
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %for.body
  store i32 0, i32* %x, align 4
  br label %if.end7

if.else:                                          ; preds = %for.body
  %13 = load i32, i32* %x, align 4
  %cmp5 = icmp sgt i32 %13, 255
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else
  store i32 255, i32* %x, align 4
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then4
  %14 = load i32, i32* %x, align 4
  %15 = load i32*, i32** %sf_out.addr, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i32, i32* %15, i32 %16
  store i32 %14, i32* %arrayidx8, align 4
  %17 = load i32, i32* %sfmax, align 4
  %18 = load i32, i32* %x, align 4
  %cmp9 = icmp slt i32 %17, %18
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  %19 = load i32, i32* %x, align 4
  store i32 %19, i32* %sfmax, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.end7
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %20 = load i32, i32* %j, align 4
  %dec = add i32 %20, -1
  store i32 %dec, i32* %j, align 4
  %21 = load i32, i32* %i, align 4
  %inc = add i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end25

if.else12:                                        ; preds = %entry
  store i32 39, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc21, %if.else12
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp ugt i32 %22, 0
  br i1 %cmp14, label %for.body15, label %for.end24

for.body15:                                       ; preds = %for.cond13
  %23 = load i32*, i32** %sfwork.addr, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %23, i32 %24
  %25 = load i32, i32* %arrayidx16, align 4
  store i32 %25, i32* %x, align 4
  %26 = load i32, i32* %x, align 4
  %27 = load i32*, i32** %sf_out.addr, align 4
  %28 = load i32, i32* %i, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %27, i32 %28
  store i32 %26, i32* %arrayidx17, align 4
  %29 = load i32, i32* %sfmax, align 4
  %30 = load i32, i32* %x, align 4
  %cmp18 = icmp slt i32 %29, %30
  br i1 %cmp18, label %if.then19, label %if.end20

if.then19:                                        ; preds = %for.body15
  %31 = load i32, i32* %x, align 4
  store i32 %31, i32* %sfmax, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then19, %for.body15
  br label %for.inc21

for.inc21:                                        ; preds = %if.end20
  %32 = load i32, i32* %j, align 4
  %dec22 = add i32 %32, -1
  store i32 %dec22, i32* %j, align 4
  %33 = load i32, i32* %i, align 4
  %inc23 = add i32 %33, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond13

for.end24:                                        ; preds = %for.cond13
  br label %if.end25

if.end25:                                         ; preds = %for.end24, %for.end
  %34 = load i32, i32* %sfmax, align 4
  ret i32 %34
}

; Function Attrs: noinline nounwind optnone
define internal i32 @tryThatOne(%struct.algo_s* %that, i32* %sftemp, i32* %vbrsfmin, i32 %vbrmax) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %sftemp.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %vbrmax.addr = alloca i32, align 4
  %xrpow_max = alloca float, align 4
  %nbits = alloca i32, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %sftemp, i32** %sftemp.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %vbrmax, i32* %vbrmax.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %xrpow_max1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 3
  %2 = load float, float* %xrpow_max1, align 4
  store float %2, float* %xrpow_max, align 4
  store i32 100000, i32* %nbits, align 4
  %3 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %alloc = getelementptr inbounds %struct.algo_s, %struct.algo_s* %3, i32 0, i32 0
  %4 = load void (%struct.algo_s*, i32*, i32*, i32)*, void (%struct.algo_s*, i32*, i32*, i32)** %alloc, align 4
  %5 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %6 = load i32*, i32** %sftemp.addr, align 4
  %7 = load i32*, i32** %vbrsfmin.addr, align 4
  %8 = load i32, i32* %vbrmax.addr, align 4
  call void %4(%struct.algo_s* %5, i32* %6, i32* %7, i32 %8)
  %9 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  call void @bitcount(%struct.algo_s* %9)
  %10 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %call = call i32 @quantizeAndCountBits(%struct.algo_s* %10)
  store i32 %call, i32* %nbits, align 4
  %11 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info2 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %11, i32 0, i32 4
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info2, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 18
  %13 = load i32, i32* %part2_length, align 4
  %14 = load i32, i32* %nbits, align 4
  %add = add nsw i32 %14, %13
  store i32 %add, i32* %nbits, align 4
  %15 = load float, float* %xrpow_max, align 4
  %16 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info3 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %16, i32 0, i32 4
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info3, align 4
  %xrpow_max4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 3
  store float %15, float* %xrpow_max4, align 4
  %18 = load i32, i32* %nbits, align 4
  ret i32 %18
}

; Function Attrs: noinline nounwind optnone
define internal void @searchGlobalStepsizeMax(%struct.algo_s* %that, i32* %sfwork, i32* %vbrsfmin, i32 %target) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %sfwork.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %target.addr = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %gain = alloca i32, align 4
  %curr = alloca i32, align 4
  %gain_ok = alloca i32, align 4
  %nbits = alloca i32, align 4
  %l = alloca i32, align 4
  %r = alloca i32, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %target, i32* %target.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info1 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info1, align 4
  store %struct.gr_info* %1, %struct.gr_info** %cod_info, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 7
  %3 = load i32, i32* %global_gain, align 4
  store i32 %3, i32* %gain, align 4
  %4 = load i32, i32* %gain, align 4
  store i32 %4, i32* %curr, align 4
  store i32 1024, i32* %gain_ok, align 4
  store i32 100000, i32* %nbits, align 4
  %5 = load i32, i32* %gain, align 4
  store i32 %5, i32* %l, align 4
  store i32 512, i32* %r, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end9, %entry
  %6 = load i32, i32* %l, align 4
  %7 = load i32, i32* %r, align 4
  %cmp = icmp sle i32 %6, %7
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %l, align 4
  %9 = load i32, i32* %r, align 4
  %add = add nsw i32 %8, %9
  %shr = ashr i32 %add, 1
  store i32 %shr, i32* %curr, align 4
  %10 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %11 = load i32*, i32** %sfwork.addr, align 4
  %12 = load i32*, i32** %vbrsfmin.addr, align 4
  %13 = load i32, i32* %curr, align 4
  %14 = load i32, i32* %gain, align 4
  %sub = sub nsw i32 %13, %14
  %call = call i32 @tryGlobalStepsize(%struct.algo_s* %10, i32* %11, i32* %12, i32 %sub)
  store i32 %call, i32* %nbits, align 4
  %15 = load i32, i32* %nbits, align 4
  %cmp2 = icmp eq i32 %15, 0
  br i1 %cmp2, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.body
  %16 = load i32, i32* %nbits, align 4
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 18
  %18 = load i32, i32* %part2_length, align 4
  %add3 = add nsw i32 %16, %18
  %19 = load i32, i32* %target.addr, align 4
  %cmp4 = icmp slt i32 %add3, %19
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %while.body
  %20 = load i32, i32* %curr, align 4
  %sub5 = sub nsw i32 %20, 1
  store i32 %sub5, i32* %r, align 4
  %21 = load i32, i32* %curr, align 4
  store i32 %21, i32* %gain_ok, align 4
  br label %if.end9

if.else:                                          ; preds = %lor.lhs.false
  %22 = load i32, i32* %curr, align 4
  %add6 = add nsw i32 %22, 1
  store i32 %add6, i32* %l, align 4
  %23 = load i32, i32* %gain_ok, align 4
  %cmp7 = icmp eq i32 %23, 1024
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %if.else
  %24 = load i32, i32* %curr, align 4
  store i32 %24, i32* %gain_ok, align 4
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.else
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %25 = load i32, i32* %gain_ok, align 4
  %26 = load i32, i32* %curr, align 4
  %cmp10 = icmp ne i32 %25, %26
  br i1 %cmp10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %while.end
  %27 = load i32, i32* %gain_ok, align 4
  store i32 %27, i32* %curr, align 4
  %28 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %29 = load i32*, i32** %sfwork.addr, align 4
  %30 = load i32*, i32** %vbrsfmin.addr, align 4
  %31 = load i32, i32* %curr, align 4
  %32 = load i32, i32* %gain, align 4
  %sub12 = sub nsw i32 %31, %32
  %call13 = call i32 @tryGlobalStepsize(%struct.algo_s* %28, i32* %29, i32* %30, i32 %sub12)
  store i32 %call13, i32* %nbits, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %while.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @tryGlobalStepsize(%struct.algo_s* %that, i32* %sfwork, i32* %vbrsfmin, i32 %delta) #0 {
entry:
  %that.addr = alloca %struct.algo_s*, align 4
  %sfwork.addr = alloca i32*, align 4
  %vbrsfmin.addr = alloca i32*, align 4
  %delta.addr = alloca i32, align 4
  %xrpow_max = alloca float, align 4
  %sftemp = alloca [39 x i32], align 16
  %i = alloca i32, align 4
  %nbits = alloca i32, align 4
  %gain = alloca i32, align 4
  %vbrmax = alloca i32, align 4
  store %struct.algo_s* %that, %struct.algo_s** %that.addr, align 4
  store i32* %sfwork, i32** %sfwork.addr, align 4
  store i32* %vbrsfmin, i32** %vbrsfmin.addr, align 4
  store i32 %delta, i32* %delta.addr, align 4
  %0 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info = getelementptr inbounds %struct.algo_s, %struct.algo_s* %0, i32 0, i32 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %xrpow_max1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 3
  %2 = load float, float* %xrpow_max1, align 4
  store float %2, float* %xrpow_max, align 4
  store i32 0, i32* %vbrmax, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %3, 39
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %sfwork.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  %7 = load i32, i32* %delta.addr, align 4
  %add = add nsw i32 %6, %7
  store i32 %add, i32* %gain, align 4
  %8 = load i32, i32* %gain, align 4
  %9 = load i32*, i32** %vbrsfmin.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx2, align 4
  %cmp3 = icmp slt i32 %8, %11
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %12 = load i32*, i32** %vbrsfmin.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %12, i32 %13
  %14 = load i32, i32* %arrayidx4, align 4
  store i32 %14, i32* %gain, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %15 = load i32, i32* %gain, align 4
  %cmp5 = icmp sgt i32 %15, 255
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store i32 255, i32* %gain, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end
  %16 = load i32, i32* %vbrmax, align 4
  %17 = load i32, i32* %gain, align 4
  %cmp8 = icmp slt i32 %16, %17
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end7
  %18 = load i32, i32* %gain, align 4
  store i32 %18, i32* %vbrmax, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.end7
  %19 = load i32, i32* %gain, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [39 x i32], [39 x i32]* %sftemp, i32 0, i32 %20
  store i32 %19, i32* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end10
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %alloc = getelementptr inbounds %struct.algo_s, %struct.algo_s* %22, i32 0, i32 0
  %23 = load void (%struct.algo_s*, i32*, i32*, i32)*, void (%struct.algo_s*, i32*, i32*, i32)** %alloc, align 4
  %24 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %sftemp, i32 0, i32 0
  %25 = load i32*, i32** %vbrsfmin.addr, align 4
  %26 = load i32, i32* %vbrmax, align 4
  call void %23(%struct.algo_s* %24, i32* %arraydecay, i32* %25, i32 %26)
  %27 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  call void @bitcount(%struct.algo_s* %27)
  %28 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %call = call i32 @quantizeAndCountBits(%struct.algo_s* %28)
  store i32 %call, i32* %nbits, align 4
  %29 = load float, float* %xrpow_max, align 4
  %30 = load %struct.algo_s*, %struct.algo_s** %that.addr, align 4
  %cod_info12 = getelementptr inbounds %struct.algo_s, %struct.algo_s* %30, i32 0, i32 4
  %31 = load %struct.gr_info*, %struct.gr_info** %cod_info12, align 4
  %xrpow_max13 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %31, i32 0, i32 3
  store float %29, float* %xrpow_max13, align 4
  %32 = load i32, i32* %nbits, align 4
  ret i32 %32
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind readnone speculatable willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
