; ModuleID = 'newmdct.c'
source_filename = "newmdct.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque

@order = internal constant [32 x i32] [i32 0, i32 1, i32 16, i32 17, i32 8, i32 9, i32 24, i32 25, i32 4, i32 5, i32 20, i32 21, i32 12, i32 13, i32 28, i32 29, i32 2, i32 3, i32 18, i32 19, i32 10, i32 11, i32 26, i32 27, i32 6, i32 7, i32 22, i32 23, i32 14, i32 15, i32 30, i32 31], align 16
@win = internal constant [4 x [36 x float]] [[36 x float] [float 0x3D50C36000000000, float 0x3D66999980000000, float 0x3D7089C420000000, float 0x3D73BEFF80000000, float 0x3D74D38B00000000, float 0x3D73BEFF80000000, float 0x3D7089C420000000, float 0x3D66999980000000, float 0x3D50C36000000000, float 0x3D97FF0A60000000, float 0x3D95753FC0000000, float 0x3D92A65240000000, float 0x3D8F5033E0000000, float 0x3D8923BDE0000000, float 0x3D82F747C0000000, float 0x3D79F5AE80000000, float 0x3D6D73F0A0000000, float 0x3D524B36E0000000, float 0xBD97FF0A60000000, float 0xBD95753FC0000000, float 0xBD92A65240000000, float 0xBD8F5033E0000000, float 0xBD8923BDE0000000, float 0xBD82F747C0000000, float 0xBD79F5AE80000000, float 0xBD6D73F0A0000000, float 0xBD524B36E0000000, float 0xBD50C36000000000, float 0xBD66999980000000, float 0xBD7089C420000000, float 0xBD73BEFF80000000, float 0xBD74D38B00000000, float 0xBD73BEFF80000000, float 0xBD7089C420000000, float 0xBD66999980000000, float 0xBD50C36000000000], [36 x float] [float 0x3D50C36000000000, float 0x3D66999980000000, float 0x3D7089C420000000, float 0x3D73BEFF80000000, float 0x3D74D38B00000000, float 0x3D73BEFF80000000, float 0x3D7089C420000000, float 0x3D66999980000000, float 0x3D50C36000000000, float 0x3D97FF0A60000000, float 0x3D95753FC0000000, float 0x3D92A65240000000, float 0x3D8F5033E0000000, float 0x3D8923BDE0000000, float 0x3D82F747C0000000, float 0x3D79F5AE80000000, float 0x3D6D73F0A0000000, float 0x3D524B36E0000000, float 0xBD9804E4A0000000, float 0xBD95A4A660000000, float 0xBD931A3D40000000, float 0xBD906A9C60000000, float 0xBD8B35FF20000000, float 0xBD8561C160000000, float 0xBD7E844CC0000000, float 0xBD71263920000000, float 0xBD53AF6940000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0xBD50120900000000, float 0xBD5C69F5C0000000, float 0xBD4E35C0A0000000], [36 x float] [float 0x3FC0D9FD40000000, float 0x3FDA8279A0000000, float 0x3FE88DF160000000, float 0x3FF175FFE0000000, float 0x3FF4DA02E0000000, float 0x3FF91D6EA0000000, float 0x3FFEBC57C0000000, float 0x4003504F40000000, float 0x40095F6D20000000, float 0x40120AF720000000, float 0x401E620D60000000, float 0x4036E75D20000000, float 0x3FEF838B80000000, float 0x3FE491B760000000, float 0x3FD5E3A880000000, float 0x3FEE11F640000000, float 0xBFC63A1A80000000, float 0xBFE8836FA0000000, float 0x3FEBB67AE0000000, float 5.000000e-01, float 0xBFE076BFC0000000, float 0xBFDE30DB40000000, float 0xBFD40E6040000000, float 0xBFC748EE80000000, float 0xBFB83603A0000000, float 0xBFA4F970E0000000, float 0xBF8D1423A0000000, float 0xBF6E4F68C0000000, float 0x3FEB709500000000, float 0x3FEC373B00000000, float 0x3FEE635BA0000000, float 0x3FEF775020000000, float 0x3FEFDB4820000000, float 0x3FEFF91FA0000000, float 0x3FEFFF2CA0000000, float 0x3FEFFFF1A0000000], [36 x float] [float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00, float 0x3D50120900000000, float 0x3D5C69F5C0000000, float 0x3D4E35C0A0000000, float 0x3D9804E4A0000000, float 0x3D95A4A660000000, float 0x3D931A3D40000000, float 0x3D906A9C60000000, float 0x3D8B35FF20000000, float 0x3D8561C160000000, float 0x3D7E844CC0000000, float 0x3D71263920000000, float 0x3D53AF6940000000, float 0xBD97FF0A60000000, float 0xBD95753FC0000000, float 0xBD92A65240000000, float 0xBD8F5033E0000000, float 0xBD8923BDE0000000, float 0xBD82F747C0000000, float 0xBD79F5AE80000000, float 0xBD6D73F0A0000000, float 0xBD524B36E0000000, float 0xBD50C36000000000, float 0xBD66999980000000, float 0xBD7089C420000000, float 0xBD73BEFF80000000, float 0xBD74D38B00000000, float 0xBD73BEFF80000000, float 0xBD7089C420000000, float 0xBD66999980000000, float 0xBD50C36000000000]], align 16
@enwindow = internal constant [285 x float] [float 0xBFC2F9EF20000000, float 0x4040277140000000, float 0x4072867480000000, float 0x408B991360000000, float 0x40C5B4F940000000, float 0x40908515E0000000, float 0x40731BD800000000, float 0x403ED37020000000, float 0x400ED398E0000000, float 0x404DB6E9A0000000, float 0x40862CB840000000, float 0x40B4A1E940000000, float 0xC0B6C5AA40000000, float 0xC0898D08E0000000, float 0xC0533AA920000000, float 0xC012608840000000, float 0x3FED00CBC0000000, float 0x3FC917A6C0000000, float 0xBFC3CC2060000000, float 0x4041298580000000, float 0x4072DCE880000000, float 0x40898CCC00000000, float 0x40C69AE5E0000000, float 0x4092750220000000, float 0x4074198EA0000000, float 0x403F3B65C0000000, float 0x400DAF8880000000, float 0x404AD35240000000, float 0x40856156E0000000, float 0x40B4689100000000, float 0xC0B8DE6460000000, float 0xC08C67D020000000, float 0xC056752A00000000, float 0xC015A54560000000, float 0x3FEA430020000000, float 0x3FD8F8B840000000, float 0xBFC4921BE0000000, float 0x404212D200000000, float 0x407301E400000000, float 0x4086E5F180000000, float 0x40C76ACE20000000, float 0x4094500CA0000000, float 0x4074F48F00000000, float 0x403F7CF820000000, float 0x400AFDB2A0000000, float 0x40479DBA20000000, float 0x408465E5C0000000, float 0x40B40C6A00000000, float 0xC0BAFD1640000000, float 0xC08F4FFE00000000, float 0xC059E7CC80000000, float 0xC0186B6F20000000, float 0x3FE7BB99E0000000, float 0x3FE2940620000000, float 0xBFC54B67A0000000, float 0x4042E03620000000, float 0x4072F02D40000000, float 0x40839FCC40000000, float 0x40C82361C0000000, float 0x4096111CA0000000, float 0x4075ABFEC0000000, float 0x403F992960000000, float 0x4009476680000000, float 0x40444A15A0000000, float 0x4083415640000000, float 0x40B38FD540000000, float 0xC0BD1E0A60000000, float 0xC0911F2F20000000, float 0xC05D8F9FA0000000, float 0xC01B461CE0000000, float 0x3FE561B820000000, float 0x3FE87DE2A0000000, float 0xBFC5F79160000000, float 0x40438EC960000000, float 0x4072A57620000000, float 0x407F786A20000000, float 0x40C8C3CB80000000, float 0x4097B25420000000, float 0x40763FC2E0000000, float 0x403F652E80000000, float 0x400754D4A0000000, float 0x4040E609A0000000, float 0x4081F727C0000000, float 0x40B2F53C20000000, float 0xC0BF3D9200000000, float 0xC092970CC0000000, float 0xC060B49FA0000000, float 0xC01EE19040000000, float 0x3FE32E1880000000, float 0x3FEE2B5D40000000, float 0xBFC6962F00000000, float 0x40441C0160000000, float 0x40721FF8A0000000, float 0x4076752960000000, float 0x40C94B2120000000, float 0x40993067E0000000, float 0x4076AD9C60000000, float 0x403F0BC180000000, float 0x400693E920000000, float 0x403AFD2620000000, float 0x40808F3280000000, float 0x40B23F5F40000000, float 0xC0C0ABF320000000, float 0xC0940A5C00000000, float 0xC062B2BCC0000000, float 0xC021497820000000, float 0x3FE11AB720000000, float 0x3FF1C73B40000000, float 0xBFD726DE80000000, float 0x404485ABA0000000, float 0x407158BDC0000000, float 0x4068746F60000000, float 0x40C9B8B820000000, float 0x409A8692C0000000, float 0x4076F68EC0000000, float 0x403E8E9A00000000, float 0x4004407520000000, float 0x403411F5A0000000, float 0x407E180880000000, float 0x40B170F860000000, float 0xC0C1B4AF40000000, float 0xC095768380000000, float 0xC064C55040000000, float 0xC0232A7100000000, float 0x3FDE450E00000000, float 0x3FF44CF320000000, float 0xBFD7A946E0000000, float 0x4044B254E0000000, float 0x407054B280000000, float 0x4029DEF6C0000000, float 0x40CA0C10E0000000, float 0x409BB208E0000000, float 0x40771C3EE0000000, float 0x403DC056C0000000, float 0x400337F320000000, float 0x402A9C0E20000000, float 0x407AE8FC40000000, float 0x40B08D0E80000000, float 0xC0C2B6F740000000, float 0xC096D6C400000000, float 0xC066E41320000000, float 0xC0256F9F00000000, float 0x3FDA8279A0000000, float 0x3FF6A09E60000000, float 0xBFD81D1760000000, float 0x4044CF4B60000000, float 0x406E21BC60000000, float 0xC0677E4B00000000, float 0x40CA450820000000, float 0x409CB09DA0000000, float 0x407717DAA0000000, float 0x403D007920000000, float 0x4000926200000000, float 0x401B1E4D20000000, float 0x40779C6DC0000000, float 0x40AF2CC5E0000000, float 0xC0C3B0DAC0000000, float 0xC09827CA20000000, float 0xC0690C0AA0000000, float 0xC027BA9D60000000, float 0x3FD6E64A00000000, float 0x3FF8BC8060000000, float 0xBFD88208E0000000, float 0x4044936920000000, float 0x406B0F4240000000, float 0xC0796282E0000000, float 0x40CA639180000000, float 0x409D7F3B00000000, float 0x4076F4D720000000, float 0x403C22DEE0000000, float 0x3FFE9F4160000000, float 0x3FD88208E0000000, float 0x40743DA8A0000000, float 0x40AD204B60000000, float 0xC0C4A09DE0000000, float 0xC09964CF80000000, float 0xC06B340200000000, float 0xC02A07E6C0000000, float 0x3FD36A0840000000, float 0x3FFA9B6620000000, float 0xBFE2A1E5C0000000, float 0x40442D9E40000000, float 0x4067866C40000000, float 0xC084022A60000000, float 0x40CA679B80000000, float 0x409E1E68C0000000, float 0x4076AD1DC0000000, float 0x403AF804C0000000, float 0x3FFBF12E00000000, float 0xC016818B00000000, float 0x4070D4D3A0000000, float 0x40AAFAC5E0000000, float 0xC0C58412A0000000, float 0xC09A8AA0C0000000, float 0xC06D588200000000, float 0xC02C53B820000000, float 0x3FD007FA80000000, float 0x3FFC38B300000000, float 0xBFE2D6C720000000, float 0x40439DFD40000000, float 0x40637859C0000000, float 0xC08BC9FE80000000, float 0x40CA51BC40000000, float 0x409E8C36C0000000, float 0x4076471F20000000, float 0x4039E51BA0000000, float 0x3FF91CAF80000000, float 0xC0265D2720000000, float 0x406AD9E200000000, float 0x40A8C25400000000, float 0xC0C659A920000000, float 0xC09B94A2A0000000, float 0xC06F760D00000000, float 0xC02EFEC020000000, float 0x3FC975F5E0000000, float 0x3FFD906BC0000000, float 0xBFE951FCC0000000, float 0x4042B26480000000, float 0x405DECC460000000, float 0xC091FC6200000000, float 0x40CA223920000000, float 0x409EC994C0000000, float 0x4075C05040000000, float 0x4038BB42C0000000, float 0x3FF6290380000000, float 0xC0306BE180000000, float 0x4064219C00000000, float 0x40A67CAA20000000, float 0xC0C71F7EE0000000, float 0xC09C7FF180000000, float 0xC070C49160000000, float 0xC030D13700000000, float 0x3FC2FCAC80000000, float 0x3FFE9F4160000000, float 0xBFE9795BE0000000, float 0x40419DB920000000, float 0x4054013BE0000000, float 0xC0964015C0000000, float 0x40C9D9E5A0000000, float 0x409ED73760000000, float 0x40751FDAA0000000, float 0x40374A0660000000, float 0x3FF64B78A0000000, float 0xC035198400000000, float 0x405B1EF160000000, float 0x40A42F66C0000000, float 0xC0C7D3DD00000000, float 0xC09D491CC0000000, float 0xC071C3F280000000, float 0xC0321D1B40000000, float 0x3FB936BB80000000, float 0x3FFF6297C0000000, float 0xBFEFF621E0000000, float 0x4040481300000000, float 0x40427AABC0000000, float 0xC09AA84D40000000, float 0x40C979A4C0000000, float 0x409EB58840000000, float 0x407466E8C0000000, float 0x40362CCE00000000, float 0x3FF32D9F60000000, float 0xC0395F0A20000000, float 0x404CC47CE0000000, float 0x40A1E0D680000000, float 0xC0C8755660000000, float 0xC09DEB5C40000000, float 0xC072B432E0000000, float 0xC03360C840000000, float 0x3FA9272780000000, float 0x3FFFD88DA0000000, float 0x40C4BA66C0000000, float 0x40B4B62880000000, float 0x408D0E35E0000000, float 0x4086C668A0000000, float 0x4072019000000000, float 0x40503AB680000000, float 0x403E200040000000, float 0x401067E440000000, float 0x40C9026620000000, float 0x409E6634E0000000, float 0x407396CAC0000000, float 0x4034CD3540000000, float 0x409F2C9F60000000, float 0x4022006E00000000, float 0xC03D33C220000000], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @mdct_sub48(%struct.lame_internal_flags* %gfc, float* %w0, float* %w1) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %w0.addr = alloca float*, align 4
  %w1.addr = alloca float*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %gr = alloca i32, align 4
  %k = alloca i32, align 4
  %ch = alloca i32, align 4
  %wk = alloca float*, align 4
  %band = alloca i32, align 4
  %gi = alloca %struct.gr_info*, align 4
  %mdct_enc = alloca float*, align 4
  %samp = alloca float*, align 4
  %type = alloca i32, align 4
  %band0 = alloca float*, align 4
  %band1 = alloca float*, align 4
  %w = alloca float, align 4
  %work = alloca [18 x float], align 16
  %a = alloca float, align 4
  %b = alloca float, align 4
  %bu = alloca float, align 4
  %bd = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %w0, float** %w0.addr, align 4
  store float* %w1, float** %w1.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load float*, float** %w0.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %2, i32 286
  store float* %add.ptr, float** %wk, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc250, %entry
  %3 = load i32, i32* %ch, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 14
  %5 = load i32, i32* %channels_out, align 4
  %cmp = icmp slt i32 %3, %5
  br i1 %cmp, label %for.body, label %for.end252

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc233, %for.body
  %6 = load i32, i32* %gr, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 15
  %8 = load i32, i32* %mode_gr, align 4
  %cmp3 = icmp slt i32 %6, %8
  br i1 %cmp3, label %for.body4, label %for.end235

for.body4:                                        ; preds = %for.cond2
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %10 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %10
  %11 = load i32, i32* %ch, align 4
  %arrayidx5 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %11
  store %struct.gr_info* %arrayidx5, %struct.gr_info** %gi, align 4
  %12 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 0
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 0
  store float* %arraydecay, float** %mdct_enc, align 4
  %13 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %sb_sample = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %13, i32 0, i32 0
  %14 = load i32, i32* %ch, align 4
  %arrayidx6 = getelementptr inbounds [2 x [2 x [18 x [32 x float]]]], [2 x [2 x [18 x [32 x float]]]]* %sb_sample, i32 0, i32 %14
  %15 = load i32, i32* %gr, align 4
  %sub = sub nsw i32 1, %15
  %arrayidx7 = getelementptr inbounds [2 x [18 x [32 x float]]], [2 x [18 x [32 x float]]]* %arrayidx6, i32 0, i32 %sub
  %arrayidx8 = getelementptr inbounds [18 x [32 x float]], [18 x [32 x float]]* %arrayidx7, i32 0, i32 0
  %arraydecay9 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx8, i32 0, i32 0
  store float* %arraydecay9, float** %samp, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc22, %for.body4
  %16 = load i32, i32* %k, align 4
  %cmp11 = icmp slt i32 %16, 9
  br i1 %cmp11, label %for.body12, label %for.end23

for.body12:                                       ; preds = %for.cond10
  %17 = load float*, float** %wk, align 4
  %18 = load float*, float** %samp, align 4
  call void @window_subband(float* %17, float* %18)
  %19 = load float*, float** %wk, align 4
  %add.ptr13 = getelementptr inbounds float, float* %19, i32 32
  %20 = load float*, float** %samp, align 4
  %add.ptr14 = getelementptr inbounds float, float* %20, i32 32
  call void @window_subband(float* %add.ptr13, float* %add.ptr14)
  %21 = load float*, float** %samp, align 4
  %add.ptr15 = getelementptr inbounds float, float* %21, i32 64
  store float* %add.ptr15, float** %samp, align 4
  %22 = load float*, float** %wk, align 4
  %add.ptr16 = getelementptr inbounds float, float* %22, i32 64
  store float* %add.ptr16, float** %wk, align 4
  store i32 1, i32* %band, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body12
  %23 = load i32, i32* %band, align 4
  %cmp18 = icmp slt i32 %23, 32
  br i1 %cmp18, label %for.body19, label %for.end

for.body19:                                       ; preds = %for.cond17
  %24 = load float*, float** %samp, align 4
  %25 = load i32, i32* %band, align 4
  %sub20 = sub nsw i32 %25, 32
  %arrayidx21 = getelementptr inbounds float, float* %24, i32 %sub20
  %26 = load float, float* %arrayidx21, align 4
  %mul = fmul float %26, -1.000000e+00
  store float %mul, float* %arrayidx21, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body19
  %27 = load i32, i32* %band, align 4
  %add = add nsw i32 %27, 2
  store i32 %add, i32* %band, align 4
  br label %for.cond17

for.end:                                          ; preds = %for.cond17
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %28 = load i32, i32* %k, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond10

for.end23:                                        ; preds = %for.cond10
  store i32 0, i32* %band, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc229, %for.end23
  %29 = load i32, i32* %band, align 4
  %cmp25 = icmp slt i32 %29, 32
  br i1 %cmp25, label %for.body26, label %for.end232

for.body26:                                       ; preds = %for.cond24
  %30 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 9
  %31 = load i32, i32* %block_type, align 4
  store i32 %31, i32* %type, align 4
  %32 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %sb_sample27 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %32, i32 0, i32 0
  %33 = load i32, i32* %ch, align 4
  %arrayidx28 = getelementptr inbounds [2 x [2 x [18 x [32 x float]]]], [2 x [2 x [18 x [32 x float]]]]* %sb_sample27, i32 0, i32 %33
  %34 = load i32, i32* %gr, align 4
  %arrayidx29 = getelementptr inbounds [2 x [18 x [32 x float]]], [2 x [18 x [32 x float]]]* %arrayidx28, i32 0, i32 %34
  %arrayidx30 = getelementptr inbounds [18 x [32 x float]], [18 x [32 x float]]* %arrayidx29, i32 0, i32 0
  %arraydecay31 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx30, i32 0, i32 0
  %35 = load i32, i32* %band, align 4
  %arrayidx32 = getelementptr inbounds [32 x i32], [32 x i32]* @order, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx32, align 4
  %add.ptr33 = getelementptr inbounds float, float* %arraydecay31, i32 %36
  store float* %add.ptr33, float** %band0, align 4
  %37 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %sb_sample34 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %37, i32 0, i32 0
  %38 = load i32, i32* %ch, align 4
  %arrayidx35 = getelementptr inbounds [2 x [2 x [18 x [32 x float]]]], [2 x [2 x [18 x [32 x float]]]]* %sb_sample34, i32 0, i32 %38
  %39 = load i32, i32* %gr, align 4
  %sub36 = sub nsw i32 1, %39
  %arrayidx37 = getelementptr inbounds [2 x [18 x [32 x float]]], [2 x [18 x [32 x float]]]* %arrayidx35, i32 0, i32 %sub36
  %arrayidx38 = getelementptr inbounds [18 x [32 x float]], [18 x [32 x float]]* %arrayidx37, i32 0, i32 0
  %arraydecay39 = getelementptr inbounds [32 x float], [32 x float]* %arrayidx38, i32 0, i32 0
  %40 = load i32, i32* %band, align 4
  %arrayidx40 = getelementptr inbounds [32 x i32], [32 x i32]* @order, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx40, align 4
  %add.ptr41 = getelementptr inbounds float, float* %arraydecay39, i32 %41
  store float* %add.ptr41, float** %band1, align 4
  %42 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %42, i32 0, i32 10
  %43 = load i32, i32* %mixed_block_flag, align 4
  %tobool = icmp ne i32 %43, 0
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body26
  %44 = load i32, i32* %band, align 4
  %cmp42 = icmp slt i32 %44, 2
  br i1 %cmp42, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %type, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body26
  %45 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %amp_filter = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %45, i32 0, i32 1
  %46 = load i32, i32* %band, align 4
  %arrayidx43 = getelementptr inbounds [32 x float], [32 x float]* %amp_filter, i32 0, i32 %46
  %47 = load float, float* %arrayidx43, align 4
  %conv = fpext float %47 to double
  %cmp44 = fcmp olt double %conv, 0x3D719799812DEA11
  br i1 %cmp44, label %if.then46, label %if.else

if.then46:                                        ; preds = %if.end
  %48 = load float*, float** %mdct_enc, align 4
  %49 = bitcast float* %48 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %49, i8 0, i32 72, i1 false)
  br label %if.end196

if.else:                                          ; preds = %if.end
  %50 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %amp_filter47 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %50, i32 0, i32 1
  %51 = load i32, i32* %band, align 4
  %arrayidx48 = getelementptr inbounds [32 x float], [32 x float]* %amp_filter47, i32 0, i32 %51
  %52 = load float, float* %arrayidx48, align 4
  %conv49 = fpext float %52 to double
  %cmp50 = fcmp olt double %conv49, 1.000000e+00
  br i1 %cmp50, label %if.then52, label %if.end65

if.then52:                                        ; preds = %if.else
  store i32 0, i32* %k, align 4
  br label %for.cond53

for.cond53:                                       ; preds = %for.inc62, %if.then52
  %53 = load i32, i32* %k, align 4
  %cmp54 = icmp slt i32 %53, 18
  br i1 %cmp54, label %for.body56, label %for.end64

for.body56:                                       ; preds = %for.cond53
  %54 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %amp_filter57 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %54, i32 0, i32 1
  %55 = load i32, i32* %band, align 4
  %arrayidx58 = getelementptr inbounds [32 x float], [32 x float]* %amp_filter57, i32 0, i32 %55
  %56 = load float, float* %arrayidx58, align 4
  %57 = load float*, float** %band1, align 4
  %58 = load i32, i32* %k, align 4
  %mul59 = mul nsw i32 %58, 32
  %arrayidx60 = getelementptr inbounds float, float* %57, i32 %mul59
  %59 = load float, float* %arrayidx60, align 4
  %mul61 = fmul float %59, %56
  store float %mul61, float* %arrayidx60, align 4
  br label %for.inc62

for.inc62:                                        ; preds = %for.body56
  %60 = load i32, i32* %k, align 4
  %inc63 = add nsw i32 %60, 1
  store i32 %inc63, i32* %k, align 4
  br label %for.cond53

for.end64:                                        ; preds = %for.cond53
  br label %if.end65

if.end65:                                         ; preds = %for.end64, %if.else
  %61 = load i32, i32* %type, align 4
  %cmp66 = icmp eq i32 %61, 2
  br i1 %cmp66, label %if.then68, label %if.else144

if.then68:                                        ; preds = %if.end65
  store i32 -3, i32* %k, align 4
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc141, %if.then68
  %62 = load i32, i32* %k, align 4
  %cmp70 = icmp slt i32 %62, 0
  br i1 %cmp70, label %for.body72, label %for.end143

for.body72:                                       ; preds = %for.cond69
  %63 = load i32, i32* %k, align 4
  %add73 = add nsw i32 %63, 3
  %arrayidx74 = getelementptr inbounds [36 x float], [36 x float]* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2), i32 0, i32 %add73
  %64 = load float, float* %arrayidx74, align 4
  store float %64, float* %w, align 4
  %65 = load float*, float** %band0, align 4
  %66 = load i32, i32* %k, align 4
  %add75 = add nsw i32 9, %66
  %mul76 = mul nsw i32 %add75, 32
  %arrayidx77 = getelementptr inbounds float, float* %65, i32 %mul76
  %67 = load float, float* %arrayidx77, align 4
  %68 = load float, float* %w, align 4
  %mul78 = fmul float %67, %68
  %69 = load float*, float** %band0, align 4
  %70 = load i32, i32* %k, align 4
  %sub79 = sub nsw i32 8, %70
  %mul80 = mul nsw i32 %sub79, 32
  %arrayidx81 = getelementptr inbounds float, float* %69, i32 %mul80
  %71 = load float, float* %arrayidx81, align 4
  %sub82 = fsub float %mul78, %71
  %72 = load float*, float** %mdct_enc, align 4
  %73 = load i32, i32* %k, align 4
  %mul83 = mul nsw i32 %73, 3
  %add84 = add nsw i32 %mul83, 9
  %arrayidx85 = getelementptr inbounds float, float* %72, i32 %add84
  store float %sub82, float* %arrayidx85, align 4
  %74 = load float*, float** %band0, align 4
  %75 = load i32, i32* %k, align 4
  %sub86 = sub nsw i32 14, %75
  %mul87 = mul nsw i32 %sub86, 32
  %arrayidx88 = getelementptr inbounds float, float* %74, i32 %mul87
  %76 = load float, float* %arrayidx88, align 4
  %77 = load float, float* %w, align 4
  %mul89 = fmul float %76, %77
  %78 = load float*, float** %band0, align 4
  %79 = load i32, i32* %k, align 4
  %add90 = add nsw i32 15, %79
  %mul91 = mul nsw i32 %add90, 32
  %arrayidx92 = getelementptr inbounds float, float* %78, i32 %mul91
  %80 = load float, float* %arrayidx92, align 4
  %add93 = fadd float %mul89, %80
  %81 = load float*, float** %mdct_enc, align 4
  %82 = load i32, i32* %k, align 4
  %mul94 = mul nsw i32 %82, 3
  %add95 = add nsw i32 %mul94, 18
  %arrayidx96 = getelementptr inbounds float, float* %81, i32 %add95
  store float %add93, float* %arrayidx96, align 4
  %83 = load float*, float** %band0, align 4
  %84 = load i32, i32* %k, align 4
  %add97 = add nsw i32 15, %84
  %mul98 = mul nsw i32 %add97, 32
  %arrayidx99 = getelementptr inbounds float, float* %83, i32 %mul98
  %85 = load float, float* %arrayidx99, align 4
  %86 = load float, float* %w, align 4
  %mul100 = fmul float %85, %86
  %87 = load float*, float** %band0, align 4
  %88 = load i32, i32* %k, align 4
  %sub101 = sub nsw i32 14, %88
  %mul102 = mul nsw i32 %sub101, 32
  %arrayidx103 = getelementptr inbounds float, float* %87, i32 %mul102
  %89 = load float, float* %arrayidx103, align 4
  %sub104 = fsub float %mul100, %89
  %90 = load float*, float** %mdct_enc, align 4
  %91 = load i32, i32* %k, align 4
  %mul105 = mul nsw i32 %91, 3
  %add106 = add nsw i32 %mul105, 10
  %arrayidx107 = getelementptr inbounds float, float* %90, i32 %add106
  store float %sub104, float* %arrayidx107, align 4
  %92 = load float*, float** %band1, align 4
  %93 = load i32, i32* %k, align 4
  %sub108 = sub nsw i32 2, %93
  %mul109 = mul nsw i32 %sub108, 32
  %arrayidx110 = getelementptr inbounds float, float* %92, i32 %mul109
  %94 = load float, float* %arrayidx110, align 4
  %95 = load float, float* %w, align 4
  %mul111 = fmul float %94, %95
  %96 = load float*, float** %band1, align 4
  %97 = load i32, i32* %k, align 4
  %add112 = add nsw i32 3, %97
  %mul113 = mul nsw i32 %add112, 32
  %arrayidx114 = getelementptr inbounds float, float* %96, i32 %mul113
  %98 = load float, float* %arrayidx114, align 4
  %add115 = fadd float %mul111, %98
  %99 = load float*, float** %mdct_enc, align 4
  %100 = load i32, i32* %k, align 4
  %mul116 = mul nsw i32 %100, 3
  %add117 = add nsw i32 %mul116, 19
  %arrayidx118 = getelementptr inbounds float, float* %99, i32 %add117
  store float %add115, float* %arrayidx118, align 4
  %101 = load float*, float** %band1, align 4
  %102 = load i32, i32* %k, align 4
  %add119 = add nsw i32 3, %102
  %mul120 = mul nsw i32 %add119, 32
  %arrayidx121 = getelementptr inbounds float, float* %101, i32 %mul120
  %103 = load float, float* %arrayidx121, align 4
  %104 = load float, float* %w, align 4
  %mul122 = fmul float %103, %104
  %105 = load float*, float** %band1, align 4
  %106 = load i32, i32* %k, align 4
  %sub123 = sub nsw i32 2, %106
  %mul124 = mul nsw i32 %sub123, 32
  %arrayidx125 = getelementptr inbounds float, float* %105, i32 %mul124
  %107 = load float, float* %arrayidx125, align 4
  %sub126 = fsub float %mul122, %107
  %108 = load float*, float** %mdct_enc, align 4
  %109 = load i32, i32* %k, align 4
  %mul127 = mul nsw i32 %109, 3
  %add128 = add nsw i32 %mul127, 11
  %arrayidx129 = getelementptr inbounds float, float* %108, i32 %add128
  store float %sub126, float* %arrayidx129, align 4
  %110 = load float*, float** %band1, align 4
  %111 = load i32, i32* %k, align 4
  %sub130 = sub nsw i32 8, %111
  %mul131 = mul nsw i32 %sub130, 32
  %arrayidx132 = getelementptr inbounds float, float* %110, i32 %mul131
  %112 = load float, float* %arrayidx132, align 4
  %113 = load float, float* %w, align 4
  %mul133 = fmul float %112, %113
  %114 = load float*, float** %band1, align 4
  %115 = load i32, i32* %k, align 4
  %add134 = add nsw i32 9, %115
  %mul135 = mul nsw i32 %add134, 32
  %arrayidx136 = getelementptr inbounds float, float* %114, i32 %mul135
  %116 = load float, float* %arrayidx136, align 4
  %add137 = fadd float %mul133, %116
  %117 = load float*, float** %mdct_enc, align 4
  %118 = load i32, i32* %k, align 4
  %mul138 = mul nsw i32 %118, 3
  %add139 = add nsw i32 %mul138, 20
  %arrayidx140 = getelementptr inbounds float, float* %117, i32 %add139
  store float %add137, float* %arrayidx140, align 4
  br label %for.inc141

for.inc141:                                       ; preds = %for.body72
  %119 = load i32, i32* %k, align 4
  %inc142 = add nsw i32 %119, 1
  store i32 %inc142, i32* %k, align 4
  br label %for.cond69

for.end143:                                       ; preds = %for.cond69
  %120 = load float*, float** %mdct_enc, align 4
  call void @mdct_short(float* %120)
  br label %if.end195

if.else144:                                       ; preds = %if.end65
  store i32 -9, i32* %k, align 4
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc191, %if.else144
  %121 = load i32, i32* %k, align 4
  %cmp146 = icmp slt i32 %121, 0
  br i1 %cmp146, label %for.body148, label %for.end193

for.body148:                                      ; preds = %for.cond145
  %122 = load i32, i32* %type, align 4
  %arrayidx149 = getelementptr inbounds [4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 %122
  %123 = load i32, i32* %k, align 4
  %add150 = add nsw i32 %123, 27
  %arrayidx151 = getelementptr inbounds [36 x float], [36 x float]* %arrayidx149, i32 0, i32 %add150
  %124 = load float, float* %arrayidx151, align 4
  %125 = load float*, float** %band1, align 4
  %126 = load i32, i32* %k, align 4
  %add152 = add nsw i32 %126, 9
  %mul153 = mul nsw i32 %add152, 32
  %arrayidx154 = getelementptr inbounds float, float* %125, i32 %mul153
  %127 = load float, float* %arrayidx154, align 4
  %mul155 = fmul float %124, %127
  %128 = load i32, i32* %type, align 4
  %arrayidx156 = getelementptr inbounds [4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 %128
  %129 = load i32, i32* %k, align 4
  %add157 = add nsw i32 %129, 36
  %arrayidx158 = getelementptr inbounds [36 x float], [36 x float]* %arrayidx156, i32 0, i32 %add157
  %130 = load float, float* %arrayidx158, align 4
  %131 = load float*, float** %band1, align 4
  %132 = load i32, i32* %k, align 4
  %sub159 = sub nsw i32 8, %132
  %mul160 = mul nsw i32 %sub159, 32
  %arrayidx161 = getelementptr inbounds float, float* %131, i32 %mul160
  %133 = load float, float* %arrayidx161, align 4
  %mul162 = fmul float %130, %133
  %add163 = fadd float %mul155, %mul162
  store float %add163, float* %a, align 4
  %134 = load i32, i32* %type, align 4
  %arrayidx164 = getelementptr inbounds [4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 %134
  %135 = load i32, i32* %k, align 4
  %add165 = add nsw i32 %135, 9
  %arrayidx166 = getelementptr inbounds [36 x float], [36 x float]* %arrayidx164, i32 0, i32 %add165
  %136 = load float, float* %arrayidx166, align 4
  %137 = load float*, float** %band0, align 4
  %138 = load i32, i32* %k, align 4
  %add167 = add nsw i32 %138, 9
  %mul168 = mul nsw i32 %add167, 32
  %arrayidx169 = getelementptr inbounds float, float* %137, i32 %mul168
  %139 = load float, float* %arrayidx169, align 4
  %mul170 = fmul float %136, %139
  %140 = load i32, i32* %type, align 4
  %arrayidx171 = getelementptr inbounds [4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 %140
  %141 = load i32, i32* %k, align 4
  %add172 = add nsw i32 %141, 18
  %arrayidx173 = getelementptr inbounds [36 x float], [36 x float]* %arrayidx171, i32 0, i32 %add172
  %142 = load float, float* %arrayidx173, align 4
  %143 = load float*, float** %band0, align 4
  %144 = load i32, i32* %k, align 4
  %sub174 = sub nsw i32 8, %144
  %mul175 = mul nsw i32 %sub174, 32
  %arrayidx176 = getelementptr inbounds float, float* %143, i32 %mul175
  %145 = load float, float* %arrayidx176, align 4
  %mul177 = fmul float %142, %145
  %sub178 = fsub float %mul170, %mul177
  store float %sub178, float* %b, align 4
  %146 = load float, float* %a, align 4
  %147 = load float, float* %b, align 4
  %148 = load i32, i32* %k, align 4
  %add179 = add nsw i32 %148, 9
  %arrayidx180 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 3), i32 %add179
  %149 = load float, float* %arrayidx180, align 4
  %mul181 = fmul float %147, %149
  %sub182 = fsub float %146, %mul181
  %150 = load i32, i32* %k, align 4
  %add183 = add nsw i32 %150, 9
  %arrayidx184 = getelementptr inbounds [18 x float], [18 x float]* %work, i32 0, i32 %add183
  store float %sub182, float* %arrayidx184, align 4
  %151 = load float, float* %a, align 4
  %152 = load i32, i32* %k, align 4
  %add185 = add nsw i32 %152, 9
  %arrayidx186 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 3), i32 %add185
  %153 = load float, float* %arrayidx186, align 4
  %mul187 = fmul float %151, %153
  %154 = load float, float* %b, align 4
  %add188 = fadd float %mul187, %154
  %155 = load i32, i32* %k, align 4
  %add189 = add nsw i32 %155, 18
  %arrayidx190 = getelementptr inbounds [18 x float], [18 x float]* %work, i32 0, i32 %add189
  store float %add188, float* %arrayidx190, align 4
  br label %for.inc191

for.inc191:                                       ; preds = %for.body148
  %156 = load i32, i32* %k, align 4
  %inc192 = add nsw i32 %156, 1
  store i32 %inc192, i32* %k, align 4
  br label %for.cond145

for.end193:                                       ; preds = %for.cond145
  %157 = load float*, float** %mdct_enc, align 4
  %arraydecay194 = getelementptr inbounds [18 x float], [18 x float]* %work, i32 0, i32 0
  call void @mdct_long(float* %157, float* %arraydecay194)
  br label %if.end195

if.end195:                                        ; preds = %for.end193, %for.end143
  br label %if.end196

if.end196:                                        ; preds = %if.end195, %if.then46
  %158 = load i32, i32* %type, align 4
  %cmp197 = icmp ne i32 %158, 2
  br i1 %cmp197, label %land.lhs.true199, label %if.end228

land.lhs.true199:                                 ; preds = %if.end196
  %159 = load i32, i32* %band, align 4
  %cmp200 = icmp ne i32 %159, 0
  br i1 %cmp200, label %if.then202, label %if.end228

if.then202:                                       ; preds = %land.lhs.true199
  store i32 7, i32* %k, align 4
  br label %for.cond203

for.cond203:                                      ; preds = %for.inc226, %if.then202
  %160 = load i32, i32* %k, align 4
  %cmp204 = icmp sge i32 %160, 0
  br i1 %cmp204, label %for.body206, label %for.end227

for.body206:                                      ; preds = %for.cond203
  %161 = load float*, float** %mdct_enc, align 4
  %162 = load i32, i32* %k, align 4
  %arrayidx207 = getelementptr inbounds float, float* %161, i32 %162
  %163 = load float, float* %arrayidx207, align 4
  %164 = load i32, i32* %k, align 4
  %arrayidx208 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 20), i32 %164
  %165 = load float, float* %arrayidx208, align 4
  %mul209 = fmul float %163, %165
  %166 = load float*, float** %mdct_enc, align 4
  %167 = load i32, i32* %k, align 4
  %sub210 = sub nsw i32 -1, %167
  %arrayidx211 = getelementptr inbounds float, float* %166, i32 %sub210
  %168 = load float, float* %arrayidx211, align 4
  %169 = load i32, i32* %k, align 4
  %arrayidx212 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 28), i32 %169
  %170 = load float, float* %arrayidx212, align 4
  %mul213 = fmul float %168, %170
  %add214 = fadd float %mul209, %mul213
  store float %add214, float* %bu, align 4
  %171 = load float*, float** %mdct_enc, align 4
  %172 = load i32, i32* %k, align 4
  %arrayidx215 = getelementptr inbounds float, float* %171, i32 %172
  %173 = load float, float* %arrayidx215, align 4
  %174 = load i32, i32* %k, align 4
  %arrayidx216 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 28), i32 %174
  %175 = load float, float* %arrayidx216, align 4
  %mul217 = fmul float %173, %175
  %176 = load float*, float** %mdct_enc, align 4
  %177 = load i32, i32* %k, align 4
  %sub218 = sub nsw i32 -1, %177
  %arrayidx219 = getelementptr inbounds float, float* %176, i32 %sub218
  %178 = load float, float* %arrayidx219, align 4
  %179 = load i32, i32* %k, align 4
  %arrayidx220 = getelementptr inbounds float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 20), i32 %179
  %180 = load float, float* %arrayidx220, align 4
  %mul221 = fmul float %178, %180
  %sub222 = fsub float %mul217, %mul221
  store float %sub222, float* %bd, align 4
  %181 = load float, float* %bu, align 4
  %182 = load float*, float** %mdct_enc, align 4
  %183 = load i32, i32* %k, align 4
  %sub223 = sub nsw i32 -1, %183
  %arrayidx224 = getelementptr inbounds float, float* %182, i32 %sub223
  store float %181, float* %arrayidx224, align 4
  %184 = load float, float* %bd, align 4
  %185 = load float*, float** %mdct_enc, align 4
  %186 = load i32, i32* %k, align 4
  %arrayidx225 = getelementptr inbounds float, float* %185, i32 %186
  store float %184, float* %arrayidx225, align 4
  br label %for.inc226

for.inc226:                                       ; preds = %for.body206
  %187 = load i32, i32* %k, align 4
  %dec = add nsw i32 %187, -1
  store i32 %dec, i32* %k, align 4
  br label %for.cond203

for.end227:                                       ; preds = %for.cond203
  br label %if.end228

if.end228:                                        ; preds = %for.end227, %land.lhs.true199, %if.end196
  br label %for.inc229

for.inc229:                                       ; preds = %if.end228
  %188 = load i32, i32* %band, align 4
  %inc230 = add nsw i32 %188, 1
  store i32 %inc230, i32* %band, align 4
  %189 = load float*, float** %mdct_enc, align 4
  %add.ptr231 = getelementptr inbounds float, float* %189, i32 18
  store float* %add.ptr231, float** %mdct_enc, align 4
  br label %for.cond24

for.end232:                                       ; preds = %for.cond24
  br label %for.inc233

for.inc233:                                       ; preds = %for.end232
  %190 = load i32, i32* %gr, align 4
  %inc234 = add nsw i32 %190, 1
  store i32 %inc234, i32* %gr, align 4
  br label %for.cond2

for.end235:                                       ; preds = %for.cond2
  %191 = load float*, float** %w1.addr, align 4
  %add.ptr236 = getelementptr inbounds float, float* %191, i32 286
  store float* %add.ptr236, float** %wk, align 4
  %192 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr237 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %192, i32 0, i32 15
  %193 = load i32, i32* %mode_gr237, align 4
  %cmp238 = icmp eq i32 %193, 1
  br i1 %cmp238, label %if.then240, label %if.end249

if.then240:                                       ; preds = %for.end235
  %194 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %sb_sample241 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %194, i32 0, i32 0
  %195 = load i32, i32* %ch, align 4
  %arrayidx242 = getelementptr inbounds [2 x [2 x [18 x [32 x float]]]], [2 x [2 x [18 x [32 x float]]]]* %sb_sample241, i32 0, i32 %195
  %arrayidx243 = getelementptr inbounds [2 x [18 x [32 x float]]], [2 x [18 x [32 x float]]]* %arrayidx242, i32 0, i32 0
  %arraydecay244 = getelementptr inbounds [18 x [32 x float]], [18 x [32 x float]]* %arrayidx243, i32 0, i32 0
  %196 = bitcast [32 x float]* %arraydecay244 to i8*
  %197 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %sb_sample245 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %197, i32 0, i32 0
  %198 = load i32, i32* %ch, align 4
  %arrayidx246 = getelementptr inbounds [2 x [2 x [18 x [32 x float]]]], [2 x [2 x [18 x [32 x float]]]]* %sb_sample245, i32 0, i32 %198
  %arrayidx247 = getelementptr inbounds [2 x [18 x [32 x float]]], [2 x [18 x [32 x float]]]* %arrayidx246, i32 0, i32 1
  %arraydecay248 = getelementptr inbounds [18 x [32 x float]], [18 x [32 x float]]* %arrayidx247, i32 0, i32 0
  %199 = bitcast [32 x float]* %arraydecay248 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %196, i8* align 8 %199, i32 2304, i1 false)
  br label %if.end249

if.end249:                                        ; preds = %if.then240, %for.end235
  br label %for.inc250

for.inc250:                                       ; preds = %if.end249
  %200 = load i32, i32* %ch, align 4
  %inc251 = add nsw i32 %200, 1
  store i32 %inc251, i32* %ch, align 4
  br label %for.cond

for.end252:                                       ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @window_subband(float* %x1, float* %a) #0 {
entry:
  %x1.addr = alloca float*, align 4
  %a.addr = alloca float*, align 4
  %i = alloca i32, align 4
  %wp = alloca float*, align 4
  %x2 = alloca float*, align 4
  %w = alloca float, align 4
  %s = alloca float, align 4
  %t = alloca float, align 4
  %s121 = alloca float, align 4
  %t122 = alloca float, align 4
  %u = alloca float, align 4
  %v = alloca float, align 4
  %xr = alloca float, align 4
  store float* %x1, float** %x1.addr, align 4
  store float* %a, float** %a.addr, align 4
  store float* getelementptr inbounds ([285 x float], [285 x float]* @enwindow, i32 0, i32 10), float** %wp, align 4
  %0 = load float*, float** %x1.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 -62
  store float* %arrayidx, float** %x2, align 4
  store i32 -15, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %wp, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 -10
  %3 = load float, float* %arrayidx1, align 4
  store float %3, float* %w, align 4
  %4 = load float*, float** %x2, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 -224
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float, float* %w, align 4
  %mul = fmul float %5, %6
  store float %mul, float* %s, align 4
  %7 = load float*, float** %x1.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 224
  %8 = load float, float* %arrayidx3, align 4
  %9 = load float, float* %w, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %t, align 4
  %10 = load float*, float** %wp, align 4
  %arrayidx5 = getelementptr inbounds float, float* %10, i32 -9
  %11 = load float, float* %arrayidx5, align 4
  store float %11, float* %w, align 4
  %12 = load float*, float** %x2, align 4
  %arrayidx6 = getelementptr inbounds float, float* %12, i32 -160
  %13 = load float, float* %arrayidx6, align 4
  %14 = load float, float* %w, align 4
  %mul7 = fmul float %13, %14
  %15 = load float, float* %s, align 4
  %add = fadd float %15, %mul7
  store float %add, float* %s, align 4
  %16 = load float*, float** %x1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %16, i32 160
  %17 = load float, float* %arrayidx8, align 4
  %18 = load float, float* %w, align 4
  %mul9 = fmul float %17, %18
  %19 = load float, float* %t, align 4
  %add10 = fadd float %19, %mul9
  store float %add10, float* %t, align 4
  %20 = load float*, float** %wp, align 4
  %arrayidx11 = getelementptr inbounds float, float* %20, i32 -8
  %21 = load float, float* %arrayidx11, align 4
  store float %21, float* %w, align 4
  %22 = load float*, float** %x2, align 4
  %arrayidx12 = getelementptr inbounds float, float* %22, i32 -96
  %23 = load float, float* %arrayidx12, align 4
  %24 = load float, float* %w, align 4
  %mul13 = fmul float %23, %24
  %25 = load float, float* %s, align 4
  %add14 = fadd float %25, %mul13
  store float %add14, float* %s, align 4
  %26 = load float*, float** %x1.addr, align 4
  %arrayidx15 = getelementptr inbounds float, float* %26, i32 96
  %27 = load float, float* %arrayidx15, align 4
  %28 = load float, float* %w, align 4
  %mul16 = fmul float %27, %28
  %29 = load float, float* %t, align 4
  %add17 = fadd float %29, %mul16
  store float %add17, float* %t, align 4
  %30 = load float*, float** %wp, align 4
  %arrayidx18 = getelementptr inbounds float, float* %30, i32 -7
  %31 = load float, float* %arrayidx18, align 4
  store float %31, float* %w, align 4
  %32 = load float*, float** %x2, align 4
  %arrayidx19 = getelementptr inbounds float, float* %32, i32 -32
  %33 = load float, float* %arrayidx19, align 4
  %34 = load float, float* %w, align 4
  %mul20 = fmul float %33, %34
  %35 = load float, float* %s, align 4
  %add21 = fadd float %35, %mul20
  store float %add21, float* %s, align 4
  %36 = load float*, float** %x1.addr, align 4
  %arrayidx22 = getelementptr inbounds float, float* %36, i32 32
  %37 = load float, float* %arrayidx22, align 4
  %38 = load float, float* %w, align 4
  %mul23 = fmul float %37, %38
  %39 = load float, float* %t, align 4
  %add24 = fadd float %39, %mul23
  store float %add24, float* %t, align 4
  %40 = load float*, float** %wp, align 4
  %arrayidx25 = getelementptr inbounds float, float* %40, i32 -6
  %41 = load float, float* %arrayidx25, align 4
  store float %41, float* %w, align 4
  %42 = load float*, float** %x2, align 4
  %arrayidx26 = getelementptr inbounds float, float* %42, i32 32
  %43 = load float, float* %arrayidx26, align 4
  %44 = load float, float* %w, align 4
  %mul27 = fmul float %43, %44
  %45 = load float, float* %s, align 4
  %add28 = fadd float %45, %mul27
  store float %add28, float* %s, align 4
  %46 = load float*, float** %x1.addr, align 4
  %arrayidx29 = getelementptr inbounds float, float* %46, i32 -32
  %47 = load float, float* %arrayidx29, align 4
  %48 = load float, float* %w, align 4
  %mul30 = fmul float %47, %48
  %49 = load float, float* %t, align 4
  %add31 = fadd float %49, %mul30
  store float %add31, float* %t, align 4
  %50 = load float*, float** %wp, align 4
  %arrayidx32 = getelementptr inbounds float, float* %50, i32 -5
  %51 = load float, float* %arrayidx32, align 4
  store float %51, float* %w, align 4
  %52 = load float*, float** %x2, align 4
  %arrayidx33 = getelementptr inbounds float, float* %52, i32 96
  %53 = load float, float* %arrayidx33, align 4
  %54 = load float, float* %w, align 4
  %mul34 = fmul float %53, %54
  %55 = load float, float* %s, align 4
  %add35 = fadd float %55, %mul34
  store float %add35, float* %s, align 4
  %56 = load float*, float** %x1.addr, align 4
  %arrayidx36 = getelementptr inbounds float, float* %56, i32 -96
  %57 = load float, float* %arrayidx36, align 4
  %58 = load float, float* %w, align 4
  %mul37 = fmul float %57, %58
  %59 = load float, float* %t, align 4
  %add38 = fadd float %59, %mul37
  store float %add38, float* %t, align 4
  %60 = load float*, float** %wp, align 4
  %arrayidx39 = getelementptr inbounds float, float* %60, i32 -4
  %61 = load float, float* %arrayidx39, align 4
  store float %61, float* %w, align 4
  %62 = load float*, float** %x2, align 4
  %arrayidx40 = getelementptr inbounds float, float* %62, i32 160
  %63 = load float, float* %arrayidx40, align 4
  %64 = load float, float* %w, align 4
  %mul41 = fmul float %63, %64
  %65 = load float, float* %s, align 4
  %add42 = fadd float %65, %mul41
  store float %add42, float* %s, align 4
  %66 = load float*, float** %x1.addr, align 4
  %arrayidx43 = getelementptr inbounds float, float* %66, i32 -160
  %67 = load float, float* %arrayidx43, align 4
  %68 = load float, float* %w, align 4
  %mul44 = fmul float %67, %68
  %69 = load float, float* %t, align 4
  %add45 = fadd float %69, %mul44
  store float %add45, float* %t, align 4
  %70 = load float*, float** %wp, align 4
  %arrayidx46 = getelementptr inbounds float, float* %70, i32 -3
  %71 = load float, float* %arrayidx46, align 4
  store float %71, float* %w, align 4
  %72 = load float*, float** %x2, align 4
  %arrayidx47 = getelementptr inbounds float, float* %72, i32 224
  %73 = load float, float* %arrayidx47, align 4
  %74 = load float, float* %w, align 4
  %mul48 = fmul float %73, %74
  %75 = load float, float* %s, align 4
  %add49 = fadd float %75, %mul48
  store float %add49, float* %s, align 4
  %76 = load float*, float** %x1.addr, align 4
  %arrayidx50 = getelementptr inbounds float, float* %76, i32 -224
  %77 = load float, float* %arrayidx50, align 4
  %78 = load float, float* %w, align 4
  %mul51 = fmul float %77, %78
  %79 = load float, float* %t, align 4
  %add52 = fadd float %79, %mul51
  store float %add52, float* %t, align 4
  %80 = load float*, float** %wp, align 4
  %arrayidx53 = getelementptr inbounds float, float* %80, i32 -2
  %81 = load float, float* %arrayidx53, align 4
  store float %81, float* %w, align 4
  %82 = load float*, float** %x1.addr, align 4
  %arrayidx54 = getelementptr inbounds float, float* %82, i32 -256
  %83 = load float, float* %arrayidx54, align 4
  %84 = load float, float* %w, align 4
  %mul55 = fmul float %83, %84
  %85 = load float, float* %s, align 4
  %add56 = fadd float %85, %mul55
  store float %add56, float* %s, align 4
  %86 = load float*, float** %x2, align 4
  %arrayidx57 = getelementptr inbounds float, float* %86, i32 256
  %87 = load float, float* %arrayidx57, align 4
  %88 = load float, float* %w, align 4
  %mul58 = fmul float %87, %88
  %89 = load float, float* %t, align 4
  %sub = fsub float %89, %mul58
  store float %sub, float* %t, align 4
  %90 = load float*, float** %wp, align 4
  %arrayidx59 = getelementptr inbounds float, float* %90, i32 -1
  %91 = load float, float* %arrayidx59, align 4
  store float %91, float* %w, align 4
  %92 = load float*, float** %x1.addr, align 4
  %arrayidx60 = getelementptr inbounds float, float* %92, i32 -192
  %93 = load float, float* %arrayidx60, align 4
  %94 = load float, float* %w, align 4
  %mul61 = fmul float %93, %94
  %95 = load float, float* %s, align 4
  %add62 = fadd float %95, %mul61
  store float %add62, float* %s, align 4
  %96 = load float*, float** %x2, align 4
  %arrayidx63 = getelementptr inbounds float, float* %96, i32 192
  %97 = load float, float* %arrayidx63, align 4
  %98 = load float, float* %w, align 4
  %mul64 = fmul float %97, %98
  %99 = load float, float* %t, align 4
  %sub65 = fsub float %99, %mul64
  store float %sub65, float* %t, align 4
  %100 = load float*, float** %wp, align 4
  %arrayidx66 = getelementptr inbounds float, float* %100, i32 0
  %101 = load float, float* %arrayidx66, align 4
  store float %101, float* %w, align 4
  %102 = load float*, float** %x1.addr, align 4
  %arrayidx67 = getelementptr inbounds float, float* %102, i32 -128
  %103 = load float, float* %arrayidx67, align 4
  %104 = load float, float* %w, align 4
  %mul68 = fmul float %103, %104
  %105 = load float, float* %s, align 4
  %add69 = fadd float %105, %mul68
  store float %add69, float* %s, align 4
  %106 = load float*, float** %x2, align 4
  %arrayidx70 = getelementptr inbounds float, float* %106, i32 128
  %107 = load float, float* %arrayidx70, align 4
  %108 = load float, float* %w, align 4
  %mul71 = fmul float %107, %108
  %109 = load float, float* %t, align 4
  %sub72 = fsub float %109, %mul71
  store float %sub72, float* %t, align 4
  %110 = load float*, float** %wp, align 4
  %arrayidx73 = getelementptr inbounds float, float* %110, i32 1
  %111 = load float, float* %arrayidx73, align 4
  store float %111, float* %w, align 4
  %112 = load float*, float** %x1.addr, align 4
  %arrayidx74 = getelementptr inbounds float, float* %112, i32 -64
  %113 = load float, float* %arrayidx74, align 4
  %114 = load float, float* %w, align 4
  %mul75 = fmul float %113, %114
  %115 = load float, float* %s, align 4
  %add76 = fadd float %115, %mul75
  store float %add76, float* %s, align 4
  %116 = load float*, float** %x2, align 4
  %arrayidx77 = getelementptr inbounds float, float* %116, i32 64
  %117 = load float, float* %arrayidx77, align 4
  %118 = load float, float* %w, align 4
  %mul78 = fmul float %117, %118
  %119 = load float, float* %t, align 4
  %sub79 = fsub float %119, %mul78
  store float %sub79, float* %t, align 4
  %120 = load float*, float** %wp, align 4
  %arrayidx80 = getelementptr inbounds float, float* %120, i32 2
  %121 = load float, float* %arrayidx80, align 4
  store float %121, float* %w, align 4
  %122 = load float*, float** %x1.addr, align 4
  %arrayidx81 = getelementptr inbounds float, float* %122, i32 0
  %123 = load float, float* %arrayidx81, align 4
  %124 = load float, float* %w, align 4
  %mul82 = fmul float %123, %124
  %125 = load float, float* %s, align 4
  %add83 = fadd float %125, %mul82
  store float %add83, float* %s, align 4
  %126 = load float*, float** %x2, align 4
  %arrayidx84 = getelementptr inbounds float, float* %126, i32 0
  %127 = load float, float* %arrayidx84, align 4
  %128 = load float, float* %w, align 4
  %mul85 = fmul float %127, %128
  %129 = load float, float* %t, align 4
  %sub86 = fsub float %129, %mul85
  store float %sub86, float* %t, align 4
  %130 = load float*, float** %wp, align 4
  %arrayidx87 = getelementptr inbounds float, float* %130, i32 3
  %131 = load float, float* %arrayidx87, align 4
  store float %131, float* %w, align 4
  %132 = load float*, float** %x1.addr, align 4
  %arrayidx88 = getelementptr inbounds float, float* %132, i32 64
  %133 = load float, float* %arrayidx88, align 4
  %134 = load float, float* %w, align 4
  %mul89 = fmul float %133, %134
  %135 = load float, float* %s, align 4
  %add90 = fadd float %135, %mul89
  store float %add90, float* %s, align 4
  %136 = load float*, float** %x2, align 4
  %arrayidx91 = getelementptr inbounds float, float* %136, i32 -64
  %137 = load float, float* %arrayidx91, align 4
  %138 = load float, float* %w, align 4
  %mul92 = fmul float %137, %138
  %139 = load float, float* %t, align 4
  %sub93 = fsub float %139, %mul92
  store float %sub93, float* %t, align 4
  %140 = load float*, float** %wp, align 4
  %arrayidx94 = getelementptr inbounds float, float* %140, i32 4
  %141 = load float, float* %arrayidx94, align 4
  store float %141, float* %w, align 4
  %142 = load float*, float** %x1.addr, align 4
  %arrayidx95 = getelementptr inbounds float, float* %142, i32 128
  %143 = load float, float* %arrayidx95, align 4
  %144 = load float, float* %w, align 4
  %mul96 = fmul float %143, %144
  %145 = load float, float* %s, align 4
  %add97 = fadd float %145, %mul96
  store float %add97, float* %s, align 4
  %146 = load float*, float** %x2, align 4
  %arrayidx98 = getelementptr inbounds float, float* %146, i32 -128
  %147 = load float, float* %arrayidx98, align 4
  %148 = load float, float* %w, align 4
  %mul99 = fmul float %147, %148
  %149 = load float, float* %t, align 4
  %sub100 = fsub float %149, %mul99
  store float %sub100, float* %t, align 4
  %150 = load float*, float** %wp, align 4
  %arrayidx101 = getelementptr inbounds float, float* %150, i32 5
  %151 = load float, float* %arrayidx101, align 4
  store float %151, float* %w, align 4
  %152 = load float*, float** %x1.addr, align 4
  %arrayidx102 = getelementptr inbounds float, float* %152, i32 192
  %153 = load float, float* %arrayidx102, align 4
  %154 = load float, float* %w, align 4
  %mul103 = fmul float %153, %154
  %155 = load float, float* %s, align 4
  %add104 = fadd float %155, %mul103
  store float %add104, float* %s, align 4
  %156 = load float*, float** %x2, align 4
  %arrayidx105 = getelementptr inbounds float, float* %156, i32 -192
  %157 = load float, float* %arrayidx105, align 4
  %158 = load float, float* %w, align 4
  %mul106 = fmul float %157, %158
  %159 = load float, float* %t, align 4
  %sub107 = fsub float %159, %mul106
  store float %sub107, float* %t, align 4
  %160 = load float*, float** %wp, align 4
  %arrayidx108 = getelementptr inbounds float, float* %160, i32 6
  %161 = load float, float* %arrayidx108, align 4
  %162 = load float, float* %s, align 4
  %mul109 = fmul float %162, %161
  store float %mul109, float* %s, align 4
  %163 = load float, float* %t, align 4
  %164 = load float, float* %s, align 4
  %sub110 = fsub float %163, %164
  store float %sub110, float* %w, align 4
  %165 = load float, float* %t, align 4
  %166 = load float, float* %s, align 4
  %add111 = fadd float %165, %166
  %167 = load float*, float** %a.addr, align 4
  %168 = load i32, i32* %i, align 4
  %mul112 = mul nsw i32 %168, 2
  %add113 = add nsw i32 30, %mul112
  %arrayidx114 = getelementptr inbounds float, float* %167, i32 %add113
  store float %add111, float* %arrayidx114, align 4
  %169 = load float*, float** %wp, align 4
  %arrayidx115 = getelementptr inbounds float, float* %169, i32 7
  %170 = load float, float* %arrayidx115, align 4
  %171 = load float, float* %w, align 4
  %mul116 = fmul float %170, %171
  %172 = load float*, float** %a.addr, align 4
  %173 = load i32, i32* %i, align 4
  %mul117 = mul nsw i32 %173, 2
  %add118 = add nsw i32 31, %mul117
  %arrayidx119 = getelementptr inbounds float, float* %172, i32 %add118
  store float %mul116, float* %arrayidx119, align 4
  %174 = load float*, float** %wp, align 4
  %add.ptr = getelementptr inbounds float, float* %174, i32 18
  store float* %add.ptr, float** %wp, align 4
  %175 = load float*, float** %x1.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %175, i32 -1
  store float* %incdec.ptr, float** %x1.addr, align 4
  %176 = load float*, float** %x2, align 4
  %incdec.ptr120 = getelementptr inbounds float, float* %176, i32 1
  store float* %incdec.ptr120, float** %x2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %177 = load i32, i32* %i, align 4
  %inc = add nsw i32 %177, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %178 = load float*, float** %x1.addr, align 4
  %arrayidx123 = getelementptr inbounds float, float* %178, i32 -16
  %179 = load float, float* %arrayidx123, align 4
  %180 = load float*, float** %wp, align 4
  %arrayidx124 = getelementptr inbounds float, float* %180, i32 -10
  %181 = load float, float* %arrayidx124, align 4
  %mul125 = fmul float %179, %181
  store float %mul125, float* %t122, align 4
  %182 = load float*, float** %x1.addr, align 4
  %arrayidx126 = getelementptr inbounds float, float* %182, i32 -32
  %183 = load float, float* %arrayidx126, align 4
  %184 = load float*, float** %wp, align 4
  %arrayidx127 = getelementptr inbounds float, float* %184, i32 -2
  %185 = load float, float* %arrayidx127, align 4
  %mul128 = fmul float %183, %185
  store float %mul128, float* %s121, align 4
  %186 = load float*, float** %x1.addr, align 4
  %arrayidx129 = getelementptr inbounds float, float* %186, i32 -48
  %187 = load float, float* %arrayidx129, align 4
  %188 = load float*, float** %x1.addr, align 4
  %arrayidx130 = getelementptr inbounds float, float* %188, i32 16
  %189 = load float, float* %arrayidx130, align 4
  %sub131 = fsub float %187, %189
  %190 = load float*, float** %wp, align 4
  %arrayidx132 = getelementptr inbounds float, float* %190, i32 -9
  %191 = load float, float* %arrayidx132, align 4
  %mul133 = fmul float %sub131, %191
  %192 = load float, float* %t122, align 4
  %add134 = fadd float %192, %mul133
  store float %add134, float* %t122, align 4
  %193 = load float*, float** %x1.addr, align 4
  %arrayidx135 = getelementptr inbounds float, float* %193, i32 -96
  %194 = load float, float* %arrayidx135, align 4
  %195 = load float*, float** %wp, align 4
  %arrayidx136 = getelementptr inbounds float, float* %195, i32 -1
  %196 = load float, float* %arrayidx136, align 4
  %mul137 = fmul float %194, %196
  %197 = load float, float* %s121, align 4
  %add138 = fadd float %197, %mul137
  store float %add138, float* %s121, align 4
  %198 = load float*, float** %x1.addr, align 4
  %arrayidx139 = getelementptr inbounds float, float* %198, i32 -80
  %199 = load float, float* %arrayidx139, align 4
  %200 = load float*, float** %x1.addr, align 4
  %arrayidx140 = getelementptr inbounds float, float* %200, i32 48
  %201 = load float, float* %arrayidx140, align 4
  %add141 = fadd float %199, %201
  %202 = load float*, float** %wp, align 4
  %arrayidx142 = getelementptr inbounds float, float* %202, i32 -8
  %203 = load float, float* %arrayidx142, align 4
  %mul143 = fmul float %add141, %203
  %204 = load float, float* %t122, align 4
  %add144 = fadd float %204, %mul143
  store float %add144, float* %t122, align 4
  %205 = load float*, float** %x1.addr, align 4
  %arrayidx145 = getelementptr inbounds float, float* %205, i32 -160
  %206 = load float, float* %arrayidx145, align 4
  %207 = load float*, float** %wp, align 4
  %arrayidx146 = getelementptr inbounds float, float* %207, i32 0
  %208 = load float, float* %arrayidx146, align 4
  %mul147 = fmul float %206, %208
  %209 = load float, float* %s121, align 4
  %add148 = fadd float %209, %mul147
  store float %add148, float* %s121, align 4
  %210 = load float*, float** %x1.addr, align 4
  %arrayidx149 = getelementptr inbounds float, float* %210, i32 -112
  %211 = load float, float* %arrayidx149, align 4
  %212 = load float*, float** %x1.addr, align 4
  %arrayidx150 = getelementptr inbounds float, float* %212, i32 80
  %213 = load float, float* %arrayidx150, align 4
  %sub151 = fsub float %211, %213
  %214 = load float*, float** %wp, align 4
  %arrayidx152 = getelementptr inbounds float, float* %214, i32 -7
  %215 = load float, float* %arrayidx152, align 4
  %mul153 = fmul float %sub151, %215
  %216 = load float, float* %t122, align 4
  %add154 = fadd float %216, %mul153
  store float %add154, float* %t122, align 4
  %217 = load float*, float** %x1.addr, align 4
  %arrayidx155 = getelementptr inbounds float, float* %217, i32 -224
  %218 = load float, float* %arrayidx155, align 4
  %219 = load float*, float** %wp, align 4
  %arrayidx156 = getelementptr inbounds float, float* %219, i32 1
  %220 = load float, float* %arrayidx156, align 4
  %mul157 = fmul float %218, %220
  %221 = load float, float* %s121, align 4
  %add158 = fadd float %221, %mul157
  store float %add158, float* %s121, align 4
  %222 = load float*, float** %x1.addr, align 4
  %arrayidx159 = getelementptr inbounds float, float* %222, i32 -144
  %223 = load float, float* %arrayidx159, align 4
  %224 = load float*, float** %x1.addr, align 4
  %arrayidx160 = getelementptr inbounds float, float* %224, i32 112
  %225 = load float, float* %arrayidx160, align 4
  %add161 = fadd float %223, %225
  %226 = load float*, float** %wp, align 4
  %arrayidx162 = getelementptr inbounds float, float* %226, i32 -6
  %227 = load float, float* %arrayidx162, align 4
  %mul163 = fmul float %add161, %227
  %228 = load float, float* %t122, align 4
  %add164 = fadd float %228, %mul163
  store float %add164, float* %t122, align 4
  %229 = load float*, float** %x1.addr, align 4
  %arrayidx165 = getelementptr inbounds float, float* %229, i32 32
  %230 = load float, float* %arrayidx165, align 4
  %231 = load float*, float** %wp, align 4
  %arrayidx166 = getelementptr inbounds float, float* %231, i32 2
  %232 = load float, float* %arrayidx166, align 4
  %mul167 = fmul float %230, %232
  %233 = load float, float* %s121, align 4
  %sub168 = fsub float %233, %mul167
  store float %sub168, float* %s121, align 4
  %234 = load float*, float** %x1.addr, align 4
  %arrayidx169 = getelementptr inbounds float, float* %234, i32 -176
  %235 = load float, float* %arrayidx169, align 4
  %236 = load float*, float** %x1.addr, align 4
  %arrayidx170 = getelementptr inbounds float, float* %236, i32 144
  %237 = load float, float* %arrayidx170, align 4
  %sub171 = fsub float %235, %237
  %238 = load float*, float** %wp, align 4
  %arrayidx172 = getelementptr inbounds float, float* %238, i32 -5
  %239 = load float, float* %arrayidx172, align 4
  %mul173 = fmul float %sub171, %239
  %240 = load float, float* %t122, align 4
  %add174 = fadd float %240, %mul173
  store float %add174, float* %t122, align 4
  %241 = load float*, float** %x1.addr, align 4
  %arrayidx175 = getelementptr inbounds float, float* %241, i32 96
  %242 = load float, float* %arrayidx175, align 4
  %243 = load float*, float** %wp, align 4
  %arrayidx176 = getelementptr inbounds float, float* %243, i32 3
  %244 = load float, float* %arrayidx176, align 4
  %mul177 = fmul float %242, %244
  %245 = load float, float* %s121, align 4
  %sub178 = fsub float %245, %mul177
  store float %sub178, float* %s121, align 4
  %246 = load float*, float** %x1.addr, align 4
  %arrayidx179 = getelementptr inbounds float, float* %246, i32 -208
  %247 = load float, float* %arrayidx179, align 4
  %248 = load float*, float** %x1.addr, align 4
  %arrayidx180 = getelementptr inbounds float, float* %248, i32 176
  %249 = load float, float* %arrayidx180, align 4
  %add181 = fadd float %247, %249
  %250 = load float*, float** %wp, align 4
  %arrayidx182 = getelementptr inbounds float, float* %250, i32 -4
  %251 = load float, float* %arrayidx182, align 4
  %mul183 = fmul float %add181, %251
  %252 = load float, float* %t122, align 4
  %add184 = fadd float %252, %mul183
  store float %add184, float* %t122, align 4
  %253 = load float*, float** %x1.addr, align 4
  %arrayidx185 = getelementptr inbounds float, float* %253, i32 160
  %254 = load float, float* %arrayidx185, align 4
  %255 = load float*, float** %wp, align 4
  %arrayidx186 = getelementptr inbounds float, float* %255, i32 4
  %256 = load float, float* %arrayidx186, align 4
  %mul187 = fmul float %254, %256
  %257 = load float, float* %s121, align 4
  %sub188 = fsub float %257, %mul187
  store float %sub188, float* %s121, align 4
  %258 = load float*, float** %x1.addr, align 4
  %arrayidx189 = getelementptr inbounds float, float* %258, i32 -240
  %259 = load float, float* %arrayidx189, align 4
  %260 = load float*, float** %x1.addr, align 4
  %arrayidx190 = getelementptr inbounds float, float* %260, i32 208
  %261 = load float, float* %arrayidx190, align 4
  %sub191 = fsub float %259, %261
  %262 = load float*, float** %wp, align 4
  %arrayidx192 = getelementptr inbounds float, float* %262, i32 -3
  %263 = load float, float* %arrayidx192, align 4
  %mul193 = fmul float %sub191, %263
  %264 = load float, float* %t122, align 4
  %add194 = fadd float %264, %mul193
  store float %add194, float* %t122, align 4
  %265 = load float*, float** %x1.addr, align 4
  %arrayidx195 = getelementptr inbounds float, float* %265, i32 224
  %266 = load float, float* %arrayidx195, align 4
  %267 = load float, float* %s121, align 4
  %sub196 = fsub float %267, %266
  store float %sub196, float* %s121, align 4
  %268 = load float, float* %s121, align 4
  %269 = load float, float* %t122, align 4
  %sub197 = fsub float %268, %269
  store float %sub197, float* %u, align 4
  %270 = load float, float* %s121, align 4
  %271 = load float, float* %t122, align 4
  %add198 = fadd float %270, %271
  store float %add198, float* %v, align 4
  %272 = load float*, float** %a.addr, align 4
  %arrayidx199 = getelementptr inbounds float, float* %272, i32 14
  %273 = load float, float* %arrayidx199, align 4
  store float %273, float* %t122, align 4
  %274 = load float*, float** %a.addr, align 4
  %arrayidx200 = getelementptr inbounds float, float* %274, i32 15
  %275 = load float, float* %arrayidx200, align 4
  %276 = load float, float* %t122, align 4
  %sub201 = fsub float %275, %276
  store float %sub201, float* %s121, align 4
  %277 = load float, float* %v, align 4
  %278 = load float, float* %t122, align 4
  %add202 = fadd float %277, %278
  %279 = load float*, float** %a.addr, align 4
  %arrayidx203 = getelementptr inbounds float, float* %279, i32 31
  store float %add202, float* %arrayidx203, align 4
  %280 = load float, float* %u, align 4
  %281 = load float, float* %s121, align 4
  %add204 = fadd float %280, %281
  %282 = load float*, float** %a.addr, align 4
  %arrayidx205 = getelementptr inbounds float, float* %282, i32 30
  store float %add204, float* %arrayidx205, align 4
  %283 = load float, float* %u, align 4
  %284 = load float, float* %s121, align 4
  %sub206 = fsub float %283, %284
  %285 = load float*, float** %a.addr, align 4
  %arrayidx207 = getelementptr inbounds float, float* %285, i32 15
  store float %sub206, float* %arrayidx207, align 4
  %286 = load float, float* %v, align 4
  %287 = load float, float* %t122, align 4
  %sub208 = fsub float %286, %287
  %288 = load float*, float** %a.addr, align 4
  %arrayidx209 = getelementptr inbounds float, float* %288, i32 14
  store float %sub208, float* %arrayidx209, align 4
  %289 = load float*, float** %a.addr, align 4
  %arrayidx210 = getelementptr inbounds float, float* %289, i32 28
  %290 = load float, float* %arrayidx210, align 4
  %291 = load float*, float** %a.addr, align 4
  %arrayidx211 = getelementptr inbounds float, float* %291, i32 0
  %292 = load float, float* %arrayidx211, align 4
  %sub212 = fsub float %290, %292
  store float %sub212, float* %xr, align 4
  %293 = load float*, float** %a.addr, align 4
  %arrayidx213 = getelementptr inbounds float, float* %293, i32 28
  %294 = load float, float* %arrayidx213, align 4
  %295 = load float*, float** %a.addr, align 4
  %arrayidx214 = getelementptr inbounds float, float* %295, i32 0
  %296 = load float, float* %arrayidx214, align 4
  %add215 = fadd float %296, %294
  store float %add215, float* %arrayidx214, align 4
  %297 = load float, float* %xr, align 4
  %298 = load float*, float** %wp, align 4
  %arrayidx216 = getelementptr inbounds float, float* %298, i32 -29
  %299 = load float, float* %arrayidx216, align 4
  %mul217 = fmul float %297, %299
  %300 = load float*, float** %a.addr, align 4
  %arrayidx218 = getelementptr inbounds float, float* %300, i32 28
  store float %mul217, float* %arrayidx218, align 4
  %301 = load float*, float** %a.addr, align 4
  %arrayidx219 = getelementptr inbounds float, float* %301, i32 29
  %302 = load float, float* %arrayidx219, align 4
  %303 = load float*, float** %a.addr, align 4
  %arrayidx220 = getelementptr inbounds float, float* %303, i32 1
  %304 = load float, float* %arrayidx220, align 4
  %sub221 = fsub float %302, %304
  store float %sub221, float* %xr, align 4
  %305 = load float*, float** %a.addr, align 4
  %arrayidx222 = getelementptr inbounds float, float* %305, i32 29
  %306 = load float, float* %arrayidx222, align 4
  %307 = load float*, float** %a.addr, align 4
  %arrayidx223 = getelementptr inbounds float, float* %307, i32 1
  %308 = load float, float* %arrayidx223, align 4
  %add224 = fadd float %308, %306
  store float %add224, float* %arrayidx223, align 4
  %309 = load float, float* %xr, align 4
  %310 = load float*, float** %wp, align 4
  %arrayidx225 = getelementptr inbounds float, float* %310, i32 -29
  %311 = load float, float* %arrayidx225, align 4
  %mul226 = fmul float %309, %311
  %312 = load float*, float** %a.addr, align 4
  %arrayidx227 = getelementptr inbounds float, float* %312, i32 29
  store float %mul226, float* %arrayidx227, align 4
  %313 = load float*, float** %a.addr, align 4
  %arrayidx228 = getelementptr inbounds float, float* %313, i32 26
  %314 = load float, float* %arrayidx228, align 4
  %315 = load float*, float** %a.addr, align 4
  %arrayidx229 = getelementptr inbounds float, float* %315, i32 2
  %316 = load float, float* %arrayidx229, align 4
  %sub230 = fsub float %314, %316
  store float %sub230, float* %xr, align 4
  %317 = load float*, float** %a.addr, align 4
  %arrayidx231 = getelementptr inbounds float, float* %317, i32 26
  %318 = load float, float* %arrayidx231, align 4
  %319 = load float*, float** %a.addr, align 4
  %arrayidx232 = getelementptr inbounds float, float* %319, i32 2
  %320 = load float, float* %arrayidx232, align 4
  %add233 = fadd float %320, %318
  store float %add233, float* %arrayidx232, align 4
  %321 = load float, float* %xr, align 4
  %322 = load float*, float** %wp, align 4
  %arrayidx234 = getelementptr inbounds float, float* %322, i32 -65
  %323 = load float, float* %arrayidx234, align 4
  %mul235 = fmul float %321, %323
  %324 = load float*, float** %a.addr, align 4
  %arrayidx236 = getelementptr inbounds float, float* %324, i32 26
  store float %mul235, float* %arrayidx236, align 4
  %325 = load float*, float** %a.addr, align 4
  %arrayidx237 = getelementptr inbounds float, float* %325, i32 27
  %326 = load float, float* %arrayidx237, align 4
  %327 = load float*, float** %a.addr, align 4
  %arrayidx238 = getelementptr inbounds float, float* %327, i32 3
  %328 = load float, float* %arrayidx238, align 4
  %sub239 = fsub float %326, %328
  store float %sub239, float* %xr, align 4
  %329 = load float*, float** %a.addr, align 4
  %arrayidx240 = getelementptr inbounds float, float* %329, i32 27
  %330 = load float, float* %arrayidx240, align 4
  %331 = load float*, float** %a.addr, align 4
  %arrayidx241 = getelementptr inbounds float, float* %331, i32 3
  %332 = load float, float* %arrayidx241, align 4
  %add242 = fadd float %332, %330
  store float %add242, float* %arrayidx241, align 4
  %333 = load float, float* %xr, align 4
  %334 = load float*, float** %wp, align 4
  %arrayidx243 = getelementptr inbounds float, float* %334, i32 -65
  %335 = load float, float* %arrayidx243, align 4
  %mul244 = fmul float %333, %335
  %336 = load float*, float** %a.addr, align 4
  %arrayidx245 = getelementptr inbounds float, float* %336, i32 27
  store float %mul244, float* %arrayidx245, align 4
  %337 = load float*, float** %a.addr, align 4
  %arrayidx246 = getelementptr inbounds float, float* %337, i32 24
  %338 = load float, float* %arrayidx246, align 4
  %339 = load float*, float** %a.addr, align 4
  %arrayidx247 = getelementptr inbounds float, float* %339, i32 4
  %340 = load float, float* %arrayidx247, align 4
  %sub248 = fsub float %338, %340
  store float %sub248, float* %xr, align 4
  %341 = load float*, float** %a.addr, align 4
  %arrayidx249 = getelementptr inbounds float, float* %341, i32 24
  %342 = load float, float* %arrayidx249, align 4
  %343 = load float*, float** %a.addr, align 4
  %arrayidx250 = getelementptr inbounds float, float* %343, i32 4
  %344 = load float, float* %arrayidx250, align 4
  %add251 = fadd float %344, %342
  store float %add251, float* %arrayidx250, align 4
  %345 = load float, float* %xr, align 4
  %346 = load float*, float** %wp, align 4
  %arrayidx252 = getelementptr inbounds float, float* %346, i32 -101
  %347 = load float, float* %arrayidx252, align 4
  %mul253 = fmul float %345, %347
  %348 = load float*, float** %a.addr, align 4
  %arrayidx254 = getelementptr inbounds float, float* %348, i32 24
  store float %mul253, float* %arrayidx254, align 4
  %349 = load float*, float** %a.addr, align 4
  %arrayidx255 = getelementptr inbounds float, float* %349, i32 25
  %350 = load float, float* %arrayidx255, align 4
  %351 = load float*, float** %a.addr, align 4
  %arrayidx256 = getelementptr inbounds float, float* %351, i32 5
  %352 = load float, float* %arrayidx256, align 4
  %sub257 = fsub float %350, %352
  store float %sub257, float* %xr, align 4
  %353 = load float*, float** %a.addr, align 4
  %arrayidx258 = getelementptr inbounds float, float* %353, i32 25
  %354 = load float, float* %arrayidx258, align 4
  %355 = load float*, float** %a.addr, align 4
  %arrayidx259 = getelementptr inbounds float, float* %355, i32 5
  %356 = load float, float* %arrayidx259, align 4
  %add260 = fadd float %356, %354
  store float %add260, float* %arrayidx259, align 4
  %357 = load float, float* %xr, align 4
  %358 = load float*, float** %wp, align 4
  %arrayidx261 = getelementptr inbounds float, float* %358, i32 -101
  %359 = load float, float* %arrayidx261, align 4
  %mul262 = fmul float %357, %359
  %360 = load float*, float** %a.addr, align 4
  %arrayidx263 = getelementptr inbounds float, float* %360, i32 25
  store float %mul262, float* %arrayidx263, align 4
  %361 = load float*, float** %a.addr, align 4
  %arrayidx264 = getelementptr inbounds float, float* %361, i32 22
  %362 = load float, float* %arrayidx264, align 4
  %363 = load float*, float** %a.addr, align 4
  %arrayidx265 = getelementptr inbounds float, float* %363, i32 6
  %364 = load float, float* %arrayidx265, align 4
  %sub266 = fsub float %362, %364
  store float %sub266, float* %xr, align 4
  %365 = load float*, float** %a.addr, align 4
  %arrayidx267 = getelementptr inbounds float, float* %365, i32 22
  %366 = load float, float* %arrayidx267, align 4
  %367 = load float*, float** %a.addr, align 4
  %arrayidx268 = getelementptr inbounds float, float* %367, i32 6
  %368 = load float, float* %arrayidx268, align 4
  %add269 = fadd float %368, %366
  store float %add269, float* %arrayidx268, align 4
  %369 = load float, float* %xr, align 4
  %conv = fpext float %369 to double
  %mul270 = fmul double %conv, 0x3FF6A09E667F3BCD
  %conv271 = fptrunc double %mul270 to float
  %370 = load float*, float** %a.addr, align 4
  %arrayidx272 = getelementptr inbounds float, float* %370, i32 22
  store float %conv271, float* %arrayidx272, align 4
  %371 = load float*, float** %a.addr, align 4
  %arrayidx273 = getelementptr inbounds float, float* %371, i32 23
  %372 = load float, float* %arrayidx273, align 4
  %373 = load float*, float** %a.addr, align 4
  %arrayidx274 = getelementptr inbounds float, float* %373, i32 7
  %374 = load float, float* %arrayidx274, align 4
  %sub275 = fsub float %372, %374
  store float %sub275, float* %xr, align 4
  %375 = load float*, float** %a.addr, align 4
  %arrayidx276 = getelementptr inbounds float, float* %375, i32 23
  %376 = load float, float* %arrayidx276, align 4
  %377 = load float*, float** %a.addr, align 4
  %arrayidx277 = getelementptr inbounds float, float* %377, i32 7
  %378 = load float, float* %arrayidx277, align 4
  %add278 = fadd float %378, %376
  store float %add278, float* %arrayidx277, align 4
  %379 = load float, float* %xr, align 4
  %conv279 = fpext float %379 to double
  %mul280 = fmul double %conv279, 0x3FF6A09E667F3BCD
  %380 = load float*, float** %a.addr, align 4
  %arrayidx281 = getelementptr inbounds float, float* %380, i32 7
  %381 = load float, float* %arrayidx281, align 4
  %conv282 = fpext float %381 to double
  %sub283 = fsub double %mul280, %conv282
  %conv284 = fptrunc double %sub283 to float
  %382 = load float*, float** %a.addr, align 4
  %arrayidx285 = getelementptr inbounds float, float* %382, i32 23
  store float %conv284, float* %arrayidx285, align 4
  %383 = load float*, float** %a.addr, align 4
  %arrayidx286 = getelementptr inbounds float, float* %383, i32 6
  %384 = load float, float* %arrayidx286, align 4
  %385 = load float*, float** %a.addr, align 4
  %arrayidx287 = getelementptr inbounds float, float* %385, i32 7
  %386 = load float, float* %arrayidx287, align 4
  %sub288 = fsub float %386, %384
  store float %sub288, float* %arrayidx287, align 4
  %387 = load float*, float** %a.addr, align 4
  %arrayidx289 = getelementptr inbounds float, float* %387, i32 7
  %388 = load float, float* %arrayidx289, align 4
  %389 = load float*, float** %a.addr, align 4
  %arrayidx290 = getelementptr inbounds float, float* %389, i32 22
  %390 = load float, float* %arrayidx290, align 4
  %sub291 = fsub float %390, %388
  store float %sub291, float* %arrayidx290, align 4
  %391 = load float*, float** %a.addr, align 4
  %arrayidx292 = getelementptr inbounds float, float* %391, i32 22
  %392 = load float, float* %arrayidx292, align 4
  %393 = load float*, float** %a.addr, align 4
  %arrayidx293 = getelementptr inbounds float, float* %393, i32 23
  %394 = load float, float* %arrayidx293, align 4
  %sub294 = fsub float %394, %392
  store float %sub294, float* %arrayidx293, align 4
  %395 = load float*, float** %a.addr, align 4
  %arrayidx295 = getelementptr inbounds float, float* %395, i32 6
  %396 = load float, float* %arrayidx295, align 4
  store float %396, float* %xr, align 4
  %397 = load float*, float** %a.addr, align 4
  %arrayidx296 = getelementptr inbounds float, float* %397, i32 31
  %398 = load float, float* %arrayidx296, align 4
  %399 = load float, float* %xr, align 4
  %sub297 = fsub float %398, %399
  %400 = load float*, float** %a.addr, align 4
  %arrayidx298 = getelementptr inbounds float, float* %400, i32 6
  store float %sub297, float* %arrayidx298, align 4
  %401 = load float*, float** %a.addr, align 4
  %arrayidx299 = getelementptr inbounds float, float* %401, i32 31
  %402 = load float, float* %arrayidx299, align 4
  %403 = load float, float* %xr, align 4
  %add300 = fadd float %402, %403
  %404 = load float*, float** %a.addr, align 4
  %arrayidx301 = getelementptr inbounds float, float* %404, i32 31
  store float %add300, float* %arrayidx301, align 4
  %405 = load float*, float** %a.addr, align 4
  %arrayidx302 = getelementptr inbounds float, float* %405, i32 7
  %406 = load float, float* %arrayidx302, align 4
  store float %406, float* %xr, align 4
  %407 = load float*, float** %a.addr, align 4
  %arrayidx303 = getelementptr inbounds float, float* %407, i32 30
  %408 = load float, float* %arrayidx303, align 4
  %409 = load float, float* %xr, align 4
  %sub304 = fsub float %408, %409
  %410 = load float*, float** %a.addr, align 4
  %arrayidx305 = getelementptr inbounds float, float* %410, i32 7
  store float %sub304, float* %arrayidx305, align 4
  %411 = load float*, float** %a.addr, align 4
  %arrayidx306 = getelementptr inbounds float, float* %411, i32 30
  %412 = load float, float* %arrayidx306, align 4
  %413 = load float, float* %xr, align 4
  %add307 = fadd float %412, %413
  %414 = load float*, float** %a.addr, align 4
  %arrayidx308 = getelementptr inbounds float, float* %414, i32 30
  store float %add307, float* %arrayidx308, align 4
  %415 = load float*, float** %a.addr, align 4
  %arrayidx309 = getelementptr inbounds float, float* %415, i32 22
  %416 = load float, float* %arrayidx309, align 4
  store float %416, float* %xr, align 4
  %417 = load float*, float** %a.addr, align 4
  %arrayidx310 = getelementptr inbounds float, float* %417, i32 15
  %418 = load float, float* %arrayidx310, align 4
  %419 = load float, float* %xr, align 4
  %sub311 = fsub float %418, %419
  %420 = load float*, float** %a.addr, align 4
  %arrayidx312 = getelementptr inbounds float, float* %420, i32 22
  store float %sub311, float* %arrayidx312, align 4
  %421 = load float*, float** %a.addr, align 4
  %arrayidx313 = getelementptr inbounds float, float* %421, i32 15
  %422 = load float, float* %arrayidx313, align 4
  %423 = load float, float* %xr, align 4
  %add314 = fadd float %422, %423
  %424 = load float*, float** %a.addr, align 4
  %arrayidx315 = getelementptr inbounds float, float* %424, i32 15
  store float %add314, float* %arrayidx315, align 4
  %425 = load float*, float** %a.addr, align 4
  %arrayidx316 = getelementptr inbounds float, float* %425, i32 23
  %426 = load float, float* %arrayidx316, align 4
  store float %426, float* %xr, align 4
  %427 = load float*, float** %a.addr, align 4
  %arrayidx317 = getelementptr inbounds float, float* %427, i32 14
  %428 = load float, float* %arrayidx317, align 4
  %429 = load float, float* %xr, align 4
  %sub318 = fsub float %428, %429
  %430 = load float*, float** %a.addr, align 4
  %arrayidx319 = getelementptr inbounds float, float* %430, i32 23
  store float %sub318, float* %arrayidx319, align 4
  %431 = load float*, float** %a.addr, align 4
  %arrayidx320 = getelementptr inbounds float, float* %431, i32 14
  %432 = load float, float* %arrayidx320, align 4
  %433 = load float, float* %xr, align 4
  %add321 = fadd float %432, %433
  %434 = load float*, float** %a.addr, align 4
  %arrayidx322 = getelementptr inbounds float, float* %434, i32 14
  store float %add321, float* %arrayidx322, align 4
  %435 = load float*, float** %a.addr, align 4
  %arrayidx323 = getelementptr inbounds float, float* %435, i32 20
  %436 = load float, float* %arrayidx323, align 4
  %437 = load float*, float** %a.addr, align 4
  %arrayidx324 = getelementptr inbounds float, float* %437, i32 8
  %438 = load float, float* %arrayidx324, align 4
  %sub325 = fsub float %436, %438
  store float %sub325, float* %xr, align 4
  %439 = load float*, float** %a.addr, align 4
  %arrayidx326 = getelementptr inbounds float, float* %439, i32 20
  %440 = load float, float* %arrayidx326, align 4
  %441 = load float*, float** %a.addr, align 4
  %arrayidx327 = getelementptr inbounds float, float* %441, i32 8
  %442 = load float, float* %arrayidx327, align 4
  %add328 = fadd float %442, %440
  store float %add328, float* %arrayidx327, align 4
  %443 = load float, float* %xr, align 4
  %444 = load float*, float** %wp, align 4
  %arrayidx329 = getelementptr inbounds float, float* %444, i32 -173
  %445 = load float, float* %arrayidx329, align 4
  %mul330 = fmul float %443, %445
  %446 = load float*, float** %a.addr, align 4
  %arrayidx331 = getelementptr inbounds float, float* %446, i32 20
  store float %mul330, float* %arrayidx331, align 4
  %447 = load float*, float** %a.addr, align 4
  %arrayidx332 = getelementptr inbounds float, float* %447, i32 21
  %448 = load float, float* %arrayidx332, align 4
  %449 = load float*, float** %a.addr, align 4
  %arrayidx333 = getelementptr inbounds float, float* %449, i32 9
  %450 = load float, float* %arrayidx333, align 4
  %sub334 = fsub float %448, %450
  store float %sub334, float* %xr, align 4
  %451 = load float*, float** %a.addr, align 4
  %arrayidx335 = getelementptr inbounds float, float* %451, i32 21
  %452 = load float, float* %arrayidx335, align 4
  %453 = load float*, float** %a.addr, align 4
  %arrayidx336 = getelementptr inbounds float, float* %453, i32 9
  %454 = load float, float* %arrayidx336, align 4
  %add337 = fadd float %454, %452
  store float %add337, float* %arrayidx336, align 4
  %455 = load float, float* %xr, align 4
  %456 = load float*, float** %wp, align 4
  %arrayidx338 = getelementptr inbounds float, float* %456, i32 -173
  %457 = load float, float* %arrayidx338, align 4
  %mul339 = fmul float %455, %457
  %458 = load float*, float** %a.addr, align 4
  %arrayidx340 = getelementptr inbounds float, float* %458, i32 21
  store float %mul339, float* %arrayidx340, align 4
  %459 = load float*, float** %a.addr, align 4
  %arrayidx341 = getelementptr inbounds float, float* %459, i32 18
  %460 = load float, float* %arrayidx341, align 4
  %461 = load float*, float** %a.addr, align 4
  %arrayidx342 = getelementptr inbounds float, float* %461, i32 10
  %462 = load float, float* %arrayidx342, align 4
  %sub343 = fsub float %460, %462
  store float %sub343, float* %xr, align 4
  %463 = load float*, float** %a.addr, align 4
  %arrayidx344 = getelementptr inbounds float, float* %463, i32 18
  %464 = load float, float* %arrayidx344, align 4
  %465 = load float*, float** %a.addr, align 4
  %arrayidx345 = getelementptr inbounds float, float* %465, i32 10
  %466 = load float, float* %arrayidx345, align 4
  %add346 = fadd float %466, %464
  store float %add346, float* %arrayidx345, align 4
  %467 = load float, float* %xr, align 4
  %468 = load float*, float** %wp, align 4
  %arrayidx347 = getelementptr inbounds float, float* %468, i32 -209
  %469 = load float, float* %arrayidx347, align 4
  %mul348 = fmul float %467, %469
  %470 = load float*, float** %a.addr, align 4
  %arrayidx349 = getelementptr inbounds float, float* %470, i32 18
  store float %mul348, float* %arrayidx349, align 4
  %471 = load float*, float** %a.addr, align 4
  %arrayidx350 = getelementptr inbounds float, float* %471, i32 19
  %472 = load float, float* %arrayidx350, align 4
  %473 = load float*, float** %a.addr, align 4
  %arrayidx351 = getelementptr inbounds float, float* %473, i32 11
  %474 = load float, float* %arrayidx351, align 4
  %sub352 = fsub float %472, %474
  store float %sub352, float* %xr, align 4
  %475 = load float*, float** %a.addr, align 4
  %arrayidx353 = getelementptr inbounds float, float* %475, i32 19
  %476 = load float, float* %arrayidx353, align 4
  %477 = load float*, float** %a.addr, align 4
  %arrayidx354 = getelementptr inbounds float, float* %477, i32 11
  %478 = load float, float* %arrayidx354, align 4
  %add355 = fadd float %478, %476
  store float %add355, float* %arrayidx354, align 4
  %479 = load float, float* %xr, align 4
  %480 = load float*, float** %wp, align 4
  %arrayidx356 = getelementptr inbounds float, float* %480, i32 -209
  %481 = load float, float* %arrayidx356, align 4
  %mul357 = fmul float %479, %481
  %482 = load float*, float** %a.addr, align 4
  %arrayidx358 = getelementptr inbounds float, float* %482, i32 19
  store float %mul357, float* %arrayidx358, align 4
  %483 = load float*, float** %a.addr, align 4
  %arrayidx359 = getelementptr inbounds float, float* %483, i32 16
  %484 = load float, float* %arrayidx359, align 4
  %485 = load float*, float** %a.addr, align 4
  %arrayidx360 = getelementptr inbounds float, float* %485, i32 12
  %486 = load float, float* %arrayidx360, align 4
  %sub361 = fsub float %484, %486
  store float %sub361, float* %xr, align 4
  %487 = load float*, float** %a.addr, align 4
  %arrayidx362 = getelementptr inbounds float, float* %487, i32 16
  %488 = load float, float* %arrayidx362, align 4
  %489 = load float*, float** %a.addr, align 4
  %arrayidx363 = getelementptr inbounds float, float* %489, i32 12
  %490 = load float, float* %arrayidx363, align 4
  %add364 = fadd float %490, %488
  store float %add364, float* %arrayidx363, align 4
  %491 = load float, float* %xr, align 4
  %492 = load float*, float** %wp, align 4
  %arrayidx365 = getelementptr inbounds float, float* %492, i32 -245
  %493 = load float, float* %arrayidx365, align 4
  %mul366 = fmul float %491, %493
  %494 = load float*, float** %a.addr, align 4
  %arrayidx367 = getelementptr inbounds float, float* %494, i32 16
  store float %mul366, float* %arrayidx367, align 4
  %495 = load float*, float** %a.addr, align 4
  %arrayidx368 = getelementptr inbounds float, float* %495, i32 17
  %496 = load float, float* %arrayidx368, align 4
  %497 = load float*, float** %a.addr, align 4
  %arrayidx369 = getelementptr inbounds float, float* %497, i32 13
  %498 = load float, float* %arrayidx369, align 4
  %sub370 = fsub float %496, %498
  store float %sub370, float* %xr, align 4
  %499 = load float*, float** %a.addr, align 4
  %arrayidx371 = getelementptr inbounds float, float* %499, i32 17
  %500 = load float, float* %arrayidx371, align 4
  %501 = load float*, float** %a.addr, align 4
  %arrayidx372 = getelementptr inbounds float, float* %501, i32 13
  %502 = load float, float* %arrayidx372, align 4
  %add373 = fadd float %502, %500
  store float %add373, float* %arrayidx372, align 4
  %503 = load float, float* %xr, align 4
  %504 = load float*, float** %wp, align 4
  %arrayidx374 = getelementptr inbounds float, float* %504, i32 -245
  %505 = load float, float* %arrayidx374, align 4
  %mul375 = fmul float %503, %505
  %506 = load float*, float** %a.addr, align 4
  %arrayidx376 = getelementptr inbounds float, float* %506, i32 17
  store float %mul375, float* %arrayidx376, align 4
  %507 = load float*, float** %a.addr, align 4
  %arrayidx377 = getelementptr inbounds float, float* %507, i32 20
  %508 = load float, float* %arrayidx377, align 4
  %fneg = fneg float %508
  %509 = load float*, float** %a.addr, align 4
  %arrayidx378 = getelementptr inbounds float, float* %509, i32 24
  %510 = load float, float* %arrayidx378, align 4
  %add379 = fadd float %fneg, %510
  store float %add379, float* %xr, align 4
  %511 = load float*, float** %a.addr, align 4
  %arrayidx380 = getelementptr inbounds float, float* %511, i32 24
  %512 = load float, float* %arrayidx380, align 4
  %513 = load float*, float** %a.addr, align 4
  %arrayidx381 = getelementptr inbounds float, float* %513, i32 20
  %514 = load float, float* %arrayidx381, align 4
  %add382 = fadd float %514, %512
  store float %add382, float* %arrayidx381, align 4
  %515 = load float, float* %xr, align 4
  %516 = load float*, float** %wp, align 4
  %arrayidx383 = getelementptr inbounds float, float* %516, i32 -209
  %517 = load float, float* %arrayidx383, align 4
  %mul384 = fmul float %515, %517
  %518 = load float*, float** %a.addr, align 4
  %arrayidx385 = getelementptr inbounds float, float* %518, i32 24
  store float %mul384, float* %arrayidx385, align 4
  %519 = load float*, float** %a.addr, align 4
  %arrayidx386 = getelementptr inbounds float, float* %519, i32 21
  %520 = load float, float* %arrayidx386, align 4
  %fneg387 = fneg float %520
  %521 = load float*, float** %a.addr, align 4
  %arrayidx388 = getelementptr inbounds float, float* %521, i32 25
  %522 = load float, float* %arrayidx388, align 4
  %add389 = fadd float %fneg387, %522
  store float %add389, float* %xr, align 4
  %523 = load float*, float** %a.addr, align 4
  %arrayidx390 = getelementptr inbounds float, float* %523, i32 25
  %524 = load float, float* %arrayidx390, align 4
  %525 = load float*, float** %a.addr, align 4
  %arrayidx391 = getelementptr inbounds float, float* %525, i32 21
  %526 = load float, float* %arrayidx391, align 4
  %add392 = fadd float %526, %524
  store float %add392, float* %arrayidx391, align 4
  %527 = load float, float* %xr, align 4
  %528 = load float*, float** %wp, align 4
  %arrayidx393 = getelementptr inbounds float, float* %528, i32 -209
  %529 = load float, float* %arrayidx393, align 4
  %mul394 = fmul float %527, %529
  %530 = load float*, float** %a.addr, align 4
  %arrayidx395 = getelementptr inbounds float, float* %530, i32 25
  store float %mul394, float* %arrayidx395, align 4
  %531 = load float*, float** %a.addr, align 4
  %arrayidx396 = getelementptr inbounds float, float* %531, i32 4
  %532 = load float, float* %arrayidx396, align 4
  %533 = load float*, float** %a.addr, align 4
  %arrayidx397 = getelementptr inbounds float, float* %533, i32 8
  %534 = load float, float* %arrayidx397, align 4
  %sub398 = fsub float %532, %534
  store float %sub398, float* %xr, align 4
  %535 = load float*, float** %a.addr, align 4
  %arrayidx399 = getelementptr inbounds float, float* %535, i32 8
  %536 = load float, float* %arrayidx399, align 4
  %537 = load float*, float** %a.addr, align 4
  %arrayidx400 = getelementptr inbounds float, float* %537, i32 4
  %538 = load float, float* %arrayidx400, align 4
  %add401 = fadd float %538, %536
  store float %add401, float* %arrayidx400, align 4
  %539 = load float, float* %xr, align 4
  %540 = load float*, float** %wp, align 4
  %arrayidx402 = getelementptr inbounds float, float* %540, i32 -209
  %541 = load float, float* %arrayidx402, align 4
  %mul403 = fmul float %539, %541
  %542 = load float*, float** %a.addr, align 4
  %arrayidx404 = getelementptr inbounds float, float* %542, i32 8
  store float %mul403, float* %arrayidx404, align 4
  %543 = load float*, float** %a.addr, align 4
  %arrayidx405 = getelementptr inbounds float, float* %543, i32 5
  %544 = load float, float* %arrayidx405, align 4
  %545 = load float*, float** %a.addr, align 4
  %arrayidx406 = getelementptr inbounds float, float* %545, i32 9
  %546 = load float, float* %arrayidx406, align 4
  %sub407 = fsub float %544, %546
  store float %sub407, float* %xr, align 4
  %547 = load float*, float** %a.addr, align 4
  %arrayidx408 = getelementptr inbounds float, float* %547, i32 9
  %548 = load float, float* %arrayidx408, align 4
  %549 = load float*, float** %a.addr, align 4
  %arrayidx409 = getelementptr inbounds float, float* %549, i32 5
  %550 = load float, float* %arrayidx409, align 4
  %add410 = fadd float %550, %548
  store float %add410, float* %arrayidx409, align 4
  %551 = load float, float* %xr, align 4
  %552 = load float*, float** %wp, align 4
  %arrayidx411 = getelementptr inbounds float, float* %552, i32 -209
  %553 = load float, float* %arrayidx411, align 4
  %mul412 = fmul float %551, %553
  %554 = load float*, float** %a.addr, align 4
  %arrayidx413 = getelementptr inbounds float, float* %554, i32 9
  store float %mul412, float* %arrayidx413, align 4
  %555 = load float*, float** %a.addr, align 4
  %arrayidx414 = getelementptr inbounds float, float* %555, i32 0
  %556 = load float, float* %arrayidx414, align 4
  %557 = load float*, float** %a.addr, align 4
  %arrayidx415 = getelementptr inbounds float, float* %557, i32 12
  %558 = load float, float* %arrayidx415, align 4
  %sub416 = fsub float %556, %558
  store float %sub416, float* %xr, align 4
  %559 = load float*, float** %a.addr, align 4
  %arrayidx417 = getelementptr inbounds float, float* %559, i32 12
  %560 = load float, float* %arrayidx417, align 4
  %561 = load float*, float** %a.addr, align 4
  %arrayidx418 = getelementptr inbounds float, float* %561, i32 0
  %562 = load float, float* %arrayidx418, align 4
  %add419 = fadd float %562, %560
  store float %add419, float* %arrayidx418, align 4
  %563 = load float, float* %xr, align 4
  %564 = load float*, float** %wp, align 4
  %arrayidx420 = getelementptr inbounds float, float* %564, i32 -65
  %565 = load float, float* %arrayidx420, align 4
  %mul421 = fmul float %563, %565
  %566 = load float*, float** %a.addr, align 4
  %arrayidx422 = getelementptr inbounds float, float* %566, i32 12
  store float %mul421, float* %arrayidx422, align 4
  %567 = load float*, float** %a.addr, align 4
  %arrayidx423 = getelementptr inbounds float, float* %567, i32 1
  %568 = load float, float* %arrayidx423, align 4
  %569 = load float*, float** %a.addr, align 4
  %arrayidx424 = getelementptr inbounds float, float* %569, i32 13
  %570 = load float, float* %arrayidx424, align 4
  %sub425 = fsub float %568, %570
  store float %sub425, float* %xr, align 4
  %571 = load float*, float** %a.addr, align 4
  %arrayidx426 = getelementptr inbounds float, float* %571, i32 13
  %572 = load float, float* %arrayidx426, align 4
  %573 = load float*, float** %a.addr, align 4
  %arrayidx427 = getelementptr inbounds float, float* %573, i32 1
  %574 = load float, float* %arrayidx427, align 4
  %add428 = fadd float %574, %572
  store float %add428, float* %arrayidx427, align 4
  %575 = load float, float* %xr, align 4
  %576 = load float*, float** %wp, align 4
  %arrayidx429 = getelementptr inbounds float, float* %576, i32 -65
  %577 = load float, float* %arrayidx429, align 4
  %mul430 = fmul float %575, %577
  %578 = load float*, float** %a.addr, align 4
  %arrayidx431 = getelementptr inbounds float, float* %578, i32 13
  store float %mul430, float* %arrayidx431, align 4
  %579 = load float*, float** %a.addr, align 4
  %arrayidx432 = getelementptr inbounds float, float* %579, i32 16
  %580 = load float, float* %arrayidx432, align 4
  %581 = load float*, float** %a.addr, align 4
  %arrayidx433 = getelementptr inbounds float, float* %581, i32 28
  %582 = load float, float* %arrayidx433, align 4
  %sub434 = fsub float %580, %582
  store float %sub434, float* %xr, align 4
  %583 = load float*, float** %a.addr, align 4
  %arrayidx435 = getelementptr inbounds float, float* %583, i32 28
  %584 = load float, float* %arrayidx435, align 4
  %585 = load float*, float** %a.addr, align 4
  %arrayidx436 = getelementptr inbounds float, float* %585, i32 16
  %586 = load float, float* %arrayidx436, align 4
  %add437 = fadd float %586, %584
  store float %add437, float* %arrayidx436, align 4
  %587 = load float, float* %xr, align 4
  %588 = load float*, float** %wp, align 4
  %arrayidx438 = getelementptr inbounds float, float* %588, i32 -65
  %589 = load float, float* %arrayidx438, align 4
  %mul439 = fmul float %587, %589
  %590 = load float*, float** %a.addr, align 4
  %arrayidx440 = getelementptr inbounds float, float* %590, i32 28
  store float %mul439, float* %arrayidx440, align 4
  %591 = load float*, float** %a.addr, align 4
  %arrayidx441 = getelementptr inbounds float, float* %591, i32 17
  %592 = load float, float* %arrayidx441, align 4
  %fneg442 = fneg float %592
  %593 = load float*, float** %a.addr, align 4
  %arrayidx443 = getelementptr inbounds float, float* %593, i32 29
  %594 = load float, float* %arrayidx443, align 4
  %add444 = fadd float %fneg442, %594
  store float %add444, float* %xr, align 4
  %595 = load float*, float** %a.addr, align 4
  %arrayidx445 = getelementptr inbounds float, float* %595, i32 29
  %596 = load float, float* %arrayidx445, align 4
  %597 = load float*, float** %a.addr, align 4
  %arrayidx446 = getelementptr inbounds float, float* %597, i32 17
  %598 = load float, float* %arrayidx446, align 4
  %add447 = fadd float %598, %596
  store float %add447, float* %arrayidx446, align 4
  %599 = load float, float* %xr, align 4
  %600 = load float*, float** %wp, align 4
  %arrayidx448 = getelementptr inbounds float, float* %600, i32 -65
  %601 = load float, float* %arrayidx448, align 4
  %mul449 = fmul float %599, %601
  %602 = load float*, float** %a.addr, align 4
  %arrayidx450 = getelementptr inbounds float, float* %602, i32 29
  store float %mul449, float* %arrayidx450, align 4
  %603 = load float*, float** %a.addr, align 4
  %arrayidx451 = getelementptr inbounds float, float* %603, i32 2
  %604 = load float, float* %arrayidx451, align 4
  %605 = load float*, float** %a.addr, align 4
  %arrayidx452 = getelementptr inbounds float, float* %605, i32 10
  %606 = load float, float* %arrayidx452, align 4
  %sub453 = fsub float %604, %606
  %conv454 = fpext float %sub453 to double
  %mul455 = fmul double 0x3FF6A09E667F3BCD, %conv454
  %conv456 = fptrunc double %mul455 to float
  store float %conv456, float* %xr, align 4
  %607 = load float*, float** %a.addr, align 4
  %arrayidx457 = getelementptr inbounds float, float* %607, i32 10
  %608 = load float, float* %arrayidx457, align 4
  %609 = load float*, float** %a.addr, align 4
  %arrayidx458 = getelementptr inbounds float, float* %609, i32 2
  %610 = load float, float* %arrayidx458, align 4
  %add459 = fadd float %610, %608
  store float %add459, float* %arrayidx458, align 4
  %611 = load float, float* %xr, align 4
  %612 = load float*, float** %a.addr, align 4
  %arrayidx460 = getelementptr inbounds float, float* %612, i32 10
  store float %611, float* %arrayidx460, align 4
  %613 = load float*, float** %a.addr, align 4
  %arrayidx461 = getelementptr inbounds float, float* %613, i32 3
  %614 = load float, float* %arrayidx461, align 4
  %615 = load float*, float** %a.addr, align 4
  %arrayidx462 = getelementptr inbounds float, float* %615, i32 11
  %616 = load float, float* %arrayidx462, align 4
  %sub463 = fsub float %614, %616
  %conv464 = fpext float %sub463 to double
  %mul465 = fmul double 0x3FF6A09E667F3BCD, %conv464
  %conv466 = fptrunc double %mul465 to float
  store float %conv466, float* %xr, align 4
  %617 = load float*, float** %a.addr, align 4
  %arrayidx467 = getelementptr inbounds float, float* %617, i32 11
  %618 = load float, float* %arrayidx467, align 4
  %619 = load float*, float** %a.addr, align 4
  %arrayidx468 = getelementptr inbounds float, float* %619, i32 3
  %620 = load float, float* %arrayidx468, align 4
  %add469 = fadd float %620, %618
  store float %add469, float* %arrayidx468, align 4
  %621 = load float, float* %xr, align 4
  %622 = load float*, float** %a.addr, align 4
  %arrayidx470 = getelementptr inbounds float, float* %622, i32 11
  store float %621, float* %arrayidx470, align 4
  %623 = load float*, float** %a.addr, align 4
  %arrayidx471 = getelementptr inbounds float, float* %623, i32 18
  %624 = load float, float* %arrayidx471, align 4
  %fneg472 = fneg float %624
  %625 = load float*, float** %a.addr, align 4
  %arrayidx473 = getelementptr inbounds float, float* %625, i32 26
  %626 = load float, float* %arrayidx473, align 4
  %add474 = fadd float %fneg472, %626
  %conv475 = fpext float %add474 to double
  %mul476 = fmul double 0x3FF6A09E667F3BCD, %conv475
  %conv477 = fptrunc double %mul476 to float
  store float %conv477, float* %xr, align 4
  %627 = load float*, float** %a.addr, align 4
  %arrayidx478 = getelementptr inbounds float, float* %627, i32 26
  %628 = load float, float* %arrayidx478, align 4
  %629 = load float*, float** %a.addr, align 4
  %arrayidx479 = getelementptr inbounds float, float* %629, i32 18
  %630 = load float, float* %arrayidx479, align 4
  %add480 = fadd float %630, %628
  store float %add480, float* %arrayidx479, align 4
  %631 = load float, float* %xr, align 4
  %632 = load float*, float** %a.addr, align 4
  %arrayidx481 = getelementptr inbounds float, float* %632, i32 18
  %633 = load float, float* %arrayidx481, align 4
  %sub482 = fsub float %631, %633
  %634 = load float*, float** %a.addr, align 4
  %arrayidx483 = getelementptr inbounds float, float* %634, i32 26
  store float %sub482, float* %arrayidx483, align 4
  %635 = load float*, float** %a.addr, align 4
  %arrayidx484 = getelementptr inbounds float, float* %635, i32 19
  %636 = load float, float* %arrayidx484, align 4
  %fneg485 = fneg float %636
  %637 = load float*, float** %a.addr, align 4
  %arrayidx486 = getelementptr inbounds float, float* %637, i32 27
  %638 = load float, float* %arrayidx486, align 4
  %add487 = fadd float %fneg485, %638
  %conv488 = fpext float %add487 to double
  %mul489 = fmul double 0x3FF6A09E667F3BCD, %conv488
  %conv490 = fptrunc double %mul489 to float
  store float %conv490, float* %xr, align 4
  %639 = load float*, float** %a.addr, align 4
  %arrayidx491 = getelementptr inbounds float, float* %639, i32 27
  %640 = load float, float* %arrayidx491, align 4
  %641 = load float*, float** %a.addr, align 4
  %arrayidx492 = getelementptr inbounds float, float* %641, i32 19
  %642 = load float, float* %arrayidx492, align 4
  %add493 = fadd float %642, %640
  store float %add493, float* %arrayidx492, align 4
  %643 = load float, float* %xr, align 4
  %644 = load float*, float** %a.addr, align 4
  %arrayidx494 = getelementptr inbounds float, float* %644, i32 19
  %645 = load float, float* %arrayidx494, align 4
  %sub495 = fsub float %643, %645
  %646 = load float*, float** %a.addr, align 4
  %arrayidx496 = getelementptr inbounds float, float* %646, i32 27
  store float %sub495, float* %arrayidx496, align 4
  %647 = load float*, float** %a.addr, align 4
  %arrayidx497 = getelementptr inbounds float, float* %647, i32 2
  %648 = load float, float* %arrayidx497, align 4
  store float %648, float* %xr, align 4
  %649 = load float*, float** %a.addr, align 4
  %arrayidx498 = getelementptr inbounds float, float* %649, i32 3
  %650 = load float, float* %arrayidx498, align 4
  %651 = load float*, float** %a.addr, align 4
  %arrayidx499 = getelementptr inbounds float, float* %651, i32 19
  %652 = load float, float* %arrayidx499, align 4
  %sub500 = fsub float %652, %650
  store float %sub500, float* %arrayidx499, align 4
  %653 = load float, float* %xr, align 4
  %654 = load float*, float** %a.addr, align 4
  %arrayidx501 = getelementptr inbounds float, float* %654, i32 3
  %655 = load float, float* %arrayidx501, align 4
  %sub502 = fsub float %655, %653
  store float %sub502, float* %arrayidx501, align 4
  %656 = load float*, float** %a.addr, align 4
  %arrayidx503 = getelementptr inbounds float, float* %656, i32 31
  %657 = load float, float* %arrayidx503, align 4
  %658 = load float, float* %xr, align 4
  %sub504 = fsub float %657, %658
  %659 = load float*, float** %a.addr, align 4
  %arrayidx505 = getelementptr inbounds float, float* %659, i32 2
  store float %sub504, float* %arrayidx505, align 4
  %660 = load float, float* %xr, align 4
  %661 = load float*, float** %a.addr, align 4
  %arrayidx506 = getelementptr inbounds float, float* %661, i32 31
  %662 = load float, float* %arrayidx506, align 4
  %add507 = fadd float %662, %660
  store float %add507, float* %arrayidx506, align 4
  %663 = load float*, float** %a.addr, align 4
  %arrayidx508 = getelementptr inbounds float, float* %663, i32 3
  %664 = load float, float* %arrayidx508, align 4
  store float %664, float* %xr, align 4
  %665 = load float*, float** %a.addr, align 4
  %arrayidx509 = getelementptr inbounds float, float* %665, i32 19
  %666 = load float, float* %arrayidx509, align 4
  %667 = load float*, float** %a.addr, align 4
  %arrayidx510 = getelementptr inbounds float, float* %667, i32 11
  %668 = load float, float* %arrayidx510, align 4
  %sub511 = fsub float %668, %666
  store float %sub511, float* %arrayidx510, align 4
  %669 = load float, float* %xr, align 4
  %670 = load float*, float** %a.addr, align 4
  %arrayidx512 = getelementptr inbounds float, float* %670, i32 18
  %671 = load float, float* %arrayidx512, align 4
  %sub513 = fsub float %671, %669
  store float %sub513, float* %arrayidx512, align 4
  %672 = load float*, float** %a.addr, align 4
  %arrayidx514 = getelementptr inbounds float, float* %672, i32 30
  %673 = load float, float* %arrayidx514, align 4
  %674 = load float, float* %xr, align 4
  %sub515 = fsub float %673, %674
  %675 = load float*, float** %a.addr, align 4
  %arrayidx516 = getelementptr inbounds float, float* %675, i32 3
  store float %sub515, float* %arrayidx516, align 4
  %676 = load float, float* %xr, align 4
  %677 = load float*, float** %a.addr, align 4
  %arrayidx517 = getelementptr inbounds float, float* %677, i32 30
  %678 = load float, float* %arrayidx517, align 4
  %add518 = fadd float %678, %676
  store float %add518, float* %arrayidx517, align 4
  %679 = load float*, float** %a.addr, align 4
  %arrayidx519 = getelementptr inbounds float, float* %679, i32 18
  %680 = load float, float* %arrayidx519, align 4
  store float %680, float* %xr, align 4
  %681 = load float*, float** %a.addr, align 4
  %arrayidx520 = getelementptr inbounds float, float* %681, i32 11
  %682 = load float, float* %arrayidx520, align 4
  %683 = load float*, float** %a.addr, align 4
  %arrayidx521 = getelementptr inbounds float, float* %683, i32 27
  %684 = load float, float* %arrayidx521, align 4
  %sub522 = fsub float %684, %682
  store float %sub522, float* %arrayidx521, align 4
  %685 = load float, float* %xr, align 4
  %686 = load float*, float** %a.addr, align 4
  %arrayidx523 = getelementptr inbounds float, float* %686, i32 19
  %687 = load float, float* %arrayidx523, align 4
  %sub524 = fsub float %687, %685
  store float %sub524, float* %arrayidx523, align 4
  %688 = load float*, float** %a.addr, align 4
  %arrayidx525 = getelementptr inbounds float, float* %688, i32 15
  %689 = load float, float* %arrayidx525, align 4
  %690 = load float, float* %xr, align 4
  %sub526 = fsub float %689, %690
  %691 = load float*, float** %a.addr, align 4
  %arrayidx527 = getelementptr inbounds float, float* %691, i32 18
  store float %sub526, float* %arrayidx527, align 4
  %692 = load float, float* %xr, align 4
  %693 = load float*, float** %a.addr, align 4
  %arrayidx528 = getelementptr inbounds float, float* %693, i32 15
  %694 = load float, float* %arrayidx528, align 4
  %add529 = fadd float %694, %692
  store float %add529, float* %arrayidx528, align 4
  %695 = load float*, float** %a.addr, align 4
  %arrayidx530 = getelementptr inbounds float, float* %695, i32 19
  %696 = load float, float* %arrayidx530, align 4
  store float %696, float* %xr, align 4
  %697 = load float, float* %xr, align 4
  %698 = load float*, float** %a.addr, align 4
  %arrayidx531 = getelementptr inbounds float, float* %698, i32 10
  %699 = load float, float* %arrayidx531, align 4
  %sub532 = fsub float %699, %697
  store float %sub532, float* %arrayidx531, align 4
  %700 = load float*, float** %a.addr, align 4
  %arrayidx533 = getelementptr inbounds float, float* %700, i32 14
  %701 = load float, float* %arrayidx533, align 4
  %702 = load float, float* %xr, align 4
  %sub534 = fsub float %701, %702
  %703 = load float*, float** %a.addr, align 4
  %arrayidx535 = getelementptr inbounds float, float* %703, i32 19
  store float %sub534, float* %arrayidx535, align 4
  %704 = load float, float* %xr, align 4
  %705 = load float*, float** %a.addr, align 4
  %arrayidx536 = getelementptr inbounds float, float* %705, i32 14
  %706 = load float, float* %arrayidx536, align 4
  %add537 = fadd float %706, %704
  store float %add537, float* %arrayidx536, align 4
  %707 = load float*, float** %a.addr, align 4
  %arrayidx538 = getelementptr inbounds float, float* %707, i32 10
  %708 = load float, float* %arrayidx538, align 4
  store float %708, float* %xr, align 4
  %709 = load float, float* %xr, align 4
  %710 = load float*, float** %a.addr, align 4
  %arrayidx539 = getelementptr inbounds float, float* %710, i32 11
  %711 = load float, float* %arrayidx539, align 4
  %sub540 = fsub float %711, %709
  store float %sub540, float* %arrayidx539, align 4
  %712 = load float*, float** %a.addr, align 4
  %arrayidx541 = getelementptr inbounds float, float* %712, i32 23
  %713 = load float, float* %arrayidx541, align 4
  %714 = load float, float* %xr, align 4
  %sub542 = fsub float %713, %714
  %715 = load float*, float** %a.addr, align 4
  %arrayidx543 = getelementptr inbounds float, float* %715, i32 10
  store float %sub542, float* %arrayidx543, align 4
  %716 = load float, float* %xr, align 4
  %717 = load float*, float** %a.addr, align 4
  %arrayidx544 = getelementptr inbounds float, float* %717, i32 23
  %718 = load float, float* %arrayidx544, align 4
  %add545 = fadd float %718, %716
  store float %add545, float* %arrayidx544, align 4
  %719 = load float*, float** %a.addr, align 4
  %arrayidx546 = getelementptr inbounds float, float* %719, i32 11
  %720 = load float, float* %arrayidx546, align 4
  store float %720, float* %xr, align 4
  %721 = load float, float* %xr, align 4
  %722 = load float*, float** %a.addr, align 4
  %arrayidx547 = getelementptr inbounds float, float* %722, i32 26
  %723 = load float, float* %arrayidx547, align 4
  %sub548 = fsub float %723, %721
  store float %sub548, float* %arrayidx547, align 4
  %724 = load float*, float** %a.addr, align 4
  %arrayidx549 = getelementptr inbounds float, float* %724, i32 22
  %725 = load float, float* %arrayidx549, align 4
  %726 = load float, float* %xr, align 4
  %sub550 = fsub float %725, %726
  %727 = load float*, float** %a.addr, align 4
  %arrayidx551 = getelementptr inbounds float, float* %727, i32 11
  store float %sub550, float* %arrayidx551, align 4
  %728 = load float, float* %xr, align 4
  %729 = load float*, float** %a.addr, align 4
  %arrayidx552 = getelementptr inbounds float, float* %729, i32 22
  %730 = load float, float* %arrayidx552, align 4
  %add553 = fadd float %730, %728
  store float %add553, float* %arrayidx552, align 4
  %731 = load float*, float** %a.addr, align 4
  %arrayidx554 = getelementptr inbounds float, float* %731, i32 26
  %732 = load float, float* %arrayidx554, align 4
  store float %732, float* %xr, align 4
  %733 = load float, float* %xr, align 4
  %734 = load float*, float** %a.addr, align 4
  %arrayidx555 = getelementptr inbounds float, float* %734, i32 27
  %735 = load float, float* %arrayidx555, align 4
  %sub556 = fsub float %735, %733
  store float %sub556, float* %arrayidx555, align 4
  %736 = load float*, float** %a.addr, align 4
  %arrayidx557 = getelementptr inbounds float, float* %736, i32 7
  %737 = load float, float* %arrayidx557, align 4
  %738 = load float, float* %xr, align 4
  %sub558 = fsub float %737, %738
  %739 = load float*, float** %a.addr, align 4
  %arrayidx559 = getelementptr inbounds float, float* %739, i32 26
  store float %sub558, float* %arrayidx559, align 4
  %740 = load float, float* %xr, align 4
  %741 = load float*, float** %a.addr, align 4
  %arrayidx560 = getelementptr inbounds float, float* %741, i32 7
  %742 = load float, float* %arrayidx560, align 4
  %add561 = fadd float %742, %740
  store float %add561, float* %arrayidx560, align 4
  %743 = load float*, float** %a.addr, align 4
  %arrayidx562 = getelementptr inbounds float, float* %743, i32 27
  %744 = load float, float* %arrayidx562, align 4
  store float %744, float* %xr, align 4
  %745 = load float*, float** %a.addr, align 4
  %arrayidx563 = getelementptr inbounds float, float* %745, i32 6
  %746 = load float, float* %arrayidx563, align 4
  %747 = load float, float* %xr, align 4
  %sub564 = fsub float %746, %747
  %748 = load float*, float** %a.addr, align 4
  %arrayidx565 = getelementptr inbounds float, float* %748, i32 27
  store float %sub564, float* %arrayidx565, align 4
  %749 = load float, float* %xr, align 4
  %750 = load float*, float** %a.addr, align 4
  %arrayidx566 = getelementptr inbounds float, float* %750, i32 6
  %751 = load float, float* %arrayidx566, align 4
  %add567 = fadd float %751, %749
  store float %add567, float* %arrayidx566, align 4
  %752 = load float*, float** %a.addr, align 4
  %arrayidx568 = getelementptr inbounds float, float* %752, i32 0
  %753 = load float, float* %arrayidx568, align 4
  %754 = load float*, float** %a.addr, align 4
  %arrayidx569 = getelementptr inbounds float, float* %754, i32 4
  %755 = load float, float* %arrayidx569, align 4
  %sub570 = fsub float %753, %755
  %conv571 = fpext float %sub570 to double
  %mul572 = fmul double 0x3FF6A09E667F3BCD, %conv571
  %conv573 = fptrunc double %mul572 to float
  store float %conv573, float* %xr, align 4
  %756 = load float*, float** %a.addr, align 4
  %arrayidx574 = getelementptr inbounds float, float* %756, i32 4
  %757 = load float, float* %arrayidx574, align 4
  %758 = load float*, float** %a.addr, align 4
  %arrayidx575 = getelementptr inbounds float, float* %758, i32 0
  %759 = load float, float* %arrayidx575, align 4
  %add576 = fadd float %759, %757
  store float %add576, float* %arrayidx575, align 4
  %760 = load float, float* %xr, align 4
  %761 = load float*, float** %a.addr, align 4
  %arrayidx577 = getelementptr inbounds float, float* %761, i32 4
  store float %760, float* %arrayidx577, align 4
  %762 = load float*, float** %a.addr, align 4
  %arrayidx578 = getelementptr inbounds float, float* %762, i32 1
  %763 = load float, float* %arrayidx578, align 4
  %764 = load float*, float** %a.addr, align 4
  %arrayidx579 = getelementptr inbounds float, float* %764, i32 5
  %765 = load float, float* %arrayidx579, align 4
  %sub580 = fsub float %763, %765
  %conv581 = fpext float %sub580 to double
  %mul582 = fmul double 0x3FF6A09E667F3BCD, %conv581
  %conv583 = fptrunc double %mul582 to float
  store float %conv583, float* %xr, align 4
  %766 = load float*, float** %a.addr, align 4
  %arrayidx584 = getelementptr inbounds float, float* %766, i32 5
  %767 = load float, float* %arrayidx584, align 4
  %768 = load float*, float** %a.addr, align 4
  %arrayidx585 = getelementptr inbounds float, float* %768, i32 1
  %769 = load float, float* %arrayidx585, align 4
  %add586 = fadd float %769, %767
  store float %add586, float* %arrayidx585, align 4
  %770 = load float, float* %xr, align 4
  %771 = load float*, float** %a.addr, align 4
  %arrayidx587 = getelementptr inbounds float, float* %771, i32 5
  store float %770, float* %arrayidx587, align 4
  %772 = load float*, float** %a.addr, align 4
  %arrayidx588 = getelementptr inbounds float, float* %772, i32 16
  %773 = load float, float* %arrayidx588, align 4
  %774 = load float*, float** %a.addr, align 4
  %arrayidx589 = getelementptr inbounds float, float* %774, i32 20
  %775 = load float, float* %arrayidx589, align 4
  %sub590 = fsub float %773, %775
  %conv591 = fpext float %sub590 to double
  %mul592 = fmul double 0x3FF6A09E667F3BCD, %conv591
  %conv593 = fptrunc double %mul592 to float
  store float %conv593, float* %xr, align 4
  %776 = load float*, float** %a.addr, align 4
  %arrayidx594 = getelementptr inbounds float, float* %776, i32 20
  %777 = load float, float* %arrayidx594, align 4
  %778 = load float*, float** %a.addr, align 4
  %arrayidx595 = getelementptr inbounds float, float* %778, i32 16
  %779 = load float, float* %arrayidx595, align 4
  %add596 = fadd float %779, %777
  store float %add596, float* %arrayidx595, align 4
  %780 = load float, float* %xr, align 4
  %781 = load float*, float** %a.addr, align 4
  %arrayidx597 = getelementptr inbounds float, float* %781, i32 20
  store float %780, float* %arrayidx597, align 4
  %782 = load float*, float** %a.addr, align 4
  %arrayidx598 = getelementptr inbounds float, float* %782, i32 17
  %783 = load float, float* %arrayidx598, align 4
  %784 = load float*, float** %a.addr, align 4
  %arrayidx599 = getelementptr inbounds float, float* %784, i32 21
  %785 = load float, float* %arrayidx599, align 4
  %sub600 = fsub float %783, %785
  %conv601 = fpext float %sub600 to double
  %mul602 = fmul double 0x3FF6A09E667F3BCD, %conv601
  %conv603 = fptrunc double %mul602 to float
  store float %conv603, float* %xr, align 4
  %786 = load float*, float** %a.addr, align 4
  %arrayidx604 = getelementptr inbounds float, float* %786, i32 21
  %787 = load float, float* %arrayidx604, align 4
  %788 = load float*, float** %a.addr, align 4
  %arrayidx605 = getelementptr inbounds float, float* %788, i32 17
  %789 = load float, float* %arrayidx605, align 4
  %add606 = fadd float %789, %787
  store float %add606, float* %arrayidx605, align 4
  %790 = load float, float* %xr, align 4
  %791 = load float*, float** %a.addr, align 4
  %arrayidx607 = getelementptr inbounds float, float* %791, i32 21
  store float %790, float* %arrayidx607, align 4
  %792 = load float*, float** %a.addr, align 4
  %arrayidx608 = getelementptr inbounds float, float* %792, i32 8
  %793 = load float, float* %arrayidx608, align 4
  %794 = load float*, float** %a.addr, align 4
  %arrayidx609 = getelementptr inbounds float, float* %794, i32 12
  %795 = load float, float* %arrayidx609, align 4
  %sub610 = fsub float %793, %795
  %conv611 = fpext float %sub610 to double
  %mul612 = fmul double 0xBFF6A09E667F3BCD, %conv611
  %conv613 = fptrunc double %mul612 to float
  store float %conv613, float* %xr, align 4
  %796 = load float*, float** %a.addr, align 4
  %arrayidx614 = getelementptr inbounds float, float* %796, i32 12
  %797 = load float, float* %arrayidx614, align 4
  %798 = load float*, float** %a.addr, align 4
  %arrayidx615 = getelementptr inbounds float, float* %798, i32 8
  %799 = load float, float* %arrayidx615, align 4
  %add616 = fadd float %799, %797
  store float %add616, float* %arrayidx615, align 4
  %800 = load float, float* %xr, align 4
  %801 = load float*, float** %a.addr, align 4
  %arrayidx617 = getelementptr inbounds float, float* %801, i32 8
  %802 = load float, float* %arrayidx617, align 4
  %sub618 = fsub float %800, %802
  %803 = load float*, float** %a.addr, align 4
  %arrayidx619 = getelementptr inbounds float, float* %803, i32 12
  store float %sub618, float* %arrayidx619, align 4
  %804 = load float*, float** %a.addr, align 4
  %arrayidx620 = getelementptr inbounds float, float* %804, i32 9
  %805 = load float, float* %arrayidx620, align 4
  %806 = load float*, float** %a.addr, align 4
  %arrayidx621 = getelementptr inbounds float, float* %806, i32 13
  %807 = load float, float* %arrayidx621, align 4
  %sub622 = fsub float %805, %807
  %conv623 = fpext float %sub622 to double
  %mul624 = fmul double 0xBFF6A09E667F3BCD, %conv623
  %conv625 = fptrunc double %mul624 to float
  store float %conv625, float* %xr, align 4
  %808 = load float*, float** %a.addr, align 4
  %arrayidx626 = getelementptr inbounds float, float* %808, i32 13
  %809 = load float, float* %arrayidx626, align 4
  %810 = load float*, float** %a.addr, align 4
  %arrayidx627 = getelementptr inbounds float, float* %810, i32 9
  %811 = load float, float* %arrayidx627, align 4
  %add628 = fadd float %811, %809
  store float %add628, float* %arrayidx627, align 4
  %812 = load float, float* %xr, align 4
  %813 = load float*, float** %a.addr, align 4
  %arrayidx629 = getelementptr inbounds float, float* %813, i32 9
  %814 = load float, float* %arrayidx629, align 4
  %sub630 = fsub float %812, %814
  %815 = load float*, float** %a.addr, align 4
  %arrayidx631 = getelementptr inbounds float, float* %815, i32 13
  store float %sub630, float* %arrayidx631, align 4
  %816 = load float*, float** %a.addr, align 4
  %arrayidx632 = getelementptr inbounds float, float* %816, i32 25
  %817 = load float, float* %arrayidx632, align 4
  %818 = load float*, float** %a.addr, align 4
  %arrayidx633 = getelementptr inbounds float, float* %818, i32 29
  %819 = load float, float* %arrayidx633, align 4
  %sub634 = fsub float %817, %819
  %conv635 = fpext float %sub634 to double
  %mul636 = fmul double 0xBFF6A09E667F3BCD, %conv635
  %conv637 = fptrunc double %mul636 to float
  store float %conv637, float* %xr, align 4
  %820 = load float*, float** %a.addr, align 4
  %arrayidx638 = getelementptr inbounds float, float* %820, i32 29
  %821 = load float, float* %arrayidx638, align 4
  %822 = load float*, float** %a.addr, align 4
  %arrayidx639 = getelementptr inbounds float, float* %822, i32 25
  %823 = load float, float* %arrayidx639, align 4
  %add640 = fadd float %823, %821
  store float %add640, float* %arrayidx639, align 4
  %824 = load float, float* %xr, align 4
  %825 = load float*, float** %a.addr, align 4
  %arrayidx641 = getelementptr inbounds float, float* %825, i32 25
  %826 = load float, float* %arrayidx641, align 4
  %sub642 = fsub float %824, %826
  %827 = load float*, float** %a.addr, align 4
  %arrayidx643 = getelementptr inbounds float, float* %827, i32 29
  store float %sub642, float* %arrayidx643, align 4
  %828 = load float*, float** %a.addr, align 4
  %arrayidx644 = getelementptr inbounds float, float* %828, i32 24
  %829 = load float, float* %arrayidx644, align 4
  %830 = load float*, float** %a.addr, align 4
  %arrayidx645 = getelementptr inbounds float, float* %830, i32 28
  %831 = load float, float* %arrayidx645, align 4
  %add646 = fadd float %829, %831
  %conv647 = fpext float %add646 to double
  %mul648 = fmul double 0xBFF6A09E667F3BCD, %conv647
  %conv649 = fptrunc double %mul648 to float
  store float %conv649, float* %xr, align 4
  %832 = load float*, float** %a.addr, align 4
  %arrayidx650 = getelementptr inbounds float, float* %832, i32 28
  %833 = load float, float* %arrayidx650, align 4
  %834 = load float*, float** %a.addr, align 4
  %arrayidx651 = getelementptr inbounds float, float* %834, i32 24
  %835 = load float, float* %arrayidx651, align 4
  %sub652 = fsub float %835, %833
  store float %sub652, float* %arrayidx651, align 4
  %836 = load float, float* %xr, align 4
  %837 = load float*, float** %a.addr, align 4
  %arrayidx653 = getelementptr inbounds float, float* %837, i32 24
  %838 = load float, float* %arrayidx653, align 4
  %sub654 = fsub float %836, %838
  %839 = load float*, float** %a.addr, align 4
  %arrayidx655 = getelementptr inbounds float, float* %839, i32 28
  store float %sub654, float* %arrayidx655, align 4
  %840 = load float*, float** %a.addr, align 4
  %arrayidx656 = getelementptr inbounds float, float* %840, i32 24
  %841 = load float, float* %arrayidx656, align 4
  %842 = load float*, float** %a.addr, align 4
  %arrayidx657 = getelementptr inbounds float, float* %842, i32 16
  %843 = load float, float* %arrayidx657, align 4
  %sub658 = fsub float %841, %843
  store float %sub658, float* %xr, align 4
  %844 = load float, float* %xr, align 4
  %845 = load float*, float** %a.addr, align 4
  %arrayidx659 = getelementptr inbounds float, float* %845, i32 24
  store float %844, float* %arrayidx659, align 4
  %846 = load float*, float** %a.addr, align 4
  %arrayidx660 = getelementptr inbounds float, float* %846, i32 20
  %847 = load float, float* %arrayidx660, align 4
  %848 = load float, float* %xr, align 4
  %sub661 = fsub float %847, %848
  store float %sub661, float* %xr, align 4
  %849 = load float, float* %xr, align 4
  %850 = load float*, float** %a.addr, align 4
  %arrayidx662 = getelementptr inbounds float, float* %850, i32 20
  store float %849, float* %arrayidx662, align 4
  %851 = load float*, float** %a.addr, align 4
  %arrayidx663 = getelementptr inbounds float, float* %851, i32 28
  %852 = load float, float* %arrayidx663, align 4
  %853 = load float, float* %xr, align 4
  %sub664 = fsub float %852, %853
  store float %sub664, float* %xr, align 4
  %854 = load float, float* %xr, align 4
  %855 = load float*, float** %a.addr, align 4
  %arrayidx665 = getelementptr inbounds float, float* %855, i32 28
  store float %854, float* %arrayidx665, align 4
  %856 = load float*, float** %a.addr, align 4
  %arrayidx666 = getelementptr inbounds float, float* %856, i32 25
  %857 = load float, float* %arrayidx666, align 4
  %858 = load float*, float** %a.addr, align 4
  %arrayidx667 = getelementptr inbounds float, float* %858, i32 17
  %859 = load float, float* %arrayidx667, align 4
  %sub668 = fsub float %857, %859
  store float %sub668, float* %xr, align 4
  %860 = load float, float* %xr, align 4
  %861 = load float*, float** %a.addr, align 4
  %arrayidx669 = getelementptr inbounds float, float* %861, i32 25
  store float %860, float* %arrayidx669, align 4
  %862 = load float*, float** %a.addr, align 4
  %arrayidx670 = getelementptr inbounds float, float* %862, i32 21
  %863 = load float, float* %arrayidx670, align 4
  %864 = load float, float* %xr, align 4
  %sub671 = fsub float %863, %864
  store float %sub671, float* %xr, align 4
  %865 = load float, float* %xr, align 4
  %866 = load float*, float** %a.addr, align 4
  %arrayidx672 = getelementptr inbounds float, float* %866, i32 21
  store float %865, float* %arrayidx672, align 4
  %867 = load float*, float** %a.addr, align 4
  %arrayidx673 = getelementptr inbounds float, float* %867, i32 29
  %868 = load float, float* %arrayidx673, align 4
  %869 = load float, float* %xr, align 4
  %sub674 = fsub float %868, %869
  store float %sub674, float* %xr, align 4
  %870 = load float, float* %xr, align 4
  %871 = load float*, float** %a.addr, align 4
  %arrayidx675 = getelementptr inbounds float, float* %871, i32 29
  store float %870, float* %arrayidx675, align 4
  %872 = load float*, float** %a.addr, align 4
  %arrayidx676 = getelementptr inbounds float, float* %872, i32 17
  %873 = load float, float* %arrayidx676, align 4
  %874 = load float*, float** %a.addr, align 4
  %arrayidx677 = getelementptr inbounds float, float* %874, i32 1
  %875 = load float, float* %arrayidx677, align 4
  %sub678 = fsub float %873, %875
  store float %sub678, float* %xr, align 4
  %876 = load float, float* %xr, align 4
  %877 = load float*, float** %a.addr, align 4
  %arrayidx679 = getelementptr inbounds float, float* %877, i32 17
  store float %876, float* %arrayidx679, align 4
  %878 = load float*, float** %a.addr, align 4
  %arrayidx680 = getelementptr inbounds float, float* %878, i32 9
  %879 = load float, float* %arrayidx680, align 4
  %880 = load float, float* %xr, align 4
  %sub681 = fsub float %879, %880
  store float %sub681, float* %xr, align 4
  %881 = load float, float* %xr, align 4
  %882 = load float*, float** %a.addr, align 4
  %arrayidx682 = getelementptr inbounds float, float* %882, i32 9
  store float %881, float* %arrayidx682, align 4
  %883 = load float*, float** %a.addr, align 4
  %arrayidx683 = getelementptr inbounds float, float* %883, i32 25
  %884 = load float, float* %arrayidx683, align 4
  %885 = load float, float* %xr, align 4
  %sub684 = fsub float %884, %885
  store float %sub684, float* %xr, align 4
  %886 = load float, float* %xr, align 4
  %887 = load float*, float** %a.addr, align 4
  %arrayidx685 = getelementptr inbounds float, float* %887, i32 25
  store float %886, float* %arrayidx685, align 4
  %888 = load float*, float** %a.addr, align 4
  %arrayidx686 = getelementptr inbounds float, float* %888, i32 5
  %889 = load float, float* %arrayidx686, align 4
  %890 = load float, float* %xr, align 4
  %sub687 = fsub float %889, %890
  store float %sub687, float* %xr, align 4
  %891 = load float, float* %xr, align 4
  %892 = load float*, float** %a.addr, align 4
  %arrayidx688 = getelementptr inbounds float, float* %892, i32 5
  store float %891, float* %arrayidx688, align 4
  %893 = load float*, float** %a.addr, align 4
  %arrayidx689 = getelementptr inbounds float, float* %893, i32 21
  %894 = load float, float* %arrayidx689, align 4
  %895 = load float, float* %xr, align 4
  %sub690 = fsub float %894, %895
  store float %sub690, float* %xr, align 4
  %896 = load float, float* %xr, align 4
  %897 = load float*, float** %a.addr, align 4
  %arrayidx691 = getelementptr inbounds float, float* %897, i32 21
  store float %896, float* %arrayidx691, align 4
  %898 = load float*, float** %a.addr, align 4
  %arrayidx692 = getelementptr inbounds float, float* %898, i32 13
  %899 = load float, float* %arrayidx692, align 4
  %900 = load float, float* %xr, align 4
  %sub693 = fsub float %899, %900
  store float %sub693, float* %xr, align 4
  %901 = load float, float* %xr, align 4
  %902 = load float*, float** %a.addr, align 4
  %arrayidx694 = getelementptr inbounds float, float* %902, i32 13
  store float %901, float* %arrayidx694, align 4
  %903 = load float*, float** %a.addr, align 4
  %arrayidx695 = getelementptr inbounds float, float* %903, i32 29
  %904 = load float, float* %arrayidx695, align 4
  %905 = load float, float* %xr, align 4
  %sub696 = fsub float %904, %905
  store float %sub696, float* %xr, align 4
  %906 = load float, float* %xr, align 4
  %907 = load float*, float** %a.addr, align 4
  %arrayidx697 = getelementptr inbounds float, float* %907, i32 29
  store float %906, float* %arrayidx697, align 4
  %908 = load float*, float** %a.addr, align 4
  %arrayidx698 = getelementptr inbounds float, float* %908, i32 1
  %909 = load float, float* %arrayidx698, align 4
  %910 = load float*, float** %a.addr, align 4
  %arrayidx699 = getelementptr inbounds float, float* %910, i32 0
  %911 = load float, float* %arrayidx699, align 4
  %sub700 = fsub float %909, %911
  store float %sub700, float* %xr, align 4
  %912 = load float, float* %xr, align 4
  %913 = load float*, float** %a.addr, align 4
  %arrayidx701 = getelementptr inbounds float, float* %913, i32 1
  store float %912, float* %arrayidx701, align 4
  %914 = load float*, float** %a.addr, align 4
  %arrayidx702 = getelementptr inbounds float, float* %914, i32 16
  %915 = load float, float* %arrayidx702, align 4
  %916 = load float, float* %xr, align 4
  %sub703 = fsub float %915, %916
  store float %sub703, float* %xr, align 4
  %917 = load float, float* %xr, align 4
  %918 = load float*, float** %a.addr, align 4
  %arrayidx704 = getelementptr inbounds float, float* %918, i32 16
  store float %917, float* %arrayidx704, align 4
  %919 = load float*, float** %a.addr, align 4
  %arrayidx705 = getelementptr inbounds float, float* %919, i32 17
  %920 = load float, float* %arrayidx705, align 4
  %921 = load float, float* %xr, align 4
  %sub706 = fsub float %920, %921
  store float %sub706, float* %xr, align 4
  %922 = load float, float* %xr, align 4
  %923 = load float*, float** %a.addr, align 4
  %arrayidx707 = getelementptr inbounds float, float* %923, i32 17
  store float %922, float* %arrayidx707, align 4
  %924 = load float*, float** %a.addr, align 4
  %arrayidx708 = getelementptr inbounds float, float* %924, i32 8
  %925 = load float, float* %arrayidx708, align 4
  %926 = load float, float* %xr, align 4
  %sub709 = fsub float %925, %926
  store float %sub709, float* %xr, align 4
  %927 = load float, float* %xr, align 4
  %928 = load float*, float** %a.addr, align 4
  %arrayidx710 = getelementptr inbounds float, float* %928, i32 8
  store float %927, float* %arrayidx710, align 4
  %929 = load float*, float** %a.addr, align 4
  %arrayidx711 = getelementptr inbounds float, float* %929, i32 9
  %930 = load float, float* %arrayidx711, align 4
  %931 = load float, float* %xr, align 4
  %sub712 = fsub float %930, %931
  store float %sub712, float* %xr, align 4
  %932 = load float, float* %xr, align 4
  %933 = load float*, float** %a.addr, align 4
  %arrayidx713 = getelementptr inbounds float, float* %933, i32 9
  store float %932, float* %arrayidx713, align 4
  %934 = load float*, float** %a.addr, align 4
  %arrayidx714 = getelementptr inbounds float, float* %934, i32 24
  %935 = load float, float* %arrayidx714, align 4
  %936 = load float, float* %xr, align 4
  %sub715 = fsub float %935, %936
  store float %sub715, float* %xr, align 4
  %937 = load float, float* %xr, align 4
  %938 = load float*, float** %a.addr, align 4
  %arrayidx716 = getelementptr inbounds float, float* %938, i32 24
  store float %937, float* %arrayidx716, align 4
  %939 = load float*, float** %a.addr, align 4
  %arrayidx717 = getelementptr inbounds float, float* %939, i32 25
  %940 = load float, float* %arrayidx717, align 4
  %941 = load float, float* %xr, align 4
  %sub718 = fsub float %940, %941
  store float %sub718, float* %xr, align 4
  %942 = load float, float* %xr, align 4
  %943 = load float*, float** %a.addr, align 4
  %arrayidx719 = getelementptr inbounds float, float* %943, i32 25
  store float %942, float* %arrayidx719, align 4
  %944 = load float*, float** %a.addr, align 4
  %arrayidx720 = getelementptr inbounds float, float* %944, i32 4
  %945 = load float, float* %arrayidx720, align 4
  %946 = load float, float* %xr, align 4
  %sub721 = fsub float %945, %946
  store float %sub721, float* %xr, align 4
  %947 = load float, float* %xr, align 4
  %948 = load float*, float** %a.addr, align 4
  %arrayidx722 = getelementptr inbounds float, float* %948, i32 4
  store float %947, float* %arrayidx722, align 4
  %949 = load float*, float** %a.addr, align 4
  %arrayidx723 = getelementptr inbounds float, float* %949, i32 5
  %950 = load float, float* %arrayidx723, align 4
  %951 = load float, float* %xr, align 4
  %sub724 = fsub float %950, %951
  store float %sub724, float* %xr, align 4
  %952 = load float, float* %xr, align 4
  %953 = load float*, float** %a.addr, align 4
  %arrayidx725 = getelementptr inbounds float, float* %953, i32 5
  store float %952, float* %arrayidx725, align 4
  %954 = load float*, float** %a.addr, align 4
  %arrayidx726 = getelementptr inbounds float, float* %954, i32 20
  %955 = load float, float* %arrayidx726, align 4
  %956 = load float, float* %xr, align 4
  %sub727 = fsub float %955, %956
  store float %sub727, float* %xr, align 4
  %957 = load float, float* %xr, align 4
  %958 = load float*, float** %a.addr, align 4
  %arrayidx728 = getelementptr inbounds float, float* %958, i32 20
  store float %957, float* %arrayidx728, align 4
  %959 = load float*, float** %a.addr, align 4
  %arrayidx729 = getelementptr inbounds float, float* %959, i32 21
  %960 = load float, float* %arrayidx729, align 4
  %961 = load float, float* %xr, align 4
  %sub730 = fsub float %960, %961
  store float %sub730, float* %xr, align 4
  %962 = load float, float* %xr, align 4
  %963 = load float*, float** %a.addr, align 4
  %arrayidx731 = getelementptr inbounds float, float* %963, i32 21
  store float %962, float* %arrayidx731, align 4
  %964 = load float*, float** %a.addr, align 4
  %arrayidx732 = getelementptr inbounds float, float* %964, i32 12
  %965 = load float, float* %arrayidx732, align 4
  %966 = load float, float* %xr, align 4
  %sub733 = fsub float %965, %966
  store float %sub733, float* %xr, align 4
  %967 = load float, float* %xr, align 4
  %968 = load float*, float** %a.addr, align 4
  %arrayidx734 = getelementptr inbounds float, float* %968, i32 12
  store float %967, float* %arrayidx734, align 4
  %969 = load float*, float** %a.addr, align 4
  %arrayidx735 = getelementptr inbounds float, float* %969, i32 13
  %970 = load float, float* %arrayidx735, align 4
  %971 = load float, float* %xr, align 4
  %sub736 = fsub float %970, %971
  store float %sub736, float* %xr, align 4
  %972 = load float, float* %xr, align 4
  %973 = load float*, float** %a.addr, align 4
  %arrayidx737 = getelementptr inbounds float, float* %973, i32 13
  store float %972, float* %arrayidx737, align 4
  %974 = load float*, float** %a.addr, align 4
  %arrayidx738 = getelementptr inbounds float, float* %974, i32 28
  %975 = load float, float* %arrayidx738, align 4
  %976 = load float, float* %xr, align 4
  %sub739 = fsub float %975, %976
  store float %sub739, float* %xr, align 4
  %977 = load float, float* %xr, align 4
  %978 = load float*, float** %a.addr, align 4
  %arrayidx740 = getelementptr inbounds float, float* %978, i32 28
  store float %977, float* %arrayidx740, align 4
  %979 = load float*, float** %a.addr, align 4
  %arrayidx741 = getelementptr inbounds float, float* %979, i32 29
  %980 = load float, float* %arrayidx741, align 4
  %981 = load float, float* %xr, align 4
  %sub742 = fsub float %980, %981
  store float %sub742, float* %xr, align 4
  %982 = load float, float* %xr, align 4
  %983 = load float*, float** %a.addr, align 4
  %arrayidx743 = getelementptr inbounds float, float* %983, i32 29
  store float %982, float* %arrayidx743, align 4
  %984 = load float*, float** %a.addr, align 4
  %arrayidx744 = getelementptr inbounds float, float* %984, i32 0
  %985 = load float, float* %arrayidx744, align 4
  store float %985, float* %xr, align 4
  %986 = load float*, float** %a.addr, align 4
  %arrayidx745 = getelementptr inbounds float, float* %986, i32 31
  %987 = load float, float* %arrayidx745, align 4
  %988 = load float*, float** %a.addr, align 4
  %arrayidx746 = getelementptr inbounds float, float* %988, i32 0
  %989 = load float, float* %arrayidx746, align 4
  %add747 = fadd float %989, %987
  store float %add747, float* %arrayidx746, align 4
  %990 = load float, float* %xr, align 4
  %991 = load float*, float** %a.addr, align 4
  %arrayidx748 = getelementptr inbounds float, float* %991, i32 31
  %992 = load float, float* %arrayidx748, align 4
  %sub749 = fsub float %992, %990
  store float %sub749, float* %arrayidx748, align 4
  %993 = load float*, float** %a.addr, align 4
  %arrayidx750 = getelementptr inbounds float, float* %993, i32 1
  %994 = load float, float* %arrayidx750, align 4
  store float %994, float* %xr, align 4
  %995 = load float*, float** %a.addr, align 4
  %arrayidx751 = getelementptr inbounds float, float* %995, i32 30
  %996 = load float, float* %arrayidx751, align 4
  %997 = load float*, float** %a.addr, align 4
  %arrayidx752 = getelementptr inbounds float, float* %997, i32 1
  %998 = load float, float* %arrayidx752, align 4
  %add753 = fadd float %998, %996
  store float %add753, float* %arrayidx752, align 4
  %999 = load float, float* %xr, align 4
  %1000 = load float*, float** %a.addr, align 4
  %arrayidx754 = getelementptr inbounds float, float* %1000, i32 30
  %1001 = load float, float* %arrayidx754, align 4
  %sub755 = fsub float %1001, %999
  store float %sub755, float* %arrayidx754, align 4
  %1002 = load float*, float** %a.addr, align 4
  %arrayidx756 = getelementptr inbounds float, float* %1002, i32 16
  %1003 = load float, float* %arrayidx756, align 4
  store float %1003, float* %xr, align 4
  %1004 = load float*, float** %a.addr, align 4
  %arrayidx757 = getelementptr inbounds float, float* %1004, i32 15
  %1005 = load float, float* %arrayidx757, align 4
  %1006 = load float*, float** %a.addr, align 4
  %arrayidx758 = getelementptr inbounds float, float* %1006, i32 16
  %1007 = load float, float* %arrayidx758, align 4
  %add759 = fadd float %1007, %1005
  store float %add759, float* %arrayidx758, align 4
  %1008 = load float, float* %xr, align 4
  %1009 = load float*, float** %a.addr, align 4
  %arrayidx760 = getelementptr inbounds float, float* %1009, i32 15
  %1010 = load float, float* %arrayidx760, align 4
  %sub761 = fsub float %1010, %1008
  store float %sub761, float* %arrayidx760, align 4
  %1011 = load float*, float** %a.addr, align 4
  %arrayidx762 = getelementptr inbounds float, float* %1011, i32 17
  %1012 = load float, float* %arrayidx762, align 4
  store float %1012, float* %xr, align 4
  %1013 = load float*, float** %a.addr, align 4
  %arrayidx763 = getelementptr inbounds float, float* %1013, i32 14
  %1014 = load float, float* %arrayidx763, align 4
  %1015 = load float*, float** %a.addr, align 4
  %arrayidx764 = getelementptr inbounds float, float* %1015, i32 17
  %1016 = load float, float* %arrayidx764, align 4
  %add765 = fadd float %1016, %1014
  store float %add765, float* %arrayidx764, align 4
  %1017 = load float, float* %xr, align 4
  %1018 = load float*, float** %a.addr, align 4
  %arrayidx766 = getelementptr inbounds float, float* %1018, i32 14
  %1019 = load float, float* %arrayidx766, align 4
  %sub767 = fsub float %1019, %1017
  store float %sub767, float* %arrayidx766, align 4
  %1020 = load float*, float** %a.addr, align 4
  %arrayidx768 = getelementptr inbounds float, float* %1020, i32 8
  %1021 = load float, float* %arrayidx768, align 4
  store float %1021, float* %xr, align 4
  %1022 = load float*, float** %a.addr, align 4
  %arrayidx769 = getelementptr inbounds float, float* %1022, i32 23
  %1023 = load float, float* %arrayidx769, align 4
  %1024 = load float*, float** %a.addr, align 4
  %arrayidx770 = getelementptr inbounds float, float* %1024, i32 8
  %1025 = load float, float* %arrayidx770, align 4
  %add771 = fadd float %1025, %1023
  store float %add771, float* %arrayidx770, align 4
  %1026 = load float, float* %xr, align 4
  %1027 = load float*, float** %a.addr, align 4
  %arrayidx772 = getelementptr inbounds float, float* %1027, i32 23
  %1028 = load float, float* %arrayidx772, align 4
  %sub773 = fsub float %1028, %1026
  store float %sub773, float* %arrayidx772, align 4
  %1029 = load float*, float** %a.addr, align 4
  %arrayidx774 = getelementptr inbounds float, float* %1029, i32 9
  %1030 = load float, float* %arrayidx774, align 4
  store float %1030, float* %xr, align 4
  %1031 = load float*, float** %a.addr, align 4
  %arrayidx775 = getelementptr inbounds float, float* %1031, i32 22
  %1032 = load float, float* %arrayidx775, align 4
  %1033 = load float*, float** %a.addr, align 4
  %arrayidx776 = getelementptr inbounds float, float* %1033, i32 9
  %1034 = load float, float* %arrayidx776, align 4
  %add777 = fadd float %1034, %1032
  store float %add777, float* %arrayidx776, align 4
  %1035 = load float, float* %xr, align 4
  %1036 = load float*, float** %a.addr, align 4
  %arrayidx778 = getelementptr inbounds float, float* %1036, i32 22
  %1037 = load float, float* %arrayidx778, align 4
  %sub779 = fsub float %1037, %1035
  store float %sub779, float* %arrayidx778, align 4
  %1038 = load float*, float** %a.addr, align 4
  %arrayidx780 = getelementptr inbounds float, float* %1038, i32 24
  %1039 = load float, float* %arrayidx780, align 4
  store float %1039, float* %xr, align 4
  %1040 = load float*, float** %a.addr, align 4
  %arrayidx781 = getelementptr inbounds float, float* %1040, i32 7
  %1041 = load float, float* %arrayidx781, align 4
  %1042 = load float*, float** %a.addr, align 4
  %arrayidx782 = getelementptr inbounds float, float* %1042, i32 24
  %1043 = load float, float* %arrayidx782, align 4
  %add783 = fadd float %1043, %1041
  store float %add783, float* %arrayidx782, align 4
  %1044 = load float, float* %xr, align 4
  %1045 = load float*, float** %a.addr, align 4
  %arrayidx784 = getelementptr inbounds float, float* %1045, i32 7
  %1046 = load float, float* %arrayidx784, align 4
  %sub785 = fsub float %1046, %1044
  store float %sub785, float* %arrayidx784, align 4
  %1047 = load float*, float** %a.addr, align 4
  %arrayidx786 = getelementptr inbounds float, float* %1047, i32 25
  %1048 = load float, float* %arrayidx786, align 4
  store float %1048, float* %xr, align 4
  %1049 = load float*, float** %a.addr, align 4
  %arrayidx787 = getelementptr inbounds float, float* %1049, i32 6
  %1050 = load float, float* %arrayidx787, align 4
  %1051 = load float*, float** %a.addr, align 4
  %arrayidx788 = getelementptr inbounds float, float* %1051, i32 25
  %1052 = load float, float* %arrayidx788, align 4
  %add789 = fadd float %1052, %1050
  store float %add789, float* %arrayidx788, align 4
  %1053 = load float, float* %xr, align 4
  %1054 = load float*, float** %a.addr, align 4
  %arrayidx790 = getelementptr inbounds float, float* %1054, i32 6
  %1055 = load float, float* %arrayidx790, align 4
  %sub791 = fsub float %1055, %1053
  store float %sub791, float* %arrayidx790, align 4
  %1056 = load float*, float** %a.addr, align 4
  %arrayidx792 = getelementptr inbounds float, float* %1056, i32 4
  %1057 = load float, float* %arrayidx792, align 4
  store float %1057, float* %xr, align 4
  %1058 = load float*, float** %a.addr, align 4
  %arrayidx793 = getelementptr inbounds float, float* %1058, i32 27
  %1059 = load float, float* %arrayidx793, align 4
  %1060 = load float*, float** %a.addr, align 4
  %arrayidx794 = getelementptr inbounds float, float* %1060, i32 4
  %1061 = load float, float* %arrayidx794, align 4
  %add795 = fadd float %1061, %1059
  store float %add795, float* %arrayidx794, align 4
  %1062 = load float, float* %xr, align 4
  %1063 = load float*, float** %a.addr, align 4
  %arrayidx796 = getelementptr inbounds float, float* %1063, i32 27
  %1064 = load float, float* %arrayidx796, align 4
  %sub797 = fsub float %1064, %1062
  store float %sub797, float* %arrayidx796, align 4
  %1065 = load float*, float** %a.addr, align 4
  %arrayidx798 = getelementptr inbounds float, float* %1065, i32 5
  %1066 = load float, float* %arrayidx798, align 4
  store float %1066, float* %xr, align 4
  %1067 = load float*, float** %a.addr, align 4
  %arrayidx799 = getelementptr inbounds float, float* %1067, i32 26
  %1068 = load float, float* %arrayidx799, align 4
  %1069 = load float*, float** %a.addr, align 4
  %arrayidx800 = getelementptr inbounds float, float* %1069, i32 5
  %1070 = load float, float* %arrayidx800, align 4
  %add801 = fadd float %1070, %1068
  store float %add801, float* %arrayidx800, align 4
  %1071 = load float, float* %xr, align 4
  %1072 = load float*, float** %a.addr, align 4
  %arrayidx802 = getelementptr inbounds float, float* %1072, i32 26
  %1073 = load float, float* %arrayidx802, align 4
  %sub803 = fsub float %1073, %1071
  store float %sub803, float* %arrayidx802, align 4
  %1074 = load float*, float** %a.addr, align 4
  %arrayidx804 = getelementptr inbounds float, float* %1074, i32 20
  %1075 = load float, float* %arrayidx804, align 4
  store float %1075, float* %xr, align 4
  %1076 = load float*, float** %a.addr, align 4
  %arrayidx805 = getelementptr inbounds float, float* %1076, i32 11
  %1077 = load float, float* %arrayidx805, align 4
  %1078 = load float*, float** %a.addr, align 4
  %arrayidx806 = getelementptr inbounds float, float* %1078, i32 20
  %1079 = load float, float* %arrayidx806, align 4
  %add807 = fadd float %1079, %1077
  store float %add807, float* %arrayidx806, align 4
  %1080 = load float, float* %xr, align 4
  %1081 = load float*, float** %a.addr, align 4
  %arrayidx808 = getelementptr inbounds float, float* %1081, i32 11
  %1082 = load float, float* %arrayidx808, align 4
  %sub809 = fsub float %1082, %1080
  store float %sub809, float* %arrayidx808, align 4
  %1083 = load float*, float** %a.addr, align 4
  %arrayidx810 = getelementptr inbounds float, float* %1083, i32 21
  %1084 = load float, float* %arrayidx810, align 4
  store float %1084, float* %xr, align 4
  %1085 = load float*, float** %a.addr, align 4
  %arrayidx811 = getelementptr inbounds float, float* %1085, i32 10
  %1086 = load float, float* %arrayidx811, align 4
  %1087 = load float*, float** %a.addr, align 4
  %arrayidx812 = getelementptr inbounds float, float* %1087, i32 21
  %1088 = load float, float* %arrayidx812, align 4
  %add813 = fadd float %1088, %1086
  store float %add813, float* %arrayidx812, align 4
  %1089 = load float, float* %xr, align 4
  %1090 = load float*, float** %a.addr, align 4
  %arrayidx814 = getelementptr inbounds float, float* %1090, i32 10
  %1091 = load float, float* %arrayidx814, align 4
  %sub815 = fsub float %1091, %1089
  store float %sub815, float* %arrayidx814, align 4
  %1092 = load float*, float** %a.addr, align 4
  %arrayidx816 = getelementptr inbounds float, float* %1092, i32 12
  %1093 = load float, float* %arrayidx816, align 4
  store float %1093, float* %xr, align 4
  %1094 = load float*, float** %a.addr, align 4
  %arrayidx817 = getelementptr inbounds float, float* %1094, i32 19
  %1095 = load float, float* %arrayidx817, align 4
  %1096 = load float*, float** %a.addr, align 4
  %arrayidx818 = getelementptr inbounds float, float* %1096, i32 12
  %1097 = load float, float* %arrayidx818, align 4
  %add819 = fadd float %1097, %1095
  store float %add819, float* %arrayidx818, align 4
  %1098 = load float, float* %xr, align 4
  %1099 = load float*, float** %a.addr, align 4
  %arrayidx820 = getelementptr inbounds float, float* %1099, i32 19
  %1100 = load float, float* %arrayidx820, align 4
  %sub821 = fsub float %1100, %1098
  store float %sub821, float* %arrayidx820, align 4
  %1101 = load float*, float** %a.addr, align 4
  %arrayidx822 = getelementptr inbounds float, float* %1101, i32 13
  %1102 = load float, float* %arrayidx822, align 4
  store float %1102, float* %xr, align 4
  %1103 = load float*, float** %a.addr, align 4
  %arrayidx823 = getelementptr inbounds float, float* %1103, i32 18
  %1104 = load float, float* %arrayidx823, align 4
  %1105 = load float*, float** %a.addr, align 4
  %arrayidx824 = getelementptr inbounds float, float* %1105, i32 13
  %1106 = load float, float* %arrayidx824, align 4
  %add825 = fadd float %1106, %1104
  store float %add825, float* %arrayidx824, align 4
  %1107 = load float, float* %xr, align 4
  %1108 = load float*, float** %a.addr, align 4
  %arrayidx826 = getelementptr inbounds float, float* %1108, i32 18
  %1109 = load float, float* %arrayidx826, align 4
  %sub827 = fsub float %1109, %1107
  store float %sub827, float* %arrayidx826, align 4
  %1110 = load float*, float** %a.addr, align 4
  %arrayidx828 = getelementptr inbounds float, float* %1110, i32 28
  %1111 = load float, float* %arrayidx828, align 4
  store float %1111, float* %xr, align 4
  %1112 = load float*, float** %a.addr, align 4
  %arrayidx829 = getelementptr inbounds float, float* %1112, i32 3
  %1113 = load float, float* %arrayidx829, align 4
  %1114 = load float*, float** %a.addr, align 4
  %arrayidx830 = getelementptr inbounds float, float* %1114, i32 28
  %1115 = load float, float* %arrayidx830, align 4
  %add831 = fadd float %1115, %1113
  store float %add831, float* %arrayidx830, align 4
  %1116 = load float, float* %xr, align 4
  %1117 = load float*, float** %a.addr, align 4
  %arrayidx832 = getelementptr inbounds float, float* %1117, i32 3
  %1118 = load float, float* %arrayidx832, align 4
  %sub833 = fsub float %1118, %1116
  store float %sub833, float* %arrayidx832, align 4
  %1119 = load float*, float** %a.addr, align 4
  %arrayidx834 = getelementptr inbounds float, float* %1119, i32 29
  %1120 = load float, float* %arrayidx834, align 4
  store float %1120, float* %xr, align 4
  %1121 = load float*, float** %a.addr, align 4
  %arrayidx835 = getelementptr inbounds float, float* %1121, i32 2
  %1122 = load float, float* %arrayidx835, align 4
  %1123 = load float*, float** %a.addr, align 4
  %arrayidx836 = getelementptr inbounds float, float* %1123, i32 29
  %1124 = load float, float* %arrayidx836, align 4
  %add837 = fadd float %1124, %1122
  store float %add837, float* %arrayidx836, align 4
  %1125 = load float, float* %xr, align 4
  %1126 = load float*, float** %a.addr, align 4
  %arrayidx838 = getelementptr inbounds float, float* %1126, i32 2
  %1127 = load float, float* %arrayidx838, align 4
  %sub839 = fsub float %1127, %1125
  store float %sub839, float* %arrayidx838, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define internal void @mdct_short(float* %inout) #0 {
entry:
  %inout.addr = alloca float*, align 4
  %l = alloca i32, align 4
  %tc0 = alloca float, align 4
  %tc1 = alloca float, align 4
  %tc2 = alloca float, align 4
  %ts0 = alloca float, align 4
  %ts1 = alloca float, align 4
  %ts2 = alloca float, align 4
  store float* %inout, float** %inout.addr, align 4
  store i32 0, i32* %l, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %l, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load float*, float** %inout.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 6
  %2 = load float, float* %arrayidx, align 4
  %3 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 0), align 16
  %mul = fmul float %2, %3
  %4 = load float*, float** %inout.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %4, i32 15
  %5 = load float, float* %arrayidx1, align 4
  %sub = fsub float %mul, %5
  store float %sub, float* %ts0, align 4
  %6 = load float*, float** %inout.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx2, align 4
  %8 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 2), align 8
  %mul3 = fmul float %7, %8
  %9 = load float*, float** %inout.addr, align 4
  %arrayidx4 = getelementptr inbounds float, float* %9, i32 9
  %10 = load float, float* %arrayidx4, align 4
  %sub5 = fsub float %mul3, %10
  store float %sub5, float* %tc0, align 4
  %11 = load float, float* %ts0, align 4
  %12 = load float, float* %tc0, align 4
  %add = fadd float %11, %12
  store float %add, float* %tc1, align 4
  %13 = load float, float* %ts0, align 4
  %14 = load float, float* %tc0, align 4
  %sub6 = fsub float %13, %14
  store float %sub6, float* %tc2, align 4
  %15 = load float*, float** %inout.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %15, i32 15
  %16 = load float, float* %arrayidx7, align 4
  %17 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 0), align 16
  %mul8 = fmul float %16, %17
  %18 = load float*, float** %inout.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %18, i32 6
  %19 = load float, float* %arrayidx9, align 4
  %add10 = fadd float %mul8, %19
  store float %add10, float* %ts0, align 4
  %20 = load float*, float** %inout.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %20, i32 9
  %21 = load float, float* %arrayidx11, align 4
  %22 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 2), align 8
  %mul12 = fmul float %21, %22
  %23 = load float*, float** %inout.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %23, i32 0
  %24 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %mul12, %24
  store float %add14, float* %tc0, align 4
  %25 = load float, float* %ts0, align 4
  %26 = load float, float* %tc0, align 4
  %add15 = fadd float %25, %26
  store float %add15, float* %ts1, align 4
  %27 = load float, float* %ts0, align 4
  %fneg = fneg float %27
  %28 = load float, float* %tc0, align 4
  %add16 = fadd float %fneg, %28
  store float %add16, float* %ts2, align 4
  %29 = load float*, float** %inout.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %29, i32 3
  %30 = load float, float* %arrayidx17, align 4
  %31 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 1), align 4
  %mul18 = fmul float %30, %31
  %32 = load float*, float** %inout.addr, align 4
  %arrayidx19 = getelementptr inbounds float, float* %32, i32 12
  %33 = load float, float* %arrayidx19, align 4
  %sub20 = fsub float %mul18, %33
  %conv = fpext float %sub20 to double
  %mul21 = fmul double %conv, 0x3DB6C2786CB19C4F
  %conv22 = fptrunc double %mul21 to float
  store float %conv22, float* %tc0, align 4
  %34 = load float*, float** %inout.addr, align 4
  %arrayidx23 = getelementptr inbounds float, float* %34, i32 12
  %35 = load float, float* %arrayidx23, align 4
  %36 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 1), align 4
  %mul24 = fmul float %35, %36
  %37 = load float*, float** %inout.addr, align 4
  %arrayidx25 = getelementptr inbounds float, float* %37, i32 3
  %38 = load float, float* %arrayidx25, align 4
  %add26 = fadd float %mul24, %38
  %conv27 = fpext float %add26 to double
  %mul28 = fmul double %conv27, 0x3DB6C2786CB19C4F
  %conv29 = fptrunc double %mul28 to float
  store float %conv29, float* %ts0, align 4
  %39 = load float, float* %tc1, align 4
  %conv30 = fpext float %39 to double
  %mul31 = fmul double %conv30, 0x3DB4F934C242E570
  %40 = load float, float* %tc0, align 4
  %conv32 = fpext float %40 to double
  %add33 = fadd double %mul31, %conv32
  %conv34 = fptrunc double %add33 to float
  %41 = load float*, float** %inout.addr, align 4
  %arrayidx35 = getelementptr inbounds float, float* %41, i32 0
  store float %conv34, float* %arrayidx35, align 4
  %42 = load float, float* %ts1, align 4
  %fneg36 = fneg float %42
  %conv37 = fpext float %fneg36 to double
  %mul38 = fmul double %conv37, 0x3DB4F934C242E570
  %43 = load float, float* %ts0, align 4
  %conv39 = fpext float %43 to double
  %add40 = fadd double %mul38, %conv39
  %conv41 = fptrunc double %add40 to float
  %44 = load float*, float** %inout.addr, align 4
  %arrayidx42 = getelementptr inbounds float, float* %44, i32 15
  store float %conv41, float* %arrayidx42, align 4
  %45 = load float, float* %tc2, align 4
  %conv43 = fpext float %45 to double
  %mul44 = fmul double %conv43, 0x3FEBB67AE8584CAB
  %mul45 = fmul double %mul44, 0x3DB4F934C242E573
  %conv46 = fptrunc double %mul45 to float
  store float %conv46, float* %tc2, align 4
  %46 = load float, float* %ts1, align 4
  %conv47 = fpext float %46 to double
  %mul48 = fmul double %conv47, 5.000000e-01
  %mul49 = fmul double %mul48, 0x3DB4F934C242E573
  %47 = load float, float* %ts0, align 4
  %conv50 = fpext float %47 to double
  %add51 = fadd double %mul49, %conv50
  %conv52 = fptrunc double %add51 to float
  store float %conv52, float* %ts1, align 4
  %48 = load float, float* %tc2, align 4
  %49 = load float, float* %ts1, align 4
  %sub53 = fsub float %48, %49
  %50 = load float*, float** %inout.addr, align 4
  %arrayidx54 = getelementptr inbounds float, float* %50, i32 3
  store float %sub53, float* %arrayidx54, align 4
  %51 = load float, float* %tc2, align 4
  %52 = load float, float* %ts1, align 4
  %add55 = fadd float %51, %52
  %53 = load float*, float** %inout.addr, align 4
  %arrayidx56 = getelementptr inbounds float, float* %53, i32 6
  store float %add55, float* %arrayidx56, align 4
  %54 = load float, float* %tc1, align 4
  %conv57 = fpext float %54 to double
  %mul58 = fmul double %conv57, 5.000000e-01
  %mul59 = fmul double %mul58, 0x3DB4F934C242E573
  %55 = load float, float* %tc0, align 4
  %conv60 = fpext float %55 to double
  %sub61 = fsub double %mul59, %conv60
  %conv62 = fptrunc double %sub61 to float
  store float %conv62, float* %tc1, align 4
  %56 = load float, float* %ts2, align 4
  %conv63 = fpext float %56 to double
  %mul64 = fmul double %conv63, 0x3FEBB67AE8584CAB
  %mul65 = fmul double %mul64, 0x3DB4F934C242E573
  %conv66 = fptrunc double %mul65 to float
  store float %conv66, float* %ts2, align 4
  %57 = load float, float* %tc1, align 4
  %58 = load float, float* %ts2, align 4
  %add67 = fadd float %57, %58
  %59 = load float*, float** %inout.addr, align 4
  %arrayidx68 = getelementptr inbounds float, float* %59, i32 9
  store float %add67, float* %arrayidx68, align 4
  %60 = load float, float* %tc1, align 4
  %61 = load float, float* %ts2, align 4
  %sub69 = fsub float %60, %61
  %62 = load float*, float** %inout.addr, align 4
  %arrayidx70 = getelementptr inbounds float, float* %62, i32 12
  store float %sub69, float* %arrayidx70, align 4
  %63 = load float*, float** %inout.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %63, i32 1
  store float* %incdec.ptr, float** %inout.addr, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %64 = load i32, i32* %l, align 4
  %inc = add nsw i32 %64, 1
  store i32 %inc, i32* %l, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @mdct_long(float* %out, float* %in) #0 {
entry:
  %out.addr = alloca float*, align 4
  %in.addr = alloca float*, align 4
  %ct = alloca float, align 4
  %st = alloca float, align 4
  %tc1 = alloca float, align 4
  %tc2 = alloca float, align 4
  %tc3 = alloca float, align 4
  %tc4 = alloca float, align 4
  %ts5 = alloca float, align 4
  %ts6 = alloca float, align 4
  %ts7 = alloca float, align 4
  %ts8 = alloca float, align 4
  %ts1 = alloca float, align 4
  %ts2 = alloca float, align 4
  %ts3 = alloca float, align 4
  %ts4 = alloca float, align 4
  %tc5 = alloca float, align 4
  %tc6 = alloca float, align 4
  %tc7 = alloca float, align 4
  %tc8 = alloca float, align 4
  store float* %out, float** %out.addr, align 4
  store float* %in, float** %in.addr, align 4
  %0 = load float*, float** %in.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 17
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %in.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 9
  %3 = load float, float* %arrayidx1, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %tc1, align 4
  %4 = load float*, float** %in.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 15
  %5 = load float, float* %arrayidx2, align 4
  %6 = load float*, float** %in.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 11
  %7 = load float, float* %arrayidx3, align 4
  %sub4 = fsub float %5, %7
  store float %sub4, float* %tc3, align 4
  %8 = load float*, float** %in.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %8, i32 14
  %9 = load float, float* %arrayidx5, align 4
  %10 = load float*, float** %in.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %10, i32 12
  %11 = load float, float* %arrayidx6, align 4
  %sub7 = fsub float %9, %11
  store float %sub7, float* %tc4, align 4
  %12 = load float*, float** %in.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %12, i32 0
  %13 = load float, float* %arrayidx8, align 4
  %14 = load float*, float** %in.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %14, i32 8
  %15 = load float, float* %arrayidx9, align 4
  %add = fadd float %13, %15
  store float %add, float* %ts5, align 4
  %16 = load float*, float** %in.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %16, i32 1
  %17 = load float, float* %arrayidx10, align 4
  %18 = load float*, float** %in.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %18, i32 7
  %19 = load float, float* %arrayidx11, align 4
  %add12 = fadd float %17, %19
  store float %add12, float* %ts6, align 4
  %20 = load float*, float** %in.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %20, i32 2
  %21 = load float, float* %arrayidx13, align 4
  %22 = load float*, float** %in.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %22, i32 6
  %23 = load float, float* %arrayidx14, align 4
  %add15 = fadd float %21, %23
  store float %add15, float* %ts7, align 4
  %24 = load float*, float** %in.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %24, i32 3
  %25 = load float, float* %arrayidx16, align 4
  %26 = load float*, float** %in.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %26, i32 5
  %27 = load float, float* %arrayidx17, align 4
  %add18 = fadd float %25, %27
  store float %add18, float* %ts8, align 4
  %28 = load float, float* %ts5, align 4
  %29 = load float, float* %ts7, align 4
  %add19 = fadd float %28, %29
  %30 = load float, float* %ts8, align 4
  %sub20 = fsub float %add19, %30
  %31 = load float, float* %ts6, align 4
  %32 = load float*, float** %in.addr, align 4
  %arrayidx21 = getelementptr inbounds float, float* %32, i32 4
  %33 = load float, float* %arrayidx21, align 4
  %sub22 = fsub float %31, %33
  %sub23 = fsub float %sub20, %sub22
  %34 = load float*, float** %out.addr, align 4
  %arrayidx24 = getelementptr inbounds float, float* %34, i32 17
  store float %sub23, float* %arrayidx24, align 4
  %35 = load float, float* %ts5, align 4
  %36 = load float, float* %ts7, align 4
  %add25 = fadd float %35, %36
  %37 = load float, float* %ts8, align 4
  %sub26 = fsub float %add25, %37
  %38 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 19), align 4
  %mul = fmul float %sub26, %38
  %39 = load float, float* %ts6, align 4
  %40 = load float*, float** %in.addr, align 4
  %arrayidx27 = getelementptr inbounds float, float* %40, i32 4
  %41 = load float, float* %arrayidx27, align 4
  %sub28 = fsub float %39, %41
  %add29 = fadd float %mul, %sub28
  store float %add29, float* %st, align 4
  %42 = load float, float* %tc1, align 4
  %43 = load float, float* %tc3, align 4
  %sub30 = fsub float %42, %43
  %44 = load float, float* %tc4, align 4
  %sub31 = fsub float %sub30, %44
  %45 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 18), align 4
  %mul32 = fmul float %sub31, %45
  store float %mul32, float* %ct, align 4
  %46 = load float, float* %ct, align 4
  %47 = load float, float* %st, align 4
  %add33 = fadd float %46, %47
  %48 = load float*, float** %out.addr, align 4
  %arrayidx34 = getelementptr inbounds float, float* %48, i32 5
  store float %add33, float* %arrayidx34, align 4
  %49 = load float, float* %ct, align 4
  %50 = load float, float* %st, align 4
  %sub35 = fsub float %49, %50
  %51 = load float*, float** %out.addr, align 4
  %arrayidx36 = getelementptr inbounds float, float* %51, i32 6
  store float %sub35, float* %arrayidx36, align 4
  %52 = load float*, float** %in.addr, align 4
  %arrayidx37 = getelementptr inbounds float, float* %52, i32 16
  %53 = load float, float* %arrayidx37, align 4
  %54 = load float*, float** %in.addr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %54, i32 10
  %55 = load float, float* %arrayidx38, align 4
  %sub39 = fsub float %53, %55
  %56 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 18), align 4
  %mul40 = fmul float %sub39, %56
  store float %mul40, float* %tc2, align 4
  %57 = load float, float* %ts6, align 4
  %58 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 19), align 4
  %mul41 = fmul float %57, %58
  %59 = load float*, float** %in.addr, align 4
  %arrayidx42 = getelementptr inbounds float, float* %59, i32 4
  %60 = load float, float* %arrayidx42, align 4
  %add43 = fadd float %mul41, %60
  store float %add43, float* %ts6, align 4
  %61 = load float, float* %tc1, align 4
  %62 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul44 = fmul float %61, %62
  %63 = load float, float* %tc2, align 4
  %add45 = fadd float %mul44, %63
  %64 = load float, float* %tc3, align 4
  %65 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul46 = fmul float %64, %65
  %add47 = fadd float %add45, %mul46
  %66 = load float, float* %tc4, align 4
  %67 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul48 = fmul float %66, %67
  %add49 = fadd float %add47, %mul48
  store float %add49, float* %ct, align 4
  %68 = load float, float* %ts5, align 4
  %fneg = fneg float %68
  %69 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul50 = fmul float %fneg, %69
  %70 = load float, float* %ts6, align 4
  %add51 = fadd float %mul50, %70
  %71 = load float, float* %ts7, align 4
  %72 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul52 = fmul float %71, %72
  %sub53 = fsub float %add51, %mul52
  %73 = load float, float* %ts8, align 4
  %74 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul54 = fmul float %73, %74
  %add55 = fadd float %sub53, %mul54
  store float %add55, float* %st, align 4
  %75 = load float, float* %ct, align 4
  %76 = load float, float* %st, align 4
  %add56 = fadd float %75, %76
  %77 = load float*, float** %out.addr, align 4
  %arrayidx57 = getelementptr inbounds float, float* %77, i32 1
  store float %add56, float* %arrayidx57, align 4
  %78 = load float, float* %ct, align 4
  %79 = load float, float* %st, align 4
  %sub58 = fsub float %78, %79
  %80 = load float*, float** %out.addr, align 4
  %arrayidx59 = getelementptr inbounds float, float* %80, i32 2
  store float %sub58, float* %arrayidx59, align 4
  %81 = load float, float* %tc1, align 4
  %82 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul60 = fmul float %81, %82
  %83 = load float, float* %tc2, align 4
  %sub61 = fsub float %mul60, %83
  %84 = load float, float* %tc3, align 4
  %85 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul62 = fmul float %84, %85
  %sub63 = fsub float %sub61, %mul62
  %86 = load float, float* %tc4, align 4
  %87 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul64 = fmul float %86, %87
  %add65 = fadd float %sub63, %mul64
  store float %add65, float* %ct, align 4
  %88 = load float, float* %ts5, align 4
  %fneg66 = fneg float %88
  %89 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul67 = fmul float %fneg66, %89
  %90 = load float, float* %ts6, align 4
  %add68 = fadd float %mul67, %90
  %91 = load float, float* %ts7, align 4
  %92 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul69 = fmul float %91, %92
  %sub70 = fsub float %add68, %mul69
  %93 = load float, float* %ts8, align 4
  %94 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul71 = fmul float %93, %94
  %add72 = fadd float %sub70, %mul71
  store float %add72, float* %st, align 4
  %95 = load float, float* %ct, align 4
  %96 = load float, float* %st, align 4
  %add73 = fadd float %95, %96
  %97 = load float*, float** %out.addr, align 4
  %arrayidx74 = getelementptr inbounds float, float* %97, i32 9
  store float %add73, float* %arrayidx74, align 4
  %98 = load float, float* %ct, align 4
  %99 = load float, float* %st, align 4
  %sub75 = fsub float %98, %99
  %100 = load float*, float** %out.addr, align 4
  %arrayidx76 = getelementptr inbounds float, float* %100, i32 10
  store float %sub75, float* %arrayidx76, align 4
  %101 = load float, float* %tc1, align 4
  %102 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul77 = fmul float %101, %102
  %103 = load float, float* %tc2, align 4
  %sub78 = fsub float %mul77, %103
  %104 = load float, float* %tc3, align 4
  %105 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul79 = fmul float %104, %105
  %add80 = fadd float %sub78, %mul79
  %106 = load float, float* %tc4, align 4
  %107 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul81 = fmul float %106, %107
  %sub82 = fsub float %add80, %mul81
  store float %sub82, float* %ct, align 4
  %108 = load float, float* %ts5, align 4
  %109 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul83 = fmul float %108, %109
  %110 = load float, float* %ts6, align 4
  %sub84 = fsub float %mul83, %110
  %111 = load float, float* %ts7, align 4
  %112 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul85 = fmul float %111, %112
  %add86 = fadd float %sub84, %mul85
  %113 = load float, float* %ts8, align 4
  %114 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul87 = fmul float %113, %114
  %sub88 = fsub float %add86, %mul87
  store float %sub88, float* %st, align 4
  %115 = load float, float* %ct, align 4
  %116 = load float, float* %st, align 4
  %add89 = fadd float %115, %116
  %117 = load float*, float** %out.addr, align 4
  %arrayidx90 = getelementptr inbounds float, float* %117, i32 13
  store float %add89, float* %arrayidx90, align 4
  %118 = load float, float* %ct, align 4
  %119 = load float, float* %st, align 4
  %sub91 = fsub float %118, %119
  %120 = load float*, float** %out.addr, align 4
  %arrayidx92 = getelementptr inbounds float, float* %120, i32 14
  store float %sub91, float* %arrayidx92, align 4
  %121 = load float*, float** %in.addr, align 4
  %arrayidx93 = getelementptr inbounds float, float* %121, i32 8
  %122 = load float, float* %arrayidx93, align 4
  %123 = load float*, float** %in.addr, align 4
  %arrayidx94 = getelementptr inbounds float, float* %123, i32 0
  %124 = load float, float* %arrayidx94, align 4
  %sub95 = fsub float %122, %124
  store float %sub95, float* %ts1, align 4
  %125 = load float*, float** %in.addr, align 4
  %arrayidx96 = getelementptr inbounds float, float* %125, i32 6
  %126 = load float, float* %arrayidx96, align 4
  %127 = load float*, float** %in.addr, align 4
  %arrayidx97 = getelementptr inbounds float, float* %127, i32 2
  %128 = load float, float* %arrayidx97, align 4
  %sub98 = fsub float %126, %128
  store float %sub98, float* %ts3, align 4
  %129 = load float*, float** %in.addr, align 4
  %arrayidx99 = getelementptr inbounds float, float* %129, i32 5
  %130 = load float, float* %arrayidx99, align 4
  %131 = load float*, float** %in.addr, align 4
  %arrayidx100 = getelementptr inbounds float, float* %131, i32 3
  %132 = load float, float* %arrayidx100, align 4
  %sub101 = fsub float %130, %132
  store float %sub101, float* %ts4, align 4
  %133 = load float*, float** %in.addr, align 4
  %arrayidx102 = getelementptr inbounds float, float* %133, i32 17
  %134 = load float, float* %arrayidx102, align 4
  %135 = load float*, float** %in.addr, align 4
  %arrayidx103 = getelementptr inbounds float, float* %135, i32 9
  %136 = load float, float* %arrayidx103, align 4
  %add104 = fadd float %134, %136
  store float %add104, float* %tc5, align 4
  %137 = load float*, float** %in.addr, align 4
  %arrayidx105 = getelementptr inbounds float, float* %137, i32 16
  %138 = load float, float* %arrayidx105, align 4
  %139 = load float*, float** %in.addr, align 4
  %arrayidx106 = getelementptr inbounds float, float* %139, i32 10
  %140 = load float, float* %arrayidx106, align 4
  %add107 = fadd float %138, %140
  store float %add107, float* %tc6, align 4
  %141 = load float*, float** %in.addr, align 4
  %arrayidx108 = getelementptr inbounds float, float* %141, i32 15
  %142 = load float, float* %arrayidx108, align 4
  %143 = load float*, float** %in.addr, align 4
  %arrayidx109 = getelementptr inbounds float, float* %143, i32 11
  %144 = load float, float* %arrayidx109, align 4
  %add110 = fadd float %142, %144
  store float %add110, float* %tc7, align 4
  %145 = load float*, float** %in.addr, align 4
  %arrayidx111 = getelementptr inbounds float, float* %145, i32 14
  %146 = load float, float* %arrayidx111, align 4
  %147 = load float*, float** %in.addr, align 4
  %arrayidx112 = getelementptr inbounds float, float* %147, i32 12
  %148 = load float, float* %arrayidx112, align 4
  %add113 = fadd float %146, %148
  store float %add113, float* %tc8, align 4
  %149 = load float, float* %tc5, align 4
  %150 = load float, float* %tc7, align 4
  %add114 = fadd float %149, %150
  %151 = load float, float* %tc8, align 4
  %add115 = fadd float %add114, %151
  %152 = load float, float* %tc6, align 4
  %153 = load float*, float** %in.addr, align 4
  %arrayidx116 = getelementptr inbounds float, float* %153, i32 13
  %154 = load float, float* %arrayidx116, align 4
  %add117 = fadd float %152, %154
  %add118 = fadd float %add115, %add117
  %155 = load float*, float** %out.addr, align 4
  %arrayidx119 = getelementptr inbounds float, float* %155, i32 0
  store float %add118, float* %arrayidx119, align 4
  %156 = load float, float* %tc5, align 4
  %157 = load float, float* %tc7, align 4
  %add120 = fadd float %156, %157
  %158 = load float, float* %tc8, align 4
  %add121 = fadd float %add120, %158
  %159 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 19), align 4
  %mul122 = fmul float %add121, %159
  %160 = load float, float* %tc6, align 4
  %161 = load float*, float** %in.addr, align 4
  %arrayidx123 = getelementptr inbounds float, float* %161, i32 13
  %162 = load float, float* %arrayidx123, align 4
  %add124 = fadd float %160, %162
  %sub125 = fsub float %mul122, %add124
  store float %sub125, float* %ct, align 4
  %163 = load float, float* %ts1, align 4
  %164 = load float, float* %ts3, align 4
  %sub126 = fsub float %163, %164
  %165 = load float, float* %ts4, align 4
  %add127 = fadd float %sub126, %165
  %166 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 18), align 4
  %mul128 = fmul float %add127, %166
  store float %mul128, float* %st, align 4
  %167 = load float, float* %ct, align 4
  %168 = load float, float* %st, align 4
  %add129 = fadd float %167, %168
  %169 = load float*, float** %out.addr, align 4
  %arrayidx130 = getelementptr inbounds float, float* %169, i32 11
  store float %add129, float* %arrayidx130, align 4
  %170 = load float, float* %ct, align 4
  %171 = load float, float* %st, align 4
  %sub131 = fsub float %170, %171
  %172 = load float*, float** %out.addr, align 4
  %arrayidx132 = getelementptr inbounds float, float* %172, i32 12
  store float %sub131, float* %arrayidx132, align 4
  %173 = load float*, float** %in.addr, align 4
  %arrayidx133 = getelementptr inbounds float, float* %173, i32 7
  %174 = load float, float* %arrayidx133, align 4
  %175 = load float*, float** %in.addr, align 4
  %arrayidx134 = getelementptr inbounds float, float* %175, i32 1
  %176 = load float, float* %arrayidx134, align 4
  %sub135 = fsub float %174, %176
  %177 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 18), align 4
  %mul136 = fmul float %sub135, %177
  store float %mul136, float* %ts2, align 4
  %178 = load float*, float** %in.addr, align 4
  %arrayidx137 = getelementptr inbounds float, float* %178, i32 13
  %179 = load float, float* %arrayidx137, align 4
  %180 = load float, float* %tc6, align 4
  %181 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 19), align 4
  %mul138 = fmul float %180, %181
  %sub139 = fsub float %179, %mul138
  store float %sub139, float* %tc6, align 4
  %182 = load float, float* %tc5, align 4
  %183 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul140 = fmul float %182, %183
  %184 = load float, float* %tc6, align 4
  %sub141 = fsub float %mul140, %184
  %185 = load float, float* %tc7, align 4
  %186 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul142 = fmul float %185, %186
  %add143 = fadd float %sub141, %mul142
  %187 = load float, float* %tc8, align 4
  %188 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul144 = fmul float %187, %188
  %add145 = fadd float %add143, %mul144
  store float %add145, float* %ct, align 4
  %189 = load float, float* %ts1, align 4
  %190 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul146 = fmul float %189, %190
  %191 = load float, float* %ts2, align 4
  %add147 = fadd float %mul146, %191
  %192 = load float, float* %ts3, align 4
  %193 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul148 = fmul float %192, %193
  %add149 = fadd float %add147, %mul148
  %194 = load float, float* %ts4, align 4
  %195 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul150 = fmul float %194, %195
  %add151 = fadd float %add149, %mul150
  store float %add151, float* %st, align 4
  %196 = load float, float* %ct, align 4
  %197 = load float, float* %st, align 4
  %add152 = fadd float %196, %197
  %198 = load float*, float** %out.addr, align 4
  %arrayidx153 = getelementptr inbounds float, float* %198, i32 3
  store float %add152, float* %arrayidx153, align 4
  %199 = load float, float* %ct, align 4
  %200 = load float, float* %st, align 4
  %sub154 = fsub float %199, %200
  %201 = load float*, float** %out.addr, align 4
  %arrayidx155 = getelementptr inbounds float, float* %201, i32 4
  store float %sub154, float* %arrayidx155, align 4
  %202 = load float, float* %tc5, align 4
  %fneg156 = fneg float %202
  %203 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul157 = fmul float %fneg156, %203
  %204 = load float, float* %tc6, align 4
  %add158 = fadd float %mul157, %204
  %205 = load float, float* %tc7, align 4
  %206 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul159 = fmul float %205, %206
  %sub160 = fsub float %add158, %mul159
  %207 = load float, float* %tc8, align 4
  %208 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul161 = fmul float %207, %208
  %sub162 = fsub float %sub160, %mul161
  store float %sub162, float* %ct, align 4
  %209 = load float, float* %ts1, align 4
  %210 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul163 = fmul float %209, %210
  %211 = load float, float* %ts2, align 4
  %add164 = fadd float %mul163, %211
  %212 = load float, float* %ts3, align 4
  %213 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul165 = fmul float %212, %213
  %sub166 = fsub float %add164, %mul165
  %214 = load float, float* %ts4, align 4
  %215 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul167 = fmul float %214, %215
  %sub168 = fsub float %sub166, %mul167
  store float %sub168, float* %st, align 4
  %216 = load float, float* %ct, align 4
  %217 = load float, float* %st, align 4
  %add169 = fadd float %216, %217
  %218 = load float*, float** %out.addr, align 4
  %arrayidx170 = getelementptr inbounds float, float* %218, i32 7
  store float %add169, float* %arrayidx170, align 4
  %219 = load float, float* %ct, align 4
  %220 = load float, float* %st, align 4
  %sub171 = fsub float %219, %220
  %221 = load float*, float** %out.addr, align 4
  %arrayidx172 = getelementptr inbounds float, float* %221, i32 8
  store float %sub171, float* %arrayidx172, align 4
  %222 = load float, float* %tc5, align 4
  %fneg173 = fneg float %222
  %223 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 16), align 4
  %mul174 = fmul float %fneg173, %223
  %224 = load float, float* %tc6, align 4
  %add175 = fadd float %mul174, %224
  %225 = load float, float* %tc7, align 4
  %226 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 17), align 4
  %mul176 = fmul float %225, %226
  %sub177 = fsub float %add175, %mul176
  %227 = load float, float* %tc8, align 4
  %228 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 15), align 4
  %mul178 = fmul float %227, %228
  %sub179 = fsub float %sub177, %mul178
  store float %sub179, float* %ct, align 4
  %229 = load float, float* %ts1, align 4
  %230 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 12), align 4
  %mul180 = fmul float %229, %230
  %231 = load float, float* %ts2, align 4
  %sub181 = fsub float %mul180, %231
  %232 = load float, float* %ts3, align 4
  %233 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 13), align 4
  %mul182 = fmul float %232, %233
  %add183 = fadd float %sub181, %mul182
  %234 = load float, float* %ts4, align 4
  %235 = load float, float* getelementptr inbounds ([4 x [36 x float]], [4 x [36 x float]]* @win, i32 0, i32 2, i32 14), align 4
  %mul184 = fmul float %234, %235
  %sub185 = fsub float %add183, %mul184
  store float %sub185, float* %st, align 4
  %236 = load float, float* %ct, align 4
  %237 = load float, float* %st, align 4
  %add186 = fadd float %236, %237
  %238 = load float*, float** %out.addr, align 4
  %arrayidx187 = getelementptr inbounds float, float* %238, i32 15
  store float %add186, float* %arrayidx187, align 4
  %239 = load float, float* %ct, align 4
  %240 = load float, float* %st, align 4
  %sub188 = fsub float %239, %240
  %241 = load float*, float** %out.addr, align 4
  %arrayidx189 = getelementptr inbounds float, float* %241, i32 16
  store float %sub188, float* %arrayidx189, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
