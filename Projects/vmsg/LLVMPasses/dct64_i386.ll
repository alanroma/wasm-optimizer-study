; ModuleID = 'dct64_i386.c'
source_filename = "dct64_i386.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

@pnts = external global [5 x float*], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @dct64(float* %a, float* %b, float* %c) #0 {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %c.addr = alloca float*, align 4
  %bufs = alloca [64 x float], align 16
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  store float* %c, float** %c.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float*, float** %b.addr, align 4
  %arraydecay = getelementptr inbounds [64 x float], [64 x float]* %bufs, i32 0, i32 0
  %arraydecay1 = getelementptr inbounds [64 x float], [64 x float]* %bufs, i32 0, i32 0
  %add.ptr = getelementptr inbounds float, float* %arraydecay1, i32 32
  %2 = load float*, float** %c.addr, align 4
  call void @dct64_1(float* %0, float* %1, float* %arraydecay, float* %add.ptr, float* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @dct64_1(float* %out0, float* %out1, float* %b1, float* %b2, float* %samples) #0 {
entry:
  %out0.addr = alloca float*, align 4
  %out1.addr = alloca float*, align 4
  %b1.addr = alloca float*, align 4
  %b2.addr = alloca float*, align 4
  %samples.addr = alloca float*, align 4
  %costab = alloca float*, align 4
  %costab157 = alloca float*, align 4
  %costab318 = alloca float*, align 4
  %cos0 = alloca float, align 4
  %cos1 = alloca float, align 4
  %cos0625 = alloca float, align 4
  store float* %out0, float** %out0.addr, align 4
  store float* %out1, float** %out1.addr, align 4
  store float* %b1, float** %b1.addr, align 4
  store float* %b2, float** %b2.addr, align 4
  store float* %samples, float** %samples.addr, align 4
  %0 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 0), align 16
  store float* %0, float** %costab, align 4
  %1 = load float*, float** %samples.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load float*, float** %samples.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %3, i32 31
  %4 = load float, float* %arrayidx1, align 4
  %add = fadd float %2, %4
  %5 = load float*, float** %b1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %5, i32 0
  store float %add, float* %arrayidx2, align 4
  %6 = load float*, float** %samples.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load float*, float** %samples.addr, align 4
  %arrayidx4 = getelementptr inbounds float, float* %8, i32 31
  %9 = load float, float* %arrayidx4, align 4
  %sub = fsub float %7, %9
  %10 = load float*, float** %costab, align 4
  %arrayidx5 = getelementptr inbounds float, float* %10, i32 0
  %11 = load float, float* %arrayidx5, align 4
  %mul = fmul float %sub, %11
  %12 = load float*, float** %b1.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %12, i32 31
  store float %mul, float* %arrayidx6, align 4
  %13 = load float*, float** %samples.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %13, i32 1
  %14 = load float, float* %arrayidx7, align 4
  %15 = load float*, float** %samples.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %15, i32 30
  %16 = load float, float* %arrayidx8, align 4
  %add9 = fadd float %14, %16
  %17 = load float*, float** %b1.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %17, i32 1
  store float %add9, float* %arrayidx10, align 4
  %18 = load float*, float** %samples.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %18, i32 1
  %19 = load float, float* %arrayidx11, align 4
  %20 = load float*, float** %samples.addr, align 4
  %arrayidx12 = getelementptr inbounds float, float* %20, i32 30
  %21 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %19, %21
  %22 = load float*, float** %costab, align 4
  %arrayidx14 = getelementptr inbounds float, float* %22, i32 1
  %23 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %sub13, %23
  %24 = load float*, float** %b1.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %24, i32 30
  store float %mul15, float* %arrayidx16, align 4
  %25 = load float*, float** %samples.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %25, i32 2
  %26 = load float, float* %arrayidx17, align 4
  %27 = load float*, float** %samples.addr, align 4
  %arrayidx18 = getelementptr inbounds float, float* %27, i32 29
  %28 = load float, float* %arrayidx18, align 4
  %add19 = fadd float %26, %28
  %29 = load float*, float** %b1.addr, align 4
  %arrayidx20 = getelementptr inbounds float, float* %29, i32 2
  store float %add19, float* %arrayidx20, align 4
  %30 = load float*, float** %samples.addr, align 4
  %arrayidx21 = getelementptr inbounds float, float* %30, i32 2
  %31 = load float, float* %arrayidx21, align 4
  %32 = load float*, float** %samples.addr, align 4
  %arrayidx22 = getelementptr inbounds float, float* %32, i32 29
  %33 = load float, float* %arrayidx22, align 4
  %sub23 = fsub float %31, %33
  %34 = load float*, float** %costab, align 4
  %arrayidx24 = getelementptr inbounds float, float* %34, i32 2
  %35 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %sub23, %35
  %36 = load float*, float** %b1.addr, align 4
  %arrayidx26 = getelementptr inbounds float, float* %36, i32 29
  store float %mul25, float* %arrayidx26, align 4
  %37 = load float*, float** %samples.addr, align 4
  %arrayidx27 = getelementptr inbounds float, float* %37, i32 3
  %38 = load float, float* %arrayidx27, align 4
  %39 = load float*, float** %samples.addr, align 4
  %arrayidx28 = getelementptr inbounds float, float* %39, i32 28
  %40 = load float, float* %arrayidx28, align 4
  %add29 = fadd float %38, %40
  %41 = load float*, float** %b1.addr, align 4
  %arrayidx30 = getelementptr inbounds float, float* %41, i32 3
  store float %add29, float* %arrayidx30, align 4
  %42 = load float*, float** %samples.addr, align 4
  %arrayidx31 = getelementptr inbounds float, float* %42, i32 3
  %43 = load float, float* %arrayidx31, align 4
  %44 = load float*, float** %samples.addr, align 4
  %arrayidx32 = getelementptr inbounds float, float* %44, i32 28
  %45 = load float, float* %arrayidx32, align 4
  %sub33 = fsub float %43, %45
  %46 = load float*, float** %costab, align 4
  %arrayidx34 = getelementptr inbounds float, float* %46, i32 3
  %47 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %sub33, %47
  %48 = load float*, float** %b1.addr, align 4
  %arrayidx36 = getelementptr inbounds float, float* %48, i32 28
  store float %mul35, float* %arrayidx36, align 4
  %49 = load float*, float** %samples.addr, align 4
  %arrayidx37 = getelementptr inbounds float, float* %49, i32 4
  %50 = load float, float* %arrayidx37, align 4
  %51 = load float*, float** %samples.addr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %51, i32 27
  %52 = load float, float* %arrayidx38, align 4
  %add39 = fadd float %50, %52
  %53 = load float*, float** %b1.addr, align 4
  %arrayidx40 = getelementptr inbounds float, float* %53, i32 4
  store float %add39, float* %arrayidx40, align 4
  %54 = load float*, float** %samples.addr, align 4
  %arrayidx41 = getelementptr inbounds float, float* %54, i32 4
  %55 = load float, float* %arrayidx41, align 4
  %56 = load float*, float** %samples.addr, align 4
  %arrayidx42 = getelementptr inbounds float, float* %56, i32 27
  %57 = load float, float* %arrayidx42, align 4
  %sub43 = fsub float %55, %57
  %58 = load float*, float** %costab, align 4
  %arrayidx44 = getelementptr inbounds float, float* %58, i32 4
  %59 = load float, float* %arrayidx44, align 4
  %mul45 = fmul float %sub43, %59
  %60 = load float*, float** %b1.addr, align 4
  %arrayidx46 = getelementptr inbounds float, float* %60, i32 27
  store float %mul45, float* %arrayidx46, align 4
  %61 = load float*, float** %samples.addr, align 4
  %arrayidx47 = getelementptr inbounds float, float* %61, i32 5
  %62 = load float, float* %arrayidx47, align 4
  %63 = load float*, float** %samples.addr, align 4
  %arrayidx48 = getelementptr inbounds float, float* %63, i32 26
  %64 = load float, float* %arrayidx48, align 4
  %add49 = fadd float %62, %64
  %65 = load float*, float** %b1.addr, align 4
  %arrayidx50 = getelementptr inbounds float, float* %65, i32 5
  store float %add49, float* %arrayidx50, align 4
  %66 = load float*, float** %samples.addr, align 4
  %arrayidx51 = getelementptr inbounds float, float* %66, i32 5
  %67 = load float, float* %arrayidx51, align 4
  %68 = load float*, float** %samples.addr, align 4
  %arrayidx52 = getelementptr inbounds float, float* %68, i32 26
  %69 = load float, float* %arrayidx52, align 4
  %sub53 = fsub float %67, %69
  %70 = load float*, float** %costab, align 4
  %arrayidx54 = getelementptr inbounds float, float* %70, i32 5
  %71 = load float, float* %arrayidx54, align 4
  %mul55 = fmul float %sub53, %71
  %72 = load float*, float** %b1.addr, align 4
  %arrayidx56 = getelementptr inbounds float, float* %72, i32 26
  store float %mul55, float* %arrayidx56, align 4
  %73 = load float*, float** %samples.addr, align 4
  %arrayidx57 = getelementptr inbounds float, float* %73, i32 6
  %74 = load float, float* %arrayidx57, align 4
  %75 = load float*, float** %samples.addr, align 4
  %arrayidx58 = getelementptr inbounds float, float* %75, i32 25
  %76 = load float, float* %arrayidx58, align 4
  %add59 = fadd float %74, %76
  %77 = load float*, float** %b1.addr, align 4
  %arrayidx60 = getelementptr inbounds float, float* %77, i32 6
  store float %add59, float* %arrayidx60, align 4
  %78 = load float*, float** %samples.addr, align 4
  %arrayidx61 = getelementptr inbounds float, float* %78, i32 6
  %79 = load float, float* %arrayidx61, align 4
  %80 = load float*, float** %samples.addr, align 4
  %arrayidx62 = getelementptr inbounds float, float* %80, i32 25
  %81 = load float, float* %arrayidx62, align 4
  %sub63 = fsub float %79, %81
  %82 = load float*, float** %costab, align 4
  %arrayidx64 = getelementptr inbounds float, float* %82, i32 6
  %83 = load float, float* %arrayidx64, align 4
  %mul65 = fmul float %sub63, %83
  %84 = load float*, float** %b1.addr, align 4
  %arrayidx66 = getelementptr inbounds float, float* %84, i32 25
  store float %mul65, float* %arrayidx66, align 4
  %85 = load float*, float** %samples.addr, align 4
  %arrayidx67 = getelementptr inbounds float, float* %85, i32 7
  %86 = load float, float* %arrayidx67, align 4
  %87 = load float*, float** %samples.addr, align 4
  %arrayidx68 = getelementptr inbounds float, float* %87, i32 24
  %88 = load float, float* %arrayidx68, align 4
  %add69 = fadd float %86, %88
  %89 = load float*, float** %b1.addr, align 4
  %arrayidx70 = getelementptr inbounds float, float* %89, i32 7
  store float %add69, float* %arrayidx70, align 4
  %90 = load float*, float** %samples.addr, align 4
  %arrayidx71 = getelementptr inbounds float, float* %90, i32 7
  %91 = load float, float* %arrayidx71, align 4
  %92 = load float*, float** %samples.addr, align 4
  %arrayidx72 = getelementptr inbounds float, float* %92, i32 24
  %93 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %91, %93
  %94 = load float*, float** %costab, align 4
  %arrayidx74 = getelementptr inbounds float, float* %94, i32 7
  %95 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %sub73, %95
  %96 = load float*, float** %b1.addr, align 4
  %arrayidx76 = getelementptr inbounds float, float* %96, i32 24
  store float %mul75, float* %arrayidx76, align 4
  %97 = load float*, float** %samples.addr, align 4
  %arrayidx77 = getelementptr inbounds float, float* %97, i32 8
  %98 = load float, float* %arrayidx77, align 4
  %99 = load float*, float** %samples.addr, align 4
  %arrayidx78 = getelementptr inbounds float, float* %99, i32 23
  %100 = load float, float* %arrayidx78, align 4
  %add79 = fadd float %98, %100
  %101 = load float*, float** %b1.addr, align 4
  %arrayidx80 = getelementptr inbounds float, float* %101, i32 8
  store float %add79, float* %arrayidx80, align 4
  %102 = load float*, float** %samples.addr, align 4
  %arrayidx81 = getelementptr inbounds float, float* %102, i32 8
  %103 = load float, float* %arrayidx81, align 4
  %104 = load float*, float** %samples.addr, align 4
  %arrayidx82 = getelementptr inbounds float, float* %104, i32 23
  %105 = load float, float* %arrayidx82, align 4
  %sub83 = fsub float %103, %105
  %106 = load float*, float** %costab, align 4
  %arrayidx84 = getelementptr inbounds float, float* %106, i32 8
  %107 = load float, float* %arrayidx84, align 4
  %mul85 = fmul float %sub83, %107
  %108 = load float*, float** %b1.addr, align 4
  %arrayidx86 = getelementptr inbounds float, float* %108, i32 23
  store float %mul85, float* %arrayidx86, align 4
  %109 = load float*, float** %samples.addr, align 4
  %arrayidx87 = getelementptr inbounds float, float* %109, i32 9
  %110 = load float, float* %arrayidx87, align 4
  %111 = load float*, float** %samples.addr, align 4
  %arrayidx88 = getelementptr inbounds float, float* %111, i32 22
  %112 = load float, float* %arrayidx88, align 4
  %add89 = fadd float %110, %112
  %113 = load float*, float** %b1.addr, align 4
  %arrayidx90 = getelementptr inbounds float, float* %113, i32 9
  store float %add89, float* %arrayidx90, align 4
  %114 = load float*, float** %samples.addr, align 4
  %arrayidx91 = getelementptr inbounds float, float* %114, i32 9
  %115 = load float, float* %arrayidx91, align 4
  %116 = load float*, float** %samples.addr, align 4
  %arrayidx92 = getelementptr inbounds float, float* %116, i32 22
  %117 = load float, float* %arrayidx92, align 4
  %sub93 = fsub float %115, %117
  %118 = load float*, float** %costab, align 4
  %arrayidx94 = getelementptr inbounds float, float* %118, i32 9
  %119 = load float, float* %arrayidx94, align 4
  %mul95 = fmul float %sub93, %119
  %120 = load float*, float** %b1.addr, align 4
  %arrayidx96 = getelementptr inbounds float, float* %120, i32 22
  store float %mul95, float* %arrayidx96, align 4
  %121 = load float*, float** %samples.addr, align 4
  %arrayidx97 = getelementptr inbounds float, float* %121, i32 10
  %122 = load float, float* %arrayidx97, align 4
  %123 = load float*, float** %samples.addr, align 4
  %arrayidx98 = getelementptr inbounds float, float* %123, i32 21
  %124 = load float, float* %arrayidx98, align 4
  %add99 = fadd float %122, %124
  %125 = load float*, float** %b1.addr, align 4
  %arrayidx100 = getelementptr inbounds float, float* %125, i32 10
  store float %add99, float* %arrayidx100, align 4
  %126 = load float*, float** %samples.addr, align 4
  %arrayidx101 = getelementptr inbounds float, float* %126, i32 10
  %127 = load float, float* %arrayidx101, align 4
  %128 = load float*, float** %samples.addr, align 4
  %arrayidx102 = getelementptr inbounds float, float* %128, i32 21
  %129 = load float, float* %arrayidx102, align 4
  %sub103 = fsub float %127, %129
  %130 = load float*, float** %costab, align 4
  %arrayidx104 = getelementptr inbounds float, float* %130, i32 10
  %131 = load float, float* %arrayidx104, align 4
  %mul105 = fmul float %sub103, %131
  %132 = load float*, float** %b1.addr, align 4
  %arrayidx106 = getelementptr inbounds float, float* %132, i32 21
  store float %mul105, float* %arrayidx106, align 4
  %133 = load float*, float** %samples.addr, align 4
  %arrayidx107 = getelementptr inbounds float, float* %133, i32 11
  %134 = load float, float* %arrayidx107, align 4
  %135 = load float*, float** %samples.addr, align 4
  %arrayidx108 = getelementptr inbounds float, float* %135, i32 20
  %136 = load float, float* %arrayidx108, align 4
  %add109 = fadd float %134, %136
  %137 = load float*, float** %b1.addr, align 4
  %arrayidx110 = getelementptr inbounds float, float* %137, i32 11
  store float %add109, float* %arrayidx110, align 4
  %138 = load float*, float** %samples.addr, align 4
  %arrayidx111 = getelementptr inbounds float, float* %138, i32 11
  %139 = load float, float* %arrayidx111, align 4
  %140 = load float*, float** %samples.addr, align 4
  %arrayidx112 = getelementptr inbounds float, float* %140, i32 20
  %141 = load float, float* %arrayidx112, align 4
  %sub113 = fsub float %139, %141
  %142 = load float*, float** %costab, align 4
  %arrayidx114 = getelementptr inbounds float, float* %142, i32 11
  %143 = load float, float* %arrayidx114, align 4
  %mul115 = fmul float %sub113, %143
  %144 = load float*, float** %b1.addr, align 4
  %arrayidx116 = getelementptr inbounds float, float* %144, i32 20
  store float %mul115, float* %arrayidx116, align 4
  %145 = load float*, float** %samples.addr, align 4
  %arrayidx117 = getelementptr inbounds float, float* %145, i32 12
  %146 = load float, float* %arrayidx117, align 4
  %147 = load float*, float** %samples.addr, align 4
  %arrayidx118 = getelementptr inbounds float, float* %147, i32 19
  %148 = load float, float* %arrayidx118, align 4
  %add119 = fadd float %146, %148
  %149 = load float*, float** %b1.addr, align 4
  %arrayidx120 = getelementptr inbounds float, float* %149, i32 12
  store float %add119, float* %arrayidx120, align 4
  %150 = load float*, float** %samples.addr, align 4
  %arrayidx121 = getelementptr inbounds float, float* %150, i32 12
  %151 = load float, float* %arrayidx121, align 4
  %152 = load float*, float** %samples.addr, align 4
  %arrayidx122 = getelementptr inbounds float, float* %152, i32 19
  %153 = load float, float* %arrayidx122, align 4
  %sub123 = fsub float %151, %153
  %154 = load float*, float** %costab, align 4
  %arrayidx124 = getelementptr inbounds float, float* %154, i32 12
  %155 = load float, float* %arrayidx124, align 4
  %mul125 = fmul float %sub123, %155
  %156 = load float*, float** %b1.addr, align 4
  %arrayidx126 = getelementptr inbounds float, float* %156, i32 19
  store float %mul125, float* %arrayidx126, align 4
  %157 = load float*, float** %samples.addr, align 4
  %arrayidx127 = getelementptr inbounds float, float* %157, i32 13
  %158 = load float, float* %arrayidx127, align 4
  %159 = load float*, float** %samples.addr, align 4
  %arrayidx128 = getelementptr inbounds float, float* %159, i32 18
  %160 = load float, float* %arrayidx128, align 4
  %add129 = fadd float %158, %160
  %161 = load float*, float** %b1.addr, align 4
  %arrayidx130 = getelementptr inbounds float, float* %161, i32 13
  store float %add129, float* %arrayidx130, align 4
  %162 = load float*, float** %samples.addr, align 4
  %arrayidx131 = getelementptr inbounds float, float* %162, i32 13
  %163 = load float, float* %arrayidx131, align 4
  %164 = load float*, float** %samples.addr, align 4
  %arrayidx132 = getelementptr inbounds float, float* %164, i32 18
  %165 = load float, float* %arrayidx132, align 4
  %sub133 = fsub float %163, %165
  %166 = load float*, float** %costab, align 4
  %arrayidx134 = getelementptr inbounds float, float* %166, i32 13
  %167 = load float, float* %arrayidx134, align 4
  %mul135 = fmul float %sub133, %167
  %168 = load float*, float** %b1.addr, align 4
  %arrayidx136 = getelementptr inbounds float, float* %168, i32 18
  store float %mul135, float* %arrayidx136, align 4
  %169 = load float*, float** %samples.addr, align 4
  %arrayidx137 = getelementptr inbounds float, float* %169, i32 14
  %170 = load float, float* %arrayidx137, align 4
  %171 = load float*, float** %samples.addr, align 4
  %arrayidx138 = getelementptr inbounds float, float* %171, i32 17
  %172 = load float, float* %arrayidx138, align 4
  %add139 = fadd float %170, %172
  %173 = load float*, float** %b1.addr, align 4
  %arrayidx140 = getelementptr inbounds float, float* %173, i32 14
  store float %add139, float* %arrayidx140, align 4
  %174 = load float*, float** %samples.addr, align 4
  %arrayidx141 = getelementptr inbounds float, float* %174, i32 14
  %175 = load float, float* %arrayidx141, align 4
  %176 = load float*, float** %samples.addr, align 4
  %arrayidx142 = getelementptr inbounds float, float* %176, i32 17
  %177 = load float, float* %arrayidx142, align 4
  %sub143 = fsub float %175, %177
  %178 = load float*, float** %costab, align 4
  %arrayidx144 = getelementptr inbounds float, float* %178, i32 14
  %179 = load float, float* %arrayidx144, align 4
  %mul145 = fmul float %sub143, %179
  %180 = load float*, float** %b1.addr, align 4
  %arrayidx146 = getelementptr inbounds float, float* %180, i32 17
  store float %mul145, float* %arrayidx146, align 4
  %181 = load float*, float** %samples.addr, align 4
  %arrayidx147 = getelementptr inbounds float, float* %181, i32 15
  %182 = load float, float* %arrayidx147, align 4
  %183 = load float*, float** %samples.addr, align 4
  %arrayidx148 = getelementptr inbounds float, float* %183, i32 16
  %184 = load float, float* %arrayidx148, align 4
  %add149 = fadd float %182, %184
  %185 = load float*, float** %b1.addr, align 4
  %arrayidx150 = getelementptr inbounds float, float* %185, i32 15
  store float %add149, float* %arrayidx150, align 4
  %186 = load float*, float** %samples.addr, align 4
  %arrayidx151 = getelementptr inbounds float, float* %186, i32 15
  %187 = load float, float* %arrayidx151, align 4
  %188 = load float*, float** %samples.addr, align 4
  %arrayidx152 = getelementptr inbounds float, float* %188, i32 16
  %189 = load float, float* %arrayidx152, align 4
  %sub153 = fsub float %187, %189
  %190 = load float*, float** %costab, align 4
  %arrayidx154 = getelementptr inbounds float, float* %190, i32 15
  %191 = load float, float* %arrayidx154, align 4
  %mul155 = fmul float %sub153, %191
  %192 = load float*, float** %b1.addr, align 4
  %arrayidx156 = getelementptr inbounds float, float* %192, i32 16
  store float %mul155, float* %arrayidx156, align 4
  %193 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 1), align 4
  store float* %193, float** %costab157, align 4
  %194 = load float*, float** %b1.addr, align 4
  %arrayidx158 = getelementptr inbounds float, float* %194, i32 0
  %195 = load float, float* %arrayidx158, align 4
  %196 = load float*, float** %b1.addr, align 4
  %arrayidx159 = getelementptr inbounds float, float* %196, i32 15
  %197 = load float, float* %arrayidx159, align 4
  %add160 = fadd float %195, %197
  %198 = load float*, float** %b2.addr, align 4
  %arrayidx161 = getelementptr inbounds float, float* %198, i32 0
  store float %add160, float* %arrayidx161, align 4
  %199 = load float*, float** %b1.addr, align 4
  %arrayidx162 = getelementptr inbounds float, float* %199, i32 0
  %200 = load float, float* %arrayidx162, align 4
  %201 = load float*, float** %b1.addr, align 4
  %arrayidx163 = getelementptr inbounds float, float* %201, i32 15
  %202 = load float, float* %arrayidx163, align 4
  %sub164 = fsub float %200, %202
  %203 = load float*, float** %costab157, align 4
  %arrayidx165 = getelementptr inbounds float, float* %203, i32 0
  %204 = load float, float* %arrayidx165, align 4
  %mul166 = fmul float %sub164, %204
  %205 = load float*, float** %b2.addr, align 4
  %arrayidx167 = getelementptr inbounds float, float* %205, i32 15
  store float %mul166, float* %arrayidx167, align 4
  %206 = load float*, float** %b1.addr, align 4
  %arrayidx168 = getelementptr inbounds float, float* %206, i32 1
  %207 = load float, float* %arrayidx168, align 4
  %208 = load float*, float** %b1.addr, align 4
  %arrayidx169 = getelementptr inbounds float, float* %208, i32 14
  %209 = load float, float* %arrayidx169, align 4
  %add170 = fadd float %207, %209
  %210 = load float*, float** %b2.addr, align 4
  %arrayidx171 = getelementptr inbounds float, float* %210, i32 1
  store float %add170, float* %arrayidx171, align 4
  %211 = load float*, float** %b1.addr, align 4
  %arrayidx172 = getelementptr inbounds float, float* %211, i32 1
  %212 = load float, float* %arrayidx172, align 4
  %213 = load float*, float** %b1.addr, align 4
  %arrayidx173 = getelementptr inbounds float, float* %213, i32 14
  %214 = load float, float* %arrayidx173, align 4
  %sub174 = fsub float %212, %214
  %215 = load float*, float** %costab157, align 4
  %arrayidx175 = getelementptr inbounds float, float* %215, i32 1
  %216 = load float, float* %arrayidx175, align 4
  %mul176 = fmul float %sub174, %216
  %217 = load float*, float** %b2.addr, align 4
  %arrayidx177 = getelementptr inbounds float, float* %217, i32 14
  store float %mul176, float* %arrayidx177, align 4
  %218 = load float*, float** %b1.addr, align 4
  %arrayidx178 = getelementptr inbounds float, float* %218, i32 2
  %219 = load float, float* %arrayidx178, align 4
  %220 = load float*, float** %b1.addr, align 4
  %arrayidx179 = getelementptr inbounds float, float* %220, i32 13
  %221 = load float, float* %arrayidx179, align 4
  %add180 = fadd float %219, %221
  %222 = load float*, float** %b2.addr, align 4
  %arrayidx181 = getelementptr inbounds float, float* %222, i32 2
  store float %add180, float* %arrayidx181, align 4
  %223 = load float*, float** %b1.addr, align 4
  %arrayidx182 = getelementptr inbounds float, float* %223, i32 2
  %224 = load float, float* %arrayidx182, align 4
  %225 = load float*, float** %b1.addr, align 4
  %arrayidx183 = getelementptr inbounds float, float* %225, i32 13
  %226 = load float, float* %arrayidx183, align 4
  %sub184 = fsub float %224, %226
  %227 = load float*, float** %costab157, align 4
  %arrayidx185 = getelementptr inbounds float, float* %227, i32 2
  %228 = load float, float* %arrayidx185, align 4
  %mul186 = fmul float %sub184, %228
  %229 = load float*, float** %b2.addr, align 4
  %arrayidx187 = getelementptr inbounds float, float* %229, i32 13
  store float %mul186, float* %arrayidx187, align 4
  %230 = load float*, float** %b1.addr, align 4
  %arrayidx188 = getelementptr inbounds float, float* %230, i32 3
  %231 = load float, float* %arrayidx188, align 4
  %232 = load float*, float** %b1.addr, align 4
  %arrayidx189 = getelementptr inbounds float, float* %232, i32 12
  %233 = load float, float* %arrayidx189, align 4
  %add190 = fadd float %231, %233
  %234 = load float*, float** %b2.addr, align 4
  %arrayidx191 = getelementptr inbounds float, float* %234, i32 3
  store float %add190, float* %arrayidx191, align 4
  %235 = load float*, float** %b1.addr, align 4
  %arrayidx192 = getelementptr inbounds float, float* %235, i32 3
  %236 = load float, float* %arrayidx192, align 4
  %237 = load float*, float** %b1.addr, align 4
  %arrayidx193 = getelementptr inbounds float, float* %237, i32 12
  %238 = load float, float* %arrayidx193, align 4
  %sub194 = fsub float %236, %238
  %239 = load float*, float** %costab157, align 4
  %arrayidx195 = getelementptr inbounds float, float* %239, i32 3
  %240 = load float, float* %arrayidx195, align 4
  %mul196 = fmul float %sub194, %240
  %241 = load float*, float** %b2.addr, align 4
  %arrayidx197 = getelementptr inbounds float, float* %241, i32 12
  store float %mul196, float* %arrayidx197, align 4
  %242 = load float*, float** %b1.addr, align 4
  %arrayidx198 = getelementptr inbounds float, float* %242, i32 4
  %243 = load float, float* %arrayidx198, align 4
  %244 = load float*, float** %b1.addr, align 4
  %arrayidx199 = getelementptr inbounds float, float* %244, i32 11
  %245 = load float, float* %arrayidx199, align 4
  %add200 = fadd float %243, %245
  %246 = load float*, float** %b2.addr, align 4
  %arrayidx201 = getelementptr inbounds float, float* %246, i32 4
  store float %add200, float* %arrayidx201, align 4
  %247 = load float*, float** %b1.addr, align 4
  %arrayidx202 = getelementptr inbounds float, float* %247, i32 4
  %248 = load float, float* %arrayidx202, align 4
  %249 = load float*, float** %b1.addr, align 4
  %arrayidx203 = getelementptr inbounds float, float* %249, i32 11
  %250 = load float, float* %arrayidx203, align 4
  %sub204 = fsub float %248, %250
  %251 = load float*, float** %costab157, align 4
  %arrayidx205 = getelementptr inbounds float, float* %251, i32 4
  %252 = load float, float* %arrayidx205, align 4
  %mul206 = fmul float %sub204, %252
  %253 = load float*, float** %b2.addr, align 4
  %arrayidx207 = getelementptr inbounds float, float* %253, i32 11
  store float %mul206, float* %arrayidx207, align 4
  %254 = load float*, float** %b1.addr, align 4
  %arrayidx208 = getelementptr inbounds float, float* %254, i32 5
  %255 = load float, float* %arrayidx208, align 4
  %256 = load float*, float** %b1.addr, align 4
  %arrayidx209 = getelementptr inbounds float, float* %256, i32 10
  %257 = load float, float* %arrayidx209, align 4
  %add210 = fadd float %255, %257
  %258 = load float*, float** %b2.addr, align 4
  %arrayidx211 = getelementptr inbounds float, float* %258, i32 5
  store float %add210, float* %arrayidx211, align 4
  %259 = load float*, float** %b1.addr, align 4
  %arrayidx212 = getelementptr inbounds float, float* %259, i32 5
  %260 = load float, float* %arrayidx212, align 4
  %261 = load float*, float** %b1.addr, align 4
  %arrayidx213 = getelementptr inbounds float, float* %261, i32 10
  %262 = load float, float* %arrayidx213, align 4
  %sub214 = fsub float %260, %262
  %263 = load float*, float** %costab157, align 4
  %arrayidx215 = getelementptr inbounds float, float* %263, i32 5
  %264 = load float, float* %arrayidx215, align 4
  %mul216 = fmul float %sub214, %264
  %265 = load float*, float** %b2.addr, align 4
  %arrayidx217 = getelementptr inbounds float, float* %265, i32 10
  store float %mul216, float* %arrayidx217, align 4
  %266 = load float*, float** %b1.addr, align 4
  %arrayidx218 = getelementptr inbounds float, float* %266, i32 6
  %267 = load float, float* %arrayidx218, align 4
  %268 = load float*, float** %b1.addr, align 4
  %arrayidx219 = getelementptr inbounds float, float* %268, i32 9
  %269 = load float, float* %arrayidx219, align 4
  %add220 = fadd float %267, %269
  %270 = load float*, float** %b2.addr, align 4
  %arrayidx221 = getelementptr inbounds float, float* %270, i32 6
  store float %add220, float* %arrayidx221, align 4
  %271 = load float*, float** %b1.addr, align 4
  %arrayidx222 = getelementptr inbounds float, float* %271, i32 6
  %272 = load float, float* %arrayidx222, align 4
  %273 = load float*, float** %b1.addr, align 4
  %arrayidx223 = getelementptr inbounds float, float* %273, i32 9
  %274 = load float, float* %arrayidx223, align 4
  %sub224 = fsub float %272, %274
  %275 = load float*, float** %costab157, align 4
  %arrayidx225 = getelementptr inbounds float, float* %275, i32 6
  %276 = load float, float* %arrayidx225, align 4
  %mul226 = fmul float %sub224, %276
  %277 = load float*, float** %b2.addr, align 4
  %arrayidx227 = getelementptr inbounds float, float* %277, i32 9
  store float %mul226, float* %arrayidx227, align 4
  %278 = load float*, float** %b1.addr, align 4
  %arrayidx228 = getelementptr inbounds float, float* %278, i32 7
  %279 = load float, float* %arrayidx228, align 4
  %280 = load float*, float** %b1.addr, align 4
  %arrayidx229 = getelementptr inbounds float, float* %280, i32 8
  %281 = load float, float* %arrayidx229, align 4
  %add230 = fadd float %279, %281
  %282 = load float*, float** %b2.addr, align 4
  %arrayidx231 = getelementptr inbounds float, float* %282, i32 7
  store float %add230, float* %arrayidx231, align 4
  %283 = load float*, float** %b1.addr, align 4
  %arrayidx232 = getelementptr inbounds float, float* %283, i32 7
  %284 = load float, float* %arrayidx232, align 4
  %285 = load float*, float** %b1.addr, align 4
  %arrayidx233 = getelementptr inbounds float, float* %285, i32 8
  %286 = load float, float* %arrayidx233, align 4
  %sub234 = fsub float %284, %286
  %287 = load float*, float** %costab157, align 4
  %arrayidx235 = getelementptr inbounds float, float* %287, i32 7
  %288 = load float, float* %arrayidx235, align 4
  %mul236 = fmul float %sub234, %288
  %289 = load float*, float** %b2.addr, align 4
  %arrayidx237 = getelementptr inbounds float, float* %289, i32 8
  store float %mul236, float* %arrayidx237, align 4
  %290 = load float*, float** %b1.addr, align 4
  %arrayidx238 = getelementptr inbounds float, float* %290, i32 16
  %291 = load float, float* %arrayidx238, align 4
  %292 = load float*, float** %b1.addr, align 4
  %arrayidx239 = getelementptr inbounds float, float* %292, i32 31
  %293 = load float, float* %arrayidx239, align 4
  %add240 = fadd float %291, %293
  %294 = load float*, float** %b2.addr, align 4
  %arrayidx241 = getelementptr inbounds float, float* %294, i32 16
  store float %add240, float* %arrayidx241, align 4
  %295 = load float*, float** %b1.addr, align 4
  %arrayidx242 = getelementptr inbounds float, float* %295, i32 31
  %296 = load float, float* %arrayidx242, align 4
  %297 = load float*, float** %b1.addr, align 4
  %arrayidx243 = getelementptr inbounds float, float* %297, i32 16
  %298 = load float, float* %arrayidx243, align 4
  %sub244 = fsub float %296, %298
  %299 = load float*, float** %costab157, align 4
  %arrayidx245 = getelementptr inbounds float, float* %299, i32 0
  %300 = load float, float* %arrayidx245, align 4
  %mul246 = fmul float %sub244, %300
  %301 = load float*, float** %b2.addr, align 4
  %arrayidx247 = getelementptr inbounds float, float* %301, i32 31
  store float %mul246, float* %arrayidx247, align 4
  %302 = load float*, float** %b1.addr, align 4
  %arrayidx248 = getelementptr inbounds float, float* %302, i32 17
  %303 = load float, float* %arrayidx248, align 4
  %304 = load float*, float** %b1.addr, align 4
  %arrayidx249 = getelementptr inbounds float, float* %304, i32 30
  %305 = load float, float* %arrayidx249, align 4
  %add250 = fadd float %303, %305
  %306 = load float*, float** %b2.addr, align 4
  %arrayidx251 = getelementptr inbounds float, float* %306, i32 17
  store float %add250, float* %arrayidx251, align 4
  %307 = load float*, float** %b1.addr, align 4
  %arrayidx252 = getelementptr inbounds float, float* %307, i32 30
  %308 = load float, float* %arrayidx252, align 4
  %309 = load float*, float** %b1.addr, align 4
  %arrayidx253 = getelementptr inbounds float, float* %309, i32 17
  %310 = load float, float* %arrayidx253, align 4
  %sub254 = fsub float %308, %310
  %311 = load float*, float** %costab157, align 4
  %arrayidx255 = getelementptr inbounds float, float* %311, i32 1
  %312 = load float, float* %arrayidx255, align 4
  %mul256 = fmul float %sub254, %312
  %313 = load float*, float** %b2.addr, align 4
  %arrayidx257 = getelementptr inbounds float, float* %313, i32 30
  store float %mul256, float* %arrayidx257, align 4
  %314 = load float*, float** %b1.addr, align 4
  %arrayidx258 = getelementptr inbounds float, float* %314, i32 18
  %315 = load float, float* %arrayidx258, align 4
  %316 = load float*, float** %b1.addr, align 4
  %arrayidx259 = getelementptr inbounds float, float* %316, i32 29
  %317 = load float, float* %arrayidx259, align 4
  %add260 = fadd float %315, %317
  %318 = load float*, float** %b2.addr, align 4
  %arrayidx261 = getelementptr inbounds float, float* %318, i32 18
  store float %add260, float* %arrayidx261, align 4
  %319 = load float*, float** %b1.addr, align 4
  %arrayidx262 = getelementptr inbounds float, float* %319, i32 29
  %320 = load float, float* %arrayidx262, align 4
  %321 = load float*, float** %b1.addr, align 4
  %arrayidx263 = getelementptr inbounds float, float* %321, i32 18
  %322 = load float, float* %arrayidx263, align 4
  %sub264 = fsub float %320, %322
  %323 = load float*, float** %costab157, align 4
  %arrayidx265 = getelementptr inbounds float, float* %323, i32 2
  %324 = load float, float* %arrayidx265, align 4
  %mul266 = fmul float %sub264, %324
  %325 = load float*, float** %b2.addr, align 4
  %arrayidx267 = getelementptr inbounds float, float* %325, i32 29
  store float %mul266, float* %arrayidx267, align 4
  %326 = load float*, float** %b1.addr, align 4
  %arrayidx268 = getelementptr inbounds float, float* %326, i32 19
  %327 = load float, float* %arrayidx268, align 4
  %328 = load float*, float** %b1.addr, align 4
  %arrayidx269 = getelementptr inbounds float, float* %328, i32 28
  %329 = load float, float* %arrayidx269, align 4
  %add270 = fadd float %327, %329
  %330 = load float*, float** %b2.addr, align 4
  %arrayidx271 = getelementptr inbounds float, float* %330, i32 19
  store float %add270, float* %arrayidx271, align 4
  %331 = load float*, float** %b1.addr, align 4
  %arrayidx272 = getelementptr inbounds float, float* %331, i32 28
  %332 = load float, float* %arrayidx272, align 4
  %333 = load float*, float** %b1.addr, align 4
  %arrayidx273 = getelementptr inbounds float, float* %333, i32 19
  %334 = load float, float* %arrayidx273, align 4
  %sub274 = fsub float %332, %334
  %335 = load float*, float** %costab157, align 4
  %arrayidx275 = getelementptr inbounds float, float* %335, i32 3
  %336 = load float, float* %arrayidx275, align 4
  %mul276 = fmul float %sub274, %336
  %337 = load float*, float** %b2.addr, align 4
  %arrayidx277 = getelementptr inbounds float, float* %337, i32 28
  store float %mul276, float* %arrayidx277, align 4
  %338 = load float*, float** %b1.addr, align 4
  %arrayidx278 = getelementptr inbounds float, float* %338, i32 20
  %339 = load float, float* %arrayidx278, align 4
  %340 = load float*, float** %b1.addr, align 4
  %arrayidx279 = getelementptr inbounds float, float* %340, i32 27
  %341 = load float, float* %arrayidx279, align 4
  %add280 = fadd float %339, %341
  %342 = load float*, float** %b2.addr, align 4
  %arrayidx281 = getelementptr inbounds float, float* %342, i32 20
  store float %add280, float* %arrayidx281, align 4
  %343 = load float*, float** %b1.addr, align 4
  %arrayidx282 = getelementptr inbounds float, float* %343, i32 27
  %344 = load float, float* %arrayidx282, align 4
  %345 = load float*, float** %b1.addr, align 4
  %arrayidx283 = getelementptr inbounds float, float* %345, i32 20
  %346 = load float, float* %arrayidx283, align 4
  %sub284 = fsub float %344, %346
  %347 = load float*, float** %costab157, align 4
  %arrayidx285 = getelementptr inbounds float, float* %347, i32 4
  %348 = load float, float* %arrayidx285, align 4
  %mul286 = fmul float %sub284, %348
  %349 = load float*, float** %b2.addr, align 4
  %arrayidx287 = getelementptr inbounds float, float* %349, i32 27
  store float %mul286, float* %arrayidx287, align 4
  %350 = load float*, float** %b1.addr, align 4
  %arrayidx288 = getelementptr inbounds float, float* %350, i32 21
  %351 = load float, float* %arrayidx288, align 4
  %352 = load float*, float** %b1.addr, align 4
  %arrayidx289 = getelementptr inbounds float, float* %352, i32 26
  %353 = load float, float* %arrayidx289, align 4
  %add290 = fadd float %351, %353
  %354 = load float*, float** %b2.addr, align 4
  %arrayidx291 = getelementptr inbounds float, float* %354, i32 21
  store float %add290, float* %arrayidx291, align 4
  %355 = load float*, float** %b1.addr, align 4
  %arrayidx292 = getelementptr inbounds float, float* %355, i32 26
  %356 = load float, float* %arrayidx292, align 4
  %357 = load float*, float** %b1.addr, align 4
  %arrayidx293 = getelementptr inbounds float, float* %357, i32 21
  %358 = load float, float* %arrayidx293, align 4
  %sub294 = fsub float %356, %358
  %359 = load float*, float** %costab157, align 4
  %arrayidx295 = getelementptr inbounds float, float* %359, i32 5
  %360 = load float, float* %arrayidx295, align 4
  %mul296 = fmul float %sub294, %360
  %361 = load float*, float** %b2.addr, align 4
  %arrayidx297 = getelementptr inbounds float, float* %361, i32 26
  store float %mul296, float* %arrayidx297, align 4
  %362 = load float*, float** %b1.addr, align 4
  %arrayidx298 = getelementptr inbounds float, float* %362, i32 22
  %363 = load float, float* %arrayidx298, align 4
  %364 = load float*, float** %b1.addr, align 4
  %arrayidx299 = getelementptr inbounds float, float* %364, i32 25
  %365 = load float, float* %arrayidx299, align 4
  %add300 = fadd float %363, %365
  %366 = load float*, float** %b2.addr, align 4
  %arrayidx301 = getelementptr inbounds float, float* %366, i32 22
  store float %add300, float* %arrayidx301, align 4
  %367 = load float*, float** %b1.addr, align 4
  %arrayidx302 = getelementptr inbounds float, float* %367, i32 25
  %368 = load float, float* %arrayidx302, align 4
  %369 = load float*, float** %b1.addr, align 4
  %arrayidx303 = getelementptr inbounds float, float* %369, i32 22
  %370 = load float, float* %arrayidx303, align 4
  %sub304 = fsub float %368, %370
  %371 = load float*, float** %costab157, align 4
  %arrayidx305 = getelementptr inbounds float, float* %371, i32 6
  %372 = load float, float* %arrayidx305, align 4
  %mul306 = fmul float %sub304, %372
  %373 = load float*, float** %b2.addr, align 4
  %arrayidx307 = getelementptr inbounds float, float* %373, i32 25
  store float %mul306, float* %arrayidx307, align 4
  %374 = load float*, float** %b1.addr, align 4
  %arrayidx308 = getelementptr inbounds float, float* %374, i32 23
  %375 = load float, float* %arrayidx308, align 4
  %376 = load float*, float** %b1.addr, align 4
  %arrayidx309 = getelementptr inbounds float, float* %376, i32 24
  %377 = load float, float* %arrayidx309, align 4
  %add310 = fadd float %375, %377
  %378 = load float*, float** %b2.addr, align 4
  %arrayidx311 = getelementptr inbounds float, float* %378, i32 23
  store float %add310, float* %arrayidx311, align 4
  %379 = load float*, float** %b1.addr, align 4
  %arrayidx312 = getelementptr inbounds float, float* %379, i32 24
  %380 = load float, float* %arrayidx312, align 4
  %381 = load float*, float** %b1.addr, align 4
  %arrayidx313 = getelementptr inbounds float, float* %381, i32 23
  %382 = load float, float* %arrayidx313, align 4
  %sub314 = fsub float %380, %382
  %383 = load float*, float** %costab157, align 4
  %arrayidx315 = getelementptr inbounds float, float* %383, i32 7
  %384 = load float, float* %arrayidx315, align 4
  %mul316 = fmul float %sub314, %384
  %385 = load float*, float** %b2.addr, align 4
  %arrayidx317 = getelementptr inbounds float, float* %385, i32 24
  store float %mul316, float* %arrayidx317, align 4
  %386 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 2), align 8
  store float* %386, float** %costab318, align 4
  %387 = load float*, float** %b2.addr, align 4
  %arrayidx319 = getelementptr inbounds float, float* %387, i32 0
  %388 = load float, float* %arrayidx319, align 4
  %389 = load float*, float** %b2.addr, align 4
  %arrayidx320 = getelementptr inbounds float, float* %389, i32 7
  %390 = load float, float* %arrayidx320, align 4
  %add321 = fadd float %388, %390
  %391 = load float*, float** %b1.addr, align 4
  %arrayidx322 = getelementptr inbounds float, float* %391, i32 0
  store float %add321, float* %arrayidx322, align 4
  %392 = load float*, float** %b2.addr, align 4
  %arrayidx323 = getelementptr inbounds float, float* %392, i32 0
  %393 = load float, float* %arrayidx323, align 4
  %394 = load float*, float** %b2.addr, align 4
  %arrayidx324 = getelementptr inbounds float, float* %394, i32 7
  %395 = load float, float* %arrayidx324, align 4
  %sub325 = fsub float %393, %395
  %396 = load float*, float** %costab318, align 4
  %arrayidx326 = getelementptr inbounds float, float* %396, i32 0
  %397 = load float, float* %arrayidx326, align 4
  %mul327 = fmul float %sub325, %397
  %398 = load float*, float** %b1.addr, align 4
  %arrayidx328 = getelementptr inbounds float, float* %398, i32 7
  store float %mul327, float* %arrayidx328, align 4
  %399 = load float*, float** %b2.addr, align 4
  %arrayidx329 = getelementptr inbounds float, float* %399, i32 1
  %400 = load float, float* %arrayidx329, align 4
  %401 = load float*, float** %b2.addr, align 4
  %arrayidx330 = getelementptr inbounds float, float* %401, i32 6
  %402 = load float, float* %arrayidx330, align 4
  %add331 = fadd float %400, %402
  %403 = load float*, float** %b1.addr, align 4
  %arrayidx332 = getelementptr inbounds float, float* %403, i32 1
  store float %add331, float* %arrayidx332, align 4
  %404 = load float*, float** %b2.addr, align 4
  %arrayidx333 = getelementptr inbounds float, float* %404, i32 1
  %405 = load float, float* %arrayidx333, align 4
  %406 = load float*, float** %b2.addr, align 4
  %arrayidx334 = getelementptr inbounds float, float* %406, i32 6
  %407 = load float, float* %arrayidx334, align 4
  %sub335 = fsub float %405, %407
  %408 = load float*, float** %costab318, align 4
  %arrayidx336 = getelementptr inbounds float, float* %408, i32 1
  %409 = load float, float* %arrayidx336, align 4
  %mul337 = fmul float %sub335, %409
  %410 = load float*, float** %b1.addr, align 4
  %arrayidx338 = getelementptr inbounds float, float* %410, i32 6
  store float %mul337, float* %arrayidx338, align 4
  %411 = load float*, float** %b2.addr, align 4
  %arrayidx339 = getelementptr inbounds float, float* %411, i32 2
  %412 = load float, float* %arrayidx339, align 4
  %413 = load float*, float** %b2.addr, align 4
  %arrayidx340 = getelementptr inbounds float, float* %413, i32 5
  %414 = load float, float* %arrayidx340, align 4
  %add341 = fadd float %412, %414
  %415 = load float*, float** %b1.addr, align 4
  %arrayidx342 = getelementptr inbounds float, float* %415, i32 2
  store float %add341, float* %arrayidx342, align 4
  %416 = load float*, float** %b2.addr, align 4
  %arrayidx343 = getelementptr inbounds float, float* %416, i32 2
  %417 = load float, float* %arrayidx343, align 4
  %418 = load float*, float** %b2.addr, align 4
  %arrayidx344 = getelementptr inbounds float, float* %418, i32 5
  %419 = load float, float* %arrayidx344, align 4
  %sub345 = fsub float %417, %419
  %420 = load float*, float** %costab318, align 4
  %arrayidx346 = getelementptr inbounds float, float* %420, i32 2
  %421 = load float, float* %arrayidx346, align 4
  %mul347 = fmul float %sub345, %421
  %422 = load float*, float** %b1.addr, align 4
  %arrayidx348 = getelementptr inbounds float, float* %422, i32 5
  store float %mul347, float* %arrayidx348, align 4
  %423 = load float*, float** %b2.addr, align 4
  %arrayidx349 = getelementptr inbounds float, float* %423, i32 3
  %424 = load float, float* %arrayidx349, align 4
  %425 = load float*, float** %b2.addr, align 4
  %arrayidx350 = getelementptr inbounds float, float* %425, i32 4
  %426 = load float, float* %arrayidx350, align 4
  %add351 = fadd float %424, %426
  %427 = load float*, float** %b1.addr, align 4
  %arrayidx352 = getelementptr inbounds float, float* %427, i32 3
  store float %add351, float* %arrayidx352, align 4
  %428 = load float*, float** %b2.addr, align 4
  %arrayidx353 = getelementptr inbounds float, float* %428, i32 3
  %429 = load float, float* %arrayidx353, align 4
  %430 = load float*, float** %b2.addr, align 4
  %arrayidx354 = getelementptr inbounds float, float* %430, i32 4
  %431 = load float, float* %arrayidx354, align 4
  %sub355 = fsub float %429, %431
  %432 = load float*, float** %costab318, align 4
  %arrayidx356 = getelementptr inbounds float, float* %432, i32 3
  %433 = load float, float* %arrayidx356, align 4
  %mul357 = fmul float %sub355, %433
  %434 = load float*, float** %b1.addr, align 4
  %arrayidx358 = getelementptr inbounds float, float* %434, i32 4
  store float %mul357, float* %arrayidx358, align 4
  %435 = load float*, float** %b2.addr, align 4
  %arrayidx359 = getelementptr inbounds float, float* %435, i32 8
  %436 = load float, float* %arrayidx359, align 4
  %437 = load float*, float** %b2.addr, align 4
  %arrayidx360 = getelementptr inbounds float, float* %437, i32 15
  %438 = load float, float* %arrayidx360, align 4
  %add361 = fadd float %436, %438
  %439 = load float*, float** %b1.addr, align 4
  %arrayidx362 = getelementptr inbounds float, float* %439, i32 8
  store float %add361, float* %arrayidx362, align 4
  %440 = load float*, float** %b2.addr, align 4
  %arrayidx363 = getelementptr inbounds float, float* %440, i32 15
  %441 = load float, float* %arrayidx363, align 4
  %442 = load float*, float** %b2.addr, align 4
  %arrayidx364 = getelementptr inbounds float, float* %442, i32 8
  %443 = load float, float* %arrayidx364, align 4
  %sub365 = fsub float %441, %443
  %444 = load float*, float** %costab318, align 4
  %arrayidx366 = getelementptr inbounds float, float* %444, i32 0
  %445 = load float, float* %arrayidx366, align 4
  %mul367 = fmul float %sub365, %445
  %446 = load float*, float** %b1.addr, align 4
  %arrayidx368 = getelementptr inbounds float, float* %446, i32 15
  store float %mul367, float* %arrayidx368, align 4
  %447 = load float*, float** %b2.addr, align 4
  %arrayidx369 = getelementptr inbounds float, float* %447, i32 9
  %448 = load float, float* %arrayidx369, align 4
  %449 = load float*, float** %b2.addr, align 4
  %arrayidx370 = getelementptr inbounds float, float* %449, i32 14
  %450 = load float, float* %arrayidx370, align 4
  %add371 = fadd float %448, %450
  %451 = load float*, float** %b1.addr, align 4
  %arrayidx372 = getelementptr inbounds float, float* %451, i32 9
  store float %add371, float* %arrayidx372, align 4
  %452 = load float*, float** %b2.addr, align 4
  %arrayidx373 = getelementptr inbounds float, float* %452, i32 14
  %453 = load float, float* %arrayidx373, align 4
  %454 = load float*, float** %b2.addr, align 4
  %arrayidx374 = getelementptr inbounds float, float* %454, i32 9
  %455 = load float, float* %arrayidx374, align 4
  %sub375 = fsub float %453, %455
  %456 = load float*, float** %costab318, align 4
  %arrayidx376 = getelementptr inbounds float, float* %456, i32 1
  %457 = load float, float* %arrayidx376, align 4
  %mul377 = fmul float %sub375, %457
  %458 = load float*, float** %b1.addr, align 4
  %arrayidx378 = getelementptr inbounds float, float* %458, i32 14
  store float %mul377, float* %arrayidx378, align 4
  %459 = load float*, float** %b2.addr, align 4
  %arrayidx379 = getelementptr inbounds float, float* %459, i32 10
  %460 = load float, float* %arrayidx379, align 4
  %461 = load float*, float** %b2.addr, align 4
  %arrayidx380 = getelementptr inbounds float, float* %461, i32 13
  %462 = load float, float* %arrayidx380, align 4
  %add381 = fadd float %460, %462
  %463 = load float*, float** %b1.addr, align 4
  %arrayidx382 = getelementptr inbounds float, float* %463, i32 10
  store float %add381, float* %arrayidx382, align 4
  %464 = load float*, float** %b2.addr, align 4
  %arrayidx383 = getelementptr inbounds float, float* %464, i32 13
  %465 = load float, float* %arrayidx383, align 4
  %466 = load float*, float** %b2.addr, align 4
  %arrayidx384 = getelementptr inbounds float, float* %466, i32 10
  %467 = load float, float* %arrayidx384, align 4
  %sub385 = fsub float %465, %467
  %468 = load float*, float** %costab318, align 4
  %arrayidx386 = getelementptr inbounds float, float* %468, i32 2
  %469 = load float, float* %arrayidx386, align 4
  %mul387 = fmul float %sub385, %469
  %470 = load float*, float** %b1.addr, align 4
  %arrayidx388 = getelementptr inbounds float, float* %470, i32 13
  store float %mul387, float* %arrayidx388, align 4
  %471 = load float*, float** %b2.addr, align 4
  %arrayidx389 = getelementptr inbounds float, float* %471, i32 11
  %472 = load float, float* %arrayidx389, align 4
  %473 = load float*, float** %b2.addr, align 4
  %arrayidx390 = getelementptr inbounds float, float* %473, i32 12
  %474 = load float, float* %arrayidx390, align 4
  %add391 = fadd float %472, %474
  %475 = load float*, float** %b1.addr, align 4
  %arrayidx392 = getelementptr inbounds float, float* %475, i32 11
  store float %add391, float* %arrayidx392, align 4
  %476 = load float*, float** %b2.addr, align 4
  %arrayidx393 = getelementptr inbounds float, float* %476, i32 12
  %477 = load float, float* %arrayidx393, align 4
  %478 = load float*, float** %b2.addr, align 4
  %arrayidx394 = getelementptr inbounds float, float* %478, i32 11
  %479 = load float, float* %arrayidx394, align 4
  %sub395 = fsub float %477, %479
  %480 = load float*, float** %costab318, align 4
  %arrayidx396 = getelementptr inbounds float, float* %480, i32 3
  %481 = load float, float* %arrayidx396, align 4
  %mul397 = fmul float %sub395, %481
  %482 = load float*, float** %b1.addr, align 4
  %arrayidx398 = getelementptr inbounds float, float* %482, i32 12
  store float %mul397, float* %arrayidx398, align 4
  %483 = load float*, float** %b2.addr, align 4
  %arrayidx399 = getelementptr inbounds float, float* %483, i32 16
  %484 = load float, float* %arrayidx399, align 4
  %485 = load float*, float** %b2.addr, align 4
  %arrayidx400 = getelementptr inbounds float, float* %485, i32 23
  %486 = load float, float* %arrayidx400, align 4
  %add401 = fadd float %484, %486
  %487 = load float*, float** %b1.addr, align 4
  %arrayidx402 = getelementptr inbounds float, float* %487, i32 16
  store float %add401, float* %arrayidx402, align 4
  %488 = load float*, float** %b2.addr, align 4
  %arrayidx403 = getelementptr inbounds float, float* %488, i32 16
  %489 = load float, float* %arrayidx403, align 4
  %490 = load float*, float** %b2.addr, align 4
  %arrayidx404 = getelementptr inbounds float, float* %490, i32 23
  %491 = load float, float* %arrayidx404, align 4
  %sub405 = fsub float %489, %491
  %492 = load float*, float** %costab318, align 4
  %arrayidx406 = getelementptr inbounds float, float* %492, i32 0
  %493 = load float, float* %arrayidx406, align 4
  %mul407 = fmul float %sub405, %493
  %494 = load float*, float** %b1.addr, align 4
  %arrayidx408 = getelementptr inbounds float, float* %494, i32 23
  store float %mul407, float* %arrayidx408, align 4
  %495 = load float*, float** %b2.addr, align 4
  %arrayidx409 = getelementptr inbounds float, float* %495, i32 17
  %496 = load float, float* %arrayidx409, align 4
  %497 = load float*, float** %b2.addr, align 4
  %arrayidx410 = getelementptr inbounds float, float* %497, i32 22
  %498 = load float, float* %arrayidx410, align 4
  %add411 = fadd float %496, %498
  %499 = load float*, float** %b1.addr, align 4
  %arrayidx412 = getelementptr inbounds float, float* %499, i32 17
  store float %add411, float* %arrayidx412, align 4
  %500 = load float*, float** %b2.addr, align 4
  %arrayidx413 = getelementptr inbounds float, float* %500, i32 17
  %501 = load float, float* %arrayidx413, align 4
  %502 = load float*, float** %b2.addr, align 4
  %arrayidx414 = getelementptr inbounds float, float* %502, i32 22
  %503 = load float, float* %arrayidx414, align 4
  %sub415 = fsub float %501, %503
  %504 = load float*, float** %costab318, align 4
  %arrayidx416 = getelementptr inbounds float, float* %504, i32 1
  %505 = load float, float* %arrayidx416, align 4
  %mul417 = fmul float %sub415, %505
  %506 = load float*, float** %b1.addr, align 4
  %arrayidx418 = getelementptr inbounds float, float* %506, i32 22
  store float %mul417, float* %arrayidx418, align 4
  %507 = load float*, float** %b2.addr, align 4
  %arrayidx419 = getelementptr inbounds float, float* %507, i32 18
  %508 = load float, float* %arrayidx419, align 4
  %509 = load float*, float** %b2.addr, align 4
  %arrayidx420 = getelementptr inbounds float, float* %509, i32 21
  %510 = load float, float* %arrayidx420, align 4
  %add421 = fadd float %508, %510
  %511 = load float*, float** %b1.addr, align 4
  %arrayidx422 = getelementptr inbounds float, float* %511, i32 18
  store float %add421, float* %arrayidx422, align 4
  %512 = load float*, float** %b2.addr, align 4
  %arrayidx423 = getelementptr inbounds float, float* %512, i32 18
  %513 = load float, float* %arrayidx423, align 4
  %514 = load float*, float** %b2.addr, align 4
  %arrayidx424 = getelementptr inbounds float, float* %514, i32 21
  %515 = load float, float* %arrayidx424, align 4
  %sub425 = fsub float %513, %515
  %516 = load float*, float** %costab318, align 4
  %arrayidx426 = getelementptr inbounds float, float* %516, i32 2
  %517 = load float, float* %arrayidx426, align 4
  %mul427 = fmul float %sub425, %517
  %518 = load float*, float** %b1.addr, align 4
  %arrayidx428 = getelementptr inbounds float, float* %518, i32 21
  store float %mul427, float* %arrayidx428, align 4
  %519 = load float*, float** %b2.addr, align 4
  %arrayidx429 = getelementptr inbounds float, float* %519, i32 19
  %520 = load float, float* %arrayidx429, align 4
  %521 = load float*, float** %b2.addr, align 4
  %arrayidx430 = getelementptr inbounds float, float* %521, i32 20
  %522 = load float, float* %arrayidx430, align 4
  %add431 = fadd float %520, %522
  %523 = load float*, float** %b1.addr, align 4
  %arrayidx432 = getelementptr inbounds float, float* %523, i32 19
  store float %add431, float* %arrayidx432, align 4
  %524 = load float*, float** %b2.addr, align 4
  %arrayidx433 = getelementptr inbounds float, float* %524, i32 19
  %525 = load float, float* %arrayidx433, align 4
  %526 = load float*, float** %b2.addr, align 4
  %arrayidx434 = getelementptr inbounds float, float* %526, i32 20
  %527 = load float, float* %arrayidx434, align 4
  %sub435 = fsub float %525, %527
  %528 = load float*, float** %costab318, align 4
  %arrayidx436 = getelementptr inbounds float, float* %528, i32 3
  %529 = load float, float* %arrayidx436, align 4
  %mul437 = fmul float %sub435, %529
  %530 = load float*, float** %b1.addr, align 4
  %arrayidx438 = getelementptr inbounds float, float* %530, i32 20
  store float %mul437, float* %arrayidx438, align 4
  %531 = load float*, float** %b2.addr, align 4
  %arrayidx439 = getelementptr inbounds float, float* %531, i32 24
  %532 = load float, float* %arrayidx439, align 4
  %533 = load float*, float** %b2.addr, align 4
  %arrayidx440 = getelementptr inbounds float, float* %533, i32 31
  %534 = load float, float* %arrayidx440, align 4
  %add441 = fadd float %532, %534
  %535 = load float*, float** %b1.addr, align 4
  %arrayidx442 = getelementptr inbounds float, float* %535, i32 24
  store float %add441, float* %arrayidx442, align 4
  %536 = load float*, float** %b2.addr, align 4
  %arrayidx443 = getelementptr inbounds float, float* %536, i32 31
  %537 = load float, float* %arrayidx443, align 4
  %538 = load float*, float** %b2.addr, align 4
  %arrayidx444 = getelementptr inbounds float, float* %538, i32 24
  %539 = load float, float* %arrayidx444, align 4
  %sub445 = fsub float %537, %539
  %540 = load float*, float** %costab318, align 4
  %arrayidx446 = getelementptr inbounds float, float* %540, i32 0
  %541 = load float, float* %arrayidx446, align 4
  %mul447 = fmul float %sub445, %541
  %542 = load float*, float** %b1.addr, align 4
  %arrayidx448 = getelementptr inbounds float, float* %542, i32 31
  store float %mul447, float* %arrayidx448, align 4
  %543 = load float*, float** %b2.addr, align 4
  %arrayidx449 = getelementptr inbounds float, float* %543, i32 25
  %544 = load float, float* %arrayidx449, align 4
  %545 = load float*, float** %b2.addr, align 4
  %arrayidx450 = getelementptr inbounds float, float* %545, i32 30
  %546 = load float, float* %arrayidx450, align 4
  %add451 = fadd float %544, %546
  %547 = load float*, float** %b1.addr, align 4
  %arrayidx452 = getelementptr inbounds float, float* %547, i32 25
  store float %add451, float* %arrayidx452, align 4
  %548 = load float*, float** %b2.addr, align 4
  %arrayidx453 = getelementptr inbounds float, float* %548, i32 30
  %549 = load float, float* %arrayidx453, align 4
  %550 = load float*, float** %b2.addr, align 4
  %arrayidx454 = getelementptr inbounds float, float* %550, i32 25
  %551 = load float, float* %arrayidx454, align 4
  %sub455 = fsub float %549, %551
  %552 = load float*, float** %costab318, align 4
  %arrayidx456 = getelementptr inbounds float, float* %552, i32 1
  %553 = load float, float* %arrayidx456, align 4
  %mul457 = fmul float %sub455, %553
  %554 = load float*, float** %b1.addr, align 4
  %arrayidx458 = getelementptr inbounds float, float* %554, i32 30
  store float %mul457, float* %arrayidx458, align 4
  %555 = load float*, float** %b2.addr, align 4
  %arrayidx459 = getelementptr inbounds float, float* %555, i32 26
  %556 = load float, float* %arrayidx459, align 4
  %557 = load float*, float** %b2.addr, align 4
  %arrayidx460 = getelementptr inbounds float, float* %557, i32 29
  %558 = load float, float* %arrayidx460, align 4
  %add461 = fadd float %556, %558
  %559 = load float*, float** %b1.addr, align 4
  %arrayidx462 = getelementptr inbounds float, float* %559, i32 26
  store float %add461, float* %arrayidx462, align 4
  %560 = load float*, float** %b2.addr, align 4
  %arrayidx463 = getelementptr inbounds float, float* %560, i32 29
  %561 = load float, float* %arrayidx463, align 4
  %562 = load float*, float** %b2.addr, align 4
  %arrayidx464 = getelementptr inbounds float, float* %562, i32 26
  %563 = load float, float* %arrayidx464, align 4
  %sub465 = fsub float %561, %563
  %564 = load float*, float** %costab318, align 4
  %arrayidx466 = getelementptr inbounds float, float* %564, i32 2
  %565 = load float, float* %arrayidx466, align 4
  %mul467 = fmul float %sub465, %565
  %566 = load float*, float** %b1.addr, align 4
  %arrayidx468 = getelementptr inbounds float, float* %566, i32 29
  store float %mul467, float* %arrayidx468, align 4
  %567 = load float*, float** %b2.addr, align 4
  %arrayidx469 = getelementptr inbounds float, float* %567, i32 27
  %568 = load float, float* %arrayidx469, align 4
  %569 = load float*, float** %b2.addr, align 4
  %arrayidx470 = getelementptr inbounds float, float* %569, i32 28
  %570 = load float, float* %arrayidx470, align 4
  %add471 = fadd float %568, %570
  %571 = load float*, float** %b1.addr, align 4
  %arrayidx472 = getelementptr inbounds float, float* %571, i32 27
  store float %add471, float* %arrayidx472, align 4
  %572 = load float*, float** %b2.addr, align 4
  %arrayidx473 = getelementptr inbounds float, float* %572, i32 28
  %573 = load float, float* %arrayidx473, align 4
  %574 = load float*, float** %b2.addr, align 4
  %arrayidx474 = getelementptr inbounds float, float* %574, i32 27
  %575 = load float, float* %arrayidx474, align 4
  %sub475 = fsub float %573, %575
  %576 = load float*, float** %costab318, align 4
  %arrayidx476 = getelementptr inbounds float, float* %576, i32 3
  %577 = load float, float* %arrayidx476, align 4
  %mul477 = fmul float %sub475, %577
  %578 = load float*, float** %b1.addr, align 4
  %arrayidx478 = getelementptr inbounds float, float* %578, i32 28
  store float %mul477, float* %arrayidx478, align 4
  %579 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 3), align 4
  %arrayidx479 = getelementptr inbounds float, float* %579, i32 0
  %580 = load float, float* %arrayidx479, align 4
  store float %580, float* %cos0, align 4
  %581 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 3), align 4
  %arrayidx480 = getelementptr inbounds float, float* %581, i32 1
  %582 = load float, float* %arrayidx480, align 4
  store float %582, float* %cos1, align 4
  %583 = load float*, float** %b1.addr, align 4
  %arrayidx481 = getelementptr inbounds float, float* %583, i32 0
  %584 = load float, float* %arrayidx481, align 4
  %585 = load float*, float** %b1.addr, align 4
  %arrayidx482 = getelementptr inbounds float, float* %585, i32 3
  %586 = load float, float* %arrayidx482, align 4
  %add483 = fadd float %584, %586
  %587 = load float*, float** %b2.addr, align 4
  %arrayidx484 = getelementptr inbounds float, float* %587, i32 0
  store float %add483, float* %arrayidx484, align 4
  %588 = load float*, float** %b1.addr, align 4
  %arrayidx485 = getelementptr inbounds float, float* %588, i32 0
  %589 = load float, float* %arrayidx485, align 4
  %590 = load float*, float** %b1.addr, align 4
  %arrayidx486 = getelementptr inbounds float, float* %590, i32 3
  %591 = load float, float* %arrayidx486, align 4
  %sub487 = fsub float %589, %591
  %592 = load float, float* %cos0, align 4
  %mul488 = fmul float %sub487, %592
  %593 = load float*, float** %b2.addr, align 4
  %arrayidx489 = getelementptr inbounds float, float* %593, i32 3
  store float %mul488, float* %arrayidx489, align 4
  %594 = load float*, float** %b1.addr, align 4
  %arrayidx490 = getelementptr inbounds float, float* %594, i32 1
  %595 = load float, float* %arrayidx490, align 4
  %596 = load float*, float** %b1.addr, align 4
  %arrayidx491 = getelementptr inbounds float, float* %596, i32 2
  %597 = load float, float* %arrayidx491, align 4
  %add492 = fadd float %595, %597
  %598 = load float*, float** %b2.addr, align 4
  %arrayidx493 = getelementptr inbounds float, float* %598, i32 1
  store float %add492, float* %arrayidx493, align 4
  %599 = load float*, float** %b1.addr, align 4
  %arrayidx494 = getelementptr inbounds float, float* %599, i32 1
  %600 = load float, float* %arrayidx494, align 4
  %601 = load float*, float** %b1.addr, align 4
  %arrayidx495 = getelementptr inbounds float, float* %601, i32 2
  %602 = load float, float* %arrayidx495, align 4
  %sub496 = fsub float %600, %602
  %603 = load float, float* %cos1, align 4
  %mul497 = fmul float %sub496, %603
  %604 = load float*, float** %b2.addr, align 4
  %arrayidx498 = getelementptr inbounds float, float* %604, i32 2
  store float %mul497, float* %arrayidx498, align 4
  %605 = load float*, float** %b1.addr, align 4
  %arrayidx499 = getelementptr inbounds float, float* %605, i32 4
  %606 = load float, float* %arrayidx499, align 4
  %607 = load float*, float** %b1.addr, align 4
  %arrayidx500 = getelementptr inbounds float, float* %607, i32 7
  %608 = load float, float* %arrayidx500, align 4
  %add501 = fadd float %606, %608
  %609 = load float*, float** %b2.addr, align 4
  %arrayidx502 = getelementptr inbounds float, float* %609, i32 4
  store float %add501, float* %arrayidx502, align 4
  %610 = load float*, float** %b1.addr, align 4
  %arrayidx503 = getelementptr inbounds float, float* %610, i32 7
  %611 = load float, float* %arrayidx503, align 4
  %612 = load float*, float** %b1.addr, align 4
  %arrayidx504 = getelementptr inbounds float, float* %612, i32 4
  %613 = load float, float* %arrayidx504, align 4
  %sub505 = fsub float %611, %613
  %614 = load float, float* %cos0, align 4
  %mul506 = fmul float %sub505, %614
  %615 = load float*, float** %b2.addr, align 4
  %arrayidx507 = getelementptr inbounds float, float* %615, i32 7
  store float %mul506, float* %arrayidx507, align 4
  %616 = load float*, float** %b1.addr, align 4
  %arrayidx508 = getelementptr inbounds float, float* %616, i32 5
  %617 = load float, float* %arrayidx508, align 4
  %618 = load float*, float** %b1.addr, align 4
  %arrayidx509 = getelementptr inbounds float, float* %618, i32 6
  %619 = load float, float* %arrayidx509, align 4
  %add510 = fadd float %617, %619
  %620 = load float*, float** %b2.addr, align 4
  %arrayidx511 = getelementptr inbounds float, float* %620, i32 5
  store float %add510, float* %arrayidx511, align 4
  %621 = load float*, float** %b1.addr, align 4
  %arrayidx512 = getelementptr inbounds float, float* %621, i32 6
  %622 = load float, float* %arrayidx512, align 4
  %623 = load float*, float** %b1.addr, align 4
  %arrayidx513 = getelementptr inbounds float, float* %623, i32 5
  %624 = load float, float* %arrayidx513, align 4
  %sub514 = fsub float %622, %624
  %625 = load float, float* %cos1, align 4
  %mul515 = fmul float %sub514, %625
  %626 = load float*, float** %b2.addr, align 4
  %arrayidx516 = getelementptr inbounds float, float* %626, i32 6
  store float %mul515, float* %arrayidx516, align 4
  %627 = load float*, float** %b1.addr, align 4
  %arrayidx517 = getelementptr inbounds float, float* %627, i32 8
  %628 = load float, float* %arrayidx517, align 4
  %629 = load float*, float** %b1.addr, align 4
  %arrayidx518 = getelementptr inbounds float, float* %629, i32 11
  %630 = load float, float* %arrayidx518, align 4
  %add519 = fadd float %628, %630
  %631 = load float*, float** %b2.addr, align 4
  %arrayidx520 = getelementptr inbounds float, float* %631, i32 8
  store float %add519, float* %arrayidx520, align 4
  %632 = load float*, float** %b1.addr, align 4
  %arrayidx521 = getelementptr inbounds float, float* %632, i32 8
  %633 = load float, float* %arrayidx521, align 4
  %634 = load float*, float** %b1.addr, align 4
  %arrayidx522 = getelementptr inbounds float, float* %634, i32 11
  %635 = load float, float* %arrayidx522, align 4
  %sub523 = fsub float %633, %635
  %636 = load float, float* %cos0, align 4
  %mul524 = fmul float %sub523, %636
  %637 = load float*, float** %b2.addr, align 4
  %arrayidx525 = getelementptr inbounds float, float* %637, i32 11
  store float %mul524, float* %arrayidx525, align 4
  %638 = load float*, float** %b1.addr, align 4
  %arrayidx526 = getelementptr inbounds float, float* %638, i32 9
  %639 = load float, float* %arrayidx526, align 4
  %640 = load float*, float** %b1.addr, align 4
  %arrayidx527 = getelementptr inbounds float, float* %640, i32 10
  %641 = load float, float* %arrayidx527, align 4
  %add528 = fadd float %639, %641
  %642 = load float*, float** %b2.addr, align 4
  %arrayidx529 = getelementptr inbounds float, float* %642, i32 9
  store float %add528, float* %arrayidx529, align 4
  %643 = load float*, float** %b1.addr, align 4
  %arrayidx530 = getelementptr inbounds float, float* %643, i32 9
  %644 = load float, float* %arrayidx530, align 4
  %645 = load float*, float** %b1.addr, align 4
  %arrayidx531 = getelementptr inbounds float, float* %645, i32 10
  %646 = load float, float* %arrayidx531, align 4
  %sub532 = fsub float %644, %646
  %647 = load float, float* %cos1, align 4
  %mul533 = fmul float %sub532, %647
  %648 = load float*, float** %b2.addr, align 4
  %arrayidx534 = getelementptr inbounds float, float* %648, i32 10
  store float %mul533, float* %arrayidx534, align 4
  %649 = load float*, float** %b1.addr, align 4
  %arrayidx535 = getelementptr inbounds float, float* %649, i32 12
  %650 = load float, float* %arrayidx535, align 4
  %651 = load float*, float** %b1.addr, align 4
  %arrayidx536 = getelementptr inbounds float, float* %651, i32 15
  %652 = load float, float* %arrayidx536, align 4
  %add537 = fadd float %650, %652
  %653 = load float*, float** %b2.addr, align 4
  %arrayidx538 = getelementptr inbounds float, float* %653, i32 12
  store float %add537, float* %arrayidx538, align 4
  %654 = load float*, float** %b1.addr, align 4
  %arrayidx539 = getelementptr inbounds float, float* %654, i32 15
  %655 = load float, float* %arrayidx539, align 4
  %656 = load float*, float** %b1.addr, align 4
  %arrayidx540 = getelementptr inbounds float, float* %656, i32 12
  %657 = load float, float* %arrayidx540, align 4
  %sub541 = fsub float %655, %657
  %658 = load float, float* %cos0, align 4
  %mul542 = fmul float %sub541, %658
  %659 = load float*, float** %b2.addr, align 4
  %arrayidx543 = getelementptr inbounds float, float* %659, i32 15
  store float %mul542, float* %arrayidx543, align 4
  %660 = load float*, float** %b1.addr, align 4
  %arrayidx544 = getelementptr inbounds float, float* %660, i32 13
  %661 = load float, float* %arrayidx544, align 4
  %662 = load float*, float** %b1.addr, align 4
  %arrayidx545 = getelementptr inbounds float, float* %662, i32 14
  %663 = load float, float* %arrayidx545, align 4
  %add546 = fadd float %661, %663
  %664 = load float*, float** %b2.addr, align 4
  %arrayidx547 = getelementptr inbounds float, float* %664, i32 13
  store float %add546, float* %arrayidx547, align 4
  %665 = load float*, float** %b1.addr, align 4
  %arrayidx548 = getelementptr inbounds float, float* %665, i32 14
  %666 = load float, float* %arrayidx548, align 4
  %667 = load float*, float** %b1.addr, align 4
  %arrayidx549 = getelementptr inbounds float, float* %667, i32 13
  %668 = load float, float* %arrayidx549, align 4
  %sub550 = fsub float %666, %668
  %669 = load float, float* %cos1, align 4
  %mul551 = fmul float %sub550, %669
  %670 = load float*, float** %b2.addr, align 4
  %arrayidx552 = getelementptr inbounds float, float* %670, i32 14
  store float %mul551, float* %arrayidx552, align 4
  %671 = load float*, float** %b1.addr, align 4
  %arrayidx553 = getelementptr inbounds float, float* %671, i32 16
  %672 = load float, float* %arrayidx553, align 4
  %673 = load float*, float** %b1.addr, align 4
  %arrayidx554 = getelementptr inbounds float, float* %673, i32 19
  %674 = load float, float* %arrayidx554, align 4
  %add555 = fadd float %672, %674
  %675 = load float*, float** %b2.addr, align 4
  %arrayidx556 = getelementptr inbounds float, float* %675, i32 16
  store float %add555, float* %arrayidx556, align 4
  %676 = load float*, float** %b1.addr, align 4
  %arrayidx557 = getelementptr inbounds float, float* %676, i32 16
  %677 = load float, float* %arrayidx557, align 4
  %678 = load float*, float** %b1.addr, align 4
  %arrayidx558 = getelementptr inbounds float, float* %678, i32 19
  %679 = load float, float* %arrayidx558, align 4
  %sub559 = fsub float %677, %679
  %680 = load float, float* %cos0, align 4
  %mul560 = fmul float %sub559, %680
  %681 = load float*, float** %b2.addr, align 4
  %arrayidx561 = getelementptr inbounds float, float* %681, i32 19
  store float %mul560, float* %arrayidx561, align 4
  %682 = load float*, float** %b1.addr, align 4
  %arrayidx562 = getelementptr inbounds float, float* %682, i32 17
  %683 = load float, float* %arrayidx562, align 4
  %684 = load float*, float** %b1.addr, align 4
  %arrayidx563 = getelementptr inbounds float, float* %684, i32 18
  %685 = load float, float* %arrayidx563, align 4
  %add564 = fadd float %683, %685
  %686 = load float*, float** %b2.addr, align 4
  %arrayidx565 = getelementptr inbounds float, float* %686, i32 17
  store float %add564, float* %arrayidx565, align 4
  %687 = load float*, float** %b1.addr, align 4
  %arrayidx566 = getelementptr inbounds float, float* %687, i32 17
  %688 = load float, float* %arrayidx566, align 4
  %689 = load float*, float** %b1.addr, align 4
  %arrayidx567 = getelementptr inbounds float, float* %689, i32 18
  %690 = load float, float* %arrayidx567, align 4
  %sub568 = fsub float %688, %690
  %691 = load float, float* %cos1, align 4
  %mul569 = fmul float %sub568, %691
  %692 = load float*, float** %b2.addr, align 4
  %arrayidx570 = getelementptr inbounds float, float* %692, i32 18
  store float %mul569, float* %arrayidx570, align 4
  %693 = load float*, float** %b1.addr, align 4
  %arrayidx571 = getelementptr inbounds float, float* %693, i32 20
  %694 = load float, float* %arrayidx571, align 4
  %695 = load float*, float** %b1.addr, align 4
  %arrayidx572 = getelementptr inbounds float, float* %695, i32 23
  %696 = load float, float* %arrayidx572, align 4
  %add573 = fadd float %694, %696
  %697 = load float*, float** %b2.addr, align 4
  %arrayidx574 = getelementptr inbounds float, float* %697, i32 20
  store float %add573, float* %arrayidx574, align 4
  %698 = load float*, float** %b1.addr, align 4
  %arrayidx575 = getelementptr inbounds float, float* %698, i32 23
  %699 = load float, float* %arrayidx575, align 4
  %700 = load float*, float** %b1.addr, align 4
  %arrayidx576 = getelementptr inbounds float, float* %700, i32 20
  %701 = load float, float* %arrayidx576, align 4
  %sub577 = fsub float %699, %701
  %702 = load float, float* %cos0, align 4
  %mul578 = fmul float %sub577, %702
  %703 = load float*, float** %b2.addr, align 4
  %arrayidx579 = getelementptr inbounds float, float* %703, i32 23
  store float %mul578, float* %arrayidx579, align 4
  %704 = load float*, float** %b1.addr, align 4
  %arrayidx580 = getelementptr inbounds float, float* %704, i32 21
  %705 = load float, float* %arrayidx580, align 4
  %706 = load float*, float** %b1.addr, align 4
  %arrayidx581 = getelementptr inbounds float, float* %706, i32 22
  %707 = load float, float* %arrayidx581, align 4
  %add582 = fadd float %705, %707
  %708 = load float*, float** %b2.addr, align 4
  %arrayidx583 = getelementptr inbounds float, float* %708, i32 21
  store float %add582, float* %arrayidx583, align 4
  %709 = load float*, float** %b1.addr, align 4
  %arrayidx584 = getelementptr inbounds float, float* %709, i32 22
  %710 = load float, float* %arrayidx584, align 4
  %711 = load float*, float** %b1.addr, align 4
  %arrayidx585 = getelementptr inbounds float, float* %711, i32 21
  %712 = load float, float* %arrayidx585, align 4
  %sub586 = fsub float %710, %712
  %713 = load float, float* %cos1, align 4
  %mul587 = fmul float %sub586, %713
  %714 = load float*, float** %b2.addr, align 4
  %arrayidx588 = getelementptr inbounds float, float* %714, i32 22
  store float %mul587, float* %arrayidx588, align 4
  %715 = load float*, float** %b1.addr, align 4
  %arrayidx589 = getelementptr inbounds float, float* %715, i32 24
  %716 = load float, float* %arrayidx589, align 4
  %717 = load float*, float** %b1.addr, align 4
  %arrayidx590 = getelementptr inbounds float, float* %717, i32 27
  %718 = load float, float* %arrayidx590, align 4
  %add591 = fadd float %716, %718
  %719 = load float*, float** %b2.addr, align 4
  %arrayidx592 = getelementptr inbounds float, float* %719, i32 24
  store float %add591, float* %arrayidx592, align 4
  %720 = load float*, float** %b1.addr, align 4
  %arrayidx593 = getelementptr inbounds float, float* %720, i32 24
  %721 = load float, float* %arrayidx593, align 4
  %722 = load float*, float** %b1.addr, align 4
  %arrayidx594 = getelementptr inbounds float, float* %722, i32 27
  %723 = load float, float* %arrayidx594, align 4
  %sub595 = fsub float %721, %723
  %724 = load float, float* %cos0, align 4
  %mul596 = fmul float %sub595, %724
  %725 = load float*, float** %b2.addr, align 4
  %arrayidx597 = getelementptr inbounds float, float* %725, i32 27
  store float %mul596, float* %arrayidx597, align 4
  %726 = load float*, float** %b1.addr, align 4
  %arrayidx598 = getelementptr inbounds float, float* %726, i32 25
  %727 = load float, float* %arrayidx598, align 4
  %728 = load float*, float** %b1.addr, align 4
  %arrayidx599 = getelementptr inbounds float, float* %728, i32 26
  %729 = load float, float* %arrayidx599, align 4
  %add600 = fadd float %727, %729
  %730 = load float*, float** %b2.addr, align 4
  %arrayidx601 = getelementptr inbounds float, float* %730, i32 25
  store float %add600, float* %arrayidx601, align 4
  %731 = load float*, float** %b1.addr, align 4
  %arrayidx602 = getelementptr inbounds float, float* %731, i32 25
  %732 = load float, float* %arrayidx602, align 4
  %733 = load float*, float** %b1.addr, align 4
  %arrayidx603 = getelementptr inbounds float, float* %733, i32 26
  %734 = load float, float* %arrayidx603, align 4
  %sub604 = fsub float %732, %734
  %735 = load float, float* %cos1, align 4
  %mul605 = fmul float %sub604, %735
  %736 = load float*, float** %b2.addr, align 4
  %arrayidx606 = getelementptr inbounds float, float* %736, i32 26
  store float %mul605, float* %arrayidx606, align 4
  %737 = load float*, float** %b1.addr, align 4
  %arrayidx607 = getelementptr inbounds float, float* %737, i32 28
  %738 = load float, float* %arrayidx607, align 4
  %739 = load float*, float** %b1.addr, align 4
  %arrayidx608 = getelementptr inbounds float, float* %739, i32 31
  %740 = load float, float* %arrayidx608, align 4
  %add609 = fadd float %738, %740
  %741 = load float*, float** %b2.addr, align 4
  %arrayidx610 = getelementptr inbounds float, float* %741, i32 28
  store float %add609, float* %arrayidx610, align 4
  %742 = load float*, float** %b1.addr, align 4
  %arrayidx611 = getelementptr inbounds float, float* %742, i32 31
  %743 = load float, float* %arrayidx611, align 4
  %744 = load float*, float** %b1.addr, align 4
  %arrayidx612 = getelementptr inbounds float, float* %744, i32 28
  %745 = load float, float* %arrayidx612, align 4
  %sub613 = fsub float %743, %745
  %746 = load float, float* %cos0, align 4
  %mul614 = fmul float %sub613, %746
  %747 = load float*, float** %b2.addr, align 4
  %arrayidx615 = getelementptr inbounds float, float* %747, i32 31
  store float %mul614, float* %arrayidx615, align 4
  %748 = load float*, float** %b1.addr, align 4
  %arrayidx616 = getelementptr inbounds float, float* %748, i32 29
  %749 = load float, float* %arrayidx616, align 4
  %750 = load float*, float** %b1.addr, align 4
  %arrayidx617 = getelementptr inbounds float, float* %750, i32 30
  %751 = load float, float* %arrayidx617, align 4
  %add618 = fadd float %749, %751
  %752 = load float*, float** %b2.addr, align 4
  %arrayidx619 = getelementptr inbounds float, float* %752, i32 29
  store float %add618, float* %arrayidx619, align 4
  %753 = load float*, float** %b1.addr, align 4
  %arrayidx620 = getelementptr inbounds float, float* %753, i32 30
  %754 = load float, float* %arrayidx620, align 4
  %755 = load float*, float** %b1.addr, align 4
  %arrayidx621 = getelementptr inbounds float, float* %755, i32 29
  %756 = load float, float* %arrayidx621, align 4
  %sub622 = fsub float %754, %756
  %757 = load float, float* %cos1, align 4
  %mul623 = fmul float %sub622, %757
  %758 = load float*, float** %b2.addr, align 4
  %arrayidx624 = getelementptr inbounds float, float* %758, i32 30
  store float %mul623, float* %arrayidx624, align 4
  %759 = load float*, float** getelementptr inbounds ([5 x float*], [5 x float*]* @pnts, i32 0, i32 4), align 16
  %arrayidx626 = getelementptr inbounds float, float* %759, i32 0
  %760 = load float, float* %arrayidx626, align 4
  store float %760, float* %cos0625, align 4
  %761 = load float*, float** %b2.addr, align 4
  %arrayidx627 = getelementptr inbounds float, float* %761, i32 0
  %762 = load float, float* %arrayidx627, align 4
  %763 = load float*, float** %b2.addr, align 4
  %arrayidx628 = getelementptr inbounds float, float* %763, i32 1
  %764 = load float, float* %arrayidx628, align 4
  %add629 = fadd float %762, %764
  %765 = load float*, float** %b1.addr, align 4
  %arrayidx630 = getelementptr inbounds float, float* %765, i32 0
  store float %add629, float* %arrayidx630, align 4
  %766 = load float*, float** %b2.addr, align 4
  %arrayidx631 = getelementptr inbounds float, float* %766, i32 0
  %767 = load float, float* %arrayidx631, align 4
  %768 = load float*, float** %b2.addr, align 4
  %arrayidx632 = getelementptr inbounds float, float* %768, i32 1
  %769 = load float, float* %arrayidx632, align 4
  %sub633 = fsub float %767, %769
  %770 = load float, float* %cos0625, align 4
  %mul634 = fmul float %sub633, %770
  %771 = load float*, float** %b1.addr, align 4
  %arrayidx635 = getelementptr inbounds float, float* %771, i32 1
  store float %mul634, float* %arrayidx635, align 4
  %772 = load float*, float** %b2.addr, align 4
  %arrayidx636 = getelementptr inbounds float, float* %772, i32 2
  %773 = load float, float* %arrayidx636, align 4
  %774 = load float*, float** %b2.addr, align 4
  %arrayidx637 = getelementptr inbounds float, float* %774, i32 3
  %775 = load float, float* %arrayidx637, align 4
  %add638 = fadd float %773, %775
  %776 = load float*, float** %b1.addr, align 4
  %arrayidx639 = getelementptr inbounds float, float* %776, i32 2
  store float %add638, float* %arrayidx639, align 4
  %777 = load float*, float** %b2.addr, align 4
  %arrayidx640 = getelementptr inbounds float, float* %777, i32 3
  %778 = load float, float* %arrayidx640, align 4
  %779 = load float*, float** %b2.addr, align 4
  %arrayidx641 = getelementptr inbounds float, float* %779, i32 2
  %780 = load float, float* %arrayidx641, align 4
  %sub642 = fsub float %778, %780
  %781 = load float, float* %cos0625, align 4
  %mul643 = fmul float %sub642, %781
  %782 = load float*, float** %b1.addr, align 4
  %arrayidx644 = getelementptr inbounds float, float* %782, i32 3
  store float %mul643, float* %arrayidx644, align 4
  %783 = load float*, float** %b1.addr, align 4
  %arrayidx645 = getelementptr inbounds float, float* %783, i32 3
  %784 = load float, float* %arrayidx645, align 4
  %785 = load float*, float** %b1.addr, align 4
  %arrayidx646 = getelementptr inbounds float, float* %785, i32 2
  %786 = load float, float* %arrayidx646, align 4
  %add647 = fadd float %786, %784
  store float %add647, float* %arrayidx646, align 4
  %787 = load float*, float** %b2.addr, align 4
  %arrayidx648 = getelementptr inbounds float, float* %787, i32 4
  %788 = load float, float* %arrayidx648, align 4
  %789 = load float*, float** %b2.addr, align 4
  %arrayidx649 = getelementptr inbounds float, float* %789, i32 5
  %790 = load float, float* %arrayidx649, align 4
  %add650 = fadd float %788, %790
  %791 = load float*, float** %b1.addr, align 4
  %arrayidx651 = getelementptr inbounds float, float* %791, i32 4
  store float %add650, float* %arrayidx651, align 4
  %792 = load float*, float** %b2.addr, align 4
  %arrayidx652 = getelementptr inbounds float, float* %792, i32 4
  %793 = load float, float* %arrayidx652, align 4
  %794 = load float*, float** %b2.addr, align 4
  %arrayidx653 = getelementptr inbounds float, float* %794, i32 5
  %795 = load float, float* %arrayidx653, align 4
  %sub654 = fsub float %793, %795
  %796 = load float, float* %cos0625, align 4
  %mul655 = fmul float %sub654, %796
  %797 = load float*, float** %b1.addr, align 4
  %arrayidx656 = getelementptr inbounds float, float* %797, i32 5
  store float %mul655, float* %arrayidx656, align 4
  %798 = load float*, float** %b2.addr, align 4
  %arrayidx657 = getelementptr inbounds float, float* %798, i32 6
  %799 = load float, float* %arrayidx657, align 4
  %800 = load float*, float** %b2.addr, align 4
  %arrayidx658 = getelementptr inbounds float, float* %800, i32 7
  %801 = load float, float* %arrayidx658, align 4
  %add659 = fadd float %799, %801
  %802 = load float*, float** %b1.addr, align 4
  %arrayidx660 = getelementptr inbounds float, float* %802, i32 6
  store float %add659, float* %arrayidx660, align 4
  %803 = load float*, float** %b2.addr, align 4
  %arrayidx661 = getelementptr inbounds float, float* %803, i32 7
  %804 = load float, float* %arrayidx661, align 4
  %805 = load float*, float** %b2.addr, align 4
  %arrayidx662 = getelementptr inbounds float, float* %805, i32 6
  %806 = load float, float* %arrayidx662, align 4
  %sub663 = fsub float %804, %806
  %807 = load float, float* %cos0625, align 4
  %mul664 = fmul float %sub663, %807
  %808 = load float*, float** %b1.addr, align 4
  %arrayidx665 = getelementptr inbounds float, float* %808, i32 7
  store float %mul664, float* %arrayidx665, align 4
  %809 = load float*, float** %b1.addr, align 4
  %arrayidx666 = getelementptr inbounds float, float* %809, i32 7
  %810 = load float, float* %arrayidx666, align 4
  %811 = load float*, float** %b1.addr, align 4
  %arrayidx667 = getelementptr inbounds float, float* %811, i32 6
  %812 = load float, float* %arrayidx667, align 4
  %add668 = fadd float %812, %810
  store float %add668, float* %arrayidx667, align 4
  %813 = load float*, float** %b1.addr, align 4
  %arrayidx669 = getelementptr inbounds float, float* %813, i32 6
  %814 = load float, float* %arrayidx669, align 4
  %815 = load float*, float** %b1.addr, align 4
  %arrayidx670 = getelementptr inbounds float, float* %815, i32 4
  %816 = load float, float* %arrayidx670, align 4
  %add671 = fadd float %816, %814
  store float %add671, float* %arrayidx670, align 4
  %817 = load float*, float** %b1.addr, align 4
  %arrayidx672 = getelementptr inbounds float, float* %817, i32 5
  %818 = load float, float* %arrayidx672, align 4
  %819 = load float*, float** %b1.addr, align 4
  %arrayidx673 = getelementptr inbounds float, float* %819, i32 6
  %820 = load float, float* %arrayidx673, align 4
  %add674 = fadd float %820, %818
  store float %add674, float* %arrayidx673, align 4
  %821 = load float*, float** %b1.addr, align 4
  %arrayidx675 = getelementptr inbounds float, float* %821, i32 7
  %822 = load float, float* %arrayidx675, align 4
  %823 = load float*, float** %b1.addr, align 4
  %arrayidx676 = getelementptr inbounds float, float* %823, i32 5
  %824 = load float, float* %arrayidx676, align 4
  %add677 = fadd float %824, %822
  store float %add677, float* %arrayidx676, align 4
  %825 = load float*, float** %b2.addr, align 4
  %arrayidx678 = getelementptr inbounds float, float* %825, i32 8
  %826 = load float, float* %arrayidx678, align 4
  %827 = load float*, float** %b2.addr, align 4
  %arrayidx679 = getelementptr inbounds float, float* %827, i32 9
  %828 = load float, float* %arrayidx679, align 4
  %add680 = fadd float %826, %828
  %829 = load float*, float** %b1.addr, align 4
  %arrayidx681 = getelementptr inbounds float, float* %829, i32 8
  store float %add680, float* %arrayidx681, align 4
  %830 = load float*, float** %b2.addr, align 4
  %arrayidx682 = getelementptr inbounds float, float* %830, i32 8
  %831 = load float, float* %arrayidx682, align 4
  %832 = load float*, float** %b2.addr, align 4
  %arrayidx683 = getelementptr inbounds float, float* %832, i32 9
  %833 = load float, float* %arrayidx683, align 4
  %sub684 = fsub float %831, %833
  %834 = load float, float* %cos0625, align 4
  %mul685 = fmul float %sub684, %834
  %835 = load float*, float** %b1.addr, align 4
  %arrayidx686 = getelementptr inbounds float, float* %835, i32 9
  store float %mul685, float* %arrayidx686, align 4
  %836 = load float*, float** %b2.addr, align 4
  %arrayidx687 = getelementptr inbounds float, float* %836, i32 10
  %837 = load float, float* %arrayidx687, align 4
  %838 = load float*, float** %b2.addr, align 4
  %arrayidx688 = getelementptr inbounds float, float* %838, i32 11
  %839 = load float, float* %arrayidx688, align 4
  %add689 = fadd float %837, %839
  %840 = load float*, float** %b1.addr, align 4
  %arrayidx690 = getelementptr inbounds float, float* %840, i32 10
  store float %add689, float* %arrayidx690, align 4
  %841 = load float*, float** %b2.addr, align 4
  %arrayidx691 = getelementptr inbounds float, float* %841, i32 11
  %842 = load float, float* %arrayidx691, align 4
  %843 = load float*, float** %b2.addr, align 4
  %arrayidx692 = getelementptr inbounds float, float* %843, i32 10
  %844 = load float, float* %arrayidx692, align 4
  %sub693 = fsub float %842, %844
  %845 = load float, float* %cos0625, align 4
  %mul694 = fmul float %sub693, %845
  %846 = load float*, float** %b1.addr, align 4
  %arrayidx695 = getelementptr inbounds float, float* %846, i32 11
  store float %mul694, float* %arrayidx695, align 4
  %847 = load float*, float** %b1.addr, align 4
  %arrayidx696 = getelementptr inbounds float, float* %847, i32 11
  %848 = load float, float* %arrayidx696, align 4
  %849 = load float*, float** %b1.addr, align 4
  %arrayidx697 = getelementptr inbounds float, float* %849, i32 10
  %850 = load float, float* %arrayidx697, align 4
  %add698 = fadd float %850, %848
  store float %add698, float* %arrayidx697, align 4
  %851 = load float*, float** %b2.addr, align 4
  %arrayidx699 = getelementptr inbounds float, float* %851, i32 12
  %852 = load float, float* %arrayidx699, align 4
  %853 = load float*, float** %b2.addr, align 4
  %arrayidx700 = getelementptr inbounds float, float* %853, i32 13
  %854 = load float, float* %arrayidx700, align 4
  %add701 = fadd float %852, %854
  %855 = load float*, float** %b1.addr, align 4
  %arrayidx702 = getelementptr inbounds float, float* %855, i32 12
  store float %add701, float* %arrayidx702, align 4
  %856 = load float*, float** %b2.addr, align 4
  %arrayidx703 = getelementptr inbounds float, float* %856, i32 12
  %857 = load float, float* %arrayidx703, align 4
  %858 = load float*, float** %b2.addr, align 4
  %arrayidx704 = getelementptr inbounds float, float* %858, i32 13
  %859 = load float, float* %arrayidx704, align 4
  %sub705 = fsub float %857, %859
  %860 = load float, float* %cos0625, align 4
  %mul706 = fmul float %sub705, %860
  %861 = load float*, float** %b1.addr, align 4
  %arrayidx707 = getelementptr inbounds float, float* %861, i32 13
  store float %mul706, float* %arrayidx707, align 4
  %862 = load float*, float** %b2.addr, align 4
  %arrayidx708 = getelementptr inbounds float, float* %862, i32 14
  %863 = load float, float* %arrayidx708, align 4
  %864 = load float*, float** %b2.addr, align 4
  %arrayidx709 = getelementptr inbounds float, float* %864, i32 15
  %865 = load float, float* %arrayidx709, align 4
  %add710 = fadd float %863, %865
  %866 = load float*, float** %b1.addr, align 4
  %arrayidx711 = getelementptr inbounds float, float* %866, i32 14
  store float %add710, float* %arrayidx711, align 4
  %867 = load float*, float** %b2.addr, align 4
  %arrayidx712 = getelementptr inbounds float, float* %867, i32 15
  %868 = load float, float* %arrayidx712, align 4
  %869 = load float*, float** %b2.addr, align 4
  %arrayidx713 = getelementptr inbounds float, float* %869, i32 14
  %870 = load float, float* %arrayidx713, align 4
  %sub714 = fsub float %868, %870
  %871 = load float, float* %cos0625, align 4
  %mul715 = fmul float %sub714, %871
  %872 = load float*, float** %b1.addr, align 4
  %arrayidx716 = getelementptr inbounds float, float* %872, i32 15
  store float %mul715, float* %arrayidx716, align 4
  %873 = load float*, float** %b1.addr, align 4
  %arrayidx717 = getelementptr inbounds float, float* %873, i32 15
  %874 = load float, float* %arrayidx717, align 4
  %875 = load float*, float** %b1.addr, align 4
  %arrayidx718 = getelementptr inbounds float, float* %875, i32 14
  %876 = load float, float* %arrayidx718, align 4
  %add719 = fadd float %876, %874
  store float %add719, float* %arrayidx718, align 4
  %877 = load float*, float** %b1.addr, align 4
  %arrayidx720 = getelementptr inbounds float, float* %877, i32 14
  %878 = load float, float* %arrayidx720, align 4
  %879 = load float*, float** %b1.addr, align 4
  %arrayidx721 = getelementptr inbounds float, float* %879, i32 12
  %880 = load float, float* %arrayidx721, align 4
  %add722 = fadd float %880, %878
  store float %add722, float* %arrayidx721, align 4
  %881 = load float*, float** %b1.addr, align 4
  %arrayidx723 = getelementptr inbounds float, float* %881, i32 13
  %882 = load float, float* %arrayidx723, align 4
  %883 = load float*, float** %b1.addr, align 4
  %arrayidx724 = getelementptr inbounds float, float* %883, i32 14
  %884 = load float, float* %arrayidx724, align 4
  %add725 = fadd float %884, %882
  store float %add725, float* %arrayidx724, align 4
  %885 = load float*, float** %b1.addr, align 4
  %arrayidx726 = getelementptr inbounds float, float* %885, i32 15
  %886 = load float, float* %arrayidx726, align 4
  %887 = load float*, float** %b1.addr, align 4
  %arrayidx727 = getelementptr inbounds float, float* %887, i32 13
  %888 = load float, float* %arrayidx727, align 4
  %add728 = fadd float %888, %886
  store float %add728, float* %arrayidx727, align 4
  %889 = load float*, float** %b2.addr, align 4
  %arrayidx729 = getelementptr inbounds float, float* %889, i32 16
  %890 = load float, float* %arrayidx729, align 4
  %891 = load float*, float** %b2.addr, align 4
  %arrayidx730 = getelementptr inbounds float, float* %891, i32 17
  %892 = load float, float* %arrayidx730, align 4
  %add731 = fadd float %890, %892
  %893 = load float*, float** %b1.addr, align 4
  %arrayidx732 = getelementptr inbounds float, float* %893, i32 16
  store float %add731, float* %arrayidx732, align 4
  %894 = load float*, float** %b2.addr, align 4
  %arrayidx733 = getelementptr inbounds float, float* %894, i32 16
  %895 = load float, float* %arrayidx733, align 4
  %896 = load float*, float** %b2.addr, align 4
  %arrayidx734 = getelementptr inbounds float, float* %896, i32 17
  %897 = load float, float* %arrayidx734, align 4
  %sub735 = fsub float %895, %897
  %898 = load float, float* %cos0625, align 4
  %mul736 = fmul float %sub735, %898
  %899 = load float*, float** %b1.addr, align 4
  %arrayidx737 = getelementptr inbounds float, float* %899, i32 17
  store float %mul736, float* %arrayidx737, align 4
  %900 = load float*, float** %b2.addr, align 4
  %arrayidx738 = getelementptr inbounds float, float* %900, i32 18
  %901 = load float, float* %arrayidx738, align 4
  %902 = load float*, float** %b2.addr, align 4
  %arrayidx739 = getelementptr inbounds float, float* %902, i32 19
  %903 = load float, float* %arrayidx739, align 4
  %add740 = fadd float %901, %903
  %904 = load float*, float** %b1.addr, align 4
  %arrayidx741 = getelementptr inbounds float, float* %904, i32 18
  store float %add740, float* %arrayidx741, align 4
  %905 = load float*, float** %b2.addr, align 4
  %arrayidx742 = getelementptr inbounds float, float* %905, i32 19
  %906 = load float, float* %arrayidx742, align 4
  %907 = load float*, float** %b2.addr, align 4
  %arrayidx743 = getelementptr inbounds float, float* %907, i32 18
  %908 = load float, float* %arrayidx743, align 4
  %sub744 = fsub float %906, %908
  %909 = load float, float* %cos0625, align 4
  %mul745 = fmul float %sub744, %909
  %910 = load float*, float** %b1.addr, align 4
  %arrayidx746 = getelementptr inbounds float, float* %910, i32 19
  store float %mul745, float* %arrayidx746, align 4
  %911 = load float*, float** %b1.addr, align 4
  %arrayidx747 = getelementptr inbounds float, float* %911, i32 19
  %912 = load float, float* %arrayidx747, align 4
  %913 = load float*, float** %b1.addr, align 4
  %arrayidx748 = getelementptr inbounds float, float* %913, i32 18
  %914 = load float, float* %arrayidx748, align 4
  %add749 = fadd float %914, %912
  store float %add749, float* %arrayidx748, align 4
  %915 = load float*, float** %b2.addr, align 4
  %arrayidx750 = getelementptr inbounds float, float* %915, i32 20
  %916 = load float, float* %arrayidx750, align 4
  %917 = load float*, float** %b2.addr, align 4
  %arrayidx751 = getelementptr inbounds float, float* %917, i32 21
  %918 = load float, float* %arrayidx751, align 4
  %add752 = fadd float %916, %918
  %919 = load float*, float** %b1.addr, align 4
  %arrayidx753 = getelementptr inbounds float, float* %919, i32 20
  store float %add752, float* %arrayidx753, align 4
  %920 = load float*, float** %b2.addr, align 4
  %arrayidx754 = getelementptr inbounds float, float* %920, i32 20
  %921 = load float, float* %arrayidx754, align 4
  %922 = load float*, float** %b2.addr, align 4
  %arrayidx755 = getelementptr inbounds float, float* %922, i32 21
  %923 = load float, float* %arrayidx755, align 4
  %sub756 = fsub float %921, %923
  %924 = load float, float* %cos0625, align 4
  %mul757 = fmul float %sub756, %924
  %925 = load float*, float** %b1.addr, align 4
  %arrayidx758 = getelementptr inbounds float, float* %925, i32 21
  store float %mul757, float* %arrayidx758, align 4
  %926 = load float*, float** %b2.addr, align 4
  %arrayidx759 = getelementptr inbounds float, float* %926, i32 22
  %927 = load float, float* %arrayidx759, align 4
  %928 = load float*, float** %b2.addr, align 4
  %arrayidx760 = getelementptr inbounds float, float* %928, i32 23
  %929 = load float, float* %arrayidx760, align 4
  %add761 = fadd float %927, %929
  %930 = load float*, float** %b1.addr, align 4
  %arrayidx762 = getelementptr inbounds float, float* %930, i32 22
  store float %add761, float* %arrayidx762, align 4
  %931 = load float*, float** %b2.addr, align 4
  %arrayidx763 = getelementptr inbounds float, float* %931, i32 23
  %932 = load float, float* %arrayidx763, align 4
  %933 = load float*, float** %b2.addr, align 4
  %arrayidx764 = getelementptr inbounds float, float* %933, i32 22
  %934 = load float, float* %arrayidx764, align 4
  %sub765 = fsub float %932, %934
  %935 = load float, float* %cos0625, align 4
  %mul766 = fmul float %sub765, %935
  %936 = load float*, float** %b1.addr, align 4
  %arrayidx767 = getelementptr inbounds float, float* %936, i32 23
  store float %mul766, float* %arrayidx767, align 4
  %937 = load float*, float** %b1.addr, align 4
  %arrayidx768 = getelementptr inbounds float, float* %937, i32 23
  %938 = load float, float* %arrayidx768, align 4
  %939 = load float*, float** %b1.addr, align 4
  %arrayidx769 = getelementptr inbounds float, float* %939, i32 22
  %940 = load float, float* %arrayidx769, align 4
  %add770 = fadd float %940, %938
  store float %add770, float* %arrayidx769, align 4
  %941 = load float*, float** %b1.addr, align 4
  %arrayidx771 = getelementptr inbounds float, float* %941, i32 22
  %942 = load float, float* %arrayidx771, align 4
  %943 = load float*, float** %b1.addr, align 4
  %arrayidx772 = getelementptr inbounds float, float* %943, i32 20
  %944 = load float, float* %arrayidx772, align 4
  %add773 = fadd float %944, %942
  store float %add773, float* %arrayidx772, align 4
  %945 = load float*, float** %b1.addr, align 4
  %arrayidx774 = getelementptr inbounds float, float* %945, i32 21
  %946 = load float, float* %arrayidx774, align 4
  %947 = load float*, float** %b1.addr, align 4
  %arrayidx775 = getelementptr inbounds float, float* %947, i32 22
  %948 = load float, float* %arrayidx775, align 4
  %add776 = fadd float %948, %946
  store float %add776, float* %arrayidx775, align 4
  %949 = load float*, float** %b1.addr, align 4
  %arrayidx777 = getelementptr inbounds float, float* %949, i32 23
  %950 = load float, float* %arrayidx777, align 4
  %951 = load float*, float** %b1.addr, align 4
  %arrayidx778 = getelementptr inbounds float, float* %951, i32 21
  %952 = load float, float* %arrayidx778, align 4
  %add779 = fadd float %952, %950
  store float %add779, float* %arrayidx778, align 4
  %953 = load float*, float** %b2.addr, align 4
  %arrayidx780 = getelementptr inbounds float, float* %953, i32 24
  %954 = load float, float* %arrayidx780, align 4
  %955 = load float*, float** %b2.addr, align 4
  %arrayidx781 = getelementptr inbounds float, float* %955, i32 25
  %956 = load float, float* %arrayidx781, align 4
  %add782 = fadd float %954, %956
  %957 = load float*, float** %b1.addr, align 4
  %arrayidx783 = getelementptr inbounds float, float* %957, i32 24
  store float %add782, float* %arrayidx783, align 4
  %958 = load float*, float** %b2.addr, align 4
  %arrayidx784 = getelementptr inbounds float, float* %958, i32 24
  %959 = load float, float* %arrayidx784, align 4
  %960 = load float*, float** %b2.addr, align 4
  %arrayidx785 = getelementptr inbounds float, float* %960, i32 25
  %961 = load float, float* %arrayidx785, align 4
  %sub786 = fsub float %959, %961
  %962 = load float, float* %cos0625, align 4
  %mul787 = fmul float %sub786, %962
  %963 = load float*, float** %b1.addr, align 4
  %arrayidx788 = getelementptr inbounds float, float* %963, i32 25
  store float %mul787, float* %arrayidx788, align 4
  %964 = load float*, float** %b2.addr, align 4
  %arrayidx789 = getelementptr inbounds float, float* %964, i32 26
  %965 = load float, float* %arrayidx789, align 4
  %966 = load float*, float** %b2.addr, align 4
  %arrayidx790 = getelementptr inbounds float, float* %966, i32 27
  %967 = load float, float* %arrayidx790, align 4
  %add791 = fadd float %965, %967
  %968 = load float*, float** %b1.addr, align 4
  %arrayidx792 = getelementptr inbounds float, float* %968, i32 26
  store float %add791, float* %arrayidx792, align 4
  %969 = load float*, float** %b2.addr, align 4
  %arrayidx793 = getelementptr inbounds float, float* %969, i32 27
  %970 = load float, float* %arrayidx793, align 4
  %971 = load float*, float** %b2.addr, align 4
  %arrayidx794 = getelementptr inbounds float, float* %971, i32 26
  %972 = load float, float* %arrayidx794, align 4
  %sub795 = fsub float %970, %972
  %973 = load float, float* %cos0625, align 4
  %mul796 = fmul float %sub795, %973
  %974 = load float*, float** %b1.addr, align 4
  %arrayidx797 = getelementptr inbounds float, float* %974, i32 27
  store float %mul796, float* %arrayidx797, align 4
  %975 = load float*, float** %b1.addr, align 4
  %arrayidx798 = getelementptr inbounds float, float* %975, i32 27
  %976 = load float, float* %arrayidx798, align 4
  %977 = load float*, float** %b1.addr, align 4
  %arrayidx799 = getelementptr inbounds float, float* %977, i32 26
  %978 = load float, float* %arrayidx799, align 4
  %add800 = fadd float %978, %976
  store float %add800, float* %arrayidx799, align 4
  %979 = load float*, float** %b2.addr, align 4
  %arrayidx801 = getelementptr inbounds float, float* %979, i32 28
  %980 = load float, float* %arrayidx801, align 4
  %981 = load float*, float** %b2.addr, align 4
  %arrayidx802 = getelementptr inbounds float, float* %981, i32 29
  %982 = load float, float* %arrayidx802, align 4
  %add803 = fadd float %980, %982
  %983 = load float*, float** %b1.addr, align 4
  %arrayidx804 = getelementptr inbounds float, float* %983, i32 28
  store float %add803, float* %arrayidx804, align 4
  %984 = load float*, float** %b2.addr, align 4
  %arrayidx805 = getelementptr inbounds float, float* %984, i32 28
  %985 = load float, float* %arrayidx805, align 4
  %986 = load float*, float** %b2.addr, align 4
  %arrayidx806 = getelementptr inbounds float, float* %986, i32 29
  %987 = load float, float* %arrayidx806, align 4
  %sub807 = fsub float %985, %987
  %988 = load float, float* %cos0625, align 4
  %mul808 = fmul float %sub807, %988
  %989 = load float*, float** %b1.addr, align 4
  %arrayidx809 = getelementptr inbounds float, float* %989, i32 29
  store float %mul808, float* %arrayidx809, align 4
  %990 = load float*, float** %b2.addr, align 4
  %arrayidx810 = getelementptr inbounds float, float* %990, i32 30
  %991 = load float, float* %arrayidx810, align 4
  %992 = load float*, float** %b2.addr, align 4
  %arrayidx811 = getelementptr inbounds float, float* %992, i32 31
  %993 = load float, float* %arrayidx811, align 4
  %add812 = fadd float %991, %993
  %994 = load float*, float** %b1.addr, align 4
  %arrayidx813 = getelementptr inbounds float, float* %994, i32 30
  store float %add812, float* %arrayidx813, align 4
  %995 = load float*, float** %b2.addr, align 4
  %arrayidx814 = getelementptr inbounds float, float* %995, i32 31
  %996 = load float, float* %arrayidx814, align 4
  %997 = load float*, float** %b2.addr, align 4
  %arrayidx815 = getelementptr inbounds float, float* %997, i32 30
  %998 = load float, float* %arrayidx815, align 4
  %sub816 = fsub float %996, %998
  %999 = load float, float* %cos0625, align 4
  %mul817 = fmul float %sub816, %999
  %1000 = load float*, float** %b1.addr, align 4
  %arrayidx818 = getelementptr inbounds float, float* %1000, i32 31
  store float %mul817, float* %arrayidx818, align 4
  %1001 = load float*, float** %b1.addr, align 4
  %arrayidx819 = getelementptr inbounds float, float* %1001, i32 31
  %1002 = load float, float* %arrayidx819, align 4
  %1003 = load float*, float** %b1.addr, align 4
  %arrayidx820 = getelementptr inbounds float, float* %1003, i32 30
  %1004 = load float, float* %arrayidx820, align 4
  %add821 = fadd float %1004, %1002
  store float %add821, float* %arrayidx820, align 4
  %1005 = load float*, float** %b1.addr, align 4
  %arrayidx822 = getelementptr inbounds float, float* %1005, i32 30
  %1006 = load float, float* %arrayidx822, align 4
  %1007 = load float*, float** %b1.addr, align 4
  %arrayidx823 = getelementptr inbounds float, float* %1007, i32 28
  %1008 = load float, float* %arrayidx823, align 4
  %add824 = fadd float %1008, %1006
  store float %add824, float* %arrayidx823, align 4
  %1009 = load float*, float** %b1.addr, align 4
  %arrayidx825 = getelementptr inbounds float, float* %1009, i32 29
  %1010 = load float, float* %arrayidx825, align 4
  %1011 = load float*, float** %b1.addr, align 4
  %arrayidx826 = getelementptr inbounds float, float* %1011, i32 30
  %1012 = load float, float* %arrayidx826, align 4
  %add827 = fadd float %1012, %1010
  store float %add827, float* %arrayidx826, align 4
  %1013 = load float*, float** %b1.addr, align 4
  %arrayidx828 = getelementptr inbounds float, float* %1013, i32 31
  %1014 = load float, float* %arrayidx828, align 4
  %1015 = load float*, float** %b1.addr, align 4
  %arrayidx829 = getelementptr inbounds float, float* %1015, i32 29
  %1016 = load float, float* %arrayidx829, align 4
  %add830 = fadd float %1016, %1014
  store float %add830, float* %arrayidx829, align 4
  %1017 = load float*, float** %b1.addr, align 4
  %arrayidx831 = getelementptr inbounds float, float* %1017, i32 0
  %1018 = load float, float* %arrayidx831, align 4
  %1019 = load float*, float** %out0.addr, align 4
  %arrayidx832 = getelementptr inbounds float, float* %1019, i32 256
  store float %1018, float* %arrayidx832, align 4
  %1020 = load float*, float** %b1.addr, align 4
  %arrayidx833 = getelementptr inbounds float, float* %1020, i32 4
  %1021 = load float, float* %arrayidx833, align 4
  %1022 = load float*, float** %out0.addr, align 4
  %arrayidx834 = getelementptr inbounds float, float* %1022, i32 192
  store float %1021, float* %arrayidx834, align 4
  %1023 = load float*, float** %b1.addr, align 4
  %arrayidx835 = getelementptr inbounds float, float* %1023, i32 2
  %1024 = load float, float* %arrayidx835, align 4
  %1025 = load float*, float** %out0.addr, align 4
  %arrayidx836 = getelementptr inbounds float, float* %1025, i32 128
  store float %1024, float* %arrayidx836, align 4
  %1026 = load float*, float** %b1.addr, align 4
  %arrayidx837 = getelementptr inbounds float, float* %1026, i32 6
  %1027 = load float, float* %arrayidx837, align 4
  %1028 = load float*, float** %out0.addr, align 4
  %arrayidx838 = getelementptr inbounds float, float* %1028, i32 64
  store float %1027, float* %arrayidx838, align 4
  %1029 = load float*, float** %b1.addr, align 4
  %arrayidx839 = getelementptr inbounds float, float* %1029, i32 1
  %1030 = load float, float* %arrayidx839, align 4
  %1031 = load float*, float** %out0.addr, align 4
  %arrayidx840 = getelementptr inbounds float, float* %1031, i32 0
  store float %1030, float* %arrayidx840, align 4
  %1032 = load float*, float** %b1.addr, align 4
  %arrayidx841 = getelementptr inbounds float, float* %1032, i32 1
  %1033 = load float, float* %arrayidx841, align 4
  %1034 = load float*, float** %out1.addr, align 4
  %arrayidx842 = getelementptr inbounds float, float* %1034, i32 0
  store float %1033, float* %arrayidx842, align 4
  %1035 = load float*, float** %b1.addr, align 4
  %arrayidx843 = getelementptr inbounds float, float* %1035, i32 5
  %1036 = load float, float* %arrayidx843, align 4
  %1037 = load float*, float** %out1.addr, align 4
  %arrayidx844 = getelementptr inbounds float, float* %1037, i32 64
  store float %1036, float* %arrayidx844, align 4
  %1038 = load float*, float** %b1.addr, align 4
  %arrayidx845 = getelementptr inbounds float, float* %1038, i32 3
  %1039 = load float, float* %arrayidx845, align 4
  %1040 = load float*, float** %out1.addr, align 4
  %arrayidx846 = getelementptr inbounds float, float* %1040, i32 128
  store float %1039, float* %arrayidx846, align 4
  %1041 = load float*, float** %b1.addr, align 4
  %arrayidx847 = getelementptr inbounds float, float* %1041, i32 7
  %1042 = load float, float* %arrayidx847, align 4
  %1043 = load float*, float** %out1.addr, align 4
  %arrayidx848 = getelementptr inbounds float, float* %1043, i32 192
  store float %1042, float* %arrayidx848, align 4
  %1044 = load float*, float** %b1.addr, align 4
  %arrayidx849 = getelementptr inbounds float, float* %1044, i32 12
  %1045 = load float, float* %arrayidx849, align 4
  %1046 = load float*, float** %b1.addr, align 4
  %arrayidx850 = getelementptr inbounds float, float* %1046, i32 8
  %1047 = load float, float* %arrayidx850, align 4
  %add851 = fadd float %1047, %1045
  store float %add851, float* %arrayidx850, align 4
  %1048 = load float*, float** %b1.addr, align 4
  %arrayidx852 = getelementptr inbounds float, float* %1048, i32 8
  %1049 = load float, float* %arrayidx852, align 4
  %1050 = load float*, float** %out0.addr, align 4
  %arrayidx853 = getelementptr inbounds float, float* %1050, i32 224
  store float %1049, float* %arrayidx853, align 4
  %1051 = load float*, float** %b1.addr, align 4
  %arrayidx854 = getelementptr inbounds float, float* %1051, i32 10
  %1052 = load float, float* %arrayidx854, align 4
  %1053 = load float*, float** %b1.addr, align 4
  %arrayidx855 = getelementptr inbounds float, float* %1053, i32 12
  %1054 = load float, float* %arrayidx855, align 4
  %add856 = fadd float %1054, %1052
  store float %add856, float* %arrayidx855, align 4
  %1055 = load float*, float** %b1.addr, align 4
  %arrayidx857 = getelementptr inbounds float, float* %1055, i32 12
  %1056 = load float, float* %arrayidx857, align 4
  %1057 = load float*, float** %out0.addr, align 4
  %arrayidx858 = getelementptr inbounds float, float* %1057, i32 160
  store float %1056, float* %arrayidx858, align 4
  %1058 = load float*, float** %b1.addr, align 4
  %arrayidx859 = getelementptr inbounds float, float* %1058, i32 14
  %1059 = load float, float* %arrayidx859, align 4
  %1060 = load float*, float** %b1.addr, align 4
  %arrayidx860 = getelementptr inbounds float, float* %1060, i32 10
  %1061 = load float, float* %arrayidx860, align 4
  %add861 = fadd float %1061, %1059
  store float %add861, float* %arrayidx860, align 4
  %1062 = load float*, float** %b1.addr, align 4
  %arrayidx862 = getelementptr inbounds float, float* %1062, i32 10
  %1063 = load float, float* %arrayidx862, align 4
  %1064 = load float*, float** %out0.addr, align 4
  %arrayidx863 = getelementptr inbounds float, float* %1064, i32 96
  store float %1063, float* %arrayidx863, align 4
  %1065 = load float*, float** %b1.addr, align 4
  %arrayidx864 = getelementptr inbounds float, float* %1065, i32 9
  %1066 = load float, float* %arrayidx864, align 4
  %1067 = load float*, float** %b1.addr, align 4
  %arrayidx865 = getelementptr inbounds float, float* %1067, i32 14
  %1068 = load float, float* %arrayidx865, align 4
  %add866 = fadd float %1068, %1066
  store float %add866, float* %arrayidx865, align 4
  %1069 = load float*, float** %b1.addr, align 4
  %arrayidx867 = getelementptr inbounds float, float* %1069, i32 14
  %1070 = load float, float* %arrayidx867, align 4
  %1071 = load float*, float** %out0.addr, align 4
  %arrayidx868 = getelementptr inbounds float, float* %1071, i32 32
  store float %1070, float* %arrayidx868, align 4
  %1072 = load float*, float** %b1.addr, align 4
  %arrayidx869 = getelementptr inbounds float, float* %1072, i32 13
  %1073 = load float, float* %arrayidx869, align 4
  %1074 = load float*, float** %b1.addr, align 4
  %arrayidx870 = getelementptr inbounds float, float* %1074, i32 9
  %1075 = load float, float* %arrayidx870, align 4
  %add871 = fadd float %1075, %1073
  store float %add871, float* %arrayidx870, align 4
  %1076 = load float*, float** %b1.addr, align 4
  %arrayidx872 = getelementptr inbounds float, float* %1076, i32 9
  %1077 = load float, float* %arrayidx872, align 4
  %1078 = load float*, float** %out1.addr, align 4
  %arrayidx873 = getelementptr inbounds float, float* %1078, i32 32
  store float %1077, float* %arrayidx873, align 4
  %1079 = load float*, float** %b1.addr, align 4
  %arrayidx874 = getelementptr inbounds float, float* %1079, i32 11
  %1080 = load float, float* %arrayidx874, align 4
  %1081 = load float*, float** %b1.addr, align 4
  %arrayidx875 = getelementptr inbounds float, float* %1081, i32 13
  %1082 = load float, float* %arrayidx875, align 4
  %add876 = fadd float %1082, %1080
  store float %add876, float* %arrayidx875, align 4
  %1083 = load float*, float** %b1.addr, align 4
  %arrayidx877 = getelementptr inbounds float, float* %1083, i32 13
  %1084 = load float, float* %arrayidx877, align 4
  %1085 = load float*, float** %out1.addr, align 4
  %arrayidx878 = getelementptr inbounds float, float* %1085, i32 96
  store float %1084, float* %arrayidx878, align 4
  %1086 = load float*, float** %b1.addr, align 4
  %arrayidx879 = getelementptr inbounds float, float* %1086, i32 15
  %1087 = load float, float* %arrayidx879, align 4
  %1088 = load float*, float** %b1.addr, align 4
  %arrayidx880 = getelementptr inbounds float, float* %1088, i32 11
  %1089 = load float, float* %arrayidx880, align 4
  %add881 = fadd float %1089, %1087
  store float %add881, float* %arrayidx880, align 4
  %1090 = load float*, float** %b1.addr, align 4
  %arrayidx882 = getelementptr inbounds float, float* %1090, i32 11
  %1091 = load float, float* %arrayidx882, align 4
  %1092 = load float*, float** %out1.addr, align 4
  %arrayidx883 = getelementptr inbounds float, float* %1092, i32 160
  store float %1091, float* %arrayidx883, align 4
  %1093 = load float*, float** %b1.addr, align 4
  %arrayidx884 = getelementptr inbounds float, float* %1093, i32 15
  %1094 = load float, float* %arrayidx884, align 4
  %1095 = load float*, float** %out1.addr, align 4
  %arrayidx885 = getelementptr inbounds float, float* %1095, i32 224
  store float %1094, float* %arrayidx885, align 4
  %1096 = load float*, float** %b1.addr, align 4
  %arrayidx886 = getelementptr inbounds float, float* %1096, i32 28
  %1097 = load float, float* %arrayidx886, align 4
  %1098 = load float*, float** %b1.addr, align 4
  %arrayidx887 = getelementptr inbounds float, float* %1098, i32 24
  %1099 = load float, float* %arrayidx887, align 4
  %add888 = fadd float %1099, %1097
  store float %add888, float* %arrayidx887, align 4
  %1100 = load float*, float** %b1.addr, align 4
  %arrayidx889 = getelementptr inbounds float, float* %1100, i32 16
  %1101 = load float, float* %arrayidx889, align 4
  %1102 = load float*, float** %b1.addr, align 4
  %arrayidx890 = getelementptr inbounds float, float* %1102, i32 24
  %1103 = load float, float* %arrayidx890, align 4
  %add891 = fadd float %1101, %1103
  %1104 = load float*, float** %out0.addr, align 4
  %arrayidx892 = getelementptr inbounds float, float* %1104, i32 240
  store float %add891, float* %arrayidx892, align 4
  %1105 = load float*, float** %b1.addr, align 4
  %arrayidx893 = getelementptr inbounds float, float* %1105, i32 24
  %1106 = load float, float* %arrayidx893, align 4
  %1107 = load float*, float** %b1.addr, align 4
  %arrayidx894 = getelementptr inbounds float, float* %1107, i32 20
  %1108 = load float, float* %arrayidx894, align 4
  %add895 = fadd float %1106, %1108
  %1109 = load float*, float** %out0.addr, align 4
  %arrayidx896 = getelementptr inbounds float, float* %1109, i32 208
  store float %add895, float* %arrayidx896, align 4
  %1110 = load float*, float** %b1.addr, align 4
  %arrayidx897 = getelementptr inbounds float, float* %1110, i32 26
  %1111 = load float, float* %arrayidx897, align 4
  %1112 = load float*, float** %b1.addr, align 4
  %arrayidx898 = getelementptr inbounds float, float* %1112, i32 28
  %1113 = load float, float* %arrayidx898, align 4
  %add899 = fadd float %1113, %1111
  store float %add899, float* %arrayidx898, align 4
  %1114 = load float*, float** %b1.addr, align 4
  %arrayidx900 = getelementptr inbounds float, float* %1114, i32 20
  %1115 = load float, float* %arrayidx900, align 4
  %1116 = load float*, float** %b1.addr, align 4
  %arrayidx901 = getelementptr inbounds float, float* %1116, i32 28
  %1117 = load float, float* %arrayidx901, align 4
  %add902 = fadd float %1115, %1117
  %1118 = load float*, float** %out0.addr, align 4
  %arrayidx903 = getelementptr inbounds float, float* %1118, i32 176
  store float %add902, float* %arrayidx903, align 4
  %1119 = load float*, float** %b1.addr, align 4
  %arrayidx904 = getelementptr inbounds float, float* %1119, i32 28
  %1120 = load float, float* %arrayidx904, align 4
  %1121 = load float*, float** %b1.addr, align 4
  %arrayidx905 = getelementptr inbounds float, float* %1121, i32 18
  %1122 = load float, float* %arrayidx905, align 4
  %add906 = fadd float %1120, %1122
  %1123 = load float*, float** %out0.addr, align 4
  %arrayidx907 = getelementptr inbounds float, float* %1123, i32 144
  store float %add906, float* %arrayidx907, align 4
  %1124 = load float*, float** %b1.addr, align 4
  %arrayidx908 = getelementptr inbounds float, float* %1124, i32 30
  %1125 = load float, float* %arrayidx908, align 4
  %1126 = load float*, float** %b1.addr, align 4
  %arrayidx909 = getelementptr inbounds float, float* %1126, i32 26
  %1127 = load float, float* %arrayidx909, align 4
  %add910 = fadd float %1127, %1125
  store float %add910, float* %arrayidx909, align 4
  %1128 = load float*, float** %b1.addr, align 4
  %arrayidx911 = getelementptr inbounds float, float* %1128, i32 18
  %1129 = load float, float* %arrayidx911, align 4
  %1130 = load float*, float** %b1.addr, align 4
  %arrayidx912 = getelementptr inbounds float, float* %1130, i32 26
  %1131 = load float, float* %arrayidx912, align 4
  %add913 = fadd float %1129, %1131
  %1132 = load float*, float** %out0.addr, align 4
  %arrayidx914 = getelementptr inbounds float, float* %1132, i32 112
  store float %add913, float* %arrayidx914, align 4
  %1133 = load float*, float** %b1.addr, align 4
  %arrayidx915 = getelementptr inbounds float, float* %1133, i32 26
  %1134 = load float, float* %arrayidx915, align 4
  %1135 = load float*, float** %b1.addr, align 4
  %arrayidx916 = getelementptr inbounds float, float* %1135, i32 22
  %1136 = load float, float* %arrayidx916, align 4
  %add917 = fadd float %1134, %1136
  %1137 = load float*, float** %out0.addr, align 4
  %arrayidx918 = getelementptr inbounds float, float* %1137, i32 80
  store float %add917, float* %arrayidx918, align 4
  %1138 = load float*, float** %b1.addr, align 4
  %arrayidx919 = getelementptr inbounds float, float* %1138, i32 25
  %1139 = load float, float* %arrayidx919, align 4
  %1140 = load float*, float** %b1.addr, align 4
  %arrayidx920 = getelementptr inbounds float, float* %1140, i32 30
  %1141 = load float, float* %arrayidx920, align 4
  %add921 = fadd float %1141, %1139
  store float %add921, float* %arrayidx920, align 4
  %1142 = load float*, float** %b1.addr, align 4
  %arrayidx922 = getelementptr inbounds float, float* %1142, i32 22
  %1143 = load float, float* %arrayidx922, align 4
  %1144 = load float*, float** %b1.addr, align 4
  %arrayidx923 = getelementptr inbounds float, float* %1144, i32 30
  %1145 = load float, float* %arrayidx923, align 4
  %add924 = fadd float %1143, %1145
  %1146 = load float*, float** %out0.addr, align 4
  %arrayidx925 = getelementptr inbounds float, float* %1146, i32 48
  store float %add924, float* %arrayidx925, align 4
  %1147 = load float*, float** %b1.addr, align 4
  %arrayidx926 = getelementptr inbounds float, float* %1147, i32 30
  %1148 = load float, float* %arrayidx926, align 4
  %1149 = load float*, float** %b1.addr, align 4
  %arrayidx927 = getelementptr inbounds float, float* %1149, i32 17
  %1150 = load float, float* %arrayidx927, align 4
  %add928 = fadd float %1148, %1150
  %1151 = load float*, float** %out0.addr, align 4
  %arrayidx929 = getelementptr inbounds float, float* %1151, i32 16
  store float %add928, float* %arrayidx929, align 4
  %1152 = load float*, float** %b1.addr, align 4
  %arrayidx930 = getelementptr inbounds float, float* %1152, i32 29
  %1153 = load float, float* %arrayidx930, align 4
  %1154 = load float*, float** %b1.addr, align 4
  %arrayidx931 = getelementptr inbounds float, float* %1154, i32 25
  %1155 = load float, float* %arrayidx931, align 4
  %add932 = fadd float %1155, %1153
  store float %add932, float* %arrayidx931, align 4
  %1156 = load float*, float** %b1.addr, align 4
  %arrayidx933 = getelementptr inbounds float, float* %1156, i32 17
  %1157 = load float, float* %arrayidx933, align 4
  %1158 = load float*, float** %b1.addr, align 4
  %arrayidx934 = getelementptr inbounds float, float* %1158, i32 25
  %1159 = load float, float* %arrayidx934, align 4
  %add935 = fadd float %1157, %1159
  %1160 = load float*, float** %out1.addr, align 4
  %arrayidx936 = getelementptr inbounds float, float* %1160, i32 16
  store float %add935, float* %arrayidx936, align 4
  %1161 = load float*, float** %b1.addr, align 4
  %arrayidx937 = getelementptr inbounds float, float* %1161, i32 25
  %1162 = load float, float* %arrayidx937, align 4
  %1163 = load float*, float** %b1.addr, align 4
  %arrayidx938 = getelementptr inbounds float, float* %1163, i32 21
  %1164 = load float, float* %arrayidx938, align 4
  %add939 = fadd float %1162, %1164
  %1165 = load float*, float** %out1.addr, align 4
  %arrayidx940 = getelementptr inbounds float, float* %1165, i32 48
  store float %add939, float* %arrayidx940, align 4
  %1166 = load float*, float** %b1.addr, align 4
  %arrayidx941 = getelementptr inbounds float, float* %1166, i32 27
  %1167 = load float, float* %arrayidx941, align 4
  %1168 = load float*, float** %b1.addr, align 4
  %arrayidx942 = getelementptr inbounds float, float* %1168, i32 29
  %1169 = load float, float* %arrayidx942, align 4
  %add943 = fadd float %1169, %1167
  store float %add943, float* %arrayidx942, align 4
  %1170 = load float*, float** %b1.addr, align 4
  %arrayidx944 = getelementptr inbounds float, float* %1170, i32 21
  %1171 = load float, float* %arrayidx944, align 4
  %1172 = load float*, float** %b1.addr, align 4
  %arrayidx945 = getelementptr inbounds float, float* %1172, i32 29
  %1173 = load float, float* %arrayidx945, align 4
  %add946 = fadd float %1171, %1173
  %1174 = load float*, float** %out1.addr, align 4
  %arrayidx947 = getelementptr inbounds float, float* %1174, i32 80
  store float %add946, float* %arrayidx947, align 4
  %1175 = load float*, float** %b1.addr, align 4
  %arrayidx948 = getelementptr inbounds float, float* %1175, i32 29
  %1176 = load float, float* %arrayidx948, align 4
  %1177 = load float*, float** %b1.addr, align 4
  %arrayidx949 = getelementptr inbounds float, float* %1177, i32 19
  %1178 = load float, float* %arrayidx949, align 4
  %add950 = fadd float %1176, %1178
  %1179 = load float*, float** %out1.addr, align 4
  %arrayidx951 = getelementptr inbounds float, float* %1179, i32 112
  store float %add950, float* %arrayidx951, align 4
  %1180 = load float*, float** %b1.addr, align 4
  %arrayidx952 = getelementptr inbounds float, float* %1180, i32 31
  %1181 = load float, float* %arrayidx952, align 4
  %1182 = load float*, float** %b1.addr, align 4
  %arrayidx953 = getelementptr inbounds float, float* %1182, i32 27
  %1183 = load float, float* %arrayidx953, align 4
  %add954 = fadd float %1183, %1181
  store float %add954, float* %arrayidx953, align 4
  %1184 = load float*, float** %b1.addr, align 4
  %arrayidx955 = getelementptr inbounds float, float* %1184, i32 19
  %1185 = load float, float* %arrayidx955, align 4
  %1186 = load float*, float** %b1.addr, align 4
  %arrayidx956 = getelementptr inbounds float, float* %1186, i32 27
  %1187 = load float, float* %arrayidx956, align 4
  %add957 = fadd float %1185, %1187
  %1188 = load float*, float** %out1.addr, align 4
  %arrayidx958 = getelementptr inbounds float, float* %1188, i32 144
  store float %add957, float* %arrayidx958, align 4
  %1189 = load float*, float** %b1.addr, align 4
  %arrayidx959 = getelementptr inbounds float, float* %1189, i32 27
  %1190 = load float, float* %arrayidx959, align 4
  %1191 = load float*, float** %b1.addr, align 4
  %arrayidx960 = getelementptr inbounds float, float* %1191, i32 23
  %1192 = load float, float* %arrayidx960, align 4
  %add961 = fadd float %1190, %1192
  %1193 = load float*, float** %out1.addr, align 4
  %arrayidx962 = getelementptr inbounds float, float* %1193, i32 176
  store float %add961, float* %arrayidx962, align 4
  %1194 = load float*, float** %b1.addr, align 4
  %arrayidx963 = getelementptr inbounds float, float* %1194, i32 23
  %1195 = load float, float* %arrayidx963, align 4
  %1196 = load float*, float** %b1.addr, align 4
  %arrayidx964 = getelementptr inbounds float, float* %1196, i32 31
  %1197 = load float, float* %arrayidx964, align 4
  %add965 = fadd float %1195, %1197
  %1198 = load float*, float** %out1.addr, align 4
  %arrayidx966 = getelementptr inbounds float, float* %1198, i32 208
  store float %add965, float* %arrayidx966, align 4
  %1199 = load float*, float** %b1.addr, align 4
  %arrayidx967 = getelementptr inbounds float, float* %1199, i32 31
  %1200 = load float, float* %arrayidx967, align 4
  %1201 = load float*, float** %out1.addr, align 4
  %arrayidx968 = getelementptr inbounds float, float* %1201, i32 240
  store float %1200, float* %arrayidx968, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
