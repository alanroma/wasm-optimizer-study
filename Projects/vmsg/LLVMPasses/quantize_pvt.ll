; ModuleID = 'quantize_pvt.c'
source_filename = "quantize_pvt.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type { i32, i32, i32, double, [2 x [1600 x double]], [2 x [1776 x double]], [2 x [2 x [576 x double]]], [2 x [2 x [576 x double]]], [2 x double], [2 x double], [4 x [1024 x double]], [2 x [4 x [1024 x double]]], [2 x [4 x double]], [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]], [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]], [4 x double], [2 x [4 x double]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x [3 x i32]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [2 x i32], i32, i32, i32 }
%struct.hip_global_struct = type opaque
%struct.III_psy_ratio = type { %struct.III_psy_xmin, %struct.III_psy_xmin }
%struct.calc_noise_result_t = type { float, float, float, i32, i32, i32 }
%struct.calc_noise_data_t = type { i32, i32, [39 x i32], [39 x float], [39 x float] }

@nr_of_sfb_block = hidden constant [6 x [3 x [4 x i32]]] [[3 x [4 x i32]] [[4 x i32] [i32 6, i32 5, i32 5, i32 5], [4 x i32] [i32 9, i32 9, i32 9, i32 9], [4 x i32] [i32 6, i32 9, i32 9, i32 9]], [3 x [4 x i32]] [[4 x i32] [i32 6, i32 5, i32 7, i32 3], [4 x i32] [i32 9, i32 9, i32 12, i32 6], [4 x i32] [i32 6, i32 9, i32 12, i32 6]], [3 x [4 x i32]] [[4 x i32] [i32 11, i32 10, i32 0, i32 0], [4 x i32] [i32 18, i32 18, i32 0, i32 0], [4 x i32] [i32 15, i32 18, i32 0, i32 0]], [3 x [4 x i32]] [[4 x i32] [i32 7, i32 7, i32 7, i32 0], [4 x i32] [i32 12, i32 12, i32 12, i32 0], [4 x i32] [i32 6, i32 15, i32 12, i32 0]], [3 x [4 x i32]] [[4 x i32] [i32 6, i32 6, i32 6, i32 3], [4 x i32] [i32 12, i32 9, i32 9, i32 6], [4 x i32] [i32 6, i32 12, i32 9, i32 6]], [3 x [4 x i32]] [[4 x i32] [i32 8, i32 8, i32 5, i32 0], [4 x i32] [i32 15, i32 12, i32 9, i32 0], [4 x i32] [i32 6, i32 18, i32 9, i32 0]]], align 16
@pretab = hidden constant [22 x i32] [i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 0, i32 1, i32 1, i32 1, i32 1, i32 2, i32 2, i32 3, i32 3, i32 3, i32 2, i32 0], align 16
@sfBandIndex = hidden constant [9 x %struct.scalefac_struct] [%struct.scalefac_struct { [23 x i32] [i32 0, i32 6, i32 12, i32 18, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 80, i32 96, i32 116, i32 140, i32 168, i32 200, i32 238, i32 284, i32 336, i32 396, i32 464, i32 522, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 18, i32 24, i32 32, i32 42, i32 56, i32 74, i32 100, i32 132, i32 174, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 6, i32 12, i32 18, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 80, i32 96, i32 114, i32 136, i32 162, i32 194, i32 232, i32 278, i32 332, i32 394, i32 464, i32 540, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 18, i32 26, i32 36, i32 48, i32 62, i32 80, i32 104, i32 136, i32 180, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 6, i32 12, i32 18, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 80, i32 96, i32 116, i32 140, i32 168, i32 200, i32 238, i32 284, i32 336, i32 396, i32 464, i32 522, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 18, i32 26, i32 36, i32 48, i32 62, i32 80, i32 104, i32 134, i32 174, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 20, i32 24, i32 30, i32 36, i32 44, i32 52, i32 62, i32 74, i32 90, i32 110, i32 134, i32 162, i32 196, i32 238, i32 288, i32 342, i32 418, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 22, i32 30, i32 40, i32 52, i32 66, i32 84, i32 106, i32 136, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 20, i32 24, i32 30, i32 36, i32 42, i32 50, i32 60, i32 72, i32 88, i32 106, i32 128, i32 156, i32 190, i32 230, i32 276, i32 330, i32 384, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 22, i32 28, i32 38, i32 50, i32 64, i32 80, i32 100, i32 126, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 20, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 82, i32 102, i32 126, i32 156, i32 194, i32 240, i32 296, i32 364, i32 448, i32 550, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 16, i32 22, i32 30, i32 42, i32 58, i32 78, i32 104, i32 138, i32 180, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 6, i32 12, i32 18, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 80, i32 96, i32 116, i32 140, i32 168, i32 200, i32 238, i32 284, i32 336, i32 396, i32 464, i32 522, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 18, i32 26, i32 36, i32 48, i32 62, i32 80, i32 104, i32 134, i32 174, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 6, i32 12, i32 18, i32 24, i32 30, i32 36, i32 44, i32 54, i32 66, i32 80, i32 96, i32 116, i32 140, i32 168, i32 200, i32 238, i32 284, i32 336, i32 396, i32 464, i32 522, i32 576], [14 x i32] [i32 0, i32 4, i32 8, i32 12, i32 18, i32 26, i32 36, i32 48, i32 62, i32 80, i32 104, i32 134, i32 174, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }, %struct.scalefac_struct { [23 x i32] [i32 0, i32 12, i32 24, i32 36, i32 48, i32 60, i32 72, i32 88, i32 108, i32 132, i32 160, i32 192, i32 232, i32 280, i32 336, i32 400, i32 476, i32 566, i32 568, i32 570, i32 572, i32 574, i32 576], [14 x i32] [i32 0, i32 8, i32 16, i32 24, i32 36, i32 52, i32 72, i32 96, i32 124, i32 160, i32 162, i32 164, i32 166, i32 192], [7 x i32] zeroinitializer, [7 x i32] zeroinitializer }], align 16
@pow43 = hidden global [8208 x float] zeroinitializer, align 16
@adj43asm = hidden global [8208 x float] zeroinitializer, align 16
@ipow20 = hidden global [257 x float] zeroinitializer, align 16
@pow20 = hidden global [374 x float] zeroinitializer, align 16
@payload_long = internal constant [2 x [4 x float]] [[4 x float] [float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float 0.000000e+00], [4 x float] [float -5.000000e-01, float -2.500000e-01, float 0xBF999999A0000000, float 5.000000e-01]], align 16
@payload_short = internal constant [2 x [4 x float]] [[4 x float] [float -0.000000e+00, float -0.000000e+00, float -0.000000e+00, float 0.000000e+00], [4 x float] [float -2.000000e+00, float -1.000000e+00, float 0xBFA99999A0000000, float 5.000000e-01]], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @iteration_init(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %adjust = alloca float, align 4
  %db = alloca float, align 4
  %i = alloca i32, align 4
  %sel = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %iteration_init_init = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 3
  %3 = load i32, i32* %iteration_init_init, align 4
  %cmp = icmp eq i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %iteration_init_init3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 3
  store i32 1, i32* %iteration_init_init3, align 4
  %5 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %5, i32 0, i32 1
  store i32 0, i32* %main_data_begin, align 4
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @compute_ath(%struct.lame_internal_flags* %6)
  store float 0.000000e+00, float* getelementptr inbounds ([8208 x float], [8208 x float]* @pow43, i32 0, i32 0), align 16
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %i, align 4
  %cmp4 = icmp slt i32 %7, 8208
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i32, i32* %i, align 4
  %conv = sitofp i32 %8 to float
  %conv5 = fpext float %conv to double
  %9 = call double @llvm.pow.f64(double %conv5, double 0x3FF5555555555555)
  %conv6 = fptrunc double %9 to float
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %10
  store float %conv6, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store float 0.000000e+00, float* getelementptr inbounds ([8208 x float], [8208 x float]* @adj43asm, i32 0, i32 0), align 16
  store i32 1, i32* %i, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc19, %for.end
  %12 = load i32, i32* %i, align 4
  %cmp8 = icmp slt i32 %12, 8208
  br i1 %cmp8, label %for.body10, label %for.end21

for.body10:                                       ; preds = %for.cond7
  %13 = load i32, i32* %i, align 4
  %conv11 = sitofp i32 %13 to double
  %sub = fsub double %conv11, 5.000000e-01
  %14 = load i32, i32* %i, align 4
  %sub12 = sub nsw i32 %14, 1
  %arrayidx13 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %sub12
  %15 = load float, float* %arrayidx13, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %16
  %17 = load float, float* %arrayidx14, align 4
  %add = fadd float %15, %17
  %conv15 = fpext float %add to double
  %mul = fmul double 5.000000e-01, %conv15
  %18 = call double @llvm.pow.f64(double %mul, double 7.500000e-01)
  %sub16 = fsub double %sub, %18
  %conv17 = fptrunc double %sub16 to float
  %19 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [8208 x float], [8208 x float]* @adj43asm, i32 0, i32 %19
  store float %conv17, float* %arrayidx18, align 4
  br label %for.inc19

for.inc19:                                        ; preds = %for.body10
  %20 = load i32, i32* %i, align 4
  %inc20 = add nsw i32 %20, 1
  store i32 %inc20, i32* %i, align 4
  br label %for.cond7

for.end21:                                        ; preds = %for.cond7
  store i32 0, i32* %i, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc31, %for.end21
  %21 = load i32, i32* %i, align 4
  %cmp23 = icmp slt i32 %21, 257
  br i1 %cmp23, label %for.body25, label %for.end33

for.body25:                                       ; preds = %for.cond22
  %22 = load i32, i32* %i, align 4
  %sub26 = sub nsw i32 %22, 210
  %conv27 = sitofp i32 %sub26 to double
  %mul28 = fmul double %conv27, -1.875000e-01
  %23 = call double @llvm.pow.f64(double 2.000000e+00, double %mul28)
  %conv29 = fptrunc double %23 to float
  %24 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %24
  store float %conv29, float* %arrayidx30, align 4
  br label %for.inc31

for.inc31:                                        ; preds = %for.body25
  %25 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %25, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond22

for.end33:                                        ; preds = %for.cond22
  store i32 0, i32* %i, align 4
  br label %for.cond34

for.cond34:                                       ; preds = %for.inc44, %for.end33
  %26 = load i32, i32* %i, align 4
  %cmp35 = icmp sle i32 %26, 373
  br i1 %cmp35, label %for.body37, label %for.end46

for.body37:                                       ; preds = %for.cond34
  %27 = load i32, i32* %i, align 4
  %sub38 = sub nsw i32 %27, 210
  %sub39 = sub nsw i32 %sub38, 116
  %conv40 = sitofp i32 %sub39 to double
  %mul41 = fmul double %conv40, 2.500000e-01
  %28 = call double @llvm.pow.f64(double 2.000000e+00, double %mul41)
  %conv42 = fptrunc double %28 to float
  %29 = load i32, i32* %i, align 4
  %arrayidx43 = getelementptr inbounds [374 x float], [374 x float]* @pow20, i32 0, i32 %29
  store float %conv42, float* %arrayidx43, align 4
  br label %for.inc44

for.inc44:                                        ; preds = %for.body37
  %30 = load i32, i32* %i, align 4
  %inc45 = add nsw i32 %30, 1
  store i32 %inc45, i32* %i, align 4
  br label %for.cond34

for.end46:                                        ; preds = %for.cond34
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @huffman_init(%struct.lame_internal_flags* %31)
  %32 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @init_xrpow_core_init(%struct.lame_internal_flags* %32)
  store i32 1, i32* %sel, align 4
  %33 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %33, i32 0, i32 54
  %34 = load float, float* %adjust_bass_db, align 4
  %35 = load i32, i32* %sel, align 4
  %arrayidx47 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_long, i32 0, i32 %35
  %arrayidx48 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx47, i32 0, i32 0
  %36 = load float, float* %arrayidx48, align 16
  %add49 = fadd float %34, %36
  store float %add49, float* %db, align 4
  %37 = load float, float* %db, align 4
  %mul50 = fmul float %37, 0x3FB99999A0000000
  %38 = call float @llvm.pow.f32(float 1.000000e+01, float %mul50)
  store float %38, float* %adjust, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc56, %for.end46
  %39 = load i32, i32* %i, align 4
  %cmp52 = icmp sle i32 %39, 6
  br i1 %cmp52, label %for.body54, label %for.end58

for.body54:                                       ; preds = %for.cond51
  %40 = load float, float* %adjust, align 4
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 13
  %longfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 0
  %42 = load i32, i32* %i, align 4
  %arrayidx55 = getelementptr inbounds [22 x float], [22 x float]* %longfact, i32 0, i32 %42
  store float %40, float* %arrayidx55, align 4
  br label %for.inc56

for.inc56:                                        ; preds = %for.body54
  %43 = load i32, i32* %i, align 4
  %inc57 = add nsw i32 %43, 1
  store i32 %inc57, i32* %i, align 4
  br label %for.cond51

for.end58:                                        ; preds = %for.cond51
  %44 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %44, i32 0, i32 53
  %45 = load float, float* %adjust_alto_db, align 4
  %46 = load i32, i32* %sel, align 4
  %arrayidx59 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_long, i32 0, i32 %46
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx59, i32 0, i32 1
  %47 = load float, float* %arrayidx60, align 4
  %add61 = fadd float %45, %47
  store float %add61, float* %db, align 4
  %48 = load float, float* %db, align 4
  %mul62 = fmul float %48, 0x3FB99999A0000000
  %49 = call float @llvm.pow.f32(float 1.000000e+01, float %mul62)
  store float %49, float* %adjust, align 4
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc70, %for.end58
  %50 = load i32, i32* %i, align 4
  %cmp64 = icmp sle i32 %50, 13
  br i1 %cmp64, label %for.body66, label %for.end72

for.body66:                                       ; preds = %for.cond63
  %51 = load float, float* %adjust, align 4
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt67 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %52, i32 0, i32 13
  %longfact68 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt67, i32 0, i32 0
  %53 = load i32, i32* %i, align 4
  %arrayidx69 = getelementptr inbounds [22 x float], [22 x float]* %longfact68, i32 0, i32 %53
  store float %51, float* %arrayidx69, align 4
  br label %for.inc70

for.inc70:                                        ; preds = %for.body66
  %54 = load i32, i32* %i, align 4
  %inc71 = add nsw i32 %54, 1
  store i32 %inc71, i32* %i, align 4
  br label %for.cond63

for.end72:                                        ; preds = %for.cond63
  %55 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %55, i32 0, i32 55
  %56 = load float, float* %adjust_treble_db, align 4
  %57 = load i32, i32* %sel, align 4
  %arrayidx73 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_long, i32 0, i32 %57
  %arrayidx74 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx73, i32 0, i32 2
  %58 = load float, float* %arrayidx74, align 8
  %add75 = fadd float %56, %58
  store float %add75, float* %db, align 4
  %59 = load float, float* %db, align 4
  %mul76 = fmul float %59, 0x3FB99999A0000000
  %60 = call float @llvm.pow.f32(float 1.000000e+01, float %mul76)
  store float %60, float* %adjust, align 4
  br label %for.cond77

for.cond77:                                       ; preds = %for.inc84, %for.end72
  %61 = load i32, i32* %i, align 4
  %cmp78 = icmp sle i32 %61, 20
  br i1 %cmp78, label %for.body80, label %for.end86

for.body80:                                       ; preds = %for.cond77
  %62 = load float, float* %adjust, align 4
  %63 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt81 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %63, i32 0, i32 13
  %longfact82 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt81, i32 0, i32 0
  %64 = load i32, i32* %i, align 4
  %arrayidx83 = getelementptr inbounds [22 x float], [22 x float]* %longfact82, i32 0, i32 %64
  store float %62, float* %arrayidx83, align 4
  br label %for.inc84

for.inc84:                                        ; preds = %for.body80
  %65 = load i32, i32* %i, align 4
  %inc85 = add nsw i32 %65, 1
  store i32 %inc85, i32* %i, align 4
  br label %for.cond77

for.end86:                                        ; preds = %for.cond77
  %66 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %66, i32 0, i32 56
  %67 = load float, float* %adjust_sfb21_db, align 4
  %68 = load i32, i32* %sel, align 4
  %arrayidx87 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_long, i32 0, i32 %68
  %arrayidx88 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx87, i32 0, i32 3
  %69 = load float, float* %arrayidx88, align 4
  %add89 = fadd float %67, %69
  store float %add89, float* %db, align 4
  %70 = load float, float* %db, align 4
  %mul90 = fmul float %70, 0x3FB99999A0000000
  %71 = call float @llvm.pow.f32(float 1.000000e+01, float %mul90)
  store float %71, float* %adjust, align 4
  br label %for.cond91

for.cond91:                                       ; preds = %for.inc98, %for.end86
  %72 = load i32, i32* %i, align 4
  %cmp92 = icmp slt i32 %72, 22
  br i1 %cmp92, label %for.body94, label %for.end100

for.body94:                                       ; preds = %for.cond91
  %73 = load float, float* %adjust, align 4
  %74 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt95 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %74, i32 0, i32 13
  %longfact96 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt95, i32 0, i32 0
  %75 = load i32, i32* %i, align 4
  %arrayidx97 = getelementptr inbounds [22 x float], [22 x float]* %longfact96, i32 0, i32 %75
  store float %73, float* %arrayidx97, align 4
  br label %for.inc98

for.inc98:                                        ; preds = %for.body94
  %76 = load i32, i32* %i, align 4
  %inc99 = add nsw i32 %76, 1
  store i32 %inc99, i32* %i, align 4
  br label %for.cond91

for.end100:                                       ; preds = %for.cond91
  %77 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db101 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %77, i32 0, i32 54
  %78 = load float, float* %adjust_bass_db101, align 4
  %79 = load i32, i32* %sel, align 4
  %arrayidx102 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_short, i32 0, i32 %79
  %arrayidx103 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx102, i32 0, i32 0
  %80 = load float, float* %arrayidx103, align 16
  %add104 = fadd float %78, %80
  store float %add104, float* %db, align 4
  %81 = load float, float* %db, align 4
  %mul105 = fmul float %81, 0x3FB99999A0000000
  %82 = call float @llvm.pow.f32(float 1.000000e+01, float %mul105)
  store float %82, float* %adjust, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond106

for.cond106:                                      ; preds = %for.inc112, %for.end100
  %83 = load i32, i32* %i, align 4
  %cmp107 = icmp sle i32 %83, 2
  br i1 %cmp107, label %for.body109, label %for.end114

for.body109:                                      ; preds = %for.cond106
  %84 = load float, float* %adjust, align 4
  %85 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt110 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %85, i32 0, i32 13
  %shortfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt110, i32 0, i32 1
  %86 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [13 x float], [13 x float]* %shortfact, i32 0, i32 %86
  store float %84, float* %arrayidx111, align 4
  br label %for.inc112

for.inc112:                                       ; preds = %for.body109
  %87 = load i32, i32* %i, align 4
  %inc113 = add nsw i32 %87, 1
  store i32 %inc113, i32* %i, align 4
  br label %for.cond106

for.end114:                                       ; preds = %for.cond106
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db115 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 53
  %89 = load float, float* %adjust_alto_db115, align 4
  %90 = load i32, i32* %sel, align 4
  %arrayidx116 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_short, i32 0, i32 %90
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx116, i32 0, i32 1
  %91 = load float, float* %arrayidx117, align 4
  %add118 = fadd float %89, %91
  store float %add118, float* %db, align 4
  %92 = load float, float* %db, align 4
  %mul119 = fmul float %92, 0x3FB99999A0000000
  %93 = call float @llvm.pow.f32(float 1.000000e+01, float %mul119)
  store float %93, float* %adjust, align 4
  br label %for.cond120

for.cond120:                                      ; preds = %for.inc127, %for.end114
  %94 = load i32, i32* %i, align 4
  %cmp121 = icmp sle i32 %94, 6
  br i1 %cmp121, label %for.body123, label %for.end129

for.body123:                                      ; preds = %for.cond120
  %95 = load float, float* %adjust, align 4
  %96 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt124 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %96, i32 0, i32 13
  %shortfact125 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt124, i32 0, i32 1
  %97 = load i32, i32* %i, align 4
  %arrayidx126 = getelementptr inbounds [13 x float], [13 x float]* %shortfact125, i32 0, i32 %97
  store float %95, float* %arrayidx126, align 4
  br label %for.inc127

for.inc127:                                       ; preds = %for.body123
  %98 = load i32, i32* %i, align 4
  %inc128 = add nsw i32 %98, 1
  store i32 %inc128, i32* %i, align 4
  br label %for.cond120

for.end129:                                       ; preds = %for.cond120
  %99 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db130 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %99, i32 0, i32 55
  %100 = load float, float* %adjust_treble_db130, align 4
  %101 = load i32, i32* %sel, align 4
  %arrayidx131 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_short, i32 0, i32 %101
  %arrayidx132 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx131, i32 0, i32 2
  %102 = load float, float* %arrayidx132, align 8
  %add133 = fadd float %100, %102
  store float %add133, float* %db, align 4
  %103 = load float, float* %db, align 4
  %mul134 = fmul float %103, 0x3FB99999A0000000
  %104 = call float @llvm.pow.f32(float 1.000000e+01, float %mul134)
  store float %104, float* %adjust, align 4
  br label %for.cond135

for.cond135:                                      ; preds = %for.inc142, %for.end129
  %105 = load i32, i32* %i, align 4
  %cmp136 = icmp sle i32 %105, 11
  br i1 %cmp136, label %for.body138, label %for.end144

for.body138:                                      ; preds = %for.cond135
  %106 = load float, float* %adjust, align 4
  %107 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt139 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %107, i32 0, i32 13
  %shortfact140 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt139, i32 0, i32 1
  %108 = load i32, i32* %i, align 4
  %arrayidx141 = getelementptr inbounds [13 x float], [13 x float]* %shortfact140, i32 0, i32 %108
  store float %106, float* %arrayidx141, align 4
  br label %for.inc142

for.inc142:                                       ; preds = %for.body138
  %109 = load i32, i32* %i, align 4
  %inc143 = add nsw i32 %109, 1
  store i32 %inc143, i32* %i, align 4
  br label %for.cond135

for.end144:                                       ; preds = %for.cond135
  %110 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db145 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %110, i32 0, i32 56
  %111 = load float, float* %adjust_sfb21_db145, align 4
  %112 = load i32, i32* %sel, align 4
  %arrayidx146 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* @payload_short, i32 0, i32 %112
  %arrayidx147 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx146, i32 0, i32 3
  %113 = load float, float* %arrayidx147, align 4
  %add148 = fadd float %111, %113
  store float %add148, float* %db, align 4
  %114 = load float, float* %db, align 4
  %mul149 = fmul float %114, 0x3FB99999A0000000
  %115 = call float @llvm.pow.f32(float 1.000000e+01, float %mul149)
  store float %115, float* %adjust, align 4
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc157, %for.end144
  %116 = load i32, i32* %i, align 4
  %cmp151 = icmp slt i32 %116, 13
  br i1 %cmp151, label %for.body153, label %for.end159

for.body153:                                      ; preds = %for.cond150
  %117 = load float, float* %adjust, align 4
  %118 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt154 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %118, i32 0, i32 13
  %shortfact155 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt154, i32 0, i32 1
  %119 = load i32, i32* %i, align 4
  %arrayidx156 = getelementptr inbounds [13 x float], [13 x float]* %shortfact155, i32 0, i32 %119
  store float %117, float* %arrayidx156, align 4
  br label %for.inc157

for.inc157:                                       ; preds = %for.body153
  %120 = load i32, i32* %i, align 4
  %inc158 = add nsw i32 %120, 1
  store i32 %inc158, i32* %i, align 4
  br label %for.cond150

for.end159:                                       ; preds = %for.cond150
  br label %if.end

if.end:                                           ; preds = %for.end159, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @compute_ath(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %ATH_l = alloca float*, align 4
  %ATH_psfb21 = alloca float*, align 4
  %ATH_s = alloca float*, align 4
  %ATH_psfb12 = alloca float*, align 4
  %sfb = alloca i32, align 4
  %i = alloca i32, align 4
  %start = alloca i32, align 4
  %end = alloca i32, align 4
  %ATH_f = alloca float, align 4
  %samp_freq = alloca float, align 4
  %freq = alloca float, align 4
  %freq43 = alloca float, align 4
  %freq79 = alloca float, align 4
  %freq125 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 21
  %2 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %l = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %2, i32 0, i32 6
  %arraydecay = getelementptr inbounds [22 x float], [22 x float]* %l, i32 0, i32 0
  store float* %arraydecay, float** %ATH_l, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 21
  %4 = load %struct.ATH_t*, %struct.ATH_t** %ATH2, align 8
  %psfb21 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %4, i32 0, i32 8
  %arraydecay3 = getelementptr inbounds [6 x float], [6 x float]* %psfb21, i32 0, i32 0
  store float* %arraydecay3, float** %ATH_psfb21, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 21
  %6 = load %struct.ATH_t*, %struct.ATH_t** %ATH4, align 8
  %s = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %6, i32 0, i32 7
  %arraydecay5 = getelementptr inbounds [13 x float], [13 x float]* %s, i32 0, i32 0
  store float* %arraydecay5, float** %ATH_s, align 4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 21
  %8 = load %struct.ATH_t*, %struct.ATH_t** %ATH6, align 8
  %psfb12 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %8, i32 0, i32 9
  %arraydecay7 = getelementptr inbounds [6 x float], [6 x float]* %psfb12, i32 0, i32 0
  store float* %arraydecay7, float** %ATH_psfb12, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 12
  %10 = load i32, i32* %samplerate_out, align 4
  %conv = sitofp i32 %10 to float
  store float %conv, float* %samp_freq, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %entry
  %11 = load i32, i32* %sfb, align 4
  %cmp = icmp slt i32 %11, 22
  br i1 %cmp, label %for.body, label %for.end26

for.body:                                         ; preds = %for.cond
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 8
  %l9 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %13 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [23 x i32], [23 x i32]* %l9, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx, align 4
  store i32 %14, i32* %start, align 4
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 8
  %l11 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band10, i32 0, i32 0
  %16 = load i32, i32* %sfb, align 4
  %add = add nsw i32 %16, 1
  %arrayidx12 = getelementptr inbounds [23 x i32], [23 x i32]* %l11, i32 0, i32 %add
  %17 = load i32, i32* %arrayidx12, align 4
  store i32 %17, i32* %end, align 4
  %18 = load float*, float** %ATH_l, align 4
  %19 = load i32, i32* %sfb, align 4
  %arrayidx13 = getelementptr inbounds float, float* %18, i32 %19
  store float 0x479E17B840000000, float* %arrayidx13, align 4
  %20 = load i32, i32* %start, align 4
  store i32 %20, i32* %i, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %for.body
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %end, align 4
  %cmp15 = icmp slt i32 %21, %22
  br i1 %cmp15, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond14
  %23 = load i32, i32* %i, align 4
  %conv18 = sitofp i32 %23 to float
  %24 = load float, float* %samp_freq, align 4
  %mul = fmul float %conv18, %24
  %div = fdiv float %mul, 1.152000e+03
  store float %div, float* %freq, align 4
  %25 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %26 = load float, float* %freq, align 4
  %call = call float @ATHmdct(%struct.SessionConfig_t* %25, float %26)
  store float %call, float* %ATH_f, align 4
  %27 = load float*, float** %ATH_l, align 4
  %28 = load i32, i32* %sfb, align 4
  %arrayidx19 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx19, align 4
  %30 = load float, float* %ATH_f, align 4
  %cmp20 = fcmp olt float %29, %30
  br i1 %cmp20, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body17
  %31 = load float*, float** %ATH_l, align 4
  %32 = load i32, i32* %sfb, align 4
  %arrayidx22 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx22, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body17
  %34 = load float, float* %ATH_f, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %33, %cond.true ], [ %34, %cond.false ]
  %35 = load float*, float** %ATH_l, align 4
  %36 = load i32, i32* %sfb, align 4
  %arrayidx23 = getelementptr inbounds float, float* %35, i32 %36
  store float %cond, float* %arrayidx23, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %37 = load i32, i32* %i, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc24

for.inc24:                                        ; preds = %for.end
  %38 = load i32, i32* %sfb, align 4
  %inc25 = add nsw i32 %38, 1
  store i32 %inc25, i32* %sfb, align 4
  br label %for.cond

for.end26:                                        ; preds = %for.cond
  store i32 0, i32* %sfb, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc60, %for.end26
  %39 = load i32, i32* %sfb, align 4
  %cmp28 = icmp slt i32 %39, 6
  br i1 %cmp28, label %for.body30, label %for.end62

for.body30:                                       ; preds = %for.cond27
  %40 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band31 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %40, i32 0, i32 8
  %psfb2132 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band31, i32 0, i32 2
  %41 = load i32, i32* %sfb, align 4
  %arrayidx33 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb2132, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx33, align 4
  store i32 %42, i32* %start, align 4
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band34 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %43, i32 0, i32 8
  %psfb2135 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band34, i32 0, i32 2
  %44 = load i32, i32* %sfb, align 4
  %add36 = add nsw i32 %44, 1
  %arrayidx37 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb2135, i32 0, i32 %add36
  %45 = load i32, i32* %arrayidx37, align 4
  store i32 %45, i32* %end, align 4
  %46 = load float*, float** %ATH_psfb21, align 4
  %47 = load i32, i32* %sfb, align 4
  %arrayidx38 = getelementptr inbounds float, float* %46, i32 %47
  store float 0x479E17B840000000, float* %arrayidx38, align 4
  %48 = load i32, i32* %start, align 4
  store i32 %48, i32* %i, align 4
  br label %for.cond39

for.cond39:                                       ; preds = %for.inc57, %for.body30
  %49 = load i32, i32* %i, align 4
  %50 = load i32, i32* %end, align 4
  %cmp40 = icmp slt i32 %49, %50
  br i1 %cmp40, label %for.body42, label %for.end59

for.body42:                                       ; preds = %for.cond39
  %51 = load i32, i32* %i, align 4
  %conv44 = sitofp i32 %51 to float
  %52 = load float, float* %samp_freq, align 4
  %mul45 = fmul float %conv44, %52
  %div46 = fdiv float %mul45, 1.152000e+03
  store float %div46, float* %freq43, align 4
  %53 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %54 = load float, float* %freq43, align 4
  %call47 = call float @ATHmdct(%struct.SessionConfig_t* %53, float %54)
  store float %call47, float* %ATH_f, align 4
  %55 = load float*, float** %ATH_psfb21, align 4
  %56 = load i32, i32* %sfb, align 4
  %arrayidx48 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx48, align 4
  %58 = load float, float* %ATH_f, align 4
  %cmp49 = fcmp olt float %57, %58
  br i1 %cmp49, label %cond.true51, label %cond.false53

cond.true51:                                      ; preds = %for.body42
  %59 = load float*, float** %ATH_psfb21, align 4
  %60 = load i32, i32* %sfb, align 4
  %arrayidx52 = getelementptr inbounds float, float* %59, i32 %60
  %61 = load float, float* %arrayidx52, align 4
  br label %cond.end54

cond.false53:                                     ; preds = %for.body42
  %62 = load float, float* %ATH_f, align 4
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false53, %cond.true51
  %cond55 = phi float [ %61, %cond.true51 ], [ %62, %cond.false53 ]
  %63 = load float*, float** %ATH_psfb21, align 4
  %64 = load i32, i32* %sfb, align 4
  %arrayidx56 = getelementptr inbounds float, float* %63, i32 %64
  store float %cond55, float* %arrayidx56, align 4
  br label %for.inc57

for.inc57:                                        ; preds = %cond.end54
  %65 = load i32, i32* %i, align 4
  %inc58 = add nsw i32 %65, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond39

for.end59:                                        ; preds = %for.cond39
  br label %for.inc60

for.inc60:                                        ; preds = %for.end59
  %66 = load i32, i32* %sfb, align 4
  %inc61 = add nsw i32 %66, 1
  store i32 %inc61, i32* %sfb, align 4
  br label %for.cond27

for.end62:                                        ; preds = %for.cond27
  store i32 0, i32* %sfb, align 4
  br label %for.cond63

for.cond63:                                       ; preds = %for.inc106, %for.end62
  %67 = load i32, i32* %sfb, align 4
  %cmp64 = icmp slt i32 %67, 13
  br i1 %cmp64, label %for.body66, label %for.end108

for.body66:                                       ; preds = %for.cond63
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band67 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %68, i32 0, i32 8
  %s68 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band67, i32 0, i32 1
  %69 = load i32, i32* %sfb, align 4
  %arrayidx69 = getelementptr inbounds [14 x i32], [14 x i32]* %s68, i32 0, i32 %69
  %70 = load i32, i32* %arrayidx69, align 4
  store i32 %70, i32* %start, align 4
  %71 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band70 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %71, i32 0, i32 8
  %s71 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band70, i32 0, i32 1
  %72 = load i32, i32* %sfb, align 4
  %add72 = add nsw i32 %72, 1
  %arrayidx73 = getelementptr inbounds [14 x i32], [14 x i32]* %s71, i32 0, i32 %add72
  %73 = load i32, i32* %arrayidx73, align 4
  store i32 %73, i32* %end, align 4
  %74 = load float*, float** %ATH_s, align 4
  %75 = load i32, i32* %sfb, align 4
  %arrayidx74 = getelementptr inbounds float, float* %74, i32 %75
  store float 0x479E17B840000000, float* %arrayidx74, align 4
  %76 = load i32, i32* %start, align 4
  store i32 %76, i32* %i, align 4
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc93, %for.body66
  %77 = load i32, i32* %i, align 4
  %78 = load i32, i32* %end, align 4
  %cmp76 = icmp slt i32 %77, %78
  br i1 %cmp76, label %for.body78, label %for.end95

for.body78:                                       ; preds = %for.cond75
  %79 = load i32, i32* %i, align 4
  %conv80 = sitofp i32 %79 to float
  %80 = load float, float* %samp_freq, align 4
  %mul81 = fmul float %conv80, %80
  %div82 = fdiv float %mul81, 3.840000e+02
  store float %div82, float* %freq79, align 4
  %81 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %82 = load float, float* %freq79, align 4
  %call83 = call float @ATHmdct(%struct.SessionConfig_t* %81, float %82)
  store float %call83, float* %ATH_f, align 4
  %83 = load float*, float** %ATH_s, align 4
  %84 = load i32, i32* %sfb, align 4
  %arrayidx84 = getelementptr inbounds float, float* %83, i32 %84
  %85 = load float, float* %arrayidx84, align 4
  %86 = load float, float* %ATH_f, align 4
  %cmp85 = fcmp olt float %85, %86
  br i1 %cmp85, label %cond.true87, label %cond.false89

cond.true87:                                      ; preds = %for.body78
  %87 = load float*, float** %ATH_s, align 4
  %88 = load i32, i32* %sfb, align 4
  %arrayidx88 = getelementptr inbounds float, float* %87, i32 %88
  %89 = load float, float* %arrayidx88, align 4
  br label %cond.end90

cond.false89:                                     ; preds = %for.body78
  %90 = load float, float* %ATH_f, align 4
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false89, %cond.true87
  %cond91 = phi float [ %89, %cond.true87 ], [ %90, %cond.false89 ]
  %91 = load float*, float** %ATH_s, align 4
  %92 = load i32, i32* %sfb, align 4
  %arrayidx92 = getelementptr inbounds float, float* %91, i32 %92
  store float %cond91, float* %arrayidx92, align 4
  br label %for.inc93

for.inc93:                                        ; preds = %cond.end90
  %93 = load i32, i32* %i, align 4
  %inc94 = add nsw i32 %93, 1
  store i32 %inc94, i32* %i, align 4
  br label %for.cond75

for.end95:                                        ; preds = %for.cond75
  %94 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band96 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %94, i32 0, i32 8
  %s97 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band96, i32 0, i32 1
  %95 = load i32, i32* %sfb, align 4
  %add98 = add nsw i32 %95, 1
  %arrayidx99 = getelementptr inbounds [14 x i32], [14 x i32]* %s97, i32 0, i32 %add98
  %96 = load i32, i32* %arrayidx99, align 4
  %97 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band100 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %97, i32 0, i32 8
  %s101 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band100, i32 0, i32 1
  %98 = load i32, i32* %sfb, align 4
  %arrayidx102 = getelementptr inbounds [14 x i32], [14 x i32]* %s101, i32 0, i32 %98
  %99 = load i32, i32* %arrayidx102, align 4
  %sub = sub nsw i32 %96, %99
  %conv103 = sitofp i32 %sub to float
  %100 = load float*, float** %ATH_s, align 4
  %101 = load i32, i32* %sfb, align 4
  %arrayidx104 = getelementptr inbounds float, float* %100, i32 %101
  %102 = load float, float* %arrayidx104, align 4
  %mul105 = fmul float %102, %conv103
  store float %mul105, float* %arrayidx104, align 4
  br label %for.inc106

for.inc106:                                       ; preds = %for.end95
  %103 = load i32, i32* %sfb, align 4
  %inc107 = add nsw i32 %103, 1
  store i32 %inc107, i32* %sfb, align 4
  br label %for.cond63

for.end108:                                       ; preds = %for.cond63
  store i32 0, i32* %sfb, align 4
  br label %for.cond109

for.cond109:                                      ; preds = %for.inc152, %for.end108
  %104 = load i32, i32* %sfb, align 4
  %cmp110 = icmp slt i32 %104, 6
  br i1 %cmp110, label %for.body112, label %for.end154

for.body112:                                      ; preds = %for.cond109
  %105 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band113 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %105, i32 0, i32 8
  %psfb12114 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band113, i32 0, i32 3
  %106 = load i32, i32* %sfb, align 4
  %arrayidx115 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb12114, i32 0, i32 %106
  %107 = load i32, i32* %arrayidx115, align 4
  store i32 %107, i32* %start, align 4
  %108 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band116 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %108, i32 0, i32 8
  %psfb12117 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band116, i32 0, i32 3
  %109 = load i32, i32* %sfb, align 4
  %add118 = add nsw i32 %109, 1
  %arrayidx119 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb12117, i32 0, i32 %add118
  %110 = load i32, i32* %arrayidx119, align 4
  store i32 %110, i32* %end, align 4
  %111 = load float*, float** %ATH_psfb12, align 4
  %112 = load i32, i32* %sfb, align 4
  %arrayidx120 = getelementptr inbounds float, float* %111, i32 %112
  store float 0x479E17B840000000, float* %arrayidx120, align 4
  %113 = load i32, i32* %start, align 4
  store i32 %113, i32* %i, align 4
  br label %for.cond121

for.cond121:                                      ; preds = %for.inc139, %for.body112
  %114 = load i32, i32* %i, align 4
  %115 = load i32, i32* %end, align 4
  %cmp122 = icmp slt i32 %114, %115
  br i1 %cmp122, label %for.body124, label %for.end141

for.body124:                                      ; preds = %for.cond121
  %116 = load i32, i32* %i, align 4
  %conv126 = sitofp i32 %116 to float
  %117 = load float, float* %samp_freq, align 4
  %mul127 = fmul float %conv126, %117
  %div128 = fdiv float %mul127, 3.840000e+02
  store float %div128, float* %freq125, align 4
  %118 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %119 = load float, float* %freq125, align 4
  %call129 = call float @ATHmdct(%struct.SessionConfig_t* %118, float %119)
  store float %call129, float* %ATH_f, align 4
  %120 = load float*, float** %ATH_psfb12, align 4
  %121 = load i32, i32* %sfb, align 4
  %arrayidx130 = getelementptr inbounds float, float* %120, i32 %121
  %122 = load float, float* %arrayidx130, align 4
  %123 = load float, float* %ATH_f, align 4
  %cmp131 = fcmp olt float %122, %123
  br i1 %cmp131, label %cond.true133, label %cond.false135

cond.true133:                                     ; preds = %for.body124
  %124 = load float*, float** %ATH_psfb12, align 4
  %125 = load i32, i32* %sfb, align 4
  %arrayidx134 = getelementptr inbounds float, float* %124, i32 %125
  %126 = load float, float* %arrayidx134, align 4
  br label %cond.end136

cond.false135:                                    ; preds = %for.body124
  %127 = load float, float* %ATH_f, align 4
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false135, %cond.true133
  %cond137 = phi float [ %126, %cond.true133 ], [ %127, %cond.false135 ]
  %128 = load float*, float** %ATH_psfb12, align 4
  %129 = load i32, i32* %sfb, align 4
  %arrayidx138 = getelementptr inbounds float, float* %128, i32 %129
  store float %cond137, float* %arrayidx138, align 4
  br label %for.inc139

for.inc139:                                       ; preds = %cond.end136
  %130 = load i32, i32* %i, align 4
  %inc140 = add nsw i32 %130, 1
  store i32 %inc140, i32* %i, align 4
  br label %for.cond121

for.end141:                                       ; preds = %for.cond121
  %131 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band142 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %131, i32 0, i32 8
  %s143 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band142, i32 0, i32 1
  %arrayidx144 = getelementptr inbounds [14 x i32], [14 x i32]* %s143, i32 0, i32 13
  %132 = load i32, i32* %arrayidx144, align 4
  %133 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band145 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %133, i32 0, i32 8
  %s146 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band145, i32 0, i32 1
  %arrayidx147 = getelementptr inbounds [14 x i32], [14 x i32]* %s146, i32 0, i32 12
  %134 = load i32, i32* %arrayidx147, align 4
  %sub148 = sub nsw i32 %132, %134
  %conv149 = sitofp i32 %sub148 to float
  %135 = load float*, float** %ATH_psfb12, align 4
  %136 = load i32, i32* %sfb, align 4
  %arrayidx150 = getelementptr inbounds float, float* %135, i32 %136
  %137 = load float, float* %arrayidx150, align 4
  %mul151 = fmul float %137, %conv149
  store float %mul151, float* %arrayidx150, align 4
  br label %for.inc152

for.inc152:                                       ; preds = %for.end141
  %138 = load i32, i32* %sfb, align 4
  %inc153 = add nsw i32 %138, 1
  store i32 %inc153, i32* %sfb, align 4
  br label %for.cond109

for.end154:                                       ; preds = %for.cond109
  %139 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noATH = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %139, i32 0, i32 51
  %140 = load i32, i32* %noATH, align 4
  %tobool = icmp ne i32 %140, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end154
  store i32 0, i32* %sfb, align 4
  br label %for.cond155

for.cond155:                                      ; preds = %for.inc160, %if.then
  %141 = load i32, i32* %sfb, align 4
  %cmp156 = icmp slt i32 %141, 22
  br i1 %cmp156, label %for.body158, label %for.end162

for.body158:                                      ; preds = %for.cond155
  %142 = load float*, float** %ATH_l, align 4
  %143 = load i32, i32* %sfb, align 4
  %arrayidx159 = getelementptr inbounds float, float* %142, i32 %143
  store float 0x3BC79CA100000000, float* %arrayidx159, align 4
  br label %for.inc160

for.inc160:                                       ; preds = %for.body158
  %144 = load i32, i32* %sfb, align 4
  %inc161 = add nsw i32 %144, 1
  store i32 %inc161, i32* %sfb, align 4
  br label %for.cond155

for.end162:                                       ; preds = %for.cond155
  store i32 0, i32* %sfb, align 4
  br label %for.cond163

for.cond163:                                      ; preds = %for.inc168, %for.end162
  %145 = load i32, i32* %sfb, align 4
  %cmp164 = icmp slt i32 %145, 6
  br i1 %cmp164, label %for.body166, label %for.end170

for.body166:                                      ; preds = %for.cond163
  %146 = load float*, float** %ATH_psfb21, align 4
  %147 = load i32, i32* %sfb, align 4
  %arrayidx167 = getelementptr inbounds float, float* %146, i32 %147
  store float 0x3BC79CA100000000, float* %arrayidx167, align 4
  br label %for.inc168

for.inc168:                                       ; preds = %for.body166
  %148 = load i32, i32* %sfb, align 4
  %inc169 = add nsw i32 %148, 1
  store i32 %inc169, i32* %sfb, align 4
  br label %for.cond163

for.end170:                                       ; preds = %for.cond163
  store i32 0, i32* %sfb, align 4
  br label %for.cond171

for.cond171:                                      ; preds = %for.inc176, %for.end170
  %149 = load i32, i32* %sfb, align 4
  %cmp172 = icmp slt i32 %149, 13
  br i1 %cmp172, label %for.body174, label %for.end178

for.body174:                                      ; preds = %for.cond171
  %150 = load float*, float** %ATH_s, align 4
  %151 = load i32, i32* %sfb, align 4
  %arrayidx175 = getelementptr inbounds float, float* %150, i32 %151
  store float 0x3BC79CA100000000, float* %arrayidx175, align 4
  br label %for.inc176

for.inc176:                                       ; preds = %for.body174
  %152 = load i32, i32* %sfb, align 4
  %inc177 = add nsw i32 %152, 1
  store i32 %inc177, i32* %sfb, align 4
  br label %for.cond171

for.end178:                                       ; preds = %for.cond171
  store i32 0, i32* %sfb, align 4
  br label %for.cond179

for.cond179:                                      ; preds = %for.inc184, %for.end178
  %153 = load i32, i32* %sfb, align 4
  %cmp180 = icmp slt i32 %153, 6
  br i1 %cmp180, label %for.body182, label %for.end186

for.body182:                                      ; preds = %for.cond179
  %154 = load float*, float** %ATH_psfb12, align 4
  %155 = load i32, i32* %sfb, align 4
  %arrayidx183 = getelementptr inbounds float, float* %154, i32 %155
  store float 0x3BC79CA100000000, float* %arrayidx183, align 4
  br label %for.inc184

for.inc184:                                       ; preds = %for.body182
  %156 = load i32, i32* %sfb, align 4
  %inc185 = add nsw i32 %156, 1
  store i32 %inc185, i32* %sfb, align 4
  br label %for.cond179

for.end186:                                       ; preds = %for.cond179
  br label %if.end

if.end:                                           ; preds = %for.end186, %for.end154
  %157 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call187 = call float @ATHmdct(%struct.SessionConfig_t* %157, float -1.000000e+00)
  %conv188 = fpext float %call187 to double
  %158 = call double @llvm.log10.f64(double %conv188)
  %mul189 = fmul double 1.000000e+01, %158
  %conv190 = fptrunc double %mul189 to float
  %159 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH191 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %159, i32 0, i32 21
  %160 = load %struct.ATH_t*, %struct.ATH_t** %ATH191, align 8
  %floor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %160, i32 0, i32 5
  store float %conv190, float* %floor, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #1

declare void @huffman_init(%struct.lame_internal_flags*) #2

declare void @init_xrpow_core_init(%struct.lame_internal_flags*) #2

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @on_pe(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, i32* %targ_bits, i32 %mean_bits, i32 %gr, i32 %cbr) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %targ_bits.addr = alloca i32*, align 4
  %mean_bits.addr = alloca i32, align 4
  %gr.addr = alloca i32, align 4
  %cbr.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %extra_bits = alloca i32, align 4
  %tbits = alloca i32, align 4
  %bits = alloca i32, align 4
  %add_bits = alloca [2 x i32], align 4
  %max_bits = alloca i32, align 4
  %ch = alloca i32, align 4
  %sum = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store i32* %targ_bits, i32** %targ_bits.addr, align 4
  store i32 %mean_bits, i32* %mean_bits.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 %cbr, i32* %cbr.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %extra_bits, align 4
  %1 = bitcast [2 x i32]* %add_bits to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 8, i1 false)
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %3 = load i32, i32* %mean_bits.addr, align 4
  %4 = load i32, i32* %cbr.addr, align 4
  call void @ResvMaxBits(%struct.lame_internal_flags* %2, i32 %3, i32* %tbits, i32* %extra_bits, i32 %4)
  %5 = load i32, i32* %tbits, align 4
  %6 = load i32, i32* %extra_bits, align 4
  %add = add nsw i32 %5, %6
  store i32 %add, i32* %max_bits, align 4
  %7 = load i32, i32* %max_bits, align 4
  %cmp = icmp sgt i32 %7, 7680
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 7680, i32* %max_bits, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %bits, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %ch, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 14
  %10 = load i32, i32* %channels_out, align 4
  %cmp2 = icmp slt i32 %8, %10
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %tbits, align 4
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out3 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 14
  %13 = load i32, i32* %channels_out3, align 4
  %div = sdiv i32 %11, %13
  %cmp4 = icmp slt i32 4095, %div
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %14 = load i32, i32* %tbits, align 4
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 14
  %16 = load i32, i32* %channels_out5, align 4
  %div6 = sdiv i32 %14, %16
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 4095, %cond.true ], [ %div6, %cond.false ]
  %17 = load i32*, i32** %targ_bits.addr, align 4
  %18 = load i32, i32* %ch, align 4
  %arrayidx = getelementptr inbounds i32, i32* %17, i32 %18
  store i32 %cond, i32* %arrayidx, align 4
  %19 = load i32*, i32** %targ_bits.addr, align 4
  %20 = load i32, i32* %ch, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %19, i32 %20
  %21 = load i32, i32* %arrayidx7, align 4
  %conv = sitofp i32 %21 to float
  %22 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %23 = load i32, i32* %gr.addr, align 4
  %arrayidx8 = getelementptr inbounds [2 x float], [2 x float]* %22, i32 %23
  %24 = load i32, i32* %ch, align 4
  %arrayidx9 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx8, i32 0, i32 %24
  %25 = load float, float* %arrayidx9, align 4
  %mul = fmul float %conv, %25
  %conv10 = fpext float %mul to double
  %div11 = fdiv double %conv10, 7.000000e+02
  %26 = load i32*, i32** %targ_bits.addr, align 4
  %27 = load i32, i32* %ch, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %26, i32 %27
  %28 = load i32, i32* %arrayidx12, align 4
  %conv13 = sitofp i32 %28 to double
  %sub = fsub double %div11, %conv13
  %conv14 = fptosi double %sub to i32
  %29 = load i32, i32* %ch, align 4
  %arrayidx15 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %29
  store i32 %conv14, i32* %arrayidx15, align 4
  %30 = load i32, i32* %ch, align 4
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %30
  %31 = load i32, i32* %arrayidx16, align 4
  %32 = load i32, i32* %mean_bits.addr, align 4
  %mul17 = mul nsw i32 %32, 3
  %div18 = sdiv i32 %mul17, 4
  %cmp19 = icmp sgt i32 %31, %div18
  br i1 %cmp19, label %if.then21, label %if.end25

if.then21:                                        ; preds = %cond.end
  %33 = load i32, i32* %mean_bits.addr, align 4
  %mul22 = mul nsw i32 %33, 3
  %div23 = sdiv i32 %mul22, 4
  %34 = load i32, i32* %ch, align 4
  %arrayidx24 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %34
  store i32 %div23, i32* %arrayidx24, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then21, %cond.end
  %35 = load i32, i32* %ch, align 4
  %arrayidx26 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx26, align 4
  %cmp27 = icmp slt i32 %36, 0
  br i1 %cmp27, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end25
  %37 = load i32, i32* %ch, align 4
  %arrayidx30 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %37
  store i32 0, i32* %arrayidx30, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end25
  %38 = load i32, i32* %ch, align 4
  %arrayidx32 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %38
  %39 = load i32, i32* %arrayidx32, align 4
  %40 = load i32*, i32** %targ_bits.addr, align 4
  %41 = load i32, i32* %ch, align 4
  %arrayidx33 = getelementptr inbounds i32, i32* %40, i32 %41
  %42 = load i32, i32* %arrayidx33, align 4
  %add34 = add nsw i32 %39, %42
  %cmp35 = icmp sgt i32 %add34, 4095
  br i1 %cmp35, label %if.then37, label %if.end49

if.then37:                                        ; preds = %if.end31
  %43 = load i32*, i32** %targ_bits.addr, align 4
  %44 = load i32, i32* %ch, align 4
  %arrayidx38 = getelementptr inbounds i32, i32* %43, i32 %44
  %45 = load i32, i32* %arrayidx38, align 4
  %sub39 = sub nsw i32 4095, %45
  %cmp40 = icmp sgt i32 0, %sub39
  br i1 %cmp40, label %cond.true42, label %cond.false43

cond.true42:                                      ; preds = %if.then37
  br label %cond.end46

cond.false43:                                     ; preds = %if.then37
  %46 = load i32*, i32** %targ_bits.addr, align 4
  %47 = load i32, i32* %ch, align 4
  %arrayidx44 = getelementptr inbounds i32, i32* %46, i32 %47
  %48 = load i32, i32* %arrayidx44, align 4
  %sub45 = sub nsw i32 4095, %48
  br label %cond.end46

cond.end46:                                       ; preds = %cond.false43, %cond.true42
  %cond47 = phi i32 [ 0, %cond.true42 ], [ %sub45, %cond.false43 ]
  %49 = load i32, i32* %ch, align 4
  %arrayidx48 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %49
  store i32 %cond47, i32* %arrayidx48, align 4
  br label %if.end49

if.end49:                                         ; preds = %cond.end46, %if.end31
  %50 = load i32, i32* %ch, align 4
  %arrayidx50 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx50, align 4
  %52 = load i32, i32* %bits, align 4
  %add51 = add nsw i32 %52, %51
  store i32 %add51, i32* %bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end49
  %53 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %53, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %54 = load i32, i32* %bits, align 4
  %55 = load i32, i32* %extra_bits, align 4
  %cmp52 = icmp sgt i32 %54, %55
  br i1 %cmp52, label %land.lhs.true, label %if.end69

land.lhs.true:                                    ; preds = %for.end
  %56 = load i32, i32* %bits, align 4
  %cmp54 = icmp sgt i32 %56, 0
  br i1 %cmp54, label %if.then56, label %if.end69

if.then56:                                        ; preds = %land.lhs.true
  store i32 0, i32* %ch, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc66, %if.then56
  %57 = load i32, i32* %ch, align 4
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out58 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 14
  %59 = load i32, i32* %channels_out58, align 4
  %cmp59 = icmp slt i32 %57, %59
  br i1 %cmp59, label %for.body61, label %for.end68

for.body61:                                       ; preds = %for.cond57
  %60 = load i32, i32* %extra_bits, align 4
  %61 = load i32, i32* %ch, align 4
  %arrayidx62 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %61
  %62 = load i32, i32* %arrayidx62, align 4
  %mul63 = mul nsw i32 %60, %62
  %63 = load i32, i32* %bits, align 4
  %div64 = sdiv i32 %mul63, %63
  %64 = load i32, i32* %ch, align 4
  %arrayidx65 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %64
  store i32 %div64, i32* %arrayidx65, align 4
  br label %for.inc66

for.inc66:                                        ; preds = %for.body61
  %65 = load i32, i32* %ch, align 4
  %inc67 = add nsw i32 %65, 1
  store i32 %inc67, i32* %ch, align 4
  br label %for.cond57

for.end68:                                        ; preds = %for.cond57
  br label %if.end69

if.end69:                                         ; preds = %for.end68, %land.lhs.true, %for.end
  store i32 0, i32* %ch, align 4
  br label %for.cond70

for.cond70:                                       ; preds = %for.inc80, %if.end69
  %66 = load i32, i32* %ch, align 4
  %67 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out71 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %67, i32 0, i32 14
  %68 = load i32, i32* %channels_out71, align 4
  %cmp72 = icmp slt i32 %66, %68
  br i1 %cmp72, label %for.body74, label %for.end82

for.body74:                                       ; preds = %for.cond70
  %69 = load i32, i32* %ch, align 4
  %arrayidx75 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %69
  %70 = load i32, i32* %arrayidx75, align 4
  %71 = load i32*, i32** %targ_bits.addr, align 4
  %72 = load i32, i32* %ch, align 4
  %arrayidx76 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx76, align 4
  %add77 = add nsw i32 %73, %70
  store i32 %add77, i32* %arrayidx76, align 4
  %74 = load i32, i32* %ch, align 4
  %arrayidx78 = getelementptr inbounds [2 x i32], [2 x i32]* %add_bits, i32 0, i32 %74
  %75 = load i32, i32* %arrayidx78, align 4
  %76 = load i32, i32* %extra_bits, align 4
  %sub79 = sub nsw i32 %76, %75
  store i32 %sub79, i32* %extra_bits, align 4
  br label %for.inc80

for.inc80:                                        ; preds = %for.body74
  %77 = load i32, i32* %ch, align 4
  %inc81 = add nsw i32 %77, 1
  store i32 %inc81, i32* %ch, align 4
  br label %for.cond70

for.end82:                                        ; preds = %for.cond70
  store i32 0, i32* %bits, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond83

for.cond83:                                       ; preds = %for.inc90, %for.end82
  %78 = load i32, i32* %ch, align 4
  %79 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out84 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %79, i32 0, i32 14
  %80 = load i32, i32* %channels_out84, align 4
  %cmp85 = icmp slt i32 %78, %80
  br i1 %cmp85, label %for.body87, label %for.end92

for.body87:                                       ; preds = %for.cond83
  %81 = load i32*, i32** %targ_bits.addr, align 4
  %82 = load i32, i32* %ch, align 4
  %arrayidx88 = getelementptr inbounds i32, i32* %81, i32 %82
  %83 = load i32, i32* %arrayidx88, align 4
  %84 = load i32, i32* %bits, align 4
  %add89 = add nsw i32 %84, %83
  store i32 %add89, i32* %bits, align 4
  br label %for.inc90

for.inc90:                                        ; preds = %for.body87
  %85 = load i32, i32* %ch, align 4
  %inc91 = add nsw i32 %85, 1
  store i32 %inc91, i32* %ch, align 4
  br label %for.cond83

for.end92:                                        ; preds = %for.cond83
  %86 = load i32, i32* %bits, align 4
  %cmp93 = icmp sgt i32 %86, 7680
  br i1 %cmp93, label %if.then95, label %if.end110

if.then95:                                        ; preds = %for.end92
  store i32 0, i32* %sum, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond96

for.cond96:                                       ; preds = %for.inc107, %if.then95
  %87 = load i32, i32* %ch, align 4
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out97 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 14
  %89 = load i32, i32* %channels_out97, align 4
  %cmp98 = icmp slt i32 %87, %89
  br i1 %cmp98, label %for.body100, label %for.end109

for.body100:                                      ; preds = %for.cond96
  %90 = load i32*, i32** %targ_bits.addr, align 4
  %91 = load i32, i32* %ch, align 4
  %arrayidx101 = getelementptr inbounds i32, i32* %90, i32 %91
  %92 = load i32, i32* %arrayidx101, align 4
  %mul102 = mul nsw i32 %92, 7680
  store i32 %mul102, i32* %arrayidx101, align 4
  %93 = load i32, i32* %bits, align 4
  %94 = load i32*, i32** %targ_bits.addr, align 4
  %95 = load i32, i32* %ch, align 4
  %arrayidx103 = getelementptr inbounds i32, i32* %94, i32 %95
  %96 = load i32, i32* %arrayidx103, align 4
  %div104 = sdiv i32 %96, %93
  store i32 %div104, i32* %arrayidx103, align 4
  %97 = load i32*, i32** %targ_bits.addr, align 4
  %98 = load i32, i32* %ch, align 4
  %arrayidx105 = getelementptr inbounds i32, i32* %97, i32 %98
  %99 = load i32, i32* %arrayidx105, align 4
  %100 = load i32, i32* %sum, align 4
  %add106 = add nsw i32 %100, %99
  store i32 %add106, i32* %sum, align 4
  br label %for.inc107

for.inc107:                                       ; preds = %for.body100
  %101 = load i32, i32* %ch, align 4
  %inc108 = add nsw i32 %101, 1
  store i32 %inc108, i32* %ch, align 4
  br label %for.cond96

for.end109:                                       ; preds = %for.cond96
  br label %if.end110

if.end110:                                        ; preds = %for.end109, %for.end92
  %102 = load i32, i32* %max_bits, align 4
  ret i32 %102
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

declare void @ResvMaxBits(%struct.lame_internal_flags*, i32, i32*, i32*, i32) #2

; Function Attrs: noinline nounwind optnone
define hidden void @reduce_side(i32* %targ_bits, float %ms_ener_ratio, i32 %mean_bits, i32 %max_bits) #0 {
entry:
  %targ_bits.addr = alloca i32*, align 4
  %ms_ener_ratio.addr = alloca float, align 4
  %mean_bits.addr = alloca i32, align 4
  %max_bits.addr = alloca i32, align 4
  %move_bits = alloca i32, align 4
  %fac = alloca float, align 4
  store i32* %targ_bits, i32** %targ_bits.addr, align 4
  store float %ms_ener_ratio, float* %ms_ener_ratio.addr, align 4
  store i32 %mean_bits, i32* %mean_bits.addr, align 4
  store i32 %max_bits, i32* %max_bits.addr, align 4
  %0 = load float, float* %ms_ener_ratio.addr, align 4
  %conv = fpext float %0 to double
  %sub = fsub double 5.000000e-01, %conv
  %mul = fmul double 3.300000e-01, %sub
  %div = fdiv double %mul, 5.000000e-01
  %conv1 = fptrunc double %div to float
  store float %conv1, float* %fac, align 4
  %1 = load float, float* %fac, align 4
  %cmp = fcmp olt float %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %fac, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load float, float* %fac, align 4
  %conv3 = fpext float %2 to double
  %cmp4 = fcmp ogt double %conv3, 5.000000e-01
  br i1 %cmp4, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end
  store float 5.000000e-01, float* %fac, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %if.end
  %3 = load float, float* %fac, align 4
  %conv8 = fpext float %3 to double
  %mul9 = fmul double %conv8, 5.000000e-01
  %4 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 0
  %5 = load i32, i32* %arrayidx, align 4
  %6 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %6, i32 1
  %7 = load i32, i32* %arrayidx10, align 4
  %add = add nsw i32 %5, %7
  %conv11 = sitofp i32 %add to double
  %mul12 = fmul double %mul9, %conv11
  %conv13 = fptosi double %mul12 to i32
  store i32 %conv13, i32* %move_bits, align 4
  %8 = load i32, i32* %move_bits, align 4
  %9 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx14 = getelementptr inbounds i32, i32* %9, i32 0
  %10 = load i32, i32* %arrayidx14, align 4
  %sub15 = sub nsw i32 4095, %10
  %cmp16 = icmp sgt i32 %8, %sub15
  br i1 %cmp16, label %if.then18, label %if.end21

if.then18:                                        ; preds = %if.end7
  %11 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx19 = getelementptr inbounds i32, i32* %11, i32 0
  %12 = load i32, i32* %arrayidx19, align 4
  %sub20 = sub nsw i32 4095, %12
  store i32 %sub20, i32* %move_bits, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.then18, %if.end7
  %13 = load i32, i32* %move_bits, align 4
  %cmp22 = icmp slt i32 %13, 0
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %if.end21
  store i32 0, i32* %move_bits, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then24, %if.end21
  %14 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx26 = getelementptr inbounds i32, i32* %14, i32 1
  %15 = load i32, i32* %arrayidx26, align 4
  %cmp27 = icmp sge i32 %15, 125
  br i1 %cmp27, label %if.then29, label %if.end50

if.then29:                                        ; preds = %if.end25
  %16 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx30 = getelementptr inbounds i32, i32* %16, i32 1
  %17 = load i32, i32* %arrayidx30, align 4
  %18 = load i32, i32* %move_bits, align 4
  %sub31 = sub nsw i32 %17, %18
  %cmp32 = icmp sgt i32 %sub31, 125
  br i1 %cmp32, label %if.then34, label %if.else

if.then34:                                        ; preds = %if.then29
  %19 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx35 = getelementptr inbounds i32, i32* %19, i32 0
  %20 = load i32, i32* %arrayidx35, align 4
  %21 = load i32, i32* %mean_bits.addr, align 4
  %cmp36 = icmp slt i32 %20, %21
  br i1 %cmp36, label %if.then38, label %if.end41

if.then38:                                        ; preds = %if.then34
  %22 = load i32, i32* %move_bits, align 4
  %23 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx39 = getelementptr inbounds i32, i32* %23, i32 0
  %24 = load i32, i32* %arrayidx39, align 4
  %add40 = add nsw i32 %24, %22
  store i32 %add40, i32* %arrayidx39, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then38, %if.then34
  %25 = load i32, i32* %move_bits, align 4
  %26 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx42 = getelementptr inbounds i32, i32* %26, i32 1
  %27 = load i32, i32* %arrayidx42, align 4
  %sub43 = sub nsw i32 %27, %25
  store i32 %sub43, i32* %arrayidx42, align 4
  br label %if.end49

if.else:                                          ; preds = %if.then29
  %28 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx44 = getelementptr inbounds i32, i32* %28, i32 1
  %29 = load i32, i32* %arrayidx44, align 4
  %sub45 = sub nsw i32 %29, 125
  %30 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx46 = getelementptr inbounds i32, i32* %30, i32 0
  %31 = load i32, i32* %arrayidx46, align 4
  %add47 = add nsw i32 %31, %sub45
  store i32 %add47, i32* %arrayidx46, align 4
  %32 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx48 = getelementptr inbounds i32, i32* %32, i32 1
  store i32 125, i32* %arrayidx48, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.else, %if.end41
  br label %if.end50

if.end50:                                         ; preds = %if.end49, %if.end25
  %33 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx51 = getelementptr inbounds i32, i32* %33, i32 0
  %34 = load i32, i32* %arrayidx51, align 4
  %35 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx52 = getelementptr inbounds i32, i32* %35, i32 1
  %36 = load i32, i32* %arrayidx52, align 4
  %add53 = add nsw i32 %34, %36
  store i32 %add53, i32* %move_bits, align 4
  %37 = load i32, i32* %move_bits, align 4
  %38 = load i32, i32* %max_bits.addr, align 4
  %cmp54 = icmp sgt i32 %37, %38
  br i1 %cmp54, label %if.then56, label %if.end65

if.then56:                                        ; preds = %if.end50
  %39 = load i32, i32* %max_bits.addr, align 4
  %40 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx57 = getelementptr inbounds i32, i32* %40, i32 0
  %41 = load i32, i32* %arrayidx57, align 4
  %mul58 = mul nsw i32 %39, %41
  %42 = load i32, i32* %move_bits, align 4
  %div59 = sdiv i32 %mul58, %42
  %43 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx60 = getelementptr inbounds i32, i32* %43, i32 0
  store i32 %div59, i32* %arrayidx60, align 4
  %44 = load i32, i32* %max_bits.addr, align 4
  %45 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx61 = getelementptr inbounds i32, i32* %45, i32 1
  %46 = load i32, i32* %arrayidx61, align 4
  %mul62 = mul nsw i32 %44, %46
  %47 = load i32, i32* %move_bits, align 4
  %div63 = sdiv i32 %mul62, %47
  %48 = load i32*, i32** %targ_bits.addr, align 4
  %arrayidx64 = getelementptr inbounds i32, i32* %48, i32 1
  store i32 %div63, i32* %arrayidx64, align 4
  br label %if.end65

if.end65:                                         ; preds = %if.then56, %if.end50
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @athAdjust(float %a, float %x, float %athFloor, float %ATHfixpoint) #0 {
entry:
  %a.addr = alloca float, align 4
  %x.addr = alloca float, align 4
  %athFloor.addr = alloca float, align 4
  %ATHfixpoint.addr = alloca float, align 4
  %o = alloca float, align 4
  %p = alloca float, align 4
  %u = alloca float, align 4
  %v = alloca float, align 4
  %w = alloca float, align 4
  store float %a, float* %a.addr, align 4
  store float %x, float* %x.addr, align 4
  store float %athFloor, float* %athFloor.addr, align 4
  store float %ATHfixpoint, float* %ATHfixpoint.addr, align 4
  store float 0x405693C240000000, float* %o, align 4
  %0 = load float, float* %ATHfixpoint.addr, align 4
  %cmp = fcmp olt float %0, 1.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %1 = load float, float* %ATHfixpoint.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x4057B4C3C0000000, %cond.true ], [ %1, %cond.false ]
  store float %cond, float* %p, align 4
  %2 = load float, float* %x.addr, align 4
  %call = call float @fast_log2(float %2)
  %conv = fpext float %call to double
  %mul = fmul double %conv, 0x4008151824C7587E
  %conv1 = fptrunc double %mul to float
  store float %conv1, float* %u, align 4
  %3 = load float, float* %a.addr, align 4
  %4 = load float, float* %a.addr, align 4
  %mul2 = fmul float %3, %4
  store float %mul2, float* %v, align 4
  store float 0.000000e+00, float* %w, align 4
  %5 = load float, float* %athFloor.addr, align 4
  %6 = load float, float* %u, align 4
  %sub = fsub float %6, %5
  store float %sub, float* %u, align 4
  %7 = load float, float* %v, align 4
  %cmp3 = fcmp ogt float %7, 0x3BC79CA100000000
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %8 = load float, float* %v, align 4
  %call5 = call float @fast_log2(float %8)
  %conv6 = fpext float %call5 to double
  %mul7 = fmul double %conv6, 0x3FA11114663DE4A6
  %add = fadd double 1.000000e+00, %mul7
  %conv8 = fptrunc double %add to float
  store float %conv8, float* %w, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  %9 = load float, float* %w, align 4
  %cmp9 = fcmp olt float %9, 0.000000e+00
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end
  store float 0.000000e+00, float* %w, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then11, %if.end
  %10 = load float, float* %w, align 4
  %11 = load float, float* %u, align 4
  %mul13 = fmul float %11, %10
  store float %mul13, float* %u, align 4
  %12 = load float, float* %athFloor.addr, align 4
  %add14 = fadd float %12, 0x405693C240000000
  %13 = load float, float* %p, align 4
  %sub15 = fsub float %add14, %13
  %14 = load float, float* %u, align 4
  %add16 = fadd float %14, %sub15
  store float %add16, float* %u, align 4
  %15 = load float, float* %u, align 4
  %mul17 = fmul float 0x3FB99999A0000000, %15
  %16 = call float @llvm.pow.f32(float 1.000000e+01, float %mul17)
  ret float %16
}

declare float @fast_log2(float) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @calc_xmin(%struct.lame_internal_flags* %gfc, %struct.III_psy_ratio* %ratio, %struct.gr_info* %cod_info, float* %pxmin) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %ratio.addr = alloca %struct.III_psy_ratio*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %pxmin.addr = alloca float*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %sfb = alloca i32, align 4
  %gsfb = alloca i32, align 4
  %j = alloca i32, align 4
  %ath_over = alloca i32, align 4
  %k = alloca i32, align 4
  %ATH = alloca %struct.ATH_t*, align 4
  %xr = alloca float*, align 4
  %max_nonzero = alloca i32, align 4
  %en0 = alloca float, align 4
  %xmin = alloca float, align 4
  %rh1 = alloca float, align 4
  %rh2 = alloca float, align 4
  %rh3 = alloca float, align 4
  %width = alloca i32, align 4
  %l = alloca i32, align 4
  %xa = alloca float, align 4
  %x2 = alloca float, align 4
  %e = alloca float, align 4
  %x = alloca float, align 4
  %sfb_l = alloca i32, align 4
  %sfb_s = alloca i32, align 4
  %limit = alloca i32, align 4
  %width121 = alloca i32, align 4
  %b = alloca i32, align 4
  %l122 = alloca i32, align 4
  %tmpATH = alloca float, align 4
  %en0138 = alloca float, align 4
  %xmin139 = alloca float, align 4
  %rh1140 = alloca float, align 4
  %rh2141 = alloca float, align 4
  %rh3142 = alloca float, align 4
  %xa149 = alloca float, align 4
  %x2152 = alloca float, align 4
  %e180 = alloca float, align 4
  %x188 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.III_psy_ratio* %ratio, %struct.III_psy_ratio** %ratio.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %pxmin, float** %pxmin.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %ath_over, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 21
  %2 = load %struct.ATH_t*, %struct.ATH_t** %ATH2, align 8
  store %struct.ATH_t* %2, %struct.ATH_t** %ATH, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 0
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %xr3, i32 0, i32 0
  store float* %arraydecay, float** %xr, align 4
  store i32 0, i32* %gsfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc63, %entry
  %4 = load i32, i32* %gsfb, align 4
  %5 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psy_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 21
  %6 = load i32, i32* %psy_lmax, align 4
  %cmp = icmp slt i32 %4, %6
  br i1 %cmp, label %for.body, label %for.end65

for.body:                                         ; preds = %for.cond
  %7 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %adjust_factor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %7, i32 0, i32 2
  %8 = load float, float* %adjust_factor, align 4
  %9 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %l4 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %9, i32 0, i32 6
  %10 = load i32, i32* %gsfb, align 4
  %arrayidx = getelementptr inbounds [22 x float], [22 x float]* %l4, i32 0, i32 %10
  %11 = load float, float* %arrayidx, align 4
  %12 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %floor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %12, i32 0, i32 5
  %13 = load float, float* %floor, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHfixpoint = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 52
  %15 = load float, float* %ATHfixpoint, align 4
  %call = call float @athAdjust(float %8, float %11, float %13, float %15)
  store float %call, float* %xmin, align 4
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 13
  %longfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 0
  %17 = load i32, i32* %gsfb, align 4
  %arrayidx5 = getelementptr inbounds [22 x float], [22 x float]* %longfact, i32 0, i32 %17
  %18 = load float, float* %arrayidx5, align 4
  %19 = load float, float* %xmin, align 4
  %mul = fmul float %19, %18
  store float %mul, float* %xmin, align 4
  %20 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width6 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %20, i32 0, i32 25
  %21 = load i32, i32* %gsfb, align 4
  %arrayidx7 = getelementptr inbounds [39 x i32], [39 x i32]* %width6, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx7, align 4
  store i32 %22, i32* %width, align 4
  %23 = load float, float* %xmin, align 4
  %24 = load i32, i32* %width, align 4
  %conv = sitofp i32 %24 to float
  %div = fdiv float %23, %conv
  store float %div, float* %rh1, align 4
  store float 0x3CB0000000000000, float* %rh2, align 4
  store float 0.000000e+00, float* %en0, align 4
  store i32 0, i32* %l, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %25 = load i32, i32* %l, align 4
  %26 = load i32, i32* %width, align 4
  %cmp9 = icmp slt i32 %25, %26
  br i1 %cmp9, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond8
  %27 = load float*, float** %xr, align 4
  %28 = load i32, i32* %j, align 4
  %inc = add nsw i32 %28, 1
  store i32 %inc, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx12, align 4
  store float %29, float* %xa, align 4
  %30 = load float, float* %xa, align 4
  %31 = load float, float* %xa, align 4
  %mul13 = fmul float %30, %31
  store float %mul13, float* %x2, align 4
  %32 = load float, float* %x2, align 4
  %33 = load float, float* %en0, align 4
  %add = fadd float %33, %32
  store float %add, float* %en0, align 4
  %34 = load float, float* %x2, align 4
  %35 = load float, float* %rh1, align 4
  %cmp14 = fcmp olt float %34, %35
  br i1 %cmp14, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body11
  %36 = load float, float* %x2, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body11
  %37 = load float, float* %rh1, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %36, %cond.true ], [ %37, %cond.false ]
  %38 = load float, float* %rh2, align 4
  %add16 = fadd float %38, %cond
  store float %add16, float* %rh2, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %39 = load i32, i32* %l, align 4
  %inc17 = add nsw i32 %39, 1
  store i32 %inc17, i32* %l, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %40 = load float, float* %en0, align 4
  %41 = load float, float* %xmin, align 4
  %cmp18 = fcmp ogt float %40, %41
  br i1 %cmp18, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %42 = load i32, i32* %ath_over, align 4
  %inc20 = add nsw i32 %42, 1
  store i32 %inc20, i32* %ath_over, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %43 = load float, float* %en0, align 4
  %44 = load float, float* %xmin, align 4
  %cmp21 = fcmp olt float %43, %44
  br i1 %cmp21, label %if.then23, label %if.else

if.then23:                                        ; preds = %if.end
  %45 = load float, float* %en0, align 4
  store float %45, float* %rh3, align 4
  br label %if.end29

if.else:                                          ; preds = %if.end
  %46 = load float, float* %rh2, align 4
  %47 = load float, float* %xmin, align 4
  %cmp24 = fcmp olt float %46, %47
  br i1 %cmp24, label %if.then26, label %if.else27

if.then26:                                        ; preds = %if.else
  %48 = load float, float* %xmin, align 4
  store float %48, float* %rh3, align 4
  br label %if.end28

if.else27:                                        ; preds = %if.else
  %49 = load float, float* %rh2, align 4
  store float %49, float* %rh3, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.else27, %if.then26
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then23
  %50 = load float, float* %rh3, align 4
  store float %50, float* %xmin, align 4
  %51 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %51, i32 0, i32 1
  %l30 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en, i32 0, i32 0
  %52 = load i32, i32* %gsfb, align 4
  %arrayidx31 = getelementptr inbounds [22 x float], [22 x float]* %l30, i32 0, i32 %52
  %53 = load float, float* %arrayidx31, align 4
  store float %53, float* %e, align 4
  %54 = load float, float* %e, align 4
  %cmp32 = fcmp ogt float %54, 0x3D71979980000000
  br i1 %cmp32, label %if.then34, label %if.end47

if.then34:                                        ; preds = %if.end29
  %55 = load float, float* %en0, align 4
  %56 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %56, i32 0, i32 0
  %l35 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm, i32 0, i32 0
  %57 = load i32, i32* %gsfb, align 4
  %arrayidx36 = getelementptr inbounds [22 x float], [22 x float]* %l35, i32 0, i32 %57
  %58 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %55, %58
  %59 = load float, float* %e, align 4
  %div38 = fdiv float %mul37, %59
  store float %div38, float* %x, align 4
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt39 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %60, i32 0, i32 13
  %longfact40 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt39, i32 0, i32 0
  %61 = load i32, i32* %gsfb, align 4
  %arrayidx41 = getelementptr inbounds [22 x float], [22 x float]* %longfact40, i32 0, i32 %61
  %62 = load float, float* %arrayidx41, align 4
  %63 = load float, float* %x, align 4
  %mul42 = fmul float %63, %62
  store float %mul42, float* %x, align 4
  %64 = load float, float* %xmin, align 4
  %65 = load float, float* %x, align 4
  %cmp43 = fcmp olt float %64, %65
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.then34
  %66 = load float, float* %x, align 4
  store float %66, float* %xmin, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.then34
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.end29
  %67 = load float, float* %xmin, align 4
  %conv48 = fpext float %67 to double
  %cmp49 = fcmp ogt double %conv48, 0x3CB0000000000000
  br i1 %cmp49, label %cond.true51, label %cond.false53

cond.true51:                                      ; preds = %if.end47
  %68 = load float, float* %xmin, align 4
  %conv52 = fpext float %68 to double
  br label %cond.end54

cond.false53:                                     ; preds = %if.end47
  br label %cond.end54

cond.end54:                                       ; preds = %cond.false53, %cond.true51
  %cond55 = phi double [ %conv52, %cond.true51 ], [ 0x3CB0000000000000, %cond.false53 ]
  %conv56 = fptrunc double %cond55 to float
  store float %conv56, float* %xmin, align 4
  %69 = load float, float* %en0, align 4
  %70 = load float, float* %xmin, align 4
  %add57 = fadd float %70, 0x3D06849B80000000
  %cmp58 = fcmp ogt float %69, %add57
  %71 = zext i1 %cmp58 to i64
  %cond60 = select i1 %cmp58, i32 1, i32 0
  %conv61 = trunc i32 %cond60 to i8
  %72 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %energy_above_cutoff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %72, i32 0, i32 31
  %73 = load i32, i32* %gsfb, align 4
  %arrayidx62 = getelementptr inbounds [39 x i8], [39 x i8]* %energy_above_cutoff, i32 0, i32 %73
  store i8 %conv61, i8* %arrayidx62, align 1
  %74 = load float, float* %xmin, align 4
  %75 = load float*, float** %pxmin.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %75, i32 1
  store float* %incdec.ptr, float** %pxmin.addr, align 4
  store float %74, float* %75, align 4
  br label %for.inc63

for.inc63:                                        ; preds = %cond.end54
  %76 = load i32, i32* %gsfb, align 4
  %inc64 = add nsw i32 %76, 1
  store i32 %inc64, i32* %gsfb, align 4
  br label %for.cond

for.end65:                                        ; preds = %for.cond
  store i32 0, i32* %max_nonzero, align 4
  store i32 575, i32* %k, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc76, %for.end65
  %77 = load i32, i32* %k, align 4
  %cmp67 = icmp sgt i32 %77, 0
  br i1 %cmp67, label %for.body69, label %for.end77

for.body69:                                       ; preds = %for.cond66
  %78 = load float*, float** %xr, align 4
  %79 = load i32, i32* %k, align 4
  %arrayidx70 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx70, align 4
  %conv71 = fpext float %80 to double
  %81 = call double @llvm.fabs.f64(double %conv71)
  %cmp72 = fcmp ogt double %81, 0x3D71979980000000
  br i1 %cmp72, label %if.then74, label %if.end75

if.then74:                                        ; preds = %for.body69
  %82 = load i32, i32* %k, align 4
  store i32 %82, i32* %max_nonzero, align 4
  br label %for.end77

if.end75:                                         ; preds = %for.body69
  br label %for.inc76

for.inc76:                                        ; preds = %if.end75
  %83 = load i32, i32* %k, align 4
  %dec = add nsw i32 %83, -1
  store i32 %dec, i32* %k, align 4
  br label %for.cond66

for.end77:                                        ; preds = %if.then74, %for.cond66
  %84 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %84, i32 0, i32 9
  %85 = load i32, i32* %block_type, align 4
  %cmp78 = icmp ne i32 %85, 2
  br i1 %cmp78, label %if.then80, label %if.else81

if.then80:                                        ; preds = %for.end77
  %86 = load i32, i32* %max_nonzero, align 4
  %or = or i32 %86, 1
  store i32 %or, i32* %max_nonzero, align 4
  br label %if.end85

if.else81:                                        ; preds = %for.end77
  %87 = load i32, i32* %max_nonzero, align 4
  %div82 = sdiv i32 %87, 6
  store i32 %div82, i32* %max_nonzero, align 4
  %88 = load i32, i32* %max_nonzero, align 4
  %mul83 = mul nsw i32 %88, 6
  store i32 %mul83, i32* %max_nonzero, align 4
  %89 = load i32, i32* %max_nonzero, align 4
  %add84 = add nsw i32 %89, 5
  store i32 %add84, i32* %max_nonzero, align 4
  br label %if.end85

if.end85:                                         ; preds = %if.else81, %if.then80
  %90 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt86 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %90, i32 0, i32 13
  %sfb21_extra = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt86, i32 0, i32 8
  %91 = load i32, i32* %sfb21_extra, align 4
  %cmp87 = icmp eq i32 %91, 0
  br i1 %cmp87, label %land.lhs.true, label %if.end116

land.lhs.true:                                    ; preds = %if.end85
  %92 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %92, i32 0, i32 12
  %93 = load i32, i32* %samplerate_out, align 4
  %cmp89 = icmp slt i32 %93, 44000
  br i1 %cmp89, label %if.then91, label %if.end116

if.then91:                                        ; preds = %land.lhs.true
  %94 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out92 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %94, i32 0, i32 12
  %95 = load i32, i32* %samplerate_out92, align 4
  %cmp93 = icmp sle i32 %95, 8000
  %96 = zext i1 %cmp93 to i64
  %cond95 = select i1 %cmp93, i32 17, i32 21
  store i32 %cond95, i32* %sfb_l, align 4
  %97 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out96 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %97, i32 0, i32 12
  %98 = load i32, i32* %samplerate_out96, align 4
  %cmp97 = icmp sle i32 %98, 8000
  %99 = zext i1 %cmp97 to i64
  %cond99 = select i1 %cmp97, i32 9, i32 12
  store i32 %cond99, i32* %sfb_s, align 4
  store i32 575, i32* %limit, align 4
  %100 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type100 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %100, i32 0, i32 9
  %101 = load i32, i32* %block_type100, align 4
  %cmp101 = icmp ne i32 %101, 2
  br i1 %cmp101, label %if.then103, label %if.else106

if.then103:                                       ; preds = %if.then91
  %102 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %102, i32 0, i32 8
  %l104 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %103 = load i32, i32* %sfb_l, align 4
  %arrayidx105 = getelementptr inbounds [23 x i32], [23 x i32]* %l104, i32 0, i32 %103
  %104 = load i32, i32* %arrayidx105, align 4
  %sub = sub nsw i32 %104, 1
  store i32 %sub, i32* %limit, align 4
  br label %if.end111

if.else106:                                       ; preds = %if.then91
  %105 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band107 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %105, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band107, i32 0, i32 1
  %106 = load i32, i32* %sfb_s, align 4
  %arrayidx108 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 %106
  %107 = load i32, i32* %arrayidx108, align 4
  %mul109 = mul nsw i32 3, %107
  %sub110 = sub nsw i32 %mul109, 1
  store i32 %sub110, i32* %limit, align 4
  br label %if.end111

if.end111:                                        ; preds = %if.else106, %if.then103
  %108 = load i32, i32* %max_nonzero, align 4
  %109 = load i32, i32* %limit, align 4
  %cmp112 = icmp sgt i32 %108, %109
  br i1 %cmp112, label %if.then114, label %if.end115

if.then114:                                       ; preds = %if.end111
  %110 = load i32, i32* %limit, align 4
  store i32 %110, i32* %max_nonzero, align 4
  br label %if.end115

if.end115:                                        ; preds = %if.then114, %if.end111
  br label %if.end116

if.end116:                                        ; preds = %if.end115, %land.lhs.true, %if.end85
  %111 = load i32, i32* %max_nonzero, align 4
  %112 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %112, i32 0, i32 30
  store i32 %111, i32* %max_nonzero_coeff, align 4
  %113 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin = getelementptr inbounds %struct.gr_info, %struct.gr_info* %113, i32 0, i32 20
  %114 = load i32, i32* %sfb_smin, align 4
  store i32 %114, i32* %sfb, align 4
  br label %for.cond117

for.cond117:                                      ; preds = %for.inc253, %if.end116
  %115 = load i32, i32* %gsfb, align 4
  %116 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %116, i32 0, i32 23
  %117 = load i32, i32* %psymax, align 4
  %cmp118 = icmp slt i32 %115, %117
  br i1 %cmp118, label %for.body120, label %for.end256

for.body120:                                      ; preds = %for.cond117
  %118 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %adjust_factor123 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %118, i32 0, i32 2
  %119 = load float, float* %adjust_factor123, align 4
  %120 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %s124 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %120, i32 0, i32 7
  %121 = load i32, i32* %sfb, align 4
  %arrayidx125 = getelementptr inbounds [13 x float], [13 x float]* %s124, i32 0, i32 %121
  %122 = load float, float* %arrayidx125, align 4
  %123 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %floor126 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %123, i32 0, i32 5
  %124 = load float, float* %floor126, align 4
  %125 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHfixpoint127 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %125, i32 0, i32 52
  %126 = load float, float* %ATHfixpoint127, align 4
  %call128 = call float @athAdjust(float %119, float %122, float %124, float %126)
  store float %call128, float* %tmpATH, align 4
  %127 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt129 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %127, i32 0, i32 13
  %shortfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt129, i32 0, i32 1
  %128 = load i32, i32* %sfb, align 4
  %arrayidx130 = getelementptr inbounds [13 x float], [13 x float]* %shortfact, i32 0, i32 %128
  %129 = load float, float* %arrayidx130, align 4
  %130 = load float, float* %tmpATH, align 4
  %mul131 = fmul float %130, %129
  store float %mul131, float* %tmpATH, align 4
  %131 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width132 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %131, i32 0, i32 25
  %132 = load i32, i32* %gsfb, align 4
  %arrayidx133 = getelementptr inbounds [39 x i32], [39 x i32]* %width132, i32 0, i32 %132
  %133 = load i32, i32* %arrayidx133, align 4
  store i32 %133, i32* %width121, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond134

for.cond134:                                      ; preds = %for.inc222, %for.body120
  %134 = load i32, i32* %b, align 4
  %cmp135 = icmp slt i32 %134, 3
  br i1 %cmp135, label %for.body137, label %for.end224

for.body137:                                      ; preds = %for.cond134
  store float 0.000000e+00, float* %en0138, align 4
  %135 = load float, float* %tmpATH, align 4
  store float %135, float* %xmin139, align 4
  %136 = load float, float* %tmpATH, align 4
  %137 = load i32, i32* %width121, align 4
  %conv143 = sitofp i32 %137 to float
  %div144 = fdiv float %136, %conv143
  store float %div144, float* %rh1140, align 4
  store float 0x3CB0000000000000, float* %rh2141, align 4
  store i32 0, i32* %l122, align 4
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc162, %for.body137
  %138 = load i32, i32* %l122, align 4
  %139 = load i32, i32* %width121, align 4
  %cmp146 = icmp slt i32 %138, %139
  br i1 %cmp146, label %for.body148, label %for.end164

for.body148:                                      ; preds = %for.cond145
  %140 = load float*, float** %xr, align 4
  %141 = load i32, i32* %j, align 4
  %inc150 = add nsw i32 %141, 1
  store i32 %inc150, i32* %j, align 4
  %arrayidx151 = getelementptr inbounds float, float* %140, i32 %141
  %142 = load float, float* %arrayidx151, align 4
  store float %142, float* %xa149, align 4
  %143 = load float, float* %xa149, align 4
  %144 = load float, float* %xa149, align 4
  %mul153 = fmul float %143, %144
  store float %mul153, float* %x2152, align 4
  %145 = load float, float* %x2152, align 4
  %146 = load float, float* %en0138, align 4
  %add154 = fadd float %146, %145
  store float %add154, float* %en0138, align 4
  %147 = load float, float* %x2152, align 4
  %148 = load float, float* %rh1140, align 4
  %cmp155 = fcmp olt float %147, %148
  br i1 %cmp155, label %cond.true157, label %cond.false158

cond.true157:                                     ; preds = %for.body148
  %149 = load float, float* %x2152, align 4
  br label %cond.end159

cond.false158:                                    ; preds = %for.body148
  %150 = load float, float* %rh1140, align 4
  br label %cond.end159

cond.end159:                                      ; preds = %cond.false158, %cond.true157
  %cond160 = phi float [ %149, %cond.true157 ], [ %150, %cond.false158 ]
  %151 = load float, float* %rh2141, align 4
  %add161 = fadd float %151, %cond160
  store float %add161, float* %rh2141, align 4
  br label %for.inc162

for.inc162:                                       ; preds = %cond.end159
  %152 = load i32, i32* %l122, align 4
  %inc163 = add nsw i32 %152, 1
  store i32 %inc163, i32* %l122, align 4
  br label %for.cond145

for.end164:                                       ; preds = %for.cond145
  %153 = load float, float* %en0138, align 4
  %154 = load float, float* %tmpATH, align 4
  %cmp165 = fcmp ogt float %153, %154
  br i1 %cmp165, label %if.then167, label %if.end169

if.then167:                                       ; preds = %for.end164
  %155 = load i32, i32* %ath_over, align 4
  %inc168 = add nsw i32 %155, 1
  store i32 %inc168, i32* %ath_over, align 4
  br label %if.end169

if.end169:                                        ; preds = %if.then167, %for.end164
  %156 = load float, float* %en0138, align 4
  %157 = load float, float* %tmpATH, align 4
  %cmp170 = fcmp olt float %156, %157
  br i1 %cmp170, label %if.then172, label %if.else173

if.then172:                                       ; preds = %if.end169
  %158 = load float, float* %en0138, align 4
  store float %158, float* %rh3142, align 4
  br label %if.end179

if.else173:                                       ; preds = %if.end169
  %159 = load float, float* %rh2141, align 4
  %160 = load float, float* %tmpATH, align 4
  %cmp174 = fcmp olt float %159, %160
  br i1 %cmp174, label %if.then176, label %if.else177

if.then176:                                       ; preds = %if.else173
  %161 = load float, float* %tmpATH, align 4
  store float %161, float* %rh3142, align 4
  br label %if.end178

if.else177:                                       ; preds = %if.else173
  %162 = load float, float* %rh2141, align 4
  store float %162, float* %rh3142, align 4
  br label %if.end178

if.end178:                                        ; preds = %if.else177, %if.then176
  br label %if.end179

if.end179:                                        ; preds = %if.end178, %if.then172
  %163 = load float, float* %rh3142, align 4
  store float %163, float* %xmin139, align 4
  %164 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en181 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %164, i32 0, i32 1
  %s182 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en181, i32 0, i32 1
  %165 = load i32, i32* %sfb, align 4
  %arrayidx183 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s182, i32 0, i32 %165
  %166 = load i32, i32* %b, align 4
  %arrayidx184 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx183, i32 0, i32 %166
  %167 = load float, float* %arrayidx184, align 4
  store float %167, float* %e180, align 4
  %168 = load float, float* %e180, align 4
  %cmp185 = fcmp ogt float %168, 0x3D71979980000000
  br i1 %cmp185, label %if.then187, label %if.end203

if.then187:                                       ; preds = %if.end179
  %169 = load float, float* %en0138, align 4
  %170 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm189 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %170, i32 0, i32 0
  %s190 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm189, i32 0, i32 1
  %171 = load i32, i32* %sfb, align 4
  %arrayidx191 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s190, i32 0, i32 %171
  %172 = load i32, i32* %b, align 4
  %arrayidx192 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx191, i32 0, i32 %172
  %173 = load float, float* %arrayidx192, align 4
  %mul193 = fmul float %169, %173
  %174 = load float, float* %e180, align 4
  %div194 = fdiv float %mul193, %174
  store float %div194, float* %x188, align 4
  %175 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt195 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %175, i32 0, i32 13
  %shortfact196 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt195, i32 0, i32 1
  %176 = load i32, i32* %sfb, align 4
  %arrayidx197 = getelementptr inbounds [13 x float], [13 x float]* %shortfact196, i32 0, i32 %176
  %177 = load float, float* %arrayidx197, align 4
  %178 = load float, float* %x188, align 4
  %mul198 = fmul float %178, %177
  store float %mul198, float* %x188, align 4
  %179 = load float, float* %xmin139, align 4
  %180 = load float, float* %x188, align 4
  %cmp199 = fcmp olt float %179, %180
  br i1 %cmp199, label %if.then201, label %if.end202

if.then201:                                       ; preds = %if.then187
  %181 = load float, float* %x188, align 4
  store float %181, float* %xmin139, align 4
  br label %if.end202

if.end202:                                        ; preds = %if.then201, %if.then187
  br label %if.end203

if.end203:                                        ; preds = %if.end202, %if.end179
  %182 = load float, float* %xmin139, align 4
  %conv204 = fpext float %182 to double
  %cmp205 = fcmp ogt double %conv204, 0x3CB0000000000000
  br i1 %cmp205, label %cond.true207, label %cond.false209

cond.true207:                                     ; preds = %if.end203
  %183 = load float, float* %xmin139, align 4
  %conv208 = fpext float %183 to double
  br label %cond.end210

cond.false209:                                    ; preds = %if.end203
  br label %cond.end210

cond.end210:                                      ; preds = %cond.false209, %cond.true207
  %cond211 = phi double [ %conv208, %cond.true207 ], [ 0x3CB0000000000000, %cond.false209 ]
  %conv212 = fptrunc double %cond211 to float
  store float %conv212, float* %xmin139, align 4
  %184 = load float, float* %en0138, align 4
  %185 = load float, float* %xmin139, align 4
  %add213 = fadd float %185, 0x3D06849B80000000
  %cmp214 = fcmp ogt float %184, %add213
  %186 = zext i1 %cmp214 to i64
  %cond216 = select i1 %cmp214, i32 1, i32 0
  %conv217 = trunc i32 %cond216 to i8
  %187 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %energy_above_cutoff218 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %187, i32 0, i32 31
  %188 = load i32, i32* %gsfb, align 4
  %189 = load i32, i32* %b, align 4
  %add219 = add nsw i32 %188, %189
  %arrayidx220 = getelementptr inbounds [39 x i8], [39 x i8]* %energy_above_cutoff218, i32 0, i32 %add219
  store i8 %conv217, i8* %arrayidx220, align 1
  %190 = load float, float* %xmin139, align 4
  %191 = load float*, float** %pxmin.addr, align 4
  %incdec.ptr221 = getelementptr inbounds float, float* %191, i32 1
  store float* %incdec.ptr221, float** %pxmin.addr, align 4
  store float %190, float* %191, align 4
  br label %for.inc222

for.inc222:                                       ; preds = %cond.end210
  %192 = load i32, i32* %b, align 4
  %inc223 = add nsw i32 %192, 1
  store i32 %inc223, i32* %b, align 4
  br label %for.cond134

for.end224:                                       ; preds = %for.cond134
  %193 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_temporal_masking_effect = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %193, i32 0, i32 19
  %194 = load i32, i32* %use_temporal_masking_effect, align 4
  %tobool = icmp ne i32 %194, 0
  br i1 %tobool, label %if.then225, label %if.end252

if.then225:                                       ; preds = %for.end224
  %195 = load float*, float** %pxmin.addr, align 4
  %arrayidx226 = getelementptr inbounds float, float* %195, i32 -3
  %196 = load float, float* %arrayidx226, align 4
  %197 = load float*, float** %pxmin.addr, align 4
  %arrayidx227 = getelementptr inbounds float, float* %197, i32 -2
  %198 = load float, float* %arrayidx227, align 4
  %cmp228 = fcmp ogt float %196, %198
  br i1 %cmp228, label %if.then230, label %if.end237

if.then230:                                       ; preds = %if.then225
  %199 = load float*, float** %pxmin.addr, align 4
  %arrayidx231 = getelementptr inbounds float, float* %199, i32 -3
  %200 = load float, float* %arrayidx231, align 4
  %201 = load float*, float** %pxmin.addr, align 4
  %arrayidx232 = getelementptr inbounds float, float* %201, i32 -2
  %202 = load float, float* %arrayidx232, align 4
  %sub233 = fsub float %200, %202
  %203 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %203, i32 0, i32 22
  %204 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %decay = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %204, i32 0, i32 6
  %205 = load float, float* %decay, align 4
  %mul234 = fmul float %sub233, %205
  %206 = load float*, float** %pxmin.addr, align 4
  %arrayidx235 = getelementptr inbounds float, float* %206, i32 -2
  %207 = load float, float* %arrayidx235, align 4
  %add236 = fadd float %207, %mul234
  store float %add236, float* %arrayidx235, align 4
  br label %if.end237

if.end237:                                        ; preds = %if.then230, %if.then225
  %208 = load float*, float** %pxmin.addr, align 4
  %arrayidx238 = getelementptr inbounds float, float* %208, i32 -2
  %209 = load float, float* %arrayidx238, align 4
  %210 = load float*, float** %pxmin.addr, align 4
  %arrayidx239 = getelementptr inbounds float, float* %210, i32 -1
  %211 = load float, float* %arrayidx239, align 4
  %cmp240 = fcmp ogt float %209, %211
  br i1 %cmp240, label %if.then242, label %if.end251

if.then242:                                       ; preds = %if.end237
  %212 = load float*, float** %pxmin.addr, align 4
  %arrayidx243 = getelementptr inbounds float, float* %212, i32 -2
  %213 = load float, float* %arrayidx243, align 4
  %214 = load float*, float** %pxmin.addr, align 4
  %arrayidx244 = getelementptr inbounds float, float* %214, i32 -1
  %215 = load float, float* %arrayidx244, align 4
  %sub245 = fsub float %213, %215
  %216 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy246 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %216, i32 0, i32 22
  %217 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy246, align 4
  %decay247 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %217, i32 0, i32 6
  %218 = load float, float* %decay247, align 4
  %mul248 = fmul float %sub245, %218
  %219 = load float*, float** %pxmin.addr, align 4
  %arrayidx249 = getelementptr inbounds float, float* %219, i32 -1
  %220 = load float, float* %arrayidx249, align 4
  %add250 = fadd float %220, %mul248
  store float %add250, float* %arrayidx249, align 4
  br label %if.end251

if.end251:                                        ; preds = %if.then242, %if.end237
  br label %if.end252

if.end252:                                        ; preds = %if.end251, %for.end224
  br label %for.inc253

for.inc253:                                       ; preds = %if.end252
  %221 = load i32, i32* %sfb, align 4
  %inc254 = add nsw i32 %221, 1
  store i32 %inc254, i32* %sfb, align 4
  %222 = load i32, i32* %gsfb, align 4
  %add255 = add nsw i32 %222, 3
  store i32 %add255, i32* %gsfb, align 4
  br label %for.cond117

for.end256:                                       ; preds = %for.cond117
  %223 = load i32, i32* %ath_over, align 4
  ret i32 %223
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @calc_noise(%struct.gr_info* %cod_info, float* %l3_xmin, float* %distort, %struct.calc_noise_result_t* %res, %struct.calc_noise_data_t* %prev_noise) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %l3_xmin.addr = alloca float*, align 4
  %distort.addr = alloca float*, align 4
  %res.addr = alloca %struct.calc_noise_result_t*, align 4
  %prev_noise.addr = alloca %struct.calc_noise_data_t*, align 4
  %sfb = alloca i32, align 4
  %l = alloca i32, align 4
  %over = alloca i32, align 4
  %over_noise_db = alloca float, align 4
  %tot_noise_db = alloca float, align 4
  %max_noise = alloca float, align 4
  %j = alloca i32, align 4
  %scalefac = alloca i32*, align 4
  %s = alloca i32, align 4
  %r_l3_xmin = alloca float, align 4
  %distort_ = alloca float, align 4
  %noise = alloca float, align 4
  %step16 = alloca float, align 4
  %usefullsize = alloca i32, align 4
  %tmp = alloca i32, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %l3_xmin, float** %l3_xmin.addr, align 4
  store float* %distort, float** %distort.addr, align 4
  store %struct.calc_noise_result_t* %res, %struct.calc_noise_result_t** %res.addr, align 4
  store %struct.calc_noise_data_t* %prev_noise, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  store i32 0, i32* %over, align 4
  store float 0.000000e+00, float* %over_noise_db, align 4
  store float 0.000000e+00, float* %tot_noise_db, align 4
  store float -2.000000e+01, float* %max_noise, align 4
  store i32 0, i32* %j, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac1, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  %1 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %over_SSD = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %1, i32 0, i32 4
  store i32 0, i32* %over_SSD, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %sfb, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 23
  %4 = load i32, i32* %psymax, align 4
  %cmp = icmp slt i32 %2, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 7
  %6 = load i32, i32* %global_gain, align 4
  %7 = load i32*, i32** %scalefac, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %7, i32 1
  store i32* %incdec.ptr, i32** %scalefac, align 4
  %8 = load i32, i32* %7, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 15
  %10 = load i32, i32* %preflag, align 4
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %11 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %12, %cond.true ], [ 0, %cond.false ]
  %add = add nsw i32 %8, %cond
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 16
  %14 = load i32, i32* %scalefac_scale, align 4
  %add2 = add nsw i32 %14, 1
  %shl = shl i32 %add, %add2
  %sub = sub nsw i32 %6, %shl
  %15 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 12
  %16 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 26
  %17 = load i32, i32* %sfb, align 4
  %arrayidx3 = getelementptr inbounds [39 x i32], [39 x i32]* %window, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx3, align 4
  %arrayidx4 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx4, align 4
  %mul = mul nsw i32 %19, 8
  %sub5 = sub nsw i32 %sub, %mul
  store i32 %sub5, i32* %s, align 4
  %20 = load float*, float** %l3_xmin.addr, align 4
  %incdec.ptr6 = getelementptr inbounds float, float* %20, i32 1
  store float* %incdec.ptr6, float** %l3_xmin.addr, align 4
  %21 = load float, float* %20, align 4
  %div = fdiv float 1.000000e+00, %21
  store float %div, float* %r_l3_xmin, align 4
  store float 0.000000e+00, float* %distort_, align 4
  store float 0.000000e+00, float* %noise, align 4
  %22 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool7 = icmp ne %struct.calc_noise_data_t* %22, null
  br i1 %tobool7, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %cond.end
  %23 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %step = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %23, i32 0, i32 2
  %24 = load i32, i32* %sfb, align 4
  %arrayidx8 = getelementptr inbounds [39 x i32], [39 x i32]* %step, i32 0, i32 %24
  %25 = load i32, i32* %arrayidx8, align 4
  %26 = load i32, i32* %s, align 4
  %cmp9 = icmp eq i32 %25, %26
  br i1 %cmp9, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %27 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width = getelementptr inbounds %struct.gr_info, %struct.gr_info* %27, i32 0, i32 25
  %28 = load i32, i32* %sfb, align 4
  %arrayidx10 = getelementptr inbounds [39 x i32], [39 x i32]* %width, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx10, align 4
  %30 = load i32, i32* %j, align 4
  %add11 = add nsw i32 %30, %29
  store i32 %add11, i32* %j, align 4
  %31 = load float, float* %r_l3_xmin, align 4
  %32 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %noise12 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %32, i32 0, i32 3
  %33 = load i32, i32* %sfb, align 4
  %arrayidx13 = getelementptr inbounds [39 x float], [39 x float]* %noise12, i32 0, i32 %33
  %34 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %31, %34
  store float %mul14, float* %distort_, align 4
  %35 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %noise_log = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %35, i32 0, i32 4
  %36 = load i32, i32* %sfb, align 4
  %arrayidx15 = getelementptr inbounds [39 x float], [39 x float]* %noise_log, i32 0, i32 %36
  %37 = load float, float* %arrayidx15, align 4
  store float %37, float* %noise, align 4
  br label %if.end55

if.else:                                          ; preds = %land.lhs.true, %cond.end
  %38 = load i32, i32* %s, align 4
  %add17 = add nsw i32 %38, 116
  %arrayidx18 = getelementptr inbounds [374 x float], [374 x float]* @pow20, i32 0, i32 %add17
  %39 = load float, float* %arrayidx18, align 4
  store float %39, float* %step16, align 4
  %40 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width19 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %40, i32 0, i32 25
  %41 = load i32, i32* %sfb, align 4
  %arrayidx20 = getelementptr inbounds [39 x i32], [39 x i32]* %width19, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx20, align 4
  %shr = ashr i32 %42, 1
  store i32 %shr, i32* %l, align 4
  %43 = load i32, i32* %j, align 4
  %44 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width21 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %44, i32 0, i32 25
  %45 = load i32, i32* %sfb, align 4
  %arrayidx22 = getelementptr inbounds [39 x i32], [39 x i32]* %width21, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx22, align 4
  %add23 = add nsw i32 %43, %46
  %47 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 30
  %48 = load i32, i32* %max_nonzero_coeff, align 4
  %cmp24 = icmp sgt i32 %add23, %48
  br i1 %cmp24, label %if.then25, label %if.end33

if.then25:                                        ; preds = %if.else
  %49 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff26 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %49, i32 0, i32 30
  %50 = load i32, i32* %max_nonzero_coeff26, align 4
  %51 = load i32, i32* %j, align 4
  %sub27 = sub nsw i32 %50, %51
  %add28 = add nsw i32 %sub27, 1
  store i32 %add28, i32* %usefullsize, align 4
  %52 = load i32, i32* %usefullsize, align 4
  %cmp29 = icmp sgt i32 %52, 0
  br i1 %cmp29, label %if.then30, label %if.else32

if.then30:                                        ; preds = %if.then25
  %53 = load i32, i32* %usefullsize, align 4
  %shr31 = ashr i32 %53, 1
  store i32 %shr31, i32* %l, align 4
  br label %if.end

if.else32:                                        ; preds = %if.then25
  store i32 0, i32* %l, align 4
  br label %if.end

if.end:                                           ; preds = %if.else32, %if.then30
  br label %if.end33

if.end33:                                         ; preds = %if.end, %if.else
  %54 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %55 = load i32, i32* %l, align 4
  %56 = load float, float* %step16, align 4
  %call = call float @calc_noise_core_c(%struct.gr_info* %54, i32* %j, i32 %55, float %56)
  store float %call, float* %noise, align 4
  %57 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool34 = icmp ne %struct.calc_noise_data_t* %57, null
  br i1 %tobool34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.end33
  %58 = load i32, i32* %s, align 4
  %59 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %step36 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %59, i32 0, i32 2
  %60 = load i32, i32* %sfb, align 4
  %arrayidx37 = getelementptr inbounds [39 x i32], [39 x i32]* %step36, i32 0, i32 %60
  store i32 %58, i32* %arrayidx37, align 4
  %61 = load float, float* %noise, align 4
  %62 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %noise38 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %62, i32 0, i32 3
  %63 = load i32, i32* %sfb, align 4
  %arrayidx39 = getelementptr inbounds [39 x float], [39 x float]* %noise38, i32 0, i32 %63
  store float %61, float* %arrayidx39, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then35, %if.end33
  %64 = load float, float* %r_l3_xmin, align 4
  %65 = load float, float* %noise, align 4
  %mul41 = fmul float %64, %65
  store float %mul41, float* %distort_, align 4
  %66 = load float, float* %distort_, align 4
  %cmp42 = fcmp ogt float %66, 0x3BC79CA100000000
  br i1 %cmp42, label %cond.true43, label %cond.false44

cond.true43:                                      ; preds = %if.end40
  %67 = load float, float* %distort_, align 4
  br label %cond.end45

cond.false44:                                     ; preds = %if.end40
  br label %cond.end45

cond.end45:                                       ; preds = %cond.false44, %cond.true43
  %cond46 = phi float [ %67, %cond.true43 ], [ 0x3BC79CA100000000, %cond.false44 ]
  %call47 = call float @fast_log2(float %cond46)
  %conv = fpext float %call47 to double
  %mul48 = fmul double %conv, 0x3FD34413509F79FE
  %conv49 = fptrunc double %mul48 to float
  store float %conv49, float* %noise, align 4
  %68 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool50 = icmp ne %struct.calc_noise_data_t* %68, null
  br i1 %tobool50, label %if.then51, label %if.end54

if.then51:                                        ; preds = %cond.end45
  %69 = load float, float* %noise, align 4
  %70 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %noise_log52 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %70, i32 0, i32 4
  %71 = load i32, i32* %sfb, align 4
  %arrayidx53 = getelementptr inbounds [39 x float], [39 x float]* %noise_log52, i32 0, i32 %71
  store float %69, float* %arrayidx53, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.then51, %cond.end45
  br label %if.end55

if.end55:                                         ; preds = %if.end54, %if.then
  %72 = load float, float* %distort_, align 4
  %73 = load float*, float** %distort.addr, align 4
  %incdec.ptr56 = getelementptr inbounds float, float* %73, i32 1
  store float* %incdec.ptr56, float** %distort.addr, align 4
  store float %72, float* %73, align 4
  %74 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %tobool57 = icmp ne %struct.calc_noise_data_t* %74, null
  br i1 %tobool57, label %if.then58, label %if.end61

if.then58:                                        ; preds = %if.end55
  %75 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain59 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %75, i32 0, i32 7
  %76 = load i32, i32* %global_gain59, align 4
  %77 = load %struct.calc_noise_data_t*, %struct.calc_noise_data_t** %prev_noise.addr, align 4
  %global_gain60 = getelementptr inbounds %struct.calc_noise_data_t, %struct.calc_noise_data_t* %77, i32 0, i32 0
  store i32 %76, i32* %global_gain60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.then58, %if.end55
  %78 = load float, float* %noise, align 4
  %79 = load float, float* %tot_noise_db, align 4
  %add62 = fadd float %79, %78
  store float %add62, float* %tot_noise_db, align 4
  %80 = load float, float* %noise, align 4
  %conv63 = fpext float %80 to double
  %cmp64 = fcmp ogt double %conv63, 0.000000e+00
  br i1 %cmp64, label %if.then66, label %if.end85

if.then66:                                        ; preds = %if.end61
  %81 = load float, float* %noise, align 4
  %mul67 = fmul float %81, 1.000000e+01
  %conv68 = fpext float %mul67 to double
  %add69 = fadd double %conv68, 5.000000e-01
  %conv70 = fptosi double %add69 to i32
  %cmp71 = icmp sgt i32 %conv70, 1
  br i1 %cmp71, label %cond.true73, label %cond.false78

cond.true73:                                      ; preds = %if.then66
  %82 = load float, float* %noise, align 4
  %mul74 = fmul float %82, 1.000000e+01
  %conv75 = fpext float %mul74 to double
  %add76 = fadd double %conv75, 5.000000e-01
  %conv77 = fptosi double %add76 to i32
  br label %cond.end79

cond.false78:                                     ; preds = %if.then66
  br label %cond.end79

cond.end79:                                       ; preds = %cond.false78, %cond.true73
  %cond80 = phi i32 [ %conv77, %cond.true73 ], [ 1, %cond.false78 ]
  store i32 %cond80, i32* %tmp, align 4
  %83 = load i32, i32* %tmp, align 4
  %84 = load i32, i32* %tmp, align 4
  %mul81 = mul nsw i32 %83, %84
  %85 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %over_SSD82 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %85, i32 0, i32 4
  %86 = load i32, i32* %over_SSD82, align 4
  %add83 = add nsw i32 %86, %mul81
  store i32 %add83, i32* %over_SSD82, align 4
  %87 = load i32, i32* %over, align 4
  %inc = add nsw i32 %87, 1
  store i32 %inc, i32* %over, align 4
  %88 = load float, float* %noise, align 4
  %89 = load float, float* %over_noise_db, align 4
  %add84 = fadd float %89, %88
  store float %add84, float* %over_noise_db, align 4
  br label %if.end85

if.end85:                                         ; preds = %cond.end79, %if.end61
  %90 = load float, float* %max_noise, align 4
  %91 = load float, float* %noise, align 4
  %cmp86 = fcmp ogt float %90, %91
  br i1 %cmp86, label %cond.true88, label %cond.false89

cond.true88:                                      ; preds = %if.end85
  %92 = load float, float* %max_noise, align 4
  br label %cond.end90

cond.false89:                                     ; preds = %if.end85
  %93 = load float, float* %noise, align 4
  br label %cond.end90

cond.end90:                                       ; preds = %cond.false89, %cond.true88
  %cond91 = phi float [ %92, %cond.true88 ], [ %93, %cond.false89 ]
  store float %cond91, float* %max_noise, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end90
  %94 = load i32, i32* %sfb, align 4
  %inc92 = add nsw i32 %94, 1
  store i32 %inc92, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %95 = load i32, i32* %over, align 4
  %96 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %over_count = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %96, i32 0, i32 3
  store i32 %95, i32* %over_count, align 4
  %97 = load float, float* %tot_noise_db, align 4
  %98 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %tot_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %98, i32 0, i32 1
  store float %97, float* %tot_noise, align 4
  %99 = load float, float* %over_noise_db, align 4
  %100 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %over_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %100, i32 0, i32 0
  store float %99, float* %over_noise, align 4
  %101 = load float, float* %max_noise, align 4
  %102 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %res.addr, align 4
  %max_noise93 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %102, i32 0, i32 2
  store float %101, float* %max_noise93, align 4
  %103 = load i32, i32* %over, align 4
  ret i32 %103
}

; Function Attrs: noinline nounwind optnone
define internal float @calc_noise_core_c(%struct.gr_info* %cod_info, i32* %startline, i32 %l, float %step) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %startline.addr = alloca i32*, align 4
  %l.addr = alloca i32, align 4
  %step.addr = alloca float, align 4
  %noise = alloca float, align 4
  %j = alloca i32, align 4
  %ix = alloca i32*, align 4
  %temp = alloca float, align 4
  %ix01 = alloca [2 x float], align 4
  %temp14 = alloca float, align 4
  %temp41 = alloca float, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32* %startline, i32** %startline.addr, align 4
  store i32 %l, i32* %l.addr, align 4
  store float %step, float* %step.addr, align 4
  store float 0.000000e+00, float* %noise, align 4
  %0 = load i32*, i32** %startline.addr, align 4
  %1 = load i32, i32* %0, align 4
  store i32 %1, i32* %j, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  store i32* %arraydecay, i32** %ix, align 4
  %3 = load i32, i32* %j, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %count1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 6
  %5 = load i32, i32* %count1, align 4
  %cmp = icmp sgt i32 %3, %5
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %6 = load i32, i32* %l.addr, align 4
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %l.addr, align 4
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 0
  %8 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %8
  %9 = load float, float* %arrayidx, align 4
  store float %9, float* %temp, align 4
  %10 = load i32, i32* %j, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %j, align 4
  %11 = load float, float* %temp, align 4
  %12 = load float, float* %temp, align 4
  %mul = fmul float %11, %12
  %13 = load float, float* %noise, align 4
  %add = fadd float %13, %mul
  store float %add, float* %noise, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 0
  %15 = load i32, i32* %j, align 4
  %arrayidx2 = getelementptr inbounds [576 x float], [576 x float]* %xr1, i32 0, i32 %15
  %16 = load float, float* %arrayidx2, align 4
  store float %16, float* %temp, align 4
  %17 = load i32, i32* %j, align 4
  %inc3 = add nsw i32 %17, 1
  store i32 %inc3, i32* %j, align 4
  %18 = load float, float* %temp, align 4
  %19 = load float, float* %temp, align 4
  %mul4 = fmul float %18, %19
  %20 = load float, float* %noise, align 4
  %add5 = fadd float %20, %mul4
  store float %add5, float* %noise, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end67

if.else:                                          ; preds = %entry
  %21 = load i32, i32* %j, align 4
  %22 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %22, i32 0, i32 5
  %23 = load i32, i32* %big_values, align 4
  %cmp6 = icmp sgt i32 %21, %23
  br i1 %cmp6, label %if.then7, label %if.else36

if.then7:                                         ; preds = %if.else
  %arrayidx8 = getelementptr inbounds [2 x float], [2 x float]* %ix01, i32 0, i32 0
  store float 0.000000e+00, float* %arrayidx8, align 4
  %24 = load float, float* %step.addr, align 4
  %arrayidx9 = getelementptr inbounds [2 x float], [2 x float]* %ix01, i32 0, i32 1
  store float %24, float* %arrayidx9, align 4
  br label %while.cond10

while.cond10:                                     ; preds = %while.body13, %if.then7
  %25 = load i32, i32* %l.addr, align 4
  %dec11 = add nsw i32 %25, -1
  store i32 %dec11, i32* %l.addr, align 4
  %tobool12 = icmp ne i32 %25, 0
  br i1 %tobool12, label %while.body13, label %while.end35

while.body13:                                     ; preds = %while.cond10
  %26 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr15 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %26, i32 0, i32 0
  %27 = load i32, i32* %j, align 4
  %arrayidx16 = getelementptr inbounds [576 x float], [576 x float]* %xr15, i32 0, i32 %27
  %28 = load float, float* %arrayidx16, align 4
  %conv = fpext float %28 to double
  %29 = call double @llvm.fabs.f64(double %conv)
  %30 = load i32*, i32** %ix, align 4
  %31 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %30, i32 %31
  %32 = load i32, i32* %arrayidx17, align 4
  %arrayidx18 = getelementptr inbounds [2 x float], [2 x float]* %ix01, i32 0, i32 %32
  %33 = load float, float* %arrayidx18, align 4
  %conv19 = fpext float %33 to double
  %sub = fsub double %29, %conv19
  %conv20 = fptrunc double %sub to float
  store float %conv20, float* %temp14, align 4
  %34 = load i32, i32* %j, align 4
  %inc21 = add nsw i32 %34, 1
  store i32 %inc21, i32* %j, align 4
  %35 = load float, float* %temp14, align 4
  %36 = load float, float* %temp14, align 4
  %mul22 = fmul float %35, %36
  %37 = load float, float* %noise, align 4
  %add23 = fadd float %37, %mul22
  store float %add23, float* %noise, align 4
  %38 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr24 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %38, i32 0, i32 0
  %39 = load i32, i32* %j, align 4
  %arrayidx25 = getelementptr inbounds [576 x float], [576 x float]* %xr24, i32 0, i32 %39
  %40 = load float, float* %arrayidx25, align 4
  %conv26 = fpext float %40 to double
  %41 = call double @llvm.fabs.f64(double %conv26)
  %42 = load i32*, i32** %ix, align 4
  %43 = load i32, i32* %j, align 4
  %arrayidx27 = getelementptr inbounds i32, i32* %42, i32 %43
  %44 = load i32, i32* %arrayidx27, align 4
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %ix01, i32 0, i32 %44
  %45 = load float, float* %arrayidx28, align 4
  %conv29 = fpext float %45 to double
  %sub30 = fsub double %41, %conv29
  %conv31 = fptrunc double %sub30 to float
  store float %conv31, float* %temp14, align 4
  %46 = load i32, i32* %j, align 4
  %inc32 = add nsw i32 %46, 1
  store i32 %inc32, i32* %j, align 4
  %47 = load float, float* %temp14, align 4
  %48 = load float, float* %temp14, align 4
  %mul33 = fmul float %47, %48
  %49 = load float, float* %noise, align 4
  %add34 = fadd float %49, %mul33
  store float %add34, float* %noise, align 4
  br label %while.cond10

while.end35:                                      ; preds = %while.cond10
  br label %if.end

if.else36:                                        ; preds = %if.else
  br label %while.cond37

while.cond37:                                     ; preds = %while.body40, %if.else36
  %50 = load i32, i32* %l.addr, align 4
  %dec38 = add nsw i32 %50, -1
  store i32 %dec38, i32* %l.addr, align 4
  %tobool39 = icmp ne i32 %50, 0
  br i1 %tobool39, label %while.body40, label %while.end66

while.body40:                                     ; preds = %while.cond37
  %51 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr42 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %51, i32 0, i32 0
  %52 = load i32, i32* %j, align 4
  %arrayidx43 = getelementptr inbounds [576 x float], [576 x float]* %xr42, i32 0, i32 %52
  %53 = load float, float* %arrayidx43, align 4
  %conv44 = fpext float %53 to double
  %54 = call double @llvm.fabs.f64(double %conv44)
  %55 = load i32*, i32** %ix, align 4
  %56 = load i32, i32* %j, align 4
  %arrayidx45 = getelementptr inbounds i32, i32* %55, i32 %56
  %57 = load i32, i32* %arrayidx45, align 4
  %arrayidx46 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %57
  %58 = load float, float* %arrayidx46, align 4
  %59 = load float, float* %step.addr, align 4
  %mul47 = fmul float %58, %59
  %conv48 = fpext float %mul47 to double
  %sub49 = fsub double %54, %conv48
  %conv50 = fptrunc double %sub49 to float
  store float %conv50, float* %temp41, align 4
  %60 = load i32, i32* %j, align 4
  %inc51 = add nsw i32 %60, 1
  store i32 %inc51, i32* %j, align 4
  %61 = load float, float* %temp41, align 4
  %62 = load float, float* %temp41, align 4
  %mul52 = fmul float %61, %62
  %63 = load float, float* %noise, align 4
  %add53 = fadd float %63, %mul52
  store float %add53, float* %noise, align 4
  %64 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr54 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %64, i32 0, i32 0
  %65 = load i32, i32* %j, align 4
  %arrayidx55 = getelementptr inbounds [576 x float], [576 x float]* %xr54, i32 0, i32 %65
  %66 = load float, float* %arrayidx55, align 4
  %conv56 = fpext float %66 to double
  %67 = call double @llvm.fabs.f64(double %conv56)
  %68 = load i32*, i32** %ix, align 4
  %69 = load i32, i32* %j, align 4
  %arrayidx57 = getelementptr inbounds i32, i32* %68, i32 %69
  %70 = load i32, i32* %arrayidx57, align 4
  %arrayidx58 = getelementptr inbounds [8208 x float], [8208 x float]* @pow43, i32 0, i32 %70
  %71 = load float, float* %arrayidx58, align 4
  %72 = load float, float* %step.addr, align 4
  %mul59 = fmul float %71, %72
  %conv60 = fpext float %mul59 to double
  %sub61 = fsub double %67, %conv60
  %conv62 = fptrunc double %sub61 to float
  store float %conv62, float* %temp41, align 4
  %73 = load i32, i32* %j, align 4
  %inc63 = add nsw i32 %73, 1
  store i32 %inc63, i32* %j, align 4
  %74 = load float, float* %temp41, align 4
  %75 = load float, float* %temp41, align 4
  %mul64 = fmul float %74, %75
  %76 = load float, float* %noise, align 4
  %add65 = fadd float %76, %mul64
  store float %add65, float* %noise, align 4
  br label %while.cond37

while.end66:                                      ; preds = %while.cond37
  br label %if.end

if.end:                                           ; preds = %while.end66, %while.end35
  br label %if.end67

if.end67:                                         ; preds = %if.end, %while.end
  %77 = load i32, i32* %j, align 4
  %78 = load i32*, i32** %startline.addr, align 4
  store i32 %77, i32* %78, align 4
  %79 = load float, float* %noise, align 4
  ret float %79
}

; Function Attrs: noinline nounwind optnone
define hidden void @set_frame_pinfo(%struct.lame_internal_flags* %gfc, [2 x %struct.III_psy_ratio]* %ratio) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %scalefac_sav = alloca [39 x i32], align 16
  %sfb = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc32, %entry
  %1 = load i32, i32* %gr, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 15
  %3 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %for.body, label %for.end34

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc29, %for.body
  %4 = load i32, i32* %ch, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 14
  %6 = load i32, i32* %channels_out, align 4
  %cmp3 = icmp slt i32 %4, %6
  br i1 %cmp3, label %for.body4, label %for.end31

for.body4:                                        ; preds = %for.cond2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %8 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %8
  %9 = load i32, i32* %ch, align 4
  %arrayidx5 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %9
  store %struct.gr_info* %arrayidx5, %struct.gr_info** %cod_info, align 4
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac_sav, i32 0, i32 0
  %10 = bitcast i32* %arraydecay to i8*
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 2
  %arraydecay6 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 0
  %12 = bitcast i32* %arraydecay6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %10, i8* align 4 %12, i32 156, i1 false)
  %13 = load i32, i32* %gr, align 4
  %cmp7 = icmp eq i32 %13, 1
  br i1 %cmp7, label %if.then, label %if.end23

if.then:                                          ; preds = %for.body4
  store i32 0, i32* %sfb, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then
  %14 = load i32, i32* %sfb, align 4
  %15 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %sfb_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 19
  %16 = load i32, i32* %sfb_lmax, align 4
  %cmp9 = icmp slt i32 %14, %16
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac11 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 2
  %18 = load i32, i32* %sfb, align 4
  %arrayidx12 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac11, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx12, align 4
  %cmp13 = icmp slt i32 %19, 0
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %for.body10
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side15 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 7
  %tt16 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt16, i32 0, i32 0
  %21 = load i32, i32* %ch, align 4
  %arrayidx18 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx17, i32 0, i32 %21
  %scalefac19 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx18, i32 0, i32 2
  %22 = load i32, i32* %sfb, align 4
  %arrayidx20 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac19, i32 0, i32 %22
  %23 = load i32, i32* %arrayidx20, align 4
  %24 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac21 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 2
  %25 = load i32, i32* %sfb, align 4
  %arrayidx22 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac21, i32 0, i32 %25
  store i32 %23, i32* %arrayidx22, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %for.body10
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %if.end23

if.end23:                                         ; preds = %for.end, %for.body4
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %28 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %29 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %30 = load i32, i32* %gr, align 4
  %arrayidx24 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %29, i32 %30
  %31 = load i32, i32* %ch, align 4
  %arrayidx25 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx24, i32 0, i32 %31
  %32 = load i32, i32* %gr, align 4
  %33 = load i32, i32* %ch, align 4
  call void @set_pinfo(%struct.lame_internal_flags* %27, %struct.gr_info* %28, %struct.III_psy_ratio* %arrayidx25, i32 %32, i32 %33)
  %34 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %scalefac26 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %34, i32 0, i32 2
  %arraydecay27 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac26, i32 0, i32 0
  %35 = bitcast i32* %arraydecay27 to i8*
  %arraydecay28 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac_sav, i32 0, i32 0
  %36 = bitcast i32* %arraydecay28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 16 %36, i32 156, i1 false)
  br label %for.inc29

for.inc29:                                        ; preds = %if.end23
  %37 = load i32, i32* %ch, align 4
  %inc30 = add nsw i32 %37, 1
  store i32 %inc30, i32* %ch, align 4
  br label %for.cond2

for.end31:                                        ; preds = %for.cond2
  br label %for.inc32

for.inc32:                                        ; preds = %for.end31
  %38 = load i32, i32* %gr, align 4
  %inc33 = add nsw i32 %38, 1
  store i32 %inc33, i32* %gr, align 4
  br label %for.cond

for.end34:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define internal void @set_pinfo(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, %struct.III_psy_ratio* %ratio, i32 %gr, i32 %ch) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %ratio.addr = alloca %struct.III_psy_ratio*, align 4
  %gr.addr = alloca i32, align 4
  %ch.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %sfb = alloca i32, align 4
  %sfb2 = alloca i32, align 4
  %j = alloca i32, align 4
  %i = alloca i32, align 4
  %l = alloca i32, align 4
  %start = alloca i32, align 4
  %end = alloca i32, align 4
  %bw = alloca i32, align 4
  %en0 = alloca float, align 4
  %en1 = alloca float, align 4
  %ifqstep = alloca float, align 4
  %scalefac = alloca i32*, align 4
  %l3_xmin = alloca [39 x float], align 16
  %xfsf = alloca [39 x float], align 16
  %noise = alloca %struct.calc_noise_result_t, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store %struct.III_psy_ratio* %ratio, %struct.III_psy_ratio** %ratio.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 16
  %2 = load i32, i32* %scalefac_scale, align 4
  %cmp = icmp eq i32 %2, 0
  %3 = zext i1 %cmp to i64
  %cond = select i1 %cmp, double 5.000000e-01, double 1.000000e+00
  %conv = fptrunc double %cond to float
  store float %conv, float* %ifqstep, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac2, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %6 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %arraydecay3 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %call = call i32 @calc_xmin(%struct.lame_internal_flags* %5, %struct.III_psy_ratio* %6, %struct.gr_info* %7, float* %arraydecay3)
  %8 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %arraydecay4 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [39 x float], [39 x float]* %xfsf, i32 0, i32 0
  %call6 = call i32 @calc_noise(%struct.gr_info* %8, float* %arraydecay4, float* %arraydecay5, %struct.calc_noise_result_t* %noise, %struct.calc_noise_data_t* null)
  store i32 0, i32* %j, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 19
  %10 = load i32, i32* %sfb_lmax, align 4
  store i32 %10, i32* %sfb2, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 9
  %12 = load i32, i32* %block_type, align 4
  %cmp7 = icmp ne i32 %12, 2
  br i1 %cmp7, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 10
  %14 = load i32, i32* %mixed_block_flag, align 4
  %tobool = icmp ne i32 %14, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i32 22, i32* %sfb2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc108, %if.end
  %15 = load i32, i32* %sfb, align 4
  %16 = load i32, i32* %sfb2, align 4
  %cmp9 = icmp slt i32 %15, %16
  br i1 %cmp9, label %for.body, label %for.end110

for.body:                                         ; preds = %for.cond
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 8
  %l11 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %18 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [23 x i32], [23 x i32]* %l11, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx, align 4
  store i32 %19, i32* %start, align 4
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 8
  %l13 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band12, i32 0, i32 0
  %21 = load i32, i32* %sfb, align 4
  %add = add nsw i32 %21, 1
  %arrayidx14 = getelementptr inbounds [23 x i32], [23 x i32]* %l13, i32 0, i32 %add
  %22 = load i32, i32* %arrayidx14, align 4
  store i32 %22, i32* %end, align 4
  %23 = load i32, i32* %end, align 4
  %24 = load i32, i32* %start, align 4
  %sub = sub nsw i32 %23, %24
  store i32 %sub, i32* %bw, align 4
  store float 0.000000e+00, float* %en0, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %for.body
  %25 = load i32, i32* %j, align 4
  %26 = load i32, i32* %end, align 4
  %cmp16 = icmp slt i32 %25, %26
  br i1 %cmp16, label %for.body18, label %for.end

for.body18:                                       ; preds = %for.cond15
  %27 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %27, i32 0, i32 0
  %28 = load i32, i32* %j, align 4
  %arrayidx19 = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %28
  %29 = load float, float* %arrayidx19, align 4
  %30 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr20 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 0
  %31 = load i32, i32* %j, align 4
  %arrayidx21 = getelementptr inbounds [576 x float], [576 x float]* %xr20, i32 0, i32 %31
  %32 = load float, float* %arrayidx21, align 4
  %mul = fmul float %29, %32
  %33 = load float, float* %en0, align 4
  %add22 = fadd float %33, %mul
  store float %add22, float* %en0, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body18
  %34 = load i32, i32* %j, align 4
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond15

for.end:                                          ; preds = %for.cond15
  %35 = load i32, i32* %bw, align 4
  %conv23 = sitofp i32 %35 to float
  %36 = load float, float* %en0, align 4
  %div = fdiv float %36, %conv23
  store float %div, float* %en0, align 4
  store float 0x430C6BF520000000, float* %en1, align 4
  %37 = load float, float* %en1, align 4
  %38 = load float, float* %en0, align 4
  %mul24 = fmul float %37, %38
  %conv25 = fpext float %mul24 to double
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 23
  %40 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  %en = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %40, i32 0, i32 14
  %41 = load i32, i32* %gr.addr, align 4
  %arrayidx26 = getelementptr inbounds [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]]* %en, i32 0, i32 %41
  %42 = load i32, i32* %ch.addr, align 4
  %arrayidx27 = getelementptr inbounds [4 x [22 x double]], [4 x [22 x double]]* %arrayidx26, i32 0, i32 %42
  %43 = load i32, i32* %sfb, align 4
  %arrayidx28 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx27, i32 0, i32 %43
  store double %conv25, double* %arrayidx28, align 8
  %44 = load float, float* %en1, align 4
  %45 = load i32, i32* %sfb, align 4
  %arrayidx29 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 %45
  %46 = load float, float* %arrayidx29, align 4
  %mul30 = fmul float %44, %46
  %47 = load i32, i32* %sfb, align 4
  %arrayidx31 = getelementptr inbounds [39 x float], [39 x float]* %xfsf, i32 0, i32 %47
  %48 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %mul30, %48
  %49 = load i32, i32* %bw, align 4
  %conv33 = sitofp i32 %49 to float
  %div34 = fdiv float %mul32, %conv33
  %conv35 = fpext float %div34 to double
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo36 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %50, i32 0, i32 23
  %51 = load %struct.plotting_data*, %struct.plotting_data** %pinfo36, align 8
  %xfsf37 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %51, i32 0, i32 27
  %52 = load i32, i32* %gr.addr, align 4
  %arrayidx38 = getelementptr inbounds [2 x [2 x [22 x double]]], [2 x [2 x [22 x double]]]* %xfsf37, i32 0, i32 %52
  %53 = load i32, i32* %ch.addr, align 4
  %arrayidx39 = getelementptr inbounds [2 x [22 x double]], [2 x [22 x double]]* %arrayidx38, i32 0, i32 %53
  %54 = load i32, i32* %sfb, align 4
  %arrayidx40 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx39, i32 0, i32 %54
  store double %conv35, double* %arrayidx40, align 8
  %55 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en41 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %55, i32 0, i32 1
  %l42 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en41, i32 0, i32 0
  %56 = load i32, i32* %sfb, align 4
  %arrayidx43 = getelementptr inbounds [22 x float], [22 x float]* %l42, i32 0, i32 %56
  %57 = load float, float* %arrayidx43, align 4
  %cmp44 = fcmp ogt float %57, 0.000000e+00
  br i1 %cmp44, label %land.lhs.true46, label %if.else

land.lhs.true46:                                  ; preds = %for.end
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHonly = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 49
  %59 = load i32, i32* %ATHonly, align 4
  %tobool47 = icmp ne i32 %59, 0
  br i1 %tobool47, label %if.else, label %if.then48

if.then48:                                        ; preds = %land.lhs.true46
  %60 = load float, float* %en0, align 4
  %61 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en49 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %61, i32 0, i32 1
  %l50 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en49, i32 0, i32 0
  %62 = load i32, i32* %sfb, align 4
  %arrayidx51 = getelementptr inbounds [22 x float], [22 x float]* %l50, i32 0, i32 %62
  %63 = load float, float* %arrayidx51, align 4
  %div52 = fdiv float %60, %63
  store float %div52, float* %en0, align 4
  br label %if.end53

if.else:                                          ; preds = %land.lhs.true46, %for.end
  store float 0.000000e+00, float* %en0, align 4
  br label %if.end53

if.end53:                                         ; preds = %if.else, %if.then48
  %64 = load float, float* %en1, align 4
  %65 = load float, float* %en0, align 4
  %66 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %66, i32 0, i32 0
  %l54 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm, i32 0, i32 0
  %67 = load i32, i32* %sfb, align 4
  %arrayidx55 = getelementptr inbounds [22 x float], [22 x float]* %l54, i32 0, i32 %67
  %68 = load float, float* %arrayidx55, align 4
  %mul56 = fmul float %65, %68
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 21
  %70 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %l57 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %70, i32 0, i32 6
  %71 = load i32, i32* %sfb, align 4
  %arrayidx58 = getelementptr inbounds [22 x float], [22 x float]* %l57, i32 0, i32 %71
  %72 = load float, float* %arrayidx58, align 4
  %cmp59 = fcmp ogt float %mul56, %72
  br i1 %cmp59, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end53
  %73 = load float, float* %en0, align 4
  %74 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm61 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %74, i32 0, i32 0
  %l62 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm61, i32 0, i32 0
  %75 = load i32, i32* %sfb, align 4
  %arrayidx63 = getelementptr inbounds [22 x float], [22 x float]* %l62, i32 0, i32 %75
  %76 = load float, float* %arrayidx63, align 4
  %mul64 = fmul float %73, %76
  br label %cond.end

cond.false:                                       ; preds = %if.end53
  %77 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH65 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %77, i32 0, i32 21
  %78 = load %struct.ATH_t*, %struct.ATH_t** %ATH65, align 8
  %l66 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %78, i32 0, i32 6
  %79 = load i32, i32* %sfb, align 4
  %arrayidx67 = getelementptr inbounds [22 x float], [22 x float]* %l66, i32 0, i32 %79
  %80 = load float, float* %arrayidx67, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond68 = phi float [ %mul64, %cond.true ], [ %80, %cond.false ]
  %mul69 = fmul float %64, %cond68
  %conv70 = fpext float %mul69 to double
  %81 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo71 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %81, i32 0, i32 23
  %82 = load %struct.plotting_data*, %struct.plotting_data** %pinfo71, align 8
  %thr = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %82, i32 0, i32 13
  %83 = load i32, i32* %gr.addr, align 4
  %arrayidx72 = getelementptr inbounds [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]]* %thr, i32 0, i32 %83
  %84 = load i32, i32* %ch.addr, align 4
  %arrayidx73 = getelementptr inbounds [4 x [22 x double]], [4 x [22 x double]]* %arrayidx72, i32 0, i32 %84
  %85 = load i32, i32* %sfb, align 4
  %arrayidx74 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx73, i32 0, i32 %85
  store double %conv70, double* %arrayidx74, align 8
  %86 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo75 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %86, i32 0, i32 23
  %87 = load %struct.plotting_data*, %struct.plotting_data** %pinfo75, align 8
  %LAMEsfb = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %87, i32 0, i32 21
  %88 = load i32, i32* %gr.addr, align 4
  %arrayidx76 = getelementptr inbounds [2 x [2 x [22 x double]]], [2 x [2 x [22 x double]]]* %LAMEsfb, i32 0, i32 %88
  %89 = load i32, i32* %ch.addr, align 4
  %arrayidx77 = getelementptr inbounds [2 x [22 x double]], [2 x [22 x double]]* %arrayidx76, i32 0, i32 %89
  %90 = load i32, i32* %sfb, align 4
  %arrayidx78 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx77, i32 0, i32 %90
  store double 0.000000e+00, double* %arrayidx78, align 8
  %91 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %91, i32 0, i32 15
  %92 = load i32, i32* %preflag, align 4
  %tobool79 = icmp ne i32 %92, 0
  br i1 %tobool79, label %land.lhs.true80, label %if.end93

land.lhs.true80:                                  ; preds = %cond.end
  %93 = load i32, i32* %sfb, align 4
  %cmp81 = icmp sge i32 %93, 11
  br i1 %cmp81, label %if.then83, label %if.end93

if.then83:                                        ; preds = %land.lhs.true80
  %94 = load float, float* %ifqstep, align 4
  %fneg = fneg float %94
  %95 = load i32, i32* %sfb, align 4
  %arrayidx84 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %95
  %96 = load i32, i32* %arrayidx84, align 4
  %conv85 = sitofp i32 %96 to float
  %mul86 = fmul float %fneg, %conv85
  %conv87 = fpext float %mul86 to double
  %97 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo88 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %97, i32 0, i32 23
  %98 = load %struct.plotting_data*, %struct.plotting_data** %pinfo88, align 8
  %LAMEsfb89 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %98, i32 0, i32 21
  %99 = load i32, i32* %gr.addr, align 4
  %arrayidx90 = getelementptr inbounds [2 x [2 x [22 x double]]], [2 x [2 x [22 x double]]]* %LAMEsfb89, i32 0, i32 %99
  %100 = load i32, i32* %ch.addr, align 4
  %arrayidx91 = getelementptr inbounds [2 x [22 x double]], [2 x [22 x double]]* %arrayidx90, i32 0, i32 %100
  %101 = load i32, i32* %sfb, align 4
  %arrayidx92 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx91, i32 0, i32 %101
  store double %conv87, double* %arrayidx92, align 8
  br label %if.end93

if.end93:                                         ; preds = %if.then83, %land.lhs.true80, %cond.end
  %102 = load i32, i32* %sfb, align 4
  %cmp94 = icmp slt i32 %102, 21
  br i1 %cmp94, label %if.then96, label %if.end107

if.then96:                                        ; preds = %if.end93
  %103 = load float, float* %ifqstep, align 4
  %104 = load i32*, i32** %scalefac, align 4
  %105 = load i32, i32* %sfb, align 4
  %arrayidx97 = getelementptr inbounds i32, i32* %104, i32 %105
  %106 = load i32, i32* %arrayidx97, align 4
  %conv98 = sitofp i32 %106 to float
  %mul99 = fmul float %103, %conv98
  %conv100 = fpext float %mul99 to double
  %107 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo101 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %107, i32 0, i32 23
  %108 = load %struct.plotting_data*, %struct.plotting_data** %pinfo101, align 8
  %LAMEsfb102 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %108, i32 0, i32 21
  %109 = load i32, i32* %gr.addr, align 4
  %arrayidx103 = getelementptr inbounds [2 x [2 x [22 x double]]], [2 x [2 x [22 x double]]]* %LAMEsfb102, i32 0, i32 %109
  %110 = load i32, i32* %ch.addr, align 4
  %arrayidx104 = getelementptr inbounds [2 x [22 x double]], [2 x [22 x double]]* %arrayidx103, i32 0, i32 %110
  %111 = load i32, i32* %sfb, align 4
  %arrayidx105 = getelementptr inbounds [22 x double], [22 x double]* %arrayidx104, i32 0, i32 %111
  %112 = load double, double* %arrayidx105, align 8
  %sub106 = fsub double %112, %conv100
  store double %sub106, double* %arrayidx105, align 8
  br label %if.end107

if.end107:                                        ; preds = %if.then96, %if.end93
  br label %for.inc108

for.inc108:                                       ; preds = %if.end107
  %113 = load i32, i32* %sfb, align 4
  %inc109 = add nsw i32 %113, 1
  store i32 %inc109, i32* %sfb, align 4
  br label %for.cond

for.end110:                                       ; preds = %for.cond
  %114 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type111 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %114, i32 0, i32 9
  %115 = load i32, i32* %block_type111, align 4
  %cmp112 = icmp eq i32 %115, 2
  br i1 %cmp112, label %if.then114, label %if.end259

if.then114:                                       ; preds = %for.end110
  %116 = load i32, i32* %sfb, align 4
  store i32 %116, i32* %sfb2, align 4
  %117 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin = getelementptr inbounds %struct.gr_info, %struct.gr_info* %117, i32 0, i32 20
  %118 = load i32, i32* %sfb_smin, align 4
  store i32 %118, i32* %sfb, align 4
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc256, %if.then114
  %119 = load i32, i32* %sfb, align 4
  %cmp116 = icmp slt i32 %119, 13
  br i1 %cmp116, label %for.body118, label %for.end258

for.body118:                                      ; preds = %for.cond115
  %120 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band119 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %120, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band119, i32 0, i32 1
  %121 = load i32, i32* %sfb, align 4
  %arrayidx120 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 %121
  %122 = load i32, i32* %arrayidx120, align 4
  store i32 %122, i32* %start, align 4
  %123 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band121 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %123, i32 0, i32 8
  %s122 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band121, i32 0, i32 1
  %124 = load i32, i32* %sfb, align 4
  %add123 = add nsw i32 %124, 1
  %arrayidx124 = getelementptr inbounds [14 x i32], [14 x i32]* %s122, i32 0, i32 %add123
  %125 = load i32, i32* %arrayidx124, align 4
  store i32 %125, i32* %end, align 4
  %126 = load i32, i32* %end, align 4
  %127 = load i32, i32* %start, align 4
  %sub125 = sub nsw i32 %126, %127
  store i32 %sub125, i32* %bw, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc253, %for.body118
  %128 = load i32, i32* %i, align 4
  %cmp127 = icmp slt i32 %128, 3
  br i1 %cmp127, label %for.body129, label %for.end255

for.body129:                                      ; preds = %for.cond126
  store float 0.000000e+00, float* %en0, align 4
  %129 = load i32, i32* %start, align 4
  store i32 %129, i32* %l, align 4
  br label %for.cond130

for.cond130:                                      ; preds = %for.inc141, %for.body129
  %130 = load i32, i32* %l, align 4
  %131 = load i32, i32* %end, align 4
  %cmp131 = icmp slt i32 %130, %131
  br i1 %cmp131, label %for.body133, label %for.end143

for.body133:                                      ; preds = %for.cond130
  %132 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr134 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %132, i32 0, i32 0
  %133 = load i32, i32* %j, align 4
  %arrayidx135 = getelementptr inbounds [576 x float], [576 x float]* %xr134, i32 0, i32 %133
  %134 = load float, float* %arrayidx135, align 4
  %135 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr136 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %135, i32 0, i32 0
  %136 = load i32, i32* %j, align 4
  %arrayidx137 = getelementptr inbounds [576 x float], [576 x float]* %xr136, i32 0, i32 %136
  %137 = load float, float* %arrayidx137, align 4
  %mul138 = fmul float %134, %137
  %138 = load float, float* %en0, align 4
  %add139 = fadd float %138, %mul138
  store float %add139, float* %en0, align 4
  %139 = load i32, i32* %j, align 4
  %inc140 = add nsw i32 %139, 1
  store i32 %inc140, i32* %j, align 4
  br label %for.inc141

for.inc141:                                       ; preds = %for.body133
  %140 = load i32, i32* %l, align 4
  %inc142 = add nsw i32 %140, 1
  store i32 %inc142, i32* %l, align 4
  br label %for.cond130

for.end143:                                       ; preds = %for.cond130
  %141 = load float, float* %en0, align 4
  %142 = load i32, i32* %bw, align 4
  %conv144 = sitofp i32 %142 to float
  %div145 = fdiv float %141, %conv144
  %conv146 = fpext float %div145 to double
  %cmp147 = fcmp ogt double %conv146, 0x3BC79CA10C924223
  br i1 %cmp147, label %cond.true149, label %cond.false153

cond.true149:                                     ; preds = %for.end143
  %143 = load float, float* %en0, align 4
  %144 = load i32, i32* %bw, align 4
  %conv150 = sitofp i32 %144 to float
  %div151 = fdiv float %143, %conv150
  %conv152 = fpext float %div151 to double
  br label %cond.end154

cond.false153:                                    ; preds = %for.end143
  br label %cond.end154

cond.end154:                                      ; preds = %cond.false153, %cond.true149
  %cond155 = phi double [ %conv152, %cond.true149 ], [ 0x3BC79CA10C924223, %cond.false153 ]
  %conv156 = fptrunc double %cond155 to float
  store float %conv156, float* %en0, align 4
  store float 0x430C6BF520000000, float* %en1, align 4
  %145 = load float, float* %en1, align 4
  %146 = load float, float* %en0, align 4
  %mul157 = fmul float %145, %146
  %conv158 = fpext float %mul157 to double
  %147 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo159 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %147, i32 0, i32 23
  %148 = load %struct.plotting_data*, %struct.plotting_data** %pinfo159, align 8
  %en_s = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %148, i32 0, i32 16
  %149 = load i32, i32* %gr.addr, align 4
  %arrayidx160 = getelementptr inbounds [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]]* %en_s, i32 0, i32 %149
  %150 = load i32, i32* %ch.addr, align 4
  %arrayidx161 = getelementptr inbounds [4 x [39 x double]], [4 x [39 x double]]* %arrayidx160, i32 0, i32 %150
  %151 = load i32, i32* %sfb, align 4
  %mul162 = mul nsw i32 3, %151
  %152 = load i32, i32* %i, align 4
  %add163 = add nsw i32 %mul162, %152
  %arrayidx164 = getelementptr inbounds [39 x double], [39 x double]* %arrayidx161, i32 0, i32 %add163
  store double %conv158, double* %arrayidx164, align 8
  %153 = load float, float* %en1, align 4
  %154 = load i32, i32* %sfb2, align 4
  %arrayidx165 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 %154
  %155 = load float, float* %arrayidx165, align 4
  %mul166 = fmul float %153, %155
  %156 = load i32, i32* %sfb2, align 4
  %arrayidx167 = getelementptr inbounds [39 x float], [39 x float]* %xfsf, i32 0, i32 %156
  %157 = load float, float* %arrayidx167, align 4
  %mul168 = fmul float %mul166, %157
  %158 = load i32, i32* %bw, align 4
  %conv169 = sitofp i32 %158 to float
  %div170 = fdiv float %mul168, %conv169
  %conv171 = fpext float %div170 to double
  %159 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo172 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %159, i32 0, i32 23
  %160 = load %struct.plotting_data*, %struct.plotting_data** %pinfo172, align 8
  %xfsf_s = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %160, i32 0, i32 28
  %161 = load i32, i32* %gr.addr, align 4
  %arrayidx173 = getelementptr inbounds [2 x [2 x [39 x double]]], [2 x [2 x [39 x double]]]* %xfsf_s, i32 0, i32 %161
  %162 = load i32, i32* %ch.addr, align 4
  %arrayidx174 = getelementptr inbounds [2 x [39 x double]], [2 x [39 x double]]* %arrayidx173, i32 0, i32 %162
  %163 = load i32, i32* %sfb, align 4
  %mul175 = mul nsw i32 3, %163
  %164 = load i32, i32* %i, align 4
  %add176 = add nsw i32 %mul175, %164
  %arrayidx177 = getelementptr inbounds [39 x double], [39 x double]* %arrayidx174, i32 0, i32 %add176
  store double %conv171, double* %arrayidx177, align 8
  %165 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en178 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %165, i32 0, i32 1
  %s179 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en178, i32 0, i32 1
  %166 = load i32, i32* %sfb, align 4
  %arrayidx180 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s179, i32 0, i32 %166
  %167 = load i32, i32* %i, align 4
  %arrayidx181 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx180, i32 0, i32 %167
  %168 = load float, float* %arrayidx181, align 4
  %cmp182 = fcmp ogt float %168, 0.000000e+00
  br i1 %cmp182, label %if.then184, label %if.else190

if.then184:                                       ; preds = %cond.end154
  %169 = load float, float* %en0, align 4
  %170 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %en185 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %170, i32 0, i32 1
  %s186 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %en185, i32 0, i32 1
  %171 = load i32, i32* %sfb, align 4
  %arrayidx187 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s186, i32 0, i32 %171
  %172 = load i32, i32* %i, align 4
  %arrayidx188 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx187, i32 0, i32 %172
  %173 = load float, float* %arrayidx188, align 4
  %div189 = fdiv float %169, %173
  store float %div189, float* %en0, align 4
  br label %if.end191

if.else190:                                       ; preds = %cond.end154
  store float 0.000000e+00, float* %en0, align 4
  br label %if.end191

if.end191:                                        ; preds = %if.else190, %if.then184
  %174 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHonly192 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %174, i32 0, i32 49
  %175 = load i32, i32* %ATHonly192, align 4
  %tobool193 = icmp ne i32 %175, 0
  br i1 %tobool193, label %if.then195, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end191
  %176 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHshort = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %176, i32 0, i32 50
  %177 = load i32, i32* %ATHshort, align 4
  %tobool194 = icmp ne i32 %177, 0
  br i1 %tobool194, label %if.then195, label %if.end196

if.then195:                                       ; preds = %lor.lhs.false, %if.end191
  store float 0.000000e+00, float* %en0, align 4
  br label %if.end196

if.end196:                                        ; preds = %if.then195, %lor.lhs.false
  %178 = load float, float* %en1, align 4
  %179 = load float, float* %en0, align 4
  %180 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm197 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %180, i32 0, i32 0
  %s198 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm197, i32 0, i32 1
  %181 = load i32, i32* %sfb, align 4
  %arrayidx199 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s198, i32 0, i32 %181
  %182 = load i32, i32* %i, align 4
  %arrayidx200 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx199, i32 0, i32 %182
  %183 = load float, float* %arrayidx200, align 4
  %mul201 = fmul float %179, %183
  %184 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH202 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %184, i32 0, i32 21
  %185 = load %struct.ATH_t*, %struct.ATH_t** %ATH202, align 8
  %s203 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %185, i32 0, i32 7
  %186 = load i32, i32* %sfb, align 4
  %arrayidx204 = getelementptr inbounds [13 x float], [13 x float]* %s203, i32 0, i32 %186
  %187 = load float, float* %arrayidx204, align 4
  %cmp205 = fcmp ogt float %mul201, %187
  br i1 %cmp205, label %cond.true207, label %cond.false213

cond.true207:                                     ; preds = %if.end196
  %188 = load float, float* %en0, align 4
  %189 = load %struct.III_psy_ratio*, %struct.III_psy_ratio** %ratio.addr, align 4
  %thm208 = getelementptr inbounds %struct.III_psy_ratio, %struct.III_psy_ratio* %189, i32 0, i32 0
  %s209 = getelementptr inbounds %struct.III_psy_xmin, %struct.III_psy_xmin* %thm208, i32 0, i32 1
  %190 = load i32, i32* %sfb, align 4
  %arrayidx210 = getelementptr inbounds [13 x [3 x float]], [13 x [3 x float]]* %s209, i32 0, i32 %190
  %191 = load i32, i32* %i, align 4
  %arrayidx211 = getelementptr inbounds [3 x float], [3 x float]* %arrayidx210, i32 0, i32 %191
  %192 = load float, float* %arrayidx211, align 4
  %mul212 = fmul float %188, %192
  br label %cond.end217

cond.false213:                                    ; preds = %if.end196
  %193 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH214 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %193, i32 0, i32 21
  %194 = load %struct.ATH_t*, %struct.ATH_t** %ATH214, align 8
  %s215 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %194, i32 0, i32 7
  %195 = load i32, i32* %sfb, align 4
  %arrayidx216 = getelementptr inbounds [13 x float], [13 x float]* %s215, i32 0, i32 %195
  %196 = load float, float* %arrayidx216, align 4
  br label %cond.end217

cond.end217:                                      ; preds = %cond.false213, %cond.true207
  %cond218 = phi float [ %mul212, %cond.true207 ], [ %196, %cond.false213 ]
  %mul219 = fmul float %178, %cond218
  %conv220 = fpext float %mul219 to double
  %197 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo221 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %197, i32 0, i32 23
  %198 = load %struct.plotting_data*, %struct.plotting_data** %pinfo221, align 8
  %thr_s = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %198, i32 0, i32 15
  %199 = load i32, i32* %gr.addr, align 4
  %arrayidx222 = getelementptr inbounds [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]]* %thr_s, i32 0, i32 %199
  %200 = load i32, i32* %ch.addr, align 4
  %arrayidx223 = getelementptr inbounds [4 x [39 x double]], [4 x [39 x double]]* %arrayidx222, i32 0, i32 %200
  %201 = load i32, i32* %sfb, align 4
  %mul224 = mul nsw i32 3, %201
  %202 = load i32, i32* %i, align 4
  %add225 = add nsw i32 %mul224, %202
  %arrayidx226 = getelementptr inbounds [39 x double], [39 x double]* %arrayidx223, i32 0, i32 %add225
  store double %conv220, double* %arrayidx226, align 8
  %203 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %203, i32 0, i32 12
  %204 = load i32, i32* %i, align 4
  %arrayidx227 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %204
  %205 = load i32, i32* %arrayidx227, align 4
  %conv228 = sitofp i32 %205 to double
  %mul229 = fmul double -2.000000e+00, %conv228
  %206 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo230 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %206, i32 0, i32 23
  %207 = load %struct.plotting_data*, %struct.plotting_data** %pinfo230, align 8
  %LAMEsfb_s = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %207, i32 0, i32 22
  %208 = load i32, i32* %gr.addr, align 4
  %arrayidx231 = getelementptr inbounds [2 x [2 x [39 x double]]], [2 x [2 x [39 x double]]]* %LAMEsfb_s, i32 0, i32 %208
  %209 = load i32, i32* %ch.addr, align 4
  %arrayidx232 = getelementptr inbounds [2 x [39 x double]], [2 x [39 x double]]* %arrayidx231, i32 0, i32 %209
  %210 = load i32, i32* %sfb, align 4
  %mul233 = mul nsw i32 3, %210
  %211 = load i32, i32* %i, align 4
  %add234 = add nsw i32 %mul233, %211
  %arrayidx235 = getelementptr inbounds [39 x double], [39 x double]* %arrayidx232, i32 0, i32 %add234
  store double %mul229, double* %arrayidx235, align 8
  %212 = load i32, i32* %sfb, align 4
  %cmp236 = icmp slt i32 %212, 12
  br i1 %cmp236, label %if.then238, label %if.end251

if.then238:                                       ; preds = %cond.end217
  %213 = load float, float* %ifqstep, align 4
  %214 = load i32*, i32** %scalefac, align 4
  %215 = load i32, i32* %sfb2, align 4
  %arrayidx239 = getelementptr inbounds i32, i32* %214, i32 %215
  %216 = load i32, i32* %arrayidx239, align 4
  %conv240 = sitofp i32 %216 to float
  %mul241 = fmul float %213, %conv240
  %conv242 = fpext float %mul241 to double
  %217 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo243 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %217, i32 0, i32 23
  %218 = load %struct.plotting_data*, %struct.plotting_data** %pinfo243, align 8
  %LAMEsfb_s244 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %218, i32 0, i32 22
  %219 = load i32, i32* %gr.addr, align 4
  %arrayidx245 = getelementptr inbounds [2 x [2 x [39 x double]]], [2 x [2 x [39 x double]]]* %LAMEsfb_s244, i32 0, i32 %219
  %220 = load i32, i32* %ch.addr, align 4
  %arrayidx246 = getelementptr inbounds [2 x [39 x double]], [2 x [39 x double]]* %arrayidx245, i32 0, i32 %220
  %221 = load i32, i32* %sfb, align 4
  %mul247 = mul nsw i32 3, %221
  %222 = load i32, i32* %i, align 4
  %add248 = add nsw i32 %mul247, %222
  %arrayidx249 = getelementptr inbounds [39 x double], [39 x double]* %arrayidx246, i32 0, i32 %add248
  %223 = load double, double* %arrayidx249, align 8
  %sub250 = fsub double %223, %conv242
  store double %sub250, double* %arrayidx249, align 8
  br label %if.end251

if.end251:                                        ; preds = %if.then238, %cond.end217
  %224 = load i32, i32* %sfb2, align 4
  %inc252 = add nsw i32 %224, 1
  store i32 %inc252, i32* %sfb2, align 4
  br label %for.inc253

for.inc253:                                       ; preds = %if.end251
  %225 = load i32, i32* %i, align 4
  %inc254 = add nsw i32 %225, 1
  store i32 %inc254, i32* %i, align 4
  br label %for.cond126

for.end255:                                       ; preds = %for.cond126
  br label %for.inc256

for.inc256:                                       ; preds = %for.end255
  %226 = load i32, i32* %sfb, align 4
  %inc257 = add nsw i32 %226, 1
  store i32 %inc257, i32* %sfb, align 4
  br label %for.cond115

for.end258:                                       ; preds = %for.cond115
  br label %if.end259

if.end259:                                        ; preds = %for.end258, %for.end110
  %227 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %227, i32 0, i32 7
  %228 = load i32, i32* %global_gain, align 4
  %229 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo260 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %229, i32 0, i32 23
  %230 = load %struct.plotting_data*, %struct.plotting_data** %pinfo260, align 8
  %LAMEqss = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %230, i32 0, i32 23
  %231 = load i32, i32* %gr.addr, align 4
  %arrayidx261 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %LAMEqss, i32 0, i32 %231
  %232 = load i32, i32* %ch.addr, align 4
  %arrayidx262 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx261, i32 0, i32 %232
  store i32 %228, i32* %arrayidx262, align 4
  %233 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %233, i32 0, i32 4
  %234 = load i32, i32* %part2_3_length, align 4
  %235 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %235, i32 0, i32 18
  %236 = load i32, i32* %part2_length, align 4
  %add263 = add nsw i32 %234, %236
  %237 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo264 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %237, i32 0, i32 23
  %238 = load %struct.plotting_data*, %struct.plotting_data** %pinfo264, align 8
  %LAMEmainbits = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %238, i32 0, i32 41
  %239 = load i32, i32* %gr.addr, align 4
  %arrayidx265 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %LAMEmainbits, i32 0, i32 %239
  %240 = load i32, i32* %ch.addr, align 4
  %arrayidx266 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx265, i32 0, i32 %240
  store i32 %add263, i32* %arrayidx266, align 4
  %241 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length267 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %241, i32 0, i32 18
  %242 = load i32, i32* %part2_length267, align 4
  %243 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo268 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %243, i32 0, i32 23
  %244 = load %struct.plotting_data*, %struct.plotting_data** %pinfo268, align 8
  %LAMEsfbits = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %244, i32 0, i32 42
  %245 = load i32, i32* %gr.addr, align 4
  %arrayidx269 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %LAMEsfbits, i32 0, i32 %245
  %246 = load i32, i32* %ch.addr, align 4
  %arrayidx270 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx269, i32 0, i32 %246
  store i32 %242, i32* %arrayidx270, align 4
  %over_count = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise, i32 0, i32 3
  %247 = load i32, i32* %over_count, align 4
  %248 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo271 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %248, i32 0, i32 23
  %249 = load %struct.plotting_data*, %struct.plotting_data** %pinfo271, align 8
  %over = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %249, i32 0, i32 29
  %250 = load i32, i32* %gr.addr, align 4
  %arrayidx272 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %over, i32 0, i32 %250
  %251 = load i32, i32* %ch.addr, align 4
  %arrayidx273 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx272, i32 0, i32 %251
  store i32 %247, i32* %arrayidx273, align 4
  %max_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise, i32 0, i32 2
  %252 = load float, float* %max_noise, align 4
  %conv274 = fpext float %252 to double
  %mul275 = fmul double %conv274, 1.000000e+01
  %253 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo276 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %253, i32 0, i32 23
  %254 = load %struct.plotting_data*, %struct.plotting_data** %pinfo276, align 8
  %max_noise277 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %254, i32 0, i32 31
  %255 = load i32, i32* %gr.addr, align 4
  %arrayidx278 = getelementptr inbounds [2 x [2 x double]], [2 x [2 x double]]* %max_noise277, i32 0, i32 %255
  %256 = load i32, i32* %ch.addr, align 4
  %arrayidx279 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx278, i32 0, i32 %256
  store double %mul275, double* %arrayidx279, align 8
  %over_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise, i32 0, i32 0
  %257 = load float, float* %over_noise, align 4
  %conv280 = fpext float %257 to double
  %mul281 = fmul double %conv280, 1.000000e+01
  %258 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo282 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %258, i32 0, i32 23
  %259 = load %struct.plotting_data*, %struct.plotting_data** %pinfo282, align 8
  %over_noise283 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %259, i32 0, i32 32
  %260 = load i32, i32* %gr.addr, align 4
  %arrayidx284 = getelementptr inbounds [2 x [2 x double]], [2 x [2 x double]]* %over_noise283, i32 0, i32 %260
  %261 = load i32, i32* %ch.addr, align 4
  %arrayidx285 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx284, i32 0, i32 %261
  store double %mul281, double* %arrayidx285, align 8
  %tot_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise, i32 0, i32 1
  %262 = load float, float* %tot_noise, align 4
  %conv286 = fpext float %262 to double
  %mul287 = fmul double %conv286, 1.000000e+01
  %263 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo288 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %263, i32 0, i32 23
  %264 = load %struct.plotting_data*, %struct.plotting_data** %pinfo288, align 8
  %tot_noise289 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %264, i32 0, i32 30
  %265 = load i32, i32* %gr.addr, align 4
  %arrayidx290 = getelementptr inbounds [2 x [2 x double]], [2 x [2 x double]]* %tot_noise289, i32 0, i32 %265
  %266 = load i32, i32* %ch.addr, align 4
  %arrayidx291 = getelementptr inbounds [2 x double], [2 x double]* %arrayidx290, i32 0, i32 %266
  store double %mul287, double* %arrayidx291, align 8
  %over_SSD = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise, i32 0, i32 4
  %267 = load i32, i32* %over_SSD, align 4
  %268 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo292 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %268, i32 0, i32 23
  %269 = load %struct.plotting_data*, %struct.plotting_data** %pinfo292, align 8
  %over_SSD293 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %269, i32 0, i32 33
  %270 = load i32, i32* %gr.addr, align 4
  %arrayidx294 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %over_SSD293, i32 0, i32 %270
  %271 = load i32, i32* %ch.addr, align 4
  %arrayidx295 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx294, i32 0, i32 %271
  store i32 %267, i32* %arrayidx295, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal float @ATHmdct(%struct.SessionConfig_t* %cfg, float %f) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %f.addr = alloca float, align 4
  %ath = alloca float, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  store float %f, float* %f.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %1 = load float, float* %f.addr, align 4
  %call = call float @ATHformula(%struct.SessionConfig_t* %0, float %1)
  store float %call, float* %ath, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATHfixpoint = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 52
  %3 = load float, float* %ATHfixpoint, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATHfixpoint1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 52
  %5 = load float, float* %ATHfixpoint1, align 4
  %6 = load float, float* %ath, align 4
  %sub = fsub float %6, %5
  store float %sub, float* %ath, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load float, float* %ath, align 4
  %sub2 = fsub float %7, 1.000000e+02
  store float %sub2, float* %ath, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %ATH_offset_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 45
  %9 = load float, float* %ATH_offset_db, align 4
  %10 = load float, float* %ath, align 4
  %add = fadd float %10, %9
  store float %add, float* %ath, align 4
  %11 = load float, float* %ath, align 4
  %mul = fmul float %11, 0x3FB99999A0000000
  %12 = call float @llvm.pow.f32(float 1.000000e+01, float %mul)
  store float %12, float* %ath, align 4
  %13 = load float, float* %ath, align 4
  ret float %13
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log10.f64(double) #1

declare float @ATHformula(%struct.SessionConfig_t*, float) #2

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { argmemonly nounwind willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
