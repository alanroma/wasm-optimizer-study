; ModuleID = 'vmsg.c'
source_filename = "vmsg.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.vmsg = type { float*, i8*, i32, i32, %struct.lame_global_struct* }
%struct.lame_global_struct = type opaque

; Function Attrs: noinline nounwind optnone
define %struct.vmsg* @vmsg_init(i32 %rate) #0 {
entry:
  %retval = alloca %struct.vmsg*, align 4
  %rate.addr = alloca i32, align 4
  %v = alloca %struct.vmsg*, align 4
  store i32 %rate, i32* %rate.addr, align 4
  %call = call i8* @calloc(i32 1, i32 20)
  %0 = bitcast i8* %call to %struct.vmsg*
  store %struct.vmsg* %0, %struct.vmsg** %v, align 4
  %1 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %tobool = icmp ne %struct.vmsg* %1, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %err

if.end:                                           ; preds = %entry
  %2 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %2, i32 0, i32 2
  store i32 0, i32* %size, align 4
  %3 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %max_size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %3, i32 0, i32 3
  store i32 1048576, i32* %max_size, align 4
  %4 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %max_size1 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %4, i32 0, i32 3
  %5 = load i32, i32* %max_size1, align 4
  %call2 = call i8* @malloc(i32 %5)
  %6 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %mp3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %6, i32 0, i32 1
  store i8* %call2, i8** %mp3, align 4
  %7 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %mp33 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %7, i32 0, i32 1
  %8 = load i8*, i8** %mp33, align 4
  %tobool4 = icmp ne i8* %8, null
  br i1 %tobool4, label %if.end6, label %if.then5

if.then5:                                         ; preds = %if.end
  br label %err

if.end6:                                          ; preds = %if.end
  %call7 = call i8* @malloc(i32 65536)
  %9 = bitcast i8* %call7 to float*
  %10 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %pcm_l = getelementptr inbounds %struct.vmsg, %struct.vmsg* %10, i32 0, i32 0
  store float* %9, float** %pcm_l, align 4
  %11 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %pcm_l8 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %11, i32 0, i32 0
  %12 = load float*, float** %pcm_l8, align 4
  %tobool9 = icmp ne float* %12, null
  br i1 %tobool9, label %if.end11, label %if.then10

if.then10:                                        ; preds = %if.end6
  br label %err

if.end11:                                         ; preds = %if.end6
  %call12 = call %struct.lame_global_struct* @lame_init()
  %13 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp = getelementptr inbounds %struct.vmsg, %struct.vmsg* %13, i32 0, i32 4
  store %struct.lame_global_struct* %call12, %struct.lame_global_struct** %gfp, align 4
  %14 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp13 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %14, i32 0, i32 4
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp13, align 4
  %tobool14 = icmp ne %struct.lame_global_struct* %15, null
  br i1 %tobool14, label %if.end16, label %if.then15

if.then15:                                        ; preds = %if.end11
  br label %err

if.end16:                                         ; preds = %if.end11
  %16 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp17 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %16, i32 0, i32 4
  %17 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp17, align 4
  %call18 = call i32 @lame_set_mode(%struct.lame_global_struct* %17, i32 3)
  %18 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp19 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %18, i32 0, i32 4
  %19 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp19, align 4
  %call20 = call i32 @lame_set_num_channels(%struct.lame_global_struct* %19, i32 1)
  %20 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp21 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %20, i32 0, i32 4
  %21 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp21, align 4
  %22 = load i32, i32* %rate.addr, align 4
  %call22 = call i32 @lame_set_in_samplerate(%struct.lame_global_struct* %21, i32 %22)
  %23 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp23 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %23, i32 0, i32 4
  %24 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp23, align 4
  %call24 = call i32 @lame_set_VBR(%struct.lame_global_struct* %24, i32 4)
  %25 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp25 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %25, i32 0, i32 4
  %26 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp25, align 4
  %call26 = call i32 @lame_set_VBR_quality(%struct.lame_global_struct* %26, float 5.000000e+00)
  %27 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  %gfp27 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %27, i32 0, i32 4
  %28 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp27, align 4
  %call28 = call i32 @lame_init_params(%struct.lame_global_struct* %28)
  %cmp = icmp slt i32 %call28, 0
  br i1 %cmp, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end16
  br label %err

if.end30:                                         ; preds = %if.end16
  %29 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  store %struct.vmsg* %29, %struct.vmsg** %retval, align 4
  br label %return

err:                                              ; preds = %if.then29, %if.then15, %if.then10, %if.then5, %if.then
  %30 = load %struct.vmsg*, %struct.vmsg** %v, align 4
  call void @vmsg_free(%struct.vmsg* %30)
  store %struct.vmsg* null, %struct.vmsg** %retval, align 4
  br label %return

return:                                           ; preds = %err, %if.end30
  %31 = load %struct.vmsg*, %struct.vmsg** %retval, align 4
  ret %struct.vmsg* %31
}

declare i8* @calloc(i32, i32) #1

declare i8* @malloc(i32) #1

declare %struct.lame_global_struct* @lame_init() #1

declare i32 @lame_set_mode(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_num_channels(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_in_samplerate(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_VBR(%struct.lame_global_struct*, i32) #1

declare i32 @lame_set_VBR_quality(%struct.lame_global_struct*, float) #1

declare i32 @lame_init_params(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define void @vmsg_free(%struct.vmsg* %v) #0 {
entry:
  %v.addr = alloca %struct.vmsg*, align 4
  store %struct.vmsg* %v, %struct.vmsg** %v.addr, align 4
  %0 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %tobool = icmp ne %struct.vmsg* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %gfp = getelementptr inbounds %struct.vmsg, %struct.vmsg* %1, i32 0, i32 4
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %call = call i32 @lame_close(%struct.lame_global_struct* %2)
  %3 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %pcm_l = getelementptr inbounds %struct.vmsg, %struct.vmsg* %3, i32 0, i32 0
  %4 = load float*, float** %pcm_l, align 4
  %5 = bitcast float* %4 to i8*
  call void @free(i8* %5)
  %6 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %6, i32 0, i32 1
  %7 = load i8*, i8** %mp3, align 4
  call void @free(i8* %7)
  %8 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %9 = bitcast %struct.vmsg* %8 to i8*
  call void @free(i8* %9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define i32 @vmsg_encode(%struct.vmsg* %v, i32 %nsamples) #0 {
entry:
  %retval = alloca i32, align 4
  %v.addr = alloca %struct.vmsg*, align 4
  %nsamples.addr = alloca i32, align 4
  %buf = alloca i8*, align 4
  %n = alloca i32, align 4
  store %struct.vmsg* %v, %struct.vmsg** %v.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  %0 = load i32, i32* %nsamples.addr, align 4
  %cmp = icmp sgt i32 %0, 16384
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %call = call i32 @fix_mp3_size(%struct.vmsg* %1)
  %cmp1 = icmp slt i32 %call, 0
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %2, i32 0, i32 1
  %3 = load i8*, i8** %mp3, align 4
  %4 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %4, i32 0, i32 2
  %5 = load i32, i32* %size, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %5
  store i8* %add.ptr, i8** %buf, align 4
  %6 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %gfp = getelementptr inbounds %struct.vmsg, %struct.vmsg* %6, i32 0, i32 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %8 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %pcm_l = getelementptr inbounds %struct.vmsg, %struct.vmsg* %8, i32 0, i32 0
  %9 = load float*, float** %pcm_l, align 4
  %10 = load i32, i32* %nsamples.addr, align 4
  %11 = load i8*, i8** %buf, align 4
  %call4 = call i32 @lame_encode_buffer_ieee_float(%struct.lame_global_struct* %7, float* %9, float* null, i32 %10, i8* %11, i32 27680)
  store i32 %call4, i32* %n, align 4
  %12 = load i32, i32* %n, align 4
  %cmp5 = icmp slt i32 %12, 0
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end3
  %13 = load i32, i32* %n, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end3
  %14 = load i32, i32* %n, align 4
  %15 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %size8 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %15, i32 0, i32 2
  %16 = load i32, i32* %size8, align 4
  %add = add i32 %16, %14
  store i32 %add, i32* %size8, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end7, %if.then6, %if.then2, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

; Function Attrs: noinline nounwind optnone
define internal i32 @fix_mp3_size(%struct.vmsg* %v) #0 {
entry:
  %retval = alloca i32, align 4
  %v.addr = alloca %struct.vmsg*, align 4
  store %struct.vmsg* %v, %struct.vmsg** %v.addr, align 4
  %0 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %0, i32 0, i32 2
  %1 = load i32, i32* %size, align 4
  %conv = uitofp i32 %1 to double
  %add = fadd double %conv, 2.768000e+04
  %2 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %max_size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %2, i32 0, i32 3
  %3 = load i32, i32* %max_size, align 4
  %conv1 = uitofp i32 %3 to double
  %cmp = fcmp ogt double %add, %conv1
  br i1 %cmp, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %4 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %max_size3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %4, i32 0, i32 3
  %5 = load i32, i32* %max_size3, align 4
  %mul = mul i32 %5, 2
  store i32 %mul, i32* %max_size3, align 4
  %6 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %6, i32 0, i32 1
  %7 = load i8*, i8** %mp3, align 4
  %8 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %max_size4 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %8, i32 0, i32 3
  %9 = load i32, i32* %max_size4, align 4
  %call = call i8* @realloc(i8* %7, i32 %9)
  %10 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp35 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %10, i32 0, i32 1
  store i8* %call, i8** %mp35, align 4
  %11 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp36 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %11, i32 0, i32 1
  %12 = load i8*, i8** %mp36, align 4
  %tobool = icmp ne i8* %12, null
  br i1 %tobool, label %if.end, label %if.then7

if.then7:                                         ; preds = %if.then
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end8, %if.then7
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

declare i32 @lame_encode_buffer_ieee_float(%struct.lame_global_struct*, float*, float*, i32, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define i32 @vmsg_flush(%struct.vmsg* %v) #0 {
entry:
  %retval = alloca i32, align 4
  %v.addr = alloca %struct.vmsg*, align 4
  %buf = alloca i8*, align 4
  %n = alloca i32, align 4
  store %struct.vmsg* %v, %struct.vmsg** %v.addr, align 4
  %0 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %call = call i32 @fix_mp3_size(%struct.vmsg* %0)
  %cmp = icmp slt i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp3 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %1, i32 0, i32 1
  %2 = load i8*, i8** %mp3, align 4
  %3 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %size = getelementptr inbounds %struct.vmsg, %struct.vmsg* %3, i32 0, i32 2
  %4 = load i32, i32* %size, align 4
  %add.ptr = getelementptr inbounds i8, i8* %2, i32 %4
  store i8* %add.ptr, i8** %buf, align 4
  %5 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %gfp = getelementptr inbounds %struct.vmsg, %struct.vmsg* %5, i32 0, i32 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %7 = load i8*, i8** %buf, align 4
  %call1 = call i32 @lame_encode_flush(%struct.lame_global_struct* %6, i8* %7, i32 27680)
  store i32 %call1, i32* %n, align 4
  %8 = load i32, i32* %n, align 4
  %cmp2 = icmp slt i32 %8, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %9 = load i32, i32* %n, align 4
  %10 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %size5 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %10, i32 0, i32 2
  %11 = load i32, i32* %size5, align 4
  %add = add i32 %11, %9
  store i32 %add, i32* %size5, align 4
  %12 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %gfp6 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %12, i32 0, i32 4
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp6, align 4
  %14 = load %struct.vmsg*, %struct.vmsg** %v.addr, align 4
  %mp37 = getelementptr inbounds %struct.vmsg, %struct.vmsg* %14, i32 0, i32 1
  %15 = load i8*, i8** %mp37, align 4
  %call8 = call i32 @lame_get_lametag_frame(%struct.lame_global_struct* %13, i8* %15, i32 27680)
  store i32 %call8, i32* %n, align 4
  %16 = load i32, i32* %n, align 4
  %cmp9 = icmp slt i32 %16, 0
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end11, %if.then10, %if.then3, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare i32 @lame_encode_flush(%struct.lame_global_struct*, i8*, i32) #1

declare i32 @lame_get_lametag_frame(%struct.lame_global_struct*, i8*, i32) #1

declare i32 @lame_close(%struct.lame_global_struct*) #1

declare void @free(i8*) #1

declare i8* @realloc(i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
