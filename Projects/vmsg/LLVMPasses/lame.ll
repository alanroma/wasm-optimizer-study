; ModuleID = 'lame.c'
source_filename = "lame.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.q_map = type { i32, float, float, float, float, i32 }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.band_pass_t = type { i32, i32 }
%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.2, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon.0], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon.0 = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type { [20 x float], float*, [2411 x float], float*, [2411 x float], float*, [20 x float], float*, [2411 x float], float*, [2411 x float], float*, i32, i32, double, double, i32, i32, [12000 x i32], [12000 x i32] }
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.1, %struct.anon.1 }
%struct.anon.1 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.2 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.anon.3 = type { i32, i32, i32 }
%struct._IO_FILE = type opaque

@__const.lame_init_params.m = private unnamed_addr constant [9 x %struct.q_map] [%struct.q_map { i32 48000, float 0.000000e+00, float 6.500000e+00, float 0.000000e+00, float 6.500000e+00, i32 23700 }, %struct.q_map { i32 44100, float 0.000000e+00, float 6.500000e+00, float 0.000000e+00, float 6.500000e+00, i32 21780 }, %struct.q_map { i32 32000, float 6.500000e+00, float 8.000000e+00, float 0x4014CCCCC0000000, float 6.500000e+00, i32 15800 }, %struct.q_map { i32 24000, float 8.000000e+00, float 8.500000e+00, float 0x4014CCCCC0000000, float 6.000000e+00, i32 11850 }, %struct.q_map { i32 22050, float 8.500000e+00, float 0x4022051EC0000000, float 0x4014CCCCC0000000, float 6.500000e+00, i32 10892 }, %struct.q_map { i32 16000, float 0x4022051EC0000000, float 0x4022CCCCC0000000, float 0x40139999A0000000, float 6.500000e+00, i32 7903 }, %struct.q_map { i32 12000, float 0x4022CCCCC0000000, float 0x4023333340000000, float 4.500000e+00, float 6.000000e+00, i32 5928 }, %struct.q_map { i32 11025, float 0x4023333340000000, float 0x4023CCCCC0000000, float 0x4014666660000000, float 6.500000e+00, i32 5446 }, %struct.q_map { i32 8000, float 0x4023CCCCC0000000, float 1.000000e+01, float 0x40139999A0000000, float 6.500000e+00, i32 3952 }], align 16
@__const.lame_init_params.x = private unnamed_addr constant [11 x i32] [i32 19500, i32 19000, i32 18600, i32 18000, i32 17500, i32 16000, i32 15600, i32 14900, i32 12500, i32 10000, i32 3950], align 16
@__const.lame_init_params.x.1 = private unnamed_addr constant [11 x i32] [i32 24000, i32 19500, i32 18500, i32 18000, i32 17500, i32 17000, i32 16500, i32 15600, i32 15200, i32 7230, i32 3950], align 16
@__const.lame_init_params.x.2 = private unnamed_addr constant [11 x i32] [i32 19500, i32 19000, i32 18500, i32 18000, i32 17500, i32 16500, i32 15500, i32 14500, i32 12500, i32 9500, i32 3950], align 16
@__const.lame_init_params.cmp = private unnamed_addr constant [10 x float] [float 0x4016CCCCC0000000, float 6.500000e+00, float 0x401D333340000000, float 0x4020666660000000, float 1.000000e+01, float 0x4027CCCCC0000000, float 1.300000e+01, float 1.400000e+01, float 1.500000e+01, float 1.650000e+01], align 16
@sfBandIndex = external constant [9 x %struct.scalefac_struct], align 16
@bitrate_table = external constant [3 x [16 x i32]], align 16
@__const.lame_init_params.m.3 = private unnamed_addr constant [2 x [2 x float]] [[2 x float] [float 1.000000e+00, float 0.000000e+00], [2 x float] [float 0.000000e+00, float 1.000000e+00]], align 16
@.str = private unnamed_addr constant [17 x i8] c"LAME %s %s (%s)\0A\00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c", \00", align 1
@.str.5 = private unnamed_addr constant [4 x i8] c"MMX\00", align 1
@.str.6 = private unnamed_addr constant [18 x i8] c"3DNow! (ASM used)\00", align 1
@.str.7 = private unnamed_addr constant [7 x i8] c"3DNow!\00", align 1
@.str.8 = private unnamed_addr constant [15 x i8] c"SSE (ASM used)\00", align 1
@.str.9 = private unnamed_addr constant [4 x i8] c"SSE\00", align 1
@.str.10 = private unnamed_addr constant [16 x i8] c"SSE2 (ASM used)\00", align 1
@.str.11 = private unnamed_addr constant [5 x i8] c"SSE2\00", align 1
@.str.12 = private unnamed_addr constant [18 x i8] c"CPU features: %s\0A\00", align 1
@.str.13 = private unnamed_addr constant [68 x i8] c"Autoconverting from stereo to mono. Setting encoding to mono mode.\0A\00", align 1
@.str.14 = private unnamed_addr constant [42 x i8] c"Resampling:  input %g kHz  output %g kHz\0A\00", align 1
@.str.15 = private unnamed_addr constant [71 x i8] c"Using polyphase highpass filter, transition band: %5.0f Hz - %5.0f Hz\0A\00", align 1
@.str.16 = private unnamed_addr constant [70 x i8] c"Using polyphase lowpass filter, transition band: %5.0f Hz - %5.0f Hz\0A\00", align 1
@.str.17 = private unnamed_addr constant [35 x i8] c"polyphase lowpass filter disabled\0A\00", align 1
@.str.18 = private unnamed_addr constant [61 x i8] c"Warning: many decoders cannot handle free format bitstreams\0A\00", align 1
@.str.19 = private unnamed_addr constant [89 x i8] c"Warning: many decoders cannot handle free format bitrates >320 kbps (see documentation)\0A\00", align 1
@.str.20 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@.str.21 = private unnamed_addr constant [9 x i8] c"\0Amisc:\0A\0A\00", align 1
@.str.22 = private unnamed_addr constant [14 x i8] c"\09scaling: %g\0A\00", align 1
@.str.23 = private unnamed_addr constant [25 x i8] c"\09ch0 (left) scaling: %g\0A\00", align 1
@.str.24 = private unnamed_addr constant [26 x i8] c"\09ch1 (right) scaling: %g\0A\00", align 1
@.str.25 = private unnamed_addr constant [7 x i8] c"normal\00", align 1
@.str.26 = private unnamed_addr constant [20 x i8] c"best (outside loop)\00", align 1
@.str.27 = private unnamed_addr constant [25 x i8] c"best (inside loop, slow)\00", align 1
@.str.28 = private unnamed_addr constant [21 x i8] c"\09huffman search: %s\0A\00", align 1
@.str.29 = private unnamed_addr constant [20 x i8] c"\09experimental Y=%d\0A\00", align 1
@.str.30 = private unnamed_addr constant [6 x i8] c"\09...\0A\00", align 1
@.str.31 = private unnamed_addr constant [18 x i8] c"\0Astream format:\0A\0A\00", align 1
@.str.32 = private unnamed_addr constant [4 x i8] c"2.5\00", align 1
@.str.33 = private unnamed_addr constant [2 x i8] c"1\00", align 1
@.str.34 = private unnamed_addr constant [2 x i8] c"2\00", align 1
@.str.35 = private unnamed_addr constant [2 x i8] c"?\00", align 1
@.str.36 = private unnamed_addr constant [18 x i8] c"\09MPEG-%s Layer 3\0A\00", align 1
@.str.37 = private unnamed_addr constant [13 x i8] c"joint stereo\00", align 1
@.str.38 = private unnamed_addr constant [7 x i8] c"stereo\00", align 1
@.str.39 = private unnamed_addr constant [13 x i8] c"dual channel\00", align 1
@.str.40 = private unnamed_addr constant [5 x i8] c"mono\00", align 1
@.str.41 = private unnamed_addr constant [16 x i8] c"not set (error)\00", align 1
@.str.42 = private unnamed_addr constant [16 x i8] c"unknown (error)\00", align 1
@.str.43 = private unnamed_addr constant [18 x i8] c"\09%d channel - %s\0A\00", align 1
@.str.44 = private unnamed_addr constant [4 x i8] c"off\00", align 1
@.str.45 = private unnamed_addr constant [4 x i8] c"all\00", align 1
@.str.46 = private unnamed_addr constant [14 x i8] c"\09padding: %s\0A\00", align 1
@.str.47 = private unnamed_addr constant [10 x i8] c"(default)\00", align 1
@.str.48 = private unnamed_addr constant [14 x i8] c"(free format)\00", align 1
@.str.49 = private unnamed_addr constant [28 x i8] c"\09constant bitrate - CBR %s\0A\00", align 1
@.str.50 = private unnamed_addr constant [28 x i8] c"\09variable bitrate - ABR %s\0A\00", align 1
@.str.51 = private unnamed_addr constant [31 x i8] c"\09variable bitrate - VBR rh %s\0A\00", align 1
@.str.52 = private unnamed_addr constant [31 x i8] c"\09variable bitrate - VBR mt %s\0A\00", align 1
@.str.53 = private unnamed_addr constant [33 x i8] c"\09variable bitrate - VBR mtrh %s\0A\00", align 1
@.str.54 = private unnamed_addr constant [29 x i8] c"\09 ?? oops, some new one ?? \0A\00", align 1
@.str.55 = private unnamed_addr constant [17 x i8] c"\09using LAME Tag\0A\00", align 1
@.str.56 = private unnamed_addr constant [19 x i8] c"\0Apsychoacoustic:\0A\0A\00", align 1
@.str.57 = private unnamed_addr constant [8 x i8] c"allowed\00", align 1
@.str.58 = private unnamed_addr constant [16 x i8] c"channel coupled\00", align 1
@.str.59 = private unnamed_addr constant [10 x i8] c"dispensed\00", align 1
@.str.60 = private unnamed_addr constant [7 x i8] c"forced\00", align 1
@.str.61 = private unnamed_addr constant [25 x i8] c"\09using short blocks: %s\0A\00", align 1
@.str.62 = private unnamed_addr constant [20 x i8] c"\09subblock gain: %d\0A\00", align 1
@.str.63 = private unnamed_addr constant [24 x i8] c"\09adjust masking: %g dB\0A\00", align 1
@.str.64 = private unnamed_addr constant [30 x i8] c"\09adjust masking short: %g dB\0A\00", align 1
@.str.65 = private unnamed_addr constant [30 x i8] c"\09quantization comparison: %d\0A\00", align 1
@.str.66 = private unnamed_addr constant [33 x i8] c"\09 ^ comparison short blocks: %d\0A\00", align 1
@.str.67 = private unnamed_addr constant [20 x i8] c"\09noise shaping: %d\0A\00", align 1
@.str.68 = private unnamed_addr constant [23 x i8] c"\09 ^ amplification: %d\0A\00", align 1
@.str.69 = private unnamed_addr constant [18 x i8] c"\09 ^ stopping: %d\0A\00", align 1
@.str.70 = private unnamed_addr constant [6 x i8] c"using\00", align 1
@.str.71 = private unnamed_addr constant [34 x i8] c"the only masking for short blocks\00", align 1
@.str.72 = private unnamed_addr constant [17 x i8] c"the only masking\00", align 1
@.str.73 = private unnamed_addr constant [9 x i8] c"not used\00", align 1
@.str.74 = private unnamed_addr constant [10 x i8] c"\09ATH: %s\0A\00", align 1
@.str.75 = private unnamed_addr constant [14 x i8] c"\09 ^ type: %d\0A\00", align 1
@.str.76 = private unnamed_addr constant [17 x i8] c"\09 ^ shape: %g%s\0A\00", align 1
@.str.77 = private unnamed_addr constant [19 x i8] c" (only for type 4)\00", align 1
@.str.78 = private unnamed_addr constant [30 x i8] c"\09 ^ level adjustement: %g dB\0A\00", align 1
@.str.79 = private unnamed_addr constant [21 x i8] c"\09 ^ adjust type: %d\0A\00", align 1
@.str.80 = private unnamed_addr constant [34 x i8] c"\09 ^ adjust sensitivity power: %f\0A\00", align 1
@.str.81 = private unnamed_addr constant [44 x i8] c"\09experimental psy tunings by Naoki Shibata\0A\00", align 1
@.str.82 = private unnamed_addr constant [70 x i8] c"\09   adjust masking bass=%g dB, alto=%g dB, treble=%g dB, sfb21=%g dB\0A\00", align 1
@.str.83 = private unnamed_addr constant [4 x i8] c"yes\00", align 1
@.str.84 = private unnamed_addr constant [3 x i8] c"no\00", align 1
@.str.85 = private unnamed_addr constant [36 x i8] c"\09using temporal masking effect: %s\0A\00", align 1
@.str.86 = private unnamed_addr constant [33 x i8] c"\09interchannel masking ratio: %g\0A\00", align 1
@.str.87 = private unnamed_addr constant [2 x i8] c"\0A\00", align 1
@.str.88 = private unnamed_addr constant [35 x i8] c"Error: could not update LAME tag.\0A\00", align 1
@.str.89 = private unnamed_addr constant [54 x i8] c"Error: could not update LAME tag, file not seekable.\0A\00", align 1
@.str.90 = private unnamed_addr constant [54 x i8] c"Error: could not update LAME tag, file not readable.\0A\00", align 1
@__const.optimum_bandwidth.freq_map = private unnamed_addr constant [17 x %struct.band_pass_t] [%struct.band_pass_t { i32 8, i32 2000 }, %struct.band_pass_t { i32 16, i32 3700 }, %struct.band_pass_t { i32 24, i32 3900 }, %struct.band_pass_t { i32 32, i32 5500 }, %struct.band_pass_t { i32 40, i32 7000 }, %struct.band_pass_t { i32 48, i32 7500 }, %struct.band_pass_t { i32 56, i32 10000 }, %struct.band_pass_t { i32 64, i32 11000 }, %struct.band_pass_t { i32 80, i32 13500 }, %struct.band_pass_t { i32 96, i32 15100 }, %struct.band_pass_t { i32 112, i32 15600 }, %struct.band_pass_t { i32 128, i32 17000 }, %struct.band_pass_t { i32 160, i32 17500 }, %struct.band_pass_t { i32 192, i32 18600 }, %struct.band_pass_t { i32 224, i32 19400 }, %struct.band_pass_t { i32 256, i32 19700 }, %struct.band_pass_t { i32 320, i32 20500 }], align 16
@.str.91 = private unnamed_addr constant [66 x i8] c"Warning: highpass filter disabled.  highpass frequency too small\0A\00", align 1
@.str.92 = private unnamed_addr constant [40 x i8] c"Error: can't allocate in_buffer buffer\0A\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp eq %struct.lame_global_struct* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %class_id = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 0
  %2 = load i32, i32* %class_id, align 4
  %cmp1 = icmp ne i32 %2, -487877
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cmp = icmp eq %struct.lame_internal_flags* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %class_id = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 0
  %2 = load i32, i32* %class_id, align 8
  %cmp1 = icmp ne i32 %2, -487877
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %lame_init_params_successful = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 1
  %4 = load i32, i32* %lame_init_params_successful, align 4
  %cmp4 = icmp sle i32 %4, 0
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then2, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_init_params(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %v = alloca i32, align 4
  %qval = alloca float, align 4
  %m = alloca [9 x %struct.q_map], align 16
  %d = alloca double, align 8
  %q_ = alloca float, align 4
  %t_ = alloca float, align 4
  %d308 = alloca double, align 8
  %lowpass = alloca double, align 8
  %highpass = alloca double, align 8
  %x = alloca [11 x i32], align 16
  %a = alloca double, align 8
  %b = alloca double, align 8
  %m361 = alloca double, align 8
  %x368 = alloca [11 x i32], align 16
  %a377 = alloca double, align 8
  %b381 = alloca double, align 8
  %m386 = alloca double, align 8
  %x393 = alloca [11 x i32], align 16
  %a402 = alloca double, align 8
  %b406 = alloca double, align 8
  %m411 = alloca double, align 8
  %cmp541 = alloca [10 x float], align 16
  %size = alloca i32, align 4
  %start = alloca i32, align 4
  %size758 = alloca i32, align 4
  %start767 = alloca i32, align 4
  %k = alloca i32, align 4
  %m1184 = alloca [2 x [2 x float]], align 16
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp = icmp eq %struct.lame_internal_flags* %3, null
  br i1 %cmp, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end2:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call3 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %4)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end2
  store i32 -1, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end2
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %class_id = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 0
  store i32 -487877, i32* %class_id, align 8
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %lame_init_params_successful = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 1
  store i32 0, i32* %lame_init_params_successful, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %7, i32 0, i32 3
  %8 = load i32, i32* %samplerate_in, align 4
  %cmp7 = icmp slt i32 %8, 1
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 -1, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %9, i32 0, i32 2
  %10 = load i32, i32* %num_channels, align 4
  %cmp10 = icmp slt i32 %10, 1
  br i1 %cmp10, label %if.then13, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end9
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels11 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %11, i32 0, i32 2
  %12 = load i32, i32* %num_channels11, align 4
  %cmp12 = icmp slt i32 2, %12
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %lor.lhs.false, %if.end9
  store i32 -1, i32* %retval, align 4
  br label %return

if.end14:                                         ; preds = %lor.lhs.false
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %13, i32 0, i32 4
  %14 = load i32, i32* %samplerate_out, align 4
  %cmp15 = icmp ne i32 %14, 0
  br i1 %cmp15, label %if.then16, label %if.end22

if.then16:                                        ; preds = %if.end14
  store i32 0, i32* %v, align 4
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out17 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %15, i32 0, i32 4
  %16 = load i32, i32* %samplerate_out17, align 4
  %call18 = call i32 @SmpFrqIndex(i32 %16, i32* %v)
  %cmp19 = icmp slt i32 %call18, 0
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.then16
  store i32 -1, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.then16
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.end14
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg23 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg23, %struct.SessionConfig_t** %cfg, align 4
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_hard_min = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %18, i32 0, i32 45
  %19 = load i32, i32* %VBR_hard_min, align 4
  %20 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %enforce_min_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %20, i32 0, i32 27
  store i32 %19, i32* %enforce_min_bitrate, align 4
  %21 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %analysis = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %21, i32 0, i32 8
  %22 = load i32, i32* %analysis, align 4
  %23 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis24 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %23, i32 0, i32 31
  store i32 %22, i32* %analysis24, align 4
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis25 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 31
  %25 = load i32, i32* %analysis25, align 4
  %tobool26 = icmp ne i32 %25, 0
  br i1 %tobool26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.end22
  %26 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %26, i32 0, i32 9
  store i32 0, i32* %write_lame_tag, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.end22
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 23
  %28 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  %cmp29 = icmp ne %struct.plotting_data* %28, null
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %if.end28
  %29 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag31 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %29, i32 0, i32 9
  store i32 0, i32* %write_lame_tag31, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %if.end28
  %30 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %30, i32 0, i32 68
  %msgf = getelementptr inbounds %struct.anon, %struct.anon* %report, i32 0, i32 0
  %31 = load void (i8*, i8*)*, void (i8*, i8*)** %msgf, align 4
  %32 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %report_msg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %32, i32 0, i32 28
  store void (i8*, i8*)* %31, void (i8*, i8*)** %report_msg, align 4
  %33 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report33 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %33, i32 0, i32 68
  %debugf = getelementptr inbounds %struct.anon, %struct.anon* %report33, i32 0, i32 1
  %34 = load void (i8*, i8*)*, void (i8*, i8*)** %debugf, align 4
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %report_dbg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %35, i32 0, i32 29
  store void (i8*, i8*)* %34, void (i8*, i8*)** %report_dbg, align 8
  %36 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report34 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %36, i32 0, i32 68
  %errorf = getelementptr inbounds %struct.anon, %struct.anon* %report34, i32 0, i32 2
  %37 = load void (i8*, i8*)*, void (i8*, i8*)** %errorf, align 4
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %report_err = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %38, i32 0, i32 30
  store void (i8*, i8*)* %37, void (i8*, i8*)** %report_err, align 4
  %39 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %39, i32 0, i32 71
  %amd3dnow = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations, i32 0, i32 1
  %40 = load i32, i32* %amd3dnow, align 4
  %tobool35 = icmp ne i32 %40, 0
  br i1 %tobool35, label %if.then36, label %if.else

if.then36:                                        ; preds = %if.end32
  %call37 = call i32 @has_3DNow()
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 19
  %42 = bitcast %struct.anon.2* %CPU_features to i32*
  %bf.load = load i32, i32* %42, align 8
  %bf.value = and i32 %call37, 1
  %bf.shl = shl i32 %bf.value, 1
  %bf.clear = and i32 %bf.load, -3
  %bf.set = or i32 %bf.clear, %bf.shl
  store i32 %bf.set, i32* %42, align 8
  br label %if.end41

if.else:                                          ; preds = %if.end32
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features38 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %43, i32 0, i32 19
  %44 = bitcast %struct.anon.2* %CPU_features38 to i32*
  %bf.load39 = load i32, i32* %44, align 8
  %bf.clear40 = and i32 %bf.load39, -3
  store i32 %bf.clear40, i32* %44, align 8
  br label %if.end41

if.end41:                                         ; preds = %if.else, %if.then36
  %45 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations42 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %45, i32 0, i32 71
  %mmx = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations42, i32 0, i32 0
  %46 = load i32, i32* %mmx, align 4
  %tobool43 = icmp ne i32 %46, 0
  br i1 %tobool43, label %if.then44, label %if.else51

if.then44:                                        ; preds = %if.end41
  %call45 = call i32 @has_MMX()
  %47 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features46 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %47, i32 0, i32 19
  %48 = bitcast %struct.anon.2* %CPU_features46 to i32*
  %bf.load47 = load i32, i32* %48, align 8
  %bf.value48 = and i32 %call45, 1
  %bf.clear49 = and i32 %bf.load47, -2
  %bf.set50 = or i32 %bf.clear49, %bf.value48
  store i32 %bf.set50, i32* %48, align 8
  br label %if.end55

if.else51:                                        ; preds = %if.end41
  %49 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features52 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %49, i32 0, i32 19
  %50 = bitcast %struct.anon.2* %CPU_features52 to i32*
  %bf.load53 = load i32, i32* %50, align 8
  %bf.clear54 = and i32 %bf.load53, -2
  store i32 %bf.clear54, i32* %50, align 8
  br label %if.end55

if.end55:                                         ; preds = %if.else51, %if.then44
  %51 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations56 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %51, i32 0, i32 71
  %sse = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations56, i32 0, i32 2
  %52 = load i32, i32* %sse, align 4
  %tobool57 = icmp ne i32 %52, 0
  br i1 %tobool57, label %if.then58, label %if.else73

if.then58:                                        ; preds = %if.end55
  %call59 = call i32 @has_SSE()
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features60 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %53, i32 0, i32 19
  %54 = bitcast %struct.anon.2* %CPU_features60 to i32*
  %bf.load61 = load i32, i32* %54, align 8
  %bf.value62 = and i32 %call59, 1
  %bf.shl63 = shl i32 %bf.value62, 2
  %bf.clear64 = and i32 %bf.load61, -5
  %bf.set65 = or i32 %bf.clear64, %bf.shl63
  store i32 %bf.set65, i32* %54, align 8
  %call66 = call i32 @has_SSE2()
  %55 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features67 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %55, i32 0, i32 19
  %56 = bitcast %struct.anon.2* %CPU_features67 to i32*
  %bf.load68 = load i32, i32* %56, align 8
  %bf.value69 = and i32 %call66, 1
  %bf.shl70 = shl i32 %bf.value69, 3
  %bf.clear71 = and i32 %bf.load68, -9
  %bf.set72 = or i32 %bf.clear71, %bf.shl70
  store i32 %bf.set72, i32* %56, align 8
  br label %if.end80

if.else73:                                        ; preds = %if.end55
  %57 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features74 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %57, i32 0, i32 19
  %58 = bitcast %struct.anon.2* %CPU_features74 to i32*
  %bf.load75 = load i32, i32* %58, align 8
  %bf.clear76 = and i32 %bf.load75, -5
  store i32 %bf.clear76, i32* %58, align 8
  %59 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features77 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %59, i32 0, i32 19
  %60 = bitcast %struct.anon.2* %CPU_features77 to i32*
  %bf.load78 = load i32, i32* %60, align 8
  %bf.clear79 = and i32 %bf.load78, -9
  store i32 %bf.clear79, i32* %60, align 8
  br label %if.end80

if.end80:                                         ; preds = %if.else73, %if.then58
  %61 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %61, i32 0, i32 39
  %62 = load i32, i32* %VBR, align 4
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 22
  store i32 %62, i32* %vbr, align 4
  %64 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %error_protection = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %64, i32 0, i32 30
  %65 = load i32, i32* %error_protection, align 4
  %66 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection81 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %66, i32 0, i32 36
  store i32 %65, i32* %error_protection81, align 4
  %67 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %copyright = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %67, i32 0, i32 26
  %68 = load i32, i32* %copyright, align 4
  %69 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %copyright82 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %69, i32 0, i32 37
  store i32 %68, i32* %copyright82, align 4
  %70 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %original = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %70, i32 0, i32 27
  %71 = load i32, i32* %original, align 4
  %72 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %original83 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %72, i32 0, i32 38
  store i32 %71, i32* %original83, align 4
  %73 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %extension = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %73, i32 0, i32 28
  %74 = load i32, i32* %extension, align 4
  %75 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %extension84 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %75, i32 0, i32 39
  store i32 %74, i32* %extension84, align 4
  %76 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %emphasis = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %76, i32 0, i32 29
  %77 = load i32, i32* %emphasis, align 4
  %78 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %emphasis85 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %78, i32 0, i32 40
  store i32 %77, i32* %emphasis85, align 4
  %79 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels86 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %79, i32 0, i32 2
  %80 = load i32, i32* %num_channels86, align 4
  %81 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %81, i32 0, i32 13
  store i32 %80, i32* %channels_in, align 4
  %82 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_in87 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %82, i32 0, i32 13
  %83 = load i32, i32* %channels_in87, align 4
  %cmp88 = icmp eq i32 %83, 1
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.end80
  %84 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %84, i32 0, i32 12
  store i32 3, i32* %mode, align 4
  br label %if.end90

if.end90:                                         ; preds = %if.then89, %if.end80
  %85 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode91 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %85, i32 0, i32 12
  %86 = load i32, i32* %mode91, align 4
  %cmp92 = icmp eq i32 %86, 3
  %87 = zext i1 %cmp92 to i64
  %cond = select i1 %cmp92, i32 1, i32 2
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 14
  store i32 %cond, i32* %channels_out, align 4
  %89 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode93 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %89, i32 0, i32 12
  %90 = load i32, i32* %mode93, align 4
  %cmp94 = icmp ne i32 %90, 1
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %if.end90
  %91 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %force_ms = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %91, i32 0, i32 13
  store i32 0, i32* %force_ms, align 4
  br label %if.end96

if.end96:                                         ; preds = %if.then95, %if.end90
  %92 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %force_ms97 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %92, i32 0, i32 13
  %93 = load i32, i32* %force_ms97, align 4
  %94 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %force_ms98 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %94, i32 0, i32 16
  store i32 %93, i32* %force_ms98, align 4
  %95 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr99 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %95, i32 0, i32 22
  %96 = load i32, i32* %vbr99, align 4
  %cmp100 = icmp eq i32 %96, 0
  br i1 %cmp100, label %land.lhs.true, label %if.end107

land.lhs.true:                                    ; preds = %if.end96
  %97 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %97, i32 0, i32 42
  %98 = load i32, i32* %VBR_mean_bitrate_kbps, align 4
  %cmp101 = icmp ne i32 %98, 128
  br i1 %cmp101, label %land.lhs.true102, label %if.end107

land.lhs.true102:                                 ; preds = %land.lhs.true
  %99 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %99, i32 0, i32 24
  %100 = load i32, i32* %brate, align 4
  %cmp103 = icmp eq i32 %100, 0
  br i1 %cmp103, label %if.then104, label %if.end107

if.then104:                                       ; preds = %land.lhs.true102
  %101 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps105 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %101, i32 0, i32 42
  %102 = load i32, i32* %VBR_mean_bitrate_kbps105, align 4
  %103 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate106 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %103, i32 0, i32 24
  store i32 %102, i32* %brate106, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.then104, %land.lhs.true102, %land.lhs.true, %if.end96
  %104 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr108 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %104, i32 0, i32 22
  %105 = load i32, i32* %vbr108, align 4
  switch i32 %105, label %sw.default [
    i32 0, label %sw.bb
    i32 4, label %sw.bb
    i32 1, label %sw.bb
  ]

sw.bb:                                            ; preds = %if.end107, %if.end107, %if.end107
  br label %sw.epilog

sw.default:                                       ; preds = %if.end107
  %106 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %free_format = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %106, i32 0, i32 14
  store i32 0, i32* %free_format, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb
  %107 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %free_format109 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %107, i32 0, i32 14
  %108 = load i32, i32* %free_format109, align 4
  %109 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format110 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %109, i32 0, i32 34
  store i32 %108, i32* %free_format110, align 4
  %110 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr111 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %110, i32 0, i32 22
  %111 = load i32, i32* %vbr111, align 4
  %cmp112 = icmp eq i32 %111, 0
  br i1 %cmp112, label %land.lhs.true113, label %if.end134

land.lhs.true113:                                 ; preds = %sw.epilog
  %112 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate114 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %112, i32 0, i32 24
  %113 = load i32, i32* %brate114, align 4
  %cmp115 = icmp eq i32 %113, 0
  br i1 %cmp115, label %if.then116, label %if.end134

if.then116:                                       ; preds = %land.lhs.true113
  %114 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %114, i32 0, i32 25
  %115 = load float, float* %compression_ratio, align 4
  %conv = fpext float %115 to double
  %116 = call double @llvm.fabs.f64(double %conv)
  %117 = call double @llvm.fabs.f64(double 0.000000e+00)
  %cmp117 = fcmp ogt double %116, %117
  br i1 %cmp117, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then116
  %118 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio119 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %118, i32 0, i32 25
  %119 = load float, float* %compression_ratio119, align 4
  %sub = fsub float %119, 0.000000e+00
  %conv120 = fpext float %sub to double
  %120 = call double @llvm.fabs.f64(double %conv120)
  %121 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio121 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %121, i32 0, i32 25
  %122 = load float, float* %compression_ratio121, align 4
  %conv122 = fpext float %122 to double
  %123 = call double @llvm.fabs.f64(double %conv122)
  %mul = fmul double %123, 0x3EB0C6F7A0000000
  %cmp123 = fcmp ole double %120, %mul
  br i1 %cmp123, label %if.then131, label %if.end133

cond.false:                                       ; preds = %if.then116
  %124 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio125 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %124, i32 0, i32 25
  %125 = load float, float* %compression_ratio125, align 4
  %sub126 = fsub float %125, 0.000000e+00
  %conv127 = fpext float %sub126 to double
  %126 = call double @llvm.fabs.f64(double %conv127)
  %127 = call double @llvm.fabs.f64(double 0.000000e+00)
  %mul128 = fmul double %127, 0x3EB0C6F7A0000000
  %cmp129 = fcmp ole double %126, %mul128
  br i1 %cmp129, label %if.then131, label %if.end133

if.then131:                                       ; preds = %cond.false, %cond.true
  %128 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio132 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %128, i32 0, i32 25
  store float 0x40260CCCC0000000, float* %compression_ratio132, align 4
  br label %if.end133

if.end133:                                        ; preds = %if.then131, %cond.false, %cond.true
  br label %if.end134

if.end134:                                        ; preds = %if.end133, %land.lhs.true113, %sw.epilog
  %129 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr135 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %129, i32 0, i32 22
  %130 = load i32, i32* %vbr135, align 4
  %cmp136 = icmp eq i32 %130, 0
  br i1 %cmp136, label %land.lhs.true138, label %if.end175

land.lhs.true138:                                 ; preds = %if.end134
  %131 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio139 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %131, i32 0, i32 25
  %132 = load float, float* %compression_ratio139, align 4
  %cmp140 = fcmp ogt float %132, 0.000000e+00
  br i1 %cmp140, label %if.then142, label %if.end175

if.then142:                                       ; preds = %land.lhs.true138
  %133 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out143 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %133, i32 0, i32 4
  %134 = load i32, i32* %samplerate_out143, align 4
  %cmp144 = icmp eq i32 %134, 0
  br i1 %cmp144, label %if.then146, label %if.end153

if.then146:                                       ; preds = %if.then142
  %135 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in147 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %135, i32 0, i32 3
  %136 = load i32, i32* %samplerate_in147, align 4
  %conv148 = sitofp i32 %136 to double
  %mul149 = fmul double 0x3FEF0A3D70A3D70A, %conv148
  %conv150 = fptosi double %mul149 to i32
  %call151 = call i32 @map2MP3Frequency(i32 %conv150)
  %137 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out152 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %137, i32 0, i32 4
  store i32 %call151, i32* %samplerate_out152, align 4
  br label %if.end153

if.end153:                                        ; preds = %if.then146, %if.then142
  %138 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out154 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %138, i32 0, i32 4
  %139 = load i32, i32* %samplerate_out154, align 4
  %mul155 = mul nsw i32 %139, 16
  %140 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out156 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %140, i32 0, i32 14
  %141 = load i32, i32* %channels_out156, align 4
  %mul157 = mul nsw i32 %mul155, %141
  %conv158 = sitofp i32 %mul157 to double
  %142 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio159 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %142, i32 0, i32 25
  %143 = load float, float* %compression_ratio159, align 4
  %conv160 = fpext float %143 to double
  %mul161 = fmul double 1.000000e+03, %conv160
  %div = fdiv double %conv158, %mul161
  %conv162 = fptosi double %div to i32
  %144 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate163 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %144, i32 0, i32 24
  store i32 %conv162, i32* %brate163, align 4
  %145 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out164 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %145, i32 0, i32 4
  %146 = load i32, i32* %samplerate_out164, align 4
  %147 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %147, i32 0, i32 0
  %call165 = call i32 @SmpFrqIndex(i32 %146, i32* %version)
  %148 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %148, i32 0, i32 1
  store i32 %call165, i32* %samplerate_index, align 4
  %149 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format166 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %149, i32 0, i32 34
  %150 = load i32, i32* %free_format166, align 4
  %tobool167 = icmp ne i32 %150, 0
  br i1 %tobool167, label %if.end174, label %if.then168

if.then168:                                       ; preds = %if.end153
  %151 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate169 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %151, i32 0, i32 24
  %152 = load i32, i32* %brate169, align 4
  %153 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version170 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %153, i32 0, i32 0
  %154 = load i32, i32* %version170, align 4
  %155 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out171 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %155, i32 0, i32 4
  %156 = load i32, i32* %samplerate_out171, align 4
  %call172 = call i32 @FindNearestBitrate(i32 %152, i32 %154, i32 %156)
  %157 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate173 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %157, i32 0, i32 24
  store i32 %call172, i32* %brate173, align 4
  br label %if.end174

if.end174:                                        ; preds = %if.then168, %if.end153
  br label %if.end175

if.end175:                                        ; preds = %if.end174, %land.lhs.true138, %if.end134
  %158 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out176 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %158, i32 0, i32 4
  %159 = load i32, i32* %samplerate_out176, align 4
  %tobool177 = icmp ne i32 %159, 0
  br i1 %tobool177, label %if.then178, label %if.end244

if.then178:                                       ; preds = %if.end175
  %160 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out179 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %160, i32 0, i32 4
  %161 = load i32, i32* %samplerate_out179, align 4
  %cmp180 = icmp slt i32 %161, 16000
  br i1 %cmp180, label %if.then182, label %if.else200

if.then182:                                       ; preds = %if.then178
  %162 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps183 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %162, i32 0, i32 42
  %163 = load i32, i32* %VBR_mean_bitrate_kbps183, align 4
  %cmp184 = icmp sgt i32 %163, 8
  br i1 %cmp184, label %cond.true186, label %cond.false188

cond.true186:                                     ; preds = %if.then182
  %164 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps187 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %164, i32 0, i32 42
  %165 = load i32, i32* %VBR_mean_bitrate_kbps187, align 4
  br label %cond.end

cond.false188:                                    ; preds = %if.then182
  br label %cond.end

cond.end:                                         ; preds = %cond.false188, %cond.true186
  %cond189 = phi i32 [ %165, %cond.true186 ], [ 8, %cond.false188 ]
  %166 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps190 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %166, i32 0, i32 42
  store i32 %cond189, i32* %VBR_mean_bitrate_kbps190, align 4
  %167 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps191 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %167, i32 0, i32 42
  %168 = load i32, i32* %VBR_mean_bitrate_kbps191, align 4
  %cmp192 = icmp slt i32 %168, 64
  br i1 %cmp192, label %cond.true194, label %cond.false196

cond.true194:                                     ; preds = %cond.end
  %169 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps195 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %169, i32 0, i32 42
  %170 = load i32, i32* %VBR_mean_bitrate_kbps195, align 4
  br label %cond.end197

cond.false196:                                    ; preds = %cond.end
  br label %cond.end197

cond.end197:                                      ; preds = %cond.false196, %cond.true194
  %cond198 = phi i32 [ %170, %cond.true194 ], [ 64, %cond.false196 ]
  %171 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps199 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %171, i32 0, i32 42
  store i32 %cond198, i32* %VBR_mean_bitrate_kbps199, align 4
  br label %if.end243

if.else200:                                       ; preds = %if.then178
  %172 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out201 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %172, i32 0, i32 4
  %173 = load i32, i32* %samplerate_out201, align 4
  %cmp202 = icmp slt i32 %173, 32000
  br i1 %cmp202, label %if.then204, label %if.else223

if.then204:                                       ; preds = %if.else200
  %174 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps205 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %174, i32 0, i32 42
  %175 = load i32, i32* %VBR_mean_bitrate_kbps205, align 4
  %cmp206 = icmp sgt i32 %175, 8
  br i1 %cmp206, label %cond.true208, label %cond.false210

cond.true208:                                     ; preds = %if.then204
  %176 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps209 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %176, i32 0, i32 42
  %177 = load i32, i32* %VBR_mean_bitrate_kbps209, align 4
  br label %cond.end211

cond.false210:                                    ; preds = %if.then204
  br label %cond.end211

cond.end211:                                      ; preds = %cond.false210, %cond.true208
  %cond212 = phi i32 [ %177, %cond.true208 ], [ 8, %cond.false210 ]
  %178 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps213 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %178, i32 0, i32 42
  store i32 %cond212, i32* %VBR_mean_bitrate_kbps213, align 4
  %179 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps214 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %179, i32 0, i32 42
  %180 = load i32, i32* %VBR_mean_bitrate_kbps214, align 4
  %cmp215 = icmp slt i32 %180, 160
  br i1 %cmp215, label %cond.true217, label %cond.false219

cond.true217:                                     ; preds = %cond.end211
  %181 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps218 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %181, i32 0, i32 42
  %182 = load i32, i32* %VBR_mean_bitrate_kbps218, align 4
  br label %cond.end220

cond.false219:                                    ; preds = %cond.end211
  br label %cond.end220

cond.end220:                                      ; preds = %cond.false219, %cond.true217
  %cond221 = phi i32 [ %182, %cond.true217 ], [ 160, %cond.false219 ]
  %183 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps222 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %183, i32 0, i32 42
  store i32 %cond221, i32* %VBR_mean_bitrate_kbps222, align 4
  br label %if.end242

if.else223:                                       ; preds = %if.else200
  %184 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps224 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %184, i32 0, i32 42
  %185 = load i32, i32* %VBR_mean_bitrate_kbps224, align 4
  %cmp225 = icmp sgt i32 %185, 32
  br i1 %cmp225, label %cond.true227, label %cond.false229

cond.true227:                                     ; preds = %if.else223
  %186 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps228 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %186, i32 0, i32 42
  %187 = load i32, i32* %VBR_mean_bitrate_kbps228, align 4
  br label %cond.end230

cond.false229:                                    ; preds = %if.else223
  br label %cond.end230

cond.end230:                                      ; preds = %cond.false229, %cond.true227
  %cond231 = phi i32 [ %187, %cond.true227 ], [ 32, %cond.false229 ]
  %188 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps232 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %188, i32 0, i32 42
  store i32 %cond231, i32* %VBR_mean_bitrate_kbps232, align 4
  %189 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps233 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %189, i32 0, i32 42
  %190 = load i32, i32* %VBR_mean_bitrate_kbps233, align 4
  %cmp234 = icmp slt i32 %190, 320
  br i1 %cmp234, label %cond.true236, label %cond.false238

cond.true236:                                     ; preds = %cond.end230
  %191 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps237 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %191, i32 0, i32 42
  %192 = load i32, i32* %VBR_mean_bitrate_kbps237, align 4
  br label %cond.end239

cond.false238:                                    ; preds = %cond.end230
  br label %cond.end239

cond.end239:                                      ; preds = %cond.false238, %cond.true236
  %cond240 = phi i32 [ %192, %cond.true236 ], [ 320, %cond.false238 ]
  %193 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps241 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %193, i32 0, i32 42
  store i32 %cond240, i32* %VBR_mean_bitrate_kbps241, align 4
  br label %if.end242

if.end242:                                        ; preds = %cond.end239, %cond.end220
  br label %if.end243

if.end243:                                        ; preds = %if.end242, %cond.end197
  br label %if.end244

if.end244:                                        ; preds = %if.end243, %if.end175
  %194 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out245 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %194, i32 0, i32 4
  %195 = load i32, i32* %samplerate_out245, align 4
  %cmp246 = icmp eq i32 %195, 0
  br i1 %cmp246, label %land.lhs.true248, label %if.end335

land.lhs.true248:                                 ; preds = %if.end244
  %196 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr249 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %196, i32 0, i32 22
  %197 = load i32, i32* %vbr249, align 4
  %cmp250 = icmp eq i32 %197, 1
  br i1 %cmp250, label %if.then256, label %lor.lhs.false252

lor.lhs.false252:                                 ; preds = %land.lhs.true248
  %198 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr253 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %198, i32 0, i32 22
  %199 = load i32, i32* %vbr253, align 4
  %cmp254 = icmp eq i32 %199, 4
  br i1 %cmp254, label %if.then256, label %if.end335

if.then256:                                       ; preds = %lor.lhs.false252, %land.lhs.true248
  %200 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %200, i32 0, i32 41
  %201 = load i32, i32* %VBR_q, align 4
  %conv257 = sitofp i32 %201 to float
  %202 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %202, i32 0, i32 40
  %203 = load float, float* %VBR_q_frac, align 4
  %add = fadd float %conv257, %203
  store float %add, float* %qval, align 4
  %204 = bitcast [9 x %struct.q_map]* %m to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %204, i8* align 16 bitcast ([9 x %struct.q_map]* @__const.lame_init_params.m to i8*), i32 216, i1 false)
  store i32 2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then256
  %205 = load i32, i32* %i, align 4
  %cmp258 = icmp slt i32 %205, 9
  br i1 %cmp258, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %206 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in260 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %206, i32 0, i32 3
  %207 = load i32, i32* %samplerate_in260, align 4
  %208 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %208
  %sr_a = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx, i32 0, i32 0
  %209 = load i32, i32* %sr_a, align 8
  %cmp261 = icmp eq i32 %207, %209
  br i1 %cmp261, label %if.then263, label %if.end283

if.then263:                                       ; preds = %for.body
  %210 = load float, float* %qval, align 4
  %211 = load i32, i32* %i, align 4
  %arrayidx264 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %211
  %qa = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx264, i32 0, i32 1
  %212 = load float, float* %qa, align 4
  %cmp265 = fcmp olt float %210, %212
  br i1 %cmp265, label %if.then267, label %if.end282

if.then267:                                       ; preds = %if.then263
  %213 = load float, float* %qval, align 4
  %214 = load i32, i32* %i, align 4
  %arrayidx268 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %214
  %qa269 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx268, i32 0, i32 1
  %215 = load float, float* %qa269, align 4
  %div270 = fdiv float %213, %215
  %conv271 = fpext float %div270 to double
  store double %conv271, double* %d, align 8
  %216 = load double, double* %d, align 8
  %217 = load i32, i32* %i, align 4
  %arrayidx272 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %217
  %ta = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx272, i32 0, i32 3
  %218 = load float, float* %ta, align 4
  %conv273 = fpext float %218 to double
  %mul274 = fmul double %216, %conv273
  store double %mul274, double* %d, align 8
  %219 = load double, double* %d, align 8
  %conv275 = fptosi double %219 to i32
  %220 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q276 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %220, i32 0, i32 41
  store i32 %conv275, i32* %VBR_q276, align 4
  %221 = load double, double* %d, align 8
  %222 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q277 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %222, i32 0, i32 41
  %223 = load i32, i32* %VBR_q277, align 4
  %conv278 = sitofp i32 %223 to double
  %sub279 = fsub double %221, %conv278
  %conv280 = fptrunc double %sub279 to float
  %224 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac281 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %224, i32 0, i32 40
  store float %conv280, float* %VBR_q_frac281, align 4
  br label %if.end282

if.end282:                                        ; preds = %if.then267, %if.then263
  br label %if.end283

if.end283:                                        ; preds = %if.end282, %for.body
  %225 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in284 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %225, i32 0, i32 3
  %226 = load i32, i32* %samplerate_in284, align 4
  %227 = load i32, i32* %i, align 4
  %arrayidx285 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %227
  %sr_a286 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx285, i32 0, i32 0
  %228 = load i32, i32* %sr_a286, align 8
  %cmp287 = icmp sge i32 %226, %228
  br i1 %cmp287, label %if.then289, label %if.end334

if.then289:                                       ; preds = %if.end283
  %229 = load i32, i32* %i, align 4
  %arrayidx290 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %229
  %qa291 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx290, i32 0, i32 1
  %230 = load float, float* %qa291, align 4
  %231 = load float, float* %qval, align 4
  %cmp292 = fcmp ole float %230, %231
  br i1 %cmp292, label %land.lhs.true294, label %if.end333

land.lhs.true294:                                 ; preds = %if.then289
  %232 = load float, float* %qval, align 4
  %233 = load i32, i32* %i, align 4
  %arrayidx295 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %233
  %qb = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx295, i32 0, i32 2
  %234 = load float, float* %qb, align 8
  %cmp296 = fcmp olt float %232, %234
  br i1 %cmp296, label %if.then298, label %if.end333

if.then298:                                       ; preds = %land.lhs.true294
  %235 = load i32, i32* %i, align 4
  %arrayidx299 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %235
  %qb300 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx299, i32 0, i32 2
  %236 = load float, float* %qb300, align 8
  %237 = load i32, i32* %i, align 4
  %arrayidx301 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %237
  %qa302 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx301, i32 0, i32 1
  %238 = load float, float* %qa302, align 4
  %sub303 = fsub float %236, %238
  store float %sub303, float* %q_, align 4
  %239 = load i32, i32* %i, align 4
  %arrayidx304 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %239
  %tb = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx304, i32 0, i32 4
  %240 = load float, float* %tb, align 8
  %241 = load i32, i32* %i, align 4
  %arrayidx305 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %241
  %ta306 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx305, i32 0, i32 3
  %242 = load float, float* %ta306, align 4
  %sub307 = fsub float %240, %242
  store float %sub307, float* %t_, align 4
  %243 = load i32, i32* %i, align 4
  %arrayidx309 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %243
  %ta310 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx309, i32 0, i32 3
  %244 = load float, float* %ta310, align 4
  %245 = load float, float* %t_, align 4
  %246 = load float, float* %qval, align 4
  %247 = load i32, i32* %i, align 4
  %arrayidx311 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %247
  %qa312 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx311, i32 0, i32 1
  %248 = load float, float* %qa312, align 4
  %sub313 = fsub float %246, %248
  %mul314 = fmul float %245, %sub313
  %249 = load float, float* %q_, align 4
  %div315 = fdiv float %mul314, %249
  %add316 = fadd float %244, %div315
  %conv317 = fpext float %add316 to double
  store double %conv317, double* %d308, align 8
  %250 = load double, double* %d308, align 8
  %conv318 = fptosi double %250 to i32
  %251 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q319 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %251, i32 0, i32 41
  store i32 %conv318, i32* %VBR_q319, align 4
  %252 = load double, double* %d308, align 8
  %253 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q320 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %253, i32 0, i32 41
  %254 = load i32, i32* %VBR_q320, align 4
  %conv321 = sitofp i32 %254 to double
  %sub322 = fsub double %252, %conv321
  %conv323 = fptrunc double %sub322 to float
  %255 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac324 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %255, i32 0, i32 40
  store float %conv323, float* %VBR_q_frac324, align 4
  %256 = load i32, i32* %i, align 4
  %arrayidx325 = getelementptr inbounds [9 x %struct.q_map], [9 x %struct.q_map]* %m, i32 0, i32 %256
  %sr_a326 = getelementptr inbounds %struct.q_map, %struct.q_map* %arrayidx325, i32 0, i32 0
  %257 = load i32, i32* %sr_a326, align 8
  %258 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out327 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %258, i32 0, i32 4
  store i32 %257, i32* %samplerate_out327, align 4
  %259 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %259, i32 0, i32 46
  %260 = load i32, i32* %lowpassfreq, align 4
  %cmp328 = icmp eq i32 %260, 0
  br i1 %cmp328, label %if.then330, label %if.end332

if.then330:                                       ; preds = %if.then298
  %261 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq331 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %261, i32 0, i32 46
  store i32 -1, i32* %lowpassfreq331, align 4
  br label %if.end332

if.end332:                                        ; preds = %if.then330, %if.then298
  br label %for.end

if.end333:                                        ; preds = %land.lhs.true294, %if.then289
  br label %if.end334

if.end334:                                        ; preds = %if.end333, %if.end283
  br label %for.inc

for.inc:                                          ; preds = %if.end334
  %262 = load i32, i32* %i, align 4
  %inc = add nsw i32 %262, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.end332, %for.cond
  br label %if.end335

if.end335:                                        ; preds = %for.end, %lor.lhs.false252, %if.end244
  %263 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq336 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %263, i32 0, i32 46
  %264 = load i32, i32* %lowpassfreq336, align 4
  %cmp337 = icmp eq i32 %264, 0
  br i1 %cmp337, label %if.then339, label %if.end434

if.then339:                                       ; preds = %if.end335
  store double 1.600000e+04, double* %lowpass, align 8
  %265 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr340 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %265, i32 0, i32 22
  %266 = load i32, i32* %vbr340, align 4
  switch i32 %266, label %sw.default392 [
    i32 0, label %sw.bb341
    i32 3, label %sw.bb343
    i32 2, label %sw.bb345
    i32 4, label %sw.bb367
    i32 1, label %sw.bb367
  ]

sw.bb341:                                         ; preds = %if.then339
  %267 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate342 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %267, i32 0, i32 24
  %268 = load i32, i32* %brate342, align 4
  call void @optimum_bandwidth(double* %lowpass, double* %highpass, i32 %268)
  br label %sw.epilog417

sw.bb343:                                         ; preds = %if.then339
  %269 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps344 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %269, i32 0, i32 42
  %270 = load i32, i32* %VBR_mean_bitrate_kbps344, align 4
  call void @optimum_bandwidth(double* %lowpass, double* %highpass, i32 %270)
  br label %sw.epilog417

sw.bb345:                                         ; preds = %if.then339
  %271 = bitcast [11 x i32]* %x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %271, i8* align 16 bitcast ([11 x i32]* @__const.lame_init_params.x to i8*), i32 44, i1 false)
  %272 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q346 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %272, i32 0, i32 41
  %273 = load i32, i32* %VBR_q346, align 4
  %cmp347 = icmp sle i32 0, %273
  br i1 %cmp347, label %land.lhs.true349, label %if.else365

land.lhs.true349:                                 ; preds = %sw.bb345
  %274 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q350 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %274, i32 0, i32 41
  %275 = load i32, i32* %VBR_q350, align 4
  %cmp351 = icmp sle i32 %275, 9
  br i1 %cmp351, label %if.then353, label %if.else365

if.then353:                                       ; preds = %land.lhs.true349
  %276 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q354 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %276, i32 0, i32 41
  %277 = load i32, i32* %VBR_q354, align 4
  %arrayidx355 = getelementptr inbounds [11 x i32], [11 x i32]* %x, i32 0, i32 %277
  %278 = load i32, i32* %arrayidx355, align 4
  %conv356 = sitofp i32 %278 to double
  store double %conv356, double* %a, align 8
  %279 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q357 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %279, i32 0, i32 41
  %280 = load i32, i32* %VBR_q357, align 4
  %add358 = add nsw i32 %280, 1
  %arrayidx359 = getelementptr inbounds [11 x i32], [11 x i32]* %x, i32 0, i32 %add358
  %281 = load i32, i32* %arrayidx359, align 4
  %conv360 = sitofp i32 %281 to double
  store double %conv360, double* %b, align 8
  %282 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac362 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %282, i32 0, i32 40
  %283 = load float, float* %VBR_q_frac362, align 4
  %conv363 = fpext float %283 to double
  store double %conv363, double* %m361, align 8
  %284 = load double, double* %a, align 8
  %285 = load double, double* %b, align 8
  %286 = load double, double* %m361, align 8
  %call364 = call double @linear_int(double %284, double %285, double %286)
  store double %call364, double* %lowpass, align 8
  br label %if.end366

if.else365:                                       ; preds = %land.lhs.true349, %sw.bb345
  store double 1.950000e+04, double* %lowpass, align 8
  br label %if.end366

if.end366:                                        ; preds = %if.else365, %if.then353
  br label %sw.epilog417

sw.bb367:                                         ; preds = %if.then339, %if.then339
  %287 = bitcast [11 x i32]* %x368 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %287, i8* align 16 bitcast ([11 x i32]* @__const.lame_init_params.x.1 to i8*), i32 44, i1 false)
  %288 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q369 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %288, i32 0, i32 41
  %289 = load i32, i32* %VBR_q369, align 4
  %cmp370 = icmp sle i32 0, %289
  br i1 %cmp370, label %land.lhs.true372, label %if.else390

land.lhs.true372:                                 ; preds = %sw.bb367
  %290 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q373 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %290, i32 0, i32 41
  %291 = load i32, i32* %VBR_q373, align 4
  %cmp374 = icmp sle i32 %291, 9
  br i1 %cmp374, label %if.then376, label %if.else390

if.then376:                                       ; preds = %land.lhs.true372
  %292 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q378 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %292, i32 0, i32 41
  %293 = load i32, i32* %VBR_q378, align 4
  %arrayidx379 = getelementptr inbounds [11 x i32], [11 x i32]* %x368, i32 0, i32 %293
  %294 = load i32, i32* %arrayidx379, align 4
  %conv380 = sitofp i32 %294 to double
  store double %conv380, double* %a377, align 8
  %295 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q382 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %295, i32 0, i32 41
  %296 = load i32, i32* %VBR_q382, align 4
  %add383 = add nsw i32 %296, 1
  %arrayidx384 = getelementptr inbounds [11 x i32], [11 x i32]* %x368, i32 0, i32 %add383
  %297 = load i32, i32* %arrayidx384, align 4
  %conv385 = sitofp i32 %297 to double
  store double %conv385, double* %b381, align 8
  %298 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac387 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %298, i32 0, i32 40
  %299 = load float, float* %VBR_q_frac387, align 4
  %conv388 = fpext float %299 to double
  store double %conv388, double* %m386, align 8
  %300 = load double, double* %a377, align 8
  %301 = load double, double* %b381, align 8
  %302 = load double, double* %m386, align 8
  %call389 = call double @linear_int(double %300, double %301, double %302)
  store double %call389, double* %lowpass, align 8
  br label %if.end391

if.else390:                                       ; preds = %land.lhs.true372, %sw.bb367
  store double 2.150000e+04, double* %lowpass, align 8
  br label %if.end391

if.end391:                                        ; preds = %if.else390, %if.then376
  br label %sw.epilog417

sw.default392:                                    ; preds = %if.then339
  %303 = bitcast [11 x i32]* %x393 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %303, i8* align 16 bitcast ([11 x i32]* @__const.lame_init_params.x.2 to i8*), i32 44, i1 false)
  %304 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q394 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %304, i32 0, i32 41
  %305 = load i32, i32* %VBR_q394, align 4
  %cmp395 = icmp sle i32 0, %305
  br i1 %cmp395, label %land.lhs.true397, label %if.else415

land.lhs.true397:                                 ; preds = %sw.default392
  %306 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q398 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %306, i32 0, i32 41
  %307 = load i32, i32* %VBR_q398, align 4
  %cmp399 = icmp sle i32 %307, 9
  br i1 %cmp399, label %if.then401, label %if.else415

if.then401:                                       ; preds = %land.lhs.true397
  %308 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q403 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %308, i32 0, i32 41
  %309 = load i32, i32* %VBR_q403, align 4
  %arrayidx404 = getelementptr inbounds [11 x i32], [11 x i32]* %x393, i32 0, i32 %309
  %310 = load i32, i32* %arrayidx404, align 4
  %conv405 = sitofp i32 %310 to double
  store double %conv405, double* %a402, align 8
  %311 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q407 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %311, i32 0, i32 41
  %312 = load i32, i32* %VBR_q407, align 4
  %add408 = add nsw i32 %312, 1
  %arrayidx409 = getelementptr inbounds [11 x i32], [11 x i32]* %x393, i32 0, i32 %add408
  %313 = load i32, i32* %arrayidx409, align 4
  %conv410 = sitofp i32 %313 to double
  store double %conv410, double* %b406, align 8
  %314 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q_frac412 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %314, i32 0, i32 40
  %315 = load float, float* %VBR_q_frac412, align 4
  %conv413 = fpext float %315 to double
  store double %conv413, double* %m411, align 8
  %316 = load double, double* %a402, align 8
  %317 = load double, double* %b406, align 8
  %318 = load double, double* %m411, align 8
  %call414 = call double @linear_int(double %316, double %317, double %318)
  store double %call414, double* %lowpass, align 8
  br label %if.end416

if.else415:                                       ; preds = %land.lhs.true397, %sw.default392
  store double 1.950000e+04, double* %lowpass, align 8
  br label %if.end416

if.end416:                                        ; preds = %if.else415, %if.then401
  br label %sw.epilog417

sw.epilog417:                                     ; preds = %if.end416, %if.end391, %if.end366, %sw.bb343, %sw.bb341
  %319 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode418 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %319, i32 0, i32 12
  %320 = load i32, i32* %mode418, align 4
  %cmp419 = icmp eq i32 %320, 3
  br i1 %cmp419, label %land.lhs.true421, label %if.end431

land.lhs.true421:                                 ; preds = %sw.epilog417
  %321 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr422 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %321, i32 0, i32 22
  %322 = load i32, i32* %vbr422, align 4
  %cmp423 = icmp eq i32 %322, 0
  br i1 %cmp423, label %if.then429, label %lor.lhs.false425

lor.lhs.false425:                                 ; preds = %land.lhs.true421
  %323 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr426 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %323, i32 0, i32 22
  %324 = load i32, i32* %vbr426, align 4
  %cmp427 = icmp eq i32 %324, 3
  br i1 %cmp427, label %if.then429, label %if.end431

if.then429:                                       ; preds = %lor.lhs.false425, %land.lhs.true421
  %325 = load double, double* %lowpass, align 8
  %mul430 = fmul double %325, 1.500000e+00
  store double %mul430, double* %lowpass, align 8
  br label %if.end431

if.end431:                                        ; preds = %if.then429, %lor.lhs.false425, %sw.epilog417
  %326 = load double, double* %lowpass, align 8
  %conv432 = fptosi double %326 to i32
  %327 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq433 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %327, i32 0, i32 46
  store i32 %conv432, i32* %lowpassfreq433, align 4
  br label %if.end434

if.end434:                                        ; preds = %if.end431, %if.end335
  %328 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out435 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %328, i32 0, i32 4
  %329 = load i32, i32* %samplerate_out435, align 4
  %cmp436 = icmp eq i32 %329, 0
  br i1 %cmp436, label %if.then438, label %if.end453

if.then438:                                       ; preds = %if.end434
  %330 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq439 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %330, i32 0, i32 46
  %331 = load i32, i32* %lowpassfreq439, align 4
  %mul440 = mul nsw i32 2, %331
  %332 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in441 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %332, i32 0, i32 3
  %333 = load i32, i32* %samplerate_in441, align 4
  %cmp442 = icmp sgt i32 %mul440, %333
  br i1 %cmp442, label %if.then444, label %if.end448

if.then444:                                       ; preds = %if.then438
  %334 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in445 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %334, i32 0, i32 3
  %335 = load i32, i32* %samplerate_in445, align 4
  %div446 = sdiv i32 %335, 2
  %336 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq447 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %336, i32 0, i32 46
  store i32 %div446, i32* %lowpassfreq447, align 4
  br label %if.end448

if.end448:                                        ; preds = %if.then444, %if.then438
  %337 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq449 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %337, i32 0, i32 46
  %338 = load i32, i32* %lowpassfreq449, align 4
  %339 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in450 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %339, i32 0, i32 3
  %340 = load i32, i32* %samplerate_in450, align 4
  %call451 = call i32 @optimum_samplefreq(i32 %338, i32 %340)
  %341 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out452 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %341, i32 0, i32 4
  store i32 %call451, i32* %samplerate_out452, align 4
  br label %if.end453

if.end453:                                        ; preds = %if.end448, %if.end434
  %342 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr454 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %342, i32 0, i32 22
  %343 = load i32, i32* %vbr454, align 4
  %cmp455 = icmp eq i32 %343, 1
  br i1 %cmp455, label %if.then461, label %lor.lhs.false457

lor.lhs.false457:                                 ; preds = %if.end453
  %344 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr458 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %344, i32 0, i32 22
  %345 = load i32, i32* %vbr458, align 4
  %cmp459 = icmp eq i32 %345, 4
  br i1 %cmp459, label %if.then461, label %if.else471

if.then461:                                       ; preds = %lor.lhs.false457, %if.end453
  %346 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq462 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %346, i32 0, i32 46
  %347 = load i32, i32* %lowpassfreq462, align 4
  %cmp463 = icmp slt i32 24000, %347
  br i1 %cmp463, label %cond.true465, label %cond.false466

cond.true465:                                     ; preds = %if.then461
  br label %cond.end468

cond.false466:                                    ; preds = %if.then461
  %348 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq467 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %348, i32 0, i32 46
  %349 = load i32, i32* %lowpassfreq467, align 4
  br label %cond.end468

cond.end468:                                      ; preds = %cond.false466, %cond.true465
  %cond469 = phi i32 [ 24000, %cond.true465 ], [ %349, %cond.false466 ]
  %350 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq470 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %350, i32 0, i32 46
  store i32 %cond469, i32* %lowpassfreq470, align 4
  br label %if.end481

if.else471:                                       ; preds = %lor.lhs.false457
  %351 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq472 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %351, i32 0, i32 46
  %352 = load i32, i32* %lowpassfreq472, align 4
  %cmp473 = icmp slt i32 20500, %352
  br i1 %cmp473, label %cond.true475, label %cond.false476

cond.true475:                                     ; preds = %if.else471
  br label %cond.end478

cond.false476:                                    ; preds = %if.else471
  %353 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq477 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %353, i32 0, i32 46
  %354 = load i32, i32* %lowpassfreq477, align 4
  br label %cond.end478

cond.end478:                                      ; preds = %cond.false476, %cond.true475
  %cond479 = phi i32 [ 20500, %cond.true475 ], [ %354, %cond.false476 ]
  %355 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq480 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %355, i32 0, i32 46
  store i32 %cond479, i32* %lowpassfreq480, align 4
  br label %if.end481

if.end481:                                        ; preds = %cond.end478, %cond.end468
  %356 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out482 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %356, i32 0, i32 4
  %357 = load i32, i32* %samplerate_out482, align 4
  %div483 = sdiv i32 %357, 2
  %358 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq484 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %358, i32 0, i32 46
  %359 = load i32, i32* %lowpassfreq484, align 4
  %cmp485 = icmp slt i32 %div483, %359
  br i1 %cmp485, label %cond.true487, label %cond.false490

cond.true487:                                     ; preds = %if.end481
  %360 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out488 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %360, i32 0, i32 4
  %361 = load i32, i32* %samplerate_out488, align 4
  %div489 = sdiv i32 %361, 2
  br label %cond.end492

cond.false490:                                    ; preds = %if.end481
  %362 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq491 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %362, i32 0, i32 46
  %363 = load i32, i32* %lowpassfreq491, align 4
  br label %cond.end492

cond.end492:                                      ; preds = %cond.false490, %cond.true487
  %cond493 = phi i32 [ %div489, %cond.true487 ], [ %363, %cond.false490 ]
  %364 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq494 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %364, i32 0, i32 46
  store i32 %cond493, i32* %lowpassfreq494, align 4
  %365 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr495 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %365, i32 0, i32 22
  %366 = load i32, i32* %vbr495, align 4
  %cmp496 = icmp eq i32 %366, 0
  br i1 %cmp496, label %if.then498, label %if.end510

if.then498:                                       ; preds = %cond.end492
  %367 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out499 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %367, i32 0, i32 4
  %368 = load i32, i32* %samplerate_out499, align 4
  %mul500 = mul nsw i32 %368, 16
  %369 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out501 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %369, i32 0, i32 14
  %370 = load i32, i32* %channels_out501, align 4
  %mul502 = mul nsw i32 %mul500, %370
  %conv503 = sitofp i32 %mul502 to double
  %371 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate504 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %371, i32 0, i32 24
  %372 = load i32, i32* %brate504, align 4
  %conv505 = sitofp i32 %372 to double
  %mul506 = fmul double 1.000000e+03, %conv505
  %div507 = fdiv double %conv503, %mul506
  %conv508 = fptrunc double %div507 to float
  %373 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio509 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %373, i32 0, i32 25
  store float %conv508, float* %compression_ratio509, align 4
  br label %if.end510

if.end510:                                        ; preds = %if.then498, %cond.end492
  %374 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr511 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %374, i32 0, i32 22
  %375 = load i32, i32* %vbr511, align 4
  %cmp512 = icmp eq i32 %375, 3
  br i1 %cmp512, label %if.then514, label %if.end526

if.then514:                                       ; preds = %if.end510
  %376 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out515 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %376, i32 0, i32 4
  %377 = load i32, i32* %samplerate_out515, align 4
  %mul516 = mul nsw i32 %377, 16
  %378 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out517 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %378, i32 0, i32 14
  %379 = load i32, i32* %channels_out517, align 4
  %mul518 = mul nsw i32 %mul516, %379
  %conv519 = sitofp i32 %mul518 to double
  %380 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps520 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %380, i32 0, i32 42
  %381 = load i32, i32* %VBR_mean_bitrate_kbps520, align 4
  %conv521 = sitofp i32 %381 to double
  %mul522 = fmul double 1.000000e+03, %conv521
  %div523 = fdiv double %conv519, %mul522
  %conv524 = fptrunc double %div523 to float
  %382 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio525 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %382, i32 0, i32 25
  store float %conv524, float* %compression_ratio525, align 4
  br label %if.end526

if.end526:                                        ; preds = %if.then514, %if.end510
  %383 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %disable_reservoir = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %383, i32 0, i32 32
  %384 = load i32, i32* %disable_reservoir, align 4
  %385 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %disable_reservoir527 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %385, i32 0, i32 32
  store i32 %384, i32* %disable_reservoir527, align 4
  %386 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq528 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %386, i32 0, i32 46
  %387 = load i32, i32* %lowpassfreq528, align 4
  %388 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq529 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %388, i32 0, i32 9
  store i32 %387, i32* %lowpassfreq529, align 4
  %389 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %389, i32 0, i32 47
  %390 = load i32, i32* %highpassfreq, align 4
  %391 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq530 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %391, i32 0, i32 10
  store i32 %390, i32* %highpassfreq530, align 4
  %392 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in531 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %392, i32 0, i32 3
  %393 = load i32, i32* %samplerate_in531, align 4
  %394 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in532 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %394, i32 0, i32 11
  store i32 %393, i32* %samplerate_in532, align 4
  %395 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_out533 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %395, i32 0, i32 4
  %396 = load i32, i32* %samplerate_out533, align 4
  %397 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out534 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %397, i32 0, i32 12
  store i32 %396, i32* %samplerate_out534, align 4
  %398 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out535 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %398, i32 0, i32 12
  %399 = load i32, i32* %samplerate_out535, align 4
  %cmp536 = icmp sle i32 %399, 24000
  %400 = zext i1 %cmp536 to i64
  %cond538 = select i1 %cmp536, i32 1, i32 2
  %401 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %401, i32 0, i32 15
  store i32 %cond538, i32* %mode_gr, align 4
  %402 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr539 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %402, i32 0, i32 22
  %403 = load i32, i32* %vbr539, align 4
  switch i32 %403, label %sw.default557 [
    i32 1, label %sw.bb540
    i32 2, label %sw.bb540
    i32 4, label %sw.bb540
    i32 3, label %sw.bb545
  ]

sw.bb540:                                         ; preds = %if.end526, %if.end526, %if.end526
  %404 = bitcast [10 x float]* %cmp541 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %404, i8* align 16 bitcast ([10 x float]* @__const.lame_init_params.cmp to i8*), i32 40, i1 false)
  %405 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q542 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %405, i32 0, i32 41
  %406 = load i32, i32* %VBR_q542, align 4
  %arrayidx543 = getelementptr inbounds [10 x float], [10 x float]* %cmp541, i32 0, i32 %406
  %407 = load float, float* %arrayidx543, align 4
  %408 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio544 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %408, i32 0, i32 25
  store float %407, float* %compression_ratio544, align 4
  br label %sw.epilog569

sw.bb545:                                         ; preds = %if.end526
  %409 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out546 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %409, i32 0, i32 12
  %410 = load i32, i32* %samplerate_out546, align 4
  %mul547 = mul nsw i32 %410, 16
  %411 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out548 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %411, i32 0, i32 14
  %412 = load i32, i32* %channels_out548, align 4
  %mul549 = mul nsw i32 %mul547, %412
  %conv550 = sitofp i32 %mul549 to double
  %413 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps551 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %413, i32 0, i32 42
  %414 = load i32, i32* %VBR_mean_bitrate_kbps551, align 4
  %conv552 = sitofp i32 %414 to double
  %mul553 = fmul double 1.000000e+03, %conv552
  %div554 = fdiv double %conv550, %mul553
  %conv555 = fptrunc double %div554 to float
  %415 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio556 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %415, i32 0, i32 25
  store float %conv555, float* %compression_ratio556, align 4
  br label %sw.epilog569

sw.default557:                                    ; preds = %if.end526
  %416 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out558 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %416, i32 0, i32 12
  %417 = load i32, i32* %samplerate_out558, align 4
  %mul559 = mul nsw i32 %417, 16
  %418 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out560 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %418, i32 0, i32 14
  %419 = load i32, i32* %channels_out560, align 4
  %mul561 = mul nsw i32 %mul559, %419
  %conv562 = sitofp i32 %mul561 to double
  %420 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate563 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %420, i32 0, i32 24
  %421 = load i32, i32* %brate563, align 4
  %conv564 = sitofp i32 %421 to double
  %mul565 = fmul double 1.000000e+03, %conv564
  %div566 = fdiv double %conv562, %mul565
  %conv567 = fptrunc double %div566 to float
  %422 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio568 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %422, i32 0, i32 25
  store float %conv567, float* %compression_ratio568, align 4
  br label %sw.epilog569

sw.epilog569:                                     ; preds = %sw.default557, %sw.bb545, %sw.bb540
  %423 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode570 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %423, i32 0, i32 12
  %424 = load i32, i32* %mode570, align 4
  %cmp571 = icmp eq i32 %424, 4
  br i1 %cmp571, label %if.then573, label %if.end575

if.then573:                                       ; preds = %sw.epilog569
  %425 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode574 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %425, i32 0, i32 12
  store i32 1, i32* %mode574, align 4
  br label %if.end575

if.end575:                                        ; preds = %if.then573, %sw.epilog569
  %426 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode576 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %426, i32 0, i32 12
  %427 = load i32, i32* %mode576, align 4
  %428 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode577 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %428, i32 0, i32 41
  store i32 %427, i32* %mode577, align 4
  %429 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq578 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %429, i32 0, i32 10
  %430 = load i32, i32* %highpassfreq578, align 4
  %cmp579 = icmp sgt i32 %430, 0
  br i1 %cmp579, label %if.then581, label %if.else610

if.then581:                                       ; preds = %if.end575
  %431 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq582 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %431, i32 0, i32 10
  %432 = load i32, i32* %highpassfreq582, align 4
  %conv583 = sitofp i32 %432 to double
  %mul584 = fmul double 2.000000e+00, %conv583
  %conv585 = fptrunc double %mul584 to float
  %433 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %433, i32 0, i32 60
  store float %conv585, float* %highpass1, align 4
  %434 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %434, i32 0, i32 49
  %435 = load i32, i32* %highpasswidth, align 4
  %cmp586 = icmp sge i32 %435, 0
  br i1 %cmp586, label %if.then588, label %if.else595

if.then588:                                       ; preds = %if.then581
  %436 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq589 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %436, i32 0, i32 10
  %437 = load i32, i32* %highpassfreq589, align 4
  %438 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpasswidth590 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %438, i32 0, i32 49
  %439 = load i32, i32* %highpasswidth590, align 4
  %add591 = add nsw i32 %437, %439
  %conv592 = sitofp i32 %add591 to double
  %mul593 = fmul double 2.000000e+00, %conv592
  %conv594 = fptrunc double %mul593 to float
  %440 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %440, i32 0, i32 61
  store float %conv594, float* %highpass2, align 4
  br label %if.end601

if.else595:                                       ; preds = %if.then581
  %441 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq596 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %441, i32 0, i32 10
  %442 = load i32, i32* %highpassfreq596, align 4
  %conv597 = sitofp i32 %442 to double
  %mul598 = fmul double 2.000000e+00, %conv597
  %conv599 = fptrunc double %mul598 to float
  %443 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2600 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %443, i32 0, i32 61
  store float %conv599, float* %highpass2600, align 4
  br label %if.end601

if.end601:                                        ; preds = %if.else595, %if.then588
  %444 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out602 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %444, i32 0, i32 12
  %445 = load i32, i32* %samplerate_out602, align 4
  %conv603 = sitofp i32 %445 to float
  %446 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1604 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %446, i32 0, i32 60
  %447 = load float, float* %highpass1604, align 4
  %div605 = fdiv float %447, %conv603
  store float %div605, float* %highpass1604, align 4
  %448 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out606 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %448, i32 0, i32 12
  %449 = load i32, i32* %samplerate_out606, align 4
  %conv607 = sitofp i32 %449 to float
  %450 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2608 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %450, i32 0, i32 61
  %451 = load float, float* %highpass2608, align 4
  %div609 = fdiv float %451, %conv607
  store float %div609, float* %highpass2608, align 4
  br label %if.end613

if.else610:                                       ; preds = %if.end575
  %452 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1611 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %452, i32 0, i32 60
  store float 0.000000e+00, float* %highpass1611, align 4
  %453 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2612 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %453, i32 0, i32 61
  store float 0.000000e+00, float* %highpass2612, align 4
  br label %if.end613

if.end613:                                        ; preds = %if.else610, %if.end601
  %454 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %454, i32 0, i32 58
  store float 0.000000e+00, float* %lowpass1, align 4
  %455 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %455, i32 0, i32 59
  store float 0.000000e+00, float* %lowpass2, align 4
  %456 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq614 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %456, i32 0, i32 9
  %457 = load i32, i32* %lowpassfreq614, align 4
  %cmp615 = icmp sgt i32 %457, 0
  br i1 %cmp615, label %land.lhs.true617, label %if.end660

land.lhs.true617:                                 ; preds = %if.end613
  %458 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq618 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %458, i32 0, i32 9
  %459 = load i32, i32* %lowpassfreq618, align 4
  %460 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out619 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %460, i32 0, i32 12
  %461 = load i32, i32* %samplerate_out619, align 4
  %div620 = sdiv i32 %461, 2
  %cmp621 = icmp slt i32 %459, %div620
  br i1 %cmp621, label %if.then623, label %if.end660

if.then623:                                       ; preds = %land.lhs.true617
  %462 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq624 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %462, i32 0, i32 9
  %463 = load i32, i32* %lowpassfreq624, align 4
  %conv625 = sitofp i32 %463 to double
  %mul626 = fmul double 2.000000e+00, %conv625
  %conv627 = fptrunc double %mul626 to float
  %464 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2628 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %464, i32 0, i32 59
  store float %conv627, float* %lowpass2628, align 4
  %465 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %465, i32 0, i32 48
  %466 = load i32, i32* %lowpasswidth, align 4
  %cmp629 = icmp sge i32 %466, 0
  br i1 %cmp629, label %if.then631, label %if.else645

if.then631:                                       ; preds = %if.then623
  %467 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq632 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %467, i32 0, i32 9
  %468 = load i32, i32* %lowpassfreq632, align 4
  %469 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpasswidth633 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %469, i32 0, i32 48
  %470 = load i32, i32* %lowpasswidth633, align 4
  %sub634 = sub nsw i32 %468, %470
  %conv635 = sitofp i32 %sub634 to double
  %mul636 = fmul double 2.000000e+00, %conv635
  %conv637 = fptrunc double %mul636 to float
  %471 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1638 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %471, i32 0, i32 58
  store float %conv637, float* %lowpass1638, align 4
  %472 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1639 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %472, i32 0, i32 58
  %473 = load float, float* %lowpass1639, align 4
  %cmp640 = fcmp olt float %473, 0.000000e+00
  br i1 %cmp640, label %if.then642, label %if.end644

if.then642:                                       ; preds = %if.then631
  %474 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1643 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %474, i32 0, i32 58
  store float 0.000000e+00, float* %lowpass1643, align 4
  br label %if.end644

if.end644:                                        ; preds = %if.then642, %if.then631
  br label %if.end651

if.else645:                                       ; preds = %if.then623
  %475 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq646 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %475, i32 0, i32 9
  %476 = load i32, i32* %lowpassfreq646, align 4
  %conv647 = sitofp i32 %476 to double
  %mul648 = fmul double 2.000000e+00, %conv647
  %conv649 = fptrunc double %mul648 to float
  %477 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1650 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %477, i32 0, i32 58
  store float %conv649, float* %lowpass1650, align 4
  br label %if.end651

if.end651:                                        ; preds = %if.else645, %if.end644
  %478 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out652 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %478, i32 0, i32 12
  %479 = load i32, i32* %samplerate_out652, align 4
  %conv653 = sitofp i32 %479 to float
  %480 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1654 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %480, i32 0, i32 58
  %481 = load float, float* %lowpass1654, align 4
  %div655 = fdiv float %481, %conv653
  store float %div655, float* %lowpass1654, align 4
  %482 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out656 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %482, i32 0, i32 12
  %483 = load i32, i32* %samplerate_out656, align 4
  %conv657 = sitofp i32 %483 to float
  %484 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2658 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %484, i32 0, i32 59
  %485 = load float, float* %lowpass2658, align 4
  %div659 = fdiv float %485, %conv657
  store float %div659, float* %lowpass2658, align 4
  br label %if.end660

if.end660:                                        ; preds = %if.end651, %land.lhs.true617, %if.end613
  %486 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @lame_init_params_ppflt(%struct.lame_internal_flags* %486)
  %487 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out661 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %487, i32 0, i32 12
  %488 = load i32, i32* %samplerate_out661, align 4
  %489 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version662 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %489, i32 0, i32 0
  %call663 = call i32 @SmpFrqIndex(i32 %488, i32* %version662)
  %490 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_index664 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %490, i32 0, i32 1
  store i32 %call663, i32* %samplerate_index664, align 4
  %491 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr665 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %491, i32 0, i32 22
  %492 = load i32, i32* %vbr665, align 4
  %cmp666 = icmp eq i32 %492, 0
  br i1 %cmp666, label %if.then668, label %if.else693

if.then668:                                       ; preds = %if.end660
  %493 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format669 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %493, i32 0, i32 34
  %494 = load i32, i32* %free_format669, align 4
  %tobool670 = icmp ne i32 %494, 0
  br i1 %tobool670, label %if.then671, label %if.else672

if.then671:                                       ; preds = %if.then668
  %495 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %495, i32 0, i32 12
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 2
  store i32 0, i32* %bitrate_index, align 8
  br label %if.end692

if.else672:                                       ; preds = %if.then668
  %496 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate673 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %496, i32 0, i32 24
  %497 = load i32, i32* %brate673, align 4
  %498 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version674 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %498, i32 0, i32 0
  %499 = load i32, i32* %version674, align 4
  %500 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out675 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %500, i32 0, i32 12
  %501 = load i32, i32* %samplerate_out675, align 4
  %call676 = call i32 @FindNearestBitrate(i32 %497, i32 %499, i32 %501)
  %502 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate677 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %502, i32 0, i32 24
  store i32 %call676, i32* %brate677, align 4
  %503 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate678 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %503, i32 0, i32 24
  %504 = load i32, i32* %brate678, align 4
  %505 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version679 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %505, i32 0, i32 0
  %506 = load i32, i32* %version679, align 4
  %507 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out680 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %507, i32 0, i32 12
  %508 = load i32, i32* %samplerate_out680, align 4
  %call681 = call i32 @BitrateIndex(i32 %504, i32 %506, i32 %508)
  %509 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc682 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %509, i32 0, i32 12
  %bitrate_index683 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc682, i32 0, i32 2
  store i32 %call681, i32* %bitrate_index683, align 8
  %510 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc684 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %510, i32 0, i32 12
  %bitrate_index685 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc684, i32 0, i32 2
  %511 = load i32, i32* %bitrate_index685, align 8
  %cmp686 = icmp sle i32 %511, 0
  br i1 %cmp686, label %if.then688, label %if.end691

if.then688:                                       ; preds = %if.else672
  %512 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc689 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %512, i32 0, i32 12
  %bitrate_index690 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc689, i32 0, i32 2
  store i32 8, i32* %bitrate_index690, align 8
  br label %if.end691

if.end691:                                        ; preds = %if.then688, %if.else672
  br label %if.end692

if.end692:                                        ; preds = %if.end691, %if.then671
  br label %if.end696

if.else693:                                       ; preds = %if.end660
  %513 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc694 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %513, i32 0, i32 12
  %bitrate_index695 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc694, i32 0, i32 2
  store i32 1, i32* %bitrate_index695, align 8
  br label %if.end696

if.end696:                                        ; preds = %if.else693, %if.end692
  %514 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @init_bit_stream_w(%struct.lame_internal_flags* %514)
  %515 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_index697 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %515, i32 0, i32 1
  %516 = load i32, i32* %samplerate_index697, align 4
  %517 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version698 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %517, i32 0, i32 0
  %518 = load i32, i32* %version698, align 4
  %mul699 = mul nsw i32 3, %518
  %add700 = add nsw i32 %516, %mul699
  %519 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out701 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %519, i32 0, i32 12
  %520 = load i32, i32* %samplerate_out701, align 4
  %cmp702 = icmp slt i32 %520, 16000
  %conv703 = zext i1 %cmp702 to i32
  %mul704 = mul nsw i32 6, %conv703
  %add705 = add nsw i32 %add700, %mul704
  store i32 %add705, i32* %j, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond706

for.cond706:                                      ; preds = %for.inc714, %if.end696
  %521 = load i32, i32* %i, align 4
  %cmp707 = icmp slt i32 %521, 23
  br i1 %cmp707, label %for.body709, label %for.end716

for.body709:                                      ; preds = %for.cond706
  %522 = load i32, i32* %j, align 4
  %arrayidx710 = getelementptr inbounds [9 x %struct.scalefac_struct], [9 x %struct.scalefac_struct]* @sfBandIndex, i32 0, i32 %522
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %arrayidx710, i32 0, i32 0
  %523 = load i32, i32* %i, align 4
  %arrayidx711 = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %523
  %524 = load i32, i32* %arrayidx711, align 4
  %525 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %525, i32 0, i32 8
  %l712 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %526 = load i32, i32* %i, align 4
  %arrayidx713 = getelementptr inbounds [23 x i32], [23 x i32]* %l712, i32 0, i32 %526
  store i32 %524, i32* %arrayidx713, align 4
  br label %for.inc714

for.inc714:                                       ; preds = %for.body709
  %527 = load i32, i32* %i, align 4
  %inc715 = add nsw i32 %527, 1
  store i32 %inc715, i32* %i, align 4
  br label %for.cond706

for.end716:                                       ; preds = %for.cond706
  store i32 0, i32* %i, align 4
  br label %for.cond717

for.cond717:                                      ; preds = %for.inc736, %for.end716
  %528 = load i32, i32* %i, align 4
  %cmp718 = icmp slt i32 %528, 7
  br i1 %cmp718, label %for.body720, label %for.end738

for.body720:                                      ; preds = %for.cond717
  %529 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band721 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %529, i32 0, i32 8
  %l722 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band721, i32 0, i32 0
  %arrayidx723 = getelementptr inbounds [23 x i32], [23 x i32]* %l722, i32 0, i32 22
  %530 = load i32, i32* %arrayidx723, align 4
  %531 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band724 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %531, i32 0, i32 8
  %l725 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band724, i32 0, i32 0
  %arrayidx726 = getelementptr inbounds [23 x i32], [23 x i32]* %l725, i32 0, i32 21
  %532 = load i32, i32* %arrayidx726, align 4
  %sub727 = sub nsw i32 %530, %532
  %div728 = sdiv i32 %sub727, 6
  store i32 %div728, i32* %size, align 4
  %533 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band729 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %533, i32 0, i32 8
  %l730 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band729, i32 0, i32 0
  %arrayidx731 = getelementptr inbounds [23 x i32], [23 x i32]* %l730, i32 0, i32 21
  %534 = load i32, i32* %arrayidx731, align 4
  %535 = load i32, i32* %i, align 4
  %536 = load i32, i32* %size, align 4
  %mul732 = mul nsw i32 %535, %536
  %add733 = add nsw i32 %534, %mul732
  store i32 %add733, i32* %start, align 4
  %537 = load i32, i32* %start, align 4
  %538 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band734 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %538, i32 0, i32 8
  %psfb21 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band734, i32 0, i32 2
  %539 = load i32, i32* %i, align 4
  %arrayidx735 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb21, i32 0, i32 %539
  store i32 %537, i32* %arrayidx735, align 4
  br label %for.inc736

for.inc736:                                       ; preds = %for.body720
  %540 = load i32, i32* %i, align 4
  %inc737 = add nsw i32 %540, 1
  store i32 %inc737, i32* %i, align 4
  br label %for.cond717

for.end738:                                       ; preds = %for.cond717
  %541 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band739 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %541, i32 0, i32 8
  %psfb21740 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band739, i32 0, i32 2
  %arrayidx741 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb21740, i32 0, i32 6
  store i32 576, i32* %arrayidx741, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond742

for.cond742:                                      ; preds = %for.inc751, %for.end738
  %542 = load i32, i32* %i, align 4
  %cmp743 = icmp slt i32 %542, 14
  br i1 %cmp743, label %for.body745, label %for.end753

for.body745:                                      ; preds = %for.cond742
  %543 = load i32, i32* %j, align 4
  %arrayidx746 = getelementptr inbounds [9 x %struct.scalefac_struct], [9 x %struct.scalefac_struct]* @sfBandIndex, i32 0, i32 %543
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %arrayidx746, i32 0, i32 1
  %544 = load i32, i32* %i, align 4
  %arrayidx747 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 %544
  %545 = load i32, i32* %arrayidx747, align 4
  %546 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band748 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %546, i32 0, i32 8
  %s749 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band748, i32 0, i32 1
  %547 = load i32, i32* %i, align 4
  %arrayidx750 = getelementptr inbounds [14 x i32], [14 x i32]* %s749, i32 0, i32 %547
  store i32 %545, i32* %arrayidx750, align 4
  br label %for.inc751

for.inc751:                                       ; preds = %for.body745
  %548 = load i32, i32* %i, align 4
  %inc752 = add nsw i32 %548, 1
  store i32 %inc752, i32* %i, align 4
  br label %for.cond742

for.end753:                                       ; preds = %for.cond742
  store i32 0, i32* %i, align 4
  br label %for.cond754

for.cond754:                                      ; preds = %for.inc775, %for.end753
  %549 = load i32, i32* %i, align 4
  %cmp755 = icmp slt i32 %549, 7
  br i1 %cmp755, label %for.body757, label %for.end777

for.body757:                                      ; preds = %for.cond754
  %550 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band759 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %550, i32 0, i32 8
  %s760 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band759, i32 0, i32 1
  %arrayidx761 = getelementptr inbounds [14 x i32], [14 x i32]* %s760, i32 0, i32 13
  %551 = load i32, i32* %arrayidx761, align 4
  %552 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band762 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %552, i32 0, i32 8
  %s763 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band762, i32 0, i32 1
  %arrayidx764 = getelementptr inbounds [14 x i32], [14 x i32]* %s763, i32 0, i32 12
  %553 = load i32, i32* %arrayidx764, align 4
  %sub765 = sub nsw i32 %551, %553
  %div766 = sdiv i32 %sub765, 6
  store i32 %div766, i32* %size758, align 4
  %554 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band768 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %554, i32 0, i32 8
  %s769 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band768, i32 0, i32 1
  %arrayidx770 = getelementptr inbounds [14 x i32], [14 x i32]* %s769, i32 0, i32 12
  %555 = load i32, i32* %arrayidx770, align 4
  %556 = load i32, i32* %i, align 4
  %557 = load i32, i32* %size758, align 4
  %mul771 = mul nsw i32 %556, %557
  %add772 = add nsw i32 %555, %mul771
  store i32 %add772, i32* %start767, align 4
  %558 = load i32, i32* %start767, align 4
  %559 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band773 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %559, i32 0, i32 8
  %psfb12 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band773, i32 0, i32 3
  %560 = load i32, i32* %i, align 4
  %arrayidx774 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb12, i32 0, i32 %560
  store i32 %558, i32* %arrayidx774, align 4
  br label %for.inc775

for.inc775:                                       ; preds = %for.body757
  %561 = load i32, i32* %i, align 4
  %inc776 = add nsw i32 %561, 1
  store i32 %inc776, i32* %i, align 4
  br label %for.cond754

for.end777:                                       ; preds = %for.cond754
  %562 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %scalefac_band778 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %562, i32 0, i32 8
  %psfb12779 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band778, i32 0, i32 3
  %arrayidx780 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb12779, i32 0, i32 6
  store i32 192, i32* %arrayidx780, align 4
  %563 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr781 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %563, i32 0, i32 15
  %564 = load i32, i32* %mode_gr781, align 4
  %cmp782 = icmp eq i32 %564, 2
  br i1 %cmp782, label %if.then784, label %if.else789

if.then784:                                       ; preds = %for.end777
  %565 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out785 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %565, i32 0, i32 14
  %566 = load i32, i32* %channels_out785, align 4
  %cmp786 = icmp eq i32 %566, 1
  %567 = zext i1 %cmp786 to i64
  %cond788 = select i1 %cmp786, i32 21, i32 36
  %568 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %568, i32 0, i32 2
  store i32 %cond788, i32* %sideinfo_len, align 4
  br label %if.end795

if.else789:                                       ; preds = %for.end777
  %569 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out790 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %569, i32 0, i32 14
  %570 = load i32, i32* %channels_out790, align 4
  %cmp791 = icmp eq i32 %570, 1
  %571 = zext i1 %cmp791 to i64
  %cond793 = select i1 %cmp791, i32 13, i32 21
  %572 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len794 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %572, i32 0, i32 2
  store i32 %cond793, i32* %sideinfo_len794, align 4
  br label %if.end795

if.end795:                                        ; preds = %if.else789, %if.then784
  %573 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection796 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %573, i32 0, i32 36
  %574 = load i32, i32* %error_protection796, align 4
  %tobool797 = icmp ne i32 %574, 0
  br i1 %tobool797, label %if.then798, label %if.end801

if.then798:                                       ; preds = %if.end795
  %575 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len799 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %575, i32 0, i32 2
  %576 = load i32, i32* %sideinfo_len799, align 4
  %add800 = add nsw i32 %576, 2
  store i32 %add800, i32* %sideinfo_len799, align 4
  br label %if.end801

if.end801:                                        ; preds = %if.then798, %if.end795
  store i32 0, i32* %k, align 4
  br label %for.cond802

for.cond802:                                      ; preds = %for.inc812, %if.end801
  %577 = load i32, i32* %k, align 4
  %cmp803 = icmp slt i32 %577, 19
  br i1 %cmp803, label %for.body805, label %for.end814

for.body805:                                      ; preds = %for.cond802
  %578 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr806 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %578, i32 0, i32 15
  %579 = load i32, i32* %mode_gr806, align 4
  %mul807 = mul nsw i32 700, %579
  %580 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out808 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %580, i32 0, i32 14
  %581 = load i32, i32* %channels_out808, align 4
  %mul809 = mul nsw i32 %mul807, %581
  %conv810 = sitofp i32 %mul809 to float
  %582 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %582, i32 0, i32 11
  %pefirbuf = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 5
  %583 = load i32, i32* %k, align 4
  %arrayidx811 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf, i32 0, i32 %583
  store float %conv810, float* %arrayidx811, align 4
  br label %for.inc812

for.inc812:                                       ; preds = %for.body805
  %584 = load i32, i32* %k, align 4
  %inc813 = add nsw i32 %584, 1
  store i32 %inc813, i32* %k, align 4
  br label %for.cond802

for.end814:                                       ; preds = %for.cond802
  %585 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %585, i32 0, i32 55
  %586 = load i32, i32* %ATHtype, align 4
  %cmp815 = icmp eq i32 %586, -1
  br i1 %cmp815, label %if.then817, label %if.end819

if.then817:                                       ; preds = %for.end814
  %587 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype818 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %587, i32 0, i32 55
  store i32 4, i32* %ATHtype818, align 4
  br label %if.end819

if.end819:                                        ; preds = %if.then817, %for.end814
  %588 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr820 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %588, i32 0, i32 22
  %589 = load i32, i32* %vbr820, align 4
  switch i32 %589, label %sw.default891 [
    i32 1, label %sw.bb821
    i32 4, label %sw.bb821
    i32 2, label %sw.bb862
  ]

sw.bb821:                                         ; preds = %if.end819, %if.end819
  %590 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %590, i32 0, i32 31
  %591 = load i32, i32* %strict_ISO, align 4
  %cmp822 = icmp slt i32 %591, 0
  br i1 %cmp822, label %if.then824, label %if.end826

if.then824:                                       ; preds = %sw.bb821
  %592 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO825 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %592, i32 0, i32 31
  store i32 2, i32* %strict_ISO825, align 4
  br label %if.end826

if.end826:                                        ; preds = %if.then824, %sw.bb821
  %593 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %593, i32 0, i32 61
  %594 = load i32, i32* %useTemporal, align 4
  %cmp827 = icmp slt i32 %594, 0
  br i1 %cmp827, label %if.then829, label %if.end831

if.then829:                                       ; preds = %if.end826
  %595 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal830 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %595, i32 0, i32 61
  store i32 0, i32* %useTemporal830, align 4
  br label %if.end831

if.end831:                                        ; preds = %if.then829, %if.end826
  %596 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %597 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q832 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %597, i32 0, i32 41
  %598 = load i32, i32* %VBR_q832, align 4
  %mul833 = mul nsw i32 %598, 10
  %sub834 = sub nsw i32 500, %mul833
  %call835 = call i32 @apply_preset(%struct.lame_global_struct* %596, i32 %sub834, i32 0)
  %599 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %599, i32 0, i32 11
  %600 = load i32, i32* %quality, align 4
  %cmp836 = icmp slt i32 %600, 0
  br i1 %cmp836, label %if.then838, label %if.end840

if.then838:                                       ; preds = %if.end831
  %601 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality839 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %601, i32 0, i32 11
  store i32 3, i32* %quality839, align 4
  br label %if.end840

if.end840:                                        ; preds = %if.then838, %if.end831
  %602 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality841 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %602, i32 0, i32 11
  %603 = load i32, i32* %quality841, align 4
  %cmp842 = icmp slt i32 %603, 5
  br i1 %cmp842, label %if.then844, label %if.end846

if.then844:                                       ; preds = %if.end840
  %604 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality845 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %604, i32 0, i32 11
  store i32 0, i32* %quality845, align 4
  br label %if.end846

if.end846:                                        ; preds = %if.then844, %if.end840
  %605 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality847 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %605, i32 0, i32 11
  %606 = load i32, i32* %quality847, align 4
  %cmp848 = icmp sgt i32 %606, 7
  br i1 %cmp848, label %if.then850, label %if.end852

if.then850:                                       ; preds = %if.end846
  %607 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality851 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %607, i32 0, i32 11
  store i32 7, i32* %quality851, align 4
  br label %if.end852

if.end852:                                        ; preds = %if.then850, %if.end846
  %608 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalY = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %608, i32 0, i32 35
  %609 = load i32, i32* %experimentalY, align 4
  %tobool853 = icmp ne i32 %609, 0
  br i1 %tobool853, label %if.then854, label %if.else855

if.then854:                                       ; preds = %if.end852
  %610 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %610, i32 0, i32 13
  %sfb21_extra = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 8
  store i32 0, i32* %sfb21_extra, align 4
  br label %if.end861

if.else855:                                       ; preds = %if.end852
  %611 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out856 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %611, i32 0, i32 12
  %612 = load i32, i32* %samplerate_out856, align 4
  %cmp857 = icmp sgt i32 %612, 44000
  %conv858 = zext i1 %cmp857 to i32
  %613 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt859 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %613, i32 0, i32 13
  %sfb21_extra860 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt859, i32 0, i32 8
  store i32 %conv858, i32* %sfb21_extra860, align 4
  br label %if.end861

if.end861:                                        ; preds = %if.else855, %if.then854
  br label %sw.epilog911

sw.bb862:                                         ; preds = %if.end819
  %614 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %615 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q863 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %615, i32 0, i32 41
  %616 = load i32, i32* %VBR_q863, align 4
  %mul864 = mul nsw i32 %616, 10
  %sub865 = sub nsw i32 500, %mul864
  %call866 = call i32 @apply_preset(%struct.lame_global_struct* %614, i32 %sub865, i32 0)
  %617 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalY867 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %617, i32 0, i32 35
  %618 = load i32, i32* %experimentalY867, align 4
  %tobool868 = icmp ne i32 %618, 0
  br i1 %tobool868, label %if.then869, label %if.else872

if.then869:                                       ; preds = %sw.bb862
  %619 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt870 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %619, i32 0, i32 13
  %sfb21_extra871 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt870, i32 0, i32 8
  store i32 0, i32* %sfb21_extra871, align 4
  br label %if.end878

if.else872:                                       ; preds = %sw.bb862
  %620 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out873 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %620, i32 0, i32 12
  %621 = load i32, i32* %samplerate_out873, align 4
  %cmp874 = icmp sgt i32 %621, 44000
  %conv875 = zext i1 %cmp874 to i32
  %622 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt876 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %622, i32 0, i32 13
  %sfb21_extra877 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt876, i32 0, i32 8
  store i32 %conv875, i32* %sfb21_extra877, align 4
  br label %if.end878

if.end878:                                        ; preds = %if.else872, %if.then869
  %623 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality879 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %623, i32 0, i32 11
  %624 = load i32, i32* %quality879, align 4
  %cmp880 = icmp sgt i32 %624, 6
  br i1 %cmp880, label %if.then882, label %if.end884

if.then882:                                       ; preds = %if.end878
  %625 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality883 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %625, i32 0, i32 11
  store i32 6, i32* %quality883, align 4
  br label %if.end884

if.end884:                                        ; preds = %if.then882, %if.end878
  %626 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality885 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %626, i32 0, i32 11
  %627 = load i32, i32* %quality885, align 4
  %cmp886 = icmp slt i32 %627, 0
  br i1 %cmp886, label %if.then888, label %if.end890

if.then888:                                       ; preds = %if.end884
  %628 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality889 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %628, i32 0, i32 11
  store i32 3, i32* %quality889, align 4
  br label %if.end890

if.end890:                                        ; preds = %if.then888, %if.end884
  br label %sw.epilog911

sw.default891:                                    ; preds = %if.end819
  %629 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt892 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %629, i32 0, i32 13
  %sfb21_extra893 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt892, i32 0, i32 8
  store i32 0, i32* %sfb21_extra893, align 4
  %630 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality894 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %630, i32 0, i32 11
  %631 = load i32, i32* %quality894, align 4
  %cmp895 = icmp slt i32 %631, 0
  br i1 %cmp895, label %if.then897, label %if.end899

if.then897:                                       ; preds = %sw.default891
  %632 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality898 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %632, i32 0, i32 11
  store i32 3, i32* %quality898, align 4
  br label %if.end899

if.end899:                                        ; preds = %if.then897, %sw.default891
  %633 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr900 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %633, i32 0, i32 22
  %634 = load i32, i32* %vbr900, align 4
  %cmp901 = icmp eq i32 %634, 0
  br i1 %cmp901, label %if.then903, label %if.end906

if.then903:                                       ; preds = %if.end899
  %635 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %636 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate904 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %636, i32 0, i32 24
  %637 = load i32, i32* %brate904, align 4
  %call905 = call i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct* %635, i32 %637)
  br label %if.end906

if.end906:                                        ; preds = %if.then903, %if.end899
  %638 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %639 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps907 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %639, i32 0, i32 42
  %640 = load i32, i32* %VBR_mean_bitrate_kbps907, align 4
  %call908 = call i32 @apply_preset(%struct.lame_global_struct* %638, i32 %640, i32 0)
  %641 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr909 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %641, i32 0, i32 22
  %642 = load i32, i32* %vbr909, align 4
  %643 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR910 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %643, i32 0, i32 39
  store i32 %642, i32* %VBR910, align 4
  br label %sw.epilog911

sw.epilog911:                                     ; preds = %if.end906, %if.end890, %if.end861
  %644 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %644, i32 0, i32 50
  %645 = load float, float* %maskingadjust, align 4
  %646 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt912 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %646, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt912, i32 0, i32 3
  store float %645, float* %mask_adjust, align 8
  %647 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %maskingadjust_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %647, i32 0, i32 51
  %648 = load float, float* %maskingadjust_short, align 4
  %649 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt913 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %649, i32 0, i32 13
  %mask_adjust_short = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt913, i32 0, i32 4
  store float %648, float* %mask_adjust_short, align 4
  %650 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tune = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %650, i32 0, i32 64
  %651 = load i32, i32* %tune, align 4
  %tobool914 = icmp ne i32 %651, 0
  br i1 %tobool914, label %if.then915, label %if.end923

if.then915:                                       ; preds = %sw.epilog911
  %652 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tune_value_a = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %652, i32 0, i32 65
  %653 = load float, float* %tune_value_a, align 4
  %654 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt916 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %654, i32 0, i32 13
  %mask_adjust917 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt916, i32 0, i32 3
  %655 = load float, float* %mask_adjust917, align 8
  %add918 = fadd float %655, %653
  store float %add918, float* %mask_adjust917, align 8
  %656 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tune_value_a919 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %656, i32 0, i32 65
  %657 = load float, float* %tune_value_a919, align 4
  %658 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt920 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %658, i32 0, i32 13
  %mask_adjust_short921 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt920, i32 0, i32 4
  %659 = load float, float* %mask_adjust_short921, align 4
  %add922 = fadd float %659, %657
  store float %add922, float* %mask_adjust_short921, align 4
  br label %if.end923

if.end923:                                        ; preds = %if.then915, %sw.epilog911
  %660 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr924 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %660, i32 0, i32 22
  %661 = load i32, i32* %vbr924, align 4
  %cmp925 = icmp ne i32 %661, 0
  br i1 %cmp925, label %if.then927, label %if.end1020

if.then927:                                       ; preds = %if.end923
  %662 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %662, i32 0, i32 24
  store i32 1, i32* %vbr_min_bitrate_index, align 4
  %663 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %663, i32 0, i32 25
  store i32 14, i32* %vbr_max_bitrate_index, align 4
  %664 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out928 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %664, i32 0, i32 12
  %665 = load i32, i32* %samplerate_out928, align 4
  %cmp929 = icmp slt i32 %665, 16000
  br i1 %cmp929, label %if.then931, label %if.end933

if.then931:                                       ; preds = %if.then927
  %666 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index932 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %666, i32 0, i32 25
  store i32 8, i32* %vbr_max_bitrate_index932, align 4
  br label %if.end933

if.end933:                                        ; preds = %if.then931, %if.then927
  %667 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %667, i32 0, i32 43
  %668 = load i32, i32* %VBR_min_bitrate_kbps, align 4
  %tobool934 = icmp ne i32 %668, 0
  br i1 %tobool934, label %if.then935, label %if.end952

if.then935:                                       ; preds = %if.end933
  %669 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps936 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %669, i32 0, i32 43
  %670 = load i32, i32* %VBR_min_bitrate_kbps936, align 4
  %671 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version937 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %671, i32 0, i32 0
  %672 = load i32, i32* %version937, align 4
  %673 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out938 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %673, i32 0, i32 12
  %674 = load i32, i32* %samplerate_out938, align 4
  %call939 = call i32 @FindNearestBitrate(i32 %670, i32 %672, i32 %674)
  %675 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps940 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %675, i32 0, i32 43
  store i32 %call939, i32* %VBR_min_bitrate_kbps940, align 4
  %676 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps941 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %676, i32 0, i32 43
  %677 = load i32, i32* %VBR_min_bitrate_kbps941, align 4
  %678 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version942 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %678, i32 0, i32 0
  %679 = load i32, i32* %version942, align 4
  %680 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out943 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %680, i32 0, i32 12
  %681 = load i32, i32* %samplerate_out943, align 4
  %call944 = call i32 @BitrateIndex(i32 %677, i32 %679, i32 %681)
  %682 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index945 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %682, i32 0, i32 24
  store i32 %call944, i32* %vbr_min_bitrate_index945, align 4
  %683 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index946 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %683, i32 0, i32 24
  %684 = load i32, i32* %vbr_min_bitrate_index946, align 4
  %cmp947 = icmp slt i32 %684, 0
  br i1 %cmp947, label %if.then949, label %if.end951

if.then949:                                       ; preds = %if.then935
  %685 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index950 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %685, i32 0, i32 24
  store i32 1, i32* %vbr_min_bitrate_index950, align 4
  br label %if.end951

if.end951:                                        ; preds = %if.then949, %if.then935
  br label %if.end952

if.end952:                                        ; preds = %if.end951, %if.end933
  %686 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %686, i32 0, i32 44
  %687 = load i32, i32* %VBR_max_bitrate_kbps, align 4
  %tobool953 = icmp ne i32 %687, 0
  br i1 %tobool953, label %if.then954, label %if.end975

if.then954:                                       ; preds = %if.end952
  %688 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps955 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %688, i32 0, i32 44
  %689 = load i32, i32* %VBR_max_bitrate_kbps955, align 4
  %690 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version956 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %690, i32 0, i32 0
  %691 = load i32, i32* %version956, align 4
  %692 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out957 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %692, i32 0, i32 12
  %693 = load i32, i32* %samplerate_out957, align 4
  %call958 = call i32 @FindNearestBitrate(i32 %689, i32 %691, i32 %693)
  %694 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps959 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %694, i32 0, i32 44
  store i32 %call958, i32* %VBR_max_bitrate_kbps959, align 4
  %695 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps960 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %695, i32 0, i32 44
  %696 = load i32, i32* %VBR_max_bitrate_kbps960, align 4
  %697 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version961 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %697, i32 0, i32 0
  %698 = load i32, i32* %version961, align 4
  %699 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out962 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %699, i32 0, i32 12
  %700 = load i32, i32* %samplerate_out962, align 4
  %call963 = call i32 @BitrateIndex(i32 %696, i32 %698, i32 %700)
  %701 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index964 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %701, i32 0, i32 25
  store i32 %call963, i32* %vbr_max_bitrate_index964, align 4
  %702 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index965 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %702, i32 0, i32 25
  %703 = load i32, i32* %vbr_max_bitrate_index965, align 4
  %cmp966 = icmp slt i32 %703, 0
  br i1 %cmp966, label %if.then968, label %if.end974

if.then968:                                       ; preds = %if.then954
  %704 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out969 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %704, i32 0, i32 12
  %705 = load i32, i32* %samplerate_out969, align 4
  %cmp970 = icmp slt i32 %705, 16000
  %706 = zext i1 %cmp970 to i64
  %cond972 = select i1 %cmp970, i32 8, i32 14
  %707 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index973 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %707, i32 0, i32 25
  store i32 %cond972, i32* %vbr_max_bitrate_index973, align 4
  br label %if.end974

if.end974:                                        ; preds = %if.then968, %if.then954
  br label %if.end975

if.end975:                                        ; preds = %if.end974, %if.end952
  %708 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version976 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %708, i32 0, i32 0
  %709 = load i32, i32* %version976, align 4
  %arrayidx977 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %709
  %710 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index978 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %710, i32 0, i32 24
  %711 = load i32, i32* %vbr_min_bitrate_index978, align 4
  %arrayidx979 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx977, i32 0, i32 %711
  %712 = load i32, i32* %arrayidx979, align 4
  %713 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps980 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %713, i32 0, i32 43
  store i32 %712, i32* %VBR_min_bitrate_kbps980, align 4
  %714 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version981 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %714, i32 0, i32 0
  %715 = load i32, i32* %version981, align 4
  %arrayidx982 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %715
  %716 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index983 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %716, i32 0, i32 25
  %717 = load i32, i32* %vbr_max_bitrate_index983, align 4
  %arrayidx984 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx982, i32 0, i32 %717
  %718 = load i32, i32* %arrayidx984, align 4
  %719 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps985 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %719, i32 0, i32 44
  store i32 %718, i32* %VBR_max_bitrate_kbps985, align 4
  %720 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version986 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %720, i32 0, i32 0
  %721 = load i32, i32* %version986, align 4
  %arrayidx987 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %721
  %722 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index988 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %722, i32 0, i32 25
  %723 = load i32, i32* %vbr_max_bitrate_index988, align 4
  %arrayidx989 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx987, i32 0, i32 %723
  %724 = load i32, i32* %arrayidx989, align 4
  %725 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps990 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %725, i32 0, i32 42
  %726 = load i32, i32* %VBR_mean_bitrate_kbps990, align 4
  %cmp991 = icmp slt i32 %724, %726
  br i1 %cmp991, label %cond.true993, label %cond.false998

cond.true993:                                     ; preds = %if.end975
  %727 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version994 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %727, i32 0, i32 0
  %728 = load i32, i32* %version994, align 4
  %arrayidx995 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %728
  %729 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index996 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %729, i32 0, i32 25
  %730 = load i32, i32* %vbr_max_bitrate_index996, align 4
  %arrayidx997 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx995, i32 0, i32 %730
  %731 = load i32, i32* %arrayidx997, align 4
  br label %cond.end1000

cond.false998:                                    ; preds = %if.end975
  %732 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps999 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %732, i32 0, i32 42
  %733 = load i32, i32* %VBR_mean_bitrate_kbps999, align 4
  br label %cond.end1000

cond.end1000:                                     ; preds = %cond.false998, %cond.true993
  %cond1001 = phi i32 [ %731, %cond.true993 ], [ %733, %cond.false998 ]
  %734 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1002 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %734, i32 0, i32 42
  store i32 %cond1001, i32* %VBR_mean_bitrate_kbps1002, align 4
  %735 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version1003 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %735, i32 0, i32 0
  %736 = load i32, i32* %version1003, align 4
  %arrayidx1004 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %736
  %737 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index1005 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %737, i32 0, i32 24
  %738 = load i32, i32* %vbr_min_bitrate_index1005, align 4
  %arrayidx1006 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx1004, i32 0, i32 %738
  %739 = load i32, i32* %arrayidx1006, align 4
  %740 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1007 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %740, i32 0, i32 42
  %741 = load i32, i32* %VBR_mean_bitrate_kbps1007, align 4
  %cmp1008 = icmp sgt i32 %739, %741
  br i1 %cmp1008, label %cond.true1010, label %cond.false1015

cond.true1010:                                    ; preds = %cond.end1000
  %742 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version1011 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %742, i32 0, i32 0
  %743 = load i32, i32* %version1011, align 4
  %arrayidx1012 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %743
  %744 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index1013 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %744, i32 0, i32 24
  %745 = load i32, i32* %vbr_min_bitrate_index1013, align 4
  %arrayidx1014 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx1012, i32 0, i32 %745
  %746 = load i32, i32* %arrayidx1014, align 4
  br label %cond.end1017

cond.false1015:                                   ; preds = %cond.end1000
  %747 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1016 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %747, i32 0, i32 42
  %748 = load i32, i32* %VBR_mean_bitrate_kbps1016, align 4
  br label %cond.end1017

cond.end1017:                                     ; preds = %cond.false1015, %cond.true1010
  %cond1018 = phi i32 [ %746, %cond.true1010 ], [ %748, %cond.false1015 ]
  %749 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1019 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %749, i32 0, i32 42
  store i32 %cond1018, i32* %VBR_mean_bitrate_kbps1019, align 4
  br label %if.end1020

if.end1020:                                       ; preds = %cond.end1017, %if.end923
  %750 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %750, i32 0, i32 38
  %751 = load i32, i32* %preset, align 4
  %752 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %preset1021 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %752, i32 0, i32 21
  store i32 %751, i32* %preset1021, align 4
  %753 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag1022 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %753, i32 0, i32 9
  %754 = load i32, i32* %write_lame_tag1022, align 4
  %755 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %write_lame_tag1023 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %755, i32 0, i32 35
  store i32 %754, i32* %write_lame_tag1023, align 4
  %756 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %substep_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %756, i32 0, i32 20
  %757 = load i32, i32* %substep_shaping, align 4
  %758 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt1024 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %758, i32 0, i32 13
  %substep_shaping1025 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt1024, i32 0, i32 9
  store i32 %757, i32* %substep_shaping1025, align 8
  %759 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noise_shaping = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %759, i32 0, i32 21
  %760 = load i32, i32* %noise_shaping, align 4
  %761 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping1026 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %761, i32 0, i32 3
  store i32 %760, i32* %noise_shaping1026, align 4
  %762 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %762, i32 0, i32 22
  %763 = load i32, i32* %subblock_gain, align 4
  %764 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain1027 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %764, i32 0, i32 4
  store i32 %763, i32* %subblock_gain1027, align 4
  %765 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %use_best_huffman = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %765, i32 0, i32 23
  %766 = load i32, i32* %use_best_huffman, align 4
  %767 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman1028 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %767, i32 0, i32 5
  store i32 %766, i32* %use_best_huffman1028, align 4
  %768 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %brate1029 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %768, i32 0, i32 24
  %769 = load i32, i32* %brate1029, align 4
  %770 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %770, i32 0, i32 26
  store i32 %769, i32* %avg_bitrate, align 4
  %771 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps1030 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %771, i32 0, i32 42
  %772 = load i32, i32* %VBR_mean_bitrate_kbps1030, align 4
  %773 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_avg_bitrate_kbps = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %773, i32 0, i32 23
  store i32 %772, i32* %vbr_avg_bitrate_kbps, align 4
  %774 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %compression_ratio1031 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %774, i32 0, i32 25
  %775 = load float, float* %compression_ratio1031, align 4
  %776 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %compression_ratio1032 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %776, i32 0, i32 57
  store float %775, float* %compression_ratio1032, align 4
  %777 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  call void @lame_init_qval(%struct.lame_global_struct* %777)
  %778 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_type = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %778, i32 0, i32 58
  %779 = load i32, i32* %athaa_type, align 4
  %cmp1033 = icmp slt i32 %779, 0
  br i1 %cmp1033, label %if.then1035, label %if.else1036

if.then1035:                                      ; preds = %if.end1020
  %780 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %780, i32 0, i32 21
  %781 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %use_adjust = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %781, i32 0, i32 0
  store i32 3, i32* %use_adjust, align 4
  br label %if.end1040

if.else1036:                                      ; preds = %if.end1020
  %782 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_type1037 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %782, i32 0, i32 58
  %783 = load i32, i32* %athaa_type1037, align 4
  %784 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH1038 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %784, i32 0, i32 21
  %785 = load %struct.ATH_t*, %struct.ATH_t** %ATH1038, align 8
  %use_adjust1039 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %785, i32 0, i32 0
  store i32 %783, i32* %use_adjust1039, align 4
  br label %if.end1040

if.end1040:                                       ; preds = %if.else1036, %if.then1035
  %786 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_sensitivity = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %786, i32 0, i32 59
  %787 = load float, float* %athaa_sensitivity, align 4
  %conv1041 = fpext float %787 to double
  %div1042 = fdiv double %conv1041, -1.000000e+01
  %788 = call double @llvm.pow.f64(double 1.000000e+01, double %div1042)
  %conv1043 = fptrunc double %788 to float
  %789 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH1044 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %789, i32 0, i32 21
  %790 = load %struct.ATH_t*, %struct.ATH_t** %ATH1044, align 8
  %aa_sensitivity_p = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %790, i32 0, i32 1
  store float %conv1043, float* %aa_sensitivity_p, align 4
  %791 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %791, i32 0, i32 60
  %792 = load i32, i32* %short_blocks, align 4
  %cmp1045 = icmp eq i32 %792, -1
  br i1 %cmp1045, label %if.then1047, label %if.end1049

if.then1047:                                      ; preds = %if.end1040
  %793 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks1048 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %793, i32 0, i32 60
  store i32 0, i32* %short_blocks1048, align 4
  br label %if.end1049

if.end1049:                                       ; preds = %if.then1047, %if.end1040
  %794 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks1050 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %794, i32 0, i32 60
  %795 = load i32, i32* %short_blocks1050, align 4
  %cmp1051 = icmp eq i32 %795, 0
  br i1 %cmp1051, label %land.lhs.true1053, label %if.end1063

land.lhs.true1053:                                ; preds = %if.end1049
  %796 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode1054 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %796, i32 0, i32 41
  %797 = load i32, i32* %mode1054, align 4
  %cmp1055 = icmp eq i32 %797, 1
  br i1 %cmp1055, label %if.then1061, label %lor.lhs.false1057

lor.lhs.false1057:                                ; preds = %land.lhs.true1053
  %798 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode1058 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %798, i32 0, i32 41
  %799 = load i32, i32* %mode1058, align 4
  %cmp1059 = icmp eq i32 %799, 0
  br i1 %cmp1059, label %if.then1061, label %if.end1063

if.then1061:                                      ; preds = %lor.lhs.false1057, %land.lhs.true1053
  %800 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks1062 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %800, i32 0, i32 60
  store i32 1, i32* %short_blocks1062, align 4
  br label %if.end1063

if.end1063:                                       ; preds = %if.then1061, %lor.lhs.false1057, %if.end1049
  %801 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks1064 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %801, i32 0, i32 60
  %802 = load i32, i32* %short_blocks1064, align 4
  %803 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %short_blocks1065 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %803, i32 0, i32 42
  store i32 %802, i32* %short_blocks1065, align 4
  %804 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1066 = call i32 @lame_get_quant_comp(%struct.lame_global_struct* %804)
  %cmp1067 = icmp slt i32 %call1066, 0
  br i1 %cmp1067, label %if.then1069, label %if.end1071

if.then1069:                                      ; preds = %if.end1063
  %805 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1070 = call i32 @lame_set_quant_comp(%struct.lame_global_struct* %805, i32 1)
  br label %if.end1071

if.end1071:                                       ; preds = %if.then1069, %if.end1063
  %806 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1072 = call i32 @lame_get_quant_comp_short(%struct.lame_global_struct* %806)
  %cmp1073 = icmp slt i32 %call1072, 0
  br i1 %cmp1073, label %if.then1075, label %if.end1077

if.then1075:                                      ; preds = %if.end1071
  %807 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1076 = call i32 @lame_set_quant_comp_short(%struct.lame_global_struct* %807, i32 0)
  br label %if.end1077

if.end1077:                                       ; preds = %if.then1075, %if.end1071
  %808 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1078 = call float @lame_get_msfix(%struct.lame_global_struct* %808)
  %cmp1079 = fcmp olt float %call1078, 0.000000e+00
  br i1 %cmp1079, label %if.then1081, label %if.end1082

if.then1081:                                      ; preds = %if.end1077
  %809 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  call void @lame_set_msfix(%struct.lame_global_struct* %809, double 0.000000e+00)
  br label %if.end1082

if.end1082:                                       ; preds = %if.then1081, %if.end1077
  %810 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %811 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1083 = call i32 @lame_get_exp_nspsytune(%struct.lame_global_struct* %811)
  %or = or i32 %call1083, 1
  %call1084 = call i32 @lame_set_exp_nspsytune(%struct.lame_global_struct* %810, i32 %or)
  %812 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype1085 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %812, i32 0, i32 55
  %813 = load i32, i32* %ATHtype1085, align 4
  %cmp1086 = icmp slt i32 %813, 0
  br i1 %cmp1086, label %if.then1088, label %if.end1090

if.then1088:                                      ; preds = %if.end1082
  %814 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype1089 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %814, i32 0, i32 55
  store i32 4, i32* %ATHtype1089, align 4
  br label %if.end1090

if.end1090:                                       ; preds = %if.then1088, %if.end1082
  %815 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %815, i32 0, i32 56
  %816 = load float, float* %ATHcurve, align 4
  %cmp1091 = fcmp olt float %816, 0.000000e+00
  br i1 %cmp1091, label %if.then1093, label %if.end1095

if.then1093:                                      ; preds = %if.end1090
  %817 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve1094 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %817, i32 0, i32 56
  store float 4.000000e+00, float* %ATHcurve1094, align 4
  br label %if.end1095

if.end1095:                                       ; preds = %if.then1093, %if.end1090
  %818 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %818, i32 0, i32 62
  %819 = load float, float* %interChRatio, align 4
  %cmp1096 = fcmp olt float %819, 0.000000e+00
  br i1 %cmp1096, label %if.then1098, label %if.end1100

if.then1098:                                      ; preds = %if.end1095
  %820 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio1099 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %820, i32 0, i32 62
  store float 0.000000e+00, float* %interChRatio1099, align 4
  br label %if.end1100

if.end1100:                                       ; preds = %if.then1098, %if.end1095
  %821 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal1101 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %821, i32 0, i32 61
  %822 = load i32, i32* %useTemporal1101, align 4
  %cmp1102 = icmp slt i32 %822, 0
  br i1 %cmp1102, label %if.then1104, label %if.end1106

if.then1104:                                      ; preds = %if.end1100
  %823 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal1105 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %823, i32 0, i32 61
  store i32 1, i32* %useTemporal1105, align 4
  br label %if.end1106

if.end1106:                                       ; preds = %if.then1104, %if.end1100
  %824 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio1107 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %824, i32 0, i32 62
  %825 = load float, float* %interChRatio1107, align 4
  %826 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %interChRatio1108 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %826, i32 0, i32 43
  store float %825, float* %interChRatio1108, align 4
  %827 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %msfix = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %827, i32 0, i32 63
  %828 = load float, float* %msfix, align 4
  %829 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %msfix1109 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %829, i32 0, i32 44
  store float %828, float* %msfix1109, align 4
  %830 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATH_lower_db = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %830, i32 0, i32 57
  %831 = load float, float* %ATH_lower_db, align 4
  %sub1110 = fsub float 0.000000e+00, %831
  %832 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATH_offset_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %832, i32 0, i32 45
  store float %sub1110, float* %ATH_offset_db, align 4
  %833 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATH_offset_db1111 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %833, i32 0, i32 45
  %834 = load float, float* %ATH_offset_db1111, align 4
  %mul1112 = fmul float %834, 0x3FB99999A0000000
  %835 = call float @llvm.pow.f32(float 1.000000e+01, float %mul1112)
  %836 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATH_offset_factor = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %836, i32 0, i32 46
  store float %835, float* %ATH_offset_factor, align 4
  %837 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve1113 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %837, i32 0, i32 56
  %838 = load float, float* %ATHcurve1113, align 4
  %839 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHcurve1114 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %839, i32 0, i32 47
  store float %838, float* %ATHcurve1114, align 4
  %840 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype1115 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %840, i32 0, i32 55
  %841 = load i32, i32* %ATHtype1115, align 4
  %842 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHtype1116 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %842, i32 0, i32 48
  store i32 %841, i32* %ATHtype1116, align 4
  %843 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHonly = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %843, i32 0, i32 52
  %844 = load i32, i32* %ATHonly, align 4
  %845 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHonly1117 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %845, i32 0, i32 49
  store i32 %844, i32* %ATHonly1117, align 4
  %846 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHshort = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %846, i32 0, i32 53
  %847 = load i32, i32* %ATHshort, align 4
  %848 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHshort1118 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %848, i32 0, i32 50
  store i32 %847, i32* %ATHshort1118, align 4
  %849 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %noATH = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %849, i32 0, i32 54
  %850 = load i32, i32* %noATH, align 4
  %851 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noATH1119 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %851, i32 0, i32 51
  store i32 %850, i32* %noATH1119, align 4
  %852 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %852, i32 0, i32 33
  %853 = load i32, i32* %quant_comp, align 4
  %854 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp1120 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %854, i32 0, i32 17
  store i32 %853, i32* %quant_comp1120, align 4
  %855 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %855, i32 0, i32 34
  %856 = load i32, i32* %quant_comp_short, align 4
  %857 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp_short1121 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %857, i32 0, i32 18
  store i32 %856, i32* %quant_comp_short1121, align 4
  %858 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal1122 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %858, i32 0, i32 61
  %859 = load i32, i32* %useTemporal1122, align 4
  %860 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_temporal_masking_effect = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %860, i32 0, i32 19
  store i32 %859, i32* %use_temporal_masking_effect, align 4
  %861 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode1123 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %861, i32 0, i32 41
  %862 = load i32, i32* %mode1123, align 4
  %cmp1124 = icmp eq i32 %862, 1
  br i1 %cmp1124, label %if.then1126, label %if.else1127

if.then1126:                                      ; preds = %if.end1106
  %863 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %863, i32 0, i32 37
  %864 = load i32, i32* %exp_nspsytune, align 4
  %and = and i32 %864, 2
  %865 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_safe_joint_stereo = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %865, i32 0, i32 20
  store i32 %and, i32* %use_safe_joint_stereo, align 4
  br label %if.end1129

if.else1127:                                      ; preds = %if.end1106
  %866 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_safe_joint_stereo1128 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %866, i32 0, i32 20
  store i32 0, i32* %use_safe_joint_stereo1128, align 4
  br label %if.end1129

if.end1129:                                       ; preds = %if.else1127, %if.then1126
  %867 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune1130 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %867, i32 0, i32 37
  %868 = load i32, i32* %exp_nspsytune1130, align 4
  %shr = ashr i32 %868, 2
  %and1131 = and i32 %shr, 63
  %conv1132 = sitofp i32 %and1131 to float
  %869 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %869, i32 0, i32 54
  store float %conv1132, float* %adjust_bass_db, align 4
  %870 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db1133 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %870, i32 0, i32 54
  %871 = load float, float* %adjust_bass_db1133, align 4
  %cmp1134 = fcmp oge float %871, 3.200000e+01
  br i1 %cmp1134, label %if.then1136, label %if.end1139

if.then1136:                                      ; preds = %if.end1129
  %872 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db1137 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %872, i32 0, i32 54
  %873 = load float, float* %adjust_bass_db1137, align 4
  %sub1138 = fsub float %873, 6.400000e+01
  store float %sub1138, float* %adjust_bass_db1137, align 4
  br label %if.end1139

if.end1139:                                       ; preds = %if.then1136, %if.end1129
  %874 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_bass_db1140 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %874, i32 0, i32 54
  %875 = load float, float* %adjust_bass_db1140, align 4
  %mul1141 = fmul float %875, 2.500000e-01
  store float %mul1141, float* %adjust_bass_db1140, align 4
  %876 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune1142 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %876, i32 0, i32 37
  %877 = load i32, i32* %exp_nspsytune1142, align 4
  %shr1143 = ashr i32 %877, 8
  %and1144 = and i32 %shr1143, 63
  %conv1145 = sitofp i32 %and1144 to float
  %878 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %878, i32 0, i32 53
  store float %conv1145, float* %adjust_alto_db, align 4
  %879 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db1146 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %879, i32 0, i32 53
  %880 = load float, float* %adjust_alto_db1146, align 4
  %cmp1147 = fcmp oge float %880, 3.200000e+01
  br i1 %cmp1147, label %if.then1149, label %if.end1152

if.then1149:                                      ; preds = %if.end1139
  %881 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db1150 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %881, i32 0, i32 53
  %882 = load float, float* %adjust_alto_db1150, align 4
  %sub1151 = fsub float %882, 6.400000e+01
  store float %sub1151, float* %adjust_alto_db1150, align 4
  br label %if.end1152

if.end1152:                                       ; preds = %if.then1149, %if.end1139
  %883 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_alto_db1153 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %883, i32 0, i32 53
  %884 = load float, float* %adjust_alto_db1153, align 4
  %mul1154 = fmul float %884, 2.500000e-01
  store float %mul1154, float* %adjust_alto_db1153, align 4
  %885 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune1155 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %885, i32 0, i32 37
  %886 = load i32, i32* %exp_nspsytune1155, align 4
  %shr1156 = ashr i32 %886, 14
  %and1157 = and i32 %shr1156, 63
  %conv1158 = sitofp i32 %and1157 to float
  %887 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %887, i32 0, i32 55
  store float %conv1158, float* %adjust_treble_db, align 4
  %888 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db1159 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %888, i32 0, i32 55
  %889 = load float, float* %adjust_treble_db1159, align 4
  %cmp1160 = fcmp oge float %889, 3.200000e+01
  br i1 %cmp1160, label %if.then1162, label %if.end1165

if.then1162:                                      ; preds = %if.end1152
  %890 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db1163 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %890, i32 0, i32 55
  %891 = load float, float* %adjust_treble_db1163, align 4
  %sub1164 = fsub float %891, 6.400000e+01
  store float %sub1164, float* %adjust_treble_db1163, align 4
  br label %if.end1165

if.end1165:                                       ; preds = %if.then1162, %if.end1152
  %892 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db1166 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %892, i32 0, i32 55
  %893 = load float, float* %adjust_treble_db1166, align 4
  %mul1167 = fmul float %893, 2.500000e-01
  store float %mul1167, float* %adjust_treble_db1166, align 4
  %894 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %exp_nspsytune1168 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %894, i32 0, i32 37
  %895 = load i32, i32* %exp_nspsytune1168, align 4
  %shr1169 = ashr i32 %895, 20
  %and1170 = and i32 %shr1169, 63
  %conv1171 = sitofp i32 %and1170 to float
  %896 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %896, i32 0, i32 56
  store float %conv1171, float* %adjust_sfb21_db, align 4
  %897 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db1172 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %897, i32 0, i32 56
  %898 = load float, float* %adjust_sfb21_db1172, align 4
  %cmp1173 = fcmp oge float %898, 3.200000e+01
  br i1 %cmp1173, label %if.then1175, label %if.end1178

if.then1175:                                      ; preds = %if.end1165
  %899 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db1176 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %899, i32 0, i32 56
  %900 = load float, float* %adjust_sfb21_db1176, align 4
  %sub1177 = fsub float %900, 6.400000e+01
  store float %sub1177, float* %adjust_sfb21_db1176, align 4
  br label %if.end1178

if.end1178:                                       ; preds = %if.then1175, %if.end1165
  %901 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db1179 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %901, i32 0, i32 56
  %902 = load float, float* %adjust_sfb21_db1179, align 4
  %mul1180 = fmul float %902, 2.500000e-01
  store float %mul1180, float* %adjust_sfb21_db1179, align 4
  %903 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_treble_db1181 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %903, i32 0, i32 55
  %904 = load float, float* %adjust_treble_db1181, align 4
  %905 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %adjust_sfb21_db1182 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %905, i32 0, i32 56
  %906 = load float, float* %adjust_sfb21_db1182, align 4
  %add1183 = fadd float %906, %904
  store float %add1183, float* %adjust_sfb21_db1182, align 4
  %907 = bitcast [2 x [2 x float]]* %m1184 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %907, i8* align 16 bitcast ([2 x [2 x float]]* @__const.lame_init_params.m.3 to i8*), i32 16, i1 false)
  %908 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %908, i32 0, i32 5
  %909 = load float, float* %scale, align 4
  %arrayidx1185 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1186 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1185, i32 0, i32 0
  %910 = load float, float* %arrayidx1186, align 16
  %mul1187 = fmul float %910, %909
  store float %mul1187, float* %arrayidx1186, align 16
  %911 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale1188 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %911, i32 0, i32 5
  %912 = load float, float* %scale1188, align 4
  %arrayidx1189 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1190 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1189, i32 0, i32 1
  %913 = load float, float* %arrayidx1190, align 4
  %mul1191 = fmul float %913, %912
  store float %mul1191, float* %arrayidx1190, align 4
  %914 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale1192 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %914, i32 0, i32 5
  %915 = load float, float* %scale1192, align 4
  %arrayidx1193 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1194 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1193, i32 0, i32 0
  %916 = load float, float* %arrayidx1194, align 8
  %mul1195 = fmul float %916, %915
  store float %mul1195, float* %arrayidx1194, align 8
  %917 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale1196 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %917, i32 0, i32 5
  %918 = load float, float* %scale1196, align 4
  %arrayidx1197 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1198 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1197, i32 0, i32 1
  %919 = load float, float* %arrayidx1198, align 4
  %mul1199 = fmul float %919, %918
  store float %mul1199, float* %arrayidx1198, align 4
  %920 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %920, i32 0, i32 6
  %921 = load float, float* %scale_left, align 4
  %arrayidx1200 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1201 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1200, i32 0, i32 0
  %922 = load float, float* %arrayidx1201, align 16
  %mul1202 = fmul float %922, %921
  store float %mul1202, float* %arrayidx1201, align 16
  %923 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left1203 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %923, i32 0, i32 6
  %924 = load float, float* %scale_left1203, align 4
  %arrayidx1204 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1205 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1204, i32 0, i32 1
  %925 = load float, float* %arrayidx1205, align 4
  %mul1206 = fmul float %925, %924
  store float %mul1206, float* %arrayidx1205, align 4
  %926 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %926, i32 0, i32 7
  %927 = load float, float* %scale_right, align 4
  %arrayidx1207 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1208 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1207, i32 0, i32 0
  %928 = load float, float* %arrayidx1208, align 8
  %mul1209 = fmul float %928, %927
  store float %mul1209, float* %arrayidx1208, align 8
  %929 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right1210 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %929, i32 0, i32 7
  %930 = load float, float* %scale_right1210, align 4
  %arrayidx1211 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1212 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1211, i32 0, i32 1
  %931 = load float, float* %arrayidx1212, align 4
  %mul1213 = fmul float %931, %930
  store float %mul1213, float* %arrayidx1212, align 4
  %932 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_in1214 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %932, i32 0, i32 13
  %933 = load i32, i32* %channels_in1214, align 4
  %cmp1215 = icmp eq i32 %933, 2
  br i1 %cmp1215, label %land.lhs.true1217, label %if.end1242

land.lhs.true1217:                                ; preds = %if.end1178
  %934 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out1218 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %934, i32 0, i32 14
  %935 = load i32, i32* %channels_out1218, align 4
  %cmp1219 = icmp eq i32 %935, 1
  br i1 %cmp1219, label %if.then1221, label %if.end1242

if.then1221:                                      ; preds = %land.lhs.true1217
  %arrayidx1222 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1223 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1222, i32 0, i32 0
  %936 = load float, float* %arrayidx1223, align 16
  %arrayidx1224 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1225 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1224, i32 0, i32 0
  %937 = load float, float* %arrayidx1225, align 8
  %add1226 = fadd float %936, %937
  %mul1227 = fmul float 5.000000e-01, %add1226
  %arrayidx1228 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1229 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1228, i32 0, i32 0
  store float %mul1227, float* %arrayidx1229, align 16
  %arrayidx1230 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1231 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1230, i32 0, i32 1
  %938 = load float, float* %arrayidx1231, align 4
  %arrayidx1232 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1233 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1232, i32 0, i32 1
  %939 = load float, float* %arrayidx1233, align 4
  %add1234 = fadd float %938, %939
  %mul1235 = fmul float 5.000000e-01, %add1234
  %arrayidx1236 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1237 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1236, i32 0, i32 1
  store float %mul1235, float* %arrayidx1237, align 4
  %arrayidx1238 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1239 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1238, i32 0, i32 0
  store float 0.000000e+00, float* %arrayidx1239, align 8
  %arrayidx1240 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1241 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1240, i32 0, i32 1
  store float 0.000000e+00, float* %arrayidx1241, align 4
  br label %if.end1242

if.end1242:                                       ; preds = %if.then1221, %land.lhs.true1217, %if.end1178
  %arrayidx1243 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1244 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1243, i32 0, i32 0
  %940 = load float, float* %arrayidx1244, align 16
  %941 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %941, i32 0, i32 62
  %arrayidx1245 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform, i32 0, i32 0
  %arrayidx1246 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1245, i32 0, i32 0
  store float %940, float* %arrayidx1246, align 4
  %arrayidx1247 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 0
  %arrayidx1248 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1247, i32 0, i32 1
  %942 = load float, float* %arrayidx1248, align 4
  %943 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform1249 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %943, i32 0, i32 62
  %arrayidx1250 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform1249, i32 0, i32 0
  %arrayidx1251 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1250, i32 0, i32 1
  store float %942, float* %arrayidx1251, align 4
  %arrayidx1252 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1253 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1252, i32 0, i32 0
  %944 = load float, float* %arrayidx1253, align 8
  %945 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform1254 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %945, i32 0, i32 62
  %arrayidx1255 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform1254, i32 0, i32 1
  %arrayidx1256 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1255, i32 0, i32 0
  store float %944, float* %arrayidx1256, align 4
  %arrayidx1257 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m1184, i32 0, i32 1
  %arrayidx1258 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1257, i32 0, i32 1
  %946 = load float, float* %arrayidx1258, align 4
  %947 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform1259 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %947, i32 0, i32 62
  %arrayidx1260 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform1259, i32 0, i32 1
  %arrayidx1261 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx1260, i32 0, i32 1
  store float %946, float* %arrayidx1261, align 4
  %948 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc1262 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %948, i32 0, i32 11
  %frac_SpF = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc1262, i32 0, i32 6
  store i32 0, i32* %frac_SpF, align 8
  %949 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc1263 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %949, i32 0, i32 11
  %slot_lag = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc1263, i32 0, i32 7
  store i32 0, i32* %slot_lag, align 4
  %950 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr1264 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %950, i32 0, i32 22
  %951 = load i32, i32* %vbr1264, align 4
  %cmp1265 = icmp eq i32 %951, 0
  br i1 %cmp1265, label %if.then1267, label %if.end1278

if.then1267:                                      ; preds = %if.end1242
  %952 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version1268 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %952, i32 0, i32 0
  %953 = load i32, i32* %version1268, align 4
  %add1269 = add nsw i32 %953, 1
  %mul1270 = mul nsw i32 %add1269, 72000
  %954 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate1271 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %954, i32 0, i32 26
  %955 = load i32, i32* %avg_bitrate1271, align 4
  %mul1272 = mul nsw i32 %mul1270, %955
  %956 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out1273 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %956, i32 0, i32 12
  %957 = load i32, i32* %samplerate_out1273, align 4
  %rem = srem i32 %mul1272, %957
  %958 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc1274 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %958, i32 0, i32 11
  %frac_SpF1275 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc1274, i32 0, i32 6
  store i32 %rem, i32* %frac_SpF1275, align 8
  %959 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc1276 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %959, i32 0, i32 11
  %slot_lag1277 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc1276, i32 0, i32 7
  store i32 %rem, i32* %slot_lag1277, align 4
  br label %if.end1278

if.end1278:                                       ; preds = %if.then1267, %if.end1242
  %960 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1279 = call i32 @lame_init_bitstream(%struct.lame_global_struct* %960)
  %961 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @iteration_init(%struct.lame_internal_flags* %961)
  %962 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1280 = call i32 @psymodel_init(%struct.lame_global_struct* %962)
  %963 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %964 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO1281 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %964, i32 0, i32 31
  %965 = load i32, i32* %strict_ISO1281, align 4
  %call1282 = call i32 @get_max_frame_buffer_size_by_constraint(%struct.SessionConfig_t* %963, i32 %965)
  %966 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %buffer_constraint = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %966, i32 0, i32 33
  store i32 %call1282, i32* %buffer_constraint, align 4
  %967 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %findReplayGain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %967, i32 0, i32 15
  %968 = load i32, i32* %findReplayGain, align 4
  %969 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain1283 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %969, i32 0, i32 28
  store i32 %968, i32* %findReplayGain1283, align 4
  %970 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %decode_on_the_fly = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %970, i32 0, i32 16
  %971 = load i32, i32* %decode_on_the_fly, align 4
  %972 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %decode_on_the_fly1284 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %972, i32 0, i32 30
  store i32 %971, i32* %decode_on_the_fly1284, align 4
  %973 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %decode_on_the_fly1285 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %973, i32 0, i32 30
  %974 = load i32, i32* %decode_on_the_fly1285, align 4
  %tobool1286 = icmp ne i32 %974, 0
  br i1 %tobool1286, label %if.then1287, label %if.end1288

if.then1287:                                      ; preds = %if.end1278
  %975 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findPeakSample = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %975, i32 0, i32 29
  store i32 1, i32* %findPeakSample, align 4
  br label %if.end1288

if.end1288:                                       ; preds = %if.then1287, %if.end1278
  %976 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain1289 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %976, i32 0, i32 28
  %977 = load i32, i32* %findReplayGain1289, align 4
  %tobool1290 = icmp ne i32 %977, 0
  br i1 %tobool1290, label %if.then1291, label %if.end1299

if.then1291:                                      ; preds = %if.end1288
  %978 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %978, i32 0, i32 14
  %rgdata = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg, i32 0, i32 0
  %979 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata, align 4
  %980 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out1292 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %980, i32 0, i32 12
  %981 = load i32, i32* %samplerate_out1292, align 4
  %call1293 = call i32 @InitGainAnalysis(%struct.replaygain_data* %979, i32 %981)
  %cmp1294 = icmp eq i32 %call1293, 0
  br i1 %cmp1294, label %if.then1296, label %if.end1298

if.then1296:                                      ; preds = %if.then1291
  %982 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain1297 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %982, i32 0, i32 28
  store i32 0, i32* %findReplayGain1297, align 4
  br label %if.end1298

if.end1298:                                       ; preds = %if.then1296, %if.then1291
  br label %if.end1299

if.end1299:                                       ; preds = %if.end1298, %if.end1288
  %983 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %lame_init_params_successful1300 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %983, i32 0, i32 1
  store i32 1, i32* %lame_init_params_successful1300, align 4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end1299, %if.then20, %if.then13, %if.then8, %if.then5, %if.then1, %if.then
  %984 = load i32, i32* %retval, align 4
  ret i32 %984
}

declare i32 @SmpFrqIndex(i32, i32*) #1

declare i32 @has_3DNow() #1

declare i32 @has_MMX() #1

declare i32 @has_SSE() #1

declare i32 @has_SSE2() #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #2

declare i32 @map2MP3Frequency(i32) #1

declare i32 @FindNearestBitrate(i32, i32, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal void @optimum_bandwidth(double* %lowerlimit, double* %upperlimit, i32 %bitrate) #0 {
entry:
  %lowerlimit.addr = alloca double*, align 4
  %upperlimit.addr = alloca double*, align 4
  %bitrate.addr = alloca i32, align 4
  %table_index = alloca i32, align 4
  %freq_map = alloca [17 x %struct.band_pass_t], align 16
  store double* %lowerlimit, double** %lowerlimit.addr, align 4
  store double* %upperlimit, double** %upperlimit.addr, align 4
  store i32 %bitrate, i32* %bitrate.addr, align 4
  %0 = bitcast [17 x %struct.band_pass_t]* %freq_map to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %0, i8* align 16 bitcast ([17 x %struct.band_pass_t]* @__const.optimum_bandwidth.freq_map to i8*), i32 136, i1 false)
  %1 = load i32, i32* %bitrate.addr, align 4
  %conv = trunc i32 %1 to i16
  %call = call i32 @nearestBitrateFullIndex(i16 zeroext %conv)
  store i32 %call, i32* %table_index, align 4
  %2 = load i32, i32* %table_index, align 4
  %arrayidx = getelementptr inbounds [17 x %struct.band_pass_t], [17 x %struct.band_pass_t]* %freq_map, i32 0, i32 %2
  %bitrate1 = getelementptr inbounds %struct.band_pass_t, %struct.band_pass_t* %arrayidx, i32 0, i32 0
  %3 = load i32, i32* %bitrate1, align 8
  %4 = load i32, i32* %table_index, align 4
  %arrayidx2 = getelementptr inbounds [17 x %struct.band_pass_t], [17 x %struct.band_pass_t]* %freq_map, i32 0, i32 %4
  %lowpass = getelementptr inbounds %struct.band_pass_t, %struct.band_pass_t* %arrayidx2, i32 0, i32 1
  %5 = load i32, i32* %lowpass, align 4
  %conv3 = sitofp i32 %5 to double
  %6 = load double*, double** %lowerlimit.addr, align 4
  store double %conv3, double* %6, align 8
  %7 = load double*, double** %upperlimit.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal double @linear_int(double %a, double %b, double %m) #0 {
entry:
  %a.addr = alloca double, align 8
  %b.addr = alloca double, align 8
  %m.addr = alloca double, align 8
  store double %a, double* %a.addr, align 8
  store double %b, double* %b.addr, align 8
  store double %m, double* %m.addr, align 8
  %0 = load double, double* %a.addr, align 8
  %1 = load double, double* %m.addr, align 8
  %2 = load double, double* %b.addr, align 8
  %3 = load double, double* %a.addr, align 8
  %sub = fsub double %2, %3
  %mul = fmul double %1, %sub
  %add = fadd double %0, %mul
  ret double %add
}

; Function Attrs: noinline nounwind optnone
define internal i32 @optimum_samplefreq(i32 %lowpassfreq, i32 %input_samplefreq) #0 {
entry:
  %retval = alloca i32, align 4
  %lowpassfreq.addr = alloca i32, align 4
  %input_samplefreq.addr = alloca i32, align 4
  %suggested_samplefreq = alloca i32, align 4
  store i32 %lowpassfreq, i32* %lowpassfreq.addr, align 4
  store i32 %input_samplefreq, i32* %input_samplefreq.addr, align 4
  store i32 44100, i32* %suggested_samplefreq, align 4
  %0 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp = icmp sge i32 %0, 48000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 48000, i32* %suggested_samplefreq, align 4
  br label %if.end31

if.else:                                          ; preds = %entry
  %1 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp1 = icmp sge i32 %1, 44100
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  store i32 44100, i32* %suggested_samplefreq, align 4
  br label %if.end30

if.else3:                                         ; preds = %if.else
  %2 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp4 = icmp sge i32 %2, 32000
  br i1 %cmp4, label %if.then5, label %if.else6

if.then5:                                         ; preds = %if.else3
  store i32 32000, i32* %suggested_samplefreq, align 4
  br label %if.end29

if.else6:                                         ; preds = %if.else3
  %3 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp7 = icmp sge i32 %3, 24000
  br i1 %cmp7, label %if.then8, label %if.else9

if.then8:                                         ; preds = %if.else6
  store i32 24000, i32* %suggested_samplefreq, align 4
  br label %if.end28

if.else9:                                         ; preds = %if.else6
  %4 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp10 = icmp sge i32 %4, 22050
  br i1 %cmp10, label %if.then11, label %if.else12

if.then11:                                        ; preds = %if.else9
  store i32 22050, i32* %suggested_samplefreq, align 4
  br label %if.end27

if.else12:                                        ; preds = %if.else9
  %5 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp13 = icmp sge i32 %5, 16000
  br i1 %cmp13, label %if.then14, label %if.else15

if.then14:                                        ; preds = %if.else12
  store i32 16000, i32* %suggested_samplefreq, align 4
  br label %if.end26

if.else15:                                        ; preds = %if.else12
  %6 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp16 = icmp sge i32 %6, 12000
  br i1 %cmp16, label %if.then17, label %if.else18

if.then17:                                        ; preds = %if.else15
  store i32 12000, i32* %suggested_samplefreq, align 4
  br label %if.end25

if.else18:                                        ; preds = %if.else15
  %7 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp19 = icmp sge i32 %7, 11025
  br i1 %cmp19, label %if.then20, label %if.else21

if.then20:                                        ; preds = %if.else18
  store i32 11025, i32* %suggested_samplefreq, align 4
  br label %if.end24

if.else21:                                        ; preds = %if.else18
  %8 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp22 = icmp sge i32 %8, 8000
  br i1 %cmp22, label %if.then23, label %if.end

if.then23:                                        ; preds = %if.else21
  store i32 8000, i32* %suggested_samplefreq, align 4
  br label %if.end

if.end:                                           ; preds = %if.then23, %if.else21
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then20
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then17
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %if.then14
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %if.then11
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then8
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then5
  br label %if.end30

if.end30:                                         ; preds = %if.end29, %if.then2
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then
  %9 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp32 = icmp eq i32 %9, -1
  br i1 %cmp32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.end31
  %10 = load i32, i32* %suggested_samplefreq, align 4
  store i32 %10, i32* %retval, align 4
  br label %return

if.end34:                                         ; preds = %if.end31
  %11 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp35 = icmp sle i32 %11, 15960
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %if.end34
  store i32 44100, i32* %suggested_samplefreq, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %if.end34
  %12 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp38 = icmp sle i32 %12, 15250
  br i1 %cmp38, label %if.then39, label %if.end40

if.then39:                                        ; preds = %if.end37
  store i32 32000, i32* %suggested_samplefreq, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %if.end37
  %13 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp41 = icmp sle i32 %13, 11220
  br i1 %cmp41, label %if.then42, label %if.end43

if.then42:                                        ; preds = %if.end40
  store i32 24000, i32* %suggested_samplefreq, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then42, %if.end40
  %14 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp44 = icmp sle i32 %14, 9970
  br i1 %cmp44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.end43
  store i32 22050, i32* %suggested_samplefreq, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.then45, %if.end43
  %15 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp47 = icmp sle i32 %15, 7230
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %if.end46
  store i32 16000, i32* %suggested_samplefreq, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then48, %if.end46
  %16 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp50 = icmp sle i32 %16, 5420
  br i1 %cmp50, label %if.then51, label %if.end52

if.then51:                                        ; preds = %if.end49
  store i32 12000, i32* %suggested_samplefreq, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.then51, %if.end49
  %17 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp53 = icmp sle i32 %17, 4510
  br i1 %cmp53, label %if.then54, label %if.end55

if.then54:                                        ; preds = %if.end52
  store i32 11025, i32* %suggested_samplefreq, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.then54, %if.end52
  %18 = load i32, i32* %lowpassfreq.addr, align 4
  %cmp56 = icmp sle i32 %18, 3970
  br i1 %cmp56, label %if.then57, label %if.end58

if.then57:                                        ; preds = %if.end55
  store i32 8000, i32* %suggested_samplefreq, align 4
  br label %if.end58

if.end58:                                         ; preds = %if.then57, %if.end55
  %19 = load i32, i32* %input_samplefreq.addr, align 4
  %20 = load i32, i32* %suggested_samplefreq, align 4
  %cmp59 = icmp slt i32 %19, %20
  br i1 %cmp59, label %if.then60, label %if.end85

if.then60:                                        ; preds = %if.end58
  %21 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp61 = icmp sgt i32 %21, 44100
  br i1 %cmp61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %if.then60
  store i32 48000, i32* %retval, align 4
  br label %return

if.end63:                                         ; preds = %if.then60
  %22 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp64 = icmp sgt i32 %22, 32000
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end63
  store i32 44100, i32* %retval, align 4
  br label %return

if.end66:                                         ; preds = %if.end63
  %23 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp67 = icmp sgt i32 %23, 24000
  br i1 %cmp67, label %if.then68, label %if.end69

if.then68:                                        ; preds = %if.end66
  store i32 32000, i32* %retval, align 4
  br label %return

if.end69:                                         ; preds = %if.end66
  %24 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp70 = icmp sgt i32 %24, 22050
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %if.end69
  store i32 24000, i32* %retval, align 4
  br label %return

if.end72:                                         ; preds = %if.end69
  %25 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp73 = icmp sgt i32 %25, 16000
  br i1 %cmp73, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.end72
  store i32 22050, i32* %retval, align 4
  br label %return

if.end75:                                         ; preds = %if.end72
  %26 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp76 = icmp sgt i32 %26, 12000
  br i1 %cmp76, label %if.then77, label %if.end78

if.then77:                                        ; preds = %if.end75
  store i32 16000, i32* %retval, align 4
  br label %return

if.end78:                                         ; preds = %if.end75
  %27 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp79 = icmp sgt i32 %27, 11025
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %if.end78
  store i32 12000, i32* %retval, align 4
  br label %return

if.end81:                                         ; preds = %if.end78
  %28 = load i32, i32* %input_samplefreq.addr, align 4
  %cmp82 = icmp sgt i32 %28, 8000
  br i1 %cmp82, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end81
  store i32 11025, i32* %retval, align 4
  br label %return

if.end84:                                         ; preds = %if.end81
  store i32 8000, i32* %retval, align 4
  br label %return

if.end85:                                         ; preds = %if.end58
  %29 = load i32, i32* %suggested_samplefreq, align 4
  store i32 %29, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end85, %if.end84, %if.then83, %if.then80, %if.then77, %if.then74, %if.then71, %if.then68, %if.then65, %if.then62, %if.then33
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: noinline nounwind optnone
define internal void @lame_init_params_ppflt(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %band = alloca i32, align 4
  %maxband = alloca i32, align 4
  %minband = alloca i32, align 4
  %freq = alloca float, align 4
  %lowpass_band = alloca i32, align 4
  %highpass_band = alloca i32, align 4
  %fc1 = alloca float, align 4
  %fc2 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 32, i32* %lowpass_band, align 4
  store i32 -1, i32* %highpass_band, align 4
  %1 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %1, i32 0, i32 58
  %2 = load float, float* %lowpass1, align 4
  %cmp = fcmp ogt float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  store i32 999, i32* %minband, align 4
  store i32 0, i32* %band, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %band, align 4
  %cmp2 = icmp sle i32 %3, 31
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %band, align 4
  %conv = sitofp i32 %4 to double
  %div = fdiv double %conv, 3.100000e+01
  %conv3 = fptrunc double %div to float
  store float %conv3, float* %freq, align 4
  %5 = load float, float* %freq, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 59
  %7 = load float, float* %lowpass2, align 4
  %cmp4 = fcmp oge float %5, %7
  br i1 %cmp4, label %if.then6, label %if.end

if.then6:                                         ; preds = %for.body
  %8 = load i32, i32* %lowpass_band, align 4
  %9 = load i32, i32* %band, align 4
  %cmp7 = icmp slt i32 %8, %9
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then6
  %10 = load i32, i32* %lowpass_band, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then6
  %11 = load i32, i32* %band, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %10, %cond.true ], [ %11, %cond.false ]
  store i32 %cond, i32* %lowpass_band, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %for.body
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass19 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 58
  %13 = load float, float* %lowpass19, align 4
  %14 = load float, float* %freq, align 4
  %cmp10 = fcmp olt float %13, %14
  br i1 %cmp10, label %land.lhs.true, label %if.end22

land.lhs.true:                                    ; preds = %if.end
  %15 = load float, float* %freq, align 4
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass212 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 59
  %17 = load float, float* %lowpass212, align 4
  %cmp13 = fcmp olt float %15, %17
  br i1 %cmp13, label %if.then15, label %if.end22

if.then15:                                        ; preds = %land.lhs.true
  %18 = load i32, i32* %minband, align 4
  %19 = load i32, i32* %band, align 4
  %cmp16 = icmp slt i32 %18, %19
  br i1 %cmp16, label %cond.true18, label %cond.false19

cond.true18:                                      ; preds = %if.then15
  %20 = load i32, i32* %minband, align 4
  br label %cond.end20

cond.false19:                                     ; preds = %if.then15
  %21 = load i32, i32* %band, align 4
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false19, %cond.true18
  %cond21 = phi i32 [ %20, %cond.true18 ], [ %21, %cond.false19 ]
  store i32 %cond21, i32* %minband, align 4
  br label %if.end22

if.end22:                                         ; preds = %cond.end20, %land.lhs.true, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %22 = load i32, i32* %band, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %band, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load i32, i32* %minband, align 4
  %cmp23 = icmp eq i32 %23, 999
  br i1 %cmp23, label %if.then25, label %if.else

if.then25:                                        ; preds = %for.end
  %24 = load i32, i32* %lowpass_band, align 4
  %conv26 = sitofp i32 %24 to double
  %sub = fsub double %conv26, 7.500000e-01
  %div27 = fdiv double %sub, 3.100000e+01
  %conv28 = fptrunc double %div27 to float
  %25 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass129 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %25, i32 0, i32 58
  store float %conv28, float* %lowpass129, align 4
  br label %if.end35

if.else:                                          ; preds = %for.end
  %26 = load i32, i32* %minband, align 4
  %conv30 = sitofp i32 %26 to double
  %sub31 = fsub double %conv30, 7.500000e-01
  %div32 = fdiv double %sub31, 3.100000e+01
  %conv33 = fptrunc double %div32 to float
  %27 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass134 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %27, i32 0, i32 58
  store float %conv33, float* %lowpass134, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.else, %if.then25
  %28 = load i32, i32* %lowpass_band, align 4
  %conv36 = sitofp i32 %28 to double
  %div37 = fdiv double %conv36, 3.100000e+01
  %conv38 = fptrunc double %div37 to float
  %29 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass239 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %29, i32 0, i32 59
  store float %conv38, float* %lowpass239, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.end35, %entry
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 61
  %31 = load float, float* %highpass2, align 4
  %cmp41 = fcmp ogt float %31, 0.000000e+00
  br i1 %cmp41, label %if.then43, label %if.end51

if.then43:                                        ; preds = %if.end40
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass244 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 61
  %33 = load float, float* %highpass244, align 4
  %conv45 = fpext float %33 to double
  %cmp46 = fcmp olt double %conv45, 0x3F964BF964BF964C
  br i1 %cmp46, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.then43
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 60
  store float 0.000000e+00, float* %highpass1, align 4
  %35 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass249 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %35, i32 0, i32 61
  store float 0.000000e+00, float* %highpass249, align 4
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %36, i8* getelementptr inbounds ([66 x i8], [66 x i8]* @.str.91, i32 0, i32 0))
  br label %if.end50

if.end50:                                         ; preds = %if.then48, %if.then43
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %if.end40
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass252 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %37, i32 0, i32 61
  %38 = load float, float* %highpass252, align 4
  %cmp53 = fcmp ogt float %38, 0.000000e+00
  br i1 %cmp53, label %if.then55, label %if.end110

if.then55:                                        ; preds = %if.end51
  store i32 -1, i32* %maxband, align 4
  store i32 0, i32* %band, align 4
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc89, %if.then55
  %39 = load i32, i32* %band, align 4
  %cmp57 = icmp sle i32 %39, 31
  br i1 %cmp57, label %for.body59, label %for.end91

for.body59:                                       ; preds = %for.cond56
  %40 = load i32, i32* %band, align 4
  %conv60 = sitofp i32 %40 to double
  %div61 = fdiv double %conv60, 3.100000e+01
  %conv62 = fptrunc double %div61 to float
  store float %conv62, float* %freq, align 4
  %41 = load float, float* %freq, align 4
  %42 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass163 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %42, i32 0, i32 60
  %43 = load float, float* %highpass163, align 4
  %cmp64 = fcmp ole float %41, %43
  br i1 %cmp64, label %if.then66, label %if.end73

if.then66:                                        ; preds = %for.body59
  %44 = load i32, i32* %highpass_band, align 4
  %45 = load i32, i32* %band, align 4
  %cmp67 = icmp sgt i32 %44, %45
  br i1 %cmp67, label %cond.true69, label %cond.false70

cond.true69:                                      ; preds = %if.then66
  %46 = load i32, i32* %highpass_band, align 4
  br label %cond.end71

cond.false70:                                     ; preds = %if.then66
  %47 = load i32, i32* %band, align 4
  br label %cond.end71

cond.end71:                                       ; preds = %cond.false70, %cond.true69
  %cond72 = phi i32 [ %46, %cond.true69 ], [ %47, %cond.false70 ]
  store i32 %cond72, i32* %highpass_band, align 4
  br label %if.end73

if.end73:                                         ; preds = %cond.end71, %for.body59
  %48 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass174 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %48, i32 0, i32 60
  %49 = load float, float* %highpass174, align 4
  %50 = load float, float* %freq, align 4
  %cmp75 = fcmp olt float %49, %50
  br i1 %cmp75, label %land.lhs.true77, label %if.end88

land.lhs.true77:                                  ; preds = %if.end73
  %51 = load float, float* %freq, align 4
  %52 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass278 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %52, i32 0, i32 61
  %53 = load float, float* %highpass278, align 4
  %cmp79 = fcmp olt float %51, %53
  br i1 %cmp79, label %if.then81, label %if.end88

if.then81:                                        ; preds = %land.lhs.true77
  %54 = load i32, i32* %maxband, align 4
  %55 = load i32, i32* %band, align 4
  %cmp82 = icmp sgt i32 %54, %55
  br i1 %cmp82, label %cond.true84, label %cond.false85

cond.true84:                                      ; preds = %if.then81
  %56 = load i32, i32* %maxband, align 4
  br label %cond.end86

cond.false85:                                     ; preds = %if.then81
  %57 = load i32, i32* %band, align 4
  br label %cond.end86

cond.end86:                                       ; preds = %cond.false85, %cond.true84
  %cond87 = phi i32 [ %56, %cond.true84 ], [ %57, %cond.false85 ]
  store i32 %cond87, i32* %maxband, align 4
  br label %if.end88

if.end88:                                         ; preds = %cond.end86, %land.lhs.true77, %if.end73
  br label %for.inc89

for.inc89:                                        ; preds = %if.end88
  %58 = load i32, i32* %band, align 4
  %inc90 = add nsw i32 %58, 1
  store i32 %inc90, i32* %band, align 4
  br label %for.cond56

for.end91:                                        ; preds = %for.cond56
  %59 = load i32, i32* %highpass_band, align 4
  %conv92 = sitofp i32 %59 to double
  %div93 = fdiv double %conv92, 3.100000e+01
  %conv94 = fptrunc double %div93 to float
  %60 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass195 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %60, i32 0, i32 60
  store float %conv94, float* %highpass195, align 4
  %61 = load i32, i32* %maxband, align 4
  %cmp96 = icmp eq i32 %61, -1
  br i1 %cmp96, label %if.then98, label %if.else103

if.then98:                                        ; preds = %for.end91
  %62 = load i32, i32* %highpass_band, align 4
  %conv99 = sitofp i32 %62 to double
  %add = fadd double %conv99, 7.500000e-01
  %div100 = fdiv double %add, 3.100000e+01
  %conv101 = fptrunc double %div100 to float
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2102 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 61
  store float %conv101, float* %highpass2102, align 4
  br label %if.end109

if.else103:                                       ; preds = %for.end91
  %64 = load i32, i32* %maxband, align 4
  %conv104 = sitofp i32 %64 to double
  %add105 = fadd double %conv104, 7.500000e-01
  %div106 = fdiv double %add105, 3.100000e+01
  %conv107 = fptrunc double %div106 to float
  %65 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2108 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %65, i32 0, i32 61
  store float %conv107, float* %highpass2108, align 4
  br label %if.end109

if.end109:                                        ; preds = %if.else103, %if.then98
  br label %if.end110

if.end110:                                        ; preds = %if.end109, %if.end51
  store i32 0, i32* %band, align 4
  br label %for.cond111

for.cond111:                                      ; preds = %for.inc152, %if.end110
  %66 = load i32, i32* %band, align 4
  %cmp112 = icmp slt i32 %66, 32
  br i1 %cmp112, label %for.body114, label %for.end154

for.body114:                                      ; preds = %for.cond111
  %67 = load i32, i32* %band, align 4
  %conv115 = sitofp i32 %67 to float
  %div116 = fdiv float %conv115, 3.100000e+01
  store float %div116, float* %freq, align 4
  %68 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2117 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %68, i32 0, i32 61
  %69 = load float, float* %highpass2117, align 4
  %70 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1118 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %70, i32 0, i32 60
  %71 = load float, float* %highpass1118, align 4
  %cmp119 = fcmp ogt float %69, %71
  br i1 %cmp119, label %if.then121, label %if.else132

if.then121:                                       ; preds = %for.body114
  %72 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2122 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %72, i32 0, i32 61
  %73 = load float, float* %highpass2122, align 4
  %74 = load float, float* %freq, align 4
  %sub123 = fsub float %73, %74
  %conv124 = fpext float %sub123 to double
  %75 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2125 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %75, i32 0, i32 61
  %76 = load float, float* %highpass2125, align 4
  %77 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1126 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %77, i32 0, i32 60
  %78 = load float, float* %highpass1126, align 4
  %sub127 = fsub float %76, %78
  %conv128 = fpext float %sub127 to double
  %add129 = fadd double %conv128, 0x3BC79CA10C924223
  %div130 = fdiv double %conv124, %add129
  %conv131 = fptrunc double %div130 to float
  %call = call float @filter_coef(float %conv131)
  store float %call, float* %fc1, align 4
  br label %if.end133

if.else132:                                       ; preds = %for.body114
  store float 1.000000e+00, float* %fc1, align 4
  br label %if.end133

if.end133:                                        ; preds = %if.else132, %if.then121
  %79 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2134 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %79, i32 0, i32 59
  %80 = load float, float* %lowpass2134, align 4
  %81 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1135 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %81, i32 0, i32 58
  %82 = load float, float* %lowpass1135, align 4
  %cmp136 = fcmp ogt float %80, %82
  br i1 %cmp136, label %if.then138, label %if.else150

if.then138:                                       ; preds = %if.end133
  %83 = load float, float* %freq, align 4
  %84 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1139 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %84, i32 0, i32 58
  %85 = load float, float* %lowpass1139, align 4
  %sub140 = fsub float %83, %85
  %conv141 = fpext float %sub140 to double
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2142 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 59
  %87 = load float, float* %lowpass2142, align 4
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1143 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 58
  %89 = load float, float* %lowpass1143, align 4
  %sub144 = fsub float %87, %89
  %conv145 = fpext float %sub144 to double
  %add146 = fadd double %conv145, 0x3BC79CA10C924223
  %div147 = fdiv double %conv141, %add146
  %conv148 = fptrunc double %div147 to float
  %call149 = call float @filter_coef(float %conv148)
  store float %call149, float* %fc2, align 4
  br label %if.end151

if.else150:                                       ; preds = %if.end133
  store float 1.000000e+00, float* %fc2, align 4
  br label %if.end151

if.end151:                                        ; preds = %if.else150, %if.then138
  %90 = load float, float* %fc1, align 4
  %91 = load float, float* %fc2, align 4
  %mul = fmul float %90, %91
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %92, i32 0, i32 11
  %amp_filter = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 1
  %93 = load i32, i32* %band, align 4
  %arrayidx = getelementptr inbounds [32 x float], [32 x float]* %amp_filter, i32 0, i32 %93
  store float %mul, float* %arrayidx, align 4
  br label %for.inc152

for.inc152:                                       ; preds = %if.end151
  %94 = load i32, i32* %band, align 4
  %inc153 = add nsw i32 %94, 1
  store i32 %inc153, i32* %band, align 4
  br label %for.cond111

for.end154:                                       ; preds = %for.cond111
  ret void
}

declare i32 @BitrateIndex(i32, i32, i32) #1

declare void @init_bit_stream_w(%struct.lame_internal_flags*) #1

declare i32 @apply_preset(%struct.lame_global_struct*, i32, i32) #1

declare i32 @lame_set_VBR_mean_bitrate_kbps(%struct.lame_global_struct*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @lame_init_qval(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 11
  %4 = load i32, i32* %quality, align 4
  switch i32 %4, label %sw.default [
    i32 9, label %sw.bb
    i32 8, label %sw.bb2
    i32 7, label %sw.bb4
    i32 6, label %sw.bb13
    i32 5, label %sw.bb27
    i32 4, label %sw.bb42
    i32 3, label %sw.bb57
    i32 2, label %sw.bb72
    i32 1, label %sw.bb92
    i32 0, label %sw.bb114
  ]

sw.default:                                       ; preds = %entry
  br label %sw.bb

sw.bb:                                            ; preds = %entry, %sw.default
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 3
  store i32 0, i32* %noise_shaping, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 6
  store i32 0, i32* %noise_shaping_amp, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 7
  store i32 0, i32* %noise_shaping_stop, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 5
  store i32 0, i32* %use_best_huffman, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 8
  store i32 0, i32* %full_outer_loop, align 4
  br label %sw.epilog

sw.bb2:                                           ; preds = %entry
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %10, i32 0, i32 11
  store i32 7, i32* %quality3, align 4
  br label %sw.bb4

sw.bb4:                                           ; preds = %entry, %sw.bb2
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 3
  store i32 0, i32* %noise_shaping5, align 4
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp6 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 6
  store i32 0, i32* %noise_shaping_amp6, align 4
  %13 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %13, i32 0, i32 7
  store i32 0, i32* %noise_shaping_stop7, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman8 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 5
  store i32 0, i32* %use_best_huffman8, align 4
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop9 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 8
  store i32 0, i32* %full_outer_loop9, align 4
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 22
  %17 = load i32, i32* %vbr, align 4
  %cmp = icmp eq i32 %17, 1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %sw.bb4
  %18 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr10 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %18, i32 0, i32 22
  %19 = load i32, i32* %vbr10, align 4
  %cmp11 = icmp eq i32 %19, 4
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %sw.bb4
  %20 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop12 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %20, i32 0, i32 8
  store i32 -1, i32* %full_outer_loop12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  br label %sw.epilog

sw.bb13:                                          ; preds = %entry
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping14 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 3
  %22 = load i32, i32* %noise_shaping14, align 4
  %cmp15 = icmp eq i32 %22, 0
  br i1 %cmp15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %sw.bb13
  %23 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping17 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %23, i32 0, i32 3
  store i32 1, i32* %noise_shaping17, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %sw.bb13
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp19 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 6
  store i32 0, i32* %noise_shaping_amp19, align 4
  %25 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop20 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %25, i32 0, i32 7
  store i32 0, i32* %noise_shaping_stop20, align 4
  %26 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %26, i32 0, i32 4
  %27 = load i32, i32* %subblock_gain, align 4
  %cmp21 = icmp eq i32 %27, -1
  br i1 %cmp21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.end18
  %28 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain23 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %28, i32 0, i32 4
  store i32 1, i32* %subblock_gain23, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.end18
  %29 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman25 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %29, i32 0, i32 5
  store i32 0, i32* %use_best_huffman25, align 4
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop26 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 8
  store i32 0, i32* %full_outer_loop26, align 4
  br label %sw.epilog

sw.bb27:                                          ; preds = %entry
  %31 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping28 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %31, i32 0, i32 3
  %32 = load i32, i32* %noise_shaping28, align 4
  %cmp29 = icmp eq i32 %32, 0
  br i1 %cmp29, label %if.then30, label %if.end32

if.then30:                                        ; preds = %sw.bb27
  %33 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping31 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %33, i32 0, i32 3
  store i32 1, i32* %noise_shaping31, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then30, %sw.bb27
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp33 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 6
  store i32 0, i32* %noise_shaping_amp33, align 4
  %35 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop34 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %35, i32 0, i32 7
  store i32 0, i32* %noise_shaping_stop34, align 4
  %36 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain35 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %36, i32 0, i32 4
  %37 = load i32, i32* %subblock_gain35, align 4
  %cmp36 = icmp eq i32 %37, -1
  br i1 %cmp36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end32
  %38 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain38 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %38, i32 0, i32 4
  store i32 1, i32* %subblock_gain38, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end32
  %39 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman40 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %39, i32 0, i32 5
  store i32 0, i32* %use_best_huffman40, align 4
  %40 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop41 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %40, i32 0, i32 8
  store i32 0, i32* %full_outer_loop41, align 4
  br label %sw.epilog

sw.bb42:                                          ; preds = %entry
  %41 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping43 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %41, i32 0, i32 3
  %42 = load i32, i32* %noise_shaping43, align 4
  %cmp44 = icmp eq i32 %42, 0
  br i1 %cmp44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %sw.bb42
  %43 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping46 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %43, i32 0, i32 3
  store i32 1, i32* %noise_shaping46, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then45, %sw.bb42
  %44 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp48 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %44, i32 0, i32 6
  store i32 0, i32* %noise_shaping_amp48, align 4
  %45 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop49 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %45, i32 0, i32 7
  store i32 0, i32* %noise_shaping_stop49, align 4
  %46 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain50 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %46, i32 0, i32 4
  %47 = load i32, i32* %subblock_gain50, align 4
  %cmp51 = icmp eq i32 %47, -1
  br i1 %cmp51, label %if.then52, label %if.end54

if.then52:                                        ; preds = %if.end47
  %48 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain53 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %48, i32 0, i32 4
  store i32 1, i32* %subblock_gain53, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.then52, %if.end47
  %49 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman55 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %49, i32 0, i32 5
  store i32 1, i32* %use_best_huffman55, align 4
  %50 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop56 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %50, i32 0, i32 8
  store i32 0, i32* %full_outer_loop56, align 4
  br label %sw.epilog

sw.bb57:                                          ; preds = %entry
  %51 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping58 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %51, i32 0, i32 3
  %52 = load i32, i32* %noise_shaping58, align 4
  %cmp59 = icmp eq i32 %52, 0
  br i1 %cmp59, label %if.then60, label %if.end62

if.then60:                                        ; preds = %sw.bb57
  %53 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping61 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %53, i32 0, i32 3
  store i32 1, i32* %noise_shaping61, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.then60, %sw.bb57
  %54 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp63 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %54, i32 0, i32 6
  store i32 1, i32* %noise_shaping_amp63, align 4
  %55 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop64 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %55, i32 0, i32 7
  store i32 1, i32* %noise_shaping_stop64, align 4
  %56 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain65 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %56, i32 0, i32 4
  %57 = load i32, i32* %subblock_gain65, align 4
  %cmp66 = icmp eq i32 %57, -1
  br i1 %cmp66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.end62
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain68 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 4
  store i32 1, i32* %subblock_gain68, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.end62
  %59 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman70 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %59, i32 0, i32 5
  store i32 1, i32* %use_best_huffman70, align 4
  %60 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop71 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %60, i32 0, i32 8
  store i32 0, i32* %full_outer_loop71, align 4
  br label %sw.epilog

sw.bb72:                                          ; preds = %entry
  %61 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping73 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %61, i32 0, i32 3
  %62 = load i32, i32* %noise_shaping73, align 4
  %cmp74 = icmp eq i32 %62, 0
  br i1 %cmp74, label %if.then75, label %if.end77

if.then75:                                        ; preds = %sw.bb72
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping76 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 3
  store i32 1, i32* %noise_shaping76, align 4
  br label %if.end77

if.end77:                                         ; preds = %if.then75, %sw.bb72
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %64, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %65 = load i32, i32* %substep_shaping, align 8
  %cmp78 = icmp eq i32 %65, 0
  br i1 %cmp78, label %if.then79, label %if.end82

if.then79:                                        ; preds = %if.end77
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt80 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 13
  %substep_shaping81 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt80, i32 0, i32 9
  store i32 2, i32* %substep_shaping81, align 8
  br label %if.end82

if.end82:                                         ; preds = %if.then79, %if.end77
  %67 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp83 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %67, i32 0, i32 6
  store i32 1, i32* %noise_shaping_amp83, align 4
  %68 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop84 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %68, i32 0, i32 7
  store i32 1, i32* %noise_shaping_stop84, align 4
  %69 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain85 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %69, i32 0, i32 4
  %70 = load i32, i32* %subblock_gain85, align 4
  %cmp86 = icmp eq i32 %70, -1
  br i1 %cmp86, label %if.then87, label %if.end89

if.then87:                                        ; preds = %if.end82
  %71 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain88 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %71, i32 0, i32 4
  store i32 1, i32* %subblock_gain88, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then87, %if.end82
  %72 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman90 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %72, i32 0, i32 5
  store i32 1, i32* %use_best_huffman90, align 4
  %73 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop91 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %73, i32 0, i32 8
  store i32 0, i32* %full_outer_loop91, align 4
  br label %sw.epilog

sw.bb92:                                          ; preds = %entry
  %74 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping93 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %74, i32 0, i32 3
  %75 = load i32, i32* %noise_shaping93, align 4
  %cmp94 = icmp eq i32 %75, 0
  br i1 %cmp94, label %if.then95, label %if.end97

if.then95:                                        ; preds = %sw.bb92
  %76 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping96 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %76, i32 0, i32 3
  store i32 1, i32* %noise_shaping96, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then95, %sw.bb92
  %77 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt98 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %77, i32 0, i32 13
  %substep_shaping99 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt98, i32 0, i32 9
  %78 = load i32, i32* %substep_shaping99, align 8
  %cmp100 = icmp eq i32 %78, 0
  br i1 %cmp100, label %if.then101, label %if.end104

if.then101:                                       ; preds = %if.end97
  %79 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt102 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %79, i32 0, i32 13
  %substep_shaping103 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt102, i32 0, i32 9
  store i32 2, i32* %substep_shaping103, align 8
  br label %if.end104

if.end104:                                        ; preds = %if.then101, %if.end97
  %80 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp105 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %80, i32 0, i32 6
  store i32 2, i32* %noise_shaping_amp105, align 4
  %81 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop106 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %81, i32 0, i32 7
  store i32 1, i32* %noise_shaping_stop106, align 4
  %82 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain107 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %82, i32 0, i32 4
  %83 = load i32, i32* %subblock_gain107, align 4
  %cmp108 = icmp eq i32 %83, -1
  br i1 %cmp108, label %if.then109, label %if.end111

if.then109:                                       ; preds = %if.end104
  %84 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain110 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %84, i32 0, i32 4
  store i32 1, i32* %subblock_gain110, align 4
  br label %if.end111

if.end111:                                        ; preds = %if.then109, %if.end104
  %85 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman112 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %85, i32 0, i32 5
  store i32 1, i32* %use_best_huffman112, align 4
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop113 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 8
  store i32 0, i32* %full_outer_loop113, align 4
  br label %sw.epilog

sw.bb114:                                         ; preds = %entry
  %87 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping115 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %87, i32 0, i32 3
  %88 = load i32, i32* %noise_shaping115, align 4
  %cmp116 = icmp eq i32 %88, 0
  br i1 %cmp116, label %if.then117, label %if.end119

if.then117:                                       ; preds = %sw.bb114
  %89 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping118 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %89, i32 0, i32 3
  store i32 1, i32* %noise_shaping118, align 4
  br label %if.end119

if.end119:                                        ; preds = %if.then117, %sw.bb114
  %90 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt120 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %90, i32 0, i32 13
  %substep_shaping121 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt120, i32 0, i32 9
  %91 = load i32, i32* %substep_shaping121, align 8
  %cmp122 = icmp eq i32 %91, 0
  br i1 %cmp122, label %if.then123, label %if.end126

if.then123:                                       ; preds = %if.end119
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt124 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %92, i32 0, i32 13
  %substep_shaping125 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt124, i32 0, i32 9
  store i32 2, i32* %substep_shaping125, align 8
  br label %if.end126

if.end126:                                        ; preds = %if.then123, %if.end119
  %93 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp127 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %93, i32 0, i32 6
  store i32 2, i32* %noise_shaping_amp127, align 4
  %94 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop128 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %94, i32 0, i32 7
  store i32 1, i32* %noise_shaping_stop128, align 4
  %95 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain129 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %95, i32 0, i32 4
  %96 = load i32, i32* %subblock_gain129, align 4
  %cmp130 = icmp eq i32 %96, -1
  br i1 %cmp130, label %if.then131, label %if.end133

if.then131:                                       ; preds = %if.end126
  %97 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain132 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %97, i32 0, i32 4
  store i32 1, i32* %subblock_gain132, align 4
  br label %if.end133

if.end133:                                        ; preds = %if.then131, %if.end126
  %98 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman134 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %98, i32 0, i32 5
  store i32 1, i32* %use_best_huffman134, align 4
  %99 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop135 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %99, i32 0, i32 8
  store i32 1, i32* %full_outer_loop135, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end133, %if.end111, %if.end89, %if.end69, %if.end54, %if.end39, %if.end24, %if.end, %sw.bb
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #2

declare i32 @lame_get_quant_comp(%struct.lame_global_struct*) #1

declare i32 @lame_set_quant_comp(%struct.lame_global_struct*, i32) #1

declare i32 @lame_get_quant_comp_short(%struct.lame_global_struct*) #1

declare i32 @lame_set_quant_comp_short(%struct.lame_global_struct*, i32) #1

declare float @lame_get_msfix(%struct.lame_global_struct*) #1

declare void @lame_set_msfix(%struct.lame_global_struct*, double) #1

declare i32 @lame_set_exp_nspsytune(%struct.lame_global_struct*, i32) #1

declare i32 @lame_get_exp_nspsytune(%struct.lame_global_struct*) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_init_bitstream(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %cmp, label %if.then1, label %if.end12

if.then1:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  %frame_number = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 3
  store i32 0, i32* %frame_number, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_id3tag_automatic = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 17
  %6 = load i32, i32* %write_id3tag_automatic, align 4
  %tobool2 = icmp ne i32 %6, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then1
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call4 = call i32 @id3tag_write_v2(%struct.lame_global_struct* %7)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then1
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 12
  %bitrate_channelmode_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc5, i32 0, i32 0
  %arraydecay = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist, i32 0, i32 0
  %9 = bitcast [5 x i32]* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %9, i8 0, i32 320, i1 false)
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 12
  %bitrate_blocktype_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc6, i32 0, i32 1
  %arraydecay7 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist, i32 0, i32 0
  %11 = bitcast [6 x i32]* %arraydecay7 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %11, i8 0, i32 384, i1 false)
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 15
  %PeakSample = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 1
  store float 0.000000e+00, float* %PeakSample, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 5
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 35
  %14 = load i32, i32* %write_lame_tag, align 4
  %tobool8 = icmp ne i32 %14, 0
  br i1 %tobool8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call10 = call i32 @InitVbrTag(%struct.lame_global_struct* %15)
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.then
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %entry
  store i32 -3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end13, %if.end11
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

declare void @iteration_init(%struct.lame_internal_flags*) #1

declare i32 @psymodel_init(%struct.lame_global_struct*) #1

declare i32 @get_max_frame_buffer_size_by_constraint(%struct.SessionConfig_t*, i32) #1

declare i32 @InitGainAnalysis(%struct.replaygain_data*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @lame_print_config(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %out_samplerate = alloca double, align 8
  %in_samplerate = alloca double, align 8
  %text = alloca [256 x i8], align 16
  %fft_asm_used = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 12
  %4 = load i32, i32* %samplerate_out, align 4
  %conv = sitofp i32 %4 to double
  store double %conv, double* %out_samplerate, align 8
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 11
  %6 = load i32, i32* %samplerate_in, align 4
  %conv2 = sitofp i32 %6 to double
  store double %conv2, double* %in_samplerate, align 8
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call = call i8* @get_lame_version()
  %call3 = call i8* @get_lame_os_bitness()
  %call4 = call i8* @get_lame_url()
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %7, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str, i32 0, i32 0), i8* %call, i8* %call3, i8* %call4)
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 19
  %9 = bitcast %struct.anon.2* %CPU_features to i32*
  %bf.load = load i32, i32* %9, align 8
  %bf.clear = and i32 %bf.load, 1
  %tobool = icmp ne i32 %bf.clear, 0
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 19
  %11 = bitcast %struct.anon.2* %CPU_features5 to i32*
  %bf.load6 = load i32, i32* %11, align 8
  %bf.lshr = lshr i32 %bf.load6, 1
  %bf.clear7 = and i32 %bf.lshr, 1
  %tobool8 = icmp ne i32 %bf.clear7, 0
  br i1 %tobool8, label %if.then, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %lor.lhs.false
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 19
  %13 = bitcast %struct.anon.2* %CPU_features10 to i32*
  %bf.load11 = load i32, i32* %13, align 8
  %bf.lshr12 = lshr i32 %bf.load11, 2
  %bf.clear13 = and i32 %bf.lshr12, 1
  %tobool14 = icmp ne i32 %bf.clear13, 0
  br i1 %tobool14, label %if.then, label %lor.lhs.false15

lor.lhs.false15:                                  ; preds = %lor.lhs.false9
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 19
  %15 = bitcast %struct.anon.2* %CPU_features16 to i32*
  %bf.load17 = load i32, i32* %15, align 8
  %bf.lshr18 = lshr i32 %bf.load17, 3
  %bf.clear19 = and i32 %bf.lshr18, 1
  %tobool20 = icmp ne i32 %bf.clear19, 0
  br i1 %tobool20, label %if.then, label %if.end58

if.then:                                          ; preds = %lor.lhs.false15, %lor.lhs.false9, %lor.lhs.false, %entry
  %16 = bitcast [256 x i8]* %text to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %16, i8 0, i32 256, i1 false)
  store i32 0, i32* %fft_asm_used, align 4
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 19
  %18 = bitcast %struct.anon.2* %CPU_features21 to i32*
  %bf.load22 = load i32, i32* %18, align 8
  %bf.clear23 = and i32 %bf.load22, 1
  %tobool24 = icmp ne i32 %bf.clear23, 0
  br i1 %tobool24, label %if.then25, label %if.end

if.then25:                                        ; preds = %if.then
  %arraydecay = getelementptr inbounds [256 x i8], [256 x i8]* %text, i32 0, i32 0
  call void @concatSep(i8* %arraydecay, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then25, %if.then
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features26 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 19
  %20 = bitcast %struct.anon.2* %CPU_features26 to i32*
  %bf.load27 = load i32, i32* %20, align 8
  %bf.lshr28 = lshr i32 %bf.load27, 1
  %bf.clear29 = and i32 %bf.lshr28, 1
  %tobool30 = icmp ne i32 %bf.clear29, 0
  br i1 %tobool30, label %if.then31, label %if.end34

if.then31:                                        ; preds = %if.end
  %arraydecay32 = getelementptr inbounds [256 x i8], [256 x i8]* %text, i32 0, i32 0
  %21 = load i32, i32* %fft_asm_used, align 4
  %cmp = icmp eq i32 %21, 1
  %22 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.6, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.7, i32 0, i32 0)
  call void @concatSep(i8* %arraydecay32, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), i8* %cond)
  br label %if.end34

if.end34:                                         ; preds = %if.then31, %if.end
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 19
  %24 = bitcast %struct.anon.2* %CPU_features35 to i32*
  %bf.load36 = load i32, i32* %24, align 8
  %bf.lshr37 = lshr i32 %bf.load36, 2
  %bf.clear38 = and i32 %bf.lshr37, 1
  %tobool39 = icmp ne i32 %bf.clear38, 0
  br i1 %tobool39, label %if.then40, label %if.end45

if.then40:                                        ; preds = %if.end34
  %arraydecay41 = getelementptr inbounds [256 x i8], [256 x i8]* %text, i32 0, i32 0
  %25 = load i32, i32* %fft_asm_used, align 4
  %cmp42 = icmp eq i32 %25, 2
  %26 = zext i1 %cmp42 to i64
  %cond44 = select i1 %cmp42, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.9, i32 0, i32 0)
  call void @concatSep(i8* %arraydecay41, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), i8* %cond44)
  br label %if.end45

if.end45:                                         ; preds = %if.then40, %if.end34
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %CPU_features46 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 19
  %28 = bitcast %struct.anon.2* %CPU_features46 to i32*
  %bf.load47 = load i32, i32* %28, align 8
  %bf.lshr48 = lshr i32 %bf.load47, 3
  %bf.clear49 = and i32 %bf.lshr48, 1
  %tobool50 = icmp ne i32 %bf.clear49, 0
  br i1 %tobool50, label %if.then51, label %if.end56

if.then51:                                        ; preds = %if.end45
  %arraydecay52 = getelementptr inbounds [256 x i8], [256 x i8]* %text, i32 0, i32 0
  %29 = load i32, i32* %fft_asm_used, align 4
  %cmp53 = icmp eq i32 %29, 3
  %30 = zext i1 %cmp53 to i64
  %cond55 = select i1 %cmp53, i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.10, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.11, i32 0, i32 0)
  call void @concatSep(i8* %arraydecay52, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i32 0, i32 0), i8* %cond55)
  br label %if.end56

if.end56:                                         ; preds = %if.then51, %if.end45
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %arraydecay57 = getelementptr inbounds [256 x i8], [256 x i8]* %text, i32 0, i32 0
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %31, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.12, i32 0, i32 0), i8* %arraydecay57)
  br label %if.end58

if.end58:                                         ; preds = %if.end56, %lor.lhs.false15
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 13
  %33 = load i32, i32* %channels_in, align 4
  %cmp59 = icmp eq i32 %33, 2
  br i1 %cmp59, label %land.lhs.true, label %if.end64

land.lhs.true:                                    ; preds = %if.end58
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 14
  %35 = load i32, i32* %channels_out, align 4
  %cmp61 = icmp eq i32 %35, 1
  br i1 %cmp61, label %if.then63, label %if.end64

if.then63:                                        ; preds = %land.lhs.true
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %36, i8* getelementptr inbounds ([68 x i8], [68 x i8]* @.str.13, i32 0, i32 0))
  br label %if.end64

if.end64:                                         ; preds = %if.then63, %land.lhs.true, %if.end58
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call65 = call i32 @isResamplingNecessary(%struct.SessionConfig_t* %37)
  %tobool66 = icmp ne i32 %call65, 0
  br i1 %tobool66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.end64
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %39 = load double, double* %in_samplerate, align 8
  %mul = fmul double 1.000000e-03, %39
  %40 = load double, double* %out_samplerate, align 8
  %mul68 = fmul double 1.000000e-03, %40
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %38, i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str.14, i32 0, i32 0), double %mul, double %mul68)
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.end64
  %41 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %41, i32 0, i32 61
  %42 = load float, float* %highpass2, align 4
  %conv70 = fpext float %42 to double
  %cmp71 = fcmp ogt double %conv70, 0.000000e+00
  br i1 %cmp71, label %if.then73, label %if.end81

if.then73:                                        ; preds = %if.end69
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %44 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %44, i32 0, i32 60
  %45 = load float, float* %highpass1, align 4
  %conv74 = fpext float %45 to double
  %mul75 = fmul double 5.000000e-01, %conv74
  %46 = load double, double* %out_samplerate, align 8
  %mul76 = fmul double %mul75, %46
  %47 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpass277 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %47, i32 0, i32 61
  %48 = load float, float* %highpass277, align 4
  %conv78 = fpext float %48 to double
  %mul79 = fmul double 5.000000e-01, %conv78
  %49 = load double, double* %out_samplerate, align 8
  %mul80 = fmul double %mul79, %49
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %43, i8* getelementptr inbounds ([71 x i8], [71 x i8]* @.str.15, i32 0, i32 0), double %mul76, double %mul80)
  br label %if.end81

if.end81:                                         ; preds = %if.then73, %if.end69
  %50 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass1 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %50, i32 0, i32 58
  %51 = load float, float* %lowpass1, align 4
  %conv82 = fpext float %51 to double
  %cmp83 = fcmp olt double 0.000000e+00, %conv82
  br i1 %cmp83, label %if.then89, label %lor.lhs.false85

lor.lhs.false85:                                  ; preds = %if.end81
  %52 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %52, i32 0, i32 59
  %53 = load float, float* %lowpass2, align 4
  %conv86 = fpext float %53 to double
  %cmp87 = fcmp olt double 0.000000e+00, %conv86
  br i1 %cmp87, label %if.then89, label %if.else

if.then89:                                        ; preds = %lor.lhs.false85, %if.end81
  %54 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %55 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass190 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %55, i32 0, i32 58
  %56 = load float, float* %lowpass190, align 4
  %conv91 = fpext float %56 to double
  %mul92 = fmul double 5.000000e-01, %conv91
  %57 = load double, double* %out_samplerate, align 8
  %mul93 = fmul double %mul92, %57
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpass294 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 59
  %59 = load float, float* %lowpass294, align 4
  %conv95 = fpext float %59 to double
  %mul96 = fmul double 5.000000e-01, %conv95
  %60 = load double, double* %out_samplerate, align 8
  %mul97 = fmul double %mul96, %60
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %54, i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.16, i32 0, i32 0), double %mul93, double %mul97)
  br label %if.end98

if.else:                                          ; preds = %lor.lhs.false85
  %61 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %61, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.17, i32 0, i32 0))
  br label %if.end98

if.end98:                                         ; preds = %if.else, %if.then89
  %62 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %62, i32 0, i32 34
  %63 = load i32, i32* %free_format, align 4
  %tobool99 = icmp ne i32 %63, 0
  br i1 %tobool99, label %if.then100, label %if.end105

if.then100:                                       ; preds = %if.end98
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %64, i8* getelementptr inbounds ([61 x i8], [61 x i8]* @.str.18, i32 0, i32 0))
  %65 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %65, i32 0, i32 26
  %66 = load i32, i32* %avg_bitrate, align 4
  %cmp101 = icmp sgt i32 %66, 320
  br i1 %cmp101, label %if.then103, label %if.end104

if.then103:                                       ; preds = %if.then100
  %67 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %67, i8* getelementptr inbounds ([89 x i8], [89 x i8]* @.str.19, i32 0, i32 0))
  br label %if.end104

if.end104:                                        ; preds = %if.then103, %if.then100
  br label %if.end105

if.end105:                                        ; preds = %if.end104, %if.end98
  ret void
}

declare void @lame_msgf(%struct.lame_internal_flags*, i8*, ...) #1

declare i8* @get_lame_version() #1

declare i8* @get_lame_os_bitness() #1

declare i8* @get_lame_url() #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define internal void @concatSep(i8* %dest, i8* %sep, i8* %str) #0 {
entry:
  %dest.addr = alloca i8*, align 4
  %sep.addr = alloca i8*, align 4
  %str.addr = alloca i8*, align 4
  store i8* %dest, i8** %dest.addr, align 4
  store i8* %sep, i8** %sep.addr, align 4
  store i8* %str, i8** %str.addr, align 4
  %0 = load i8*, i8** %dest.addr, align 4
  %1 = load i8, i8* %0, align 1
  %conv = sext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %dest.addr, align 4
  %3 = load i8*, i8** %sep.addr, align 4
  %call = call i8* @strcat(i8* %2, i8* %3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load i8*, i8** %dest.addr, align 4
  %5 = load i8*, i8** %str.addr, align 4
  %call2 = call i8* @strcat(i8* %4, i8* %5)
  ret void
}

declare i32 @isResamplingNecessary(%struct.SessionConfig_t*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @lame_print_internals(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %pc = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.20, i32 0, i32 0), i8** %pc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %3, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.21, i32 0, i32 0))
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 5
  %6 = load float, float* %scale, align 4
  %conv = fpext float %6 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %4, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.22, i32 0, i32 0), double %conv)
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %8, i32 0, i32 6
  %9 = load float, float* %scale_left, align 4
  %conv2 = fpext float %9 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %7, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.23, i32 0, i32 0), double %conv2)
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %11, i32 0, i32 7
  %12 = load float, float* %scale_right, align 4
  %conv3 = fpext float %12 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %10, i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.24, i32 0, i32 0), double %conv3)
  %13 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %13, i32 0, i32 5
  %14 = load i32, i32* %use_best_huffman, align 4
  switch i32 %14, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb4
  ]

sw.default:                                       ; preds = %entry
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.25, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog

sw.bb:                                            ; preds = %entry
  store i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.26, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog

sw.bb4:                                           ; preds = %entry
  store i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.27, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb4, %sw.bb, %sw.default
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %16 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %15, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.28, i32 0, i32 0), i8* %16)
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %experimentalY = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %18, i32 0, i32 35
  %19 = load i32, i32* %experimentalY, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %17, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.29, i32 0, i32 0), i32 %19)
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %20, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.30, i32 0, i32 0))
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %21, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.31, i32 0, i32 0))
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %22, i32 0, i32 0
  %23 = load i32, i32* %version, align 4
  switch i32 %23, label %sw.default8 [
    i32 0, label %sw.bb5
    i32 1, label %sw.bb6
    i32 2, label %sw.bb7
  ]

sw.bb5:                                           ; preds = %sw.epilog
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.32, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog9

sw.bb6:                                           ; preds = %sw.epilog
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.33, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog9

sw.bb7:                                           ; preds = %sw.epilog
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.34, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog9

sw.default8:                                      ; preds = %sw.epilog
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.35, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog9

sw.epilog9:                                       ; preds = %sw.default8, %sw.bb7, %sw.bb6, %sw.bb5
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %25 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %24, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.36, i32 0, i32 0), i8* %25)
  %26 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %26, i32 0, i32 41
  %27 = load i32, i32* %mode, align 4
  switch i32 %27, label %sw.default15 [
    i32 1, label %sw.bb10
    i32 0, label %sw.bb11
    i32 2, label %sw.bb12
    i32 3, label %sw.bb13
    i32 4, label %sw.bb14
  ]

sw.bb10:                                          ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.37, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.bb11:                                          ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.38, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.bb12:                                          ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.39, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.bb13:                                          ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.40, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.bb14:                                          ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.41, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.default15:                                     ; preds = %sw.epilog9
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.42, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog16

sw.epilog16:                                      ; preds = %sw.default15, %sw.bb14, %sw.bb13, %sw.bb12, %sw.bb11, %sw.bb10
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %29 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %29, i32 0, i32 14
  %30 = load i32, i32* %channels_out, align 4
  %31 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %28, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.43, i32 0, i32 0), i32 %30, i8* %31)
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 22
  %33 = load i32, i32* %vbr, align 4
  switch i32 %33, label %sw.default18 [
    i32 0, label %sw.bb17
  ]

sw.bb17:                                          ; preds = %sw.epilog16
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.44, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog19

sw.default18:                                     ; preds = %sw.epilog16
  store i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.45, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog19

sw.epilog19:                                      ; preds = %sw.default18, %sw.bb17
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %35 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %34, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.46, i32 0, i32 0), i8* %35)
  %36 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr20 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %36, i32 0, i32 22
  %37 = load i32, i32* %vbr20, align 4
  %cmp = icmp eq i32 4, %37
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.epilog19
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.47, i32 0, i32 0), i8** %pc, align 4
  br label %if.end24

if.else:                                          ; preds = %sw.epilog19
  %38 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %38, i32 0, i32 34
  %39 = load i32, i32* %free_format, align 4
  %tobool = icmp ne i32 %39, 0
  br i1 %tobool, label %if.then22, label %if.else23

if.then22:                                        ; preds = %if.else
  store i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.48, i32 0, i32 0), i8** %pc, align 4
  br label %if.end

if.else23:                                        ; preds = %if.else
  store i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.20, i32 0, i32 0), i8** %pc, align 4
  br label %if.end

if.end:                                           ; preds = %if.else23, %if.then22
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then
  %40 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr25 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %40, i32 0, i32 22
  %41 = load i32, i32* %vbr25, align 4
  switch i32 %41, label %sw.default31 [
    i32 0, label %sw.bb26
    i32 3, label %sw.bb27
    i32 2, label %sw.bb28
    i32 1, label %sw.bb29
    i32 4, label %sw.bb30
  ]

sw.bb26:                                          ; preds = %if.end24
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %43 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %42, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.49, i32 0, i32 0), i8* %43)
  br label %sw.epilog32

sw.bb27:                                          ; preds = %if.end24
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %45 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %44, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.50, i32 0, i32 0), i8* %45)
  br label %sw.epilog32

sw.bb28:                                          ; preds = %if.end24
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %47 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %46, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.51, i32 0, i32 0), i8* %47)
  br label %sw.epilog32

sw.bb29:                                          ; preds = %if.end24
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %49 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %48, i8* getelementptr inbounds ([31 x i8], [31 x i8]* @.str.52, i32 0, i32 0), i8* %49)
  br label %sw.epilog32

sw.bb30:                                          ; preds = %if.end24
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %51 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %50, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.53, i32 0, i32 0), i8* %51)
  br label %sw.epilog32

sw.default31:                                     ; preds = %if.end24
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %52, i8* getelementptr inbounds ([29 x i8], [29 x i8]* @.str.54, i32 0, i32 0))
  br label %sw.epilog32

sw.epilog32:                                      ; preds = %sw.default31, %sw.bb30, %sw.bb29, %sw.bb28, %sw.bb27, %sw.bb26
  %53 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %53, i32 0, i32 35
  %54 = load i32, i32* %write_lame_tag, align 4
  %tobool33 = icmp ne i32 %54, 0
  br i1 %tobool33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %sw.epilog32
  %55 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %55, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.55, i32 0, i32 0))
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %sw.epilog32
  %56 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %56, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.30, i32 0, i32 0))
  %57 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %57, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.56, i32 0, i32 0))
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %short_blocks = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 42
  %59 = load i32, i32* %short_blocks, align 4
  switch i32 %59, label %sw.default36 [
    i32 -1, label %sw.bb37
    i32 0, label %sw.bb38
    i32 1, label %sw.bb39
    i32 2, label %sw.bb40
    i32 3, label %sw.bb41
  ]

sw.default36:                                     ; preds = %if.end35
  br label %sw.bb37

sw.bb37:                                          ; preds = %if.end35, %sw.default36
  store i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.35, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog42

sw.bb38:                                          ; preds = %if.end35
  store i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.57, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog42

sw.bb39:                                          ; preds = %if.end35
  store i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.58, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog42

sw.bb40:                                          ; preds = %if.end35
  store i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.59, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog42

sw.bb41:                                          ; preds = %if.end35
  store i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.60, i32 0, i32 0), i8** %pc, align 4
  br label %sw.epilog42

sw.epilog42:                                      ; preds = %sw.bb41, %sw.bb40, %sw.bb39, %sw.bb38, %sw.bb37
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %61 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %60, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str.61, i32 0, i32 0), i8* %61)
  %62 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 4
  %64 = load i32, i32* %subblock_gain, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %62, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.62, i32 0, i32 0), i32 %64)
  %65 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 3
  %67 = load float, float* %mask_adjust, align 8
  %conv43 = fpext float %67 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %65, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.63, i32 0, i32 0), double %conv43)
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 13
  %mask_adjust_short = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt44, i32 0, i32 4
  %70 = load float, float* %mask_adjust_short, align 4
  %conv45 = fpext float %70 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %68, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.64, i32 0, i32 0), double %conv45)
  %71 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %72 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %72, i32 0, i32 17
  %73 = load i32, i32* %quant_comp, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %71, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.65, i32 0, i32 0), i32 %73)
  %74 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %75 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp_short = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %75, i32 0, i32 18
  %76 = load i32, i32* %quant_comp_short, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %74, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.66, i32 0, i32 0), i32 %76)
  %77 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %78 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %78, i32 0, i32 3
  %79 = load i32, i32* %noise_shaping, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %77, i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.67, i32 0, i32 0), i32 %79)
  %80 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %81 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %81, i32 0, i32 6
  %82 = load i32, i32* %noise_shaping_amp, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %80, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.68, i32 0, i32 0), i32 %82)
  %83 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %84 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_stop = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %84, i32 0, i32 7
  %85 = load i32, i32* %noise_shaping_stop, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %83, i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.69, i32 0, i32 0), i32 %85)
  store i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.70, i32 0, i32 0), i8** %pc, align 4
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHshort = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 50
  %87 = load i32, i32* %ATHshort, align 4
  %tobool46 = icmp ne i32 %87, 0
  br i1 %tobool46, label %if.then47, label %if.end48

if.then47:                                        ; preds = %sw.epilog42
  store i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.71, i32 0, i32 0), i8** %pc, align 4
  br label %if.end48

if.end48:                                         ; preds = %if.then47, %sw.epilog42
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHonly = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 49
  %89 = load i32, i32* %ATHonly, align 4
  %tobool49 = icmp ne i32 %89, 0
  br i1 %tobool49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.end48
  store i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.72, i32 0, i32 0), i8** %pc, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.then50, %if.end48
  %90 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noATH = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %90, i32 0, i32 51
  %91 = load i32, i32* %noATH, align 4
  %tobool52 = icmp ne i32 %91, 0
  br i1 %tobool52, label %if.then53, label %if.end54

if.then53:                                        ; preds = %if.end51
  store i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.73, i32 0, i32 0), i8** %pc, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.then53, %if.end51
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %93 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %92, i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.74, i32 0, i32 0), i8* %93)
  %94 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %95 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHtype = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %95, i32 0, i32 48
  %96 = load i32, i32* %ATHtype, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %94, i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.75, i32 0, i32 0), i32 %96)
  %97 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %98 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHcurve = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %98, i32 0, i32 47
  %99 = load float, float* %ATHcurve, align 4
  %conv55 = fpext float %99 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %97, i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.76, i32 0, i32 0), double %conv55, i8* getelementptr inbounds ([19 x i8], [19 x i8]* @.str.77, i32 0, i32 0))
  %100 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %101 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATH_offset_db = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %101, i32 0, i32 45
  %102 = load float, float* %ATH_offset_db, align 4
  %conv56 = fpext float %102 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %100, i8* getelementptr inbounds ([30 x i8], [30 x i8]* @.str.78, i32 0, i32 0), double %conv56)
  %103 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %104 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %104, i32 0, i32 21
  %105 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %use_adjust = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %105, i32 0, i32 0
  %106 = load i32, i32* %use_adjust, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %103, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.79, i32 0, i32 0), i32 %106)
  %107 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %108 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ATH57 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %108, i32 0, i32 21
  %109 = load %struct.ATH_t*, %struct.ATH_t** %ATH57, align 8
  %aa_sensitivity_p = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %109, i32 0, i32 1
  %110 = load float, float* %aa_sensitivity_p, align 4
  %conv58 = fpext float %110 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %107, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str.80, i32 0, i32 0), double %conv58)
  %111 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %111, i8* getelementptr inbounds ([44 x i8], [44 x i8]* @.str.81, i32 0, i32 0))
  %112 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %113 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt59 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %113, i32 0, i32 13
  %longfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt59, i32 0, i32 0
  %arrayidx = getelementptr inbounds [22 x float], [22 x float]* %longfact, i32 0, i32 0
  %114 = load float, float* %arrayidx, align 8
  %conv60 = fpext float %114 to double
  %115 = call double @llvm.log10.f64(double %conv60)
  %mul = fmul double 1.000000e+01, %115
  %116 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt61 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %116, i32 0, i32 13
  %longfact62 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt61, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [22 x float], [22 x float]* %longfact62, i32 0, i32 7
  %117 = load float, float* %arrayidx63, align 4
  %conv64 = fpext float %117 to double
  %118 = call double @llvm.log10.f64(double %conv64)
  %mul65 = fmul double 1.000000e+01, %118
  %119 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt66 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %119, i32 0, i32 13
  %longfact67 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt66, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [22 x float], [22 x float]* %longfact67, i32 0, i32 14
  %120 = load float, float* %arrayidx68, align 8
  %conv69 = fpext float %120 to double
  %121 = call double @llvm.log10.f64(double %conv69)
  %mul70 = fmul double 1.000000e+01, %121
  %122 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_qnt71 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %122, i32 0, i32 13
  %longfact72 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt71, i32 0, i32 0
  %arrayidx73 = getelementptr inbounds [22 x float], [22 x float]* %longfact72, i32 0, i32 21
  %123 = load float, float* %arrayidx73, align 4
  %conv74 = fpext float %123 to double
  %124 = call double @llvm.log10.f64(double %conv74)
  %mul75 = fmul double 1.000000e+01, %124
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %112, i8* getelementptr inbounds ([70 x i8], [70 x i8]* @.str.82, i32 0, i32 0), double %mul, double %mul65, double %mul70, double %mul75)
  %125 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_temporal_masking_effect = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %125, i32 0, i32 19
  %126 = load i32, i32* %use_temporal_masking_effect, align 4
  %tobool76 = icmp ne i32 %126, 0
  %127 = zext i1 %tobool76 to i64
  %cond = select i1 %tobool76, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.83, i32 0, i32 0), i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.84, i32 0, i32 0)
  store i8* %cond, i8** %pc, align 4
  %128 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %129 = load i8*, i8** %pc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %128, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str.85, i32 0, i32 0), i8* %129)
  %130 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %131 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %interChRatio = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %131, i32 0, i32 43
  %132 = load float, float* %interChRatio, align 4
  %conv77 = fpext float %132 to double
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %130, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.86, i32 0, i32 0), double %conv77)
  %133 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %133, i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.30, i32 0, i32 0))
  %134 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_msgf(%struct.lame_internal_flags* %134, i8* getelementptr inbounds ([2 x i8], [2 x i8]* @.str.87, i32 0, i32 0))
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log10.f64(double) #2

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer(%struct.lame_global_struct* %gfp, i16* %pcm_l, i16* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca i16*, align 4
  %pcm_r.addr = alloca i16*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i16* %pcm_l, i16** %pcm_l.addr, align 4
  store i16* %pcm_r, i16** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i16*, i16** %pcm_l.addr, align 4
  %2 = bitcast i16* %1 to i8*
  %3 = load i16*, i16** %pcm_r.addr, align 4
  %4 = bitcast i16* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 0, i32 1, float 1.000000e+00)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @lame_encode_buffer_template(%struct.lame_global_struct* %gfp, i8* %buffer_l, i8* %buffer_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size, i32 %pcm_type, i32 %aa, float %norm) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer_l.addr = alloca i8*, align 4
  %buffer_r.addr = alloca i8*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %pcm_type.addr = alloca i32, align 4
  %aa.addr = alloca i32, align 4
  %norm.addr = alloca float, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %buffer_l, i8** %buffer_l.addr, align 4
  store i8* %buffer_r, i8** %buffer_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  store i32 %pcm_type, i32* %pcm_type.addr, align 4
  store i32 %aa, i32* %aa.addr, align 4
  store float %norm, float* %norm.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end22

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end21

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load i32, i32* %nsamples.addr, align 4
  %cmp = icmp eq i32 %5, 0
  br i1 %cmp, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then3
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then3
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %7 = load i32, i32* %nsamples.addr, align 4
  %call6 = call i32 @update_inbuffer_size(%struct.lame_internal_flags* %6, i32 %7)
  %cmp7 = icmp ne i32 %call6, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  store i32 -2, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 13
  %9 = load i32, i32* %channels_in, align 4
  %cmp10 = icmp sgt i32 %9, 1
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end9
  %10 = load i8*, i8** %buffer_l.addr, align 4
  %cmp12 = icmp eq i8* %10, null
  br i1 %cmp12, label %if.then14, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then11
  %11 = load i8*, i8** %buffer_r.addr, align 4
  %cmp13 = icmp eq i8* %11, null
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %lor.lhs.false, %if.then11
  store i32 0, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %lor.lhs.false
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %13 = load i8*, i8** %buffer_l.addr, align 4
  %14 = load i8*, i8** %buffer_r.addr, align 4
  %15 = load i32, i32* %nsamples.addr, align 4
  %16 = load i32, i32* %pcm_type.addr, align 4
  %17 = load i32, i32* %aa.addr, align 4
  %18 = load float, float* %norm.addr, align 4
  call void @lame_copy_inbuffer(%struct.lame_internal_flags* %12, i8* %13, i8* %14, i32 %15, i32 %16, i32 %17, float %18)
  br label %if.end19

if.else:                                          ; preds = %if.end9
  %19 = load i8*, i8** %buffer_l.addr, align 4
  %cmp16 = icmp eq i8* %19, null
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.else
  store i32 0, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.else
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %21 = load i8*, i8** %buffer_l.addr, align 4
  %22 = load i8*, i8** %buffer_l.addr, align 4
  %23 = load i32, i32* %nsamples.addr, align 4
  %24 = load i32, i32* %pcm_type.addr, align 4
  %25 = load i32, i32* %aa.addr, align 4
  %26 = load float, float* %norm.addr, align 4
  call void @lame_copy_inbuffer(%struct.lame_internal_flags* %20, i8* %21, i8* %22, i32 %23, i32 %24, i32 %25, float %26)
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.end15
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %28 = load i32, i32* %nsamples.addr, align 4
  %29 = load i8*, i8** %mp3buf.addr, align 4
  %30 = load i32, i32* %mp3buf_size.addr, align 4
  %call20 = call i32 @lame_encode_buffer_sample_t(%struct.lame_internal_flags* %27, i32 %28, i8* %29, i32 %30)
  store i32 %call20, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.then
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %entry
  store i32 -3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.end19, %if.then17, %if.then14, %if.then8, %if.then5
  %31 = load i32, i32* %retval, align 4
  ret i32 %31
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_float(%struct.lame_global_struct* %gfp, float* %pcm_l, float* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca float*, align 4
  %pcm_r.addr = alloca float*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float* %pcm_l, float** %pcm_l.addr, align 4
  store float* %pcm_r, float** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load float*, float** %pcm_l.addr, align 4
  %2 = bitcast float* %1 to i8*
  %3 = load float*, float** %pcm_r.addr, align 4
  %4 = bitcast float* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 3, i32 1, float 1.000000e+00)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_ieee_float(%struct.lame_global_struct* %gfp, float* %pcm_l, float* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca float*, align 4
  %pcm_r.addr = alloca float*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float* %pcm_l, float** %pcm_l.addr, align 4
  store float* %pcm_r, float** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load float*, float** %pcm_l.addr, align 4
  %2 = bitcast float* %1 to i8*
  %3 = load float*, float** %pcm_r.addr, align 4
  %4 = bitcast float* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 3, i32 1, float 3.276700e+04)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_interleaved_ieee_float(%struct.lame_global_struct* %gfp, float* %pcm, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm.addr = alloca float*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store float* %pcm, float** %pcm.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load float*, float** %pcm.addr, align 4
  %2 = bitcast float* %1 to i8*
  %3 = load float*, float** %pcm.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %3, i32 1
  %4 = bitcast float* %add.ptr to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 3, i32 2, float 3.276700e+04)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_ieee_double(%struct.lame_global_struct* %gfp, double* %pcm_l, double* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca double*, align 4
  %pcm_r.addr = alloca double*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store double* %pcm_l, double** %pcm_l.addr, align 4
  store double* %pcm_r, double** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load double*, double** %pcm_l.addr, align 4
  %2 = bitcast double* %1 to i8*
  %3 = load double*, double** %pcm_r.addr, align 4
  %4 = bitcast double* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 4, i32 1, float 3.276700e+04)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_interleaved_ieee_double(%struct.lame_global_struct* %gfp, double* %pcm, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm.addr = alloca double*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store double* %pcm, double** %pcm.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load double*, double** %pcm.addr, align 4
  %2 = bitcast double* %1 to i8*
  %3 = load double*, double** %pcm.addr, align 4
  %add.ptr = getelementptr inbounds double, double* %3, i32 1
  %4 = bitcast double* %add.ptr to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 4, i32 2, float 3.276700e+04)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_int(%struct.lame_global_struct* %gfp, i32* %pcm_l, i32* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca i32*, align 4
  %pcm_r.addr = alloca i32*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %norm = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %pcm_l, i32** %pcm_l.addr, align 4
  store i32* %pcm_r, i32** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  store float 0x3EF0000000000000, float* %norm, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32*, i32** %pcm_l.addr, align 4
  %2 = bitcast i32* %1 to i8*
  %3 = load i32*, i32** %pcm_r.addr, align 4
  %4 = bitcast i32* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 1, i32 1, float 0x3EF0000000000000)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_long2(%struct.lame_global_struct* %gfp, i32* %pcm_l, i32* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca i32*, align 4
  %pcm_r.addr = alloca i32*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %norm = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %pcm_l, i32** %pcm_l.addr, align 4
  store i32* %pcm_r, i32** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  store float 0x3EF0000000000000, float* %norm, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32*, i32** %pcm_l.addr, align 4
  %2 = bitcast i32* %1 to i8*
  %3 = load i32*, i32** %pcm_r.addr, align 4
  %4 = bitcast i32* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 2, i32 1, float 0x3EF0000000000000)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_long(%struct.lame_global_struct* %gfp, i32* %pcm_l, i32* %pcm_r, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm_l.addr = alloca i32*, align 4
  %pcm_r.addr = alloca i32*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %pcm_l, i32** %pcm_l.addr, align 4
  store i32* %pcm_r, i32** %pcm_r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32*, i32** %pcm_l.addr, align 4
  %2 = bitcast i32* %1 to i8*
  %3 = load i32*, i32** %pcm_r.addr, align 4
  %4 = bitcast i32* %3 to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 2, i32 1, float 1.000000e+00)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_interleaved(%struct.lame_global_struct* %gfp, i16* %pcm, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm.addr = alloca i16*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i16* %pcm, i16** %pcm.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i16*, i16** %pcm.addr, align 4
  %2 = bitcast i16* %1 to i8*
  %3 = load i16*, i16** %pcm.addr, align 4
  %add.ptr = getelementptr inbounds i16, i16* %3, i32 1
  %4 = bitcast i16* %add.ptr to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 0, i32 2, float 1.000000e+00)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_buffer_interleaved_int(%struct.lame_global_struct* %gfp, i32* %pcm, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %pcm.addr = alloca i32*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %norm = alloca float, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %pcm, i32** %pcm.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  store float 0x3EF0000000000000, float* %norm, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i32*, i32** %pcm.addr, align 4
  %2 = bitcast i32* %1 to i8*
  %3 = load i32*, i32** %pcm.addr, align 4
  %add.ptr = getelementptr inbounds i32, i32* %3, i32 1
  %4 = bitcast i32* %add.ptr to i8*
  %5 = load i32, i32* %nsamples.addr, align 4
  %6 = load i8*, i8** %mp3buf.addr, align 4
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %call = call i32 @lame_encode_buffer_template(%struct.lame_global_struct* %0, i8* %2, i8* %4, i32 %5, i8* %6, i32 %7, i32 1, i32 2, float 0x3EF0000000000000)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_flush_nogap(%struct.lame_global_struct* %gfp, i8* %mp3buffer, i32 %mp3buffer_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %mp3buffer.addr = alloca i8*, align 4
  %mp3buffer_size.addr = alloca i32, align 4
  %rc = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %mp3buffer, i8** %mp3buffer.addr, align 4
  store i32 %mp3buffer_size, i32* %mp3buffer_size.addr, align 4
  store i32 -3, i32* %rc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end6

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @flush_bitstream(%struct.lame_internal_flags* %4)
  %5 = load i32, i32* %mp3buffer_size.addr, align 4
  %cmp = icmp eq i32 %5, 0
  br i1 %cmp, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then3
  store i32 2147483647, i32* %mp3buffer_size.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.then3
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %7 = load i8*, i8** %mp3buffer.addr, align 4
  %8 = load i32, i32* %mp3buffer_size.addr, align 4
  %call5 = call i32 @copy_buffer(%struct.lame_internal_flags* %6, i8* %7, i32 %8, i32 1)
  store i32 %call5, i32* %rc, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @save_gain_values(%struct.lame_internal_flags* %9)
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then
  br label %if.end7

if.end7:                                          ; preds = %if.end6, %entry
  %10 = load i32, i32* %rc, align 4
  ret i32 %10
}

declare void @flush_bitstream(%struct.lame_internal_flags*) #1

declare i32 @copy_buffer(%struct.lame_internal_flags*, i8*, i32, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @save_gain_values(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %rsv = alloca %struct.RpgStateVar_t*, align 4
  %rov = alloca %struct.RpgResult_t*, align 4
  %RadioGain = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 14
  store %struct.RpgStateVar_t* %sv_rpg, %struct.RpgStateVar_t** %rsv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 15
  store %struct.RpgResult_t* %ov_rpg, %struct.RpgResult_t** %rov, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 28
  %4 = load i32, i32* %findReplayGain, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end18

if.then:                                          ; preds = %entry
  %5 = load %struct.RpgStateVar_t*, %struct.RpgStateVar_t** %rsv, align 4
  %rgdata = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %5, i32 0, i32 0
  %6 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata, align 4
  %call = call float @GetTitleGain(%struct.replaygain_data* %6)
  store float %call, float* %RadioGain, align 4
  %7 = load float, float* %RadioGain, align 4
  %conv = fpext float %7 to double
  %8 = call double @llvm.fabs.f64(double %conv)
  %9 = call double @llvm.fabs.f64(double -2.460100e+04)
  %cmp = fcmp ogt double %8, %9
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %10 = load float, float* %RadioGain, align 4
  %sub = fsub float %10, -2.460100e+04
  %conv3 = fpext float %sub to double
  %11 = call double @llvm.fabs.f64(double %conv3)
  %12 = load float, float* %RadioGain, align 4
  %conv4 = fpext float %12 to double
  %13 = call double @llvm.fabs.f64(double %conv4)
  %mul = fmul double %13, 0x3EB0C6F7A0000000
  %cmp5 = fcmp ole double %11, %mul
  br i1 %cmp5, label %if.else, label %if.then12

cond.false:                                       ; preds = %if.then
  %14 = load float, float* %RadioGain, align 4
  %sub7 = fsub float %14, -2.460100e+04
  %conv8 = fpext float %sub7 to double
  %15 = call double @llvm.fabs.f64(double %conv8)
  %16 = call double @llvm.fabs.f64(double -2.460100e+04)
  %mul9 = fmul double %16, 0x3EB0C6F7A0000000
  %cmp10 = fcmp ole double %15, %mul9
  br i1 %cmp10, label %if.else, label %if.then12

if.then12:                                        ; preds = %cond.false, %cond.true
  %17 = load float, float* %RadioGain, align 4
  %conv13 = fpext float %17 to double
  %mul14 = fmul double %conv13, 1.000000e+01
  %add = fadd double %mul14, 5.000000e-01
  %18 = call double @llvm.floor.f64(double %add)
  %conv15 = fptosi double %18 to i32
  %19 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %RadioGain16 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %19, i32 0, i32 2
  store i32 %conv15, i32* %RadioGain16, align 4
  br label %if.end

if.else:                                          ; preds = %cond.false, %cond.true
  %20 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %RadioGain17 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %20, i32 0, i32 2
  store i32 0, i32* %RadioGain17, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end, %entry
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findPeakSample = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 29
  %22 = load i32, i32* %findPeakSample, align 4
  %tobool19 = icmp ne i32 %22, 0
  br i1 %tobool19, label %if.then20, label %if.end38

if.then20:                                        ; preds = %if.end18
  %23 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %PeakSample = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %23, i32 0, i32 1
  %24 = load float, float* %PeakSample, align 4
  %conv21 = fpext float %24 to double
  %div = fdiv double %conv21, 3.276700e+04
  %25 = call double @llvm.log10.f64(double %div)
  %mul22 = fmul double %25, 2.000000e+01
  %mul23 = fmul double %mul22, 1.000000e+01
  %26 = call double @llvm.ceil.f64(double %mul23)
  %conv24 = fptosi double %26 to i32
  %27 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %noclipGainChange = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %27, i32 0, i32 3
  store i32 %conv24, i32* %noclipGainChange, align 4
  %28 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %noclipGainChange25 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %28, i32 0, i32 3
  %29 = load i32, i32* %noclipGainChange25, align 4
  %cmp26 = icmp sgt i32 %29, 0
  br i1 %cmp26, label %if.then28, label %if.else35

if.then28:                                        ; preds = %if.then20
  %30 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %PeakSample29 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %30, i32 0, i32 1
  %31 = load float, float* %PeakSample29, align 4
  %div30 = fdiv float 3.276700e+04, %31
  %mul31 = fmul float %div30, 1.000000e+02
  %conv32 = fpext float %mul31 to double
  %32 = call double @llvm.floor.f64(double %conv32)
  %div33 = fdiv double %32, 1.000000e+02
  %conv34 = fptrunc double %div33 to float
  %33 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %noclipScale = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %33, i32 0, i32 0
  store float %conv34, float* %noclipScale, align 4
  br label %if.end37

if.else35:                                        ; preds = %if.then20
  %34 = load %struct.RpgResult_t*, %struct.RpgResult_t** %rov, align 4
  %noclipScale36 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %34, i32 0, i32 0
  store float -1.000000e+00, float* %noclipScale36, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else35, %if.then28
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.end18
  ret void
}

declare i32 @id3tag_write_v2(%struct.lame_global_struct*) #1

declare i32 @InitVbrTag(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_flush(%struct.lame_global_struct* %gfp, i8* %mp3buffer, i32 %mp3buffer_size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %mp3buffer.addr = alloca i8*, align 4
  %mp3buffer_size.addr = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %buffer = alloca [2 x [1152 x i16]], align 16
  %imp3 = alloca i32, align 4
  %mp3count = alloca i32, align 4
  %mp3buffer_size_remaining = alloca i32, align 4
  %end_padding = alloca i32, align 4
  %frames_left = alloca i32, align 4
  %samples_to_encode = alloca i32, align 4
  %pcm_samples_per_frame = alloca i32, align 4
  %mf_needed = alloca i32, align 4
  %is_resampling_necessary = alloca i32, align 4
  %resample_ratio = alloca double, align 8
  %frame_num = alloca i32, align 4
  %bunch = alloca i32, align 4
  %new_frames = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %mp3buffer, i8** %mp3buffer.addr, align 4
  store i32 %mp3buffer_size, i32* %mp3buffer_size.addr, align 4
  store i32 0, i32* %imp3, align 4
  store double 1.000000e+00, double* %resample_ratio, align 8
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 -3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  store i32 -3, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg5, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %6 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %6, i32 0, i32 18
  %7 = load i32, i32* %mf_samples_to_encode, align 8
  %cmp = icmp slt i32 %7, 1
  br i1 %cmp, label %if.then6, label %if.end7

if.then6:                                         ; preds = %if.end4
  store i32 0, i32* %retval, align 4
  br label %return

if.end7:                                          ; preds = %if.end4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 15
  %9 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %9
  store i32 %mul, i32* %pcm_samples_per_frame, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call8 = call i32 @calcNeeded(%struct.SessionConfig_t* %10)
  store i32 %call8, i32* %mf_needed, align 4
  %11 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode9 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %11, i32 0, i32 18
  %12 = load i32, i32* %mf_samples_to_encode9, align 8
  %sub = sub nsw i32 %12, 1152
  store i32 %sub, i32* %samples_to_encode, align 4
  %arraydecay = getelementptr inbounds [2 x [1152 x i16]], [2 x [1152 x i16]]* %buffer, i32 0, i32 0
  %13 = bitcast [1152 x i16]* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %13, i8 0, i32 4608, i1 false)
  store i32 0, i32* %mp3count, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call10 = call i32 @isResamplingNecessary(%struct.SessionConfig_t* %14)
  store i32 %call10, i32* %is_resampling_necessary, align 4
  %15 = load i32, i32* %is_resampling_necessary, align 4
  %tobool11 = icmp ne i32 %15, 0
  br i1 %tobool11, label %if.then12, label %if.end17

if.then12:                                        ; preds = %if.end7
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 11
  %17 = load i32, i32* %samplerate_in, align 4
  %conv = sitofp i32 %17 to double
  %18 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %18, i32 0, i32 12
  %19 = load i32, i32* %samplerate_out, align 4
  %conv13 = sitofp i32 %19 to double
  %div = fdiv double %conv, %conv13
  store double %div, double* %resample_ratio, align 8
  %20 = load double, double* %resample_ratio, align 8
  %div14 = fdiv double 1.600000e+01, %20
  %21 = load i32, i32* %samples_to_encode, align 4
  %conv15 = sitofp i32 %21 to double
  %add = fadd double %conv15, %div14
  %conv16 = fptosi double %add to i32
  store i32 %conv16, i32* %samples_to_encode, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then12, %if.end7
  %22 = load i32, i32* %pcm_samples_per_frame, align 4
  %23 = load i32, i32* %samples_to_encode, align 4
  %24 = load i32, i32* %pcm_samples_per_frame, align 4
  %rem = srem i32 %23, %24
  %sub18 = sub nsw i32 %22, %rem
  store i32 %sub18, i32* %end_padding, align 4
  %25 = load i32, i32* %end_padding, align 4
  %cmp19 = icmp slt i32 %25, 576
  br i1 %cmp19, label %if.then21, label %if.end23

if.then21:                                        ; preds = %if.end17
  %26 = load i32, i32* %pcm_samples_per_frame, align 4
  %27 = load i32, i32* %end_padding, align 4
  %add22 = add nsw i32 %27, %26
  store i32 %add22, i32* %end_padding, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.then21, %if.end17
  %28 = load i32, i32* %end_padding, align 4
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 12
  %encoder_padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 7
  store i32 %28, i32* %encoder_padding, align 4
  %30 = load i32, i32* %samples_to_encode, align 4
  %31 = load i32, i32* %end_padding, align 4
  %add24 = add nsw i32 %30, %31
  %32 = load i32, i32* %pcm_samples_per_frame, align 4
  %div25 = sdiv i32 %add24, %32
  store i32 %div25, i32* %frames_left, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end60, %if.end23
  %33 = load i32, i32* %frames_left, align 4
  %cmp26 = icmp sgt i32 %33, 0
  br i1 %cmp26, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %34 = load i32, i32* %imp3, align 4
  %cmp28 = icmp sge i32 %34, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %35 = phi i1 [ false, %while.cond ], [ %cmp28, %land.rhs ]
  br i1 %35, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc30 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 12
  %frame_number = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc30, i32 0, i32 3
  %37 = load i32, i32* %frame_number, align 4
  store i32 %37, i32* %frame_num, align 4
  %38 = load i32, i32* %mf_needed, align 4
  %39 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %39, i32 0, i32 19
  %40 = load i32, i32* %mf_size, align 4
  %sub31 = sub nsw i32 %38, %40
  store i32 %sub31, i32* %bunch, align 4
  %41 = load double, double* %resample_ratio, align 8
  %42 = load i32, i32* %bunch, align 4
  %conv32 = sitofp i32 %42 to double
  %mul33 = fmul double %conv32, %41
  %conv34 = fptosi double %mul33 to i32
  store i32 %conv34, i32* %bunch, align 4
  %43 = load i32, i32* %bunch, align 4
  %cmp35 = icmp sgt i32 %43, 1152
  br i1 %cmp35, label %if.then37, label %if.end38

if.then37:                                        ; preds = %while.body
  store i32 1152, i32* %bunch, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %while.body
  %44 = load i32, i32* %bunch, align 4
  %cmp39 = icmp slt i32 %44, 1
  br i1 %cmp39, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.end38
  store i32 1, i32* %bunch, align 4
  br label %if.end42

if.end42:                                         ; preds = %if.then41, %if.end38
  %45 = load i32, i32* %mp3buffer_size.addr, align 4
  %46 = load i32, i32* %mp3count, align 4
  %sub43 = sub nsw i32 %45, %46
  store i32 %sub43, i32* %mp3buffer_size_remaining, align 4
  %47 = load i32, i32* %mp3buffer_size.addr, align 4
  %cmp44 = icmp eq i32 %47, 0
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %if.end42
  store i32 0, i32* %mp3buffer_size_remaining, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %if.end42
  %48 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arrayidx = getelementptr inbounds [2 x [1152 x i16]], [2 x [1152 x i16]]* %buffer, i32 0, i32 0
  %arraydecay48 = getelementptr inbounds [1152 x i16], [1152 x i16]* %arrayidx, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [2 x [1152 x i16]], [2 x [1152 x i16]]* %buffer, i32 0, i32 1
  %arraydecay50 = getelementptr inbounds [1152 x i16], [1152 x i16]* %arrayidx49, i32 0, i32 0
  %49 = load i32, i32* %bunch, align 4
  %50 = load i8*, i8** %mp3buffer.addr, align 4
  %51 = load i32, i32* %mp3buffer_size_remaining, align 4
  %call51 = call i32 @lame_encode_buffer(%struct.lame_global_struct* %48, i16* %arraydecay48, i16* %arraydecay50, i32 %49, i8* %50, i32 %51)
  store i32 %call51, i32* %imp3, align 4
  %52 = load i32, i32* %imp3, align 4
  %53 = load i8*, i8** %mp3buffer.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %53, i32 %52
  store i8* %add.ptr, i8** %mp3buffer.addr, align 4
  %54 = load i32, i32* %imp3, align 4
  %55 = load i32, i32* %mp3count, align 4
  %add52 = add nsw i32 %55, %54
  store i32 %add52, i32* %mp3count, align 4
  %56 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc53 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %56, i32 0, i32 12
  %frame_number54 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc53, i32 0, i32 3
  %57 = load i32, i32* %frame_number54, align 4
  %58 = load i32, i32* %frame_num, align 4
  %sub55 = sub nsw i32 %57, %58
  store i32 %sub55, i32* %new_frames, align 4
  %59 = load i32, i32* %new_frames, align 4
  %cmp56 = icmp sgt i32 %59, 0
  br i1 %cmp56, label %if.then58, label %if.end60

if.then58:                                        ; preds = %if.end47
  %60 = load i32, i32* %new_frames, align 4
  %61 = load i32, i32* %frames_left, align 4
  %sub59 = sub nsw i32 %61, %60
  store i32 %sub59, i32* %frames_left, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then58, %if.end47
  br label %while.cond

while.end:                                        ; preds = %land.end
  %62 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode61 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %62, i32 0, i32 18
  store i32 0, i32* %mf_samples_to_encode61, align 8
  %63 = load i32, i32* %imp3, align 4
  %cmp62 = icmp slt i32 %63, 0
  br i1 %cmp62, label %if.then64, label %if.end65

if.then64:                                        ; preds = %while.end
  %64 = load i32, i32* %imp3, align 4
  store i32 %64, i32* %retval, align 4
  br label %return

if.end65:                                         ; preds = %while.end
  %65 = load i32, i32* %mp3buffer_size.addr, align 4
  %66 = load i32, i32* %mp3count, align 4
  %sub66 = sub nsw i32 %65, %66
  store i32 %sub66, i32* %mp3buffer_size_remaining, align 4
  %67 = load i32, i32* %mp3buffer_size.addr, align 4
  %cmp67 = icmp eq i32 %67, 0
  br i1 %cmp67, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.end65
  store i32 2147483647, i32* %mp3buffer_size_remaining, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then69, %if.end65
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @flush_bitstream(%struct.lame_internal_flags* %68)
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %70 = load i8*, i8** %mp3buffer.addr, align 4
  %71 = load i32, i32* %mp3buffer_size_remaining, align 4
  %call71 = call i32 @copy_buffer(%struct.lame_internal_flags* %69, i8* %70, i32 %71, i32 1)
  store i32 %call71, i32* %imp3, align 4
  %72 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @save_gain_values(%struct.lame_internal_flags* %72)
  %73 = load i32, i32* %imp3, align 4
  %cmp72 = icmp slt i32 %73, 0
  br i1 %cmp72, label %if.then74, label %if.end75

if.then74:                                        ; preds = %if.end70
  %74 = load i32, i32* %imp3, align 4
  store i32 %74, i32* %retval, align 4
  br label %return

if.end75:                                         ; preds = %if.end70
  %75 = load i32, i32* %imp3, align 4
  %76 = load i8*, i8** %mp3buffer.addr, align 4
  %add.ptr76 = getelementptr inbounds i8, i8* %76, i32 %75
  store i8* %add.ptr76, i8** %mp3buffer.addr, align 4
  %77 = load i32, i32* %imp3, align 4
  %78 = load i32, i32* %mp3count, align 4
  %add77 = add nsw i32 %78, %77
  store i32 %add77, i32* %mp3count, align 4
  %79 = load i32, i32* %mp3buffer_size.addr, align 4
  %80 = load i32, i32* %mp3count, align 4
  %sub78 = sub nsw i32 %79, %80
  store i32 %sub78, i32* %mp3buffer_size_remaining, align 4
  %81 = load i32, i32* %mp3buffer_size.addr, align 4
  %cmp79 = icmp eq i32 %81, 0
  br i1 %cmp79, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.end75
  store i32 2147483647, i32* %mp3buffer_size_remaining, align 4
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %if.end75
  %82 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_id3tag_automatic = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %82, i32 0, i32 17
  %83 = load i32, i32* %write_id3tag_automatic, align 4
  %tobool83 = icmp ne i32 %83, 0
  br i1 %tobool83, label %if.then84, label %if.end92

if.then84:                                        ; preds = %if.end82
  %84 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call85 = call i32 @id3tag_write_v1(%struct.lame_global_struct* %84)
  %85 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %86 = load i8*, i8** %mp3buffer.addr, align 4
  %87 = load i32, i32* %mp3buffer_size_remaining, align 4
  %call86 = call i32 @copy_buffer(%struct.lame_internal_flags* %85, i8* %86, i32 %87, i32 0)
  store i32 %call86, i32* %imp3, align 4
  %88 = load i32, i32* %imp3, align 4
  %cmp87 = icmp slt i32 %88, 0
  br i1 %cmp87, label %if.then89, label %if.end90

if.then89:                                        ; preds = %if.then84
  %89 = load i32, i32* %imp3, align 4
  store i32 %89, i32* %retval, align 4
  br label %return

if.end90:                                         ; preds = %if.then84
  %90 = load i32, i32* %imp3, align 4
  %91 = load i32, i32* %mp3count, align 4
  %add91 = add nsw i32 %91, %90
  store i32 %add91, i32* %mp3count, align 4
  br label %if.end92

if.end92:                                         ; preds = %if.end90, %if.end82
  %92 = load i32, i32* %mp3count, align 4
  store i32 %92, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end92, %if.then89, %if.then74, %if.then64, %if.then6, %if.then3, %if.then
  %93 = load i32, i32* %retval, align 4
  ret i32 %93
}

; Function Attrs: noinline nounwind optnone
define internal i32 @calcNeeded(%struct.SessionConfig_t* %cfg) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %mf_needed = alloca i32, align 4
  %pcm_samples_per_frame = alloca i32, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 15
  %1 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %1
  store i32 %mul, i32* %pcm_samples_per_frame, align 4
  %2 = load i32, i32* %pcm_samples_per_frame, align 4
  %add = add nsw i32 1024, %2
  %sub = sub nsw i32 %add, 272
  store i32 %sub, i32* %mf_needed, align 4
  %3 = load i32, i32* %mf_needed, align 4
  %4 = load i32, i32* %pcm_samples_per_frame, align 4
  %add1 = add nsw i32 512, %4
  %sub2 = sub nsw i32 %add1, 32
  %cmp = icmp sgt i32 %3, %sub2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load i32, i32* %mf_needed, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load i32, i32* %pcm_samples_per_frame, align 4
  %add3 = add nsw i32 512, %6
  %sub4 = sub nsw i32 %add3, 32
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %5, %cond.true ], [ %sub4, %cond.false ]
  store i32 %cond, i32* %mf_needed, align 4
  %7 = load i32, i32* %mf_needed, align 4
  ret i32 %7
}

declare i32 @id3tag_write_v1(%struct.lame_global_struct*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_close(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ret = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 0, i32* %ret, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tobool = icmp ne %struct.lame_global_struct* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end15

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %class_id = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 0
  %2 = load i32, i32* %class_id, align 4
  %cmp = icmp eq i32 %2, -487877
  br i1 %cmp, label %if.then, label %if.end15

if.then:                                          ; preds = %land.lhs.true
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 70
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %4, %struct.lame_internal_flags** %gfc, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %class_id1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 0
  store i32 0, i32* %class_id1, align 4
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp2 = icmp eq %struct.lame_internal_flags* null, %6
  br i1 %cmp2, label %if.then5, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %class_id3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 0
  %8 = load i32, i32* %class_id3, align 8
  %cmp4 = icmp ne i32 %8, -487877
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %lor.lhs.false, %if.then
  store i32 -3, i32* %ret, align 4
  br label %if.end

if.end:                                           ; preds = %if.then5, %lor.lhs.false
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp6 = icmp ne %struct.lame_internal_flags* null, %9
  br i1 %cmp6, label %if.then7, label %if.end10

if.then7:                                         ; preds = %if.end
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %lame_init_params_successful = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 1
  store i32 0, i32* %lame_init_params_successful, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %class_id8 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 0
  store i32 0, i32* %class_id8, align 8
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @freegfc(%struct.lame_internal_flags* %12)
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags9 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %13, i32 0, i32 70
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %internal_flags9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then7, %if.end
  %14 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lame_allocated_gfp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %14, i32 0, i32 69
  %15 = load i32, i32* %lame_allocated_gfp, align 4
  %tobool11 = icmp ne i32 %15, 0
  br i1 %tobool11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %if.end10
  %16 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lame_allocated_gfp13 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %16, i32 0, i32 69
  store i32 0, i32* %lame_allocated_gfp13, align 4
  %17 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %18 = bitcast %struct.lame_global_struct* %17 to i8*
  call void @free(i8* %18)
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %if.end10
  br label %if.end15

if.end15:                                         ; preds = %if.end14, %land.lhs.true, %entry
  %19 = load i32, i32* %ret, align 4
  ret i32 %19
}

declare void @freegfc(%struct.lame_internal_flags*) #1

declare void @free(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_finish(%struct.lame_global_struct* %gfp, i8* %mp3buffer, i32 %mp3buffer_size) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %mp3buffer.addr = alloca i8*, align 4
  %mp3buffer_size.addr = alloca i32, align 4
  %ret = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %mp3buffer, i8** %mp3buffer.addr, align 4
  store i32 %mp3buffer_size, i32* %mp3buffer_size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i8*, i8** %mp3buffer.addr, align 4
  %2 = load i32, i32* %mp3buffer_size.addr, align 4
  %call = call i32 @lame_encode_flush(%struct.lame_global_struct* %0, i8* %1, i32 %2)
  store i32 %call, i32* %ret, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1 = call i32 @lame_close(%struct.lame_global_struct* %3)
  %4 = load i32, i32* %ret, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_mp3_tags_fid(%struct.lame_global_struct* %gfp, %struct._IO_FILE* %fpStream) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %fpStream.addr = alloca %struct._IO_FILE*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %rc = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct._IO_FILE* %fpStream, %struct._IO_FILE** %fpStream.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end16

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.end4, label %if.then3

if.then3:                                         ; preds = %if.end
  br label %if.end16

if.end4:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg5, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 35
  %6 = load i32, i32* %write_lame_tag, align 4
  %tobool6 = icmp ne i32 %6, 0
  br i1 %tobool6, label %if.end8, label %if.then7

if.then7:                                         ; preds = %if.end4
  br label %if.end16

if.end8:                                          ; preds = %if.end4
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %tobool9 = icmp ne %struct._IO_FILE* %7, null
  br i1 %tobool9, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %if.end8
  %8 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call10 = call i32 @fseek(%struct._IO_FILE* %8, i32 0, i32 0)
  %tobool11 = icmp ne i32 %call10, 0
  br i1 %tobool11, label %if.end16, label %if.then12

if.then12:                                        ; preds = %land.lhs.true
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call13 = call i32 @PutVbrTag(%struct.lame_global_struct* %9, %struct._IO_FILE* %10)
  store i32 %call13, i32* %rc, align 4
  %11 = load i32, i32* %rc, align 4
  switch i32 %11, label %sw.default [
    i32 -1, label %sw.bb
    i32 -2, label %sw.bb14
    i32 -3, label %sw.bb15
  ]

sw.default:                                       ; preds = %if.then12
  br label %sw.epilog

sw.bb:                                            ; preds = %if.then12
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %12, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.88, i32 0, i32 0))
  br label %sw.epilog

sw.bb14:                                          ; preds = %if.then12
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %13, i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.89, i32 0, i32 0))
  br label %sw.epilog

sw.bb15:                                          ; preds = %if.then12
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %14, i8* getelementptr inbounds ([54 x i8], [54 x i8]* @.str.90, i32 0, i32 0))
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb15, %sw.bb14, %sw.bb, %sw.default
  br label %if.end16

if.end16:                                         ; preds = %if.then, %if.then3, %if.then7, %sw.epilog, %land.lhs.true, %if.end8
  ret void
}

declare i32 @fseek(%struct._IO_FILE*, i32, i32) #1

declare i32 @PutVbrTag(%struct.lame_global_struct*, %struct._IO_FILE*) #1

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define hidden %struct.lame_global_struct* @lame_init() #0 {
entry:
  %retval = alloca %struct.lame_global_struct*, align 4
  %gfp = alloca %struct.lame_global_struct*, align 4
  %ret = alloca i32, align 4
  call void @init_log_table()
  %call = call i8* @calloc(i32 1, i32 304)
  %0 = bitcast i8* %call to %struct.lame_global_struct*
  store %struct.lame_global_struct* %0, %struct.lame_global_struct** %gfp, align 4
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %cmp = icmp eq %struct.lame_global_struct* %1, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.lame_global_struct* null, %struct.lame_global_struct** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %call1 = call i32 @lame_init_old(%struct.lame_global_struct* %2)
  store i32 %call1, i32* %ret, align 4
  %3 = load i32, i32* %ret, align 4
  %cmp2 = icmp ne i32 %3, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %5 = bitcast %struct.lame_global_struct* %4 to i8*
  call void @free(i8* %5)
  store %struct.lame_global_struct* null, %struct.lame_global_struct** %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  %lame_allocated_gfp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %6, i32 0, i32 69
  store i32 1, i32* %lame_allocated_gfp, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp, align 4
  store %struct.lame_global_struct* %7, %struct.lame_global_struct** %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %retval, align 4
  ret %struct.lame_global_struct* %8
}

declare void @init_log_table() #1

declare i8* @calloc(i32, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @lame_init_old(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  call void @disable_FPE()
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = bitcast %struct.lame_global_struct* %0 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %1, i8 0, i32 304, i1 false)
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %class_id = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 0
  store i32 -487877, i32* %class_id, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %strict_ISO = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %3, i32 0, i32 31
  store i32 2, i32* %strict_ISO, align 4
  %4 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %mode = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %4, i32 0, i32 12
  store i32 4, i32* %mode, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %original = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %5, i32 0, i32 27
  store i32 1, i32* %original, align 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %samplerate_in = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %6, i32 0, i32 3
  store i32 44100, i32* %samplerate_in, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_channels = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %7, i32 0, i32 2
  store i32 2, i32* %num_channels, align 4
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %8, i32 0, i32 1
  store i32 -1, i32* %num_samples, align 4
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_lame_tag = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %9, i32 0, i32 9
  store i32 1, i32* %write_lame_tag, align 4
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %10, i32 0, i32 11
  store i32 -1, i32* %quality, align 4
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %short_blocks = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %11, i32 0, i32 60
  store i32 -1, i32* %short_blocks, align 4
  %12 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %12, i32 0, i32 22
  store i32 -1, i32* %subblock_gain, align 4
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %13, i32 0, i32 46
  store i32 0, i32* %lowpassfreq, align 4
  %14 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpassfreq = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %14, i32 0, i32 47
  store i32 0, i32* %highpassfreq, align 4
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %lowpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %15, i32 0, i32 48
  store i32 -1, i32* %lowpasswidth, align 4
  %16 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %highpasswidth = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %16, i32 0, i32 49
  store i32 -1, i32* %highpasswidth, align 4
  %17 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %17, i32 0, i32 39
  store i32 0, i32* %VBR, align 4
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %18, i32 0, i32 41
  store i32 4, i32* %VBR_q, align 4
  %19 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_mean_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %19, i32 0, i32 42
  store i32 128, i32* %VBR_mean_bitrate_kbps, align 4
  %20 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_min_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %20, i32 0, i32 43
  store i32 0, i32* %VBR_min_bitrate_kbps, align 4
  %21 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_max_bitrate_kbps = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %21, i32 0, i32 44
  store i32 0, i32* %VBR_max_bitrate_kbps, align 4
  %22 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_hard_min = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %22, i32 0, i32 45
  store i32 0, i32* %VBR_hard_min, align 4
  %23 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %23, i32 0, i32 33
  store i32 -1, i32* %quant_comp, align 4
  %24 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quant_comp_short = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %24, i32 0, i32 34
  store i32 -1, i32* %quant_comp_short, align 4
  %25 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %msfix = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %25, i32 0, i32 63
  store float -1.000000e+00, float* %msfix, align 4
  %26 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %26, i32 0, i32 66
  store float -1.000000e+00, float* %attackthre, align 4
  %27 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %attackthre_s = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %27, i32 0, i32 67
  store float -1.000000e+00, float* %attackthre_s, align 4
  %28 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %28, i32 0, i32 5
  store float 1.000000e+00, float* %scale, align 4
  %29 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_left = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %29, i32 0, i32 6
  store float 1.000000e+00, float* %scale_left, align 4
  %30 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %scale_right = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %30, i32 0, i32 7
  store float 1.000000e+00, float* %scale_right, align 4
  %31 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHcurve = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %31, i32 0, i32 56
  store float -1.000000e+00, float* %ATHcurve, align 4
  %32 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %ATHtype = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %32, i32 0, i32 55
  store i32 -1, i32* %ATHtype, align 4
  %33 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_sensitivity = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %33, i32 0, i32 59
  store float 0.000000e+00, float* %athaa_sensitivity, align 4
  %34 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %athaa_type = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %34, i32 0, i32 58
  store i32 -1, i32* %athaa_type, align 4
  %35 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %useTemporal = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %35, i32 0, i32 61
  store i32 -1, i32* %useTemporal, align 4
  %36 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %interChRatio = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %36, i32 0, i32 62
  store float -1.000000e+00, float* %interChRatio, align 4
  %37 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %findReplayGain = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %37, i32 0, i32 15
  store i32 0, i32* %findReplayGain, align 4
  %38 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %decode_on_the_fly = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %38, i32 0, i32 16
  store i32 0, i32* %decode_on_the_fly, align 4
  %39 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %39, i32 0, i32 71
  %mmx = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations, i32 0, i32 0
  store i32 1, i32* %mmx, align 4
  %40 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations1 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %40, i32 0, i32 71
  %amd3dnow = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations1, i32 0, i32 1
  store i32 1, i32* %amd3dnow, align 4
  %41 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %asm_optimizations2 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %41, i32 0, i32 71
  %sse = getelementptr inbounds %struct.anon.3, %struct.anon.3* %asm_optimizations2, i32 0, i32 2
  store i32 1, i32* %sse, align 4
  %42 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %preset = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %42, i32 0, i32 38
  store i32 0, i32* %preset, align 4
  %43 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %write_id3tag_automatic = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %43, i32 0, i32 17
  store i32 1, i32* %write_id3tag_automatic, align 4
  %44 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %44, i32 0, i32 68
  %debugf = getelementptr inbounds %struct.anon, %struct.anon* %report, i32 0, i32 1
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %debugf, align 4
  %45 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report3 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %45, i32 0, i32 68
  %errorf = getelementptr inbounds %struct.anon, %struct.anon* %report3, i32 0, i32 2
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %errorf, align 4
  %46 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %report4 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %46, i32 0, i32 68
  %msgf = getelementptr inbounds %struct.anon, %struct.anon* %report4, i32 0, i32 0
  store void (i8*, i8*)* @lame_report_def, void (i8*, i8*)** %msgf, align 4
  %call = call i8* @calloc(i32 1, i32 85840)
  %47 = bitcast i8* %call to %struct.lame_internal_flags*
  %48 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %48, i32 0, i32 70
  store %struct.lame_internal_flags* %47, %struct.lame_internal_flags** %internal_flags, align 4
  %49 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags5 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %49, i32 0, i32 70
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags5, align 4
  %call6 = call i32 @lame_init_internal_flags(%struct.lame_internal_flags* %50)
  %cmp = icmp slt i32 %call6, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %51 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags7 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %51, i32 0, i32 70
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags7, align 4
  call void @freegfc(%struct.lame_internal_flags* %52)
  %53 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags8 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %53, i32 0, i32 70
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %internal_flags8, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %54 = load i32, i32* %retval, align 4
  ret i32 %54
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_bitrate_kbps(%struct.lame_global_struct* %gfp, i32* %bitrate_kbps) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %bitrate_kbps.addr = alloca i32*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %i = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %bitrate_kbps, i32** %bitrate_kbps.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end18

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 34
  %6 = load i32, i32* %free_format, align 4
  %tobool5 = icmp ne i32 %6, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then3
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %7 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %7, 14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load i32*, i32** %bitrate_kbps.addr, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %8, i32 %9
  store i32 -1, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 26
  %12 = load i32, i32* %avg_bitrate, align 4
  %13 = load i32*, i32** %bitrate_kbps.addr, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %13, i32 0
  store i32 %12, i32* %arrayidx7, align 4
  br label %if.end

if.else:                                          ; preds = %if.then3
  store i32 0, i32* %i, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc14, %if.else
  %14 = load i32, i32* %i, align 4
  %cmp9 = icmp slt i32 %14, 14
  br i1 %cmp9, label %for.body10, label %for.end16

for.body10:                                       ; preds = %for.cond8
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 0
  %16 = load i32, i32* %version, align 4
  %arrayidx11 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %16
  %17 = load i32, i32* %i, align 4
  %add = add nsw i32 %17, 1
  %arrayidx12 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx11, i32 0, i32 %add
  %18 = load i32, i32* %arrayidx12, align 4
  %19 = load i32*, i32** %bitrate_kbps.addr, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds i32, i32* %19, i32 %20
  store i32 %18, i32* %arrayidx13, align 4
  br label %for.inc14

for.inc14:                                        ; preds = %for.body10
  %21 = load i32, i32* %i, align 4
  %inc15 = add nsw i32 %21, 1
  store i32 %inc15, i32* %i, align 4
  br label %for.cond8

for.end16:                                        ; preds = %for.cond8
  br label %if.end

if.end:                                           ; preds = %for.end16, %for.end
  br label %if.end17

if.end17:                                         ; preds = %if.end, %if.then
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_bitrate_hist(%struct.lame_global_struct* %gfp, i32* %bitrate_count) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %bitrate_count.addr = alloca i32*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %i = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %bitrate_count, i32** %bitrate_count.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end21

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end20

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 34
  %7 = load i32, i32* %free_format, align 4
  %tobool5 = icmp ne i32 %7, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then3
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then6
  %8 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %8, 14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load i32*, i32** %bitrate_count.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %9, i32 %10
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %12, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx7, i32 0, i32 4
  %13 = load i32, i32* %arrayidx8, align 4
  %14 = load i32*, i32** %bitrate_count.addr, align 4
  %arrayidx9 = getelementptr inbounds i32, i32* %14, i32 0
  store i32 %13, i32* %arrayidx9, align 4
  br label %if.end

if.else:                                          ; preds = %if.then3
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc17, %if.else
  %15 = load i32, i32* %i, align 4
  %cmp11 = icmp slt i32 %15, 14
  br i1 %cmp11, label %for.body12, label %for.end19

for.body12:                                       ; preds = %for.cond10
  %16 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist13 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %16, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %add = add nsw i32 %17, 1
  %arrayidx14 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist13, i32 0, i32 %add
  %arrayidx15 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx14, i32 0, i32 4
  %18 = load i32, i32* %arrayidx15, align 4
  %19 = load i32*, i32** %bitrate_count.addr, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %19, i32 %20
  store i32 %18, i32* %arrayidx16, align 4
  br label %for.inc17

for.inc17:                                        ; preds = %for.body12
  %21 = load i32, i32* %i, align 4
  %inc18 = add nsw i32 %21, 1
  store i32 %inc18, i32* %i, align 4
  br label %for.cond10

for.end19:                                        ; preds = %for.cond10
  br label %if.end

if.end:                                           ; preds = %for.end19, %for.end
  br label %if.end20

if.end20:                                         ; preds = %if.end, %if.then
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_stereo_mode_hist(%struct.lame_global_struct* %gfp, i32* %stmode_count) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %stmode_count.addr = alloca i32*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %i = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %stmode_count, i32** %stmode_count.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %5 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %5, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 0
  %arrayidx = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist, i32 0, i32 15
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx4, align 4
  %9 = load i32*, i32** %stmode_count.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %9, i32 %10
  store i32 %8, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_bitrate_stereo_mode_hist(%struct.lame_global_struct* %gfp, [4 x i32]* %bitrate_stmode_count) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %bitrate_stmode_count.addr = alloca [4 x i32]*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store [4 x i32]* %bitrate_stmode_count, [4 x i32]** %bitrate_stmode_count.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end42

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end41

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 34
  %7 = load i32, i32* %free_format, align 4
  %tobool5 = icmp ne i32 %7, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then3
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %if.then6
  %8 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %8, 14
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %i, align 4
  %cmp8 = icmp slt i32 %9, 4
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %10 = load [4 x i32]*, [4 x i32]** %bitrate_stmode_count.addr, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [4 x i32], [4 x i32]* %10, i32 %11
  %12 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx, i32 0, i32 %12
  store i32 0, i32* %arrayidx10, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %14 = load i32, i32* %j, align 4
  %inc12 = add nsw i32 %14, 1
  store i32 %inc12, i32* %j, align 4
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc21, %for.end13
  %15 = load i32, i32* %i, align 4
  %cmp15 = icmp slt i32 %15, 4
  br i1 %cmp15, label %for.body16, label %for.end23

for.body16:                                       ; preds = %for.cond14
  %16 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %16, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx17, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx18, align 4
  %19 = load [4 x i32]*, [4 x i32]** %bitrate_stmode_count.addr, align 4
  %arrayidx19 = getelementptr inbounds [4 x i32], [4 x i32]* %19, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx19, i32 0, i32 %20
  store i32 %18, i32* %arrayidx20, align 4
  br label %for.inc21

for.inc21:                                        ; preds = %for.body16
  %21 = load i32, i32* %i, align 4
  %inc22 = add nsw i32 %21, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond14

for.end23:                                        ; preds = %for.cond14
  br label %if.end

if.else:                                          ; preds = %if.then3
  store i32 0, i32* %j, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc38, %if.else
  %22 = load i32, i32* %j, align 4
  %cmp25 = icmp slt i32 %22, 14
  br i1 %cmp25, label %for.body26, label %for.end40

for.body26:                                       ; preds = %for.cond24
  store i32 0, i32* %i, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc35, %for.body26
  %23 = load i32, i32* %i, align 4
  %cmp28 = icmp slt i32 %23, 4
  br i1 %cmp28, label %for.body29, label %for.end37

for.body29:                                       ; preds = %for.cond27
  %24 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist30 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %24, i32 0, i32 0
  %25 = load i32, i32* %j, align 4
  %add = add nsw i32 %25, 1
  %arrayidx31 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist30, i32 0, i32 %add
  %26 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx31, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx32, align 4
  %28 = load [4 x i32]*, [4 x i32]** %bitrate_stmode_count.addr, align 4
  %29 = load i32, i32* %j, align 4
  %arrayidx33 = getelementptr inbounds [4 x i32], [4 x i32]* %28, i32 %29
  %30 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx33, i32 0, i32 %30
  store i32 %27, i32* %arrayidx34, align 4
  br label %for.inc35

for.inc35:                                        ; preds = %for.body29
  %31 = load i32, i32* %i, align 4
  %inc36 = add nsw i32 %31, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond27

for.end37:                                        ; preds = %for.cond27
  br label %for.inc38

for.inc38:                                        ; preds = %for.end37
  %32 = load i32, i32* %j, align 4
  %inc39 = add nsw i32 %32, 1
  store i32 %inc39, i32* %j, align 4
  br label %for.cond24

for.end40:                                        ; preds = %for.cond24
  br label %if.end

if.end:                                           ; preds = %for.end40, %for.end23
  br label %if.end41

if.end41:                                         ; preds = %if.end, %if.then
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_block_type_hist(%struct.lame_global_struct* %gfp, i32* %btype_count) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %btype_count.addr = alloca i32*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %i = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32* %btype_count, i32** %btype_count.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %5 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %5, 6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 1
  %arrayidx = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist, i32 0, i32 15
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx4, align 4
  %9 = load i32*, i32** %btype_count.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %9, i32 %10
  store i32 %8, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @lame_bitrate_block_type_hist(%struct.lame_global_struct* %gfp, [6 x i32]* %bitrate_btype_count) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %bitrate_btype_count.addr = alloca [6 x i32]*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store [6 x i32]* %bitrate_btype_count, [6 x i32]** %bitrate_btype_count.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_global_flags_valid(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end42

if.then:                                          ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %3)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end41

if.then3:                                         ; preds = %if.then
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg4, %struct.SessionConfig_t** %cfg, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 34
  %7 = load i32, i32* %free_format, align 4
  %tobool5 = icmp ne i32 %7, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then3
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc11, %if.then6
  %8 = load i32, i32* %j, align 4
  %cmp = icmp slt i32 %8, 14
  br i1 %cmp, label %for.body, label %for.end13

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %i, align 4
  %cmp8 = icmp slt i32 %9, 6
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %10 = load [6 x i32]*, [6 x i32]** %bitrate_btype_count.addr, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [6 x i32], [6 x i32]* %10, i32 %11
  %12 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx, i32 0, i32 %12
  store i32 0, i32* %arrayidx10, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body9
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc11

for.inc11:                                        ; preds = %for.end
  %14 = load i32, i32* %j, align 4
  %inc12 = add nsw i32 %14, 1
  store i32 %inc12, i32* %j, align 4
  br label %for.cond

for.end13:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc21, %for.end13
  %15 = load i32, i32* %i, align 4
  %cmp15 = icmp slt i32 %15, 6
  br i1 %cmp15, label %for.body16, label %for.end23

for.body16:                                       ; preds = %for.cond14
  %16 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %16, i32 0, i32 1
  %arrayidx17 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist, i32 0, i32 0
  %17 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx17, i32 0, i32 %17
  %18 = load i32, i32* %arrayidx18, align 4
  %19 = load [6 x i32]*, [6 x i32]** %bitrate_btype_count.addr, align 4
  %arrayidx19 = getelementptr inbounds [6 x i32], [6 x i32]* %19, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx19, i32 0, i32 %20
  store i32 %18, i32* %arrayidx20, align 4
  br label %for.inc21

for.inc21:                                        ; preds = %for.body16
  %21 = load i32, i32* %i, align 4
  %inc22 = add nsw i32 %21, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond14

for.end23:                                        ; preds = %for.cond14
  br label %if.end

if.else:                                          ; preds = %if.then3
  store i32 0, i32* %j, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc38, %if.else
  %22 = load i32, i32* %j, align 4
  %cmp25 = icmp slt i32 %22, 14
  br i1 %cmp25, label %for.body26, label %for.end40

for.body26:                                       ; preds = %for.cond24
  store i32 0, i32* %i, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc35, %for.body26
  %23 = load i32, i32* %i, align 4
  %cmp28 = icmp slt i32 %23, 6
  br i1 %cmp28, label %for.body29, label %for.end37

for.body29:                                       ; preds = %for.cond27
  %24 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist30 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %24, i32 0, i32 1
  %25 = load i32, i32* %j, align 4
  %add = add nsw i32 %25, 1
  %arrayidx31 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist30, i32 0, i32 %add
  %26 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx31, i32 0, i32 %26
  %27 = load i32, i32* %arrayidx32, align 4
  %28 = load [6 x i32]*, [6 x i32]** %bitrate_btype_count.addr, align 4
  %29 = load i32, i32* %j, align 4
  %arrayidx33 = getelementptr inbounds [6 x i32], [6 x i32]* %28, i32 %29
  %30 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx33, i32 0, i32 %30
  store i32 %27, i32* %arrayidx34, align 4
  br label %for.inc35

for.inc35:                                        ; preds = %for.body29
  %31 = load i32, i32* %i, align 4
  %inc36 = add nsw i32 %31, 1
  store i32 %inc36, i32* %i, align 4
  br label %for.cond27

for.end37:                                        ; preds = %for.cond27
  br label %for.inc38

for.inc38:                                        ; preds = %for.end37
  %32 = load i32, i32* %j, align 4
  %inc39 = add nsw i32 %32, 1
  store i32 %inc39, i32* %j, align 4
  br label %for.cond24

for.end40:                                        ; preds = %for.cond24
  br label %if.end

if.end:                                           ; preds = %for.end40, %for.end23
  br label %if.end41

if.end41:                                         ; preds = %if.end, %if.then
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %entry
  ret void
}

declare i32 @nearestBitrateFullIndex(i16 zeroext) #1

; Function Attrs: noinline nounwind optnone
define internal float @filter_coef(float %x) #0 {
entry:
  %retval = alloca float, align 4
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %conv = fpext float %0 to double
  %cmp = fcmp ogt double %conv, 1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load float, float* %x.addr, align 4
  %conv2 = fpext float %1 to double
  %cmp3 = fcmp ole double %conv2, 0.000000e+00
  br i1 %cmp3, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %2 = load float, float* %x.addr, align 4
  %conv7 = fpext float %2 to double
  %mul = fmul double 0x3FF921FB54442D18, %conv7
  %3 = call double @llvm.cos.f64(double %mul)
  %conv8 = fptrunc double %3 to float
  store float %conv8, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then5, %if.then
  %4 = load float, float* %retval, align 4
  ret float %4
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #2

declare i8* @strcat(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @update_inbuffer_size(%struct.lame_internal_flags* %gfc, i32 %nsamples) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %nsamples.addr = alloca i32, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %1 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_0 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %1, i32 0, i32 15
  %2 = load float*, float** %in_buffer_0, align 8
  %cmp = icmp eq float* %2, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_nsamples = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %3, i32 0, i32 14
  %4 = load i32, i32* %in_buffer_nsamples, align 4
  %5 = load i32, i32* %nsamples.addr, align 4
  %cmp1 = icmp slt i32 %4, %5
  br i1 %cmp1, label %if.then, label %if.end13

if.then:                                          ; preds = %lor.lhs.false, %entry
  %6 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_02 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %6, i32 0, i32 15
  %7 = load float*, float** %in_buffer_02, align 8
  %tobool = icmp ne float* %7, null
  br i1 %tobool, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %8 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_04 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %8, i32 0, i32 15
  %9 = load float*, float** %in_buffer_04, align 8
  %10 = bitcast float* %9 to i8*
  call void @free(i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %11 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_1 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %11, i32 0, i32 16
  %12 = load float*, float** %in_buffer_1, align 4
  %tobool5 = icmp ne float* %12, null
  br i1 %tobool5, label %if.then6, label %if.end8

if.then6:                                         ; preds = %if.end
  %13 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_17 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %13, i32 0, i32 16
  %14 = load float*, float** %in_buffer_17, align 4
  %15 = bitcast float* %14 to i8*
  call void @free(i8* %15)
  br label %if.end8

if.end8:                                          ; preds = %if.then6, %if.end
  %16 = load i32, i32* %nsamples.addr, align 4
  %call = call i8* @calloc(i32 %16, i32 4)
  %17 = bitcast i8* %call to float*
  %18 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_09 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %18, i32 0, i32 15
  store float* %17, float** %in_buffer_09, align 8
  %19 = load i32, i32* %nsamples.addr, align 4
  %call10 = call i8* @calloc(i32 %19, i32 4)
  %20 = bitcast i8* %call10 to float*
  %21 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_111 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %21, i32 0, i32 16
  store float* %20, float** %in_buffer_111, align 4
  %22 = load i32, i32* %nsamples.addr, align 4
  %23 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_nsamples12 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %23, i32 0, i32 14
  store i32 %22, i32* %in_buffer_nsamples12, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.end8, %lor.lhs.false
  %24 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_014 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %24, i32 0, i32 15
  %25 = load float*, float** %in_buffer_014, align 8
  %cmp15 = icmp eq float* %25, null
  br i1 %cmp15, label %if.then19, label %lor.lhs.false16

lor.lhs.false16:                                  ; preds = %if.end13
  %26 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_117 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %26, i32 0, i32 16
  %27 = load float*, float** %in_buffer_117, align 4
  %cmp18 = icmp eq float* %27, null
  br i1 %cmp18, label %if.then19, label %if.end33

if.then19:                                        ; preds = %lor.lhs.false16, %if.end13
  %28 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_020 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %28, i32 0, i32 15
  %29 = load float*, float** %in_buffer_020, align 8
  %tobool21 = icmp ne float* %29, null
  br i1 %tobool21, label %if.then22, label %if.end24

if.then22:                                        ; preds = %if.then19
  %30 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_023 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %30, i32 0, i32 15
  %31 = load float*, float** %in_buffer_023, align 8
  %32 = bitcast float* %31 to i8*
  call void @free(i8* %32)
  br label %if.end24

if.end24:                                         ; preds = %if.then22, %if.then19
  %33 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_125 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %33, i32 0, i32 16
  %34 = load float*, float** %in_buffer_125, align 4
  %tobool26 = icmp ne float* %34, null
  br i1 %tobool26, label %if.then27, label %if.end29

if.then27:                                        ; preds = %if.end24
  %35 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_128 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %35, i32 0, i32 16
  %36 = load float*, float** %in_buffer_128, align 4
  %37 = bitcast float* %36 to i8*
  call void @free(i8* %37)
  br label %if.end29

if.end29:                                         ; preds = %if.then27, %if.end24
  %38 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_030 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %38, i32 0, i32 15
  store float* null, float** %in_buffer_030, align 8
  %39 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_131 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %39, i32 0, i32 16
  store float* null, float** %in_buffer_131, align 4
  %40 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_nsamples32 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %40, i32 0, i32 14
  store i32 0, i32* %in_buffer_nsamples32, align 4
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %41, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str.92, i32 0, i32 0))
  store i32 -2, i32* %retval, align 4
  br label %return

if.end33:                                         ; preds = %lor.lhs.false16
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end33, %if.end29
  %42 = load i32, i32* %retval, align 4
  ret i32 %42
}

; Function Attrs: noinline nounwind optnone
define internal void @lame_copy_inbuffer(%struct.lame_internal_flags* %gfc, i8* %l, i8* %r, i32 %nsamples, i32 %pcm_type, i32 %jump, float %s) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %l.addr = alloca i8*, align 4
  %r.addr = alloca i8*, align 4
  %nsamples.addr = alloca i32, align 4
  %pcm_type.addr = alloca i32, align 4
  %jump.addr = alloca i32, align 4
  %s.addr = alloca float, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %ib0 = alloca float*, align 4
  %ib1 = alloca float*, align 4
  %m = alloca [2 x [2 x float]], align 16
  %bl = alloca i16*, align 4
  %br = alloca i16*, align 4
  %i = alloca i32, align 4
  %xl = alloca float, align 4
  %xr = alloca float, align 4
  %u = alloca float, align 4
  %v = alloca float, align 4
  %bl41 = alloca i32*, align 4
  %br42 = alloca i32*, align 4
  %i43 = alloca i32, align 4
  %xl48 = alloca float, align 4
  %xr50 = alloca float, align 4
  %u52 = alloca float, align 4
  %v60 = alloca float, align 4
  %bl76 = alloca i32*, align 4
  %br77 = alloca i32*, align 4
  %i78 = alloca i32, align 4
  %xl83 = alloca float, align 4
  %xr85 = alloca float, align 4
  %u87 = alloca float, align 4
  %v95 = alloca float, align 4
  %bl111 = alloca float*, align 4
  %br112 = alloca float*, align 4
  %i113 = alloca i32, align 4
  %xl118 = alloca float, align 4
  %xr119 = alloca float, align 4
  %u120 = alloca float, align 4
  %v128 = alloca float, align 4
  %bl144 = alloca double*, align 4
  %br145 = alloca double*, align 4
  %i146 = alloca i32, align 4
  %xl151 = alloca float, align 4
  %xr153 = alloca float, align 4
  %u155 = alloca float, align 4
  %v163 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %l, i8** %l.addr, align 4
  store i8* %r, i8** %r.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i32 %pcm_type, i32* %pcm_type.addr, align 4
  store i32 %jump, i32* %jump.addr, align 4
  store float %s, float* %s.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_0 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %2, i32 0, i32 15
  %3 = load float*, float** %in_buffer_0, align 8
  store float* %3, float** %ib0, align 4
  %4 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_1 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %4, i32 0, i32 16
  %5 = load float*, float** %in_buffer_1, align 4
  store float* %5, float** %ib1, align 4
  %6 = load float, float* %s.addr, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 62
  %arrayidx = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx, i32 0, i32 0
  %8 = load float, float* %arrayidx2, align 4
  %mul = fmul float %6, %8
  %arrayidx3 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx3, i32 0, i32 0
  store float %mul, float* %arrayidx4, align 16
  %9 = load float, float* %s.addr, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 62
  %arrayidx6 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform5, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx6, i32 0, i32 1
  %11 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %9, %11
  %arrayidx9 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx9, i32 0, i32 1
  store float %mul8, float* %arrayidx10, align 4
  %12 = load float, float* %s.addr, align 4
  %13 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform11 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %13, i32 0, i32 62
  %arrayidx12 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform11, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx12, i32 0, i32 0
  %14 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %12, %14
  %arrayidx15 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx15, i32 0, i32 0
  store float %mul14, float* %arrayidx16, align 8
  %15 = load float, float* %s.addr, align 4
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %pcm_transform17 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 62
  %arrayidx18 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pcm_transform17, i32 0, i32 1
  %arrayidx19 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx18, i32 0, i32 1
  %17 = load float, float* %arrayidx19, align 4
  %mul20 = fmul float %15, %17
  %arrayidx21 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx21, i32 0, i32 1
  store float %mul20, float* %arrayidx22, align 4
  %18 = load i32, i32* %pcm_type.addr, align 4
  switch i32 %18, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb40
    i32 2, label %sw.bb75
    i32 3, label %sw.bb110
    i32 4, label %sw.bb143
  ]

sw.bb:                                            ; preds = %entry
  %19 = load i8*, i8** %l.addr, align 4
  %20 = bitcast i8* %19 to i16*
  store i16* %20, i16** %bl, align 4
  %21 = load i8*, i8** %r.addr, align 4
  %22 = bitcast i8* %21 to i16*
  store i16* %22, i16** %br, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb
  %23 = load i32, i32* %i, align 4
  %24 = load i32, i32* %nsamples.addr, align 4
  %cmp = icmp slt i32 %23, %24
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load i16*, i16** %bl, align 4
  %26 = load i16, i16* %25, align 2
  %conv = sitofp i16 %26 to float
  store float %conv, float* %xl, align 4
  %27 = load i16*, i16** %br, align 4
  %28 = load i16, i16* %27, align 2
  %conv23 = sitofp i16 %28 to float
  store float %conv23, float* %xr, align 4
  %29 = load float, float* %xl, align 4
  %arrayidx24 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx24, i32 0, i32 0
  %30 = load float, float* %arrayidx25, align 16
  %mul26 = fmul float %29, %30
  %31 = load float, float* %xr, align 4
  %arrayidx27 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx27, i32 0, i32 1
  %32 = load float, float* %arrayidx28, align 4
  %mul29 = fmul float %31, %32
  %add = fadd float %mul26, %mul29
  store float %add, float* %u, align 4
  %33 = load float, float* %xl, align 4
  %arrayidx30 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx31 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx30, i32 0, i32 0
  %34 = load float, float* %arrayidx31, align 8
  %mul32 = fmul float %33, %34
  %35 = load float, float* %xr, align 4
  %arrayidx33 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx34 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx33, i32 0, i32 1
  %36 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %35, %36
  %add36 = fadd float %mul32, %mul35
  store float %add36, float* %v, align 4
  %37 = load float, float* %u, align 4
  %38 = load float*, float** %ib0, align 4
  %39 = load i32, i32* %i, align 4
  %arrayidx37 = getelementptr inbounds float, float* %38, i32 %39
  store float %37, float* %arrayidx37, align 4
  %40 = load float, float* %v, align 4
  %41 = load float*, float** %ib1, align 4
  %42 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds float, float* %41, i32 %42
  store float %40, float* %arrayidx38, align 4
  %43 = load i32, i32* %jump.addr, align 4
  %44 = load i16*, i16** %bl, align 4
  %add.ptr = getelementptr inbounds i16, i16* %44, i32 %43
  store i16* %add.ptr, i16** %bl, align 4
  %45 = load i32, i32* %jump.addr, align 4
  %46 = load i16*, i16** %br, align 4
  %add.ptr39 = getelementptr inbounds i16, i16* %46, i32 %45
  store i16* %add.ptr39, i16** %br, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %47 = load i32, i32* %i, align 4
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.bb40:                                          ; preds = %entry
  %48 = load i8*, i8** %l.addr, align 4
  %49 = bitcast i8* %48 to i32*
  store i32* %49, i32** %bl41, align 4
  %50 = load i8*, i8** %r.addr, align 4
  %51 = bitcast i8* %50 to i32*
  store i32* %51, i32** %br42, align 4
  store i32 0, i32* %i43, align 4
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc72, %sw.bb40
  %52 = load i32, i32* %i43, align 4
  %53 = load i32, i32* %nsamples.addr, align 4
  %cmp45 = icmp slt i32 %52, %53
  br i1 %cmp45, label %for.body47, label %for.end74

for.body47:                                       ; preds = %for.cond44
  %54 = load i32*, i32** %bl41, align 4
  %55 = load i32, i32* %54, align 4
  %conv49 = sitofp i32 %55 to float
  store float %conv49, float* %xl48, align 4
  %56 = load i32*, i32** %br42, align 4
  %57 = load i32, i32* %56, align 4
  %conv51 = sitofp i32 %57 to float
  store float %conv51, float* %xr50, align 4
  %58 = load float, float* %xl48, align 4
  %arrayidx53 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx53, i32 0, i32 0
  %59 = load float, float* %arrayidx54, align 16
  %mul55 = fmul float %58, %59
  %60 = load float, float* %xr50, align 4
  %arrayidx56 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx57 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx56, i32 0, i32 1
  %61 = load float, float* %arrayidx57, align 4
  %mul58 = fmul float %60, %61
  %add59 = fadd float %mul55, %mul58
  store float %add59, float* %u52, align 4
  %62 = load float, float* %xl48, align 4
  %arrayidx61 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx62 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx61, i32 0, i32 0
  %63 = load float, float* %arrayidx62, align 8
  %mul63 = fmul float %62, %63
  %64 = load float, float* %xr50, align 4
  %arrayidx64 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx65 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx64, i32 0, i32 1
  %65 = load float, float* %arrayidx65, align 4
  %mul66 = fmul float %64, %65
  %add67 = fadd float %mul63, %mul66
  store float %add67, float* %v60, align 4
  %66 = load float, float* %u52, align 4
  %67 = load float*, float** %ib0, align 4
  %68 = load i32, i32* %i43, align 4
  %arrayidx68 = getelementptr inbounds float, float* %67, i32 %68
  store float %66, float* %arrayidx68, align 4
  %69 = load float, float* %v60, align 4
  %70 = load float*, float** %ib1, align 4
  %71 = load i32, i32* %i43, align 4
  %arrayidx69 = getelementptr inbounds float, float* %70, i32 %71
  store float %69, float* %arrayidx69, align 4
  %72 = load i32, i32* %jump.addr, align 4
  %73 = load i32*, i32** %bl41, align 4
  %add.ptr70 = getelementptr inbounds i32, i32* %73, i32 %72
  store i32* %add.ptr70, i32** %bl41, align 4
  %74 = load i32, i32* %jump.addr, align 4
  %75 = load i32*, i32** %br42, align 4
  %add.ptr71 = getelementptr inbounds i32, i32* %75, i32 %74
  store i32* %add.ptr71, i32** %br42, align 4
  br label %for.inc72

for.inc72:                                        ; preds = %for.body47
  %76 = load i32, i32* %i43, align 4
  %inc73 = add nsw i32 %76, 1
  store i32 %inc73, i32* %i43, align 4
  br label %for.cond44

for.end74:                                        ; preds = %for.cond44
  br label %sw.epilog

sw.bb75:                                          ; preds = %entry
  %77 = load i8*, i8** %l.addr, align 4
  %78 = bitcast i8* %77 to i32*
  store i32* %78, i32** %bl76, align 4
  %79 = load i8*, i8** %r.addr, align 4
  %80 = bitcast i8* %79 to i32*
  store i32* %80, i32** %br77, align 4
  store i32 0, i32* %i78, align 4
  br label %for.cond79

for.cond79:                                       ; preds = %for.inc107, %sw.bb75
  %81 = load i32, i32* %i78, align 4
  %82 = load i32, i32* %nsamples.addr, align 4
  %cmp80 = icmp slt i32 %81, %82
  br i1 %cmp80, label %for.body82, label %for.end109

for.body82:                                       ; preds = %for.cond79
  %83 = load i32*, i32** %bl76, align 4
  %84 = load i32, i32* %83, align 4
  %conv84 = sitofp i32 %84 to float
  store float %conv84, float* %xl83, align 4
  %85 = load i32*, i32** %br77, align 4
  %86 = load i32, i32* %85, align 4
  %conv86 = sitofp i32 %86 to float
  store float %conv86, float* %xr85, align 4
  %87 = load float, float* %xl83, align 4
  %arrayidx88 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx89 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx88, i32 0, i32 0
  %88 = load float, float* %arrayidx89, align 16
  %mul90 = fmul float %87, %88
  %89 = load float, float* %xr85, align 4
  %arrayidx91 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx92 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx91, i32 0, i32 1
  %90 = load float, float* %arrayidx92, align 4
  %mul93 = fmul float %89, %90
  %add94 = fadd float %mul90, %mul93
  store float %add94, float* %u87, align 4
  %91 = load float, float* %xl83, align 4
  %arrayidx96 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx97 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx96, i32 0, i32 0
  %92 = load float, float* %arrayidx97, align 8
  %mul98 = fmul float %91, %92
  %93 = load float, float* %xr85, align 4
  %arrayidx99 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx100 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx99, i32 0, i32 1
  %94 = load float, float* %arrayidx100, align 4
  %mul101 = fmul float %93, %94
  %add102 = fadd float %mul98, %mul101
  store float %add102, float* %v95, align 4
  %95 = load float, float* %u87, align 4
  %96 = load float*, float** %ib0, align 4
  %97 = load i32, i32* %i78, align 4
  %arrayidx103 = getelementptr inbounds float, float* %96, i32 %97
  store float %95, float* %arrayidx103, align 4
  %98 = load float, float* %v95, align 4
  %99 = load float*, float** %ib1, align 4
  %100 = load i32, i32* %i78, align 4
  %arrayidx104 = getelementptr inbounds float, float* %99, i32 %100
  store float %98, float* %arrayidx104, align 4
  %101 = load i32, i32* %jump.addr, align 4
  %102 = load i32*, i32** %bl76, align 4
  %add.ptr105 = getelementptr inbounds i32, i32* %102, i32 %101
  store i32* %add.ptr105, i32** %bl76, align 4
  %103 = load i32, i32* %jump.addr, align 4
  %104 = load i32*, i32** %br77, align 4
  %add.ptr106 = getelementptr inbounds i32, i32* %104, i32 %103
  store i32* %add.ptr106, i32** %br77, align 4
  br label %for.inc107

for.inc107:                                       ; preds = %for.body82
  %105 = load i32, i32* %i78, align 4
  %inc108 = add nsw i32 %105, 1
  store i32 %inc108, i32* %i78, align 4
  br label %for.cond79

for.end109:                                       ; preds = %for.cond79
  br label %sw.epilog

sw.bb110:                                         ; preds = %entry
  %106 = load i8*, i8** %l.addr, align 4
  %107 = bitcast i8* %106 to float*
  store float* %107, float** %bl111, align 4
  %108 = load i8*, i8** %r.addr, align 4
  %109 = bitcast i8* %108 to float*
  store float* %109, float** %br112, align 4
  store i32 0, i32* %i113, align 4
  br label %for.cond114

for.cond114:                                      ; preds = %for.inc140, %sw.bb110
  %110 = load i32, i32* %i113, align 4
  %111 = load i32, i32* %nsamples.addr, align 4
  %cmp115 = icmp slt i32 %110, %111
  br i1 %cmp115, label %for.body117, label %for.end142

for.body117:                                      ; preds = %for.cond114
  %112 = load float*, float** %bl111, align 4
  %113 = load float, float* %112, align 4
  store float %113, float* %xl118, align 4
  %114 = load float*, float** %br112, align 4
  %115 = load float, float* %114, align 4
  store float %115, float* %xr119, align 4
  %116 = load float, float* %xl118, align 4
  %arrayidx121 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx122 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx121, i32 0, i32 0
  %117 = load float, float* %arrayidx122, align 16
  %mul123 = fmul float %116, %117
  %118 = load float, float* %xr119, align 4
  %arrayidx124 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx125 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx124, i32 0, i32 1
  %119 = load float, float* %arrayidx125, align 4
  %mul126 = fmul float %118, %119
  %add127 = fadd float %mul123, %mul126
  store float %add127, float* %u120, align 4
  %120 = load float, float* %xl118, align 4
  %arrayidx129 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx130 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx129, i32 0, i32 0
  %121 = load float, float* %arrayidx130, align 8
  %mul131 = fmul float %120, %121
  %122 = load float, float* %xr119, align 4
  %arrayidx132 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx133 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx132, i32 0, i32 1
  %123 = load float, float* %arrayidx133, align 4
  %mul134 = fmul float %122, %123
  %add135 = fadd float %mul131, %mul134
  store float %add135, float* %v128, align 4
  %124 = load float, float* %u120, align 4
  %125 = load float*, float** %ib0, align 4
  %126 = load i32, i32* %i113, align 4
  %arrayidx136 = getelementptr inbounds float, float* %125, i32 %126
  store float %124, float* %arrayidx136, align 4
  %127 = load float, float* %v128, align 4
  %128 = load float*, float** %ib1, align 4
  %129 = load i32, i32* %i113, align 4
  %arrayidx137 = getelementptr inbounds float, float* %128, i32 %129
  store float %127, float* %arrayidx137, align 4
  %130 = load i32, i32* %jump.addr, align 4
  %131 = load float*, float** %bl111, align 4
  %add.ptr138 = getelementptr inbounds float, float* %131, i32 %130
  store float* %add.ptr138, float** %bl111, align 4
  %132 = load i32, i32* %jump.addr, align 4
  %133 = load float*, float** %br112, align 4
  %add.ptr139 = getelementptr inbounds float, float* %133, i32 %132
  store float* %add.ptr139, float** %br112, align 4
  br label %for.inc140

for.inc140:                                       ; preds = %for.body117
  %134 = load i32, i32* %i113, align 4
  %inc141 = add nsw i32 %134, 1
  store i32 %inc141, i32* %i113, align 4
  br label %for.cond114

for.end142:                                       ; preds = %for.cond114
  br label %sw.epilog

sw.bb143:                                         ; preds = %entry
  %135 = load i8*, i8** %l.addr, align 4
  %136 = bitcast i8* %135 to double*
  store double* %136, double** %bl144, align 4
  %137 = load i8*, i8** %r.addr, align 4
  %138 = bitcast i8* %137 to double*
  store double* %138, double** %br145, align 4
  store i32 0, i32* %i146, align 4
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc175, %sw.bb143
  %139 = load i32, i32* %i146, align 4
  %140 = load i32, i32* %nsamples.addr, align 4
  %cmp148 = icmp slt i32 %139, %140
  br i1 %cmp148, label %for.body150, label %for.end177

for.body150:                                      ; preds = %for.cond147
  %141 = load double*, double** %bl144, align 4
  %142 = load double, double* %141, align 8
  %conv152 = fptrunc double %142 to float
  store float %conv152, float* %xl151, align 4
  %143 = load double*, double** %br145, align 4
  %144 = load double, double* %143, align 8
  %conv154 = fptrunc double %144 to float
  store float %conv154, float* %xr153, align 4
  %145 = load float, float* %xl151, align 4
  %arrayidx156 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx157 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx156, i32 0, i32 0
  %146 = load float, float* %arrayidx157, align 16
  %mul158 = fmul float %145, %146
  %147 = load float, float* %xr153, align 4
  %arrayidx159 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 0
  %arrayidx160 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx159, i32 0, i32 1
  %148 = load float, float* %arrayidx160, align 4
  %mul161 = fmul float %147, %148
  %add162 = fadd float %mul158, %mul161
  store float %add162, float* %u155, align 4
  %149 = load float, float* %xl151, align 4
  %arrayidx164 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx165 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx164, i32 0, i32 0
  %150 = load float, float* %arrayidx165, align 8
  %mul166 = fmul float %149, %150
  %151 = load float, float* %xr153, align 4
  %arrayidx167 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %m, i32 0, i32 1
  %arrayidx168 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx167, i32 0, i32 1
  %152 = load float, float* %arrayidx168, align 4
  %mul169 = fmul float %151, %152
  %add170 = fadd float %mul166, %mul169
  store float %add170, float* %v163, align 4
  %153 = load float, float* %u155, align 4
  %154 = load float*, float** %ib0, align 4
  %155 = load i32, i32* %i146, align 4
  %arrayidx171 = getelementptr inbounds float, float* %154, i32 %155
  store float %153, float* %arrayidx171, align 4
  %156 = load float, float* %v163, align 4
  %157 = load float*, float** %ib1, align 4
  %158 = load i32, i32* %i146, align 4
  %arrayidx172 = getelementptr inbounds float, float* %157, i32 %158
  store float %156, float* %arrayidx172, align 4
  %159 = load i32, i32* %jump.addr, align 4
  %160 = load double*, double** %bl144, align 4
  %add.ptr173 = getelementptr inbounds double, double* %160, i32 %159
  store double* %add.ptr173, double** %bl144, align 4
  %161 = load i32, i32* %jump.addr, align 4
  %162 = load double*, double** %br145, align 4
  %add.ptr174 = getelementptr inbounds double, double* %162, i32 %161
  store double* %add.ptr174, double** %br145, align 4
  br label %for.inc175

for.inc175:                                       ; preds = %for.body150
  %163 = load i32, i32* %i146, align 4
  %inc176 = add nsw i32 %163, 1
  store i32 %inc176, i32* %i146, align 4
  br label %for.cond147

for.end177:                                       ; preds = %for.cond147
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %for.end177, %for.end142, %for.end109, %for.end74, %for.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @lame_encode_buffer_sample_t(%struct.lame_internal_flags* %gfc, i32 %nsamples, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %nsamples.addr = alloca i32, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %pcm_samples_per_frame = alloca i32, align 4
  %mp3size = alloca i32, align 4
  %ret = alloca i32, align 4
  %i = alloca i32, align 4
  %ch = alloca i32, align 4
  %mf_needed = alloca i32, align 4
  %mp3out = alloca i32, align 4
  %mfbuf = alloca [2 x float*], align 4
  %in_buffer = alloca [2 x float*], align 4
  %buf_size = alloca i32, align 4
  %in_buffer_ptr = alloca [2 x float*], align 4
  %n_in = alloca i32, align 4
  %n_out = alloca i32, align 4
  %buf_size56 = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %nsamples, i32* %nsamples.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 15
  %3 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %3
  store i32 %mul, i32* %pcm_samples_per_frame, align 4
  store i32 0, i32* %mp3size, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %class_id = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 0
  %5 = load i32, i32* %class_id, align 8
  %cmp = icmp ne i32 %5, -487877
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %6 = load i32, i32* %nsamples.addr, align 4
  %cmp2 = icmp eq i32 %6, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %7 = load i32, i32* %mp3buf_size.addr, align 4
  %cmp5 = icmp eq i32 %7, 0
  br i1 %cmp5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end4
  br label %cond.end

cond.false:                                       ; preds = %if.end4
  %8 = load i32, i32* %mp3buf_size.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 2147483647, %cond.true ], [ %8, %cond.false ]
  store i32 %cond, i32* %buf_size, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %10 = load i8*, i8** %mp3buf.addr, align 4
  %11 = load i32, i32* %buf_size, align 4
  %call = call i32 @copy_buffer(%struct.lame_internal_flags* %9, i8* %10, i32 %11, i32 0)
  store i32 %call, i32* %mp3out, align 4
  %12 = load i32, i32* %mp3out, align 4
  %cmp6 = icmp slt i32 %12, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %cond.end
  %13 = load i32, i32* %mp3out, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %cond.end
  %14 = load i32, i32* %mp3out, align 4
  %15 = load i8*, i8** %mp3buf.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %15, i32 %14
  store i8* %add.ptr, i8** %mp3buf.addr, align 4
  %16 = load i32, i32* %mp3out, align 4
  %17 = load i32, i32* %mp3size, align 4
  %add = add nsw i32 %17, %16
  store i32 %add, i32* %mp3size, align 4
  %18 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_0 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %18, i32 0, i32 15
  %19 = load float*, float** %in_buffer_0, align 8
  %arrayidx = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 0
  store float* %19, float** %arrayidx, align 4
  %20 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %in_buffer_1 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %20, i32 0, i32 16
  %21 = load float*, float** %in_buffer_1, align 4
  %arrayidx9 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 1
  store float* %21, float** %arrayidx9, align 4
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %call10 = call i32 @calcNeeded(%struct.SessionConfig_t* %22)
  store i32 %call10, i32* %mf_needed, align 4
  %23 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mfbuf11 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %23, i32 0, i32 17
  %arrayidx12 = getelementptr inbounds [2 x [3984 x float]], [2 x [3984 x float]]* %mfbuf11, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3984 x float], [3984 x float]* %arrayidx12, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 0
  store float* %arraydecay, float** %arrayidx13, align 4
  %24 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mfbuf14 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %24, i32 0, i32 17
  %arrayidx15 = getelementptr inbounds [2 x [3984 x float]], [2 x [3984 x float]]* %mfbuf14, i32 0, i32 1
  %arraydecay16 = getelementptr inbounds [3984 x float], [3984 x float]* %arrayidx15, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 1
  store float* %arraydecay16, float** %arrayidx17, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end87, %if.end8
  %25 = load i32, i32* %nsamples.addr, align 4
  %cmp18 = icmp sgt i32 %25, 0
  br i1 %cmp18, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store i32 0, i32* %n_in, align 4
  store i32 0, i32* %n_out, align 4
  %arrayidx19 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 0
  %26 = load float*, float** %arrayidx19, align 4
  %arrayidx20 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer_ptr, i32 0, i32 0
  store float* %26, float** %arrayidx20, align 4
  %arrayidx21 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 1
  %27 = load float*, float** %arrayidx21, align 4
  %arrayidx22 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer_ptr, i32 0, i32 1
  store float* %27, float** %arrayidx22, align 4
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay23 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer_ptr, i32 0, i32 0
  %29 = load i32, i32* %nsamples.addr, align 4
  call void @fill_buffer(%struct.lame_internal_flags* %28, float** %arraydecay23, float** %arrayidx24, i32 %29, i32* %n_in, i32* %n_out)
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 28
  %31 = load i32, i32* %findReplayGain, align 4
  %tobool = icmp ne i32 %31, 0
  br i1 %tobool, label %land.lhs.true, label %if.end36

land.lhs.true:                                    ; preds = %while.body
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %decode_on_the_fly = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 30
  %33 = load i32, i32* %decode_on_the_fly, align 4
  %tobool25 = icmp ne i32 %33, 0
  br i1 %tobool25, label %if.end36, label %if.then26

if.then26:                                        ; preds = %land.lhs.true
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 14
  %rgdata = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg, i32 0, i32 0
  %35 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata, align 4
  %arrayidx27 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 0
  %36 = load float*, float** %arrayidx27, align 4
  %37 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %37, i32 0, i32 19
  %38 = load i32, i32* %mf_size, align 4
  %arrayidx28 = getelementptr inbounds float, float* %36, i32 %38
  %arrayidx29 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 1
  %39 = load float*, float** %arrayidx29, align 4
  %40 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size30 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %40, i32 0, i32 19
  %41 = load i32, i32* %mf_size30, align 4
  %arrayidx31 = getelementptr inbounds float, float* %39, i32 %41
  %42 = load i32, i32* %n_out, align 4
  %43 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %43, i32 0, i32 14
  %44 = load i32, i32* %channels_out, align 4
  %call32 = call i32 @AnalyzeSamples(%struct.replaygain_data* %35, float* %arrayidx28, float* %arrayidx31, i32 %42, i32 %44)
  %cmp33 = icmp eq i32 %call32, 0
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.then26
  store i32 -6, i32* %retval, align 4
  br label %return

if.end35:                                         ; preds = %if.then26
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %land.lhs.true, %while.body
  %45 = load i32, i32* %n_in, align 4
  %46 = load i32, i32* %nsamples.addr, align 4
  %sub = sub nsw i32 %46, %45
  store i32 %sub, i32* %nsamples.addr, align 4
  %47 = load i32, i32* %n_in, align 4
  %arrayidx37 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 0
  %48 = load float*, float** %arrayidx37, align 4
  %add.ptr38 = getelementptr inbounds float, float* %48, i32 %47
  store float* %add.ptr38, float** %arrayidx37, align 4
  %49 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out39 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %49, i32 0, i32 14
  %50 = load i32, i32* %channels_out39, align 4
  %cmp40 = icmp eq i32 %50, 2
  br i1 %cmp40, label %if.then41, label %if.end44

if.then41:                                        ; preds = %if.end36
  %51 = load i32, i32* %n_in, align 4
  %arrayidx42 = getelementptr inbounds [2 x float*], [2 x float*]* %in_buffer, i32 0, i32 1
  %52 = load float*, float** %arrayidx42, align 4
  %add.ptr43 = getelementptr inbounds float, float* %52, i32 %51
  store float* %add.ptr43, float** %arrayidx42, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then41, %if.end36
  %53 = load i32, i32* %n_out, align 4
  %54 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size45 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %54, i32 0, i32 19
  %55 = load i32, i32* %mf_size45, align 4
  %add46 = add nsw i32 %55, %53
  store i32 %add46, i32* %mf_size45, align 4
  %56 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %56, i32 0, i32 18
  %57 = load i32, i32* %mf_samples_to_encode, align 8
  %cmp47 = icmp slt i32 %57, 1
  br i1 %cmp47, label %if.then48, label %if.end50

if.then48:                                        ; preds = %if.end44
  %58 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode49 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %58, i32 0, i32 18
  store i32 1728, i32* %mf_samples_to_encode49, align 8
  br label %if.end50

if.end50:                                         ; preds = %if.then48, %if.end44
  %59 = load i32, i32* %n_out, align 4
  %60 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode51 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %60, i32 0, i32 18
  %61 = load i32, i32* %mf_samples_to_encode51, align 8
  %add52 = add nsw i32 %61, %59
  store i32 %add52, i32* %mf_samples_to_encode51, align 8
  %62 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size53 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %62, i32 0, i32 19
  %63 = load i32, i32* %mf_size53, align 4
  %64 = load i32, i32* %mf_needed, align 4
  %cmp54 = icmp sge i32 %63, %64
  br i1 %cmp54, label %if.then55, label %if.end87

if.then55:                                        ; preds = %if.end50
  %65 = load i32, i32* %mp3buf_size.addr, align 4
  %66 = load i32, i32* %mp3size, align 4
  %sub57 = sub nsw i32 %65, %66
  store i32 %sub57, i32* %buf_size56, align 4
  %67 = load i32, i32* %mp3buf_size.addr, align 4
  %cmp58 = icmp eq i32 %67, 0
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %if.then55
  store i32 2147483647, i32* %buf_size56, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then59, %if.then55
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arrayidx61 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 0
  %69 = load float*, float** %arrayidx61, align 4
  %arrayidx62 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 1
  %70 = load float*, float** %arrayidx62, align 4
  %71 = load i8*, i8** %mp3buf.addr, align 4
  %72 = load i32, i32* %buf_size56, align 4
  %call63 = call i32 @lame_encode_mp3_frame(%struct.lame_internal_flags* %68, float* %69, float* %70, i8* %71, i32 %72)
  store i32 %call63, i32* %ret, align 4
  %73 = load i32, i32* %ret, align 4
  %cmp64 = icmp slt i32 %73, 0
  br i1 %cmp64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %if.end60
  %74 = load i32, i32* %ret, align 4
  store i32 %74, i32* %retval, align 4
  br label %return

if.end66:                                         ; preds = %if.end60
  %75 = load i32, i32* %ret, align 4
  %76 = load i8*, i8** %mp3buf.addr, align 4
  %add.ptr67 = getelementptr inbounds i8, i8* %76, i32 %75
  store i8* %add.ptr67, i8** %mp3buf.addr, align 4
  %77 = load i32, i32* %ret, align 4
  %78 = load i32, i32* %mp3size, align 4
  %add68 = add nsw i32 %78, %77
  store i32 %add68, i32* %mp3size, align 4
  %79 = load i32, i32* %pcm_samples_per_frame, align 4
  %80 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size69 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %80, i32 0, i32 19
  %81 = load i32, i32* %mf_size69, align 4
  %sub70 = sub nsw i32 %81, %79
  store i32 %sub70, i32* %mf_size69, align 4
  %82 = load i32, i32* %pcm_samples_per_frame, align 4
  %83 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_samples_to_encode71 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %83, i32 0, i32 18
  %84 = load i32, i32* %mf_samples_to_encode71, align 8
  %sub72 = sub nsw i32 %84, %82
  store i32 %sub72, i32* %mf_samples_to_encode71, align 8
  store i32 0, i32* %ch, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc84, %if.end66
  %85 = load i32, i32* %ch, align 4
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out73 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 14
  %87 = load i32, i32* %channels_out73, align 4
  %cmp74 = icmp slt i32 %85, %87
  br i1 %cmp74, label %for.body, label %for.end86

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc, %for.body
  %88 = load i32, i32* %i, align 4
  %89 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %mf_size76 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %89, i32 0, i32 19
  %90 = load i32, i32* %mf_size76, align 4
  %cmp77 = icmp slt i32 %88, %90
  br i1 %cmp77, label %for.body78, label %for.end

for.body78:                                       ; preds = %for.cond75
  %91 = load i32, i32* %ch, align 4
  %arrayidx79 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 %91
  %92 = load float*, float** %arrayidx79, align 4
  %93 = load i32, i32* %i, align 4
  %94 = load i32, i32* %pcm_samples_per_frame, align 4
  %add80 = add nsw i32 %93, %94
  %arrayidx81 = getelementptr inbounds float, float* %92, i32 %add80
  %95 = load float, float* %arrayidx81, align 4
  %96 = load i32, i32* %ch, align 4
  %arrayidx82 = getelementptr inbounds [2 x float*], [2 x float*]* %mfbuf, i32 0, i32 %96
  %97 = load float*, float** %arrayidx82, align 4
  %98 = load i32, i32* %i, align 4
  %arrayidx83 = getelementptr inbounds float, float* %97, i32 %98
  store float %95, float* %arrayidx83, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body78
  %99 = load i32, i32* %i, align 4
  %inc = add nsw i32 %99, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond75

for.end:                                          ; preds = %for.cond75
  br label %for.inc84

for.inc84:                                        ; preds = %for.end
  %100 = load i32, i32* %ch, align 4
  %inc85 = add nsw i32 %100, 1
  store i32 %inc85, i32* %ch, align 4
  br label %for.cond

for.end86:                                        ; preds = %for.cond
  br label %if.end87

if.end87:                                         ; preds = %for.end86, %if.end50
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %101 = load i32, i32* %mp3size, align 4
  store i32 %101, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then65, %if.then34, %if.then7, %if.then3, %if.then
  %102 = load i32, i32* %retval, align 4
  ret i32 %102
}

declare void @fill_buffer(%struct.lame_internal_flags*, float**, float**, i32, i32*, i32*) #1

declare i32 @AnalyzeSamples(%struct.replaygain_data*, float*, float*, i32, i32) #1

declare i32 @lame_encode_mp3_frame(%struct.lame_internal_flags*, float*, float*, i8*, i32) #1

declare float @GetTitleGain(%struct.replaygain_data*) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #2

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.ceil.f64(double) #2

declare void @disable_FPE() #1

declare void @lame_report_def(i8*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @lame_init_internal_flags(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cmp = icmp eq %struct.lame_internal_flags* null, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 5
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 24
  store i32 1, i32* %vbr_min_bitrate_index, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg1, i32 0, i32 25
  store i32 13, i32* %vbr_max_bitrate_index, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 5
  %decode_on_the_fly = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg2, i32 0, i32 30
  store i32 0, i32* %decode_on_the_fly, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 5
  %findReplayGain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg3, i32 0, i32 28
  store i32 0, i32* %findReplayGain, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 5
  %findPeakSample = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg4, i32 0, i32 29
  store i32 0, i32* %findPeakSample, align 4
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 13
  %OldValue = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 5
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %OldValue, i32 0, i32 0
  store i32 180, i32* %arrayidx, align 8
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 13
  %OldValue6 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt5, i32 0, i32 5
  %arrayidx7 = getelementptr inbounds [2 x i32], [2 x i32]* %OldValue6, i32 0, i32 1
  store i32 180, i32* %arrayidx7, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt8 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 13
  %CurrentStep = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt8, i32 0, i32 6
  %arrayidx9 = getelementptr inbounds [2 x i32], [2 x i32]* %CurrentStep, i32 0, i32 0
  store i32 4, i32* %arrayidx9, align 8
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 13
  %CurrentStep11 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt10, i32 0, i32 6
  %arrayidx12 = getelementptr inbounds [2 x i32], [2 x i32]* %CurrentStep11, i32 0, i32 1
  store i32 4, i32* %arrayidx12, align 4
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt13 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt13, i32 0, i32 2
  store float 1.000000e+00, float* %masking_lower, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 11
  %mf_samples_to_encode = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 18
  store i32 1728, i32* %mf_samples_to_encode, align 8
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 11
  %mf_size = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc14, i32 0, i32 19
  store i32 528, i32* %mf_size, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 12
  %encoder_padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 7
  store i32 0, i32* %encoder_padding, align 4
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc15 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 12
  %encoder_delay = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc15, i32 0, i32 6
  store i32 576, i32* %encoder_delay, align 8
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 15
  %RadioGain = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 2
  store i32 0, i32* %RadioGain, align 8
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_rpg16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 15
  %noclipGainChange = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg16, i32 0, i32 3
  store i32 0, i32* %noclipGainChange, align 4
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_rpg17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 15
  %noclipScale = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg17, i32 0, i32 0
  store float -1.000000e+00, float* %noclipScale, align 8
  %call = call i8* @calloc(i32 1, i32 2772)
  %18 = bitcast i8* %call to %struct.ATH_t*
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 21
  store %struct.ATH_t* %18, %struct.ATH_t** %ATH, align 8
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH18 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 21
  %21 = load %struct.ATH_t*, %struct.ATH_t** %ATH18, align 8
  %cmp19 = icmp eq %struct.ATH_t* null, %21
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end
  store i32 -2, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end
  %call22 = call i8* @calloc(i32 1, i32 134792)
  %22 = bitcast i8* %call22 to %struct.replaygain_data*
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 14
  %rgdata = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg, i32 0, i32 0
  store %struct.replaygain_data* %22, %struct.replaygain_data** %rgdata, align 4
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg23 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %24, i32 0, i32 14
  %rgdata24 = getelementptr inbounds %struct.RpgStateVar_t, %struct.RpgStateVar_t* %sv_rpg23, i32 0, i32 0
  %25 = load %struct.replaygain_data*, %struct.replaygain_data** %rgdata24, align 4
  %cmp25 = icmp eq %struct.replaygain_data* null, %25
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.end21
  store i32 -2, i32* %retval, align 4
  br label %return

if.end27:                                         ; preds = %if.end21
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end27, %if.then26, %if.then20, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind readnone speculatable willreturn }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
