; ModuleID = 'bitstream.c'
source_filename = "bitstream.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.huffcodetab = type { i32, i32, i16*, i8* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type { [20 x float], float*, [2411 x float], float*, [2411 x float], float*, [20 x float], float*, [2411 x float], float*, [2411 x float], float*, i32, i32, double, double, i32, i32, [12000 x i32], [12000 x i32] }
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque

@bitrate_table = external constant [3 x [16 x i32]], align 16
@.str = private unnamed_addr constant [36 x i8] c"strange error flushing buffer ... \0A\00", align 1
@.str.1 = private unnamed_addr constant [53 x i8] c"Internal buffer inconsistency. flushbits <> ResvSize\00", align 1
@.str.2 = private unnamed_addr constant [279 x i8] c"bit reservoir error: \0Al3_side->main_data_begin: %i \0AResvoir size:             %i \0Aresv drain (post)         %i \0Aresv drain (pre)          %i \0Aheader and sideinfo:      %i \0Adata bits:                %i \0Atotal bits:               %i (remainder: %i) \0Abitsperframe:             %i \0A\00", align 1
@.str.3 = private unnamed_addr constant [56 x i8] c"This is a fatal error.  It has several possible causes:\00", align 1
@.str.4 = private unnamed_addr constant [75 x i8] c"90%%  LAME compiled with buggy version of gcc using advanced optimizations\00", align 1
@.str.5 = private unnamed_addr constant [33 x i8] c" 9%%  Your system is overclocked\00", align 1
@.str.6 = private unnamed_addr constant [35 x i8] c" 1%%  bug in LAME encoding library\00", align 1
@.str.7 = private unnamed_addr constant [49 x i8] c"Error: MAX_HEADER_BUF too small in bitstream.c \0A\00", align 1
@slen1_tab = external constant [16 x i32], align 16
@slen2_tab = external constant [16 x i32], align 16
@ht = external constant [34 x %struct.huffcodetab], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @getframebits(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %bit_rate = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %2, i32 0, i32 2
  %3 = load i32, i32* %bitrate_index, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 0
  %5 = load i32, i32* %version, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %5
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index2 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 2
  %7 = load i32, i32* %bitrate_index2, align 4
  %arrayidx3 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx3, align 4
  store i32 %8, i32* %bit_rate, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 26
  %10 = load i32, i32* %avg_bitrate, align 4
  store i32 %10, i32* %bit_rate, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %12 = load i32, i32* %bit_rate, align 4
  %13 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %13, i32 0, i32 4
  %14 = load i32, i32* %padding, align 4
  %call = call i32 @calcFrameLength(%struct.SessionConfig_t* %11, i32 %12, i32 %14)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define internal i32 @calcFrameLength(%struct.SessionConfig_t* %cfg, i32 %kbps, i32 %pad) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %kbps.addr = alloca i32, align 4
  %pad.addr = alloca i32, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  store i32 %kbps, i32* %kbps.addr, align 4
  store i32 %pad, i32* %pad.addr, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 0
  %1 = load i32, i32* %version, align 4
  %add = add nsw i32 %1, 1
  %mul = mul nsw i32 %add, 72000
  %2 = load i32, i32* %kbps.addr, align 4
  %mul1 = mul nsw i32 %mul, %2
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 12
  %4 = load i32, i32* %samplerate_out, align 4
  %div = sdiv i32 %mul1, %4
  %5 = load i32, i32* %pad.addr, align 4
  %add2 = add nsw i32 %div, %5
  %mul3 = mul nsw i32 8, %add2
  ret i32 %mul3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @get_max_frame_buffer_size_by_constraint(%struct.SessionConfig_t* %cfg, i32 %constraint) #0 {
entry:
  %cfg.addr = alloca %struct.SessionConfig_t*, align 4
  %constraint.addr = alloca i32, align 4
  %maxmp3buf = alloca i32, align 4
  %max_kbps = alloca i32, align 4
  store %struct.SessionConfig_t* %cfg, %struct.SessionConfig_t** %cfg.addr, align 4
  store i32 %constraint, i32* %constraint.addr, align 4
  store i32 0, i32* %maxmp3buf, align 4
  %0 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %0, i32 0, i32 26
  %1 = load i32, i32* %avg_bitrate, align 4
  %cmp = icmp sgt i32 %1, 320
  br i1 %cmp, label %if.then, label %if.else4

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %constraint.addr, align 4
  %cmp1 = icmp eq i32 %2, 1
  br i1 %cmp1, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %avg_bitrate3 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 26
  %5 = load i32, i32* %avg_bitrate3, align 4
  %call = call i32 @calcFrameLength(%struct.SessionConfig_t* %3, i32 %5, i32 0)
  store i32 %call, i32* %maxmp3buf, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 0
  %7 = load i32, i32* %version, align 4
  %add = add nsw i32 %7, 1
  %mul = mul nsw i32 7680, %add
  store i32 %mul, i32* %maxmp3buf, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  br label %if.end20

if.else4:                                         ; preds = %entry
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 12
  %9 = load i32, i32* %samplerate_out, align 4
  %cmp5 = icmp slt i32 %9, 16000
  br i1 %cmp5, label %if.then6, label %if.else9

if.then6:                                         ; preds = %if.else4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %version7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 0
  %11 = load i32, i32* %version7, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %11
  %arrayidx8 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 8
  %12 = load i32, i32* %arrayidx8, align 16
  store i32 %12, i32* %max_kbps, align 4
  br label %if.end13

if.else9:                                         ; preds = %if.else4
  %13 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %version10 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %13, i32 0, i32 0
  %14 = load i32, i32* %version10, align 4
  %arrayidx11 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %14
  %arrayidx12 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx11, i32 0, i32 14
  %15 = load i32, i32* %arrayidx12, align 8
  store i32 %15, i32* %max_kbps, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else9, %if.then6
  %16 = load i32, i32* %constraint.addr, align 4
  switch i32 %16, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb14
    i32 2, label %sw.bb16
  ]

sw.default:                                       ; preds = %if.end13
  br label %sw.bb

sw.bb:                                            ; preds = %if.end13, %sw.default
  store i32 11520, i32* %maxmp3buf, align 4
  br label %sw.epilog

sw.bb14:                                          ; preds = %if.end13
  %17 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %18 = load i32, i32* %max_kbps, align 4
  %call15 = call i32 @calcFrameLength(%struct.SessionConfig_t* %17, i32 %18, i32 0)
  store i32 %call15, i32* %maxmp3buf, align 4
  br label %sw.epilog

sw.bb16:                                          ; preds = %if.end13
  %19 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg.addr, align 4
  %version17 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %19, i32 0, i32 0
  %20 = load i32, i32* %version17, align 4
  %add18 = add nsw i32 %20, 1
  %mul19 = mul nsw i32 7680, %add18
  store i32 %mul19, i32* %maxmp3buf, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb16, %sw.bb14, %sw.bb
  br label %if.end20

if.end20:                                         ; preds = %sw.epilog, %if.end
  %21 = load i32, i32* %maxmp3buf, align 4
  ret i32 %21
}

; Function Attrs: noinline nounwind optnone
define hidden void @CRC_writeheader(%struct.lame_internal_flags* %gfc, i8* %header) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %header.addr = alloca i8*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %crc = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %header, i8** %header.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 65535, i32* %crc, align 4
  %1 = load i8*, i8** %header.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 2
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %3 = load i32, i32* %crc, align 4
  %call = call i32 @CRC_update(i32 %conv, i32 %3)
  store i32 %call, i32* %crc, align 4
  %4 = load i8*, i8** %header.addr, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %4, i32 3
  %5 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %5 to i32
  %6 = load i32, i32* %crc, align 4
  %call4 = call i32 @CRC_update(i32 %conv3, i32 %6)
  store i32 %call4, i32* %crc, align 4
  store i32 6, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 2
  %9 = load i32, i32* %sideinfo_len, align 4
  %cmp = icmp slt i32 %7, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load i8*, i8** %header.addr, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %10, i32 %11
  %12 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %12 to i32
  %13 = load i32, i32* %crc, align 4
  %call8 = call i32 @CRC_update(i32 %conv7, i32 %13)
  store i32 %call8, i32* %crc, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load i32, i32* %crc, align 4
  %shr = ashr i32 %15, 8
  %conv9 = trunc i32 %shr to i8
  %16 = load i8*, i8** %header.addr, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %16, i32 4
  store i8 %conv9, i8* %arrayidx10, align 1
  %17 = load i32, i32* %crc, align 4
  %and = and i32 %17, 255
  %conv11 = trunc i32 %and to i8
  %18 = load i8*, i8** %header.addr, align 4
  %arrayidx12 = getelementptr inbounds i8, i8* %18, i32 5
  store i8 %conv11, i8* %arrayidx12, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @CRC_update(i32 %value, i32 %crc) #0 {
entry:
  %value.addr = alloca i32, align 4
  %crc.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i32 %value, i32* %value.addr, align 4
  store i32 %crc, i32* %crc.addr, align 4
  %0 = load i32, i32* %value.addr, align 4
  %shl = shl i32 %0, 8
  store i32 %shl, i32* %value.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %value.addr, align 4
  %shl1 = shl i32 %2, 1
  store i32 %shl1, i32* %value.addr, align 4
  %3 = load i32, i32* %crc.addr, align 4
  %shl2 = shl i32 %3, 1
  store i32 %shl2, i32* %crc.addr, align 4
  %4 = load i32, i32* %crc.addr, align 4
  %5 = load i32, i32* %value.addr, align 4
  %xor = xor i32 %4, %5
  %and = and i32 %xor, 65536
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %crc.addr, align 4
  %xor3 = xor i32 %6, 32773
  store i32 %xor3, i32* %crc.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i32, i32* %crc.addr, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @compute_flushbits(%struct.lame_internal_flags* %gfc, i32* %total_bytes_output) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %total_bytes_output.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %flushbits = alloca i32, align 4
  %remaining_headers = alloca i32, align 4
  %bitsPerFrame = alloca i32, align 4
  %last_ptr = alloca i32, align 4
  %first_ptr = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32* %total_bytes_output, i32** %total_bytes_output.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %2, i32 0, i32 10
  %3 = load i32, i32* %w_ptr, align 4
  store i32 %3, i32* %first_ptr, align 4
  %4 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %4, i32 0, i32 9
  %5 = load i32, i32* %h_ptr, align 8
  %sub = sub nsw i32 %5, 1
  store i32 %sub, i32* %last_ptr, align 4
  %6 = load i32, i32* %last_ptr, align 4
  %cmp = icmp eq i32 %6, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 255, i32* %last_ptr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %7, i32 0, i32 8
  %8 = load i32, i32* %last_ptr, align 4
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %8
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 0
  %9 = load i32, i32* %write_timing, align 8
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 6
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs, i32 0, i32 2
  %11 = load i32, i32* %totbit, align 8
  %sub2 = sub nsw i32 %9, %11
  store i32 %sub2, i32* %flushbits, align 4
  %12 = load i32, i32* %flushbits, align 4
  %13 = load i32*, i32** %total_bytes_output.addr, align 4
  store i32 %12, i32* %13, align 4
  %14 = load i32, i32* %flushbits, align 4
  %cmp3 = icmp sge i32 %14, 0
  br i1 %cmp3, label %if.then4, label %if.end14

if.then4:                                         ; preds = %if.end
  %15 = load i32, i32* %last_ptr, align 4
  %add = add nsw i32 1, %15
  %16 = load i32, i32* %first_ptr, align 4
  %sub5 = sub nsw i32 %add, %16
  store i32 %sub5, i32* %remaining_headers, align 4
  %17 = load i32, i32* %last_ptr, align 4
  %18 = load i32, i32* %first_ptr, align 4
  %cmp6 = icmp slt i32 %17, %18
  br i1 %cmp6, label %if.then7, label %if.end11

if.then7:                                         ; preds = %if.then4
  %19 = load i32, i32* %last_ptr, align 4
  %add8 = add nsw i32 1, %19
  %20 = load i32, i32* %first_ptr, align 4
  %sub9 = sub nsw i32 %add8, %20
  %add10 = add nsw i32 %sub9, 256
  store i32 %add10, i32* %remaining_headers, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then7, %if.then4
  %21 = load i32, i32* %remaining_headers, align 4
  %mul = mul nsw i32 %21, 8
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %22, i32 0, i32 2
  %23 = load i32, i32* %sideinfo_len, align 4
  %mul12 = mul nsw i32 %mul, %23
  %24 = load i32, i32* %flushbits, align 4
  %sub13 = sub nsw i32 %24, %mul12
  store i32 %sub13, i32* %flushbits, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.end11, %if.end
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @getframebits(%struct.lame_internal_flags* %25)
  store i32 %call, i32* %bitsPerFrame, align 4
  %26 = load i32, i32* %bitsPerFrame, align 4
  %27 = load i32, i32* %flushbits, align 4
  %add15 = add nsw i32 %27, %26
  store i32 %add15, i32* %flushbits, align 4
  %28 = load i32, i32* %bitsPerFrame, align 4
  %29 = load i32*, i32** %total_bytes_output.addr, align 4
  %30 = load i32, i32* %29, align 4
  %add16 = add nsw i32 %30, %28
  store i32 %add16, i32* %29, align 4
  %31 = load i32*, i32** %total_bytes_output.addr, align 4
  %32 = load i32, i32* %31, align 4
  %rem = srem i32 %32, 8
  %tobool = icmp ne i32 %rem, 0
  br i1 %tobool, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end14
  %33 = load i32*, i32** %total_bytes_output.addr, align 4
  %34 = load i32, i32* %33, align 4
  %div = sdiv i32 %34, 8
  %add18 = add nsw i32 1, %div
  %35 = load i32*, i32** %total_bytes_output.addr, align 4
  store i32 %add18, i32* %35, align 4
  br label %if.end20

if.else:                                          ; preds = %if.end14
  %36 = load i32*, i32** %total_bytes_output.addr, align 4
  %37 = load i32, i32* %36, align 4
  %div19 = sdiv i32 %37, 8
  %38 = load i32*, i32** %total_bytes_output.addr, align 4
  store i32 %div19, i32* %38, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then17
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 6
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs21, i32 0, i32 3
  %40 = load i32, i32* %buf_byte_idx, align 4
  %add22 = add nsw i32 %40, 1
  %41 = load i32*, i32** %total_bytes_output.addr, align 4
  %42 = load i32, i32* %41, align 4
  %add23 = add nsw i32 %42, %add22
  store i32 %add23, i32* %41, align 4
  %43 = load i32, i32* %flushbits, align 4
  %cmp24 = icmp slt i32 %43, 0
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end20
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %44, i8* getelementptr inbounds ([36 x i8], [36 x i8]* @.str, i32 0, i32 0))
  br label %if.end26

if.end26:                                         ; preds = %if.then25, %if.end20
  %45 = load i32, i32* %flushbits, align 4
  ret i32 %45
}

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define hidden void @flush_bitstream(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %nbytes = alloca i32, align 4
  %flushbits = alloca i32, align 4
  %last_ptr = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %1 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %1, i32 0, i32 9
  %2 = load i32, i32* %h_ptr, align 8
  %sub = sub nsw i32 %2, 1
  store i32 %sub, i32* %last_ptr, align 4
  %3 = load i32, i32* %last_ptr, align 4
  %cmp = icmp eq i32 %3, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 255, i32* %last_ptr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side1, %struct.III_side_info_t** %l3_side, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @compute_flushbits(%struct.lame_internal_flags* %5, i32* %nbytes)
  store i32 %call, i32* %flushbits, align 4
  %cmp2 = icmp slt i32 %call, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %7 = load i32, i32* %flushbits, align 4
  call void @drain_into_ancillary(%struct.lame_internal_flags* %6, i32 %7)
  %8 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %8, i32 0, i32 12
  store i32 0, i32* %ResvSize, align 4
  %9 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %9, i32 0, i32 1
  store i32 0, i32* %main_data_begin, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @drain_into_ancillary(%struct.lame_internal_flags* %gfc, i32 %remainingBits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %remainingBits.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %i = alloca i32, align 4
  %version = alloca i8*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %remainingBits, i32* %remainingBits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load i32, i32* %remainingBits.addr, align 4
  %cmp = icmp sge i32 %2, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @putbits2(%struct.lame_internal_flags* %3, i32 76, i32 8)
  %4 = load i32, i32* %remainingBits.addr, align 4
  %sub = sub nsw i32 %4, 8
  store i32 %sub, i32* %remainingBits.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load i32, i32* %remainingBits.addr, align 4
  %cmp2 = icmp sge i32 %5, 8
  br i1 %cmp2, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @putbits2(%struct.lame_internal_flags* %6, i32 65, i32 8)
  %7 = load i32, i32* %remainingBits.addr, align 4
  %sub4 = sub nsw i32 %7, 8
  store i32 %sub4, i32* %remainingBits.addr, align 4
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %8 = load i32, i32* %remainingBits.addr, align 4
  %cmp6 = icmp sge i32 %8, 8
  br i1 %cmp6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end5
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @putbits2(%struct.lame_internal_flags* %9, i32 77, i32 8)
  %10 = load i32, i32* %remainingBits.addr, align 4
  %sub8 = sub nsw i32 %10, 8
  store i32 %sub8, i32* %remainingBits.addr, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.then7, %if.end5
  %11 = load i32, i32* %remainingBits.addr, align 4
  %cmp10 = icmp sge i32 %11, 8
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %if.end9
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @putbits2(%struct.lame_internal_flags* %12, i32 69, i32 8)
  %13 = load i32, i32* %remainingBits.addr, align 4
  %sub12 = sub nsw i32 %13, 8
  store i32 %sub12, i32* %remainingBits.addr, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %if.end9
  %14 = load i32, i32* %remainingBits.addr, align 4
  %cmp14 = icmp sge i32 %14, 32
  br i1 %cmp14, label %if.then15, label %if.end23

if.then15:                                        ; preds = %if.end13
  %call = call i8* @get_lame_short_version()
  store i8* %call, i8** %version, align 4
  %15 = load i32, i32* %remainingBits.addr, align 4
  %cmp16 = icmp sge i32 %15, 32
  br i1 %cmp16, label %if.then17, label %if.end22

if.then17:                                        ; preds = %if.then15
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then17
  %16 = load i32, i32* %i, align 4
  %17 = load i8*, i8** %version, align 4
  %call18 = call i32 @strlen(i8* %17)
  %cmp19 = icmp slt i32 %16, %call18
  br i1 %cmp19, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %18 = load i32, i32* %remainingBits.addr, align 4
  %cmp20 = icmp sge i32 %18, 8
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %19 = phi i1 [ false, %for.cond ], [ %cmp20, %land.rhs ]
  br i1 %19, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %20 = load i32, i32* %remainingBits.addr, align 4
  %sub21 = sub nsw i32 %20, 8
  store i32 %sub21, i32* %remainingBits.addr, align 4
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %22 = load i8*, i8** %version, align 4
  %23 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %22, i32 %23
  %24 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %24 to i32
  call void @putbits2(%struct.lame_internal_flags* %21, i32 %conv, i32 8)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  br label %if.end22

if.end22:                                         ; preds = %for.end, %if.then15
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end13
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc29, %if.end23
  %26 = load i32, i32* %remainingBits.addr, align 4
  %cmp25 = icmp sge i32 %26, 1
  br i1 %cmp25, label %for.body27, label %for.end31

for.body27:                                       ; preds = %for.cond24
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %28 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ancillary_flag = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %28, i32 0, i32 11
  %29 = load i32, i32* %ancillary_flag, align 8
  call void @putbits2(%struct.lame_internal_flags* %27, i32 %29, i32 1)
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %disable_reservoir = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 32
  %31 = load i32, i32* %disable_reservoir, align 4
  %tobool = icmp ne i32 %31, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  %32 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ancillary_flag28 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %32, i32 0, i32 11
  %33 = load i32, i32* %ancillary_flag28, align 8
  %xor = xor i32 %33, %lnot.ext
  store i32 %xor, i32* %ancillary_flag28, align 8
  br label %for.inc29

for.inc29:                                        ; preds = %for.body27
  %34 = load i32, i32* %remainingBits.addr, align 4
  %sub30 = sub nsw i32 %34, 1
  store i32 %sub30, i32* %remainingBits.addr, align 4
  br label %for.cond24

for.end31:                                        ; preds = %for.cond24
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @add_dummy_byte(%struct.lame_internal_flags* %gfc, i8 zeroext %val, i32 %n) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %val.addr = alloca i8, align 1
  %n.addr = alloca i32, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8 %val, i8* %val.addr, align 1
  store i32 %n, i32* %n.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  br label %while.cond

while.cond:                                       ; preds = %for.end, %entry
  %1 = load i32, i32* %n.addr, align 4
  %dec = add i32 %1, -1
  store i32 %dec, i32* %n.addr, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %3 = load i8, i8* %val.addr, align 1
  %conv = zext i8 %3 to i32
  call void @putbits_noheaders(%struct.lame_internal_flags* %2, i32 %conv, i32 8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %while.body
  %4 = load i32, i32* %i, align 4
  %cmp1 = icmp slt i32 %4, 256
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %5, i32 0, i32 8
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %6
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 0
  %7 = load i32, i32* %write_timing, align 8
  %add = add nsw i32 %7, 8
  store i32 %add, i32* %write_timing, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @putbits_noheaders(%struct.lame_internal_flags* %gfc, i32 %val, i32 %j) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %val.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  %bs = alloca %struct.bit_stream_struc*, align 4
  %k = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  store i32 %j, i32* %j.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 6
  store %struct.bit_stream_struc* %bs1, %struct.bit_stream_struc** %bs, align 4
  br label %while.cond

while.cond:                                       ; preds = %cond.end, %entry
  %1 = load i32, i32* %j.addr, align 4
  %cmp = icmp sgt i32 %1, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %2, i32 0, i32 4
  %3 = load i32, i32* %buf_bit_idx, align 4
  %cmp2 = icmp eq i32 %3, 0
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %4 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx3 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %4, i32 0, i32 4
  store i32 8, i32* %buf_bit_idx3, align 4
  %5 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %5, i32 0, i32 3
  %6 = load i32, i32* %buf_byte_idx, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %buf_byte_idx, align 4
  %7 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %7, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4
  %9 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx4 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %9, i32 0, i32 3
  %10 = load i32, i32* %buf_byte_idx4, align 4
  %arrayidx = getelementptr inbounds i8, i8* %8, i32 %10
  store i8 0, i8* %arrayidx, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %while.body
  %11 = load i32, i32* %j.addr, align 4
  %12 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx5 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %12, i32 0, i32 4
  %13 = load i32, i32* %buf_bit_idx5, align 4
  %cmp6 = icmp slt i32 %11, %13
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %14 = load i32, i32* %j.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %15 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx7 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %15, i32 0, i32 4
  %16 = load i32, i32* %buf_bit_idx7, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %14, %cond.true ], [ %16, %cond.false ]
  store i32 %cond, i32* %k, align 4
  %17 = load i32, i32* %k, align 4
  %18 = load i32, i32* %j.addr, align 4
  %sub = sub nsw i32 %18, %17
  store i32 %sub, i32* %j.addr, align 4
  %19 = load i32, i32* %k, align 4
  %20 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx8 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %20, i32 0, i32 4
  %21 = load i32, i32* %buf_bit_idx8, align 4
  %sub9 = sub nsw i32 %21, %19
  store i32 %sub9, i32* %buf_bit_idx8, align 4
  %22 = load i32, i32* %val.addr, align 4
  %23 = load i32, i32* %j.addr, align 4
  %shr = ashr i32 %22, %23
  %24 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx10 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %24, i32 0, i32 4
  %25 = load i32, i32* %buf_bit_idx10, align 4
  %shl = shl i32 %shr, %25
  %26 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf11 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %26, i32 0, i32 0
  %27 = load i8*, i8** %buf11, align 4
  %28 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx12 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %28, i32 0, i32 3
  %29 = load i32, i32* %buf_byte_idx12, align 4
  %arrayidx13 = getelementptr inbounds i8, i8* %27, i32 %29
  %30 = load i8, i8* %arrayidx13, align 1
  %conv = zext i8 %30 to i32
  %or = or i32 %conv, %shl
  %conv14 = trunc i32 %or to i8
  store i8 %conv14, i8* %arrayidx13, align 1
  %31 = load i32, i32* %k, align 4
  %32 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %32, i32 0, i32 2
  %33 = load i32, i32* %totbit, align 4
  %add = add nsw i32 %33, %31
  store i32 %add, i32* %totbit, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @format_bitstream(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %bits = alloca i32, align 4
  %nbytes = alloca i32, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %bitsPerFrame = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @getframebits(%struct.lame_internal_flags* %3)
  store i32 %call, i32* %bitsPerFrame, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %5 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_pre = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %5, i32 0, i32 3
  %6 = load i32, i32* %resvDrain_pre, align 4
  call void @drain_into_ancillary(%struct.lame_internal_flags* %4, i32 %6)
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %8 = load i32, i32* %bitsPerFrame, align 4
  call void @encodeSideInfo2(%struct.lame_internal_flags* %7, i32 %8)
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 2
  %10 = load i32, i32* %sideinfo_len, align 4
  %mul = mul nsw i32 8, %10
  store i32 %mul, i32* %bits, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call3 = call i32 @writeMainData(%struct.lame_internal_flags* %11)
  %12 = load i32, i32* %bits, align 4
  %add = add nsw i32 %12, %call3
  store i32 %add, i32* %bits, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %14 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %14, i32 0, i32 4
  %15 = load i32, i32* %resvDrain_post, align 4
  call void @drain_into_ancillary(%struct.lame_internal_flags* %13, i32 %15)
  %16 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post4 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %16, i32 0, i32 4
  %17 = load i32, i32* %resvDrain_post4, align 4
  %18 = load i32, i32* %bits, align 4
  %add5 = add nsw i32 %18, %17
  store i32 %add5, i32* %bits, align 4
  %19 = load i32, i32* %bitsPerFrame, align 4
  %20 = load i32, i32* %bits, align 4
  %sub = sub nsw i32 %19, %20
  %div = sdiv i32 %sub, 8
  %21 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %21, i32 0, i32 1
  %22 = load i32, i32* %main_data_begin, align 4
  %add6 = add nsw i32 %22, %div
  store i32 %add6, i32* %main_data_begin, align 4
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call7 = call i32 @compute_flushbits(%struct.lame_internal_flags* %23, i32* %nbytes)
  %24 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %24, i32 0, i32 12
  %25 = load i32, i32* %ResvSize, align 4
  %cmp = icmp ne i32 %call7, %25
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %26, i8* getelementptr inbounds ([53 x i8], [53 x i8]* @.str.1, i32 0, i32 0))
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %27 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin8 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %27, i32 0, i32 1
  %28 = load i32, i32* %main_data_begin8, align 4
  %mul9 = mul nsw i32 %28, 8
  %29 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize10 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %29, i32 0, i32 12
  %30 = load i32, i32* %ResvSize10, align 4
  %cmp11 = icmp ne i32 %mul9, %30
  br i1 %cmp11, label %if.then12, label %if.end28

if.then12:                                        ; preds = %if.end
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %32 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin13 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %32, i32 0, i32 1
  %33 = load i32, i32* %main_data_begin13, align 4
  %mul14 = mul nsw i32 8, %33
  %34 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize15 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %34, i32 0, i32 12
  %35 = load i32, i32* %ResvSize15, align 4
  %36 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post16 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %36, i32 0, i32 4
  %37 = load i32, i32* %resvDrain_post16, align 4
  %38 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_pre17 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %38, i32 0, i32 3
  %39 = load i32, i32* %resvDrain_pre17, align 4
  %40 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len18 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %40, i32 0, i32 2
  %41 = load i32, i32* %sideinfo_len18, align 4
  %mul19 = mul nsw i32 8, %41
  %42 = load i32, i32* %bits, align 4
  %43 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post20 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %43, i32 0, i32 4
  %44 = load i32, i32* %resvDrain_post20, align 4
  %sub21 = sub nsw i32 %42, %44
  %45 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len22 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %45, i32 0, i32 2
  %46 = load i32, i32* %sideinfo_len22, align 4
  %mul23 = mul nsw i32 8, %46
  %sub24 = sub nsw i32 %sub21, %mul23
  %47 = load i32, i32* %bits, align 4
  %48 = load i32, i32* %bits, align 4
  %rem = srem i32 %48, 8
  %49 = load i32, i32* %bitsPerFrame, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %31, i8* getelementptr inbounds ([279 x i8], [279 x i8]* @.str.2, i32 0, i32 0), i32 %mul14, i32 %35, i32 %37, i32 %39, i32 %mul19, i32 %sub24, i32 %47, i32 %rem, i32 %49)
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %50, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str.3, i32 0, i32 0))
  %51 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %51, i8* getelementptr inbounds ([75 x i8], [75 x i8]* @.str.4, i32 0, i32 0))
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %52, i8* getelementptr inbounds ([33 x i8], [33 x i8]* @.str.5, i32 0, i32 0))
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %53, i8* getelementptr inbounds ([35 x i8], [35 x i8]* @.str.6, i32 0, i32 0))
  %54 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin25 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %54, i32 0, i32 1
  %55 = load i32, i32* %main_data_begin25, align 4
  %mul26 = mul nsw i32 %55, 8
  %56 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize27 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %56, i32 0, i32 12
  store i32 %mul26, i32* %ResvSize27, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then12, %if.end
  %57 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %57, i32 0, i32 6
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs, i32 0, i32 2
  %58 = load i32, i32* %totbit, align 8
  %cmp29 = icmp sgt i32 %58, 1000000000
  br i1 %cmp29, label %if.then30, label %if.end37

if.then30:                                        ; preds = %if.end28
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then30
  %59 = load i32, i32* %i, align 4
  %cmp31 = icmp slt i32 %59, 256
  br i1 %cmp31, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs32 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %60, i32 0, i32 6
  %totbit33 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs32, i32 0, i32 2
  %61 = load i32, i32* %totbit33, align 8
  %62 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %62, i32 0, i32 8
  %63 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %63
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 0
  %64 = load i32, i32* %write_timing, align 8
  %sub34 = sub nsw i32 %64, %61
  store i32 %sub34, i32* %write_timing, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %65 = load i32, i32* %i, align 4
  %inc = add nsw i32 %65, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 6
  %totbit36 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs35, i32 0, i32 2
  store i32 0, i32* %totbit36, align 8
  br label %if.end37

if.end37:                                         ; preds = %for.end, %if.end28
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define internal void @encodeSideInfo2(%struct.lame_internal_flags* %gfc, i32 %bitsPerFrame) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %bitsPerFrame.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %band = alloca i32, align 4
  %gi = alloca %struct.gr_info*, align 4
  %gi105 = alloca %struct.gr_info*, align 4
  %old = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %bitsPerFrame, i32* %bitsPerFrame.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %4 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %4, i32 0, i32 8
  %5 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %5, i32 0, i32 9
  %6 = load i32, i32* %h_ptr, align 8
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %6
  %ptr = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 1
  store i32 0, i32* %ptr, align 4
  %7 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header3 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %7, i32 0, i32 8
  %8 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr4 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %8, i32 0, i32 9
  %9 = load i32, i32* %h_ptr4, align 8
  %arrayidx5 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header3, i32 0, i32 %9
  %buf = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx5, i32 0, i32 2
  %arraydecay = getelementptr inbounds [40 x i8], [40 x i8]* %buf, i32 0, i32 0
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 2
  %11 = load i32, i32* %sideinfo_len, align 4
  call void @llvm.memset.p0i8.i32(i8* align 8 %arraydecay, i8 0, i32 %11, i1 false)
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 12
  %13 = load i32, i32* %samplerate_out, align 4
  %cmp = icmp slt i32 %13, 16000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %14, i32 4094, i32 12)
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %15, i32 4095, i32 12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %17 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %17, i32 0, i32 0
  %18 = load i32, i32* %version, align 4
  call void @writeheader(%struct.lame_internal_flags* %16, i32 %18, i32 1)
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %19, i32 1, i32 2)
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 36
  %22 = load i32, i32* %error_protection, align 4
  %tobool = icmp ne i32 %22, 0
  %lnot = xor i1 %tobool, true
  %lnot.ext = zext i1 %lnot to i32
  call void @writeheader(%struct.lame_internal_flags* %20, i32 %lnot.ext, i32 1)
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %24 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %24, i32 0, i32 2
  %25 = load i32, i32* %bitrate_index, align 4
  call void @writeheader(%struct.lame_internal_flags* %23, i32 %25, i32 4)
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %27 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %27, i32 0, i32 1
  %28 = load i32, i32* %samplerate_index, align 4
  call void @writeheader(%struct.lame_internal_flags* %26, i32 %28, i32 2)
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %30 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %30, i32 0, i32 4
  %31 = load i32, i32* %padding, align 4
  call void @writeheader(%struct.lame_internal_flags* %29, i32 %31, i32 1)
  %32 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %33 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %extension = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %33, i32 0, i32 39
  %34 = load i32, i32* %extension, align 4
  call void @writeheader(%struct.lame_internal_flags* %32, i32 %34, i32 1)
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %36 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %36, i32 0, i32 41
  %37 = load i32, i32* %mode, align 4
  call void @writeheader(%struct.lame_internal_flags* %35, i32 %37, i32 2)
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %39 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %39, i32 0, i32 5
  %40 = load i32, i32* %mode_ext, align 4
  call void @writeheader(%struct.lame_internal_flags* %38, i32 %40, i32 2)
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %42 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %copyright = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %42, i32 0, i32 37
  %43 = load i32, i32* %copyright, align 4
  call void @writeheader(%struct.lame_internal_flags* %41, i32 %43, i32 1)
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %45 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %original = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %45, i32 0, i32 38
  %46 = load i32, i32* %original, align 4
  call void @writeheader(%struct.lame_internal_flags* %44, i32 %46, i32 1)
  %47 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %48 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %emphasis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %48, i32 0, i32 40
  %49 = load i32, i32* %emphasis, align 4
  call void @writeheader(%struct.lame_internal_flags* %47, i32 %49, i32 2)
  %50 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection6 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %50, i32 0, i32 36
  %51 = load i32, i32* %error_protection6, align 4
  %tobool7 = icmp ne i32 %51, 0
  br i1 %tobool7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %52, i32 0, i32 16)
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.end
  %53 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version10 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %53, i32 0, i32 0
  %54 = load i32, i32* %version10, align 4
  %cmp11 = icmp eq i32 %54, 1
  br i1 %cmp11, label %if.then12, label %if.else97

if.then12:                                        ; preds = %if.end9
  %55 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %56 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %56, i32 0, i32 1
  %57 = load i32, i32* %main_data_begin, align 4
  call void @writeheader(%struct.lame_internal_flags* %55, i32 %57, i32 9)
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 14
  %59 = load i32, i32* %channels_out, align 4
  %cmp13 = icmp eq i32 %59, 2
  br i1 %cmp13, label %if.then14, label %if.else15

if.then14:                                        ; preds = %if.then12
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %61 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %private_bits = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %61, i32 0, i32 2
  %62 = load i32, i32* %private_bits, align 4
  call void @writeheader(%struct.lame_internal_flags* %60, i32 %62, i32 3)
  br label %if.end17

if.else15:                                        ; preds = %if.then12
  %63 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %64 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %private_bits16 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %64, i32 0, i32 2
  %65 = load i32, i32* %private_bits16, align 4
  call void @writeheader(%struct.lame_internal_flags* %63, i32 %65, i32 5)
  br label %if.end17

if.end17:                                         ; preds = %if.else15, %if.then14
  store i32 0, i32* %ch, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.end17
  %66 = load i32, i32* %ch, align 4
  %67 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out18 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %67, i32 0, i32 14
  %68 = load i32, i32* %channels_out18, align 4
  %cmp19 = icmp slt i32 %66, %68
  br i1 %cmp19, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %band, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc, %for.body
  %69 = load i32, i32* %band, align 4
  %cmp21 = icmp slt i32 %69, 4
  br i1 %cmp21, label %for.body22, label %for.end

for.body22:                                       ; preds = %for.cond20
  %70 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %71 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %scfsi = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %71, i32 0, i32 5
  %72 = load i32, i32* %ch, align 4
  %arrayidx23 = getelementptr inbounds [2 x [4 x i32]], [2 x [4 x i32]]* %scfsi, i32 0, i32 %72
  %73 = load i32, i32* %band, align 4
  %arrayidx24 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx23, i32 0, i32 %73
  %74 = load i32, i32* %arrayidx24, align 4
  call void @writeheader(%struct.lame_internal_flags* %70, i32 %74, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body22
  %75 = load i32, i32* %band, align 4
  %inc = add nsw i32 %75, 1
  store i32 %inc, i32* %band, align 4
  br label %for.cond20

for.end:                                          ; preds = %for.cond20
  br label %for.inc25

for.inc25:                                        ; preds = %for.end
  %76 = load i32, i32* %ch, align 4
  %inc26 = add nsw i32 %76, 1
  store i32 %inc26, i32* %ch, align 4
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc94, %for.end27
  %77 = load i32, i32* %gr, align 4
  %cmp29 = icmp slt i32 %77, 2
  br i1 %cmp29, label %for.body30, label %for.end96

for.body30:                                       ; preds = %for.cond28
  store i32 0, i32* %ch, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc91, %for.body30
  %78 = load i32, i32* %ch, align 4
  %79 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out32 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %79, i32 0, i32 14
  %80 = load i32, i32* %channels_out32, align 4
  %cmp33 = icmp slt i32 %78, %80
  br i1 %cmp33, label %for.body34, label %for.end93

for.body34:                                       ; preds = %for.cond31
  %81 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %81, i32 0, i32 0
  %82 = load i32, i32* %gr, align 4
  %arrayidx35 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %82
  %83 = load i32, i32* %ch, align 4
  %arrayidx36 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx35, i32 0, i32 %83
  store %struct.gr_info* %arrayidx36, %struct.gr_info** %gi, align 4
  %84 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %85 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %85, i32 0, i32 4
  %86 = load i32, i32* %part2_3_length, align 4
  %87 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %87, i32 0, i32 18
  %88 = load i32, i32* %part2_length, align 4
  %add = add nsw i32 %86, %88
  call void @writeheader(%struct.lame_internal_flags* %84, i32 %add, i32 12)
  %89 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %90 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %90, i32 0, i32 5
  %91 = load i32, i32* %big_values, align 4
  %div = sdiv i32 %91, 2
  call void @writeheader(%struct.lame_internal_flags* %89, i32 %div, i32 9)
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %93 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %93, i32 0, i32 7
  %94 = load i32, i32* %global_gain, align 4
  call void @writeheader(%struct.lame_internal_flags* %92, i32 %94, i32 8)
  %95 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %96 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %96, i32 0, i32 8
  %97 = load i32, i32* %scalefac_compress, align 4
  call void @writeheader(%struct.lame_internal_flags* %95, i32 %97, i32 4)
  %98 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %98, i32 0, i32 9
  %99 = load i32, i32* %block_type, align 4
  %cmp37 = icmp ne i32 %99, 0
  br i1 %cmp37, label %if.then38, label %if.else62

if.then38:                                        ; preds = %for.body34
  %100 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %100, i32 1, i32 1)
  %101 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %102 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type39 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %102, i32 0, i32 9
  %103 = load i32, i32* %block_type39, align 4
  call void @writeheader(%struct.lame_internal_flags* %101, i32 %103, i32 2)
  %104 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %105 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %105, i32 0, i32 10
  %106 = load i32, i32* %mixed_block_flag, align 4
  call void @writeheader(%struct.lame_internal_flags* %104, i32 %106, i32 1)
  %107 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %107, i32 0, i32 11
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  %108 = load i32, i32* %arrayidx40, align 4
  %cmp41 = icmp eq i32 %108, 14
  br i1 %cmp41, label %if.then42, label %if.end45

if.then42:                                        ; preds = %if.then38
  %109 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select43 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %109, i32 0, i32 11
  %arrayidx44 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select43, i32 0, i32 0
  store i32 16, i32* %arrayidx44, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.then42, %if.then38
  %110 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %111 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select46 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %111, i32 0, i32 11
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select46, i32 0, i32 0
  %112 = load i32, i32* %arrayidx47, align 4
  call void @writeheader(%struct.lame_internal_flags* %110, i32 %112, i32 5)
  %113 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select48 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %113, i32 0, i32 11
  %arrayidx49 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select48, i32 0, i32 1
  %114 = load i32, i32* %arrayidx49, align 4
  %cmp50 = icmp eq i32 %114, 14
  br i1 %cmp50, label %if.then51, label %if.end54

if.then51:                                        ; preds = %if.end45
  %115 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select52 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %115, i32 0, i32 11
  %arrayidx53 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select52, i32 0, i32 1
  store i32 16, i32* %arrayidx53, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.then51, %if.end45
  %116 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %117 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select55 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %117, i32 0, i32 11
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select55, i32 0, i32 1
  %118 = load i32, i32* %arrayidx56, align 4
  call void @writeheader(%struct.lame_internal_flags* %116, i32 %118, i32 5)
  %119 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %120 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %120, i32 0, i32 12
  %arrayidx57 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 0
  %121 = load i32, i32* %arrayidx57, align 4
  call void @writeheader(%struct.lame_internal_flags* %119, i32 %121, i32 3)
  %122 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %123 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %subblock_gain58 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %123, i32 0, i32 12
  %arrayidx59 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain58, i32 0, i32 1
  %124 = load i32, i32* %arrayidx59, align 4
  call void @writeheader(%struct.lame_internal_flags* %122, i32 %124, i32 3)
  %125 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %126 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %subblock_gain60 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %126, i32 0, i32 12
  %arrayidx61 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain60, i32 0, i32 2
  %127 = load i32, i32* %arrayidx61, align 4
  call void @writeheader(%struct.lame_internal_flags* %125, i32 %127, i32 3)
  br label %if.end90

if.else62:                                        ; preds = %for.body34
  %128 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %128, i32 0, i32 1)
  %129 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select63 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %129, i32 0, i32 11
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select63, i32 0, i32 0
  %130 = load i32, i32* %arrayidx64, align 4
  %cmp65 = icmp eq i32 %130, 14
  br i1 %cmp65, label %if.then66, label %if.end69

if.then66:                                        ; preds = %if.else62
  %131 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select67 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %131, i32 0, i32 11
  %arrayidx68 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select67, i32 0, i32 0
  store i32 16, i32* %arrayidx68, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.then66, %if.else62
  %132 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %133 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select70 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %133, i32 0, i32 11
  %arrayidx71 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select70, i32 0, i32 0
  %134 = load i32, i32* %arrayidx71, align 4
  call void @writeheader(%struct.lame_internal_flags* %132, i32 %134, i32 5)
  %135 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select72 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %135, i32 0, i32 11
  %arrayidx73 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select72, i32 0, i32 1
  %136 = load i32, i32* %arrayidx73, align 4
  %cmp74 = icmp eq i32 %136, 14
  br i1 %cmp74, label %if.then75, label %if.end78

if.then75:                                        ; preds = %if.end69
  %137 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select76 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %137, i32 0, i32 11
  %arrayidx77 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select76, i32 0, i32 1
  store i32 16, i32* %arrayidx77, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.then75, %if.end69
  %138 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %139 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select79 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %139, i32 0, i32 11
  %arrayidx80 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select79, i32 0, i32 1
  %140 = load i32, i32* %arrayidx80, align 4
  call void @writeheader(%struct.lame_internal_flags* %138, i32 %140, i32 5)
  %141 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select81 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %141, i32 0, i32 11
  %arrayidx82 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select81, i32 0, i32 2
  %142 = load i32, i32* %arrayidx82, align 4
  %cmp83 = icmp eq i32 %142, 14
  br i1 %cmp83, label %if.then84, label %if.end87

if.then84:                                        ; preds = %if.end78
  %143 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select85 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %143, i32 0, i32 11
  %arrayidx86 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select85, i32 0, i32 2
  store i32 16, i32* %arrayidx86, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.then84, %if.end78
  %144 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %145 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %table_select88 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %145, i32 0, i32 11
  %arrayidx89 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select88, i32 0, i32 2
  %146 = load i32, i32* %arrayidx89, align 4
  call void @writeheader(%struct.lame_internal_flags* %144, i32 %146, i32 5)
  %147 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %148 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %region0_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %148, i32 0, i32 13
  %149 = load i32, i32* %region0_count, align 4
  call void @writeheader(%struct.lame_internal_flags* %147, i32 %149, i32 4)
  %150 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %151 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %region1_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %151, i32 0, i32 14
  %152 = load i32, i32* %region1_count, align 4
  call void @writeheader(%struct.lame_internal_flags* %150, i32 %152, i32 3)
  br label %if.end90

if.end90:                                         ; preds = %if.end87, %if.end54
  %153 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %154 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %154, i32 0, i32 15
  %155 = load i32, i32* %preflag, align 4
  call void @writeheader(%struct.lame_internal_flags* %153, i32 %155, i32 1)
  %156 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %157 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %157, i32 0, i32 16
  %158 = load i32, i32* %scalefac_scale, align 4
  call void @writeheader(%struct.lame_internal_flags* %156, i32 %158, i32 1)
  %159 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %160 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %count1table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %160, i32 0, i32 17
  %161 = load i32, i32* %count1table_select, align 4
  call void @writeheader(%struct.lame_internal_flags* %159, i32 %161, i32 1)
  br label %for.inc91

for.inc91:                                        ; preds = %if.end90
  %162 = load i32, i32* %ch, align 4
  %inc92 = add nsw i32 %162, 1
  store i32 %inc92, i32* %ch, align 4
  br label %for.cond31

for.end93:                                        ; preds = %for.cond31
  br label %for.inc94

for.inc94:                                        ; preds = %for.end93
  %163 = load i32, i32* %gr, align 4
  %inc95 = add nsw i32 %163, 1
  store i32 %inc95, i32* %gr, align 4
  br label %for.cond28

for.end96:                                        ; preds = %for.cond28
  br label %if.end181

if.else97:                                        ; preds = %if.end9
  %164 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %165 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin98 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %165, i32 0, i32 1
  %166 = load i32, i32* %main_data_begin98, align 4
  call void @writeheader(%struct.lame_internal_flags* %164, i32 %166, i32 8)
  %167 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %168 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %private_bits99 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %168, i32 0, i32 2
  %169 = load i32, i32* %private_bits99, align 4
  %170 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out100 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %170, i32 0, i32 14
  %171 = load i32, i32* %channels_out100, align 4
  call void @writeheader(%struct.lame_internal_flags* %167, i32 %169, i32 %171)
  store i32 0, i32* %gr, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond101

for.cond101:                                      ; preds = %for.inc178, %if.else97
  %172 = load i32, i32* %ch, align 4
  %173 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out102 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %173, i32 0, i32 14
  %174 = load i32, i32* %channels_out102, align 4
  %cmp103 = icmp slt i32 %172, %174
  br i1 %cmp103, label %for.body104, label %for.end180

for.body104:                                      ; preds = %for.cond101
  %175 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt106 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %175, i32 0, i32 0
  %176 = load i32, i32* %gr, align 4
  %arrayidx107 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt106, i32 0, i32 %176
  %177 = load i32, i32* %ch, align 4
  %arrayidx108 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx107, i32 0, i32 %177
  store %struct.gr_info* %arrayidx108, %struct.gr_info** %gi105, align 4
  %178 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %179 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %part2_3_length109 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %179, i32 0, i32 4
  %180 = load i32, i32* %part2_3_length109, align 4
  %181 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %part2_length110 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %181, i32 0, i32 18
  %182 = load i32, i32* %part2_length110, align 4
  %add111 = add nsw i32 %180, %182
  call void @writeheader(%struct.lame_internal_flags* %178, i32 %add111, i32 12)
  %183 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %184 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %big_values112 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %184, i32 0, i32 5
  %185 = load i32, i32* %big_values112, align 4
  %div113 = sdiv i32 %185, 2
  call void @writeheader(%struct.lame_internal_flags* %183, i32 %div113, i32 9)
  %186 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %187 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %global_gain114 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %187, i32 0, i32 7
  %188 = load i32, i32* %global_gain114, align 4
  call void @writeheader(%struct.lame_internal_flags* %186, i32 %188, i32 8)
  %189 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %190 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %scalefac_compress115 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %190, i32 0, i32 8
  %191 = load i32, i32* %scalefac_compress115, align 4
  call void @writeheader(%struct.lame_internal_flags* %189, i32 %191, i32 9)
  %192 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %block_type116 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %192, i32 0, i32 9
  %193 = load i32, i32* %block_type116, align 4
  %cmp117 = icmp ne i32 %193, 0
  br i1 %cmp117, label %if.then118, label %if.else145

if.then118:                                       ; preds = %for.body104
  %194 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %194, i32 1, i32 1)
  %195 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %196 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %block_type119 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %196, i32 0, i32 9
  %197 = load i32, i32* %block_type119, align 4
  call void @writeheader(%struct.lame_internal_flags* %195, i32 %197, i32 2)
  %198 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %199 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %mixed_block_flag120 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %199, i32 0, i32 10
  %200 = load i32, i32* %mixed_block_flag120, align 4
  call void @writeheader(%struct.lame_internal_flags* %198, i32 %200, i32 1)
  %201 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select121 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %201, i32 0, i32 11
  %arrayidx122 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select121, i32 0, i32 0
  %202 = load i32, i32* %arrayidx122, align 4
  %cmp123 = icmp eq i32 %202, 14
  br i1 %cmp123, label %if.then124, label %if.end127

if.then124:                                       ; preds = %if.then118
  %203 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select125 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %203, i32 0, i32 11
  %arrayidx126 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select125, i32 0, i32 0
  store i32 16, i32* %arrayidx126, align 4
  br label %if.end127

if.end127:                                        ; preds = %if.then124, %if.then118
  %204 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %205 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select128 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %205, i32 0, i32 11
  %arrayidx129 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select128, i32 0, i32 0
  %206 = load i32, i32* %arrayidx129, align 4
  call void @writeheader(%struct.lame_internal_flags* %204, i32 %206, i32 5)
  %207 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select130 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %207, i32 0, i32 11
  %arrayidx131 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select130, i32 0, i32 1
  %208 = load i32, i32* %arrayidx131, align 4
  %cmp132 = icmp eq i32 %208, 14
  br i1 %cmp132, label %if.then133, label %if.end136

if.then133:                                       ; preds = %if.end127
  %209 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select134 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %209, i32 0, i32 11
  %arrayidx135 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select134, i32 0, i32 1
  store i32 16, i32* %arrayidx135, align 4
  br label %if.end136

if.end136:                                        ; preds = %if.then133, %if.end127
  %210 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %211 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select137 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %211, i32 0, i32 11
  %arrayidx138 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select137, i32 0, i32 1
  %212 = load i32, i32* %arrayidx138, align 4
  call void @writeheader(%struct.lame_internal_flags* %210, i32 %212, i32 5)
  %213 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %214 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %subblock_gain139 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %214, i32 0, i32 12
  %arrayidx140 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain139, i32 0, i32 0
  %215 = load i32, i32* %arrayidx140, align 4
  call void @writeheader(%struct.lame_internal_flags* %213, i32 %215, i32 3)
  %216 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %217 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %subblock_gain141 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %217, i32 0, i32 12
  %arrayidx142 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain141, i32 0, i32 1
  %218 = load i32, i32* %arrayidx142, align 4
  call void @writeheader(%struct.lame_internal_flags* %216, i32 %218, i32 3)
  %219 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %220 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %subblock_gain143 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %220, i32 0, i32 12
  %arrayidx144 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain143, i32 0, i32 2
  %221 = load i32, i32* %arrayidx144, align 4
  call void @writeheader(%struct.lame_internal_flags* %219, i32 %221, i32 3)
  br label %if.end175

if.else145:                                       ; preds = %for.body104
  %222 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @writeheader(%struct.lame_internal_flags* %222, i32 0, i32 1)
  %223 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select146 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %223, i32 0, i32 11
  %arrayidx147 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select146, i32 0, i32 0
  %224 = load i32, i32* %arrayidx147, align 4
  %cmp148 = icmp eq i32 %224, 14
  br i1 %cmp148, label %if.then149, label %if.end152

if.then149:                                       ; preds = %if.else145
  %225 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select150 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %225, i32 0, i32 11
  %arrayidx151 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select150, i32 0, i32 0
  store i32 16, i32* %arrayidx151, align 4
  br label %if.end152

if.end152:                                        ; preds = %if.then149, %if.else145
  %226 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %227 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select153 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %227, i32 0, i32 11
  %arrayidx154 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select153, i32 0, i32 0
  %228 = load i32, i32* %arrayidx154, align 4
  call void @writeheader(%struct.lame_internal_flags* %226, i32 %228, i32 5)
  %229 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select155 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %229, i32 0, i32 11
  %arrayidx156 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select155, i32 0, i32 1
  %230 = load i32, i32* %arrayidx156, align 4
  %cmp157 = icmp eq i32 %230, 14
  br i1 %cmp157, label %if.then158, label %if.end161

if.then158:                                       ; preds = %if.end152
  %231 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select159 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %231, i32 0, i32 11
  %arrayidx160 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select159, i32 0, i32 1
  store i32 16, i32* %arrayidx160, align 4
  br label %if.end161

if.end161:                                        ; preds = %if.then158, %if.end152
  %232 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %233 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select162 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %233, i32 0, i32 11
  %arrayidx163 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select162, i32 0, i32 1
  %234 = load i32, i32* %arrayidx163, align 4
  call void @writeheader(%struct.lame_internal_flags* %232, i32 %234, i32 5)
  %235 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select164 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %235, i32 0, i32 11
  %arrayidx165 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select164, i32 0, i32 2
  %236 = load i32, i32* %arrayidx165, align 4
  %cmp166 = icmp eq i32 %236, 14
  br i1 %cmp166, label %if.then167, label %if.end170

if.then167:                                       ; preds = %if.end161
  %237 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select168 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %237, i32 0, i32 11
  %arrayidx169 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select168, i32 0, i32 2
  store i32 16, i32* %arrayidx169, align 4
  br label %if.end170

if.end170:                                        ; preds = %if.then167, %if.end161
  %238 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %239 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %table_select171 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %239, i32 0, i32 11
  %arrayidx172 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select171, i32 0, i32 2
  %240 = load i32, i32* %arrayidx172, align 4
  call void @writeheader(%struct.lame_internal_flags* %238, i32 %240, i32 5)
  %241 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %242 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %region0_count173 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %242, i32 0, i32 13
  %243 = load i32, i32* %region0_count173, align 4
  call void @writeheader(%struct.lame_internal_flags* %241, i32 %243, i32 4)
  %244 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %245 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %region1_count174 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %245, i32 0, i32 14
  %246 = load i32, i32* %region1_count174, align 4
  call void @writeheader(%struct.lame_internal_flags* %244, i32 %246, i32 3)
  br label %if.end175

if.end175:                                        ; preds = %if.end170, %if.end136
  %247 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %248 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %scalefac_scale176 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %248, i32 0, i32 16
  %249 = load i32, i32* %scalefac_scale176, align 4
  call void @writeheader(%struct.lame_internal_flags* %247, i32 %249, i32 1)
  %250 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %251 = load %struct.gr_info*, %struct.gr_info** %gi105, align 4
  %count1table_select177 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %251, i32 0, i32 17
  %252 = load i32, i32* %count1table_select177, align 4
  call void @writeheader(%struct.lame_internal_flags* %250, i32 %252, i32 1)
  br label %for.inc178

for.inc178:                                       ; preds = %if.end175
  %253 = load i32, i32* %ch, align 4
  %inc179 = add nsw i32 %253, 1
  store i32 %inc179, i32* %ch, align 4
  br label %for.cond101

for.end180:                                       ; preds = %for.cond101
  br label %if.end181

if.end181:                                        ; preds = %for.end180, %for.end96
  %254 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection182 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %254, i32 0, i32 36
  %255 = load i32, i32* %error_protection182, align 4
  %tobool183 = icmp ne i32 %255, 0
  br i1 %tobool183, label %if.then184, label %if.end190

if.then184:                                       ; preds = %if.end181
  %256 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %257 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header185 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %257, i32 0, i32 8
  %258 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr186 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %258, i32 0, i32 9
  %259 = load i32, i32* %h_ptr186, align 8
  %arrayidx187 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header185, i32 0, i32 %259
  %buf188 = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx187, i32 0, i32 2
  %arraydecay189 = getelementptr inbounds [40 x i8], [40 x i8]* %buf188, i32 0, i32 0
  call void @CRC_writeheader(%struct.lame_internal_flags* %256, i8* %arraydecay189)
  br label %if.end190

if.end190:                                        ; preds = %if.then184, %if.end181
  %260 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr191 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %260, i32 0, i32 9
  %261 = load i32, i32* %h_ptr191, align 8
  store i32 %261, i32* %old, align 4
  %262 = load i32, i32* %old, align 4
  %add192 = add nsw i32 %262, 1
  %and = and i32 %add192, 255
  %263 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr193 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %263, i32 0, i32 9
  store i32 %and, i32* %h_ptr193, align 8
  %264 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header194 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %264, i32 0, i32 8
  %265 = load i32, i32* %old, align 4
  %arrayidx195 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header194, i32 0, i32 %265
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx195, i32 0, i32 0
  %266 = load i32, i32* %write_timing, align 8
  %267 = load i32, i32* %bitsPerFrame.addr, align 4
  %add196 = add nsw i32 %266, %267
  %268 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header197 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %268, i32 0, i32 8
  %269 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr198 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %269, i32 0, i32 9
  %270 = load i32, i32* %h_ptr198, align 8
  %arrayidx199 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header197, i32 0, i32 %270
  %write_timing200 = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx199, i32 0, i32 0
  store i32 %add196, i32* %write_timing200, align 8
  %271 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr201 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %271, i32 0, i32 9
  %272 = load i32, i32* %h_ptr201, align 8
  %273 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %273, i32 0, i32 10
  %274 = load i32, i32* %w_ptr, align 4
  %cmp202 = icmp eq i32 %272, %274
  br i1 %cmp202, label %if.then203, label %if.end204

if.then203:                                       ; preds = %if.end190
  %275 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %275, i8* getelementptr inbounds ([49 x i8], [49 x i8]* @.str.7, i32 0, i32 0))
  br label %if.end204

if.end204:                                        ; preds = %if.then203, %if.end190
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @writeMainData(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %sfb = alloca i32, align 4
  %data_bits = alloca i32, align 4
  %tot_bits = alloca i32, align 4
  %gi = alloca %struct.gr_info*, align 4
  %slen1 = alloca i32, align 4
  %slen2 = alloca i32, align 4
  %gi53 = alloca %struct.gr_info*, align 4
  %i = alloca i32, align 4
  %sfb_partition = alloca i32, align 4
  %scale_bits = alloca i32, align 4
  %sfbs = alloca i32, align 4
  %slen = alloca i32, align 4
  %sfbs118 = alloca i32, align 4
  %slen121 = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  store i32 0, i32* %tot_bits, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 0
  %3 = load i32, i32* %version, align 4
  %cmp = icmp eq i32 %3, 1
  br i1 %cmp, label %if.then, label %if.else48

if.then:                                          ; preds = %entry
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %if.then
  %4 = load i32, i32* %gr, align 4
  %cmp3 = icmp slt i32 %4, 2
  br i1 %cmp3, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc42, %for.body
  %5 = load i32, i32* %ch, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 14
  %7 = load i32, i32* %channels_out, align 4
  %cmp5 = icmp slt i32 %5, %7
  br i1 %cmp5, label %for.body6, label %for.end44

for.body6:                                        ; preds = %for.cond4
  %8 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %8, i32 0, i32 0
  %9 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %9
  %10 = load i32, i32* %ch, align 4
  %arrayidx7 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %10
  store %struct.gr_info* %arrayidx7, %struct.gr_info** %gi, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 8
  %12 = load i32, i32* %scalefac_compress, align 4
  %arrayidx8 = getelementptr inbounds [16 x i32], [16 x i32]* @slen1_tab, i32 0, i32 %12
  %13 = load i32, i32* %arrayidx8, align 4
  store i32 %13, i32* %slen1, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac_compress9 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 8
  %15 = load i32, i32* %scalefac_compress9, align 4
  %arrayidx10 = getelementptr inbounds [16 x i32], [16 x i32]* @slen2_tab, i32 0, i32 %15
  %16 = load i32, i32* %arrayidx10, align 4
  store i32 %16, i32* %slen2, align 4
  store i32 0, i32* %data_bits, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc, %for.body6
  %17 = load i32, i32* %sfb, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbdivide = getelementptr inbounds %struct.gr_info, %struct.gr_info* %18, i32 0, i32 24
  %19 = load i32, i32* %sfbdivide, align 4
  %cmp12 = icmp slt i32 %17, %19
  br i1 %cmp12, label %for.body13, label %for.end

for.body13:                                       ; preds = %for.cond11
  %20 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %20, i32 0, i32 2
  %21 = load i32, i32* %sfb, align 4
  %arrayidx14 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx14, align 4
  %cmp15 = icmp eq i32 %22, -1
  br i1 %cmp15, label %if.then16, label %if.end

if.then16:                                        ; preds = %for.body13
  br label %for.inc

if.end:                                           ; preds = %for.body13
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %24 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac17 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 2
  %25 = load i32, i32* %sfb, align 4
  %arrayidx18 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac17, i32 0, i32 %25
  %26 = load i32, i32* %arrayidx18, align 4
  %27 = load i32, i32* %slen1, align 4
  call void @putbits2(%struct.lame_internal_flags* %23, i32 %26, i32 %27)
  %28 = load i32, i32* %slen1, align 4
  %29 = load i32, i32* %data_bits, align 4
  %add = add nsw i32 %29, %28
  store i32 %add, i32* %data_bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then16
  %30 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond11

for.end:                                          ; preds = %for.cond11
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc30, %for.end
  %31 = load i32, i32* %sfb, align 4
  %32 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %32, i32 0, i32 22
  %33 = load i32, i32* %sfbmax, align 4
  %cmp20 = icmp slt i32 %31, %33
  br i1 %cmp20, label %for.body21, label %for.end32

for.body21:                                       ; preds = %for.cond19
  %34 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac22 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %34, i32 0, i32 2
  %35 = load i32, i32* %sfb, align 4
  %arrayidx23 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac22, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx23, align 4
  %cmp24 = icmp eq i32 %36, -1
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %for.body21
  br label %for.inc30

if.end26:                                         ; preds = %for.body21
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %38 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %scalefac27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %38, i32 0, i32 2
  %39 = load i32, i32* %sfb, align 4
  %arrayidx28 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac27, i32 0, i32 %39
  %40 = load i32, i32* %arrayidx28, align 4
  %41 = load i32, i32* %slen2, align 4
  call void @putbits2(%struct.lame_internal_flags* %37, i32 %40, i32 %41)
  %42 = load i32, i32* %slen2, align 4
  %43 = load i32, i32* %data_bits, align 4
  %add29 = add nsw i32 %43, %42
  store i32 %add29, i32* %data_bits, align 4
  br label %for.inc30

for.inc30:                                        ; preds = %if.end26, %if.then25
  %44 = load i32, i32* %sfb, align 4
  %inc31 = add nsw i32 %44, 1
  store i32 %inc31, i32* %sfb, align 4
  br label %for.cond19

for.end32:                                        ; preds = %for.cond19
  %45 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %45, i32 0, i32 9
  %46 = load i32, i32* %block_type, align 4
  %cmp33 = icmp eq i32 %46, 2
  br i1 %cmp33, label %if.then34, label %if.else

if.then34:                                        ; preds = %for.end32
  %47 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %48 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %call = call i32 @ShortHuffmancodebits(%struct.lame_internal_flags* %47, %struct.gr_info* %48)
  %49 = load i32, i32* %data_bits, align 4
  %add35 = add nsw i32 %49, %call
  store i32 %add35, i32* %data_bits, align 4
  br label %if.end38

if.else:                                          ; preds = %for.end32
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %51 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %call36 = call i32 @LongHuffmancodebits(%struct.lame_internal_flags* %50, %struct.gr_info* %51)
  %52 = load i32, i32* %data_bits, align 4
  %add37 = add nsw i32 %52, %call36
  store i32 %add37, i32* %data_bits, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.else, %if.then34
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %54 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %call39 = call i32 @huffman_coder_count1(%struct.lame_internal_flags* %53, %struct.gr_info* %54)
  %55 = load i32, i32* %data_bits, align 4
  %add40 = add nsw i32 %55, %call39
  store i32 %add40, i32* %data_bits, align 4
  %56 = load i32, i32* %data_bits, align 4
  %57 = load i32, i32* %tot_bits, align 4
  %add41 = add nsw i32 %57, %56
  store i32 %add41, i32* %tot_bits, align 4
  br label %for.inc42

for.inc42:                                        ; preds = %if.end38
  %58 = load i32, i32* %ch, align 4
  %inc43 = add nsw i32 %58, 1
  store i32 %inc43, i32* %ch, align 4
  br label %for.cond4

for.end44:                                        ; preds = %for.cond4
  br label %for.inc45

for.inc45:                                        ; preds = %for.end44
  %59 = load i32, i32* %gr, align 4
  %inc46 = add nsw i32 %59, 1
  store i32 %inc46, i32* %gr, align 4
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  br label %if.end154

if.else48:                                        ; preds = %entry
  store i32 0, i32* %gr, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond49

for.cond49:                                       ; preds = %for.inc151, %if.else48
  %60 = load i32, i32* %ch, align 4
  %61 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out50 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %61, i32 0, i32 14
  %62 = load i32, i32* %channels_out50, align 4
  %cmp51 = icmp slt i32 %60, %62
  br i1 %cmp51, label %for.body52, label %for.end153

for.body52:                                       ; preds = %for.cond49
  %63 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt54 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %63, i32 0, i32 0
  %64 = load i32, i32* %gr, align 4
  %arrayidx55 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt54, i32 0, i32 %64
  %65 = load i32, i32* %ch, align 4
  %arrayidx56 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx55, i32 0, i32 %65
  store %struct.gr_info* %arrayidx56, %struct.gr_info** %gi53, align 4
  store i32 0, i32* %scale_bits, align 4
  store i32 0, i32* %data_bits, align 4
  store i32 0, i32* %sfb, align 4
  store i32 0, i32* %sfb_partition, align 4
  %66 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %block_type57 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %66, i32 0, i32 9
  %67 = load i32, i32* %block_type57, align 4
  %cmp58 = icmp eq i32 %67, 2
  br i1 %cmp58, label %if.then59, label %if.else114

if.then59:                                        ; preds = %for.body52
  br label %for.cond60

for.cond60:                                       ; preds = %for.inc109, %if.then59
  %68 = load i32, i32* %sfb_partition, align 4
  %cmp61 = icmp slt i32 %68, 4
  br i1 %cmp61, label %for.body62, label %for.end111

for.body62:                                       ; preds = %for.cond60
  %69 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %sfb_partition_table = getelementptr inbounds %struct.gr_info, %struct.gr_info* %69, i32 0, i32 28
  %70 = load i32*, i32** %sfb_partition_table, align 4
  %71 = load i32, i32* %sfb_partition, align 4
  %arrayidx63 = getelementptr inbounds i32, i32* %70, i32 %71
  %72 = load i32, i32* %arrayidx63, align 4
  %div = sdiv i32 %72, 3
  store i32 %div, i32* %sfbs, align 4
  %73 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %slen64 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %73, i32 0, i32 29
  %74 = load i32, i32* %sfb_partition, align 4
  %arrayidx65 = getelementptr inbounds [4 x i32], [4 x i32]* %slen64, i32 0, i32 %74
  %75 = load i32, i32* %arrayidx65, align 4
  store i32 %75, i32* %slen, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc105, %for.body62
  %76 = load i32, i32* %i, align 4
  %77 = load i32, i32* %sfbs, align 4
  %cmp67 = icmp slt i32 %76, %77
  br i1 %cmp67, label %for.body68, label %for.end108

for.body68:                                       ; preds = %for.cond66
  %78 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %79 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac69 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %79, i32 0, i32 2
  %80 = load i32, i32* %sfb, align 4
  %mul = mul nsw i32 %80, 3
  %add70 = add nsw i32 %mul, 0
  %arrayidx71 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac69, i32 0, i32 %add70
  %81 = load i32, i32* %arrayidx71, align 4
  %cmp72 = icmp sgt i32 %81, 0
  br i1 %cmp72, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body68
  %82 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac73 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %82, i32 0, i32 2
  %83 = load i32, i32* %sfb, align 4
  %mul74 = mul nsw i32 %83, 3
  %add75 = add nsw i32 %mul74, 0
  %arrayidx76 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac73, i32 0, i32 %add75
  %84 = load i32, i32* %arrayidx76, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body68
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %84, %cond.true ], [ 0, %cond.false ]
  %85 = load i32, i32* %slen, align 4
  call void @putbits2(%struct.lame_internal_flags* %78, i32 %cond, i32 %85)
  %86 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %87 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac77 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %87, i32 0, i32 2
  %88 = load i32, i32* %sfb, align 4
  %mul78 = mul nsw i32 %88, 3
  %add79 = add nsw i32 %mul78, 1
  %arrayidx80 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac77, i32 0, i32 %add79
  %89 = load i32, i32* %arrayidx80, align 4
  %cmp81 = icmp sgt i32 %89, 0
  br i1 %cmp81, label %cond.true82, label %cond.false87

cond.true82:                                      ; preds = %cond.end
  %90 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac83 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %90, i32 0, i32 2
  %91 = load i32, i32* %sfb, align 4
  %mul84 = mul nsw i32 %91, 3
  %add85 = add nsw i32 %mul84, 1
  %arrayidx86 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac83, i32 0, i32 %add85
  %92 = load i32, i32* %arrayidx86, align 4
  br label %cond.end88

cond.false87:                                     ; preds = %cond.end
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false87, %cond.true82
  %cond89 = phi i32 [ %92, %cond.true82 ], [ 0, %cond.false87 ]
  %93 = load i32, i32* %slen, align 4
  call void @putbits2(%struct.lame_internal_flags* %86, i32 %cond89, i32 %93)
  %94 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %95 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac90 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %95, i32 0, i32 2
  %96 = load i32, i32* %sfb, align 4
  %mul91 = mul nsw i32 %96, 3
  %add92 = add nsw i32 %mul91, 2
  %arrayidx93 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac90, i32 0, i32 %add92
  %97 = load i32, i32* %arrayidx93, align 4
  %cmp94 = icmp sgt i32 %97, 0
  br i1 %cmp94, label %cond.true95, label %cond.false100

cond.true95:                                      ; preds = %cond.end88
  %98 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac96 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %98, i32 0, i32 2
  %99 = load i32, i32* %sfb, align 4
  %mul97 = mul nsw i32 %99, 3
  %add98 = add nsw i32 %mul97, 2
  %arrayidx99 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac96, i32 0, i32 %add98
  %100 = load i32, i32* %arrayidx99, align 4
  br label %cond.end101

cond.false100:                                    ; preds = %cond.end88
  br label %cond.end101

cond.end101:                                      ; preds = %cond.false100, %cond.true95
  %cond102 = phi i32 [ %100, %cond.true95 ], [ 0, %cond.false100 ]
  %101 = load i32, i32* %slen, align 4
  call void @putbits2(%struct.lame_internal_flags* %94, i32 %cond102, i32 %101)
  %102 = load i32, i32* %slen, align 4
  %mul103 = mul nsw i32 3, %102
  %103 = load i32, i32* %scale_bits, align 4
  %add104 = add nsw i32 %103, %mul103
  store i32 %add104, i32* %scale_bits, align 4
  br label %for.inc105

for.inc105:                                       ; preds = %cond.end101
  %104 = load i32, i32* %i, align 4
  %inc106 = add nsw i32 %104, 1
  store i32 %inc106, i32* %i, align 4
  %105 = load i32, i32* %sfb, align 4
  %inc107 = add nsw i32 %105, 1
  store i32 %inc107, i32* %sfb, align 4
  br label %for.cond66

for.end108:                                       ; preds = %for.cond66
  br label %for.inc109

for.inc109:                                       ; preds = %for.end108
  %106 = load i32, i32* %sfb_partition, align 4
  %inc110 = add nsw i32 %106, 1
  store i32 %inc110, i32* %sfb_partition, align 4
  br label %for.cond60

for.end111:                                       ; preds = %for.cond60
  %107 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %108 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %call112 = call i32 @ShortHuffmancodebits(%struct.lame_internal_flags* %107, %struct.gr_info* %108)
  %109 = load i32, i32* %data_bits, align 4
  %add113 = add nsw i32 %109, %call112
  store i32 %add113, i32* %data_bits, align 4
  br label %if.end146

if.else114:                                       ; preds = %for.body52
  br label %for.cond115

for.cond115:                                      ; preds = %for.inc141, %if.else114
  %110 = load i32, i32* %sfb_partition, align 4
  %cmp116 = icmp slt i32 %110, 4
  br i1 %cmp116, label %for.body117, label %for.end143

for.body117:                                      ; preds = %for.cond115
  %111 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %sfb_partition_table119 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %111, i32 0, i32 28
  %112 = load i32*, i32** %sfb_partition_table119, align 4
  %113 = load i32, i32* %sfb_partition, align 4
  %arrayidx120 = getelementptr inbounds i32, i32* %112, i32 %113
  %114 = load i32, i32* %arrayidx120, align 4
  store i32 %114, i32* %sfbs118, align 4
  %115 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %slen122 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %115, i32 0, i32 29
  %116 = load i32, i32* %sfb_partition, align 4
  %arrayidx123 = getelementptr inbounds [4 x i32], [4 x i32]* %slen122, i32 0, i32 %116
  %117 = load i32, i32* %arrayidx123, align 4
  store i32 %117, i32* %slen121, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond124

for.cond124:                                      ; preds = %for.inc137, %for.body117
  %118 = load i32, i32* %i, align 4
  %119 = load i32, i32* %sfbs118, align 4
  %cmp125 = icmp slt i32 %118, %119
  br i1 %cmp125, label %for.body126, label %for.end140

for.body126:                                      ; preds = %for.cond124
  %120 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %121 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac127 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %121, i32 0, i32 2
  %122 = load i32, i32* %sfb, align 4
  %arrayidx128 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac127, i32 0, i32 %122
  %123 = load i32, i32* %arrayidx128, align 4
  %cmp129 = icmp sgt i32 %123, 0
  br i1 %cmp129, label %cond.true130, label %cond.false133

cond.true130:                                     ; preds = %for.body126
  %124 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %scalefac131 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %124, i32 0, i32 2
  %125 = load i32, i32* %sfb, align 4
  %arrayidx132 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac131, i32 0, i32 %125
  %126 = load i32, i32* %arrayidx132, align 4
  br label %cond.end134

cond.false133:                                    ; preds = %for.body126
  br label %cond.end134

cond.end134:                                      ; preds = %cond.false133, %cond.true130
  %cond135 = phi i32 [ %126, %cond.true130 ], [ 0, %cond.false133 ]
  %127 = load i32, i32* %slen121, align 4
  call void @putbits2(%struct.lame_internal_flags* %120, i32 %cond135, i32 %127)
  %128 = load i32, i32* %slen121, align 4
  %129 = load i32, i32* %scale_bits, align 4
  %add136 = add nsw i32 %129, %128
  store i32 %add136, i32* %scale_bits, align 4
  br label %for.inc137

for.inc137:                                       ; preds = %cond.end134
  %130 = load i32, i32* %i, align 4
  %inc138 = add nsw i32 %130, 1
  store i32 %inc138, i32* %i, align 4
  %131 = load i32, i32* %sfb, align 4
  %inc139 = add nsw i32 %131, 1
  store i32 %inc139, i32* %sfb, align 4
  br label %for.cond124

for.end140:                                       ; preds = %for.cond124
  br label %for.inc141

for.inc141:                                       ; preds = %for.end140
  %132 = load i32, i32* %sfb_partition, align 4
  %inc142 = add nsw i32 %132, 1
  store i32 %inc142, i32* %sfb_partition, align 4
  br label %for.cond115

for.end143:                                       ; preds = %for.cond115
  %133 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %134 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %call144 = call i32 @LongHuffmancodebits(%struct.lame_internal_flags* %133, %struct.gr_info* %134)
  %135 = load i32, i32* %data_bits, align 4
  %add145 = add nsw i32 %135, %call144
  store i32 %add145, i32* %data_bits, align 4
  br label %if.end146

if.end146:                                        ; preds = %for.end143, %for.end111
  %136 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %137 = load %struct.gr_info*, %struct.gr_info** %gi53, align 4
  %call147 = call i32 @huffman_coder_count1(%struct.lame_internal_flags* %136, %struct.gr_info* %137)
  %138 = load i32, i32* %data_bits, align 4
  %add148 = add nsw i32 %138, %call147
  store i32 %add148, i32* %data_bits, align 4
  %139 = load i32, i32* %scale_bits, align 4
  %140 = load i32, i32* %data_bits, align 4
  %add149 = add nsw i32 %139, %140
  %141 = load i32, i32* %tot_bits, align 4
  %add150 = add nsw i32 %141, %add149
  store i32 %add150, i32* %tot_bits, align 4
  br label %for.inc151

for.inc151:                                       ; preds = %if.end146
  %142 = load i32, i32* %ch, align 4
  %inc152 = add nsw i32 %142, 1
  store i32 %inc152, i32* %ch, align 4
  br label %for.cond49

for.end153:                                       ; preds = %for.cond49
  br label %if.end154

if.end154:                                        ; preds = %for.end153, %for.end47
  %143 = load i32, i32* %tot_bits, align 4
  ret i32 %143
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @copy_buffer(%struct.lame_internal_flags* %gfc, i8* %buffer, i32 %size, i32 %mp3data) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %mp3data.addr = alloca i32, align 4
  %minimum = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %mp3data, i32* %mp3data.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %1 = load i8*, i8** %buffer.addr, align 4
  %2 = load i32, i32* %size.addr, align 4
  %call = call i32 @do_copy_buffer(%struct.lame_internal_flags* %0, i8* %1, i32 %2)
  store i32 %call, i32* %minimum, align 4
  %3 = load i32, i32* %minimum, align 4
  %cmp = icmp sgt i32 %3, 0
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %4 = load i32, i32* %mp3data.addr, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %nMusicCRC = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 17
  %6 = load i8*, i8** %buffer.addr, align 4
  %7 = load i32, i32* %minimum, align 4
  call void @UpdateMusicCRC(i16* %nMusicCRC, i8* %6, i32 %7)
  %8 = load i32, i32* %minimum, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 20
  %nBytesWritten = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table, i32 0, i32 7
  %10 = load i32, i32* %nBytesWritten, align 4
  %add = add i32 %10, %8
  store i32 %add, i32* %nBytesWritten, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %12 = load i8*, i8** %buffer.addr, align 4
  %13 = load i32, i32* %minimum, align 4
  %call1 = call i32 @do_gain_analysis(%struct.lame_internal_flags* %11, i8* %12, i32 %13)
  store i32 %call1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %14 = load i32, i32* %minimum, align 4
  store i32 %14, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %15 = load i32, i32* %retval, align 4
  ret i32 %15
}

; Function Attrs: noinline nounwind optnone
define internal i32 @do_copy_buffer(%struct.lame_internal_flags* %gfc, i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %bs = alloca %struct.bit_stream_struc*, align 4
  %minimum = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 6
  store %struct.bit_stream_struc* %bs1, %struct.bit_stream_struc** %bs, align 4
  %1 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %1, i32 0, i32 3
  %2 = load i32, i32* %buf_byte_idx, align 4
  %add = add nsw i32 %2, 1
  store i32 %add, i32* %minimum, align 4
  %3 = load i32, i32* %minimum, align 4
  %cmp = icmp sle i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %minimum, align 4
  %5 = load i32, i32* %size.addr, align 4
  %cmp2 = icmp sgt i32 %4, %5
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %6 = load i8*, i8** %buffer.addr, align 4
  %7 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %7, i32 0, i32 0
  %8 = load i8*, i8** %buf, align 4
  %9 = load i32, i32* %minimum, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %6, i8* align 1 %8, i32 %9, i1 false)
  %10 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx5 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %10, i32 0, i32 3
  store i32 -1, i32* %buf_byte_idx5, align 4
  %11 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %11, i32 0, i32 4
  store i32 0, i32* %buf_bit_idx, align 4
  %12 = load i32, i32* %minimum, align 4
  store i32 %12, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %13 = load i32, i32* %retval, align 4
  ret i32 %13
}

declare void @UpdateMusicCRC(i16*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @do_gain_analysis(%struct.lame_internal_flags* %gfc, i8* %buffer, i32 %minimum) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca i8*, align 4
  %minimum.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %rsv = alloca %struct.RpgStateVar_t*, align 4
  %rov = alloca %struct.RpgResult_t*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %minimum, i32* %minimum.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 14
  store %struct.RpgStateVar_t* %sv_rpg, %struct.RpgStateVar_t** %rsv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 15
  store %struct.RpgResult_t* %ov_rpg, %struct.RpgResult_t** %rov, align 4
  %3 = load i32, i32* %minimum.addr, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden void @init_bit_stream_w(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %1 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %1, i32 0, i32 10
  store i32 0, i32* %w_ptr, align 4
  %2 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %2, i32 0, i32 9
  store i32 0, i32* %h_ptr, align 8
  %3 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %3, i32 0, i32 8
  %4 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr1 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %4, i32 0, i32 9
  %5 = load i32, i32* %h_ptr1, align 8
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %5
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 0
  store i32 0, i32* %write_timing, align 8
  %call = call i8* @calloc(i32 147456, i32 1)
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 6
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs, i32 0, i32 0
  store i8* %call, i8** %buf, align 8
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 6
  %buf_size = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs2, i32 0, i32 1
  store i32 147456, i32* %buf_size, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 6
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs3, i32 0, i32 3
  store i32 -1, i32* %buf_byte_idx, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 6
  %buf_bit_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs4, i32 0, i32 4
  store i32 0, i32* %buf_bit_idx, align 8
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 6
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %bs5, i32 0, i32 2
  store i32 0, i32* %totbit, align 8
  ret void
}

declare i8* @calloc(i32, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @putbits2(%struct.lame_internal_flags* %gfc, i32 %val, i32 %j) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %val.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %bs = alloca %struct.bit_stream_struc*, align 4
  %k = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  store i32 %j, i32* %j.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 6
  store %struct.bit_stream_struc* %bs1, %struct.bit_stream_struc** %bs, align 4
  br label %while.cond

while.cond:                                       ; preds = %cond.end, %entry
  %2 = load i32, i32* %j.addr, align 4
  %cmp = icmp sgt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %3, i32 0, i32 4
  %4 = load i32, i32* %buf_bit_idx, align 4
  %cmp2 = icmp eq i32 %4, 0
  br i1 %cmp2, label %if.then, label %if.end8

if.then:                                          ; preds = %while.body
  %5 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx3 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %5, i32 0, i32 4
  store i32 8, i32* %buf_bit_idx3, align 4
  %6 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %6, i32 0, i32 3
  %7 = load i32, i32* %buf_byte_idx, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %buf_byte_idx, align 4
  %8 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %8, i32 0, i32 8
  %9 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %9, i32 0, i32 10
  %10 = load i32, i32* %w_ptr, align 4
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %10
  %write_timing = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 0
  %11 = load i32, i32* %write_timing, align 8
  %12 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %12, i32 0, i32 2
  %13 = load i32, i32* %totbit, align 4
  %cmp4 = icmp eq i32 %11, %13
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.then
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @putheader_bits(%struct.lame_internal_flags* %14)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.then
  %15 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %15, i32 0, i32 0
  %16 = load i8*, i8** %buf, align 4
  %17 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx6 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %17, i32 0, i32 3
  %18 = load i32, i32* %buf_byte_idx6, align 4
  %arrayidx7 = getelementptr inbounds i8, i8* %16, i32 %18
  store i8 0, i8* %arrayidx7, align 1
  br label %if.end8

if.end8:                                          ; preds = %if.end, %while.body
  %19 = load i32, i32* %j.addr, align 4
  %20 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx9 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %20, i32 0, i32 4
  %21 = load i32, i32* %buf_bit_idx9, align 4
  %cmp10 = icmp slt i32 %19, %21
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end8
  %22 = load i32, i32* %j.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end8
  %23 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx11 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %23, i32 0, i32 4
  %24 = load i32, i32* %buf_bit_idx11, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %22, %cond.true ], [ %24, %cond.false ]
  store i32 %cond, i32* %k, align 4
  %25 = load i32, i32* %k, align 4
  %26 = load i32, i32* %j.addr, align 4
  %sub = sub nsw i32 %26, %25
  store i32 %sub, i32* %j.addr, align 4
  %27 = load i32, i32* %k, align 4
  %28 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx12 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %28, i32 0, i32 4
  %29 = load i32, i32* %buf_bit_idx12, align 4
  %sub13 = sub nsw i32 %29, %27
  store i32 %sub13, i32* %buf_bit_idx12, align 4
  %30 = load i32, i32* %val.addr, align 4
  %31 = load i32, i32* %j.addr, align 4
  %shr = ashr i32 %30, %31
  %32 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_bit_idx14 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %32, i32 0, i32 4
  %33 = load i32, i32* %buf_bit_idx14, align 4
  %shl = shl i32 %shr, %33
  %34 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf15 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %34, i32 0, i32 0
  %35 = load i8*, i8** %buf15, align 4
  %36 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx16 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %36, i32 0, i32 3
  %37 = load i32, i32* %buf_byte_idx16, align 4
  %arrayidx17 = getelementptr inbounds i8, i8* %35, i32 %37
  %38 = load i8, i8* %arrayidx17, align 1
  %conv = zext i8 %38 to i32
  %or = or i32 %conv, %shl
  %conv18 = trunc i32 %or to i8
  store i8 %conv18, i8* %arrayidx17, align 1
  %39 = load i32, i32* %k, align 4
  %40 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %totbit19 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %40, i32 0, i32 2
  %41 = load i32, i32* %totbit19, align 4
  %add = add nsw i32 %41, %39
  store i32 %add, i32* %totbit19, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

declare i8* @get_lame_short_version() #1

declare i32 @strlen(i8*) #1

; Function Attrs: noinline nounwind optnone
define internal void @putheader_bits(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %bs = alloca %struct.bit_stream_struc*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %bs2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 6
  store %struct.bit_stream_struc* %bs2, %struct.bit_stream_struc** %bs, align 4
  %3 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %3, i32 0, i32 0
  %4 = load i8*, i8** %buf, align 4
  %5 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %5, i32 0, i32 3
  %6 = load i32, i32* %buf_byte_idx, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 %6
  %7 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %7, i32 0, i32 8
  %8 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %8, i32 0, i32 10
  %9 = load i32, i32* %w_ptr, align 4
  %arrayidx3 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %9
  %buf4 = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx3, i32 0, i32 2
  %arraydecay = getelementptr inbounds [40 x i8], [40 x i8]* %buf4, i32 0, i32 0
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 2
  %11 = load i32, i32* %sideinfo_len, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx, i8* align 8 %arraydecay, i32 %11, i1 false)
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 2
  %13 = load i32, i32* %sideinfo_len5, align 4
  %14 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %buf_byte_idx6 = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %14, i32 0, i32 3
  %15 = load i32, i32* %buf_byte_idx6, align 4
  %add = add nsw i32 %15, %13
  store i32 %add, i32* %buf_byte_idx6, align 4
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 2
  %17 = load i32, i32* %sideinfo_len7, align 4
  %mul = mul nsw i32 %17, 8
  %18 = load %struct.bit_stream_struc*, %struct.bit_stream_struc** %bs, align 4
  %totbit = getelementptr inbounds %struct.bit_stream_struc, %struct.bit_stream_struc* %18, i32 0, i32 2
  %19 = load i32, i32* %totbit, align 4
  %add8 = add nsw i32 %19, %mul
  store i32 %add8, i32* %totbit, align 4
  %20 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr9 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %20, i32 0, i32 10
  %21 = load i32, i32* %w_ptr9, align 4
  %add10 = add nsw i32 %21, 1
  %and = and i32 %add10, 255
  %22 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %w_ptr11 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %22, i32 0, i32 10
  store i32 %and, i32* %w_ptr11, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define internal void @writeheader(%struct.lame_internal_flags* %gfc, i32 %val, i32 %j) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %val.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %ptr = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %val, i32* %val.addr, align 4
  store i32 %j, i32* %j.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %1 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %1, i32 0, i32 8
  %2 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %2, i32 0, i32 9
  %3 = load i32, i32* %h_ptr, align 8
  %arrayidx = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header, i32 0, i32 %3
  %ptr1 = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx, i32 0, i32 1
  %4 = load i32, i32* %ptr1, align 4
  store i32 %4, i32* %ptr, align 4
  br label %while.cond

while.cond:                                       ; preds = %cond.end, %entry
  %5 = load i32, i32* %j.addr, align 4
  %cmp = icmp sgt i32 %5, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load i32, i32* %j.addr, align 4
  %7 = load i32, i32* %ptr, align 4
  %and = and i32 %7, 7
  %sub = sub nsw i32 8, %and
  %cmp2 = icmp slt i32 %6, %sub
  br i1 %cmp2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %8 = load i32, i32* %j.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %9 = load i32, i32* %ptr, align 4
  %and3 = and i32 %9, 7
  %sub4 = sub nsw i32 8, %and3
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %8, %cond.true ], [ %sub4, %cond.false ]
  store i32 %cond, i32* %k, align 4
  %10 = load i32, i32* %k, align 4
  %11 = load i32, i32* %j.addr, align 4
  %sub5 = sub nsw i32 %11, %10
  store i32 %sub5, i32* %j.addr, align 4
  %12 = load i32, i32* %val.addr, align 4
  %13 = load i32, i32* %j.addr, align 4
  %shr = ashr i32 %12, %13
  %14 = load i32, i32* %ptr, align 4
  %and6 = and i32 %14, 7
  %sub7 = sub nsw i32 8, %and6
  %15 = load i32, i32* %k, align 4
  %sub8 = sub nsw i32 %sub7, %15
  %shl = shl i32 %shr, %sub8
  %16 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header9 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %16, i32 0, i32 8
  %17 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr10 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %17, i32 0, i32 9
  %18 = load i32, i32* %h_ptr10, align 8
  %arrayidx11 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header9, i32 0, i32 %18
  %buf = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx11, i32 0, i32 2
  %19 = load i32, i32* %ptr, align 4
  %shr12 = ashr i32 %19, 3
  %arrayidx13 = getelementptr inbounds [40 x i8], [40 x i8]* %buf, i32 0, i32 %shr12
  %20 = load i8, i8* %arrayidx13, align 1
  %conv = sext i8 %20 to i32
  %or = or i32 %conv, %shl
  %conv14 = trunc i32 %or to i8
  store i8 %conv14, i8* %arrayidx13, align 1
  %21 = load i32, i32* %k, align 4
  %22 = load i32, i32* %ptr, align 4
  %add = add nsw i32 %22, %21
  store i32 %add, i32* %ptr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %23 = load i32, i32* %ptr, align 4
  %24 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %header15 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %24, i32 0, i32 8
  %25 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %h_ptr16 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %25, i32 0, i32 9
  %26 = load i32, i32* %h_ptr16, align 8
  %arrayidx17 = getelementptr inbounds [256 x %struct.anon], [256 x %struct.anon]* %header15, i32 0, i32 %26
  %ptr18 = getelementptr inbounds %struct.anon, %struct.anon* %arrayidx17, i32 0, i32 1
  store i32 %23, i32* %ptr18, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @ShortHuffmancodebits(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %bits = alloca i32, align 4
  %region1Start = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 1
  %arrayidx = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 3
  %1 = load i32, i32* %arrayidx, align 4
  %mul = mul nsw i32 3, %1
  store i32 %mul, i32* %region1Start, align 4
  %2 = load i32, i32* %region1Start, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 5
  %4 = load i32, i32* %big_values, align 4
  %cmp = icmp sgt i32 %2, %4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 5
  %6 = load i32, i32* %big_values1, align 4
  store i32 %6, i32* %region1Start, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %8 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 11
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  %9 = load i32, i32* %arrayidx2, align 4
  %10 = load i32, i32* %region1Start, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call = call i32 @Huffmancode(%struct.lame_internal_flags* %7, i32 %9, i32 0, i32 %10, %struct.gr_info* %11)
  store i32 %call, i32* %bits, align 4
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 11
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select3, i32 0, i32 1
  %14 = load i32, i32* %arrayidx4, align 4
  %15 = load i32, i32* %region1Start, align 4
  %16 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values5 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 5
  %17 = load i32, i32* %big_values5, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call6 = call i32 @Huffmancode(%struct.lame_internal_flags* %12, i32 %14, i32 %15, i32 %17, %struct.gr_info* %18)
  %19 = load i32, i32* %bits, align 4
  %add = add nsw i32 %19, %call6
  store i32 %add, i32* %bits, align 4
  %20 = load i32, i32* %bits, align 4
  ret i32 %20
}

; Function Attrs: noinline nounwind optnone
define internal i32 @LongHuffmancodebits(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %i = alloca i32, align 4
  %bigvalues = alloca i32, align 4
  %bits = alloca i32, align 4
  %region1Start = alloca i32, align 4
  %region2Start = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 5
  %1 = load i32, i32* %big_values, align 4
  store i32 %1, i32* %bigvalues, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region0_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 13
  %3 = load i32, i32* %region0_count, align 4
  %add = add nsw i32 %3, 1
  store i32 %add, i32* %i, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  store i32 %6, i32* %region1Start, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %region1_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 14
  %8 = load i32, i32* %region1_count, align 4
  %add1 = add nsw i32 %8, 1
  %9 = load i32, i32* %i, align 4
  %add2 = add i32 %9, %add1
  store i32 %add2, i32* %i, align 4
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 8
  %l4 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band3, i32 0, i32 0
  %11 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [23 x i32], [23 x i32]* %l4, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx5, align 4
  store i32 %12, i32* %region2Start, align 4
  %13 = load i32, i32* %region1Start, align 4
  %14 = load i32, i32* %bigvalues, align 4
  %cmp = icmp sgt i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %15 = load i32, i32* %bigvalues, align 4
  store i32 %15, i32* %region1Start, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %16 = load i32, i32* %region2Start, align 4
  %17 = load i32, i32* %bigvalues, align 4
  %cmp6 = icmp sgt i32 %16, %17
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %18 = load i32, i32* %bigvalues, align 4
  store i32 %18, i32* %region2Start, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %20 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %20, i32 0, i32 11
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  %21 = load i32, i32* %arrayidx9, align 4
  %22 = load i32, i32* %region1Start, align 4
  %23 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call = call i32 @Huffmancode(%struct.lame_internal_flags* %19, i32 %21, i32 0, i32 %22, %struct.gr_info* %23)
  store i32 %call, i32* %bits, align 4
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %25 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select10 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 11
  %arrayidx11 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select10, i32 0, i32 1
  %26 = load i32, i32* %arrayidx11, align 4
  %27 = load i32, i32* %region1Start, align 4
  %28 = load i32, i32* %region2Start, align 4
  %29 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call12 = call i32 @Huffmancode(%struct.lame_internal_flags* %24, i32 %26, i32 %27, i32 %28, %struct.gr_info* %29)
  %30 = load i32, i32* %bits, align 4
  %add13 = add nsw i32 %30, %call12
  store i32 %add13, i32* %bits, align 4
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %32 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %table_select14 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %32, i32 0, i32 11
  %arrayidx15 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select14, i32 0, i32 2
  %33 = load i32, i32* %arrayidx15, align 4
  %34 = load i32, i32* %region2Start, align 4
  %35 = load i32, i32* %bigvalues, align 4
  %36 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call16 = call i32 @Huffmancode(%struct.lame_internal_flags* %31, i32 %33, i32 %34, i32 %35, %struct.gr_info* %36)
  %37 = load i32, i32* %bits, align 4
  %add17 = add nsw i32 %37, %call16
  store i32 %add17, i32* %bits, align 4
  %38 = load i32, i32* %bits, align 4
  ret i32 %38
}

; Function Attrs: noinline nounwind optnone
define internal i32 @huffman_coder_count1(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %h = alloca %struct.huffcodetab*, align 4
  %i = alloca i32, align 4
  %bits = alloca i32, align 4
  %ix = alloca i32*, align 4
  %xr = alloca float*, align 4
  %huffbits = alloca i32, align 4
  %p = alloca i32, align 4
  %v = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 17
  %1 = load i32, i32* %count1table_select, align 4
  %add = add nsw i32 %1, 32
  %arrayidx = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %add
  store %struct.huffcodetab* %arrayidx, %struct.huffcodetab** %h, align 4
  store i32 0, i32* %bits, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 1
  %3 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 5
  %4 = load i32, i32* %big_values, align 4
  %arrayidx1 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 %4
  store i32* %arrayidx1, i32** %ix, align 4
  %5 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xr2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 0
  %6 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values3 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 5
  %7 = load i32, i32* %big_values3, align 4
  %arrayidx4 = getelementptr inbounds [576 x float], [576 x float]* %xr2, i32 0, i32 %7
  store float* %arrayidx4, float** %xr, align 4
  %8 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %count1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 6
  %9 = load i32, i32* %count1, align 4
  %10 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %big_values5 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %10, i32 0, i32 5
  %11 = load i32, i32* %big_values5, align 4
  %sub = sub nsw i32 %9, %11
  %div = sdiv i32 %sub, 4
  store i32 %div, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %12 = load i32, i32* %i, align 4
  %cmp = icmp sgt i32 %12, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %huffbits, align 4
  store i32 0, i32* %p, align 4
  %13 = load i32*, i32** %ix, align 4
  %arrayidx6 = getelementptr inbounds i32, i32* %13, i32 0
  %14 = load i32, i32* %arrayidx6, align 4
  store i32 %14, i32* %v, align 4
  %15 = load i32, i32* %v, align 4
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %if.then, label %if.end11

if.then:                                          ; preds = %for.body
  %16 = load i32, i32* %p, align 4
  %add7 = add nsw i32 %16, 8
  store i32 %add7, i32* %p, align 4
  %17 = load float*, float** %xr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %17, i32 0
  %18 = load float, float* %arrayidx8, align 4
  %cmp9 = fcmp olt float %18, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.then
  %19 = load i32, i32* %huffbits, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %huffbits, align 4
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.then
  br label %if.end11

if.end11:                                         ; preds = %if.end, %for.body
  %20 = load i32*, i32** %ix, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %20, i32 1
  %21 = load i32, i32* %arrayidx12, align 4
  store i32 %21, i32* %v, align 4
  %22 = load i32, i32* %v, align 4
  %tobool13 = icmp ne i32 %22, 0
  br i1 %tobool13, label %if.then14, label %if.end21

if.then14:                                        ; preds = %if.end11
  %23 = load i32, i32* %p, align 4
  %add15 = add nsw i32 %23, 4
  store i32 %add15, i32* %p, align 4
  %24 = load i32, i32* %huffbits, align 4
  %mul = mul nsw i32 %24, 2
  store i32 %mul, i32* %huffbits, align 4
  %25 = load float*, float** %xr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %25, i32 1
  %26 = load float, float* %arrayidx16, align 4
  %cmp17 = fcmp olt float %26, 0.000000e+00
  br i1 %cmp17, label %if.then18, label %if.end20

if.then18:                                        ; preds = %if.then14
  %27 = load i32, i32* %huffbits, align 4
  %inc19 = add nsw i32 %27, 1
  store i32 %inc19, i32* %huffbits, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then18, %if.then14
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.end11
  %28 = load i32*, i32** %ix, align 4
  %arrayidx22 = getelementptr inbounds i32, i32* %28, i32 2
  %29 = load i32, i32* %arrayidx22, align 4
  store i32 %29, i32* %v, align 4
  %30 = load i32, i32* %v, align 4
  %tobool23 = icmp ne i32 %30, 0
  br i1 %tobool23, label %if.then24, label %if.end32

if.then24:                                        ; preds = %if.end21
  %31 = load i32, i32* %p, align 4
  %add25 = add nsw i32 %31, 2
  store i32 %add25, i32* %p, align 4
  %32 = load i32, i32* %huffbits, align 4
  %mul26 = mul nsw i32 %32, 2
  store i32 %mul26, i32* %huffbits, align 4
  %33 = load float*, float** %xr, align 4
  %arrayidx27 = getelementptr inbounds float, float* %33, i32 2
  %34 = load float, float* %arrayidx27, align 4
  %cmp28 = fcmp olt float %34, 0.000000e+00
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.then24
  %35 = load i32, i32* %huffbits, align 4
  %inc30 = add nsw i32 %35, 1
  store i32 %inc30, i32* %huffbits, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.then24
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.end21
  %36 = load i32*, i32** %ix, align 4
  %arrayidx33 = getelementptr inbounds i32, i32* %36, i32 3
  %37 = load i32, i32* %arrayidx33, align 4
  store i32 %37, i32* %v, align 4
  %38 = load i32, i32* %v, align 4
  %tobool34 = icmp ne i32 %38, 0
  br i1 %tobool34, label %if.then35, label %if.end43

if.then35:                                        ; preds = %if.end32
  %39 = load i32, i32* %p, align 4
  %inc36 = add nsw i32 %39, 1
  store i32 %inc36, i32* %p, align 4
  %40 = load i32, i32* %huffbits, align 4
  %mul37 = mul nsw i32 %40, 2
  store i32 %mul37, i32* %huffbits, align 4
  %41 = load float*, float** %xr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %41, i32 3
  %42 = load float, float* %arrayidx38, align 4
  %cmp39 = fcmp olt float %42, 0.000000e+00
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %if.then35
  %43 = load i32, i32* %huffbits, align 4
  %inc41 = add nsw i32 %43, 1
  store i32 %inc41, i32* %huffbits, align 4
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %if.then35
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.end32
  %44 = load i32*, i32** %ix, align 4
  %add.ptr = getelementptr inbounds i32, i32* %44, i32 4
  store i32* %add.ptr, i32** %ix, align 4
  %45 = load float*, float** %xr, align 4
  %add.ptr44 = getelementptr inbounds float, float* %45, i32 4
  store float* %add.ptr44, float** %xr, align 4
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %47 = load i32, i32* %huffbits, align 4
  %48 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %table = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %48, i32 0, i32 2
  %49 = load i16*, i16** %table, align 4
  %50 = load i32, i32* %p, align 4
  %arrayidx45 = getelementptr inbounds i16, i16* %49, i32 %50
  %51 = load i16, i16* %arrayidx45, align 2
  %conv = zext i16 %51 to i32
  %add46 = add nsw i32 %47, %conv
  %52 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %hlen = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %52, i32 0, i32 3
  %53 = load i8*, i8** %hlen, align 4
  %54 = load i32, i32* %p, align 4
  %arrayidx47 = getelementptr inbounds i8, i8* %53, i32 %54
  %55 = load i8, i8* %arrayidx47, align 1
  %conv48 = zext i8 %55 to i32
  call void @putbits2(%struct.lame_internal_flags* %46, i32 %add46, i32 %conv48)
  %56 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %hlen49 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %56, i32 0, i32 3
  %57 = load i8*, i8** %hlen49, align 4
  %58 = load i32, i32* %p, align 4
  %arrayidx50 = getelementptr inbounds i8, i8* %57, i32 %58
  %59 = load i8, i8* %arrayidx50, align 1
  %conv51 = zext i8 %59 to i32
  %60 = load i32, i32* %bits, align 4
  %add52 = add nsw i32 %60, %conv51
  store i32 %add52, i32* %bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end43
  %61 = load i32, i32* %i, align 4
  %dec = add nsw i32 %61, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %62 = load i32, i32* %bits, align 4
  ret i32 %62
}

; Function Attrs: noinline nounwind optnone
define internal i32 @Huffmancode(%struct.lame_internal_flags* %gfc, i32 %tableindex, i32 %start, i32 %end, %struct.gr_info* %gi) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %tableindex.addr = alloca i32, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %h = alloca %struct.huffcodetab*, align 4
  %linbits = alloca i32, align 4
  %i = alloca i32, align 4
  %bits = alloca i32, align 4
  %cbits = alloca i16, align 2
  %xbits = alloca i16, align 2
  %xlen1 = alloca i32, align 4
  %ext = alloca i32, align 4
  %x1 = alloca i32, align 4
  %x2 = alloca i32, align 4
  %linbits_x1 = alloca i16, align 2
  %linbits_x2 = alloca i16, align 2
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %tableindex, i32* %tableindex.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load i32, i32* %tableindex.addr, align 4
  %arrayidx = getelementptr inbounds [34 x %struct.huffcodetab], [34 x %struct.huffcodetab]* @ht, i32 0, i32 %0
  store %struct.huffcodetab* %arrayidx, %struct.huffcodetab** %h, align 4
  %1 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %xlen = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %1, i32 0, i32 0
  %2 = load i32, i32* %xlen, align 4
  store i32 %2, i32* %linbits, align 4
  store i32 0, i32* %bits, align 4
  %3 = load i32, i32* %tableindex.addr, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %4 = load i32, i32* %bits, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %start.addr, align 4
  store i32 %5, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i16 0, i16* %cbits, align 2
  store i16 0, i16* %xbits, align 2
  %8 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %xlen2 = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %8, i32 0, i32 0
  %9 = load i32, i32* %xlen2, align 4
  store i32 %9, i32* %xlen1, align 4
  store i32 0, i32* %ext, align 4
  %10 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %10, i32 0, i32 1
  %11 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx3, align 4
  store i32 %12, i32* %x1, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %add = add nsw i32 %14, 1
  %arrayidx5 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc4, i32 0, i32 %add
  %15 = load i32, i32* %arrayidx5, align 4
  store i32 %15, i32* %x2, align 4
  %16 = load i32, i32* %x1, align 4
  %cmp6 = icmp ne i32 %16, 0
  br i1 %cmp6, label %if.then7, label %if.end12

if.then7:                                         ; preds = %for.body
  %17 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %18
  %19 = load float, float* %arrayidx8, align 4
  %cmp9 = fcmp olt float %19, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then7
  %20 = load i32, i32* %ext, align 4
  %inc = add i32 %20, 1
  store i32 %inc, i32* %ext, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.then7
  %21 = load i16, i16* %cbits, align 2
  %dec = add i16 %21, -1
  store i16 %dec, i16* %cbits, align 2
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %for.body
  %22 = load i32, i32* %tableindex.addr, align 4
  %cmp13 = icmp ugt i32 %22, 15
  br i1 %cmp13, label %if.then14, label %if.end32

if.then14:                                        ; preds = %if.end12
  %23 = load i32, i32* %x1, align 4
  %cmp15 = icmp uge i32 %23, 15
  br i1 %cmp15, label %if.then16, label %if.end19

if.then16:                                        ; preds = %if.then14
  %24 = load i32, i32* %x1, align 4
  %sub = sub i32 %24, 15
  %conv = trunc i32 %sub to i16
  store i16 %conv, i16* %linbits_x1, align 2
  %25 = load i16, i16* %linbits_x1, align 2
  %conv17 = zext i16 %25 to i32
  %shl = shl i32 %conv17, 1
  %26 = load i32, i32* %ext, align 4
  %or = or i32 %26, %shl
  store i32 %or, i32* %ext, align 4
  %27 = load i32, i32* %linbits, align 4
  %conv18 = trunc i32 %27 to i16
  store i16 %conv18, i16* %xbits, align 2
  store i32 15, i32* %x1, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then16, %if.then14
  %28 = load i32, i32* %x2, align 4
  %cmp20 = icmp uge i32 %28, 15
  br i1 %cmp20, label %if.then22, label %if.end31

if.then22:                                        ; preds = %if.end19
  %29 = load i32, i32* %x2, align 4
  %sub23 = sub i32 %29, 15
  %conv24 = trunc i32 %sub23 to i16
  store i16 %conv24, i16* %linbits_x2, align 2
  %30 = load i32, i32* %linbits, align 4
  %31 = load i32, i32* %ext, align 4
  %shl25 = shl i32 %31, %30
  store i32 %shl25, i32* %ext, align 4
  %32 = load i16, i16* %linbits_x2, align 2
  %conv26 = zext i16 %32 to i32
  %33 = load i32, i32* %ext, align 4
  %or27 = or i32 %33, %conv26
  store i32 %or27, i32* %ext, align 4
  %34 = load i32, i32* %linbits, align 4
  %35 = load i16, i16* %xbits, align 2
  %conv28 = zext i16 %35 to i32
  %add29 = add i32 %conv28, %34
  %conv30 = trunc i32 %add29 to i16
  store i16 %conv30, i16* %xbits, align 2
  store i32 15, i32* %x2, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then22, %if.end19
  store i32 16, i32* %xlen1, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.end12
  %36 = load i32, i32* %x2, align 4
  %cmp33 = icmp ne i32 %36, 0
  br i1 %cmp33, label %if.then35, label %if.end46

if.then35:                                        ; preds = %if.end32
  %37 = load i32, i32* %ext, align 4
  %shl36 = shl i32 %37, 1
  store i32 %shl36, i32* %ext, align 4
  %38 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xr37 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %38, i32 0, i32 0
  %39 = load i32, i32* %i, align 4
  %add38 = add nsw i32 %39, 1
  %arrayidx39 = getelementptr inbounds [576 x float], [576 x float]* %xr37, i32 0, i32 %add38
  %40 = load float, float* %arrayidx39, align 4
  %cmp40 = fcmp olt float %40, 0.000000e+00
  br i1 %cmp40, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.then35
  %41 = load i32, i32* %ext, align 4
  %inc43 = add i32 %41, 1
  store i32 %inc43, i32* %ext, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.then35
  %42 = load i16, i16* %cbits, align 2
  %dec45 = add i16 %42, -1
  store i16 %dec45, i16* %cbits, align 2
  br label %if.end46

if.end46:                                         ; preds = %if.end44, %if.end32
  %43 = load i32, i32* %x1, align 4
  %44 = load i32, i32* %xlen1, align 4
  %mul = mul i32 %43, %44
  %45 = load i32, i32* %x2, align 4
  %add47 = add i32 %mul, %45
  store i32 %add47, i32* %x1, align 4
  %46 = load i16, i16* %cbits, align 2
  %conv48 = sext i16 %46 to i32
  %47 = load i16, i16* %xbits, align 2
  %conv49 = zext i16 %47 to i32
  %sub50 = sub nsw i32 %conv49, %conv48
  %conv51 = trunc i32 %sub50 to i16
  store i16 %conv51, i16* %xbits, align 2
  %48 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %hlen = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %48, i32 0, i32 3
  %49 = load i8*, i8** %hlen, align 4
  %50 = load i32, i32* %x1, align 4
  %arrayidx52 = getelementptr inbounds i8, i8* %49, i32 %50
  %51 = load i8, i8* %arrayidx52, align 1
  %conv53 = zext i8 %51 to i32
  %52 = load i16, i16* %cbits, align 2
  %conv54 = sext i16 %52 to i32
  %add55 = add nsw i32 %conv54, %conv53
  %conv56 = trunc i32 %add55 to i16
  store i16 %conv56, i16* %cbits, align 2
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %54 = load %struct.huffcodetab*, %struct.huffcodetab** %h, align 4
  %table = getelementptr inbounds %struct.huffcodetab, %struct.huffcodetab* %54, i32 0, i32 2
  %55 = load i16*, i16** %table, align 4
  %56 = load i32, i32* %x1, align 4
  %arrayidx57 = getelementptr inbounds i16, i16* %55, i32 %56
  %57 = load i16, i16* %arrayidx57, align 2
  %conv58 = zext i16 %57 to i32
  %58 = load i16, i16* %cbits, align 2
  %conv59 = sext i16 %58 to i32
  call void @putbits2(%struct.lame_internal_flags* %53, i32 %conv58, i32 %conv59)
  %59 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %60 = load i32, i32* %ext, align 4
  %61 = load i16, i16* %xbits, align 2
  %conv60 = zext i16 %61 to i32
  call void @putbits2(%struct.lame_internal_flags* %59, i32 %60, i32 %conv60)
  %62 = load i16, i16* %cbits, align 2
  %conv61 = sext i16 %62 to i32
  %63 = load i16, i16* %xbits, align 2
  %conv62 = zext i16 %63 to i32
  %add63 = add nsw i32 %conv61, %conv62
  %64 = load i32, i32* %bits, align 4
  %add64 = add nsw i32 %64, %add63
  store i32 %add64, i32* %bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end46
  %65 = load i32, i32* %i, align 4
  %add65 = add nsw i32 %65, 2
  store i32 %add65, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %66 = load i32, i32* %bits, align 4
  store i32 %66, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %67 = load i32, i32* %retval, align 4
  ret i32 %67
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { argmemonly nounwind willreturn writeonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
