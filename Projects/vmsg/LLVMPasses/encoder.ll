; ModuleID = 'encoder.c'
source_filename = "encoder.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type { i32, i32, i32, double, [2 x [1600 x double]], [2 x [1776 x double]], [2 x [2 x [576 x double]]], [2 x [2 x [576 x double]]], [2 x double], [2 x double], [4 x [1024 x double]], [2 x [4 x [1024 x double]]], [2 x [4 x double]], [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]], [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]], [4 x double], [2 x [4 x double]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x [3 x i32]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [2 x i32], i32, i32, i32 }
%struct.hip_global_struct = type opaque
%struct.III_psy_ratio = type { %struct.III_psy_xmin, %struct.III_psy_xmin }

@__const.lame_encode_mp3_frame.ms_ener_ratio = private unnamed_addr constant [2 x float] [float 5.000000e-01, float 5.000000e-01], align 4
@lame_encode_mp3_frame.fircoef = internal constant [9 x float] [float 0xBFBA9C0A80000000, float 0xBFC837EB20000000, float 0xBFCBAD9F00000000, float 0xBFC3F505E0000000, float 0x3C86787FC0000000, float 0x3FCDEF88C0000000, float 0x3FE0254600000000, float 0x3FE837E900000000, float 0x3FEDEF88C0000000], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_encode_mp3_frame(%struct.lame_internal_flags* %gfc, float* %inbuf_l, float* %inbuf_r, i8* %mp3buf, i32 %mp3buf_size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %inbuf_l.addr = alloca float*, align 4
  %inbuf_r.addr = alloca float*, align 4
  %mp3buf.addr = alloca i8*, align 4
  %mp3buf_size.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %mp3count = alloca i32, align 4
  %masking_LR = alloca [2 x [2 x %struct.III_psy_ratio]], align 16
  %masking_MS = alloca [2 x [2 x %struct.III_psy_ratio]], align 16
  %masking = alloca [2 x %struct.III_psy_ratio]*, align 4
  %inbuf = alloca [2 x float*], align 4
  %tot_ener = alloca [2 x [4 x float]], align 16
  %ms_ener_ratio = alloca [2 x float], align 4
  %pe = alloca [2 x [2 x float]], align 16
  %pe_MS = alloca [2 x [2 x float]], align 16
  %pe_use = alloca [2 x float]*, align 4
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %ret = alloca i32, align 4
  %bufp = alloca [2 x float*], align 4
  %blocktype = alloca [2 x i32], align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %sum_pe_MS = alloca float, align 4
  %sum_pe_LR = alloca float, align 4
  %gi0 = alloca %struct.gr_info*, align 4
  %gi1 = alloca %struct.gr_info*, align 4
  %i = alloca i32, align 4
  %f = alloca float, align 4
  %framesize = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %inbuf_l, float** %inbuf_l.addr, align 4
  store float* %inbuf_r, float** %inbuf_r.addr, align 4
  store i8* %mp3buf, i8** %mp3buf.addr, align 4
  store i32 %mp3buf_size, i32* %mp3buf_size.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = bitcast [2 x float]* %ms_ener_ratio to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast ([2 x float]* @__const.lame_encode_mp3_frame.ms_ener_ratio to i8*), i32 8, i1 false)
  %2 = bitcast [2 x [2 x float]]* %pe to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %2, i8 0, i32 16, i1 false)
  %3 = bitcast [2 x [2 x float]]* %pe_MS to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %3, i8 0, i32 16, i1 false)
  %4 = load float*, float** %inbuf_l.addr, align 4
  %arrayidx = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 0
  store float* %4, float** %arrayidx, align 4
  %5 = load float*, float** %inbuf_r.addr, align 4
  %arrayidx2 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 1
  store float* %5, float** %arrayidx2, align 4
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %lame_encode_frame_init = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 2
  %7 = load i32, i32* %lame_encode_frame_init, align 8
  %cmp = icmp eq i32 %7, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 0
  call void @lame_encode_frame_init(%struct.lame_internal_flags* %8, float** %arraydecay)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 12
  %padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 4
  store i32 0, i32* %padding, align 8
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 11
  %frac_SpF = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 6
  %11 = load i32, i32* %frac_SpF, align 8
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 11
  %slot_lag = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc3, i32 0, i32 7
  %13 = load i32, i32* %slot_lag, align 4
  %sub = sub nsw i32 %13, %11
  store i32 %sub, i32* %slot_lag, align 4
  %cmp4 = icmp slt i32 %sub, 0
  br i1 %cmp4, label %if.then5, label %if.end10

if.then5:                                         ; preds = %if.end
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 12
  %15 = load i32, i32* %samplerate_out, align 4
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 11
  %slot_lag7 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc6, i32 0, i32 7
  %17 = load i32, i32* %slot_lag7, align 4
  %add = add nsw i32 %17, %15
  store i32 %add, i32* %slot_lag7, align 4
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc8 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 12
  %padding9 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc8, i32 0, i32 4
  store i32 1, i32* %padding9, align 8
  br label %if.end10

if.end10:                                         ; preds = %if.then5, %if.end
  %19 = bitcast [2 x float*]* %bufp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %19, i8 0, i32 8, i1 false)
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc60, %if.end10
  %20 = load i32, i32* %gr, align 4
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 15
  %22 = load i32, i32* %mode_gr, align 4
  %cmp11 = icmp slt i32 %20, %22
  br i1 %cmp11, label %for.body, label %for.end62

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body
  %23 = load i32, i32* %ch, align 4
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 14
  %25 = load i32, i32* %channels_out, align 4
  %cmp13 = icmp slt i32 %23, %25
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  %26 = load i32, i32* %ch, align 4
  %arrayidx15 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 %26
  %27 = load float*, float** %arrayidx15, align 4
  %28 = load i32, i32* %gr, align 4
  %mul = mul nsw i32 %28, 576
  %add16 = add nsw i32 576, %mul
  %sub17 = sub nsw i32 %add16, 272
  %arrayidx18 = getelementptr inbounds float, float* %27, i32 %sub17
  %29 = load i32, i32* %ch, align 4
  %arrayidx19 = getelementptr inbounds [2 x float*], [2 x float*]* %bufp, i32 0, i32 %29
  store float* %arrayidx18, float** %arrayidx19, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %30 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay20 = getelementptr inbounds [2 x float*], [2 x float*]* %bufp, i32 0, i32 0
  %32 = load i32, i32* %gr, align 4
  %arraydecay21 = getelementptr inbounds [2 x [2 x %struct.III_psy_ratio]], [2 x [2 x %struct.III_psy_ratio]]* %masking_LR, i32 0, i32 0
  %arraydecay22 = getelementptr inbounds [2 x [2 x %struct.III_psy_ratio]], [2 x [2 x %struct.III_psy_ratio]]* %masking_MS, i32 0, i32 0
  %33 = load i32, i32* %gr, align 4
  %arrayidx23 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe, i32 0, i32 %33
  %arraydecay24 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx23, i32 0, i32 0
  %34 = load i32, i32* %gr, align 4
  %arrayidx25 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe_MS, i32 0, i32 %34
  %arraydecay26 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx25, i32 0, i32 0
  %35 = load i32, i32* %gr, align 4
  %arrayidx27 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* %tot_ener, i32 0, i32 %35
  %arraydecay28 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx27, i32 0, i32 0
  %arraydecay29 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype, i32 0, i32 0
  %call = call i32 @L3psycho_anal_vbr(%struct.lame_internal_flags* %31, float** %arraydecay20, i32 %32, [2 x %struct.III_psy_ratio]* %arraydecay21, [2 x %struct.III_psy_ratio]* %arraydecay22, float* %arraydecay24, float* %arraydecay26, float* %arraydecay28, i32* %arraydecay29)
  store i32 %call, i32* %ret, align 4
  %36 = load i32, i32* %ret, align 4
  %cmp30 = icmp ne i32 %36, 0
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %for.end
  store i32 -4, i32* %retval, align 4
  br label %return

if.end32:                                         ; preds = %for.end
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %37, i32 0, i32 41
  %38 = load i32, i32* %mode, align 4
  %cmp33 = icmp eq i32 %38, 1
  br i1 %cmp33, label %if.then34, label %if.end49

if.then34:                                        ; preds = %if.end32
  %39 = load i32, i32* %gr, align 4
  %arrayidx35 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* %tot_ener, i32 0, i32 %39
  %arrayidx36 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx35, i32 0, i32 2
  %40 = load float, float* %arrayidx36, align 8
  %41 = load i32, i32* %gr, align 4
  %arrayidx37 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* %tot_ener, i32 0, i32 %41
  %arrayidx38 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx37, i32 0, i32 3
  %42 = load float, float* %arrayidx38, align 4
  %add39 = fadd float %40, %42
  %43 = load i32, i32* %gr, align 4
  %arrayidx40 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 %43
  store float %add39, float* %arrayidx40, align 4
  %44 = load i32, i32* %gr, align 4
  %arrayidx41 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 %44
  %45 = load float, float* %arrayidx41, align 4
  %cmp42 = fcmp ogt float %45, 0.000000e+00
  br i1 %cmp42, label %if.then43, label %if.end48

if.then43:                                        ; preds = %if.then34
  %46 = load i32, i32* %gr, align 4
  %arrayidx44 = getelementptr inbounds [2 x [4 x float]], [2 x [4 x float]]* %tot_ener, i32 0, i32 %46
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %arrayidx44, i32 0, i32 3
  %47 = load float, float* %arrayidx45, align 4
  %48 = load i32, i32* %gr, align 4
  %arrayidx46 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 %48
  %49 = load float, float* %arrayidx46, align 4
  %div = fdiv float %47, %49
  %50 = load i32, i32* %gr, align 4
  %arrayidx47 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 %50
  store float %div, float* %arrayidx47, align 4
  br label %if.end48

if.end48:                                         ; preds = %if.then43, %if.then34
  br label %if.end49

if.end49:                                         ; preds = %if.end48, %if.end32
  store i32 0, i32* %ch, align 4
  br label %for.cond50

for.cond50:                                       ; preds = %for.inc57, %if.end49
  %51 = load i32, i32* %ch, align 4
  %52 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out51 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %52, i32 0, i32 14
  %53 = load i32, i32* %channels_out51, align 4
  %cmp52 = icmp slt i32 %51, %53
  br i1 %cmp52, label %for.body53, label %for.end59

for.body53:                                       ; preds = %for.cond50
  %54 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %54, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %55 = load i32, i32* %gr, align 4
  %arrayidx54 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %55
  %56 = load i32, i32* %ch, align 4
  %arrayidx55 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx54, i32 0, i32 %56
  store %struct.gr_info* %arrayidx55, %struct.gr_info** %cod_info, align 4
  %57 = load i32, i32* %ch, align 4
  %arrayidx56 = getelementptr inbounds [2 x i32], [2 x i32]* %blocktype, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx56, align 4
  %59 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %59, i32 0, i32 9
  store i32 %58, i32* %block_type, align 4
  %60 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %60, i32 0, i32 10
  store i32 0, i32* %mixed_block_flag, align 4
  br label %for.inc57

for.inc57:                                        ; preds = %for.body53
  %61 = load i32, i32* %ch, align 4
  %inc58 = add nsw i32 %61, 1
  store i32 %inc58, i32* %ch, align 4
  br label %for.cond50

for.end59:                                        ; preds = %for.cond50
  br label %for.inc60

for.inc60:                                        ; preds = %for.end59
  %62 = load i32, i32* %gr, align 4
  %inc61 = add nsw i32 %62, 1
  store i32 %inc61, i32* %gr, align 4
  br label %for.cond

for.end62:                                        ; preds = %for.cond
  %63 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @adjust_ATH(%struct.lame_internal_flags* %63)
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arrayidx63 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 0
  %65 = load float*, float** %arrayidx63, align 4
  %arrayidx64 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 1
  %66 = load float*, float** %arrayidx64, align 4
  call void @mdct_sub48(%struct.lame_internal_flags* %64, float* %65, float* %66)
  %67 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc65 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %67, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc65, i32 0, i32 5
  store i32 0, i32* %mode_ext, align 4
  %68 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %force_ms = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %68, i32 0, i32 16
  %69 = load i32, i32* %force_ms, align 4
  %tobool = icmp ne i32 %69, 0
  br i1 %tobool, label %if.then66, label %if.else

if.then66:                                        ; preds = %for.end62
  %70 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc67 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %70, i32 0, i32 12
  %mode_ext68 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc67, i32 0, i32 5
  store i32 2, i32* %mode_ext68, align 4
  br label %if.end125

if.else:                                          ; preds = %for.end62
  %71 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode69 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %71, i32 0, i32 41
  %72 = load i32, i32* %mode69, align 4
  %cmp70 = icmp eq i32 %72, 1
  br i1 %cmp70, label %if.then71, label %if.end124

if.then71:                                        ; preds = %if.else
  store float 0.000000e+00, float* %sum_pe_MS, align 4
  store float 0.000000e+00, float* %sum_pe_LR, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc89, %if.then71
  %73 = load i32, i32* %gr, align 4
  %74 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr73 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %74, i32 0, i32 15
  %75 = load i32, i32* %mode_gr73, align 4
  %cmp74 = icmp slt i32 %73, %75
  br i1 %cmp74, label %for.body75, label %for.end91

for.body75:                                       ; preds = %for.cond72
  store i32 0, i32* %ch, align 4
  br label %for.cond76

for.cond76:                                       ; preds = %for.inc86, %for.body75
  %76 = load i32, i32* %ch, align 4
  %77 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out77 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %77, i32 0, i32 14
  %78 = load i32, i32* %channels_out77, align 4
  %cmp78 = icmp slt i32 %76, %78
  br i1 %cmp78, label %for.body79, label %for.end88

for.body79:                                       ; preds = %for.cond76
  %79 = load i32, i32* %gr, align 4
  %arrayidx80 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe_MS, i32 0, i32 %79
  %80 = load i32, i32* %ch, align 4
  %arrayidx81 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx80, i32 0, i32 %80
  %81 = load float, float* %arrayidx81, align 4
  %82 = load float, float* %sum_pe_MS, align 4
  %add82 = fadd float %82, %81
  store float %add82, float* %sum_pe_MS, align 4
  %83 = load i32, i32* %gr, align 4
  %arrayidx83 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe, i32 0, i32 %83
  %84 = load i32, i32* %ch, align 4
  %arrayidx84 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx83, i32 0, i32 %84
  %85 = load float, float* %arrayidx84, align 4
  %86 = load float, float* %sum_pe_LR, align 4
  %add85 = fadd float %86, %85
  store float %add85, float* %sum_pe_LR, align 4
  br label %for.inc86

for.inc86:                                        ; preds = %for.body79
  %87 = load i32, i32* %ch, align 4
  %inc87 = add nsw i32 %87, 1
  store i32 %inc87, i32* %ch, align 4
  br label %for.cond76

for.end88:                                        ; preds = %for.cond76
  br label %for.inc89

for.inc89:                                        ; preds = %for.end88
  %88 = load i32, i32* %gr, align 4
  %inc90 = add nsw i32 %88, 1
  store i32 %inc90, i32* %gr, align 4
  br label %for.cond72

for.end91:                                        ; preds = %for.cond72
  %89 = load float, float* %sum_pe_MS, align 4
  %conv = fpext float %89 to double
  %90 = load float, float* %sum_pe_LR, align 4
  %conv92 = fpext float %90 to double
  %mul93 = fmul double 1.000000e+00, %conv92
  %cmp94 = fcmp ole double %conv, %mul93
  br i1 %cmp94, label %if.then96, label %if.end123

if.then96:                                        ; preds = %for.end91
  %91 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side97 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %91, i32 0, i32 7
  %tt98 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side97, i32 0, i32 0
  %arrayidx99 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt98, i32 0, i32 0
  %arrayidx100 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx99, i32 0, i32 0
  store %struct.gr_info* %arrayidx100, %struct.gr_info** %gi0, align 4
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side101 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %92, i32 0, i32 7
  %tt102 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side101, i32 0, i32 0
  %93 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr103 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %93, i32 0, i32 15
  %94 = load i32, i32* %mode_gr103, align 4
  %sub104 = sub nsw i32 %94, 1
  %arrayidx105 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt102, i32 0, i32 %sub104
  %arrayidx106 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx105, i32 0, i32 0
  store %struct.gr_info* %arrayidx106, %struct.gr_info** %gi1, align 4
  %95 = load %struct.gr_info*, %struct.gr_info** %gi0, align 4
  %arrayidx107 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %95, i32 0
  %block_type108 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx107, i32 0, i32 9
  %96 = load i32, i32* %block_type108, align 4
  %97 = load %struct.gr_info*, %struct.gr_info** %gi0, align 4
  %arrayidx109 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %97, i32 1
  %block_type110 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx109, i32 0, i32 9
  %98 = load i32, i32* %block_type110, align 4
  %cmp111 = icmp eq i32 %96, %98
  br i1 %cmp111, label %land.lhs.true, label %if.end122

land.lhs.true:                                    ; preds = %if.then96
  %99 = load %struct.gr_info*, %struct.gr_info** %gi1, align 4
  %arrayidx113 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %99, i32 0
  %block_type114 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx113, i32 0, i32 9
  %100 = load i32, i32* %block_type114, align 4
  %101 = load %struct.gr_info*, %struct.gr_info** %gi1, align 4
  %arrayidx115 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %101, i32 1
  %block_type116 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx115, i32 0, i32 9
  %102 = load i32, i32* %block_type116, align 4
  %cmp117 = icmp eq i32 %100, %102
  br i1 %cmp117, label %if.then119, label %if.end122

if.then119:                                       ; preds = %land.lhs.true
  %103 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc120 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %103, i32 0, i32 12
  %mode_ext121 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc120, i32 0, i32 5
  store i32 2, i32* %mode_ext121, align 4
  br label %if.end122

if.end122:                                        ; preds = %if.then119, %land.lhs.true, %if.then96
  br label %if.end123

if.end123:                                        ; preds = %if.end122, %for.end91
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %if.else
  br label %if.end125

if.end125:                                        ; preds = %if.end124, %if.then66
  %104 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc126 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %104, i32 0, i32 12
  %mode_ext127 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc126, i32 0, i32 5
  %105 = load i32, i32* %mode_ext127, align 4
  %cmp128 = icmp eq i32 %105, 2
  br i1 %cmp128, label %if.then130, label %if.else133

if.then130:                                       ; preds = %if.end125
  %arraydecay131 = getelementptr inbounds [2 x [2 x %struct.III_psy_ratio]], [2 x [2 x %struct.III_psy_ratio]]* %masking_MS, i32 0, i32 0
  store [2 x %struct.III_psy_ratio]* %arraydecay131, [2 x %struct.III_psy_ratio]** %masking, align 4
  %arraydecay132 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe_MS, i32 0, i32 0
  store [2 x float]* %arraydecay132, [2 x float]** %pe_use, align 4
  br label %if.end136

if.else133:                                       ; preds = %if.end125
  %arraydecay134 = getelementptr inbounds [2 x [2 x %struct.III_psy_ratio]], [2 x [2 x %struct.III_psy_ratio]]* %masking_LR, i32 0, i32 0
  store [2 x %struct.III_psy_ratio]* %arraydecay134, [2 x %struct.III_psy_ratio]** %masking, align 4
  %arraydecay135 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %pe, i32 0, i32 0
  store [2 x float]* %arraydecay135, [2 x float]** %pe_use, align 4
  br label %if.end136

if.end136:                                        ; preds = %if.else133, %if.then130
  %106 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %106, i32 0, i32 31
  %107 = load i32, i32* %analysis, align 4
  %tobool137 = icmp ne i32 %107, 0
  br i1 %tobool137, label %land.lhs.true138, label %if.end215

land.lhs.true138:                                 ; preds = %if.end136
  %108 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %108, i32 0, i32 23
  %109 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  %cmp139 = icmp ne %struct.plotting_data* %109, null
  br i1 %cmp139, label %if.then141, label %if.end215

if.then141:                                       ; preds = %land.lhs.true138
  store i32 0, i32* %gr, align 4
  br label %for.cond142

for.cond142:                                      ; preds = %for.inc212, %if.then141
  %110 = load i32, i32* %gr, align 4
  %111 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr143 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %111, i32 0, i32 15
  %112 = load i32, i32* %mode_gr143, align 4
  %cmp144 = icmp slt i32 %110, %112
  br i1 %cmp144, label %for.body146, label %for.end214

for.body146:                                      ; preds = %for.cond142
  store i32 0, i32* %ch, align 4
  br label %for.cond147

for.cond147:                                      ; preds = %for.inc209, %for.body146
  %113 = load i32, i32* %ch, align 4
  %114 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out148 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %114, i32 0, i32 14
  %115 = load i32, i32* %channels_out148, align 4
  %cmp149 = icmp slt i32 %113, %115
  br i1 %cmp149, label %for.body151, label %for.end211

for.body151:                                      ; preds = %for.cond147
  %116 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo152 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %116, i32 0, i32 23
  %117 = load %struct.plotting_data*, %struct.plotting_data** %pinfo152, align 8
  %ms_ratio = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %117, i32 0, i32 8
  %118 = load i32, i32* %gr, align 4
  %arrayidx153 = getelementptr inbounds [2 x double], [2 x double]* %ms_ratio, i32 0, i32 %118
  store double 0.000000e+00, double* %arrayidx153, align 8
  %119 = load i32, i32* %gr, align 4
  %arrayidx154 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 %119
  %120 = load float, float* %arrayidx154, align 4
  %conv155 = fpext float %120 to double
  %121 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo156 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %121, i32 0, i32 23
  %122 = load %struct.plotting_data*, %struct.plotting_data** %pinfo156, align 8
  %ms_ener_ratio157 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %122, i32 0, i32 9
  %123 = load i32, i32* %gr, align 4
  %arrayidx158 = getelementptr inbounds [2 x double], [2 x double]* %ms_ener_ratio157, i32 0, i32 %123
  store double %conv155, double* %arrayidx158, align 8
  %124 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side159 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %124, i32 0, i32 7
  %tt160 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side159, i32 0, i32 0
  %125 = load i32, i32* %gr, align 4
  %arrayidx161 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt160, i32 0, i32 %125
  %126 = load i32, i32* %ch, align 4
  %arrayidx162 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx161, i32 0, i32 %126
  %block_type163 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx162, i32 0, i32 9
  %127 = load i32, i32* %block_type163, align 4
  %128 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo164 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %128, i32 0, i32 23
  %129 = load %struct.plotting_data*, %struct.plotting_data** %pinfo164, align 8
  %blocktype165 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %129, i32 0, i32 34
  %130 = load i32, i32* %gr, align 4
  %arrayidx166 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %blocktype165, i32 0, i32 %130
  %131 = load i32, i32* %ch, align 4
  %arrayidx167 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx166, i32 0, i32 %131
  store i32 %127, i32* %arrayidx167, align 4
  %132 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %133 = load i32, i32* %gr, align 4
  %arrayidx168 = getelementptr inbounds [2 x float], [2 x float]* %132, i32 %133
  %134 = load i32, i32* %ch, align 4
  %arrayidx169 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx168, i32 0, i32 %134
  %135 = load float, float* %arrayidx169, align 4
  %conv170 = fpext float %135 to double
  %136 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo171 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %136, i32 0, i32 23
  %137 = load %struct.plotting_data*, %struct.plotting_data** %pinfo171, align 8
  %pe172 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %137, i32 0, i32 12
  %138 = load i32, i32* %gr, align 4
  %arrayidx173 = getelementptr inbounds [2 x [4 x double]], [2 x [4 x double]]* %pe172, i32 0, i32 %138
  %139 = load i32, i32* %ch, align 4
  %arrayidx174 = getelementptr inbounds [4 x double], [4 x double]* %arrayidx173, i32 0, i32 %139
  store double %conv170, double* %arrayidx174, align 8
  %140 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo175 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %140, i32 0, i32 23
  %141 = load %struct.plotting_data*, %struct.plotting_data** %pinfo175, align 8
  %xr = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %141, i32 0, i32 6
  %142 = load i32, i32* %gr, align 4
  %arrayidx176 = getelementptr inbounds [2 x [2 x [576 x double]]], [2 x [2 x [576 x double]]]* %xr, i32 0, i32 %142
  %143 = load i32, i32* %ch, align 4
  %arrayidx177 = getelementptr inbounds [2 x [576 x double]], [2 x [576 x double]]* %arrayidx176, i32 0, i32 %143
  %arraydecay178 = getelementptr inbounds [576 x double], [576 x double]* %arrayidx177, i32 0, i32 0
  %144 = bitcast double* %arraydecay178 to i8*
  %145 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side179 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %145, i32 0, i32 7
  %tt180 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side179, i32 0, i32 0
  %146 = load i32, i32* %gr, align 4
  %arrayidx181 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt180, i32 0, i32 %146
  %147 = load i32, i32* %ch, align 4
  %arrayidx182 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx181, i32 0, i32 %147
  %xr183 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx182, i32 0, i32 0
  %arrayidx184 = getelementptr inbounds [576 x float], [576 x float]* %xr183, i32 0, i32 0
  %148 = bitcast float* %arrayidx184 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %144, i8* align 4 %148, i32 2304, i1 false)
  %149 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc185 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %149, i32 0, i32 12
  %mode_ext186 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc185, i32 0, i32 5
  %150 = load i32, i32* %mode_ext186, align 4
  %cmp187 = icmp eq i32 %150, 2
  br i1 %cmp187, label %if.then189, label %if.end208

if.then189:                                       ; preds = %for.body151
  %151 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo190 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %151, i32 0, i32 23
  %152 = load %struct.plotting_data*, %struct.plotting_data** %pinfo190, align 8
  %ers = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %152, i32 0, i32 18
  %153 = load i32, i32* %gr, align 4
  %arrayidx191 = getelementptr inbounds [2 x [4 x double]], [2 x [4 x double]]* %ers, i32 0, i32 %153
  %154 = load i32, i32* %ch, align 4
  %add192 = add nsw i32 %154, 2
  %arrayidx193 = getelementptr inbounds [4 x double], [4 x double]* %arrayidx191, i32 0, i32 %add192
  %155 = load double, double* %arrayidx193, align 8
  %156 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo194 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %156, i32 0, i32 23
  %157 = load %struct.plotting_data*, %struct.plotting_data** %pinfo194, align 8
  %ers195 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %157, i32 0, i32 18
  %158 = load i32, i32* %gr, align 4
  %arrayidx196 = getelementptr inbounds [2 x [4 x double]], [2 x [4 x double]]* %ers195, i32 0, i32 %158
  %159 = load i32, i32* %ch, align 4
  %arrayidx197 = getelementptr inbounds [4 x double], [4 x double]* %arrayidx196, i32 0, i32 %159
  store double %155, double* %arrayidx197, align 8
  %160 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo198 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %160, i32 0, i32 23
  %161 = load %struct.plotting_data*, %struct.plotting_data** %pinfo198, align 8
  %energy = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %161, i32 0, i32 11
  %162 = load i32, i32* %gr, align 4
  %arrayidx199 = getelementptr inbounds [2 x [4 x [1024 x double]]], [2 x [4 x [1024 x double]]]* %energy, i32 0, i32 %162
  %163 = load i32, i32* %ch, align 4
  %arrayidx200 = getelementptr inbounds [4 x [1024 x double]], [4 x [1024 x double]]* %arrayidx199, i32 0, i32 %163
  %arraydecay201 = getelementptr inbounds [1024 x double], [1024 x double]* %arrayidx200, i32 0, i32 0
  %164 = bitcast double* %arraydecay201 to i8*
  %165 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo202 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %165, i32 0, i32 23
  %166 = load %struct.plotting_data*, %struct.plotting_data** %pinfo202, align 8
  %energy203 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %166, i32 0, i32 11
  %167 = load i32, i32* %gr, align 4
  %arrayidx204 = getelementptr inbounds [2 x [4 x [1024 x double]]], [2 x [4 x [1024 x double]]]* %energy203, i32 0, i32 %167
  %168 = load i32, i32* %ch, align 4
  %add205 = add nsw i32 %168, 2
  %arrayidx206 = getelementptr inbounds [4 x [1024 x double]], [4 x [1024 x double]]* %arrayidx204, i32 0, i32 %add205
  %arraydecay207 = getelementptr inbounds [1024 x double], [1024 x double]* %arrayidx206, i32 0, i32 0
  %169 = bitcast double* %arraydecay207 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %164, i8* align 8 %169, i32 8192, i1 false)
  br label %if.end208

if.end208:                                        ; preds = %if.then189, %for.body151
  br label %for.inc209

for.inc209:                                       ; preds = %if.end208
  %170 = load i32, i32* %ch, align 4
  %inc210 = add nsw i32 %170, 1
  store i32 %inc210, i32* %ch, align 4
  br label %for.cond147

for.end211:                                       ; preds = %for.cond147
  br label %for.inc212

for.inc212:                                       ; preds = %for.end211
  %171 = load i32, i32* %gr, align 4
  %inc213 = add nsw i32 %171, 1
  store i32 %inc213, i32* %gr, align 4
  br label %for.cond142

for.end214:                                       ; preds = %for.cond142
  br label %if.end215

if.end215:                                        ; preds = %for.end214, %land.lhs.true138, %if.end136
  %172 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %172, i32 0, i32 22
  %173 = load i32, i32* %vbr, align 4
  %cmp216 = icmp eq i32 %173, 0
  br i1 %cmp216, label %if.then221, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end215
  %174 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr218 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %174, i32 0, i32 22
  %175 = load i32, i32* %vbr218, align 4
  %cmp219 = icmp eq i32 %175, 3
  br i1 %cmp219, label %if.then221, label %if.end303

if.then221:                                       ; preds = %lor.lhs.false, %if.end215
  store i32 0, i32* %i, align 4
  br label %for.cond222

for.cond222:                                      ; preds = %for.inc232, %if.then221
  %176 = load i32, i32* %i, align 4
  %cmp223 = icmp slt i32 %176, 18
  br i1 %cmp223, label %for.body225, label %for.end234

for.body225:                                      ; preds = %for.cond222
  %177 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc226 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %177, i32 0, i32 11
  %pefirbuf = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc226, i32 0, i32 5
  %178 = load i32, i32* %i, align 4
  %add227 = add nsw i32 %178, 1
  %arrayidx228 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf, i32 0, i32 %add227
  %179 = load float, float* %arrayidx228, align 4
  %180 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc229 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %180, i32 0, i32 11
  %pefirbuf230 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc229, i32 0, i32 5
  %181 = load i32, i32* %i, align 4
  %arrayidx231 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf230, i32 0, i32 %181
  store float %179, float* %arrayidx231, align 4
  br label %for.inc232

for.inc232:                                       ; preds = %for.body225
  %182 = load i32, i32* %i, align 4
  %inc233 = add nsw i32 %182, 1
  store i32 %inc233, i32* %i, align 4
  br label %for.cond222

for.end234:                                       ; preds = %for.cond222
  store float 0.000000e+00, float* %f, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond235

for.cond235:                                      ; preds = %for.inc251, %for.end234
  %183 = load i32, i32* %gr, align 4
  %184 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr236 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %184, i32 0, i32 15
  %185 = load i32, i32* %mode_gr236, align 4
  %cmp237 = icmp slt i32 %183, %185
  br i1 %cmp237, label %for.body239, label %for.end253

for.body239:                                      ; preds = %for.cond235
  store i32 0, i32* %ch, align 4
  br label %for.cond240

for.cond240:                                      ; preds = %for.inc248, %for.body239
  %186 = load i32, i32* %ch, align 4
  %187 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out241 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %187, i32 0, i32 14
  %188 = load i32, i32* %channels_out241, align 4
  %cmp242 = icmp slt i32 %186, %188
  br i1 %cmp242, label %for.body244, label %for.end250

for.body244:                                      ; preds = %for.cond240
  %189 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %190 = load i32, i32* %gr, align 4
  %arrayidx245 = getelementptr inbounds [2 x float], [2 x float]* %189, i32 %190
  %191 = load i32, i32* %ch, align 4
  %arrayidx246 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx245, i32 0, i32 %191
  %192 = load float, float* %arrayidx246, align 4
  %193 = load float, float* %f, align 4
  %add247 = fadd float %193, %192
  store float %add247, float* %f, align 4
  br label %for.inc248

for.inc248:                                       ; preds = %for.body244
  %194 = load i32, i32* %ch, align 4
  %inc249 = add nsw i32 %194, 1
  store i32 %inc249, i32* %ch, align 4
  br label %for.cond240

for.end250:                                       ; preds = %for.cond240
  br label %for.inc251

for.inc251:                                       ; preds = %for.end250
  %195 = load i32, i32* %gr, align 4
  %inc252 = add nsw i32 %195, 1
  store i32 %inc252, i32* %gr, align 4
  br label %for.cond235

for.end253:                                       ; preds = %for.cond235
  %196 = load float, float* %f, align 4
  %197 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc254 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %197, i32 0, i32 11
  %pefirbuf255 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc254, i32 0, i32 5
  %arrayidx256 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf255, i32 0, i32 18
  store float %196, float* %arrayidx256, align 4
  %198 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc257 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %198, i32 0, i32 11
  %pefirbuf258 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc257, i32 0, i32 5
  %arrayidx259 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf258, i32 0, i32 9
  %199 = load float, float* %arrayidx259, align 4
  store float %199, float* %f, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond260

for.cond260:                                      ; preds = %for.inc275, %for.end253
  %200 = load i32, i32* %i, align 4
  %cmp261 = icmp slt i32 %200, 9
  br i1 %cmp261, label %for.body263, label %for.end277

for.body263:                                      ; preds = %for.cond260
  %201 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc264 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %201, i32 0, i32 11
  %pefirbuf265 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc264, i32 0, i32 5
  %202 = load i32, i32* %i, align 4
  %arrayidx266 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf265, i32 0, i32 %202
  %203 = load float, float* %arrayidx266, align 4
  %204 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc267 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %204, i32 0, i32 11
  %pefirbuf268 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc267, i32 0, i32 5
  %205 = load i32, i32* %i, align 4
  %sub269 = sub nsw i32 18, %205
  %arrayidx270 = getelementptr inbounds [19 x float], [19 x float]* %pefirbuf268, i32 0, i32 %sub269
  %206 = load float, float* %arrayidx270, align 4
  %add271 = fadd float %203, %206
  %207 = load i32, i32* %i, align 4
  %arrayidx272 = getelementptr inbounds [9 x float], [9 x float]* @lame_encode_mp3_frame.fircoef, i32 0, i32 %207
  %208 = load float, float* %arrayidx272, align 4
  %mul273 = fmul float %add271, %208
  %209 = load float, float* %f, align 4
  %add274 = fadd float %209, %mul273
  store float %add274, float* %f, align 4
  br label %for.inc275

for.inc275:                                       ; preds = %for.body263
  %210 = load i32, i32* %i, align 4
  %inc276 = add nsw i32 %210, 1
  store i32 %inc276, i32* %i, align 4
  br label %for.cond260

for.end277:                                       ; preds = %for.cond260
  %211 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr278 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %211, i32 0, i32 15
  %212 = load i32, i32* %mode_gr278, align 4
  %mul279 = mul nsw i32 3350, %212
  %213 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out280 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %213, i32 0, i32 14
  %214 = load i32, i32* %channels_out280, align 4
  %mul281 = mul nsw i32 %mul279, %214
  %conv282 = sitofp i32 %mul281 to float
  %215 = load float, float* %f, align 4
  %div283 = fdiv float %conv282, %215
  store float %div283, float* %f, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond284

for.cond284:                                      ; preds = %for.inc300, %for.end277
  %216 = load i32, i32* %gr, align 4
  %217 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr285 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %217, i32 0, i32 15
  %218 = load i32, i32* %mode_gr285, align 4
  %cmp286 = icmp slt i32 %216, %218
  br i1 %cmp286, label %for.body288, label %for.end302

for.body288:                                      ; preds = %for.cond284
  store i32 0, i32* %ch, align 4
  br label %for.cond289

for.cond289:                                      ; preds = %for.inc297, %for.body288
  %219 = load i32, i32* %ch, align 4
  %220 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out290 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %220, i32 0, i32 14
  %221 = load i32, i32* %channels_out290, align 4
  %cmp291 = icmp slt i32 %219, %221
  br i1 %cmp291, label %for.body293, label %for.end299

for.body293:                                      ; preds = %for.cond289
  %222 = load float, float* %f, align 4
  %223 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %224 = load i32, i32* %gr, align 4
  %arrayidx294 = getelementptr inbounds [2 x float], [2 x float]* %223, i32 %224
  %225 = load i32, i32* %ch, align 4
  %arrayidx295 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx294, i32 0, i32 %225
  %226 = load float, float* %arrayidx295, align 4
  %mul296 = fmul float %226, %222
  store float %mul296, float* %arrayidx295, align 4
  br label %for.inc297

for.inc297:                                       ; preds = %for.body293
  %227 = load i32, i32* %ch, align 4
  %inc298 = add nsw i32 %227, 1
  store i32 %inc298, i32* %ch, align 4
  br label %for.cond289

for.end299:                                       ; preds = %for.cond289
  br label %for.inc300

for.inc300:                                       ; preds = %for.end299
  %228 = load i32, i32* %gr, align 4
  %inc301 = add nsw i32 %228, 1
  store i32 %inc301, i32* %gr, align 4
  br label %for.cond284

for.end302:                                       ; preds = %for.cond284
  br label %if.end303

if.end303:                                        ; preds = %for.end302, %lor.lhs.false
  %229 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr304 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %229, i32 0, i32 22
  %230 = load i32, i32* %vbr304, align 4
  switch i32 %230, label %sw.default [
    i32 0, label %sw.bb
    i32 3, label %sw.bb306
    i32 2, label %sw.bb308
    i32 1, label %sw.bb310
    i32 4, label %sw.bb310
  ]

sw.default:                                       ; preds = %if.end303
  br label %sw.bb

sw.bb:                                            ; preds = %if.end303, %sw.default
  %231 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %232 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %arraydecay305 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 0
  %233 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking, align 4
  call void @CBR_iteration_loop(%struct.lame_internal_flags* %231, [2 x float]* %232, float* %arraydecay305, [2 x %struct.III_psy_ratio]* %233)
  br label %sw.epilog

sw.bb306:                                         ; preds = %if.end303
  %234 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %235 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %arraydecay307 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 0
  %236 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking, align 4
  call void @ABR_iteration_loop(%struct.lame_internal_flags* %234, [2 x float]* %235, float* %arraydecay307, [2 x %struct.III_psy_ratio]* %236)
  br label %sw.epilog

sw.bb308:                                         ; preds = %if.end303
  %237 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %238 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %arraydecay309 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 0
  %239 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking, align 4
  call void @VBR_old_iteration_loop(%struct.lame_internal_flags* %237, [2 x float]* %238, float* %arraydecay309, [2 x %struct.III_psy_ratio]* %239)
  br label %sw.epilog

sw.bb310:                                         ; preds = %if.end303, %if.end303
  %240 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %241 = load [2 x float]*, [2 x float]** %pe_use, align 4
  %arraydecay311 = getelementptr inbounds [2 x float], [2 x float]* %ms_ener_ratio, i32 0, i32 0
  %242 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking, align 4
  call void @VBR_new_iteration_loop(%struct.lame_internal_flags* %240, [2 x float]* %241, float* %arraydecay311, [2 x %struct.III_psy_ratio]* %242)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb310, %sw.bb308, %sw.bb306, %sw.bb
  %243 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call312 = call i32 @format_bitstream(%struct.lame_internal_flags* %243)
  %244 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %245 = load i8*, i8** %mp3buf.addr, align 4
  %246 = load i32, i32* %mp3buf_size.addr, align 4
  %call313 = call i32 @copy_buffer(%struct.lame_internal_flags* %244, i8* %245, i32 %246, i32 1)
  store i32 %call313, i32* %mp3count, align 4
  %247 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %247, i32 0, i32 35
  %248 = load i32, i32* %write_lame_tag, align 4
  %tobool314 = icmp ne i32 %248, 0
  br i1 %tobool314, label %if.then315, label %if.end316

if.then315:                                       ; preds = %sw.epilog
  %249 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @AddVbrFrame(%struct.lame_internal_flags* %249)
  br label %if.end316

if.end316:                                        ; preds = %if.then315, %sw.epilog
  %250 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %analysis317 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %250, i32 0, i32 31
  %251 = load i32, i32* %analysis317, align 4
  %tobool318 = icmp ne i32 %251, 0
  br i1 %tobool318, label %land.lhs.true319, label %if.end364

land.lhs.true319:                                 ; preds = %if.end316
  %252 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo320 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %252, i32 0, i32 23
  %253 = load %struct.plotting_data*, %struct.plotting_data** %pinfo320, align 8
  %cmp321 = icmp ne %struct.plotting_data* %253, null
  br i1 %cmp321, label %if.then323, label %if.end364

if.then323:                                       ; preds = %land.lhs.true319
  %254 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr324 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %254, i32 0, i32 15
  %255 = load i32, i32* %mode_gr324, align 4
  %mul325 = mul nsw i32 576, %255
  store i32 %mul325, i32* %framesize, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond326

for.cond326:                                      ; preds = %for.inc361, %if.then323
  %256 = load i32, i32* %ch, align 4
  %257 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out327 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %257, i32 0, i32 14
  %258 = load i32, i32* %channels_out327, align 4
  %cmp328 = icmp slt i32 %256, %258
  br i1 %cmp328, label %for.body330, label %for.end363

for.body330:                                      ; preds = %for.cond326
  store i32 0, i32* %j, align 4
  br label %for.cond331

for.cond331:                                      ; preds = %for.inc343, %for.body330
  %259 = load i32, i32* %j, align 4
  %cmp332 = icmp slt i32 %259, 272
  br i1 %cmp332, label %for.body334, label %for.end345

for.body334:                                      ; preds = %for.cond331
  %260 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo335 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %260, i32 0, i32 23
  %261 = load %struct.plotting_data*, %struct.plotting_data** %pinfo335, align 8
  %pcmdata = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %261, i32 0, i32 4
  %262 = load i32, i32* %ch, align 4
  %arrayidx336 = getelementptr inbounds [2 x [1600 x double]], [2 x [1600 x double]]* %pcmdata, i32 0, i32 %262
  %263 = load i32, i32* %j, align 4
  %264 = load i32, i32* %framesize, align 4
  %add337 = add nsw i32 %263, %264
  %arrayidx338 = getelementptr inbounds [1600 x double], [1600 x double]* %arrayidx336, i32 0, i32 %add337
  %265 = load double, double* %arrayidx338, align 8
  %266 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo339 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %266, i32 0, i32 23
  %267 = load %struct.plotting_data*, %struct.plotting_data** %pinfo339, align 8
  %pcmdata340 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %267, i32 0, i32 4
  %268 = load i32, i32* %ch, align 4
  %arrayidx341 = getelementptr inbounds [2 x [1600 x double]], [2 x [1600 x double]]* %pcmdata340, i32 0, i32 %268
  %269 = load i32, i32* %j, align 4
  %arrayidx342 = getelementptr inbounds [1600 x double], [1600 x double]* %arrayidx341, i32 0, i32 %269
  store double %265, double* %arrayidx342, align 8
  br label %for.inc343

for.inc343:                                       ; preds = %for.body334
  %270 = load i32, i32* %j, align 4
  %inc344 = add nsw i32 %270, 1
  store i32 %inc344, i32* %j, align 4
  br label %for.cond331

for.end345:                                       ; preds = %for.cond331
  store i32 272, i32* %j, align 4
  br label %for.cond346

for.cond346:                                      ; preds = %for.inc358, %for.end345
  %271 = load i32, i32* %j, align 4
  %cmp347 = icmp slt i32 %271, 1600
  br i1 %cmp347, label %for.body349, label %for.end360

for.body349:                                      ; preds = %for.cond346
  %272 = load i32, i32* %ch, align 4
  %arrayidx350 = getelementptr inbounds [2 x float*], [2 x float*]* %inbuf, i32 0, i32 %272
  %273 = load float*, float** %arrayidx350, align 4
  %274 = load i32, i32* %j, align 4
  %sub351 = sub nsw i32 %274, 272
  %arrayidx352 = getelementptr inbounds float, float* %273, i32 %sub351
  %275 = load float, float* %arrayidx352, align 4
  %conv353 = fpext float %275 to double
  %276 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo354 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %276, i32 0, i32 23
  %277 = load %struct.plotting_data*, %struct.plotting_data** %pinfo354, align 8
  %pcmdata355 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %277, i32 0, i32 4
  %278 = load i32, i32* %ch, align 4
  %arrayidx356 = getelementptr inbounds [2 x [1600 x double]], [2 x [1600 x double]]* %pcmdata355, i32 0, i32 %278
  %279 = load i32, i32* %j, align 4
  %arrayidx357 = getelementptr inbounds [1600 x double], [1600 x double]* %arrayidx356, i32 0, i32 %279
  store double %conv353, double* %arrayidx357, align 8
  br label %for.inc358

for.inc358:                                       ; preds = %for.body349
  %280 = load i32, i32* %j, align 4
  %inc359 = add nsw i32 %280, 1
  store i32 %inc359, i32* %j, align 4
  br label %for.cond346

for.end360:                                       ; preds = %for.cond346
  br label %for.inc361

for.inc361:                                       ; preds = %for.end360
  %281 = load i32, i32* %ch, align 4
  %inc362 = add nsw i32 %281, 1
  store i32 %inc362, i32* %ch, align 4
  br label %for.cond326

for.end363:                                       ; preds = %for.cond326
  %282 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %282, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 2
  store float 1.000000e+00, float* %masking_lower, align 4
  %283 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %284 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %masking, align 4
  call void @set_frame_pinfo(%struct.lame_internal_flags* %283, [2 x %struct.III_psy_ratio]* %284)
  br label %if.end364

if.end364:                                        ; preds = %for.end363, %land.lhs.true319, %if.end316
  %285 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc365 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %285, i32 0, i32 12
  %frame_number = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc365, i32 0, i32 3
  %286 = load i32, i32* %frame_number, align 4
  %inc366 = add nsw i32 %286, 1
  store i32 %inc366, i32* %frame_number, align 4
  %287 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void @updateStats(%struct.lame_internal_flags* %287)
  %288 = load i32, i32* %mp3count, align 4
  store i32 %288, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end364, %if.then31
  %289 = load i32, i32* %retval, align 4
  ret i32 %289
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @lame_encode_frame_init(%struct.lame_internal_flags* %gfc, float** %inbuf) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %inbuf.addr = alloca float**, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %primebuff0 = alloca [2014 x float], align 16
  %primebuff1 = alloca [2014 x float], align 16
  %framesize = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float** %inbuf, float*** %inbuf.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %lame_encode_frame_init = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 2
  %2 = load i32, i32* %lame_encode_frame_init, align 8
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.end43

if.then:                                          ; preds = %entry
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 15
  %4 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %4
  store i32 %mul, i32* %framesize, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %lame_encode_frame_init2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 2
  store i32 1, i32* %lame_encode_frame_init2, align 8
  %arraydecay = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff0, i32 0, i32 0
  %6 = bitcast float* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %6, i8 0, i32 8056, i1 false)
  %arraydecay3 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff1, i32 0, i32 0
  %7 = bitcast float* %arraydecay3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %7, i8 0, i32 8056, i1 false)
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %8 = load i32, i32* %i, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr4 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 15
  %10 = load i32, i32* %mode_gr4, align 4
  %add = add nsw i32 1, %10
  %mul5 = mul nsw i32 576, %add
  %add6 = add nsw i32 286, %mul5
  %cmp7 = icmp slt i32 %8, %add6
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %framesize, align 4
  %cmp8 = icmp slt i32 %11, %12
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %for.body
  %13 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff0, i32 0, i32 %13
  store float 0.000000e+00, float* %arrayidx, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 14
  %15 = load i32, i32* %channels_out, align 4
  %cmp10 = icmp eq i32 %15, 2
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %if.then9
  %16 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff1, i32 0, i32 %16
  store float 0.000000e+00, float* %arrayidx12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then11, %if.then9
  br label %if.end23

if.else:                                          ; preds = %for.body
  %17 = load float**, float*** %inbuf.addr, align 4
  %arrayidx13 = getelementptr inbounds float*, float** %17, i32 0
  %18 = load float*, float** %arrayidx13, align 4
  %19 = load i32, i32* %j, align 4
  %arrayidx14 = getelementptr inbounds float, float* %18, i32 %19
  %20 = load float, float* %arrayidx14, align 4
  %21 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff0, i32 0, i32 %21
  store float %20, float* %arrayidx15, align 4
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out16 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %22, i32 0, i32 14
  %23 = load i32, i32* %channels_out16, align 4
  %cmp17 = icmp eq i32 %23, 2
  br i1 %cmp17, label %if.then18, label %if.end22

if.then18:                                        ; preds = %if.else
  %24 = load float**, float*** %inbuf.addr, align 4
  %arrayidx19 = getelementptr inbounds float*, float** %24, i32 1
  %25 = load float*, float** %arrayidx19, align 4
  %26 = load i32, i32* %j, align 4
  %arrayidx20 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx20, align 4
  %28 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff1, i32 0, i32 %28
  store float %27, float* %arrayidx21, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then18, %if.else
  %29 = load i32, i32* %j, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %j, align 4
  br label %if.end23

if.end23:                                         ; preds = %if.end22, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end23
  %30 = load i32, i32* %i, align 4
  %inc24 = add nsw i32 %30, 1
  store i32 %inc24, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc38, %for.end
  %31 = load i32, i32* %gr, align 4
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr26 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 15
  %33 = load i32, i32* %mode_gr26, align 4
  %cmp27 = icmp slt i32 %31, %33
  br i1 %cmp27, label %for.body28, label %for.end40

for.body28:                                       ; preds = %for.cond25
  store i32 0, i32* %ch, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc35, %for.body28
  %34 = load i32, i32* %ch, align 4
  %35 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out30 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %35, i32 0, i32 14
  %36 = load i32, i32* %channels_out30, align 4
  %cmp31 = icmp slt i32 %34, %36
  br i1 %cmp31, label %for.body32, label %for.end37

for.body32:                                       ; preds = %for.cond29
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %37, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %38 = load i32, i32* %gr, align 4
  %arrayidx33 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %38
  %39 = load i32, i32* %ch, align 4
  %arrayidx34 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx33, i32 0, i32 %39
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx34, i32 0, i32 9
  store i32 2, i32* %block_type, align 4
  br label %for.inc35

for.inc35:                                        ; preds = %for.body32
  %40 = load i32, i32* %ch, align 4
  %inc36 = add nsw i32 %40, 1
  store i32 %inc36, i32* %ch, align 4
  br label %for.cond29

for.end37:                                        ; preds = %for.cond29
  br label %for.inc38

for.inc38:                                        ; preds = %for.end37
  %41 = load i32, i32* %gr, align 4
  %inc39 = add nsw i32 %41, 1
  store i32 %inc39, i32* %gr, align 4
  br label %for.cond25

for.end40:                                        ; preds = %for.cond25
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay41 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff0, i32 0, i32 0
  %arraydecay42 = getelementptr inbounds [2014 x float], [2014 x float]* %primebuff1, i32 0, i32 0
  call void @mdct_sub48(%struct.lame_internal_flags* %42, float* %arraydecay41, float* %arraydecay42)
  br label %if.end43

if.end43:                                         ; preds = %for.end40, %entry
  ret void
}

declare i32 @L3psycho_anal_vbr(%struct.lame_internal_flags*, float**, i32, [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]*, float*, float*, float*, i32*) #3

; Function Attrs: noinline nounwind optnone
define internal void @adjust_ATH(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %gr2_max = alloca float, align 4
  %max_pow = alloca float, align 4
  %adj_lim_new = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 21
  %2 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 8
  %use_adjust = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %2, i32 0, i32 0
  %3 = load i32, i32* %use_adjust, align 4
  %cmp = icmp eq i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 21
  %5 = load %struct.ATH_t*, %struct.ATH_t** %ATH2, align 8
  %adjust_factor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %5, i32 0, i32 2
  store float 1.000000e+00, float* %adjust_factor, align 4
  br label %if.end107

if.end:                                           ; preds = %entry
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 10
  %loudness_sq = getelementptr inbounds %struct.PsyResult_t, %struct.PsyResult_t* %ov_psy, i32 0, i32 0
  %arrayidx = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %loudness_sq, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 8
  store float %7, float* %max_pow, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_psy4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 10
  %loudness_sq5 = getelementptr inbounds %struct.PsyResult_t, %struct.PsyResult_t* %ov_psy4, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %loudness_sq5, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx6, i32 0, i32 0
  %9 = load float, float* %arrayidx7, align 8
  store float %9, float* %gr2_max, align 4
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 14
  %11 = load i32, i32* %channels_out, align 4
  %cmp8 = icmp eq i32 %11, 2
  br i1 %cmp8, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.end
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_psy10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 10
  %loudness_sq11 = getelementptr inbounds %struct.PsyResult_t, %struct.PsyResult_t* %ov_psy10, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %loudness_sq11, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx12, i32 0, i32 1
  %13 = load float, float* %arrayidx13, align 4
  %14 = load float, float* %max_pow, align 4
  %add = fadd float %14, %13
  store float %add, float* %max_pow, align 4
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_psy14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 10
  %loudness_sq15 = getelementptr inbounds %struct.PsyResult_t, %struct.PsyResult_t* %ov_psy14, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [2 x [2 x float]], [2 x [2 x float]]* %loudness_sq15, i32 0, i32 1
  %arrayidx17 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx16, i32 0, i32 1
  %16 = load float, float* %arrayidx17, align 4
  %17 = load float, float* %gr2_max, align 4
  %add18 = fadd float %17, %16
  store float %add18, float* %gr2_max, align 4
  br label %if.end21

if.else:                                          ; preds = %if.end
  %18 = load float, float* %max_pow, align 4
  %19 = load float, float* %max_pow, align 4
  %add19 = fadd float %19, %18
  store float %add19, float* %max_pow, align 4
  %20 = load float, float* %gr2_max, align 4
  %21 = load float, float* %gr2_max, align 4
  %add20 = fadd float %21, %20
  store float %add20, float* %gr2_max, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.else, %if.then9
  %22 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %22, i32 0, i32 15
  %23 = load i32, i32* %mode_gr, align 4
  %cmp22 = icmp eq i32 %23, 2
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %24 = load float, float* %max_pow, align 4
  %25 = load float, float* %gr2_max, align 4
  %cmp24 = fcmp ogt float %24, %25
  br i1 %cmp24, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then23
  %26 = load float, float* %max_pow, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then23
  %27 = load float, float* %gr2_max, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %26, %cond.true ], [ %27, %cond.false ]
  store float %cond, float* %max_pow, align 4
  br label %if.end25

if.end25:                                         ; preds = %cond.end, %if.end21
  %28 = load float, float* %max_pow, align 4
  %conv = fpext float %28 to double
  %mul = fmul double %conv, 5.000000e-01
  %conv26 = fptrunc double %mul to float
  store float %conv26, float* %max_pow, align 4
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH27 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 21
  %30 = load %struct.ATH_t*, %struct.ATH_t** %ATH27, align 8
  %aa_sensitivity_p = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %30, i32 0, i32 1
  %31 = load float, float* %aa_sensitivity_p, align 4
  %32 = load float, float* %max_pow, align 4
  %mul28 = fmul float %32, %31
  store float %mul28, float* %max_pow, align 4
  %33 = load float, float* %max_pow, align 4
  %conv29 = fpext float %33 to double
  %cmp30 = fcmp ogt double %conv29, 3.125000e-02
  br i1 %cmp30, label %if.then32, label %if.else56

if.then32:                                        ; preds = %if.end25
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH33 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 21
  %35 = load %struct.ATH_t*, %struct.ATH_t** %ATH33, align 8
  %adjust_factor34 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %35, i32 0, i32 2
  %36 = load float, float* %adjust_factor34, align 4
  %conv35 = fpext float %36 to double
  %cmp36 = fcmp oge double %conv35, 1.000000e+00
  br i1 %cmp36, label %if.then38, label %if.else41

if.then38:                                        ; preds = %if.then32
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH39 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %37, i32 0, i32 21
  %38 = load %struct.ATH_t*, %struct.ATH_t** %ATH39, align 8
  %adjust_factor40 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %38, i32 0, i32 2
  store float 1.000000e+00, float* %adjust_factor40, align 4
  br label %if.end53

if.else41:                                        ; preds = %if.then32
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH42 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 21
  %40 = load %struct.ATH_t*, %struct.ATH_t** %ATH42, align 8
  %adjust_factor43 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %40, i32 0, i32 2
  %41 = load float, float* %adjust_factor43, align 4
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %42, i32 0, i32 21
  %43 = load %struct.ATH_t*, %struct.ATH_t** %ATH44, align 8
  %adjust_limit = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %43, i32 0, i32 3
  %44 = load float, float* %adjust_limit, align 4
  %cmp45 = fcmp olt float %41, %44
  br i1 %cmp45, label %if.then47, label %if.end52

if.then47:                                        ; preds = %if.else41
  %45 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH48 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %45, i32 0, i32 21
  %46 = load %struct.ATH_t*, %struct.ATH_t** %ATH48, align 8
  %adjust_limit49 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %46, i32 0, i32 3
  %47 = load float, float* %adjust_limit49, align 4
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH50 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %48, i32 0, i32 21
  %49 = load %struct.ATH_t*, %struct.ATH_t** %ATH50, align 8
  %adjust_factor51 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %49, i32 0, i32 2
  store float %47, float* %adjust_factor51, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.then47, %if.else41
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then38
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH54 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %50, i32 0, i32 21
  %51 = load %struct.ATH_t*, %struct.ATH_t** %ATH54, align 8
  %adjust_limit55 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %51, i32 0, i32 3
  store float 1.000000e+00, float* %adjust_limit55, align 4
  br label %if.end107

if.else56:                                        ; preds = %if.end25
  %52 = load float, float* %max_pow, align 4
  %conv57 = fpext float %52 to double
  %mul58 = fmul double 3.198000e+01, %conv57
  %add59 = fadd double %mul58, 6.250000e-04
  %conv60 = fptrunc double %add59 to float
  store float %conv60, float* %adj_lim_new, align 4
  %53 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH61 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %53, i32 0, i32 21
  %54 = load %struct.ATH_t*, %struct.ATH_t** %ATH61, align 8
  %adjust_factor62 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %54, i32 0, i32 2
  %55 = load float, float* %adjust_factor62, align 4
  %56 = load float, float* %adj_lim_new, align 4
  %cmp63 = fcmp oge float %55, %56
  br i1 %cmp63, label %if.then65, label %if.else82

if.then65:                                        ; preds = %if.else56
  %57 = load float, float* %adj_lim_new, align 4
  %conv66 = fpext float %57 to double
  %mul67 = fmul double %conv66, 0x3FB3333333333333
  %add68 = fadd double %mul67, 9.250000e-01
  %58 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH69 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %58, i32 0, i32 21
  %59 = load %struct.ATH_t*, %struct.ATH_t** %ATH69, align 8
  %adjust_factor70 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %59, i32 0, i32 2
  %60 = load float, float* %adjust_factor70, align 4
  %conv71 = fpext float %60 to double
  %mul72 = fmul double %conv71, %add68
  %conv73 = fptrunc double %mul72 to float
  store float %conv73, float* %adjust_factor70, align 4
  %61 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH74 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %61, i32 0, i32 21
  %62 = load %struct.ATH_t*, %struct.ATH_t** %ATH74, align 8
  %adjust_factor75 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %62, i32 0, i32 2
  %63 = load float, float* %adjust_factor75, align 4
  %64 = load float, float* %adj_lim_new, align 4
  %cmp76 = fcmp olt float %63, %64
  br i1 %cmp76, label %if.then78, label %if.end81

if.then78:                                        ; preds = %if.then65
  %65 = load float, float* %adj_lim_new, align 4
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH79 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %66, i32 0, i32 21
  %67 = load %struct.ATH_t*, %struct.ATH_t** %ATH79, align 8
  %adjust_factor80 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %67, i32 0, i32 2
  store float %65, float* %adjust_factor80, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then78, %if.then65
  br label %if.end104

if.else82:                                        ; preds = %if.else56
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH83 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %68, i32 0, i32 21
  %69 = load %struct.ATH_t*, %struct.ATH_t** %ATH83, align 8
  %adjust_limit84 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %69, i32 0, i32 3
  %70 = load float, float* %adjust_limit84, align 4
  %71 = load float, float* %adj_lim_new, align 4
  %cmp85 = fcmp oge float %70, %71
  br i1 %cmp85, label %if.then87, label %if.else90

if.then87:                                        ; preds = %if.else82
  %72 = load float, float* %adj_lim_new, align 4
  %73 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH88 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %73, i32 0, i32 21
  %74 = load %struct.ATH_t*, %struct.ATH_t** %ATH88, align 8
  %adjust_factor89 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %74, i32 0, i32 2
  store float %72, float* %adjust_factor89, align 4
  br label %if.end103

if.else90:                                        ; preds = %if.else82
  %75 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH91 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %75, i32 0, i32 21
  %76 = load %struct.ATH_t*, %struct.ATH_t** %ATH91, align 8
  %adjust_factor92 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %76, i32 0, i32 2
  %77 = load float, float* %adjust_factor92, align 4
  %78 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH93 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %78, i32 0, i32 21
  %79 = load %struct.ATH_t*, %struct.ATH_t** %ATH93, align 8
  %adjust_limit94 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %79, i32 0, i32 3
  %80 = load float, float* %adjust_limit94, align 4
  %cmp95 = fcmp olt float %77, %80
  br i1 %cmp95, label %if.then97, label %if.end102

if.then97:                                        ; preds = %if.else90
  %81 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH98 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %81, i32 0, i32 21
  %82 = load %struct.ATH_t*, %struct.ATH_t** %ATH98, align 8
  %adjust_limit99 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %82, i32 0, i32 3
  %83 = load float, float* %adjust_limit99, align 4
  %84 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH100 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %84, i32 0, i32 21
  %85 = load %struct.ATH_t*, %struct.ATH_t** %ATH100, align 8
  %adjust_factor101 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %85, i32 0, i32 2
  store float %83, float* %adjust_factor101, align 4
  br label %if.end102

if.end102:                                        ; preds = %if.then97, %if.else90
  br label %if.end103

if.end103:                                        ; preds = %if.end102, %if.then87
  br label %if.end104

if.end104:                                        ; preds = %if.end103, %if.end81
  %86 = load float, float* %adj_lim_new, align 4
  %87 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH105 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %87, i32 0, i32 21
  %88 = load %struct.ATH_t*, %struct.ATH_t** %ATH105, align 8
  %adjust_limit106 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %88, i32 0, i32 3
  store float %86, float* %adjust_limit106, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.then, %if.end104, %if.end53
  ret void
}

declare void @mdct_sub48(%struct.lame_internal_flags*, float*, float*) #3

declare void @CBR_iteration_loop(%struct.lame_internal_flags*, [2 x float]*, float*, [2 x %struct.III_psy_ratio]*) #3

declare void @ABR_iteration_loop(%struct.lame_internal_flags*, [2 x float]*, float*, [2 x %struct.III_psy_ratio]*) #3

declare void @VBR_old_iteration_loop(%struct.lame_internal_flags*, [2 x float]*, float*, [2 x %struct.III_psy_ratio]*) #3

declare void @VBR_new_iteration_loop(%struct.lame_internal_flags*, [2 x float]*, float*, [2 x %struct.III_psy_ratio]*) #3

declare i32 @format_bitstream(%struct.lame_internal_flags*) #3

declare i32 @copy_buffer(%struct.lame_internal_flags*, i8*, i32, i32) #3

declare void @AddVbrFrame(%struct.lame_internal_flags*) #3

declare void @set_frame_pinfo(%struct.lame_internal_flags*, [2 x %struct.III_psy_ratio]*) #3

; Function Attrs: noinline nounwind optnone
define internal void @updateStats(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %bt = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %2, i32 0, i32 0
  %3 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %3, i32 0, i32 2
  %4 = load i32, i32* %bitrate_index, align 4
  %arrayidx = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist, i32 0, i32 %4
  %arrayidx2 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx, i32 0, i32 4
  %5 = load i32, i32* %arrayidx2, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %arrayidx2, align 4
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist3 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist3, i32 0, i32 15
  %arrayidx5 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx4, i32 0, i32 4
  %7 = load i32, i32* %arrayidx5, align 4
  %inc6 = add nsw i32 %7, 1
  store i32 %inc6, i32* %arrayidx5, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 14
  %9 = load i32, i32* %channels_out, align 4
  %cmp = icmp eq i32 %9, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %10 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist7 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %10, i32 0, i32 0
  %11 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index8 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %11, i32 0, i32 2
  %12 = load i32, i32* %bitrate_index8, align 4
  %arrayidx9 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist7, i32 0, i32 %12
  %13 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %13, i32 0, i32 5
  %14 = load i32, i32* %mode_ext, align 4
  %arrayidx10 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx9, i32 0, i32 %14
  %15 = load i32, i32* %arrayidx10, align 4
  %inc11 = add nsw i32 %15, 1
  store i32 %inc11, i32* %arrayidx10, align 4
  %16 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_channelmode_hist12 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [16 x [5 x i32]], [16 x [5 x i32]]* %bitrate_channelmode_hist12, i32 0, i32 15
  %17 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %mode_ext14 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %17, i32 0, i32 5
  %18 = load i32, i32* %mode_ext14, align 4
  %arrayidx15 = getelementptr inbounds [5 x i32], [5 x i32]* %arrayidx13, i32 0, i32 %18
  %19 = load i32, i32* %arrayidx15, align 4
  %inc16 = add nsw i32 %19, 1
  store i32 %inc16, i32* %arrayidx15, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc48, %if.end
  %20 = load i32, i32* %gr, align 4
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 15
  %22 = load i32, i32* %mode_gr, align 4
  %cmp17 = icmp slt i32 %20, %22
  br i1 %cmp17, label %for.body, label %for.end50

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc, %for.body
  %23 = load i32, i32* %ch, align 4
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out19 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 14
  %25 = load i32, i32* %channels_out19, align 4
  %cmp20 = icmp slt i32 %23, %25
  br i1 %cmp20, label %for.body21, label %for.end

for.body21:                                       ; preds = %for.cond18
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %27 = load i32, i32* %gr, align 4
  %arrayidx22 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %27
  %28 = load i32, i32* %ch, align 4
  %arrayidx23 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx22, i32 0, i32 %28
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx23, i32 0, i32 9
  %29 = load i32, i32* %block_type, align 4
  store i32 %29, i32* %bt, align 4
  %30 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %30, i32 0, i32 7
  %tt25 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side24, i32 0, i32 0
  %31 = load i32, i32* %gr, align 4
  %arrayidx26 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt25, i32 0, i32 %31
  %32 = load i32, i32* %ch, align 4
  %arrayidx27 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx26, i32 0, i32 %32
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx27, i32 0, i32 10
  %33 = load i32, i32* %mixed_block_flag, align 4
  %tobool = icmp ne i32 %33, 0
  br i1 %tobool, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.body21
  store i32 4, i32* %bt, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.then28, %for.body21
  %34 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %34, i32 0, i32 1
  %35 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index30 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %35, i32 0, i32 2
  %36 = load i32, i32* %bitrate_index30, align 4
  %arrayidx31 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist, i32 0, i32 %36
  %37 = load i32, i32* %bt, align 4
  %arrayidx32 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx31, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx32, align 4
  %inc33 = add nsw i32 %38, 1
  store i32 %inc33, i32* %arrayidx32, align 4
  %39 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist34 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %39, i32 0, i32 1
  %40 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index35 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %40, i32 0, i32 2
  %41 = load i32, i32* %bitrate_index35, align 4
  %arrayidx36 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist34, i32 0, i32 %41
  %arrayidx37 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx36, i32 0, i32 5
  %42 = load i32, i32* %arrayidx37, align 4
  %inc38 = add nsw i32 %42, 1
  store i32 %inc38, i32* %arrayidx37, align 4
  %43 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist39 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %43, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist39, i32 0, i32 15
  %44 = load i32, i32* %bt, align 4
  %arrayidx41 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx40, i32 0, i32 %44
  %45 = load i32, i32* %arrayidx41, align 4
  %inc42 = add nsw i32 %45, 1
  store i32 %inc42, i32* %arrayidx41, align 4
  %46 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_blocktype_hist43 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %46, i32 0, i32 1
  %arrayidx44 = getelementptr inbounds [16 x [6 x i32]], [16 x [6 x i32]]* %bitrate_blocktype_hist43, i32 0, i32 15
  %arrayidx45 = getelementptr inbounds [6 x i32], [6 x i32]* %arrayidx44, i32 0, i32 5
  %47 = load i32, i32* %arrayidx45, align 4
  %inc46 = add nsw i32 %47, 1
  store i32 %inc46, i32* %arrayidx45, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %48 = load i32, i32* %ch, align 4
  %inc47 = add nsw i32 %48, 1
  store i32 %inc47, i32* %ch, align 4
  br label %for.cond18

for.end:                                          ; preds = %for.cond18
  br label %for.inc48

for.inc48:                                        ; preds = %for.end
  %49 = load i32, i32* %gr, align 4
  %inc49 = add nsw i32 %49, 1
  store i32 %inc49, i32* %gr, align 4
  br label %for.cond

for.end50:                                        ; preds = %for.cond
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
