; ModuleID = 'id3tag.c'
source_filename = "id3tag.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.2, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon.0], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon.0 = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.1, %struct.anon.1 }
%struct.anon.1 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.2 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.anon.3 = type { i32, i32, i32 }

@genre_alpha_map = internal constant [148 x i32] [i32 123, i32 34, i32 74, i32 73, i32 99, i32 20, i32 40, i32 26, i32 145, i32 90, i32 116, i32 41, i32 135, i32 85, i32 96, i32 138, i32 89, i32 0, i32 107, i32 132, i32 65, i32 88, i32 104, i32 102, i32 97, i32 136, i32 61, i32 141, i32 32, i32 1, i32 112, i32 128, i32 57, i32 140, i32 2, i32 139, i32 58, i32 3, i32 125, i32 50, i32 22, i32 4, i32 55, i32 127, i32 122, i32 120, i32 98, i32 52, i32 48, i32 54, i32 124, i32 25, i32 84, i32 80, i32 115, i32 81, i32 119, i32 5, i32 30, i32 36, i32 59, i32 126, i32 38, i32 49, i32 91, i32 6, i32 129, i32 79, i32 137, i32 7, i32 35, i32 100, i32 131, i32 19, i32 33, i32 46, i32 47, i32 8, i32 29, i32 146, i32 63, i32 86, i32 71, i32 45, i32 142, i32 9, i32 77, i32 82, i32 64, i32 133, i32 10, i32 66, i32 39, i32 11, i32 103, i32 12, i32 75, i32 134, i32 13, i32 53, i32 62, i32 109, i32 117, i32 23, i32 108, i32 92, i32 67, i32 93, i32 43, i32 121, i32 15, i32 68, i32 14, i32 16, i32 76, i32 87, i32 118, i32 17, i32 78, i32 143, i32 114, i32 110, i32 69, i32 21, i32 111, i32 95, i32 105, i32 42, i32 37, i32 24, i32 56, i32 44, i32 101, i32 83, i32 94, i32 106, i32 147, i32 113, i32 18, i32 51, i32 130, i32 144, i32 60, i32 70, i32 31, i32 72, i32 27, i32 28], align 16
@genre_names = internal constant [148 x i8*] [i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.7, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.8, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.9, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.10, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.11, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.12, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.13, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.14, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.15, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.16, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.17, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.18, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.19, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.20, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.21, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.22, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.23, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.24, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.25, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.26, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.27, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.28, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.29, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.30, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.31, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.32, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.33, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.34, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.35, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.36, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.37, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.38, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.39, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.40, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.41, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.42, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.43, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.44, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.45, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.46, i32 0, i32 0), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.47, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.48, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.49, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.50, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.51, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.52, i32 0, i32 0), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.53, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.54, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.55, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.56, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.57, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.58, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.59, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.60, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.61, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.62, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.63, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.64, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.65, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.66, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.67, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.68, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.69, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.70, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.71, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.72, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.73, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.74, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.75, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.76, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.77, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.78, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.79, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.80, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.81, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.82, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.83, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.84, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.85, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.86, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.87, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.88, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.89, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.90, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.91, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.92, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.93, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.94, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.95, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.96, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.97, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.98, i32 0, i32 0), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.99, i32 0, i32 0), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @.str.100, i32 0, i32 0), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.101, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.102, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.103, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.104, i32 0, i32 0), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.105, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.106, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.107, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.108, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.109, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.110, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.111, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.112, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.113, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.114, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.115, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.116, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.117, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.118, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.119, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.120, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.121, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.122, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.123, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.124, i32 0, i32 0), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.125, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.126, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.127, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.128, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.129, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.130, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.131, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.132, i32 0, i32 0), i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.133, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.134, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.135, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.136, i32 0, i32 0), i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.137, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.138, i32 0, i32 0), i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.139, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.140, i32 0, i32 0), i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.141, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.142, i32 0, i32 0), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @.str.143, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.144, i32 0, i32 0), i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.145, i32 0, i32 0), i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.146, i32 0, i32 0), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.147, i32 0, i32 0), i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.148, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.149, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.150, i32 0, i32 0), i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.151, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8], [6 x i8]* @.str.152, i32 0, i32 0), i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.153, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.154, i32 0, i32 0)], align 16
@.str = private unnamed_addr constant [4 x i8] c"PNG\00", align 1
@.str.1 = private unnamed_addr constant [5 x i8] c"GIF8\00", align 1
@.str.2 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@lame_get_id3v2_tag.mime_jpeg = internal global i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.3, i32 0, i32 0), align 4
@.str.3 = private unnamed_addr constant [11 x i8] c"image/jpeg\00", align 1
@lame_get_id3v2_tag.mime_png = internal global i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.4, i32 0, i32 0), align 4
@.str.4 = private unnamed_addr constant [10 x i8] c"image/png\00", align 1
@lame_get_id3v2_tag.mime_gif = internal global i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.5, i32 0, i32 0), align 4
@.str.5 = private unnamed_addr constant [10 x i8] c"image/gif\00", align 1
@.str.6 = private unnamed_addr constant [3 x i8] c"%d\00", align 1
@.str.7 = private unnamed_addr constant [6 x i8] c"Blues\00", align 1
@.str.8 = private unnamed_addr constant [13 x i8] c"Classic Rock\00", align 1
@.str.9 = private unnamed_addr constant [8 x i8] c"Country\00", align 1
@.str.10 = private unnamed_addr constant [6 x i8] c"Dance\00", align 1
@.str.11 = private unnamed_addr constant [6 x i8] c"Disco\00", align 1
@.str.12 = private unnamed_addr constant [5 x i8] c"Funk\00", align 1
@.str.13 = private unnamed_addr constant [7 x i8] c"Grunge\00", align 1
@.str.14 = private unnamed_addr constant [8 x i8] c"Hip-Hop\00", align 1
@.str.15 = private unnamed_addr constant [5 x i8] c"Jazz\00", align 1
@.str.16 = private unnamed_addr constant [6 x i8] c"Metal\00", align 1
@.str.17 = private unnamed_addr constant [8 x i8] c"New Age\00", align 1
@.str.18 = private unnamed_addr constant [7 x i8] c"Oldies\00", align 1
@.str.19 = private unnamed_addr constant [6 x i8] c"Other\00", align 1
@.str.20 = private unnamed_addr constant [4 x i8] c"Pop\00", align 1
@.str.21 = private unnamed_addr constant [4 x i8] c"R&B\00", align 1
@.str.22 = private unnamed_addr constant [4 x i8] c"Rap\00", align 1
@.str.23 = private unnamed_addr constant [7 x i8] c"Reggae\00", align 1
@.str.24 = private unnamed_addr constant [5 x i8] c"Rock\00", align 1
@.str.25 = private unnamed_addr constant [7 x i8] c"Techno\00", align 1
@.str.26 = private unnamed_addr constant [11 x i8] c"Industrial\00", align 1
@.str.27 = private unnamed_addr constant [12 x i8] c"Alternative\00", align 1
@.str.28 = private unnamed_addr constant [4 x i8] c"Ska\00", align 1
@.str.29 = private unnamed_addr constant [12 x i8] c"Death Metal\00", align 1
@.str.30 = private unnamed_addr constant [7 x i8] c"Pranks\00", align 1
@.str.31 = private unnamed_addr constant [11 x i8] c"Soundtrack\00", align 1
@.str.32 = private unnamed_addr constant [12 x i8] c"Euro-Techno\00", align 1
@.str.33 = private unnamed_addr constant [8 x i8] c"Ambient\00", align 1
@.str.34 = private unnamed_addr constant [9 x i8] c"Trip-Hop\00", align 1
@.str.35 = private unnamed_addr constant [6 x i8] c"Vocal\00", align 1
@.str.36 = private unnamed_addr constant [10 x i8] c"Jazz+Funk\00", align 1
@.str.37 = private unnamed_addr constant [7 x i8] c"Fusion\00", align 1
@.str.38 = private unnamed_addr constant [7 x i8] c"Trance\00", align 1
@.str.39 = private unnamed_addr constant [10 x i8] c"Classical\00", align 1
@.str.40 = private unnamed_addr constant [13 x i8] c"Instrumental\00", align 1
@.str.41 = private unnamed_addr constant [5 x i8] c"Acid\00", align 1
@.str.42 = private unnamed_addr constant [6 x i8] c"House\00", align 1
@.str.43 = private unnamed_addr constant [5 x i8] c"Game\00", align 1
@.str.44 = private unnamed_addr constant [11 x i8] c"Sound Clip\00", align 1
@.str.45 = private unnamed_addr constant [7 x i8] c"Gospel\00", align 1
@.str.46 = private unnamed_addr constant [6 x i8] c"Noise\00", align 1
@.str.47 = private unnamed_addr constant [17 x i8] c"Alternative Rock\00", align 1
@.str.48 = private unnamed_addr constant [5 x i8] c"Bass\00", align 1
@.str.49 = private unnamed_addr constant [5 x i8] c"Soul\00", align 1
@.str.50 = private unnamed_addr constant [5 x i8] c"Punk\00", align 1
@.str.51 = private unnamed_addr constant [6 x i8] c"Space\00", align 1
@.str.52 = private unnamed_addr constant [11 x i8] c"Meditative\00", align 1
@.str.53 = private unnamed_addr constant [17 x i8] c"Instrumental Pop\00", align 1
@.str.54 = private unnamed_addr constant [18 x i8] c"Instrumental Rock\00", align 1
@.str.55 = private unnamed_addr constant [7 x i8] c"Ethnic\00", align 1
@.str.56 = private unnamed_addr constant [7 x i8] c"Gothic\00", align 1
@.str.57 = private unnamed_addr constant [9 x i8] c"Darkwave\00", align 1
@.str.58 = private unnamed_addr constant [18 x i8] c"Techno-Industrial\00", align 1
@.str.59 = private unnamed_addr constant [11 x i8] c"Electronic\00", align 1
@.str.60 = private unnamed_addr constant [9 x i8] c"Pop-Folk\00", align 1
@.str.61 = private unnamed_addr constant [10 x i8] c"Eurodance\00", align 1
@.str.62 = private unnamed_addr constant [6 x i8] c"Dream\00", align 1
@.str.63 = private unnamed_addr constant [14 x i8] c"Southern Rock\00", align 1
@.str.64 = private unnamed_addr constant [7 x i8] c"Comedy\00", align 1
@.str.65 = private unnamed_addr constant [5 x i8] c"Cult\00", align 1
@.str.66 = private unnamed_addr constant [8 x i8] c"Gangsta\00", align 1
@.str.67 = private unnamed_addr constant [7 x i8] c"Top 40\00", align 1
@.str.68 = private unnamed_addr constant [14 x i8] c"Christian Rap\00", align 1
@.str.69 = private unnamed_addr constant [9 x i8] c"Pop/Funk\00", align 1
@.str.70 = private unnamed_addr constant [7 x i8] c"Jungle\00", align 1
@.str.71 = private unnamed_addr constant [10 x i8] c"Native US\00", align 1
@.str.72 = private unnamed_addr constant [8 x i8] c"Cabaret\00", align 1
@.str.73 = private unnamed_addr constant [9 x i8] c"New Wave\00", align 1
@.str.74 = private unnamed_addr constant [12 x i8] c"Psychedelic\00", align 1
@.str.75 = private unnamed_addr constant [5 x i8] c"Rave\00", align 1
@.str.76 = private unnamed_addr constant [10 x i8] c"Showtunes\00", align 1
@.str.77 = private unnamed_addr constant [8 x i8] c"Trailer\00", align 1
@.str.78 = private unnamed_addr constant [6 x i8] c"Lo-Fi\00", align 1
@.str.79 = private unnamed_addr constant [7 x i8] c"Tribal\00", align 1
@.str.80 = private unnamed_addr constant [10 x i8] c"Acid Punk\00", align 1
@.str.81 = private unnamed_addr constant [10 x i8] c"Acid Jazz\00", align 1
@.str.82 = private unnamed_addr constant [6 x i8] c"Polka\00", align 1
@.str.83 = private unnamed_addr constant [6 x i8] c"Retro\00", align 1
@.str.84 = private unnamed_addr constant [8 x i8] c"Musical\00", align 1
@.str.85 = private unnamed_addr constant [12 x i8] c"Rock & Roll\00", align 1
@.str.86 = private unnamed_addr constant [10 x i8] c"Hard Rock\00", align 1
@.str.87 = private unnamed_addr constant [5 x i8] c"Folk\00", align 1
@.str.88 = private unnamed_addr constant [10 x i8] c"Folk-Rock\00", align 1
@.str.89 = private unnamed_addr constant [14 x i8] c"National Folk\00", align 1
@.str.90 = private unnamed_addr constant [6 x i8] c"Swing\00", align 1
@.str.91 = private unnamed_addr constant [12 x i8] c"Fast Fusion\00", align 1
@.str.92 = private unnamed_addr constant [6 x i8] c"Bebob\00", align 1
@.str.93 = private unnamed_addr constant [6 x i8] c"Latin\00", align 1
@.str.94 = private unnamed_addr constant [8 x i8] c"Revival\00", align 1
@.str.95 = private unnamed_addr constant [7 x i8] c"Celtic\00", align 1
@.str.96 = private unnamed_addr constant [10 x i8] c"Bluegrass\00", align 1
@.str.97 = private unnamed_addr constant [11 x i8] c"Avantgarde\00", align 1
@.str.98 = private unnamed_addr constant [12 x i8] c"Gothic Rock\00", align 1
@.str.99 = private unnamed_addr constant [17 x i8] c"Progressive Rock\00", align 1
@.str.100 = private unnamed_addr constant [17 x i8] c"Psychedelic Rock\00", align 1
@.str.101 = private unnamed_addr constant [15 x i8] c"Symphonic Rock\00", align 1
@.str.102 = private unnamed_addr constant [10 x i8] c"Slow Rock\00", align 1
@.str.103 = private unnamed_addr constant [9 x i8] c"Big Band\00", align 1
@.str.104 = private unnamed_addr constant [7 x i8] c"Chorus\00", align 1
@.str.105 = private unnamed_addr constant [15 x i8] c"Easy Listening\00", align 1
@.str.106 = private unnamed_addr constant [9 x i8] c"Acoustic\00", align 1
@.str.107 = private unnamed_addr constant [7 x i8] c"Humour\00", align 1
@.str.108 = private unnamed_addr constant [7 x i8] c"Speech\00", align 1
@.str.109 = private unnamed_addr constant [8 x i8] c"Chanson\00", align 1
@.str.110 = private unnamed_addr constant [6 x i8] c"Opera\00", align 1
@.str.111 = private unnamed_addr constant [14 x i8] c"Chamber Music\00", align 1
@.str.112 = private unnamed_addr constant [7 x i8] c"Sonata\00", align 1
@.str.113 = private unnamed_addr constant [9 x i8] c"Symphony\00", align 1
@.str.114 = private unnamed_addr constant [11 x i8] c"Booty Bass\00", align 1
@.str.115 = private unnamed_addr constant [7 x i8] c"Primus\00", align 1
@.str.116 = private unnamed_addr constant [12 x i8] c"Porn Groove\00", align 1
@.str.117 = private unnamed_addr constant [7 x i8] c"Satire\00", align 1
@.str.118 = private unnamed_addr constant [9 x i8] c"Slow Jam\00", align 1
@.str.119 = private unnamed_addr constant [5 x i8] c"Club\00", align 1
@.str.120 = private unnamed_addr constant [6 x i8] c"Tango\00", align 1
@.str.121 = private unnamed_addr constant [6 x i8] c"Samba\00", align 1
@.str.122 = private unnamed_addr constant [9 x i8] c"Folklore\00", align 1
@.str.123 = private unnamed_addr constant [7 x i8] c"Ballad\00", align 1
@.str.124 = private unnamed_addr constant [13 x i8] c"Power Ballad\00", align 1
@.str.125 = private unnamed_addr constant [14 x i8] c"Rhythmic Soul\00", align 1
@.str.126 = private unnamed_addr constant [10 x i8] c"Freestyle\00", align 1
@.str.127 = private unnamed_addr constant [5 x i8] c"Duet\00", align 1
@.str.128 = private unnamed_addr constant [10 x i8] c"Punk Rock\00", align 1
@.str.129 = private unnamed_addr constant [10 x i8] c"Drum Solo\00", align 1
@.str.130 = private unnamed_addr constant [11 x i8] c"A Cappella\00", align 1
@.str.131 = private unnamed_addr constant [11 x i8] c"Euro-House\00", align 1
@.str.132 = private unnamed_addr constant [11 x i8] c"Dance Hall\00", align 1
@.str.133 = private unnamed_addr constant [4 x i8] c"Goa\00", align 1
@.str.134 = private unnamed_addr constant [12 x i8] c"Drum & Bass\00", align 1
@.str.135 = private unnamed_addr constant [11 x i8] c"Club-House\00", align 1
@.str.136 = private unnamed_addr constant [9 x i8] c"Hardcore\00", align 1
@.str.137 = private unnamed_addr constant [7 x i8] c"Terror\00", align 1
@.str.138 = private unnamed_addr constant [6 x i8] c"Indie\00", align 1
@.str.139 = private unnamed_addr constant [8 x i8] c"BritPop\00", align 1
@.str.140 = private unnamed_addr constant [10 x i8] c"Negerpunk\00", align 1
@.str.141 = private unnamed_addr constant [11 x i8] c"Polsk Punk\00", align 1
@.str.142 = private unnamed_addr constant [5 x i8] c"Beat\00", align 1
@.str.143 = private unnamed_addr constant [18 x i8] c"Christian Gangsta\00", align 1
@.str.144 = private unnamed_addr constant [12 x i8] c"Heavy Metal\00", align 1
@.str.145 = private unnamed_addr constant [12 x i8] c"Black Metal\00", align 1
@.str.146 = private unnamed_addr constant [10 x i8] c"Crossover\00", align 1
@.str.147 = private unnamed_addr constant [23 x i8] c"Contemporary Christian\00", align 1
@.str.148 = private unnamed_addr constant [15 x i8] c"Christian Rock\00", align 1
@.str.149 = private unnamed_addr constant [9 x i8] c"Merengue\00", align 1
@.str.150 = private unnamed_addr constant [6 x i8] c"Salsa\00", align 1
@.str.151 = private unnamed_addr constant [13 x i8] c"Thrash Metal\00", align 1
@.str.152 = private unnamed_addr constant [6 x i8] c"Anime\00", align 1
@.str.153 = private unnamed_addr constant [5 x i8] c"JPop\00", align 1
@.str.154 = private unnamed_addr constant [9 x i8] c"SynthPop\00", align 1
@.str.155 = private unnamed_addr constant [24 x i8] c"LAME %s version %s (%s)\00", align 1
@.str.156 = private unnamed_addr constant [21 x i8] c"LAME version %s (%s)\00", align 1
@.str.157 = private unnamed_addr constant [4 x i8] c"%lu\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_genre_list(void (i32, i8*, i8*)* %handler, i8* %cookie) #0 {
entry:
  %handler.addr = alloca void (i32, i8*, i8*)*, align 4
  %cookie.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store void (i32, i8*, i8*)* %handler, void (i32, i8*, i8*)** %handler.addr, align 4
  store i8* %cookie, i8** %cookie.addr, align 4
  %0 = load void (i32, i8*, i8*)*, void (i32, i8*, i8*)** %handler.addr, align 4
  %tobool = icmp ne void (i32, i8*, i8*)* %0, null
  br i1 %tobool, label %if.then, label %if.end4

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 148
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %cmp1 = icmp slt i32 %2, 148
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [148 x i32], [148 x i32]* @genre_alpha_map, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  store i32 %4, i32* %j, align 4
  %5 = load void (i32, i8*, i8*)*, void (i32, i8*, i8*)** %handler.addr, align 4
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %j, align 4
  %arrayidx3 = getelementptr inbounds [148 x i8*], [148 x i8*]* @genre_names, i32 0, i32 %7
  %8 = load i8*, i8** %arrayidx3, align 4
  %9 = load i8*, i8** %cookie.addr, align 4
  call void %5(i32 %6, i8* %8, i8* %9)
  br label %if.end

if.end:                                           ; preds = %if.then2, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end4

if.end4:                                          ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_init(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void @free_id3tag(%struct.lame_internal_flags* %3)
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 16
  %5 = bitcast %struct.id3tag_spec* %tag_spec to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %5, i8 0, i32 60, i1 false)
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 16
  %genre_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 7
  store i32 255, i32* %genre_id3v1, align 4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %padding_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec2, i32 0, i32 10
  store i32 128, i32* %padding_size, align 8
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  call void @id3v2AddLameVersion(%struct.lame_global_struct* %8)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tobool = icmp ne %struct.lame_global_struct* %0, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  %tobool1 = icmp ne %struct.lame_internal_flags* %2, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %3 = phi i1 [ false, %entry ], [ %tobool1, %land.rhs ]
  %4 = zext i1 %3 to i64
  %cond = select i1 %3, i32 0, i32 1
  ret i32 %cond
}

declare void @free_id3tag(%struct.lame_internal_flags*) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @id3v2AddLameVersion(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer = alloca [1024 x i8], align 16
  %b = alloca i8*, align 4
  %v = alloca i8*, align 4
  %u = alloca i8*, align 4
  %lenb = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i8* @get_lame_os_bitness()
  store i8* %call, i8** %b, align 4
  %call1 = call i8* @get_lame_version()
  store i8* %call1, i8** %v, align 4
  %call2 = call i8* @get_lame_url()
  store i8* %call2, i8** %u, align 4
  %0 = load i8*, i8** %b, align 4
  %call3 = call i32 @strlen(i8* %0)
  store i32 %call3, i32* %lenb, align 4
  %1 = load i32, i32* %lenb, align 4
  %cmp = icmp ugt i32 %1, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %buffer, i32 0, i32 0
  %2 = load i8*, i8** %b, align 4
  %3 = load i8*, i8** %v, align 4
  %4 = load i8*, i8** %u, align 4
  %call4 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.155, i32 0, i32 0), i8* %2, i8* %3, i8* %4)
  br label %if.end

if.else:                                          ; preds = %entry
  %arraydecay5 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buffer, i32 0, i32 0
  %5 = load i8*, i8** %v, align 4
  %6 = load i8*, i8** %u, align 4
  %call6 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay5, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.156, i32 0, i32 0), i8* %5, i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arraydecay7 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buffer, i32 0, i32 0
  call void @copyV1ToV2(%struct.lame_global_struct* %7, i32 1414746949, i8* %arraydecay7)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_add_v2(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %4 = load i32, i32* %flags, align 8
  %and = and i32 %4, -5
  store i32 %and, i32* %flags, align 8
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 0
  %6 = load i32, i32* %flags2, align 8
  %or = or i32 %6, 2
  store i32 %or, i32* %flags2, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_v1_only(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %4 = load i32, i32* %flags, align 8
  %and = and i32 %4, -11
  store i32 %and, i32* %flags, align 8
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 0
  %6 = load i32, i32* %flags2, align 8
  %or = or i32 %6, 4
  store i32 %or, i32* %flags2, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_v2_only(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %4 = load i32, i32* %flags, align 8
  %and = and i32 %4, -5
  store i32 %and, i32* %flags, align 8
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 0
  %6 = load i32, i32* %flags2, align 8
  %or = or i32 %6, 8
  store i32 %or, i32* %flags2, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_space_v1(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %4 = load i32, i32* %flags, align 8
  %and = and i32 %4, -9
  store i32 %and, i32* %flags, align 8
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 0
  %6 = load i32, i32* %flags2, align 8
  %or = or i32 %6, 16
  store i32 %or, i32* %flags2, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_pad_v2(%struct.lame_global_struct* %gfp) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  call void @id3tag_set_pad(%struct.lame_global_struct* %0, i32 128)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_pad(%struct.lame_global_struct* %gfp, i32 %n) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %n.addr = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %4 = load i32, i32* %flags, align 8
  %and = and i32 %4, -5
  store i32 %and, i32* %flags, align 8
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec1, i32 0, i32 0
  %6 = load i32, i32* %flags2, align 8
  %or = or i32 %6, 32
  store i32 %or, i32* %flags2, align 8
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %flags4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec3, i32 0, i32 0
  %8 = load i32, i32* %flags4, align 8
  %or5 = or i32 %8, 2
  store i32 %or5, i32* %flags4, align 8
  %9 = load i32, i32* %n.addr, align 4
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 16
  %padding_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec6, i32 0, i32 10
  store i32 %9, i32* %padding_size, align 8
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_albumart(%struct.lame_global_struct* %gfp, i8* %image, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %image.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %mimetype = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %data = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %image, i8** %image.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 0, i32* %mimetype, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load i8*, i8** %image.addr, align 4
  %cmp = icmp ne i8* %3, null
  br i1 %cmp, label %if.then1, label %if.end36

if.then1:                                         ; preds = %if.end
  %4 = load i8*, i8** %image.addr, align 4
  store i8* %4, i8** %data, align 4
  %5 = load i32, i32* %size.addr, align 4
  %cmp2 = icmp ult i32 2, %5
  br i1 %cmp2, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then1
  %6 = load i8*, i8** %data, align 4
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 0
  %7 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %7 to i32
  %cmp3 = icmp eq i32 %conv, 255
  br i1 %cmp3, label %land.lhs.true5, label %if.else

land.lhs.true5:                                   ; preds = %land.lhs.true
  %8 = load i8*, i8** %data, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %8, i32 1
  %9 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %9 to i32
  %cmp8 = icmp eq i32 %conv7, 216
  br i1 %cmp8, label %if.then10, label %if.else

if.then10:                                        ; preds = %land.lhs.true5
  store i32 1, i32* %mimetype, align 4
  br label %if.end35

if.else:                                          ; preds = %land.lhs.true5, %land.lhs.true, %if.then1
  %10 = load i32, i32* %size.addr, align 4
  %cmp11 = icmp ult i32 4, %10
  br i1 %cmp11, label %land.lhs.true13, label %if.else24

land.lhs.true13:                                  ; preds = %if.else
  %11 = load i8*, i8** %data, align 4
  %arrayidx14 = getelementptr inbounds i8, i8* %11, i32 0
  %12 = load i8, i8* %arrayidx14, align 1
  %conv15 = zext i8 %12 to i32
  %cmp16 = icmp eq i32 %conv15, 137
  br i1 %cmp16, label %land.lhs.true18, label %if.else24

land.lhs.true18:                                  ; preds = %land.lhs.true13
  %13 = load i8*, i8** %data, align 4
  %arrayidx19 = getelementptr inbounds i8, i8* %13, i32 1
  %call20 = call i32 @strncmp(i8* %arrayidx19, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str, i32 0, i32 0), i32 3)
  %cmp21 = icmp eq i32 %call20, 0
  br i1 %cmp21, label %if.then23, label %if.else24

if.then23:                                        ; preds = %land.lhs.true18
  store i32 2, i32* %mimetype, align 4
  br label %if.end34

if.else24:                                        ; preds = %land.lhs.true18, %land.lhs.true13, %if.else
  %14 = load i32, i32* %size.addr, align 4
  %cmp25 = icmp ult i32 4, %14
  br i1 %cmp25, label %land.lhs.true27, label %if.else32

land.lhs.true27:                                  ; preds = %if.else24
  %15 = load i8*, i8** %data, align 4
  %call28 = call i32 @strncmp(i8* %15, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.1, i32 0, i32 0), i32 4)
  %cmp29 = icmp eq i32 %call28, 0
  br i1 %cmp29, label %if.then31, label %if.else32

if.then31:                                        ; preds = %land.lhs.true27
  store i32 3, i32* %mimetype, align 4
  br label %if.end33

if.else32:                                        ; preds = %land.lhs.true27, %if.else24
  store i32 -1, i32* %retval, align 4
  br label %return

if.end33:                                         ; preds = %if.then31
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.then23
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.then10
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 16
  %albumart = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 8
  %17 = load i8*, i8** %albumart, align 8
  %cmp37 = icmp ne i8* %17, null
  br i1 %cmp37, label %if.then39, label %if.end46

if.then39:                                        ; preds = %if.end36
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec40 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 16
  %albumart41 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec40, i32 0, i32 8
  %19 = load i8*, i8** %albumart41, align 8
  call void @free(i8* %19)
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec42 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 16
  %albumart43 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec42, i32 0, i32 8
  store i8* null, i8** %albumart43, align 8
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 16
  %albumart_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec44, i32 0, i32 9
  store i32 0, i32* %albumart_size, align 4
  %22 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec45 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %22, i32 0, i32 16
  %albumart_mimetype = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec45, i32 0, i32 11
  store i32 0, i32* %albumart_mimetype, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.then39, %if.end36
  %23 = load i32, i32* %size.addr, align 4
  %cmp47 = icmp ult i32 %23, 1
  br i1 %cmp47, label %if.then51, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end46
  %24 = load i32, i32* %mimetype, align 4
  %cmp49 = icmp eq i32 %24, 0
  br i1 %cmp49, label %if.then51, label %if.end52

if.then51:                                        ; preds = %lor.lhs.false, %if.end46
  store i32 0, i32* %retval, align 4
  br label %return

if.end52:                                         ; preds = %lor.lhs.false
  %25 = load i32, i32* %size.addr, align 4
  %call53 = call i8* @calloc(i32 %25, i32 1)
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec54 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 16
  %albumart55 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec54, i32 0, i32 8
  store i8* %call53, i8** %albumart55, align 8
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec56 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 16
  %albumart57 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec56, i32 0, i32 8
  %28 = load i8*, i8** %albumart57, align 8
  %cmp58 = icmp ne i8* %28, null
  br i1 %cmp58, label %if.then60, label %if.end68

if.then60:                                        ; preds = %if.end52
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec61 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 16
  %albumart62 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec61, i32 0, i32 8
  %30 = load i8*, i8** %albumart62, align 8
  %31 = load i8*, i8** %image.addr, align 4
  %32 = load i32, i32* %size.addr, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %30, i8* align 1 %31, i32 %32, i1 false)
  %33 = load i32, i32* %size.addr, align 4
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec63 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 16
  %albumart_size64 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec63, i32 0, i32 9
  store i32 %33, i32* %albumart_size64, align 4
  %35 = load i32, i32* %mimetype, align 4
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec65 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 16
  %albumart_mimetype66 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec65, i32 0, i32 11
  store i32 %35, i32* %albumart_mimetype66, align 4
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec67 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %37, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec67, i32 0, i32 0
  %38 = load i32, i32* %flags, align 8
  %or = or i32 %38, 1
  store i32 %or, i32* %flags, align 8
  %39 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  call void @id3tag_add_v2(%struct.lame_global_struct* %39)
  br label %if.end68

if.end68:                                         ; preds = %if.then60, %if.end52
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end68, %if.then51, %if.else32, %if.then
  %40 = load i32, i32* %retval, align 4
  ret i32 %40
}

declare i32 @strncmp(i8*, i8*, i32) #1

declare void @free(i8*) #1

declare i8* @calloc(i32, i32) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_textinfo_utf16(%struct.lame_global_struct* %gfp, i8* %id, i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %id.addr = alloca i8*, align 4
  %text.addr = alloca i16*, align 4
  %frame_id = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %id, i8** %id.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load i8*, i8** %id.addr, align 4
  %call = call i32 @toID3v2TagId(i8* %0)
  store i32 %call, i32* %frame_id, align 4
  %1 = load i32, i32* %frame_id, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1 = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %2)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load i16*, i16** %text.addr, align 4
  %cmp4 = icmp eq i16* %3, null
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %4 = load i16*, i16** %text.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx, align 2
  %call7 = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %5)
  %tobool8 = icmp ne i32 %call7, 0
  br i1 %tobool8, label %if.end10, label %if.then9

if.then9:                                         ; preds = %if.end6
  store i32 -3, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.end6
  %6 = load i32, i32* %frame_id, align 4
  %cmp11 = icmp eq i32 %6, 1415075928
  br i1 %cmp11, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end10
  %7 = load i32, i32* %frame_id, align 4
  %cmp12 = icmp eq i32 %7, 1465407576
  br i1 %cmp12, label %if.then15, label %lor.lhs.false13

lor.lhs.false13:                                  ; preds = %lor.lhs.false
  %8 = load i32, i32* %frame_id, align 4
  %cmp14 = icmp eq i32 %8, 1129270605
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %lor.lhs.false13, %lor.lhs.false, %if.end10
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %10 = load i32, i32* %frame_id, align 4
  %11 = load i16*, i16** %text.addr, align 4
  %call16 = call i32 @id3tag_set_userinfo_ucs2(%struct.lame_global_struct* %9, i32 %10, i16* %11)
  store i32 %call16, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %lor.lhs.false13
  %12 = load i32, i32* %frame_id, align 4
  %cmp18 = icmp eq i32 %12, 1413697358
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end17
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %14 = load i16*, i16** %text.addr, align 4
  %call20 = call i32 @id3tag_set_genre_utf16(%struct.lame_global_struct* %13, i16* %14)
  store i32 %call20, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end17
  %15 = load i32, i32* %frame_id, align 4
  %cmp22 = icmp eq i32 %15, 1346589524
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %16 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %17 = load i32, i32* %frame_id, align 4
  %18 = load i16*, i16** %text.addr, align 4
  %call24 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %16, i32 %17, i16* null, i16* %18)
  store i32 %call24, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end21
  %19 = load i32, i32* %frame_id, align 4
  %cmp26 = icmp eq i32 %19, 1431520594
  br i1 %cmp26, label %if.then27, label %if.end29

if.then27:                                        ; preds = %if.end25
  %20 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %21 = load i32, i32* %frame_id, align 4
  %22 = load i16*, i16** %text.addr, align 4
  %call28 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %20, i32 %21, i16* %22, i16* null)
  store i32 %call28, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.end25
  %23 = load i32, i32* %frame_id, align 4
  %cmp30 = icmp eq i32 %23, 1464223044
  br i1 %cmp30, label %if.then31, label %if.end33

if.then31:                                        ; preds = %if.end29
  %24 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %25 = load i32, i32* %frame_id, align 4
  %26 = load i16*, i16** %text.addr, align 4
  %call32 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %24, i32 %25, i16* %26, i16* null)
  store i32 %call32, i32* %retval, align 4
  br label %return

if.end33:                                         ; preds = %if.end29
  %27 = load i32, i32* %frame_id, align 4
  %call34 = call i32 @isFrameIdMatching(i32 %27, i32 1409286144)
  %tobool35 = icmp ne i32 %call34, 0
  br i1 %tobool35, label %if.then39, label %lor.lhs.false36

lor.lhs.false36:                                  ; preds = %if.end33
  %28 = load i32, i32* %frame_id, align 4
  %call37 = call i32 @isFrameIdMatching(i32 %28, i32 1459617792)
  %tobool38 = icmp ne i32 %call37, 0
  br i1 %tobool38, label %if.then39, label %if.end41

if.then39:                                        ; preds = %lor.lhs.false36, %if.end33
  %29 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %30 = load i32, i32* %frame_id, align 4
  %31 = load i16*, i16** %text.addr, align 4
  %call40 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %29, i32 %30, i16* null, i16* %31)
  store i32 %call40, i32* %retval, align 4
  br label %return

if.end41:                                         ; preds = %lor.lhs.false36
  store i32 -255, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end41, %if.then39, %if.then31, %if.then27, %if.then23, %if.then19, %if.then15, %if.then9, %if.then5, %if.then2, %if.then
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

; Function Attrs: noinline nounwind optnone
define internal i32 @toID3v2TagId(i8* %s) #0 {
entry:
  %retval = alloca i32, align 4
  %s.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %c = alloca i8, align 1
  %u = alloca i32, align 4
  store i8* %s, i8** %s.addr, align 4
  store i32 0, i32* %x, align 4
  %0 = load i8*, i8** %s.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %1 = load i32, i32* %i, align 4
  %cmp1 = icmp ult i32 %1, 4
  br i1 %cmp1, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %2 = load i8*, i8** %s.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %4 to i32
  %cmp2 = icmp ne i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %5 = phi i1 [ false, %for.cond ], [ %cmp2, %land.rhs ]
  br i1 %5, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %6 = load i8*, i8** %s.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %6, i32 %7
  %8 = load i8, i8* %arrayidx4, align 1
  store i8 %8, i8* %c, align 1
  %9 = load i8, i8* %c, align 1
  %conv5 = sext i8 %9 to i32
  %and = and i32 255, %conv5
  store i32 %and, i32* %u, align 4
  %10 = load i32, i32* %x, align 4
  %shl = shl i32 %10, 8
  store i32 %shl, i32* %x, align 4
  %11 = load i32, i32* %u, align 4
  %12 = load i32, i32* %x, align 4
  %or = or i32 %12, %11
  store i32 %or, i32* %x, align 4
  %13 = load i8, i8* %c, align 1
  %conv6 = sext i8 %13 to i32
  %cmp7 = icmp slt i32 %conv6, 65
  br i1 %cmp7, label %if.then12, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %14 = load i8, i8* %c, align 1
  %conv9 = sext i8 %14 to i32
  %cmp10 = icmp slt i32 90, %conv9
  br i1 %cmp10, label %if.then12, label %if.end22

if.then12:                                        ; preds = %lor.lhs.false, %for.body
  %15 = load i8, i8* %c, align 1
  %conv13 = sext i8 %15 to i32
  %cmp14 = icmp slt i32 %conv13, 48
  br i1 %cmp14, label %if.then20, label %lor.lhs.false16

lor.lhs.false16:                                  ; preds = %if.then12
  %16 = load i8, i8* %c, align 1
  %conv17 = sext i8 %16 to i32
  %cmp18 = icmp slt i32 57, %conv17
  br i1 %cmp18, label %if.then20, label %if.end21

if.then20:                                        ; preds = %lor.lhs.false16, %if.then12
  store i32 0, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %lor.lhs.false16
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end22
  %17 = load i32, i32* %i, align 4
  %inc = add i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  %18 = load i32, i32* %x, align 4
  store i32 %18, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then20, %if.then
  %19 = load i32, i32* %retval, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone
define internal i32 @hasUcs2ByteOrderMarker(i16 zeroext %bom) #0 {
entry:
  %retval = alloca i32, align 4
  %bom.addr = alloca i16, align 2
  store i16 %bom, i16* %bom.addr, align 2
  %0 = load i16, i16* %bom.addr, align 2
  %conv = zext i16 %0 to i32
  %cmp = icmp eq i32 %conv, 65534
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i16, i16* %bom.addr, align 2
  %conv2 = zext i16 %1 to i32
  %cmp3 = icmp eq i32 %conv2, 65279
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i32, i32* %retval, align 4
  ret i32 %2
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3tag_set_userinfo_ucs2(%struct.lame_global_struct* %gfp, i32 %id, i16* %fieldvalue) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %id.addr = alloca i32, align 4
  %fieldvalue.addr = alloca i16*, align 4
  %separator = alloca i16, align 2
  %rc = alloca i32, align 4
  %b = alloca i32, align 4
  %a = alloca i32, align 4
  %dsc = alloca i16*, align 4
  %val = alloca i16*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  store i16* %fieldvalue, i16** %fieldvalue.addr, align 4
  %0 = load i16*, i16** %fieldvalue.addr, align 4
  %call = call zeroext i16 @fromLatin1Char(i16* %0, i16 zeroext 61)
  store i16 %call, i16* %separator, align 2
  store i32 -7, i32* %rc, align 4
  %1 = load i16*, i16** %fieldvalue.addr, align 4
  %call1 = call i32 @local_ucs2_strlen(i16* %1)
  store i32 %call1, i32* %b, align 4
  %2 = load i16*, i16** %fieldvalue.addr, align 4
  %3 = load i16, i16* %separator, align 2
  %call2 = call i32 @local_ucs2_pos(i16* %2, i16 zeroext %3)
  store i32 %call2, i32* %a, align 4
  %4 = load i32, i32* %a, align 4
  %cmp = icmp sge i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i16* null, i16** %dsc, align 4
  store i16* null, i16** %val, align 4
  %5 = load i16*, i16** %fieldvalue.addr, align 4
  %6 = load i32, i32* %a, align 4
  %call3 = call i32 @local_ucs2_substr(i16** %dsc, i16* %5, i32 0, i32 %6)
  %7 = load i16*, i16** %fieldvalue.addr, align 4
  %8 = load i32, i32* %a, align 4
  %add = add nsw i32 %8, 1
  %9 = load i32, i32* %b, align 4
  %call4 = call i32 @local_ucs2_substr(i16** %val, i16* %7, i32 %add, i32 %9)
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %11 = load i32, i32* %id.addr, align 4
  %12 = load i16*, i16** %dsc, align 4
  %13 = load i16*, i16** %val, align 4
  %call5 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %10, i32 %11, i16* %12, i16* %13)
  store i32 %call5, i32* %rc, align 4
  %14 = load i16*, i16** %dsc, align 4
  %15 = bitcast i16* %14 to i8*
  call void @free(i8* %15)
  %16 = load i16*, i16** %val, align 4
  %17 = bitcast i16* %16 to i8*
  call void @free(i8* %17)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %18 = load i32, i32* %rc, align 4
  ret i32 %18
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3tag_set_genre_utf16(%struct.lame_global_struct* %gfp, i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %text.addr = alloca i16*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %ret = alloca i32, align 4
  %latin1 = alloca i8*, align 4
  %num = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load i16*, i16** %text.addr, align 4
  %cmp = icmp eq i16* %2, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -3, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i16*, i16** %text.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 0
  %4 = load i16, i16* %arrayidx, align 2
  %call = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end2, label %if.then1

if.then1:                                         ; preds = %if.end
  store i32 -3, i32* %retval, align 4
  br label %return

if.end2:                                          ; preds = %if.end
  %5 = load i16*, i16** %text.addr, align 4
  %call3 = call i32 @maybeLatin1(i16* %5)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end16

if.then5:                                         ; preds = %if.end2
  %6 = load i16*, i16** %text.addr, align 4
  %call6 = call i8* @local_strdup_utf16_to_latin1(i16* %6)
  store i8* %call6, i8** %latin1, align 4
  %7 = load i8*, i8** %latin1, align 4
  %call7 = call i32 @lookupGenre(i8* %7)
  store i32 %call7, i32* %num, align 4
  %8 = load i8*, i8** %latin1, align 4
  call void @free(i8* %8)
  %9 = load i32, i32* %num, align 4
  %cmp8 = icmp eq i32 %9, -1
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.then5
  store i32 -1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.then5
  %10 = load i32, i32* %num, align 4
  %cmp11 = icmp sge i32 %10, 0
  br i1 %cmp11, label %if.then12, label %if.end15

if.then12:                                        ; preds = %if.end10
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %12 = load i32, i32* %flags, align 8
  %or = or i32 %12, 1
  store i32 %or, i32* %flags, align 8
  %13 = load i32, i32* %num, align 4
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec13 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 16
  %genre_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec13, i32 0, i32 7
  store i32 %13, i32* %genre_id3v1, align 4
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %16 = load i32, i32* %num, align 4
  %arrayidx14 = getelementptr inbounds [148 x i8*], [148 x i8*]* @genre_names, i32 0, i32 %16
  %17 = load i8*, i8** %arrayidx14, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %15, i32 1413697358, i8* %17)
  store i32 0, i32* %retval, align 4
  br label %return

if.end15:                                         ; preds = %if.end10
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %if.end2
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %19 = load i16*, i16** %text.addr, align 4
  %call17 = call i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %18, i32 1413697358, i16* null, i16* %19)
  store i32 %call17, i32* %ret, align 4
  %20 = load i32, i32* %ret, align 4
  %cmp18 = icmp eq i32 %20, 0
  br i1 %cmp18, label %if.then19, label %if.end25

if.then19:                                        ; preds = %if.end16
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 16
  %flags21 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec20, i32 0, i32 0
  %22 = load i32, i32* %flags21, align 8
  %or22 = or i32 %22, 1
  store i32 %or22, i32* %flags21, align 8
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec23 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 16
  %genre_id3v124 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec23, i32 0, i32 7
  store i32 12, i32* %genre_id3v124, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then19, %if.end16
  %24 = load i32, i32* %ret, align 4
  store i32 %24, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end25, %if.then12, %if.then9, %if.then1, %if.then
  %25 = load i32, i32* %retval, align 4
  ret i32 %25
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3v2_add_ucs2_lng(%struct.lame_global_struct* %gfp, i32 %frame_id, i16* %desc, i16* %text) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %frame_id.addr = alloca i32, align 4
  %desc.addr = alloca i16*, align 4
  %text.addr = alloca i16*, align 4
  %lang = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store i16* %desc, i16** %desc.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i8* @id3v2_get_language(%struct.lame_global_struct* %0)
  store i8* %call, i8** %lang, align 4
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i32, i32* %frame_id.addr, align 4
  %3 = load i8*, i8** %lang, align 4
  %4 = load i16*, i16** %desc.addr, align 4
  %5 = load i16*, i16** %text.addr, align 4
  %call1 = call i32 @id3v2_add_ucs2(%struct.lame_global_struct* %1, i32 %2, i8* %3, i16* %4, i16* %5)
  ret i32 %call1
}

; Function Attrs: noinline nounwind optnone
define internal i32 @isFrameIdMatching(i32 %id, i32 %mask) #0 {
entry:
  %id.addr = alloca i32, align 4
  %mask.addr = alloca i32, align 4
  store i32 %id, i32* %id.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %1 = load i32, i32* %mask.addr, align 4
  %call = call i32 @frame_id_matches(i32 %0, i32 %1)
  %cmp = icmp eq i32 %call, 0
  %2 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_textinfo_ucs2(%struct.lame_global_struct* %gfp, i8* %id, i16* %text) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %id.addr = alloca i8*, align 4
  %text.addr = alloca i16*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %id, i8** %id.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %1 = load i8*, i8** %id.addr, align 4
  %2 = load i16*, i16** %text.addr, align 4
  %call = call i32 @id3tag_set_textinfo_utf16(%struct.lame_global_struct* %0, i8* %1, i16* %2)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_textinfo_latin1(%struct.lame_global_struct* %gfp, i8* %id, i8* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %id.addr = alloca i8*, align 4
  %text.addr = alloca i8*, align 4
  %frame_id = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %id, i8** %id.addr, align 4
  store i8* %text, i8** %text.addr, align 4
  %0 = load i8*, i8** %id.addr, align 4
  %call = call i32 @toID3v2TagId(i8* %0)
  store i32 %call, i32* %frame_id, align 4
  %1 = load i32, i32* %frame_id, align 4
  %cmp = icmp eq i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call1 = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %2)
  %tobool = icmp ne i32 %call1, 0
  br i1 %tobool, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %3 = load i8*, i8** %text.addr, align 4
  %cmp4 = icmp eq i8* %3, null
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %4 = load i32, i32* %frame_id, align 4
  %cmp7 = icmp eq i32 %4, 1415075928
  br i1 %cmp7, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end6
  %5 = load i32, i32* %frame_id, align 4
  %cmp8 = icmp eq i32 %5, 1465407576
  br i1 %cmp8, label %if.then11, label %lor.lhs.false9

lor.lhs.false9:                                   ; preds = %lor.lhs.false
  %6 = load i32, i32* %frame_id, align 4
  %cmp10 = icmp eq i32 %6, 1129270605
  br i1 %cmp10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %lor.lhs.false9, %lor.lhs.false, %if.end6
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %8 = load i32, i32* %frame_id, align 4
  %9 = load i8*, i8** %text.addr, align 4
  %call12 = call i32 @id3tag_set_userinfo_latin1(%struct.lame_global_struct* %7, i32 %8, i8* %9)
  store i32 %call12, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %lor.lhs.false9
  %10 = load i32, i32* %frame_id, align 4
  %cmp14 = icmp eq i32 %10, 1413697358
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %if.end13
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %12 = load i8*, i8** %text.addr, align 4
  %call16 = call i32 @id3tag_set_genre(%struct.lame_global_struct* %11, i8* %12)
  store i32 %call16, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.end13
  %13 = load i32, i32* %frame_id, align 4
  %cmp18 = icmp eq i32 %13, 1346589524
  br i1 %cmp18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end17
  %14 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %15 = load i32, i32* %frame_id, align 4
  %16 = load i8*, i8** %text.addr, align 4
  %call20 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %14, i32 %15, i8* null, i8* %16)
  store i32 %call20, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end17
  %17 = load i32, i32* %frame_id, align 4
  %cmp22 = icmp eq i32 %17, 1431520594
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %if.end21
  %18 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %19 = load i32, i32* %frame_id, align 4
  %20 = load i8*, i8** %text.addr, align 4
  %call24 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %18, i32 %19, i8* %20, i8* null)
  store i32 %call24, i32* %retval, align 4
  br label %return

if.end25:                                         ; preds = %if.end21
  %21 = load i32, i32* %frame_id, align 4
  %cmp26 = icmp eq i32 %21, 1464223044
  br i1 %cmp26, label %if.then27, label %if.end29

if.then27:                                        ; preds = %if.end25
  %22 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %23 = load i32, i32* %frame_id, align 4
  %24 = load i8*, i8** %text.addr, align 4
  %call28 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %22, i32 %23, i8* %24, i8* null)
  store i32 %call28, i32* %retval, align 4
  br label %return

if.end29:                                         ; preds = %if.end25
  %25 = load i32, i32* %frame_id, align 4
  %call30 = call i32 @isFrameIdMatching(i32 %25, i32 1409286144)
  %tobool31 = icmp ne i32 %call30, 0
  br i1 %tobool31, label %if.then35, label %lor.lhs.false32

lor.lhs.false32:                                  ; preds = %if.end29
  %26 = load i32, i32* %frame_id, align 4
  %call33 = call i32 @isFrameIdMatching(i32 %26, i32 1459617792)
  %tobool34 = icmp ne i32 %call33, 0
  br i1 %tobool34, label %if.then35, label %if.end37

if.then35:                                        ; preds = %lor.lhs.false32, %if.end29
  %27 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %28 = load i32, i32* %frame_id, align 4
  %29 = load i8*, i8** %text.addr, align 4
  %call36 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %27, i32 %28, i8* null, i8* %29)
  store i32 %call36, i32* %retval, align 4
  br label %return

if.end37:                                         ; preds = %lor.lhs.false32
  store i32 -255, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end37, %if.then35, %if.then27, %if.then23, %if.then19, %if.then15, %if.then11, %if.then5, %if.then2, %if.then
  %30 = load i32, i32* %retval, align 4
  ret i32 %30
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3tag_set_userinfo_latin1(%struct.lame_global_struct* %gfp, i32 %id, i8* %fieldvalue) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %id.addr = alloca i32, align 4
  %fieldvalue.addr = alloca i8*, align 4
  %separator = alloca i8, align 1
  %rc = alloca i32, align 4
  %a = alloca i32, align 4
  %dup = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  store i8* %fieldvalue, i8** %fieldvalue.addr, align 4
  store i8 61, i8* %separator, align 1
  store i32 -7, i32* %rc, align 4
  %0 = load i8*, i8** %fieldvalue.addr, align 4
  %call = call i32 @local_char_pos(i8* %0, i8 signext 61)
  store i32 %call, i32* %a, align 4
  %1 = load i32, i32* %a, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i8* null, i8** %dup, align 4
  %2 = load i8*, i8** %fieldvalue.addr, align 4
  %call1 = call i32 @local_strdup(i8** %dup, i8* %2)
  %3 = load i8*, i8** %dup, align 4
  %4 = load i32, i32* %a, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  store i8 0, i8* %arrayidx, align 1
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %6 = load i32, i32* %id.addr, align 4
  %7 = load i8*, i8** %dup, align 4
  %8 = load i8*, i8** %dup, align 4
  %9 = load i32, i32* %a, align 4
  %add.ptr = getelementptr inbounds i8, i8* %8, i32 %9
  %add.ptr2 = getelementptr inbounds i8, i8* %add.ptr, i32 1
  %call3 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %5, i32 %6, i8* %7, i8* %add.ptr2)
  store i32 %call3, i32* %rc, align 4
  %10 = load i8*, i8** %dup, align 4
  call void @free(i8* %10)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i32, i32* %rc, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_genre(%struct.lame_global_struct* %gfp, i8* %genre) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %genre.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %ret = alloca i32, align 4
  %num = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %genre, i8** %genre.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  store i32 0, i32* %ret, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end17

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %genre.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end17

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %genre.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end17

if.then:                                          ; preds = %land.lhs.true2
  %7 = load i8*, i8** %genre.addr, align 4
  %call = call i32 @lookupGenre(i8* %7)
  store i32 %call, i32* %num, align 4
  %8 = load i32, i32* %num, align 4
  %cmp4 = icmp eq i32 %8, -1
  br i1 %cmp4, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %9 = load i32, i32* %num, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %11 = load i32, i32* %flags, align 8
  %or = or i32 %11, 1
  store i32 %or, i32* %flags, align 8
  %12 = load i32, i32* %num, align 4
  %cmp7 = icmp sge i32 %12, 0
  br i1 %cmp7, label %if.then9, label %if.else

if.then9:                                         ; preds = %if.end
  %13 = load i32, i32* %num, align 4
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 16
  %genre_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec10, i32 0, i32 7
  store i32 %13, i32* %genre_id3v1, align 4
  %15 = load i32, i32* %num, align 4
  %arrayidx = getelementptr inbounds [148 x i8*], [148 x i8*]* @genre_names, i32 0, i32 %15
  %16 = load i8*, i8** %arrayidx, align 4
  store i8* %16, i8** %genre.addr, align 4
  br label %if.end16

if.else:                                          ; preds = %if.end
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec11 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 16
  %genre_id3v112 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec11, i32 0, i32 7
  store i32 12, i32* %genre_id3v112, align 4
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec13 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 16
  %flags14 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec13, i32 0, i32 0
  %19 = load i32, i32* %flags14, align 8
  %or15 = or i32 %19, 2
  store i32 %or15, i32* %flags14, align 8
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then9
  %20 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %21 = load i8*, i8** %genre.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %20, i32 1413697358, i8* %21)
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %land.lhs.true2, %land.lhs.true, %cond.end
  %22 = load i32, i32* %ret, align 4
  store i32 %22, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end17, %if.then6
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %gfp, i32 %frame_id, i8* %desc, i8* %text) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %frame_id.addr = alloca i32, align 4
  %desc.addr = alloca i8*, align 4
  %text.addr = alloca i8*, align 4
  %lang = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store i8* %desc, i8** %desc.addr, align 4
  store i8* %text, i8** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i8* @id3v2_get_language(%struct.lame_global_struct* %0)
  store i8* %call, i8** %lang, align 4
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i32, i32* %frame_id.addr, align 4
  %3 = load i8*, i8** %lang, align 4
  %4 = load i8*, i8** %desc.addr, align 4
  %5 = load i8*, i8** %text.addr, align 4
  %call1 = call i32 @id3v2_add_latin1(%struct.lame_global_struct* %1, i32 %2, i8* %3, i8* %4, i8* %5)
  ret i32 %call1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_comment_latin1(%struct.lame_global_struct* %gfp, i8* %lang, i8* %desc, i8* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lang.addr = alloca i8*, align 4
  %desc.addr = alloca i8*, align 4
  %text.addr = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %lang, i8** %lang.addr, align 4
  store i8* %desc, i8** %desc.addr, align 4
  store i8* %text, i8** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i8*, i8** %lang.addr, align 4
  %3 = load i8*, i8** %desc.addr, align 4
  %4 = load i8*, i8** %text.addr, align 4
  %call1 = call i32 @id3v2_add_latin1(%struct.lame_global_struct* %1, i32 1129270605, i8* %2, i8* %3, i8* %4)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3v2_add_latin1(%struct.lame_global_struct* %gfp, i32 %frame_id, i8* %lng, i8* %desc, i8* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %frame_id.addr = alloca i32, align 4
  %lng.addr = alloca i8*, align 4
  %desc.addr = alloca i8*, align 4
  %text.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %node = alloca %struct.FrameDataNode*, align 4
  %lang = alloca [4 x i8], align 1
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store i8* %lng, i8** %lng.addr, align 4
  store i8* %desc, i8** %desc.addr, align 4
  store i8* %text, i8** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp1 = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %cmp1, label %if.then, label %if.end40

if.then:                                          ; preds = %cond.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 16
  %5 = load i32, i32* %frame_id.addr, align 4
  %call = call %struct.FrameDataNode* @findNode(%struct.id3tag_spec* %tag_spec, i32 %5, %struct.FrameDataNode* null)
  store %struct.FrameDataNode* %call, %struct.FrameDataNode** %node, align 4
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  %6 = load i8*, i8** %lng.addr, align 4
  call void @setLang(i8* %arraydecay, i8* %6)
  %7 = load i32, i32* %frame_id.addr, align 4
  %call2 = call i32 @isMultiFrame(i32 %7)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.then
  br label %while.cond

while.cond:                                       ; preds = %if.end14, %if.then3
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %tobool4 = icmp ne %struct.FrameDataNode* %8, null
  br i1 %tobool4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %lng5 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 2
  %arraydecay6 = getelementptr inbounds [4 x i8], [4 x i8]* %lng5, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  %call8 = call i32 @isSameLang(i8* %arraydecay6, i8* %arraydecay7)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.then10, label %if.end14

if.then10:                                        ; preds = %while.body
  %10 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %11 = load i8*, i8** %desc.addr, align 4
  %call11 = call i32 @isSameDescriptor(%struct.FrameDataNode* %10, i8* %11)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then10
  br label %while.end

if.end:                                           ; preds = %if.then10
  br label %if.end14

if.end14:                                         ; preds = %if.end, %while.body
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec15 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 16
  %13 = load i32, i32* %frame_id.addr, align 4
  %14 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %call16 = call %struct.FrameDataNode* @findNode(%struct.id3tag_spec* %tag_spec15, i32 %13, %struct.FrameDataNode* %14)
  store %struct.FrameDataNode* %call16, %struct.FrameDataNode** %node, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then13, %while.cond
  br label %if.end17

if.end17:                                         ; preds = %while.end, %if.then
  %15 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp18 = icmp eq %struct.FrameDataNode* %15, null
  br i1 %cmp18, label %if.then19, label %if.end25

if.then19:                                        ; preds = %if.end17
  %call20 = call i8* @calloc(i32 1, i32 36)
  %16 = bitcast i8* %call20 to %struct.FrameDataNode*
  store %struct.FrameDataNode* %16, %struct.FrameDataNode** %node, align 4
  %17 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp21 = icmp eq %struct.FrameDataNode* %17, null
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then19
  store i32 -254, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.then19
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 16
  %19 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  call void @appendNode(%struct.id3tag_spec* %tag_spec24, %struct.FrameDataNode* %19)
  br label %if.end25

if.end25:                                         ; preds = %if.end23, %if.end17
  %20 = load i32, i32* %frame_id.addr, align 4
  %21 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %21, i32 0, i32 1
  store i32 %20, i32* %fid, align 4
  %22 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %lng26 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %22, i32 0, i32 2
  %arraydecay27 = getelementptr inbounds [4 x i8], [4 x i8]* %lng26, i32 0, i32 0
  %arraydecay28 = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  call void @setLang(i8* %arraydecay27, i8* %arraydecay28)
  %23 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %23, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 0
  %l = bitcast %union.anon* %ptr to i8**
  %24 = load i8*, i8** %desc.addr, align 4
  %call29 = call i32 @local_strdup(i8** %l, i8* %24)
  %25 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc30 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %25, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc30, i32 0, i32 1
  store i32 %call29, i32* %dim, align 4
  %26 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc31 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %26, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc31, i32 0, i32 2
  store i32 0, i32* %enc, align 4
  %27 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %27, i32 0, i32 4
  %ptr32 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 0
  %l33 = bitcast %union.anon* %ptr32 to i8**
  %28 = load i8*, i8** %text.addr, align 4
  %call34 = call i32 @local_strdup(i8** %l33, i8* %28)
  %29 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt35 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %29, i32 0, i32 4
  %dim36 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt35, i32 0, i32 1
  store i32 %call34, i32* %dim36, align 4
  %30 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt37 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %30, i32 0, i32 4
  %enc38 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt37, i32 0, i32 2
  store i32 0, i32* %enc38, align 4
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec39 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %31, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec39, i32 0, i32 0
  %32 = load i32, i32* %flags, align 8
  %or = or i32 %32, 3
  store i32 %or, i32* %flags, align 8
  store i32 0, i32* %retval, align 4
  br label %return

if.end40:                                         ; preds = %cond.end
  store i32 -255, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end40, %if.end25, %if.then22
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_comment_utf16(%struct.lame_global_struct* %gfp, i8* %lang, i16* %desc, i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lang.addr = alloca i8*, align 4
  %desc.addr = alloca i16*, align 4
  %text.addr = alloca i16*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %lang, i8** %lang.addr, align 4
  store i16* %desc, i16** %desc.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i8*, i8** %lang.addr, align 4
  %3 = load i16*, i16** %desc.addr, align 4
  %4 = load i16*, i16** %text.addr, align 4
  %call1 = call i32 @id3v2_add_ucs2(%struct.lame_global_struct* %1, i32 1129270605, i8* %2, i16* %3, i16* %4)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define internal i32 @id3v2_add_ucs2(%struct.lame_global_struct* %gfp, i32 %frame_id, i8* %lng, i16* %desc, i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %frame_id.addr = alloca i32, align 4
  %lng.addr = alloca i8*, align 4
  %desc.addr = alloca i16*, align 4
  %text.addr = alloca i16*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %node = alloca %struct.FrameDataNode*, align 4
  %lang = alloca [4 x i8], align 1
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store i8* %lng, i8** %lng.addr, align 4
  store i16* %desc, i16** %desc.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp1 = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %cmp1, label %if.then, label %if.end40

if.then:                                          ; preds = %cond.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 16
  %5 = load i32, i32* %frame_id.addr, align 4
  %call = call %struct.FrameDataNode* @findNode(%struct.id3tag_spec* %tag_spec, i32 %5, %struct.FrameDataNode* null)
  store %struct.FrameDataNode* %call, %struct.FrameDataNode** %node, align 4
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  %6 = load i8*, i8** %lng.addr, align 4
  call void @setLang(i8* %arraydecay, i8* %6)
  %7 = load i32, i32* %frame_id.addr, align 4
  %call2 = call i32 @isMultiFrame(i32 %7)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then3, label %if.end17

if.then3:                                         ; preds = %if.then
  br label %while.cond

while.cond:                                       ; preds = %if.end14, %if.then3
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %tobool4 = icmp ne %struct.FrameDataNode* %8, null
  br i1 %tobool4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %lng5 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 2
  %arraydecay6 = getelementptr inbounds [4 x i8], [4 x i8]* %lng5, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  %call8 = call i32 @isSameLang(i8* %arraydecay6, i8* %arraydecay7)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %if.then10, label %if.end14

if.then10:                                        ; preds = %while.body
  %10 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %11 = load i16*, i16** %desc.addr, align 4
  %call11 = call i32 @isSameDescriptorUcs2(%struct.FrameDataNode* %10, i16* %11)
  %tobool12 = icmp ne i32 %call11, 0
  br i1 %tobool12, label %if.then13, label %if.end

if.then13:                                        ; preds = %if.then10
  br label %while.end

if.end:                                           ; preds = %if.then10
  br label %if.end14

if.end14:                                         ; preds = %if.end, %while.body
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec15 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 16
  %13 = load i32, i32* %frame_id.addr, align 4
  %14 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %call16 = call %struct.FrameDataNode* @findNode(%struct.id3tag_spec* %tag_spec15, i32 %13, %struct.FrameDataNode* %14)
  store %struct.FrameDataNode* %call16, %struct.FrameDataNode** %node, align 4
  br label %while.cond

while.end:                                        ; preds = %if.then13, %while.cond
  br label %if.end17

if.end17:                                         ; preds = %while.end, %if.then
  %15 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp18 = icmp eq %struct.FrameDataNode* %15, null
  br i1 %cmp18, label %if.then19, label %if.end25

if.then19:                                        ; preds = %if.end17
  %call20 = call i8* @calloc(i32 1, i32 36)
  %16 = bitcast i8* %call20 to %struct.FrameDataNode*
  store %struct.FrameDataNode* %16, %struct.FrameDataNode** %node, align 4
  %17 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp21 = icmp eq %struct.FrameDataNode* %17, null
  br i1 %cmp21, label %if.then22, label %if.end23

if.then22:                                        ; preds = %if.then19
  store i32 -254, i32* %retval, align 4
  br label %return

if.end23:                                         ; preds = %if.then19
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %18, i32 0, i32 16
  %19 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  call void @appendNode(%struct.id3tag_spec* %tag_spec24, %struct.FrameDataNode* %19)
  br label %if.end25

if.end25:                                         ; preds = %if.end23, %if.end17
  %20 = load i32, i32* %frame_id.addr, align 4
  %21 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %21, i32 0, i32 1
  store i32 %20, i32* %fid, align 4
  %22 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %lng26 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %22, i32 0, i32 2
  %arraydecay27 = getelementptr inbounds [4 x i8], [4 x i8]* %lng26, i32 0, i32 0
  %arraydecay28 = getelementptr inbounds [4 x i8], [4 x i8]* %lang, i32 0, i32 0
  call void @setLang(i8* %arraydecay27, i8* %arraydecay28)
  %23 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %23, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 0
  %u = bitcast %union.anon* %ptr to i16**
  %24 = load i16*, i16** %desc.addr, align 4
  %call29 = call i32 @local_ucs2_strdup(i16** %u, i16* %24)
  %25 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc30 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %25, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc30, i32 0, i32 1
  store i32 %call29, i32* %dim, align 4
  %26 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %dsc31 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %26, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc31, i32 0, i32 2
  store i32 1, i32* %enc, align 4
  %27 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %27, i32 0, i32 4
  %ptr32 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 0
  %u33 = bitcast %union.anon* %ptr32 to i16**
  %28 = load i16*, i16** %text.addr, align 4
  %call34 = call i32 @local_ucs2_strdup(i16** %u33, i16* %28)
  %29 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt35 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %29, i32 0, i32 4
  %dim36 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt35, i32 0, i32 1
  store i32 %call34, i32* %dim36, align 4
  %30 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %txt37 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %30, i32 0, i32 4
  %enc38 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt37, i32 0, i32 2
  store i32 1, i32* %enc38, align 4
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec39 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %31, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec39, i32 0, i32 0
  %32 = load i32, i32* %flags, align 8
  %or = or i32 %32, 3
  store i32 %or, i32* %flags, align 8
  store i32 0, i32* %retval, align 4
  br label %return

if.end40:                                         ; preds = %cond.end
  store i32 -255, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end40, %if.end25, %if.then22
  %33 = load i32, i32* %retval, align 4
  ret i32 %33
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_comment_ucs2(%struct.lame_global_struct* %gfp, i8* %lang, i16* %desc, i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %lang.addr = alloca i8*, align 4
  %desc.addr = alloca i16*, align 4
  %text.addr = alloca i16*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %lang, i8** %lang.addr, align 4
  store i16* %desc, i16** %desc.addr, align 4
  store i16* %text, i16** %text.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i8*, i8** %lang.addr, align 4
  %3 = load i16*, i16** %desc.addr, align 4
  %4 = load i16*, i16** %text.addr, align 4
  %call1 = call i32 @id3tag_set_comment_utf16(%struct.lame_global_struct* %1, i8* %2, i16* %3, i16* %4)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i32, i32* %retval, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_title(%struct.lame_global_struct* %gfp, i8* %title) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %title.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %title, i8** %title.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %title.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %title.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %title4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 2
  %8 = load i8*, i8** %title.addr, align 4
  %call = call i32 @local_strdup(i8** %title4, i8* %8)
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec5, i32 0, i32 0
  %10 = load i32, i32* %flags, align 8
  %or = or i32 %10, 1
  store i32 %or, i32* %flags, align 8
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %12 = load i8*, i8** %title.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %11, i32 1414091826, i8* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true2, %land.lhs.true, %cond.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_strdup(i8** %dst, i8* %src) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i8**, align 4
  %src.addr = alloca i8*, align 4
  %n = alloca i32, align 4
  store i8** %dst, i8*** %dst.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8**, i8*** %dst.addr, align 4
  %cmp = icmp eq i8** %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8**, i8*** %dst.addr, align 4
  %2 = load i8*, i8** %1, align 4
  call void @free(i8* %2)
  %3 = load i8**, i8*** %dst.addr, align 4
  store i8* null, i8** %3, align 4
  %4 = load i8*, i8** %src.addr, align 4
  %cmp1 = icmp ne i8* %4, null
  br i1 %cmp1, label %if.then2, label %if.end14

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %n, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %5 = load i8*, i8** %src.addr, align 4
  %6 = load i32, i32* %n, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %7 to i32
  %cmp3 = icmp ne i32 %conv, 0
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %n, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %n, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i32, i32* %n, align 4
  %cmp5 = icmp ugt i32 %9, 0
  br i1 %cmp5, label %if.then7, label %if.end13

if.then7:                                         ; preds = %for.end
  %10 = load i32, i32* %n, align 4
  %add = add i32 %10, 1
  %call = call i8* @calloc(i32 %add, i32 1)
  %11 = load i8**, i8*** %dst.addr, align 4
  store i8* %call, i8** %11, align 4
  %12 = load i8**, i8*** %dst.addr, align 4
  %13 = load i8*, i8** %12, align 4
  %cmp8 = icmp ne i8* %13, null
  br i1 %cmp8, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.then7
  %14 = load i8**, i8*** %dst.addr, align 4
  %15 = load i8*, i8** %14, align 4
  %16 = load i8*, i8** %src.addr, align 4
  %17 = load i32, i32* %n, align 4
  %mul = mul i32 %17, 1
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %15, i8* align 1 %16, i32 %mul, i1 false)
  %18 = load i8**, i8*** %dst.addr, align 4
  %19 = load i8*, i8** %18, align 4
  %20 = load i32, i32* %n, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %19, i32 %20
  store i8 0, i8* %arrayidx11, align 1
  %21 = load i32, i32* %n, align 4
  store i32 %21, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.then7
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %for.end
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end14, %if.then10, %if.then
  %22 = load i32, i32* %retval, align 4
  ret i32 %22
}

; Function Attrs: noinline nounwind optnone
define internal void @copyV1ToV2(%struct.lame_global_struct* %gfp, i32 %frame_id, i8* %s) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %frame_id.addr = alloca i32, align 4
  %s.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %flags = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store i8* %s, i8** %s.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp1 = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 16
  %flags2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %5 = load i32, i32* %flags2, align 8
  store i32 %5, i32* %flags, align 4
  %6 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %7 = load i32, i32* %frame_id.addr, align 4
  %8 = load i8*, i8** %s.addr, align 4
  %call = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %6, i32 %7, i8* null, i8* %8)
  %9 = load i32, i32* %flags, align 4
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 16
  %flags4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec3, i32 0, i32 0
  store i32 %9, i32* %flags4, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_artist(%struct.lame_global_struct* %gfp, i8* %artist) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %artist.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %artist, i8** %artist.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %artist.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %artist.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %artist4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 3
  %8 = load i8*, i8** %artist.addr, align 4
  %call = call i32 @local_strdup(i8** %artist4, i8* %8)
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec5, i32 0, i32 0
  %10 = load i32, i32* %flags, align 8
  %or = or i32 %10, 1
  store i32 %or, i32* %flags, align 8
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %12 = load i8*, i8** %artist.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %11, i32 1414546737, i8* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true2, %land.lhs.true, %cond.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_album(%struct.lame_global_struct* %gfp, i8* %album) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %album.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %album, i8** %album.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %album.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %album.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %album4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 4
  %8 = load i8*, i8** %album.addr, align 4
  %call = call i32 @local_strdup(i8** %album4, i8* %8)
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec5, i32 0, i32 0
  %10 = load i32, i32* %flags, align 8
  %or = or i32 %10, 1
  store i32 %or, i32* %flags, align 8
  %11 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %12 = load i8*, i8** %album.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %11, i32 1413565506, i8* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true2, %land.lhs.true, %cond.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_year(%struct.lame_global_struct* %gfp, i8* %year) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %year.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %num = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %year, i8** %year.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %year.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end16

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %year.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end16

if.then:                                          ; preds = %land.lhs.true2
  %7 = load i8*, i8** %year.addr, align 4
  %call = call i32 @atoi(i8* %7)
  store i32 %call, i32* %num, align 4
  %8 = load i32, i32* %num, align 4
  %cmp4 = icmp slt i32 %8, 0
  br i1 %cmp4, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  store i32 0, i32* %num, align 4
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  %9 = load i32, i32* %num, align 4
  %cmp7 = icmp sgt i32 %9, 9999
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end
  store i32 9999, i32* %num, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then9, %if.end
  %10 = load i32, i32* %num, align 4
  %tobool11 = icmp ne i32 %10, 0
  br i1 %tobool11, label %if.then12, label %if.end15

if.then12:                                        ; preds = %if.end10
  %11 = load i32, i32* %num, align 4
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 16
  %year13 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 1
  store i32 %11, i32* %year13, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec14, i32 0, i32 0
  %14 = load i32, i32* %flags, align 8
  %or = or i32 %14, 1
  store i32 %or, i32* %flags, align 8
  br label %if.end15

if.end15:                                         ; preds = %if.then12, %if.end10
  %15 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %16 = load i8*, i8** %year.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %15, i32 1415136594, i8* %16)
  br label %if.end16

if.end16:                                         ; preds = %if.end15, %land.lhs.true2, %land.lhs.true, %cond.end
  ret void
}

declare i32 @atoi(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @id3tag_set_comment(%struct.lame_global_struct* %gfp, i8* %comment) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %comment.addr = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %flags6 = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %comment, i8** %comment.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %comment.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %comment.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %comment4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 5
  %8 = load i8*, i8** %comment.addr, align 4
  %call = call i32 @local_strdup(i8** %comment4, i8* %8)
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec5, i32 0, i32 0
  %10 = load i32, i32* %flags, align 8
  %or = or i32 %10, 1
  store i32 %or, i32* %flags, align 8
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec7 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 16
  %flags8 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec7, i32 0, i32 0
  %12 = load i32, i32* %flags8, align 8
  store i32 %12, i32* %flags6, align 4
  %13 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %14 = load i8*, i8** %comment.addr, align 4
  %call9 = call i32 @id3v2_add_latin1_lng(%struct.lame_global_struct* %13, i32 1129270605, i8* getelementptr inbounds ([1 x i8], [1 x i8]* @.str.2, i32 0, i32 0), i8* %14)
  %15 = load i32, i32* %flags6, align 4
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 16
  %flags11 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec10, i32 0, i32 0
  store i32 %15, i32* %flags11, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true2, %land.lhs.true, %cond.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_track(%struct.lame_global_struct* %gfp, i8* %track) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %track.addr = alloca i8*, align 4
  %trackcount = alloca i8*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %ret = alloca i32, align 4
  %num = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %track, i8** %track.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp ne %struct.lame_global_struct* %0, null
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  store i32 0, i32* %ret, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool, label %land.lhs.true, label %if.end26

land.lhs.true:                                    ; preds = %cond.end
  %4 = load i8*, i8** %track.addr, align 4
  %tobool1 = icmp ne i8* %4, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end26

land.lhs.true2:                                   ; preds = %land.lhs.true
  %5 = load i8*, i8** %track.addr, align 4
  %6 = load i8, i8* %5, align 1
  %conv = sext i8 %6 to i32
  %tobool3 = icmp ne i32 %conv, 0
  br i1 %tobool3, label %if.then, label %if.end26

if.then:                                          ; preds = %land.lhs.true2
  %7 = load i8*, i8** %track.addr, align 4
  %call = call i32 @atoi(i8* %7)
  store i32 %call, i32* %num, align 4
  %8 = load i32, i32* %num, align 4
  %cmp4 = icmp slt i32 %8, 1
  br i1 %cmp4, label %if.then8, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %9 = load i32, i32* %num, align 4
  %cmp6 = icmp sgt i32 %9, 255
  br i1 %cmp6, label %if.then8, label %if.end

if.then8:                                         ; preds = %lor.lhs.false, %if.then
  store i32 0, i32* %num, align 4
  store i32 -1, i32* %ret, align 4
  %10 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %10, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %11 = load i32, i32* %flags, align 8
  %or = or i32 %11, 3
  store i32 %or, i32* %flags, align 8
  br label %if.end

if.end:                                           ; preds = %if.then8, %lor.lhs.false
  %12 = load i32, i32* %num, align 4
  %tobool9 = icmp ne i32 %12, 0
  br i1 %tobool9, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.end
  %13 = load i32, i32* %num, align 4
  %14 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec11 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %14, i32 0, i32 16
  %track_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec11, i32 0, i32 6
  store i32 %13, i32* %track_id3v1, align 8
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 16
  %flags13 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec12, i32 0, i32 0
  %16 = load i32, i32* %flags13, align 8
  %or14 = or i32 %16, 1
  store i32 %or14, i32* %flags13, align 8
  br label %if.end15

if.end15:                                         ; preds = %if.then10, %if.end
  %17 = load i8*, i8** %track.addr, align 4
  %call16 = call i8* @strchr(i8* %17, i32 47)
  store i8* %call16, i8** %trackcount, align 4
  %18 = load i8*, i8** %trackcount, align 4
  %tobool17 = icmp ne i8* %18, null
  br i1 %tobool17, label %land.lhs.true18, label %if.end25

land.lhs.true18:                                  ; preds = %if.end15
  %19 = load i8*, i8** %trackcount, align 4
  %20 = load i8, i8* %19, align 1
  %conv19 = sext i8 %20 to i32
  %tobool20 = icmp ne i32 %conv19, 0
  br i1 %tobool20, label %if.then21, label %if.end25

if.then21:                                        ; preds = %land.lhs.true18
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 16
  %flags23 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec22, i32 0, i32 0
  %22 = load i32, i32* %flags23, align 8
  %or24 = or i32 %22, 3
  store i32 %or24, i32* %flags23, align 8
  br label %if.end25

if.end25:                                         ; preds = %if.then21, %land.lhs.true18, %if.end15
  %23 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %24 = load i8*, i8** %track.addr, align 4
  call void @copyV1ToV2(%struct.lame_global_struct* %23, i32 1414677323, i8* %24)
  br label %if.end26

if.end26:                                         ; preds = %if.end25, %land.lhs.true2, %land.lhs.true, %cond.end
  %25 = load i32, i32* %ret, align 4
  ret i32 %25
}

declare i8* @strchr(i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @lookupGenre(i8* %genre) #0 {
entry:
  %retval = alloca i32, align 4
  %genre.addr = alloca i8*, align 4
  %str = alloca i8*, align 4
  %num = alloca i32, align 4
  store i8* %genre, i8** %genre.addr, align 4
  %0 = load i8*, i8** %genre.addr, align 4
  %call = call i32 @strtol(i8* %0, i8** %str, i32 10)
  store i32 %call, i32* %num, align 4
  %1 = load i8*, i8** %str, align 4
  %2 = load i8, i8* %1, align 1
  %tobool = icmp ne i8 %2, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load i8*, i8** %genre.addr, align 4
  %call1 = call i32 @searchGenre(i8* %3)
  store i32 %call1, i32* %num, align 4
  %4 = load i32, i32* %num, align 4
  %cmp = icmp eq i32 %4, 148
  br i1 %cmp, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %5 = load i8*, i8** %genre.addr, align 4
  %call3 = call i32 @sloppySearchGenre(i8* %5)
  store i32 %call3, i32* %num, align 4
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %6 = load i32, i32* %num, align 4
  %cmp4 = icmp eq i32 %6, 148
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end
  store i32 -2, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  br label %if.end11

if.else:                                          ; preds = %entry
  %7 = load i32, i32* %num, align 4
  %cmp7 = icmp slt i32 %7, 0
  br i1 %cmp7, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %8 = load i32, i32* %num, align 4
  %cmp8 = icmp sge i32 %8, 148
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false, %if.else
  store i32 -1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %lor.lhs.false
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.end6
  %9 = load i32, i32* %num, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end11, %if.then9, %if.then5
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_fieldvalue(%struct.lame_global_struct* %gfp, i8* %fieldvalue) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %fieldvalue.addr = alloca i8*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %fieldvalue, i8** %fieldvalue.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %fieldvalue.addr, align 4
  %tobool1 = icmp ne i8* %1, null
  br i1 %tobool1, label %land.lhs.true, label %if.end13

land.lhs.true:                                    ; preds = %if.end
  %2 = load i8*, i8** %fieldvalue.addr, align 4
  %3 = load i8, i8* %2, align 1
  %conv = sext i8 %3 to i32
  %tobool2 = icmp ne i32 %conv, 0
  br i1 %tobool2, label %if.then3, label %if.end13

if.then3:                                         ; preds = %land.lhs.true
  %4 = load i8*, i8** %fieldvalue.addr, align 4
  %call4 = call i32 @strlen(i8* %4)
  %cmp = icmp ult i32 %call4, 5
  br i1 %cmp, label %if.then9, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then3
  %5 = load i8*, i8** %fieldvalue.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 4
  %6 = load i8, i8* %arrayidx, align 1
  %conv6 = sext i8 %6 to i32
  %cmp7 = icmp ne i32 %conv6, 61
  br i1 %cmp7, label %if.then9, label %if.end10

if.then9:                                         ; preds = %lor.lhs.false, %if.then3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %lor.lhs.false
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %8 = load i8*, i8** %fieldvalue.addr, align 4
  %9 = load i8*, i8** %fieldvalue.addr, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %9, i32 5
  %call12 = call i32 @id3tag_set_textinfo_latin1(%struct.lame_global_struct* %7, i8* %8, i8* %arrayidx11)
  store i32 %call12, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %land.lhs.true, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end13, %if.end10, %if.then9, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

declare i32 @strlen(i8*) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_fieldvalue_utf16(%struct.lame_global_struct* %gfp, i16* %fieldvalue) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %fieldvalue.addr = alloca i16*, align 4
  %dx = alloca i32, align 4
  %separator = alloca i16, align 2
  %fid = alloca [5 x i8], align 1
  %frame_id = alloca i32, align 4
  %txt = alloca i16*, align 4
  %rc = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i16* %fieldvalue, i16** %fieldvalue.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i16*, i16** %fieldvalue.addr, align 4
  %tobool1 = icmp ne i16* %1, null
  br i1 %tobool1, label %land.lhs.true, label %if.end38

land.lhs.true:                                    ; preds = %if.end
  %2 = load i16*, i16** %fieldvalue.addr, align 4
  %3 = load i16, i16* %2, align 2
  %conv = zext i16 %3 to i32
  %tobool2 = icmp ne i32 %conv, 0
  br i1 %tobool2, label %if.then3, label %if.end38

if.then3:                                         ; preds = %land.lhs.true
  %4 = load i16*, i16** %fieldvalue.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx, align 2
  %call4 = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %5)
  store i32 %call4, i32* %dx, align 4
  %6 = load i16*, i16** %fieldvalue.addr, align 4
  %call5 = call zeroext i16 @fromLatin1Char(i16* %6, i16 zeroext 61)
  store i16 %call5, i16* %separator, align 2
  %7 = bitcast [5 x i8]* %fid to i8*
  call void @llvm.memset.p0i8.i32(i8* align 1 %7, i8 0, i32 5, i1 false)
  %8 = load i16*, i16** %fieldvalue.addr, align 4
  %call6 = call i32 @toID3v2TagId_ucs2(i16* %8)
  store i32 %call6, i32* %frame_id, align 4
  %9 = load i16*, i16** %fieldvalue.addr, align 4
  %call7 = call i32 @local_ucs2_strlen(i16* %9)
  %10 = load i32, i32* %dx, align 4
  %add = add i32 5, %10
  %cmp = icmp ult i32 %call7, %add
  br i1 %cmp, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then3
  %11 = load i16*, i16** %fieldvalue.addr, align 4
  %12 = load i32, i32* %dx, align 4
  %add9 = add i32 4, %12
  %arrayidx10 = getelementptr inbounds i16, i16* %11, i32 %add9
  %13 = load i16, i16* %arrayidx10, align 2
  %conv11 = zext i16 %13 to i32
  %14 = load i16, i16* %separator, align 2
  %conv12 = zext i16 %14 to i32
  %cmp13 = icmp ne i32 %conv11, %conv12
  br i1 %cmp13, label %if.then15, label %if.end16

if.then15:                                        ; preds = %lor.lhs.false, %if.then3
  store i32 -1, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %lor.lhs.false
  %15 = load i32, i32* %frame_id, align 4
  %shr = lshr i32 %15, 24
  %and = and i32 %shr, 255
  %conv17 = trunc i32 %and to i8
  %arrayidx18 = getelementptr inbounds [5 x i8], [5 x i8]* %fid, i32 0, i32 0
  store i8 %conv17, i8* %arrayidx18, align 1
  %16 = load i32, i32* %frame_id, align 4
  %shr19 = lshr i32 %16, 16
  %and20 = and i32 %shr19, 255
  %conv21 = trunc i32 %and20 to i8
  %arrayidx22 = getelementptr inbounds [5 x i8], [5 x i8]* %fid, i32 0, i32 1
  store i8 %conv21, i8* %arrayidx22, align 1
  %17 = load i32, i32* %frame_id, align 4
  %shr23 = lshr i32 %17, 8
  %and24 = and i32 %shr23, 255
  %conv25 = trunc i32 %and24 to i8
  %arrayidx26 = getelementptr inbounds [5 x i8], [5 x i8]* %fid, i32 0, i32 2
  store i8 %conv25, i8* %arrayidx26, align 1
  %18 = load i32, i32* %frame_id, align 4
  %and27 = and i32 %18, 255
  %conv28 = trunc i32 %and27 to i8
  %arrayidx29 = getelementptr inbounds [5 x i8], [5 x i8]* %fid, i32 0, i32 3
  store i8 %conv28, i8* %arrayidx29, align 1
  %19 = load i32, i32* %frame_id, align 4
  %cmp30 = icmp ne i32 %19, 0
  br i1 %cmp30, label %if.then32, label %if.end37

if.then32:                                        ; preds = %if.end16
  store i16* null, i16** %txt, align 4
  %20 = load i16*, i16** %fieldvalue.addr, align 4
  %21 = load i32, i32* %dx, align 4
  %add33 = add i32 %21, 5
  %22 = load i16*, i16** %fieldvalue.addr, align 4
  %call34 = call i32 @local_ucs2_strlen(i16* %22)
  %call35 = call i32 @local_ucs2_substr(i16** %txt, i16* %20, i32 %add33, i32 %call34)
  %23 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arraydecay = getelementptr inbounds [5 x i8], [5 x i8]* %fid, i32 0, i32 0
  %24 = load i16*, i16** %txt, align 4
  %call36 = call i32 @id3tag_set_textinfo_utf16(%struct.lame_global_struct* %23, i8* %arraydecay, i16* %24)
  store i32 %call36, i32* %rc, align 4
  %25 = load i16*, i16** %txt, align 4
  %26 = bitcast i16* %25 to i8*
  call void @free(i8* %26)
  %27 = load i32, i32* %rc, align 4
  store i32 %27, i32* %retval, align 4
  br label %return

if.end37:                                         ; preds = %if.end16
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %land.lhs.true, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end38, %if.then32, %if.then15, %if.then
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i16 @fromLatin1Char(i16* %s, i16 zeroext %c) #0 {
entry:
  %retval = alloca i16, align 2
  %s.addr = alloca i16*, align 4
  %c.addr = alloca i16, align 2
  store i16* %s, i16** %s.addr, align 4
  store i16 %c, i16* %c.addr, align 2
  %0 = load i16*, i16** %s.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %1 to i32
  %cmp = icmp eq i32 %conv, 65534
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i16, i16* %c.addr, align 2
  %call = call zeroext i16 @swap_bytes(i16 zeroext %2)
  store i16 %call, i16* %retval, align 2
  br label %return

if.end:                                           ; preds = %entry
  %3 = load i16, i16* %c.addr, align 2
  store i16 %3, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i16, i16* %retval, align 2
  ret i16 %4
}

; Function Attrs: noinline nounwind optnone
define internal i32 @toID3v2TagId_ucs2(i16* %s) #0 {
entry:
  %retval = alloca i32, align 4
  %s.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %bom = alloca i16, align 2
  %c = alloca i16, align 2
  store i16* %s, i16** %s.addr, align 4
  store i32 0, i32* %x, align 4
  store i16 0, i16* %bom, align 2
  %0 = load i16*, i16** %s.addr, align 4
  %cmp = icmp eq i16* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i16*, i16** %s.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 0
  %2 = load i16, i16* %arrayidx, align 2
  store i16 %2, i16* %bom, align 2
  %3 = load i16, i16* %bom, align 2
  %call = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then1, label %if.end2

if.then1:                                         ; preds = %if.end
  %4 = load i16*, i16** %s.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %4, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4
  br label %if.end2

if.end2:                                          ; preds = %if.then1, %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end2
  %5 = load i32, i32* %i, align 4
  %cmp3 = icmp ult i32 %5, 4
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %6 = load i16*, i16** %s.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx4, align 2
  %conv = zext i16 %8 to i32
  %cmp5 = icmp ne i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %9 = phi i1 [ false, %for.cond ], [ %cmp5, %land.rhs ]
  br i1 %9, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %10 = load i16, i16* %bom, align 2
  %11 = load i16*, i16** %s.addr, align 4
  %12 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds i16, i16* %11, i32 %12
  %13 = load i16, i16* %arrayidx7, align 2
  %call8 = call zeroext i16 @toLittleEndian(i16 zeroext %10, i16 zeroext %13)
  store i16 %call8, i16* %c, align 2
  %14 = load i16, i16* %c, align 2
  %conv9 = zext i16 %14 to i32
  %cmp10 = icmp slt i32 %conv9, 65
  br i1 %cmp10, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %15 = load i16, i16* %c, align 2
  %conv12 = zext i16 %15 to i32
  %cmp13 = icmp slt i32 90, %conv12
  br i1 %cmp13, label %if.then15, label %if.end25

if.then15:                                        ; preds = %lor.lhs.false, %for.body
  %16 = load i16, i16* %c, align 2
  %conv16 = zext i16 %16 to i32
  %cmp17 = icmp slt i32 %conv16, 48
  br i1 %cmp17, label %if.then23, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %if.then15
  %17 = load i16, i16* %c, align 2
  %conv20 = zext i16 %17 to i32
  %cmp21 = icmp slt i32 57, %conv20
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %lor.lhs.false19, %if.then15
  store i32 0, i32* %retval, align 4
  br label %return

if.end24:                                         ; preds = %lor.lhs.false19
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %lor.lhs.false
  %18 = load i32, i32* %x, align 4
  %shl = shl i32 %18, 8
  store i32 %shl, i32* %x, align 4
  %19 = load i16, i16* %c, align 2
  %conv26 = zext i16 %19 to i32
  %20 = load i32, i32* %x, align 4
  %or = or i32 %20, %conv26
  store i32 %or, i32* %x, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %21 = load i32, i32* %i, align 4
  %inc = add i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  %22 = load i32, i32* %x, align 4
  store i32 %22, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then23, %if.then
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_ucs2_strlen(i16* %s) #0 {
entry:
  %s.addr = alloca i16*, align 4
  %n = alloca i32, align 4
  store i16* %s, i16** %s.addr, align 4
  store i32 0, i32* %n, align 4
  %0 = load i16*, i16** %s.addr, align 4
  %cmp = icmp ne i16* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %1 = load i16*, i16** %s.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %1, i32 1
  store i16* %incdec.ptr, i16** %s.addr, align 4
  %2 = load i16, i16* %1, align 2
  %tobool = icmp ne i16 %2, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load i32, i32* %n, align 4
  %inc = add i32 %3, 1
  store i32 %inc, i32* %n, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.end:                                           ; preds = %while.end, %entry
  %4 = load i32, i32* %n, align 4
  ret i32 %4
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_ucs2_substr(i16** %dst, i16* %src, i32 %start, i32 %end) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i16**, align 4
  %src.addr = alloca i16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %len = alloca i32, align 4
  %n = alloca i32, align 4
  %ptr = alloca i16*, align 4
  store i16** %dst, i16*** %dst.addr, align 4
  store i16* %src, i16** %src.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  %1 = load i32, i32* %end.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load i32, i32* %end.addr, align 4
  %3 = load i32, i32* %start.addr, align 4
  %sub = sub i32 %2, %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub, %cond.true ], [ 0, %cond.false ]
  %add = add i32 2, %cond
  store i32 %add, i32* %len, align 4
  store i32 0, i32* %n, align 4
  %4 = load i32, i32* %len, align 4
  %call = call i8* @calloc(i32 %4, i32 2)
  %5 = bitcast i8* %call to i16*
  store i16* %5, i16** %ptr, align 4
  %6 = load i16*, i16** %ptr, align 4
  %7 = load i16**, i16*** %dst.addr, align 4
  store i16* %6, i16** %7, align 4
  %8 = load i16*, i16** %ptr, align 4
  %cmp1 = icmp eq i16* %8, null
  br i1 %cmp1, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end
  %9 = load i16*, i16** %src.addr, align 4
  %cmp2 = icmp eq i16* %9, null
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %cond.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %10 = load i16*, i16** %src.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %10, i32 0
  %11 = load i16, i16* %arrayidx, align 2
  %call3 = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %11)
  %tobool = icmp ne i32 %call3, 0
  br i1 %tobool, label %if.then4, label %if.end11

if.then4:                                         ; preds = %if.end
  %12 = load i16*, i16** %src.addr, align 4
  %arrayidx5 = getelementptr inbounds i16, i16* %12, i32 0
  %13 = load i16, i16* %arrayidx5, align 2
  %14 = load i16*, i16** %ptr, align 4
  %15 = load i32, i32* %n, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %n, align 4
  %arrayidx6 = getelementptr inbounds i16, i16* %14, i32 %15
  store i16 %13, i16* %arrayidx6, align 2
  %16 = load i32, i32* %start.addr, align 4
  %cmp7 = icmp eq i32 %16, 0
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then4
  %17 = load i32, i32* %start.addr, align 4
  %inc9 = add i32 %17, 1
  store i32 %inc9, i32* %start.addr, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.then4
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %if.end
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end11
  %18 = load i32, i32* %start.addr, align 4
  %19 = load i32, i32* %end.addr, align 4
  %cmp12 = icmp ult i32 %18, %19
  br i1 %cmp12, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %20 = load i16*, i16** %src.addr, align 4
  %21 = load i32, i32* %start.addr, align 4
  %inc13 = add i32 %21, 1
  store i32 %inc13, i32* %start.addr, align 4
  %arrayidx14 = getelementptr inbounds i16, i16* %20, i32 %21
  %22 = load i16, i16* %arrayidx14, align 2
  %23 = load i16*, i16** %ptr, align 4
  %24 = load i32, i32* %n, align 4
  %inc15 = add i32 %24, 1
  store i32 %inc15, i32* %n, align 4
  %arrayidx16 = getelementptr inbounds i16, i16* %23, i32 %24
  store i16 %22, i16* %arrayidx16, align 2
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %25 = load i16*, i16** %ptr, align 4
  %26 = load i32, i32* %n, align 4
  %arrayidx17 = getelementptr inbounds i16, i16* %25, i32 %26
  store i16 0, i16* %arrayidx17, align 2
  %27 = load i32, i32* %n, align 4
  store i32 %27, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %28 = load i32, i32* %retval, align 4
  ret i32 %28
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_set_fieldvalue_ucs2(%struct.lame_global_struct* %gfp, i16* %fieldvalue) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %fieldvalue.addr = alloca i16*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i16* %fieldvalue, i16** %fieldvalue.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %2 = load i16*, i16** %fieldvalue.addr, align 4
  %call1 = call i32 @id3tag_set_fieldvalue_utf16(%struct.lame_global_struct* %1, i16* %2)
  store i32 %call1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i32, i32* %retval, align 4
  ret i32 %3
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_id3v2_tag(%struct.lame_global_struct* %gfp, i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %usev2 = alloca i32, align 4
  %title_length = alloca i32, align 4
  %artist_length = alloca i32, align 4
  %album_length = alloca i32, align 4
  %comment_length = alloca i32, align 4
  %tag_size = alloca i32, align 4
  %p = alloca i8*, align 4
  %adjusted_tag_size = alloca i32, align 4
  %albumart_mime = alloca i8*, align 4
  %tag = alloca %struct.id3tag_spec*, align 4
  %node = alloca %struct.FrameDataNode*, align 4
  %tag132 = alloca %struct.id3tag_spec*, align 4
  %node138 = alloca %struct.FrameDataNode*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %3, i32 4)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call5 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %4, i32 10)
  store i32 %call5, i32* %usev2, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 16
  %title = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 2
  %6 = load i8*, i8** %title, align 8
  %tobool6 = icmp ne i8* %6, null
  br i1 %tobool6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec7 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 16
  %title8 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec7, i32 0, i32 2
  %8 = load i8*, i8** %title8, align 8
  %call9 = call i32 @strlen(i8* %8)
  br label %cond.end

cond.false:                                       ; preds = %if.end4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call9, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %title_length, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 16
  %artist = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec10, i32 0, i32 3
  %10 = load i8*, i8** %artist, align 4
  %tobool11 = icmp ne i8* %10, null
  br i1 %tobool11, label %cond.true12, label %cond.false16

cond.true12:                                      ; preds = %cond.end
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec13 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 16
  %artist14 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec13, i32 0, i32 3
  %12 = load i8*, i8** %artist14, align 4
  %call15 = call i32 @strlen(i8* %12)
  br label %cond.end17

cond.false16:                                     ; preds = %cond.end
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false16, %cond.true12
  %cond18 = phi i32 [ %call15, %cond.true12 ], [ 0, %cond.false16 ]
  store i32 %cond18, i32* %artist_length, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec19 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 16
  %album = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec19, i32 0, i32 4
  %14 = load i8*, i8** %album, align 8
  %tobool20 = icmp ne i8* %14, null
  br i1 %tobool20, label %cond.true21, label %cond.false25

cond.true21:                                      ; preds = %cond.end17
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 16
  %album23 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec22, i32 0, i32 4
  %16 = load i8*, i8** %album23, align 8
  %call24 = call i32 @strlen(i8* %16)
  br label %cond.end26

cond.false25:                                     ; preds = %cond.end17
  br label %cond.end26

cond.end26:                                       ; preds = %cond.false25, %cond.true21
  %cond27 = phi i32 [ %call24, %cond.true21 ], [ 0, %cond.false25 ]
  store i32 %cond27, i32* %album_length, align 4
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec28 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 16
  %comment = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec28, i32 0, i32 5
  %18 = load i8*, i8** %comment, align 4
  %tobool29 = icmp ne i8* %18, null
  br i1 %tobool29, label %cond.true30, label %cond.false34

cond.true30:                                      ; preds = %cond.end26
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec31 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 16
  %comment32 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec31, i32 0, i32 5
  %20 = load i8*, i8** %comment32, align 4
  %call33 = call i32 @strlen(i8* %20)
  br label %cond.end35

cond.false34:                                     ; preds = %cond.end26
  br label %cond.end35

cond.end35:                                       ; preds = %cond.false34, %cond.true30
  %cond36 = phi i32 [ %call33, %cond.true30 ], [ 0, %cond.false34 ]
  store i32 %cond36, i32* %comment_length, align 4
  %21 = load i32, i32* %title_length, align 4
  %cmp = icmp ugt i32 %21, 30
  br i1 %cmp, label %if.then46, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end35
  %22 = load i32, i32* %artist_length, align 4
  %cmp37 = icmp ugt i32 %22, 30
  br i1 %cmp37, label %if.then46, label %lor.lhs.false38

lor.lhs.false38:                                  ; preds = %lor.lhs.false
  %23 = load i32, i32* %album_length, align 4
  %cmp39 = icmp ugt i32 %23, 30
  br i1 %cmp39, label %if.then46, label %lor.lhs.false40

lor.lhs.false40:                                  ; preds = %lor.lhs.false38
  %24 = load i32, i32* %comment_length, align 4
  %cmp41 = icmp ugt i32 %24, 30
  br i1 %cmp41, label %if.then46, label %lor.lhs.false42

lor.lhs.false42:                                  ; preds = %lor.lhs.false40
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec43 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 16
  %track_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec43, i32 0, i32 6
  %26 = load i32, i32* %track_id3v1, align 8
  %tobool44 = icmp ne i32 %26, 0
  br i1 %tobool44, label %land.lhs.true, label %if.end47

land.lhs.true:                                    ; preds = %lor.lhs.false42
  %27 = load i32, i32* %comment_length, align 4
  %cmp45 = icmp ugt i32 %27, 28
  br i1 %cmp45, label %if.then46, label %if.end47

if.then46:                                        ; preds = %land.lhs.true, %lor.lhs.false40, %lor.lhs.false38, %lor.lhs.false, %cond.end35
  store i32 1, i32* %usev2, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then46, %land.lhs.true, %lor.lhs.false42
  %28 = load i32, i32* %usev2, align 4
  %tobool48 = icmp ne i32 %28, 0
  br i1 %tobool48, label %if.then49, label %if.end176

if.then49:                                        ; preds = %if.end47
  store i8* null, i8** %albumart_mime, align 4
  %29 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %29, i32 0, i32 1
  %30 = load i32, i32* %num_samples, align 4
  %cmp50 = icmp ne i32 %30, -1
  br i1 %cmp50, label %if.then51, label %if.end53

if.then51:                                        ; preds = %if.then49
  %31 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %32 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %num_samples52 = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %32, i32 0, i32 1
  %33 = load i32, i32* %num_samples52, align 4
  %conv = uitofp i32 %33 to double
  call void @id3v2AddAudioDuration(%struct.lame_global_struct* %31, double %conv)
  br label %if.end53

if.end53:                                         ; preds = %if.then51, %if.then49
  store i32 10, i32* %tag_size, align 4
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec54 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 16
  %albumart = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec54, i32 0, i32 8
  %35 = load i8*, i8** %albumart, align 8
  %tobool55 = icmp ne i8* %35, null
  br i1 %tobool55, label %land.lhs.true56, label %if.end71

land.lhs.true56:                                  ; preds = %if.end53
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec57 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 16
  %albumart_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec57, i32 0, i32 9
  %37 = load i32, i32* %albumart_size, align 4
  %tobool58 = icmp ne i32 %37, 0
  br i1 %tobool58, label %if.then59, label %if.end71

if.then59:                                        ; preds = %land.lhs.true56
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec60 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %38, i32 0, i32 16
  %albumart_mimetype = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec60, i32 0, i32 11
  %39 = load i32, i32* %albumart_mimetype, align 4
  switch i32 %39, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb61
    i32 3, label %sw.bb62
  ]

sw.bb:                                            ; preds = %if.then59
  %40 = load i8*, i8** @lame_get_id3v2_tag.mime_jpeg, align 4
  store i8* %40, i8** %albumart_mime, align 4
  br label %sw.epilog

sw.bb61:                                          ; preds = %if.then59
  %41 = load i8*, i8** @lame_get_id3v2_tag.mime_png, align 4
  store i8* %41, i8** %albumart_mime, align 4
  br label %sw.epilog

sw.bb62:                                          ; preds = %if.then59
  %42 = load i8*, i8** @lame_get_id3v2_tag.mime_gif, align 4
  store i8* %42, i8** %albumart_mime, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.then59, %sw.bb62, %sw.bb61, %sw.bb
  %43 = load i8*, i8** %albumart_mime, align 4
  %tobool63 = icmp ne i8* %43, null
  br i1 %tobool63, label %if.then64, label %if.end70

if.then64:                                        ; preds = %sw.epilog
  %44 = load i8*, i8** %albumart_mime, align 4
  %call65 = call i32 @strlen(i8* %44)
  %add = add i32 14, %call65
  %45 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec66 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %45, i32 0, i32 16
  %albumart_size67 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec66, i32 0, i32 9
  %46 = load i32, i32* %albumart_size67, align 4
  %add68 = add i32 %add, %46
  %47 = load i32, i32* %tag_size, align 4
  %add69 = add i32 %47, %add68
  store i32 %add69, i32* %tag_size, align 4
  br label %if.end70

if.end70:                                         ; preds = %if.then64, %sw.epilog
  br label %if.end71

if.end71:                                         ; preds = %if.end70, %land.lhs.true56, %if.end53
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec72 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %48, i32 0, i32 16
  store %struct.id3tag_spec* %tag_spec72, %struct.id3tag_spec** %tag, align 4
  %49 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag, align 4
  %v2_head = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %49, i32 0, i32 13
  %50 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head, align 4
  %cmp73 = icmp ne %struct.FrameDataNode* %50, null
  br i1 %cmp73, label %if.then75, label %if.end99

if.then75:                                        ; preds = %if.end71
  %51 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag, align 4
  %v2_head76 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %51, i32 0, i32 13
  %52 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head76, align 4
  store %struct.FrameDataNode* %52, %struct.FrameDataNode** %node, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then75
  %53 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp77 = icmp ne %struct.FrameDataNode* %53, null
  br i1 %cmp77, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %54 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %54, i32 0, i32 1
  %55 = load i32, i32* %fid, align 4
  %cmp79 = icmp eq i32 %55, 1129270605
  br i1 %cmp79, label %if.then85, label %lor.lhs.false81

lor.lhs.false81:                                  ; preds = %for.body
  %56 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid82 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %56, i32 0, i32 1
  %57 = load i32, i32* %fid82, align 4
  %cmp83 = icmp eq i32 %57, 1431520594
  br i1 %cmp83, label %if.then85, label %if.else

if.then85:                                        ; preds = %lor.lhs.false81, %for.body
  %58 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %call86 = call i32 @sizeOfCommentNode(%struct.FrameDataNode* %58)
  %59 = load i32, i32* %tag_size, align 4
  %add87 = add i32 %59, %call86
  store i32 %add87, i32* %tag_size, align 4
  br label %if.end98

if.else:                                          ; preds = %lor.lhs.false81
  %60 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid88 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %60, i32 0, i32 1
  %61 = load i32, i32* %fid88, align 4
  %call89 = call i32 @isFrameIdMatching(i32 %61, i32 1459617792)
  %tobool90 = icmp ne i32 %call89, 0
  br i1 %tobool90, label %if.then91, label %if.else94

if.then91:                                        ; preds = %if.else
  %62 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %call92 = call i32 @sizeOfWxxxNode(%struct.FrameDataNode* %62)
  %63 = load i32, i32* %tag_size, align 4
  %add93 = add i32 %63, %call92
  store i32 %add93, i32* %tag_size, align 4
  br label %if.end97

if.else94:                                        ; preds = %if.else
  %64 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %call95 = call i32 @sizeOfNode(%struct.FrameDataNode* %64)
  %65 = load i32, i32* %tag_size, align 4
  %add96 = add i32 %65, %call95
  store i32 %add96, i32* %tag_size, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.else94, %if.then91
  br label %if.end98

if.end98:                                         ; preds = %if.end97, %if.then85
  br label %for.inc

for.inc:                                          ; preds = %if.end98
  %66 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %nxt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %66, i32 0, i32 0
  %67 = load %struct.FrameDataNode*, %struct.FrameDataNode** %nxt, align 4
  store %struct.FrameDataNode* %67, %struct.FrameDataNode** %node, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end99

if.end99:                                         ; preds = %for.end, %if.end71
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call100 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %68, i32 32)
  %tobool101 = icmp ne i32 %call100, 0
  br i1 %tobool101, label %if.then102, label %if.end105

if.then102:                                       ; preds = %if.end99
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec103 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 16
  %padding_size = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec103, i32 0, i32 10
  %70 = load i32, i32* %padding_size, align 8
  %71 = load i32, i32* %tag_size, align 4
  %add104 = add i32 %71, %70
  store i32 %add104, i32* %tag_size, align 4
  br label %if.end105

if.end105:                                        ; preds = %if.then102, %if.end99
  %72 = load i32, i32* %size.addr, align 4
  %73 = load i32, i32* %tag_size, align 4
  %cmp106 = icmp ult i32 %72, %73
  br i1 %cmp106, label %if.then108, label %if.end109

if.then108:                                       ; preds = %if.end105
  %74 = load i32, i32* %tag_size, align 4
  store i32 %74, i32* %retval, align 4
  br label %return

if.end109:                                        ; preds = %if.end105
  %75 = load i8*, i8** %buffer.addr, align 4
  %cmp110 = icmp eq i8* %75, null
  br i1 %cmp110, label %if.then112, label %if.end113

if.then112:                                       ; preds = %if.end109
  store i32 0, i32* %retval, align 4
  br label %return

if.end113:                                        ; preds = %if.end109
  %76 = load i8*, i8** %buffer.addr, align 4
  store i8* %76, i8** %p, align 4
  %77 = load i8*, i8** %p, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %77, i32 1
  store i8* %incdec.ptr, i8** %p, align 4
  store i8 73, i8* %77, align 1
  %78 = load i8*, i8** %p, align 4
  %incdec.ptr114 = getelementptr inbounds i8, i8* %78, i32 1
  store i8* %incdec.ptr114, i8** %p, align 4
  store i8 68, i8* %78, align 1
  %79 = load i8*, i8** %p, align 4
  %incdec.ptr115 = getelementptr inbounds i8, i8* %79, i32 1
  store i8* %incdec.ptr115, i8** %p, align 4
  store i8 51, i8* %79, align 1
  %80 = load i8*, i8** %p, align 4
  %incdec.ptr116 = getelementptr inbounds i8, i8* %80, i32 1
  store i8* %incdec.ptr116, i8** %p, align 4
  store i8 3, i8* %80, align 1
  %81 = load i8*, i8** %p, align 4
  %incdec.ptr117 = getelementptr inbounds i8, i8* %81, i32 1
  store i8* %incdec.ptr117, i8** %p, align 4
  store i8 0, i8* %81, align 1
  %82 = load i8*, i8** %p, align 4
  %incdec.ptr118 = getelementptr inbounds i8, i8* %82, i32 1
  store i8* %incdec.ptr118, i8** %p, align 4
  store i8 0, i8* %82, align 1
  %83 = load i32, i32* %tag_size, align 4
  %sub = sub i32 %83, 10
  store i32 %sub, i32* %adjusted_tag_size, align 4
  %84 = load i32, i32* %adjusted_tag_size, align 4
  %shr = lshr i32 %84, 21
  %and = and i32 %shr, 127
  %conv119 = trunc i32 %and to i8
  %85 = load i8*, i8** %p, align 4
  %incdec.ptr120 = getelementptr inbounds i8, i8* %85, i32 1
  store i8* %incdec.ptr120, i8** %p, align 4
  store i8 %conv119, i8* %85, align 1
  %86 = load i32, i32* %adjusted_tag_size, align 4
  %shr121 = lshr i32 %86, 14
  %and122 = and i32 %shr121, 127
  %conv123 = trunc i32 %and122 to i8
  %87 = load i8*, i8** %p, align 4
  %incdec.ptr124 = getelementptr inbounds i8, i8* %87, i32 1
  store i8* %incdec.ptr124, i8** %p, align 4
  store i8 %conv123, i8* %87, align 1
  %88 = load i32, i32* %adjusted_tag_size, align 4
  %shr125 = lshr i32 %88, 7
  %and126 = and i32 %shr125, 127
  %conv127 = trunc i32 %and126 to i8
  %89 = load i8*, i8** %p, align 4
  %incdec.ptr128 = getelementptr inbounds i8, i8* %89, i32 1
  store i8* %incdec.ptr128, i8** %p, align 4
  store i8 %conv127, i8* %89, align 1
  %90 = load i32, i32* %adjusted_tag_size, align 4
  %and129 = and i32 %90, 127
  %conv130 = trunc i32 %and129 to i8
  %91 = load i8*, i8** %p, align 4
  %incdec.ptr131 = getelementptr inbounds i8, i8* %91, i32 1
  store i8* %incdec.ptr131, i8** %p, align 4
  store i8 %conv130, i8* %91, align 1
  %92 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec133 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %92, i32 0, i32 16
  store %struct.id3tag_spec* %tag_spec133, %struct.id3tag_spec** %tag132, align 4
  %93 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag132, align 4
  %v2_head134 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %93, i32 0, i32 13
  %94 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head134, align 4
  %cmp135 = icmp ne %struct.FrameDataNode* %94, null
  br i1 %cmp135, label %if.then137, label %if.end166

if.then137:                                       ; preds = %if.end113
  %95 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag132, align 4
  %v2_head139 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %95, i32 0, i32 13
  %96 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head139, align 4
  store %struct.FrameDataNode* %96, %struct.FrameDataNode** %node138, align 4
  br label %for.cond140

for.cond140:                                      ; preds = %for.inc163, %if.then137
  %97 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %cmp141 = icmp ne %struct.FrameDataNode* %97, null
  br i1 %cmp141, label %for.body143, label %for.end165

for.body143:                                      ; preds = %for.cond140
  %98 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %fid144 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %98, i32 0, i32 1
  %99 = load i32, i32* %fid144, align 4
  %cmp145 = icmp eq i32 %99, 1129270605
  br i1 %cmp145, label %if.then151, label %lor.lhs.false147

lor.lhs.false147:                                 ; preds = %for.body143
  %100 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %fid148 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %100, i32 0, i32 1
  %101 = load i32, i32* %fid148, align 4
  %cmp149 = icmp eq i32 %101, 1431520594
  br i1 %cmp149, label %if.then151, label %if.else153

if.then151:                                       ; preds = %lor.lhs.false147, %for.body143
  %102 = load i8*, i8** %p, align 4
  %103 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %call152 = call i8* @set_frame_comment(i8* %102, %struct.FrameDataNode* %103)
  store i8* %call152, i8** %p, align 4
  br label %if.end162

if.else153:                                       ; preds = %lor.lhs.false147
  %104 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %fid154 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %104, i32 0, i32 1
  %105 = load i32, i32* %fid154, align 4
  %call155 = call i32 @isFrameIdMatching(i32 %105, i32 1459617792)
  %tobool156 = icmp ne i32 %call155, 0
  br i1 %tobool156, label %if.then157, label %if.else159

if.then157:                                       ; preds = %if.else153
  %106 = load i8*, i8** %p, align 4
  %107 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %call158 = call i8* @set_frame_wxxx(i8* %106, %struct.FrameDataNode* %107)
  store i8* %call158, i8** %p, align 4
  br label %if.end161

if.else159:                                       ; preds = %if.else153
  %108 = load i8*, i8** %p, align 4
  %109 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %call160 = call i8* @set_frame_custom2(i8* %108, %struct.FrameDataNode* %109)
  store i8* %call160, i8** %p, align 4
  br label %if.end161

if.end161:                                        ; preds = %if.else159, %if.then157
  br label %if.end162

if.end162:                                        ; preds = %if.end161, %if.then151
  br label %for.inc163

for.inc163:                                       ; preds = %if.end162
  %110 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node138, align 4
  %nxt164 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %110, i32 0, i32 0
  %111 = load %struct.FrameDataNode*, %struct.FrameDataNode** %nxt164, align 4
  store %struct.FrameDataNode* %111, %struct.FrameDataNode** %node138, align 4
  br label %for.cond140

for.end165:                                       ; preds = %for.cond140
  br label %if.end166

if.end166:                                        ; preds = %for.end165, %if.end113
  %112 = load i8*, i8** %albumart_mime, align 4
  %tobool167 = icmp ne i8* %112, null
  br i1 %tobool167, label %if.then168, label %if.end174

if.then168:                                       ; preds = %if.end166
  %113 = load i8*, i8** %p, align 4
  %114 = load i8*, i8** %albumart_mime, align 4
  %115 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec169 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %115, i32 0, i32 16
  %albumart170 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec169, i32 0, i32 8
  %116 = load i8*, i8** %albumart170, align 8
  %117 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec171 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %117, i32 0, i32 16
  %albumart_size172 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec171, i32 0, i32 9
  %118 = load i32, i32* %albumart_size172, align 4
  %call173 = call i8* @set_frame_apic(i8* %113, i8* %114, i8* %116, i32 %118)
  store i8* %call173, i8** %p, align 4
  br label %if.end174

if.end174:                                        ; preds = %if.then168, %if.end166
  %119 = load i8*, i8** %p, align 4
  %120 = load i32, i32* %tag_size, align 4
  %121 = load i8*, i8** %p, align 4
  %122 = load i8*, i8** %buffer.addr, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %121 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %122 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub175 = sub i32 %120, %sub.ptr.sub
  call void @llvm.memset.p0i8.i32(i8* align 1 %119, i8 0, i32 %sub175, i1 false)
  %123 = load i32, i32* %tag_size, align 4
  store i32 %123, i32* %retval, align 4
  br label %return

if.end176:                                        ; preds = %if.end47
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end176, %if.end174, %if.then112, %if.then108, %if.then3, %if.then
  %124 = load i32, i32* %retval, align 4
  ret i32 %124
}

; Function Attrs: noinline nounwind optnone
define internal i32 @test_tag_spec_flags(%struct.lame_internal_flags* %gfc, i32 %tst) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %tst.addr = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %tst, i32* %tst.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 16
  %flags = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 0
  %1 = load i32, i32* %flags, align 8
  %2 = load i32, i32* %tst.addr, align 4
  %and = and i32 %1, %2
  %cmp = icmp ne i32 %and, 0
  %3 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 1, i32 0
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define internal void @id3v2AddAudioDuration(%struct.lame_global_struct* %gfp, double %ms) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %ms.addr = alloca double, align 8
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %buffer = alloca [1024 x i8], align 16
  %max_ulong = alloca double, align 8
  %playlength_ms = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store double %ms, double* %ms.addr, align 8
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store double 0x41EFFFFFFFE00000, double* %max_ulong, align 8
  %2 = load double, double* %ms.addr, align 8
  %mul = fmul double %2, 1.000000e+03
  store double %mul, double* %ms.addr, align 8
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 11
  %4 = load i32, i32* %samplerate_in, align 4
  %conv = sitofp i32 %4 to double
  %5 = load double, double* %ms.addr, align 8
  %div = fdiv double %5, %conv
  store double %div, double* %ms.addr, align 8
  %6 = load double, double* %ms.addr, align 8
  %cmp = fcmp ogt double %6, 0x41EFFFFFFFE00000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 -1, i32* %playlength_ms, align 4
  br label %if.end8

if.else:                                          ; preds = %entry
  %7 = load double, double* %ms.addr, align 8
  %cmp3 = fcmp olt double %7, 0.000000e+00
  br i1 %cmp3, label %if.then5, label %if.else6

if.then5:                                         ; preds = %if.else
  store i32 0, i32* %playlength_ms, align 4
  br label %if.end

if.else6:                                         ; preds = %if.else
  %8 = load double, double* %ms.addr, align 8
  %conv7 = fptoui double %8 to i32
  store i32 %conv7, i32* %playlength_ms, align 4
  br label %if.end

if.end:                                           ; preds = %if.else6, %if.then5
  br label %if.end8

if.end8:                                          ; preds = %if.end, %if.then
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %buffer, i32 0, i32 0
  %9 = load i32, i32* %playlength_ms, align 4
  %call = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.157, i32 0, i32 0), i32 %9)
  %10 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arraydecay9 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buffer, i32 0, i32 0
  call void @copyV1ToV2(%struct.lame_global_struct* %10, i32 1414284622, i8* %arraydecay9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sizeOfCommentNode(%struct.FrameDataNode* %node) #0 {
entry:
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  store i32 0, i32* %n, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %tobool = icmp ne %struct.FrameDataNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 10, i32* %n, align 4
  %1 = load i32, i32* %n, align 4
  %add = add i32 %1, 1
  store i32 %add, i32* %n, align 4
  %2 = load i32, i32* %n, align 4
  %add1 = add i32 %2, 3
  store i32 %add1, i32* %n, align 4
  %3 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %3, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 2
  %4 = load i32, i32* %enc, align 4
  switch i32 %4, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb5
  ]

sw.default:                                       ; preds = %if.then
  br label %sw.bb

sw.bb:                                            ; preds = %if.then, %sw.default
  %5 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %5, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc2, i32 0, i32 1
  %6 = load i32, i32* %dim, align 4
  %add3 = add i32 1, %6
  %7 = load i32, i32* %n, align 4
  %add4 = add i32 %7, %add3
  store i32 %add4, i32* %n, align 4
  br label %sw.epilog

sw.bb5:                                           ; preds = %if.then
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc6 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %8, i32 0, i32 3
  %dim7 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc6, i32 0, i32 1
  %9 = load i32, i32* %dim7, align 4
  %mul = mul i32 %9, 2
  %add8 = add i32 2, %mul
  %10 = load i32, i32* %n, align 4
  %add9 = add i32 %10, %add8
  store i32 %add9, i32* %n, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb5, %sw.bb
  %11 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %11, i32 0, i32 4
  %enc10 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 2
  %12 = load i32, i32* %enc10, align 4
  switch i32 %12, label %sw.default11 [
    i32 0, label %sw.bb12
    i32 1, label %sw.bb16
  ]

sw.default11:                                     ; preds = %sw.epilog
  br label %sw.bb12

sw.bb12:                                          ; preds = %sw.epilog, %sw.default11
  %13 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt13 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %13, i32 0, i32 4
  %dim14 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt13, i32 0, i32 1
  %14 = load i32, i32* %dim14, align 4
  %15 = load i32, i32* %n, align 4
  %add15 = add i32 %15, %14
  store i32 %add15, i32* %n, align 4
  br label %sw.epilog21

sw.bb16:                                          ; preds = %sw.epilog
  %16 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt17 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %16, i32 0, i32 4
  %dim18 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt17, i32 0, i32 1
  %17 = load i32, i32* %dim18, align 4
  %mul19 = mul i32 %17, 2
  %18 = load i32, i32* %n, align 4
  %add20 = add i32 %18, %mul19
  store i32 %add20, i32* %n, align 4
  br label %sw.epilog21

sw.epilog21:                                      ; preds = %sw.bb16, %sw.bb12
  br label %if.end

if.end:                                           ; preds = %sw.epilog21, %entry
  %19 = load i32, i32* %n, align 4
  ret i32 %19
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sizeOfWxxxNode(%struct.FrameDataNode* %node) #0 {
entry:
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  store i32 0, i32* %n, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %tobool = icmp ne %struct.FrameDataNode* %0, null
  br i1 %tobool, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  store i32 10, i32* %n, align 4
  %1 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %1, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 1
  %2 = load i32, i32* %dim, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %3 = load i32, i32* %n, align 4
  %add = add i32 %3, 1
  store i32 %add, i32* %n, align 4
  %4 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %4, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc2, i32 0, i32 2
  %5 = load i32, i32* %enc, align 4
  switch i32 %5, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb7
  ]

sw.default:                                       ; preds = %if.then1
  br label %sw.bb

sw.bb:                                            ; preds = %if.then1, %sw.default
  %6 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc3 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %6, i32 0, i32 3
  %dim4 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc3, i32 0, i32 1
  %7 = load i32, i32* %dim4, align 4
  %add5 = add i32 1, %7
  %8 = load i32, i32* %n, align 4
  %add6 = add i32 %8, %add5
  store i32 %add6, i32* %n, align 4
  br label %sw.epilog

sw.bb7:                                           ; preds = %if.then1
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc8 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 3
  %dim9 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc8, i32 0, i32 1
  %10 = load i32, i32* %dim9, align 4
  %mul = mul i32 %10, 2
  %add10 = add i32 2, %mul
  %11 = load i32, i32* %n, align 4
  %add11 = add i32 %11, %add10
  store i32 %add11, i32* %n, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.bb7, %sw.bb
  br label %if.end

if.end:                                           ; preds = %sw.epilog, %if.then
  %12 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %12, i32 0, i32 4
  %dim12 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 1
  %13 = load i32, i32* %dim12, align 4
  %cmp13 = icmp ugt i32 %13, 0
  br i1 %cmp13, label %if.then14, label %if.end27

if.then14:                                        ; preds = %if.end
  %14 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt15 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %14, i32 0, i32 4
  %enc16 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt15, i32 0, i32 2
  %15 = load i32, i32* %enc16, align 4
  switch i32 %15, label %sw.default17 [
    i32 0, label %sw.bb18
    i32 1, label %sw.bb22
  ]

sw.default17:                                     ; preds = %if.then14
  br label %sw.bb18

sw.bb18:                                          ; preds = %if.then14, %sw.default17
  %16 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt19 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %16, i32 0, i32 4
  %dim20 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt19, i32 0, i32 1
  %17 = load i32, i32* %dim20, align 4
  %18 = load i32, i32* %n, align 4
  %add21 = add i32 %18, %17
  store i32 %add21, i32* %n, align 4
  br label %sw.epilog26

sw.bb22:                                          ; preds = %if.then14
  %19 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt23 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %19, i32 0, i32 4
  %dim24 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt23, i32 0, i32 1
  %20 = load i32, i32* %dim24, align 4
  %sub = sub i32 %20, 1
  %21 = load i32, i32* %n, align 4
  %add25 = add i32 %21, %sub
  store i32 %add25, i32* %n, align 4
  br label %sw.epilog26

sw.epilog26:                                      ; preds = %sw.bb22, %sw.bb18
  br label %if.end27

if.end27:                                         ; preds = %sw.epilog26, %if.end
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %entry
  %22 = load i32, i32* %n, align 4
  ret i32 %22
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sizeOfNode(%struct.FrameDataNode* %node) #0 {
entry:
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  store i32 0, i32* %n, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %tobool = icmp ne %struct.FrameDataNode* %0, null
  br i1 %tobool, label %if.then, label %if.end23

if.then:                                          ; preds = %entry
  store i32 10, i32* %n, align 4
  %1 = load i32, i32* %n, align 4
  %add = add i32 %1, 1
  store i32 %add, i32* %n, align 4
  %2 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %2, i32 0, i32 4
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 2
  %3 = load i32, i32* %enc, align 4
  switch i32 %3, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb9
  ]

sw.default:                                       ; preds = %if.then
  br label %sw.bb

sw.bb:                                            ; preds = %if.then, %sw.default
  %4 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %4, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 1
  %5 = load i32, i32* %dim, align 4
  %cmp = icmp ugt i32 %5, 0
  br i1 %cmp, label %if.then1, label %if.end

if.then1:                                         ; preds = %sw.bb
  %6 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %6, i32 0, i32 3
  %dim3 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc2, i32 0, i32 1
  %7 = load i32, i32* %dim3, align 4
  %add4 = add i32 %7, 1
  %8 = load i32, i32* %n, align 4
  %add5 = add i32 %8, %add4
  store i32 %add5, i32* %n, align 4
  br label %if.end

if.end:                                           ; preds = %if.then1, %sw.bb
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt6 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 4
  %dim7 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt6, i32 0, i32 1
  %10 = load i32, i32* %dim7, align 4
  %11 = load i32, i32* %n, align 4
  %add8 = add i32 %11, %10
  store i32 %add8, i32* %n, align 4
  br label %sw.epilog

sw.bb9:                                           ; preds = %if.then
  %12 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc10 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %12, i32 0, i32 3
  %dim11 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc10, i32 0, i32 1
  %13 = load i32, i32* %dim11, align 4
  %cmp12 = icmp ugt i32 %13, 0
  br i1 %cmp12, label %if.then13, label %if.end18

if.then13:                                        ; preds = %sw.bb9
  %14 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc14 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %14, i32 0, i32 3
  %dim15 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc14, i32 0, i32 1
  %15 = load i32, i32* %dim15, align 4
  %add16 = add i32 %15, 1
  %mul = mul i32 %add16, 2
  %16 = load i32, i32* %n, align 4
  %add17 = add i32 %16, %mul
  store i32 %add17, i32* %n, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then13, %sw.bb9
  %17 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt19 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %17, i32 0, i32 4
  %dim20 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt19, i32 0, i32 1
  %18 = load i32, i32* %dim20, align 4
  %mul21 = mul i32 %18, 2
  %19 = load i32, i32* %n, align 4
  %add22 = add i32 %19, %mul21
  store i32 %add22, i32* %n, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end18, %if.end
  br label %if.end23

if.end23:                                         ; preds = %sw.epilog, %entry
  %20 = load i32, i32* %n, align 4
  ret i32 %20
}

; Function Attrs: noinline nounwind optnone
define internal i8* @set_frame_comment(i8* %frame, %struct.FrameDataNode* %node) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store i8* %frame, i8** %frame.addr, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %call = call i32 @sizeOfCommentNode(%struct.FrameDataNode* %0)
  store i32 %call, i32* %n, align 4
  %1 = load i32, i32* %n, align 4
  %cmp = icmp ugt i32 %1, 10
  br i1 %cmp, label %if.then, label %if.end47

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %frame.addr, align 4
  %3 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %3, i32 0, i32 1
  %4 = load i32, i32* %fid, align 4
  %call1 = call i8* @set_4_byte_value(i8* %2, i32 %4)
  store i8* %call1, i8** %frame.addr, align 4
  %5 = load i8*, i8** %frame.addr, align 4
  %6 = load i32, i32* %n, align 4
  %sub = sub i32 %6, 10
  %call2 = call i8* @set_4_byte_value(i8* %5, i32 %sub)
  store i8* %call2, i8** %frame.addr, align 4
  %7 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %frame.addr, align 4
  store i8 0, i8* %7, align 1
  %8 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr3, i8** %frame.addr, align 4
  store i8 0, i8* %8, align 1
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 4
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 2
  %10 = load i32, i32* %enc, align 4
  %cmp4 = icmp eq i32 %10, 1
  %11 = zext i1 %cmp4 to i64
  %cond = select i1 %cmp4, i32 1, i32 0
  %conv = trunc i32 %cond to i8
  %12 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr5 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr5, i8** %frame.addr, align 4
  store i8 %conv, i8* %12, align 1
  %13 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %lng = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %13, i32 0, i32 2
  %arrayidx = getelementptr inbounds [4 x i8], [4 x i8]* %lng, i32 0, i32 0
  %14 = load i8, i8* %arrayidx, align 4
  %15 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr6 = getelementptr inbounds i8, i8* %15, i32 1
  store i8* %incdec.ptr6, i8** %frame.addr, align 4
  store i8 %14, i8* %15, align 1
  %16 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %lng7 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %16, i32 0, i32 2
  %arrayidx8 = getelementptr inbounds [4 x i8], [4 x i8]* %lng7, i32 0, i32 1
  %17 = load i8, i8* %arrayidx8, align 1
  %18 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr9 = getelementptr inbounds i8, i8* %18, i32 1
  store i8* %incdec.ptr9, i8** %frame.addr, align 4
  store i8 %17, i8* %18, align 1
  %19 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %lng10 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %19, i32 0, i32 2
  %arrayidx11 = getelementptr inbounds [4 x i8], [4 x i8]* %lng10, i32 0, i32 2
  %20 = load i8, i8* %arrayidx11, align 2
  %21 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr12 = getelementptr inbounds i8, i8* %21, i32 1
  store i8* %incdec.ptr12, i8** %frame.addr, align 4
  store i8 %20, i8* %21, align 1
  %22 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %22, i32 0, i32 3
  %enc13 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 2
  %23 = load i32, i32* %enc13, align 4
  %cmp14 = icmp ne i32 %23, 1
  br i1 %cmp14, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.then
  %24 = load i8*, i8** %frame.addr, align 4
  %25 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc17 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %25, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc17, i32 0, i32 0
  %l = bitcast %union.anon* %ptr to i8**
  %26 = load i8*, i8** %l, align 4
  %27 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc18 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %27, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc18, i32 0, i32 1
  %28 = load i32, i32* %dim, align 4
  %call19 = call i8* @writeChars(i8* %24, i8* %26, i32 %28)
  store i8* %call19, i8** %frame.addr, align 4
  %29 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr20 = getelementptr inbounds i8, i8* %29, i32 1
  store i8* %incdec.ptr20, i8** %frame.addr, align 4
  store i8 0, i8* %29, align 1
  br label %if.end

if.else:                                          ; preds = %if.then
  %30 = load i8*, i8** %frame.addr, align 4
  %31 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc21 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %31, i32 0, i32 3
  %ptr22 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc21, i32 0, i32 0
  %u = bitcast %union.anon* %ptr22 to i16**
  %32 = load i16*, i16** %u, align 4
  %33 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc23 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %33, i32 0, i32 3
  %dim24 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc23, i32 0, i32 1
  %34 = load i32, i32* %dim24, align 4
  %call25 = call i8* @writeUcs2s(i8* %30, i16* %32, i32 %34)
  store i8* %call25, i8** %frame.addr, align 4
  %35 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr26 = getelementptr inbounds i8, i8* %35, i32 1
  store i8* %incdec.ptr26, i8** %frame.addr, align 4
  store i8 0, i8* %35, align 1
  %36 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr27 = getelementptr inbounds i8, i8* %36, i32 1
  store i8* %incdec.ptr27, i8** %frame.addr, align 4
  store i8 0, i8* %36, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then16
  %37 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt28 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %37, i32 0, i32 4
  %enc29 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt28, i32 0, i32 2
  %38 = load i32, i32* %enc29, align 4
  %cmp30 = icmp ne i32 %38, 1
  br i1 %cmp30, label %if.then32, label %if.else39

if.then32:                                        ; preds = %if.end
  %39 = load i8*, i8** %frame.addr, align 4
  %40 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt33 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %40, i32 0, i32 4
  %ptr34 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt33, i32 0, i32 0
  %l35 = bitcast %union.anon* %ptr34 to i8**
  %41 = load i8*, i8** %l35, align 4
  %42 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt36 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %42, i32 0, i32 4
  %dim37 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt36, i32 0, i32 1
  %43 = load i32, i32* %dim37, align 4
  %call38 = call i8* @writeChars(i8* %39, i8* %41, i32 %43)
  store i8* %call38, i8** %frame.addr, align 4
  br label %if.end46

if.else39:                                        ; preds = %if.end
  %44 = load i8*, i8** %frame.addr, align 4
  %45 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt40 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %45, i32 0, i32 4
  %ptr41 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt40, i32 0, i32 0
  %u42 = bitcast %union.anon* %ptr41 to i16**
  %46 = load i16*, i16** %u42, align 4
  %47 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt43 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %47, i32 0, i32 4
  %dim44 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt43, i32 0, i32 1
  %48 = load i32, i32* %dim44, align 4
  %call45 = call i8* @writeUcs2s(i8* %44, i16* %46, i32 %48)
  store i8* %call45, i8** %frame.addr, align 4
  br label %if.end46

if.end46:                                         ; preds = %if.else39, %if.then32
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %entry
  %49 = load i8*, i8** %frame.addr, align 4
  ret i8* %49
}

; Function Attrs: noinline nounwind optnone
define internal i8* @set_frame_wxxx(i8* %frame, %struct.FrameDataNode* %node) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store i8* %frame, i8** %frame.addr, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %call = call i32 @sizeOfWxxxNode(%struct.FrameDataNode* %0)
  store i32 %call, i32* %n, align 4
  %1 = load i32, i32* %n, align 4
  %cmp = icmp ugt i32 %1, 10
  br i1 %cmp, label %if.then, label %if.end45

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %frame.addr, align 4
  %3 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %3, i32 0, i32 1
  %4 = load i32, i32* %fid, align 4
  %call1 = call i8* @set_4_byte_value(i8* %2, i32 %4)
  store i8* %call1, i8** %frame.addr, align 4
  %5 = load i8*, i8** %frame.addr, align 4
  %6 = load i32, i32* %n, align 4
  %sub = sub i32 %6, 10
  %call2 = call i8* @set_4_byte_value(i8* %5, i32 %sub)
  store i8* %call2, i8** %frame.addr, align 4
  %7 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %frame.addr, align 4
  store i8 0, i8* %7, align 1
  %8 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr3, i8** %frame.addr, align 4
  store i8 0, i8* %8, align 1
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 1
  %10 = load i32, i32* %dim, align 4
  %cmp4 = icmp ugt i32 %10, 0
  br i1 %cmp4, label %if.then5, label %if.end26

if.then5:                                         ; preds = %if.then
  %11 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc6 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %11, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc6, i32 0, i32 2
  %12 = load i32, i32* %enc, align 4
  %cmp7 = icmp eq i32 %12, 1
  %13 = zext i1 %cmp7 to i64
  %cond = select i1 %cmp7, i32 1, i32 0
  %conv = trunc i32 %cond to i8
  %14 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr8 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr8, i8** %frame.addr, align 4
  store i8 %conv, i8* %14, align 1
  %15 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc9 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %15, i32 0, i32 3
  %enc10 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc9, i32 0, i32 2
  %16 = load i32, i32* %enc10, align 4
  %cmp11 = icmp ne i32 %16, 1
  br i1 %cmp11, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then5
  %17 = load i8*, i8** %frame.addr, align 4
  %18 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc14 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %18, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc14, i32 0, i32 0
  %l = bitcast %union.anon* %ptr to i8**
  %19 = load i8*, i8** %l, align 4
  %20 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc15 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %20, i32 0, i32 3
  %dim16 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc15, i32 0, i32 1
  %21 = load i32, i32* %dim16, align 4
  %call17 = call i8* @writeChars(i8* %17, i8* %19, i32 %21)
  store i8* %call17, i8** %frame.addr, align 4
  %22 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr18 = getelementptr inbounds i8, i8* %22, i32 1
  store i8* %incdec.ptr18, i8** %frame.addr, align 4
  store i8 0, i8* %22, align 1
  br label %if.end

if.else:                                          ; preds = %if.then5
  %23 = load i8*, i8** %frame.addr, align 4
  %24 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc19 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %24, i32 0, i32 3
  %ptr20 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc19, i32 0, i32 0
  %u = bitcast %union.anon* %ptr20 to i16**
  %25 = load i16*, i16** %u, align 4
  %26 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc21 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %26, i32 0, i32 3
  %dim22 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc21, i32 0, i32 1
  %27 = load i32, i32* %dim22, align 4
  %call23 = call i8* @writeUcs2s(i8* %23, i16* %25, i32 %27)
  store i8* %call23, i8** %frame.addr, align 4
  %28 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr24 = getelementptr inbounds i8, i8* %28, i32 1
  store i8* %incdec.ptr24, i8** %frame.addr, align 4
  store i8 0, i8* %28, align 1
  %29 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr25 = getelementptr inbounds i8, i8* %29, i32 1
  store i8* %incdec.ptr25, i8** %frame.addr, align 4
  store i8 0, i8* %29, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then13
  br label %if.end26

if.end26:                                         ; preds = %if.end, %if.then
  %30 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %30, i32 0, i32 4
  %enc27 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 2
  %31 = load i32, i32* %enc27, align 4
  %cmp28 = icmp ne i32 %31, 1
  br i1 %cmp28, label %if.then30, label %if.else37

if.then30:                                        ; preds = %if.end26
  %32 = load i8*, i8** %frame.addr, align 4
  %33 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt31 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %33, i32 0, i32 4
  %ptr32 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt31, i32 0, i32 0
  %l33 = bitcast %union.anon* %ptr32 to i8**
  %34 = load i8*, i8** %l33, align 4
  %35 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt34 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %35, i32 0, i32 4
  %dim35 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt34, i32 0, i32 1
  %36 = load i32, i32* %dim35, align 4
  %call36 = call i8* @writeChars(i8* %32, i8* %34, i32 %36)
  store i8* %call36, i8** %frame.addr, align 4
  br label %if.end44

if.else37:                                        ; preds = %if.end26
  %37 = load i8*, i8** %frame.addr, align 4
  %38 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt38 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %38, i32 0, i32 4
  %ptr39 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt38, i32 0, i32 0
  %u40 = bitcast %union.anon* %ptr39 to i16**
  %39 = load i16*, i16** %u40, align 4
  %40 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt41 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %40, i32 0, i32 4
  %dim42 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt41, i32 0, i32 1
  %41 = load i32, i32* %dim42, align 4
  %call43 = call i8* @writeLoBytes(i8* %37, i16* %39, i32 %41)
  store i8* %call43, i8** %frame.addr, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.else37, %if.then30
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %entry
  %42 = load i8*, i8** %frame.addr, align 4
  ret i8* %42
}

; Function Attrs: noinline nounwind optnone
define internal i8* @set_frame_custom2(i8* %frame, %struct.FrameDataNode* %node) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %n = alloca i32, align 4
  store i8* %frame, i8** %frame.addr, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %call = call i32 @sizeOfNode(%struct.FrameDataNode* %0)
  store i32 %call, i32* %n, align 4
  %1 = load i32, i32* %n, align 4
  %cmp = icmp ugt i32 %1, 10
  br i1 %cmp, label %if.then, label %if.end46

if.then:                                          ; preds = %entry
  %2 = load i8*, i8** %frame.addr, align 4
  %3 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %3, i32 0, i32 1
  %4 = load i32, i32* %fid, align 4
  %call1 = call i8* @set_4_byte_value(i8* %2, i32 %4)
  store i8* %call1, i8** %frame.addr, align 4
  %5 = load i8*, i8** %frame.addr, align 4
  %6 = load i32, i32* %n, align 4
  %sub = sub i32 %6, 10
  %call2 = call i8* @set_4_byte_value(i8* %5, i32 %sub)
  store i8* %call2, i8** %frame.addr, align 4
  %7 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %frame.addr, align 4
  store i8 0, i8* %7, align 1
  %8 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr3, i8** %frame.addr, align 4
  store i8 0, i8* %8, align 1
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %9, i32 0, i32 4
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt, i32 0, i32 2
  %10 = load i32, i32* %enc, align 4
  %cmp4 = icmp eq i32 %10, 1
  %11 = zext i1 %cmp4 to i64
  %cond = select i1 %cmp4, i32 1, i32 0
  %conv = trunc i32 %cond to i8
  %12 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr5 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr5, i8** %frame.addr, align 4
  store i8 %conv, i8* %12, align 1
  %13 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %13, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc, i32 0, i32 1
  %14 = load i32, i32* %dim, align 4
  %cmp6 = icmp ugt i32 %14, 0
  br i1 %cmp6, label %if.then8, label %if.end26

if.then8:                                         ; preds = %if.then
  %15 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc9 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %15, i32 0, i32 3
  %enc10 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc9, i32 0, i32 2
  %16 = load i32, i32* %enc10, align 4
  %cmp11 = icmp ne i32 %16, 1
  br i1 %cmp11, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then8
  %17 = load i8*, i8** %frame.addr, align 4
  %18 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc14 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %18, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc14, i32 0, i32 0
  %l = bitcast %union.anon* %ptr to i8**
  %19 = load i8*, i8** %l, align 4
  %20 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc15 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %20, i32 0, i32 3
  %dim16 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc15, i32 0, i32 1
  %21 = load i32, i32* %dim16, align 4
  %call17 = call i8* @writeChars(i8* %17, i8* %19, i32 %21)
  store i8* %call17, i8** %frame.addr, align 4
  %22 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr18 = getelementptr inbounds i8, i8* %22, i32 1
  store i8* %incdec.ptr18, i8** %frame.addr, align 4
  store i8 0, i8* %22, align 1
  br label %if.end

if.else:                                          ; preds = %if.then8
  %23 = load i8*, i8** %frame.addr, align 4
  %24 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc19 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %24, i32 0, i32 3
  %ptr20 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc19, i32 0, i32 0
  %u = bitcast %union.anon* %ptr20 to i16**
  %25 = load i16*, i16** %u, align 4
  %26 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc21 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %26, i32 0, i32 3
  %dim22 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc21, i32 0, i32 1
  %27 = load i32, i32* %dim22, align 4
  %call23 = call i8* @writeUcs2s(i8* %23, i16* %25, i32 %27)
  store i8* %call23, i8** %frame.addr, align 4
  %28 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr24 = getelementptr inbounds i8, i8* %28, i32 1
  store i8* %incdec.ptr24, i8** %frame.addr, align 4
  store i8 0, i8* %28, align 1
  %29 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr25 = getelementptr inbounds i8, i8* %29, i32 1
  store i8* %incdec.ptr25, i8** %frame.addr, align 4
  store i8 0, i8* %29, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then13
  br label %if.end26

if.end26:                                         ; preds = %if.end, %if.then
  %30 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt27 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %30, i32 0, i32 4
  %enc28 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt27, i32 0, i32 2
  %31 = load i32, i32* %enc28, align 4
  %cmp29 = icmp ne i32 %31, 1
  br i1 %cmp29, label %if.then31, label %if.else38

if.then31:                                        ; preds = %if.end26
  %32 = load i8*, i8** %frame.addr, align 4
  %33 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt32 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %33, i32 0, i32 4
  %ptr33 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt32, i32 0, i32 0
  %l34 = bitcast %union.anon* %ptr33 to i8**
  %34 = load i8*, i8** %l34, align 4
  %35 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt35 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %35, i32 0, i32 4
  %dim36 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt35, i32 0, i32 1
  %36 = load i32, i32* %dim36, align 4
  %call37 = call i8* @writeChars(i8* %32, i8* %34, i32 %36)
  store i8* %call37, i8** %frame.addr, align 4
  br label %if.end45

if.else38:                                        ; preds = %if.end26
  %37 = load i8*, i8** %frame.addr, align 4
  %38 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt39 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %38, i32 0, i32 4
  %ptr40 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt39, i32 0, i32 0
  %u41 = bitcast %union.anon* %ptr40 to i16**
  %39 = load i16*, i16** %u41, align 4
  %40 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %txt42 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %40, i32 0, i32 4
  %dim43 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %txt42, i32 0, i32 1
  %41 = load i32, i32* %dim43, align 4
  %call44 = call i8* @writeUcs2s(i8* %37, i16* %39, i32 %41)
  store i8* %call44, i8** %frame.addr, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.else38, %if.then31
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %entry
  %42 = load i8*, i8** %frame.addr, align 4
  ret i8* %42
}

; Function Attrs: noinline nounwind optnone
define internal i8* @set_frame_apic(i8* %frame, i8* %mimetype, i8* %data, i32 %size) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %mimetype.addr = alloca i8*, align 4
  %data.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  store i8* %frame, i8** %frame.addr, align 4
  store i8* %mimetype, i8** %mimetype.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i8*, i8** %mimetype.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i8*, i8** %data.addr, align 4
  %tobool1 = icmp ne i8* %1, null
  br i1 %tobool1, label %land.lhs.true2, label %if.end

land.lhs.true2:                                   ; preds = %land.lhs.true
  %2 = load i32, i32* %size.addr, align 4
  %tobool3 = icmp ne i32 %2, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true2
  %3 = load i8*, i8** %frame.addr, align 4
  %call = call i8* @set_4_byte_value(i8* %3, i32 1095780675)
  store i8* %call, i8** %frame.addr, align 4
  %4 = load i8*, i8** %frame.addr, align 4
  %5 = load i8*, i8** %mimetype.addr, align 4
  %call4 = call i32 @strlen(i8* %5)
  %add = add i32 4, %call4
  %6 = load i32, i32* %size.addr, align 4
  %add5 = add i32 %add, %6
  %call6 = call i8* @set_4_byte_value(i8* %4, i32 %add5)
  store i8* %call6, i8** %frame.addr, align 4
  %7 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %7, i32 1
  store i8* %incdec.ptr, i8** %frame.addr, align 4
  store i8 0, i8* %7, align 1
  %8 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr7 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr7, i8** %frame.addr, align 4
  store i8 0, i8* %8, align 1
  %9 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr8 = getelementptr inbounds i8, i8* %9, i32 1
  store i8* %incdec.ptr8, i8** %frame.addr, align 4
  store i8 0, i8* %9, align 1
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %10 = load i8*, i8** %mimetype.addr, align 4
  %11 = load i8, i8* %10, align 1
  %tobool9 = icmp ne i8 %11, 0
  br i1 %tobool9, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = load i8*, i8** %mimetype.addr, align 4
  %incdec.ptr10 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr10, i8** %mimetype.addr, align 4
  %13 = load i8, i8* %12, align 1
  %14 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr11 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr11, i8** %frame.addr, align 4
  store i8 %13, i8* %14, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr12 = getelementptr inbounds i8, i8* %15, i32 1
  store i8* %incdec.ptr12, i8** %frame.addr, align 4
  store i8 0, i8* %15, align 1
  %16 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr13 = getelementptr inbounds i8, i8* %16, i32 1
  store i8* %incdec.ptr13, i8** %frame.addr, align 4
  store i8 0, i8* %16, align 1
  %17 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr14 = getelementptr inbounds i8, i8* %17, i32 1
  store i8* %incdec.ptr14, i8** %frame.addr, align 4
  store i8 0, i8* %17, align 1
  br label %while.cond15

while.cond15:                                     ; preds = %while.body17, %while.end
  %18 = load i32, i32* %size.addr, align 4
  %dec = add i32 %18, -1
  store i32 %dec, i32* %size.addr, align 4
  %tobool16 = icmp ne i32 %18, 0
  br i1 %tobool16, label %while.body17, label %while.end20

while.body17:                                     ; preds = %while.cond15
  %19 = load i8*, i8** %data.addr, align 4
  %incdec.ptr18 = getelementptr inbounds i8, i8* %19, i32 1
  store i8* %incdec.ptr18, i8** %data.addr, align 4
  %20 = load i8, i8* %19, align 1
  %21 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr19 = getelementptr inbounds i8, i8* %21, i32 1
  store i8* %incdec.ptr19, i8** %frame.addr, align 4
  store i8 %20, i8* %21, align 1
  br label %while.cond15

while.end20:                                      ; preds = %while.cond15
  br label %if.end

if.end:                                           ; preds = %while.end20, %land.lhs.true2, %land.lhs.true, %entry
  %22 = load i8*, i8** %frame.addr, align 4
  ret i8* %22
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_write_v2(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %tag = alloca i8*, align 4
  %tag_size = alloca i32, align 4
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call1 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %3, i32 4)
  %tobool2 = icmp ne i32 %call1, 0
  br i1 %tobool2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call5 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %4, i32 1)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.then7, label %if.end17

if.then7:                                         ; preds = %if.end4
  store i8* null, i8** %tag, align 4
  %5 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call8 = call i32 @lame_get_id3v2_tag(%struct.lame_global_struct* %5, i8* null, i32 0)
  store i32 %call8, i32* %n, align 4
  %6 = load i32, i32* %n, align 4
  %call9 = call i8* @calloc(i32 %6, i32 1)
  store i8* %call9, i8** %tag, align 4
  %7 = load i8*, i8** %tag, align 4
  %cmp = icmp eq i8* %7, null
  br i1 %cmp, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.then7
  store i32 -1, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.then7
  %8 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %9 = load i8*, i8** %tag, align 4
  %10 = load i32, i32* %n, align 4
  %call12 = call i32 @lame_get_id3v2_tag(%struct.lame_global_struct* %8, i8* %9, i32 %10)
  store i32 %call12, i32* %tag_size, align 4
  %11 = load i32, i32* %tag_size, align 4
  %12 = load i32, i32* %n, align 4
  %cmp13 = icmp ugt i32 %11, %12
  br i1 %cmp13, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.end11
  %13 = load i8*, i8** %tag, align 4
  call void @free(i8* %13)
  store i32 -1, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end11
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %tag_size, align 4
  %cmp15 = icmp ult i32 %14, %15
  br i1 %cmp15, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %17 = load i8*, i8** %tag, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %17, i32 %18
  %19 = load i8, i8* %arrayidx, align 1
  call void @add_dummy_byte(%struct.lame_internal_flags* %16, i8 zeroext %19, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  %inc = add i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.end16:                                         ; preds = %for.end
  %21 = load i8*, i8** %tag, align 4
  call void @free(i8* %21)
  %22 = load i32, i32* %tag_size, align 4
  store i32 %22, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %if.end4
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end17, %if.end16, %if.then14, %if.then10, %if.then3, %if.then
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

declare void @add_dummy_byte(%struct.lame_internal_flags*, i8 zeroext, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_id3v1_tag(%struct.lame_global_struct* %gfp, i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %tag_size = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %p = alloca i8*, align 4
  %pad = alloca i32, align 4
  %year = alloca [5 x i8], align 1
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 128, i32* %tag_size, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp eq %struct.lame_global_struct* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %cmp1 = icmp ult i32 %1, 128
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 128, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %2 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %2, i32 0, i32 70
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %3, %struct.lame_internal_flags** %gfc, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp4 = icmp eq %struct.lame_internal_flags* %4, null
  br i1 %cmp4, label %if.then5, label %if.end6

if.then5:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end3
  %5 = load i8*, i8** %buffer.addr, align 4
  %cmp7 = icmp eq i8* %5, null
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end6
  store i32 0, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end6
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %6, i32 8)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end9
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call12 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %7, i32 1)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %if.then14, label %if.end50

if.then14:                                        ; preds = %if.end11
  %8 = load i8*, i8** %buffer.addr, align 4
  store i8* %8, i8** %p, align 4
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call15 = call i32 @test_tag_spec_flags(%struct.lame_internal_flags* %9, i32 16)
  %tobool16 = icmp ne i32 %call15, 0
  %10 = zext i1 %tobool16 to i64
  %cond = select i1 %tobool16, i32 32, i32 0
  store i32 %cond, i32* %pad, align 4
  %11 = load i8*, i8** %p, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %11, i32 1
  store i8* %incdec.ptr, i8** %p, align 4
  store i8 84, i8* %11, align 1
  %12 = load i8*, i8** %p, align 4
  %incdec.ptr17 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr17, i8** %p, align 4
  store i8 65, i8* %12, align 1
  %13 = load i8*, i8** %p, align 4
  %incdec.ptr18 = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr18, i8** %p, align 4
  store i8 71, i8* %13, align 1
  %14 = load i8*, i8** %p, align 4
  %15 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %15, i32 0, i32 16
  %title = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 2
  %16 = load i8*, i8** %title, align 8
  %17 = load i32, i32* %pad, align 4
  %call19 = call i8* @set_text_field(i8* %14, i8* %16, i32 30, i32 %17)
  store i8* %call19, i8** %p, align 4
  %18 = load i8*, i8** %p, align 4
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 16
  %artist = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec20, i32 0, i32 3
  %20 = load i8*, i8** %artist, align 4
  %21 = load i32, i32* %pad, align 4
  %call21 = call i8* @set_text_field(i8* %18, i8* %20, i32 30, i32 %21)
  store i8* %call21, i8** %p, align 4
  %22 = load i8*, i8** %p, align 4
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 16
  %album = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec22, i32 0, i32 4
  %24 = load i8*, i8** %album, align 8
  %25 = load i32, i32* %pad, align 4
  %call23 = call i8* @set_text_field(i8* %22, i8* %24, i32 30, i32 %25)
  store i8* %call23, i8** %p, align 4
  %arraydecay = getelementptr inbounds [5 x i8], [5 x i8]* %year, i32 0, i32 0
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 16
  %year25 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec24, i32 0, i32 1
  %27 = load i32, i32* %year25, align 4
  %call26 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.6, i32 0, i32 0), i32 %27)
  %28 = load i8*, i8** %p, align 4
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec27 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 16
  %year28 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec27, i32 0, i32 1
  %30 = load i32, i32* %year28, align 4
  %tobool29 = icmp ne i32 %30, 0
  br i1 %tobool29, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then14
  %arraydecay30 = getelementptr inbounds [5 x i8], [5 x i8]* %year, i32 0, i32 0
  br label %cond.end

cond.false:                                       ; preds = %if.then14
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond31 = phi i8* [ %arraydecay30, %cond.true ], [ null, %cond.false ]
  %31 = load i32, i32* %pad, align 4
  %call32 = call i8* @set_text_field(i8* %28, i8* %cond31, i32 4, i32 %31)
  store i8* %call32, i8** %p, align 4
  %32 = load i8*, i8** %p, align 4
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec33 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 16
  %comment = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec33, i32 0, i32 5
  %34 = load i8*, i8** %comment, align 4
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec34 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %35, i32 0, i32 16
  %track_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec34, i32 0, i32 6
  %36 = load i32, i32* %track_id3v1, align 8
  %tobool35 = icmp ne i32 %36, 0
  %37 = zext i1 %tobool35 to i64
  %cond36 = select i1 %tobool35, i32 28, i32 30
  %38 = load i32, i32* %pad, align 4
  %call37 = call i8* @set_text_field(i8* %32, i8* %34, i32 %cond36, i32 %38)
  store i8* %call37, i8** %p, align 4
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec38 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 16
  %track_id3v139 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec38, i32 0, i32 6
  %40 = load i32, i32* %track_id3v139, align 8
  %tobool40 = icmp ne i32 %40, 0
  br i1 %tobool40, label %if.then41, label %if.end46

if.then41:                                        ; preds = %cond.end
  %41 = load i8*, i8** %p, align 4
  %incdec.ptr42 = getelementptr inbounds i8, i8* %41, i32 1
  store i8* %incdec.ptr42, i8** %p, align 4
  store i8 0, i8* %41, align 1
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec43 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %42, i32 0, i32 16
  %track_id3v144 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec43, i32 0, i32 6
  %43 = load i32, i32* %track_id3v144, align 8
  %conv = trunc i32 %43 to i8
  %44 = load i8*, i8** %p, align 4
  %incdec.ptr45 = getelementptr inbounds i8, i8* %44, i32 1
  store i8* %incdec.ptr45, i8** %p, align 4
  store i8 %conv, i8* %44, align 1
  br label %if.end46

if.end46:                                         ; preds = %if.then41, %cond.end
  %45 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec47 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %45, i32 0, i32 16
  %genre_id3v1 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec47, i32 0, i32 7
  %46 = load i32, i32* %genre_id3v1, align 4
  %conv48 = trunc i32 %46 to i8
  %47 = load i8*, i8** %p, align 4
  %incdec.ptr49 = getelementptr inbounds i8, i8* %47, i32 1
  store i8* %incdec.ptr49, i8** %p, align 4
  store i8 %conv48, i8* %47, align 1
  store i32 128, i32* %retval, align 4
  br label %return

if.end50:                                         ; preds = %if.end11
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end50, %if.end46, %if.then10, %if.then8, %if.then5, %if.then2, %if.then
  %48 = load i32, i32* %retval, align 4
  ret i32 %48
}

; Function Attrs: noinline nounwind optnone
define internal i8* @set_text_field(i8* %field, i8* %text, i32 %size, i32 %pad) #0 {
entry:
  %field.addr = alloca i8*, align 4
  %text.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %pad.addr = alloca i32, align 4
  store i8* %field, i8** %field.addr, align 4
  store i8* %text, i8** %text.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %pad, i32* %pad.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %0 = load i32, i32* %size.addr, align 4
  %dec = add i32 %0, -1
  store i32 %dec, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i8*, i8** %text.addr, align 4
  %tobool1 = icmp ne i8* %1, null
  br i1 %tobool1, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %while.body
  %2 = load i8*, i8** %text.addr, align 4
  %3 = load i8, i8* %2, align 1
  %conv = sext i8 %3 to i32
  %tobool2 = icmp ne i32 %conv, 0
  br i1 %tobool2, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %4 = load i8*, i8** %text.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %4, i32 1
  store i8* %incdec.ptr, i8** %text.addr, align 4
  %5 = load i8, i8* %4, align 1
  %6 = load i8*, i8** %field.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr3, i8** %field.addr, align 4
  store i8 %5, i8* %6, align 1
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %while.body
  %7 = load i32, i32* %pad.addr, align 4
  %conv4 = trunc i32 %7 to i8
  %8 = load i8*, i8** %field.addr, align 4
  %incdec.ptr5 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr5, i8** %field.addr, align 4
  store i8 %conv4, i8* %8, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = load i8*, i8** %field.addr, align 4
  ret i8* %9
}

declare i32 @sprintf(i8*, i8*, ...) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @id3tag_write_v1(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %i = alloca i32, align 4
  %n = alloca i32, align 4
  %m = alloca i32, align 4
  %tag = alloca [128 x i8], align 16
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct.lame_internal_flags* null, %struct.lame_internal_flags** %gfc, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %call = call i32 @is_lame_internal_flags_null(%struct.lame_global_struct* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  store i32 128, i32* %m, align 4
  %3 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arraydecay = getelementptr inbounds [128 x i8], [128 x i8]* %tag, i32 0, i32 0
  %4 = load i32, i32* %m, align 4
  %call1 = call i32 @lame_get_id3v1_tag(%struct.lame_global_struct* %3, i8* %arraydecay, i32 %4)
  store i32 %call1, i32* %n, align 4
  %5 = load i32, i32* %n, align 4
  %6 = load i32, i32* %m, align 4
  %cmp = icmp ugt i32 %5, %6
  br i1 %cmp, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end3
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %n, align 4
  %cmp4 = icmp ult i32 %7, %8
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [128 x i8], [128 x i8]* %tag, i32 0, i32 %10
  %11 = load i8, i8* %arrayidx, align 1
  call void @add_dummy_byte(%struct.lame_internal_flags* %9, i8 zeroext %11, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %13 = load i32, i32* %n, align 4
  store i32 %13, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then2, %if.then
  %14 = load i32, i32* %retval, align 4
  ret i32 %14
}

declare i8* @get_lame_os_bitness() #1

declare i8* @get_lame_version() #1

declare i8* @get_lame_url() #1

; Function Attrs: noinline nounwind optnone
define internal i32 @local_ucs2_pos(i16* %str, i16 zeroext %c) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i16*, align 4
  %c.addr = alloca i16, align 2
  %i = alloca i32, align 4
  store i16* %str, i16** %str.addr, align 4
  store i16 %c, i16* %c.addr, align 2
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i16*, i16** %str.addr, align 4
  %cmp = icmp ne i16* %0, null
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %1 = load i16*, i16** %str.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %2
  %3 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %3 to i32
  %cmp1 = icmp ne i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %4 = phi i1 [ false, %for.cond ], [ %cmp1, %land.rhs ]
  br i1 %4, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %5 = load i16*, i16** %str.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds i16, i16* %5, i32 %6
  %7 = load i16, i16* %arrayidx3, align 2
  %conv4 = zext i16 %7 to i32
  %8 = load i16, i16* %c.addr, align 2
  %conv5 = zext i16 %8 to i32
  %cmp6 = icmp eq i32 %conv4, %conv5
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define internal i32 @maybeLatin1(i16* %text) #0 {
entry:
  %retval = alloca i32, align 4
  %text.addr = alloca i16*, align 4
  %bom = alloca i16, align 2
  %c = alloca i16, align 2
  store i16* %text, i16** %text.addr, align 4
  %0 = load i16*, i16** %text.addr, align 4
  %tobool = icmp ne i16* %0, null
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load i16*, i16** %text.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %1, i32 1
  store i16* %incdec.ptr, i16** %text.addr, align 4
  %2 = load i16, i16* %1, align 2
  store i16 %2, i16* %bom, align 2
  br label %while.cond

while.cond:                                       ; preds = %if.end, %if.then
  %3 = load i16*, i16** %text.addr, align 4
  %4 = load i16, i16* %3, align 2
  %tobool1 = icmp ne i16 %4, 0
  br i1 %tobool1, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i16, i16* %bom, align 2
  %6 = load i16*, i16** %text.addr, align 4
  %incdec.ptr2 = getelementptr inbounds i16, i16* %6, i32 1
  store i16* %incdec.ptr2, i16** %text.addr, align 4
  %7 = load i16, i16* %6, align 2
  %call = call zeroext i16 @toLittleEndian(i16 zeroext %5, i16 zeroext %7)
  store i16 %call, i16* %c, align 2
  %8 = load i16, i16* %c, align 2
  %conv = zext i16 %8 to i32
  %cmp = icmp sgt i32 %conv, 254
  br i1 %cmp, label %if.then4, label %if.end

if.then4:                                         ; preds = %while.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end5

if.end5:                                          ; preds = %while.end, %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %9 = load i32, i32* %retval, align 4
  ret i32 %9
}

; Function Attrs: noinline nounwind optnone
define internal i8* @local_strdup_utf16_to_latin1(i16* %utf16) #0 {
entry:
  %utf16.addr = alloca i16*, align 4
  %len = alloca i32, align 4
  %latin1 = alloca i8*, align 4
  store i16* %utf16, i16** %utf16.addr, align 4
  %0 = load i16*, i16** %utf16.addr, align 4
  %call = call i32 @local_ucs2_strlen(i16* %0)
  store i32 %call, i32* %len, align 4
  %1 = load i32, i32* %len, align 4
  %add = add i32 %1, 1
  %call1 = call i8* @calloc(i32 %add, i32 1)
  store i8* %call1, i8** %latin1, align 4
  %2 = load i8*, i8** %latin1, align 4
  %3 = load i16*, i16** %utf16.addr, align 4
  %4 = load i32, i32* %len, align 4
  %call2 = call i8* @writeLoBytes(i8* %2, i16* %3, i32 %4)
  %5 = load i8*, i8** %latin1, align 4
  ret i8* %5
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i16 @toLittleEndian(i16 zeroext %bom, i16 zeroext %c) #0 {
entry:
  %retval = alloca i16, align 2
  %bom.addr = alloca i16, align 2
  %c.addr = alloca i16, align 2
  store i16 %bom, i16* %bom.addr, align 2
  store i16 %c, i16* %c.addr, align 2
  %0 = load i16, i16* %bom.addr, align 2
  %conv = zext i16 %0 to i32
  %cmp = icmp eq i32 %conv, 65534
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i16, i16* %c.addr, align 2
  %call = call zeroext i16 @swap_bytes(i16 zeroext %1)
  store i16 %call, i16* %retval, align 2
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i16, i16* %c.addr, align 2
  store i16 %2, i16* %retval, align 2
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i16, i16* %retval, align 2
  ret i16 %3
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i16 @swap_bytes(i16 zeroext %w) #0 {
entry:
  %w.addr = alloca i16, align 2
  store i16 %w, i16* %w.addr, align 2
  %0 = load i16, i16* %w.addr, align 2
  %conv = zext i16 %0 to i32
  %shl = shl i32 %conv, 8
  %and = and i32 65280, %shl
  %1 = load i16, i16* %w.addr, align 2
  %conv1 = zext i16 %1 to i32
  %shr = ashr i32 %conv1, 8
  %and2 = and i32 255, %shr
  %or = or i32 %and, %and2
  %conv3 = trunc i32 %or to i16
  ret i16 %conv3
}

; Function Attrs: noinline nounwind optnone
define internal i8* @writeLoBytes(i8* %frame, i16* %str, i32 %n) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %str.addr = alloca i16*, align 4
  %n.addr = alloca i32, align 4
  %bom = alloca i16, align 2
  %c = alloca i16, align 2
  store i8* %frame, i8** %frame.addr, align 4
  store i16* %str, i16** %str.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %1 = load i16*, i16** %str.addr, align 4
  %2 = load i16, i16* %1, align 2
  store i16 %2, i16* %bom, align 2
  %3 = load i16, i16* %bom, align 2
  %call = call i32 @hasUcs2ByteOrderMarker(i16 zeroext %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  %4 = load i16*, i16** %str.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %4, i32 1
  store i16* %incdec.ptr, i16** %str.addr, align 4
  %5 = load i32, i32* %n.addr, align 4
  %dec = add i32 %5, -1
  store i32 %dec, i32* %n.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  br label %while.cond

while.cond:                                       ; preds = %if.end15, %if.end
  %6 = load i32, i32* %n.addr, align 4
  %dec2 = add i32 %6, -1
  store i32 %dec2, i32* %n.addr, align 4
  %tobool3 = icmp ne i32 %6, 0
  br i1 %tobool3, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %7 = load i16, i16* %bom, align 2
  %8 = load i16*, i16** %str.addr, align 4
  %incdec.ptr4 = getelementptr inbounds i16, i16* %8, i32 1
  store i16* %incdec.ptr4, i16** %str.addr, align 4
  %9 = load i16, i16* %8, align 2
  %call5 = call zeroext i16 @toLittleEndian(i16 zeroext %7, i16 zeroext %9)
  store i16 %call5, i16* %c, align 2
  %10 = load i16, i16* %c, align 2
  %conv = zext i16 %10 to i32
  %cmp6 = icmp ult i32 %conv, 32
  br i1 %cmp6, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %while.body
  %11 = load i16, i16* %c, align 2
  %conv8 = zext i16 %11 to i32
  %cmp9 = icmp ult i32 255, %conv8
  br i1 %cmp9, label %if.then11, label %if.else

if.then11:                                        ; preds = %lor.lhs.false, %while.body
  %12 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr12 = getelementptr inbounds i8, i8* %12, i32 1
  store i8* %incdec.ptr12, i8** %frame.addr, align 4
  store i8 32, i8* %12, align 1
  br label %if.end15

if.else:                                          ; preds = %lor.lhs.false
  %13 = load i16, i16* %c, align 2
  %conv13 = trunc i16 %13 to i8
  %14 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr14 = getelementptr inbounds i8, i8* %14, i32 1
  store i8* %incdec.ptr14, i8** %frame.addr, align 4
  store i8 %conv13, i8* %14, align 1
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then11
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end16

if.end16:                                         ; preds = %while.end, %entry
  %15 = load i8*, i8** %frame.addr, align 4
  ret i8* %15
}

; Function Attrs: noinline nounwind optnone
define internal i8* @id3v2_get_language(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i8*, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %tobool = icmp ne %struct.lame_global_struct* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.lame_internal_flags* [ %2, %cond.true ], [ null, %cond.false ]
  store %struct.lame_internal_flags* %cond, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tobool1 = icmp ne %struct.lame_internal_flags* %3, null
  br i1 %tobool1, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %tag_spec = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 16
  %language = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %tag_spec, i32 0, i32 12
  %arraydecay = getelementptr inbounds [4 x i8], [4 x i8]* %language, i32 0, i32 0
  store i8* %arraydecay, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %cond.end
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i8*, i8** %retval, align 4
  ret i8* %5
}

; Function Attrs: noinline nounwind optnone
define internal i32 @frame_id_matches(i32 %id, i32 %mask) #0 {
entry:
  %id.addr = alloca i32, align 4
  %mask.addr = alloca i32, align 4
  %result = alloca i32, align 4
  %i = alloca i32, align 4
  %window = alloca i32, align 4
  %mw = alloca i32, align 4
  %iw = alloca i32, align 4
  store i32 %id, i32* %id.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  store i32 0, i32* %result, align 4
  store i32 255, i32* %window, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %mask.addr, align 4
  %2 = load i32, i32* %window, align 4
  %and = and i32 %1, %2
  store i32 %and, i32* %mw, align 4
  %3 = load i32, i32* %id.addr, align 4
  %4 = load i32, i32* %window, align 4
  %and1 = and i32 %3, %4
  store i32 %and1, i32* %iw, align 4
  %5 = load i32, i32* %mw, align 4
  %cmp2 = icmp ne i32 %5, 0
  br i1 %cmp2, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %6 = load i32, i32* %mw, align 4
  %7 = load i32, i32* %iw, align 4
  %cmp3 = icmp ne i32 %6, %7
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %8 = load i32, i32* %iw, align 4
  %9 = load i32, i32* %result, align 4
  %or = or i32 %9, %8
  store i32 %or, i32* %result, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  %11 = load i32, i32* %window, align 4
  %shl = shl i32 %11, 8
  store i32 %shl, i32* %window, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load i32, i32* %result, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_char_pos(i8* %str, i8 signext %c) #0 {
entry:
  %retval = alloca i32, align 4
  %str.addr = alloca i8*, align 4
  %c.addr = alloca i8, align 1
  %i = alloca i32, align 4
  store i8* %str, i8** %str.addr, align 4
  store i8 %c, i8* %c.addr, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i8*, i8** %str.addr, align 4
  %cmp = icmp ne i8* %0, null
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %1 = load i8*, i8** %str.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 %2
  %3 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %3 to i32
  %cmp1 = icmp ne i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %4 = phi i1 [ false, %for.cond ], [ %cmp1, %land.rhs ]
  br i1 %4, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %5 = load i8*, i8** %str.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx3, align 1
  %conv4 = sext i8 %7 to i32
  %8 = load i8, i8* %c.addr, align 1
  %conv5 = sext i8 %8 to i32
  %cmp6 = icmp eq i32 %conv4, %conv5
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define internal %struct.FrameDataNode* @findNode(%struct.id3tag_spec* %tag, i32 %frame_id, %struct.FrameDataNode* %last) #0 {
entry:
  %retval = alloca %struct.FrameDataNode*, align 4
  %tag.addr = alloca %struct.id3tag_spec*, align 4
  %frame_id.addr = alloca i32, align 4
  %last.addr = alloca %struct.FrameDataNode*, align 4
  %node = alloca %struct.FrameDataNode*, align 4
  store %struct.id3tag_spec* %tag, %struct.id3tag_spec** %tag.addr, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  store %struct.FrameDataNode* %last, %struct.FrameDataNode** %last.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %last.addr, align 4
  %tobool = icmp ne %struct.FrameDataNode* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load %struct.FrameDataNode*, %struct.FrameDataNode** %last.addr, align 4
  %nxt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %1, i32 0, i32 0
  %2 = load %struct.FrameDataNode*, %struct.FrameDataNode** %nxt, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %3 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_head = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %3, i32 0, i32 13
  %4 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.FrameDataNode* [ %2, %cond.true ], [ %4, %cond.false ]
  store %struct.FrameDataNode* %cond, %struct.FrameDataNode** %node, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %cond.end
  %5 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %cmp = icmp ne %struct.FrameDataNode* %5, null
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %fid = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %6, i32 0, i32 1
  %7 = load i32, i32* %fid, align 4
  %8 = load i32, i32* %frame_id.addr, align 4
  %cmp1 = icmp eq i32 %7, %8
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %9 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  store %struct.FrameDataNode* %9, %struct.FrameDataNode** %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %10 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node, align 4
  %nxt2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %10, i32 0, i32 0
  %11 = load %struct.FrameDataNode*, %struct.FrameDataNode** %nxt2, align 4
  store %struct.FrameDataNode* %11, %struct.FrameDataNode** %node, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  store %struct.FrameDataNode* null, %struct.FrameDataNode** %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %12 = load %struct.FrameDataNode*, %struct.FrameDataNode** %retval, align 4
  ret %struct.FrameDataNode* %12
}

; Function Attrs: noinline nounwind optnone
define internal void @setLang(i8* %dst, i8* %src) #0 {
entry:
  %dst.addr = alloca i8*, align 4
  %src.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %dst, i8** %dst.addr, align 4
  store i8* %src, i8** %src.addr, align 4
  %0 = load i8*, i8** %src.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i8*, i8** %src.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  %2 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %2 to i32
  %cmp1 = icmp eq i32 %conv, 0
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %3 = load i8*, i8** %dst.addr, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %3, i32 0
  store i8 101, i8* %arrayidx3, align 1
  %4 = load i8*, i8** %dst.addr, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %4, i32 1
  store i8 110, i8* %arrayidx4, align 1
  %5 = load i8*, i8** %dst.addr, align 4
  %arrayidx5 = getelementptr inbounds i8, i8* %5, i32 2
  store i8 103, i8* %arrayidx5, align 1
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %6 = load i32, i32* %i, align 4
  %cmp6 = icmp slt i32 %6, 3
  br i1 %cmp6, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %for.cond
  %7 = load i8*, i8** %src.addr, align 4
  %tobool = icmp ne i8* %7, null
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true
  %8 = load i8*, i8** %src.addr, align 4
  %9 = load i8, i8* %8, align 1
  %conv8 = sext i8 %9 to i32
  %tobool9 = icmp ne i32 %conv8, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true, %for.cond
  %10 = phi i1 [ false, %land.lhs.true ], [ false, %for.cond ], [ %tobool9, %land.rhs ]
  br i1 %10, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %11 = load i8*, i8** %src.addr, align 4
  %12 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %11, i32 %12
  %13 = load i8, i8* %arrayidx10, align 1
  %14 = load i8*, i8** %dst.addr, align 4
  %15 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %14, i32 %15
  store i8 %13, i8* %arrayidx11, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc17, %for.end
  %17 = load i32, i32* %i, align 4
  %cmp13 = icmp slt i32 %17, 3
  br i1 %cmp13, label %for.body15, label %for.end19

for.body15:                                       ; preds = %for.cond12
  %18 = load i8*, i8** %dst.addr, align 4
  %19 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds i8, i8* %18, i32 %19
  store i8 32, i8* %arrayidx16, align 1
  br label %for.inc17

for.inc17:                                        ; preds = %for.body15
  %20 = load i32, i32* %i, align 4
  %inc18 = add nsw i32 %20, 1
  store i32 %inc18, i32* %i, align 4
  br label %for.cond12

for.end19:                                        ; preds = %for.cond12
  br label %if.end

if.end:                                           ; preds = %for.end19, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @isMultiFrame(i32 %frame_id) #0 {
entry:
  %retval = alloca i32, align 4
  %frame_id.addr = alloca i32, align 4
  store i32 %frame_id, i32* %frame_id.addr, align 4
  %0 = load i32, i32* %frame_id.addr, align 4
  switch i32 %0, label %sw.epilog [
    i32 1415075928, label %sw.bb
    i32 1465407576, label %sw.bb
    i32 1129270605, label %sw.bb
    i32 1398361172, label %sw.bb
    i32 1095780675, label %sw.bb
    i32 1195724610, label %sw.bb
    i32 1346588244, label %sw.bb
    i32 1095061059, label %sw.bb
    i32 1279872587, label %sw.bb
    i32 1162756946, label %sw.bb
    i32 1196575044, label %sw.bb
    i32 1347570006, label %sw.bb
  ]

sw.bb:                                            ; preds = %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry, %entry
  store i32 1, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.bb
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define internal i32 @isSameLang(i8* %l1, i8* %l2) #0 {
entry:
  %retval = alloca i32, align 4
  %l1.addr = alloca i8*, align 4
  %l2.addr = alloca i8*, align 4
  %d = alloca [3 x i8], align 1
  %i = alloca i32, align 4
  %a = alloca i8, align 1
  %b = alloca i8, align 1
  store i8* %l1, i8** %l1.addr, align 4
  store i8* %l2, i8** %l2.addr, align 4
  %arraydecay = getelementptr inbounds [3 x i8], [3 x i8]* %d, i32 0, i32 0
  %0 = load i8*, i8** %l2.addr, align 4
  call void @setLang(i8* %arraydecay, i8* %0)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %l1.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %4 to i32
  %call = call i32 @tolower(i32 %conv) #5
  %conv1 = trunc i32 %call to i8
  store i8 %conv1, i8* %a, align 1
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [3 x i8], [3 x i8]* %d, i32 0, i32 %5
  %6 = load i8, i8* %arrayidx2, align 1
  %conv3 = sext i8 %6 to i32
  %call4 = call i32 @tolower(i32 %conv3) #5
  %conv5 = trunc i32 %call4 to i8
  store i8 %conv5, i8* %b, align 1
  %7 = load i8, i8* %a, align 1
  %conv6 = sext i8 %7 to i32
  %cmp7 = icmp slt i32 %conv6, 32
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i8 32, i8* %a, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %8 = load i8, i8* %b, align 1
  %conv9 = sext i8 %8 to i32
  %cmp10 = icmp slt i32 %conv9, 32
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end
  store i8 32, i8* %b, align 1
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %if.end
  %9 = load i8, i8* %a, align 1
  %conv14 = sext i8 %9 to i32
  %10 = load i8, i8* %b, align 1
  %conv15 = sext i8 %10 to i32
  %cmp16 = icmp ne i32 %conv14, %conv15
  br i1 %cmp16, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end13
  store i32 0, i32* %retval, align 4
  br label %return

if.end19:                                         ; preds = %if.end13
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then18
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

; Function Attrs: noinline nounwind optnone
define internal i32 @isSameDescriptor(%struct.FrameDataNode* %node, i8* %dsc) #0 {
entry:
  %retval = alloca i32, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %dsc.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  store i8* %dsc, i8** %dsc.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc1 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %0, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc1, i32 0, i32 2
  %1 = load i32, i32* %enc, align 4
  %cmp = icmp eq i32 %1, 1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %2, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc2, i32 0, i32 1
  %3 = load i32, i32* %dim, align 4
  %cmp3 = icmp ugt i32 %3, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4
  %5 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc4 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %5, i32 0, i32 3
  %dim5 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc4, i32 0, i32 1
  %6 = load i32, i32* %dim5, align 4
  %cmp6 = icmp ult i32 %4, %6
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i8*, i8** %dsc.addr, align 4
  %tobool = icmp ne i8* %7, null
  br i1 %tobool, label %lor.lhs.false, label %if.then12

lor.lhs.false:                                    ; preds = %for.body
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc7 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %8, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc7, i32 0, i32 0
  %l = bitcast %union.anon* %ptr to i8**
  %9 = load i8*, i8** %l, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8, i8* %arrayidx, align 1
  %conv = sext i8 %11 to i32
  %12 = load i8*, i8** %dsc.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %12, i32 %13
  %14 = load i8, i8* %arrayidx8, align 1
  %conv9 = sext i8 %14 to i32
  %cmp10 = icmp ne i32 %conv, %conv9
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %lor.lhs.false, %for.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %15 = load i32, i32* %i, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then12, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define internal void @appendNode(%struct.id3tag_spec* %tag, %struct.FrameDataNode* %node) #0 {
entry:
  %tag.addr = alloca %struct.id3tag_spec*, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  store %struct.id3tag_spec* %tag, %struct.id3tag_spec** %tag.addr, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  %0 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_tail = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %0, i32 0, i32 14
  %1 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_tail, align 4
  %cmp = icmp eq %struct.FrameDataNode* %1, null
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_head = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %2, i32 0, i32 13
  %3 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_head, align 4
  %cmp1 = icmp eq %struct.FrameDataNode* %3, null
  br i1 %cmp1, label %if.then, label %if.else

if.then:                                          ; preds = %lor.lhs.false, %entry
  %4 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %5 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_head2 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %5, i32 0, i32 13
  store %struct.FrameDataNode* %4, %struct.FrameDataNode** %v2_head2, align 4
  %6 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %7 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_tail3 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %7, i32 0, i32 14
  store %struct.FrameDataNode* %6, %struct.FrameDataNode** %v2_tail3, align 4
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %9 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_tail4 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %9, i32 0, i32 14
  %10 = load %struct.FrameDataNode*, %struct.FrameDataNode** %v2_tail4, align 4
  %nxt = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %10, i32 0, i32 0
  store %struct.FrameDataNode* %8, %struct.FrameDataNode** %nxt, align 4
  %11 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %12 = load %struct.id3tag_spec*, %struct.id3tag_spec** %tag.addr, align 4
  %v2_tail5 = getelementptr inbounds %struct.id3tag_spec, %struct.id3tag_spec* %12, i32 0, i32 14
  store %struct.FrameDataNode* %11, %struct.FrameDataNode** %v2_tail5, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: nounwind readonly
declare i32 @tolower(i32) #4

; Function Attrs: noinline nounwind optnone
define internal i32 @isSameDescriptorUcs2(%struct.FrameDataNode* %node, i16* %dsc) #0 {
entry:
  %retval = alloca i32, align 4
  %node.addr = alloca %struct.FrameDataNode*, align 4
  %dsc.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  store %struct.FrameDataNode* %node, %struct.FrameDataNode** %node.addr, align 4
  store i16* %dsc, i16** %dsc.addr, align 4
  %0 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc1 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %0, i32 0, i32 3
  %enc = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc1, i32 0, i32 2
  %1 = load i32, i32* %enc, align 4
  %cmp = icmp ne i32 %1, 1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc2 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %2, i32 0, i32 3
  %dim = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc2, i32 0, i32 1
  %3 = load i32, i32* %dim, align 4
  %cmp3 = icmp ugt i32 %3, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %4 = load i32, i32* %i, align 4
  %5 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc4 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %5, i32 0, i32 3
  %dim5 = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc4, i32 0, i32 1
  %6 = load i32, i32* %dim5, align 4
  %cmp6 = icmp ult i32 %4, %6
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i16*, i16** %dsc.addr, align 4
  %tobool = icmp ne i16* %7, null
  br i1 %tobool, label %lor.lhs.false, label %if.then12

lor.lhs.false:                                    ; preds = %for.body
  %8 = load %struct.FrameDataNode*, %struct.FrameDataNode** %node.addr, align 4
  %dsc7 = getelementptr inbounds %struct.FrameDataNode, %struct.FrameDataNode* %8, i32 0, i32 3
  %ptr = getelementptr inbounds %struct.anon.1, %struct.anon.1* %dsc7, i32 0, i32 0
  %u = bitcast %union.anon* %ptr to i16**
  %9 = load i16*, i16** %u, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i16, i16* %9, i32 %10
  %11 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %11 to i32
  %12 = load i16*, i16** %dsc.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds i16, i16* %12, i32 %13
  %14 = load i16, i16* %arrayidx8, align 2
  %conv9 = zext i16 %14 to i32
  %cmp10 = icmp ne i32 %conv, %conv9
  br i1 %cmp10, label %if.then12, label %if.end13

if.then12:                                        ; preds = %lor.lhs.false, %for.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %15 = load i32, i32* %i, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then12, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_ucs2_strdup(i16** %dst, i16* %src) #0 {
entry:
  %retval = alloca i32, align 4
  %dst.addr = alloca i16**, align 4
  %src.addr = alloca i16*, align 4
  %n = alloca i32, align 4
  store i16** %dst, i16*** %dst.addr, align 4
  store i16* %src, i16** %src.addr, align 4
  %0 = load i16**, i16*** %dst.addr, align 4
  %cmp = icmp eq i16** %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i16**, i16*** %dst.addr, align 4
  %2 = load i16*, i16** %1, align 4
  %3 = bitcast i16* %2 to i8*
  call void @free(i8* %3)
  %4 = load i16**, i16*** %dst.addr, align 4
  store i16* null, i16** %4, align 4
  %5 = load i16*, i16** %src.addr, align 4
  %cmp1 = icmp ne i16* %5, null
  br i1 %cmp1, label %if.then2, label %if.end14

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %n, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %6 = load i16*, i16** %src.addr, align 4
  %7 = load i32, i32* %n, align 4
  %arrayidx = getelementptr inbounds i16, i16* %6, i32 %7
  %8 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %8 to i32
  %cmp3 = icmp ne i32 %conv, 0
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %n, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %n, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load i32, i32* %n, align 4
  %cmp5 = icmp ugt i32 %10, 0
  br i1 %cmp5, label %if.then7, label %if.end13

if.then7:                                         ; preds = %for.end
  %11 = load i32, i32* %n, align 4
  %add = add i32 %11, 1
  %call = call i8* @calloc(i32 %add, i32 2)
  %12 = bitcast i8* %call to i16*
  %13 = load i16**, i16*** %dst.addr, align 4
  store i16* %12, i16** %13, align 4
  %14 = load i16**, i16*** %dst.addr, align 4
  %15 = load i16*, i16** %14, align 4
  %cmp8 = icmp ne i16* %15, null
  br i1 %cmp8, label %if.then10, label %if.end12

if.then10:                                        ; preds = %if.then7
  %16 = load i16**, i16*** %dst.addr, align 4
  %17 = load i16*, i16** %16, align 4
  %18 = bitcast i16* %17 to i8*
  %19 = load i16*, i16** %src.addr, align 4
  %20 = bitcast i16* %19 to i8*
  %21 = load i32, i32* %n, align 4
  %mul = mul i32 %21, 2
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %18, i8* align 2 %20, i32 %mul, i1 false)
  %22 = load i16**, i16*** %dst.addr, align 4
  %23 = load i16*, i16** %22, align 4
  %24 = load i32, i32* %n, align 4
  %arrayidx11 = getelementptr inbounds i16, i16* %23, i32 %24
  store i16 0, i16* %arrayidx11, align 2
  %25 = load i32, i32* %n, align 4
  store i32 %25, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.then7
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %for.end
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end14, %if.then10, %if.then
  %26 = load i32, i32* %retval, align 4
  ret i32 %26
}

declare i32 @strtol(i8*, i8**, i32) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @searchGenre(i8* %genre) #0 {
entry:
  %retval = alloca i32, align 4
  %genre.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %genre, i8** %genre.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 148
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %genre.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [148 x i8*], [148 x i8*]* @genre_names, i32 0, i32 %2
  %3 = load i8*, i8** %arrayidx, align 4
  %call = call i32 @local_strcasecmp(i8* %1, i8* %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 148, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sloppySearchGenre(i8* %genre) #0 {
entry:
  %retval = alloca i32, align 4
  %genre.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store i8* %genre, i8** %genre.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 148
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i8*, i8** %genre.addr, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [148 x i8*], [148 x i8*]* @genre_names, i32 0, i32 %2
  %3 = load i8*, i8** %arrayidx, align 4
  %call = call i32 @sloppyCompared(i8* %1, i8* %3)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  store i32 %4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 148, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %6 = load i32, i32* %retval, align 4
  ret i32 %6
}

; Function Attrs: noinline nounwind optnone
define internal i32 @local_strcasecmp(i8* %s1, i8* %s2) #0 {
entry:
  %s1.addr = alloca i8*, align 4
  %s2.addr = alloca i8*, align 4
  %c1 = alloca i8, align 1
  %c2 = alloca i8, align 1
  store i8* %s1, i8** %s1.addr, align 4
  store i8* %s2, i8** %s2.addr, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %0 = load i8*, i8** %s1.addr, align 4
  %1 = load i8, i8* %0, align 1
  %conv = sext i8 %1 to i32
  %call = call i32 @tolower(i32 %conv) #5
  %conv1 = trunc i32 %call to i8
  store i8 %conv1, i8* %c1, align 1
  %2 = load i8*, i8** %s2.addr, align 4
  %3 = load i8, i8* %2, align 1
  %conv2 = sext i8 %3 to i32
  %call3 = call i32 @tolower(i32 %conv2) #5
  %conv4 = trunc i32 %call3 to i8
  store i8 %conv4, i8* %c2, align 1
  %4 = load i8, i8* %c1, align 1
  %tobool = icmp ne i8 %4, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %do.body
  br label %do.end

if.end:                                           ; preds = %do.body
  %5 = load i8*, i8** %s1.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %5, i32 1
  store i8* %incdec.ptr, i8** %s1.addr, align 4
  %6 = load i8*, i8** %s2.addr, align 4
  %incdec.ptr5 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr5, i8** %s2.addr, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %7 = load i8, i8* %c1, align 1
  %conv6 = zext i8 %7 to i32
  %8 = load i8, i8* %c2, align 1
  %conv7 = zext i8 %8 to i32
  %cmp = icmp eq i32 %conv6, %conv7
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %if.then
  %9 = load i8, i8* %c1, align 1
  %conv9 = zext i8 %9 to i32
  %10 = load i8, i8* %c2, align 1
  %conv10 = zext i8 %10 to i32
  %sub = sub nsw i32 %conv9, %conv10
  ret i32 %sub
}

; Function Attrs: noinline nounwind optnone
define internal i32 @sloppyCompared(i8* %p, i8* %q) #0 {
entry:
  %retval = alloca i32, align 4
  %p.addr = alloca i8*, align 4
  %q.addr = alloca i8*, align 4
  %cp = alloca i8, align 1
  %cq = alloca i8, align 1
  store i8* %p, i8** %p.addr, align 4
  store i8* %q, i8** %q.addr, align 4
  %0 = load i8*, i8** %p.addr, align 4
  %call = call i8* @nextUpperAlpha(i8* %0, i8 signext 0)
  store i8* %call, i8** %p.addr, align 4
  %1 = load i8*, i8** %q.addr, align 4
  %call1 = call i8* @nextUpperAlpha(i8* %1, i8 signext 0)
  store i8* %call1, i8** %q.addr, align 4
  %2 = load i8*, i8** %p.addr, align 4
  %3 = load i8, i8* %2, align 1
  %conv = sext i8 %3 to i32
  %call2 = call i32 @toupper(i32 %conv) #5
  %conv3 = trunc i32 %call2 to i8
  store i8 %conv3, i8* %cp, align 1
  %4 = load i8*, i8** %q.addr, align 4
  %5 = load i8, i8* %4, align 1
  %conv4 = sext i8 %5 to i32
  %call5 = call i32 @toupper(i32 %conv4) #5
  %conv6 = trunc i32 %call5 to i8
  store i8 %conv6, i8* %cq, align 1
  br label %while.cond

while.cond:                                       ; preds = %if.end23, %entry
  %6 = load i8, i8* %cp, align 1
  %conv7 = sext i8 %6 to i32
  %7 = load i8, i8* %cq, align 1
  %conv8 = sext i8 %7 to i32
  %cmp = icmp eq i32 %conv7, %conv8
  br i1 %cmp, label %while.body, label %while.end32

while.body:                                       ; preds = %while.cond
  %8 = load i8, i8* %cp, align 1
  %conv10 = sext i8 %8 to i32
  %cmp11 = icmp eq i32 %conv10, 0
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %9 = load i8*, i8** %p.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %9, i32 1
  %10 = load i8, i8* %arrayidx, align 1
  %conv13 = sext i8 %10 to i32
  %cmp14 = icmp eq i32 %conv13, 46
  br i1 %cmp14, label %if.then16, label %if.end23

if.then16:                                        ; preds = %if.end
  br label %while.cond17

while.cond17:                                     ; preds = %while.body22, %if.then16
  %11 = load i8*, i8** %q.addr, align 4
  %12 = load i8, i8* %11, align 1
  %conv18 = sext i8 %12 to i32
  %tobool = icmp ne i32 %conv18, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond17
  %13 = load i8*, i8** %q.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %13, i32 1
  store i8* %incdec.ptr, i8** %q.addr, align 4
  %14 = load i8, i8* %13, align 1
  %conv19 = sext i8 %14 to i32
  %cmp20 = icmp ne i32 %conv19, 32
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond17
  %15 = phi i1 [ false, %while.cond17 ], [ %cmp20, %land.rhs ]
  br i1 %15, label %while.body22, label %while.end

while.body22:                                     ; preds = %land.end
  br label %while.cond17

while.end:                                        ; preds = %land.end
  br label %if.end23

if.end23:                                         ; preds = %while.end, %if.end
  %16 = load i8*, i8** %p.addr, align 4
  %17 = load i8, i8* %cp, align 1
  %call24 = call i8* @nextUpperAlpha(i8* %16, i8 signext %17)
  store i8* %call24, i8** %p.addr, align 4
  %18 = load i8*, i8** %q.addr, align 4
  %19 = load i8, i8* %cq, align 1
  %call25 = call i8* @nextUpperAlpha(i8* %18, i8 signext %19)
  store i8* %call25, i8** %q.addr, align 4
  %20 = load i8*, i8** %p.addr, align 4
  %21 = load i8, i8* %20, align 1
  %conv26 = sext i8 %21 to i32
  %call27 = call i32 @toupper(i32 %conv26) #5
  %conv28 = trunc i32 %call27 to i8
  store i8 %conv28, i8* %cp, align 1
  %22 = load i8*, i8** %q.addr, align 4
  %23 = load i8, i8* %22, align 1
  %conv29 = sext i8 %23 to i32
  %call30 = call i32 @toupper(i32 %conv29) #5
  %conv31 = trunc i32 %call30 to i8
  store i8 %conv31, i8* %cq, align 1
  br label %while.cond

while.end32:                                      ; preds = %while.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end32, %if.then
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define internal i8* @nextUpperAlpha(i8* %p, i8 signext %x) #0 {
entry:
  %retval = alloca i8*, align 4
  %p.addr = alloca i8*, align 4
  %x.addr = alloca i8, align 1
  %c = alloca i8, align 1
  store i8* %p, i8** %p.addr, align 4
  store i8 %x, i8* %x.addr, align 1
  %0 = load i8*, i8** %p.addr, align 4
  %1 = load i8, i8* %0, align 1
  %conv = sext i8 %1 to i32
  %call = call i32 @toupper(i32 %conv) #5
  %conv1 = trunc i32 %call to i8
  store i8 %conv1, i8* %c, align 1
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i8*, i8** %p.addr, align 4
  %3 = load i8, i8* %2, align 1
  %conv2 = sext i8 %3 to i32
  %cmp = icmp ne i32 %conv2, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i8, i8* %c, align 1
  %conv4 = sext i8 %4 to i32
  %cmp5 = icmp sle i32 65, %conv4
  br i1 %cmp5, label %land.lhs.true, label %if.end15

land.lhs.true:                                    ; preds = %for.body
  %5 = load i8, i8* %c, align 1
  %conv7 = sext i8 %5 to i32
  %cmp8 = icmp sle i32 %conv7, 90
  br i1 %cmp8, label %if.then, label %if.end15

if.then:                                          ; preds = %land.lhs.true
  %6 = load i8, i8* %c, align 1
  %conv10 = sext i8 %6 to i32
  %7 = load i8, i8* %x.addr, align 1
  %conv11 = sext i8 %7 to i32
  %cmp12 = icmp ne i32 %conv10, %conv11
  br i1 %cmp12, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then
  %8 = load i8*, i8** %p.addr, align 4
  store i8* %8, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end15

if.end15:                                         ; preds = %if.end, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end15
  %9 = load i8*, i8** %p.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %9, i32 1
  store i8* %incdec.ptr, i8** %p.addr, align 4
  %10 = load i8, i8* %incdec.ptr, align 1
  %conv16 = sext i8 %10 to i32
  %call17 = call i32 @toupper(i32 %conv16) #5
  %conv18 = trunc i32 %call17 to i8
  store i8 %conv18, i8* %c, align 1
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load i8*, i8** %p.addr, align 4
  store i8* %11, i8** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then14
  %12 = load i8*, i8** %retval, align 4
  ret i8* %12
}

; Function Attrs: nounwind readonly
declare i32 @toupper(i32) #4

; Function Attrs: noinline nounwind optnone
define internal i8* @set_4_byte_value(i8* %bytes, i32 %value) #0 {
entry:
  %bytes.addr = alloca i8*, align 4
  %value.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %bytes, i8** %bytes.addr, align 4
  store i32 %value, i32* %value.addr, align 4
  store i32 3, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %value.addr, align 4
  %and = and i32 %1, 255
  %conv = trunc i32 %and to i8
  %2 = load i8*, i8** %bytes.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  store i8 %conv, i8* %arrayidx, align 1
  %4 = load i32, i32* %value.addr, align 4
  %shr = lshr i32 %4, 8
  store i32 %shr, i32* %value.addr, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %6 = load i8*, i8** %bytes.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %6, i32 4
  ret i8* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define internal i8* @writeChars(i8* %frame, i8* %str, i32 %n) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %str.addr = alloca i8*, align 4
  %n.addr = alloca i32, align 4
  store i8* %frame, i8** %frame.addr, align 4
  store i8* %str, i8** %str.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %n.addr, align 4
  %dec = add i32 %0, -1
  store i32 %dec, i32* %n.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load i8*, i8** %str.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %1, i32 1
  store i8* %incdec.ptr, i8** %str.addr, align 4
  %2 = load i8, i8* %1, align 1
  %3 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr1 = getelementptr inbounds i8, i8* %3, i32 1
  store i8* %incdec.ptr1, i8** %frame.addr, align 4
  store i8 %2, i8* %3, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %frame.addr, align 4
  ret i8* %4
}

; Function Attrs: noinline nounwind optnone
define internal i8* @writeUcs2s(i8* %frame, i16* %str, i32 %n) #0 {
entry:
  %frame.addr = alloca i8*, align 4
  %str.addr = alloca i16*, align 4
  %n.addr = alloca i32, align 4
  %bom = alloca i16, align 2
  %c = alloca i16, align 2
  store i8* %frame, i8** %frame.addr, align 4
  store i16* %str, i16** %str.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp ugt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i16*, i16** %str.addr, align 4
  %2 = load i16, i16* %1, align 2
  store i16 %2, i16* %bom, align 2
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then
  %3 = load i32, i32* %n.addr, align 4
  %dec = add i32 %3, -1
  store i32 %dec, i32* %n.addr, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %4 = load i16, i16* %bom, align 2
  %5 = load i16*, i16** %str.addr, align 4
  %incdec.ptr = getelementptr inbounds i16, i16* %5, i32 1
  store i16* %incdec.ptr, i16** %str.addr, align 4
  %6 = load i16, i16* %5, align 2
  %call = call zeroext i16 @toLittleEndian(i16 zeroext %4, i16 zeroext %6)
  store i16 %call, i16* %c, align 2
  %7 = load i16, i16* %c, align 2
  %conv = zext i16 %7 to i32
  %and = and i32 255, %conv
  %conv1 = trunc i32 %and to i8
  %8 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr2 = getelementptr inbounds i8, i8* %8, i32 1
  store i8* %incdec.ptr2, i8** %frame.addr, align 4
  store i8 %conv1, i8* %8, align 1
  %9 = load i16, i16* %c, align 2
  %conv3 = zext i16 %9 to i32
  %shr = ashr i32 %conv3, 8
  %and4 = and i32 255, %shr
  %conv5 = trunc i32 %and4 to i8
  %10 = load i8*, i8** %frame.addr, align 4
  %incdec.ptr6 = getelementptr inbounds i8, i8* %10, i32 1
  store i8* %incdec.ptr6, i8** %frame.addr, align 4
  store i8 %conv5, i8* %10, align 1
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.end:                                           ; preds = %while.end, %entry
  %11 = load i8*, i8** %frame.addr, align 4
  ret i8* %11
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readonly }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
