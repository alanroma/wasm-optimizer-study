; ModuleID = 'reservoir.c'
source_filename = "reservoir.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type { i32, i32, i32, double, [2 x [1600 x double]], [2 x [1776 x double]], [2 x [2 x [576 x double]]], [2 x [2 x [576 x double]]], [2 x double], [2 x double], [4 x [1024 x double]], [2 x [4 x [1024 x double]]], [2 x [4 x double]], [2 x [4 x [22 x double]]], [2 x [4 x [22 x double]]], [2 x [4 x [39 x double]]], [2 x [4 x [39 x double]]], [4 x double], [2 x [4 x double]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x [3 x i32]]], [2 x [2 x [22 x double]]], [2 x [2 x [39 x double]]], [2 x [2 x i32]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x double]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], [2 x [2 x i32]], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [2 x i32], i32, i32, i32 }
%struct.hip_global_struct = type opaque

; Function Attrs: noinline nounwind optnone
define hidden i32 @ResvFrameBegin(%struct.lame_internal_flags* %gfc, i32* %mean_bits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %mean_bits.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %fullFrameBits = alloca i32, align 4
  %resvLimit = alloca i32, align 4
  %maxmp3buf = alloca i32, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %frameLength = alloca i32, align 4
  %meanBits = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32* %mean_bits, i32** %mean_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @getframebits(%struct.lame_internal_flags* %3)
  store i32 %call, i32* %frameLength, align 4
  %4 = load i32, i32* %frameLength, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 2
  %6 = load i32, i32* %sideinfo_len, align 4
  %mul = mul nsw i32 %6, 8
  %sub = sub nsw i32 %4, %mul
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 15
  %8 = load i32, i32* %mode_gr, align 4
  %div = sdiv i32 %sub, %8
  store i32 %div, i32* %meanBits, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr3 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 15
  %10 = load i32, i32* %mode_gr3, align 4
  %mul4 = mul nsw i32 2048, %10
  %sub5 = sub nsw i32 %mul4, 8
  store i32 %sub5, i32* %resvLimit, align 4
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %buffer_constraint = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 33
  %12 = load i32, i32* %buffer_constraint, align 4
  store i32 %12, i32* %maxmp3buf, align 4
  %13 = load i32, i32* %maxmp3buf, align 4
  %14 = load i32, i32* %frameLength, align 4
  %sub6 = sub nsw i32 %13, %14
  %15 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %15, i32 0, i32 13
  store i32 %sub6, i32* %ResvMax, align 8
  %16 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax7 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %16, i32 0, i32 13
  %17 = load i32, i32* %ResvMax7, align 8
  %18 = load i32, i32* %resvLimit, align 4
  %cmp = icmp sgt i32 %17, %18
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %19 = load i32, i32* %resvLimit, align 4
  %20 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax8 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %20, i32 0, i32 13
  store i32 %19, i32* %ResvMax8, align 8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %21 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax9 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %21, i32 0, i32 13
  %22 = load i32, i32* %ResvMax9, align 8
  %cmp10 = icmp slt i32 %22, 0
  br i1 %cmp10, label %if.then11, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %23 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %disable_reservoir = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %23, i32 0, i32 32
  %24 = load i32, i32* %disable_reservoir, align 4
  %tobool = icmp ne i32 %24, 0
  br i1 %tobool, label %if.then11, label %if.end13

if.then11:                                        ; preds = %lor.lhs.false, %if.end
  %25 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax12 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %25, i32 0, i32 13
  store i32 0, i32* %ResvMax12, align 8
  br label %if.end13

if.end13:                                         ; preds = %if.then11, %lor.lhs.false
  %26 = load i32, i32* %meanBits, align 4
  %27 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr14 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %27, i32 0, i32 15
  %28 = load i32, i32* %mode_gr14, align 4
  %mul15 = mul nsw i32 %26, %28
  %29 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %29, i32 0, i32 12
  %30 = load i32, i32* %ResvSize, align 4
  %31 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax16 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %31, i32 0, i32 13
  %32 = load i32, i32* %ResvMax16, align 8
  %cmp17 = icmp slt i32 %30, %32
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end13
  %33 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize18 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %33, i32 0, i32 12
  %34 = load i32, i32* %ResvSize18, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end13
  %35 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax19 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %35, i32 0, i32 13
  %36 = load i32, i32* %ResvMax19, align 8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %34, %cond.true ], [ %36, %cond.false ]
  %add = add nsw i32 %mul15, %cond
  store i32 %add, i32* %fullFrameBits, align 4
  %37 = load i32, i32* %fullFrameBits, align 4
  %38 = load i32, i32* %maxmp3buf, align 4
  %cmp20 = icmp sgt i32 %37, %38
  br i1 %cmp20, label %if.then21, label %if.end22

if.then21:                                        ; preds = %cond.end
  %39 = load i32, i32* %maxmp3buf, align 4
  store i32 %39, i32* %fullFrameBits, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then21, %cond.end
  %40 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_pre = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %40, i32 0, i32 3
  store i32 0, i32* %resvDrain_pre, align 4
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 23
  %42 = load %struct.plotting_data*, %struct.plotting_data** %pinfo, align 8
  %cmp23 = icmp ne %struct.plotting_data* %42, null
  br i1 %cmp23, label %if.then24, label %if.end30

if.then24:                                        ; preds = %if.end22
  %43 = load i32, i32* %meanBits, align 4
  %div25 = sdiv i32 %43, 2
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo26 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %44, i32 0, i32 23
  %45 = load %struct.plotting_data*, %struct.plotting_data** %pinfo26, align 8
  %mean_bits27 = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %45, i32 0, i32 55
  store i32 %div25, i32* %mean_bits27, align 4
  %46 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize28 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %46, i32 0, i32 12
  %47 = load i32, i32* %ResvSize28, align 4
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %pinfo29 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %48, i32 0, i32 23
  %49 = load %struct.plotting_data*, %struct.plotting_data** %pinfo29, align 8
  %resvsize = getelementptr inbounds %struct.plotting_data, %struct.plotting_data* %49, i32 0, i32 56
  store i32 %47, i32* %resvsize, align 8
  br label %if.end30

if.end30:                                         ; preds = %if.then24, %if.end22
  %50 = load i32, i32* %meanBits, align 4
  %51 = load i32*, i32** %mean_bits.addr, align 4
  store i32 %50, i32* %51, align 4
  %52 = load i32, i32* %fullFrameBits, align 4
  ret i32 %52
}

declare i32 @getframebits(%struct.lame_internal_flags*) #1

; Function Attrs: noinline nounwind optnone
define hidden void @ResvMaxBits(%struct.lame_internal_flags* %gfc, i32 %mean_bits, i32* %targ_bits, i32* %extra_bits, i32 %cbr) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %mean_bits.addr = alloca i32, align 4
  %targ_bits.addr = alloca i32*, align 4
  %extra_bits.addr = alloca i32*, align 4
  %cbr.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %add_bits = alloca i32, align 4
  %targBits = alloca i32, align 4
  %extraBits = alloca i32, align 4
  %ResvSize = alloca i32, align 4
  %ResvMax = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %mean_bits, i32* %mean_bits.addr, align 4
  store i32* %targ_bits, i32** %targ_bits.addr, align 4
  store i32* %extra_bits, i32** %extra_bits.addr, align 4
  store i32 %cbr, i32* %cbr.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize2 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %2, i32 0, i32 12
  %3 = load i32, i32* %ResvSize2, align 4
  store i32 %3, i32* %ResvSize, align 4
  %4 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax3 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %4, i32 0, i32 13
  %5 = load i32, i32* %ResvMax3, align 8
  store i32 %5, i32* %ResvMax, align 4
  %6 = load i32, i32* %cbr.addr, align 4
  %tobool = icmp ne i32 %6, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load i32, i32* %mean_bits.addr, align 4
  %8 = load i32, i32* %ResvSize, align 4
  %add = add nsw i32 %8, %7
  store i32 %add, i32* %ResvSize, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %10 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %10, 1
  %tobool4 = icmp ne i32 %and, 0
  br i1 %tobool4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %11 = load i32, i32* %ResvMax, align 4
  %conv = sitofp i32 %11 to double
  %mul = fmul double %conv, 9.000000e-01
  %conv6 = fptosi double %mul to i32
  store i32 %conv6, i32* %ResvMax, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  %12 = load i32, i32* %mean_bits.addr, align 4
  store i32 %12, i32* %targBits, align 4
  %13 = load i32, i32* %ResvSize, align 4
  %mul8 = mul nsw i32 %13, 10
  %14 = load i32, i32* %ResvMax, align 4
  %mul9 = mul nsw i32 %14, 9
  %cmp = icmp sgt i32 %mul8, %mul9
  br i1 %cmp, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end7
  %15 = load i32, i32* %ResvSize, align 4
  %16 = load i32, i32* %ResvMax, align 4
  %mul12 = mul nsw i32 %16, 9
  %div = sdiv i32 %mul12, 10
  %sub = sub nsw i32 %15, %div
  store i32 %sub, i32* %add_bits, align 4
  %17 = load i32, i32* %add_bits, align 4
  %18 = load i32, i32* %targBits, align 4
  %add13 = add nsw i32 %18, %17
  store i32 %add13, i32* %targBits, align 4
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 13
  %substep_shaping15 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt14, i32 0, i32 9
  %20 = load i32, i32* %substep_shaping15, align 8
  %or = or i32 %20, 128
  store i32 %or, i32* %substep_shaping15, align 8
  br label %if.end31

if.else:                                          ; preds = %if.end7
  store i32 0, i32* %add_bits, align 4
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 13
  %substep_shaping17 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt16, i32 0, i32 9
  %22 = load i32, i32* %substep_shaping17, align 8
  %and18 = and i32 %22, 127
  store i32 %and18, i32* %substep_shaping17, align 8
  %23 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %disable_reservoir = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %23, i32 0, i32 32
  %24 = load i32, i32* %disable_reservoir, align 4
  %tobool19 = icmp ne i32 %24, 0
  br i1 %tobool19, label %if.end30, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.else
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 13
  %substep_shaping21 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt20, i32 0, i32 9
  %26 = load i32, i32* %substep_shaping21, align 8
  %and22 = and i32 %26, 1
  %tobool23 = icmp ne i32 %and22, 0
  br i1 %tobool23, label %if.end30, label %if.then24

if.then24:                                        ; preds = %land.lhs.true
  %27 = load i32, i32* %mean_bits.addr, align 4
  %conv25 = sitofp i32 %27 to double
  %mul26 = fmul double 1.000000e-01, %conv25
  %28 = load i32, i32* %targBits, align 4
  %conv27 = sitofp i32 %28 to double
  %sub28 = fsub double %conv27, %mul26
  %conv29 = fptosi double %sub28 to i32
  store i32 %conv29, i32* %targBits, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then24, %land.lhs.true, %if.else
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then11
  %29 = load i32, i32* %ResvSize, align 4
  %30 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax32 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %30, i32 0, i32 13
  %31 = load i32, i32* %ResvMax32, align 8
  %mul33 = mul nsw i32 %31, 6
  %div34 = sdiv i32 %mul33, 10
  %cmp35 = icmp slt i32 %29, %div34
  br i1 %cmp35, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end31
  %32 = load i32, i32* %ResvSize, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end31
  %33 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax37 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %33, i32 0, i32 13
  %34 = load i32, i32* %ResvMax37, align 8
  %mul38 = mul nsw i32 %34, 6
  %div39 = sdiv i32 %mul38, 10
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %32, %cond.true ], [ %div39, %cond.false ]
  store i32 %cond, i32* %extraBits, align 4
  %35 = load i32, i32* %add_bits, align 4
  %36 = load i32, i32* %extraBits, align 4
  %sub40 = sub nsw i32 %36, %35
  store i32 %sub40, i32* %extraBits, align 4
  %37 = load i32, i32* %extraBits, align 4
  %cmp41 = icmp slt i32 %37, 0
  br i1 %cmp41, label %if.then43, label %if.end44

if.then43:                                        ; preds = %cond.end
  store i32 0, i32* %extraBits, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then43, %cond.end
  %38 = load i32, i32* %targBits, align 4
  %39 = load i32*, i32** %targ_bits.addr, align 4
  store i32 %38, i32* %39, align 4
  %40 = load i32, i32* %extraBits, align 4
  %41 = load i32*, i32** %extra_bits.addr, align 4
  store i32 %40, i32* %41, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @ResvAdjust(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 4
  %1 = load i32, i32* %part2_3_length, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 18
  %3 = load i32, i32* %part2_length, align 4
  %add = add nsw i32 %1, %3
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 11
  %ResvSize = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 12
  %5 = load i32, i32* %ResvSize, align 4
  %sub = sub nsw i32 %5, %add
  store i32 %sub, i32* %ResvSize, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @ResvFrameEnd(%struct.lame_internal_flags* %gfc, i32 %mean_bits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %mean_bits.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %esv = alloca %struct.EncStateVar_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %stuffingBits = alloca i32, align 4
  %over_bits = alloca i32, align 4
  %mdb_bytes = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %mean_bits, i32* %mean_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 11
  store %struct.EncStateVar_t* %sv_enc, %struct.EncStateVar_t** %esv, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %3 = load i32, i32* %mean_bits.addr, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 15
  %5 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 %3, %5
  %6 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %6, i32 0, i32 12
  %7 = load i32, i32* %ResvSize, align 4
  %add = add nsw i32 %7, %mul
  store i32 %add, i32* %ResvSize, align 4
  store i32 0, i32* %stuffingBits, align 4
  %8 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %8, i32 0, i32 4
  store i32 0, i32* %resvDrain_post, align 4
  %9 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_pre = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %9, i32 0, i32 3
  store i32 0, i32* %resvDrain_pre, align 4
  %10 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize3 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %10, i32 0, i32 12
  %11 = load i32, i32* %ResvSize3, align 4
  %rem = srem i32 %11, 8
  store i32 %rem, i32* %over_bits, align 4
  %cmp = icmp ne i32 %rem, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load i32, i32* %over_bits, align 4
  %13 = load i32, i32* %stuffingBits, align 4
  %add4 = add nsw i32 %13, %12
  store i32 %add4, i32* %stuffingBits, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize5 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %14, i32 0, i32 12
  %15 = load i32, i32* %ResvSize5, align 4
  %16 = load i32, i32* %stuffingBits, align 4
  %sub = sub nsw i32 %15, %16
  %17 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvMax = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %17, i32 0, i32 13
  %18 = load i32, i32* %ResvMax, align 8
  %sub6 = sub nsw i32 %sub, %18
  store i32 %sub6, i32* %over_bits, align 4
  %19 = load i32, i32* %over_bits, align 4
  %cmp7 = icmp sgt i32 %19, 0
  br i1 %cmp7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.end
  %20 = load i32, i32* %over_bits, align 4
  %21 = load i32, i32* %stuffingBits, align 4
  %add9 = add nsw i32 %21, %20
  store i32 %add9, i32* %stuffingBits, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then8, %if.end
  %22 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %22, i32 0, i32 1
  %23 = load i32, i32* %main_data_begin, align 4
  %mul11 = mul nsw i32 %23, 8
  %24 = load i32, i32* %stuffingBits, align 4
  %cmp12 = icmp slt i32 %mul11, %24
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end10
  %25 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin13 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %25, i32 0, i32 1
  %26 = load i32, i32* %main_data_begin13, align 4
  %mul14 = mul nsw i32 %26, 8
  br label %cond.end

cond.false:                                       ; preds = %if.end10
  %27 = load i32, i32* %stuffingBits, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul14, %cond.true ], [ %27, %cond.false ]
  %div = sdiv i32 %cond, 8
  store i32 %div, i32* %mdb_bytes, align 4
  %28 = load i32, i32* %mdb_bytes, align 4
  %mul15 = mul nsw i32 8, %28
  %29 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_pre16 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %29, i32 0, i32 3
  %30 = load i32, i32* %resvDrain_pre16, align 4
  %add17 = add nsw i32 %30, %mul15
  store i32 %add17, i32* %resvDrain_pre16, align 4
  %31 = load i32, i32* %mdb_bytes, align 4
  %mul18 = mul nsw i32 8, %31
  %32 = load i32, i32* %stuffingBits, align 4
  %sub19 = sub nsw i32 %32, %mul18
  store i32 %sub19, i32* %stuffingBits, align 4
  %33 = load i32, i32* %mdb_bytes, align 4
  %mul20 = mul nsw i32 8, %33
  %34 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize21 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %34, i32 0, i32 12
  %35 = load i32, i32* %ResvSize21, align 4
  %sub22 = sub nsw i32 %35, %mul20
  store i32 %sub22, i32* %ResvSize21, align 4
  %36 = load i32, i32* %mdb_bytes, align 4
  %37 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %main_data_begin23 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %37, i32 0, i32 1
  %38 = load i32, i32* %main_data_begin23, align 4
  %sub24 = sub nsw i32 %38, %36
  store i32 %sub24, i32* %main_data_begin23, align 4
  %39 = load i32, i32* %stuffingBits, align 4
  %40 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %resvDrain_post25 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %40, i32 0, i32 4
  %41 = load i32, i32* %resvDrain_post25, align 4
  %add26 = add nsw i32 %41, %39
  store i32 %add26, i32* %resvDrain_post25, align 4
  %42 = load i32, i32* %stuffingBits, align 4
  %43 = load %struct.EncStateVar_t*, %struct.EncStateVar_t** %esv, align 4
  %ResvSize27 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %43, i32 0, i32 12
  %44 = load i32, i32* %ResvSize27, align 4
  %sub28 = sub nsw i32 %44, %42
  store i32 %sub28, i32* %ResvSize27, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
