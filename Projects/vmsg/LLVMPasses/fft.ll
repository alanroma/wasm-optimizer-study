; ModuleID = 'fft.c'
source_filename = "fft.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque

@rv_tbl = internal constant [128 x i8] c"\00\80@\C0 \A0`\E0\10\90P\D00\B0p\F0\08\88H\C8(\A8h\E8\18\98X\D88\B8x\F8\04\84D\C4$\A4d\E4\14\94T\D44\B4t\F4\0C\8CL\CC,\ACl\EC\1C\9C\\\DC<\BC|\FC\02\82B\C2\22\A2b\E2\12\92R\D22\B2r\F2\0A\8AJ\CA*\AAj\EA\1A\9AZ\DA:\BAz\FA\06\86F\C6&\A6f\E6\16\96V\D66\B6v\F6\0E\8EN\CE.\AEn\EE\1E\9E^\DE>\BE~\FE", align 16
@costab = internal constant [8 x float] [float 0x3FED906BC0000000, float 0x3FD87DE2A0000000, float 0x3FEFD88DA0000000, float 0x3FB917A6C0000000, float 0x3FEFFD8860000000, float 0x3F99215600000000, float 0x3FEFFFD880000000, float 0x3F7921F100000000], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @fft_short(%struct.lame_internal_flags* %gfc, [256 x float]* %x_real, i32 %chn, float** %buffer) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %x_real.addr = alloca [256 x float]*, align 4
  %chn.addr = alloca i32, align 4
  %buffer.addr = alloca float**, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %b = alloca i32, align 4
  %x = alloca float*, align 4
  %k = alloca i16, align 2
  %f0 = alloca float, align 4
  %f1 = alloca float, align 4
  %f2 = alloca float, align 4
  %f3 = alloca float, align 4
  %w = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [256 x float]* %x_real, [256 x float]** %x_real.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 0, i32* %b, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %b, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load [256 x float]*, [256 x float]** %x_real.addr, align 4
  %2 = load i32, i32* %b, align 4
  %arrayidx = getelementptr inbounds [256 x float], [256 x float]* %1, i32 %2
  %arrayidx1 = getelementptr inbounds [256 x float], [256 x float]* %arrayidx, i32 0, i32 128
  store float* %arrayidx1, float** %x, align 4
  %3 = load i32, i32* %b, align 4
  %add = add nsw i32 %3, 1
  %mul = mul nsw i32 192, %add
  %conv = trunc i32 %mul to i16
  store i16 %conv, i16* %k, align 2
  store i32 31, i32* %j, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %for.body
  %4 = load i32, i32* %j, align 4
  %shl = shl i32 %4, 2
  %arrayidx2 = getelementptr inbounds [128 x i8], [128 x i8]* @rv_tbl, i32 0, i32 %shl
  %5 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %5 to i32
  store i32 %conv3, i32* %i, align 4
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %6, i32 0, i32 22
  %7 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %window_s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %7, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [128 x float], [128 x float]* %window_s, i32 0, i32 %8
  %9 = load float, float* %arrayidx4, align 4
  %10 = load float**, float*** %buffer.addr, align 4
  %11 = load i32, i32* %chn.addr, align 4
  %arrayidx5 = getelementptr inbounds float*, float** %10, i32 %11
  %12 = load float*, float** %arrayidx5, align 4
  %13 = load i32, i32* %i, align 4
  %14 = load i16, i16* %k, align 2
  %conv6 = sext i16 %14 to i32
  %add7 = add nsw i32 %13, %conv6
  %arrayidx8 = getelementptr inbounds float, float* %12, i32 %add7
  %15 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %9, %15
  store float %mul9, float* %f0, align 4
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 22
  %17 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy10, align 4
  %window_s11 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %17, i32 0, i32 1
  %18 = load i32, i32* %i, align 4
  %sub = sub nsw i32 127, %18
  %arrayidx12 = getelementptr inbounds [128 x float], [128 x float]* %window_s11, i32 0, i32 %sub
  %19 = load float, float* %arrayidx12, align 4
  %20 = load float**, float*** %buffer.addr, align 4
  %21 = load i32, i32* %chn.addr, align 4
  %arrayidx13 = getelementptr inbounds float*, float** %20, i32 %21
  %22 = load float*, float** %arrayidx13, align 4
  %23 = load i32, i32* %i, align 4
  %24 = load i16, i16* %k, align 2
  %conv14 = sext i16 %24 to i32
  %add15 = add nsw i32 %23, %conv14
  %add16 = add nsw i32 %add15, 128
  %arrayidx17 = getelementptr inbounds float, float* %22, i32 %add16
  %25 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %19, %25
  store float %mul18, float* %w, align 4
  %26 = load float, float* %f0, align 4
  %27 = load float, float* %w, align 4
  %sub19 = fsub float %26, %27
  store float %sub19, float* %f1, align 4
  %28 = load float, float* %f0, align 4
  %29 = load float, float* %w, align 4
  %add20 = fadd float %28, %29
  store float %add20, float* %f0, align 4
  %30 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %30, i32 0, i32 22
  %31 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy21, align 4
  %window_s22 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %31, i32 0, i32 1
  %32 = load i32, i32* %i, align 4
  %add23 = add nsw i32 %32, 64
  %arrayidx24 = getelementptr inbounds [128 x float], [128 x float]* %window_s22, i32 0, i32 %add23
  %33 = load float, float* %arrayidx24, align 4
  %34 = load float**, float*** %buffer.addr, align 4
  %35 = load i32, i32* %chn.addr, align 4
  %arrayidx25 = getelementptr inbounds float*, float** %34, i32 %35
  %36 = load float*, float** %arrayidx25, align 4
  %37 = load i32, i32* %i, align 4
  %38 = load i16, i16* %k, align 2
  %conv26 = sext i16 %38 to i32
  %add27 = add nsw i32 %37, %conv26
  %add28 = add nsw i32 %add27, 64
  %arrayidx29 = getelementptr inbounds float, float* %36, i32 %add28
  %39 = load float, float* %arrayidx29, align 4
  %mul30 = fmul float %33, %39
  store float %mul30, float* %f2, align 4
  %40 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy31 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %40, i32 0, i32 22
  %41 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy31, align 4
  %window_s32 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %41, i32 0, i32 1
  %42 = load i32, i32* %i, align 4
  %sub33 = sub nsw i32 63, %42
  %arrayidx34 = getelementptr inbounds [128 x float], [128 x float]* %window_s32, i32 0, i32 %sub33
  %43 = load float, float* %arrayidx34, align 4
  %44 = load float**, float*** %buffer.addr, align 4
  %45 = load i32, i32* %chn.addr, align 4
  %arrayidx35 = getelementptr inbounds float*, float** %44, i32 %45
  %46 = load float*, float** %arrayidx35, align 4
  %47 = load i32, i32* %i, align 4
  %48 = load i16, i16* %k, align 2
  %conv36 = sext i16 %48 to i32
  %add37 = add nsw i32 %47, %conv36
  %add38 = add nsw i32 %add37, 192
  %arrayidx39 = getelementptr inbounds float, float* %46, i32 %add38
  %49 = load float, float* %arrayidx39, align 4
  %mul40 = fmul float %43, %49
  store float %mul40, float* %w, align 4
  %50 = load float, float* %f2, align 4
  %51 = load float, float* %w, align 4
  %sub41 = fsub float %50, %51
  store float %sub41, float* %f3, align 4
  %52 = load float, float* %f2, align 4
  %53 = load float, float* %w, align 4
  %add42 = fadd float %52, %53
  store float %add42, float* %f2, align 4
  %54 = load float*, float** %x, align 4
  %add.ptr = getelementptr inbounds float, float* %54, i32 -4
  store float* %add.ptr, float** %x, align 4
  %55 = load float, float* %f0, align 4
  %56 = load float, float* %f2, align 4
  %add43 = fadd float %55, %56
  %57 = load float*, float** %x, align 4
  %arrayidx44 = getelementptr inbounds float, float* %57, i32 0
  store float %add43, float* %arrayidx44, align 4
  %58 = load float, float* %f0, align 4
  %59 = load float, float* %f2, align 4
  %sub45 = fsub float %58, %59
  %60 = load float*, float** %x, align 4
  %arrayidx46 = getelementptr inbounds float, float* %60, i32 2
  store float %sub45, float* %arrayidx46, align 4
  %61 = load float, float* %f1, align 4
  %62 = load float, float* %f3, align 4
  %add47 = fadd float %61, %62
  %63 = load float*, float** %x, align 4
  %arrayidx48 = getelementptr inbounds float, float* %63, i32 1
  store float %add47, float* %arrayidx48, align 4
  %64 = load float, float* %f1, align 4
  %65 = load float, float* %f3, align 4
  %sub49 = fsub float %64, %65
  %66 = load float*, float** %x, align 4
  %arrayidx50 = getelementptr inbounds float, float* %66, i32 3
  store float %sub49, float* %arrayidx50, align 4
  %67 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy51 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %67, i32 0, i32 22
  %68 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy51, align 4
  %window_s52 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %68, i32 0, i32 1
  %69 = load i32, i32* %i, align 4
  %add53 = add nsw i32 %69, 1
  %arrayidx54 = getelementptr inbounds [128 x float], [128 x float]* %window_s52, i32 0, i32 %add53
  %70 = load float, float* %arrayidx54, align 4
  %71 = load float**, float*** %buffer.addr, align 4
  %72 = load i32, i32* %chn.addr, align 4
  %arrayidx55 = getelementptr inbounds float*, float** %71, i32 %72
  %73 = load float*, float** %arrayidx55, align 4
  %74 = load i32, i32* %i, align 4
  %75 = load i16, i16* %k, align 2
  %conv56 = sext i16 %75 to i32
  %add57 = add nsw i32 %74, %conv56
  %add58 = add nsw i32 %add57, 1
  %arrayidx59 = getelementptr inbounds float, float* %73, i32 %add58
  %76 = load float, float* %arrayidx59, align 4
  %mul60 = fmul float %70, %76
  store float %mul60, float* %f0, align 4
  %77 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy61 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %77, i32 0, i32 22
  %78 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy61, align 4
  %window_s62 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %78, i32 0, i32 1
  %79 = load i32, i32* %i, align 4
  %sub63 = sub nsw i32 126, %79
  %arrayidx64 = getelementptr inbounds [128 x float], [128 x float]* %window_s62, i32 0, i32 %sub63
  %80 = load float, float* %arrayidx64, align 4
  %81 = load float**, float*** %buffer.addr, align 4
  %82 = load i32, i32* %chn.addr, align 4
  %arrayidx65 = getelementptr inbounds float*, float** %81, i32 %82
  %83 = load float*, float** %arrayidx65, align 4
  %84 = load i32, i32* %i, align 4
  %85 = load i16, i16* %k, align 2
  %conv66 = sext i16 %85 to i32
  %add67 = add nsw i32 %84, %conv66
  %add68 = add nsw i32 %add67, 129
  %arrayidx69 = getelementptr inbounds float, float* %83, i32 %add68
  %86 = load float, float* %arrayidx69, align 4
  %mul70 = fmul float %80, %86
  store float %mul70, float* %w, align 4
  %87 = load float, float* %f0, align 4
  %88 = load float, float* %w, align 4
  %sub71 = fsub float %87, %88
  store float %sub71, float* %f1, align 4
  %89 = load float, float* %f0, align 4
  %90 = load float, float* %w, align 4
  %add72 = fadd float %89, %90
  store float %add72, float* %f0, align 4
  %91 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy73 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %91, i32 0, i32 22
  %92 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy73, align 4
  %window_s74 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %92, i32 0, i32 1
  %93 = load i32, i32* %i, align 4
  %add75 = add nsw i32 %93, 65
  %arrayidx76 = getelementptr inbounds [128 x float], [128 x float]* %window_s74, i32 0, i32 %add75
  %94 = load float, float* %arrayidx76, align 4
  %95 = load float**, float*** %buffer.addr, align 4
  %96 = load i32, i32* %chn.addr, align 4
  %arrayidx77 = getelementptr inbounds float*, float** %95, i32 %96
  %97 = load float*, float** %arrayidx77, align 4
  %98 = load i32, i32* %i, align 4
  %99 = load i16, i16* %k, align 2
  %conv78 = sext i16 %99 to i32
  %add79 = add nsw i32 %98, %conv78
  %add80 = add nsw i32 %add79, 65
  %arrayidx81 = getelementptr inbounds float, float* %97, i32 %add80
  %100 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %94, %100
  store float %mul82, float* %f2, align 4
  %101 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy83 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %101, i32 0, i32 22
  %102 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy83, align 4
  %window_s84 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %102, i32 0, i32 1
  %103 = load i32, i32* %i, align 4
  %sub85 = sub nsw i32 62, %103
  %arrayidx86 = getelementptr inbounds [128 x float], [128 x float]* %window_s84, i32 0, i32 %sub85
  %104 = load float, float* %arrayidx86, align 4
  %105 = load float**, float*** %buffer.addr, align 4
  %106 = load i32, i32* %chn.addr, align 4
  %arrayidx87 = getelementptr inbounds float*, float** %105, i32 %106
  %107 = load float*, float** %arrayidx87, align 4
  %108 = load i32, i32* %i, align 4
  %109 = load i16, i16* %k, align 2
  %conv88 = sext i16 %109 to i32
  %add89 = add nsw i32 %108, %conv88
  %add90 = add nsw i32 %add89, 193
  %arrayidx91 = getelementptr inbounds float, float* %107, i32 %add90
  %110 = load float, float* %arrayidx91, align 4
  %mul92 = fmul float %104, %110
  store float %mul92, float* %w, align 4
  %111 = load float, float* %f2, align 4
  %112 = load float, float* %w, align 4
  %sub93 = fsub float %111, %112
  store float %sub93, float* %f3, align 4
  %113 = load float, float* %f2, align 4
  %114 = load float, float* %w, align 4
  %add94 = fadd float %113, %114
  store float %add94, float* %f2, align 4
  %115 = load float, float* %f0, align 4
  %116 = load float, float* %f2, align 4
  %add95 = fadd float %115, %116
  %117 = load float*, float** %x, align 4
  %arrayidx96 = getelementptr inbounds float, float* %117, i32 128
  store float %add95, float* %arrayidx96, align 4
  %118 = load float, float* %f0, align 4
  %119 = load float, float* %f2, align 4
  %sub97 = fsub float %118, %119
  %120 = load float*, float** %x, align 4
  %arrayidx98 = getelementptr inbounds float, float* %120, i32 130
  store float %sub97, float* %arrayidx98, align 4
  %121 = load float, float* %f1, align 4
  %122 = load float, float* %f3, align 4
  %add99 = fadd float %121, %122
  %123 = load float*, float** %x, align 4
  %arrayidx100 = getelementptr inbounds float, float* %123, i32 129
  store float %add99, float* %arrayidx100, align 4
  %124 = load float, float* %f1, align 4
  %125 = load float, float* %f3, align 4
  %sub101 = fsub float %124, %125
  %126 = load float*, float** %x, align 4
  %arrayidx102 = getelementptr inbounds float, float* %126, i32 131
  store float %sub101, float* %arrayidx102, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %127 = load i32, i32* %j, align 4
  %dec = add nsw i32 %127, -1
  store i32 %dec, i32* %j, align 4
  %cmp103 = icmp sge i32 %dec, 0
  br i1 %cmp103, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %128 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %fft_fht = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %128, i32 0, i32 26
  %129 = load void (float*, i32)*, void (float*, i32)** %fft_fht, align 4
  %130 = load float*, float** %x, align 4
  call void %129(float* %130, i32 128)
  br label %for.inc

for.inc:                                          ; preds = %do.end
  %131 = load i32, i32* %b, align 4
  %inc = add nsw i32 %131, 1
  store i32 %inc, i32* %b, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @fft_long(%struct.lame_internal_flags* %gfc, float* %x, i32 %chn, float** %buffer) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %x.addr = alloca float*, align 4
  %chn.addr = alloca i32, align 4
  %buffer.addr = alloca float**, align 4
  %i = alloca i32, align 4
  %jj = alloca i32, align 4
  %f0 = alloca float, align 4
  %f1 = alloca float, align 4
  %f2 = alloca float, align 4
  %f3 = alloca float, align 4
  %w = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store float* %x, float** %x.addr, align 4
  store i32 %chn, i32* %chn.addr, align 4
  store float** %buffer, float*** %buffer.addr, align 4
  store i32 127, i32* %jj, align 4
  %0 = load float*, float** %x.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %0, i32 512
  store float* %add.ptr, float** %x.addr, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %1 = load i32, i32* %jj, align 4
  %arrayidx = getelementptr inbounds [128 x i8], [128 x i8]* @rv_tbl, i32 0, i32 %1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  store i32 %conv, i32* %i, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 22
  %4 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %window = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %4, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds [1024 x float], [1024 x float]* %window, i32 0, i32 %5
  %6 = load float, float* %arrayidx1, align 4
  %7 = load float**, float*** %buffer.addr, align 4
  %8 = load i32, i32* %chn.addr, align 4
  %arrayidx2 = getelementptr inbounds float*, float** %7, i32 %8
  %9 = load float*, float** %arrayidx2, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx3, align 4
  %mul = fmul float %6, %11
  store float %mul, float* %f0, align 4
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 22
  %13 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy4, align 4
  %window5 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %13, i32 0, i32 0
  %14 = load i32, i32* %i, align 4
  %add = add nsw i32 %14, 512
  %arrayidx6 = getelementptr inbounds [1024 x float], [1024 x float]* %window5, i32 0, i32 %add
  %15 = load float, float* %arrayidx6, align 4
  %16 = load float**, float*** %buffer.addr, align 4
  %17 = load i32, i32* %chn.addr, align 4
  %arrayidx7 = getelementptr inbounds float*, float** %16, i32 %17
  %18 = load float*, float** %arrayidx7, align 4
  %19 = load i32, i32* %i, align 4
  %add8 = add nsw i32 %19, 512
  %arrayidx9 = getelementptr inbounds float, float* %18, i32 %add8
  %20 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %20
  store float %mul10, float* %w, align 4
  %21 = load float, float* %f0, align 4
  %22 = load float, float* %w, align 4
  %sub = fsub float %21, %22
  store float %sub, float* %f1, align 4
  %23 = load float, float* %f0, align 4
  %24 = load float, float* %w, align 4
  %add11 = fadd float %23, %24
  store float %add11, float* %f0, align 4
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 22
  %26 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy12, align 4
  %window13 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %26, i32 0, i32 0
  %27 = load i32, i32* %i, align 4
  %add14 = add nsw i32 %27, 256
  %arrayidx15 = getelementptr inbounds [1024 x float], [1024 x float]* %window13, i32 0, i32 %add14
  %28 = load float, float* %arrayidx15, align 4
  %29 = load float**, float*** %buffer.addr, align 4
  %30 = load i32, i32* %chn.addr, align 4
  %arrayidx16 = getelementptr inbounds float*, float** %29, i32 %30
  %31 = load float*, float** %arrayidx16, align 4
  %32 = load i32, i32* %i, align 4
  %add17 = add nsw i32 %32, 256
  %arrayidx18 = getelementptr inbounds float, float* %31, i32 %add17
  %33 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %28, %33
  store float %mul19, float* %f2, align 4
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 22
  %35 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy20, align 4
  %window21 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %35, i32 0, i32 0
  %36 = load i32, i32* %i, align 4
  %add22 = add nsw i32 %36, 768
  %arrayidx23 = getelementptr inbounds [1024 x float], [1024 x float]* %window21, i32 0, i32 %add22
  %37 = load float, float* %arrayidx23, align 4
  %38 = load float**, float*** %buffer.addr, align 4
  %39 = load i32, i32* %chn.addr, align 4
  %arrayidx24 = getelementptr inbounds float*, float** %38, i32 %39
  %40 = load float*, float** %arrayidx24, align 4
  %41 = load i32, i32* %i, align 4
  %add25 = add nsw i32 %41, 768
  %arrayidx26 = getelementptr inbounds float, float* %40, i32 %add25
  %42 = load float, float* %arrayidx26, align 4
  %mul27 = fmul float %37, %42
  store float %mul27, float* %w, align 4
  %43 = load float, float* %f2, align 4
  %44 = load float, float* %w, align 4
  %sub28 = fsub float %43, %44
  store float %sub28, float* %f3, align 4
  %45 = load float, float* %f2, align 4
  %46 = load float, float* %w, align 4
  %add29 = fadd float %45, %46
  store float %add29, float* %f2, align 4
  %47 = load float*, float** %x.addr, align 4
  %add.ptr30 = getelementptr inbounds float, float* %47, i32 -4
  store float* %add.ptr30, float** %x.addr, align 4
  %48 = load float, float* %f0, align 4
  %49 = load float, float* %f2, align 4
  %add31 = fadd float %48, %49
  %50 = load float*, float** %x.addr, align 4
  %arrayidx32 = getelementptr inbounds float, float* %50, i32 0
  store float %add31, float* %arrayidx32, align 4
  %51 = load float, float* %f0, align 4
  %52 = load float, float* %f2, align 4
  %sub33 = fsub float %51, %52
  %53 = load float*, float** %x.addr, align 4
  %arrayidx34 = getelementptr inbounds float, float* %53, i32 2
  store float %sub33, float* %arrayidx34, align 4
  %54 = load float, float* %f1, align 4
  %55 = load float, float* %f3, align 4
  %add35 = fadd float %54, %55
  %56 = load float*, float** %x.addr, align 4
  %arrayidx36 = getelementptr inbounds float, float* %56, i32 1
  store float %add35, float* %arrayidx36, align 4
  %57 = load float, float* %f1, align 4
  %58 = load float, float* %f3, align 4
  %sub37 = fsub float %57, %58
  %59 = load float*, float** %x.addr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %59, i32 3
  store float %sub37, float* %arrayidx38, align 4
  %60 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy39 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %60, i32 0, i32 22
  %61 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy39, align 4
  %window40 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %61, i32 0, i32 0
  %62 = load i32, i32* %i, align 4
  %add41 = add nsw i32 %62, 1
  %arrayidx42 = getelementptr inbounds [1024 x float], [1024 x float]* %window40, i32 0, i32 %add41
  %63 = load float, float* %arrayidx42, align 4
  %64 = load float**, float*** %buffer.addr, align 4
  %65 = load i32, i32* %chn.addr, align 4
  %arrayidx43 = getelementptr inbounds float*, float** %64, i32 %65
  %66 = load float*, float** %arrayidx43, align 4
  %67 = load i32, i32* %i, align 4
  %add44 = add nsw i32 %67, 1
  %arrayidx45 = getelementptr inbounds float, float* %66, i32 %add44
  %68 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %63, %68
  store float %mul46, float* %f0, align 4
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy47 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 22
  %70 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy47, align 4
  %window48 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %70, i32 0, i32 0
  %71 = load i32, i32* %i, align 4
  %add49 = add nsw i32 %71, 513
  %arrayidx50 = getelementptr inbounds [1024 x float], [1024 x float]* %window48, i32 0, i32 %add49
  %72 = load float, float* %arrayidx50, align 4
  %73 = load float**, float*** %buffer.addr, align 4
  %74 = load i32, i32* %chn.addr, align 4
  %arrayidx51 = getelementptr inbounds float*, float** %73, i32 %74
  %75 = load float*, float** %arrayidx51, align 4
  %76 = load i32, i32* %i, align 4
  %add52 = add nsw i32 %76, 513
  %arrayidx53 = getelementptr inbounds float, float* %75, i32 %add52
  %77 = load float, float* %arrayidx53, align 4
  %mul54 = fmul float %72, %77
  store float %mul54, float* %w, align 4
  %78 = load float, float* %f0, align 4
  %79 = load float, float* %w, align 4
  %sub55 = fsub float %78, %79
  store float %sub55, float* %f1, align 4
  %80 = load float, float* %f0, align 4
  %81 = load float, float* %w, align 4
  %add56 = fadd float %80, %81
  store float %add56, float* %f0, align 4
  %82 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy57 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %82, i32 0, i32 22
  %83 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy57, align 4
  %window58 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %83, i32 0, i32 0
  %84 = load i32, i32* %i, align 4
  %add59 = add nsw i32 %84, 257
  %arrayidx60 = getelementptr inbounds [1024 x float], [1024 x float]* %window58, i32 0, i32 %add59
  %85 = load float, float* %arrayidx60, align 4
  %86 = load float**, float*** %buffer.addr, align 4
  %87 = load i32, i32* %chn.addr, align 4
  %arrayidx61 = getelementptr inbounds float*, float** %86, i32 %87
  %88 = load float*, float** %arrayidx61, align 4
  %89 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %89, 257
  %arrayidx63 = getelementptr inbounds float, float* %88, i32 %add62
  %90 = load float, float* %arrayidx63, align 4
  %mul64 = fmul float %85, %90
  store float %mul64, float* %f2, align 4
  %91 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy65 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %91, i32 0, i32 22
  %92 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy65, align 4
  %window66 = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %92, i32 0, i32 0
  %93 = load i32, i32* %i, align 4
  %add67 = add nsw i32 %93, 769
  %arrayidx68 = getelementptr inbounds [1024 x float], [1024 x float]* %window66, i32 0, i32 %add67
  %94 = load float, float* %arrayidx68, align 4
  %95 = load float**, float*** %buffer.addr, align 4
  %96 = load i32, i32* %chn.addr, align 4
  %arrayidx69 = getelementptr inbounds float*, float** %95, i32 %96
  %97 = load float*, float** %arrayidx69, align 4
  %98 = load i32, i32* %i, align 4
  %add70 = add nsw i32 %98, 769
  %arrayidx71 = getelementptr inbounds float, float* %97, i32 %add70
  %99 = load float, float* %arrayidx71, align 4
  %mul72 = fmul float %94, %99
  store float %mul72, float* %w, align 4
  %100 = load float, float* %f2, align 4
  %101 = load float, float* %w, align 4
  %sub73 = fsub float %100, %101
  store float %sub73, float* %f3, align 4
  %102 = load float, float* %f2, align 4
  %103 = load float, float* %w, align 4
  %add74 = fadd float %102, %103
  store float %add74, float* %f2, align 4
  %104 = load float, float* %f0, align 4
  %105 = load float, float* %f2, align 4
  %add75 = fadd float %104, %105
  %106 = load float*, float** %x.addr, align 4
  %arrayidx76 = getelementptr inbounds float, float* %106, i32 512
  store float %add75, float* %arrayidx76, align 4
  %107 = load float, float* %f0, align 4
  %108 = load float, float* %f2, align 4
  %sub77 = fsub float %107, %108
  %109 = load float*, float** %x.addr, align 4
  %arrayidx78 = getelementptr inbounds float, float* %109, i32 514
  store float %sub77, float* %arrayidx78, align 4
  %110 = load float, float* %f1, align 4
  %111 = load float, float* %f3, align 4
  %add79 = fadd float %110, %111
  %112 = load float*, float** %x.addr, align 4
  %arrayidx80 = getelementptr inbounds float, float* %112, i32 513
  store float %add79, float* %arrayidx80, align 4
  %113 = load float, float* %f1, align 4
  %114 = load float, float* %f3, align 4
  %sub81 = fsub float %113, %114
  %115 = load float*, float** %x.addr, align 4
  %arrayidx82 = getelementptr inbounds float, float* %115, i32 515
  store float %sub81, float* %arrayidx82, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %116 = load i32, i32* %jj, align 4
  %dec = add nsw i32 %116, -1
  store i32 %dec, i32* %jj, align 4
  %cmp = icmp sge i32 %dec, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %117 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %fft_fht = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %117, i32 0, i32 26
  %118 = load void (float*, i32)*, void (float*, i32)** %fft_fht, align 4
  %119 = load float*, float** %x.addr, align 4
  call void %118(float* %119, i32 512)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @init_fft(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 1024
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %conv = sitofp i32 %1 to double
  %add = fadd double %conv, 5.000000e-01
  %mul = fmul double 0x401921FB54442D18, %add
  %div = fdiv double %mul, 1.024000e+03
  %2 = call double @llvm.cos.f64(double %div)
  %mul1 = fmul double 5.000000e-01, %2
  %sub = fsub double 4.200000e-01, %mul1
  %3 = load i32, i32* %i, align 4
  %conv2 = sitofp i32 %3 to double
  %add3 = fadd double %conv2, 5.000000e-01
  %mul4 = fmul double 0x402921FB54442D18, %add3
  %div5 = fdiv double %mul4, 1.024000e+03
  %4 = call double @llvm.cos.f64(double %div5)
  %mul6 = fmul double 8.000000e-02, %4
  %add7 = fadd double %sub, %mul6
  %conv8 = fptrunc double %add7 to float
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 22
  %6 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy, align 4
  %window = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %6, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [1024 x float], [1024 x float]* %window, i32 0, i32 %7
  store float %conv8, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc22, %for.end
  %9 = load i32, i32* %i, align 4
  %cmp10 = icmp slt i32 %9, 128
  br i1 %cmp10, label %for.body12, label %for.end24

for.body12:                                       ; preds = %for.cond9
  %10 = load i32, i32* %i, align 4
  %conv13 = sitofp i32 %10 to double
  %add14 = fadd double %conv13, 5.000000e-01
  %mul15 = fmul double 0x401921FB54442D18, %add14
  %div16 = fdiv double %mul15, 2.560000e+02
  %11 = call double @llvm.cos.f64(double %div16)
  %sub17 = fsub double 1.000000e+00, %11
  %mul18 = fmul double 5.000000e-01, %sub17
  %conv19 = fptrunc double %mul18 to float
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cd_psy20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %12, i32 0, i32 22
  %13 = load %struct.PsyConst_t*, %struct.PsyConst_t** %cd_psy20, align 4
  %window_s = getelementptr inbounds %struct.PsyConst_t, %struct.PsyConst_t* %13, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds [128 x float], [128 x float]* %window_s, i32 0, i32 %14
  store float %conv19, float* %arrayidx21, align 4
  br label %for.inc22

for.inc22:                                        ; preds = %for.body12
  %15 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %15, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond9

for.end24:                                        ; preds = %for.cond9
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %fft_fht = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %16, i32 0, i32 26
  store void (float*, i32)* @fht, void (float*, i32)** %fft_fht, align 4
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #1

; Function Attrs: noinline nounwind optnone
define internal void @fht(float* %fz, i32 %n) #0 {
entry:
  %fz.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %tri = alloca float*, align 4
  %k4 = alloca i32, align 4
  %fi = alloca float*, align 4
  %gi = alloca float*, align 4
  %fn = alloca float*, align 4
  %s1 = alloca float, align 4
  %c1 = alloca float, align 4
  %i = alloca i32, align 4
  %k1 = alloca i32, align 4
  %k2 = alloca i32, align 4
  %k3 = alloca i32, align 4
  %kx = alloca i32, align 4
  %f0 = alloca float, align 4
  %f1 = alloca float, align 4
  %f2 = alloca float, align 4
  %f3 = alloca float, align 4
  %c2 = alloca float, align 4
  %s2 = alloca float, align 4
  %a = alloca float, align 4
  %b = alloca float, align 4
  %g0 = alloca float, align 4
  %f059 = alloca float, align 4
  %f160 = alloca float, align 4
  %g1 = alloca float, align 4
  %f261 = alloca float, align 4
  %g2 = alloca float, align 4
  %f362 = alloca float, align 4
  %g3 = alloca float, align 4
  store float* %fz, float** %fz.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float* getelementptr inbounds ([8 x float], [8 x float]* @costab, i32 0, i32 0), float** %tri, align 4
  %0 = load i32, i32* %n.addr, align 4
  %shl = shl i32 %0, 1
  store i32 %shl, i32* %n.addr, align 4
  %1 = load float*, float** %fz.addr, align 4
  %2 = load i32, i32* %n.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %1, i32 %2
  store float* %add.ptr, float** %fn, align 4
  store i32 4, i32* %k4, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond144, %entry
  %3 = load i32, i32* %k4, align 4
  %shr = ashr i32 %3, 1
  store i32 %shr, i32* %kx, align 4
  %4 = load i32, i32* %k4, align 4
  store i32 %4, i32* %k1, align 4
  %5 = load i32, i32* %k4, align 4
  %shl1 = shl i32 %5, 1
  store i32 %shl1, i32* %k2, align 4
  %6 = load i32, i32* %k2, align 4
  %7 = load i32, i32* %k1, align 4
  %add = add nsw i32 %6, %7
  store i32 %add, i32* %k3, align 4
  %8 = load i32, i32* %k2, align 4
  %shl2 = shl i32 %8, 1
  store i32 %shl2, i32* %k4, align 4
  %9 = load float*, float** %fz.addr, align 4
  store float* %9, float** %fi, align 4
  %10 = load float*, float** %fi, align 4
  %11 = load i32, i32* %kx, align 4
  %add.ptr3 = getelementptr inbounds float, float* %10, i32 %11
  store float* %add.ptr3, float** %gi, align 4
  br label %do.body4

do.body4:                                         ; preds = %do.cond, %do.body
  %12 = load float*, float** %fi, align 4
  %arrayidx = getelementptr inbounds float, float* %12, i32 0
  %13 = load float, float* %arrayidx, align 4
  %14 = load float*, float** %fi, align 4
  %15 = load i32, i32* %k1, align 4
  %arrayidx5 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx5, align 4
  %sub = fsub float %13, %16
  store float %sub, float* %f1, align 4
  %17 = load float*, float** %fi, align 4
  %arrayidx6 = getelementptr inbounds float, float* %17, i32 0
  %18 = load float, float* %arrayidx6, align 4
  %19 = load float*, float** %fi, align 4
  %20 = load i32, i32* %k1, align 4
  %arrayidx7 = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %18, %21
  store float %add8, float* %f0, align 4
  %22 = load float*, float** %fi, align 4
  %23 = load i32, i32* %k2, align 4
  %arrayidx9 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx9, align 4
  %25 = load float*, float** %fi, align 4
  %26 = load i32, i32* %k3, align 4
  %arrayidx10 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx10, align 4
  %sub11 = fsub float %24, %27
  store float %sub11, float* %f3, align 4
  %28 = load float*, float** %fi, align 4
  %29 = load i32, i32* %k2, align 4
  %arrayidx12 = getelementptr inbounds float, float* %28, i32 %29
  %30 = load float, float* %arrayidx12, align 4
  %31 = load float*, float** %fi, align 4
  %32 = load i32, i32* %k3, align 4
  %arrayidx13 = getelementptr inbounds float, float* %31, i32 %32
  %33 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %30, %33
  store float %add14, float* %f2, align 4
  %34 = load float, float* %f0, align 4
  %35 = load float, float* %f2, align 4
  %sub15 = fsub float %34, %35
  %36 = load float*, float** %fi, align 4
  %37 = load i32, i32* %k2, align 4
  %arrayidx16 = getelementptr inbounds float, float* %36, i32 %37
  store float %sub15, float* %arrayidx16, align 4
  %38 = load float, float* %f0, align 4
  %39 = load float, float* %f2, align 4
  %add17 = fadd float %38, %39
  %40 = load float*, float** %fi, align 4
  %arrayidx18 = getelementptr inbounds float, float* %40, i32 0
  store float %add17, float* %arrayidx18, align 4
  %41 = load float, float* %f1, align 4
  %42 = load float, float* %f3, align 4
  %sub19 = fsub float %41, %42
  %43 = load float*, float** %fi, align 4
  %44 = load i32, i32* %k3, align 4
  %arrayidx20 = getelementptr inbounds float, float* %43, i32 %44
  store float %sub19, float* %arrayidx20, align 4
  %45 = load float, float* %f1, align 4
  %46 = load float, float* %f3, align 4
  %add21 = fadd float %45, %46
  %47 = load float*, float** %fi, align 4
  %48 = load i32, i32* %k1, align 4
  %arrayidx22 = getelementptr inbounds float, float* %47, i32 %48
  store float %add21, float* %arrayidx22, align 4
  %49 = load float*, float** %gi, align 4
  %arrayidx23 = getelementptr inbounds float, float* %49, i32 0
  %50 = load float, float* %arrayidx23, align 4
  %51 = load float*, float** %gi, align 4
  %52 = load i32, i32* %k1, align 4
  %arrayidx24 = getelementptr inbounds float, float* %51, i32 %52
  %53 = load float, float* %arrayidx24, align 4
  %sub25 = fsub float %50, %53
  store float %sub25, float* %f1, align 4
  %54 = load float*, float** %gi, align 4
  %arrayidx26 = getelementptr inbounds float, float* %54, i32 0
  %55 = load float, float* %arrayidx26, align 4
  %56 = load float*, float** %gi, align 4
  %57 = load i32, i32* %k1, align 4
  %arrayidx27 = getelementptr inbounds float, float* %56, i32 %57
  %58 = load float, float* %arrayidx27, align 4
  %add28 = fadd float %55, %58
  store float %add28, float* %f0, align 4
  %59 = load float*, float** %gi, align 4
  %60 = load i32, i32* %k3, align 4
  %arrayidx29 = getelementptr inbounds float, float* %59, i32 %60
  %61 = load float, float* %arrayidx29, align 4
  %conv = fpext float %61 to double
  %mul = fmul double 0x3FF6A09E667F3BCD, %conv
  %conv30 = fptrunc double %mul to float
  store float %conv30, float* %f3, align 4
  %62 = load float*, float** %gi, align 4
  %63 = load i32, i32* %k2, align 4
  %arrayidx31 = getelementptr inbounds float, float* %62, i32 %63
  %64 = load float, float* %arrayidx31, align 4
  %conv32 = fpext float %64 to double
  %mul33 = fmul double 0x3FF6A09E667F3BCD, %conv32
  %conv34 = fptrunc double %mul33 to float
  store float %conv34, float* %f2, align 4
  %65 = load float, float* %f0, align 4
  %66 = load float, float* %f2, align 4
  %sub35 = fsub float %65, %66
  %67 = load float*, float** %gi, align 4
  %68 = load i32, i32* %k2, align 4
  %arrayidx36 = getelementptr inbounds float, float* %67, i32 %68
  store float %sub35, float* %arrayidx36, align 4
  %69 = load float, float* %f0, align 4
  %70 = load float, float* %f2, align 4
  %add37 = fadd float %69, %70
  %71 = load float*, float** %gi, align 4
  %arrayidx38 = getelementptr inbounds float, float* %71, i32 0
  store float %add37, float* %arrayidx38, align 4
  %72 = load float, float* %f1, align 4
  %73 = load float, float* %f3, align 4
  %sub39 = fsub float %72, %73
  %74 = load float*, float** %gi, align 4
  %75 = load i32, i32* %k3, align 4
  %arrayidx40 = getelementptr inbounds float, float* %74, i32 %75
  store float %sub39, float* %arrayidx40, align 4
  %76 = load float, float* %f1, align 4
  %77 = load float, float* %f3, align 4
  %add41 = fadd float %76, %77
  %78 = load float*, float** %gi, align 4
  %79 = load i32, i32* %k1, align 4
  %arrayidx42 = getelementptr inbounds float, float* %78, i32 %79
  store float %add41, float* %arrayidx42, align 4
  %80 = load i32, i32* %k4, align 4
  %81 = load float*, float** %gi, align 4
  %add.ptr43 = getelementptr inbounds float, float* %81, i32 %80
  store float* %add.ptr43, float** %gi, align 4
  %82 = load i32, i32* %k4, align 4
  %83 = load float*, float** %fi, align 4
  %add.ptr44 = getelementptr inbounds float, float* %83, i32 %82
  store float* %add.ptr44, float** %fi, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body4
  %84 = load float*, float** %fi, align 4
  %85 = load float*, float** %fn, align 4
  %cmp = icmp ult float* %84, %85
  br i1 %cmp, label %do.body4, label %do.end

do.end:                                           ; preds = %do.cond
  %86 = load float*, float** %tri, align 4
  %arrayidx46 = getelementptr inbounds float, float* %86, i32 0
  %87 = load float, float* %arrayidx46, align 4
  store float %87, float* %c1, align 4
  %88 = load float*, float** %tri, align 4
  %arrayidx47 = getelementptr inbounds float, float* %88, i32 1
  %89 = load float, float* %arrayidx47, align 4
  store float %89, float* %s1, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %do.end
  %90 = load i32, i32* %i, align 4
  %91 = load i32, i32* %kx, align 4
  %cmp48 = icmp slt i32 %90, %91
  br i1 %cmp48, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %92 = load float, float* %s1, align 4
  %mul50 = fmul float 2.000000e+00, %92
  %93 = load float, float* %s1, align 4
  %mul51 = fmul float %mul50, %93
  %sub52 = fsub float 1.000000e+00, %mul51
  store float %sub52, float* %c2, align 4
  %94 = load float, float* %s1, align 4
  %mul53 = fmul float 2.000000e+00, %94
  %95 = load float, float* %c1, align 4
  %mul54 = fmul float %mul53, %95
  store float %mul54, float* %s2, align 4
  %96 = load float*, float** %fz.addr, align 4
  %97 = load i32, i32* %i, align 4
  %add.ptr55 = getelementptr inbounds float, float* %96, i32 %97
  store float* %add.ptr55, float** %fi, align 4
  %98 = load float*, float** %fz.addr, align 4
  %99 = load i32, i32* %k1, align 4
  %add.ptr56 = getelementptr inbounds float, float* %98, i32 %99
  %100 = load i32, i32* %i, align 4
  %idx.neg = sub i32 0, %100
  %add.ptr57 = getelementptr inbounds float, float* %add.ptr56, i32 %idx.neg
  store float* %add.ptr57, float** %gi, align 4
  br label %do.body58

do.body58:                                        ; preds = %do.cond129, %for.body
  %101 = load float, float* %s2, align 4
  %102 = load float*, float** %fi, align 4
  %103 = load i32, i32* %k1, align 4
  %arrayidx63 = getelementptr inbounds float, float* %102, i32 %103
  %104 = load float, float* %arrayidx63, align 4
  %mul64 = fmul float %101, %104
  %105 = load float, float* %c2, align 4
  %106 = load float*, float** %gi, align 4
  %107 = load i32, i32* %k1, align 4
  %arrayidx65 = getelementptr inbounds float, float* %106, i32 %107
  %108 = load float, float* %arrayidx65, align 4
  %mul66 = fmul float %105, %108
  %sub67 = fsub float %mul64, %mul66
  store float %sub67, float* %b, align 4
  %109 = load float, float* %c2, align 4
  %110 = load float*, float** %fi, align 4
  %111 = load i32, i32* %k1, align 4
  %arrayidx68 = getelementptr inbounds float, float* %110, i32 %111
  %112 = load float, float* %arrayidx68, align 4
  %mul69 = fmul float %109, %112
  %113 = load float, float* %s2, align 4
  %114 = load float*, float** %gi, align 4
  %115 = load i32, i32* %k1, align 4
  %arrayidx70 = getelementptr inbounds float, float* %114, i32 %115
  %116 = load float, float* %arrayidx70, align 4
  %mul71 = fmul float %113, %116
  %add72 = fadd float %mul69, %mul71
  store float %add72, float* %a, align 4
  %117 = load float*, float** %fi, align 4
  %arrayidx73 = getelementptr inbounds float, float* %117, i32 0
  %118 = load float, float* %arrayidx73, align 4
  %119 = load float, float* %a, align 4
  %sub74 = fsub float %118, %119
  store float %sub74, float* %f160, align 4
  %120 = load float*, float** %fi, align 4
  %arrayidx75 = getelementptr inbounds float, float* %120, i32 0
  %121 = load float, float* %arrayidx75, align 4
  %122 = load float, float* %a, align 4
  %add76 = fadd float %121, %122
  store float %add76, float* %f059, align 4
  %123 = load float*, float** %gi, align 4
  %arrayidx77 = getelementptr inbounds float, float* %123, i32 0
  %124 = load float, float* %arrayidx77, align 4
  %125 = load float, float* %b, align 4
  %sub78 = fsub float %124, %125
  store float %sub78, float* %g1, align 4
  %126 = load float*, float** %gi, align 4
  %arrayidx79 = getelementptr inbounds float, float* %126, i32 0
  %127 = load float, float* %arrayidx79, align 4
  %128 = load float, float* %b, align 4
  %add80 = fadd float %127, %128
  store float %add80, float* %g0, align 4
  %129 = load float, float* %s2, align 4
  %130 = load float*, float** %fi, align 4
  %131 = load i32, i32* %k3, align 4
  %arrayidx81 = getelementptr inbounds float, float* %130, i32 %131
  %132 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %129, %132
  %133 = load float, float* %c2, align 4
  %134 = load float*, float** %gi, align 4
  %135 = load i32, i32* %k3, align 4
  %arrayidx83 = getelementptr inbounds float, float* %134, i32 %135
  %136 = load float, float* %arrayidx83, align 4
  %mul84 = fmul float %133, %136
  %sub85 = fsub float %mul82, %mul84
  store float %sub85, float* %b, align 4
  %137 = load float, float* %c2, align 4
  %138 = load float*, float** %fi, align 4
  %139 = load i32, i32* %k3, align 4
  %arrayidx86 = getelementptr inbounds float, float* %138, i32 %139
  %140 = load float, float* %arrayidx86, align 4
  %mul87 = fmul float %137, %140
  %141 = load float, float* %s2, align 4
  %142 = load float*, float** %gi, align 4
  %143 = load i32, i32* %k3, align 4
  %arrayidx88 = getelementptr inbounds float, float* %142, i32 %143
  %144 = load float, float* %arrayidx88, align 4
  %mul89 = fmul float %141, %144
  %add90 = fadd float %mul87, %mul89
  store float %add90, float* %a, align 4
  %145 = load float*, float** %fi, align 4
  %146 = load i32, i32* %k2, align 4
  %arrayidx91 = getelementptr inbounds float, float* %145, i32 %146
  %147 = load float, float* %arrayidx91, align 4
  %148 = load float, float* %a, align 4
  %sub92 = fsub float %147, %148
  store float %sub92, float* %f362, align 4
  %149 = load float*, float** %fi, align 4
  %150 = load i32, i32* %k2, align 4
  %arrayidx93 = getelementptr inbounds float, float* %149, i32 %150
  %151 = load float, float* %arrayidx93, align 4
  %152 = load float, float* %a, align 4
  %add94 = fadd float %151, %152
  store float %add94, float* %f261, align 4
  %153 = load float*, float** %gi, align 4
  %154 = load i32, i32* %k2, align 4
  %arrayidx95 = getelementptr inbounds float, float* %153, i32 %154
  %155 = load float, float* %arrayidx95, align 4
  %156 = load float, float* %b, align 4
  %sub96 = fsub float %155, %156
  store float %sub96, float* %g3, align 4
  %157 = load float*, float** %gi, align 4
  %158 = load i32, i32* %k2, align 4
  %arrayidx97 = getelementptr inbounds float, float* %157, i32 %158
  %159 = load float, float* %arrayidx97, align 4
  %160 = load float, float* %b, align 4
  %add98 = fadd float %159, %160
  store float %add98, float* %g2, align 4
  %161 = load float, float* %s1, align 4
  %162 = load float, float* %f261, align 4
  %mul99 = fmul float %161, %162
  %163 = load float, float* %c1, align 4
  %164 = load float, float* %g3, align 4
  %mul100 = fmul float %163, %164
  %sub101 = fsub float %mul99, %mul100
  store float %sub101, float* %b, align 4
  %165 = load float, float* %c1, align 4
  %166 = load float, float* %f261, align 4
  %mul102 = fmul float %165, %166
  %167 = load float, float* %s1, align 4
  %168 = load float, float* %g3, align 4
  %mul103 = fmul float %167, %168
  %add104 = fadd float %mul102, %mul103
  store float %add104, float* %a, align 4
  %169 = load float, float* %f059, align 4
  %170 = load float, float* %a, align 4
  %sub105 = fsub float %169, %170
  %171 = load float*, float** %fi, align 4
  %172 = load i32, i32* %k2, align 4
  %arrayidx106 = getelementptr inbounds float, float* %171, i32 %172
  store float %sub105, float* %arrayidx106, align 4
  %173 = load float, float* %f059, align 4
  %174 = load float, float* %a, align 4
  %add107 = fadd float %173, %174
  %175 = load float*, float** %fi, align 4
  %arrayidx108 = getelementptr inbounds float, float* %175, i32 0
  store float %add107, float* %arrayidx108, align 4
  %176 = load float, float* %g1, align 4
  %177 = load float, float* %b, align 4
  %sub109 = fsub float %176, %177
  %178 = load float*, float** %gi, align 4
  %179 = load i32, i32* %k3, align 4
  %arrayidx110 = getelementptr inbounds float, float* %178, i32 %179
  store float %sub109, float* %arrayidx110, align 4
  %180 = load float, float* %g1, align 4
  %181 = load float, float* %b, align 4
  %add111 = fadd float %180, %181
  %182 = load float*, float** %gi, align 4
  %183 = load i32, i32* %k1, align 4
  %arrayidx112 = getelementptr inbounds float, float* %182, i32 %183
  store float %add111, float* %arrayidx112, align 4
  %184 = load float, float* %c1, align 4
  %185 = load float, float* %g2, align 4
  %mul113 = fmul float %184, %185
  %186 = load float, float* %s1, align 4
  %187 = load float, float* %f362, align 4
  %mul114 = fmul float %186, %187
  %sub115 = fsub float %mul113, %mul114
  store float %sub115, float* %b, align 4
  %188 = load float, float* %s1, align 4
  %189 = load float, float* %g2, align 4
  %mul116 = fmul float %188, %189
  %190 = load float, float* %c1, align 4
  %191 = load float, float* %f362, align 4
  %mul117 = fmul float %190, %191
  %add118 = fadd float %mul116, %mul117
  store float %add118, float* %a, align 4
  %192 = load float, float* %g0, align 4
  %193 = load float, float* %a, align 4
  %sub119 = fsub float %192, %193
  %194 = load float*, float** %gi, align 4
  %195 = load i32, i32* %k2, align 4
  %arrayidx120 = getelementptr inbounds float, float* %194, i32 %195
  store float %sub119, float* %arrayidx120, align 4
  %196 = load float, float* %g0, align 4
  %197 = load float, float* %a, align 4
  %add121 = fadd float %196, %197
  %198 = load float*, float** %gi, align 4
  %arrayidx122 = getelementptr inbounds float, float* %198, i32 0
  store float %add121, float* %arrayidx122, align 4
  %199 = load float, float* %f160, align 4
  %200 = load float, float* %b, align 4
  %sub123 = fsub float %199, %200
  %201 = load float*, float** %fi, align 4
  %202 = load i32, i32* %k3, align 4
  %arrayidx124 = getelementptr inbounds float, float* %201, i32 %202
  store float %sub123, float* %arrayidx124, align 4
  %203 = load float, float* %f160, align 4
  %204 = load float, float* %b, align 4
  %add125 = fadd float %203, %204
  %205 = load float*, float** %fi, align 4
  %206 = load i32, i32* %k1, align 4
  %arrayidx126 = getelementptr inbounds float, float* %205, i32 %206
  store float %add125, float* %arrayidx126, align 4
  %207 = load i32, i32* %k4, align 4
  %208 = load float*, float** %gi, align 4
  %add.ptr127 = getelementptr inbounds float, float* %208, i32 %207
  store float* %add.ptr127, float** %gi, align 4
  %209 = load i32, i32* %k4, align 4
  %210 = load float*, float** %fi, align 4
  %add.ptr128 = getelementptr inbounds float, float* %210, i32 %209
  store float* %add.ptr128, float** %fi, align 4
  br label %do.cond129

do.cond129:                                       ; preds = %do.body58
  %211 = load float*, float** %fi, align 4
  %212 = load float*, float** %fn, align 4
  %cmp130 = icmp ult float* %211, %212
  br i1 %cmp130, label %do.body58, label %do.end132

do.end132:                                        ; preds = %do.cond129
  %213 = load float, float* %c1, align 4
  store float %213, float* %c2, align 4
  %214 = load float, float* %c2, align 4
  %215 = load float*, float** %tri, align 4
  %arrayidx133 = getelementptr inbounds float, float* %215, i32 0
  %216 = load float, float* %arrayidx133, align 4
  %mul134 = fmul float %214, %216
  %217 = load float, float* %s1, align 4
  %218 = load float*, float** %tri, align 4
  %arrayidx135 = getelementptr inbounds float, float* %218, i32 1
  %219 = load float, float* %arrayidx135, align 4
  %mul136 = fmul float %217, %219
  %sub137 = fsub float %mul134, %mul136
  store float %sub137, float* %c1, align 4
  %220 = load float, float* %c2, align 4
  %221 = load float*, float** %tri, align 4
  %arrayidx138 = getelementptr inbounds float, float* %221, i32 1
  %222 = load float, float* %arrayidx138, align 4
  %mul139 = fmul float %220, %222
  %223 = load float, float* %s1, align 4
  %224 = load float*, float** %tri, align 4
  %arrayidx140 = getelementptr inbounds float, float* %224, i32 0
  %225 = load float, float* %arrayidx140, align 4
  %mul141 = fmul float %223, %225
  %add142 = fadd float %mul139, %mul141
  store float %add142, float* %s1, align 4
  br label %for.inc

for.inc:                                          ; preds = %do.end132
  %226 = load i32, i32* %i, align 4
  %inc = add nsw i32 %226, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %227 = load float*, float** %tri, align 4
  %add.ptr143 = getelementptr inbounds float, float* %227, i32 2
  store float* %add.ptr143, float** %tri, align 4
  br label %do.cond144

do.cond144:                                       ; preds = %for.end
  %228 = load i32, i32* %k4, align 4
  %229 = load i32, i32* %n.addr, align 4
  %cmp145 = icmp slt i32 %228, %229
  br i1 %cmp145, label %do.body, label %do.end147

do.end147:                                        ; preds = %do.cond144
  ret void
}

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
