; ModuleID = 'quantize.c'
source_filename = "quantize.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.III_psy_ratio = type { %struct.III_psy_xmin, %struct.III_psy_xmin }
%struct.calc_noise_result_t = type { float, float, float, i32, i32, i32 }
%struct.calc_noise_data_t = type { i32, i32, [39 x i32], [39 x float], [39 x float] }

@.str = private unnamed_addr constant [56 x i8] c"INTERNAL ERROR IN VBR NEW CODE, please send bug report\0A\00", align 1
@nr_of_sfb_block = external constant [6 x [3 x [4 x i32]]], align 16
@pretab = external constant [22 x i32], align 16
@ipow20 = external global [257 x float], align 16

; Function Attrs: noinline nounwind optnone
define hidden void @init_xrpow_core_init(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %init_xrpow_core = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 27
  store void (%struct.gr_info*, float*, i32, float*)* @init_xrpow_core_c, void (%struct.gr_info*, float*, i32, float*)** %init_xrpow_core, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @init_xrpow_core_c(%struct.gr_info* %cod_info, float* %xrpow, i32 %upper, float* %sum) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %xrpow.addr = alloca float*, align 4
  %upper.addr = alloca i32, align 4
  %sum.addr = alloca float*, align 4
  %i = alloca i32, align 4
  %tmp = alloca float, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store i32 %upper, i32* %upper.addr, align 4
  store float* %sum, float** %sum.addr, align 4
  %0 = load float*, float** %sum.addr, align 4
  store float 0.000000e+00, float* %0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %upper.addr, align 4
  %cmp = icmp sle i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %4
  %5 = load float, float* %arrayidx, align 4
  %conv = fpext float %5 to double
  %6 = call double @llvm.fabs.f64(double %conv)
  %conv1 = fptrunc double %6 to float
  store float %conv1, float* %tmp, align 4
  %7 = load float, float* %tmp, align 4
  %8 = load float*, float** %sum.addr, align 4
  %9 = load float, float* %8, align 4
  %add = fadd float %9, %7
  store float %add, float* %8, align 4
  %10 = load float, float* %tmp, align 4
  %conv2 = fpext float %10 to double
  %11 = load float, float* %tmp, align 4
  %conv3 = fpext float %11 to double
  %12 = call double @llvm.sqrt.f64(double %conv3)
  %mul = fmul double %conv2, %12
  %13 = call double @llvm.sqrt.f64(double %mul)
  %conv4 = fptrunc double %13 to float
  %14 = load float*, float** %xrpow.addr, align 4
  %15 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds float, float* %14, i32 %15
  store float %conv4, float* %arrayidx5, align 4
  %16 = load float*, float** %xrpow.addr, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds float, float* %16, i32 %17
  %18 = load float, float* %arrayidx6, align 4
  %19 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %19, i32 0, i32 3
  %20 = load float, float* %xrpow_max, align 4
  %cmp7 = fcmp ogt float %18, %20
  br i1 %cmp7, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %21 = load float*, float** %xrpow.addr, align 4
  %22 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds float, float* %21, i32 %22
  %23 = load float, float* %arrayidx9, align 4
  %24 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max10 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 3
  store float %23, float* %xrpow_max10, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @VBR_old_iteration_loop(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x %struct.III_psy_ratio]* %ratio) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %l3_xmin = alloca [2 x [2 x [39 x float]]], align 16
  %xrpow = alloca [576 x float], align 16
  %bands = alloca [2 x [2 x i32]], align 16
  %frameBits = alloca [15 x i32], align 16
  %used_bits = alloca i32, align 4
  %bits = alloca i32, align 4
  %min_bits = alloca [2 x [2 x i32]], align 16
  %max_bits = alloca [2 x [2 x i32]], align 16
  %mean_bits = alloca i32, align 4
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %analog_silence = alloca i32, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %ret = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %4 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %5 = load float*, float** %ms_ener_ratio.addr, align 4
  %6 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %arraydecay = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 0
  %arraydecay3 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 0
  %arraydecay4 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %min_bits, i32 0, i32 0
  %arraydecay5 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 0
  %arraydecay6 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %bands, i32 0, i32 0
  %call = call i32 @VBR_old_prepare(%struct.lame_internal_flags* %3, [2 x float]* %4, float* %5, [2 x %struct.III_psy_ratio]* %6, [2 x [39 x float]]* %arraydecay, i32* %arraydecay3, [2 x i32]* %arraydecay4, [2 x i32]* %arraydecay5, [2 x i32]* %arraydecay6)
  store i32 %call, i32* %analog_silence, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end60, %entry
  store i32 0, i32* %used_bits, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc36, %for.cond
  %7 = load i32, i32* %gr, align 4
  %8 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %8, i32 0, i32 15
  %9 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %7, %9
  br i1 %cmp, label %for.body, label %for.end38

for.body:                                         ; preds = %for.cond7
  store i32 0, i32* %ch, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %ch, align 4
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 14
  %12 = load i32, i32* %channels_out, align 4
  %cmp9 = icmp slt i32 %10, %12
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %13 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %13, i32 0, i32 0
  %14 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %14
  %15 = load i32, i32* %ch, align 4
  %arrayidx11 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %15
  store %struct.gr_info* %arrayidx11, %struct.gr_info** %cod_info, align 4
  %16 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay12 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %call13 = call i32 @init_xrpow(%struct.lame_internal_flags* %16, %struct.gr_info* %17, float* %arraydecay12)
  store i32 %call13, i32* %ret, align 4
  %18 = load i32, i32* %ret, align 4
  %cmp14 = icmp eq i32 %18, 0
  br i1 %cmp14, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body10
  %19 = load i32, i32* %gr, align 4
  %arrayidx15 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 %19
  %20 = load i32, i32* %ch, align 4
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx15, i32 0, i32 %20
  %21 = load i32, i32* %arrayidx16, align 4
  %cmp17 = icmp eq i32 %21, 0
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body10
  br label %for.inc

if.end:                                           ; preds = %lor.lhs.false
  %22 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %23 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %24 = load i32, i32* %gr, align 4
  %arrayidx18 = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 %24
  %25 = load i32, i32* %ch, align 4
  %arrayidx19 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx18, i32 0, i32 %25
  %arraydecay20 = getelementptr inbounds [39 x float], [39 x float]* %arrayidx19, i32 0, i32 0
  %arraydecay21 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %26 = load i32, i32* %ch, align 4
  %27 = load i32, i32* %gr, align 4
  %arrayidx22 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %min_bits, i32 0, i32 %27
  %28 = load i32, i32* %ch, align 4
  %arrayidx23 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx22, i32 0, i32 %28
  %29 = load i32, i32* %arrayidx23, align 4
  %30 = load i32, i32* %gr, align 4
  %arrayidx24 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 %30
  %31 = load i32, i32* %ch, align 4
  %arrayidx25 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx24, i32 0, i32 %31
  %32 = load i32, i32* %arrayidx25, align 4
  call void @VBR_encode_granule(%struct.lame_internal_flags* %22, %struct.gr_info* %23, float* %arraydecay20, float* %arraydecay21, i32 %26, i32 %29, i32 %32)
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %34 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %34, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then26, label %if.end34

if.then26:                                        ; preds = %if.end
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %36 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt27 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %36, i32 0, i32 0
  %37 = load i32, i32* %gr, align 4
  %arrayidx28 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt27, i32 0, i32 %37
  %38 = load i32, i32* %ch, align 4
  %arrayidx29 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx28, i32 0, i32 %38
  %39 = load i32, i32* %gr, align 4
  %arrayidx30 = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 %39
  %40 = load i32, i32* %ch, align 4
  %arrayidx31 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx30, i32 0, i32 %40
  %arraydecay32 = getelementptr inbounds [39 x float], [39 x float]* %arrayidx31, i32 0, i32 0
  %arraydecay33 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  call void @trancate_smallspectrums(%struct.lame_internal_flags* %35, %struct.gr_info* %arrayidx29, float* %arraydecay32, float* %arraydecay33)
  br label %if.end34

if.end34:                                         ; preds = %if.then26, %if.end
  %41 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %41, i32 0, i32 4
  %42 = load i32, i32* %part2_3_length, align 4
  %43 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %43, i32 0, i32 18
  %44 = load i32, i32* %part2_length, align 4
  %add = add nsw i32 %42, %44
  store i32 %add, i32* %ret, align 4
  %45 = load i32, i32* %ret, align 4
  %46 = load i32, i32* %used_bits, align 4
  %add35 = add nsw i32 %46, %45
  store i32 %add35, i32* %used_bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end34, %if.then
  %47 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %47, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %for.inc36

for.inc36:                                        ; preds = %for.end
  %48 = load i32, i32* %gr, align 4
  %inc37 = add nsw i32 %48, 1
  store i32 %inc37, i32* %gr, align 4
  br label %for.cond7

for.end38:                                        ; preds = %for.cond7
  %49 = load i32, i32* %analog_silence, align 4
  %tobool39 = icmp ne i32 %49, 0
  br i1 %tobool39, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %for.end38
  %50 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %enforce_min_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %50, i32 0, i32 27
  %51 = load i32, i32* %enforce_min_bitrate, align 4
  %tobool40 = icmp ne i32 %51, 0
  br i1 %tobool40, label %if.else, label %if.then41

if.then41:                                        ; preds = %land.lhs.true
  %52 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %52, i32 0, i32 2
  store i32 1, i32* %bitrate_index, align 4
  br label %if.end43

if.else:                                          ; preds = %land.lhs.true, %for.end38
  %53 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %53, i32 0, i32 24
  %54 = load i32, i32* %vbr_min_bitrate_index, align 4
  %55 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index42 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %55, i32 0, i32 2
  store i32 %54, i32* %bitrate_index42, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.else, %if.then41
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc53, %if.end43
  %56 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index45 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %56, i32 0, i32 2
  %57 = load i32, i32* %bitrate_index45, align 4
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 25
  %59 = load i32, i32* %vbr_max_bitrate_index, align 4
  %cmp46 = icmp slt i32 %57, %59
  br i1 %cmp46, label %for.body47, label %for.end56

for.body47:                                       ; preds = %for.cond44
  %60 = load i32, i32* %used_bits, align 4
  %61 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index48 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %61, i32 0, i32 2
  %62 = load i32, i32* %bitrate_index48, align 4
  %arrayidx49 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 %62
  %63 = load i32, i32* %arrayidx49, align 4
  %cmp50 = icmp sle i32 %60, %63
  br i1 %cmp50, label %if.then51, label %if.end52

if.then51:                                        ; preds = %for.body47
  br label %for.end56

if.end52:                                         ; preds = %for.body47
  br label %for.inc53

for.inc53:                                        ; preds = %if.end52
  %64 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index54 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %64, i32 0, i32 2
  %65 = load i32, i32* %bitrate_index54, align 4
  %inc55 = add nsw i32 %65, 1
  store i32 %inc55, i32* %bitrate_index54, align 4
  br label %for.cond44

for.end56:                                        ; preds = %if.then51, %for.cond44
  %66 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call57 = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %66, i32* %mean_bits)
  store i32 %call57, i32* %bits, align 4
  %67 = load i32, i32* %used_bits, align 4
  %68 = load i32, i32* %bits, align 4
  %cmp58 = icmp sle i32 %67, %68
  br i1 %cmp58, label %if.then59, label %if.end60

if.then59:                                        ; preds = %for.end56
  br label %for.end64

if.end60:                                         ; preds = %for.end56
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay61 = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 0
  %arraydecay62 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %min_bits, i32 0, i32 0
  %arraydecay63 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 0
  call void @bitpressure_strategy(%struct.lame_internal_flags* %69, [2 x [39 x float]]* %arraydecay61, [2 x i32]* %arraydecay62, [2 x i32]* %arraydecay63)
  br label %for.cond

for.end64:                                        ; preds = %if.then59
  store i32 0, i32* %gr, align 4
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc76, %for.end64
  %70 = load i32, i32* %gr, align 4
  %71 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr66 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %71, i32 0, i32 15
  %72 = load i32, i32* %mode_gr66, align 4
  %cmp67 = icmp slt i32 %70, %72
  br i1 %cmp67, label %for.body68, label %for.end78

for.body68:                                       ; preds = %for.cond65
  store i32 0, i32* %ch, align 4
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc73, %for.body68
  %73 = load i32, i32* %ch, align 4
  %74 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out70 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %74, i32 0, i32 14
  %75 = load i32, i32* %channels_out70, align 4
  %cmp71 = icmp slt i32 %73, %75
  br i1 %cmp71, label %for.body72, label %for.end75

for.body72:                                       ; preds = %for.cond69
  %76 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %77 = load i32, i32* %gr, align 4
  %78 = load i32, i32* %ch, align 4
  call void @iteration_finish_one(%struct.lame_internal_flags* %76, i32 %77, i32 %78)
  br label %for.inc73

for.inc73:                                        ; preds = %for.body72
  %79 = load i32, i32* %ch, align 4
  %inc74 = add nsw i32 %79, 1
  store i32 %inc74, i32* %ch, align 4
  br label %for.cond69

for.end75:                                        ; preds = %for.cond69
  br label %for.inc76

for.inc76:                                        ; preds = %for.end75
  %80 = load i32, i32* %gr, align 4
  %inc77 = add nsw i32 %80, 1
  store i32 %inc77, i32* %gr, align 4
  br label %for.cond65

for.end78:                                        ; preds = %for.cond65
  %81 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %82 = load i32, i32* %mean_bits, align 4
  call void @ResvFrameEnd(%struct.lame_internal_flags* %81, i32 %82)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @VBR_old_prepare(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x %struct.III_psy_ratio]* %ratio, [2 x [39 x float]]* %l3_xmin, i32* %frameBits, [2 x i32]* %min_bits, [2 x i32]* %max_bits, [2 x i32]* %bands) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %l3_xmin.addr = alloca [2 x [39 x float]]*, align 4
  %frameBits.addr = alloca i32*, align 4
  %min_bits.addr = alloca [2 x i32]*, align 4
  %max_bits.addr = alloca [2 x i32]*, align 4
  %bands.addr = alloca [2 x i32]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %masking_lower_db = alloca float, align 4
  %adjust = alloca float, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %analog_silence = alloca i32, align 4
  %avg = alloca i32, align 4
  %mxb = alloca i32, align 4
  %bits = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  store [2 x [39 x float]]* %l3_xmin, [2 x [39 x float]]** %l3_xmin.addr, align 4
  store i32* %frameBits, i32** %frameBits.addr, align 4
  store [2 x i32]* %min_bits, [2 x i32]** %min_bits.addr, align 4
  store [2 x i32]* %max_bits, [2 x i32]** %max_bits.addr, align 4
  store [2 x i32]* %bands, [2 x i32]** %bands.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  store float 0.000000e+00, float* %adjust, align 4
  store i32 1, i32* %analog_silence, align 4
  store i32 0, i32* %bits, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 25
  %3 = load i32, i32* %vbr_max_bitrate_index, align 4
  %4 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %4, i32 0, i32 2
  store i32 %3, i32* %bitrate_index, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %5, i32* %avg)
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 15
  %7 = load i32, i32* %mode_gr, align 4
  %div = sdiv i32 %call, %7
  store i32 %div, i32* %avg, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %9 = load i32*, i32** %frameBits.addr, align 4
  call void @get_framebits(%struct.lame_internal_flags* %8, i32* %9)
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc56, %entry
  %10 = load i32, i32* %gr, align 4
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 15
  %12 = load i32, i32* %mode_gr2, align 4
  %cmp = icmp slt i32 %10, %12
  br i1 %cmp, label %for.body, label %for.end58

for.body:                                         ; preds = %for.cond
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %14 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %15 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %16 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %15, i32 %16
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 0
  %17 = load i32, i32* %avg, align 4
  %18 = load i32, i32* %gr, align 4
  %call3 = call i32 @on_pe(%struct.lame_internal_flags* %13, [2 x float]* %14, i32* %arraydecay, i32 %17, i32 %18, i32 0)
  store i32 %call3, i32* %mxb, align 4
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc4, i32 0, i32 5
  %20 = load i32, i32* %mode_ext, align 4
  %cmp5 = icmp eq i32 %20, 2
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 7
  %22 = load i32, i32* %gr, align 4
  call void @ms_convert(%struct.III_side_info_t* %l3_side, i32 %22)
  %23 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %24 = load i32, i32* %gr, align 4
  %arrayidx6 = getelementptr inbounds [2 x i32], [2 x i32]* %23, i32 %24
  %arraydecay7 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx6, i32 0, i32 0
  %25 = load float*, float** %ms_ener_ratio.addr, align 4
  %26 = load i32, i32* %gr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %25, i32 %26
  %27 = load float, float* %arrayidx8, align 4
  %28 = load i32, i32* %avg, align 4
  %29 = load i32, i32* %mxb, align 4
  call void @reduce_side(i32* %arraydecay7, float %27, i32 %28, i32 %29)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  store i32 0, i32* %ch, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %if.end
  %30 = load i32, i32* %ch, align 4
  %31 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %31, i32 0, i32 14
  %32 = load i32, i32* %channels_out, align 4
  %cmp10 = icmp slt i32 %30, %32
  br i1 %cmp10, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond9
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side12, i32 0, i32 0
  %34 = load i32, i32* %gr, align 4
  %arrayidx13 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %34
  %35 = load i32, i32* %ch, align 4
  %arrayidx14 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx13, i32 0, i32 %35
  store %struct.gr_info* %arrayidx14, %struct.gr_info** %cod_info, align 4
  %36 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %36, i32 0, i32 9
  %37 = load i32, i32* %block_type, align 4
  %cmp15 = icmp ne i32 %37, 2
  br i1 %cmp15, label %if.then16, label %if.else

if.then16:                                        ; preds = %for.body11
  %38 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %39 = load i32, i32* %gr, align 4
  %arrayidx17 = getelementptr inbounds [2 x float], [2 x float]* %38, i32 %39
  %40 = load i32, i32* %ch, align 4
  %arrayidx18 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx17, i32 0, i32 %40
  %41 = load float, float* %arrayidx18, align 4
  %conv = fpext float %41 to double
  %div19 = fdiv double %conv, 3.000000e+02
  %sub = fsub double 3.500000e+00, %div19
  %42 = call double @llvm.exp.f64(double %sub)
  %add = fadd double 1.000000e+00, %42
  %div20 = fdiv double 1.280000e+00, %add
  %sub21 = fsub double %div20, 5.000000e-02
  %conv22 = fptrunc double %sub21 to float
  store float %conv22, float* %adjust, align 4
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %43, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 3
  %44 = load float, float* %mask_adjust, align 8
  %45 = load float, float* %adjust, align 4
  %sub23 = fsub float %44, %45
  store float %sub23, float* %masking_lower_db, align 4
  br label %if.end35

if.else:                                          ; preds = %for.body11
  %46 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %47 = load i32, i32* %gr, align 4
  %arrayidx24 = getelementptr inbounds [2 x float], [2 x float]* %46, i32 %47
  %48 = load i32, i32* %ch, align 4
  %arrayidx25 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx24, i32 0, i32 %48
  %49 = load float, float* %arrayidx25, align 4
  %conv26 = fpext float %49 to double
  %div27 = fdiv double %conv26, 3.000000e+02
  %sub28 = fsub double 3.500000e+00, %div27
  %50 = call double @llvm.exp.f64(double %sub28)
  %add29 = fadd double 1.000000e+00, %50
  %div30 = fdiv double 2.560000e+00, %add29
  %sub31 = fsub double %div30, 1.400000e-01
  %conv32 = fptrunc double %sub31 to float
  store float %conv32, float* %adjust, align 4
  %51 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt33 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %51, i32 0, i32 13
  %mask_adjust_short = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt33, i32 0, i32 4
  %52 = load float, float* %mask_adjust_short, align 4
  %53 = load float, float* %adjust, align 4
  %sub34 = fsub float %52, %53
  store float %sub34, float* %masking_lower_db, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.else, %if.then16
  %54 = load float, float* %masking_lower_db, align 4
  %conv36 = fpext float %54 to double
  %mul = fmul double %conv36, 1.000000e-01
  %55 = call double @llvm.pow.f64(double 1.000000e+01, double %mul)
  %conv37 = fptrunc double %55 to float
  %56 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt38 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %56, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt38, i32 0, i32 2
  store float %conv37, float* %masking_lower, align 4
  %57 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %58 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @init_outer_loop(%struct.lame_internal_flags* %57, %struct.gr_info* %58)
  %59 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %60 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %61 = load i32, i32* %gr, align 4
  %arrayidx39 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %60, i32 %61
  %62 = load i32, i32* %ch, align 4
  %arrayidx40 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx39, i32 0, i32 %62
  %63 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %64 = load [2 x [39 x float]]*, [2 x [39 x float]]** %l3_xmin.addr, align 4
  %65 = load i32, i32* %gr, align 4
  %arrayidx41 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %64, i32 %65
  %66 = load i32, i32* %ch, align 4
  %arrayidx42 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx41, i32 0, i32 %66
  %arraydecay43 = getelementptr inbounds [39 x float], [39 x float]* %arrayidx42, i32 0, i32 0
  %call44 = call i32 @calc_xmin(%struct.lame_internal_flags* %59, %struct.III_psy_ratio* %arrayidx40, %struct.gr_info* %63, float* %arraydecay43)
  %67 = load [2 x i32]*, [2 x i32]** %bands.addr, align 4
  %68 = load i32, i32* %gr, align 4
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %67, i32 %68
  %69 = load i32, i32* %ch, align 4
  %arrayidx46 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx45, i32 0, i32 %69
  store i32 %call44, i32* %arrayidx46, align 4
  %70 = load [2 x i32]*, [2 x i32]** %bands.addr, align 4
  %71 = load i32, i32* %gr, align 4
  %arrayidx47 = getelementptr inbounds [2 x i32], [2 x i32]* %70, i32 %71
  %72 = load i32, i32* %ch, align 4
  %arrayidx48 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx47, i32 0, i32 %72
  %73 = load i32, i32* %arrayidx48, align 4
  %tobool = icmp ne i32 %73, 0
  br i1 %tobool, label %if.then49, label %if.end50

if.then49:                                        ; preds = %if.end35
  store i32 0, i32* %analog_silence, align 4
  br label %if.end50

if.end50:                                         ; preds = %if.then49, %if.end35
  %74 = load [2 x i32]*, [2 x i32]** %min_bits.addr, align 4
  %75 = load i32, i32* %gr, align 4
  %arrayidx51 = getelementptr inbounds [2 x i32], [2 x i32]* %74, i32 %75
  %76 = load i32, i32* %ch, align 4
  %arrayidx52 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx51, i32 0, i32 %76
  store i32 126, i32* %arrayidx52, align 4
  %77 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %78 = load i32, i32* %gr, align 4
  %arrayidx53 = getelementptr inbounds [2 x i32], [2 x i32]* %77, i32 %78
  %79 = load i32, i32* %ch, align 4
  %arrayidx54 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx53, i32 0, i32 %79
  %80 = load i32, i32* %arrayidx54, align 4
  %81 = load i32, i32* %bits, align 4
  %add55 = add nsw i32 %81, %80
  store i32 %add55, i32* %bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end50
  %82 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %82, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  br label %for.inc56

for.inc56:                                        ; preds = %for.end
  %83 = load i32, i32* %gr, align 4
  %inc57 = add nsw i32 %83, 1
  store i32 %inc57, i32* %gr, align 4
  br label %for.cond

for.end58:                                        ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc100, %for.end58
  %84 = load i32, i32* %gr, align 4
  %85 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr60 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %85, i32 0, i32 15
  %86 = load i32, i32* %mode_gr60, align 4
  %cmp61 = icmp slt i32 %84, %86
  br i1 %cmp61, label %for.body63, label %for.end102

for.body63:                                       ; preds = %for.cond59
  store i32 0, i32* %ch, align 4
  br label %for.cond64

for.cond64:                                       ; preds = %for.inc97, %for.body63
  %87 = load i32, i32* %ch, align 4
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out65 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 14
  %89 = load i32, i32* %channels_out65, align 4
  %cmp66 = icmp slt i32 %87, %89
  br i1 %cmp66, label %for.body68, label %for.end99

for.body68:                                       ; preds = %for.cond64
  %90 = load i32, i32* %bits, align 4
  %91 = load i32*, i32** %frameBits.addr, align 4
  %92 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index69 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %92, i32 0, i32 25
  %93 = load i32, i32* %vbr_max_bitrate_index69, align 4
  %arrayidx70 = getelementptr inbounds i32, i32* %91, i32 %93
  %94 = load i32, i32* %arrayidx70, align 4
  %cmp71 = icmp sgt i32 %90, %94
  br i1 %cmp71, label %land.lhs.true, label %if.end84

land.lhs.true:                                    ; preds = %for.body68
  %95 = load i32, i32* %bits, align 4
  %cmp73 = icmp sgt i32 %95, 0
  br i1 %cmp73, label %if.then75, label %if.end84

if.then75:                                        ; preds = %land.lhs.true
  %96 = load i32*, i32** %frameBits.addr, align 4
  %97 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index76 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %97, i32 0, i32 25
  %98 = load i32, i32* %vbr_max_bitrate_index76, align 4
  %arrayidx77 = getelementptr inbounds i32, i32* %96, i32 %98
  %99 = load i32, i32* %arrayidx77, align 4
  %100 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %101 = load i32, i32* %gr, align 4
  %arrayidx78 = getelementptr inbounds [2 x i32], [2 x i32]* %100, i32 %101
  %102 = load i32, i32* %ch, align 4
  %arrayidx79 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx78, i32 0, i32 %102
  %103 = load i32, i32* %arrayidx79, align 4
  %mul80 = mul nsw i32 %103, %99
  store i32 %mul80, i32* %arrayidx79, align 4
  %104 = load i32, i32* %bits, align 4
  %105 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %106 = load i32, i32* %gr, align 4
  %arrayidx81 = getelementptr inbounds [2 x i32], [2 x i32]* %105, i32 %106
  %107 = load i32, i32* %ch, align 4
  %arrayidx82 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx81, i32 0, i32 %107
  %108 = load i32, i32* %arrayidx82, align 4
  %div83 = sdiv i32 %108, %104
  store i32 %div83, i32* %arrayidx82, align 4
  br label %if.end84

if.end84:                                         ; preds = %if.then75, %land.lhs.true, %for.body68
  %109 = load [2 x i32]*, [2 x i32]** %min_bits.addr, align 4
  %110 = load i32, i32* %gr, align 4
  %arrayidx85 = getelementptr inbounds [2 x i32], [2 x i32]* %109, i32 %110
  %111 = load i32, i32* %ch, align 4
  %arrayidx86 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx85, i32 0, i32 %111
  %112 = load i32, i32* %arrayidx86, align 4
  %113 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %114 = load i32, i32* %gr, align 4
  %arrayidx87 = getelementptr inbounds [2 x i32], [2 x i32]* %113, i32 %114
  %115 = load i32, i32* %ch, align 4
  %arrayidx88 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx87, i32 0, i32 %115
  %116 = load i32, i32* %arrayidx88, align 4
  %cmp89 = icmp sgt i32 %112, %116
  br i1 %cmp89, label %if.then91, label %if.end96

if.then91:                                        ; preds = %if.end84
  %117 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %118 = load i32, i32* %gr, align 4
  %arrayidx92 = getelementptr inbounds [2 x i32], [2 x i32]* %117, i32 %118
  %119 = load i32, i32* %ch, align 4
  %arrayidx93 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx92, i32 0, i32 %119
  %120 = load i32, i32* %arrayidx93, align 4
  %121 = load [2 x i32]*, [2 x i32]** %min_bits.addr, align 4
  %122 = load i32, i32* %gr, align 4
  %arrayidx94 = getelementptr inbounds [2 x i32], [2 x i32]* %121, i32 %122
  %123 = load i32, i32* %ch, align 4
  %arrayidx95 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx94, i32 0, i32 %123
  store i32 %120, i32* %arrayidx95, align 4
  br label %if.end96

if.end96:                                         ; preds = %if.then91, %if.end84
  br label %for.inc97

for.inc97:                                        ; preds = %if.end96
  %124 = load i32, i32* %ch, align 4
  %inc98 = add nsw i32 %124, 1
  store i32 %inc98, i32* %ch, align 4
  br label %for.cond64

for.end99:                                        ; preds = %for.cond64
  br label %for.inc100

for.inc100:                                       ; preds = %for.end99
  %125 = load i32, i32* %gr, align 4
  %inc101 = add nsw i32 %125, 1
  store i32 %inc101, i32* %gr, align 4
  br label %for.cond59

for.end102:                                       ; preds = %for.cond59
  %126 = load i32, i32* %analog_silence, align 4
  ret i32 %126
}

; Function Attrs: noinline nounwind optnone
define internal i32 @init_xrpow(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %xrpow) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %xrpow.addr = alloca float*, align 4
  %sum = alloca float, align 4
  %i = alloca i32, align 4
  %upper = alloca i32, align 4
  %j = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store float 0.000000e+00, float* %sum, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 30
  %1 = load i32, i32* %max_nonzero_coeff, align 4
  store i32 %1, i32* %upper, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 3
  store float 0.000000e+00, float* %xrpow_max, align 4
  %3 = load float*, float** %xrpow.addr, align 4
  %4 = load i32, i32* %upper, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = load i32, i32* %upper, align 4
  %sub = sub nsw i32 576, %6
  %mul = mul i32 %sub, 4
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 %mul, i1 false)
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %init_xrpow_core = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 27
  %8 = load void (%struct.gr_info*, float*, i32, float*)*, void (%struct.gr_info*, float*, i32, float*)** %init_xrpow_core, align 8
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %10 = load float*, float** %xrpow.addr, align 4
  %11 = load i32, i32* %upper, align 4
  call void %8(%struct.gr_info* %9, float* %10, i32 %11, float* %sum)
  %12 = load float, float* %sum, align 4
  %cmp = fcmp ogt float %12, 0x3BC79CA100000000
  br i1 %cmp, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  store i32 0, i32* %j, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %14 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %14, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then1, label %if.end

if.then1:                                         ; preds = %if.then
  store i32 1, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then1, %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %15 = load i32, i32* %i, align 4
  %16 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 23
  %17 = load i32, i32* %psymax, align 4
  %cmp2 = icmp slt i32 %15, %17
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load i32, i32* %j, align 4
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 13
  %pseudohalf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt3, i32 0, i32 7
  %20 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf, i32 0, i32 %20
  store i32 %18, i32* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %entry
  %22 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %22, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  %23 = bitcast i32* %arrayidx6 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %23, i8 0, i32 2304, i1 false)
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end5, %for.end
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define internal void @VBR_encode_granule(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %l3_xmin, float* %xrpow, i32 %ch, i32 %min_bits, i32 %max_bits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %l3_xmin.addr = alloca float*, align 4
  %xrpow.addr = alloca float*, align 4
  %ch.addr = alloca i32, align 4
  %min_bits.addr = alloca i32, align 4
  %max_bits.addr = alloca i32, align 4
  %bst_cod_info = alloca %struct.gr_info, align 4
  %bst_xrpow = alloca [576 x float], align 16
  %Max_bits = alloca i32, align 4
  %real_bits = alloca i32, align 4
  %this_bits = alloca i32, align 4
  %dbits = alloca i32, align 4
  %over = alloca i32, align 4
  %found = alloca i32, align 4
  %sfb21_extra = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %l3_xmin, float** %l3_xmin.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  store i32 %min_bits, i32* %min_bits.addr, align 4
  store i32 %max_bits, i32* %max_bits.addr, align 4
  %0 = load i32, i32* %max_bits.addr, align 4
  store i32 %0, i32* %Max_bits, align 4
  %1 = load i32, i32* %max_bits.addr, align 4
  %add = add nsw i32 %1, 1
  store i32 %add, i32* %real_bits, align 4
  %2 = load i32, i32* %max_bits.addr, align 4
  %3 = load i32, i32* %min_bits.addr, align 4
  %add1 = add nsw i32 %2, %3
  %div = sdiv i32 %add1, 2
  store i32 %div, i32* %this_bits, align 4
  store i32 0, i32* %found, align 4
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 13
  %sfb21_extra2 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 8
  %5 = load i32, i32* %sfb21_extra2, align 4
  store i32 %5, i32* %sfb21_extra, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %bst_cod_info, i32 0, i32 1
  %arraydecay = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 0
  %6 = bitcast i32* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %6, i8 0, i32 2304, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  %7 = load i32, i32* %this_bits, align 4
  %8 = load i32, i32* %Max_bits, align 4
  %sub = sub nsw i32 %8, 42
  %cmp = icmp sgt i32 %7, %sub
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %do.body
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 13
  %sfb21_extra4 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt3, i32 0, i32 8
  store i32 0, i32* %sfb21_extra4, align 4
  br label %if.end

if.else:                                          ; preds = %do.body
  %10 = load i32, i32* %sfb21_extra, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 13
  %sfb21_extra6 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt5, i32 0, i32 8
  store i32 %10, i32* %sfb21_extra6, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %12 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %14 = load float*, float** %l3_xmin.addr, align 4
  %15 = load float*, float** %xrpow.addr, align 4
  %16 = load i32, i32* %ch.addr, align 4
  %17 = load i32, i32* %this_bits, align 4
  %call = call i32 @outer_loop(%struct.lame_internal_flags* %12, %struct.gr_info* %13, float* %14, float* %15, i32 %16, i32 %17)
  store i32 %call, i32* %over, align 4
  %18 = load i32, i32* %over, align 4
  %cmp7 = icmp sle i32 %18, 0
  br i1 %cmp7, label %if.then8, label %if.else14

if.then8:                                         ; preds = %if.end
  store i32 1, i32* %found, align 4
  %19 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %19, i32 0, i32 4
  %20 = load i32, i32* %part2_3_length, align 4
  store i32 %20, i32* %real_bits, align 4
  %21 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %22 = bitcast %struct.gr_info* %bst_cod_info to i8*
  %23 = bitcast %struct.gr_info* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 5252, i1 false)
  %arraydecay9 = getelementptr inbounds [576 x float], [576 x float]* %bst_xrpow, i32 0, i32 0
  %24 = bitcast float* %arraydecay9 to i8*
  %25 = load float*, float** %xrpow.addr, align 4
  %26 = bitcast float* %25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %24, i8* align 4 %26, i32 2304, i1 false)
  %27 = load i32, i32* %real_bits, align 4
  %sub10 = sub nsw i32 %27, 32
  store i32 %sub10, i32* %max_bits.addr, align 4
  %28 = load i32, i32* %max_bits.addr, align 4
  %29 = load i32, i32* %min_bits.addr, align 4
  %sub11 = sub nsw i32 %28, %29
  store i32 %sub11, i32* %dbits, align 4
  %30 = load i32, i32* %max_bits.addr, align 4
  %31 = load i32, i32* %min_bits.addr, align 4
  %add12 = add nsw i32 %30, %31
  %div13 = sdiv i32 %add12, 2
  store i32 %div13, i32* %this_bits, align 4
  br label %if.end22

if.else14:                                        ; preds = %if.end
  %32 = load i32, i32* %this_bits, align 4
  %add15 = add nsw i32 %32, 32
  store i32 %add15, i32* %min_bits.addr, align 4
  %33 = load i32, i32* %max_bits.addr, align 4
  %34 = load i32, i32* %min_bits.addr, align 4
  %sub16 = sub nsw i32 %33, %34
  store i32 %sub16, i32* %dbits, align 4
  %35 = load i32, i32* %max_bits.addr, align 4
  %36 = load i32, i32* %min_bits.addr, align 4
  %add17 = add nsw i32 %35, %36
  %div18 = sdiv i32 %add17, 2
  store i32 %div18, i32* %this_bits, align 4
  %37 = load i32, i32* %found, align 4
  %tobool = icmp ne i32 %37, 0
  br i1 %tobool, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.else14
  store i32 2, i32* %found, align 4
  %38 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %39 = bitcast %struct.gr_info* %38 to i8*
  %40 = bitcast %struct.gr_info* %bst_cod_info to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 5252, i1 false)
  %41 = load float*, float** %xrpow.addr, align 4
  %42 = bitcast float* %41 to i8*
  %arraydecay20 = getelementptr inbounds [576 x float], [576 x float]* %bst_xrpow, i32 0, i32 0
  %43 = bitcast float* %arraydecay20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 16 %43, i32 2304, i1 false)
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.else14
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then8
  br label %do.cond

do.cond:                                          ; preds = %if.end22
  %44 = load i32, i32* %dbits, align 4
  %cmp23 = icmp sgt i32 %44, 12
  br i1 %cmp23, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %45 = load i32, i32* %sfb21_extra, align 4
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %46, i32 0, i32 13
  %sfb21_extra25 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt24, i32 0, i32 8
  store i32 %45, i32* %sfb21_extra25, align 4
  %47 = load i32, i32* %found, align 4
  %cmp26 = icmp eq i32 %47, 2
  br i1 %cmp26, label %if.then27, label %if.end32

if.then27:                                        ; preds = %do.end
  %48 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %l3_enc28 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %48, i32 0, i32 1
  %arraydecay29 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc28, i32 0, i32 0
  %49 = bitcast i32* %arraydecay29 to i8*
  %l3_enc30 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %bst_cod_info, i32 0, i32 1
  %arraydecay31 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc30, i32 0, i32 0
  %50 = bitcast i32* %arraydecay31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 2304, i1 false)
  br label %if.end32

if.end32:                                         ; preds = %if.then27, %do.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @trancate_smallspectrums(%struct.lame_internal_flags* %gfc, %struct.gr_info* %gi, float* %l3_xmin, float* %work) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %l3_xmin.addr = alloca float*, align 4
  %work.addr = alloca float*, align 4
  %sfb = alloca i32, align 4
  %j = alloca i32, align 4
  %width = alloca i32, align 4
  %distort = alloca [39 x float], align 16
  %dummy = alloca %struct.calc_noise_result_t, align 4
  %xr = alloca float, align 4
  %allowedNoise = alloca float, align 4
  %trancateThreshold = alloca float, align 4
  %nsame = alloca i32, align 4
  %start = alloca i32, align 4
  %noise = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store float* %l3_xmin, float** %l3_xmin.addr, align 4
  store float* %work, float** %work.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %1 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %1, 4
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %lor.lhs.false, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %2 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 9
  %3 = load i32, i32* %block_type, align 4
  %cmp = icmp eq i32 %3, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %entry
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %4, i32 0, i32 13
  %substep_shaping2 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt1, i32 0, i32 9
  %5 = load i32, i32* %substep_shaping2, align 8
  %and3 = and i32 %5, 128
  %tobool4 = icmp ne i32 %and3, 0
  br i1 %tobool4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %land.lhs.true
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %6 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %7 = load float*, float** %l3_xmin.addr, align 4
  %arraydecay = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 0
  %call = call i32 @calc_noise(%struct.gr_info* %6, float* %7, float* %arraydecay, %struct.calc_noise_result_t* %dummy, %struct.calc_noise_data_t* null)
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %j, align 4
  %cmp5 = icmp slt i32 %8, 576
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %xr, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 1
  %10 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc, i32 0, i32 %10
  %11 = load i32, i32* %arrayidx, align 4
  %cmp6 = icmp ne i32 %11, 0
  br i1 %cmp6, label %if.then7, label %if.end11

if.then7:                                         ; preds = %for.body
  %12 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xr8 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 0
  %13 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds [576 x float], [576 x float]* %xr8, i32 0, i32 %13
  %14 = load float, float* %arrayidx9, align 4
  %conv = fpext float %14 to double
  %15 = call double @llvm.fabs.f64(double %conv)
  %conv10 = fptrunc double %15 to float
  store float %conv10, float* %xr, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then7, %for.body
  %16 = load float, float* %xr, align 4
  %17 = load float*, float** %work.addr, align 4
  %18 = load i32, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds float, float* %17, i32 %18
  store float %16, float* %arrayidx12, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end11
  %19 = load i32, i32* %j, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j, align 4
  store i32 8, i32* %sfb, align 4
  %20 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %block_type13 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %20, i32 0, i32 9
  %21 = load i32, i32* %block_type13, align 4
  %cmp14 = icmp eq i32 %21, 2
  br i1 %cmp14, label %if.then16, label %if.end17

if.then16:                                        ; preds = %for.end
  store i32 6, i32* %sfb, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %for.end
  br label %do.body

do.body:                                          ; preds = %do.cond174, %if.end17
  %22 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %width18 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %22, i32 0, i32 25
  %23 = load i32, i32* %sfb, align 4
  %arrayidx19 = getelementptr inbounds [39 x i32], [39 x i32]* %width18, i32 0, i32 %23
  %24 = load i32, i32* %arrayidx19, align 4
  store i32 %24, i32* %width, align 4
  %25 = load i32, i32* %width, align 4
  %26 = load i32, i32* %j, align 4
  %add = add nsw i32 %26, %25
  store i32 %add, i32* %j, align 4
  %27 = load i32, i32* %sfb, align 4
  %arrayidx20 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 %27
  %28 = load float, float* %arrayidx20, align 4
  %conv21 = fpext float %28 to double
  %cmp22 = fcmp oge double %conv21, 1.000000e+00
  br i1 %cmp22, label %if.then24, label %if.end25

if.then24:                                        ; preds = %do.body
  br label %do.cond174

if.end25:                                         ; preds = %do.body
  %29 = load float*, float** %work.addr, align 4
  %30 = load i32, i32* %j, align 4
  %31 = load i32, i32* %width, align 4
  %sub = sub nsw i32 %30, %31
  %arrayidx26 = getelementptr inbounds float, float* %29, i32 %sub
  %32 = bitcast float* %arrayidx26 to i8*
  %33 = load i32, i32* %width, align 4
  call void @qsort(i8* %32, i32 %33, i32 4, i32 (i8*, i8*)* @floatcompare)
  %34 = load float*, float** %work.addr, align 4
  %35 = load i32, i32* %j, align 4
  %sub27 = sub nsw i32 %35, 1
  %arrayidx28 = getelementptr inbounds float, float* %34, i32 %sub27
  %36 = load float, float* %arrayidx28, align 4
  %conv29 = fpext float %36 to double
  %37 = call double @llvm.fabs.f64(double %conv29)
  %38 = call double @llvm.fabs.f64(double 0.000000e+00)
  %cmp30 = fcmp ogt double %37, %38
  br i1 %cmp30, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end25
  %39 = load float*, float** %work.addr, align 4
  %40 = load i32, i32* %j, align 4
  %sub32 = sub nsw i32 %40, 1
  %arrayidx33 = getelementptr inbounds float, float* %39, i32 %sub32
  %41 = load float, float* %arrayidx33, align 4
  %conv34 = fpext float %41 to double
  %sub35 = fsub double %conv34, 0.000000e+00
  %42 = call double @llvm.fabs.f64(double %sub35)
  %43 = load float*, float** %work.addr, align 4
  %44 = load i32, i32* %j, align 4
  %sub36 = sub nsw i32 %44, 1
  %arrayidx37 = getelementptr inbounds float, float* %43, i32 %sub36
  %45 = load float, float* %arrayidx37, align 4
  %conv38 = fpext float %45 to double
  %46 = call double @llvm.fabs.f64(double %conv38)
  %mul = fmul double %46, 0x3EB0C6F7A0000000
  %cmp39 = fcmp ole double %42, %mul
  br i1 %cmp39, label %if.then48, label %if.end49

cond.false:                                       ; preds = %if.end25
  %47 = load float*, float** %work.addr, align 4
  %48 = load i32, i32* %j, align 4
  %sub41 = sub nsw i32 %48, 1
  %arrayidx42 = getelementptr inbounds float, float* %47, i32 %sub41
  %49 = load float, float* %arrayidx42, align 4
  %conv43 = fpext float %49 to double
  %sub44 = fsub double %conv43, 0.000000e+00
  %50 = call double @llvm.fabs.f64(double %sub44)
  %51 = call double @llvm.fabs.f64(double 0.000000e+00)
  %mul45 = fmul double %51, 0x3EB0C6F7A0000000
  %cmp46 = fcmp ole double %50, %mul45
  br i1 %cmp46, label %if.then48, label %if.end49

if.then48:                                        ; preds = %cond.false, %cond.true
  br label %do.cond174

if.end49:                                         ; preds = %cond.false, %cond.true
  %52 = load i32, i32* %sfb, align 4
  %arrayidx50 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 %52
  %53 = load float, float* %arrayidx50, align 4
  %conv51 = fpext float %53 to double
  %sub52 = fsub double 1.000000e+00, %conv51
  %54 = load float*, float** %l3_xmin.addr, align 4
  %55 = load i32, i32* %sfb, align 4
  %arrayidx53 = getelementptr inbounds float, float* %54, i32 %55
  %56 = load float, float* %arrayidx53, align 4
  %conv54 = fpext float %56 to double
  %mul55 = fmul double %sub52, %conv54
  %conv56 = fptrunc double %mul55 to float
  store float %conv56, float* %allowedNoise, align 4
  store float 0.000000e+00, float* %trancateThreshold, align 4
  store i32 0, i32* %start, align 4
  br label %do.body57

do.body57:                                        ; preds = %do.cond, %if.end49
  store i32 1, i32* %nsame, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc111, %do.body57
  %57 = load i32, i32* %start, align 4
  %58 = load i32, i32* %nsame, align 4
  %add59 = add nsw i32 %57, %58
  %59 = load i32, i32* %width, align 4
  %cmp60 = icmp slt i32 %add59, %59
  br i1 %cmp60, label %for.body62, label %for.end113

for.body62:                                       ; preds = %for.cond58
  %60 = load float*, float** %work.addr, align 4
  %61 = load i32, i32* %start, align 4
  %62 = load i32, i32* %j, align 4
  %add63 = add nsw i32 %61, %62
  %63 = load i32, i32* %width, align 4
  %sub64 = sub nsw i32 %add63, %63
  %arrayidx65 = getelementptr inbounds float, float* %60, i32 %sub64
  %64 = load float, float* %arrayidx65, align 4
  %conv66 = fpext float %64 to double
  %65 = call double @llvm.fabs.f64(double %conv66)
  %66 = load float*, float** %work.addr, align 4
  %67 = load i32, i32* %start, align 4
  %68 = load i32, i32* %j, align 4
  %add67 = add nsw i32 %67, %68
  %69 = load i32, i32* %nsame, align 4
  %add68 = add nsw i32 %add67, %69
  %70 = load i32, i32* %width, align 4
  %sub69 = sub nsw i32 %add68, %70
  %arrayidx70 = getelementptr inbounds float, float* %66, i32 %sub69
  %71 = load float, float* %arrayidx70, align 4
  %conv71 = fpext float %71 to double
  %72 = call double @llvm.fabs.f64(double %conv71)
  %cmp72 = fcmp ogt double %65, %72
  br i1 %cmp72, label %cond.true74, label %cond.false91

cond.true74:                                      ; preds = %for.body62
  %73 = load float*, float** %work.addr, align 4
  %74 = load i32, i32* %start, align 4
  %75 = load i32, i32* %j, align 4
  %add75 = add nsw i32 %74, %75
  %76 = load i32, i32* %width, align 4
  %sub76 = sub nsw i32 %add75, %76
  %arrayidx77 = getelementptr inbounds float, float* %73, i32 %sub76
  %77 = load float, float* %arrayidx77, align 4
  %78 = load float*, float** %work.addr, align 4
  %79 = load i32, i32* %start, align 4
  %80 = load i32, i32* %j, align 4
  %add78 = add nsw i32 %79, %80
  %81 = load i32, i32* %nsame, align 4
  %add79 = add nsw i32 %add78, %81
  %82 = load i32, i32* %width, align 4
  %sub80 = sub nsw i32 %add79, %82
  %arrayidx81 = getelementptr inbounds float, float* %78, i32 %sub80
  %83 = load float, float* %arrayidx81, align 4
  %sub82 = fsub float %77, %83
  %conv83 = fpext float %sub82 to double
  %84 = call double @llvm.fabs.f64(double %conv83)
  %85 = load float*, float** %work.addr, align 4
  %86 = load i32, i32* %start, align 4
  %87 = load i32, i32* %j, align 4
  %add84 = add nsw i32 %86, %87
  %88 = load i32, i32* %width, align 4
  %sub85 = sub nsw i32 %add84, %88
  %arrayidx86 = getelementptr inbounds float, float* %85, i32 %sub85
  %89 = load float, float* %arrayidx86, align 4
  %conv87 = fpext float %89 to double
  %90 = call double @llvm.fabs.f64(double %conv87)
  %mul88 = fmul double %90, 0x3EB0C6F7A0000000
  %cmp89 = fcmp ole double %84, %mul88
  br i1 %cmp89, label %if.end110, label %if.then109

cond.false91:                                     ; preds = %for.body62
  %91 = load float*, float** %work.addr, align 4
  %92 = load i32, i32* %start, align 4
  %93 = load i32, i32* %j, align 4
  %add92 = add nsw i32 %92, %93
  %94 = load i32, i32* %width, align 4
  %sub93 = sub nsw i32 %add92, %94
  %arrayidx94 = getelementptr inbounds float, float* %91, i32 %sub93
  %95 = load float, float* %arrayidx94, align 4
  %96 = load float*, float** %work.addr, align 4
  %97 = load i32, i32* %start, align 4
  %98 = load i32, i32* %j, align 4
  %add95 = add nsw i32 %97, %98
  %99 = load i32, i32* %nsame, align 4
  %add96 = add nsw i32 %add95, %99
  %100 = load i32, i32* %width, align 4
  %sub97 = sub nsw i32 %add96, %100
  %arrayidx98 = getelementptr inbounds float, float* %96, i32 %sub97
  %101 = load float, float* %arrayidx98, align 4
  %sub99 = fsub float %95, %101
  %conv100 = fpext float %sub99 to double
  %102 = call double @llvm.fabs.f64(double %conv100)
  %103 = load float*, float** %work.addr, align 4
  %104 = load i32, i32* %start, align 4
  %105 = load i32, i32* %j, align 4
  %add101 = add nsw i32 %104, %105
  %106 = load i32, i32* %nsame, align 4
  %add102 = add nsw i32 %add101, %106
  %107 = load i32, i32* %width, align 4
  %sub103 = sub nsw i32 %add102, %107
  %arrayidx104 = getelementptr inbounds float, float* %103, i32 %sub103
  %108 = load float, float* %arrayidx104, align 4
  %conv105 = fpext float %108 to double
  %109 = call double @llvm.fabs.f64(double %conv105)
  %mul106 = fmul double %109, 0x3EB0C6F7A0000000
  %cmp107 = fcmp ole double %102, %mul106
  br i1 %cmp107, label %if.end110, label %if.then109

if.then109:                                       ; preds = %cond.false91, %cond.true74
  br label %for.end113

if.end110:                                        ; preds = %cond.false91, %cond.true74
  br label %for.inc111

for.inc111:                                       ; preds = %if.end110
  %110 = load i32, i32* %nsame, align 4
  %inc112 = add nsw i32 %110, 1
  store i32 %inc112, i32* %nsame, align 4
  br label %for.cond58

for.end113:                                       ; preds = %if.then109, %for.cond58
  %111 = load float*, float** %work.addr, align 4
  %112 = load i32, i32* %start, align 4
  %113 = load i32, i32* %j, align 4
  %add114 = add nsw i32 %112, %113
  %114 = load i32, i32* %width, align 4
  %sub115 = sub nsw i32 %add114, %114
  %arrayidx116 = getelementptr inbounds float, float* %111, i32 %sub115
  %115 = load float, float* %arrayidx116, align 4
  %116 = load float*, float** %work.addr, align 4
  %117 = load i32, i32* %start, align 4
  %118 = load i32, i32* %j, align 4
  %add117 = add nsw i32 %117, %118
  %119 = load i32, i32* %width, align 4
  %sub118 = sub nsw i32 %add117, %119
  %arrayidx119 = getelementptr inbounds float, float* %116, i32 %sub118
  %120 = load float, float* %arrayidx119, align 4
  %mul120 = fmul float %115, %120
  %121 = load i32, i32* %nsame, align 4
  %conv121 = sitofp i32 %121 to float
  %mul122 = fmul float %mul120, %conv121
  store float %mul122, float* %noise, align 4
  %122 = load float, float* %allowedNoise, align 4
  %123 = load float, float* %noise, align 4
  %cmp123 = fcmp olt float %122, %123
  br i1 %cmp123, label %if.then125, label %if.end134

if.then125:                                       ; preds = %for.end113
  %124 = load i32, i32* %start, align 4
  %cmp126 = icmp ne i32 %124, 0
  br i1 %cmp126, label %if.then128, label %if.end133

if.then128:                                       ; preds = %if.then125
  %125 = load float*, float** %work.addr, align 4
  %126 = load i32, i32* %start, align 4
  %127 = load i32, i32* %j, align 4
  %add129 = add nsw i32 %126, %127
  %128 = load i32, i32* %width, align 4
  %sub130 = sub nsw i32 %add129, %128
  %sub131 = sub nsw i32 %sub130, 1
  %arrayidx132 = getelementptr inbounds float, float* %125, i32 %sub131
  %129 = load float, float* %arrayidx132, align 4
  store float %129, float* %trancateThreshold, align 4
  br label %if.end133

if.end133:                                        ; preds = %if.then128, %if.then125
  br label %do.end

if.end134:                                        ; preds = %for.end113
  %130 = load float, float* %noise, align 4
  %131 = load float, float* %allowedNoise, align 4
  %sub135 = fsub float %131, %130
  store float %sub135, float* %allowedNoise, align 4
  %132 = load i32, i32* %nsame, align 4
  %133 = load i32, i32* %start, align 4
  %add136 = add nsw i32 %133, %132
  store i32 %add136, i32* %start, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end134
  %134 = load i32, i32* %start, align 4
  %135 = load i32, i32* %width, align 4
  %cmp137 = icmp slt i32 %134, %135
  br i1 %cmp137, label %do.body57, label %do.end

do.end:                                           ; preds = %do.cond, %if.end133
  %136 = load float, float* %trancateThreshold, align 4
  %conv139 = fpext float %136 to double
  %137 = call double @llvm.fabs.f64(double %conv139)
  %138 = call double @llvm.fabs.f64(double 0.000000e+00)
  %cmp140 = fcmp ogt double %137, %138
  br i1 %cmp140, label %cond.true142, label %cond.false149

cond.true142:                                     ; preds = %do.end
  %139 = load float, float* %trancateThreshold, align 4
  %conv143 = fpext float %139 to double
  %sub144 = fsub double %conv143, 0.000000e+00
  %140 = call double @llvm.fabs.f64(double %sub144)
  %141 = load float, float* %trancateThreshold, align 4
  %conv145 = fpext float %141 to double
  %142 = call double @llvm.fabs.f64(double %conv145)
  %mul146 = fmul double %142, 0x3EB0C6F7A0000000
  %cmp147 = fcmp ole double %140, %mul146
  br i1 %cmp147, label %if.then155, label %if.end156

cond.false149:                                    ; preds = %do.end
  %143 = load float, float* %trancateThreshold, align 4
  %conv150 = fpext float %143 to double
  %sub151 = fsub double %conv150, 0.000000e+00
  %144 = call double @llvm.fabs.f64(double %sub151)
  %145 = call double @llvm.fabs.f64(double 0.000000e+00)
  %mul152 = fmul double %145, 0x3EB0C6F7A0000000
  %cmp153 = fcmp ole double %144, %mul152
  br i1 %cmp153, label %if.then155, label %if.end156

if.then155:                                       ; preds = %cond.false149, %cond.true142
  br label %do.cond174

if.end156:                                        ; preds = %cond.false149, %cond.true142
  br label %do.body157

do.body157:                                       ; preds = %do.cond170, %if.end156
  %146 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %xr158 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %146, i32 0, i32 0
  %147 = load i32, i32* %j, align 4
  %148 = load i32, i32* %width, align 4
  %sub159 = sub nsw i32 %147, %148
  %arrayidx160 = getelementptr inbounds [576 x float], [576 x float]* %xr158, i32 0, i32 %sub159
  %149 = load float, float* %arrayidx160, align 4
  %conv161 = fpext float %149 to double
  %150 = call double @llvm.fabs.f64(double %conv161)
  %151 = load float, float* %trancateThreshold, align 4
  %conv162 = fpext float %151 to double
  %cmp163 = fcmp ole double %150, %conv162
  br i1 %cmp163, label %if.then165, label %if.end169

if.then165:                                       ; preds = %do.body157
  %152 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %l3_enc166 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %152, i32 0, i32 1
  %153 = load i32, i32* %j, align 4
  %154 = load i32, i32* %width, align 4
  %sub167 = sub nsw i32 %153, %154
  %arrayidx168 = getelementptr inbounds [576 x i32], [576 x i32]* %l3_enc166, i32 0, i32 %sub167
  store i32 0, i32* %arrayidx168, align 4
  br label %if.end169

if.end169:                                        ; preds = %if.then165, %do.body157
  br label %do.cond170

do.cond170:                                       ; preds = %if.end169
  %155 = load i32, i32* %width, align 4
  %dec = add nsw i32 %155, -1
  store i32 %dec, i32* %width, align 4
  %cmp171 = icmp sgt i32 %dec, 0
  br i1 %cmp171, label %do.body157, label %do.end173

do.end173:                                        ; preds = %do.cond170
  br label %do.cond174

do.cond174:                                       ; preds = %do.end173, %if.then155, %if.then48, %if.then24
  %156 = load i32, i32* %sfb, align 4
  %inc175 = add nsw i32 %156, 1
  store i32 %inc175, i32* %sfb, align 4
  %157 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %157, i32 0, i32 23
  %158 = load i32, i32* %psymax, align 4
  %cmp176 = icmp slt i32 %inc175, %158
  br i1 %cmp176, label %do.body, label %do.end178

do.end178:                                        ; preds = %do.cond174
  %159 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %160 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call179 = call i32 @noquant_count_bits(%struct.lame_internal_flags* %159, %struct.gr_info* %160, %struct.calc_noise_data_t* null)
  %161 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %161, i32 0, i32 4
  store i32 %call179, i32* %part2_3_length, align 4
  br label %return

return:                                           ; preds = %do.end178, %if.then
  ret void
}

declare i32 @ResvFrameBegin(%struct.lame_internal_flags*, i32*) #1

; Function Attrs: noinline nounwind optnone
define internal void @bitpressure_strategy(%struct.lame_internal_flags* %gfc, [2 x [39 x float]]* %l3_xmin, [2 x i32]* %min_bits, [2 x i32]* %max_bits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %l3_xmin.addr = alloca [2 x [39 x float]]*, align 4
  %min_bits.addr = alloca [2 x i32]*, align 4
  %max_bits.addr = alloca [2 x i32]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %sfb = alloca i32, align 4
  %gi = alloca %struct.gr_info*, align 4
  %pxmin = alloca float*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x [39 x float]]* %l3_xmin, [2 x [39 x float]]** %l3_xmin.addr, align 4
  store [2 x i32]* %min_bits, [2 x i32]** %min_bits.addr, align 4
  store [2 x i32]* %max_bits, [2 x i32]** %max_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc81, %entry
  %1 = load i32, i32* %gr, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 15
  %3 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %for.body, label %for.end83

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc78, %for.body
  %4 = load i32, i32* %ch, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 14
  %6 = load i32, i32* %channels_out, align 4
  %cmp3 = icmp slt i32 %4, %6
  br i1 %cmp3, label %for.body4, label %for.end80

for.body4:                                        ; preds = %for.cond2
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %7, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side, i32 0, i32 0
  %8 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %8
  %9 = load i32, i32* %ch, align 4
  %arrayidx5 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %9
  store %struct.gr_info* %arrayidx5, %struct.gr_info** %gi, align 4
  %10 = load [2 x [39 x float]]*, [2 x [39 x float]]** %l3_xmin.addr, align 4
  %11 = load i32, i32* %gr, align 4
  %arrayidx6 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %10, i32 %11
  %12 = load i32, i32* %ch, align 4
  %arrayidx7 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx6, i32 0, i32 %12
  %arraydecay = getelementptr inbounds [39 x float], [39 x float]* %arrayidx7, i32 0, i32 0
  store float* %arraydecay, float** %pxmin, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body4
  %13 = load i32, i32* %sfb, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %psy_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 21
  %15 = load i32, i32* %psy_lmax, align 4
  %cmp9 = icmp slt i32 %13, %15
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %16 = load i32, i32* %sfb, align 4
  %conv = sitofp i32 %16 to double
  %mul = fmul double 2.900000e-02, %conv
  %17 = load i32, i32* %sfb, align 4
  %conv11 = sitofp i32 %17 to double
  %mul12 = fmul double %mul, %conv11
  %div = fdiv double %mul12, 2.200000e+01
  %div13 = fdiv double %div, 2.200000e+01
  %add = fadd double 1.000000e+00, %div13
  %18 = load float*, float** %pxmin, align 4
  %incdec.ptr = getelementptr inbounds float, float* %18, i32 1
  store float* %incdec.ptr, float** %pxmin, align 4
  %19 = load float, float* %18, align 4
  %conv14 = fpext float %19 to double
  %mul15 = fmul double %conv14, %add
  %conv16 = fptrunc double %mul15 to float
  store float %conv16, float* %18, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %20 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %21 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %21, i32 0, i32 9
  %22 = load i32, i32* %block_type, align 4
  %cmp17 = icmp eq i32 %22, 2
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %23 = load %struct.gr_info*, %struct.gr_info** %gi, align 4
  %sfb_smin = getelementptr inbounds %struct.gr_info, %struct.gr_info* %23, i32 0, i32 20
  %24 = load i32, i32* %sfb_smin, align 4
  store i32 %24, i32* %sfb, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc56, %if.then
  %25 = load i32, i32* %sfb, align 4
  %cmp20 = icmp slt i32 %25, 13
  br i1 %cmp20, label %for.body22, label %for.end58

for.body22:                                       ; preds = %for.cond19
  %26 = load i32, i32* %sfb, align 4
  %conv23 = sitofp i32 %26 to double
  %mul24 = fmul double 2.900000e-02, %conv23
  %27 = load i32, i32* %sfb, align 4
  %conv25 = sitofp i32 %27 to double
  %mul26 = fmul double %mul24, %conv25
  %div27 = fdiv double %mul26, 1.300000e+01
  %div28 = fdiv double %div27, 1.300000e+01
  %add29 = fadd double 1.000000e+00, %div28
  %28 = load float*, float** %pxmin, align 4
  %incdec.ptr30 = getelementptr inbounds float, float* %28, i32 1
  store float* %incdec.ptr30, float** %pxmin, align 4
  %29 = load float, float* %28, align 4
  %conv31 = fpext float %29 to double
  %mul32 = fmul double %conv31, %add29
  %conv33 = fptrunc double %mul32 to float
  store float %conv33, float* %28, align 4
  %30 = load i32, i32* %sfb, align 4
  %conv34 = sitofp i32 %30 to double
  %mul35 = fmul double 2.900000e-02, %conv34
  %31 = load i32, i32* %sfb, align 4
  %conv36 = sitofp i32 %31 to double
  %mul37 = fmul double %mul35, %conv36
  %div38 = fdiv double %mul37, 1.300000e+01
  %div39 = fdiv double %div38, 1.300000e+01
  %add40 = fadd double 1.000000e+00, %div39
  %32 = load float*, float** %pxmin, align 4
  %incdec.ptr41 = getelementptr inbounds float, float* %32, i32 1
  store float* %incdec.ptr41, float** %pxmin, align 4
  %33 = load float, float* %32, align 4
  %conv42 = fpext float %33 to double
  %mul43 = fmul double %conv42, %add40
  %conv44 = fptrunc double %mul43 to float
  store float %conv44, float* %32, align 4
  %34 = load i32, i32* %sfb, align 4
  %conv45 = sitofp i32 %34 to double
  %mul46 = fmul double 2.900000e-02, %conv45
  %35 = load i32, i32* %sfb, align 4
  %conv47 = sitofp i32 %35 to double
  %mul48 = fmul double %mul46, %conv47
  %div49 = fdiv double %mul48, 1.300000e+01
  %div50 = fdiv double %div49, 1.300000e+01
  %add51 = fadd double 1.000000e+00, %div50
  %36 = load float*, float** %pxmin, align 4
  %incdec.ptr52 = getelementptr inbounds float, float* %36, i32 1
  store float* %incdec.ptr52, float** %pxmin, align 4
  %37 = load float, float* %36, align 4
  %conv53 = fpext float %37 to double
  %mul54 = fmul double %conv53, %add51
  %conv55 = fptrunc double %mul54 to float
  store float %conv55, float* %36, align 4
  br label %for.inc56

for.inc56:                                        ; preds = %for.body22
  %38 = load i32, i32* %sfb, align 4
  %inc57 = add nsw i32 %38, 1
  store i32 %inc57, i32* %sfb, align 4
  br label %for.cond19

for.end58:                                        ; preds = %for.cond19
  br label %if.end

if.end:                                           ; preds = %for.end58, %for.end
  %39 = load [2 x i32]*, [2 x i32]** %min_bits.addr, align 4
  %40 = load i32, i32* %gr, align 4
  %arrayidx59 = getelementptr inbounds [2 x i32], [2 x i32]* %39, i32 %40
  %41 = load i32, i32* %ch, align 4
  %arrayidx60 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx59, i32 0, i32 %41
  %42 = load i32, i32* %arrayidx60, align 4
  %conv61 = sitofp i32 %42 to double
  %43 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %44 = load i32, i32* %gr, align 4
  %arrayidx62 = getelementptr inbounds [2 x i32], [2 x i32]* %43, i32 %44
  %45 = load i32, i32* %ch, align 4
  %arrayidx63 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx62, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx63, align 4
  %conv64 = sitofp i32 %46 to double
  %mul65 = fmul double 9.000000e-01, %conv64
  %cmp66 = fcmp ogt double %conv61, %mul65
  br i1 %cmp66, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %47 = load [2 x i32]*, [2 x i32]** %min_bits.addr, align 4
  %48 = load i32, i32* %gr, align 4
  %arrayidx68 = getelementptr inbounds [2 x i32], [2 x i32]* %47, i32 %48
  %49 = load i32, i32* %ch, align 4
  %arrayidx69 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx68, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx69, align 4
  %conv70 = sitofp i32 %50 to double
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %51 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %52 = load i32, i32* %gr, align 4
  %arrayidx71 = getelementptr inbounds [2 x i32], [2 x i32]* %51, i32 %52
  %53 = load i32, i32* %ch, align 4
  %arrayidx72 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx71, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx72, align 4
  %conv73 = sitofp i32 %54 to double
  %mul74 = fmul double 9.000000e-01, %conv73
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ %conv70, %cond.true ], [ %mul74, %cond.false ]
  %conv75 = fptosi double %cond to i32
  %55 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %56 = load i32, i32* %gr, align 4
  %arrayidx76 = getelementptr inbounds [2 x i32], [2 x i32]* %55, i32 %56
  %57 = load i32, i32* %ch, align 4
  %arrayidx77 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx76, i32 0, i32 %57
  store i32 %conv75, i32* %arrayidx77, align 4
  br label %for.inc78

for.inc78:                                        ; preds = %cond.end
  %58 = load i32, i32* %ch, align 4
  %inc79 = add nsw i32 %58, 1
  store i32 %inc79, i32* %ch, align 4
  br label %for.cond2

for.end80:                                        ; preds = %for.cond2
  br label %for.inc81

for.inc81:                                        ; preds = %for.end80
  %59 = load i32, i32* %gr, align 4
  %inc82 = add nsw i32 %59, 1
  store i32 %inc82, i32* %gr, align 4
  br label %for.cond

for.end83:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @iteration_finish_one(%struct.lame_internal_flags* %gfc, i32 %gr, i32 %ch) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %gr.addr = alloca i32, align 4
  %ch.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %2 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %2, i32 0, i32 0
  %3 = load i32, i32* %gr.addr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %3
  %4 = load i32, i32* %ch.addr, align 4
  %arrayidx3 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %4
  store %struct.gr_info* %arrayidx3, %struct.gr_info** %cod_info, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %6 = load i32, i32* %gr.addr, align 4
  %7 = load i32, i32* %ch.addr, align 4
  %8 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  call void @best_scalefac_store(%struct.lame_internal_flags* %5, i32 %6, i32 %7, %struct.III_side_info_t* %8)
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_best_huffman = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 5
  %10 = load i32, i32* %use_best_huffman, align 4
  %cmp = icmp eq i32 %10, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @best_huffman_divide(%struct.lame_internal_flags* %11, %struct.gr_info* %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @ResvAdjust(%struct.lame_internal_flags* %13, %struct.gr_info* %14)
  ret void
}

declare void @ResvFrameEnd(%struct.lame_internal_flags*, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @VBR_new_iteration_loop(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x %struct.III_psy_ratio]* %ratio) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %l3_xmin = alloca [2 x [2 x [39 x float]]], align 16
  %xrpow = alloca [2 x [2 x [576 x float]]], align 16
  %frameBits = alloca [15 x i32], align 16
  %used_bits = alloca i32, align 4
  %max_bits = alloca [2 x [2 x i32]], align 16
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %analog_silence = alloca i32, align 4
  %pad = alloca i32, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %const_l3_xmin = alloca [2 x [39 x float]]*, align 4
  %const_xrpow = alloca [2 x [576 x float]]*, align 4
  %const_max_bits = alloca [2 x i32]*, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %unused = alloca i32, align 4
  %mean_bits = alloca i32, align 4
  %fullframebits = alloca i32, align 4
  %cod_info75 = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %arraydecay = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 0
  store [2 x [39 x float]]* %arraydecay, [2 x [39 x float]]** %const_l3_xmin, align 4
  %arraydecay3 = getelementptr inbounds [2 x [2 x [576 x float]]], [2 x [2 x [576 x float]]]* %xrpow, i32 0, i32 0
  store [2 x [576 x float]]* %arraydecay3, [2 x [576 x float]]** %const_xrpow, align 4
  %arraydecay4 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 0
  store [2 x i32]* %arraydecay4, [2 x i32]** %const_max_bits, align 4
  %3 = load float*, float** %ms_ener_ratio.addr, align 4
  %arraydecay5 = getelementptr inbounds [2 x [2 x [576 x float]]], [2 x [2 x [576 x float]]]* %xrpow, i32 0, i32 0
  %4 = bitcast [2 x [576 x float]]* %arraydecay5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %4, i8 0, i32 9216, i1 false)
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %6 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %7 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %arraydecay6 = getelementptr inbounds [2 x [2 x [39 x float]]], [2 x [2 x [39 x float]]]* %l3_xmin, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 0
  %call = call i32 @VBR_new_prepare(%struct.lame_internal_flags* %5, [2 x float]* %6, [2 x %struct.III_psy_ratio]* %7, [2 x [39 x float]]* %arraydecay6, i32* %arraydecay7, [2 x i32]* %arraydecay8, i32* %pad)
  store i32 %call, i32* %analog_silence, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %entry
  %8 = load i32, i32* %gr, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 15
  %10 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %for.body, label %for.end22

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %ch, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc, %for.body
  %11 = load i32, i32* %ch, align 4
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 14
  %13 = load i32, i32* %channels_out, align 4
  %cmp10 = icmp slt i32 %11, %13
  br i1 %cmp10, label %for.body11, label %for.end

for.body11:                                       ; preds = %for.cond9
  %14 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %14, i32 0, i32 0
  %15 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %15
  %16 = load i32, i32* %ch, align 4
  %arrayidx12 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %16
  store %struct.gr_info* %arrayidx12, %struct.gr_info** %cod_info, align 4
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %19 = load i32, i32* %gr, align 4
  %arrayidx13 = getelementptr inbounds [2 x [2 x [576 x float]]], [2 x [2 x [576 x float]]]* %xrpow, i32 0, i32 %19
  %20 = load i32, i32* %ch, align 4
  %arrayidx14 = getelementptr inbounds [2 x [576 x float]], [2 x [576 x float]]* %arrayidx13, i32 0, i32 %20
  %arraydecay15 = getelementptr inbounds [576 x float], [576 x float]* %arrayidx14, i32 0, i32 0
  %call16 = call i32 @init_xrpow(%struct.lame_internal_flags* %17, %struct.gr_info* %18, float* %arraydecay15)
  %cmp17 = icmp eq i32 0, %call16
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body11
  %21 = load i32, i32* %gr, align 4
  %arrayidx18 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %max_bits, i32 0, i32 %21
  %22 = load i32, i32* %ch, align 4
  %arrayidx19 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx18, i32 0, i32 %22
  store i32 0, i32* %arrayidx19, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body11
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %23 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond9

for.end:                                          ; preds = %for.cond9
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %24 = load i32, i32* %gr, align 4
  %inc21 = add nsw i32 %24, 1
  store i32 %inc21, i32* %gr, align 4
  br label %for.cond

for.end22:                                        ; preds = %for.cond
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %26 = load [2 x [576 x float]]*, [2 x [576 x float]]** %const_xrpow, align 4
  %27 = load [2 x [39 x float]]*, [2 x [39 x float]]** %const_l3_xmin, align 4
  %28 = load [2 x i32]*, [2 x i32]** %const_max_bits, align 4
  %call23 = call i32 @VBR_encode_frame(%struct.lame_internal_flags* %25, [2 x [576 x float]]* %26, [2 x [39 x float]]* %27, [2 x i32]* %28)
  store i32 %call23, i32* %used_bits, align 4
  %29 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %29, i32 0, i32 34
  %30 = load i32, i32* %free_format, align 4
  %tobool = icmp ne i32 %30, 0
  br i1 %tobool, label %if.else59, label %if.then24

if.then24:                                        ; preds = %for.end22
  %31 = load i32, i32* %analog_silence, align 4
  %tobool25 = icmp ne i32 %31, 0
  br i1 %tobool25, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.then24
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %enforce_min_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 27
  %33 = load i32, i32* %enforce_min_bitrate, align 4
  %tobool26 = icmp ne i32 %33, 0
  br i1 %tobool26, label %if.else, label %if.then27

if.then27:                                        ; preds = %land.lhs.true
  store i32 1, i32* %i, align 4
  br label %if.end28

if.else:                                          ; preds = %land.lhs.true, %if.then24
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 24
  %35 = load i32, i32* %vbr_min_bitrate_index, align 4
  store i32 %35, i32* %i, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.else, %if.then27
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc36, %if.end28
  %36 = load i32, i32* %i, align 4
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %37, i32 0, i32 25
  %38 = load i32, i32* %vbr_max_bitrate_index, align 4
  %cmp30 = icmp slt i32 %36, %38
  br i1 %cmp30, label %for.body31, label %for.end38

for.body31:                                       ; preds = %for.cond29
  %39 = load i32, i32* %used_bits, align 4
  %40 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx32, align 4
  %cmp33 = icmp sle i32 %39, %41
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %for.body31
  br label %for.end38

if.end35:                                         ; preds = %for.body31
  br label %for.inc36

for.inc36:                                        ; preds = %if.end35
  %42 = load i32, i32* %i, align 4
  %inc37 = add nsw i32 %42, 1
  store i32 %inc37, i32* %i, align 4
  br label %for.cond29

for.end38:                                        ; preds = %if.then34, %for.cond29
  %43 = load i32, i32* %i, align 4
  %44 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index39 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %44, i32 0, i32 25
  %45 = load i32, i32* %vbr_max_bitrate_index39, align 4
  %cmp40 = icmp sgt i32 %43, %45
  br i1 %cmp40, label %if.then41, label %if.end43

if.then41:                                        ; preds = %for.end38
  %46 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index42 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %46, i32 0, i32 25
  %47 = load i32, i32* %vbr_max_bitrate_index42, align 4
  store i32 %47, i32* %i, align 4
  br label %if.end43

if.end43:                                         ; preds = %if.then41, %for.end38
  %48 = load i32, i32* %pad, align 4
  %cmp44 = icmp sgt i32 %48, 0
  br i1 %cmp44, label %if.then45, label %if.else56

if.then45:                                        ; preds = %if.end43
  %49 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index46 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %49, i32 0, i32 25
  %50 = load i32, i32* %vbr_max_bitrate_index46, align 4
  store i32 %50, i32* %j, align 4
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc54, %if.then45
  %51 = load i32, i32* %j, align 4
  %52 = load i32, i32* %i, align 4
  %cmp48 = icmp sgt i32 %51, %52
  br i1 %cmp48, label %for.body49, label %for.end55

for.body49:                                       ; preds = %for.cond47
  %53 = load i32, i32* %j, align 4
  %arrayidx50 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx50, align 4
  %55 = load i32, i32* %used_bits, align 4
  %sub = sub nsw i32 %54, %55
  store i32 %sub, i32* %unused, align 4
  %56 = load i32, i32* %unused, align 4
  %57 = load i32, i32* %pad, align 4
  %cmp51 = icmp sle i32 %56, %57
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %for.body49
  br label %for.end55

if.end53:                                         ; preds = %for.body49
  br label %for.inc54

for.inc54:                                        ; preds = %if.end53
  %58 = load i32, i32* %j, align 4
  %dec = add nsw i32 %58, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond47

for.end55:                                        ; preds = %if.then52, %for.cond47
  %59 = load i32, i32* %j, align 4
  %60 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %60, i32 0, i32 2
  store i32 %59, i32* %bitrate_index, align 4
  br label %if.end58

if.else56:                                        ; preds = %if.end43
  %61 = load i32, i32* %i, align 4
  %62 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index57 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %62, i32 0, i32 2
  store i32 %61, i32* %bitrate_index57, align 4
  br label %if.end58

if.end58:                                         ; preds = %if.else56, %for.end55
  br label %if.end61

if.else59:                                        ; preds = %for.end22
  %63 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index60 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %63, i32 0, i32 2
  store i32 0, i32* %bitrate_index60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.else59, %if.end58
  %64 = load i32, i32* %used_bits, align 4
  %65 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index62 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %65, i32 0, i32 2
  %66 = load i32, i32* %bitrate_index62, align 4
  %arrayidx63 = getelementptr inbounds [15 x i32], [15 x i32]* %frameBits, i32 0, i32 %66
  %67 = load i32, i32* %arrayidx63, align 4
  %cmp64 = icmp sle i32 %64, %67
  br i1 %cmp64, label %if.then65, label %if.else85

if.then65:                                        ; preds = %if.end61
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call66 = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %68, i32* %mean_bits)
  store i32 %call66, i32* %fullframebits, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond67

for.cond67:                                       ; preds = %for.inc82, %if.then65
  %69 = load i32, i32* %gr, align 4
  %70 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr68 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %70, i32 0, i32 15
  %71 = load i32, i32* %mode_gr68, align 4
  %cmp69 = icmp slt i32 %69, %71
  br i1 %cmp69, label %for.body70, label %for.end84

for.body70:                                       ; preds = %for.cond67
  store i32 0, i32* %ch, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc79, %for.body70
  %72 = load i32, i32* %ch, align 4
  %73 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out72 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %73, i32 0, i32 14
  %74 = load i32, i32* %channels_out72, align 4
  %cmp73 = icmp slt i32 %72, %74
  br i1 %cmp73, label %for.body74, label %for.end81

for.body74:                                       ; preds = %for.cond71
  %75 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt76 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %75, i32 0, i32 0
  %76 = load i32, i32* %gr, align 4
  %arrayidx77 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt76, i32 0, i32 %76
  %77 = load i32, i32* %ch, align 4
  %arrayidx78 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx77, i32 0, i32 %77
  store %struct.gr_info* %arrayidx78, %struct.gr_info** %cod_info75, align 4
  %78 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %79 = load %struct.gr_info*, %struct.gr_info** %cod_info75, align 4
  call void @ResvAdjust(%struct.lame_internal_flags* %78, %struct.gr_info* %79)
  br label %for.inc79

for.inc79:                                        ; preds = %for.body74
  %80 = load i32, i32* %ch, align 4
  %inc80 = add nsw i32 %80, 1
  store i32 %inc80, i32* %ch, align 4
  br label %for.cond71

for.end81:                                        ; preds = %for.cond71
  br label %for.inc82

for.inc82:                                        ; preds = %for.end81
  %81 = load i32, i32* %gr, align 4
  %inc83 = add nsw i32 %81, 1
  store i32 %inc83, i32* %gr, align 4
  br label %for.cond67

for.end84:                                        ; preds = %for.cond67
  %82 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %83 = load i32, i32* %mean_bits, align 4
  call void @ResvFrameEnd(%struct.lame_internal_flags* %82, i32 %83)
  br label %if.end86

if.else85:                                        ; preds = %if.end61
  %84 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %84, i8* getelementptr inbounds ([56 x i8], [56 x i8]* @.str, i32 0, i32 0))
  call void @exit(i32 -1) #6
  unreachable

if.end86:                                         ; preds = %for.end84
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal i32 @VBR_new_prepare(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, [2 x %struct.III_psy_ratio]* %ratio, [2 x [39 x float]]* %l3_xmin, i32* %frameBits, [2 x i32]* %max_bits, i32* %max_resv) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %l3_xmin.addr = alloca [2 x [39 x float]]*, align 4
  %frameBits.addr = alloca i32*, align 4
  %max_bits.addr = alloca [2 x i32]*, align 4
  %max_resv.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %analog_silence = alloca i32, align 4
  %avg = alloca i32, align 4
  %bits = alloca i32, align 4
  %maximum_framebits = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  store [2 x [39 x float]]* %l3_xmin, [2 x [39 x float]]** %l3_xmin.addr, align 4
  store i32* %frameBits, i32** %frameBits.addr, align 4
  store [2 x i32]* %max_bits, [2 x i32]** %max_bits.addr, align 4
  store i32* %max_resv, i32** %max_resv.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  store i32 1, i32* %analog_silence, align 4
  store i32 0, i32* %bits, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 34
  %3 = load i32, i32* %free_format, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 25
  %5 = load i32, i32* %vbr_max_bitrate_index, align 4
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 2
  store i32 %5, i32* %bitrate_index, align 4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %7, i32* %avg)
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 11
  %ResvMax = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc, i32 0, i32 13
  %9 = load i32, i32* %ResvMax, align 8
  %10 = load i32*, i32** %max_resv.addr, align 4
  store i32 %9, i32* %10, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %12 = load i32*, i32** %frameBits.addr, align 4
  call void @get_framebits(%struct.lame_internal_flags* %11, i32* %12)
  %13 = load i32*, i32** %frameBits.addr, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index2 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 25
  %15 = load i32, i32* %vbr_max_bitrate_index2, align 4
  %arrayidx = getelementptr inbounds i32, i32* %13, i32 %15
  %16 = load i32, i32* %arrayidx, align 4
  store i32 %16, i32* %maximum_framebits, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %17 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index3 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %17, i32 0, i32 2
  store i32 0, i32* %bitrate_index3, align 4
  %18 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call4 = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %18, i32* %avg)
  store i32 %call4, i32* %maximum_framebits, align 4
  %19 = load i32, i32* %maximum_framebits, align 4
  %20 = load i32*, i32** %frameBits.addr, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %20, i32 0
  store i32 %19, i32* %arrayidx5, align 4
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_enc6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 11
  %ResvMax7 = getelementptr inbounds %struct.EncStateVar_t, %struct.EncStateVar_t* %sv_enc6, i32 0, i32 13
  %22 = load i32, i32* %ResvMax7, align 8
  %23 = load i32*, i32** %max_resv.addr, align 4
  store i32 %22, i32* %23, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc34, %if.end
  %24 = load i32, i32* %gr, align 4
  %25 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %25, i32 0, i32 15
  %26 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %24, %26
  br i1 %cmp, label %for.body, label %for.end36

for.body:                                         ; preds = %for.cond
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %28 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %29 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %30 = load i32, i32* %gr, align 4
  %arrayidx8 = getelementptr inbounds [2 x i32], [2 x i32]* %29, i32 %30
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx8, i32 0, i32 0
  %31 = load i32, i32* %avg, align 4
  %32 = load i32, i32* %gr, align 4
  %call9 = call i32 @on_pe(%struct.lame_internal_flags* %27, [2 x float]* %28, i32* %arraydecay, i32 %31, i32 %32, i32 0)
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc10 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc10, i32 0, i32 5
  %34 = load i32, i32* %mode_ext, align 4
  %cmp11 = icmp eq i32 %34, 2
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.body
  %35 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %35, i32 0, i32 7
  %36 = load i32, i32* %gr, align 4
  call void @ms_convert(%struct.III_side_info_t* %l3_side, i32 %36)
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %for.body
  store i32 0, i32* %ch, align 4
  br label %for.cond14

for.cond14:                                       ; preds = %for.inc, %if.end13
  %37 = load i32, i32* %ch, align 4
  %38 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %38, i32 0, i32 14
  %39 = load i32, i32* %channels_out, align 4
  %cmp15 = icmp slt i32 %37, %39
  br i1 %cmp15, label %for.body16, label %for.end

for.body16:                                       ; preds = %for.cond14
  %40 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side17 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %40, i32 0, i32 7
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %l3_side17, i32 0, i32 0
  %41 = load i32, i32* %gr, align 4
  %arrayidx18 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %41
  %42 = load i32, i32* %ch, align 4
  %arrayidx19 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx18, i32 0, i32 %42
  store %struct.gr_info* %arrayidx19, %struct.gr_info** %cod_info, align 4
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %43, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 3
  %44 = load float, float* %mask_adjust, align 8
  %conv = fpext float %44 to double
  %mul = fmul double %conv, 1.000000e-01
  %45 = call double @llvm.pow.f64(double 1.000000e+01, double %mul)
  %conv20 = fptrunc double %45 to float
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %46, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt21, i32 0, i32 2
  store float %conv20, float* %masking_lower, align 4
  %47 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %48 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @init_outer_loop(%struct.lame_internal_flags* %47, %struct.gr_info* %48)
  %49 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %50 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %51 = load i32, i32* %gr, align 4
  %arrayidx22 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %50, i32 %51
  %52 = load i32, i32* %ch, align 4
  %arrayidx23 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx22, i32 0, i32 %52
  %53 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %54 = load [2 x [39 x float]]*, [2 x [39 x float]]** %l3_xmin.addr, align 4
  %55 = load i32, i32* %gr, align 4
  %arrayidx24 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %54, i32 %55
  %56 = load i32, i32* %ch, align 4
  %arrayidx25 = getelementptr inbounds [2 x [39 x float]], [2 x [39 x float]]* %arrayidx24, i32 0, i32 %56
  %arraydecay26 = getelementptr inbounds [39 x float], [39 x float]* %arrayidx25, i32 0, i32 0
  %call27 = call i32 @calc_xmin(%struct.lame_internal_flags* %49, %struct.III_psy_ratio* %arrayidx23, %struct.gr_info* %53, float* %arraydecay26)
  %cmp28 = icmp ne i32 0, %call27
  br i1 %cmp28, label %if.then30, label %if.end31

if.then30:                                        ; preds = %for.body16
  store i32 0, i32* %analog_silence, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %for.body16
  %57 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %58 = load i32, i32* %gr, align 4
  %arrayidx32 = getelementptr inbounds [2 x i32], [2 x i32]* %57, i32 %58
  %59 = load i32, i32* %ch, align 4
  %arrayidx33 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx32, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx33, align 4
  %61 = load i32, i32* %bits, align 4
  %add = add nsw i32 %61, %60
  store i32 %add, i32* %bits, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end31
  %62 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %62, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond14

for.end:                                          ; preds = %for.cond14
  br label %for.inc34

for.inc34:                                        ; preds = %for.end
  %63 = load i32, i32* %gr, align 4
  %inc35 = add nsw i32 %63, 1
  store i32 %inc35, i32* %gr, align 4
  br label %for.cond

for.end36:                                        ; preds = %for.cond
  store i32 0, i32* %gr, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc61, %for.end36
  %64 = load i32, i32* %gr, align 4
  %65 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr38 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %65, i32 0, i32 15
  %66 = load i32, i32* %mode_gr38, align 4
  %cmp39 = icmp slt i32 %64, %66
  br i1 %cmp39, label %for.body41, label %for.end63

for.body41:                                       ; preds = %for.cond37
  store i32 0, i32* %ch, align 4
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc58, %for.body41
  %67 = load i32, i32* %ch, align 4
  %68 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out43 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %68, i32 0, i32 14
  %69 = load i32, i32* %channels_out43, align 4
  %cmp44 = icmp slt i32 %67, %69
  br i1 %cmp44, label %for.body46, label %for.end60

for.body46:                                       ; preds = %for.cond42
  %70 = load i32, i32* %bits, align 4
  %71 = load i32, i32* %maximum_framebits, align 4
  %cmp47 = icmp sgt i32 %70, %71
  br i1 %cmp47, label %land.lhs.true, label %if.end57

land.lhs.true:                                    ; preds = %for.body46
  %72 = load i32, i32* %bits, align 4
  %cmp49 = icmp sgt i32 %72, 0
  br i1 %cmp49, label %if.then51, label %if.end57

if.then51:                                        ; preds = %land.lhs.true
  %73 = load i32, i32* %maximum_framebits, align 4
  %74 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %75 = load i32, i32* %gr, align 4
  %arrayidx52 = getelementptr inbounds [2 x i32], [2 x i32]* %74, i32 %75
  %76 = load i32, i32* %ch, align 4
  %arrayidx53 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx52, i32 0, i32 %76
  %77 = load i32, i32* %arrayidx53, align 4
  %mul54 = mul nsw i32 %77, %73
  store i32 %mul54, i32* %arrayidx53, align 4
  %78 = load i32, i32* %bits, align 4
  %79 = load [2 x i32]*, [2 x i32]** %max_bits.addr, align 4
  %80 = load i32, i32* %gr, align 4
  %arrayidx55 = getelementptr inbounds [2 x i32], [2 x i32]* %79, i32 %80
  %81 = load i32, i32* %ch, align 4
  %arrayidx56 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx55, i32 0, i32 %81
  %82 = load i32, i32* %arrayidx56, align 4
  %div = sdiv i32 %82, %78
  store i32 %div, i32* %arrayidx56, align 4
  br label %if.end57

if.end57:                                         ; preds = %if.then51, %land.lhs.true, %for.body46
  br label %for.inc58

for.inc58:                                        ; preds = %if.end57
  %83 = load i32, i32* %ch, align 4
  %inc59 = add nsw i32 %83, 1
  store i32 %inc59, i32* %ch, align 4
  br label %for.cond42

for.end60:                                        ; preds = %for.cond42
  br label %for.inc61

for.inc61:                                        ; preds = %for.end60
  %84 = load i32, i32* %gr, align 4
  %inc62 = add nsw i32 %84, 1
  store i32 %inc62, i32* %gr, align 4
  br label %for.cond37

for.end63:                                        ; preds = %for.cond37
  %85 = load i32, i32* %analog_silence, align 4
  %tobool64 = icmp ne i32 %85, 0
  br i1 %tobool64, label %if.then65, label %if.end66

if.then65:                                        ; preds = %for.end63
  %86 = load i32*, i32** %max_resv.addr, align 4
  store i32 0, i32* %86, align 4
  br label %if.end66

if.end66:                                         ; preds = %if.then65, %for.end63
  %87 = load i32, i32* %analog_silence, align 4
  ret i32 %87
}

declare i32 @VBR_encode_frame(%struct.lame_internal_flags*, [2 x [576 x float]]*, [2 x [39 x float]]*, [2 x i32]*) #1

declare void @ResvAdjust(%struct.lame_internal_flags*, %struct.gr_info*) #1

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #1

; Function Attrs: noreturn
declare void @exit(i32) #3

; Function Attrs: noinline nounwind optnone
define hidden void @ABR_iteration_loop(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x %struct.III_psy_ratio]* %ratio) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %l3_xmin = alloca [39 x float], align 16
  %xrpow = alloca [576 x float], align 16
  %targ_bits = alloca [2 x [2 x i32]], align 16
  %mean_bits = alloca i32, align 4
  %max_frame_bits = alloca i32, align 4
  %ch = alloca i32, align 4
  %gr = alloca i32, align 4
  %ath_over = alloca i32, align 4
  %analog_silence_bits = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %adjust = alloca float, align 4
  %masking_lower_db = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  store i32 0, i32* %mean_bits, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %4 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %5 = load float*, float** %ms_ener_ratio.addr, align 4
  %arraydecay = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %targ_bits, i32 0, i32 0
  call void @calc_target_bits(%struct.lame_internal_flags* %3, [2 x float]* %4, float* %5, [2 x i32]* %arraydecay, i32* %analog_silence_bits, i32* %max_frame_bits)
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc35, %entry
  %6 = load i32, i32* %gr, align 4
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 15
  %8 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %6, %8
  br i1 %cmp, label %for.body, label %for.end37

for.body:                                         ; preds = %for.cond
  %9 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc3 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %9, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc3, i32 0, i32 5
  %10 = load i32, i32* %mode_ext, align 4
  %cmp4 = icmp eq i32 %10, 2
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 7
  %12 = load i32, i32* %gr, align 4
  call void @ms_convert(%struct.III_side_info_t* %l3_side5, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  store i32 0, i32* %ch, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %if.end
  %13 = load i32, i32* %ch, align 4
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 14
  %15 = load i32, i32* %channels_out, align 4
  %cmp7 = icmp slt i32 %13, %15
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %16 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %16, i32 0, i32 0
  %17 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %17
  %18 = load i32, i32* %ch, align 4
  %arrayidx9 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 %18
  store %struct.gr_info* %arrayidx9, %struct.gr_info** %cod_info, align 4
  %19 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %19, i32 0, i32 9
  %20 = load i32, i32* %block_type, align 4
  %cmp10 = icmp ne i32 %20, 2
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %for.body8
  store float 0.000000e+00, float* %adjust, align 4
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 3
  %22 = load float, float* %mask_adjust, align 8
  %23 = load float, float* %adjust, align 4
  %sub = fsub float %22, %23
  store float %sub, float* %masking_lower_db, align 4
  br label %if.end14

if.else:                                          ; preds = %for.body8
  store float 0.000000e+00, float* %adjust, align 4
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %24, i32 0, i32 13
  %mask_adjust_short = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt12, i32 0, i32 4
  %25 = load float, float* %mask_adjust_short, align 4
  %26 = load float, float* %adjust, align 4
  %sub13 = fsub float %25, %26
  store float %sub13, float* %masking_lower_db, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.else, %if.then11
  %27 = load float, float* %masking_lower_db, align 4
  %conv = fpext float %27 to double
  %mul = fmul double %conv, 1.000000e-01
  %28 = call double @llvm.pow.f64(double 1.000000e+01, double %mul)
  %conv15 = fptrunc double %28 to float
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt16, i32 0, i32 2
  store float %conv15, float* %masking_lower, align 4
  %30 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %31 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @init_outer_loop(%struct.lame_internal_flags* %30, %struct.gr_info* %31)
  %32 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %33 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay17 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %call = call i32 @init_xrpow(%struct.lame_internal_flags* %32, %struct.gr_info* %33, float* %arraydecay17)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then18, label %if.end34

if.then18:                                        ; preds = %if.end14
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %35 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %36 = load i32, i32* %gr, align 4
  %arrayidx19 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %35, i32 %36
  %37 = load i32, i32* %ch, align 4
  %arrayidx20 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx19, i32 0, i32 %37
  %38 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay21 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %call22 = call i32 @calc_xmin(%struct.lame_internal_flags* %34, %struct.III_psy_ratio* %arrayidx20, %struct.gr_info* %38, float* %arraydecay21)
  store i32 %call22, i32* %ath_over, align 4
  %39 = load i32, i32* %ath_over, align 4
  %cmp23 = icmp eq i32 0, %39
  br i1 %cmp23, label %if.then25, label %if.end28

if.then25:                                        ; preds = %if.then18
  %40 = load i32, i32* %analog_silence_bits, align 4
  %41 = load i32, i32* %gr, align 4
  %arrayidx26 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %targ_bits, i32 0, i32 %41
  %42 = load i32, i32* %ch, align 4
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx26, i32 0, i32 %42
  store i32 %40, i32* %arrayidx27, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then25, %if.then18
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %44 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay29 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %arraydecay30 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %45 = load i32, i32* %ch, align 4
  %46 = load i32, i32* %gr, align 4
  %arrayidx31 = getelementptr inbounds [2 x [2 x i32]], [2 x [2 x i32]]* %targ_bits, i32 0, i32 %46
  %47 = load i32, i32* %ch, align 4
  %arrayidx32 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx31, i32 0, i32 %47
  %48 = load i32, i32* %arrayidx32, align 4
  %call33 = call i32 @outer_loop(%struct.lame_internal_flags* %43, %struct.gr_info* %44, float* %arraydecay29, float* %arraydecay30, i32 %45, i32 %48)
  br label %if.end34

if.end34:                                         ; preds = %if.end28, %if.end14
  %49 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %50 = load i32, i32* %gr, align 4
  %51 = load i32, i32* %ch, align 4
  call void @iteration_finish_one(%struct.lame_internal_flags* %49, i32 %50, i32 %51)
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %52 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %52, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc35

for.inc35:                                        ; preds = %for.end
  %53 = load i32, i32* %gr, align 4
  %inc36 = add nsw i32 %53, 1
  store i32 %inc36, i32* %gr, align 4
  br label %for.cond

for.end37:                                        ; preds = %for.cond
  %54 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %54, i32 0, i32 24
  %55 = load i32, i32* %vbr_min_bitrate_index, align 4
  %56 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %56, i32 0, i32 2
  store i32 %55, i32* %bitrate_index, align 4
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc48, %for.end37
  %57 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index39 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %57, i32 0, i32 2
  %58 = load i32, i32* %bitrate_index39, align 4
  %59 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %59, i32 0, i32 25
  %60 = load i32, i32* %vbr_max_bitrate_index, align 4
  %cmp40 = icmp sle i32 %58, %60
  br i1 %cmp40, label %for.body42, label %for.end51

for.body42:                                       ; preds = %for.cond38
  %61 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call43 = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %61, i32* %mean_bits)
  %cmp44 = icmp sge i32 %call43, 0
  br i1 %cmp44, label %if.then46, label %if.end47

if.then46:                                        ; preds = %for.body42
  br label %for.end51

if.end47:                                         ; preds = %for.body42
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47
  %62 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index49 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %62, i32 0, i32 2
  %63 = load i32, i32* %bitrate_index49, align 4
  %inc50 = add nsw i32 %63, 1
  store i32 %inc50, i32* %bitrate_index49, align 4
  br label %for.cond38

for.end51:                                        ; preds = %if.then46, %for.cond38
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %65 = load i32, i32* %mean_bits, align 4
  call void @ResvFrameEnd(%struct.lame_internal_flags* %64, i32 %65)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @calc_target_bits(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x i32]* %targ_bits, i32* %analog_silence_bits, i32* %max_frame_bits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %targ_bits.addr = alloca [2 x i32]*, align 4
  %analog_silence_bits.addr = alloca i32*, align 4
  %max_frame_bits.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %res_factor = alloca float, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %totbits = alloca i32, align 4
  %mean_bits = alloca i32, align 4
  %framesize = alloca i32, align 4
  %sum = alloca i32, align 4
  %add_bits = alloca i32, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x i32]* %targ_bits, [2 x i32]** %targ_bits.addr, align 4
  store i32* %analog_silence_bits, i32** %analog_silence_bits.addr, align 4
  store i32* %max_frame_bits, i32** %max_frame_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 15
  %4 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 576, %4
  store i32 %mul, i32* %framesize, align 4
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 25
  %6 = load i32, i32* %vbr_max_bitrate_index, align 4
  %7 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %7, i32 0, i32 2
  store i32 %6, i32* %bitrate_index, align 4
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %8, i32* %mean_bits)
  %9 = load i32*, i32** %max_frame_bits.addr, align 4
  store i32 %call, i32* %9, align 4
  %10 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index3 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %10, i32 0, i32 2
  store i32 1, i32* %bitrate_index3, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call4 = call i32 @getframebits(%struct.lame_internal_flags* %11)
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 2
  %13 = load i32, i32* %sideinfo_len, align 4
  %mul5 = mul nsw i32 %13, 8
  %sub = sub nsw i32 %call4, %mul5
  store i32 %sub, i32* %mean_bits, align 4
  %14 = load i32, i32* %mean_bits, align 4
  %15 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr6 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %15, i32 0, i32 15
  %16 = load i32, i32* %mode_gr6, align 4
  %17 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %17, i32 0, i32 14
  %18 = load i32, i32* %channels_out, align 4
  %mul7 = mul nsw i32 %16, %18
  %div = sdiv i32 %14, %mul7
  %19 = load i32*, i32** %analog_silence_bits.addr, align 4
  store i32 %div, i32* %19, align 4
  %20 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_avg_bitrate_kbps = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %20, i32 0, i32 23
  %21 = load i32, i32* %vbr_avg_bitrate_kbps, align 4
  %22 = load i32, i32* %framesize, align 4
  %mul8 = mul nsw i32 %21, %22
  %mul9 = mul nsw i32 %mul8, 1000
  store i32 %mul9, i32* %mean_bits, align 4
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %24 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %24, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %25 = load i32, i32* %mean_bits, align 4
  %conv = sitofp i32 %25 to double
  %mul10 = fmul double %conv, 1.090000e+00
  %conv11 = fptosi double %mul10 to i32
  store i32 %conv11, i32* %mean_bits, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %26, i32 0, i32 12
  %27 = load i32, i32* %samplerate_out, align 4
  %28 = load i32, i32* %mean_bits, align 4
  %div12 = sdiv i32 %28, %27
  store i32 %div12, i32* %mean_bits, align 4
  %29 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len13 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %29, i32 0, i32 2
  %30 = load i32, i32* %sideinfo_len13, align 4
  %mul14 = mul nsw i32 %30, 8
  %31 = load i32, i32* %mean_bits, align 4
  %sub15 = sub nsw i32 %31, %mul14
  store i32 %sub15, i32* %mean_bits, align 4
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr16 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 15
  %33 = load i32, i32* %mode_gr16, align 4
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out17 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 14
  %35 = load i32, i32* %channels_out17, align 4
  %mul18 = mul nsw i32 %33, %35
  %36 = load i32, i32* %mean_bits, align 4
  %div19 = sdiv i32 %36, %mul18
  store i32 %div19, i32* %mean_bits, align 4
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %compression_ratio = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %37, i32 0, i32 57
  %38 = load float, float* %compression_ratio, align 4
  %conv20 = fpext float %38 to double
  %sub21 = fsub double 1.100000e+01, %conv20
  %mul22 = fmul double 7.000000e-02, %sub21
  %div23 = fdiv double %mul22, 5.500000e+00
  %add = fadd double 9.300000e-01, %div23
  %conv24 = fptrunc double %add to float
  store float %conv24, float* %res_factor, align 4
  %39 = load float, float* %res_factor, align 4
  %conv25 = fpext float %39 to double
  %cmp = fcmp olt double %conv25, 9.000000e-01
  br i1 %cmp, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.end
  store float 0x3FECCCCCC0000000, float* %res_factor, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.end
  %40 = load float, float* %res_factor, align 4
  %conv29 = fpext float %40 to double
  %cmp30 = fcmp ogt double %conv29, 1.000000e+00
  br i1 %cmp30, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end28
  store float 1.000000e+00, float* %res_factor, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.end28
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc119, %if.end33
  %41 = load i32, i32* %gr, align 4
  %42 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr34 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %42, i32 0, i32 15
  %43 = load i32, i32* %mode_gr34, align 4
  %cmp35 = icmp slt i32 %41, %43
  br i1 %cmp35, label %for.body, label %for.end121

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %sum, align 4
  store i32 0, i32* %ch, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %for.body
  %44 = load i32, i32* %ch, align 4
  %45 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out38 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %45, i32 0, i32 14
  %46 = load i32, i32* %channels_out38, align 4
  %cmp39 = icmp slt i32 %44, %46
  br i1 %cmp39, label %for.body41, label %for.end

for.body41:                                       ; preds = %for.cond37
  %47 = load float, float* %res_factor, align 4
  %48 = load i32, i32* %mean_bits, align 4
  %conv42 = sitofp i32 %48 to float
  %mul43 = fmul float %47, %conv42
  %conv44 = fptosi float %mul43 to i32
  %49 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %50 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %49, i32 %50
  %51 = load i32, i32* %ch, align 4
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 %51
  store i32 %conv44, i32* %arrayidx45, align 4
  %52 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %53 = load i32, i32* %gr, align 4
  %arrayidx46 = getelementptr inbounds [2 x float], [2 x float]* %52, i32 %53
  %54 = load i32, i32* %ch, align 4
  %arrayidx47 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx46, i32 0, i32 %54
  %55 = load float, float* %arrayidx47, align 4
  %cmp48 = fcmp ogt float %55, 7.000000e+02
  br i1 %cmp48, label %if.then50, label %if.end89

if.then50:                                        ; preds = %for.body41
  %56 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %57 = load i32, i32* %gr, align 4
  %arrayidx51 = getelementptr inbounds [2 x float], [2 x float]* %56, i32 %57
  %58 = load i32, i32* %ch, align 4
  %arrayidx52 = getelementptr inbounds [2 x float], [2 x float]* %arrayidx51, i32 0, i32 %58
  %59 = load float, float* %arrayidx52, align 4
  %sub53 = fsub float %59, 7.000000e+02
  %conv54 = fpext float %sub53 to double
  %div55 = fdiv double %conv54, 1.400000e+00
  %conv56 = fptosi double %div55 to i32
  store i32 %conv56, i32* %add_bits, align 4
  %60 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %60, i32 0, i32 0
  %61 = load i32, i32* %gr, align 4
  %arrayidx57 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %61
  %62 = load i32, i32* %ch, align 4
  %arrayidx58 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx57, i32 0, i32 %62
  store %struct.gr_info* %arrayidx58, %struct.gr_info** %cod_info, align 4
  %63 = load float, float* %res_factor, align 4
  %64 = load i32, i32* %mean_bits, align 4
  %conv59 = sitofp i32 %64 to float
  %mul60 = fmul float %63, %conv59
  %conv61 = fptosi float %mul60 to i32
  %65 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %66 = load i32, i32* %gr, align 4
  %arrayidx62 = getelementptr inbounds [2 x i32], [2 x i32]* %65, i32 %66
  %67 = load i32, i32* %ch, align 4
  %arrayidx63 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx62, i32 0, i32 %67
  store i32 %conv61, i32* %arrayidx63, align 4
  %68 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %68, i32 0, i32 9
  %69 = load i32, i32* %block_type, align 4
  %cmp64 = icmp eq i32 %69, 2
  br i1 %cmp64, label %if.then66, label %if.end73

if.then66:                                        ; preds = %if.then50
  %70 = load i32, i32* %add_bits, align 4
  %71 = load i32, i32* %mean_bits, align 4
  %div67 = sdiv i32 %71, 2
  %cmp68 = icmp slt i32 %70, %div67
  br i1 %cmp68, label %if.then70, label %if.end72

if.then70:                                        ; preds = %if.then66
  %72 = load i32, i32* %mean_bits, align 4
  %div71 = sdiv i32 %72, 2
  store i32 %div71, i32* %add_bits, align 4
  br label %if.end72

if.end72:                                         ; preds = %if.then70, %if.then66
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %if.then50
  %73 = load i32, i32* %add_bits, align 4
  %74 = load i32, i32* %mean_bits, align 4
  %mul74 = mul nsw i32 %74, 3
  %div75 = sdiv i32 %mul74, 2
  %cmp76 = icmp sgt i32 %73, %div75
  br i1 %cmp76, label %if.then78, label %if.else

if.then78:                                        ; preds = %if.end73
  %75 = load i32, i32* %mean_bits, align 4
  %mul79 = mul nsw i32 %75, 3
  %div80 = sdiv i32 %mul79, 2
  store i32 %div80, i32* %add_bits, align 4
  br label %if.end85

if.else:                                          ; preds = %if.end73
  %76 = load i32, i32* %add_bits, align 4
  %cmp81 = icmp slt i32 %76, 0
  br i1 %cmp81, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.else
  store i32 0, i32* %add_bits, align 4
  br label %if.end84

if.end84:                                         ; preds = %if.then83, %if.else
  br label %if.end85

if.end85:                                         ; preds = %if.end84, %if.then78
  %77 = load i32, i32* %add_bits, align 4
  %78 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %79 = load i32, i32* %gr, align 4
  %arrayidx86 = getelementptr inbounds [2 x i32], [2 x i32]* %78, i32 %79
  %80 = load i32, i32* %ch, align 4
  %arrayidx87 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx86, i32 0, i32 %80
  %81 = load i32, i32* %arrayidx87, align 4
  %add88 = add nsw i32 %81, %77
  store i32 %add88, i32* %arrayidx87, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.end85, %for.body41
  %82 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %83 = load i32, i32* %gr, align 4
  %arrayidx90 = getelementptr inbounds [2 x i32], [2 x i32]* %82, i32 %83
  %84 = load i32, i32* %ch, align 4
  %arrayidx91 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx90, i32 0, i32 %84
  %85 = load i32, i32* %arrayidx91, align 4
  %cmp92 = icmp sgt i32 %85, 4095
  br i1 %cmp92, label %if.then94, label %if.end97

if.then94:                                        ; preds = %if.end89
  %86 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %87 = load i32, i32* %gr, align 4
  %arrayidx95 = getelementptr inbounds [2 x i32], [2 x i32]* %86, i32 %87
  %88 = load i32, i32* %ch, align 4
  %arrayidx96 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx95, i32 0, i32 %88
  store i32 4095, i32* %arrayidx96, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then94, %if.end89
  %89 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %90 = load i32, i32* %gr, align 4
  %arrayidx98 = getelementptr inbounds [2 x i32], [2 x i32]* %89, i32 %90
  %91 = load i32, i32* %ch, align 4
  %arrayidx99 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx98, i32 0, i32 %91
  %92 = load i32, i32* %arrayidx99, align 4
  %93 = load i32, i32* %sum, align 4
  %add100 = add nsw i32 %93, %92
  store i32 %add100, i32* %sum, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end97
  %94 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %94, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  %95 = load i32, i32* %sum, align 4
  %cmp101 = icmp sgt i32 %95, 7680
  br i1 %cmp101, label %if.then103, label %if.end118

if.then103:                                       ; preds = %for.end
  store i32 0, i32* %ch, align 4
  br label %for.cond104

for.cond104:                                      ; preds = %for.inc115, %if.then103
  %96 = load i32, i32* %ch, align 4
  %97 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out105 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %97, i32 0, i32 14
  %98 = load i32, i32* %channels_out105, align 4
  %cmp106 = icmp slt i32 %96, %98
  br i1 %cmp106, label %for.body108, label %for.end117

for.body108:                                      ; preds = %for.cond104
  %99 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %100 = load i32, i32* %gr, align 4
  %arrayidx109 = getelementptr inbounds [2 x i32], [2 x i32]* %99, i32 %100
  %101 = load i32, i32* %ch, align 4
  %arrayidx110 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx109, i32 0, i32 %101
  %102 = load i32, i32* %arrayidx110, align 4
  %mul111 = mul nsw i32 %102, 7680
  store i32 %mul111, i32* %arrayidx110, align 4
  %103 = load i32, i32* %sum, align 4
  %104 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %105 = load i32, i32* %gr, align 4
  %arrayidx112 = getelementptr inbounds [2 x i32], [2 x i32]* %104, i32 %105
  %106 = load i32, i32* %ch, align 4
  %arrayidx113 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx112, i32 0, i32 %106
  %107 = load i32, i32* %arrayidx113, align 4
  %div114 = sdiv i32 %107, %103
  store i32 %div114, i32* %arrayidx113, align 4
  br label %for.inc115

for.inc115:                                       ; preds = %for.body108
  %108 = load i32, i32* %ch, align 4
  %inc116 = add nsw i32 %108, 1
  store i32 %inc116, i32* %ch, align 4
  br label %for.cond104

for.end117:                                       ; preds = %for.cond104
  br label %if.end118

if.end118:                                        ; preds = %for.end117, %for.end
  br label %for.inc119

for.inc119:                                       ; preds = %if.end118
  %109 = load i32, i32* %gr, align 4
  %inc120 = add nsw i32 %109, 1
  store i32 %inc120, i32* %gr, align 4
  br label %for.cond

for.end121:                                       ; preds = %for.cond
  %110 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc122 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %110, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc122, i32 0, i32 5
  %111 = load i32, i32* %mode_ext, align 4
  %cmp123 = icmp eq i32 %111, 2
  br i1 %cmp123, label %if.then125, label %if.end138

if.then125:                                       ; preds = %for.end121
  store i32 0, i32* %gr, align 4
  br label %for.cond126

for.cond126:                                      ; preds = %for.inc135, %if.then125
  %112 = load i32, i32* %gr, align 4
  %113 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr127 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %113, i32 0, i32 15
  %114 = load i32, i32* %mode_gr127, align 4
  %cmp128 = icmp slt i32 %112, %114
  br i1 %cmp128, label %for.body130, label %for.end137

for.body130:                                      ; preds = %for.cond126
  %115 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %116 = load i32, i32* %gr, align 4
  %arrayidx131 = getelementptr inbounds [2 x i32], [2 x i32]* %115, i32 %116
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx131, i32 0, i32 0
  %117 = load float*, float** %ms_ener_ratio.addr, align 4
  %118 = load i32, i32* %gr, align 4
  %arrayidx132 = getelementptr inbounds float, float* %117, i32 %118
  %119 = load float, float* %arrayidx132, align 4
  %120 = load i32, i32* %mean_bits, align 4
  %121 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out133 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %121, i32 0, i32 14
  %122 = load i32, i32* %channels_out133, align 4
  %mul134 = mul nsw i32 %120, %122
  call void @reduce_side(i32* %arraydecay, float %119, i32 %mul134, i32 7680)
  br label %for.inc135

for.inc135:                                       ; preds = %for.body130
  %123 = load i32, i32* %gr, align 4
  %inc136 = add nsw i32 %123, 1
  store i32 %inc136, i32* %gr, align 4
  br label %for.cond126

for.end137:                                       ; preds = %for.cond126
  br label %if.end138

if.end138:                                        ; preds = %for.end137, %for.end121
  store i32 0, i32* %totbits, align 4
  store i32 0, i32* %gr, align 4
  br label %for.cond139

for.cond139:                                      ; preds = %for.inc163, %if.end138
  %124 = load i32, i32* %gr, align 4
  %125 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr140 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %125, i32 0, i32 15
  %126 = load i32, i32* %mode_gr140, align 4
  %cmp141 = icmp slt i32 %124, %126
  br i1 %cmp141, label %for.body143, label %for.end165

for.body143:                                      ; preds = %for.cond139
  store i32 0, i32* %ch, align 4
  br label %for.cond144

for.cond144:                                      ; preds = %for.inc160, %for.body143
  %127 = load i32, i32* %ch, align 4
  %128 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out145 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %128, i32 0, i32 14
  %129 = load i32, i32* %channels_out145, align 4
  %cmp146 = icmp slt i32 %127, %129
  br i1 %cmp146, label %for.body148, label %for.end162

for.body148:                                      ; preds = %for.cond144
  %130 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %131 = load i32, i32* %gr, align 4
  %arrayidx149 = getelementptr inbounds [2 x i32], [2 x i32]* %130, i32 %131
  %132 = load i32, i32* %ch, align 4
  %arrayidx150 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx149, i32 0, i32 %132
  %133 = load i32, i32* %arrayidx150, align 4
  %cmp151 = icmp sgt i32 %133, 4095
  br i1 %cmp151, label %if.then153, label %if.end156

if.then153:                                       ; preds = %for.body148
  %134 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %135 = load i32, i32* %gr, align 4
  %arrayidx154 = getelementptr inbounds [2 x i32], [2 x i32]* %134, i32 %135
  %136 = load i32, i32* %ch, align 4
  %arrayidx155 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx154, i32 0, i32 %136
  store i32 4095, i32* %arrayidx155, align 4
  br label %if.end156

if.end156:                                        ; preds = %if.then153, %for.body148
  %137 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %138 = load i32, i32* %gr, align 4
  %arrayidx157 = getelementptr inbounds [2 x i32], [2 x i32]* %137, i32 %138
  %139 = load i32, i32* %ch, align 4
  %arrayidx158 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx157, i32 0, i32 %139
  %140 = load i32, i32* %arrayidx158, align 4
  %141 = load i32, i32* %totbits, align 4
  %add159 = add nsw i32 %141, %140
  store i32 %add159, i32* %totbits, align 4
  br label %for.inc160

for.inc160:                                       ; preds = %if.end156
  %142 = load i32, i32* %ch, align 4
  %inc161 = add nsw i32 %142, 1
  store i32 %inc161, i32* %ch, align 4
  br label %for.cond144

for.end162:                                       ; preds = %for.cond144
  br label %for.inc163

for.inc163:                                       ; preds = %for.end162
  %143 = load i32, i32* %gr, align 4
  %inc164 = add nsw i32 %143, 1
  store i32 %inc164, i32* %gr, align 4
  br label %for.cond139

for.end165:                                       ; preds = %for.cond139
  %144 = load i32, i32* %totbits, align 4
  %145 = load i32*, i32** %max_frame_bits.addr, align 4
  %146 = load i32, i32* %145, align 4
  %cmp166 = icmp sgt i32 %144, %146
  br i1 %cmp166, label %land.lhs.true, label %if.end193

land.lhs.true:                                    ; preds = %for.end165
  %147 = load i32, i32* %totbits, align 4
  %cmp168 = icmp sgt i32 %147, 0
  br i1 %cmp168, label %if.then170, label %if.end193

if.then170:                                       ; preds = %land.lhs.true
  store i32 0, i32* %gr, align 4
  br label %for.cond171

for.cond171:                                      ; preds = %for.inc190, %if.then170
  %148 = load i32, i32* %gr, align 4
  %149 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr172 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %149, i32 0, i32 15
  %150 = load i32, i32* %mode_gr172, align 4
  %cmp173 = icmp slt i32 %148, %150
  br i1 %cmp173, label %for.body175, label %for.end192

for.body175:                                      ; preds = %for.cond171
  store i32 0, i32* %ch, align 4
  br label %for.cond176

for.cond176:                                      ; preds = %for.inc187, %for.body175
  %151 = load i32, i32* %ch, align 4
  %152 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out177 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %152, i32 0, i32 14
  %153 = load i32, i32* %channels_out177, align 4
  %cmp178 = icmp slt i32 %151, %153
  br i1 %cmp178, label %for.body180, label %for.end189

for.body180:                                      ; preds = %for.cond176
  %154 = load i32*, i32** %max_frame_bits.addr, align 4
  %155 = load i32, i32* %154, align 4
  %156 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %157 = load i32, i32* %gr, align 4
  %arrayidx181 = getelementptr inbounds [2 x i32], [2 x i32]* %156, i32 %157
  %158 = load i32, i32* %ch, align 4
  %arrayidx182 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx181, i32 0, i32 %158
  %159 = load i32, i32* %arrayidx182, align 4
  %mul183 = mul nsw i32 %159, %155
  store i32 %mul183, i32* %arrayidx182, align 4
  %160 = load i32, i32* %totbits, align 4
  %161 = load [2 x i32]*, [2 x i32]** %targ_bits.addr, align 4
  %162 = load i32, i32* %gr, align 4
  %arrayidx184 = getelementptr inbounds [2 x i32], [2 x i32]* %161, i32 %162
  %163 = load i32, i32* %ch, align 4
  %arrayidx185 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx184, i32 0, i32 %163
  %164 = load i32, i32* %arrayidx185, align 4
  %div186 = sdiv i32 %164, %160
  store i32 %div186, i32* %arrayidx185, align 4
  br label %for.inc187

for.inc187:                                       ; preds = %for.body180
  %165 = load i32, i32* %ch, align 4
  %inc188 = add nsw i32 %165, 1
  store i32 %inc188, i32* %ch, align 4
  br label %for.cond176

for.end189:                                       ; preds = %for.cond176
  br label %for.inc190

for.inc190:                                       ; preds = %for.end189
  %166 = load i32, i32* %gr, align 4
  %inc191 = add nsw i32 %166, 1
  store i32 %inc191, i32* %gr, align 4
  br label %for.cond171

for.end192:                                       ; preds = %for.cond171
  br label %if.end193

if.end193:                                        ; preds = %for.end192, %land.lhs.true, %for.end165
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @ms_convert(%struct.III_side_info_t* %l3_side, i32 %gr) #0 {
entry:
  %l3_side.addr = alloca %struct.III_side_info_t*, align 4
  %gr.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %l = alloca float, align 4
  %r = alloca float, align 4
  store %struct.III_side_info_t* %l3_side, %struct.III_side_info_t** %l3_side.addr, align 4
  store i32 %gr, i32* %gr.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 576
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %1, i32 0, i32 0
  %2 = load i32, i32* %gr.addr, align 4
  %arrayidx = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %2
  %arrayidx1 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx, i32 0, i32 0
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx1, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %3
  %4 = load float, float* %arrayidx2, align 4
  store float %4, float* %l, align 4
  %5 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt3 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %5, i32 0, i32 0
  %6 = load i32, i32* %gr.addr, align 4
  %arrayidx4 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt3, i32 0, i32 %6
  %arrayidx5 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx4, i32 0, i32 1
  %xr6 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx5, i32 0, i32 0
  %7 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [576 x float], [576 x float]* %xr6, i32 0, i32 %7
  %8 = load float, float* %arrayidx7, align 4
  store float %8, float* %r, align 4
  %9 = load float, float* %l, align 4
  %10 = load float, float* %r, align 4
  %add = fadd float %9, %10
  %mul = fmul float %add, 0x3FE6A09E60000000
  %11 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt8 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %11, i32 0, i32 0
  %12 = load i32, i32* %gr.addr, align 4
  %arrayidx9 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt8, i32 0, i32 %12
  %arrayidx10 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx9, i32 0, i32 0
  %xr11 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx10, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [576 x float], [576 x float]* %xr11, i32 0, i32 %13
  store float %mul, float* %arrayidx12, align 4
  %14 = load float, float* %l, align 4
  %15 = load float, float* %r, align 4
  %sub = fsub float %14, %15
  %mul13 = fmul float %sub, 0x3FE6A09E60000000
  %16 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side.addr, align 4
  %tt14 = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %16, i32 0, i32 0
  %17 = load i32, i32* %gr.addr, align 4
  %arrayidx15 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt14, i32 0, i32 %17
  %arrayidx16 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx15, i32 0, i32 1
  %xr17 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %arrayidx16, i32 0, i32 0
  %18 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds [576 x float], [576 x float]* %xr17, i32 0, i32 %18
  store float %mul13, float* %arrayidx18, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #4

; Function Attrs: noinline nounwind optnone
define internal void @init_outer_loop(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %sfb = alloca i32, align 4
  %j = alloca i32, align 4
  %ixwork = alloca [576 x float], align 16
  %ix = alloca float*, align 4
  %start = alloca i32, align 4
  %end = alloca i32, align 4
  %window90 = alloca i32, align 4
  %l91 = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 4
  store i32 0, i32* %part2_3_length, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %big_values = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 5
  store i32 0, i32* %big_values, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %count1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 6
  store i32 0, i32* %count1, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 7
  store i32 210, i32* %global_gain, align 4
  %5 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_compress = getelementptr inbounds %struct.gr_info, %struct.gr_info* %5, i32 0, i32 8
  store i32 0, i32* %scalefac_compress, align 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 11
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %table_select, i32 0, i32 0
  store i32 0, i32* %arrayidx, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %table_select2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 11
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select2, i32 0, i32 1
  store i32 0, i32* %arrayidx3, align 4
  %8 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %table_select4 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 11
  %arrayidx5 = getelementptr inbounds [3 x i32], [3 x i32]* %table_select4, i32 0, i32 2
  store i32 0, i32* %arrayidx5, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 12
  %arrayidx6 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 0
  store i32 0, i32* %arrayidx6, align 4
  %10 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain7 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %10, i32 0, i32 12
  %arrayidx8 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain7, i32 0, i32 1
  store i32 0, i32* %arrayidx8, align 4
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain9 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 12
  %arrayidx10 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain9, i32 0, i32 2
  store i32 0, i32* %arrayidx10, align 4
  %12 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain11 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %12, i32 0, i32 12
  %arrayidx12 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain11, i32 0, i32 3
  store i32 0, i32* %arrayidx12, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %region0_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 13
  store i32 0, i32* %region0_count, align 4
  %14 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %region1_count = getelementptr inbounds %struct.gr_info, %struct.gr_info* %14, i32 0, i32 14
  store i32 0, i32* %region1_count, align 4
  %15 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 15
  store i32 0, i32* %preflag, align 4
  %16 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %16, i32 0, i32 16
  store i32 0, i32* %scalefac_scale, align 4
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %count1table_select = getelementptr inbounds %struct.gr_info, %struct.gr_info* %17, i32 0, i32 17
  store i32 0, i32* %count1table_select, align 4
  %18 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %18, i32 0, i32 18
  store i32 0, i32* %part2_length, align 4
  %19 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %19, i32 0, i32 12
  %20 = load i32, i32* %samplerate_out, align 4
  %cmp = icmp sle i32 %20, 8000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %21 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %21, i32 0, i32 19
  store i32 17, i32* %sfb_lmax, align 4
  %22 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin = getelementptr inbounds %struct.gr_info, %struct.gr_info* %22, i32 0, i32 20
  store i32 9, i32* %sfb_smin, align 4
  %23 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psy_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %23, i32 0, i32 21
  store i32 17, i32* %psy_lmax, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %24 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax13 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 19
  store i32 21, i32* %sfb_lmax13, align 4
  %25 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin14 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %25, i32 0, i32 20
  store i32 12, i32* %sfb_smin14, align 4
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 13
  %sfb21_extra = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 8
  %27 = load i32, i32* %sfb21_extra, align 4
  %tobool = icmp ne i32 %27, 0
  %28 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 22, i32 21
  %29 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psy_lmax15 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %29, i32 0, i32 21
  store i32 %cond, i32* %psy_lmax15, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %30 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psy_lmax16 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 21
  %31 = load i32, i32* %psy_lmax16, align 4
  %32 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %32, i32 0, i32 23
  store i32 %31, i32* %psymax, align 4
  %33 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax17 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %33, i32 0, i32 19
  %34 = load i32, i32* %sfb_lmax17, align 4
  %35 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %35, i32 0, i32 22
  store i32 %34, i32* %sfbmax, align 4
  %36 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbdivide = getelementptr inbounds %struct.gr_info, %struct.gr_info* %36, i32 0, i32 24
  store i32 11, i32* %sfbdivide, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %37 = load i32, i32* %sfb, align 4
  %cmp18 = icmp slt i32 %37, 22
  br i1 %cmp18, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %38, i32 0, i32 8
  %l = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %39 = load i32, i32* %sfb, align 4
  %add = add nsw i32 %39, 1
  %arrayidx19 = getelementptr inbounds [23 x i32], [23 x i32]* %l, i32 0, i32 %add
  %40 = load i32, i32* %arrayidx19, align 4
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 8
  %l21 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band20, i32 0, i32 0
  %42 = load i32, i32* %sfb, align 4
  %arrayidx22 = getelementptr inbounds [23 x i32], [23 x i32]* %l21, i32 0, i32 %42
  %43 = load i32, i32* %arrayidx22, align 4
  %sub = sub nsw i32 %40, %43
  %44 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width = getelementptr inbounds %struct.gr_info, %struct.gr_info* %44, i32 0, i32 25
  %45 = load i32, i32* %sfb, align 4
  %arrayidx23 = getelementptr inbounds [39 x i32], [39 x i32]* %width, i32 0, i32 %45
  store i32 %sub, i32* %arrayidx23, align 4
  %46 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window = getelementptr inbounds %struct.gr_info, %struct.gr_info* %46, i32 0, i32 26
  %47 = load i32, i32* %sfb, align 4
  %arrayidx24 = getelementptr inbounds [39 x i32], [39 x i32]* %window, i32 0, i32 %47
  store i32 3, i32* %arrayidx24, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %48, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %49 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %49, i32 0, i32 9
  %50 = load i32, i32* %block_type, align 4
  %cmp25 = icmp eq i32 %50, 2
  br i1 %cmp25, label %if.then26, label %if.end143

if.then26:                                        ; preds = %for.end
  %51 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %51, i32 0, i32 20
  store i32 0, i32* %sfb_smin27, align 4
  %52 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax28 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %52, i32 0, i32 19
  store i32 0, i32* %sfb_lmax28, align 4
  %53 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %mixed_block_flag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %53, i32 0, i32 10
  %54 = load i32, i32* %mixed_block_flag, align 4
  %tobool29 = icmp ne i32 %54, 0
  br i1 %tobool29, label %if.then30, label %if.end34

if.then30:                                        ; preds = %if.then26
  %55 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin31 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %55, i32 0, i32 20
  store i32 3, i32* %sfb_smin31, align 4
  %56 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %56, i32 0, i32 15
  %57 = load i32, i32* %mode_gr, align 4
  %mul = mul nsw i32 %57, 2
  %add32 = add nsw i32 %mul, 4
  %58 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax33 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %58, i32 0, i32 19
  store i32 %add32, i32* %sfb_lmax33, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.then30, %if.then26
  %59 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out35 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %59, i32 0, i32 12
  %60 = load i32, i32* %samplerate_out35, align 4
  %cmp36 = icmp sle i32 %60, 8000
  br i1 %cmp36, label %if.then37, label %if.else50

if.then37:                                        ; preds = %if.end34
  %61 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax38 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %61, i32 0, i32 19
  %62 = load i32, i32* %sfb_lmax38, align 4
  %63 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin39 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %63, i32 0, i32 20
  %64 = load i32, i32* %sfb_smin39, align 4
  %sub40 = sub nsw i32 9, %64
  %mul41 = mul nsw i32 3, %sub40
  %add42 = add nsw i32 %62, %mul41
  %65 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax43 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %65, i32 0, i32 23
  store i32 %add42, i32* %psymax43, align 4
  %66 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax44 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %66, i32 0, i32 19
  %67 = load i32, i32* %sfb_lmax44, align 4
  %68 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin45 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %68, i32 0, i32 20
  %69 = load i32, i32* %sfb_smin45, align 4
  %sub46 = sub nsw i32 9, %69
  %mul47 = mul nsw i32 3, %sub46
  %add48 = add nsw i32 %67, %mul47
  %70 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax49 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %70, i32 0, i32 22
  store i32 %add48, i32* %sfbmax49, align 4
  br label %if.end67

if.else50:                                        ; preds = %if.end34
  %71 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax51 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %71, i32 0, i32 19
  %72 = load i32, i32* %sfb_lmax51, align 4
  %73 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt52 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %73, i32 0, i32 13
  %sfb21_extra53 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt52, i32 0, i32 8
  %74 = load i32, i32* %sfb21_extra53, align 4
  %tobool54 = icmp ne i32 %74, 0
  %75 = zext i1 %tobool54 to i64
  %cond55 = select i1 %tobool54, i32 13, i32 12
  %76 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin56 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %76, i32 0, i32 20
  %77 = load i32, i32* %sfb_smin56, align 4
  %sub57 = sub nsw i32 %cond55, %77
  %mul58 = mul nsw i32 3, %sub57
  %add59 = add nsw i32 %72, %mul58
  %78 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psymax60 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %78, i32 0, i32 23
  store i32 %add59, i32* %psymax60, align 4
  %79 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax61 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %79, i32 0, i32 19
  %80 = load i32, i32* %sfb_lmax61, align 4
  %81 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin62 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %81, i32 0, i32 20
  %82 = load i32, i32* %sfb_smin62, align 4
  %sub63 = sub nsw i32 12, %82
  %mul64 = mul nsw i32 3, %sub63
  %add65 = add nsw i32 %80, %mul64
  %83 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax66 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %83, i32 0, i32 22
  store i32 %add65, i32* %sfbmax66, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.else50, %if.then37
  %84 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax68 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %84, i32 0, i32 22
  %85 = load i32, i32* %sfbmax68, align 4
  %sub69 = sub nsw i32 %85, 18
  %86 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbdivide70 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %86, i32 0, i32 24
  store i32 %sub69, i32* %sfbdivide70, align 4
  %87 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax71 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %87, i32 0, i32 19
  %88 = load i32, i32* %sfb_lmax71, align 4
  %89 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %psy_lmax72 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %89, i32 0, i32 21
  store i32 %88, i32* %psy_lmax72, align 4
  %90 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr = getelementptr inbounds %struct.gr_info, %struct.gr_info* %90, i32 0, i32 0
  %91 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band73 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %91, i32 0, i32 8
  %l74 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band73, i32 0, i32 0
  %92 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax75 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %92, i32 0, i32 19
  %93 = load i32, i32* %sfb_lmax75, align 4
  %arrayidx76 = getelementptr inbounds [23 x i32], [23 x i32]* %l74, i32 0, i32 %93
  %94 = load i32, i32* %arrayidx76, align 4
  %arrayidx77 = getelementptr inbounds [576 x float], [576 x float]* %xr, i32 0, i32 %94
  store float* %arrayidx77, float** %ix, align 4
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %ixwork, i32 0, i32 0
  %95 = bitcast float* %arraydecay to i8*
  %96 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr78 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %96, i32 0, i32 0
  %arraydecay79 = getelementptr inbounds [576 x float], [576 x float]* %xr78, i32 0, i32 0
  %97 = bitcast float* %arraydecay79 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %95, i8* align 4 %97, i32 2304, i1 false)
  %98 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin80 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %98, i32 0, i32 20
  %99 = load i32, i32* %sfb_smin80, align 4
  store i32 %99, i32* %sfb, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc107, %if.end67
  %100 = load i32, i32* %sfb, align 4
  %cmp82 = icmp slt i32 %100, 13
  br i1 %cmp82, label %for.body83, label %for.end109

for.body83:                                       ; preds = %for.cond81
  %101 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band84 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %101, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band84, i32 0, i32 1
  %102 = load i32, i32* %sfb, align 4
  %arrayidx85 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 %102
  %103 = load i32, i32* %arrayidx85, align 4
  store i32 %103, i32* %start, align 4
  %104 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band86 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %104, i32 0, i32 8
  %s87 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band86, i32 0, i32 1
  %105 = load i32, i32* %sfb, align 4
  %add88 = add nsw i32 %105, 1
  %arrayidx89 = getelementptr inbounds [14 x i32], [14 x i32]* %s87, i32 0, i32 %add88
  %106 = load i32, i32* %arrayidx89, align 4
  store i32 %106, i32* %end, align 4
  store i32 0, i32* %window90, align 4
  br label %for.cond92

for.cond92:                                       ; preds = %for.inc104, %for.body83
  %107 = load i32, i32* %window90, align 4
  %cmp93 = icmp slt i32 %107, 3
  br i1 %cmp93, label %for.body94, label %for.end106

for.body94:                                       ; preds = %for.cond92
  %108 = load i32, i32* %start, align 4
  store i32 %108, i32* %l91, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc101, %for.body94
  %109 = load i32, i32* %l91, align 4
  %110 = load i32, i32* %end, align 4
  %cmp96 = icmp slt i32 %109, %110
  br i1 %cmp96, label %for.body97, label %for.end103

for.body97:                                       ; preds = %for.cond95
  %111 = load i32, i32* %l91, align 4
  %mul98 = mul nsw i32 3, %111
  %112 = load i32, i32* %window90, align 4
  %add99 = add nsw i32 %mul98, %112
  %arrayidx100 = getelementptr inbounds [576 x float], [576 x float]* %ixwork, i32 0, i32 %add99
  %113 = load float, float* %arrayidx100, align 4
  %114 = load float*, float** %ix, align 4
  %incdec.ptr = getelementptr inbounds float, float* %114, i32 1
  store float* %incdec.ptr, float** %ix, align 4
  store float %113, float* %114, align 4
  br label %for.inc101

for.inc101:                                       ; preds = %for.body97
  %115 = load i32, i32* %l91, align 4
  %inc102 = add nsw i32 %115, 1
  store i32 %inc102, i32* %l91, align 4
  br label %for.cond95

for.end103:                                       ; preds = %for.cond95
  br label %for.inc104

for.inc104:                                       ; preds = %for.end103
  %116 = load i32, i32* %window90, align 4
  %inc105 = add nsw i32 %116, 1
  store i32 %inc105, i32* %window90, align 4
  br label %for.cond92

for.end106:                                       ; preds = %for.cond92
  br label %for.inc107

for.inc107:                                       ; preds = %for.end106
  %117 = load i32, i32* %sfb, align 4
  %inc108 = add nsw i32 %117, 1
  store i32 %inc108, i32* %sfb, align 4
  br label %for.cond81

for.end109:                                       ; preds = %for.cond81
  %118 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax110 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %118, i32 0, i32 19
  %119 = load i32, i32* %sfb_lmax110, align 4
  store i32 %119, i32* %j, align 4
  %120 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_smin111 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %120, i32 0, i32 20
  %121 = load i32, i32* %sfb_smin111, align 4
  store i32 %121, i32* %sfb, align 4
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc140, %for.end109
  %122 = load i32, i32* %sfb, align 4
  %cmp113 = icmp slt i32 %122, 13
  br i1 %cmp113, label %for.body114, label %for.end142

for.body114:                                      ; preds = %for.cond112
  %123 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band115 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %123, i32 0, i32 8
  %s116 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band115, i32 0, i32 1
  %124 = load i32, i32* %sfb, align 4
  %add117 = add nsw i32 %124, 1
  %arrayidx118 = getelementptr inbounds [14 x i32], [14 x i32]* %s116, i32 0, i32 %add117
  %125 = load i32, i32* %arrayidx118, align 4
  %126 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band119 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %126, i32 0, i32 8
  %s120 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band119, i32 0, i32 1
  %127 = load i32, i32* %sfb, align 4
  %arrayidx121 = getelementptr inbounds [14 x i32], [14 x i32]* %s120, i32 0, i32 %127
  %128 = load i32, i32* %arrayidx121, align 4
  %sub122 = sub nsw i32 %125, %128
  %129 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width123 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %129, i32 0, i32 25
  %130 = load i32, i32* %j, align 4
  %add124 = add nsw i32 %130, 2
  %arrayidx125 = getelementptr inbounds [39 x i32], [39 x i32]* %width123, i32 0, i32 %add124
  store i32 %sub122, i32* %arrayidx125, align 4
  %131 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width126 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %131, i32 0, i32 25
  %132 = load i32, i32* %j, align 4
  %add127 = add nsw i32 %132, 1
  %arrayidx128 = getelementptr inbounds [39 x i32], [39 x i32]* %width126, i32 0, i32 %add127
  store i32 %sub122, i32* %arrayidx128, align 4
  %133 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width129 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %133, i32 0, i32 25
  %134 = load i32, i32* %j, align 4
  %arrayidx130 = getelementptr inbounds [39 x i32], [39 x i32]* %width129, i32 0, i32 %134
  store i32 %sub122, i32* %arrayidx130, align 4
  %135 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window131 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %135, i32 0, i32 26
  %136 = load i32, i32* %j, align 4
  %arrayidx132 = getelementptr inbounds [39 x i32], [39 x i32]* %window131, i32 0, i32 %136
  store i32 0, i32* %arrayidx132, align 4
  %137 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window133 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %137, i32 0, i32 26
  %138 = load i32, i32* %j, align 4
  %add134 = add nsw i32 %138, 1
  %arrayidx135 = getelementptr inbounds [39 x i32], [39 x i32]* %window133, i32 0, i32 %add134
  store i32 1, i32* %arrayidx135, align 4
  %139 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window136 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %139, i32 0, i32 26
  %140 = load i32, i32* %j, align 4
  %add137 = add nsw i32 %140, 2
  %arrayidx138 = getelementptr inbounds [39 x i32], [39 x i32]* %window136, i32 0, i32 %add137
  store i32 2, i32* %arrayidx138, align 4
  %141 = load i32, i32* %j, align 4
  %add139 = add nsw i32 %141, 3
  store i32 %add139, i32* %j, align 4
  br label %for.inc140

for.inc140:                                       ; preds = %for.body114
  %142 = load i32, i32* %sfb, align 4
  %inc141 = add nsw i32 %142, 1
  store i32 %inc141, i32* %sfb, align 4
  br label %for.cond112

for.end142:                                       ; preds = %for.cond112
  br label %if.end143

if.end143:                                        ; preds = %for.end142, %for.end
  %143 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %count1bits = getelementptr inbounds %struct.gr_info, %struct.gr_info* %143, i32 0, i32 27
  store i32 0, i32* %count1bits, align 4
  %144 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_partition_table = getelementptr inbounds %struct.gr_info, %struct.gr_info* %144, i32 0, i32 28
  store i32* getelementptr inbounds ([6 x [3 x [4 x i32]]], [6 x [3 x [4 x i32]]]* @nr_of_sfb_block, i32 0, i32 0, i32 0, i32 0), i32** %sfb_partition_table, align 4
  %145 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen = getelementptr inbounds %struct.gr_info, %struct.gr_info* %145, i32 0, i32 29
  %arrayidx144 = getelementptr inbounds [4 x i32], [4 x i32]* %slen, i32 0, i32 0
  store i32 0, i32* %arrayidx144, align 4
  %146 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen145 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %146, i32 0, i32 29
  %arrayidx146 = getelementptr inbounds [4 x i32], [4 x i32]* %slen145, i32 0, i32 1
  store i32 0, i32* %arrayidx146, align 4
  %147 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen147 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %147, i32 0, i32 29
  %arrayidx148 = getelementptr inbounds [4 x i32], [4 x i32]* %slen147, i32 0, i32 2
  store i32 0, i32* %arrayidx148, align 4
  %148 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %slen149 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %148, i32 0, i32 29
  %arrayidx150 = getelementptr inbounds [4 x i32], [4 x i32]* %slen149, i32 0, i32 3
  store i32 0, i32* %arrayidx150, align 4
  %149 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %max_nonzero_coeff = getelementptr inbounds %struct.gr_info, %struct.gr_info* %149, i32 0, i32 30
  store i32 575, i32* %max_nonzero_coeff, align 4
  %150 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %150, i32 0, i32 2
  %arraydecay151 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 0
  %151 = bitcast i32* %arraydecay151 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %151, i8 0, i32 156, i1 false)
  %152 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %152, i32 0, i32 22
  %153 = load i32, i32* %vbr, align 4
  %cmp152 = icmp ne i32 %153, 1
  br i1 %cmp152, label %land.lhs.true, label %if.end162

land.lhs.true:                                    ; preds = %if.end143
  %154 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr153 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %154, i32 0, i32 22
  %155 = load i32, i32* %vbr153, align 4
  %cmp154 = icmp ne i32 %155, 4
  br i1 %cmp154, label %land.lhs.true155, label %if.end162

land.lhs.true155:                                 ; preds = %land.lhs.true
  %156 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr156 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %156, i32 0, i32 22
  %157 = load i32, i32* %vbr156, align 4
  %cmp157 = icmp ne i32 %157, 3
  br i1 %cmp157, label %land.lhs.true158, label %if.end162

land.lhs.true158:                                 ; preds = %land.lhs.true155
  %158 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr159 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %158, i32 0, i32 22
  %159 = load i32, i32* %vbr159, align 4
  %cmp160 = icmp ne i32 %159, 0
  br i1 %cmp160, label %if.then161, label %if.end162

if.then161:                                       ; preds = %land.lhs.true158
  %160 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %161 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  call void @psfb21_analogsilence(%struct.lame_internal_flags* %160, %struct.gr_info* %161)
  br label %if.end162

if.end162:                                        ; preds = %if.then161, %land.lhs.true158, %land.lhs.true155, %land.lhs.true, %if.end143
  ret void
}

declare i32 @calc_xmin(%struct.lame_internal_flags*, %struct.III_psy_ratio*, %struct.gr_info*, float*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @outer_loop(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %l3_xmin, float* %xrpow, i32 %ch, i32 %targ_bits) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %l3_xmin.addr = alloca float*, align 4
  %xrpow.addr = alloca float*, align 4
  %ch.addr = alloca i32, align 4
  %targ_bits.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %cod_info_w = alloca %struct.gr_info, align 4
  %save_xrpow = alloca [576 x float], align 16
  %distort = alloca [39 x float], align 16
  %best_noise_info = alloca %struct.calc_noise_result_t, align 4
  %huff_bits = alloca i32, align 4
  %better = alloca i32, align 4
  %age = alloca i32, align 4
  %prev_noise = alloca %struct.calc_noise_data_t, align 4
  %best_part2_3_length = alloca i32, align 4
  %bEndOfSearch = alloca i32, align 4
  %bRefine = alloca i32, align 4
  %best_ggain_pass1 = alloca i32, align 4
  %noise_info = alloca %struct.calc_noise_result_t, align 4
  %search_limit = alloca i32, align 4
  %maxggain = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %l3_xmin, float** %l3_xmin.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  store i32 %targ_bits, i32* %targ_bits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 9999999, i32* %best_part2_3_length, align 4
  store i32 0, i32* %bEndOfSearch, align 4
  store i32 0, i32* %bRefine, align 4
  store i32 0, i32* %best_ggain_pass1, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %3 = load i32, i32* %targ_bits.addr, align 4
  %4 = load i32, i32* %ch.addr, align 4
  %5 = load float*, float** %xrpow.addr, align 4
  %call = call i32 @bin_search_StepSize(%struct.lame_internal_flags* %1, %struct.gr_info* %2, i32 %3, i32 %4, float* %5)
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 3
  %7 = load i32, i32* %noise_shaping, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 100, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %8 = bitcast %struct.calc_noise_data_t* %prev_noise to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %8, i8 0, i32 476, i1 false)
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %10 = load float*, float** %l3_xmin.addr, align 4
  %arraydecay = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 0
  %call2 = call i32 @calc_noise(%struct.gr_info* %9, float* %10, float* %arraydecay, %struct.calc_noise_result_t* %best_noise_info, %struct.calc_noise_data_t* %prev_noise)
  %11 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %11, i32 0, i32 4
  %12 = load i32, i32* %part2_3_length, align 4
  %bits = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %best_noise_info, i32 0, i32 5
  store i32 %12, i32* %bits, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %14 = bitcast %struct.gr_info* %cod_info_w to i8*
  %15 = bitcast %struct.gr_info* %13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 5252, i1 false)
  store i32 0, i32* %age, align 4
  %arraydecay3 = getelementptr inbounds [576 x float], [576 x float]* %save_xrpow, i32 0, i32 0
  %16 = bitcast float* %arraydecay3 to i8*
  %17 = load float*, float** %xrpow.addr, align 4
  %18 = bitcast float* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %16, i8* align 4 %18, i32 2304, i1 false)
  br label %while.cond

while.cond:                                       ; preds = %if.end148, %if.end
  %19 = load i32, i32* %bEndOfSearch, align 4
  %tobool4 = icmp ne i32 %19, 0
  %lnot = xor i1 %tobool4, true
  br i1 %lnot, label %while.body, label %while.end149

while.body:                                       ; preds = %while.cond
  br label %do.body

do.body:                                          ; preds = %do.cond, %while.body
  store i32 255, i32* %maxggain, align 4
  %20 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %20, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %21 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %21, 2
  %tobool5 = icmp ne i32 %and, 0
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %do.body
  store i32 20, i32* %search_limit, align 4
  br label %if.end7

if.else:                                          ; preds = %do.body
  store i32 3, i32* %search_limit, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.else, %if.then6
  %22 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt8 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %22, i32 0, i32 13
  %sfb21_extra = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt8, i32 0, i32 8
  %23 = load i32, i32* %sfb21_extra, align 4
  %tobool9 = icmp ne i32 %23, 0
  br i1 %tobool9, label %if.then10, label %if.end29

if.then10:                                        ; preds = %if.end7
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 22
  %24 = load i32, i32* %sfbmax, align 4
  %arrayidx = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 %24
  %25 = load float, float* %arrayidx, align 4
  %conv = fpext float %25 to double
  %cmp = fcmp ogt double %conv, 1.000000e+00
  br i1 %cmp, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.then10
  br label %do.end

if.end13:                                         ; preds = %if.then10
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 9
  %26 = load i32, i32* %block_type, align 4
  %cmp14 = icmp eq i32 %26, 2
  br i1 %cmp14, label %land.lhs.true, label %if.end28

land.lhs.true:                                    ; preds = %if.end13
  %sfbmax16 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 22
  %27 = load i32, i32* %sfbmax16, align 4
  %add = add nsw i32 %27, 1
  %arrayidx17 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 %add
  %28 = load float, float* %arrayidx17, align 4
  %conv18 = fpext float %28 to double
  %cmp19 = fcmp ogt double %conv18, 1.000000e+00
  br i1 %cmp19, label %if.then27, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %sfbmax21 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 22
  %29 = load i32, i32* %sfbmax21, align 4
  %add22 = add nsw i32 %29, 2
  %arrayidx23 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 %add22
  %30 = load float, float* %arrayidx23, align 4
  %conv24 = fpext float %30 to double
  %cmp25 = fcmp ogt double %conv24, 1.000000e+00
  br i1 %cmp25, label %if.then27, label %if.end28

if.then27:                                        ; preds = %lor.lhs.false, %land.lhs.true
  br label %do.end

if.end28:                                         ; preds = %lor.lhs.false, %if.end13
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.end7
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %arraydecay30 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 0
  %32 = load float*, float** %xrpow.addr, align 4
  %33 = load i32, i32* %bRefine, align 4
  %call31 = call i32 @balance_noise(%struct.lame_internal_flags* %31, %struct.gr_info* %cod_info_w, float* %arraydecay30, float* %32, i32 %33)
  %cmp32 = icmp eq i32 %call31, 0
  br i1 %cmp32, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end29
  br label %do.end

if.end35:                                         ; preds = %if.end29
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 16
  %34 = load i32, i32* %scalefac_scale, align 4
  %tobool36 = icmp ne i32 %34, 0
  br i1 %tobool36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end35
  store i32 254, i32* %maxggain, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %if.end35
  %35 = load i32, i32* %targ_bits.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 18
  %36 = load i32, i32* %part2_length, align 4
  %sub = sub nsw i32 %35, %36
  store i32 %sub, i32* %huff_bits, align 4
  %37 = load i32, i32* %huff_bits, align 4
  %cmp39 = icmp sle i32 %37, 0
  br i1 %cmp39, label %if.then41, label %if.end42

if.then41:                                        ; preds = %if.end38
  br label %do.end

if.end42:                                         ; preds = %if.end38
  br label %while.cond43

while.cond43:                                     ; preds = %while.body50, %if.end42
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %39 = load float*, float** %xrpow.addr, align 4
  %call44 = call i32 @count_bits(%struct.lame_internal_flags* %38, float* %39, %struct.gr_info* %cod_info_w, %struct.calc_noise_data_t* %prev_noise)
  %part2_3_length45 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 4
  store i32 %call44, i32* %part2_3_length45, align 4
  %40 = load i32, i32* %huff_bits, align 4
  %cmp46 = icmp sgt i32 %call44, %40
  br i1 %cmp46, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond43
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %41 = load i32, i32* %global_gain, align 4
  %42 = load i32, i32* %maxggain, align 4
  %cmp48 = icmp sle i32 %41, %42
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond43
  %43 = phi i1 [ false, %while.cond43 ], [ %cmp48, %land.rhs ]
  br i1 %43, label %while.body50, label %while.end

while.body50:                                     ; preds = %land.end
  %global_gain51 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %44 = load i32, i32* %global_gain51, align 4
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %global_gain51, align 4
  br label %while.cond43

while.end:                                        ; preds = %land.end
  %global_gain52 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %45 = load i32, i32* %global_gain52, align 4
  %46 = load i32, i32* %maxggain, align 4
  %cmp53 = icmp sgt i32 %45, %46
  br i1 %cmp53, label %if.then55, label %if.end56

if.then55:                                        ; preds = %while.end
  br label %do.end

if.end56:                                         ; preds = %while.end
  %over_count = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %best_noise_info, i32 0, i32 3
  %47 = load i32, i32* %over_count, align 4
  %cmp57 = icmp eq i32 %47, 0
  br i1 %cmp57, label %if.then59, label %if.end79

if.then59:                                        ; preds = %if.end56
  br label %while.cond60

while.cond60:                                     ; preds = %while.body70, %if.then59
  %48 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %49 = load float*, float** %xrpow.addr, align 4
  %call61 = call i32 @count_bits(%struct.lame_internal_flags* %48, float* %49, %struct.gr_info* %cod_info_w, %struct.calc_noise_data_t* %prev_noise)
  %part2_3_length62 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 4
  store i32 %call61, i32* %part2_3_length62, align 4
  %50 = load i32, i32* %best_part2_3_length, align 4
  %cmp63 = icmp sgt i32 %call61, %50
  br i1 %cmp63, label %land.rhs65, label %land.end69

land.rhs65:                                       ; preds = %while.cond60
  %global_gain66 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %51 = load i32, i32* %global_gain66, align 4
  %52 = load i32, i32* %maxggain, align 4
  %cmp67 = icmp sle i32 %51, %52
  br label %land.end69

land.end69:                                       ; preds = %land.rhs65, %while.cond60
  %53 = phi i1 [ false, %while.cond60 ], [ %cmp67, %land.rhs65 ]
  br i1 %53, label %while.body70, label %while.end73

while.body70:                                     ; preds = %land.end69
  %global_gain71 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %54 = load i32, i32* %global_gain71, align 4
  %inc72 = add nsw i32 %54, 1
  store i32 %inc72, i32* %global_gain71, align 4
  br label %while.cond60

while.end73:                                      ; preds = %land.end69
  %global_gain74 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %55 = load i32, i32* %global_gain74, align 4
  %56 = load i32, i32* %maxggain, align 4
  %cmp75 = icmp sgt i32 %55, %56
  br i1 %cmp75, label %if.then77, label %if.end78

if.then77:                                        ; preds = %while.end73
  br label %do.end

if.end78:                                         ; preds = %while.end73
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.end56
  %57 = load float*, float** %l3_xmin.addr, align 4
  %arraydecay80 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 0
  %call81 = call i32 @calc_noise(%struct.gr_info* %cod_info_w, float* %57, float* %arraydecay80, %struct.calc_noise_result_t* %noise_info, %struct.calc_noise_data_t* %prev_noise)
  %part2_3_length82 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 4
  %58 = load i32, i32* %part2_3_length82, align 4
  %bits83 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %noise_info, i32 0, i32 5
  store i32 %58, i32* %bits83, align 4
  %59 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type84 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %59, i32 0, i32 9
  %60 = load i32, i32* %block_type84, align 4
  %cmp85 = icmp ne i32 %60, 2
  br i1 %cmp85, label %if.then87, label %if.else88

if.then87:                                        ; preds = %if.end79
  %61 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %61, i32 0, i32 17
  %62 = load i32, i32* %quant_comp, align 4
  store i32 %62, i32* %better, align 4
  br label %if.end89

if.else88:                                        ; preds = %if.end79
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %quant_comp_short = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 18
  %64 = load i32, i32* %quant_comp_short, align 4
  store i32 %64, i32* %better, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.else88, %if.then87
  %65 = load i32, i32* %better, align 4
  %arraydecay90 = getelementptr inbounds [39 x float], [39 x float]* %distort, i32 0, i32 0
  %call91 = call i32 @quant_compare(i32 %65, %struct.calc_noise_result_t* %best_noise_info, %struct.calc_noise_result_t* %noise_info, %struct.gr_info* %cod_info_w, float* %arraydecay90)
  store i32 %call91, i32* %better, align 4
  %66 = load i32, i32* %better, align 4
  %tobool92 = icmp ne i32 %66, 0
  br i1 %tobool92, label %if.then93, label %if.else96

if.then93:                                        ; preds = %if.end89
  %67 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length94 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %67, i32 0, i32 4
  %68 = load i32, i32* %part2_3_length94, align 4
  store i32 %68, i32* %best_part2_3_length, align 4
  %69 = bitcast %struct.calc_noise_result_t* %best_noise_info to i8*
  %70 = bitcast %struct.calc_noise_result_t* %noise_info to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 24, i1 false)
  %71 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %72 = bitcast %struct.gr_info* %71 to i8*
  %73 = bitcast %struct.gr_info* %cod_info_w to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %72, i8* align 4 %73, i32 5252, i1 false)
  store i32 0, i32* %age, align 4
  %arraydecay95 = getelementptr inbounds [576 x float], [576 x float]* %save_xrpow, i32 0, i32 0
  %74 = bitcast float* %arraydecay95 to i8*
  %75 = load float*, float** %xrpow.addr, align 4
  %76 = bitcast float* %75 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %74, i8* align 4 %76, i32 2304, i1 false)
  br label %if.end131

if.else96:                                        ; preds = %if.end89
  %77 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %full_outer_loop = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %77, i32 0, i32 8
  %78 = load i32, i32* %full_outer_loop, align 4
  %cmp97 = icmp eq i32 %78, 0
  br i1 %cmp97, label %if.then99, label %if.end130

if.then99:                                        ; preds = %if.else96
  %79 = load i32, i32* %age, align 4
  %inc100 = add nsw i32 %79, 1
  store i32 %inc100, i32* %age, align 4
  %80 = load i32, i32* %search_limit, align 4
  %cmp101 = icmp sgt i32 %inc100, %80
  br i1 %cmp101, label %land.lhs.true103, label %if.end108

land.lhs.true103:                                 ; preds = %if.then99
  %over_count104 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %best_noise_info, i32 0, i32 3
  %81 = load i32, i32* %over_count104, align 4
  %cmp105 = icmp eq i32 %81, 0
  br i1 %cmp105, label %if.then107, label %if.end108

if.then107:                                       ; preds = %land.lhs.true103
  br label %do.end

if.end108:                                        ; preds = %land.lhs.true103, %if.then99
  %82 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %82, i32 0, i32 6
  %83 = load i32, i32* %noise_shaping_amp, align 4
  %cmp109 = icmp eq i32 %83, 3
  br i1 %cmp109, label %land.lhs.true111, label %if.end117

land.lhs.true111:                                 ; preds = %if.end108
  %84 = load i32, i32* %bRefine, align 4
  %tobool112 = icmp ne i32 %84, 0
  br i1 %tobool112, label %land.lhs.true113, label %if.end117

land.lhs.true113:                                 ; preds = %land.lhs.true111
  %85 = load i32, i32* %age, align 4
  %cmp114 = icmp sgt i32 %85, 30
  br i1 %cmp114, label %if.then116, label %if.end117

if.then116:                                       ; preds = %land.lhs.true113
  br label %do.end

if.end117:                                        ; preds = %land.lhs.true113, %land.lhs.true111, %if.end108
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp118 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 6
  %87 = load i32, i32* %noise_shaping_amp118, align 4
  %cmp119 = icmp eq i32 %87, 3
  br i1 %cmp119, label %land.lhs.true121, label %if.end129

land.lhs.true121:                                 ; preds = %if.end117
  %88 = load i32, i32* %bRefine, align 4
  %tobool122 = icmp ne i32 %88, 0
  br i1 %tobool122, label %land.lhs.true123, label %if.end129

land.lhs.true123:                                 ; preds = %land.lhs.true121
  %global_gain124 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %89 = load i32, i32* %global_gain124, align 4
  %90 = load i32, i32* %best_ggain_pass1, align 4
  %sub125 = sub nsw i32 %89, %90
  %cmp126 = icmp sgt i32 %sub125, 15
  br i1 %cmp126, label %if.then128, label %if.end129

if.then128:                                       ; preds = %land.lhs.true123
  br label %do.end

if.end129:                                        ; preds = %land.lhs.true123, %land.lhs.true121, %if.end117
  br label %if.end130

if.end130:                                        ; preds = %if.end129, %if.else96
  br label %if.end131

if.end131:                                        ; preds = %if.end130, %if.then93
  br label %do.cond

do.cond:                                          ; preds = %if.end131
  %global_gain132 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %91 = load i32, i32* %global_gain132, align 4
  %scalefac_scale133 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 16
  %92 = load i32, i32* %scalefac_scale133, align 4
  %add134 = add nsw i32 %91, %92
  %cmp135 = icmp slt i32 %add134, 255
  br i1 %cmp135, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond, %if.then128, %if.then116, %if.then107, %if.then77, %if.then55, %if.then41, %if.then34, %if.then27, %if.then12
  %93 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp137 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %93, i32 0, i32 6
  %94 = load i32, i32* %noise_shaping_amp137, align 4
  %cmp138 = icmp eq i32 %94, 3
  br i1 %cmp138, label %if.then140, label %if.else147

if.then140:                                       ; preds = %do.end
  %95 = load i32, i32* %bRefine, align 4
  %tobool141 = icmp ne i32 %95, 0
  br i1 %tobool141, label %if.else145, label %if.then142

if.then142:                                       ; preds = %if.then140
  %96 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %97 = bitcast %struct.gr_info* %cod_info_w to i8*
  %98 = bitcast %struct.gr_info* %96 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %97, i8* align 4 %98, i32 5252, i1 false)
  %99 = load float*, float** %xrpow.addr, align 4
  %100 = bitcast float* %99 to i8*
  %arraydecay143 = getelementptr inbounds [576 x float], [576 x float]* %save_xrpow, i32 0, i32 0
  %101 = bitcast float* %arraydecay143 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 16 %101, i32 2304, i1 false)
  store i32 0, i32* %age, align 4
  %global_gain144 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %cod_info_w, i32 0, i32 7
  %102 = load i32, i32* %global_gain144, align 4
  store i32 %102, i32* %best_ggain_pass1, align 4
  store i32 1, i32* %bRefine, align 4
  br label %if.end146

if.else145:                                       ; preds = %if.then140
  store i32 1, i32* %bEndOfSearch, align 4
  br label %if.end146

if.end146:                                        ; preds = %if.else145, %if.then142
  br label %if.end148

if.else147:                                       ; preds = %do.end
  store i32 1, i32* %bEndOfSearch, align 4
  br label %if.end148

if.end148:                                        ; preds = %if.else147, %if.end146
  br label %while.cond

while.end149:                                     ; preds = %while.cond
  %103 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %103, i32 0, i32 22
  %104 = load i32, i32* %vbr, align 4
  %cmp150 = icmp eq i32 %104, 2
  br i1 %cmp150, label %if.then160, label %lor.lhs.false152

lor.lhs.false152:                                 ; preds = %while.end149
  %105 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr153 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %105, i32 0, i32 22
  %106 = load i32, i32* %vbr153, align 4
  %cmp154 = icmp eq i32 %106, 4
  br i1 %cmp154, label %if.then160, label %lor.lhs.false156

lor.lhs.false156:                                 ; preds = %lor.lhs.false152
  %107 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr157 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %107, i32 0, i32 22
  %108 = load i32, i32* %vbr157, align 4
  %cmp158 = icmp eq i32 %108, 1
  br i1 %cmp158, label %if.then160, label %if.else162

if.then160:                                       ; preds = %lor.lhs.false156, %lor.lhs.false152, %while.end149
  %109 = load float*, float** %xrpow.addr, align 4
  %110 = bitcast float* %109 to i8*
  %arraydecay161 = getelementptr inbounds [576 x float], [576 x float]* %save_xrpow, i32 0, i32 0
  %111 = bitcast float* %arraydecay161 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %110, i8* align 16 %111, i32 2304, i1 false)
  br label %if.end169

if.else162:                                       ; preds = %lor.lhs.false156
  %112 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt163 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %112, i32 0, i32 13
  %substep_shaping164 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt163, i32 0, i32 9
  %113 = load i32, i32* %substep_shaping164, align 8
  %and165 = and i32 %113, 1
  %tobool166 = icmp ne i32 %and165, 0
  br i1 %tobool166, label %if.then167, label %if.end168

if.then167:                                       ; preds = %if.else162
  %114 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %115 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %116 = load float*, float** %l3_xmin.addr, align 4
  %117 = load float*, float** %xrpow.addr, align 4
  call void @trancate_smallspectrums(%struct.lame_internal_flags* %114, %struct.gr_info* %115, float* %116, float* %117)
  br label %if.end168

if.end168:                                        ; preds = %if.then167, %if.else162
  br label %if.end169

if.end169:                                        ; preds = %if.end168, %if.then160
  %over_count170 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %best_noise_info, i32 0, i32 3
  %118 = load i32, i32* %over_count170, align 4
  store i32 %118, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end169, %if.then
  %119 = load i32, i32* %retval, align 4
  ret i32 %119
}

; Function Attrs: noinline nounwind optnone
define hidden void @CBR_iteration_loop(%struct.lame_internal_flags* %gfc, [2 x float]* %pe, float* %ms_ener_ratio, [2 x %struct.III_psy_ratio]* %ratio) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %pe.addr = alloca [2 x float]*, align 4
  %ms_ener_ratio.addr = alloca float*, align 4
  %ratio.addr = alloca [2 x %struct.III_psy_ratio]*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %l3_xmin = alloca [39 x float], align 16
  %xrpow = alloca [576 x float], align 16
  %targ_bits = alloca [2 x i32], align 4
  %mean_bits = alloca i32, align 4
  %max_bits = alloca i32, align 4
  %gr = alloca i32, align 4
  %ch = alloca i32, align 4
  %l3_side = alloca %struct.III_side_info_t*, align 4
  %cod_info = alloca %struct.gr_info*, align 4
  %adjust = alloca float, align 4
  %masking_lower_db = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store [2 x float]* %pe, [2 x float]** %pe.addr, align 4
  store float* %ms_ener_ratio, float** %ms_ener_ratio.addr, align 4
  store [2 x %struct.III_psy_ratio]* %ratio, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 7
  store %struct.III_side_info_t* %l3_side2, %struct.III_side_info_t** %l3_side, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %2, i32* %mean_bits)
  store i32 0, i32* %gr, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc31, %entry
  %3 = load i32, i32* %gr, align 4
  %4 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode_gr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %4, i32 0, i32 15
  %5 = load i32, i32* %mode_gr, align 4
  %cmp = icmp slt i32 %3, %5
  br i1 %cmp, label %for.body, label %for.end33

for.body:                                         ; preds = %for.cond
  %6 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %7 = load [2 x float]*, [2 x float]** %pe.addr, align 4
  %arraydecay = getelementptr inbounds [2 x i32], [2 x i32]* %targ_bits, i32 0, i32 0
  %8 = load i32, i32* %mean_bits, align 4
  %9 = load i32, i32* %gr, align 4
  %10 = load i32, i32* %gr, align 4
  %call3 = call i32 @on_pe(%struct.lame_internal_flags* %6, [2 x float]* %7, i32* %arraydecay, i32 %8, i32 %9, i32 %10)
  store i32 %call3, i32* %max_bits, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 12
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 5
  %12 = load i32, i32* %mode_ext, align 4
  %cmp4 = icmp eq i32 %12, 2
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %l3_side5 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 7
  %14 = load i32, i32* %gr, align 4
  call void @ms_convert(%struct.III_side_info_t* %l3_side5, i32 %14)
  %arraydecay6 = getelementptr inbounds [2 x i32], [2 x i32]* %targ_bits, i32 0, i32 0
  %15 = load float*, float** %ms_ener_ratio.addr, align 4
  %16 = load i32, i32* %gr, align 4
  %arrayidx = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx, align 4
  %18 = load i32, i32* %mean_bits, align 4
  %19 = load i32, i32* %max_bits, align 4
  call void @reduce_side(i32* %arraydecay6, float %17, i32 %18, i32 %19)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  store i32 0, i32* %ch, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %if.end
  %20 = load i32, i32* %ch, align 4
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %channels_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 14
  %22 = load i32, i32* %channels_out, align 4
  %cmp8 = icmp slt i32 %20, %22
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %23 = load %struct.III_side_info_t*, %struct.III_side_info_t** %l3_side, align 4
  %tt = getelementptr inbounds %struct.III_side_info_t, %struct.III_side_info_t* %23, i32 0, i32 0
  %24 = load i32, i32* %gr, align 4
  %arrayidx10 = getelementptr inbounds [2 x [2 x %struct.gr_info]], [2 x [2 x %struct.gr_info]]* %tt, i32 0, i32 %24
  %25 = load i32, i32* %ch, align 4
  %arrayidx11 = getelementptr inbounds [2 x %struct.gr_info], [2 x %struct.gr_info]* %arrayidx10, i32 0, i32 %25
  store %struct.gr_info* %arrayidx11, %struct.gr_info** %cod_info, align 4
  %26 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %26, i32 0, i32 9
  %27 = load i32, i32* %block_type, align 4
  %cmp12 = icmp ne i32 %27, 2
  br i1 %cmp12, label %if.then13, label %if.else

if.then13:                                        ; preds = %for.body9
  store float 0.000000e+00, float* %adjust, align 4
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %28, i32 0, i32 13
  %mask_adjust = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 3
  %29 = load float, float* %mask_adjust, align 8
  %30 = load float, float* %adjust, align 4
  %sub = fsub float %29, %30
  store float %sub, float* %masking_lower_db, align 4
  br label %if.end16

if.else:                                          ; preds = %for.body9
  store float 0.000000e+00, float* %adjust, align 4
  %31 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt14 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %31, i32 0, i32 13
  %mask_adjust_short = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt14, i32 0, i32 4
  %32 = load float, float* %mask_adjust_short, align 4
  %33 = load float, float* %adjust, align 4
  %sub15 = fsub float %32, %33
  store float %sub15, float* %masking_lower_db, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.else, %if.then13
  %34 = load float, float* %masking_lower_db, align 4
  %conv = fpext float %34 to double
  %mul = fmul double %conv, 1.000000e-01
  %35 = call double @llvm.pow.f64(double 1.000000e+01, double %mul)
  %conv17 = fptrunc double %35 to float
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt18 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 13
  %masking_lower = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt18, i32 0, i32 2
  store float %conv17, float* %masking_lower, align 4
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %38 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  call void @init_outer_loop(%struct.lame_internal_flags* %37, %struct.gr_info* %38)
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %40 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay19 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %call20 = call i32 @init_xrpow(%struct.lame_internal_flags* %39, %struct.gr_info* %40, float* %arraydecay19)
  %tobool = icmp ne i32 %call20, 0
  br i1 %tobool, label %if.then21, label %if.end30

if.then21:                                        ; preds = %if.end16
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %42 = load [2 x %struct.III_psy_ratio]*, [2 x %struct.III_psy_ratio]** %ratio.addr, align 4
  %43 = load i32, i32* %gr, align 4
  %arrayidx22 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %42, i32 %43
  %44 = load i32, i32* %ch, align 4
  %arrayidx23 = getelementptr inbounds [2 x %struct.III_psy_ratio], [2 x %struct.III_psy_ratio]* %arrayidx22, i32 0, i32 %44
  %45 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay24 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %call25 = call i32 @calc_xmin(%struct.lame_internal_flags* %41, %struct.III_psy_ratio* %arrayidx23, %struct.gr_info* %45, float* %arraydecay24)
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %47 = load %struct.gr_info*, %struct.gr_info** %cod_info, align 4
  %arraydecay26 = getelementptr inbounds [39 x float], [39 x float]* %l3_xmin, i32 0, i32 0
  %arraydecay27 = getelementptr inbounds [576 x float], [576 x float]* %xrpow, i32 0, i32 0
  %48 = load i32, i32* %ch, align 4
  %49 = load i32, i32* %ch, align 4
  %arrayidx28 = getelementptr inbounds [2 x i32], [2 x i32]* %targ_bits, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx28, align 4
  %call29 = call i32 @outer_loop(%struct.lame_internal_flags* %46, %struct.gr_info* %47, float* %arraydecay26, float* %arraydecay27, i32 %48, i32 %50)
  br label %if.end30

if.end30:                                         ; preds = %if.then21, %if.end16
  %51 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %52 = load i32, i32* %gr, align 4
  %53 = load i32, i32* %ch, align 4
  call void @iteration_finish_one(%struct.lame_internal_flags* %51, i32 %52, i32 %53)
  br label %for.inc

for.inc:                                          ; preds = %if.end30
  %54 = load i32, i32* %ch, align 4
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %ch, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %for.inc31

for.inc31:                                        ; preds = %for.end
  %55 = load i32, i32* %gr, align 4
  %inc32 = add nsw i32 %55, 1
  store i32 %inc32, i32* %gr, align 4
  br label %for.cond

for.end33:                                        ; preds = %for.cond
  %56 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %57 = load i32, i32* %mean_bits, align 4
  call void @ResvFrameEnd(%struct.lame_internal_flags* %56, i32 %57)
  ret void
}

declare i32 @on_pe(%struct.lame_internal_flags*, [2 x float]*, i32*, i32, i32, i32) #1

declare void @reduce_side(i32*, float, i32, i32) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #4

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sqrt.f64(double) #4

; Function Attrs: noinline nounwind optnone
define internal void @get_framebits(%struct.lame_internal_flags* %gfc, i32* %frameBits) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %frameBits.addr = alloca i32*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %bitsPerFrame = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i32* %frameBits, i32** %frameBits.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %2, i32 0, i32 24
  %3 = load i32, i32* %vbr_min_bitrate_index, align 4
  %4 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %4, i32 0, i32 2
  store i32 %3, i32* %bitrate_index, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call = call i32 @getframebits(%struct.lame_internal_flags* %5)
  store i32 %call, i32* %bitsPerFrame, align 4
  %6 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index2 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %6, i32 0, i32 2
  store i32 1, i32* %bitrate_index2, align 4
  %7 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call3 = call i32 @getframebits(%struct.lame_internal_flags* %7)
  store i32 %call3, i32* %bitsPerFrame, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_max_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 25
  %10 = load i32, i32* %vbr_max_bitrate_index, align 4
  %cmp = icmp sle i32 %8, %10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load i32, i32* %i, align 4
  %12 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index4 = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %12, i32 0, i32 2
  store i32 %11, i32* %bitrate_index4, align 4
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %call5 = call i32 @ResvFrameBegin(%struct.lame_internal_flags* %13, i32* %bitsPerFrame)
  %14 = load i32*, i32** %frameBits.addr, align 4
  %15 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %14, i32 %15
  store i32 %call5, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.exp.f64(double) #4

declare i32 @getframebits(%struct.lame_internal_flags*) #1

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

declare i32 @calc_noise(%struct.gr_info*, float*, float*, %struct.calc_noise_result_t*, %struct.calc_noise_data_t*) #1

declare void @qsort(i8*, i32, i32, i32 (i8*, i8*)*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @floatcompare(i8* %v1, i8* %v2) #0 {
entry:
  %retval = alloca i32, align 4
  %v1.addr = alloca i8*, align 4
  %v2.addr = alloca i8*, align 4
  %a = alloca float*, align 4
  %b = alloca float*, align 4
  store i8* %v1, i8** %v1.addr, align 4
  store i8* %v2, i8** %v2.addr, align 4
  %0 = load i8*, i8** %v1.addr, align 4
  %1 = bitcast i8* %0 to float*
  store float* %1, float** %a, align 4
  %2 = load i8*, i8** %v2.addr, align 4
  %3 = bitcast i8* %2 to float*
  store float* %3, float** %b, align 4
  %4 = load float*, float** %a, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %b, align 4
  %7 = load float, float* %6, align 4
  %cmp = fcmp ogt float %5, %7
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %8 = load float*, float** %a, align 4
  %9 = load float, float* %8, align 4
  %10 = load float*, float** %b, align 4
  %11 = load float, float* %10, align 4
  %cmp1 = fcmp olt float %9, %11
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end3, %if.then2, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare i32 @noquant_count_bits(%struct.lame_internal_flags*, %struct.gr_info*, %struct.calc_noise_data_t*) #1

declare void @best_scalefac_store(%struct.lame_internal_flags*, i32, i32, %struct.III_side_info_t*) #1

declare void @best_huffman_divide(%struct.lame_internal_flags*, %struct.gr_info*) #1

; Function Attrs: noinline nounwind optnone
define internal void @psfb21_analogsilence(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %ATH = alloca %struct.ATH_t*, align 4
  %xr = alloca float*, align 4
  %gsfb = alloca i32, align 4
  %stop = alloca i32, align 4
  %start = alloca i32, align 4
  %end = alloca i32, align 4
  %j = alloca i32, align 4
  %ath21 = alloca float, align 4
  %block = alloca i32, align 4
  %gsfb33 = alloca i32, align 4
  %stop34 = alloca i32, align 4
  %start43 = alloca i32, align 4
  %end63 = alloca i32, align 4
  %j73 = alloca i32, align 4
  %ath12 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ATH1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 21
  %1 = load %struct.ATH_t*, %struct.ATH_t** %ATH1, align 8
  store %struct.ATH_t* %1, %struct.ATH_t** %ATH, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xr2 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 0
  %arraydecay = getelementptr inbounds [576 x float], [576 x float]* %xr2, i32 0, i32 0
  store float* %arraydecay, float** %xr, align 4
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 9
  %4 = load i32, i32* %block_type, align 4
  %cmp = icmp ne i32 %4, 2
  br i1 %cmp, label %if.then, label %if.else28

if.then:                                          ; preds = %entry
  store i32 0, i32* %stop, align 4
  store i32 5, i32* %gsfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %if.then
  %5 = load i32, i32* %gsfb, align 4
  %cmp3 = icmp sge i32 %5, 0
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %6 = load i32, i32* %stop, align 4
  %tobool = icmp ne i32 %6, 0
  %lnot = xor i1 %tobool, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %7 = phi i1 [ false, %for.cond ], [ %lnot, %land.rhs ]
  br i1 %7, label %for.body, label %for.end27

for.body:                                         ; preds = %land.end
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 8
  %psfb21 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 2
  %9 = load i32, i32* %gsfb, align 4
  %arrayidx = getelementptr inbounds [7 x i32], [7 x i32]* %psfb21, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx, align 4
  store i32 %10, i32* %start, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band4 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 8
  %psfb215 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band4, i32 0, i32 2
  %12 = load i32, i32* %gsfb, align 4
  %add = add nsw i32 %12, 1
  %arrayidx6 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb215, i32 0, i32 %add
  %13 = load i32, i32* %arrayidx6, align 4
  store i32 %13, i32* %end, align 4
  %14 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %adjust_factor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %14, i32 0, i32 2
  %15 = load float, float* %adjust_factor, align 4
  %16 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %psfb217 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %16, i32 0, i32 8
  %17 = load i32, i32* %gsfb, align 4
  %arrayidx8 = getelementptr inbounds [6 x float], [6 x float]* %psfb217, i32 0, i32 %17
  %18 = load float, float* %arrayidx8, align 4
  %19 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %floor = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %19, i32 0, i32 5
  %20 = load float, float* %floor, align 4
  %call = call float @athAdjust(float %15, float %18, float %20, float 0.000000e+00)
  store float %call, float* %ath21, align 4
  %21 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %21, i32 0, i32 13
  %longfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [22 x float], [22 x float]* %longfact, i32 0, i32 21
  %22 = load float, float* %arrayidx9, align 4
  %cmp10 = fcmp ogt float %22, 0x3D71979980000000
  br i1 %cmp10, label %if.then11, label %if.end

if.then11:                                        ; preds = %for.body
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt12 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 13
  %longfact13 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt12, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [22 x float], [22 x float]* %longfact13, i32 0, i32 21
  %24 = load float, float* %arrayidx14, align 4
  %25 = load float, float* %ath21, align 4
  %mul = fmul float %25, %24
  store float %mul, float* %ath21, align 4
  br label %if.end

if.end:                                           ; preds = %if.then11, %for.body
  %26 = load i32, i32* %end, align 4
  %sub = sub nsw i32 %26, 1
  store i32 %sub, i32* %j, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc, %if.end
  %27 = load i32, i32* %j, align 4
  %28 = load i32, i32* %start, align 4
  %cmp16 = icmp sge i32 %27, %28
  br i1 %cmp16, label %for.body17, label %for.end

for.body17:                                       ; preds = %for.cond15
  %29 = load float*, float** %xr, align 4
  %30 = load i32, i32* %j, align 4
  %arrayidx18 = getelementptr inbounds float, float* %29, i32 %30
  %31 = load float, float* %arrayidx18, align 4
  %conv = fpext float %31 to double
  %32 = call double @llvm.fabs.f64(double %conv)
  %33 = load float, float* %ath21, align 4
  %conv19 = fpext float %33 to double
  %cmp20 = fcmp olt double %32, %conv19
  br i1 %cmp20, label %if.then22, label %if.else

if.then22:                                        ; preds = %for.body17
  %34 = load float*, float** %xr, align 4
  %35 = load i32, i32* %j, align 4
  %arrayidx23 = getelementptr inbounds float, float* %34, i32 %35
  store float 0.000000e+00, float* %arrayidx23, align 4
  br label %if.end24

if.else:                                          ; preds = %for.body17
  store i32 1, i32* %stop, align 4
  br label %for.end

if.end24:                                         ; preds = %if.then22
  br label %for.inc

for.inc:                                          ; preds = %if.end24
  %36 = load i32, i32* %j, align 4
  %dec = add nsw i32 %36, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond15

for.end:                                          ; preds = %if.else, %for.cond15
  br label %for.inc25

for.inc25:                                        ; preds = %for.end
  %37 = load i32, i32* %gsfb, align 4
  %dec26 = add nsw i32 %37, -1
  store i32 %dec26, i32* %gsfb, align 4
  br label %for.cond

for.end27:                                        ; preds = %land.end
  br label %if.end111

if.else28:                                        ; preds = %entry
  store i32 0, i32* %block, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc109, %if.else28
  %38 = load i32, i32* %block, align 4
  %cmp30 = icmp slt i32 %38, 3
  br i1 %cmp30, label %for.body32, label %for.end110

for.body32:                                       ; preds = %for.cond29
  store i32 0, i32* %stop34, align 4
  store i32 5, i32* %gsfb33, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc106, %for.body32
  %39 = load i32, i32* %gsfb33, align 4
  %cmp36 = icmp sge i32 %39, 0
  br i1 %cmp36, label %land.rhs38, label %land.end41

land.rhs38:                                       ; preds = %for.cond35
  %40 = load i32, i32* %stop34, align 4
  %tobool39 = icmp ne i32 %40, 0
  %lnot40 = xor i1 %tobool39, true
  br label %land.end41

land.end41:                                       ; preds = %land.rhs38, %for.cond35
  %41 = phi i1 [ false, %for.cond35 ], [ %lnot40, %land.rhs38 ]
  br i1 %41, label %for.body42, label %for.end108

for.body42:                                       ; preds = %land.end41
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %42, i32 0, i32 8
  %s = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band44, i32 0, i32 1
  %arrayidx45 = getelementptr inbounds [14 x i32], [14 x i32]* %s, i32 0, i32 12
  %43 = load i32, i32* %arrayidx45, align 4
  %mul46 = mul nsw i32 %43, 3
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band47 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %44, i32 0, i32 8
  %s48 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band47, i32 0, i32 1
  %arrayidx49 = getelementptr inbounds [14 x i32], [14 x i32]* %s48, i32 0, i32 13
  %45 = load i32, i32* %arrayidx49, align 4
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band50 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %46, i32 0, i32 8
  %s51 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band50, i32 0, i32 1
  %arrayidx52 = getelementptr inbounds [14 x i32], [14 x i32]* %s51, i32 0, i32 12
  %47 = load i32, i32* %arrayidx52, align 4
  %sub53 = sub nsw i32 %45, %47
  %48 = load i32, i32* %block, align 4
  %mul54 = mul nsw i32 %sub53, %48
  %add55 = add nsw i32 %mul46, %mul54
  %49 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band56 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %49, i32 0, i32 8
  %psfb12 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band56, i32 0, i32 3
  %50 = load i32, i32* %gsfb33, align 4
  %arrayidx57 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb12, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx57, align 4
  %52 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band58 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %52, i32 0, i32 8
  %psfb1259 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band58, i32 0, i32 3
  %arrayidx60 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb1259, i32 0, i32 0
  %53 = load i32, i32* %arrayidx60, align 4
  %sub61 = sub nsw i32 %51, %53
  %add62 = add nsw i32 %add55, %sub61
  store i32 %add62, i32* %start43, align 4
  %54 = load i32, i32* %start43, align 4
  %55 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band64 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %55, i32 0, i32 8
  %psfb1265 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band64, i32 0, i32 3
  %56 = load i32, i32* %gsfb33, align 4
  %add66 = add nsw i32 %56, 1
  %arrayidx67 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb1265, i32 0, i32 %add66
  %57 = load i32, i32* %arrayidx67, align 4
  %58 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band68 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %58, i32 0, i32 8
  %psfb1269 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band68, i32 0, i32 3
  %59 = load i32, i32* %gsfb33, align 4
  %arrayidx70 = getelementptr inbounds [7 x i32], [7 x i32]* %psfb1269, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx70, align 4
  %sub71 = sub nsw i32 %57, %60
  %add72 = add nsw i32 %54, %sub71
  store i32 %add72, i32* %end63, align 4
  %61 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %adjust_factor74 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %61, i32 0, i32 2
  %62 = load float, float* %adjust_factor74, align 4
  %63 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %psfb1275 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %63, i32 0, i32 9
  %64 = load i32, i32* %gsfb33, align 4
  %arrayidx76 = getelementptr inbounds [6 x float], [6 x float]* %psfb1275, i32 0, i32 %64
  %65 = load float, float* %arrayidx76, align 4
  %66 = load %struct.ATH_t*, %struct.ATH_t** %ATH, align 4
  %floor77 = getelementptr inbounds %struct.ATH_t, %struct.ATH_t* %66, i32 0, i32 5
  %67 = load float, float* %floor77, align 4
  %call78 = call float @athAdjust(float %62, float %65, float %67, float 0.000000e+00)
  store float %call78, float* %ath12, align 4
  %68 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt79 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %68, i32 0, i32 13
  %shortfact = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt79, i32 0, i32 1
  %arrayidx80 = getelementptr inbounds [13 x float], [13 x float]* %shortfact, i32 0, i32 12
  %69 = load float, float* %arrayidx80, align 8
  %cmp81 = fcmp ogt float %69, 0x3D71979980000000
  br i1 %cmp81, label %if.then83, label %if.end88

if.then83:                                        ; preds = %for.body42
  %70 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt84 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %70, i32 0, i32 13
  %shortfact85 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt84, i32 0, i32 1
  %arrayidx86 = getelementptr inbounds [13 x float], [13 x float]* %shortfact85, i32 0, i32 12
  %71 = load float, float* %arrayidx86, align 8
  %72 = load float, float* %ath12, align 4
  %mul87 = fmul float %72, %71
  store float %mul87, float* %ath12, align 4
  br label %if.end88

if.end88:                                         ; preds = %if.then83, %for.body42
  %73 = load i32, i32* %end63, align 4
  %sub89 = sub nsw i32 %73, 1
  store i32 %sub89, i32* %j73, align 4
  br label %for.cond90

for.cond90:                                       ; preds = %for.inc103, %if.end88
  %74 = load i32, i32* %j73, align 4
  %75 = load i32, i32* %start43, align 4
  %cmp91 = icmp sge i32 %74, %75
  br i1 %cmp91, label %for.body93, label %for.end105

for.body93:                                       ; preds = %for.cond90
  %76 = load float*, float** %xr, align 4
  %77 = load i32, i32* %j73, align 4
  %arrayidx94 = getelementptr inbounds float, float* %76, i32 %77
  %78 = load float, float* %arrayidx94, align 4
  %conv95 = fpext float %78 to double
  %79 = call double @llvm.fabs.f64(double %conv95)
  %80 = load float, float* %ath12, align 4
  %conv96 = fpext float %80 to double
  %cmp97 = fcmp olt double %79, %conv96
  br i1 %cmp97, label %if.then99, label %if.else101

if.then99:                                        ; preds = %for.body93
  %81 = load float*, float** %xr, align 4
  %82 = load i32, i32* %j73, align 4
  %arrayidx100 = getelementptr inbounds float, float* %81, i32 %82
  store float 0.000000e+00, float* %arrayidx100, align 4
  br label %if.end102

if.else101:                                       ; preds = %for.body93
  store i32 1, i32* %stop34, align 4
  br label %for.end105

if.end102:                                        ; preds = %if.then99
  br label %for.inc103

for.inc103:                                       ; preds = %if.end102
  %83 = load i32, i32* %j73, align 4
  %dec104 = add nsw i32 %83, -1
  store i32 %dec104, i32* %j73, align 4
  br label %for.cond90

for.end105:                                       ; preds = %if.else101, %for.cond90
  br label %for.inc106

for.inc106:                                       ; preds = %for.end105
  %84 = load i32, i32* %gsfb33, align 4
  %dec107 = add nsw i32 %84, -1
  store i32 %dec107, i32* %gsfb33, align 4
  br label %for.cond35

for.end108:                                       ; preds = %land.end41
  br label %for.inc109

for.inc109:                                       ; preds = %for.end108
  %85 = load i32, i32* %block, align 4
  %inc = add nsw i32 %85, 1
  store i32 %inc, i32* %block, align 4
  br label %for.cond29

for.end110:                                       ; preds = %for.cond29
  br label %if.end111

if.end111:                                        ; preds = %for.end110, %for.end27
  ret void
}

declare float @athAdjust(float, float, float, float) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @bin_search_StepSize(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, i32 %desired_rate, i32 %ch, float* %xrpow) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %desired_rate.addr = alloca i32, align 4
  %ch.addr = alloca i32, align 4
  %xrpow.addr = alloca float*, align 4
  %nBits = alloca i32, align 4
  %CurrentStep = alloca i32, align 4
  %flag_GoneOver = alloca i32, align 4
  %start = alloca i32, align 4
  %Direction = alloca i32, align 4
  %step = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32 %desired_rate, i32* %desired_rate.addr, align 4
  store i32 %ch, i32* %ch.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 13
  %CurrentStep1 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 6
  %1 = load i32, i32* %ch.addr, align 4
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %CurrentStep1, i32 0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  store i32 %2, i32* %CurrentStep, align 4
  store i32 0, i32* %flag_GoneOver, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 13
  %OldValue = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt2, i32 0, i32 5
  %4 = load i32, i32* %ch.addr, align 4
  %arrayidx3 = getelementptr inbounds [2 x i32], [2 x i32]* %OldValue, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx3, align 4
  store i32 %5, i32* %start, align 4
  store i32 0, i32* %Direction, align 4
  %6 = load i32, i32* %start, align 4
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 7
  store i32 %6, i32* %global_gain, align 4
  %8 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %8, i32 0, i32 18
  %9 = load i32, i32* %part2_length, align 4
  %10 = load i32, i32* %desired_rate.addr, align 4
  %sub = sub nsw i32 %10, %9
  store i32 %sub, i32* %desired_rate.addr, align 4
  br label %for.cond

for.cond:                                         ; preds = %if.end31, %entry
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %12 = load float*, float** %xrpow.addr, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call = call i32 @count_bits(%struct.lame_internal_flags* %11, float* %12, %struct.gr_info* %13, %struct.calc_noise_data_t* null)
  store i32 %call, i32* %nBits, align 4
  %14 = load i32, i32* %CurrentStep, align 4
  %cmp = icmp eq i32 %14, 1
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.cond
  %15 = load i32, i32* %nBits, align 4
  %16 = load i32, i32* %desired_rate.addr, align 4
  %cmp4 = icmp eq i32 %15, %16
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.cond
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false
  %17 = load i32, i32* %nBits, align 4
  %18 = load i32, i32* %desired_rate.addr, align 4
  %cmp5 = icmp sgt i32 %17, %18
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %19 = load i32, i32* %Direction, align 4
  %cmp7 = icmp eq i32 %19, 2
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.then6
  store i32 1, i32* %flag_GoneOver, align 4
  br label %if.end9

if.end9:                                          ; preds = %if.then8, %if.then6
  %20 = load i32, i32* %flag_GoneOver, align 4
  %tobool = icmp ne i32 %20, 0
  br i1 %tobool, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end9
  %21 = load i32, i32* %CurrentStep, align 4
  %div = sdiv i32 %21, 2
  store i32 %div, i32* %CurrentStep, align 4
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %if.end9
  store i32 1, i32* %Direction, align 4
  %22 = load i32, i32* %CurrentStep, align 4
  store i32 %22, i32* %step, align 4
  br label %if.end20

if.else:                                          ; preds = %if.end
  %23 = load i32, i32* %Direction, align 4
  %cmp12 = icmp eq i32 %23, 1
  br i1 %cmp12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.else
  store i32 1, i32* %flag_GoneOver, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.else
  %24 = load i32, i32* %flag_GoneOver, align 4
  %tobool15 = icmp ne i32 %24, 0
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %if.end14
  %25 = load i32, i32* %CurrentStep, align 4
  %div17 = sdiv i32 %25, 2
  store i32 %div17, i32* %CurrentStep, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then16, %if.end14
  store i32 2, i32* %Direction, align 4
  %26 = load i32, i32* %CurrentStep, align 4
  %sub19 = sub nsw i32 0, %26
  store i32 %sub19, i32* %step, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.end18, %if.end11
  %27 = load i32, i32* %step, align 4
  %28 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain21 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %28, i32 0, i32 7
  %29 = load i32, i32* %global_gain21, align 4
  %add = add nsw i32 %29, %27
  store i32 %add, i32* %global_gain21, align 4
  %30 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain22 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %30, i32 0, i32 7
  %31 = load i32, i32* %global_gain22, align 4
  %cmp23 = icmp slt i32 %31, 0
  br i1 %cmp23, label %if.then24, label %if.end26

if.then24:                                        ; preds = %if.end20
  %32 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain25 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %32, i32 0, i32 7
  store i32 0, i32* %global_gain25, align 4
  store i32 1, i32* %flag_GoneOver, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.then24, %if.end20
  %33 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %33, i32 0, i32 7
  %34 = load i32, i32* %global_gain27, align 4
  %cmp28 = icmp sgt i32 %34, 255
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.end26
  %35 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain30 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %35, i32 0, i32 7
  store i32 255, i32* %global_gain30, align 4
  store i32 1, i32* %flag_GoneOver, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.end26
  br label %for.cond

for.end:                                          ; preds = %if.then
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.end
  %36 = load i32, i32* %nBits, align 4
  %37 = load i32, i32* %desired_rate.addr, align 4
  %cmp32 = icmp sgt i32 %36, %37
  br i1 %cmp32, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %38 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain33 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %38, i32 0, i32 7
  %39 = load i32, i32* %global_gain33, align 4
  %cmp34 = icmp slt i32 %39, 255
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %40 = phi i1 [ false, %while.cond ], [ %cmp34, %land.rhs ]
  br i1 %40, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %41 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain35 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %41, i32 0, i32 7
  %42 = load i32, i32* %global_gain35, align 4
  %inc = add nsw i32 %42, 1
  store i32 %inc, i32* %global_gain35, align 4
  %43 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %44 = load float*, float** %xrpow.addr, align 4
  %45 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call36 = call i32 @count_bits(%struct.lame_internal_flags* %43, float* %44, %struct.gr_info* %45, %struct.calc_noise_data_t* null)
  store i32 %call36, i32* %nBits, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %46 = load i32, i32* %start, align 4
  %47 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain37 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %47, i32 0, i32 7
  %48 = load i32, i32* %global_gain37, align 4
  %sub38 = sub nsw i32 %46, %48
  %cmp39 = icmp sge i32 %sub38, 4
  %49 = zext i1 %cmp39 to i64
  %cond = select i1 %cmp39, i32 4, i32 2
  %50 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt40 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %50, i32 0, i32 13
  %CurrentStep41 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt40, i32 0, i32 6
  %51 = load i32, i32* %ch.addr, align 4
  %arrayidx42 = getelementptr inbounds [2 x i32], [2 x i32]* %CurrentStep41, i32 0, i32 %51
  store i32 %cond, i32* %arrayidx42, align 4
  %52 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %global_gain43 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %52, i32 0, i32 7
  %53 = load i32, i32* %global_gain43, align 4
  %54 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt44 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %54, i32 0, i32 13
  %OldValue45 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt44, i32 0, i32 5
  %55 = load i32, i32* %ch.addr, align 4
  %arrayidx46 = getelementptr inbounds [2 x i32], [2 x i32]* %OldValue45, i32 0, i32 %55
  store i32 %53, i32* %arrayidx46, align 4
  %56 = load i32, i32* %nBits, align 4
  %57 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %part2_3_length = getelementptr inbounds %struct.gr_info, %struct.gr_info* %57, i32 0, i32 4
  store i32 %56, i32* %part2_3_length, align 4
  %58 = load i32, i32* %nBits, align 4
  ret i32 %58
}

; Function Attrs: noinline nounwind optnone
define internal i32 @balance_noise(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %distort, float* %xrpow, i32 %bRefine) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %distort.addr = alloca float*, align 4
  %xrpow.addr = alloca float*, align 4
  %bRefine.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %status = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %distort, float** %distort.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store i32 %bRefine, i32* %bRefine.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %3 = load float*, float** %distort.addr, align 4
  %4 = load float*, float** %xrpow.addr, align 4
  %5 = load i32, i32* %bRefine.addr, align 4
  call void @amp_scalefac_bands(%struct.lame_internal_flags* %1, %struct.gr_info* %2, float* %3, float* %4, i32 %5)
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call = call i32 @loop_break(%struct.gr_info* %6)
  store i32 %call, i32* %status, align 4
  %7 = load i32, i32* %status, align 4
  %tobool = icmp ne i32 %7, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call2 = call i32 @scale_bitcount(%struct.lame_internal_flags* %8, %struct.gr_info* %9)
  store i32 %call2, i32* %status, align 4
  %10 = load i32, i32* %status, align 4
  %tobool3 = icmp ne i32 %10, 0
  br i1 %tobool3, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end
  store i32 1, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 3
  %12 = load i32, i32* %noise_shaping, align 4
  %cmp = icmp sgt i32 %12, 1
  br i1 %cmp, label %if.then6, label %if.end18

if.then6:                                         ; preds = %if.end5
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 13
  %pseudohalf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 7
  %arrayidx = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf, i32 0, i32 0
  %14 = bitcast i32* %arrayidx to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %14, i8 0, i32 156, i1 false)
  %15 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %15, i32 0, i32 16
  %16 = load i32, i32* %scalefac_scale, align 4
  %tobool7 = icmp ne i32 %16, 0
  br i1 %tobool7, label %if.else, label %if.then8

if.then8:                                         ; preds = %if.then6
  %17 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %18 = load float*, float** %xrpow.addr, align 4
  call void @inc_scalefac_scale(%struct.gr_info* %17, float* %18)
  store i32 0, i32* %status, align 4
  br label %if.end17

if.else:                                          ; preds = %if.then6
  %19 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %block_type = getelementptr inbounds %struct.gr_info, %struct.gr_info* %19, i32 0, i32 9
  %20 = load i32, i32* %block_type, align 4
  %cmp9 = icmp eq i32 %20, 2
  br i1 %cmp9, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %if.else
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %subblock_gain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 4
  %22 = load i32, i32* %subblock_gain, align 4
  %cmp10 = icmp sgt i32 %22, 0
  br i1 %cmp10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %land.lhs.true
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %24 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %25 = load float*, float** %xrpow.addr, align 4
  %call12 = call i32 @inc_subblock_gain(%struct.lame_internal_flags* %23, %struct.gr_info* %24, float* %25)
  %tobool13 = icmp ne i32 %call12, 0
  br i1 %tobool13, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then11
  %26 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call14 = call i32 @loop_break(%struct.gr_info* %26)
  %tobool15 = icmp ne i32 %call14, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then11
  %27 = phi i1 [ true, %if.then11 ], [ %tobool15, %lor.rhs ]
  %lor.ext = zext i1 %27 to i32
  store i32 %lor.ext, i32* %status, align 4
  br label %if.end16

if.end16:                                         ; preds = %lor.end, %land.lhs.true, %if.else
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then8
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.end5
  %28 = load i32, i32* %status, align 4
  %tobool19 = icmp ne i32 %28, 0
  br i1 %tobool19, label %if.end22, label %if.then20

if.then20:                                        ; preds = %if.end18
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %30 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %call21 = call i32 @scale_bitcount(%struct.lame_internal_flags* %29, %struct.gr_info* %30)
  store i32 %call21, i32* %status, align 4
  br label %if.end22

if.end22:                                         ; preds = %if.then20, %if.end18
  %31 = load i32, i32* %status, align 4
  %tobool23 = icmp ne i32 %31, 0
  %lnot = xor i1 %tobool23, true
  %lnot.ext = zext i1 %lnot to i32
  store i32 %lnot.ext, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then4, %if.then
  %32 = load i32, i32* %retval, align 4
  ret i32 %32
}

declare i32 @count_bits(%struct.lame_internal_flags*, float*, %struct.gr_info*, %struct.calc_noise_data_t*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @quant_compare(i32 %quant_comp, %struct.calc_noise_result_t* %best, %struct.calc_noise_result_t* %calc, %struct.gr_info* %gi, float* %distort) #0 {
entry:
  %quant_comp.addr = alloca i32, align 4
  %best.addr = alloca %struct.calc_noise_result_t*, align 4
  %calc.addr = alloca %struct.calc_noise_result_t*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %distort.addr = alloca float*, align 4
  %better = alloca i32, align 4
  store i32 %quant_comp, i32* %quant_comp.addr, align 4
  store %struct.calc_noise_result_t* %best, %struct.calc_noise_result_t** %best.addr, align 4
  store %struct.calc_noise_result_t* %calc, %struct.calc_noise_result_t** %calc.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store float* %distort, float** %distort.addr, align 4
  %0 = load i32, i32* %quant_comp.addr, align 4
  switch i32 %0, label %sw.default [
    i32 9, label %sw.bb
    i32 0, label %sw.bb24
    i32 8, label %sw.bb70
    i32 1, label %sw.bb73
    i32 2, label %sw.bb78
    i32 3, label %sw.bb83
    i32 4, label %sw.bb95
    i32 5, label %sw.bb215
    i32 6, label %sw.bb256
    i32 7, label %sw.bb333
  ]

sw.default:                                       ; preds = %entry
  br label %sw.bb

sw.bb:                                            ; preds = %entry, %sw.default
  %1 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %1, i32 0, i32 3
  %2 = load i32, i32* %over_count, align 4
  %cmp = icmp sgt i32 %2, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %3 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_SSD = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %3, i32 0, i32 4
  %4 = load i32, i32* %over_SSD, align 4
  %5 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_SSD1 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %5, i32 0, i32 4
  %6 = load i32, i32* %over_SSD1, align 4
  %cmp2 = icmp sle i32 %4, %6
  %conv = zext i1 %cmp2 to i32
  store i32 %conv, i32* %better, align 4
  %7 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_SSD3 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %7, i32 0, i32 4
  %8 = load i32, i32* %over_SSD3, align 4
  %9 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_SSD4 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %9, i32 0, i32 4
  %10 = load i32, i32* %over_SSD4, align 4
  %cmp5 = icmp eq i32 %8, %10
  br i1 %cmp5, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  %11 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %bits = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %11, i32 0, i32 5
  %12 = load i32, i32* %bits, align 4
  %13 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %bits8 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %13, i32 0, i32 5
  %14 = load i32, i32* %bits8, align 4
  %cmp9 = icmp slt i32 %12, %14
  %conv10 = zext i1 %cmp9 to i32
  store i32 %conv10, i32* %better, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  br label %if.end23

if.else:                                          ; preds = %sw.bb
  %15 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %15, i32 0, i32 2
  %16 = load float, float* %max_noise, align 4
  %cmp11 = fcmp olt float %16, 0.000000e+00
  br i1 %cmp11, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.else
  %17 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise13 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %17, i32 0, i32 2
  %18 = load float, float* %max_noise13, align 4
  %mul = fmul float %18, 1.000000e+01
  %19 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %bits14 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %19, i32 0, i32 5
  %20 = load i32, i32* %bits14, align 4
  %conv15 = sitofp i32 %20 to float
  %add = fadd float %mul, %conv15
  %21 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise16 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %21, i32 0, i32 2
  %22 = load float, float* %max_noise16, align 4
  %mul17 = fmul float %22, 1.000000e+01
  %23 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %bits18 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %23, i32 0, i32 5
  %24 = load i32, i32* %bits18, align 4
  %conv19 = sitofp i32 %24 to float
  %add20 = fadd float %mul17, %conv19
  %cmp21 = fcmp ole float %add, %add20
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.else
  %25 = phi i1 [ false, %if.else ], [ %cmp21, %land.rhs ]
  %land.ext = zext i1 %25 to i32
  store i32 %land.ext, i32* %better, align 4
  br label %if.end23

if.end23:                                         ; preds = %land.end, %if.end
  br label %sw.epilog

sw.bb24:                                          ; preds = %entry
  %26 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_count25 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %26, i32 0, i32 3
  %27 = load i32, i32* %over_count25, align 4
  %28 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count26 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %28, i32 0, i32 3
  %29 = load i32, i32* %over_count26, align 4
  %cmp27 = icmp slt i32 %27, %29
  br i1 %cmp27, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %sw.bb24
  %30 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_count29 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %30, i32 0, i32 3
  %31 = load i32, i32* %over_count29, align 4
  %32 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count30 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %32, i32 0, i32 3
  %33 = load i32, i32* %over_count30, align 4
  %cmp31 = icmp eq i32 %31, %33
  br i1 %cmp31, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %34 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %34, i32 0, i32 0
  %35 = load float, float* %over_noise, align 4
  %36 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise33 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %36, i32 0, i32 0
  %37 = load float, float* %over_noise33, align 4
  %cmp34 = fcmp olt float %35, %37
  br i1 %cmp34, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %38 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_count36 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %38, i32 0, i32 3
  %39 = load i32, i32* %over_count36, align 4
  %40 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count37 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %40, i32 0, i32 3
  %41 = load i32, i32* %over_count37, align 4
  %cmp38 = icmp eq i32 %39, %41
  br i1 %cmp38, label %land.lhs.true40, label %land.end68

land.lhs.true40:                                  ; preds = %lor.rhs
  %42 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise41 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %42, i32 0, i32 0
  %43 = load float, float* %over_noise41, align 4
  %conv42 = fpext float %43 to double
  %44 = call double @llvm.fabs.f64(double %conv42)
  %45 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise43 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %45, i32 0, i32 0
  %46 = load float, float* %over_noise43, align 4
  %conv44 = fpext float %46 to double
  %47 = call double @llvm.fabs.f64(double %conv44)
  %cmp45 = fcmp ogt double %44, %47
  br i1 %cmp45, label %cond.true, label %cond.false

cond.true:                                        ; preds = %land.lhs.true40
  %48 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise47 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %48, i32 0, i32 0
  %49 = load float, float* %over_noise47, align 4
  %50 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise48 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %50, i32 0, i32 0
  %51 = load float, float* %over_noise48, align 4
  %sub = fsub float %49, %51
  %conv49 = fpext float %sub to double
  %52 = call double @llvm.fabs.f64(double %conv49)
  %53 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise50 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %53, i32 0, i32 0
  %54 = load float, float* %over_noise50, align 4
  %conv51 = fpext float %54 to double
  %55 = call double @llvm.fabs.f64(double %conv51)
  %mul52 = fmul double %55, 0x3EB0C6F7A0000000
  %cmp53 = fcmp ole double %52, %mul52
  br i1 %cmp53, label %land.rhs64, label %land.end68

cond.false:                                       ; preds = %land.lhs.true40
  %56 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise55 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %56, i32 0, i32 0
  %57 = load float, float* %over_noise55, align 4
  %58 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise56 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %58, i32 0, i32 0
  %59 = load float, float* %over_noise56, align 4
  %sub57 = fsub float %57, %59
  %conv58 = fpext float %sub57 to double
  %60 = call double @llvm.fabs.f64(double %conv58)
  %61 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise59 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %61, i32 0, i32 0
  %62 = load float, float* %over_noise59, align 4
  %conv60 = fpext float %62 to double
  %63 = call double @llvm.fabs.f64(double %conv60)
  %mul61 = fmul double %63, 0x3EB0C6F7A0000000
  %cmp62 = fcmp ole double %60, %mul61
  br i1 %cmp62, label %land.rhs64, label %land.end68

land.rhs64:                                       ; preds = %cond.false, %cond.true
  %64 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %64, i32 0, i32 1
  %65 = load float, float* %tot_noise, align 4
  %66 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise65 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %66, i32 0, i32 1
  %67 = load float, float* %tot_noise65, align 4
  %cmp66 = fcmp olt float %65, %67
  br label %land.end68

land.end68:                                       ; preds = %land.rhs64, %cond.false, %cond.true, %lor.rhs
  %68 = phi i1 [ false, %cond.false ], [ false, %cond.true ], [ false, %lor.rhs ], [ %cmp66, %land.rhs64 ]
  br label %lor.end

lor.end:                                          ; preds = %land.end68, %land.lhs.true, %sw.bb24
  %69 = phi i1 [ true, %land.lhs.true ], [ true, %sw.bb24 ], [ %68, %land.end68 ]
  %lor.ext = zext i1 %69 to i32
  store i32 %lor.ext, i32* %better, align 4
  br label %sw.epilog

sw.bb70:                                          ; preds = %entry
  %70 = load float*, float** %distort.addr, align 4
  %71 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %call = call double @get_klemm_noise(float* %70, %struct.gr_info* %71)
  %conv71 = fptrunc double %call to float
  %72 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise72 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %72, i32 0, i32 2
  store float %conv71, float* %max_noise72, align 4
  br label %sw.bb73

sw.bb73:                                          ; preds = %entry, %sw.bb70
  %73 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise74 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %73, i32 0, i32 2
  %74 = load float, float* %max_noise74, align 4
  %75 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise75 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %75, i32 0, i32 2
  %76 = load float, float* %max_noise75, align 4
  %cmp76 = fcmp olt float %74, %76
  %conv77 = zext i1 %cmp76 to i32
  store i32 %conv77, i32* %better, align 4
  br label %sw.epilog

sw.bb78:                                          ; preds = %entry
  %77 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise79 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %77, i32 0, i32 1
  %78 = load float, float* %tot_noise79, align 4
  %79 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise80 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %79, i32 0, i32 1
  %80 = load float, float* %tot_noise80, align 4
  %cmp81 = fcmp olt float %78, %80
  %conv82 = zext i1 %cmp81 to i32
  store i32 %conv82, i32* %better, align 4
  br label %sw.epilog

sw.bb83:                                          ; preds = %entry
  %81 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise84 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %81, i32 0, i32 1
  %82 = load float, float* %tot_noise84, align 4
  %83 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise85 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %83, i32 0, i32 1
  %84 = load float, float* %tot_noise85, align 4
  %cmp86 = fcmp olt float %82, %84
  br i1 %cmp86, label %land.rhs88, label %land.end93

land.rhs88:                                       ; preds = %sw.bb83
  %85 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise89 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %85, i32 0, i32 2
  %86 = load float, float* %max_noise89, align 4
  %87 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise90 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %87, i32 0, i32 2
  %88 = load float, float* %max_noise90, align 4
  %cmp91 = fcmp olt float %86, %88
  br label %land.end93

land.end93:                                       ; preds = %land.rhs88, %sw.bb83
  %89 = phi i1 [ false, %sw.bb83 ], [ %cmp91, %land.rhs88 ]
  %land.ext94 = zext i1 %89 to i32
  store i32 %land.ext94, i32* %better, align 4
  br label %sw.epilog

sw.bb95:                                          ; preds = %entry
  %90 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise96 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %90, i32 0, i32 2
  %91 = load float, float* %max_noise96, align 4
  %conv97 = fpext float %91 to double
  %cmp98 = fcmp ole double %conv97, 0.000000e+00
  br i1 %cmp98, label %land.lhs.true100, label %lor.lhs.false105

land.lhs.true100:                                 ; preds = %sw.bb95
  %92 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise101 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %92, i32 0, i32 2
  %93 = load float, float* %max_noise101, align 4
  %conv102 = fpext float %93 to double
  %cmp103 = fcmp ogt double %conv102, 2.000000e-01
  br i1 %cmp103, label %lor.end213, label %lor.lhs.false105

lor.lhs.false105:                                 ; preds = %land.lhs.true100, %sw.bb95
  %94 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise106 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %94, i32 0, i32 2
  %95 = load float, float* %max_noise106, align 4
  %conv107 = fpext float %95 to double
  %cmp108 = fcmp ole double %conv107, 0.000000e+00
  br i1 %cmp108, label %land.lhs.true110, label %lor.lhs.false128

land.lhs.true110:                                 ; preds = %lor.lhs.false105
  %96 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise111 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %96, i32 0, i32 2
  %97 = load float, float* %max_noise111, align 4
  %conv112 = fpext float %97 to double
  %cmp113 = fcmp olt double %conv112, 0.000000e+00
  br i1 %cmp113, label %land.lhs.true115, label %lor.lhs.false128

land.lhs.true115:                                 ; preds = %land.lhs.true110
  %98 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise116 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %98, i32 0, i32 2
  %99 = load float, float* %max_noise116, align 4
  %conv117 = fpext float %99 to double
  %100 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise118 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %100, i32 0, i32 2
  %101 = load float, float* %max_noise118, align 4
  %conv119 = fpext float %101 to double
  %sub120 = fsub double %conv119, 2.000000e-01
  %cmp121 = fcmp ogt double %conv117, %sub120
  br i1 %cmp121, label %land.lhs.true123, label %lor.lhs.false128

land.lhs.true123:                                 ; preds = %land.lhs.true115
  %102 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise124 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %102, i32 0, i32 1
  %103 = load float, float* %tot_noise124, align 4
  %104 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise125 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %104, i32 0, i32 1
  %105 = load float, float* %tot_noise125, align 4
  %cmp126 = fcmp olt float %103, %105
  br i1 %cmp126, label %lor.end213, label %lor.lhs.false128

lor.lhs.false128:                                 ; preds = %land.lhs.true123, %land.lhs.true115, %land.lhs.true110, %lor.lhs.false105
  %106 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise129 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %106, i32 0, i32 2
  %107 = load float, float* %max_noise129, align 4
  %conv130 = fpext float %107 to double
  %cmp131 = fcmp ole double %conv130, 0.000000e+00
  br i1 %cmp131, label %land.lhs.true133, label %lor.lhs.false153

land.lhs.true133:                                 ; preds = %lor.lhs.false128
  %108 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise134 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %108, i32 0, i32 2
  %109 = load float, float* %max_noise134, align 4
  %conv135 = fpext float %109 to double
  %cmp136 = fcmp ogt double %conv135, 0.000000e+00
  br i1 %cmp136, label %land.lhs.true138, label %lor.lhs.false153

land.lhs.true138:                                 ; preds = %land.lhs.true133
  %110 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise139 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %110, i32 0, i32 2
  %111 = load float, float* %max_noise139, align 4
  %conv140 = fpext float %111 to double
  %112 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise141 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %112, i32 0, i32 2
  %113 = load float, float* %max_noise141, align 4
  %conv142 = fpext float %113 to double
  %sub143 = fsub double %conv142, 2.000000e-01
  %cmp144 = fcmp ogt double %conv140, %sub143
  br i1 %cmp144, label %land.lhs.true146, label %lor.lhs.false153

land.lhs.true146:                                 ; preds = %land.lhs.true138
  %114 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise147 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %114, i32 0, i32 1
  %115 = load float, float* %tot_noise147, align 4
  %116 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise148 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %116, i32 0, i32 1
  %117 = load float, float* %tot_noise148, align 4
  %118 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise149 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %118, i32 0, i32 0
  %119 = load float, float* %over_noise149, align 4
  %add150 = fadd float %117, %119
  %cmp151 = fcmp olt float %115, %add150
  br i1 %cmp151, label %lor.end213, label %lor.lhs.false153

lor.lhs.false153:                                 ; preds = %land.lhs.true146, %land.lhs.true138, %land.lhs.true133, %lor.lhs.false128
  %120 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise154 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %120, i32 0, i32 2
  %121 = load float, float* %max_noise154, align 4
  %conv155 = fpext float %121 to double
  %cmp156 = fcmp ogt double %conv155, 0.000000e+00
  br i1 %cmp156, label %land.lhs.true158, label %lor.rhs180

land.lhs.true158:                                 ; preds = %lor.lhs.false153
  %122 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise159 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %122, i32 0, i32 2
  %123 = load float, float* %max_noise159, align 4
  %conv160 = fpext float %123 to double
  %cmp161 = fcmp ogt double %conv160, -5.000000e-02
  br i1 %cmp161, label %land.lhs.true163, label %lor.rhs180

land.lhs.true163:                                 ; preds = %land.lhs.true158
  %124 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise164 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %124, i32 0, i32 2
  %125 = load float, float* %max_noise164, align 4
  %conv165 = fpext float %125 to double
  %126 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise166 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %126, i32 0, i32 2
  %127 = load float, float* %max_noise166, align 4
  %conv167 = fpext float %127 to double
  %sub168 = fsub double %conv167, 1.000000e-01
  %cmp169 = fcmp ogt double %conv165, %sub168
  br i1 %cmp169, label %land.lhs.true171, label %lor.rhs180

land.lhs.true171:                                 ; preds = %land.lhs.true163
  %128 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise172 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %128, i32 0, i32 1
  %129 = load float, float* %tot_noise172, align 4
  %130 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise173 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %130, i32 0, i32 0
  %131 = load float, float* %over_noise173, align 4
  %add174 = fadd float %129, %131
  %132 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise175 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %132, i32 0, i32 1
  %133 = load float, float* %tot_noise175, align 4
  %134 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise176 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %134, i32 0, i32 0
  %135 = load float, float* %over_noise176, align 4
  %add177 = fadd float %133, %135
  %cmp178 = fcmp olt float %add174, %add177
  br i1 %cmp178, label %lor.end213, label %lor.rhs180

lor.rhs180:                                       ; preds = %land.lhs.true171, %land.lhs.true163, %land.lhs.true158, %lor.lhs.false153
  %136 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise181 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %136, i32 0, i32 2
  %137 = load float, float* %max_noise181, align 4
  %conv182 = fpext float %137 to double
  %cmp183 = fcmp ogt double %conv182, 0.000000e+00
  br i1 %cmp183, label %land.lhs.true185, label %land.end211

land.lhs.true185:                                 ; preds = %lor.rhs180
  %138 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise186 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %138, i32 0, i32 2
  %139 = load float, float* %max_noise186, align 4
  %conv187 = fpext float %139 to double
  %cmp188 = fcmp ogt double %conv187, -1.000000e-01
  br i1 %cmp188, label %land.lhs.true190, label %land.end211

land.lhs.true190:                                 ; preds = %land.lhs.true185
  %140 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise191 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %140, i32 0, i32 2
  %141 = load float, float* %max_noise191, align 4
  %conv192 = fpext float %141 to double
  %142 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise193 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %142, i32 0, i32 2
  %143 = load float, float* %max_noise193, align 4
  %conv194 = fpext float %143 to double
  %sub195 = fsub double %conv194, 1.500000e-01
  %cmp196 = fcmp ogt double %conv192, %sub195
  br i1 %cmp196, label %land.rhs198, label %land.end211

land.rhs198:                                      ; preds = %land.lhs.true190
  %144 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise199 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %144, i32 0, i32 1
  %145 = load float, float* %tot_noise199, align 4
  %146 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise200 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %146, i32 0, i32 0
  %147 = load float, float* %over_noise200, align 4
  %add201 = fadd float %145, %147
  %148 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise202 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %148, i32 0, i32 0
  %149 = load float, float* %over_noise202, align 4
  %add203 = fadd float %add201, %149
  %150 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise204 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %150, i32 0, i32 1
  %151 = load float, float* %tot_noise204, align 4
  %152 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise205 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %152, i32 0, i32 0
  %153 = load float, float* %over_noise205, align 4
  %add206 = fadd float %151, %153
  %154 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise207 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %154, i32 0, i32 0
  %155 = load float, float* %over_noise207, align 4
  %add208 = fadd float %add206, %155
  %cmp209 = fcmp olt float %add203, %add208
  br label %land.end211

land.end211:                                      ; preds = %land.rhs198, %land.lhs.true190, %land.lhs.true185, %lor.rhs180
  %156 = phi i1 [ false, %land.lhs.true190 ], [ false, %land.lhs.true185 ], [ false, %lor.rhs180 ], [ %cmp209, %land.rhs198 ]
  br label %lor.end213

lor.end213:                                       ; preds = %land.end211, %land.lhs.true171, %land.lhs.true146, %land.lhs.true123, %land.lhs.true100
  %157 = phi i1 [ true, %land.lhs.true171 ], [ true, %land.lhs.true146 ], [ true, %land.lhs.true123 ], [ true, %land.lhs.true100 ], [ %156, %land.end211 ]
  %lor.ext214 = zext i1 %157 to i32
  store i32 %lor.ext214, i32* %better, align 4
  br label %sw.epilog

sw.bb215:                                         ; preds = %entry
  %158 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise216 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %158, i32 0, i32 0
  %159 = load float, float* %over_noise216, align 4
  %160 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise217 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %160, i32 0, i32 0
  %161 = load float, float* %over_noise217, align 4
  %cmp218 = fcmp olt float %159, %161
  br i1 %cmp218, label %lor.end254, label %lor.rhs220

lor.rhs220:                                       ; preds = %sw.bb215
  %162 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise221 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %162, i32 0, i32 0
  %163 = load float, float* %over_noise221, align 4
  %conv222 = fpext float %163 to double
  %164 = call double @llvm.fabs.f64(double %conv222)
  %165 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise223 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %165, i32 0, i32 0
  %166 = load float, float* %over_noise223, align 4
  %conv224 = fpext float %166 to double
  %167 = call double @llvm.fabs.f64(double %conv224)
  %cmp225 = fcmp ogt double %164, %167
  br i1 %cmp225, label %cond.true227, label %cond.false237

cond.true227:                                     ; preds = %lor.rhs220
  %168 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise228 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %168, i32 0, i32 0
  %169 = load float, float* %over_noise228, align 4
  %170 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise229 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %170, i32 0, i32 0
  %171 = load float, float* %over_noise229, align 4
  %sub230 = fsub float %169, %171
  %conv231 = fpext float %sub230 to double
  %172 = call double @llvm.fabs.f64(double %conv231)
  %173 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise232 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %173, i32 0, i32 0
  %174 = load float, float* %over_noise232, align 4
  %conv233 = fpext float %174 to double
  %175 = call double @llvm.fabs.f64(double %conv233)
  %mul234 = fmul double %175, 0x3EB0C6F7A0000000
  %cmp235 = fcmp ole double %172, %mul234
  br i1 %cmp235, label %land.rhs247, label %land.end252

cond.false237:                                    ; preds = %lor.rhs220
  %176 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise238 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %176, i32 0, i32 0
  %177 = load float, float* %over_noise238, align 4
  %178 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise239 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %178, i32 0, i32 0
  %179 = load float, float* %over_noise239, align 4
  %sub240 = fsub float %177, %179
  %conv241 = fpext float %sub240 to double
  %180 = call double @llvm.fabs.f64(double %conv241)
  %181 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise242 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %181, i32 0, i32 0
  %182 = load float, float* %over_noise242, align 4
  %conv243 = fpext float %182 to double
  %183 = call double @llvm.fabs.f64(double %conv243)
  %mul244 = fmul double %183, 0x3EB0C6F7A0000000
  %cmp245 = fcmp ole double %180, %mul244
  br i1 %cmp245, label %land.rhs247, label %land.end252

land.rhs247:                                      ; preds = %cond.false237, %cond.true227
  %184 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise248 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %184, i32 0, i32 1
  %185 = load float, float* %tot_noise248, align 4
  %186 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise249 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %186, i32 0, i32 1
  %187 = load float, float* %tot_noise249, align 4
  %cmp250 = fcmp olt float %185, %187
  br label %land.end252

land.end252:                                      ; preds = %land.rhs247, %cond.false237, %cond.true227
  %188 = phi i1 [ false, %cond.false237 ], [ false, %cond.true227 ], [ %cmp250, %land.rhs247 ]
  br label %lor.end254

lor.end254:                                       ; preds = %land.end252, %sw.bb215
  %189 = phi i1 [ true, %sw.bb215 ], [ %188, %land.end252 ]
  %lor.ext255 = zext i1 %189 to i32
  store i32 %lor.ext255, i32* %better, align 4
  br label %sw.epilog

sw.bb256:                                         ; preds = %entry
  %190 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise257 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %190, i32 0, i32 0
  %191 = load float, float* %over_noise257, align 4
  %192 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise258 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %192, i32 0, i32 0
  %193 = load float, float* %over_noise258, align 4
  %cmp259 = fcmp olt float %191, %193
  br i1 %cmp259, label %lor.end331, label %lor.rhs261

lor.rhs261:                                       ; preds = %sw.bb256
  %194 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise262 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %194, i32 0, i32 0
  %195 = load float, float* %over_noise262, align 4
  %conv263 = fpext float %195 to double
  %196 = call double @llvm.fabs.f64(double %conv263)
  %197 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise264 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %197, i32 0, i32 0
  %198 = load float, float* %over_noise264, align 4
  %conv265 = fpext float %198 to double
  %199 = call double @llvm.fabs.f64(double %conv265)
  %cmp266 = fcmp ogt double %196, %199
  br i1 %cmp266, label %cond.true268, label %cond.false278

cond.true268:                                     ; preds = %lor.rhs261
  %200 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise269 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %200, i32 0, i32 0
  %201 = load float, float* %over_noise269, align 4
  %202 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise270 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %202, i32 0, i32 0
  %203 = load float, float* %over_noise270, align 4
  %sub271 = fsub float %201, %203
  %conv272 = fpext float %sub271 to double
  %204 = call double @llvm.fabs.f64(double %conv272)
  %205 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise273 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %205, i32 0, i32 0
  %206 = load float, float* %over_noise273, align 4
  %conv274 = fpext float %206 to double
  %207 = call double @llvm.fabs.f64(double %conv274)
  %mul275 = fmul double %207, 0x3EB0C6F7A0000000
  %cmp276 = fcmp ole double %204, %mul275
  br i1 %cmp276, label %land.rhs288, label %land.end329

cond.false278:                                    ; preds = %lor.rhs261
  %208 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise279 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %208, i32 0, i32 0
  %209 = load float, float* %over_noise279, align 4
  %210 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise280 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %210, i32 0, i32 0
  %211 = load float, float* %over_noise280, align 4
  %sub281 = fsub float %209, %211
  %conv282 = fpext float %sub281 to double
  %212 = call double @llvm.fabs.f64(double %conv282)
  %213 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise283 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %213, i32 0, i32 0
  %214 = load float, float* %over_noise283, align 4
  %conv284 = fpext float %214 to double
  %215 = call double @llvm.fabs.f64(double %conv284)
  %mul285 = fmul double %215, 0x3EB0C6F7A0000000
  %cmp286 = fcmp ole double %212, %mul285
  br i1 %cmp286, label %land.rhs288, label %land.end329

land.rhs288:                                      ; preds = %cond.false278, %cond.true268
  %216 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise289 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %216, i32 0, i32 2
  %217 = load float, float* %max_noise289, align 4
  %218 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise290 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %218, i32 0, i32 2
  %219 = load float, float* %max_noise290, align 4
  %cmp291 = fcmp olt float %217, %219
  br i1 %cmp291, label %lor.end327, label %lor.rhs293

lor.rhs293:                                       ; preds = %land.rhs288
  %220 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise294 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %220, i32 0, i32 2
  %221 = load float, float* %max_noise294, align 4
  %conv295 = fpext float %221 to double
  %222 = call double @llvm.fabs.f64(double %conv295)
  %223 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise296 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %223, i32 0, i32 2
  %224 = load float, float* %max_noise296, align 4
  %conv297 = fpext float %224 to double
  %225 = call double @llvm.fabs.f64(double %conv297)
  %cmp298 = fcmp ogt double %222, %225
  br i1 %cmp298, label %cond.true300, label %cond.false310

cond.true300:                                     ; preds = %lor.rhs293
  %226 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise301 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %226, i32 0, i32 2
  %227 = load float, float* %max_noise301, align 4
  %228 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise302 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %228, i32 0, i32 2
  %229 = load float, float* %max_noise302, align 4
  %sub303 = fsub float %227, %229
  %conv304 = fpext float %sub303 to double
  %230 = call double @llvm.fabs.f64(double %conv304)
  %231 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise305 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %231, i32 0, i32 2
  %232 = load float, float* %max_noise305, align 4
  %conv306 = fpext float %232 to double
  %233 = call double @llvm.fabs.f64(double %conv306)
  %mul307 = fmul double %233, 0x3EB0C6F7A0000000
  %cmp308 = fcmp ole double %230, %mul307
  br i1 %cmp308, label %land.rhs320, label %land.end325

cond.false310:                                    ; preds = %lor.rhs293
  %234 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %max_noise311 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %234, i32 0, i32 2
  %235 = load float, float* %max_noise311, align 4
  %236 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise312 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %236, i32 0, i32 2
  %237 = load float, float* %max_noise312, align 4
  %sub313 = fsub float %235, %237
  %conv314 = fpext float %sub313 to double
  %238 = call double @llvm.fabs.f64(double %conv314)
  %239 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %max_noise315 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %239, i32 0, i32 2
  %240 = load float, float* %max_noise315, align 4
  %conv316 = fpext float %240 to double
  %241 = call double @llvm.fabs.f64(double %conv316)
  %mul317 = fmul double %241, 0x3EB0C6F7A0000000
  %cmp318 = fcmp ole double %238, %mul317
  br i1 %cmp318, label %land.rhs320, label %land.end325

land.rhs320:                                      ; preds = %cond.false310, %cond.true300
  %242 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %tot_noise321 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %242, i32 0, i32 1
  %243 = load float, float* %tot_noise321, align 4
  %244 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %tot_noise322 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %244, i32 0, i32 1
  %245 = load float, float* %tot_noise322, align 4
  %cmp323 = fcmp ole float %243, %245
  br label %land.end325

land.end325:                                      ; preds = %land.rhs320, %cond.false310, %cond.true300
  %246 = phi i1 [ false, %cond.false310 ], [ false, %cond.true300 ], [ %cmp323, %land.rhs320 ]
  br label %lor.end327

lor.end327:                                       ; preds = %land.end325, %land.rhs288
  %247 = phi i1 [ true, %land.rhs288 ], [ %246, %land.end325 ]
  br label %land.end329

land.end329:                                      ; preds = %lor.end327, %cond.false278, %cond.true268
  %248 = phi i1 [ false, %cond.false278 ], [ false, %cond.true268 ], [ %247, %lor.end327 ]
  br label %lor.end331

lor.end331:                                       ; preds = %land.end329, %sw.bb256
  %249 = phi i1 [ true, %sw.bb256 ], [ %248, %land.end329 ]
  %lor.ext332 = zext i1 %249 to i32
  store i32 %lor.ext332, i32* %better, align 4
  br label %sw.epilog

sw.bb333:                                         ; preds = %entry
  %250 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_count334 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %250, i32 0, i32 3
  %251 = load i32, i32* %over_count334, align 4
  %252 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count335 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %252, i32 0, i32 3
  %253 = load i32, i32* %over_count335, align 4
  %cmp336 = icmp slt i32 %251, %253
  br i1 %cmp336, label %lor.end343, label %lor.rhs338

lor.rhs338:                                       ; preds = %sw.bb333
  %254 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %over_noise339 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %254, i32 0, i32 0
  %255 = load float, float* %over_noise339, align 4
  %256 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_noise340 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %256, i32 0, i32 0
  %257 = load float, float* %over_noise340, align 4
  %cmp341 = fcmp olt float %255, %257
  br label %lor.end343

lor.end343:                                       ; preds = %lor.rhs338, %sw.bb333
  %258 = phi i1 [ true, %sw.bb333 ], [ %cmp341, %lor.rhs338 ]
  %lor.ext344 = zext i1 %258 to i32
  store i32 %lor.ext344, i32* %better, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %lor.end343, %lor.end331, %lor.end254, %lor.end213, %land.end93, %sw.bb78, %sw.bb73, %lor.end, %if.end23
  %259 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %over_count345 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %259, i32 0, i32 3
  %260 = load i32, i32* %over_count345, align 4
  %cmp346 = icmp eq i32 %260, 0
  br i1 %cmp346, label %if.then348, label %if.end356

if.then348:                                       ; preds = %sw.epilog
  %261 = load i32, i32* %better, align 4
  %tobool = icmp ne i32 %261, 0
  br i1 %tobool, label %land.rhs349, label %land.end354

land.rhs349:                                      ; preds = %if.then348
  %262 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %calc.addr, align 4
  %bits350 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %262, i32 0, i32 5
  %263 = load i32, i32* %bits350, align 4
  %264 = load %struct.calc_noise_result_t*, %struct.calc_noise_result_t** %best.addr, align 4
  %bits351 = getelementptr inbounds %struct.calc_noise_result_t, %struct.calc_noise_result_t* %264, i32 0, i32 5
  %265 = load i32, i32* %bits351, align 4
  %cmp352 = icmp slt i32 %263, %265
  br label %land.end354

land.end354:                                      ; preds = %land.rhs349, %if.then348
  %266 = phi i1 [ false, %if.then348 ], [ %cmp352, %land.rhs349 ]
  %land.ext355 = zext i1 %266 to i32
  store i32 %land.ext355, i32* %better, align 4
  br label %if.end356

if.end356:                                        ; preds = %land.end354, %sw.epilog
  %267 = load i32, i32* %better, align 4
  ret i32 %267
}

; Function Attrs: noinline nounwind optnone
define internal void @amp_scalefac_bands(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %distort, float* %xrpow, i32 %bRefine) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %distort.addr = alloca float*, align 4
  %xrpow.addr = alloca float*, align 4
  %bRefine.addr = alloca i32, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %j = alloca i32, align 4
  %sfb = alloca i32, align 4
  %ifqstep34 = alloca float, align 4
  %trigger = alloca float, align 4
  %noise_shaping_amp = alloca i32, align 4
  %width = alloca i32, align 4
  %l = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %distort, float** %distort.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store i32 %bRefine, i32* %bRefine.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 16
  %2 = load i32, i32* %scalefac_scale, align 4
  %cmp = icmp eq i32 %2, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 0x3FF4BFDAE0000000, float* %ifqstep34, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  store float 0x3FFAE89FA0000000, float* %ifqstep34, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  store float 0.000000e+00, float* %trigger, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %3 = load i32, i32* %sfb, align 4
  %4 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %4, i32 0, i32 22
  %5 = load i32, i32* %sfbmax, align 4
  %cmp2 = icmp slt i32 %3, %5
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load float, float* %trigger, align 4
  %7 = load float*, float** %distort.addr, align 4
  %8 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx, align 4
  %cmp3 = fcmp olt float %6, %9
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %for.body
  %10 = load float*, float** %distort.addr, align 4
  %11 = load i32, i32* %sfb, align 4
  %arrayidx5 = getelementptr inbounds float, float* %10, i32 %11
  %12 = load float, float* %arrayidx5, align 4
  store float %12, float* %trigger, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.then4, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end6
  %13 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp7 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 6
  %15 = load i32, i32* %noise_shaping_amp7, align 4
  store i32 %15, i32* %noise_shaping_amp, align 4
  %16 = load i32, i32* %noise_shaping_amp, align 4
  %cmp8 = icmp eq i32 %16, 3
  br i1 %cmp8, label %if.then9, label %if.end14

if.then9:                                         ; preds = %for.end
  %17 = load i32, i32* %bRefine.addr, align 4
  %cmp10 = icmp eq i32 %17, 1
  br i1 %cmp10, label %if.then11, label %if.else12

if.then11:                                        ; preds = %if.then9
  store i32 2, i32* %noise_shaping_amp, align 4
  br label %if.end13

if.else12:                                        ; preds = %if.then9
  store i32 1, i32* %noise_shaping_amp, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else12, %if.then11
  br label %if.end14

if.end14:                                         ; preds = %if.end13, %for.end
  %18 = load i32, i32* %noise_shaping_amp, align 4
  switch i32 %18, label %sw.default [
    i32 2, label %sw.bb
    i32 1, label %sw.bb15
    i32 0, label %sw.bb25
  ]

sw.bb:                                            ; preds = %if.end14
  br label %sw.epilog

sw.bb15:                                          ; preds = %if.end14
  %19 = load float, float* %trigger, align 4
  %conv = fpext float %19 to double
  %cmp16 = fcmp ogt double %conv, 1.000000e+00
  br i1 %cmp16, label %if.then18, label %if.else21

if.then18:                                        ; preds = %sw.bb15
  %20 = load float, float* %trigger, align 4
  %conv19 = fpext float %20 to double
  %21 = call double @llvm.pow.f64(double %conv19, double 5.000000e-01)
  %conv20 = fptrunc double %21 to float
  store float %conv20, float* %trigger, align 4
  br label %if.end24

if.else21:                                        ; preds = %sw.bb15
  %22 = load float, float* %trigger, align 4
  %conv22 = fpext float %22 to double
  %mul = fmul double %conv22, 0x3FEE666666666666
  %conv23 = fptrunc double %mul to float
  store float %conv23, float* %trigger, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.else21, %if.then18
  br label %sw.epilog

sw.bb25:                                          ; preds = %if.end14
  br label %sw.default

sw.default:                                       ; preds = %if.end14, %sw.bb25
  %23 = load float, float* %trigger, align 4
  %conv26 = fpext float %23 to double
  %cmp27 = fcmp ogt double %conv26, 1.000000e+00
  br i1 %cmp27, label %if.then29, label %if.else30

if.then29:                                        ; preds = %sw.default
  store float 1.000000e+00, float* %trigger, align 4
  br label %if.end34

if.else30:                                        ; preds = %sw.default
  %24 = load float, float* %trigger, align 4
  %conv31 = fpext float %24 to double
  %mul32 = fmul double %conv31, 0x3FEE666666666666
  %conv33 = fptrunc double %mul32 to float
  store float %conv33, float* %trigger, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.else30, %if.then29
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end34, %if.end24, %sw.bb
  store i32 0, i32* %j, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc90, %sw.epilog
  %25 = load i32, i32* %sfb, align 4
  %26 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax36 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %26, i32 0, i32 22
  %27 = load i32, i32* %sfbmax36, align 4
  %cmp37 = icmp slt i32 %25, %27
  br i1 %cmp37, label %for.body39, label %for.end92

for.body39:                                       ; preds = %for.cond35
  %28 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width40 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %28, i32 0, i32 25
  %29 = load i32, i32* %sfb, align 4
  %arrayidx41 = getelementptr inbounds [39 x i32], [39 x i32]* %width40, i32 0, i32 %29
  %30 = load i32, i32* %arrayidx41, align 4
  store i32 %30, i32* %width, align 4
  %31 = load i32, i32* %width, align 4
  %32 = load i32, i32* %j, align 4
  %add = add nsw i32 %32, %31
  store i32 %add, i32* %j, align 4
  %33 = load float*, float** %distort.addr, align 4
  %34 = load i32, i32* %sfb, align 4
  %arrayidx42 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx42, align 4
  %36 = load float, float* %trigger, align 4
  %cmp43 = fcmp olt float %35, %36
  br i1 %cmp43, label %if.then45, label %if.end46

if.then45:                                        ; preds = %for.body39
  br label %for.inc90

if.end46:                                         ; preds = %for.body39
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %37, i32 0, i32 13
  %substep_shaping = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt, i32 0, i32 9
  %38 = load i32, i32* %substep_shaping, align 8
  %and = and i32 %38, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then47, label %if.end63

if.then47:                                        ; preds = %if.end46
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt48 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 13
  %pseudohalf = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt48, i32 0, i32 7
  %40 = load i32, i32* %sfb, align 4
  %arrayidx49 = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx49, align 4
  %tobool50 = icmp ne i32 %41, 0
  %lnot = xor i1 %tobool50, true
  %lnot.ext = zext i1 %lnot to i32
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt51 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %42, i32 0, i32 13
  %pseudohalf52 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt51, i32 0, i32 7
  %43 = load i32, i32* %sfb, align 4
  %arrayidx53 = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf52, i32 0, i32 %43
  store i32 %lnot.ext, i32* %arrayidx53, align 4
  %44 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %sv_qnt54 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %44, i32 0, i32 13
  %pseudohalf55 = getelementptr inbounds %struct.QntStateVar_t, %struct.QntStateVar_t* %sv_qnt54, i32 0, i32 7
  %45 = load i32, i32* %sfb, align 4
  %arrayidx56 = getelementptr inbounds [39 x i32], [39 x i32]* %pseudohalf55, i32 0, i32 %45
  %46 = load i32, i32* %arrayidx56, align 4
  %tobool57 = icmp ne i32 %46, 0
  br i1 %tobool57, label %if.end62, label %land.lhs.true

land.lhs.true:                                    ; preds = %if.then47
  %47 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp58 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %47, i32 0, i32 6
  %48 = load i32, i32* %noise_shaping_amp58, align 4
  %cmp59 = icmp eq i32 %48, 2
  br i1 %cmp59, label %if.then61, label %if.end62

if.then61:                                        ; preds = %land.lhs.true
  br label %for.end92

if.end62:                                         ; preds = %land.lhs.true, %if.then47
  br label %if.end63

if.end63:                                         ; preds = %if.end62, %if.end46
  %49 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %49, i32 0, i32 2
  %50 = load i32, i32* %sfb, align 4
  %arrayidx64 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %50
  %51 = load i32, i32* %arrayidx64, align 4
  %inc65 = add nsw i32 %51, 1
  store i32 %inc65, i32* %arrayidx64, align 4
  %52 = load i32, i32* %width, align 4
  %sub = sub nsw i32 0, %52
  store i32 %sub, i32* %l, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc82, %if.end63
  %53 = load i32, i32* %l, align 4
  %cmp67 = icmp slt i32 %53, 0
  br i1 %cmp67, label %for.body69, label %for.end84

for.body69:                                       ; preds = %for.cond66
  %54 = load float, float* %ifqstep34, align 4
  %55 = load float*, float** %xrpow.addr, align 4
  %56 = load i32, i32* %j, align 4
  %57 = load i32, i32* %l, align 4
  %add70 = add nsw i32 %56, %57
  %arrayidx71 = getelementptr inbounds float, float* %55, i32 %add70
  %58 = load float, float* %arrayidx71, align 4
  %mul72 = fmul float %58, %54
  store float %mul72, float* %arrayidx71, align 4
  %59 = load float*, float** %xrpow.addr, align 4
  %60 = load i32, i32* %j, align 4
  %61 = load i32, i32* %l, align 4
  %add73 = add nsw i32 %60, %61
  %arrayidx74 = getelementptr inbounds float, float* %59, i32 %add73
  %62 = load float, float* %arrayidx74, align 4
  %63 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %63, i32 0, i32 3
  %64 = load float, float* %xrpow_max, align 4
  %cmp75 = fcmp ogt float %62, %64
  br i1 %cmp75, label %if.then77, label %if.end81

if.then77:                                        ; preds = %for.body69
  %65 = load float*, float** %xrpow.addr, align 4
  %66 = load i32, i32* %j, align 4
  %67 = load i32, i32* %l, align 4
  %add78 = add nsw i32 %66, %67
  %arrayidx79 = getelementptr inbounds float, float* %65, i32 %add78
  %68 = load float, float* %arrayidx79, align 4
  %69 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max80 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %69, i32 0, i32 3
  store float %68, float* %xrpow_max80, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then77, %for.body69
  br label %for.inc82

for.inc82:                                        ; preds = %if.end81
  %70 = load i32, i32* %l, align 4
  %inc83 = add nsw i32 %70, 1
  store i32 %inc83, i32* %l, align 4
  br label %for.cond66

for.end84:                                        ; preds = %for.cond66
  %71 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping_amp85 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %71, i32 0, i32 6
  %72 = load i32, i32* %noise_shaping_amp85, align 4
  %cmp86 = icmp eq i32 %72, 2
  br i1 %cmp86, label %if.then88, label %if.end89

if.then88:                                        ; preds = %for.end84
  br label %for.end92

if.end89:                                         ; preds = %for.end84
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89, %if.then45
  %73 = load i32, i32* %sfb, align 4
  %inc91 = add nsw i32 %73, 1
  store i32 %inc91, i32* %sfb, align 4
  br label %for.cond35

for.end92:                                        ; preds = %if.then61, %if.then88, %for.cond35
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @loop_break(%struct.gr_info* %cod_info) #0 {
entry:
  %retval = alloca i32, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %sfb = alloca i32, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %sfb, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 22
  %2 = load i32, i32* %sfbmax, align 4
  %cmp = icmp slt i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 2
  %4 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 12
  %7 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %window = getelementptr inbounds %struct.gr_info, %struct.gr_info* %7, i32 0, i32 26
  %8 = load i32, i32* %sfb, align 4
  %arrayidx1 = getelementptr inbounds [39 x i32], [39 x i32]* %window, i32 0, i32 %8
  %9 = load i32, i32* %arrayidx1, align 4
  %arrayidx2 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %9
  %10 = load i32, i32* %arrayidx2, align 4
  %add = add nsw i32 %5, %10
  %cmp3 = icmp eq i32 %add, 0
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %12 = load i32, i32* %retval, align 4
  ret i32 %12
}

declare i32 @scale_bitcount(%struct.lame_internal_flags*, %struct.gr_info*) #1

; Function Attrs: noinline nounwind optnone
define internal void @inc_scalefac_scale(%struct.gr_info* %cod_info, float* %xrpow) #0 {
entry:
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %xrpow.addr = alloca float*, align 4
  %l = alloca i32, align 4
  %j = alloca i32, align 4
  %sfb = alloca i32, align 4
  %ifqstep34 = alloca float, align 4
  %width = alloca i32, align 4
  %s = alloca i32, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  store float 0x3FF4BFDAE0000000, float* %ifqstep34, align 4
  store i32 0, i32* %j, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc24, %entry
  %0 = load i32, i32* %sfb, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 22
  %2 = load i32, i32* %sfbmax, align 4
  %cmp = icmp slt i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end26

for.body:                                         ; preds = %for.cond
  %3 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %3, i32 0, i32 25
  %4 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds [39 x i32], [39 x i32]* %width1, i32 0, i32 %4
  %5 = load i32, i32* %arrayidx, align 4
  store i32 %5, i32* %width, align 4
  %6 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac = getelementptr inbounds %struct.gr_info, %struct.gr_info* %6, i32 0, i32 2
  %7 = load i32, i32* %sfb, align 4
  %arrayidx2 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac, i32 0, i32 %7
  %8 = load i32, i32* %arrayidx2, align 4
  store i32 %8, i32* %s, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 15
  %10 = load i32, i32* %preflag, align 4
  %tobool = icmp ne i32 %10, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %11 = load i32, i32* %sfb, align 4
  %arrayidx3 = getelementptr inbounds [22 x i32], [22 x i32]* @pretab, i32 0, i32 %11
  %12 = load i32, i32* %arrayidx3, align 4
  %13 = load i32, i32* %s, align 4
  %add = add nsw i32 %13, %12
  store i32 %add, i32* %s, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %14 = load i32, i32* %width, align 4
  %15 = load i32, i32* %j, align 4
  %add4 = add nsw i32 %15, %14
  store i32 %add4, i32* %j, align 4
  %16 = load i32, i32* %s, align 4
  %and = and i32 %16, 1
  %tobool5 = icmp ne i32 %and, 0
  br i1 %tobool5, label %if.then6, label %if.end21

if.then6:                                         ; preds = %if.end
  %17 = load i32, i32* %s, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %s, align 4
  %18 = load i32, i32* %width, align 4
  %sub = sub nsw i32 0, %18
  store i32 %sub, i32* %l, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc, %if.then6
  %19 = load i32, i32* %l, align 4
  %cmp8 = icmp slt i32 %19, 0
  br i1 %cmp8, label %for.body9, label %for.end

for.body9:                                        ; preds = %for.cond7
  %20 = load float*, float** %xrpow.addr, align 4
  %21 = load i32, i32* %j, align 4
  %22 = load i32, i32* %l, align 4
  %add10 = add nsw i32 %21, %22
  %arrayidx11 = getelementptr inbounds float, float* %20, i32 %add10
  %23 = load float, float* %arrayidx11, align 4
  %mul = fmul float %23, 0x3FF4BFDAE0000000
  store float %mul, float* %arrayidx11, align 4
  %24 = load float*, float** %xrpow.addr, align 4
  %25 = load i32, i32* %j, align 4
  %26 = load i32, i32* %l, align 4
  %add12 = add nsw i32 %25, %26
  %arrayidx13 = getelementptr inbounds float, float* %24, i32 %add12
  %27 = load float, float* %arrayidx13, align 4
  %28 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %28, i32 0, i32 3
  %29 = load float, float* %xrpow_max, align 4
  %cmp14 = fcmp ogt float %27, %29
  br i1 %cmp14, label %if.then15, label %if.end19

if.then15:                                        ; preds = %for.body9
  %30 = load float*, float** %xrpow.addr, align 4
  %31 = load i32, i32* %j, align 4
  %32 = load i32, i32* %l, align 4
  %add16 = add nsw i32 %31, %32
  %arrayidx17 = getelementptr inbounds float, float* %30, i32 %add16
  %33 = load float, float* %arrayidx17, align 4
  %34 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max18 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %34, i32 0, i32 3
  store float %33, float* %xrpow_max18, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then15, %for.body9
  br label %for.inc

for.inc:                                          ; preds = %if.end19
  %35 = load i32, i32* %l, align 4
  %inc20 = add nsw i32 %35, 1
  store i32 %inc20, i32* %l, align 4
  br label %for.cond7

for.end:                                          ; preds = %for.cond7
  br label %if.end21

if.end21:                                         ; preds = %for.end, %if.end
  %36 = load i32, i32* %s, align 4
  %shr = ashr i32 %36, 1
  %37 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac22 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %37, i32 0, i32 2
  %38 = load i32, i32* %sfb, align 4
  %arrayidx23 = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac22, i32 0, i32 %38
  store i32 %shr, i32* %arrayidx23, align 4
  br label %for.inc24

for.inc24:                                        ; preds = %if.end21
  %39 = load i32, i32* %sfb, align 4
  %inc25 = add nsw i32 %39, 1
  store i32 %inc25, i32* %sfb, align 4
  br label %for.cond

for.end26:                                        ; preds = %for.cond
  %40 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %preflag27 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %40, i32 0, i32 15
  store i32 0, i32* %preflag27, align 4
  %41 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %41, i32 0, i32 16
  store i32 1, i32* %scalefac_scale, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @inc_subblock_gain(%struct.lame_internal_flags* %gfc, %struct.gr_info* %cod_info, float* %xrpow) #0 {
entry:
  %retval = alloca i32, align 4
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %cod_info.addr = alloca %struct.gr_info*, align 4
  %xrpow.addr = alloca float*, align 4
  %sfb = alloca i32, align 4
  %window = alloca i32, align 4
  %scalefac = alloca i32*, align 4
  %s1 = alloca i32, align 4
  %s2 = alloca i32, align 4
  %l = alloca i32, align 4
  %j = alloca i32, align 4
  %amp = alloca float, align 4
  %width = alloca i32, align 4
  %s = alloca i32, align 4
  %gain = alloca i32, align 4
  %amp90 = alloca float, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store %struct.gr_info* %cod_info, %struct.gr_info** %cod_info.addr, align 4
  store float* %xrpow, float** %xrpow.addr, align 4
  %0 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac1 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %0, i32 0, i32 2
  %arraydecay = getelementptr inbounds [39 x i32], [39 x i32]* %scalefac1, i32 0, i32 0
  store i32* %arraydecay, i32** %scalefac, align 4
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %sfb, align 4
  %2 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %2, i32 0, i32 19
  %3 = load i32, i32* %sfb_lmax, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %scalefac, align 4
  %5 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  %cmp2 = icmp sge i32 %6, 16
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %window, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc117, %for.end
  %8 = load i32, i32* %window, align 4
  %cmp4 = icmp slt i32 %8, 3
  br i1 %cmp4, label %for.body5, label %for.end119

for.body5:                                        ; preds = %for.cond3
  store i32 0, i32* %s2, align 4
  store i32 0, i32* %s1, align 4
  %9 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax6 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %9, i32 0, i32 19
  %10 = load i32, i32* %sfb_lmax6, align 4
  %11 = load i32, i32* %window, align 4
  %add = add nsw i32 %10, %11
  store i32 %add, i32* %sfb, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc15, %for.body5
  %12 = load i32, i32* %sfb, align 4
  %13 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbdivide = getelementptr inbounds %struct.gr_info, %struct.gr_info* %13, i32 0, i32 24
  %14 = load i32, i32* %sfbdivide, align 4
  %cmp8 = icmp slt i32 %12, %14
  br i1 %cmp8, label %for.body9, label %for.end17

for.body9:                                        ; preds = %for.cond7
  %15 = load i32, i32* %s1, align 4
  %16 = load i32*, i32** %scalefac, align 4
  %17 = load i32, i32* %sfb, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %16, i32 %17
  %18 = load i32, i32* %arrayidx10, align 4
  %cmp11 = icmp slt i32 %15, %18
  br i1 %cmp11, label %if.then12, label %if.end14

if.then12:                                        ; preds = %for.body9
  %19 = load i32*, i32** %scalefac, align 4
  %20 = load i32, i32* %sfb, align 4
  %arrayidx13 = getelementptr inbounds i32, i32* %19, i32 %20
  %21 = load i32, i32* %arrayidx13, align 4
  store i32 %21, i32* %s1, align 4
  br label %if.end14

if.end14:                                         ; preds = %if.then12, %for.body9
  br label %for.inc15

for.inc15:                                        ; preds = %if.end14
  %22 = load i32, i32* %sfb, align 4
  %add16 = add nsw i32 %22, 3
  store i32 %add16, i32* %sfb, align 4
  br label %for.cond7

for.end17:                                        ; preds = %for.cond7
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc26, %for.end17
  %23 = load i32, i32* %sfb, align 4
  %24 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %24, i32 0, i32 22
  %25 = load i32, i32* %sfbmax, align 4
  %cmp19 = icmp slt i32 %23, %25
  br i1 %cmp19, label %for.body20, label %for.end28

for.body20:                                       ; preds = %for.cond18
  %26 = load i32, i32* %s2, align 4
  %27 = load i32*, i32** %scalefac, align 4
  %28 = load i32, i32* %sfb, align 4
  %arrayidx21 = getelementptr inbounds i32, i32* %27, i32 %28
  %29 = load i32, i32* %arrayidx21, align 4
  %cmp22 = icmp slt i32 %26, %29
  br i1 %cmp22, label %if.then23, label %if.end25

if.then23:                                        ; preds = %for.body20
  %30 = load i32*, i32** %scalefac, align 4
  %31 = load i32, i32* %sfb, align 4
  %arrayidx24 = getelementptr inbounds i32, i32* %30, i32 %31
  %32 = load i32, i32* %arrayidx24, align 4
  store i32 %32, i32* %s2, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then23, %for.body20
  br label %for.inc26

for.inc26:                                        ; preds = %if.end25
  %33 = load i32, i32* %sfb, align 4
  %add27 = add nsw i32 %33, 3
  store i32 %add27, i32* %sfb, align 4
  br label %for.cond18

for.end28:                                        ; preds = %for.cond18
  %34 = load i32, i32* %s1, align 4
  %cmp29 = icmp slt i32 %34, 16
  br i1 %cmp29, label %land.lhs.true, label %if.end32

land.lhs.true:                                    ; preds = %for.end28
  %35 = load i32, i32* %s2, align 4
  %cmp30 = icmp slt i32 %35, 8
  br i1 %cmp30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %land.lhs.true
  br label %for.inc117

if.end32:                                         ; preds = %land.lhs.true, %for.end28
  %36 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain = getelementptr inbounds %struct.gr_info, %struct.gr_info* %36, i32 0, i32 12
  %37 = load i32, i32* %window, align 4
  %arrayidx33 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain, i32 0, i32 %37
  %38 = load i32, i32* %arrayidx33, align 4
  %cmp34 = icmp sge i32 %38, 7
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end32
  store i32 1, i32* %retval, align 4
  br label %return

if.end36:                                         ; preds = %if.end32
  %39 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %subblock_gain37 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %39, i32 0, i32 12
  %40 = load i32, i32* %window, align 4
  %arrayidx38 = getelementptr inbounds [4 x i32], [4 x i32]* %subblock_gain37, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx38, align 4
  %inc39 = add nsw i32 %41, 1
  store i32 %inc39, i32* %arrayidx38, align 4
  %42 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %scalefac_band = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %42, i32 0, i32 8
  %l40 = getelementptr inbounds %struct.scalefac_struct, %struct.scalefac_struct* %scalefac_band, i32 0, i32 0
  %43 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax41 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %43, i32 0, i32 19
  %44 = load i32, i32* %sfb_lmax41, align 4
  %arrayidx42 = getelementptr inbounds [23 x i32], [23 x i32]* %l40, i32 0, i32 %44
  %45 = load i32, i32* %arrayidx42, align 4
  store i32 %45, i32* %j, align 4
  %46 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfb_lmax43 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %46, i32 0, i32 19
  %47 = load i32, i32* %sfb_lmax43, align 4
  %48 = load i32, i32* %window, align 4
  %add44 = add nsw i32 %47, %48
  store i32 %add44, i32* %sfb, align 4
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc87, %if.end36
  %49 = load i32, i32* %sfb, align 4
  %50 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %sfbmax46 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %50, i32 0, i32 22
  %51 = load i32, i32* %sfbmax46, align 4
  %cmp47 = icmp slt i32 %49, %51
  br i1 %cmp47, label %for.body48, label %for.end89

for.body48:                                       ; preds = %for.cond45
  %52 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width49 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %52, i32 0, i32 25
  %53 = load i32, i32* %sfb, align 4
  %arrayidx50 = getelementptr inbounds [39 x i32], [39 x i32]* %width49, i32 0, i32 %53
  %54 = load i32, i32* %arrayidx50, align 4
  store i32 %54, i32* %width, align 4
  %55 = load i32*, i32** %scalefac, align 4
  %56 = load i32, i32* %sfb, align 4
  %arrayidx51 = getelementptr inbounds i32, i32* %55, i32 %56
  %57 = load i32, i32* %arrayidx51, align 4
  store i32 %57, i32* %s, align 4
  %58 = load i32, i32* %s, align 4
  %59 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale = getelementptr inbounds %struct.gr_info, %struct.gr_info* %59, i32 0, i32 16
  %60 = load i32, i32* %scalefac_scale, align 4
  %shr = ashr i32 4, %60
  %sub = sub nsw i32 %58, %shr
  store i32 %sub, i32* %s, align 4
  %61 = load i32, i32* %s, align 4
  %cmp52 = icmp sge i32 %61, 0
  br i1 %cmp52, label %if.then53, label %if.end56

if.then53:                                        ; preds = %for.body48
  %62 = load i32, i32* %s, align 4
  %63 = load i32*, i32** %scalefac, align 4
  %64 = load i32, i32* %sfb, align 4
  %arrayidx54 = getelementptr inbounds i32, i32* %63, i32 %64
  store i32 %62, i32* %arrayidx54, align 4
  %65 = load i32, i32* %width, align 4
  %mul = mul nsw i32 %65, 3
  %66 = load i32, i32* %j, align 4
  %add55 = add nsw i32 %66, %mul
  store i32 %add55, i32* %j, align 4
  br label %for.inc87

if.end56:                                         ; preds = %for.body48
  %67 = load i32*, i32** %scalefac, align 4
  %68 = load i32, i32* %sfb, align 4
  %arrayidx57 = getelementptr inbounds i32, i32* %67, i32 %68
  store i32 0, i32* %arrayidx57, align 4
  %69 = load i32, i32* %s, align 4
  %70 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %scalefac_scale58 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %70, i32 0, i32 16
  %71 = load i32, i32* %scalefac_scale58, align 4
  %add59 = add nsw i32 %71, 1
  %shl = shl i32 %69, %add59
  %add60 = add nsw i32 210, %shl
  store i32 %add60, i32* %gain, align 4
  %72 = load i32, i32* %gain, align 4
  %arrayidx61 = getelementptr inbounds [257 x float], [257 x float]* @ipow20, i32 0, i32 %72
  %73 = load float, float* %arrayidx61, align 4
  store float %73, float* %amp, align 4
  %74 = load i32, i32* %width, align 4
  %75 = load i32, i32* %window, align 4
  %add62 = add nsw i32 %75, 1
  %mul63 = mul nsw i32 %74, %add62
  %76 = load i32, i32* %j, align 4
  %add64 = add nsw i32 %76, %mul63
  store i32 %add64, i32* %j, align 4
  %77 = load i32, i32* %width, align 4
  %sub65 = sub nsw i32 0, %77
  store i32 %sub65, i32* %l, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc80, %if.end56
  %78 = load i32, i32* %l, align 4
  %cmp67 = icmp slt i32 %78, 0
  br i1 %cmp67, label %for.body68, label %for.end82

for.body68:                                       ; preds = %for.cond66
  %79 = load float, float* %amp, align 4
  %80 = load float*, float** %xrpow.addr, align 4
  %81 = load i32, i32* %j, align 4
  %82 = load i32, i32* %l, align 4
  %add69 = add nsw i32 %81, %82
  %arrayidx70 = getelementptr inbounds float, float* %80, i32 %add69
  %83 = load float, float* %arrayidx70, align 4
  %mul71 = fmul float %83, %79
  store float %mul71, float* %arrayidx70, align 4
  %84 = load float*, float** %xrpow.addr, align 4
  %85 = load i32, i32* %j, align 4
  %86 = load i32, i32* %l, align 4
  %add72 = add nsw i32 %85, %86
  %arrayidx73 = getelementptr inbounds float, float* %84, i32 %add72
  %87 = load float, float* %arrayidx73, align 4
  %88 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max = getelementptr inbounds %struct.gr_info, %struct.gr_info* %88, i32 0, i32 3
  %89 = load float, float* %xrpow_max, align 4
  %cmp74 = fcmp ogt float %87, %89
  br i1 %cmp74, label %if.then75, label %if.end79

if.then75:                                        ; preds = %for.body68
  %90 = load float*, float** %xrpow.addr, align 4
  %91 = load i32, i32* %j, align 4
  %92 = load i32, i32* %l, align 4
  %add76 = add nsw i32 %91, %92
  %arrayidx77 = getelementptr inbounds float, float* %90, i32 %add76
  %93 = load float, float* %arrayidx77, align 4
  %94 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max78 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %94, i32 0, i32 3
  store float %93, float* %xrpow_max78, align 4
  br label %if.end79

if.end79:                                         ; preds = %if.then75, %for.body68
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %95 = load i32, i32* %l, align 4
  %inc81 = add nsw i32 %95, 1
  store i32 %inc81, i32* %l, align 4
  br label %for.cond66

for.end82:                                        ; preds = %for.cond66
  %96 = load i32, i32* %width, align 4
  %97 = load i32, i32* %window, align 4
  %sub83 = sub nsw i32 3, %97
  %sub84 = sub nsw i32 %sub83, 1
  %mul85 = mul nsw i32 %96, %sub84
  %98 = load i32, i32* %j, align 4
  %add86 = add nsw i32 %98, %mul85
  store i32 %add86, i32* %j, align 4
  br label %for.inc87

for.inc87:                                        ; preds = %for.end82, %if.then53
  %99 = load i32, i32* %sfb, align 4
  %add88 = add nsw i32 %99, 3
  store i32 %add88, i32* %sfb, align 4
  br label %for.cond45

for.end89:                                        ; preds = %for.cond45
  %100 = load float, float* getelementptr inbounds ([257 x float], [257 x float]* @ipow20, i32 0, i32 202), align 8
  store float %100, float* %amp90, align 4
  %101 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width91 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %101, i32 0, i32 25
  %102 = load i32, i32* %sfb, align 4
  %arrayidx92 = getelementptr inbounds [39 x i32], [39 x i32]* %width91, i32 0, i32 %102
  %103 = load i32, i32* %arrayidx92, align 4
  %104 = load i32, i32* %window, align 4
  %add93 = add nsw i32 %104, 1
  %mul94 = mul nsw i32 %103, %add93
  %105 = load i32, i32* %j, align 4
  %add95 = add nsw i32 %105, %mul94
  store i32 %add95, i32* %j, align 4
  %106 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %width96 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %106, i32 0, i32 25
  %107 = load i32, i32* %sfb, align 4
  %arrayidx97 = getelementptr inbounds [39 x i32], [39 x i32]* %width96, i32 0, i32 %107
  %108 = load i32, i32* %arrayidx97, align 4
  %sub98 = sub nsw i32 0, %108
  store i32 %sub98, i32* %l, align 4
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc114, %for.end89
  %109 = load i32, i32* %l, align 4
  %cmp100 = icmp slt i32 %109, 0
  br i1 %cmp100, label %for.body101, label %for.end116

for.body101:                                      ; preds = %for.cond99
  %110 = load float, float* %amp90, align 4
  %111 = load float*, float** %xrpow.addr, align 4
  %112 = load i32, i32* %j, align 4
  %113 = load i32, i32* %l, align 4
  %add102 = add nsw i32 %112, %113
  %arrayidx103 = getelementptr inbounds float, float* %111, i32 %add102
  %114 = load float, float* %arrayidx103, align 4
  %mul104 = fmul float %114, %110
  store float %mul104, float* %arrayidx103, align 4
  %115 = load float*, float** %xrpow.addr, align 4
  %116 = load i32, i32* %j, align 4
  %117 = load i32, i32* %l, align 4
  %add105 = add nsw i32 %116, %117
  %arrayidx106 = getelementptr inbounds float, float* %115, i32 %add105
  %118 = load float, float* %arrayidx106, align 4
  %119 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max107 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %119, i32 0, i32 3
  %120 = load float, float* %xrpow_max107, align 4
  %cmp108 = fcmp ogt float %118, %120
  br i1 %cmp108, label %if.then109, label %if.end113

if.then109:                                       ; preds = %for.body101
  %121 = load float*, float** %xrpow.addr, align 4
  %122 = load i32, i32* %j, align 4
  %123 = load i32, i32* %l, align 4
  %add110 = add nsw i32 %122, %123
  %arrayidx111 = getelementptr inbounds float, float* %121, i32 %add110
  %124 = load float, float* %arrayidx111, align 4
  %125 = load %struct.gr_info*, %struct.gr_info** %cod_info.addr, align 4
  %xrpow_max112 = getelementptr inbounds %struct.gr_info, %struct.gr_info* %125, i32 0, i32 3
  store float %124, float* %xrpow_max112, align 4
  br label %if.end113

if.end113:                                        ; preds = %if.then109, %for.body101
  br label %for.inc114

for.inc114:                                       ; preds = %if.end113
  %126 = load i32, i32* %l, align 4
  %inc115 = add nsw i32 %126, 1
  store i32 %inc115, i32* %l, align 4
  br label %for.cond99

for.end116:                                       ; preds = %for.cond99
  br label %for.inc117

for.inc117:                                       ; preds = %for.end116, %if.then31
  %127 = load i32, i32* %window, align 4
  %inc118 = add nsw i32 %127, 1
  store i32 %inc118, i32* %window, align 4
  br label %for.cond3

for.end119:                                       ; preds = %for.cond3
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end119, %if.then35, %if.then
  %128 = load i32, i32* %retval, align 4
  ret i32 %128
}

; Function Attrs: noinline nounwind optnone
define internal double @get_klemm_noise(float* %distort, %struct.gr_info* %gi) #0 {
entry:
  %distort.addr = alloca float*, align 4
  %gi.addr = alloca %struct.gr_info*, align 4
  %sfb = alloca i32, align 4
  %klemm_noise = alloca double, align 8
  store float* %distort, float** %distort.addr, align 4
  store %struct.gr_info* %gi, %struct.gr_info** %gi.addr, align 4
  store double 1.000000e-37, double* %klemm_noise, align 8
  store i32 0, i32* %sfb, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %sfb, align 4
  %1 = load %struct.gr_info*, %struct.gr_info** %gi.addr, align 4
  %psymax = getelementptr inbounds %struct.gr_info, %struct.gr_info* %1, i32 0, i32 23
  %2 = load i32, i32* %psymax, align 4
  %cmp = icmp slt i32 %0, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %distort.addr, align 4
  %4 = load i32, i32* %sfb, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = load float, float* %arrayidx, align 4
  %conv = fpext float %5 to double
  %call = call double @penalties(double %conv)
  %6 = load double, double* %klemm_noise, align 8
  %add = fadd double %6, %call
  store double %add, double* %klemm_noise, align 8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %sfb, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %sfb, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load double, double* %klemm_noise, align 8
  %cmp1 = fcmp ogt double 0x3BC79CA10C924223, %8
  br i1 %cmp1, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  br label %cond.end

cond.false:                                       ; preds = %for.end
  %9 = load double, double* %klemm_noise, align 8
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ 0x3BC79CA10C924223, %cond.true ], [ %9, %cond.false ]
  ret double %cond
}

; Function Attrs: noinline nounwind optnone
define internal double @penalties(double %noise) #0 {
entry:
  %noise.addr = alloca double, align 8
  store double %noise, double* %noise.addr, align 8
  %0 = load double, double* %noise.addr, align 8
  %mul = fmul double 6.320000e-01, %0
  %1 = load double, double* %noise.addr, align 8
  %mul1 = fmul double %mul, %1
  %2 = load double, double* %noise.addr, align 8
  %mul2 = fmul double %mul1, %2
  %add = fadd double 3.680000e-01, %mul2
  %conv = fptrunc double %add to float
  %call = call float @fast_log2(float %conv)
  %conv3 = fpext float %call to double
  %mul4 = fmul double %conv3, 0x3FD34413509F79FE
  ret double %mul4
}

declare float @fast_log2(float) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { noreturn "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { noreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
