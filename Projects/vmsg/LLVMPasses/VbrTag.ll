; ModuleID = 'VbrTag.c'
source_filename = "VbrTag.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.lame_internal_flags = type { i32, i32, i32, i32, i32, %struct.SessionConfig_t, %struct.bit_stream_struc, %struct.III_side_info_t, %struct.scalefac_struct, %struct.PsyStateVar_t, %struct.PsyResult_t, %struct.EncStateVar_t, %struct.EncResult_t, %struct.QntStateVar_t, %struct.RpgStateVar_t, %struct.RpgResult_t, %struct.id3tag_spec, i16, i16, %struct.anon.1, %struct.VBR_seek_info_t, %struct.ATH_t*, %struct.PsyConst_t*, %struct.plotting_data*, %struct.hip_global_struct*, i32 (i32*, i32*, i32*)*, void (float*, i32)*, void (%struct.gr_info*, float*, i32, float*)*, void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.SessionConfig_t = type { i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, float, float, float, i32, i32, i32, i32, float, float, float, float, float, float, float, float, float, float, [2 x [2 x float]], float }
%struct.bit_stream_struc = type { i8*, i32, i32, i32, i32 }
%struct.III_side_info_t = type { [2 x [2 x %struct.gr_info]], i32, i32, i32, i32, [2 x [4 x i32]] }
%struct.gr_info = type { [576 x float], [576 x i32], [39 x i32], float, i32, i32, i32, i32, i32, i32, i32, [3 x i32], [4 x i32], i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, [39 x i32], [39 x i32], i32, i32*, [4 x i32], i32, [39 x i8] }
%struct.scalefac_struct = type { [23 x i32], [14 x i32], [7 x i32], [7 x i32] }
%struct.PsyStateVar_t = type { [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x [64 x float]], [4 x %struct.III_psy_xmin], [4 x %struct.III_psy_xmin], [2 x float], [4 x float], [4 x [9 x float]], [4 x i32], [2 x i32] }
%struct.III_psy_xmin = type { [22 x float], [13 x [3 x float]] }
%struct.PsyResult_t = type { [2 x [2 x float]] }
%struct.EncStateVar_t = type { [2 x [2 x [18 x [32 x float]]]], [32 x float], [2 x double], [2 x float*], [641 x float*], [19 x float], i32, i32, [256 x %struct.anon], i32, i32, i32, i32, i32, i32, float*, float*, [2 x [3984 x float]], i32, i32 }
%struct.anon = type { i32, i32, [40 x i8] }
%struct.EncResult_t = type { [16 x [5 x i32]], [16 x [6 x i32]], i32, i32, i32, i32, i32, i32 }
%struct.QntStateVar_t = type { [22 x float], [13 x float], float, float, float, [2 x i32], [2 x i32], [39 x i32], i32, i32, [576 x i8] }
%struct.RpgStateVar_t = type { %struct.replaygain_data* }
%struct.replaygain_data = type opaque
%struct.RpgResult_t = type { float, float, i32, i32 }
%struct.id3tag_spec = type { i32, i32, i8*, i8*, i8*, i8*, i32, i32, i8*, i32, i32, i32, [4 x i8], %struct.FrameDataNode*, %struct.FrameDataNode* }
%struct.FrameDataNode = type { %struct.FrameDataNode*, i32, [4 x i8], %struct.anon.0, %struct.anon.0 }
%struct.anon.0 = type { %union.anon, i32, i32 }
%union.anon = type { i8* }
%struct.anon.1 = type { i32 }
%struct.VBR_seek_info_t = type { i32, i32, i32, i32, i32, i32*, i32, i32, i32 }
%struct.ATH_t = type { i32, float, float, float, float, float, [22 x float], [13 x float], [6 x float], [6 x float], [64 x float], [64 x float], [512 x float] }
%struct.PsyConst_t = type { [1024 x float], [128 x float], %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, %struct.PsyConst_CB2SB_t, [4 x float], float, i32 }
%struct.PsyConst_CB2SB_t = type { [64 x float], [64 x float], [64 x float], [64 x float], [22 x float], [22 x float], float, [64 x [2 x i32]], [64 x i32], [22 x i32], [22 x i32], i32, i32, float* }
%struct.plotting_data = type opaque
%struct.hip_global_struct = type opaque
%struct.VBRTAGDATA = type { i32, i32, i32, i32, i32, i32, [100 x i8], i32, i32, i32 }
%struct.lame_global_struct = type { i32, i32, i32, i32, i32, float, float, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i32, i32, i32, i32, i32, i32, float, float, i32, i32, i32, i32, float, float, i32, float, i32, i32, float, float, i32, float, float, float, %struct.anon.2, i32, %struct.lame_internal_flags*, %struct.anon.3 }
%struct.anon.2 = type { void (i8*, i8*)*, void (i8*, i8*)*, void (i8*, i8*)* }
%struct.anon.3 = type { i32, i32, i32 }
%struct._IO_FILE = type opaque

@bitrate_table = external constant [3 x [16 x i32]], align 16
@samplerate_table = external constant [3 x [4 x i32]], align 16
@.str = private unnamed_addr constant [40 x i8] c"Error: can't allocate VbrFrames buffer\0A\00", align 1
@VBRTag1 = internal constant [5 x i8] c"Info\00", align 1
@VBRTag0 = internal constant [5 x i8] c"Xing\00", align 1
@crc16_lookup = internal constant [256 x i32] [i32 0, i32 49345, i32 49537, i32 320, i32 49921, i32 960, i32 640, i32 49729, i32 50689, i32 1728, i32 1920, i32 51009, i32 1280, i32 50625, i32 50305, i32 1088, i32 52225, i32 3264, i32 3456, i32 52545, i32 3840, i32 53185, i32 52865, i32 3648, i32 2560, i32 51905, i32 52097, i32 2880, i32 51457, i32 2496, i32 2176, i32 51265, i32 55297, i32 6336, i32 6528, i32 55617, i32 6912, i32 56257, i32 55937, i32 6720, i32 7680, i32 57025, i32 57217, i32 8000, i32 56577, i32 7616, i32 7296, i32 56385, i32 5120, i32 54465, i32 54657, i32 5440, i32 55041, i32 6080, i32 5760, i32 54849, i32 53761, i32 4800, i32 4992, i32 54081, i32 4352, i32 53697, i32 53377, i32 4160, i32 61441, i32 12480, i32 12672, i32 61761, i32 13056, i32 62401, i32 62081, i32 12864, i32 13824, i32 63169, i32 63361, i32 14144, i32 62721, i32 13760, i32 13440, i32 62529, i32 15360, i32 64705, i32 64897, i32 15680, i32 65281, i32 16320, i32 16000, i32 65089, i32 64001, i32 15040, i32 15232, i32 64321, i32 14592, i32 63937, i32 63617, i32 14400, i32 10240, i32 59585, i32 59777, i32 10560, i32 60161, i32 11200, i32 10880, i32 59969, i32 60929, i32 11968, i32 12160, i32 61249, i32 11520, i32 60865, i32 60545, i32 11328, i32 58369, i32 9408, i32 9600, i32 58689, i32 9984, i32 59329, i32 59009, i32 9792, i32 8704, i32 58049, i32 58241, i32 9024, i32 57601, i32 8640, i32 8320, i32 57409, i32 40961, i32 24768, i32 24960, i32 41281, i32 25344, i32 41921, i32 41601, i32 25152, i32 26112, i32 42689, i32 42881, i32 26432, i32 42241, i32 26048, i32 25728, i32 42049, i32 27648, i32 44225, i32 44417, i32 27968, i32 44801, i32 28608, i32 28288, i32 44609, i32 43521, i32 27328, i32 27520, i32 43841, i32 26880, i32 43457, i32 43137, i32 26688, i32 30720, i32 47297, i32 47489, i32 31040, i32 47873, i32 31680, i32 31360, i32 47681, i32 48641, i32 32448, i32 32640, i32 48961, i32 32000, i32 48577, i32 48257, i32 31808, i32 46081, i32 29888, i32 30080, i32 46401, i32 30464, i32 47041, i32 46721, i32 30272, i32 29184, i32 45761, i32 45953, i32 29504, i32 45313, i32 29120, i32 28800, i32 45121, i32 20480, i32 37057, i32 37249, i32 20800, i32 37633, i32 21440, i32 21120, i32 37441, i32 38401, i32 22208, i32 22400, i32 38721, i32 21760, i32 38337, i32 38017, i32 21568, i32 39937, i32 23744, i32 23936, i32 40257, i32 24320, i32 40897, i32 40577, i32 24128, i32 23040, i32 39617, i32 39809, i32 23360, i32 39169, i32 22976, i32 22656, i32 38977, i32 34817, i32 18624, i32 18816, i32 35137, i32 19200, i32 35777, i32 35457, i32 19008, i32 19968, i32 36545, i32 36737, i32 20288, i32 36097, i32 19904, i32 19584, i32 35905, i32 17408, i32 33985, i32 34177, i32 17728, i32 34561, i32 18368, i32 18048, i32 34369, i32 33281, i32 17088, i32 17280, i32 33601, i32 16640, i32 33217, i32 32897, i32 16448], align 16
@__const.PutLameVBR.vbr_type_translator = private unnamed_addr constant [7 x i8] c"\01\05\03\02\04\00\03", align 1
@.str.1 = private unnamed_addr constant [4 x i8] c"ID3\00", align 1

; Function Attrs: noinline nounwind optnone
define hidden void @AddVbrFrame(%struct.lame_internal_flags* %gfc) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %kbps = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg, i32 0, i32 0
  %1 = load i32, i32* %version, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %1
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 12
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 2
  %3 = load i32, i32* %bitrate_index, align 8
  %arrayidx1 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx1, align 4
  store i32 %4, i32* %kbps, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 20
  %6 = load i32, i32* %kbps, align 4
  call void @addVbr(%struct.VBR_seek_info_t* %VBR_seek_table, i32 %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @addVbr(%struct.VBR_seek_info_t* %v, i32 %bitrate) #0 {
entry:
  %v.addr = alloca %struct.VBR_seek_info_t*, align 4
  %bitrate.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.VBR_seek_info_t* %v, %struct.VBR_seek_info_t** %v.addr, align 4
  store i32 %bitrate, i32* %bitrate.addr, align 4
  %0 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %nVbrNumFrames = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %0, i32 0, i32 6
  %1 = load i32, i32* %nVbrNumFrames, align 4
  %inc = add i32 %1, 1
  store i32 %inc, i32* %nVbrNumFrames, align 4
  %2 = load i32, i32* %bitrate.addr, align 4
  %3 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %sum = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %3, i32 0, i32 0
  %4 = load i32, i32* %sum, align 4
  %add = add nsw i32 %4, %2
  store i32 %add, i32* %sum, align 4
  %5 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %seen = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %5, i32 0, i32 1
  %6 = load i32, i32* %seen, align 4
  %inc1 = add nsw i32 %6, 1
  store i32 %inc1, i32* %seen, align 4
  %7 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %seen2 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %7, i32 0, i32 1
  %8 = load i32, i32* %seen2, align 4
  %9 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %want = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %9, i32 0, i32 2
  %10 = load i32, i32* %want, align 4
  %cmp = icmp slt i32 %8, %10
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end25

if.end:                                           ; preds = %entry
  %11 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %11, i32 0, i32 3
  %12 = load i32, i32* %pos, align 4
  %13 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %size = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %13, i32 0, i32 4
  %14 = load i32, i32* %size, align 4
  %cmp3 = icmp slt i32 %12, %14
  br i1 %cmp3, label %if.then4, label %if.end10

if.then4:                                         ; preds = %if.end
  %15 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %sum5 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %15, i32 0, i32 0
  %16 = load i32, i32* %sum5, align 4
  %17 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %bag = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %17, i32 0, i32 5
  %18 = load i32*, i32** %bag, align 4
  %19 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos6 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %19, i32 0, i32 3
  %20 = load i32, i32* %pos6, align 4
  %arrayidx = getelementptr inbounds i32, i32* %18, i32 %20
  store i32 %16, i32* %arrayidx, align 4
  %21 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos7 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %21, i32 0, i32 3
  %22 = load i32, i32* %pos7, align 4
  %inc8 = add nsw i32 %22, 1
  store i32 %inc8, i32* %pos7, align 4
  %23 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %seen9 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %23, i32 0, i32 1
  store i32 0, i32* %seen9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.then4, %if.end
  %24 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos11 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %24, i32 0, i32 3
  %25 = load i32, i32* %pos11, align 4
  %26 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %size12 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %26, i32 0, i32 4
  %27 = load i32, i32* %size12, align 4
  %cmp13 = icmp eq i32 %25, %27
  br i1 %cmp13, label %if.then14, label %if.end25

if.then14:                                        ; preds = %if.end10
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then14
  %28 = load i32, i32* %i, align 4
  %29 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %size15 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %29, i32 0, i32 4
  %30 = load i32, i32* %size15, align 4
  %cmp16 = icmp slt i32 %28, %30
  br i1 %cmp16, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %31 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %bag17 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %31, i32 0, i32 5
  %32 = load i32*, i32** %bag17, align 4
  %33 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds i32, i32* %32, i32 %33
  %34 = load i32, i32* %arrayidx18, align 4
  %35 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %bag19 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %35, i32 0, i32 5
  %36 = load i32*, i32** %bag19, align 4
  %37 = load i32, i32* %i, align 4
  %div = sdiv i32 %37, 2
  %arrayidx20 = getelementptr inbounds i32, i32* %36, i32 %div
  store i32 %34, i32* %arrayidx20, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %38 = load i32, i32* %i, align 4
  %add21 = add nsw i32 %38, 2
  store i32 %add21, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %39 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %want22 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %39, i32 0, i32 2
  %40 = load i32, i32* %want22, align 4
  %mul = mul nsw i32 %40, 2
  store i32 %mul, i32* %want22, align 4
  %41 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos23 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %41, i32 0, i32 3
  %42 = load i32, i32* %pos23, align 4
  %div24 = sdiv i32 %42, 2
  store i32 %div24, i32* %pos23, align 4
  br label %if.end25

if.end25:                                         ; preds = %if.then, %for.end, %if.end10
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @GetVbrTag(%struct.VBRTAGDATA* %pTagData, i8* %buf) #0 {
entry:
  %retval = alloca i32, align 4
  %pTagData.addr = alloca %struct.VBRTAGDATA*, align 4
  %buf.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %head_flags = alloca i32, align 4
  %h_bitrate = alloca i32, align 4
  %h_id = alloca i32, align 4
  %h_mode = alloca i32, align 4
  %h_sr_index = alloca i32, align 4
  %h_layer = alloca i32, align 4
  %enc_delay = alloca i32, align 4
  %enc_padding = alloca i32, align 4
  store %struct.VBRTAGDATA* %pTagData, %struct.VBRTAGDATA** %pTagData.addr, align 4
  store i8* %buf, i8** %buf.addr, align 4
  %0 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %flags = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %0, i32 0, i32 2
  store i32 0, i32* %flags, align 4
  %1 = load i8*, i8** %buf.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 1
  %2 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %shr = ashr i32 %conv, 1
  %and = and i32 %shr, 3
  store i32 %and, i32* %h_layer, align 4
  %3 = load i32, i32* %h_layer, align 4
  %cmp = icmp ne i32 %3, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i8*, i8** %buf.addr, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %4, i32 1
  %5 = load i8, i8* %arrayidx2, align 1
  %conv3 = zext i8 %5 to i32
  %shr4 = ashr i32 %conv3, 3
  %and5 = and i32 %shr4, 1
  store i32 %and5, i32* %h_id, align 4
  %6 = load i8*, i8** %buf.addr, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %6, i32 2
  %7 = load i8, i8* %arrayidx6, align 1
  %conv7 = zext i8 %7 to i32
  %shr8 = ashr i32 %conv7, 2
  %and9 = and i32 %shr8, 3
  store i32 %and9, i32* %h_sr_index, align 4
  %8 = load i8*, i8** %buf.addr, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %8, i32 3
  %9 = load i8, i8* %arrayidx10, align 1
  %conv11 = zext i8 %9 to i32
  %shr12 = ashr i32 %conv11, 6
  %and13 = and i32 %shr12, 3
  store i32 %and13, i32* %h_mode, align 4
  %10 = load i8*, i8** %buf.addr, align 4
  %arrayidx14 = getelementptr inbounds i8, i8* %10, i32 2
  %11 = load i8, i8* %arrayidx14, align 1
  %conv15 = zext i8 %11 to i32
  %shr16 = ashr i32 %conv15, 4
  %and17 = and i32 %shr16, 15
  store i32 %and17, i32* %h_bitrate, align 4
  %12 = load i32, i32* %h_id, align 4
  %arrayidx18 = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %12
  %13 = load i32, i32* %h_bitrate, align 4
  %arrayidx19 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx18, i32 0, i32 %13
  %14 = load i32, i32* %arrayidx19, align 4
  store i32 %14, i32* %h_bitrate, align 4
  %15 = load i8*, i8** %buf.addr, align 4
  %arrayidx20 = getelementptr inbounds i8, i8* %15, i32 1
  %16 = load i8, i8* %arrayidx20, align 1
  %conv21 = zext i8 %16 to i32
  %shr22 = ashr i32 %conv21, 4
  %cmp23 = icmp eq i32 %shr22, 14
  br i1 %cmp23, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end
  %17 = load i32, i32* %h_sr_index, align 4
  %arrayidx26 = getelementptr inbounds [4 x i32], [4 x i32]* getelementptr inbounds ([3 x [4 x i32]], [3 x [4 x i32]]* @samplerate_table, i32 0, i32 2), i32 0, i32 %17
  %18 = load i32, i32* %arrayidx26, align 4
  %19 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %samprate = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %19, i32 0, i32 1
  store i32 %18, i32* %samprate, align 4
  br label %if.end30

if.else:                                          ; preds = %if.end
  %20 = load i32, i32* %h_id, align 4
  %arrayidx27 = getelementptr inbounds [3 x [4 x i32]], [3 x [4 x i32]]* @samplerate_table, i32 0, i32 %20
  %21 = load i32, i32* %h_sr_index, align 4
  %arrayidx28 = getelementptr inbounds [4 x i32], [4 x i32]* %arrayidx27, i32 0, i32 %21
  %22 = load i32, i32* %arrayidx28, align 4
  %23 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %samprate29 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %23, i32 0, i32 1
  store i32 %22, i32* %samprate29, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.else, %if.then25
  %24 = load i32, i32* %h_id, align 4
  %tobool = icmp ne i32 %24, 0
  br i1 %tobool, label %if.then31, label %if.else38

if.then31:                                        ; preds = %if.end30
  %25 = load i32, i32* %h_mode, align 4
  %cmp32 = icmp ne i32 %25, 3
  br i1 %cmp32, label %if.then34, label %if.else35

if.then34:                                        ; preds = %if.then31
  %26 = load i8*, i8** %buf.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %26, i32 36
  store i8* %add.ptr, i8** %buf.addr, align 4
  br label %if.end37

if.else35:                                        ; preds = %if.then31
  %27 = load i8*, i8** %buf.addr, align 4
  %add.ptr36 = getelementptr inbounds i8, i8* %27, i32 21
  store i8* %add.ptr36, i8** %buf.addr, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else35, %if.then34
  br label %if.end46

if.else38:                                        ; preds = %if.end30
  %28 = load i32, i32* %h_mode, align 4
  %cmp39 = icmp ne i32 %28, 3
  br i1 %cmp39, label %if.then41, label %if.else43

if.then41:                                        ; preds = %if.else38
  %29 = load i8*, i8** %buf.addr, align 4
  %add.ptr42 = getelementptr inbounds i8, i8* %29, i32 21
  store i8* %add.ptr42, i8** %buf.addr, align 4
  br label %if.end45

if.else43:                                        ; preds = %if.else38
  %30 = load i8*, i8** %buf.addr, align 4
  %add.ptr44 = getelementptr inbounds i8, i8* %30, i32 13
  store i8* %add.ptr44, i8** %buf.addr, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.else43, %if.then41
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.end37
  %31 = load i8*, i8** %buf.addr, align 4
  %call = call i32 @IsVbrTag(i8* %31)
  %tobool47 = icmp ne i32 %call, 0
  br i1 %tobool47, label %if.end49, label %if.then48

if.then48:                                        ; preds = %if.end46
  store i32 0, i32* %retval, align 4
  br label %return

if.end49:                                         ; preds = %if.end46
  %32 = load i8*, i8** %buf.addr, align 4
  %add.ptr50 = getelementptr inbounds i8, i8* %32, i32 4
  store i8* %add.ptr50, i8** %buf.addr, align 4
  %33 = load i32, i32* %h_id, align 4
  %34 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %h_id51 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %34, i32 0, i32 0
  store i32 %33, i32* %h_id51, align 4
  %35 = load i8*, i8** %buf.addr, align 4
  %call52 = call i32 @ExtractI4(i8* %35)
  %36 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %flags53 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %36, i32 0, i32 2
  store i32 %call52, i32* %flags53, align 4
  store i32 %call52, i32* %head_flags, align 4
  %37 = load i8*, i8** %buf.addr, align 4
  %add.ptr54 = getelementptr inbounds i8, i8* %37, i32 4
  store i8* %add.ptr54, i8** %buf.addr, align 4
  %38 = load i32, i32* %head_flags, align 4
  %and55 = and i32 %38, 1
  %tobool56 = icmp ne i32 %and55, 0
  br i1 %tobool56, label %if.then57, label %if.end60

if.then57:                                        ; preds = %if.end49
  %39 = load i8*, i8** %buf.addr, align 4
  %call58 = call i32 @ExtractI4(i8* %39)
  %40 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %frames = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %40, i32 0, i32 3
  store i32 %call58, i32* %frames, align 4
  %41 = load i8*, i8** %buf.addr, align 4
  %add.ptr59 = getelementptr inbounds i8, i8* %41, i32 4
  store i8* %add.ptr59, i8** %buf.addr, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then57, %if.end49
  %42 = load i32, i32* %head_flags, align 4
  %and61 = and i32 %42, 2
  %tobool62 = icmp ne i32 %and61, 0
  br i1 %tobool62, label %if.then63, label %if.end66

if.then63:                                        ; preds = %if.end60
  %43 = load i8*, i8** %buf.addr, align 4
  %call64 = call i32 @ExtractI4(i8* %43)
  %44 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %bytes = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %44, i32 0, i32 4
  store i32 %call64, i32* %bytes, align 4
  %45 = load i8*, i8** %buf.addr, align 4
  %add.ptr65 = getelementptr inbounds i8, i8* %45, i32 4
  store i8* %add.ptr65, i8** %buf.addr, align 4
  br label %if.end66

if.end66:                                         ; preds = %if.then63, %if.end60
  %46 = load i32, i32* %head_flags, align 4
  %and67 = and i32 %46, 4
  %tobool68 = icmp ne i32 %and67, 0
  br i1 %tobool68, label %if.then69, label %if.end80

if.then69:                                        ; preds = %if.end66
  %47 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %toc = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %47, i32 0, i32 6
  %arraydecay = getelementptr inbounds [100 x i8], [100 x i8]* %toc, i32 0, i32 0
  %cmp70 = icmp ne i8* %arraydecay, null
  br i1 %cmp70, label %if.then72, label %if.end78

if.then72:                                        ; preds = %if.then69
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then72
  %48 = load i32, i32* %i, align 4
  %cmp73 = icmp slt i32 %48, 100
  br i1 %cmp73, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %49 = load i8*, i8** %buf.addr, align 4
  %50 = load i32, i32* %i, align 4
  %arrayidx75 = getelementptr inbounds i8, i8* %49, i32 %50
  %51 = load i8, i8* %arrayidx75, align 1
  %52 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %toc76 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %52, i32 0, i32 6
  %53 = load i32, i32* %i, align 4
  %arrayidx77 = getelementptr inbounds [100 x i8], [100 x i8]* %toc76, i32 0, i32 %53
  store i8 %51, i8* %arrayidx77, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %54 = load i32, i32* %i, align 4
  %inc = add nsw i32 %54, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end78

if.end78:                                         ; preds = %for.end, %if.then69
  %55 = load i8*, i8** %buf.addr, align 4
  %add.ptr79 = getelementptr inbounds i8, i8* %55, i32 100
  store i8* %add.ptr79, i8** %buf.addr, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.end78, %if.end66
  %56 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %vbr_scale = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %56, i32 0, i32 5
  store i32 -1, i32* %vbr_scale, align 4
  %57 = load i32, i32* %head_flags, align 4
  %and81 = and i32 %57, 8
  %tobool82 = icmp ne i32 %and81, 0
  br i1 %tobool82, label %if.then83, label %if.end87

if.then83:                                        ; preds = %if.end80
  %58 = load i8*, i8** %buf.addr, align 4
  %call84 = call i32 @ExtractI4(i8* %58)
  %59 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %vbr_scale85 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %59, i32 0, i32 5
  store i32 %call84, i32* %vbr_scale85, align 4
  %60 = load i8*, i8** %buf.addr, align 4
  %add.ptr86 = getelementptr inbounds i8, i8* %60, i32 4
  store i8* %add.ptr86, i8** %buf.addr, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.then83, %if.end80
  %61 = load i32, i32* %h_id, align 4
  %add = add nsw i32 %61, 1
  %mul = mul nsw i32 %add, 72000
  %62 = load i32, i32* %h_bitrate, align 4
  %mul88 = mul nsw i32 %mul, %62
  %63 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %samprate89 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %63, i32 0, i32 1
  %64 = load i32, i32* %samprate89, align 4
  %div = sdiv i32 %mul88, %64
  %65 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %headersize = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %65, i32 0, i32 7
  store i32 %div, i32* %headersize, align 4
  %66 = load i8*, i8** %buf.addr, align 4
  %add.ptr90 = getelementptr inbounds i8, i8* %66, i32 21
  store i8* %add.ptr90, i8** %buf.addr, align 4
  %67 = load i8*, i8** %buf.addr, align 4
  %arrayidx91 = getelementptr inbounds i8, i8* %67, i32 0
  %68 = load i8, i8* %arrayidx91, align 1
  %conv92 = zext i8 %68 to i32
  %shl = shl i32 %conv92, 4
  store i32 %shl, i32* %enc_delay, align 4
  %69 = load i8*, i8** %buf.addr, align 4
  %arrayidx93 = getelementptr inbounds i8, i8* %69, i32 1
  %70 = load i8, i8* %arrayidx93, align 1
  %conv94 = zext i8 %70 to i32
  %shr95 = ashr i32 %conv94, 4
  %71 = load i32, i32* %enc_delay, align 4
  %add96 = add nsw i32 %71, %shr95
  store i32 %add96, i32* %enc_delay, align 4
  %72 = load i8*, i8** %buf.addr, align 4
  %arrayidx97 = getelementptr inbounds i8, i8* %72, i32 1
  %73 = load i8, i8* %arrayidx97, align 1
  %conv98 = zext i8 %73 to i32
  %and99 = and i32 %conv98, 15
  %shl100 = shl i32 %and99, 8
  store i32 %shl100, i32* %enc_padding, align 4
  %74 = load i8*, i8** %buf.addr, align 4
  %arrayidx101 = getelementptr inbounds i8, i8* %74, i32 2
  %75 = load i8, i8* %arrayidx101, align 1
  %conv102 = zext i8 %75 to i32
  %76 = load i32, i32* %enc_padding, align 4
  %add103 = add nsw i32 %76, %conv102
  store i32 %add103, i32* %enc_padding, align 4
  %77 = load i32, i32* %enc_delay, align 4
  %cmp104 = icmp slt i32 %77, 0
  br i1 %cmp104, label %if.then108, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end87
  %78 = load i32, i32* %enc_delay, align 4
  %cmp106 = icmp sgt i32 %78, 3000
  br i1 %cmp106, label %if.then108, label %if.end109

if.then108:                                       ; preds = %lor.lhs.false, %if.end87
  store i32 -1, i32* %enc_delay, align 4
  br label %if.end109

if.end109:                                        ; preds = %if.then108, %lor.lhs.false
  %79 = load i32, i32* %enc_padding, align 4
  %cmp110 = icmp slt i32 %79, 0
  br i1 %cmp110, label %if.then115, label %lor.lhs.false112

lor.lhs.false112:                                 ; preds = %if.end109
  %80 = load i32, i32* %enc_padding, align 4
  %cmp113 = icmp sgt i32 %80, 3000
  br i1 %cmp113, label %if.then115, label %if.end116

if.then115:                                       ; preds = %lor.lhs.false112, %if.end109
  store i32 -1, i32* %enc_padding, align 4
  br label %if.end116

if.end116:                                        ; preds = %if.then115, %lor.lhs.false112
  %81 = load i32, i32* %enc_delay, align 4
  %82 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %enc_delay117 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %82, i32 0, i32 8
  store i32 %81, i32* %enc_delay117, align 4
  %83 = load i32, i32* %enc_padding, align 4
  %84 = load %struct.VBRTAGDATA*, %struct.VBRTAGDATA** %pTagData.addr, align 4
  %enc_padding118 = getelementptr inbounds %struct.VBRTAGDATA, %struct.VBRTAGDATA* %84, i32 0, i32 9
  store i32 %83, i32* %enc_padding118, align 4
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end116, %if.then48, %if.then
  %85 = load i32, i32* %retval, align 4
  ret i32 %85
}

; Function Attrs: noinline nounwind optnone
define internal i32 @IsVbrTag(i8* %buf) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %isTag0 = alloca i32, align 4
  %isTag1 = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  %0 = load i8*, i8** %buf.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  %2 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 0), align 1
  %conv1 = sext i8 %2 to i32
  %cmp = icmp eq i32 %conv, %conv1
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %3 = load i8*, i8** %buf.addr, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx3, align 1
  %conv4 = zext i8 %4 to i32
  %5 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 1), align 1
  %conv5 = sext i8 %5 to i32
  %cmp6 = icmp eq i32 %conv4, %conv5
  br i1 %cmp6, label %land.lhs.true8, label %land.end

land.lhs.true8:                                   ; preds = %land.lhs.true
  %6 = load i8*, i8** %buf.addr, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %6, i32 2
  %7 = load i8, i8* %arrayidx9, align 1
  %conv10 = zext i8 %7 to i32
  %8 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 2), align 1
  %conv11 = sext i8 %8 to i32
  %cmp12 = icmp eq i32 %conv10, %conv11
  br i1 %cmp12, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true8
  %9 = load i8*, i8** %buf.addr, align 4
  %arrayidx14 = getelementptr inbounds i8, i8* %9, i32 3
  %10 = load i8, i8* %arrayidx14, align 1
  %conv15 = zext i8 %10 to i32
  %11 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 3), align 1
  %conv16 = sext i8 %11 to i32
  %cmp17 = icmp eq i32 %conv15, %conv16
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true8, %land.lhs.true, %entry
  %12 = phi i1 [ false, %land.lhs.true8 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp17, %land.rhs ]
  %land.ext = zext i1 %12 to i32
  store i32 %land.ext, i32* %isTag0, align 4
  %13 = load i8*, i8** %buf.addr, align 4
  %arrayidx19 = getelementptr inbounds i8, i8* %13, i32 0
  %14 = load i8, i8* %arrayidx19, align 1
  %conv20 = zext i8 %14 to i32
  %15 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 0), align 1
  %conv21 = sext i8 %15 to i32
  %cmp22 = icmp eq i32 %conv20, %conv21
  br i1 %cmp22, label %land.lhs.true24, label %land.end42

land.lhs.true24:                                  ; preds = %land.end
  %16 = load i8*, i8** %buf.addr, align 4
  %arrayidx25 = getelementptr inbounds i8, i8* %16, i32 1
  %17 = load i8, i8* %arrayidx25, align 1
  %conv26 = zext i8 %17 to i32
  %18 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 1), align 1
  %conv27 = sext i8 %18 to i32
  %cmp28 = icmp eq i32 %conv26, %conv27
  br i1 %cmp28, label %land.lhs.true30, label %land.end42

land.lhs.true30:                                  ; preds = %land.lhs.true24
  %19 = load i8*, i8** %buf.addr, align 4
  %arrayidx31 = getelementptr inbounds i8, i8* %19, i32 2
  %20 = load i8, i8* %arrayidx31, align 1
  %conv32 = zext i8 %20 to i32
  %21 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 2), align 1
  %conv33 = sext i8 %21 to i32
  %cmp34 = icmp eq i32 %conv32, %conv33
  br i1 %cmp34, label %land.rhs36, label %land.end42

land.rhs36:                                       ; preds = %land.lhs.true30
  %22 = load i8*, i8** %buf.addr, align 4
  %arrayidx37 = getelementptr inbounds i8, i8* %22, i32 3
  %23 = load i8, i8* %arrayidx37, align 1
  %conv38 = zext i8 %23 to i32
  %24 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 3), align 1
  %conv39 = sext i8 %24 to i32
  %cmp40 = icmp eq i32 %conv38, %conv39
  br label %land.end42

land.end42:                                       ; preds = %land.rhs36, %land.lhs.true30, %land.lhs.true24, %land.end
  %25 = phi i1 [ false, %land.lhs.true30 ], [ false, %land.lhs.true24 ], [ false, %land.end ], [ %cmp40, %land.rhs36 ]
  %land.ext43 = zext i1 %25 to i32
  store i32 %land.ext43, i32* %isTag1, align 4
  %26 = load i32, i32* %isTag0, align 4
  %tobool = icmp ne i32 %26, 0
  br i1 %tobool, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.end42
  %27 = load i32, i32* %isTag1, align 4
  %tobool44 = icmp ne i32 %27, 0
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.end42
  %28 = phi i1 [ true, %land.end42 ], [ %tobool44, %lor.rhs ]
  %lor.ext = zext i1 %28 to i32
  ret i32 %lor.ext
}

; Function Attrs: noinline nounwind optnone
define internal i32 @ExtractI4(i8* %buf) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %x = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  %0 = load i8*, i8** %buf.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 0
  %1 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %1 to i32
  store i32 %conv, i32* %x, align 4
  %2 = load i32, i32* %x, align 4
  %shl = shl i32 %2, 8
  store i32 %shl, i32* %x, align 4
  %3 = load i8*, i8** %buf.addr, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %3, i32 1
  %4 = load i8, i8* %arrayidx1, align 1
  %conv2 = zext i8 %4 to i32
  %5 = load i32, i32* %x, align 4
  %or = or i32 %5, %conv2
  store i32 %or, i32* %x, align 4
  %6 = load i32, i32* %x, align 4
  %shl3 = shl i32 %6, 8
  store i32 %shl3, i32* %x, align 4
  %7 = load i8*, i8** %buf.addr, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %7, i32 2
  %8 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %8 to i32
  %9 = load i32, i32* %x, align 4
  %or6 = or i32 %9, %conv5
  store i32 %or6, i32* %x, align 4
  %10 = load i32, i32* %x, align 4
  %shl7 = shl i32 %10, 8
  store i32 %shl7, i32* %x, align 4
  %11 = load i8*, i8** %buf.addr, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %11, i32 3
  %12 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %12 to i32
  %13 = load i32, i32* %x, align 4
  %or10 = or i32 %13, %conv9
  store i32 %or10, i32* %x, align 4
  %14 = load i32, i32* %x, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @InitVbrTag(%struct.lame_global_struct* %gfp) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %kbps_header = alloca i32, align 4
  %total_frame_size = alloca i32, align 4
  %header_size = alloca i32, align 4
  %buffer = alloca [2880 x i8], align 16
  %i = alloca i32, align 4
  %n = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %3 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %3, i32 0, i32 0
  %4 = load i32, i32* %version, align 4
  %cmp = icmp eq i32 1, %4
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 128, i32* %kbps_header, align 4
  br label %if.end5

if.else:                                          ; preds = %entry
  %5 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %5, i32 0, i32 12
  %6 = load i32, i32* %samplerate_out, align 4
  %cmp2 = icmp slt i32 %6, 16000
  br i1 %cmp2, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  store i32 32, i32* %kbps_header, align 4
  br label %if.end

if.else4:                                         ; preds = %if.else
  store i32 64, i32* %kbps_header, align 4
  br label %if.end

if.end:                                           ; preds = %if.else4, %if.then3
  br label %if.end5

if.end5:                                          ; preds = %if.end, %if.then
  %7 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %7, i32 0, i32 22
  %8 = load i32, i32* %vbr, align 4
  %cmp6 = icmp eq i32 %8, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end5
  %9 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %9, i32 0, i32 26
  %10 = load i32, i32* %avg_bitrate, align 4
  store i32 %10, i32* %kbps_header, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end5
  %11 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version9 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %11, i32 0, i32 0
  %12 = load i32, i32* %version9, align 4
  %add = add nsw i32 %12, 1
  %mul = mul nsw i32 %add, 72000
  %13 = load i32, i32* %kbps_header, align 4
  %mul10 = mul nsw i32 %mul, %13
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out11 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 12
  %15 = load i32, i32* %samplerate_out11, align 4
  %div = sdiv i32 %mul10, %15
  store i32 %div, i32* %total_frame_size, align 4
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 2
  %17 = load i32, i32* %sideinfo_len, align 4
  %add12 = add nsw i32 %17, 156
  store i32 %add12, i32* %header_size, align 4
  %18 = load i32, i32* %total_frame_size, align 4
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %19, i32 0, i32 20
  %TotalFrameSize = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table, i32 0, i32 8
  store i32 %18, i32* %TotalFrameSize, align 4
  %20 = load i32, i32* %total_frame_size, align 4
  %21 = load i32, i32* %header_size, align 4
  %cmp13 = icmp slt i32 %20, %21
  br i1 %cmp13, label %if.then15, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end8
  %22 = load i32, i32* %total_frame_size, align 4
  %cmp14 = icmp sgt i32 %22, 2880
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %lor.lhs.false, %if.end8
  %23 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %23, i32 0, i32 5
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg16, i32 0, i32 35
  store i32 0, i32* %write_lame_tag, align 4
  store i32 0, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %lor.lhs.false
  %24 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table18 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %24, i32 0, i32 20
  %nVbrNumFrames = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table18, i32 0, i32 6
  store i32 0, i32* %nVbrNumFrames, align 4
  %25 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table19 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %25, i32 0, i32 20
  %nBytesWritten = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table19, i32 0, i32 7
  store i32 0, i32* %nBytesWritten, align 4
  %26 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table20 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %26, i32 0, i32 20
  %sum = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table20, i32 0, i32 0
  store i32 0, i32* %sum, align 4
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table21 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 20
  %seen = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table21, i32 0, i32 1
  store i32 0, i32* %seen, align 4
  %28 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %28, i32 0, i32 20
  %want = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table22, i32 0, i32 2
  store i32 1, i32* %want, align 4
  %29 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table23 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %29, i32 0, i32 20
  %pos = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table23, i32 0, i32 3
  store i32 0, i32* %pos, align 4
  %30 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table24 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %30, i32 0, i32 20
  %bag = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table24, i32 0, i32 5
  %31 = load i32*, i32** %bag, align 4
  %cmp25 = icmp eq i32* %31, null
  br i1 %cmp25, label %if.then26, label %if.end40

if.then26:                                        ; preds = %if.end17
  %call = call i8* @calloc(i32 400, i32 4)
  %32 = bitcast i8* %call to i32*
  %33 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table27 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %33, i32 0, i32 20
  %bag28 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table27, i32 0, i32 5
  store i32* %32, i32** %bag28, align 4
  %34 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table29 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %34, i32 0, i32 20
  %bag30 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table29, i32 0, i32 5
  %35 = load i32*, i32** %bag30, align 4
  %cmp31 = icmp ne i32* %35, null
  br i1 %cmp31, label %if.then32, label %if.else34

if.then32:                                        ; preds = %if.then26
  %36 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table33 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %36, i32 0, i32 20
  %size = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table33, i32 0, i32 4
  store i32 400, i32* %size, align 4
  br label %if.end39

if.else34:                                        ; preds = %if.then26
  %37 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table35 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %37, i32 0, i32 20
  %size36 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table35, i32 0, i32 4
  store i32 0, i32* %size36, align 4
  %38 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  call void (%struct.lame_internal_flags*, i8*, ...) @lame_errorf(%struct.lame_internal_flags* %38, i8* getelementptr inbounds ([40 x i8], [40 x i8]* @.str, i32 0, i32 0))
  %39 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg37 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %39, i32 0, i32 5
  %write_lame_tag38 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %cfg37, i32 0, i32 35
  store i32 0, i32* %write_lame_tag38, align 4
  store i32 -1, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %if.then32
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end17
  %arraydecay = getelementptr inbounds [2880 x i8], [2880 x i8]* %buffer, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 2880, i1 false)
  %40 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %arraydecay41 = getelementptr inbounds [2880 x i8], [2880 x i8]* %buffer, i32 0, i32 0
  call void @setLameTagFrameHeader(%struct.lame_internal_flags* %40, i8* %arraydecay41)
  %41 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table42 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %41, i32 0, i32 20
  %TotalFrameSize43 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table42, i32 0, i32 8
  %42 = load i32, i32* %TotalFrameSize43, align 4
  store i32 %42, i32* %n, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end40
  %43 = load i32, i32* %i, align 4
  %44 = load i32, i32* %n, align 4
  %cmp44 = icmp ult i32 %43, %44
  br i1 %cmp44, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %45 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %46 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2880 x i8], [2880 x i8]* %buffer, i32 0, i32 %46
  %47 = load i8, i8* %arrayidx, align 1
  call void @add_dummy_byte(%struct.lame_internal_flags* %45, i8 zeroext %47, i32 1)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %48 = load i32, i32* %i, align 4
  %inc = add i32 %48, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.else34, %if.then15
  %49 = load i32, i32* %retval, align 4
  ret i32 %49
}

declare i8* @calloc(i32, i32) #1

declare void @lame_errorf(%struct.lame_internal_flags*, i8*, ...) #1

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @setLameTagFrameHeader(%struct.lame_internal_flags* %gfc, i8* %buffer) #0 {
entry:
  %gfc.addr = alloca %struct.lame_internal_flags*, align 4
  %buffer.addr = alloca i8*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %eov = alloca %struct.EncResult_t*, align 4
  %abyte = alloca i8, align 1
  %bbyte = alloca i8, align 1
  %bitrate = alloca i32, align 4
  store %struct.lame_internal_flags* %gfc, %struct.lame_internal_flags** %gfc.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  %0 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %0, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc.addr, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %1, i32 0, i32 12
  store %struct.EncResult_t* %ov_enc, %struct.EncResult_t** %eov, align 4
  %2 = load i8*, i8** %buffer.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 0
  %3 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %3 to i32
  %shl = shl i32 %conv, 8
  %or = or i32 %shl, 255
  %conv2 = trunc i32 %or to i8
  %4 = load i8*, i8** %buffer.addr, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %4, i32 0
  store i8 %conv2, i8* %arrayidx3, align 1
  %5 = load i8*, i8** %buffer.addr, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %5, i32 1
  %6 = load i8, i8* %arrayidx4, align 1
  %conv5 = zext i8 %6 to i32
  %shl6 = shl i32 %conv5, 3
  %or7 = or i32 %shl6, 7
  %conv8 = trunc i32 %or7 to i8
  %7 = load i8*, i8** %buffer.addr, align 4
  %arrayidx9 = getelementptr inbounds i8, i8* %7, i32 1
  store i8 %conv8, i8* %arrayidx9, align 1
  %8 = load i8*, i8** %buffer.addr, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %8, i32 1
  %9 = load i8, i8* %arrayidx10, align 1
  %conv11 = zext i8 %9 to i32
  %shl12 = shl i32 %conv11, 1
  %10 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %10, i32 0, i32 12
  %11 = load i32, i32* %samplerate_out, align 4
  %cmp = icmp slt i32 %11, 16000
  %12 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 0, i32 1
  %and = and i32 %cond, 1
  %or14 = or i32 %shl12, %and
  %conv15 = trunc i32 %or14 to i8
  %13 = load i8*, i8** %buffer.addr, align 4
  %arrayidx16 = getelementptr inbounds i8, i8* %13, i32 1
  store i8 %conv15, i8* %arrayidx16, align 1
  %14 = load i8*, i8** %buffer.addr, align 4
  %arrayidx17 = getelementptr inbounds i8, i8* %14, i32 1
  %15 = load i8, i8* %arrayidx17, align 1
  %conv18 = zext i8 %15 to i32
  %shl19 = shl i32 %conv18, 1
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 0
  %17 = load i32, i32* %version, align 4
  %and20 = and i32 %17, 1
  %or21 = or i32 %shl19, %and20
  %conv22 = trunc i32 %or21 to i8
  %18 = load i8*, i8** %buffer.addr, align 4
  %arrayidx23 = getelementptr inbounds i8, i8* %18, i32 1
  store i8 %conv22, i8* %arrayidx23, align 1
  %19 = load i8*, i8** %buffer.addr, align 4
  %arrayidx24 = getelementptr inbounds i8, i8* %19, i32 1
  %20 = load i8, i8* %arrayidx24, align 1
  %conv25 = zext i8 %20 to i32
  %shl26 = shl i32 %conv25, 2
  %or27 = or i32 %shl26, 1
  %conv28 = trunc i32 %or27 to i8
  %21 = load i8*, i8** %buffer.addr, align 4
  %arrayidx29 = getelementptr inbounds i8, i8* %21, i32 1
  store i8 %conv28, i8* %arrayidx29, align 1
  %22 = load i8*, i8** %buffer.addr, align 4
  %arrayidx30 = getelementptr inbounds i8, i8* %22, i32 1
  %23 = load i8, i8* %arrayidx30, align 1
  %conv31 = zext i8 %23 to i32
  %shl32 = shl i32 %conv31, 1
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 36
  %25 = load i32, i32* %error_protection, align 4
  %tobool = icmp ne i32 %25, 0
  %lnot = xor i1 %tobool, true
  %26 = zext i1 %lnot to i64
  %cond33 = select i1 %lnot, i32 1, i32 0
  %and34 = and i32 %cond33, 1
  %or35 = or i32 %shl32, %and34
  %conv36 = trunc i32 %or35 to i8
  %27 = load i8*, i8** %buffer.addr, align 4
  %arrayidx37 = getelementptr inbounds i8, i8* %27, i32 1
  store i8 %conv36, i8* %arrayidx37, align 1
  %28 = load i8*, i8** %buffer.addr, align 4
  %arrayidx38 = getelementptr inbounds i8, i8* %28, i32 2
  %29 = load i8, i8* %arrayidx38, align 1
  %conv39 = zext i8 %29 to i32
  %shl40 = shl i32 %conv39, 4
  %30 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %bitrate_index = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %30, i32 0, i32 2
  %31 = load i32, i32* %bitrate_index, align 4
  %and41 = and i32 %31, 15
  %or42 = or i32 %shl40, %and41
  %conv43 = trunc i32 %or42 to i8
  %32 = load i8*, i8** %buffer.addr, align 4
  %arrayidx44 = getelementptr inbounds i8, i8* %32, i32 2
  store i8 %conv43, i8* %arrayidx44, align 1
  %33 = load i8*, i8** %buffer.addr, align 4
  %arrayidx45 = getelementptr inbounds i8, i8* %33, i32 2
  %34 = load i8, i8* %arrayidx45, align 1
  %conv46 = zext i8 %34 to i32
  %shl47 = shl i32 %conv46, 2
  %35 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %35, i32 0, i32 1
  %36 = load i32, i32* %samplerate_index, align 4
  %and48 = and i32 %36, 3
  %or49 = or i32 %shl47, %and48
  %conv50 = trunc i32 %or49 to i8
  %37 = load i8*, i8** %buffer.addr, align 4
  %arrayidx51 = getelementptr inbounds i8, i8* %37, i32 2
  store i8 %conv50, i8* %arrayidx51, align 1
  %38 = load i8*, i8** %buffer.addr, align 4
  %arrayidx52 = getelementptr inbounds i8, i8* %38, i32 2
  %39 = load i8, i8* %arrayidx52, align 1
  %conv53 = zext i8 %39 to i32
  %shl54 = shl i32 %conv53, 1
  %conv55 = trunc i32 %shl54 to i8
  %40 = load i8*, i8** %buffer.addr, align 4
  %arrayidx56 = getelementptr inbounds i8, i8* %40, i32 2
  store i8 %conv55, i8* %arrayidx56, align 1
  %41 = load i8*, i8** %buffer.addr, align 4
  %arrayidx57 = getelementptr inbounds i8, i8* %41, i32 2
  %42 = load i8, i8* %arrayidx57, align 1
  %conv58 = zext i8 %42 to i32
  %shl59 = shl i32 %conv58, 1
  %43 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %extension = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %43, i32 0, i32 39
  %44 = load i32, i32* %extension, align 4
  %and60 = and i32 %44, 1
  %or61 = or i32 %shl59, %and60
  %conv62 = trunc i32 %or61 to i8
  %45 = load i8*, i8** %buffer.addr, align 4
  %arrayidx63 = getelementptr inbounds i8, i8* %45, i32 2
  store i8 %conv62, i8* %arrayidx63, align 1
  %46 = load i8*, i8** %buffer.addr, align 4
  %arrayidx64 = getelementptr inbounds i8, i8* %46, i32 3
  %47 = load i8, i8* %arrayidx64, align 1
  %conv65 = zext i8 %47 to i32
  %shl66 = shl i32 %conv65, 2
  %48 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %48, i32 0, i32 41
  %49 = load i32, i32* %mode, align 4
  %and67 = and i32 %49, 3
  %or68 = or i32 %shl66, %and67
  %conv69 = trunc i32 %or68 to i8
  %50 = load i8*, i8** %buffer.addr, align 4
  %arrayidx70 = getelementptr inbounds i8, i8* %50, i32 3
  store i8 %conv69, i8* %arrayidx70, align 1
  %51 = load i8*, i8** %buffer.addr, align 4
  %arrayidx71 = getelementptr inbounds i8, i8* %51, i32 3
  %52 = load i8, i8* %arrayidx71, align 1
  %conv72 = zext i8 %52 to i32
  %shl73 = shl i32 %conv72, 2
  %53 = load %struct.EncResult_t*, %struct.EncResult_t** %eov, align 4
  %mode_ext = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %53, i32 0, i32 5
  %54 = load i32, i32* %mode_ext, align 4
  %and74 = and i32 %54, 3
  %or75 = or i32 %shl73, %and74
  %conv76 = trunc i32 %or75 to i8
  %55 = load i8*, i8** %buffer.addr, align 4
  %arrayidx77 = getelementptr inbounds i8, i8* %55, i32 3
  store i8 %conv76, i8* %arrayidx77, align 1
  %56 = load i8*, i8** %buffer.addr, align 4
  %arrayidx78 = getelementptr inbounds i8, i8* %56, i32 3
  %57 = load i8, i8* %arrayidx78, align 1
  %conv79 = zext i8 %57 to i32
  %shl80 = shl i32 %conv79, 1
  %58 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %copyright = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %58, i32 0, i32 37
  %59 = load i32, i32* %copyright, align 4
  %and81 = and i32 %59, 1
  %or82 = or i32 %shl80, %and81
  %conv83 = trunc i32 %or82 to i8
  %60 = load i8*, i8** %buffer.addr, align 4
  %arrayidx84 = getelementptr inbounds i8, i8* %60, i32 3
  store i8 %conv83, i8* %arrayidx84, align 1
  %61 = load i8*, i8** %buffer.addr, align 4
  %arrayidx85 = getelementptr inbounds i8, i8* %61, i32 3
  %62 = load i8, i8* %arrayidx85, align 1
  %conv86 = zext i8 %62 to i32
  %shl87 = shl i32 %conv86, 1
  %63 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %original = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %63, i32 0, i32 38
  %64 = load i32, i32* %original, align 4
  %and88 = and i32 %64, 1
  %or89 = or i32 %shl87, %and88
  %conv90 = trunc i32 %or89 to i8
  %65 = load i8*, i8** %buffer.addr, align 4
  %arrayidx91 = getelementptr inbounds i8, i8* %65, i32 3
  store i8 %conv90, i8* %arrayidx91, align 1
  %66 = load i8*, i8** %buffer.addr, align 4
  %arrayidx92 = getelementptr inbounds i8, i8* %66, i32 3
  %67 = load i8, i8* %arrayidx92, align 1
  %conv93 = zext i8 %67 to i32
  %shl94 = shl i32 %conv93, 2
  %68 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %emphasis = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %68, i32 0, i32 40
  %69 = load i32, i32* %emphasis, align 4
  %and95 = and i32 %69, 3
  %or96 = or i32 %shl94, %and95
  %conv97 = trunc i32 %or96 to i8
  %70 = load i8*, i8** %buffer.addr, align 4
  %arrayidx98 = getelementptr inbounds i8, i8* %70, i32 3
  store i8 %conv97, i8* %arrayidx98, align 1
  %71 = load i8*, i8** %buffer.addr, align 4
  %arrayidx99 = getelementptr inbounds i8, i8* %71, i32 0
  store i8 -1, i8* %arrayidx99, align 1
  %72 = load i8*, i8** %buffer.addr, align 4
  %arrayidx100 = getelementptr inbounds i8, i8* %72, i32 1
  %73 = load i8, i8* %arrayidx100, align 1
  %conv101 = zext i8 %73 to i32
  %and102 = and i32 %conv101, 241
  %conv103 = trunc i32 %and102 to i8
  store i8 %conv103, i8* %abyte, align 1
  %74 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version104 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %74, i32 0, i32 0
  %75 = load i32, i32* %version104, align 4
  %cmp105 = icmp eq i32 1, %75
  br i1 %cmp105, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store i32 128, i32* %bitrate, align 4
  br label %if.end112

if.else:                                          ; preds = %entry
  %76 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out107 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %76, i32 0, i32 12
  %77 = load i32, i32* %samplerate_out107, align 4
  %cmp108 = icmp slt i32 %77, 16000
  br i1 %cmp108, label %if.then110, label %if.else111

if.then110:                                       ; preds = %if.else
  store i32 32, i32* %bitrate, align 4
  br label %if.end

if.else111:                                       ; preds = %if.else
  store i32 64, i32* %bitrate, align 4
  br label %if.end

if.end:                                           ; preds = %if.else111, %if.then110
  br label %if.end112

if.end112:                                        ; preds = %if.end, %if.then
  %78 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %78, i32 0, i32 22
  %79 = load i32, i32* %vbr, align 4
  %cmp113 = icmp eq i32 %79, 0
  br i1 %cmp113, label %if.then115, label %if.end116

if.then115:                                       ; preds = %if.end112
  %80 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %80, i32 0, i32 26
  %81 = load i32, i32* %avg_bitrate, align 4
  store i32 %81, i32* %bitrate, align 4
  br label %if.end116

if.end116:                                        ; preds = %if.then115, %if.end112
  %82 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %82, i32 0, i32 34
  %83 = load i32, i32* %free_format, align 4
  %tobool117 = icmp ne i32 %83, 0
  br i1 %tobool117, label %if.then118, label %if.else119

if.then118:                                       ; preds = %if.end116
  store i8 0, i8* %bbyte, align 1
  br label %if.end123

if.else119:                                       ; preds = %if.end116
  %84 = load i32, i32* %bitrate, align 4
  %85 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version120 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %85, i32 0, i32 0
  %86 = load i32, i32* %version120, align 4
  %87 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_out121 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %87, i32 0, i32 12
  %88 = load i32, i32* %samplerate_out121, align 4
  %call = call i32 @BitrateIndex(i32 %84, i32 %86, i32 %88)
  %mul = mul nsw i32 16, %call
  %conv122 = trunc i32 %mul to i8
  store i8 %conv122, i8* %bbyte, align 1
  br label %if.end123

if.end123:                                        ; preds = %if.else119, %if.then118
  %89 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version124 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %89, i32 0, i32 0
  %90 = load i32, i32* %version124, align 4
  %cmp125 = icmp eq i32 %90, 1
  br i1 %cmp125, label %if.then127, label %if.else141

if.then127:                                       ; preds = %if.end123
  %91 = load i8, i8* %abyte, align 1
  %conv128 = sext i8 %91 to i32
  %or129 = or i32 %conv128, 10
  %conv130 = trunc i32 %or129 to i8
  %92 = load i8*, i8** %buffer.addr, align 4
  %arrayidx131 = getelementptr inbounds i8, i8* %92, i32 1
  store i8 %conv130, i8* %arrayidx131, align 1
  %93 = load i8*, i8** %buffer.addr, align 4
  %arrayidx132 = getelementptr inbounds i8, i8* %93, i32 2
  %94 = load i8, i8* %arrayidx132, align 1
  %conv133 = zext i8 %94 to i32
  %and134 = and i32 %conv133, 13
  %conv135 = trunc i32 %and134 to i8
  store i8 %conv135, i8* %abyte, align 1
  %95 = load i8, i8* %bbyte, align 1
  %conv136 = sext i8 %95 to i32
  %96 = load i8, i8* %abyte, align 1
  %conv137 = sext i8 %96 to i32
  %or138 = or i32 %conv136, %conv137
  %conv139 = trunc i32 %or138 to i8
  %97 = load i8*, i8** %buffer.addr, align 4
  %arrayidx140 = getelementptr inbounds i8, i8* %97, i32 2
  store i8 %conv139, i8* %arrayidx140, align 1
  br label %if.end155

if.else141:                                       ; preds = %if.end123
  %98 = load i8, i8* %abyte, align 1
  %conv142 = sext i8 %98 to i32
  %or143 = or i32 %conv142, 2
  %conv144 = trunc i32 %or143 to i8
  %99 = load i8*, i8** %buffer.addr, align 4
  %arrayidx145 = getelementptr inbounds i8, i8* %99, i32 1
  store i8 %conv144, i8* %arrayidx145, align 1
  %100 = load i8*, i8** %buffer.addr, align 4
  %arrayidx146 = getelementptr inbounds i8, i8* %100, i32 2
  %101 = load i8, i8* %arrayidx146, align 1
  %conv147 = zext i8 %101 to i32
  %and148 = and i32 %conv147, 13
  %conv149 = trunc i32 %and148 to i8
  store i8 %conv149, i8* %abyte, align 1
  %102 = load i8, i8* %bbyte, align 1
  %conv150 = sext i8 %102 to i32
  %103 = load i8, i8* %abyte, align 1
  %conv151 = sext i8 %103 to i32
  %or152 = or i32 %conv150, %conv151
  %conv153 = trunc i32 %or152 to i8
  %104 = load i8*, i8** %buffer.addr, align 4
  %arrayidx154 = getelementptr inbounds i8, i8* %104, i32 2
  store i8 %conv153, i8* %arrayidx154, align 1
  br label %if.end155

if.end155:                                        ; preds = %if.else141, %if.then127
  ret void
}

declare void @add_dummy_byte(%struct.lame_internal_flags*, i8 zeroext, i32) #1

; Function Attrs: noinline nounwind optnone
define hidden void @UpdateMusicCRC(i16* %crc, i8* %buffer, i32 %size) #0 {
entry:
  %crc.addr = alloca i16*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store i16* %crc, i16** %crc.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %size.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i8*, i8** %buffer.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %2, i32 %3
  %4 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %4 to i16
  %5 = load i16*, i16** %crc.addr, align 4
  %6 = load i16, i16* %5, align 2
  %call = call zeroext i16 @CRC_update_lookup(i16 zeroext %conv, i16 zeroext %6)
  %7 = load i16*, i16** %crc.addr, align 4
  store i16 %call, i16* %7, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal zeroext i16 @CRC_update_lookup(i16 zeroext %value, i16 zeroext %crc) #0 {
entry:
  %value.addr = alloca i16, align 2
  %crc.addr = alloca i16, align 2
  %tmp = alloca i16, align 2
  store i16 %value, i16* %value.addr, align 2
  store i16 %crc, i16* %crc.addr, align 2
  %0 = load i16, i16* %crc.addr, align 2
  %conv = zext i16 %0 to i32
  %1 = load i16, i16* %value.addr, align 2
  %conv1 = zext i16 %1 to i32
  %xor = xor i32 %conv, %conv1
  %conv2 = trunc i32 %xor to i16
  store i16 %conv2, i16* %tmp, align 2
  %2 = load i16, i16* %crc.addr, align 2
  %conv3 = zext i16 %2 to i32
  %shr = ashr i32 %conv3, 8
  %3 = load i16, i16* %tmp, align 2
  %conv4 = zext i16 %3 to i32
  %and = and i32 %conv4, 255
  %arrayidx = getelementptr inbounds [256 x i32], [256 x i32]* @crc16_lookup, i32 0, i32 %and
  %4 = load i32, i32* %arrayidx, align 4
  %xor5 = xor i32 %shr, %4
  %conv6 = trunc i32 %xor5 to i16
  store i16 %conv6, i16* %crc.addr, align 2
  %5 = load i16, i16* %crc.addr, align 2
  ret i16 %5
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @lame_get_lametag_frame(%struct.lame_global_struct* %gfp, i8* %buffer, i32 %size) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %stream_size = alloca i32, align 4
  %nStreamIndex = alloca i32, align 4
  %btToc = alloca [100 x i8], align 16
  %i = alloca i32, align 4
  %crc = alloca i16, align 2
  %i71 = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %cmp = icmp eq %struct.lame_global_struct* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %1, i32 0, i32 70
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %2, %struct.lame_internal_flags** %gfc, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cmp1 = icmp eq %struct.lame_internal_flags* %3, null
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

if.end3:                                          ; preds = %if.end
  %4 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %call = call i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags* %4)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end5, label %if.then4

if.then4:                                         ; preds = %if.end3
  store i32 0, i32* %retval, align 4
  br label %return

if.end5:                                          ; preds = %if.end3
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg6 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg6, %struct.SessionConfig_t** %cfg, align 4
  %6 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %write_lame_tag = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %6, i32 0, i32 35
  %7 = load i32, i32* %write_lame_tag, align 4
  %cmp7 = icmp eq i32 %7, 0
  br i1 %cmp7, label %if.then8, label %if.end9

if.then8:                                         ; preds = %if.end5
  store i32 0, i32* %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end5
  %8 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %8, i32 0, i32 20
  %pos = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table, i32 0, i32 3
  %9 = load i32, i32* %pos, align 4
  %cmp10 = icmp sle i32 %9, 0
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %if.end9
  store i32 0, i32* %retval, align 4
  br label %return

if.end12:                                         ; preds = %if.end9
  %10 = load i32, i32* %size.addr, align 4
  %11 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table13 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %11, i32 0, i32 20
  %TotalFrameSize = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table13, i32 0, i32 8
  %12 = load i32, i32* %TotalFrameSize, align 4
  %cmp14 = icmp ult i32 %10, %12
  br i1 %cmp14, label %if.then15, label %if.end18

if.then15:                                        ; preds = %if.end12
  %13 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table16 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %13, i32 0, i32 20
  %TotalFrameSize17 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table16, i32 0, i32 8
  %14 = load i32, i32* %TotalFrameSize17, align 4
  store i32 %14, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %if.end12
  %15 = load i8*, i8** %buffer.addr, align 4
  %cmp19 = icmp eq i8* %15, null
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end18
  store i32 0, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end18
  %16 = load i8*, i8** %buffer.addr, align 4
  %17 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table22 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %17, i32 0, i32 20
  %TotalFrameSize23 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table22, i32 0, i32 8
  %18 = load i32, i32* %TotalFrameSize23, align 4
  call void @llvm.memset.p0i8.i32(i8* align 1 %16, i8 0, i32 %18, i1 false)
  %19 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %20 = load i8*, i8** %buffer.addr, align 4
  call void @setLameTagFrameHeader(%struct.lame_internal_flags* %19, i8* %20)
  %arraydecay = getelementptr inbounds [100 x i8], [100 x i8]* %btToc, i32 0, i32 0
  call void @llvm.memset.p0i8.i32(i8* align 16 %arraydecay, i8 0, i32 100, i1 false)
  %21 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %free_format = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %21, i32 0, i32 34
  %22 = load i32, i32* %free_format, align 4
  %tobool24 = icmp ne i32 %22, 0
  br i1 %tobool24, label %if.then25, label %if.else

if.then25:                                        ; preds = %if.end21
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then25
  %23 = load i32, i32* %i, align 4
  %cmp26 = icmp slt i32 %23, 100
  br i1 %cmp26, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %24 = load i32, i32* %i, align 4
  %mul = mul nsw i32 255, %24
  %div = sdiv i32 %mul, 100
  %conv = trunc i32 %div to i8
  %25 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [100 x i8], [100 x i8]* %btToc, i32 0, i32 %25
  store i8 %conv, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end29

if.else:                                          ; preds = %if.end21
  %27 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table27 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %27, i32 0, i32 20
  %arraydecay28 = getelementptr inbounds [100 x i8], [100 x i8]* %btToc, i32 0, i32 0
  call void @Xing_seek_table(%struct.VBR_seek_info_t* %VBR_seek_table27, i8* %arraydecay28)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %for.end
  %28 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %sideinfo_len = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %28, i32 0, i32 2
  %29 = load i32, i32* %sideinfo_len, align 4
  store i32 %29, i32* %nStreamIndex, align 4
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 36
  %31 = load i32, i32* %error_protection, align 4
  %tobool30 = icmp ne i32 %31, 0
  br i1 %tobool30, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.end29
  %32 = load i32, i32* %nStreamIndex, align 4
  %sub = sub i32 %32, 2
  store i32 %sub, i32* %nStreamIndex, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.end29
  %33 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %33, i32 0, i32 22
  %34 = load i32, i32* %vbr, align 4
  %cmp33 = icmp eq i32 %34, 0
  br i1 %cmp33, label %if.then35, label %if.else44

if.then35:                                        ; preds = %if.end32
  %35 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 0), align 1
  %36 = load i8*, i8** %buffer.addr, align 4
  %37 = load i32, i32* %nStreamIndex, align 4
  %inc36 = add i32 %37, 1
  store i32 %inc36, i32* %nStreamIndex, align 4
  %arrayidx37 = getelementptr inbounds i8, i8* %36, i32 %37
  store i8 %35, i8* %arrayidx37, align 1
  %38 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 1), align 1
  %39 = load i8*, i8** %buffer.addr, align 4
  %40 = load i32, i32* %nStreamIndex, align 4
  %inc38 = add i32 %40, 1
  store i32 %inc38, i32* %nStreamIndex, align 4
  %arrayidx39 = getelementptr inbounds i8, i8* %39, i32 %40
  store i8 %38, i8* %arrayidx39, align 1
  %41 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 2), align 1
  %42 = load i8*, i8** %buffer.addr, align 4
  %43 = load i32, i32* %nStreamIndex, align 4
  %inc40 = add i32 %43, 1
  store i32 %inc40, i32* %nStreamIndex, align 4
  %arrayidx41 = getelementptr inbounds i8, i8* %42, i32 %43
  store i8 %41, i8* %arrayidx41, align 1
  %44 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag1, i32 0, i32 3), align 1
  %45 = load i8*, i8** %buffer.addr, align 4
  %46 = load i32, i32* %nStreamIndex, align 4
  %inc42 = add i32 %46, 1
  store i32 %inc42, i32* %nStreamIndex, align 4
  %arrayidx43 = getelementptr inbounds i8, i8* %45, i32 %46
  store i8 %44, i8* %arrayidx43, align 1
  br label %if.end53

if.else44:                                        ; preds = %if.end32
  %47 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 0), align 1
  %48 = load i8*, i8** %buffer.addr, align 4
  %49 = load i32, i32* %nStreamIndex, align 4
  %inc45 = add i32 %49, 1
  store i32 %inc45, i32* %nStreamIndex, align 4
  %arrayidx46 = getelementptr inbounds i8, i8* %48, i32 %49
  store i8 %47, i8* %arrayidx46, align 1
  %50 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 1), align 1
  %51 = load i8*, i8** %buffer.addr, align 4
  %52 = load i32, i32* %nStreamIndex, align 4
  %inc47 = add i32 %52, 1
  store i32 %inc47, i32* %nStreamIndex, align 4
  %arrayidx48 = getelementptr inbounds i8, i8* %51, i32 %52
  store i8 %50, i8* %arrayidx48, align 1
  %53 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 2), align 1
  %54 = load i8*, i8** %buffer.addr, align 4
  %55 = load i32, i32* %nStreamIndex, align 4
  %inc49 = add i32 %55, 1
  store i32 %inc49, i32* %nStreamIndex, align 4
  %arrayidx50 = getelementptr inbounds i8, i8* %54, i32 %55
  store i8 %53, i8* %arrayidx50, align 1
  %56 = load i8, i8* getelementptr inbounds ([5 x i8], [5 x i8]* @VBRTag0, i32 0, i32 3), align 1
  %57 = load i8*, i8** %buffer.addr, align 4
  %58 = load i32, i32* %nStreamIndex, align 4
  %inc51 = add i32 %58, 1
  store i32 %inc51, i32* %nStreamIndex, align 4
  %arrayidx52 = getelementptr inbounds i8, i8* %57, i32 %58
  store i8 %56, i8* %arrayidx52, align 1
  br label %if.end53

if.end53:                                         ; preds = %if.else44, %if.then35
  %59 = load i8*, i8** %buffer.addr, align 4
  %60 = load i32, i32* %nStreamIndex, align 4
  %arrayidx54 = getelementptr inbounds i8, i8* %59, i32 %60
  call void @CreateI4(i8* %arrayidx54, i32 15)
  %61 = load i32, i32* %nStreamIndex, align 4
  %add = add i32 %61, 4
  store i32 %add, i32* %nStreamIndex, align 4
  %62 = load i8*, i8** %buffer.addr, align 4
  %63 = load i32, i32* %nStreamIndex, align 4
  %arrayidx55 = getelementptr inbounds i8, i8* %62, i32 %63
  %64 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table56 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %64, i32 0, i32 20
  %nVbrNumFrames = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table56, i32 0, i32 6
  %65 = load i32, i32* %nVbrNumFrames, align 4
  call void @CreateI4(i8* %arrayidx55, i32 %65)
  %66 = load i32, i32* %nStreamIndex, align 4
  %add57 = add i32 %66, 4
  store i32 %add57, i32* %nStreamIndex, align 4
  %67 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table58 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %67, i32 0, i32 20
  %nBytesWritten = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table58, i32 0, i32 7
  %68 = load i32, i32* %nBytesWritten, align 4
  %69 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table59 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %69, i32 0, i32 20
  %TotalFrameSize60 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table59, i32 0, i32 8
  %70 = load i32, i32* %TotalFrameSize60, align 4
  %add61 = add i32 %68, %70
  store i32 %add61, i32* %stream_size, align 4
  %71 = load i8*, i8** %buffer.addr, align 4
  %72 = load i32, i32* %nStreamIndex, align 4
  %arrayidx62 = getelementptr inbounds i8, i8* %71, i32 %72
  %73 = load i32, i32* %stream_size, align 4
  call void @CreateI4(i8* %arrayidx62, i32 %73)
  %74 = load i32, i32* %nStreamIndex, align 4
  %add63 = add i32 %74, 4
  store i32 %add63, i32* %nStreamIndex, align 4
  %75 = load i8*, i8** %buffer.addr, align 4
  %76 = load i32, i32* %nStreamIndex, align 4
  %arrayidx64 = getelementptr inbounds i8, i8* %75, i32 %76
  %arraydecay65 = getelementptr inbounds [100 x i8], [100 x i8]* %btToc, i32 0, i32 0
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %arrayidx64, i8* align 16 %arraydecay65, i32 100, i1 false)
  %77 = load i32, i32* %nStreamIndex, align 4
  %add66 = add i32 %77, 100
  store i32 %add66, i32* %nStreamIndex, align 4
  %78 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %error_protection67 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %78, i32 0, i32 36
  %79 = load i32, i32* %error_protection67, align 4
  %tobool68 = icmp ne i32 %79, 0
  br i1 %tobool68, label %if.then69, label %if.end70

if.then69:                                        ; preds = %if.end53
  %80 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %81 = load i8*, i8** %buffer.addr, align 4
  call void @CRC_writeheader(%struct.lame_internal_flags* %80, i8* %81)
  br label %if.end70

if.end70:                                         ; preds = %if.then69, %if.end53
  store i16 0, i16* %crc, align 2
  store i32 0, i32* %i71, align 4
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc79, %if.end70
  %82 = load i32, i32* %i71, align 4
  %83 = load i32, i32* %nStreamIndex, align 4
  %cmp73 = icmp ult i32 %82, %83
  br i1 %cmp73, label %for.body75, label %for.end81

for.body75:                                       ; preds = %for.cond72
  %84 = load i8*, i8** %buffer.addr, align 4
  %85 = load i32, i32* %i71, align 4
  %arrayidx76 = getelementptr inbounds i8, i8* %84, i32 %85
  %86 = load i8, i8* %arrayidx76, align 1
  %conv77 = zext i8 %86 to i16
  %87 = load i16, i16* %crc, align 2
  %call78 = call zeroext i16 @CRC_update_lookup(i16 zeroext %conv77, i16 zeroext %87)
  store i16 %call78, i16* %crc, align 2
  br label %for.inc79

for.inc79:                                        ; preds = %for.body75
  %88 = load i32, i32* %i71, align 4
  %inc80 = add i32 %88, 1
  store i32 %inc80, i32* %i71, align 4
  br label %for.cond72

for.end81:                                        ; preds = %for.cond72
  %89 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %90 = load i32, i32* %stream_size, align 4
  %91 = load i8*, i8** %buffer.addr, align 4
  %92 = load i32, i32* %nStreamIndex, align 4
  %add.ptr = getelementptr inbounds i8, i8* %91, i32 %92
  %93 = load i16, i16* %crc, align 2
  %call82 = call i32 @PutLameVBR(%struct.lame_global_struct* %89, i32 %90, i8* %add.ptr, i16 zeroext %93)
  %94 = load i32, i32* %nStreamIndex, align 4
  %add83 = add i32 %94, %call82
  store i32 %add83, i32* %nStreamIndex, align 4
  %95 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table84 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %95, i32 0, i32 20
  %TotalFrameSize85 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table84, i32 0, i32 8
  %96 = load i32, i32* %TotalFrameSize85, align 4
  store i32 %96, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end81, %if.then20, %if.then15, %if.then11, %if.then8, %if.then4, %if.then2, %if.then
  %97 = load i32, i32* %retval, align 4
  ret i32 %97
}

declare i32 @is_lame_internal_flags_valid(%struct.lame_internal_flags*) #1

; Function Attrs: noinline nounwind optnone
define internal void @Xing_seek_table(%struct.VBR_seek_info_t* %v, i8* %t) #0 {
entry:
  %v.addr = alloca %struct.VBR_seek_info_t*, align 4
  %t.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  %indx = alloca i32, align 4
  %seek_point = alloca i32, align 4
  %j = alloca float, align 4
  %act = alloca float, align 4
  %sum = alloca float, align 4
  store %struct.VBR_seek_info_t* %v, %struct.VBR_seek_info_t** %v.addr, align 4
  store i8* %t, i8** %t.addr, align 4
  %0 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %0, i32 0, i32 3
  %1 = load i32, i32* %pos, align 4
  %cmp = icmp sle i32 %1, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end

if.end:                                           ; preds = %entry
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %2 = load i32, i32* %i, align 4
  %cmp1 = icmp slt i32 %2, 100
  br i1 %cmp1, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32, i32* %i, align 4
  %conv = sitofp i32 %3 to float
  %div = fdiv float %conv, 1.000000e+02
  store float %div, float* %j, align 4
  %4 = load float, float* %j, align 4
  %5 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos2 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %5, i32 0, i32 3
  %6 = load i32, i32* %pos2, align 4
  %conv3 = sitofp i32 %6 to float
  %mul = fmul float %4, %conv3
  %conv4 = fpext float %mul to double
  %7 = call double @llvm.floor.f64(double %conv4)
  %conv5 = fptosi double %7 to i32
  store i32 %conv5, i32* %indx, align 4
  %8 = load i32, i32* %indx, align 4
  %9 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos6 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %9, i32 0, i32 3
  %10 = load i32, i32* %pos6, align 4
  %sub = sub nsw i32 %10, 1
  %cmp7 = icmp sgt i32 %8, %sub
  br i1 %cmp7, label %if.then9, label %if.end12

if.then9:                                         ; preds = %for.body
  %11 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %pos10 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %11, i32 0, i32 3
  %12 = load i32, i32* %pos10, align 4
  %sub11 = sub nsw i32 %12, 1
  store i32 %sub11, i32* %indx, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then9, %for.body
  %13 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %bag = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %13, i32 0, i32 5
  %14 = load i32*, i32** %bag, align 4
  %15 = load i32, i32* %indx, align 4
  %arrayidx = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = load i32, i32* %arrayidx, align 4
  %conv13 = sitofp i32 %16 to float
  store float %conv13, float* %act, align 4
  %17 = load %struct.VBR_seek_info_t*, %struct.VBR_seek_info_t** %v.addr, align 4
  %sum14 = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %17, i32 0, i32 0
  %18 = load i32, i32* %sum14, align 4
  %conv15 = sitofp i32 %18 to float
  store float %conv15, float* %sum, align 4
  %19 = load float, float* %act, align 4
  %conv16 = fpext float %19 to double
  %mul17 = fmul double 2.560000e+02, %conv16
  %20 = load float, float* %sum, align 4
  %conv18 = fpext float %20 to double
  %div19 = fdiv double %mul17, %conv18
  %conv20 = fptosi double %div19 to i32
  store i32 %conv20, i32* %seek_point, align 4
  %21 = load i32, i32* %seek_point, align 4
  %cmp21 = icmp sgt i32 %21, 255
  br i1 %cmp21, label %if.then23, label %if.end24

if.then23:                                        ; preds = %if.end12
  store i32 255, i32* %seek_point, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %if.end12
  %22 = load i32, i32* %seek_point, align 4
  %conv25 = trunc i32 %22 to i8
  %23 = load i8*, i8** %t.addr, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds i8, i8* %23, i32 %24
  store i8 %conv25, i8* %arrayidx26, align 1
  br label %for.inc

for.inc:                                          ; preds = %if.end24
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @CreateI4(i8* %buf, i32 %nValue) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %nValue.addr = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %nValue, i32* %nValue.addr, align 4
  %0 = load i32, i32* %nValue.addr, align 4
  %shr = lshr i32 %0, 24
  %and = and i32 %shr, 255
  %conv = trunc i32 %and to i8
  %1 = load i8*, i8** %buf.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i32, i32* %nValue.addr, align 4
  %shr1 = lshr i32 %2, 16
  %and2 = and i32 %shr1, 255
  %conv3 = trunc i32 %and2 to i8
  %3 = load i8*, i8** %buf.addr, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %3, i32 1
  store i8 %conv3, i8* %arrayidx4, align 1
  %4 = load i32, i32* %nValue.addr, align 4
  %shr5 = lshr i32 %4, 8
  %and6 = and i32 %shr5, 255
  %conv7 = trunc i32 %and6 to i8
  %5 = load i8*, i8** %buf.addr, align 4
  %arrayidx8 = getelementptr inbounds i8, i8* %5, i32 2
  store i8 %conv7, i8* %arrayidx8, align 1
  %6 = load i32, i32* %nValue.addr, align 4
  %and9 = and i32 %6, 255
  %conv10 = trunc i32 %and9 to i8
  %7 = load i8*, i8** %buf.addr, align 4
  %arrayidx11 = getelementptr inbounds i8, i8* %7, i32 3
  store i8 %conv10, i8* %arrayidx11, align 1
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

declare void @CRC_writeheader(%struct.lame_internal_flags*, i8*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @PutLameVBR(%struct.lame_global_struct* %gfp, i32 %nMusicLength, i8* %pbtStreamBuffer, i16 zeroext %crc) #0 {
entry:
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %nMusicLength.addr = alloca i32, align 4
  %pbtStreamBuffer.addr = alloca i8*, align 4
  %crc.addr = alloca i16, align 2
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %cfg = alloca %struct.SessionConfig_t*, align 4
  %nBytesWritten = alloca i32, align 4
  %i = alloca i32, align 4
  %enc_delay = alloca i32, align 4
  %enc_padding = alloca i32, align 4
  %nQuality = alloca i32, align 4
  %szVersion = alloca i8*, align 4
  %nVBR = alloca i8, align 1
  %nRevision = alloca i8, align 1
  %nRevMethod = alloca i8, align 1
  %vbr_type_translator = alloca [7 x i8], align 1
  %nLowpass = alloca i8, align 1
  %nPeakSignalAmplitude = alloca i32, align 4
  %nRadioReplayGain = alloca i16, align 2
  %nAudiophileReplayGain = alloca i16, align 2
  %nNoiseShaping = alloca i8, align 1
  %nStereoMode = alloca i8, align 1
  %bNonOptimal = alloca i32, align 4
  %nSourceFreq = alloca i8, align 1
  %nMisc = alloca i8, align 1
  %nMusicCRC = alloca i16, align 2
  %bExpNPsyTune = alloca i8, align 1
  %bSafeJoint = alloca i8, align 1
  %bNoGapMore = alloca i8, align 1
  %bNoGapPrevious = alloca i8, align 1
  %nNoGapCount = alloca i32, align 4
  %nNoGapCurr = alloca i32, align 4
  %nAthType = alloca i8, align 1
  %nFlags = alloca i8, align 1
  %nABRBitrate = alloca i32, align 4
  %RadioGain = alloca i32, align 4
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store i32 %nMusicLength, i32* %nMusicLength.addr, align 4
  store i8* %pbtStreamBuffer, i8** %pbtStreamBuffer.addr, align 4
  store i16 %crc, i16* %crc.addr, align 2
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %cfg1 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 5
  store %struct.SessionConfig_t* %cfg1, %struct.SessionConfig_t** %cfg, align 4
  store i32 0, i32* %nBytesWritten, align 4
  %3 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %3, i32 0, i32 12
  %encoder_delay = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc, i32 0, i32 6
  %4 = load i32, i32* %encoder_delay, align 8
  store i32 %4, i32* %enc_delay, align 4
  %5 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_enc2 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %5, i32 0, i32 12
  %encoder_padding = getelementptr inbounds %struct.EncResult_t, %struct.EncResult_t* %ov_enc2, i32 0, i32 7
  %6 = load i32, i32* %encoder_padding, align 4
  store i32 %6, i32* %enc_padding, align 4
  %7 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %VBR_q = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %7, i32 0, i32 41
  %8 = load i32, i32* %VBR_q, align 4
  %mul = mul nsw i32 10, %8
  %sub = sub nsw i32 100, %mul
  %9 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %quality = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %9, i32 0, i32 11
  %10 = load i32, i32* %quality, align 4
  %sub3 = sub nsw i32 %sub, %10
  store i32 %sub3, i32* %nQuality, align 4
  %call = call i8* @get_lame_tag_encoder_short_version()
  store i8* %call, i8** %szVersion, align 4
  store i8 0, i8* %nRevision, align 1
  %11 = bitcast [7 x i8]* %vbr_type_translator to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %11, i8* align 1 getelementptr inbounds ([7 x i8], [7 x i8]* @__const.PutLameVBR.vbr_type_translator, i32 0, i32 0), i32 7, i1 false)
  %12 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %12, i32 0, i32 9
  %13 = load i32, i32* %lowpassfreq, align 4
  %conv = sitofp i32 %13 to double
  %div = fdiv double %conv, 1.000000e+02
  %add = fadd double %div, 5.000000e-01
  %cmp = fcmp ogt double %add, 2.550000e+02
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %14 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq5 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %14, i32 0, i32 9
  %15 = load i32, i32* %lowpassfreq5, align 4
  %conv6 = sitofp i32 %15 to double
  %div7 = fdiv double %conv6, 1.000000e+02
  %add8 = fadd double %div7, 5.000000e-01
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi double [ 2.550000e+02, %cond.true ], [ %add8, %cond.false ]
  %conv9 = fptoui double %cond to i8
  store i8 %conv9, i8* %nLowpass, align 1
  store i32 0, i32* %nPeakSignalAmplitude, align 4
  store i16 0, i16* %nRadioReplayGain, align 2
  store i16 0, i16* %nAudiophileReplayGain, align 2
  %16 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noise_shaping = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %16, i32 0, i32 3
  %17 = load i32, i32* %noise_shaping, align 4
  %conv10 = trunc i32 %17 to i8
  store i8 %conv10, i8* %nNoiseShaping, align 1
  store i8 0, i8* %nStereoMode, align 1
  store i32 0, i32* %bNonOptimal, align 4
  store i8 0, i8* %nSourceFreq, align 1
  store i8 0, i8* %nMisc, align 1
  store i16 0, i16* %nMusicCRC, align 2
  store i8 1, i8* %bExpNPsyTune, align 1
  %18 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %use_safe_joint_stereo = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %18, i32 0, i32 20
  %19 = load i32, i32* %use_safe_joint_stereo, align 4
  %cmp11 = icmp ne i32 %19, 0
  %conv12 = zext i1 %cmp11 to i32
  %conv13 = trunc i32 %conv12 to i8
  store i8 %conv13, i8* %bSafeJoint, align 1
  store i8 0, i8* %bNoGapMore, align 1
  store i8 0, i8* %bNoGapPrevious, align 1
  %20 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_total = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %20, i32 0, i32 18
  %21 = load i32, i32* %nogap_total, align 4
  store i32 %21, i32* %nNoGapCount, align 4
  %22 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %nogap_current = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %22, i32 0, i32 19
  %23 = load i32, i32* %nogap_current, align 4
  store i32 %23, i32* %nNoGapCurr, align 4
  %24 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHtype = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %24, i32 0, i32 48
  %25 = load i32, i32* %ATHtype, align 4
  %conv14 = trunc i32 %25 to i8
  store i8 %conv14, i8* %nAthType, align 1
  store i8 0, i8* %nFlags, align 1
  %26 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %26, i32 0, i32 22
  %27 = load i32, i32* %vbr, align 4
  switch i32 %27, label %sw.default [
    i32 3, label %sw.bb
    i32 0, label %sw.bb15
  ]

sw.bb:                                            ; preds = %cond.end
  %28 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_avg_bitrate_kbps = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %28, i32 0, i32 23
  %29 = load i32, i32* %vbr_avg_bitrate_kbps, align 4
  store i32 %29, i32* %nABRBitrate, align 4
  br label %sw.epilog

sw.bb15:                                          ; preds = %cond.end
  %30 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %30, i32 0, i32 26
  %31 = load i32, i32* %avg_bitrate, align 4
  store i32 %31, i32* %nABRBitrate, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %cond.end
  %32 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %version = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %32, i32 0, i32 0
  %33 = load i32, i32* %version, align 4
  %arrayidx = getelementptr inbounds [3 x [16 x i32]], [3 x [16 x i32]]* @bitrate_table, i32 0, i32 %33
  %34 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr_min_bitrate_index = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %34, i32 0, i32 24
  %35 = load i32, i32* %vbr_min_bitrate_index, align 4
  %arrayidx16 = getelementptr inbounds [16 x i32], [16 x i32]* %arrayidx, i32 0, i32 %35
  %36 = load i32, i32* %arrayidx16, align 4
  store i32 %36, i32* %nABRBitrate, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb15, %sw.bb
  %37 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr17 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %37, i32 0, i32 22
  %38 = load i32, i32* %vbr17, align 4
  %cmp18 = icmp ult i32 %38, 7
  br i1 %cmp18, label %if.then, label %if.else

if.then:                                          ; preds = %sw.epilog
  %39 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %vbr20 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %39, i32 0, i32 22
  %40 = load i32, i32* %vbr20, align 4
  %arrayidx21 = getelementptr inbounds [7 x i8], [7 x i8]* %vbr_type_translator, i32 0, i32 %40
  %41 = load i8, i8* %arrayidx21, align 1
  store i8 %41, i8* %nVBR, align 1
  br label %if.end

if.else:                                          ; preds = %sw.epilog
  store i8 0, i8* %nVBR, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %42 = load i8, i8* %nRevision, align 1
  %conv22 = zext i8 %42 to i32
  %mul23 = mul nsw i32 16, %conv22
  %43 = load i8, i8* %nVBR, align 1
  %conv24 = zext i8 %43 to i32
  %add25 = add nsw i32 %mul23, %conv24
  %conv26 = trunc i32 %add25 to i8
  store i8 %conv26, i8* %nRevMethod, align 1
  %44 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findReplayGain = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %44, i32 0, i32 28
  %45 = load i32, i32* %findReplayGain, align 4
  %tobool = icmp ne i32 %45, 0
  br i1 %tobool, label %if.then27, label %if.end54

if.then27:                                        ; preds = %if.end
  %46 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %46, i32 0, i32 15
  %RadioGain28 = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg, i32 0, i32 2
  %47 = load i32, i32* %RadioGain28, align 8
  store i32 %47, i32* %RadioGain, align 4
  %48 = load i32, i32* %RadioGain, align 4
  %cmp29 = icmp sgt i32 %48, 510
  br i1 %cmp29, label %if.then31, label %if.end32

if.then31:                                        ; preds = %if.then27
  store i32 510, i32* %RadioGain, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.then31, %if.then27
  %49 = load i32, i32* %RadioGain, align 4
  %cmp33 = icmp slt i32 %49, -510
  br i1 %cmp33, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end32
  store i32 -510, i32* %RadioGain, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then35, %if.end32
  store i16 8192, i16* %nRadioReplayGain, align 2
  %50 = load i16, i16* %nRadioReplayGain, align 2
  %conv37 = zext i16 %50 to i32
  %or = or i32 %conv37, 3072
  %conv38 = trunc i32 %or to i16
  store i16 %conv38, i16* %nRadioReplayGain, align 2
  %51 = load i32, i32* %RadioGain, align 4
  %cmp39 = icmp sge i32 %51, 0
  br i1 %cmp39, label %if.then41, label %if.else45

if.then41:                                        ; preds = %if.end36
  %52 = load i32, i32* %RadioGain, align 4
  %53 = load i16, i16* %nRadioReplayGain, align 2
  %conv42 = zext i16 %53 to i32
  %or43 = or i32 %conv42, %52
  %conv44 = trunc i32 %or43 to i16
  store i16 %conv44, i16* %nRadioReplayGain, align 2
  br label %if.end53

if.else45:                                        ; preds = %if.end36
  %54 = load i16, i16* %nRadioReplayGain, align 2
  %conv46 = zext i16 %54 to i32
  %or47 = or i32 %conv46, 512
  %conv48 = trunc i32 %or47 to i16
  store i16 %conv48, i16* %nRadioReplayGain, align 2
  %55 = load i32, i32* %RadioGain, align 4
  %sub49 = sub nsw i32 0, %55
  %56 = load i16, i16* %nRadioReplayGain, align 2
  %conv50 = zext i16 %56 to i32
  %or51 = or i32 %conv50, %sub49
  %conv52 = trunc i32 %or51 to i16
  store i16 %conv52, i16* %nRadioReplayGain, align 2
  br label %if.end53

if.end53:                                         ; preds = %if.else45, %if.then41
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %if.end
  %57 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %findPeakSample = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %57, i32 0, i32 29
  %58 = load i32, i32* %findPeakSample, align 4
  %tobool55 = icmp ne i32 %58, 0
  br i1 %tobool55, label %if.then56, label %if.end64

if.then56:                                        ; preds = %if.end54
  %59 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %ov_rpg57 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %59, i32 0, i32 15
  %PeakSample = getelementptr inbounds %struct.RpgResult_t, %struct.RpgResult_t* %ov_rpg57, i32 0, i32 1
  %60 = load float, float* %PeakSample, align 4
  %conv58 = fpext float %60 to double
  %div59 = fdiv double %conv58, 3.276700e+04
  %61 = call double @llvm.pow.f64(double 2.000000e+00, double 2.300000e+01)
  %mul60 = fmul double %div59, %61
  %add61 = fadd double %mul60, 5.000000e-01
  %conv62 = fptosi double %add61 to i32
  %call63 = call i32 @abs(i32 %conv62) #6
  store i32 %call63, i32* %nPeakSignalAmplitude, align 4
  br label %if.end64

if.end64:                                         ; preds = %if.then56, %if.end54
  %62 = load i32, i32* %nNoGapCount, align 4
  %cmp65 = icmp ne i32 %62, -1
  br i1 %cmp65, label %if.then67, label %if.end77

if.then67:                                        ; preds = %if.end64
  %63 = load i32, i32* %nNoGapCurr, align 4
  %cmp68 = icmp sgt i32 %63, 0
  br i1 %cmp68, label %if.then70, label %if.end71

if.then70:                                        ; preds = %if.then67
  store i8 1, i8* %bNoGapPrevious, align 1
  br label %if.end71

if.end71:                                         ; preds = %if.then70, %if.then67
  %64 = load i32, i32* %nNoGapCurr, align 4
  %65 = load i32, i32* %nNoGapCount, align 4
  %sub72 = sub nsw i32 %65, 1
  %cmp73 = icmp slt i32 %64, %sub72
  br i1 %cmp73, label %if.then75, label %if.end76

if.then75:                                        ; preds = %if.end71
  store i8 1, i8* %bNoGapMore, align 1
  br label %if.end76

if.end76:                                         ; preds = %if.then75, %if.end71
  br label %if.end77

if.end77:                                         ; preds = %if.end76, %if.end64
  %66 = load i8, i8* %nAthType, align 1
  %conv78 = zext i8 %66 to i32
  %67 = load i8, i8* %bExpNPsyTune, align 1
  %conv79 = zext i8 %67 to i32
  %shl = shl i32 %conv79, 4
  %add80 = add nsw i32 %conv78, %shl
  %68 = load i8, i8* %bSafeJoint, align 1
  %conv81 = zext i8 %68 to i32
  %shl82 = shl i32 %conv81, 5
  %add83 = add nsw i32 %add80, %shl82
  %69 = load i8, i8* %bNoGapMore, align 1
  %conv84 = zext i8 %69 to i32
  %shl85 = shl i32 %conv84, 6
  %add86 = add nsw i32 %add83, %shl85
  %70 = load i8, i8* %bNoGapPrevious, align 1
  %conv87 = zext i8 %70 to i32
  %shl88 = shl i32 %conv87, 7
  %add89 = add nsw i32 %add86, %shl88
  %conv90 = trunc i32 %add89 to i8
  store i8 %conv90, i8* %nFlags, align 1
  %71 = load i32, i32* %nQuality, align 4
  %cmp91 = icmp slt i32 %71, 0
  br i1 %cmp91, label %if.then93, label %if.end94

if.then93:                                        ; preds = %if.end77
  store i32 0, i32* %nQuality, align 4
  br label %if.end94

if.end94:                                         ; preds = %if.then93, %if.end77
  %72 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %mode = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %72, i32 0, i32 41
  %73 = load i32, i32* %mode, align 4
  switch i32 %73, label %sw.default104 [
    i32 3, label %sw.bb95
    i32 0, label %sw.bb96
    i32 2, label %sw.bb97
    i32 1, label %sw.bb98
    i32 4, label %sw.bb103
  ]

sw.bb95:                                          ; preds = %if.end94
  store i8 0, i8* %nStereoMode, align 1
  br label %sw.epilog105

sw.bb96:                                          ; preds = %if.end94
  store i8 1, i8* %nStereoMode, align 1
  br label %sw.epilog105

sw.bb97:                                          ; preds = %if.end94
  store i8 2, i8* %nStereoMode, align 1
  br label %sw.epilog105

sw.bb98:                                          ; preds = %if.end94
  %74 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %force_ms = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %74, i32 0, i32 16
  %75 = load i32, i32* %force_ms, align 4
  %tobool99 = icmp ne i32 %75, 0
  br i1 %tobool99, label %if.then100, label %if.else101

if.then100:                                       ; preds = %sw.bb98
  store i8 4, i8* %nStereoMode, align 1
  br label %if.end102

if.else101:                                       ; preds = %sw.bb98
  store i8 3, i8* %nStereoMode, align 1
  br label %if.end102

if.end102:                                        ; preds = %if.else101, %if.then100
  br label %sw.epilog105

sw.bb103:                                         ; preds = %if.end94
  br label %sw.default104

sw.default104:                                    ; preds = %if.end94, %sw.bb103
  store i8 7, i8* %nStereoMode, align 1
  br label %sw.epilog105

sw.epilog105:                                     ; preds = %sw.default104, %if.end102, %sw.bb97, %sw.bb96, %sw.bb95
  %76 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %76, i32 0, i32 11
  %77 = load i32, i32* %samplerate_in, align 4
  %cmp106 = icmp sle i32 %77, 32000
  br i1 %cmp106, label %if.then108, label %if.else109

if.then108:                                       ; preds = %sw.epilog105
  store i8 0, i8* %nSourceFreq, align 1
  br label %if.end122

if.else109:                                       ; preds = %sw.epilog105
  %78 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in110 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %78, i32 0, i32 11
  %79 = load i32, i32* %samplerate_in110, align 4
  %cmp111 = icmp eq i32 %79, 48000
  br i1 %cmp111, label %if.then113, label %if.else114

if.then113:                                       ; preds = %if.else109
  store i8 2, i8* %nSourceFreq, align 1
  br label %if.end121

if.else114:                                       ; preds = %if.else109
  %80 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in115 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %80, i32 0, i32 11
  %81 = load i32, i32* %samplerate_in115, align 4
  %cmp116 = icmp sgt i32 %81, 48000
  br i1 %cmp116, label %if.then118, label %if.else119

if.then118:                                       ; preds = %if.else114
  store i8 3, i8* %nSourceFreq, align 1
  br label %if.end120

if.else119:                                       ; preds = %if.else114
  store i8 1, i8* %nSourceFreq, align 1
  br label %if.end120

if.end120:                                        ; preds = %if.else119, %if.then118
  br label %if.end121

if.end121:                                        ; preds = %if.end120, %if.then113
  br label %if.end122

if.end122:                                        ; preds = %if.end121, %if.then108
  %82 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %short_blocks = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %82, i32 0, i32 42
  %83 = load i32, i32* %short_blocks, align 4
  %cmp123 = icmp eq i32 %83, 3
  br i1 %cmp123, label %if.then152, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end122
  %84 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %short_blocks125 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %84, i32 0, i32 42
  %85 = load i32, i32* %short_blocks125, align 4
  %cmp126 = icmp eq i32 %85, 2
  br i1 %cmp126, label %if.then152, label %lor.lhs.false128

lor.lhs.false128:                                 ; preds = %lor.lhs.false
  %86 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %lowpassfreq129 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %86, i32 0, i32 9
  %87 = load i32, i32* %lowpassfreq129, align 4
  %cmp130 = icmp eq i32 %87, -1
  br i1 %cmp130, label %land.lhs.true, label %lor.lhs.false134

land.lhs.true:                                    ; preds = %lor.lhs.false128
  %88 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %highpassfreq = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %88, i32 0, i32 10
  %89 = load i32, i32* %highpassfreq, align 4
  %cmp132 = icmp eq i32 %89, -1
  br i1 %cmp132, label %if.then152, label %lor.lhs.false134

lor.lhs.false134:                                 ; preds = %land.lhs.true, %lor.lhs.false128
  %90 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %disable_reservoir = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %90, i32 0, i32 32
  %91 = load i32, i32* %disable_reservoir, align 4
  %tobool135 = icmp ne i32 %91, 0
  br i1 %tobool135, label %land.lhs.true136, label %lor.lhs.false140

land.lhs.true136:                                 ; preds = %lor.lhs.false134
  %92 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %avg_bitrate137 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %92, i32 0, i32 26
  %93 = load i32, i32* %avg_bitrate137, align 4
  %cmp138 = icmp slt i32 %93, 320
  br i1 %cmp138, label %if.then152, label %lor.lhs.false140

lor.lhs.false140:                                 ; preds = %land.lhs.true136, %lor.lhs.false134
  %94 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %noATH = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %94, i32 0, i32 51
  %95 = load i32, i32* %noATH, align 4
  %tobool141 = icmp ne i32 %95, 0
  br i1 %tobool141, label %if.then152, label %lor.lhs.false142

lor.lhs.false142:                                 ; preds = %lor.lhs.false140
  %96 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %ATHonly = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %96, i32 0, i32 49
  %97 = load i32, i32* %ATHonly, align 4
  %tobool143 = icmp ne i32 %97, 0
  br i1 %tobool143, label %if.then152, label %lor.lhs.false144

lor.lhs.false144:                                 ; preds = %lor.lhs.false142
  %98 = load i8, i8* %nAthType, align 1
  %conv145 = zext i8 %98 to i32
  %cmp146 = icmp eq i32 %conv145, 0
  br i1 %cmp146, label %if.then152, label %lor.lhs.false148

lor.lhs.false148:                                 ; preds = %lor.lhs.false144
  %99 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %samplerate_in149 = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %99, i32 0, i32 11
  %100 = load i32, i32* %samplerate_in149, align 4
  %cmp150 = icmp sle i32 %100, 32000
  br i1 %cmp150, label %if.then152, label %if.end153

if.then152:                                       ; preds = %lor.lhs.false148, %lor.lhs.false144, %lor.lhs.false142, %lor.lhs.false140, %land.lhs.true136, %land.lhs.true, %lor.lhs.false, %if.end122
  store i32 1, i32* %bNonOptimal, align 4
  br label %if.end153

if.end153:                                        ; preds = %if.then152, %lor.lhs.false148
  %101 = load i8, i8* %nNoiseShaping, align 1
  %conv154 = zext i8 %101 to i32
  %102 = load i8, i8* %nStereoMode, align 1
  %conv155 = zext i8 %102 to i32
  %shl156 = shl i32 %conv155, 2
  %add157 = add nsw i32 %conv154, %shl156
  %103 = load i32, i32* %bNonOptimal, align 4
  %shl158 = shl i32 %103, 5
  %add159 = add nsw i32 %add157, %shl158
  %104 = load i8, i8* %nSourceFreq, align 1
  %conv160 = zext i8 %104 to i32
  %shl161 = shl i32 %conv160, 6
  %add162 = add nsw i32 %add159, %shl161
  %conv163 = trunc i32 %add162 to i8
  store i8 %conv163, i8* %nMisc, align 1
  %105 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %nMusicCRC164 = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %105, i32 0, i32 17
  %106 = load i16, i16* %nMusicCRC164, align 4
  store i16 %106, i16* %nMusicCRC, align 2
  %107 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %108 = load i32, i32* %nBytesWritten, align 4
  %arrayidx165 = getelementptr inbounds i8, i8* %107, i32 %108
  %109 = load i32, i32* %nQuality, align 4
  call void @CreateI4(i8* %arrayidx165, i32 %109)
  %110 = load i32, i32* %nBytesWritten, align 4
  %add166 = add nsw i32 %110, 4
  store i32 %add166, i32* %nBytesWritten, align 4
  %111 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %112 = load i32, i32* %nBytesWritten, align 4
  %arrayidx167 = getelementptr inbounds i8, i8* %111, i32 %112
  %113 = load i8*, i8** %szVersion, align 4
  %call168 = call i8* @strncpy(i8* %arrayidx167, i8* %113, i32 9)
  %114 = load i32, i32* %nBytesWritten, align 4
  %add169 = add nsw i32 %114, 9
  store i32 %add169, i32* %nBytesWritten, align 4
  %115 = load i8, i8* %nRevMethod, align 1
  %116 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %117 = load i32, i32* %nBytesWritten, align 4
  %arrayidx170 = getelementptr inbounds i8, i8* %116, i32 %117
  store i8 %115, i8* %arrayidx170, align 1
  %118 = load i32, i32* %nBytesWritten, align 4
  %inc = add nsw i32 %118, 1
  store i32 %inc, i32* %nBytesWritten, align 4
  %119 = load i8, i8* %nLowpass, align 1
  %120 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %121 = load i32, i32* %nBytesWritten, align 4
  %arrayidx171 = getelementptr inbounds i8, i8* %120, i32 %121
  store i8 %119, i8* %arrayidx171, align 1
  %122 = load i32, i32* %nBytesWritten, align 4
  %inc172 = add nsw i32 %122, 1
  store i32 %inc172, i32* %nBytesWritten, align 4
  %123 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %124 = load i32, i32* %nBytesWritten, align 4
  %arrayidx173 = getelementptr inbounds i8, i8* %123, i32 %124
  %125 = load i32, i32* %nPeakSignalAmplitude, align 4
  call void @CreateI4(i8* %arrayidx173, i32 %125)
  %126 = load i32, i32* %nBytesWritten, align 4
  %add174 = add nsw i32 %126, 4
  store i32 %add174, i32* %nBytesWritten, align 4
  %127 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %128 = load i32, i32* %nBytesWritten, align 4
  %arrayidx175 = getelementptr inbounds i8, i8* %127, i32 %128
  %129 = load i16, i16* %nRadioReplayGain, align 2
  %conv176 = zext i16 %129 to i32
  call void @CreateI2(i8* %arrayidx175, i32 %conv176)
  %130 = load i32, i32* %nBytesWritten, align 4
  %add177 = add nsw i32 %130, 2
  store i32 %add177, i32* %nBytesWritten, align 4
  %131 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %132 = load i32, i32* %nBytesWritten, align 4
  %arrayidx178 = getelementptr inbounds i8, i8* %131, i32 %132
  %133 = load i16, i16* %nAudiophileReplayGain, align 2
  %conv179 = zext i16 %133 to i32
  call void @CreateI2(i8* %arrayidx178, i32 %conv179)
  %134 = load i32, i32* %nBytesWritten, align 4
  %add180 = add nsw i32 %134, 2
  store i32 %add180, i32* %nBytesWritten, align 4
  %135 = load i8, i8* %nFlags, align 1
  %136 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %137 = load i32, i32* %nBytesWritten, align 4
  %arrayidx181 = getelementptr inbounds i8, i8* %136, i32 %137
  store i8 %135, i8* %arrayidx181, align 1
  %138 = load i32, i32* %nBytesWritten, align 4
  %inc182 = add nsw i32 %138, 1
  store i32 %inc182, i32* %nBytesWritten, align 4
  %139 = load i32, i32* %nABRBitrate, align 4
  %cmp183 = icmp sge i32 %139, 255
  br i1 %cmp183, label %if.then185, label %if.else187

if.then185:                                       ; preds = %if.end153
  %140 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %141 = load i32, i32* %nBytesWritten, align 4
  %arrayidx186 = getelementptr inbounds i8, i8* %140, i32 %141
  store i8 -1, i8* %arrayidx186, align 1
  br label %if.end190

if.else187:                                       ; preds = %if.end153
  %142 = load i32, i32* %nABRBitrate, align 4
  %conv188 = trunc i32 %142 to i8
  %143 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %144 = load i32, i32* %nBytesWritten, align 4
  %arrayidx189 = getelementptr inbounds i8, i8* %143, i32 %144
  store i8 %conv188, i8* %arrayidx189, align 1
  br label %if.end190

if.end190:                                        ; preds = %if.else187, %if.then185
  %145 = load i32, i32* %nBytesWritten, align 4
  %inc191 = add nsw i32 %145, 1
  store i32 %inc191, i32* %nBytesWritten, align 4
  %146 = load i32, i32* %enc_delay, align 4
  %shr = ashr i32 %146, 4
  %conv192 = trunc i32 %shr to i8
  %147 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %148 = load i32, i32* %nBytesWritten, align 4
  %arrayidx193 = getelementptr inbounds i8, i8* %147, i32 %148
  store i8 %conv192, i8* %arrayidx193, align 1
  %149 = load i32, i32* %enc_delay, align 4
  %shl194 = shl i32 %149, 4
  %150 = load i32, i32* %enc_padding, align 4
  %shr195 = ashr i32 %150, 8
  %add196 = add nsw i32 %shl194, %shr195
  %conv197 = trunc i32 %add196 to i8
  %151 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %152 = load i32, i32* %nBytesWritten, align 4
  %add198 = add nsw i32 %152, 1
  %arrayidx199 = getelementptr inbounds i8, i8* %151, i32 %add198
  store i8 %conv197, i8* %arrayidx199, align 1
  %153 = load i32, i32* %enc_padding, align 4
  %conv200 = trunc i32 %153 to i8
  %154 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %155 = load i32, i32* %nBytesWritten, align 4
  %add201 = add nsw i32 %155, 2
  %arrayidx202 = getelementptr inbounds i8, i8* %154, i32 %add201
  store i8 %conv200, i8* %arrayidx202, align 1
  %156 = load i32, i32* %nBytesWritten, align 4
  %add203 = add nsw i32 %156, 3
  store i32 %add203, i32* %nBytesWritten, align 4
  %157 = load i8, i8* %nMisc, align 1
  %158 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %159 = load i32, i32* %nBytesWritten, align 4
  %arrayidx204 = getelementptr inbounds i8, i8* %158, i32 %159
  store i8 %157, i8* %arrayidx204, align 1
  %160 = load i32, i32* %nBytesWritten, align 4
  %inc205 = add nsw i32 %160, 1
  store i32 %inc205, i32* %nBytesWritten, align 4
  %161 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %162 = load i32, i32* %nBytesWritten, align 4
  %inc206 = add nsw i32 %162, 1
  store i32 %inc206, i32* %nBytesWritten, align 4
  %arrayidx207 = getelementptr inbounds i8, i8* %161, i32 %162
  store i8 0, i8* %arrayidx207, align 1
  %163 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %164 = load i32, i32* %nBytesWritten, align 4
  %arrayidx208 = getelementptr inbounds i8, i8* %163, i32 %164
  %165 = load %struct.SessionConfig_t*, %struct.SessionConfig_t** %cfg, align 4
  %preset = getelementptr inbounds %struct.SessionConfig_t, %struct.SessionConfig_t* %165, i32 0, i32 21
  %166 = load i32, i32* %preset, align 4
  call void @CreateI2(i8* %arrayidx208, i32 %166)
  %167 = load i32, i32* %nBytesWritten, align 4
  %add209 = add nsw i32 %167, 2
  store i32 %add209, i32* %nBytesWritten, align 4
  %168 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %169 = load i32, i32* %nBytesWritten, align 4
  %arrayidx210 = getelementptr inbounds i8, i8* %168, i32 %169
  %170 = load i32, i32* %nMusicLength.addr, align 4
  call void @CreateI4(i8* %arrayidx210, i32 %170)
  %171 = load i32, i32* %nBytesWritten, align 4
  %add211 = add nsw i32 %171, 4
  store i32 %add211, i32* %nBytesWritten, align 4
  %172 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %173 = load i32, i32* %nBytesWritten, align 4
  %arrayidx212 = getelementptr inbounds i8, i8* %172, i32 %173
  %174 = load i16, i16* %nMusicCRC, align 2
  %conv213 = zext i16 %174 to i32
  call void @CreateI2(i8* %arrayidx212, i32 %conv213)
  %175 = load i32, i32* %nBytesWritten, align 4
  %add214 = add nsw i32 %175, 2
  store i32 %add214, i32* %nBytesWritten, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end190
  %176 = load i32, i32* %i, align 4
  %177 = load i32, i32* %nBytesWritten, align 4
  %cmp215 = icmp slt i32 %176, %177
  br i1 %cmp215, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %178 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %179 = load i32, i32* %i, align 4
  %arrayidx217 = getelementptr inbounds i8, i8* %178, i32 %179
  %180 = load i8, i8* %arrayidx217, align 1
  %conv218 = zext i8 %180 to i16
  %181 = load i16, i16* %crc.addr, align 2
  %call219 = call zeroext i16 @CRC_update_lookup(i16 zeroext %conv218, i16 zeroext %181)
  store i16 %call219, i16* %crc.addr, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %182 = load i32, i32* %i, align 4
  %inc220 = add nsw i32 %182, 1
  store i32 %inc220, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %183 = load i8*, i8** %pbtStreamBuffer.addr, align 4
  %184 = load i32, i32* %nBytesWritten, align 4
  %arrayidx221 = getelementptr inbounds i8, i8* %183, i32 %184
  %185 = load i16, i16* %crc.addr, align 2
  %conv222 = zext i16 %185 to i32
  call void @CreateI2(i8* %arrayidx221, i32 %conv222)
  %186 = load i32, i32* %nBytesWritten, align 4
  %add223 = add nsw i32 %186, 2
  store i32 %add223, i32* %nBytesWritten, align 4
  %187 = load i32, i32* %nBytesWritten, align 4
  ret i32 %187
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @PutVbrTag(%struct.lame_global_struct* %gfp, %struct._IO_FILE* %fpStream) #0 {
entry:
  %retval = alloca i32, align 4
  %gfp.addr = alloca %struct.lame_global_struct*, align 4
  %fpStream.addr = alloca %struct._IO_FILE*, align 4
  %gfc = alloca %struct.lame_internal_flags*, align 4
  %lFileSize = alloca i32, align 4
  %id3v2TagSize = alloca i32, align 4
  %nbytes = alloca i32, align 4
  %buffer = alloca [2880 x i8], align 16
  store %struct.lame_global_struct* %gfp, %struct.lame_global_struct** %gfp.addr, align 4
  store %struct._IO_FILE* %fpStream, %struct._IO_FILE** %fpStream.addr, align 4
  %0 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %internal_flags = getelementptr inbounds %struct.lame_global_struct, %struct.lame_global_struct* %0, i32 0, i32 70
  %1 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %internal_flags, align 4
  store %struct.lame_internal_flags* %1, %struct.lame_internal_flags** %gfc, align 4
  %2 = load %struct.lame_internal_flags*, %struct.lame_internal_flags** %gfc, align 4
  %VBR_seek_table = getelementptr inbounds %struct.lame_internal_flags, %struct.lame_internal_flags* %2, i32 0, i32 20
  %pos = getelementptr inbounds %struct.VBR_seek_info_t, %struct.VBR_seek_info_t* %VBR_seek_table, i32 0, i32 3
  %3 = load i32, i32* %pos, align 4
  %cmp = icmp sle i32 %3, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call = call i32 @fseek(%struct._IO_FILE* %4, i32 0, i32 2)
  %5 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call1 = call i32 @ftell(%struct._IO_FILE* %5)
  store i32 %call1, i32* %lFileSize, align 4
  %6 = load i32, i32* %lFileSize, align 4
  %cmp2 = icmp eq i32 %6, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call5 = call i32 @skipId3v2(%struct._IO_FILE* %7)
  store i32 %call5, i32* %id3v2TagSize, align 4
  %8 = load i32, i32* %id3v2TagSize, align 4
  %cmp6 = icmp slt i32 %8, 0
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end4
  %9 = load i32, i32* %id3v2TagSize, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

if.end8:                                          ; preds = %if.end4
  %10 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %11 = load i32, i32* %id3v2TagSize, align 4
  %call9 = call i32 @fseek(%struct._IO_FILE* %10, i32 %11, i32 0)
  %12 = load %struct.lame_global_struct*, %struct.lame_global_struct** %gfp.addr, align 4
  %arraydecay = getelementptr inbounds [2880 x i8], [2880 x i8]* %buffer, i32 0, i32 0
  %call10 = call i32 @lame_get_lametag_frame(%struct.lame_global_struct* %12, i8* %arraydecay, i32 2880)
  store i32 %call10, i32* %nbytes, align 4
  %13 = load i32, i32* %nbytes, align 4
  %cmp11 = icmp ugt i32 %13, 2880
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %if.end8
  store i32 -1, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %if.end8
  %14 = load i32, i32* %nbytes, align 4
  %cmp14 = icmp ult i32 %14, 1
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end13
  store i32 0, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.end13
  %arraydecay17 = getelementptr inbounds [2880 x i8], [2880 x i8]* %buffer, i32 0, i32 0
  %15 = load i32, i32* %nbytes, align 4
  %16 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call18 = call i32 @fwrite(i8* %arraydecay17, i32 %15, i32 1, %struct._IO_FILE* %16)
  %cmp19 = icmp ne i32 %call18, 1
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.end16
  store i32 -1, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %if.end16
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then20, %if.then15, %if.then12, %if.then7, %if.then3, %if.then
  %17 = load i32, i32* %retval, align 4
  ret i32 %17
}

declare i32 @fseek(%struct._IO_FILE*, i32, i32) #1

declare i32 @ftell(%struct._IO_FILE*) #1

; Function Attrs: noinline nounwind optnone
define internal i32 @skipId3v2(%struct._IO_FILE* %fpStream) #0 {
entry:
  %retval = alloca i32, align 4
  %fpStream.addr = alloca %struct._IO_FILE*, align 4
  %nbytes = alloca i32, align 4
  %id3v2TagSize = alloca i32, align 4
  %id3v2Header = alloca [10 x i8], align 1
  store %struct._IO_FILE* %fpStream, %struct._IO_FILE** %fpStream.addr, align 4
  %0 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call = call i32 @fseek(%struct._IO_FILE* %0, i32 0, i32 0)
  %cmp = icmp ne i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -2, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %arraydecay = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 0
  %1 = load %struct._IO_FILE*, %struct._IO_FILE** %fpStream.addr, align 4
  %call1 = call i32 @fread(i8* %arraydecay, i32 1, i32 10, %struct._IO_FILE* %1)
  store i32 %call1, i32* %nbytes, align 4
  %2 = load i32, i32* %nbytes, align 4
  %cmp2 = icmp ne i32 %2, 10
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 -3, i32* %retval, align 4
  br label %return

if.end4:                                          ; preds = %if.end
  %arraydecay5 = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 0
  %call6 = call i32 @strncmp(i8* %arraydecay5, i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.1, i32 0, i32 0), i32 3)
  %tobool = icmp ne i32 %call6, 0
  br i1 %tobool, label %if.else, label %if.then7

if.then7:                                         ; preds = %if.end4
  %arrayidx = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 6
  %3 = load i8, i8* %arrayidx, align 1
  %conv = zext i8 %3 to i32
  %and = and i32 %conv, 127
  %shl = shl i32 %and, 21
  %arrayidx8 = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 7
  %4 = load i8, i8* %arrayidx8, align 1
  %conv9 = zext i8 %4 to i32
  %and10 = and i32 %conv9, 127
  %shl11 = shl i32 %and10, 14
  %or = or i32 %shl, %shl11
  %arrayidx12 = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 8
  %5 = load i8, i8* %arrayidx12, align 1
  %conv13 = zext i8 %5 to i32
  %and14 = and i32 %conv13, 127
  %shl15 = shl i32 %and14, 7
  %or16 = or i32 %or, %shl15
  %arrayidx17 = getelementptr inbounds [10 x i8], [10 x i8]* %id3v2Header, i32 0, i32 9
  %6 = load i8, i8* %arrayidx17, align 1
  %conv18 = zext i8 %6 to i32
  %and19 = and i32 %conv18, 127
  %or20 = or i32 %or16, %and19
  %add = add i32 %or20, 10
  store i32 %add, i32* %id3v2TagSize, align 4
  br label %if.end21

if.else:                                          ; preds = %if.end4
  store i32 0, i32* %id3v2TagSize, align 4
  br label %if.end21

if.end21:                                         ; preds = %if.else, %if.then7
  %7 = load i32, i32* %id3v2TagSize, align 4
  store i32 %7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end21, %if.then3, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

declare i32 @fwrite(i8*, i32, i32, %struct._IO_FILE*) #1

declare i32 @BitrateIndex(i32, i32, i32) #1

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.floor.f64(double) #4

declare i8* @get_lame_tag_encoder_short_version() #1

; Function Attrs: nounwind readnone
declare i32 @abs(i32) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.pow.f64(double, double) #4

declare i8* @strncpy(i8*, i8*, i32) #1

; Function Attrs: noinline nounwind optnone
define internal void @CreateI2(i8* %buf, i32 %nValue) #0 {
entry:
  %buf.addr = alloca i8*, align 4
  %nValue.addr = alloca i32, align 4
  store i8* %buf, i8** %buf.addr, align 4
  store i32 %nValue, i32* %nValue.addr, align 4
  %0 = load i32, i32* %nValue.addr, align 4
  %shr = ashr i32 %0, 8
  %and = and i32 %shr, 255
  %conv = trunc i32 %and to i8
  %1 = load i8*, i8** %buf.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %1, i32 0
  store i8 %conv, i8* %arrayidx, align 1
  %2 = load i32, i32* %nValue.addr, align 4
  %and1 = and i32 %2, 255
  %conv2 = trunc i32 %and1 to i8
  %3 = load i8*, i8** %buf.addr, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %3, i32 1
  store i8 %conv2, i8* %arrayidx3, align 1
  ret void
}

declare i32 @fread(i8*, i32, i32, %struct._IO_FILE*) #1

declare i32 @strncmp(i8*, i8*, i32) #1

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { argmemonly nounwind willreturn writeonly }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
