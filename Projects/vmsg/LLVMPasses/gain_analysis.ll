; ModuleID = 'gain_analysis.c'
source_filename = "gain_analysis.c"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.replaygain_data = type { [20 x float], float*, [2411 x float], float*, [2411 x float], float*, [20 x float], float*, [2411 x float], float*, [2411 x float], float*, i32, i32, double, double, i32, i32, [12000 x i32], [12000 x i32] }

@ABYule = internal constant [9 x [24 x float]] [[24 x float] [float 0x3F67A185A0000000, float 0x3F1F860EA0000000, float 0x3F691A42E0000000, float 0x3F7857AAC0000000, float 0xBF953CFC20000000, float 0x3F96225020000000, float 0xBF90F32A60000000, float 0xBF185B8A20000000, float 0xBF54379320000000, float 0xBF961F45E0000000, float 0x3FA3C03BA0000000, float 0x3FC1D114C0000000, float 0xBFEBD5C2A0000000, float 0x4006098A80000000, float 0xC0177D8540000000, float 0x4022F743A0000000, float 0xC028934020000000, float 0x402A1C2E80000000, float 0xC026AEF3C0000000, float 0x401F4293C0000000, float 0xC00EC5EE60000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF5EC36240000000, float 0x3F7BA1D660000000, float 0xBF63BB9980000000, float 0x3F90A37C60000000, float 0xBF9A962540000000, float 0x3F96FDE6C0000000, float 0xBF8119C1E0000000, float 0xBF816E8F40000000, float 0xBF8161AE80000000, float 0xBF9DCF0840000000, float 0x3FABBE5760000000, float 0x3FC0D4C4C0000000, float 0xBFE8088B60000000, float 0x400191A5C0000000, float 0xC011942EE0000000, float 0x401B6A8300000000, float 0xC021A14600000000, float 0x4022F43100000000, float 0xC0211853E0000000, float 0x401973E4E0000000, float 0xC00BD3E2A0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF820CE100000000, float 0x3F7AAEA400000000, float 0xBF8C7AB180000000, float 0x3FA04058C0000000, float 0x3F62363BA0000000, float 0x3FA87B2D60000000, float 0xBFAC9CD1A0000000, float 0x3F96279860000000, float 0xBFAFFD38C0000000, float 0xBFB7E33240000000, float 0x3FC3C90C40000000, float 0x3F980ADF40000000, float 0xBFA9C3A520000000, float 0x3FC4F6CC00000000, float 0xBFDD6903C0000000, float 0x3FF0186900000000, float 0xBFFABE6360000000, float 0x4001E55400000000, float 0xC0052A8A60000000, float 0x4006CA1980000000, float 0xC003082B00000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF9E359A20000000, float 0x3F60DD3CA0000000, float 0xBEE15DC1C0000000, float 0x3FB0111B20000000, float 0xBF77F07740000000, float 0xBF98357440000000, float 0xBF82C0EBA0000000, float 0x3FA0CF00A0000000, float 0xBFB5FBC9E0000000, float 0xBFCCF226E0000000, float 0x3FD363D860000000, float 0x3F68C69B80000000, float 0x3F948A3860000000, float 0x3FA70A8C60000000, float 0xBFCC5639C0000000, float 0x3FD9098D40000000, float 0xBFCCFA5000000000, float 0xBFC4D58E20000000, float 0xBFD06B8580000000, float 0x3FF146C220000000, float 0xBFF9CDBFC0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF92063280000000, float 0xBF90BF0DE0000000, float 0x3F810A4E80000000, float 0x3FAD4EDC40000000, float 0xBF78255BA0000000, float 0xBF73401360000000, float 0xBFB40E6940000000, float 0x3FBE84A4E0000000, float 0xBFBE47F8C0000000, float 0xBFD05DC180000000, float 0x3FD587F480000000, float 0x3F9E7C9200000000, float 0xBFA5B1FA00000000, float 0x3FB5559C20000000, float 0xBFA4D35E00000000, float 0xBFBFE17FA0000000, float 0x3FDEA087A0000000, float 0xBFE9D91560000000, float 0x3FBF3EAF00000000, float 0x3FEBF3BC00000000, float 0xBFF7FA3940000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0x3F76325080000000, float 0xBFA059B0C0000000, float 0xBF931611E0000000, float 0x3FBAD33120000000, float 0x3FA4FAC2C0000000, float 0xBFBFBD42A0000000, float 0x3FA4E175E0000000, float 0xBF8D1061A0000000, float 0xBFCD29FD80000000, float 0xBFC25EC8A0000000, float 0x3FDCBEEA60000000, float 0x3FA08020E0000000, float 0x3FAD9E47A0000000, float 0x3FB1461EE0000000, float 0x3F79203880000000, float 0x3FCC6A61A0000000, float 0xBFDAE62A60000000, float 0x3F61830980000000, float 0xBFD7D81580000000, float 0x3FD2FBC960000000, float 0xBFE41A43E0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF7817E2C0000000, float 0xBFA3664C40000000, float 0x3FB6233A60000000, float 0x3F7A838B60000000, float 0xBFD1A3EA60000000, float 0x3FD3CBDD00000000, float 0xBFC831AD80000000, float 0x3FC56EC100000000, float 0x3FC4CA3940000000, float 0xBFE8260C60000000, float 0x3FE21E4460000000, float 0x3F9281E5A0000000, float 0x3F90CAEB80000000, float 0xBFA87ED160000000, float 0x3FB140B660000000, float 0xBFD5240760000000, float 0x3FDCD5C480000000, float 0x3F80CB28E0000000, float 0xBFD127E520000000, float 0x3FD2A8F860000000, float 0xBFF0C49F20000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF7EB452A0000000, float 0xBFA30DFDA0000000, float 0x3FB1B765E0000000, float 0x3F90ACEAA0000000, float 0xBFD0387D80000000, float 0x3FC3EA3140000000, float 0x3F9859A400000000, float 0x3FC66D2F40000000, float 0xBFC24A7B40000000, float 0xBFE10416A0000000, float 0x3FE29797C0000000, float 0x3F929FE0C0000000, float 0x3F99027E40000000, float 0xBF99A93A80000000, float 0xBFAADC0FE0000000, float 0xBFCDD74B00000000, float 0x3FD8EE0020000000, float 0x3FC2DA1F20000000, float 0xBFC9ED9F20000000, float 0xBFD46486C0000000, float 0xBFE054D060000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [24 x float] [float 0xBF96B63040000000, float 0x3FA8849980000000, float 0xBFA4C99160000000, float 0xBFBCAD8CA0000000, float 0xBF99306360000000, float 0x3FC2AD1AC0000000, float 0xBFBA2669E0000000, float 0x3FA5D9F240000000, float 0xBF669B2AA0000000, float 0xBFDAFBFDE0000000, float 0x3FE12AE8A0000000, float 0x3FA8162A00000000, float 0x3FAC0BC200000000, float 0xBFC817EC80000000, float 0xBFC678E960000000, float 0x3FC3584540000000, float 0x3FD0E6BC60000000, float 0xBFA7F3FAA0000000, float 0xBFA188CC40000000, float 0xBFDBA4E540000000, float 0xBFD0082BC0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00]], align 16
@ABButter = internal constant [9 x [8 x float]] [[8 x float] [float 0x3FEF8F0C40000000, float 0x3FEF1FA760000000, float 0xBFFF8F0C40000000, float 0xBFFF8E4500000000, float 0x3FEF8F0C40000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEF852260000000, float 0x3FEF0C1CA0000000, float 0xBFFF852260000000, float 0xBFFF843680000000, float 0x3FEF852260000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEF572840000000, float 0x3FEEB1CB80000000, float 0xBFFF572840000000, float 0xBFFF556AC0000000, float 0x3FEF572840000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEF35CF00000000, float 0x3FEE709BC0000000, float 0xBFFF35CF00000000, float 0xBFFF335020000000, float 0x3FEF35CF00000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEF242B60000000, float 0x3FEE4E3D40000000, float 0xBFFF242B60000000, float 0xBFFF213840000000, float 0x3FEF242B60000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEEDD8DC0000000, float 0x3FEDC568A0000000, float 0xBFFEDD8DC0000000, float 0xBFFED86740000000, float 0x3FEEDD8DC0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEEB911A0000000, float 0x3FED7F30C0000000, float 0xBFFEB911A0000000, float 0xBFFEB28AE0000000, float 0x3FEEB911A0000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEEAC9940000000, float 0x3FED6743E0000000, float 0xBFFEAC9940000000, float 0xBFFEA590A0000000, float 0x3FEEAC9940000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00], [8 x float] [float 0x3FEE457140000000, float 0x3FECA2CF80000000, float 0xBFFE457140000000, float 0xBFFE397AC0000000, float 0x3FEE457140000000, float 0.000000e+00, float 0.000000e+00, float 0.000000e+00]], align 16

; Function Attrs: noinline nounwind optnone
define hidden i32 @InitGainAnalysis(%struct.replaygain_data* %rgData, i32 %samplefreq) #0 {
entry:
  %retval = alloca i32, align 4
  %rgData.addr = alloca %struct.replaygain_data*, align 4
  %samplefreq.addr = alloca i32, align 4
  store %struct.replaygain_data* %rgData, %struct.replaygain_data** %rgData.addr, align 4
  store i32 %samplefreq, i32* %samplefreq.addr, align 4
  %0 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %1 = load i32, i32* %samplefreq.addr, align 4
  %call = call i32 @ResetSampleFrequency(%struct.replaygain_data* %0, i32 %1)
  %cmp = icmp ne i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %2, i32 0, i32 0
  %arraydecay = getelementptr inbounds [20 x float], [20 x float]* %linprebuf, i32 0, i32 0
  %add.ptr = getelementptr inbounds float, float* %arraydecay, i32 10
  %3 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linpre = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %3, i32 0, i32 1
  store float* %add.ptr, float** %linpre, align 8
  %4 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %4, i32 0, i32 6
  %arraydecay1 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf, i32 0, i32 0
  %add.ptr2 = getelementptr inbounds float, float* %arraydecay1, i32 10
  %5 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinpre = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %5, i32 0, i32 7
  store float* %add.ptr2, float** %rinpre, align 4
  %6 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %6, i32 0, i32 2
  %arraydecay3 = getelementptr inbounds [2411 x float], [2411 x float]* %lstepbuf, i32 0, i32 0
  %add.ptr4 = getelementptr inbounds float, float* %arraydecay3, i32 10
  %7 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstep = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %7, i32 0, i32 3
  store float* %add.ptr4, float** %lstep, align 8
  %8 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %8, i32 0, i32 8
  %arraydecay5 = getelementptr inbounds [2411 x float], [2411 x float]* %rstepbuf, i32 0, i32 0
  %add.ptr6 = getelementptr inbounds float, float* %arraydecay5, i32 10
  %9 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstep = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %9, i32 0, i32 9
  store float* %add.ptr6, float** %rstep, align 4
  %10 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %loutbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %10, i32 0, i32 4
  %arraydecay7 = getelementptr inbounds [2411 x float], [2411 x float]* %loutbuf, i32 0, i32 0
  %add.ptr8 = getelementptr inbounds float, float* %arraydecay7, i32 10
  %11 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lout = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %11, i32 0, i32 5
  store float* %add.ptr8, float** %lout, align 8
  %12 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %routbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %12, i32 0, i32 10
  %arraydecay9 = getelementptr inbounds [2411 x float], [2411 x float]* %routbuf, i32 0, i32 0
  %add.ptr10 = getelementptr inbounds float, float* %arraydecay9, i32 10
  %13 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rout = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %13, i32 0, i32 11
  store float* %add.ptr10, float** %rout, align 4
  %14 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %B = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %14, i32 0, i32 19
  %arraydecay11 = getelementptr inbounds [12000 x i32], [12000 x i32]* %B, i32 0, i32 0
  %15 = bitcast i32* %arraydecay11 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %15, i8 0, i32 48000, i1 false)
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %16 = load i32, i32* %retval, align 4
  ret i32 %16
}

; Function Attrs: noinline nounwind optnone
define internal i32 @ResetSampleFrequency(%struct.replaygain_data* %rgData, i32 %samplefreq) #0 {
entry:
  %retval = alloca i32, align 4
  %rgData.addr = alloca %struct.replaygain_data*, align 4
  %samplefreq.addr = alloca i32, align 4
  store %struct.replaygain_data* %rgData, %struct.replaygain_data** %rgData.addr, align 4
  store i32 %samplefreq, i32* %samplefreq.addr, align 4
  %0 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %0, i32 0, i32 0
  %arraydecay = getelementptr inbounds [20 x float], [20 x float]* %linprebuf, i32 0, i32 0
  %1 = bitcast float* %arraydecay to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %1, i8 0, i32 40, i1 false)
  %2 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %2, i32 0, i32 6
  %arraydecay1 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf, i32 0, i32 0
  %3 = bitcast float* %arraydecay1 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %3, i8 0, i32 40, i1 false)
  %4 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %4, i32 0, i32 2
  %arraydecay2 = getelementptr inbounds [2411 x float], [2411 x float]* %lstepbuf, i32 0, i32 0
  %5 = bitcast float* %arraydecay2 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %5, i8 0, i32 40, i1 false)
  %6 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %6, i32 0, i32 8
  %arraydecay3 = getelementptr inbounds [2411 x float], [2411 x float]* %rstepbuf, i32 0, i32 0
  %7 = bitcast float* %arraydecay3 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %7, i8 0, i32 40, i1 false)
  %8 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %loutbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %8, i32 0, i32 4
  %arraydecay4 = getelementptr inbounds [2411 x float], [2411 x float]* %loutbuf, i32 0, i32 0
  %9 = bitcast float* %arraydecay4 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %9, i8 0, i32 40, i1 false)
  %10 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %routbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %10, i32 0, i32 10
  %arraydecay5 = getelementptr inbounds [2411 x float], [2411 x float]* %routbuf, i32 0, i32 0
  %11 = bitcast float* %arraydecay5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %11, i8 0, i32 40, i1 false)
  %12 = load i32, i32* %samplefreq.addr, align 4
  switch i32 %12, label %sw.default [
    i32 48000, label %sw.bb
    i32 44100, label %sw.bb6
    i32 32000, label %sw.bb8
    i32 24000, label %sw.bb10
    i32 22050, label %sw.bb12
    i32 16000, label %sw.bb14
    i32 12000, label %sw.bb16
    i32 11025, label %sw.bb18
    i32 8000, label %sw.bb20
  ]

sw.bb:                                            ; preds = %entry
  %13 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %13, i32 0, i32 16
  store i32 0, i32* %freqindex, align 8
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  %14 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex7 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %14, i32 0, i32 16
  store i32 1, i32* %freqindex7, align 8
  br label %sw.epilog

sw.bb8:                                           ; preds = %entry
  %15 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex9 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %15, i32 0, i32 16
  store i32 2, i32* %freqindex9, align 8
  br label %sw.epilog

sw.bb10:                                          ; preds = %entry
  %16 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex11 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %16, i32 0, i32 16
  store i32 3, i32* %freqindex11, align 8
  br label %sw.epilog

sw.bb12:                                          ; preds = %entry
  %17 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex13 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %17, i32 0, i32 16
  store i32 4, i32* %freqindex13, align 8
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %18 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex15 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %18, i32 0, i32 16
  store i32 5, i32* %freqindex15, align 8
  br label %sw.epilog

sw.bb16:                                          ; preds = %entry
  %19 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex17 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %19, i32 0, i32 16
  store i32 6, i32* %freqindex17, align 8
  br label %sw.epilog

sw.bb18:                                          ; preds = %entry
  %20 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex19 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %20, i32 0, i32 16
  store i32 7, i32* %freqindex19, align 8
  br label %sw.epilog

sw.bb20:                                          ; preds = %entry
  %21 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex21 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %21, i32 0, i32 16
  store i32 8, i32* %freqindex21, align 8
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %sw.bb20, %sw.bb18, %sw.bb16, %sw.bb14, %sw.bb12, %sw.bb10, %sw.bb8, %sw.bb6, %sw.bb
  %22 = load i32, i32* %samplefreq.addr, align 4
  %mul = mul nsw i32 %22, 1
  %add = add nsw i32 %mul, 20
  %sub = sub nsw i32 %add, 1
  %div = sdiv i32 %sub, 20
  %23 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %sampleWindow = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %23, i32 0, i32 12
  store i32 %div, i32* %sampleWindow, align 8
  %24 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %24, i32 0, i32 14
  store double 0.000000e+00, double* %lsum, align 8
  %25 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %25, i32 0, i32 15
  store double 0.000000e+00, double* %rsum, align 8
  %26 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %26, i32 0, i32 13
  store i32 0, i32* %totsamp, align 4
  %27 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %A = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %27, i32 0, i32 18
  %arraydecay22 = getelementptr inbounds [12000 x i32], [12000 x i32]* %A, i32 0, i32 0
  %28 = bitcast i32* %arraydecay22 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 8 %28, i8 0, i32 48000, i1 false)
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %sw.epilog, %sw.default
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #1

; Function Attrs: noinline nounwind optnone
define hidden i32 @AnalyzeSamples(%struct.replaygain_data* %rgData, float* %left_samples, float* %right_samples, i32 %num_samples, i32 %num_channels) #0 {
entry:
  %retval = alloca i32, align 4
  %rgData.addr = alloca %struct.replaygain_data*, align 4
  %left_samples.addr = alloca float*, align 4
  %right_samples.addr = alloca float*, align 4
  %num_samples.addr = alloca i32, align 4
  %num_channels.addr = alloca i32, align 4
  %curleft = alloca float*, align 4
  %curright = alloca float*, align 4
  %batchsamples = alloca i32, align 4
  %cursamples = alloca i32, align 4
  %cursamplepos = alloca i32, align 4
  %i = alloca i32, align 4
  %sum_l = alloca float, align 4
  %sum_r = alloca float, align 4
  %l = alloca float, align 4
  %r = alloca float, align 4
  %l0 = alloca float, align 4
  %l1 = alloca float, align 4
  %l2 = alloca float, align 4
  %l3 = alloca float, align 4
  %sl = alloca float, align 4
  %r0 = alloca float, align 4
  %r1 = alloca float, align 4
  %r2 = alloca float, align 4
  %r3 = alloca float, align 4
  %sr = alloca float, align 4
  %val = alloca double, align 8
  %ival = alloca i32, align 4
  store %struct.replaygain_data* %rgData, %struct.replaygain_data** %rgData.addr, align 4
  store float* %left_samples, float** %left_samples.addr, align 4
  store float* %right_samples, float** %right_samples.addr, align 4
  store i32 %num_samples, i32* %num_samples.addr, align 4
  store i32 %num_channels, i32* %num_channels.addr, align 4
  %0 = load i32, i32* %num_samples.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i32 0, i32* %cursamplepos, align 4
  %1 = load i32, i32* %num_samples.addr, align 4
  store i32 %1, i32* %batchsamples, align 4
  %2 = load i32, i32* %num_channels.addr, align 4
  switch i32 %2, label %sw.default [
    i32 1, label %sw.bb
    i32 2, label %sw.bb1
  ]

sw.bb:                                            ; preds = %if.end
  %3 = load float*, float** %left_samples.addr, align 4
  store float* %3, float** %right_samples.addr, align 4
  br label %sw.epilog

sw.bb1:                                           ; preds = %if.end
  br label %sw.epilog

sw.default:                                       ; preds = %if.end
  store i32 0, i32* %retval, align 4
  br label %return

sw.epilog:                                        ; preds = %sw.bb1, %sw.bb
  %4 = load i32, i32* %num_samples.addr, align 4
  %cmp2 = icmp ult i32 %4, 10
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %sw.epilog
  %5 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %5, i32 0, i32 0
  %arraydecay = getelementptr inbounds [20 x float], [20 x float]* %linprebuf, i32 0, i32 0
  %add.ptr = getelementptr inbounds float, float* %arraydecay, i32 10
  %6 = bitcast float* %add.ptr to i8*
  %7 = load float*, float** %left_samples.addr, align 4
  %8 = bitcast float* %7 to i8*
  %9 = load i32, i32* %num_samples.addr, align 4
  %mul = mul i32 %9, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %8, i32 %mul, i1 false)
  %10 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %10, i32 0, i32 6
  %arraydecay4 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf, i32 0, i32 0
  %add.ptr5 = getelementptr inbounds float, float* %arraydecay4, i32 10
  %11 = bitcast float* %add.ptr5 to i8*
  %12 = load float*, float** %right_samples.addr, align 4
  %13 = bitcast float* %12 to i8*
  %14 = load i32, i32* %num_samples.addr, align 4
  %mul6 = mul i32 %14, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %13, i32 %mul6, i1 false)
  br label %if.end13

if.else:                                          ; preds = %sw.epilog
  %15 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf7 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %15, i32 0, i32 0
  %arraydecay8 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf7, i32 0, i32 0
  %add.ptr9 = getelementptr inbounds float, float* %arraydecay8, i32 10
  %16 = bitcast float* %add.ptr9 to i8*
  %17 = load float*, float** %left_samples.addr, align 4
  %18 = bitcast float* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %18, i32 40, i1 false)
  %19 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf10 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %19, i32 0, i32 6
  %arraydecay11 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf10, i32 0, i32 0
  %add.ptr12 = getelementptr inbounds float, float* %arraydecay11, i32 10
  %20 = bitcast float* %add.ptr12 to i8*
  %21 = load float*, float** %right_samples.addr, align 4
  %22 = bitcast float* %21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %22, i32 40, i1 false)
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then3
  br label %while.cond

while.cond:                                       ; preds = %if.end169, %if.end13
  %23 = load i32, i32* %batchsamples, align 4
  %cmp14 = icmp sgt i32 %23, 0
  br i1 %cmp14, label %while.body, label %while.end170

while.body:                                       ; preds = %while.cond
  %24 = load i32, i32* %batchsamples, align 4
  %25 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %sampleWindow = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %25, i32 0, i32 12
  %26 = load i32, i32* %sampleWindow, align 8
  %27 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %27, i32 0, i32 13
  %28 = load i32, i32* %totsamp, align 4
  %sub = sub nsw i32 %26, %28
  %cmp15 = icmp sgt i32 %24, %sub
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.body
  %29 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %sampleWindow16 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %29, i32 0, i32 12
  %30 = load i32, i32* %sampleWindow16, align 8
  %31 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp17 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %31, i32 0, i32 13
  %32 = load i32, i32* %totsamp17, align 4
  %sub18 = sub nsw i32 %30, %32
  br label %cond.end

cond.false:                                       ; preds = %while.body
  %33 = load i32, i32* %batchsamples, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub18, %cond.true ], [ %33, %cond.false ]
  store i32 %cond, i32* %cursamples, align 4
  %34 = load i32, i32* %cursamplepos, align 4
  %cmp19 = icmp slt i32 %34, 10
  br i1 %cmp19, label %if.then20, label %if.else28

if.then20:                                        ; preds = %cond.end
  %35 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linpre = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %35, i32 0, i32 1
  %36 = load float*, float** %linpre, align 8
  %37 = load i32, i32* %cursamplepos, align 4
  %add.ptr21 = getelementptr inbounds float, float* %36, i32 %37
  store float* %add.ptr21, float** %curleft, align 4
  %38 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinpre = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %38, i32 0, i32 7
  %39 = load float*, float** %rinpre, align 4
  %40 = load i32, i32* %cursamplepos, align 4
  %add.ptr22 = getelementptr inbounds float, float* %39, i32 %40
  store float* %add.ptr22, float** %curright, align 4
  %41 = load i32, i32* %cursamples, align 4
  %42 = load i32, i32* %cursamplepos, align 4
  %sub23 = sub nsw i32 10, %42
  %cmp24 = icmp sgt i32 %41, %sub23
  br i1 %cmp24, label %if.then25, label %if.end27

if.then25:                                        ; preds = %if.then20
  %43 = load i32, i32* %cursamplepos, align 4
  %sub26 = sub nsw i32 10, %43
  store i32 %sub26, i32* %cursamples, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then25, %if.then20
  br label %if.end31

if.else28:                                        ; preds = %cond.end
  %44 = load float*, float** %left_samples.addr, align 4
  %45 = load i32, i32* %cursamplepos, align 4
  %add.ptr29 = getelementptr inbounds float, float* %44, i32 %45
  store float* %add.ptr29, float** %curleft, align 4
  %46 = load float*, float** %right_samples.addr, align 4
  %47 = load i32, i32* %cursamplepos, align 4
  %add.ptr30 = getelementptr inbounds float, float* %46, i32 %47
  store float* %add.ptr30, float** %curright, align 4
  br label %if.end31

if.end31:                                         ; preds = %if.else28, %if.end27
  %48 = load float*, float** %curleft, align 4
  %49 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstep = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %49, i32 0, i32 3
  %50 = load float*, float** %lstep, align 8
  %51 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp32 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %51, i32 0, i32 13
  %52 = load i32, i32* %totsamp32, align 4
  %add.ptr33 = getelementptr inbounds float, float* %50, i32 %52
  %53 = load i32, i32* %cursamples, align 4
  %54 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %54, i32 0, i32 16
  %55 = load i32, i32* %freqindex, align 8
  %arrayidx = getelementptr inbounds [9 x [24 x float]], [9 x [24 x float]]* @ABYule, i32 0, i32 %55
  %arraydecay34 = getelementptr inbounds [24 x float], [24 x float]* %arrayidx, i32 0, i32 0
  call void @filterYule(float* %48, float* %add.ptr33, i32 %53, float* %arraydecay34)
  %56 = load float*, float** %curright, align 4
  %57 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstep = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %57, i32 0, i32 9
  %58 = load float*, float** %rstep, align 4
  %59 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp35 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %59, i32 0, i32 13
  %60 = load i32, i32* %totsamp35, align 4
  %add.ptr36 = getelementptr inbounds float, float* %58, i32 %60
  %61 = load i32, i32* %cursamples, align 4
  %62 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex37 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %62, i32 0, i32 16
  %63 = load i32, i32* %freqindex37, align 8
  %arrayidx38 = getelementptr inbounds [9 x [24 x float]], [9 x [24 x float]]* @ABYule, i32 0, i32 %63
  %arraydecay39 = getelementptr inbounds [24 x float], [24 x float]* %arrayidx38, i32 0, i32 0
  call void @filterYule(float* %56, float* %add.ptr36, i32 %61, float* %arraydecay39)
  %64 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstep40 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %64, i32 0, i32 3
  %65 = load float*, float** %lstep40, align 8
  %66 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp41 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %66, i32 0, i32 13
  %67 = load i32, i32* %totsamp41, align 4
  %add.ptr42 = getelementptr inbounds float, float* %65, i32 %67
  %68 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lout = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %68, i32 0, i32 5
  %69 = load float*, float** %lout, align 8
  %70 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp43 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %70, i32 0, i32 13
  %71 = load i32, i32* %totsamp43, align 4
  %add.ptr44 = getelementptr inbounds float, float* %69, i32 %71
  %72 = load i32, i32* %cursamples, align 4
  %73 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex45 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %73, i32 0, i32 16
  %74 = load i32, i32* %freqindex45, align 8
  %arrayidx46 = getelementptr inbounds [9 x [8 x float]], [9 x [8 x float]]* @ABButter, i32 0, i32 %74
  %arraydecay47 = getelementptr inbounds [8 x float], [8 x float]* %arrayidx46, i32 0, i32 0
  call void @filterButter(float* %add.ptr42, float* %add.ptr44, i32 %72, float* %arraydecay47)
  %75 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstep48 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %75, i32 0, i32 9
  %76 = load float*, float** %rstep48, align 4
  %77 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp49 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %77, i32 0, i32 13
  %78 = load i32, i32* %totsamp49, align 4
  %add.ptr50 = getelementptr inbounds float, float* %76, i32 %78
  %79 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rout = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %79, i32 0, i32 11
  %80 = load float*, float** %rout, align 4
  %81 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp51 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %81, i32 0, i32 13
  %82 = load i32, i32* %totsamp51, align 4
  %add.ptr52 = getelementptr inbounds float, float* %80, i32 %82
  %83 = load i32, i32* %cursamples, align 4
  %84 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %freqindex53 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %84, i32 0, i32 16
  %85 = load i32, i32* %freqindex53, align 8
  %arrayidx54 = getelementptr inbounds [9 x [8 x float]], [9 x [8 x float]]* @ABButter, i32 0, i32 %85
  %arraydecay55 = getelementptr inbounds [8 x float], [8 x float]* %arrayidx54, i32 0, i32 0
  call void @filterButter(float* %add.ptr50, float* %add.ptr52, i32 %83, float* %arraydecay55)
  %86 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lout56 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %86, i32 0, i32 5
  %87 = load float*, float** %lout56, align 8
  %88 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp57 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %88, i32 0, i32 13
  %89 = load i32, i32* %totsamp57, align 4
  %add.ptr58 = getelementptr inbounds float, float* %87, i32 %89
  store float* %add.ptr58, float** %curleft, align 4
  %90 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rout59 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %90, i32 0, i32 11
  %91 = load float*, float** %rout59, align 4
  %92 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp60 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %92, i32 0, i32 13
  %93 = load i32, i32* %totsamp60, align 4
  %add.ptr61 = getelementptr inbounds float, float* %91, i32 %93
  store float* %add.ptr61, float** %curright, align 4
  store float 0.000000e+00, float* %sum_l, align 4
  store float 0.000000e+00, float* %sum_r, align 4
  %94 = load i32, i32* %cursamples, align 4
  %and = and i32 %94, 3
  store i32 %and, i32* %i, align 4
  br label %while.cond62

while.cond62:                                     ; preds = %while.body63, %if.end31
  %95 = load i32, i32* %i, align 4
  %dec = add nsw i32 %95, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %95, 0
  br i1 %tobool, label %while.body63, label %while.end

while.body63:                                     ; preds = %while.cond62
  %96 = load float*, float** %curleft, align 4
  %incdec.ptr = getelementptr inbounds float, float* %96, i32 1
  store float* %incdec.ptr, float** %curleft, align 4
  %97 = load float, float* %96, align 4
  store float %97, float* %l, align 4
  %98 = load float*, float** %curright, align 4
  %incdec.ptr64 = getelementptr inbounds float, float* %98, i32 1
  store float* %incdec.ptr64, float** %curright, align 4
  %99 = load float, float* %98, align 4
  store float %99, float* %r, align 4
  %100 = load float, float* %l, align 4
  %101 = load float, float* %l, align 4
  %mul65 = fmul float %100, %101
  %102 = load float, float* %sum_l, align 4
  %add = fadd float %102, %mul65
  store float %add, float* %sum_l, align 4
  %103 = load float, float* %r, align 4
  %104 = load float, float* %r, align 4
  %mul66 = fmul float %103, %104
  %105 = load float, float* %sum_r, align 4
  %add67 = fadd float %105, %mul66
  store float %add67, float* %sum_r, align 4
  br label %while.cond62

while.end:                                        ; preds = %while.cond62
  %106 = load i32, i32* %cursamples, align 4
  %div = sdiv i32 %106, 4
  store i32 %div, i32* %i, align 4
  br label %while.cond68

while.cond68:                                     ; preds = %while.body71, %while.end
  %107 = load i32, i32* %i, align 4
  %dec69 = add nsw i32 %107, -1
  store i32 %dec69, i32* %i, align 4
  %tobool70 = icmp ne i32 %107, 0
  br i1 %tobool70, label %while.body71, label %while.end106

while.body71:                                     ; preds = %while.cond68
  %108 = load float*, float** %curleft, align 4
  %arrayidx72 = getelementptr inbounds float, float* %108, i32 0
  %109 = load float, float* %arrayidx72, align 4
  %110 = load float*, float** %curleft, align 4
  %arrayidx73 = getelementptr inbounds float, float* %110, i32 0
  %111 = load float, float* %arrayidx73, align 4
  %mul74 = fmul float %109, %111
  store float %mul74, float* %l0, align 4
  %112 = load float*, float** %curleft, align 4
  %arrayidx75 = getelementptr inbounds float, float* %112, i32 1
  %113 = load float, float* %arrayidx75, align 4
  %114 = load float*, float** %curleft, align 4
  %arrayidx76 = getelementptr inbounds float, float* %114, i32 1
  %115 = load float, float* %arrayidx76, align 4
  %mul77 = fmul float %113, %115
  store float %mul77, float* %l1, align 4
  %116 = load float*, float** %curleft, align 4
  %arrayidx78 = getelementptr inbounds float, float* %116, i32 2
  %117 = load float, float* %arrayidx78, align 4
  %118 = load float*, float** %curleft, align 4
  %arrayidx79 = getelementptr inbounds float, float* %118, i32 2
  %119 = load float, float* %arrayidx79, align 4
  %mul80 = fmul float %117, %119
  store float %mul80, float* %l2, align 4
  %120 = load float*, float** %curleft, align 4
  %arrayidx81 = getelementptr inbounds float, float* %120, i32 3
  %121 = load float, float* %arrayidx81, align 4
  %122 = load float*, float** %curleft, align 4
  %arrayidx82 = getelementptr inbounds float, float* %122, i32 3
  %123 = load float, float* %arrayidx82, align 4
  %mul83 = fmul float %121, %123
  store float %mul83, float* %l3, align 4
  %124 = load float, float* %l0, align 4
  %125 = load float, float* %l1, align 4
  %add84 = fadd float %124, %125
  %126 = load float, float* %l2, align 4
  %add85 = fadd float %add84, %126
  %127 = load float, float* %l3, align 4
  %add86 = fadd float %add85, %127
  store float %add86, float* %sl, align 4
  %128 = load float*, float** %curright, align 4
  %arrayidx87 = getelementptr inbounds float, float* %128, i32 0
  %129 = load float, float* %arrayidx87, align 4
  %130 = load float*, float** %curright, align 4
  %arrayidx88 = getelementptr inbounds float, float* %130, i32 0
  %131 = load float, float* %arrayidx88, align 4
  %mul89 = fmul float %129, %131
  store float %mul89, float* %r0, align 4
  %132 = load float*, float** %curright, align 4
  %arrayidx90 = getelementptr inbounds float, float* %132, i32 1
  %133 = load float, float* %arrayidx90, align 4
  %134 = load float*, float** %curright, align 4
  %arrayidx91 = getelementptr inbounds float, float* %134, i32 1
  %135 = load float, float* %arrayidx91, align 4
  %mul92 = fmul float %133, %135
  store float %mul92, float* %r1, align 4
  %136 = load float*, float** %curright, align 4
  %arrayidx93 = getelementptr inbounds float, float* %136, i32 2
  %137 = load float, float* %arrayidx93, align 4
  %138 = load float*, float** %curright, align 4
  %arrayidx94 = getelementptr inbounds float, float* %138, i32 2
  %139 = load float, float* %arrayidx94, align 4
  %mul95 = fmul float %137, %139
  store float %mul95, float* %r2, align 4
  %140 = load float*, float** %curright, align 4
  %arrayidx96 = getelementptr inbounds float, float* %140, i32 3
  %141 = load float, float* %arrayidx96, align 4
  %142 = load float*, float** %curright, align 4
  %arrayidx97 = getelementptr inbounds float, float* %142, i32 3
  %143 = load float, float* %arrayidx97, align 4
  %mul98 = fmul float %141, %143
  store float %mul98, float* %r3, align 4
  %144 = load float, float* %r0, align 4
  %145 = load float, float* %r1, align 4
  %add99 = fadd float %144, %145
  %146 = load float, float* %r2, align 4
  %add100 = fadd float %add99, %146
  %147 = load float, float* %r3, align 4
  %add101 = fadd float %add100, %147
  store float %add101, float* %sr, align 4
  %148 = load float, float* %sl, align 4
  %149 = load float, float* %sum_l, align 4
  %add102 = fadd float %149, %148
  store float %add102, float* %sum_l, align 4
  %150 = load float*, float** %curleft, align 4
  %add.ptr103 = getelementptr inbounds float, float* %150, i32 4
  store float* %add.ptr103, float** %curleft, align 4
  %151 = load float, float* %sr, align 4
  %152 = load float, float* %sum_r, align 4
  %add104 = fadd float %152, %151
  store float %add104, float* %sum_r, align 4
  %153 = load float*, float** %curright, align 4
  %add.ptr105 = getelementptr inbounds float, float* %153, i32 4
  store float* %add.ptr105, float** %curright, align 4
  br label %while.cond68

while.end106:                                     ; preds = %while.cond68
  %154 = load float, float* %sum_l, align 4
  %conv = fpext float %154 to double
  %155 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %155, i32 0, i32 14
  %156 = load double, double* %lsum, align 8
  %add107 = fadd double %156, %conv
  store double %add107, double* %lsum, align 8
  %157 = load float, float* %sum_r, align 4
  %conv108 = fpext float %157 to double
  %158 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %158, i32 0, i32 15
  %159 = load double, double* %rsum, align 8
  %add109 = fadd double %159, %conv108
  store double %add109, double* %rsum, align 8
  %160 = load i32, i32* %cursamples, align 4
  %161 = load i32, i32* %batchsamples, align 4
  %sub110 = sub nsw i32 %161, %160
  store i32 %sub110, i32* %batchsamples, align 4
  %162 = load i32, i32* %cursamples, align 4
  %163 = load i32, i32* %cursamplepos, align 4
  %add111 = add nsw i32 %163, %162
  store i32 %add111, i32* %cursamplepos, align 4
  %164 = load i32, i32* %cursamples, align 4
  %165 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp112 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %165, i32 0, i32 13
  %166 = load i32, i32* %totsamp112, align 4
  %add113 = add nsw i32 %166, %164
  store i32 %add113, i32* %totsamp112, align 4
  %167 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp114 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %167, i32 0, i32 13
  %168 = load i32, i32* %totsamp114, align 4
  %169 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %sampleWindow115 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %169, i32 0, i32 12
  %170 = load i32, i32* %sampleWindow115, align 8
  %cmp116 = icmp eq i32 %168, %170
  br i1 %cmp116, label %if.then118, label %if.end163

if.then118:                                       ; preds = %while.end106
  %171 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lsum119 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %171, i32 0, i32 14
  %172 = load double, double* %lsum119, align 8
  %173 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rsum120 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %173, i32 0, i32 15
  %174 = load double, double* %rsum120, align 8
  %add121 = fadd double %172, %174
  %175 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp122 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %175, i32 0, i32 13
  %176 = load i32, i32* %totsamp122, align 4
  %conv123 = sitofp i32 %176 to double
  %div124 = fdiv double %add121, %conv123
  %mul125 = fmul double %div124, 5.000000e-01
  %add126 = fadd double %mul125, 1.000000e-37
  %177 = call double @llvm.log10.f64(double %add126)
  %mul127 = fmul double 1.000000e+03, %177
  store double %mul127, double* %val, align 8
  %178 = load double, double* %val, align 8
  %cmp128 = fcmp ole double %178, 0.000000e+00
  br i1 %cmp128, label %cond.true130, label %cond.false131

cond.true130:                                     ; preds = %if.then118
  br label %cond.end133

cond.false131:                                    ; preds = %if.then118
  %179 = load double, double* %val, align 8
  %conv132 = fptoui double %179 to i32
  br label %cond.end133

cond.end133:                                      ; preds = %cond.false131, %cond.true130
  %cond134 = phi i32 [ 0, %cond.true130 ], [ %conv132, %cond.false131 ]
  store i32 %cond134, i32* %ival, align 4
  %180 = load i32, i32* %ival, align 4
  %cmp135 = icmp uge i32 %180, 12000
  br i1 %cmp135, label %if.then137, label %if.end138

if.then137:                                       ; preds = %cond.end133
  store i32 11999, i32* %ival, align 4
  br label %if.end138

if.end138:                                        ; preds = %if.then137, %cond.end133
  %181 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %A = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %181, i32 0, i32 18
  %182 = load i32, i32* %ival, align 4
  %arrayidx139 = getelementptr inbounds [12000 x i32], [12000 x i32]* %A, i32 0, i32 %182
  %183 = load i32, i32* %arrayidx139, align 4
  %inc = add i32 %183, 1
  store i32 %inc, i32* %arrayidx139, align 4
  %184 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rsum140 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %184, i32 0, i32 15
  store double 0.000000e+00, double* %rsum140, align 8
  %185 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lsum141 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %185, i32 0, i32 14
  store double 0.000000e+00, double* %lsum141, align 8
  %186 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %loutbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %186, i32 0, i32 4
  %arraydecay142 = getelementptr inbounds [2411 x float], [2411 x float]* %loutbuf, i32 0, i32 0
  %187 = bitcast float* %arraydecay142 to i8*
  %188 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %loutbuf143 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %188, i32 0, i32 4
  %arraydecay144 = getelementptr inbounds [2411 x float], [2411 x float]* %loutbuf143, i32 0, i32 0
  %189 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp145 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %189, i32 0, i32 13
  %190 = load i32, i32* %totsamp145, align 4
  %add.ptr146 = getelementptr inbounds float, float* %arraydecay144, i32 %190
  %191 = bitcast float* %add.ptr146 to i8*
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %187, i8* align 4 %191, i32 40, i1 false)
  %192 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %routbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %192, i32 0, i32 10
  %arraydecay147 = getelementptr inbounds [2411 x float], [2411 x float]* %routbuf, i32 0, i32 0
  %193 = bitcast float* %arraydecay147 to i8*
  %194 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %routbuf148 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %194, i32 0, i32 10
  %arraydecay149 = getelementptr inbounds [2411 x float], [2411 x float]* %routbuf148, i32 0, i32 0
  %195 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp150 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %195, i32 0, i32 13
  %196 = load i32, i32* %totsamp150, align 4
  %add.ptr151 = getelementptr inbounds float, float* %arraydecay149, i32 %196
  %197 = bitcast float* %add.ptr151 to i8*
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 8 %193, i8* align 4 %197, i32 40, i1 false)
  %198 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %198, i32 0, i32 2
  %arraydecay152 = getelementptr inbounds [2411 x float], [2411 x float]* %lstepbuf, i32 0, i32 0
  %199 = bitcast float* %arraydecay152 to i8*
  %200 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstepbuf153 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %200, i32 0, i32 2
  %arraydecay154 = getelementptr inbounds [2411 x float], [2411 x float]* %lstepbuf153, i32 0, i32 0
  %201 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp155 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %201, i32 0, i32 13
  %202 = load i32, i32* %totsamp155, align 4
  %add.ptr156 = getelementptr inbounds float, float* %arraydecay154, i32 %202
  %203 = bitcast float* %add.ptr156 to i8*
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %199, i8* align 4 %203, i32 40, i1 false)
  %204 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %204, i32 0, i32 8
  %arraydecay157 = getelementptr inbounds [2411 x float], [2411 x float]* %rstepbuf, i32 0, i32 0
  %205 = bitcast float* %arraydecay157 to i8*
  %206 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstepbuf158 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %206, i32 0, i32 8
  %arraydecay159 = getelementptr inbounds [2411 x float], [2411 x float]* %rstepbuf158, i32 0, i32 0
  %207 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp160 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %207, i32 0, i32 13
  %208 = load i32, i32* %totsamp160, align 4
  %add.ptr161 = getelementptr inbounds float, float* %arraydecay159, i32 %208
  %209 = bitcast float* %add.ptr161 to i8*
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 8 %205, i8* align 4 %209, i32 40, i1 false)
  %210 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp162 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %210, i32 0, i32 13
  store i32 0, i32* %totsamp162, align 4
  br label %if.end163

if.end163:                                        ; preds = %if.end138, %while.end106
  %211 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp164 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %211, i32 0, i32 13
  %212 = load i32, i32* %totsamp164, align 4
  %213 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %sampleWindow165 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %213, i32 0, i32 12
  %214 = load i32, i32* %sampleWindow165, align 8
  %cmp166 = icmp sgt i32 %212, %214
  br i1 %cmp166, label %if.then168, label %if.end169

if.then168:                                       ; preds = %if.end163
  store i32 0, i32* %retval, align 4
  br label %return

if.end169:                                        ; preds = %if.end163
  br label %while.cond

while.end170:                                     ; preds = %while.cond
  %215 = load i32, i32* %num_samples.addr, align 4
  %cmp171 = icmp ult i32 %215, 10
  br i1 %cmp171, label %if.then173, label %if.else199

if.then173:                                       ; preds = %while.end170
  %216 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf174 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %216, i32 0, i32 0
  %arraydecay175 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf174, i32 0, i32 0
  %217 = bitcast float* %arraydecay175 to i8*
  %218 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf176 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %218, i32 0, i32 0
  %arraydecay177 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf176, i32 0, i32 0
  %219 = load i32, i32* %num_samples.addr, align 4
  %add.ptr178 = getelementptr inbounds float, float* %arraydecay177, i32 %219
  %220 = bitcast float* %add.ptr178 to i8*
  %221 = load i32, i32* %num_samples.addr, align 4
  %sub179 = sub i32 10, %221
  %mul180 = mul i32 %sub179, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 8 %217, i8* align 4 %220, i32 %mul180, i1 false)
  %222 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf181 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %222, i32 0, i32 6
  %arraydecay182 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf181, i32 0, i32 0
  %223 = bitcast float* %arraydecay182 to i8*
  %224 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf183 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %224, i32 0, i32 6
  %arraydecay184 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf183, i32 0, i32 0
  %225 = load i32, i32* %num_samples.addr, align 4
  %add.ptr185 = getelementptr inbounds float, float* %arraydecay184, i32 %225
  %226 = bitcast float* %add.ptr185 to i8*
  %227 = load i32, i32* %num_samples.addr, align 4
  %sub186 = sub i32 10, %227
  %mul187 = mul i32 %sub186, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %223, i8* align 4 %226, i32 %mul187, i1 false)
  %228 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf188 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %228, i32 0, i32 0
  %arraydecay189 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf188, i32 0, i32 0
  %add.ptr190 = getelementptr inbounds float, float* %arraydecay189, i32 10
  %229 = load i32, i32* %num_samples.addr, align 4
  %idx.neg = sub i32 0, %229
  %add.ptr191 = getelementptr inbounds float, float* %add.ptr190, i32 %idx.neg
  %230 = bitcast float* %add.ptr191 to i8*
  %231 = load float*, float** %left_samples.addr, align 4
  %232 = bitcast float* %231 to i8*
  %233 = load i32, i32* %num_samples.addr, align 4
  %mul192 = mul i32 %233, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %230, i8* align 4 %232, i32 %mul192, i1 false)
  %234 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf193 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %234, i32 0, i32 6
  %arraydecay194 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf193, i32 0, i32 0
  %add.ptr195 = getelementptr inbounds float, float* %arraydecay194, i32 10
  %235 = load i32, i32* %num_samples.addr, align 4
  %idx.neg196 = sub i32 0, %235
  %add.ptr197 = getelementptr inbounds float, float* %add.ptr195, i32 %idx.neg196
  %236 = bitcast float* %add.ptr197 to i8*
  %237 = load float*, float** %right_samples.addr, align 4
  %238 = bitcast float* %237 to i8*
  %239 = load i32, i32* %num_samples.addr, align 4
  %mul198 = mul i32 %239, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %236, i8* align 4 %238, i32 %mul198, i1 false)
  br label %if.end208

if.else199:                                       ; preds = %while.end170
  %240 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf200 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %240, i32 0, i32 0
  %arraydecay201 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf200, i32 0, i32 0
  %241 = bitcast float* %arraydecay201 to i8*
  %242 = load float*, float** %left_samples.addr, align 4
  %243 = load i32, i32* %num_samples.addr, align 4
  %add.ptr202 = getelementptr inbounds float, float* %242, i32 %243
  %add.ptr203 = getelementptr inbounds float, float* %add.ptr202, i32 -10
  %244 = bitcast float* %add.ptr203 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 8 %241, i8* align 4 %244, i32 40, i1 false)
  %245 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf204 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %245, i32 0, i32 6
  %arraydecay205 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf204, i32 0, i32 0
  %246 = bitcast float* %arraydecay205 to i8*
  %247 = load float*, float** %right_samples.addr, align 4
  %248 = load i32, i32* %num_samples.addr, align 4
  %add.ptr206 = getelementptr inbounds float, float* %247, i32 %248
  %add.ptr207 = getelementptr inbounds float, float* %add.ptr206, i32 -10
  %249 = bitcast float* %add.ptr207 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %246, i8* align 4 %249, i32 40, i1 false)
  br label %if.end208

if.end208:                                        ; preds = %if.else199, %if.then173
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end208, %if.then168, %sw.default, %if.then
  %250 = load i32, i32* %retval, align 4
  ret i32 %250
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define internal void @filterYule(float* %input, float* %output, i32 %nSamples, float* %kernel) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %nSamples.addr = alloca i32, align 4
  %kernel.addr = alloca float*, align 4
  %y0 = alloca float, align 4
  %y2 = alloca float, align 4
  %y4 = alloca float, align 4
  %y6 = alloca float, align 4
  %s00 = alloca float, align 4
  %y8 = alloca float, align 4
  %yA = alloca float, align 4
  %yC = alloca float, align 4
  %yE = alloca float, align 4
  %s01 = alloca float, align 4
  %yG = alloca float, align 4
  %yK = alloca float, align 4
  %s1 = alloca float, align 4
  %x1 = alloca float, align 4
  %x5 = alloca float, align 4
  %x9 = alloca float, align 4
  %xD = alloca float, align 4
  %xH = alloca float, align 4
  %s2 = alloca float, align 4
  store float* %input, float** %input.addr, align 4
  store float* %output, float** %output.addr, align 4
  store i32 %nSamples, i32* %nSamples.addr, align 4
  store float* %kernel, float** %kernel.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %nSamples.addr, align 4
  %dec = add i32 %0, -1
  store i32 %dec, i32* %nSamples.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load float*, float** %input.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 -10
  %2 = load float, float* %arrayidx, align 4
  %3 = load float*, float** %kernel.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %3, i32 0
  %4 = load float, float* %arrayidx1, align 4
  %mul = fmul float %2, %4
  store float %mul, float* %y0, align 4
  %5 = load float*, float** %input.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %5, i32 -9
  %6 = load float, float* %arrayidx2, align 4
  %7 = load float*, float** %kernel.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 1
  %8 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %6, %8
  store float %mul4, float* %y2, align 4
  %9 = load float*, float** %input.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %9, i32 -8
  %10 = load float, float* %arrayidx5, align 4
  %11 = load float*, float** %kernel.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %11, i32 2
  %12 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %10, %12
  store float %mul7, float* %y4, align 4
  %13 = load float*, float** %input.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %13, i32 -7
  %14 = load float, float* %arrayidx8, align 4
  %15 = load float*, float** %kernel.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %15, i32 3
  %16 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %14, %16
  store float %mul10, float* %y6, align 4
  %17 = load float, float* %y0, align 4
  %18 = load float, float* %y2, align 4
  %add = fadd float %17, %18
  %19 = load float, float* %y4, align 4
  %add11 = fadd float %add, %19
  %20 = load float, float* %y6, align 4
  %add12 = fadd float %add11, %20
  store float %add12, float* %s00, align 4
  %21 = load float*, float** %input.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %21, i32 -6
  %22 = load float, float* %arrayidx13, align 4
  %23 = load float*, float** %kernel.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %23, i32 4
  %24 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %22, %24
  store float %mul15, float* %y8, align 4
  %25 = load float*, float** %input.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %25, i32 -5
  %26 = load float, float* %arrayidx16, align 4
  %27 = load float*, float** %kernel.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %27, i32 5
  %28 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %26, %28
  store float %mul18, float* %yA, align 4
  %29 = load float*, float** %input.addr, align 4
  %arrayidx19 = getelementptr inbounds float, float* %29, i32 -4
  %30 = load float, float* %arrayidx19, align 4
  %31 = load float*, float** %kernel.addr, align 4
  %arrayidx20 = getelementptr inbounds float, float* %31, i32 6
  %32 = load float, float* %arrayidx20, align 4
  %mul21 = fmul float %30, %32
  store float %mul21, float* %yC, align 4
  %33 = load float*, float** %input.addr, align 4
  %arrayidx22 = getelementptr inbounds float, float* %33, i32 -3
  %34 = load float, float* %arrayidx22, align 4
  %35 = load float*, float** %kernel.addr, align 4
  %arrayidx23 = getelementptr inbounds float, float* %35, i32 7
  %36 = load float, float* %arrayidx23, align 4
  %mul24 = fmul float %34, %36
  store float %mul24, float* %yE, align 4
  %37 = load float, float* %y8, align 4
  %38 = load float, float* %yA, align 4
  %add25 = fadd float %37, %38
  %39 = load float, float* %yC, align 4
  %add26 = fadd float %add25, %39
  %40 = load float, float* %yE, align 4
  %add27 = fadd float %add26, %40
  store float %add27, float* %s01, align 4
  %41 = load float*, float** %input.addr, align 4
  %arrayidx28 = getelementptr inbounds float, float* %41, i32 -2
  %42 = load float, float* %arrayidx28, align 4
  %43 = load float*, float** %kernel.addr, align 4
  %arrayidx29 = getelementptr inbounds float, float* %43, i32 8
  %44 = load float, float* %arrayidx29, align 4
  %mul30 = fmul float %42, %44
  %45 = load float*, float** %input.addr, align 4
  %arrayidx31 = getelementptr inbounds float, float* %45, i32 -1
  %46 = load float, float* %arrayidx31, align 4
  %47 = load float*, float** %kernel.addr, align 4
  %arrayidx32 = getelementptr inbounds float, float* %47, i32 9
  %48 = load float, float* %arrayidx32, align 4
  %mul33 = fmul float %46, %48
  %add34 = fadd float %mul30, %mul33
  store float %add34, float* %yG, align 4
  %49 = load float*, float** %input.addr, align 4
  %arrayidx35 = getelementptr inbounds float, float* %49, i32 0
  %50 = load float, float* %arrayidx35, align 4
  %51 = load float*, float** %kernel.addr, align 4
  %arrayidx36 = getelementptr inbounds float, float* %51, i32 10
  %52 = load float, float* %arrayidx36, align 4
  %mul37 = fmul float %50, %52
  store float %mul37, float* %yK, align 4
  %53 = load float, float* %s00, align 4
  %54 = load float, float* %s01, align 4
  %add38 = fadd float %53, %54
  %55 = load float, float* %yG, align 4
  %add39 = fadd float %add38, %55
  %56 = load float, float* %yK, align 4
  %add40 = fadd float %add39, %56
  store float %add40, float* %s1, align 4
  %57 = load float*, float** %output.addr, align 4
  %arrayidx41 = getelementptr inbounds float, float* %57, i32 -10
  %58 = load float, float* %arrayidx41, align 4
  %59 = load float*, float** %kernel.addr, align 4
  %arrayidx42 = getelementptr inbounds float, float* %59, i32 11
  %60 = load float, float* %arrayidx42, align 4
  %mul43 = fmul float %58, %60
  %61 = load float*, float** %output.addr, align 4
  %arrayidx44 = getelementptr inbounds float, float* %61, i32 -9
  %62 = load float, float* %arrayidx44, align 4
  %63 = load float*, float** %kernel.addr, align 4
  %arrayidx45 = getelementptr inbounds float, float* %63, i32 12
  %64 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %62, %64
  %add47 = fadd float %mul43, %mul46
  store float %add47, float* %x1, align 4
  %65 = load float*, float** %output.addr, align 4
  %arrayidx48 = getelementptr inbounds float, float* %65, i32 -8
  %66 = load float, float* %arrayidx48, align 4
  %67 = load float*, float** %kernel.addr, align 4
  %arrayidx49 = getelementptr inbounds float, float* %67, i32 13
  %68 = load float, float* %arrayidx49, align 4
  %mul50 = fmul float %66, %68
  %69 = load float*, float** %output.addr, align 4
  %arrayidx51 = getelementptr inbounds float, float* %69, i32 -7
  %70 = load float, float* %arrayidx51, align 4
  %71 = load float*, float** %kernel.addr, align 4
  %arrayidx52 = getelementptr inbounds float, float* %71, i32 14
  %72 = load float, float* %arrayidx52, align 4
  %mul53 = fmul float %70, %72
  %add54 = fadd float %mul50, %mul53
  store float %add54, float* %x5, align 4
  %73 = load float*, float** %output.addr, align 4
  %arrayidx55 = getelementptr inbounds float, float* %73, i32 -6
  %74 = load float, float* %arrayidx55, align 4
  %75 = load float*, float** %kernel.addr, align 4
  %arrayidx56 = getelementptr inbounds float, float* %75, i32 15
  %76 = load float, float* %arrayidx56, align 4
  %mul57 = fmul float %74, %76
  %77 = load float*, float** %output.addr, align 4
  %arrayidx58 = getelementptr inbounds float, float* %77, i32 -5
  %78 = load float, float* %arrayidx58, align 4
  %79 = load float*, float** %kernel.addr, align 4
  %arrayidx59 = getelementptr inbounds float, float* %79, i32 16
  %80 = load float, float* %arrayidx59, align 4
  %mul60 = fmul float %78, %80
  %add61 = fadd float %mul57, %mul60
  store float %add61, float* %x9, align 4
  %81 = load float*, float** %output.addr, align 4
  %arrayidx62 = getelementptr inbounds float, float* %81, i32 -4
  %82 = load float, float* %arrayidx62, align 4
  %83 = load float*, float** %kernel.addr, align 4
  %arrayidx63 = getelementptr inbounds float, float* %83, i32 17
  %84 = load float, float* %arrayidx63, align 4
  %mul64 = fmul float %82, %84
  %85 = load float*, float** %output.addr, align 4
  %arrayidx65 = getelementptr inbounds float, float* %85, i32 -3
  %86 = load float, float* %arrayidx65, align 4
  %87 = load float*, float** %kernel.addr, align 4
  %arrayidx66 = getelementptr inbounds float, float* %87, i32 18
  %88 = load float, float* %arrayidx66, align 4
  %mul67 = fmul float %86, %88
  %add68 = fadd float %mul64, %mul67
  store float %add68, float* %xD, align 4
  %89 = load float*, float** %output.addr, align 4
  %arrayidx69 = getelementptr inbounds float, float* %89, i32 -2
  %90 = load float, float* %arrayidx69, align 4
  %91 = load float*, float** %kernel.addr, align 4
  %arrayidx70 = getelementptr inbounds float, float* %91, i32 19
  %92 = load float, float* %arrayidx70, align 4
  %mul71 = fmul float %90, %92
  %93 = load float*, float** %output.addr, align 4
  %arrayidx72 = getelementptr inbounds float, float* %93, i32 -1
  %94 = load float, float* %arrayidx72, align 4
  %95 = load float*, float** %kernel.addr, align 4
  %arrayidx73 = getelementptr inbounds float, float* %95, i32 20
  %96 = load float, float* %arrayidx73, align 4
  %mul74 = fmul float %94, %96
  %add75 = fadd float %mul71, %mul74
  store float %add75, float* %xH, align 4
  %97 = load float, float* %x1, align 4
  %98 = load float, float* %x5, align 4
  %add76 = fadd float %97, %98
  %99 = load float, float* %x9, align 4
  %add77 = fadd float %add76, %99
  %100 = load float, float* %xD, align 4
  %add78 = fadd float %add77, %100
  %101 = load float, float* %xH, align 4
  %add79 = fadd float %add78, %101
  store float %add79, float* %s2, align 4
  %102 = load float, float* %s1, align 4
  %103 = load float, float* %s2, align 4
  %sub = fsub float %102, %103
  %104 = load float*, float** %output.addr, align 4
  %arrayidx80 = getelementptr inbounds float, float* %104, i32 0
  store float %sub, float* %arrayidx80, align 4
  %105 = load float*, float** %output.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %105, i32 1
  store float* %incdec.ptr, float** %output.addr, align 4
  %106 = load float*, float** %input.addr, align 4
  %incdec.ptr81 = getelementptr inbounds float, float* %106, i32 1
  store float* %incdec.ptr81, float** %input.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @filterButter(float* %input, float* %output, i32 %nSamples, float* %kernel) #0 {
entry:
  %input.addr = alloca float*, align 4
  %output.addr = alloca float*, align 4
  %nSamples.addr = alloca i32, align 4
  %kernel.addr = alloca float*, align 4
  %s1 = alloca float, align 4
  %s2 = alloca float, align 4
  store float* %input, float** %input.addr, align 4
  store float* %output, float** %output.addr, align 4
  store i32 %nSamples, i32* %nSamples.addr, align 4
  store float* %kernel, float** %kernel.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %nSamples.addr, align 4
  %dec = add i32 %0, -1
  store i32 %dec, i32* %nSamples.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load float*, float** %input.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 -2
  %2 = load float, float* %arrayidx, align 4
  %3 = load float*, float** %kernel.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %3, i32 0
  %4 = load float, float* %arrayidx1, align 4
  %mul = fmul float %2, %4
  %5 = load float*, float** %input.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %5, i32 -1
  %6 = load float, float* %arrayidx2, align 4
  %7 = load float*, float** %kernel.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 2
  %8 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %6, %8
  %add = fadd float %mul, %mul4
  %9 = load float*, float** %input.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %9, i32 0
  %10 = load float, float* %arrayidx5, align 4
  %11 = load float*, float** %kernel.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %11, i32 4
  %12 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %10, %12
  %add8 = fadd float %add, %mul7
  store float %add8, float* %s1, align 4
  %13 = load float*, float** %output.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %13, i32 -2
  %14 = load float, float* %arrayidx9, align 4
  %15 = load float*, float** %kernel.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %15, i32 1
  %16 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float %14, %16
  %17 = load float*, float** %output.addr, align 4
  %arrayidx12 = getelementptr inbounds float, float* %17, i32 -1
  %18 = load float, float* %arrayidx12, align 4
  %19 = load float*, float** %kernel.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %19, i32 3
  %20 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %18, %20
  %add15 = fadd float %mul11, %mul14
  store float %add15, float* %s2, align 4
  %21 = load float, float* %s1, align 4
  %22 = load float, float* %s2, align 4
  %sub = fsub float %21, %22
  %23 = load float*, float** %output.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %23, i32 0
  store float %sub, float* %arrayidx16, align 4
  %24 = load float*, float** %output.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %24, i32 1
  store float* %incdec.ptr, float** %output.addr, align 4
  %25 = load float*, float** %input.addr, align 4
  %incdec.ptr17 = getelementptr inbounds float, float* %25, i32 1
  store float* %incdec.ptr17, float** %input.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.log10.f64(double) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #2

; Function Attrs: noinline nounwind optnone
define hidden float @GetTitleGain(%struct.replaygain_data* %rgData) #0 {
entry:
  %rgData.addr = alloca %struct.replaygain_data*, align 4
  %retval1 = alloca float, align 4
  %i = alloca i32, align 4
  store %struct.replaygain_data* %rgData, %struct.replaygain_data** %rgData.addr, align 4
  %0 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %A = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %0, i32 0, i32 18
  %arraydecay = getelementptr inbounds [12000 x i32], [12000 x i32]* %A, i32 0, i32 0
  %call = call float @analyzeResult(i32* %arraydecay, i32 12000)
  store float %call, float* %retval1, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %1, 12000
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %A2 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %2, i32 0, i32 18
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [12000 x i32], [12000 x i32]* %A2, i32 0, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %B = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %5, i32 0, i32 19
  %6 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [12000 x i32], [12000 x i32]* %B, i32 0, i32 %6
  %7 = load i32, i32* %arrayidx3, align 4
  %add = add i32 %7, %4
  store i32 %add, i32* %arrayidx3, align 4
  %8 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %A4 = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %8, i32 0, i32 18
  %9 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [12000 x i32], [12000 x i32]* %A4, i32 0, i32 %9
  store i32 0, i32* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc15, %for.end
  %11 = load i32, i32* %i, align 4
  %cmp7 = icmp ult i32 %11, 10
  br i1 %cmp7, label %for.body8, label %for.end17

for.body8:                                        ; preds = %for.cond6
  %12 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %routbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %12, i32 0, i32 10
  %13 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [2411 x float], [2411 x float]* %routbuf, i32 0, i32 %13
  store float 0.000000e+00, float* %arrayidx9, align 4
  %14 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %14, i32 0, i32 8
  %15 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [2411 x float], [2411 x float]* %rstepbuf, i32 0, i32 %15
  store float 0.000000e+00, float* %arrayidx10, align 4
  %16 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rinprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %16, i32 0, i32 6
  %17 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [20 x float], [20 x float]* %rinprebuf, i32 0, i32 %17
  store float 0.000000e+00, float* %arrayidx11, align 4
  %18 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %loutbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %18, i32 0, i32 4
  %19 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [2411 x float], [2411 x float]* %loutbuf, i32 0, i32 %19
  store float 0.000000e+00, float* %arrayidx12, align 4
  %20 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lstepbuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %20, i32 0, i32 2
  %21 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds [2411 x float], [2411 x float]* %lstepbuf, i32 0, i32 %21
  store float 0.000000e+00, float* %arrayidx13, align 4
  %22 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %linprebuf = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %22, i32 0, i32 0
  %23 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [20 x float], [20 x float]* %linprebuf, i32 0, i32 %23
  store float 0.000000e+00, float* %arrayidx14, align 4
  br label %for.inc15

for.inc15:                                        ; preds = %for.body8
  %24 = load i32, i32* %i, align 4
  %inc16 = add i32 %24, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond6

for.end17:                                        ; preds = %for.cond6
  %25 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %totsamp = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %25, i32 0, i32 13
  store i32 0, i32* %totsamp, align 4
  %26 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %rsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %26, i32 0, i32 15
  store double 0.000000e+00, double* %rsum, align 8
  %27 = load %struct.replaygain_data*, %struct.replaygain_data** %rgData.addr, align 4
  %lsum = getelementptr inbounds %struct.replaygain_data, %struct.replaygain_data* %27, i32 0, i32 14
  store double 0.000000e+00, double* %lsum, align 8
  %28 = load float, float* %retval1, align 4
  ret float %28
}

; Function Attrs: noinline nounwind optnone
define internal float @analyzeResult(i32* %Array, i32 %len) #0 {
entry:
  %retval = alloca float, align 4
  %Array.addr = alloca i32*, align 4
  %len.addr = alloca i32, align 4
  %elems = alloca i32, align 4
  %upper = alloca i32, align 4
  %sum = alloca i32, align 4
  %i = alloca i32, align 4
  store i32* %Array, i32** %Array.addr, align 4
  store i32 %len, i32* %len.addr, align 4
  store i32 0, i32* %elems, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %len.addr, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32*, i32** %Array.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %2, i32 %3
  %4 = load i32, i32* %arrayidx, align 4
  %5 = load i32, i32* %elems, align 4
  %add = add i32 %5, %4
  store i32 %add, i32* %elems, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i32, i32* %elems, align 4
  %cmp1 = icmp eq i32 %7, 0
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  store float -2.460100e+04, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.end
  %8 = load i32, i32* %elems, align 4
  %conv = uitofp i32 %8 to double
  %mul = fmul double %conv, 0x3FA99999999999A0
  %9 = call double @llvm.ceil.f64(double %mul)
  %conv2 = fptoui double %9 to i32
  store i32 %conv2, i32* %upper, align 4
  store i32 0, i32* %sum, align 4
  %10 = load i32, i32* %len.addr, align 4
  store i32 %10, i32* %i, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %if.end12, %if.end
  %11 = load i32, i32* %i, align 4
  %dec = add i32 %11, -1
  store i32 %dec, i32* %i, align 4
  %cmp4 = icmp ugt i32 %11, 0
  br i1 %cmp4, label %for.body6, label %for.end13

for.body6:                                        ; preds = %for.cond3
  %12 = load i32*, i32** %Array.addr, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %12, i32 %13
  %14 = load i32, i32* %arrayidx7, align 4
  %15 = load i32, i32* %sum, align 4
  %add8 = add i32 %15, %14
  store i32 %add8, i32* %sum, align 4
  %16 = load i32, i32* %sum, align 4
  %17 = load i32, i32* %upper, align 4
  %cmp9 = icmp uge i32 %16, %17
  br i1 %cmp9, label %if.then11, label %if.end12

if.then11:                                        ; preds = %for.body6
  br label %for.end13

if.end12:                                         ; preds = %for.body6
  br label %for.cond3

for.end13:                                        ; preds = %if.then11, %for.cond3
  %18 = load i32, i32* %i, align 4
  %conv14 = uitofp i32 %18 to float
  %div = fdiv float %conv14, 1.000000e+02
  %sub = fsub float 0x4050347AE0000000, %div
  store float %sub, float* %retval, align 4
  br label %return

return:                                           ; preds = %for.end13, %if.then
  %19 = load float, float* %retval, align 4
  ret float %19
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.ceil.f64(double) #3

attributes #0 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn writeonly }
attributes #2 = { argmemonly nounwind willreturn }
attributes #3 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
